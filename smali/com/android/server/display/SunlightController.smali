.class public Lcom/android/server/display/SunlightController;
.super Ljava/lang/Object;
.source "SunlightController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/SunlightController$Callback;,
        Lcom/android/server/display/SunlightController$SunlightModeHandler;,
        Lcom/android/server/display/SunlightController$NotificationHelper;,
        Lcom/android/server/display/SunlightController$SettingsObserver;,
        Lcom/android/server/display/SunlightController$ScreenOnReceiver;,
        Lcom/android/server/display/SunlightController$ScreenHangUpReceiver;,
        Lcom/android/server/display/SunlightController$UserSwitchObserver;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field private static final ENABLE_SENSOR_REASON_DEFAULT:Ljava/lang/String; = "sunlight_mode"

.field private static final ENABLE_SENSOR_REASON_NOTIFICATION:Ljava/lang/String; = "prepare_for_notifaction"

.field private static final MSG_INITIALIZE:I = 0x4

.field private static final MSG_SCREEN_HANG_UP_RECEIVE:I = 0x3

.field private static final MSG_SCREEN_ON_OFF_RECEIVE:I = 0x2

.field private static final MSG_UPDATE_SUNLIGHT_MODE:I = 0x1

.field private static final RESET_USER_DISABLE_DURATION:I = 0x493e0

.field private static final SUNLIGHT_AMBIENT_LIGHT_HORIZON:I = 0x2710

.field private static final SUNLIGHT_LIGHT_SENSOR_RATE:I = 0xfa

.field private static final TAG:Ljava/lang/String; = "SunlightController"

.field private static final THRESHOLD_ENTER_SUNLIGHT_DURATION:I = 0x1388

.field private static final THRESHOLD_EXIT_SUNLIGHT_DURATION:I = 0x7d0

.field private static final THRESHOLD_SUNLIGHT_LUX:I = 0x2ee0

.field private static final THRESHOLD_SUNLIGHT_NIT_DEFAULT:F = 160.0f


# instance fields
.field private mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

.field private mAutoBrightnessSettingsEnable:Z

.field private mBelowThresholdNit:Z

.field private mCallback:Lcom/android/server/display/SunlightController$Callback;

.field private mContext:Landroid/content/Context;

.field private mCurrentAmbientLux:F

.field private mCurrentScreenBrightnessSettings:I

.field private mDisplayId:I

.field private mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

.field private mLastObservedLux:F

.field private mLastObservedLuxTime:J

.field private mLastScreenOffTime:J

.field private mLastSunlightSettingsEnable:Z

.field private mLightSensor:Landroid/hardware/Sensor;

.field private final mLightSensorListener:Landroid/hardware/SensorEventListener;

.field private mLowPowerState:Z

.field private mNotificationHelper:Lcom/android/server/display/SunlightController$NotificationHelper;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreparedForNotification:Z

.field private mScreenBrightnessDefaultSettings:I

.field private mScreenIsHangUp:Z

.field private mScreenOn:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSettingsObserver:Lcom/android/server/display/SunlightController$SettingsObserver;

.field private mSunlightModeActive:Z

.field private mSunlightModeDisabledByUser:Z

.field private mSunlightModeEnable:Z

.field private mSunlightSensorEnableTime:J

.field private mSunlightSensorEnabled:Z

.field private mSunlightSensorEnabledReason:Ljava/lang/String;

.field private mSunlightSettingsEnable:Z

.field private mThresholdSunlightNit:F


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/SunlightController;)Lcom/android/server/display/SunlightController$SunlightModeHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSunlightSensorEnabled(Lcom/android/server/display/SunlightController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$mhandleLightSensorEvent(Lcom/android/server/display/SunlightController;JF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/SunlightController;->handleLightSensorEvent(JF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregister(Lcom/android/server/display/SunlightController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->register()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAmbientLux(Lcom/android/server/display/SunlightController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateAmbientLux()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateHangUpState(Lcom/android/server/display/SunlightController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController;->updateHangUpState(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateLowPowerState(Lcom/android/server/display/SunlightController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateLowPowerState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateScreenState(Lcom/android/server/display/SunlightController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController;->updateScreenState(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSunlightModeSettings(Lcom/android/server/display/SunlightController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/display/SunlightController$Callback;Landroid/os/Looper;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callback"    # Lcom/android/server/display/SunlightController$Callback;
    .param p3, "looper"    # Landroid/os/Looper;
    .param p4, "displayId"    # I

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    .line 105
    const/high16 v0, 0x43200000    # 160.0f

    iput v0, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F

    .line 299
    new-instance v0, Lcom/android/server/display/SunlightController$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/SunlightController$1;-><init>(Lcom/android/server/display/SunlightController;)V

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    .line 110
    if-nez p4, :cond_0

    .line 114
    iput-object p1, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    .line 115
    iput-object p2, p0, Lcom/android/server/display/SunlightController;->mCallback:Lcom/android/server/display/SunlightController$Callback;

    .line 116
    iput p4, p0, Lcom/android/server/display/SunlightController;->mDisplayId:I

    .line 117
    new-instance v0, Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-direct {v0, p0, p3}, Lcom/android/server/display/SunlightController$SunlightModeHandler;-><init>(Lcom/android/server/display/SunlightController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    .line 118
    new-instance v0, Lcom/android/server/display/SunlightController$NotificationHelper;

    invoke-direct {v0, p0}, Lcom/android/server/display/SunlightController$NotificationHelper;-><init>(Lcom/android/server/display/SunlightController;)V

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mNotificationHelper:Lcom/android/server/display/SunlightController$NotificationHelper;

    .line 119
    new-instance v0, Landroid/hardware/SystemSensorManager;

    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-virtual {v2}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/hardware/SystemSensorManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mSensorManager:Landroid/hardware/SensorManager;

    .line 120
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mLightSensor:Landroid/hardware/Sensor;

    .line 121
    new-instance v0, Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v1, 0xfa

    const/16 v3, 0x2710

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/display/AmbientLightRingBuffer;-><init>(JI)V

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    .line 122
    new-instance v0, Lcom/android/server/display/SunlightController$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/SunlightController$SettingsObserver;-><init>(Lcom/android/server/display/SunlightController;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mSettingsObserver:Lcom/android/server/display/SunlightController$SettingsObserver;

    .line 123
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/display/SunlightController;->mPowerManager:Landroid/os/PowerManager;

    .line 124
    invoke-virtual {v0}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/android/server/display/SunlightController;->mScreenBrightnessDefaultSettings:I

    .line 125
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->sendEmptyMessage(I)Z

    .line 126
    return-void

    .line 111
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Sunlight mode can only be used on the default display."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private applyLightSensorMeasurement(JF)V
    .locals 3
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .line 367
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v1, 0x2710

    sub-long v1, p1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V

    .line 368
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/AmbientLightRingBuffer;->push(JF)V

    .line 370
    iput p3, p0, Lcom/android/server/display/SunlightController;->mLastObservedLux:F

    .line 371
    iput-wide p1, p0, Lcom/android/server/display/SunlightController;->mLastObservedLuxTime:J

    .line 372
    return-void
.end method

.method private clearAmbientLightRingBuffer()V
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->clear()V

    .line 485
    return-void
.end method

.method private handleLightSensorEvent(JF)V
    .locals 2
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .line 316
    sget-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleLightSensorEvent: lux = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SunlightController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->removeMessages(I)V

    .line 320
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/SunlightController;->applyLightSensorMeasurement(JF)V

    .line 321
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/SunlightController;->updateAmbientLux(J)V

    .line 322
    return-void
.end method

.method private nextEnterSunlightModeTransition(J)J
    .locals 6
    .param p1, "time"    # J

    .line 375
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    .line 376
    .local v0, "N":I
    move-wide v1, p1

    .line 377
    .local v1, "earliestValidTime":J
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 378
    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    const v5, 0x463b8000    # 12000.0f

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 379
    goto :goto_1

    .line 381
    :cond_0
    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v1

    .line 377
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 383
    .end local v3    # "i":I
    :cond_1
    :goto_1
    const-wide/16 v3, 0x1388

    add-long/2addr v3, v1

    return-wide v3
.end method

.method private nextExitSunlightModeTransition(J)J
    .locals 6
    .param p1, "time"    # J

    .line 387
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    .line 388
    .local v0, "N":I
    move-wide v1, p1

    .line 389
    .local v1, "earliestValidTime":J
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 390
    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    const v5, 0x463b8000    # 12000.0f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_0

    .line 391
    goto :goto_1

    .line 393
    :cond_0
    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v1

    .line 389
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 395
    .end local v3    # "i":I
    :cond_1
    :goto_1
    const-wide/16 v3, 0x7d0

    add-long/2addr v3, v1

    return-wide v3
.end method

.method private register()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerSettingsObserver()V

    .line 131
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerScreenOnReceiver()V

    .line 132
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerHangUpReceiver()V

    .line 133
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerUserSwitchObserver()V

    .line 134
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeSettings()V

    .line 135
    return-void
.end method

.method private registerHangUpReceiver()V
    .locals 4

    .line 165
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 166
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "miui.intent.action.HANG_UP_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/display/SunlightController$ScreenHangUpReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/display/SunlightController$ScreenHangUpReceiver;-><init>(Lcom/android/server/display/SunlightController;Lcom/android/server/display/SunlightController$ScreenHangUpReceiver-IA;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 168
    return-void
.end method

.method private registerScreenOnReceiver()V
    .locals 4

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 161
    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/display/SunlightController$ScreenOnReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/server/display/SunlightController$ScreenOnReceiver;-><init>(Lcom/android/server/display/SunlightController;Lcom/android/server/display/SunlightController$ScreenOnReceiver-IA;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 162
    return-void
.end method

.method private registerSettingsObserver()V
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 139
    const-string/jumbo v1, "sunlight_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSettingsObserver:Lcom/android/server/display/SunlightController$SettingsObserver;

    .line 138
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 142
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 143
    const-string v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSettingsObserver:Lcom/android/server/display/SunlightController$SettingsObserver;

    .line 142
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 146
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 147
    const-string v1, "screen_brightness"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSettingsObserver:Lcom/android/server/display/SunlightController$SettingsObserver;

    .line 146
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 150
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 151
    const-string v1, "low_power_level_state"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSettingsObserver:Lcom/android/server/display/SunlightController$SettingsObserver;

    .line 150
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 154
    return-void
.end method

.method private registerUserSwitchObserver()V
    .locals 3

    .line 172
    :try_start_0
    new-instance v0, Lcom/android/server/display/SunlightController$UserSwitchObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/SunlightController$UserSwitchObserver;-><init>(Lcom/android/server/display/SunlightController;Lcom/android/server/display/SunlightController$UserSwitchObserver-IA;)V

    .line 173
    .local v0, "observer":Lcom/android/server/display/SunlightController$UserSwitchObserver;
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    const-string v2, "SunlightController"

    invoke-interface {v1, v0, v2}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    .end local v0    # "observer":Lcom/android/server/display/SunlightController$UserSwitchObserver;
    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 177
    :goto_0
    return-void
.end method

.method private resetUserDisableTemporaryData()V
    .locals 2

    .line 476
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    if-eqz v0, :cond_0

    .line 477
    const-string v0, "SunlightController"

    const-string v1, "Reset user slide operation."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    .line 479
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z

    .line 481
    :cond_0
    return-void
.end method

.method private setLightSensorEnabled(Z)Z
    .locals 1
    .param p1, "enabled"    # Z

    .line 265
    const-string/jumbo v0, "sunlight_mode"

    invoke-direct {p0, p1, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabled(ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private setLightSensorEnabled(ZLjava/lang/String;)Z
    .locals 6
    .param p1, "enabled"    # Z
    .param p2, "reason"    # Ljava/lang/String;

    .line 269
    sget-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setLightSensorEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SunlightController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 273
    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabledReason:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 274
    iput-object p2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabledReason:Ljava/lang/String;

    .line 276
    :cond_1
    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    if-nez v2, :cond_2

    .line 277
    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    .line 278
    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/display/SunlightController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mLightSensor:Landroid/hardware/Sensor;

    iget-object v5, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-virtual {v2, v3, v4, v1, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnableTime:J

    .line 281
    return v0

    .line 283
    :cond_2
    return v1

    .line 285
    :cond_3
    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    if-eqz v2, :cond_4

    .line 286
    iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    .line 287
    iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z

    .line 288
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnableTime:J

    .line 289
    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-virtual {v2, v0}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->removeMessages(I)V

    .line 290
    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/display/SunlightController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 292
    invoke-direct {p0, v1}, Lcom/android/server/display/SunlightController;->setSunLightModeActive(Z)V

    .line 293
    return v0

    .line 295
    :cond_4
    return v1
.end method

.method private setLightSensorEnabledForNotification(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 217
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z

    if-eq p1, v0, :cond_0

    .line 218
    iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z

    .line 219
    const-string v0, "prepare_for_notifaction"

    invoke-direct {p0, p1, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabled(ZLjava/lang/String;)Z

    .line 221
    :cond_0
    return-void
.end method

.method private setSunLightModeActive(Z)V
    .locals 2
    .param p1, "active"    # Z

    .line 359
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeActive:Z

    if-eq p1, v0, :cond_0

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSunLightModeActive: active: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SunlightController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeActive:Z

    .line 362
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mCallback:Lcom/android/server/display/SunlightController$Callback;

    invoke-interface {v0, p1}, Lcom/android/server/display/SunlightController$Callback;->notifySunlightStateChange(Z)V

    .line 364
    :cond_0
    return-void
.end method

.method private shouldPrepareToNotify()V
    .locals 1

    .line 210
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mNotificationHelper:Lcom/android/server/display/SunlightController$NotificationHelper;

    invoke-static {v0}, Lcom/android/server/display/SunlightController$NotificationHelper;->-$$Nest$fgetmHasReachedLimitTimes(Lcom/android/server/display/SunlightController$NotificationHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 213
    .local v0, "enable":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabledForNotification(Z)V

    .line 214
    return-void
.end method

.method private updateAmbientLux()V
    .locals 5

    .line 325
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 326
    .local v0, "time":J
    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v3, 0x2710

    sub-long v3, v0, v3

    invoke-virtual {v2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V

    .line 327
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/SunlightController;->updateAmbientLux(J)V

    .line 328
    return-void
.end method

.method private updateAmbientLux(J)V
    .locals 10
    .param p1, "time"    # J

    .line 331
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F

    .line 332
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/SunlightController;->nextEnterSunlightModeTransition(J)J

    move-result-wide v0

    .line 333
    .local v0, "nextEnterSunlightModeTime":J
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/SunlightController;->nextExitSunlightModeTransition(J)J

    move-result-wide v2

    .line 335
    .local v2, "nextExitSunlightModeTime":J
    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z

    if-nez v4, :cond_1

    .line 336
    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mLowPowerState:Z

    if-nez v4, :cond_0

    .line 337
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateNotificationState()V

    .line 339
    :cond_0
    return-void

    .line 342
    :cond_1
    iget v4, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F

    const v5, 0x463b8000    # 12000.0f

    cmpl-float v6, v4, v5

    const/4 v7, 0x1

    if-ltz v6, :cond_2

    cmp-long v6, v0, p1

    if-gtz v6, :cond_2

    iget-boolean v6, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z

    if-eqz v6, :cond_2

    .line 344
    invoke-direct {p0, v7}, Lcom/android/server/display/SunlightController;->setSunLightModeActive(Z)V

    goto :goto_0

    .line 345
    :cond_2
    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    cmp-long v4, v2, p1

    if-gtz v4, :cond_3

    .line 346
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/android/server/display/SunlightController;->setSunLightModeActive(Z)V

    .line 348
    :cond_3
    :goto_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 350
    .local v4, "nextTransitionTime":J
    cmp-long v6, v4, p1

    if-lez v6, :cond_4

    move-wide v8, v4

    goto :goto_1

    :cond_4
    const-wide/16 v8, 0x3e8

    add-long/2addr v8, p1

    :goto_1
    move-wide v4, v8

    .line 351
    sget-boolean v6, Lcom/android/server/display/SunlightController;->DEBUG:Z

    if-eqz v6, :cond_5

    .line 352
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "updateAmbientLux: Scheduling lux update for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 353
    invoke-static {v4, v5}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 352
    const-string v8, "SunlightController"

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_5
    iget-object v6, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-virtual {v6, v7, v4, v5}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->sendEmptyMessageAtTime(IJ)Z

    .line 356
    return-void
.end method

.method private updateHangUpState(Z)V
    .locals 1
    .param p1, "screenIsHangUp"    # Z

    .line 438
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z

    if-eq p1, v0, :cond_0

    .line 439
    iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z

    .line 440
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z

    .line 442
    :cond_0
    return-void
.end method

.method private updateLowPowerState()V
    .locals 5

    .line 462
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    .line 463
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 462
    const-string v1, "low_power_level_state"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mLowPowerState:Z

    .line 464
    const-string/jumbo v3, "sunlight_mode"

    if-eqz v0, :cond_1

    .line 465
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mLastSunlightSettingsEnable:Z

    .line 466
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 470
    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mLastSunlightSettingsEnable:Z

    if-eqz v4, :cond_2

    move v2, v1

    goto :goto_1

    .line 471
    :cond_2
    nop

    .line 469
    :goto_1
    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 473
    :goto_2
    return-void
.end method

.method private updateNotificationState()V
    .locals 2

    .line 246
    iget v0, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F

    const v1, 0x463b8000    # 12000.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mNotificationHelper:Lcom/android/server/display/SunlightController$NotificationHelper;

    invoke-static {v0}, Lcom/android/server/display/SunlightController$NotificationHelper;->-$$Nest$mshowNotificationIfNecessary(Lcom/android/server/display/SunlightController$NotificationHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabledForNotification(Z)V

    .line 251
    :cond_0
    return-void
.end method

.method private updateScreenState(Z)V
    .locals 6
    .param p1, "screenOn"    # Z

    .line 445
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    if-eq p1, v0, :cond_2

    .line 446
    iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    .line 447
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 448
    .local v0, "currentTime":J
    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    if-eqz v2, :cond_0

    .line 449
    iget-wide v2, p0, Lcom/android/server/display/SunlightController;->mLastScreenOffTime:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x493e0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 450
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->resetUserDisableTemporaryData()V

    goto :goto_0

    .line 453
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->clearAmbientLightRingBuffer()V

    .line 454
    iput-wide v0, p0, Lcom/android/server/display/SunlightController;->mLastScreenOffTime:J

    .line 456
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z

    .line 457
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->shouldPrepareToNotify()V

    .line 459
    .end local v0    # "currentTime":J
    :cond_2
    return-void
.end method

.method private updateSunlightModeCondition()Z
    .locals 5

    .line 224
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mCallback:Lcom/android/server/display/SunlightController$Callback;

    iget v1, p0, Lcom/android/server/display/SunlightController;->mCurrentScreenBrightnessSettings:I

    .line 225
    invoke-static {v1}, Lcom/android/internal/display/BrightnessSynchronizerStub;->brightnessIntToFloatForLowLevel(I)F

    move-result v1

    .line 224
    invoke-interface {v0, v1}, Lcom/android/server/display/SunlightController$Callback;->convertBrightnessToNit(F)F

    move-result v0

    .line 227
    .local v0, "currentScreenNit":F
    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z

    .line 228
    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    move v1, v2

    .line 230
    .local v1, "enable":Z
    sget-boolean v2, Lcom/android/server/display/SunlightController;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 231
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateSunlightModeCondition: mSunlightModeEnable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", enable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", mScreenOn: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", mSunlightModeDisabledByUser: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", mBelowThresholdNit: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", mScreenIsHangUp"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "SunlightController"

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_2
    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z

    if-eq v1, v2, :cond_3

    .line 238
    iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z

    .line 239
    iput-boolean v3, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z

    .line 240
    invoke-direct {p0, v1}, Lcom/android/server/display/SunlightController;->setLightSensorEnabled(Z)Z

    .line 242
    :cond_3
    return v1
.end method

.method private updateSunlightModeSettings()V
    .locals 6

    .line 180
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sunlight_mode"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 184
    .local v0, "sunlightSettingsChanged":Z
    :goto_0
    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "screen_brightness_mode"

    invoke-static {v4, v5, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    if-eqz v4, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z

    .line 188
    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    iget v4, p0, Lcom/android/server/display/SunlightController;->mScreenBrightnessDefaultSettings:I

    invoke-static {v1, v2, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/SunlightController;->mCurrentScreenBrightnessSettings:I

    .line 192
    if-nez v0, :cond_2

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    if-eqz v1, :cond_2

    .line 193
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->resetUserDisableTemporaryData()V

    .line 195
    :cond_2
    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    .line 196
    sget-boolean v1, Lcom/android/server/display/SunlightController;->DEBUG:Z

    if-eqz v1, :cond_3

    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateSunlightModeSettings: mSunlightSettingsEnable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mAutoBrightnessSettingsEnable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mCurrentScreenBrightnessSettings="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/SunlightController;->mCurrentScreenBrightnessSettings:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SunlightController"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_3
    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mCallback:Lcom/android/server/display/SunlightController$Callback;

    iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    invoke-interface {v1, v2}, Lcom/android/server/display/SunlightController$Callback;->notifySunlightModeChanged(Z)V

    .line 204
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z

    move-result v1

    if-nez v1, :cond_4

    .line 205
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->shouldPrepareToNotify()V

    .line 207
    :cond_4
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 690
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 691
    const-string v0, "Sunlight Controller Configuration:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSunlightSettingsEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 693
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSunlightSensorEnableTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnableTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLastObservedLux="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SunlightController;->mLastObservedLux:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLastObservedLuxTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/SunlightController;->mLastObservedLuxTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentAmbientLux="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSunlightSensorEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 698
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    if-eqz v0, :cond_0

    .line 699
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSunlightSensorEnabledReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabledReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 701
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBelowThresholdNit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 702
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSunlightModeActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeActive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 703
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSunlightModeDisabledByUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 704
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAmbientLightRingBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mScreenIsHangUp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 706
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mThresholdSunlightNit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 707
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mNotificationHelper:Lcom/android/server/display/SunlightController$NotificationHelper;

    invoke-static {v0, p1}, Lcom/android/server/display/SunlightController$NotificationHelper;->-$$Nest$mdump(Lcom/android/server/display/SunlightController$NotificationHelper;Ljava/io/PrintWriter;)V

    .line 708
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_SC:Z

    sput-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z

    .line 709
    return-void
.end method

.method public isSunlightModeDisabledByUser()Z
    .locals 1

    .line 407
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    return v0
.end method

.method public setSunlightModeDisabledByUserTemporary()V
    .locals 2

    .line 399
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    if-nez v0, :cond_0

    .line 400
    const-string v0, "SunlightController"

    const-string v1, "Disable sunlight mode temporarily due to user slide bar."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z

    .line 402
    invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z

    .line 404
    :cond_0
    return-void
.end method

.method public updateAmbientLightSensor(Landroid/hardware/Sensor;)V
    .locals 5
    .param p1, "sensor"    # Landroid/hardware/Sensor;

    .line 254
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mLightSensor:Landroid/hardware/Sensor;

    if-eq v0, p1, :cond_0

    .line 255
    iput-object p1, p0, Lcom/android/server/display/SunlightController;->mLightSensor:Landroid/hardware/Sensor;

    .line 256
    iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 258
    iget-object v0, p0, Lcom/android/server/display/SunlightController;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/SunlightController;->mLightSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/display/SunlightController;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/server/display/SunlightController;->mHandler:Lcom/android/server/display/SunlightController$SunlightModeHandler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 262
    :cond_0
    return-void
.end method

.method public updateThresholdSunlightNit(Ljava/lang/Float;)V
    .locals 1
    .param p1, "thresholdSunlightNit"    # Ljava/lang/Float;

    .line 558
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F

    .line 559
    return-void
.end method
