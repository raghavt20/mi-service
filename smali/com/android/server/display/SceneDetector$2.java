class com.android.server.display.SceneDetector$2 implements android.content.ServiceConnection {
	 /* .source "SceneDetector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SceneDetector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.SceneDetector this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Ai4HffwdpzX-fRkOxZv5PbR9YiA ( com.android.server.display.SceneDetector$2 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/SceneDetector$2;->lambda$onServiceDisconnected$1()V */
return;
} // .end method
public static void $r8$lambda$zPRHIHqywu-keQWLmenut1qdopk ( com.android.server.display.SceneDetector$2 p0, android.os.IBinder p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector$2;->lambda$onServiceConnected$0(Landroid/os/IBinder;)V */
return;
} // .end method
 com.android.server.display.SceneDetector$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/SceneDetector; */
/* .line 230 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$onServiceConnected$0 ( android.os.IBinder p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "service" # Landroid/os/IBinder; */
/* .line 236 */
final String v0 = "SceneDetector"; // const-string v0, "SceneDetector"
try { // :try_start_0
	 v1 = this.this$0;
	 com.xiaomi.aon.IAONFlareService$Stub .asInterface ( p1 );
	 com.android.server.display.SceneDetector .-$$Nest$fputmAonFlareService ( v1,v2 );
	 /* .line 237 */
	 v1 = this.this$0;
	 int v2 = 1; // const/4 v2, 0x1
	 com.android.server.display.SceneDetector .-$$Nest$fputmServiceConnected ( v1,v2 );
	 /* .line 238 */
	 v1 = this.this$0;
	 com.android.server.display.SceneDetector .-$$Nest$fgetmDeathRecipient ( v1 );
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 239 */
	 final String v1 = "onServiceConnected: aon flare service connected."; // const-string v1, "onServiceConnected: aon flare service connected."
	 android.util.Slog .i ( v0,v1 );
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 242 */
	 /* .line 240 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 241 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 final String v2 = "onServiceConnected: aon flare service connect failed."; // const-string v2, "onServiceConnected: aon flare service connect failed."
	 android.util.Slog .e ( v0,v2 );
	 /* .line 243 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void lambda$onServiceDisconnected$1 ( ) { //synthethic
/* .locals 2 */
/* .line 250 */
final String v0 = "SceneDetector"; // const-string v0, "SceneDetector"
final String v1 = "onServiceDisconnected: aon flare service disconnected."; // const-string v1, "onServiceDisconnected: aon flare service disconnected."
android.util.Slog .i ( v0,v1 );
/* .line 251 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.display.SceneDetector .-$$Nest$fputmAonFlareService ( v0,v1 );
/* .line 252 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.SceneDetector .-$$Nest$fputmAonState ( v0,v1 );
/* .line 253 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.display.SceneDetector .-$$Nest$fputmServiceConnected ( v0,v1 );
/* .line 254 */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 234 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$fgetmSceneDetectorHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/SceneDetector$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/display/SceneDetector$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector$2;Landroid/os/IBinder;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 244 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 249 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$fgetmSceneDetectorHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/SceneDetector$2$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/SceneDetector$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/SceneDetector$2;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 255 */
return;
} // .end method
