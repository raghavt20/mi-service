.class public Lcom/android/server/display/DisplayPowerControllerImpl;
.super Ljava/lang/Object;
.source "DisplayPowerControllerImpl.java"

# interfaces
.implements Lcom/android/server/display/DisplayPowerControllerStub;
.implements Lcom/android/server/display/SunlightController$Callback;
.implements Lcom/android/server/display/MiuiDisplayCloudController$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;,
        Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;,
        Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;,
        Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;,
        Lcom/android/server/display/DisplayPowerControllerImpl$Injector;,
        Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;
    }
.end annotation


# static fields
.field private static BCBC_ENABLE:Z = false

.field private static final BCBC_STATE_DISABLE:I = 0x0

.field private static final BCBC_STATE_ENABLE:I = 0x1

.field private static final COEFFICIENT:F = 0.5f

.field private static final CURRENT_GRAYSCALE_UPDATE_DISABLED:I = 0x0

.field private static final CURRENT_GRAYSCALE_UPDATE_ENABLED:I = 0x1

.field private static final DATA_D:[F

.field private static final DELAY_TIME:J = 0xfa0L

.field private static final DISPLAY_DIM_STATE:I = 0x106

.field private static final DOLBY_PREVIEW_DEFAULT_BOOST_RATIO:F = 1.0f

.field private static final DOLBY_PREVIEW_MAX_BOOST_RATIO:F = 10.0f

.field private static final DOZE_HBM_NIT_DEFAULT:F = 60.0f

.field private static final DOZE_LBM_NIT_DEFAULT:F = 5.0f

.field private static final DOZE_LIGHT_LOW:I = 0x1

.field public static final EPSILON:I = 0x3

.field private static final HBM_MINIMUM_LUX:F = 6000.0f

.field private static final IS_FOLDABLE_DEVICE:Z

.field private static final KEY_CURTAIN_ANIM_ENABLED:Ljava/lang/String; = "curtain_anim_enabled"

.field private static final KEY_IS_DYNAMIC_LOCK_SCREEN_SHOW:Ljava/lang/String; = "is_dynamic_lockscreen_shown"

.field private static final KEY_SUNLIGHT_MODE_AVAILABLE:Ljava/lang/String; = "config_sunlight_mode_available"

.field private static final MAX_A:F

.field private static final MAX_DIFF:F

.field public static final MAX_GALLERY_HDR_FACTOR:F = 2.25f

.field private static final MAX_HBM_BRIGHTNESS_FOR_PEAK:F

.field private static final MAX_POWER_SAVE_MODE_NIT:F

.field public static final MIN_GALLERY_HDR_FACTOR:F = 1.0f

.field private static final MSG_UPDATE_CURRENT_GRAY_SCALE:I = 0x6

.field private static final MSG_UPDATE_DOLBY_STATE:I = 0x2

.field private static final MSG_UPDATE_FOREGROUND_APP:I = 0x4

.field private static final MSG_UPDATE_FOREGROUND_APP_SYNC:I = 0x3

.field private static final MSG_UPDATE_GRAY_SCALE:I = 0x1

.field private static final MSG_UPDATE_ROTATION:I = 0x7

.field private static final MSG_UPDATE_THERMAL_MAX_BRIGHTNESS:I = 0x5

.field private static final PACKAGE_DIM_SYSTEM:Ljava/lang/String; = "system"

.field private static final PEAK_BRIGHTNESS_AMBIENT_LUX_THRESHOLD:I

.field private static final PEAK_BRIGHTNESS_GRAY_SCALE_THRESHOLD:I

.field private static final SUPPORT_BCBC_BY_AMBIENT_LUX:Z

.field private static final SUPPORT_IDLE_DIM:Z

.field private static final SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z

.field private static final SYSTEM_RESOURCES:Landroid/content/res/Resources;

.field private static final TAG:Ljava/lang/String; = "DisplayPowerControllerImpl"

.field private static final TRANSACTION_NOTIFY_BRIGHTNESS:I = 0x7980

.field private static final TRANSACTION_NOTIFY_DIM:I = 0x7983

.field private static final V1:F

.field private static final V2:F

.field private static final mBCBCLuxThreshold:[F

.field private static final mBCBCNitDecreaseThreshold:[F

.field private static final mSupportGalleryHdr:Z


# instance fields
.field private final SUPPORT_DOLBY_VERSION_BRIGHTEN:Z

.field private final SUPPORT_HDR_HBM_BRIGHTEN:Z

.field private final SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

.field private final SUPPORT_MANUAL_DIMMING:Z

.field private mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mActualScreenOnBrightness:F

.field private mAppliedBcbc:Z

.field private mAppliedDimming:Z

.field private mAppliedLowPower:Z

.field private mAppliedMaxOprBrightness:F

.field private mAppliedScreenBrightnessOverride:Z

.field private mAppliedSunlightMode:Z

.field private mAutoBrightnessEnable:Z

.field private mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

.field private mBCBCState:I

.field private mBasedBrightness:F

.field private mBasedSdrBrightness:F

.field private mBatteryReceiver:Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;

.field private mBcbcBrightness:F

.field private mBootCompleted:Z

.field private mBrightnessABTester:Lcom/android/server/display/BrightnessABTester;

.field private mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

.field private mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

.field private mBrightnessRampAnimator:Lcom/android/server/display/RampAnimator$DualRampAnimator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/server/display/RampAnimator$DualRampAnimator<",
            "Lcom/android/server/display/DisplayPowerState;",
            ">;"
        }
    .end annotation
.end field

.field private mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

.field private mCloudListener:Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

.field private mColorInversionEnabled:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentBrightness:F

.field private mCurrentConditionId:I

.field private mCurrentDisplayPolicy:I

.field private mCurrentGalleryHdrBoostFactor:F

.field private mCurrentGrayScale:F

.field private mCurrentSdrBrightness:F

.field private mCurrentTemperature:F

.field private mCurtainAnimationAvailable:Z

.field private mCurtainAnimationEnabled:Z

.field private mDebug:Z

.field private mDesiredBrightness:F

.field private mDesiredBrightnessInt:I

.field private mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

.field private mDisplayFeatureManagerServicImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

.field private mDisplayId:I

.field private mDisplayMangerServiceImpl:Lcom/android/server/display/DisplayManagerServiceImpl;

.field private mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

.field private mDolbyPreviewBoostAvailable:Z

.field private mDolbyPreviewBoostRatio:F

.field private mDolbyPreviewEnable:Z

.field private mDolbyStateEnable:Z

.field private mDozeInLowBrightness:Z

.field private mDozeScreenBrightness:F

.field private mFolded:Ljava/lang/Boolean;

.field private mForegroundAppPackageName:Ljava/lang/String;

.field private final mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

.field private mGalleryHdrThrottled:Z

.field private mGrayBrightnessFactor:F

.field private mGrayScale:F

.field private mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

.field private mHbmController:Lcom/android/server/display/HighBrightnessModeController;

.field private mHdrStateListener:Lcom/android/server/display/HighBrightnessModeController$HdrStateListener;

.field private mISurfaceFlinger:Landroid/os/IBinder;

.field private mInitialBCBCParameters:Z

.field private mInjector:Lcom/android/server/display/DisplayPowerControllerImpl$Injector;

.field private mIsDynamicLockScreenShowing:Z

.field private mIsGalleryHdrEnable:Z

.field private mIsScreenOn:Z

.field private mIsSunlightModeEnable:Z

.field private mIsSupportManualBoostApp:Z

.field private mK1:F

.field private mK2:F

.field private mK3:F

.field private mK4:F

.field private mLastAnimating:Z

.field private mLastDisplayState:I

.field private mLastFoldedState:Z

.field private mLastManualBoostBrightness:F

.field private mLastMaxBrightness:F

.field private mLastSettingsBrightnessBeforeApplySunlight:F

.field private mLastSlowChange:Z

.field private mLastTemperature:F

.field private mLastTemporaryBrightness:F

.field private mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

.field private mLowBatteryLevelBrightnessThreshold:[I

.field private mLowBatteryLevelThreshold:[I

.field private mLowBatteryLimitBrightness:F

.field private mManualBrightnessBoostEnable:Z

.field private mMaxDozeBrightnessFloat:F

.field private mMaxManualBoostBrightness:F

.field private mMaxPowerSaveModeBrightness:F

.field private mMinDozeBrightnessFloat:F

.field protected mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

.field private mOprAmbientLuxThreshold:I

.field private mOprBrightnessControlAvailable:Z

.field private mOprGrayscaleThreshold:[I

.field private mOprMaxNitThreshold:I

.field private mOprNitThreshold:[I

.field private mOrientation:I

.field private mOutDoorHighTempState:Z

.field private mPendingForegroundAppPackageName:Ljava/lang/String;

.field private mPendingResetGrayscaleStateForOpr:Z

.field private mPendingShowCurtainAnimation:Z

.field private mPendingUpdateBrightness:Z

.field private mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreviousDisplayPolicy:I

.field private mRampRateController:Lcom/android/server/display/RampRateController;

.field private mRealtimeArrayD:[F

.field private mRealtimeMaxA:F

.field private mRealtimeMaxDiff:F

.field private mRotationListenerEnabled:Z

.field private mRotationWatcher:Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;

.field private mScreenBrightnessRangeMaximum:F

.field private mScreenBrightnessRangeMinimum:F

.field private mScreenGrayscaleState:Z

.field private mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

.field private mShouldDimming:Z

.field private mShowOutdoorHighTempToast:Z

.field private mSunlightController:Lcom/android/server/display/SunlightController;

.field private mSunlightModeActive:Z

.field private mSunlightModeAvailable:Z

.field private mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

.field private mSupportCustomBrightness:Z

.field private mSupportIndividualBrightness:Z

.field private mTargetBrightness:F

.field private mTargetSdrBrightness:F

.field private mTaskStackListener:Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;

.field private mThermalBrightnessCallback:Lcom/android/server/display/ThermalBrightnessController$Callback;

.field private mThermalBrightnessControlAvailable:Z

.field private mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

.field private mThermalMaxBrightness:F

.field private mThermalObserver:Lcom/android/server/display/ThermalObserver;

.field private mUpdateBrightnessAnimInfoEnable:Z

.field private mWms:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$9MdydEw6WUBFaa6WgULApqZMMv4(Lcom/android/server/display/DisplayPowerControllerImpl;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$updateOutdoorThermalAppCategoryList$2(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Ffw3zG_X6ZkLvOU37Xk8nKYUfLc(Lcom/android/server/display/DisplayPowerControllerImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$notifyOutDoorHighTempState$4(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$Qx_JfyyAhlDFM0JaQj_pe5qb60E(Lcom/android/server/display/DisplayPowerControllerImpl;IFZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$startDetailThermalUsageStatsOnThermalChanged$5(IFZ)V

    return-void
.end method

.method public static synthetic $r8$lambda$l09th6PYVtGhBJdqbyc0CsivaZk(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$init$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$vRZRNl1DftxwNyhEMYS6FMy4lOo(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$stop$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$wpIyozD0qhRbWzJcY53YXqpB4HI(Lcom/android/server/display/DisplayPowerControllerImpl;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->lambda$init$1(Ljava/lang/Boolean;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmActivityTaskManager(Lcom/android/server/display/DisplayPowerControllerImpl;)Landroid/app/IActivityTaskManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAutoBrightnessEnable(Lcom/android/server/display/DisplayPowerControllerImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCbmController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundAppPackageName(Lcom/android/server/display/DisplayPowerControllerImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOrientation(Lcom/android/server/display/DisplayPowerControllerImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRampRateController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/RampRateController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmThermalBrightnessController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/ThermalBrightnessController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmOrientation(Lcom/android/server/display/DisplayPowerControllerImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPendingForegroundAppPackageName(Lcom/android/server/display/DisplayPowerControllerImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyUpdateForegroundApp(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyUpdateForegroundApp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetBCBCState(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetScreenGrayscaleState(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetCurrentGrayScale(Lcom/android/server/display/DisplayPowerControllerImpl;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setCurrentGrayScale(F)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetGrayScale(Lcom/android/server/display/DisplayPowerControllerImpl;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setGrayScale(F)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAutoBrightnessMode(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAutoBrightnessMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateBatteryBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBatteryBrightness(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateColorInversionEnabled(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateColorInversionEnabled()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCurtainAnimationEnabled(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCurtainAnimationEnabled()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDolbyBrightnessIfNeeded(Lcom/android/server/display/DisplayPowerControllerImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyBrightnessIfNeeded(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateForegroundApp(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundApp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateForegroundAppSync(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundAppSync()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateThermalBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateThermalBrightness(F)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 81
    nop

    .line 82
    const-string v0, "ro.vendor.bcbc.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->BCBC_ENABLE:Z

    .line 83
    nop

    .line 84
    const-string v0, "persist.sys.muiltdisplay_type"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->IS_FOLDABLE_DEVICE:Z

    .line 87
    nop

    .line 88
    const-string v0, "ro.vendor.aod.brightness.cust"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z

    .line 176
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCLuxThreshold:[F

    .line 178
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCNitDecreaseThreshold:[F

    .line 180
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/DisplayPowerControllerImpl;->SYSTEM_RESOURCES:Landroid/content/res/Resources;

    .line 183
    nop

    .line 184
    const v2, 0x11030032

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 183
    invoke-static {v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v2

    sput-object v2, Lcom/android/server/display/DisplayPowerControllerImpl;->DATA_D:[F

    .line 187
    nop

    .line 188
    const v2, 0x11070025

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->V1:F

    .line 189
    nop

    .line 190
    const v2, 0x11070024

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->V2:F

    .line 193
    nop

    .line 194
    const v2, 0x11070020

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_DIFF:F

    .line 197
    nop

    .line 198
    const v2, 0x11070022

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_A:F

    .line 201
    nop

    .line 202
    const v2, 0x11070030

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_POWER_SAVE_MODE_NIT:F

    .line 205
    const v2, 0x110b001a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->PEAK_BRIGHTNESS_GRAY_SCALE_THRESHOLD:I

    .line 207
    const v2, 0x110b0009

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    sput v2, Lcom/android/server/display/DisplayPowerControllerImpl;->PEAK_BRIGHTNESS_AMBIENT_LUX_THRESHOLD:I

    .line 210
    const v2, 0x1107002e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    sput v0, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_HBM_BRIGHTNESS_FOR_PEAK:F

    .line 229
    nop

    .line 230
    const-string/jumbo v0, "support_bcbc_by_ambient_lux"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_BCBC_BY_AMBIENT_LUX:Z

    .line 281
    nop

    .line 282
    const-string/jumbo v0, "support_idle_dim"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_IDLE_DIM:Z

    .line 292
    nop

    .line 293
    const-string/jumbo v0, "support_gallery_hdr"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportGalleryHdr:Z

    .line 292
    return-void

    :array_0
    .array-data 4
        0x41200000    # 10.0f
        0x42c80000    # 100.0f
    .end array-data

    :array_1
    .array-data 4
        0x40a00000    # 5.0f
        0x41400000    # 12.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F

    .line 225
    sget-object v1, Lcom/android/server/display/DisplayPowerControllerImpl;->DATA_D:[F

    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeArrayD:[F

    .line 226
    sget v1, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_A:F

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxA:F

    .line 227
    sget v1, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_DIFF:F

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F

    .line 232
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    .line 233
    const/high16 v2, 0x7fc00000    # Float.NaN

    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F

    .line 235
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F

    .line 243
    nop

    .line 244
    const-string/jumbo v3, "support_dolby_version_brighten"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_DOLBY_VERSION_BRIGHTEN:Z

    .line 245
    nop

    .line 246
    const-string/jumbo v3, "support_hdr_hbm_brighten"

    invoke-static {v3, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_HDR_HBM_BRIGHTEN:Z

    .line 248
    nop

    .line 249
    const-string/jumbo v3, "support_manual_dimming"

    invoke-static {v3, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_DIMMING:Z

    .line 251
    nop

    .line 252
    const-string/jumbo v3, "support_manual_brightness_boost"

    invoke-static {v3, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

    .line 270
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastDisplayState:I

    .line 287
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F

    .line 288
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    .line 297
    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F

    .line 304
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    .line 305
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F

    .line 308
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F

    .line 309
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F

    .line 313
    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F

    .line 334
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    .line 375
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F

    .line 381
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$1;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

    .line 394
    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F

    .line 1004
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$3;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessCallback:Lcom/android/server/display/ThermalBrightnessController$Callback;

    .line 2076
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$5;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$5;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCloudListener:Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

    return-void
.end method

.method private adjustBrightnessByBattery(FLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 2
    .param p1, "brightness"    # F
    .param p2, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 2136
    move v0, p1

    .line 2137
    .local v0, "newBrightness":F
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2138
    cmpl-float v1, v0, p1

    if-eqz v1, :cond_0

    .line 2139
    const/16 v1, 0x80

    invoke-virtual {p2, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 2141
    :cond_0
    return v0
.end method

.method private adjustBrightnessByBcbc(FLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 4
    .param p1, "brightness"    # F
    .param p2, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 943
    move v0, p1

    .line 944
    .local v0, "newBrightness":F
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F

    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 945
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F

    invoke-direct {p0, p1, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateBrightnessBCBC(FF)F

    move-result v0

    .line 946
    cmpl-float v1, p1, v0

    const-string v2, "DisplayPowerControllerImpl"

    if-eqz v1, :cond_0

    .line 947
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z

    .line 948
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F

    .line 949
    const/16 v1, 0x10

    invoke-virtual {p2, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 950
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v1, :cond_1

    .line 951
    const-string v1, "Apply bcbc brightness."

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 953
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z

    if-eqz v1, :cond_1

    cmpl-float v1, p1, v0

    if-nez v1, :cond_1

    .line 954
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z

    .line 955
    const/high16 v3, 0x7fc00000    # Float.NaN

    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F

    .line 956
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBcbcRateModifier(Z)V

    .line 957
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v1, :cond_1

    .line 958
    const-string v1, "Exit bcbc brightness."

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    :cond_1
    :goto_0
    return v0
.end method

.method private adjustBrightnessByLux(FF)F
    .locals 8
    .param p1, "preBrightness"    # F
    .param p2, "currentBrightness"    # F

    .line 1153
    move v0, p2

    .line 1154
    .local v0, "outBrightness":F
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-nez v2, :cond_0

    goto/16 :goto_1

    .line 1157
    :cond_0
    invoke-virtual {v1, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToNits(F)F

    move-result v1

    .line 1158
    .local v1, "preNit":F
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    invoke-virtual {v2, p2}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToNits(F)F

    move-result v2

    .line 1159
    .local v2, "currentNit":F
    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F

    move-result v3

    .line 1161
    .local v3, "currentLux":F
    sget-object v4, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCLuxThreshold:[F

    const/4 v5, 0x0

    aget v6, v4, v5

    cmpg-float v6, v3, v6

    if-gez v6, :cond_1

    cmpl-float v6, v1, v2

    if-lez v6, :cond_1

    sub-float v6, v1, v2

    sget-object v7, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCNitDecreaseThreshold:[F

    aget v5, v7, v5

    cmpl-float v6, v6, v5

    if-lez v6, :cond_1

    .line 1163
    iget-object v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    sub-float v5, v1, v5

    invoke-virtual {v4, v5}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F

    move-result v0

    goto :goto_0

    .line 1164
    :cond_1
    const/4 v5, 0x1

    aget v4, v4, v5

    cmpg-float v4, v3, v4

    if-gez v4, :cond_2

    cmpl-float v4, v1, v2

    if-lez v4, :cond_2

    sub-float v4, v1, v2

    sget-object v6, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCNitDecreaseThreshold:[F

    aget v5, v6, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    .line 1166
    iget-object v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    sub-float v5, v1, v5

    invoke-virtual {v4, v5}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F

    move-result v0

    .line 1169
    :cond_2
    :goto_0
    iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v4, :cond_3

    .line 1170
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "adjustBrightnessByLux: currentLux: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", preNit: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", currentNit: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", currentBrightness: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DisplayPowerControllerImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    :cond_3
    return v0

    .line 1155
    .end local v1    # "preNit":F
    .end local v2    # "currentNit":F
    .end local v3    # "currentLux":F
    :cond_4
    :goto_1
    return v0
.end method

.method private adjustBrightnessByOpr(FLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 7
    .param p1, "brightness"    # F
    .param p2, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 1827
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    const-string v1, "DisplayPowerControllerImpl"

    if-nez v0, :cond_0

    .line 1828
    const-string v0, "adjustBrightnessByOpr: no valid display device config!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1829
    return p1

    .line 1833
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_1

    .line 1834
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F

    move-result v0

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1835
    .local v0, "shouldResetScreenGrayscaleState":Z
    :goto_0
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingResetGrayscaleStateForOpr:Z

    if-eq v2, v0, :cond_2

    .line 1836
    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingResetGrayscaleStateForOpr:Z

    .line 1837
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V

    .line 1841
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprMaxNitThreshold:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v2

    .line 1842
    .local v2, "maxOprBrightness":F
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v2, v3

    if-nez v3, :cond_3

    .line 1843
    return p1

    .line 1845
    :cond_3
    iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingResetGrayscaleStateForOpr:Z

    const/16 v4, 0x200

    if-nez v3, :cond_5

    cmpl-float v3, p1, v2

    if-lez v3, :cond_5

    .line 1846
    iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v3, :cond_4

    .line 1847
    const-string v3, "adjustBrightnessByOpr: constrain brightnesswhen current lux is below opr ambient lux threshold."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1850
    :cond_4
    invoke-virtual {p2, v4}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 1851
    return v2

    .line 1855
    :cond_5
    iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    invoke-direct {p0, v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->getRestrictedOprBrightness(F)F

    move-result v3

    .line 1856
    .local v3, "restrictedOprBrightness":F
    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1857
    return p1

    .line 1859
    :cond_6
    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    cmpl-float v5, v3, v5

    if-eqz v5, :cond_7

    .line 1860
    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    .line 1862
    :cond_7
    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    cmpl-float v5, p1, v5

    if-lez v5, :cond_9

    .line 1863
    iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v5, :cond_8

    .line 1864
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "adjustBrightnessByOpr: current brightness: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is constrained to target brightness: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    :cond_8
    invoke-virtual {p2, v4}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 1868
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    return v1

    .line 1870
    :cond_9
    return p1
.end method

.method private adjustBrightnessByPowerSaveMode(FLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 2
    .param p1, "brightness"    # F
    .param p2, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 1889
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1890
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F

    invoke-static {v0, p1}, Landroid/util/MathUtils;->min(FF)F

    move-result v0

    .line 1891
    .local v0, "newBrightness":F
    cmpl-float v1, v0, p1

    if-eqz v1, :cond_0

    .line 1892
    const/16 v1, 0x400

    invoke-virtual {p2, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 1893
    return v0

    .line 1896
    .end local v0    # "newBrightness":F
    :cond_0
    return p1
.end method

.method private adjustBrightnessByThermal(FZLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 8
    .param p1, "brightness"    # F
    .param p2, "isHdr"    # Z
    .param p3, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 919
    move v0, p1

    .line 921
    .local v0, "newBrightness":F
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z

    if-eqz v1, :cond_5

    .line 922
    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-eqz p2, :cond_3

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 923
    :cond_1
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_2

    .line 924
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_2

    .line 925
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    invoke-static {v1, p1}, Landroid/util/MathUtils;->min(FF)F

    move-result v0

    .line 928
    :cond_2
    cmpl-float v1, v0, p1

    if-eqz v1, :cond_3

    .line 929
    const/16 v1, 0x40

    invoke-virtual {p3, v1}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 934
    :cond_3
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_5

    if-nez p2, :cond_4

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v1

    if-nez v1, :cond_5

    .line 935
    :cond_4
    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentConditionId:I

    iget v6, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F

    iget-boolean v7, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOutDoorHighTempState:Z

    move-object v2, p0

    move v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/server/display/DisplayPowerControllerImpl;->startFullSceneThermalUsageStats(FFIFZ)V

    .line 939
    :cond_5
    return v0
.end method

.method private adjustBrightnessToPeak(FZLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 7
    .param p1, "brightness"    # F
    .param p2, "isHdrBrightness"    # Z
    .param p3, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 967
    const/high16 v0, 0x3f800000    # 1.0f

    .line 969
    .local v0, "currentMaxBrightness":F
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v1, :cond_4

    .line 970
    invoke-virtual {v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F

    move-result v1

    .line 971
    .local v1, "currentLux":F
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxHbmBrightnessForPeak()F

    move-result v0

    .line 973
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrVideo()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/android/server/display/DisplayPowerControllerImpl;->PEAK_BRIGHTNESS_AMBIENT_LUX_THRESHOLD:I

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    .line 976
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    sget v3, Lcom/android/server/display/DisplayPowerControllerImpl;->PEAK_BRIGHTNESS_GRAY_SCALE_THRESHOLD:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 978
    .local v2, "shouldApplyPeakBrightness":Z
    :goto_0
    const-string v3, ", current gray scale: "

    const-string v4, "DisplayPowerControllerImpl"

    if-eqz v2, :cond_1

    .line 980
    if-eqz p2, :cond_3

    .line 981
    const/high16 v0, 0x3f800000    # 1.0f

    .line 982
    const/16 v5, 0x100

    invoke-virtual {p3, v5}, Lcom/android/server/display/brightness/BrightnessReason;->addModifier(I)V

    .line 983
    iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v5, :cond_3

    .line 984
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Apply peak brightness, currentLux: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 990
    :cond_1
    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F

    cmpl-float v5, v5, v0

    if-lez v5, :cond_3

    .line 991
    iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v5, :cond_2

    .line 992
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exit peak brightness, currentLux: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    :cond_2
    const/high16 v3, 0x7fc00000    # Float.NaN

    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    .line 998
    :cond_3
    :goto_1
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F

    .line 1000
    .end local v1    # "currentLux":F
    .end local v2    # "shouldApplyPeakBrightness":Z
    :cond_4
    invoke-static {p1, v0}, Landroid/util/MathUtils;->min(FF)F

    move-result p1

    .line 1001
    return p1
.end method

.method private calculateBrightnessBCBC(FF)F
    .locals 7
    .param p1, "brightness"    # F
    .param p2, "grayScale"    # F

    .line 1112
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMaximum:F

    div-float v0, p1, v0

    .line 1114
    .local v0, "ratio":F
    sget v1, Lcom/android/server/display/DisplayPowerControllerImpl;->V2:F

    cmpl-float v2, p2, v1

    const/high16 v3, 0x3f800000    # 1.0f

    if-lez v2, :cond_3

    .line 1115
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeArrayD:[F

    const/4 v4, 0x4

    aget v4, v2, v4

    cmpl-float v5, v0, v4

    const/4 v6, 0x5

    if-lez v5, :cond_0

    aget v5, v2, v6

    cmpg-float v5, v0, v5

    if-gtz v5, :cond_0

    .line 1116
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK3:F

    sub-float v4, v0, v4

    mul-float/2addr v2, v4

    sub-float v1, p2, v1

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto/16 :goto_0

    .line 1117
    :cond_0
    aget v4, v2, v6

    cmpl-float v4, v0, v4

    const/4 v5, 0x6

    if-lez v4, :cond_1

    aget v4, v2, v5

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_1

    .line 1118
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F

    sub-float v4, p2, v1

    mul-float/2addr v2, v4

    sub-float v1, v3, v1

    mul-float/2addr v1, v0

    div-float/2addr v2, v1

    sub-float v1, v3, v2

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto/16 :goto_0

    .line 1119
    :cond_1
    aget v4, v2, v5

    cmpl-float v4, v0, v4

    if-lez v4, :cond_2

    const/4 v4, 0x7

    aget v2, v2, v4

    cmpg-float v4, v0, v2

    if-gez v4, :cond_2

    .line 1120
    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK4:F

    sub-float v2, v0, v2

    mul-float/2addr v4, v2

    sub-float v1, p2, v1

    mul-float/2addr v4, v1

    add-float/2addr v4, v3

    iput v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto :goto_0

    .line 1122
    :cond_2
    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto :goto_0

    .line 1124
    :cond_3
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-lez v1, :cond_7

    sget v1, Lcom/android/server/display/DisplayPowerControllerImpl;->V1:F

    cmpg-float v2, p2, v1

    if-gez v2, :cond_7

    .line 1125
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeArrayD:[F

    const/4 v4, 0x0

    aget v4, v2, v4

    cmpl-float v5, v0, v4

    const/4 v6, 0x1

    if-lez v5, :cond_4

    aget v5, v2, v6

    cmpg-float v5, v0, v5

    if-gtz v5, :cond_4

    .line 1126
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK1:F

    sub-float v4, v0, v4

    mul-float/2addr v2, v4

    sub-float v1, p2, v1

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    iput v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto :goto_0

    .line 1127
    :cond_4
    aget v4, v2, v6

    cmpl-float v4, v0, v4

    const/4 v5, 0x2

    if-lez v4, :cond_5

    aget v4, v2, v5

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_5

    .line 1128
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxA:F

    add-float v4, v2, v3

    div-float/2addr v2, v1

    mul-float/2addr v2, p2

    sub-float/2addr v4, v2

    iput v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto :goto_0

    .line 1129
    :cond_5
    aget v4, v2, v5

    cmpl-float v4, v0, v4

    if-lez v4, :cond_6

    const/4 v4, 0x3

    aget v2, v2, v4

    cmpg-float v4, v0, v2

    if-gez v4, :cond_6

    .line 1130
    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK2:F

    sub-float v2, v0, v2

    mul-float/2addr v4, v2

    sub-float v1, p2, v1

    mul-float/2addr v4, v1

    add-float/2addr v4, v3

    iput v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto :goto_0

    .line 1133
    :cond_6
    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    goto :goto_0

    .line 1136
    :cond_7
    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    .line 1139
    :goto_0
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    mul-float v2, p1, v1

    .line 1140
    .local v2, "outBrightness":F
    sget-boolean v4, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_BCBC_BY_AMBIENT_LUX:Z

    if-eqz v4, :cond_8

    cmpg-float v1, v1, v3

    if-gez v1, :cond_8

    .line 1141
    invoke-direct {p0, p1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByLux(FF)F

    move-result v2

    .line 1144
    :cond_8
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v1, :cond_9

    .line 1145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " grayScale = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " factor = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " inBrightness = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " outBrightness = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "DisplayPowerControllerImpl"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    :cond_9
    return v2
.end method

.method private calculateBrightnessForManualBoost(FZ)F
    .locals 5
    .param p1, "brightness"    # F
    .param p2, "isBoostEntering"    # Z

    .line 770
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v0

    iget v0, v0, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F

    .line 771
    .local v0, "transitionPoint":F
    if-eqz p2, :cond_0

    .line 773
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F

    sub-float v2, p1, v1

    sub-float v1, v0, v1

    div-float/2addr v2, v1

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    sub-float/2addr v1, v0

    mul-float/2addr v2, v1

    add-float/2addr v2, p1

    .local v2, "tempBrightness":F
    goto :goto_0

    .line 779
    .end local v2    # "tempBrightness":F
    :cond_0
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F

    sub-float v3, v0, v2

    mul-float/2addr v1, v3

    iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    sub-float v4, v3, v0

    mul-float/2addr v4, v2

    add-float/2addr v1, v4

    sub-float/2addr v3, v2

    div-float v2, v1, v3

    .line 784
    .restart local v2    # "tempBrightness":F
    :goto_0
    return v2
.end method

.method private calculateGalleryHdrBoostFactor(FF)F
    .locals 4
    .param p1, "hdrNit"    # F
    .param p2, "sdrNit"    # F

    .line 1327
    const/4 v0, 0x0

    cmpl-float v1, p1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz v1, :cond_1

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1331
    :cond_0
    div-float v0, p1, p2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    .line 1332
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1333
    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    .line 1335
    .local v0, "factor":F
    const/high16 v1, 0x40100000    # 2.25f

    invoke-static {v0, v2, v1}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v1

    return v1

    .line 1328
    .end local v0    # "factor":F
    :cond_1
    :goto_0
    return v2
.end method

.method private canApplyManualBrightnessBoost(F)V
    .locals 3
    .param p1, "brightness"    # F

    .line 739
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v0

    const-string v1, "DisplayPowerControllerImpl"

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBootCompleted:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    goto :goto_2

    .line 745
    :cond_0
    move v0, p1

    .line 746
    .local v0, "tempBrightness":F
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 747
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    .line 749
    invoke-direct {p0, v0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateBrightnessForManualBoost(FZ)F

    move-result v0

    .line 750
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    invoke-interface {v2, v0}, Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;->updateScreenBrightnessSettingDueToSunlight(F)V

    .line 751
    const-string v2, "Enter manual brightness boost."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 752
    :cond_1
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z

    move-result v2

    if-nez v2, :cond_2

    .line 753
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    .line 755
    invoke-direct {p0, v0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateBrightnessForManualBoost(FZ)F

    move-result v0

    .line 756
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    invoke-interface {v2, v0}, Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;->updateScreenBrightnessSettingDueToSunlight(F)V

    .line 757
    const-string v2, "Exit manual brightness boost."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    if-eqz v1, :cond_3

    .line 761
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F

    goto :goto_1

    .line 764
    :cond_3
    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F

    .line 766
    :goto_1
    return-void

    .line 742
    .end local v0    # "tempBrightness":F
    :cond_4
    :goto_2
    const-string v0, "Don\'t apply manual brightness boost because current device status is invalid."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    return-void
.end method

.method private computeBCBCAdjustmentParams()V
    .locals 8

    .line 1192
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxA:F

    neg-float v1, v0

    sget v2, Lcom/android/server/display/DisplayPowerControllerImpl;->V1:F

    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeArrayD:[F

    const/4 v4, 0x1

    aget v4, v3, v4

    const/4 v5, 0x0

    aget v5, v3, v5

    sub-float/2addr v4, v5

    mul-float/2addr v4, v2

    div-float/2addr v1, v4

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK1:F

    .line 1193
    const/4 v1, 0x3

    aget v1, v3, v1

    const/4 v4, 0x2

    aget v4, v3, v4

    sub-float/2addr v1, v4

    mul-float/2addr v2, v1

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK2:F

    .line 1194
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F

    neg-float v1, v0

    const/4 v2, 0x5

    aget v2, v3, v2

    sget v4, Lcom/android/server/display/DisplayPowerControllerImpl;->V2:F

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v6, v5, v4

    mul-float/2addr v6, v2

    const/4 v7, 0x4

    aget v7, v3, v7

    sub-float/2addr v2, v7

    mul-float/2addr v6, v2

    div-float/2addr v1, v6

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK3:F

    .line 1196
    const/4 v1, 0x6

    aget v1, v3, v1

    sub-float/2addr v5, v4

    mul-float/2addr v5, v1

    const/4 v2, 0x7

    aget v2, v3, v2

    sub-float/2addr v2, v1

    mul-float/2addr v5, v2

    div-float/2addr v0, v5

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mK4:F

    .line 1198
    return-void
.end method

.method private static getFloatArray(Landroid/content/res/TypedArray;)[F
    .locals 4
    .param p0, "array"    # Landroid/content/res/TypedArray;

    .line 1182
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    .line 1183
    .local v0, "length":I
    new-array v1, v0, [F

    .line 1184
    .local v1, "floatArray":[F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 1185
    const/high16 v3, 0x7fc00000    # Float.NaN

    invoke-virtual {p0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    aput v3, v1, v2

    .line 1184
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1187
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1188
    return-object v1
.end method

.method private getRestrictedOprBrightness(F)F
    .locals 3
    .param p1, "grayScale"    # F

    .line 1874
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprGrayscaleThreshold:[I

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprNitThreshold:[I

    if-eqz v1, :cond_3

    array-length v0, v0

    if-eqz v0, :cond_3

    array-length v0, v1

    if-nez v0, :cond_0

    goto :goto_2

    .line 1880
    :cond_0
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprGrayscaleThreshold:[I

    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 1881
    aget v1, v1, v0

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_1

    .line 1882
    goto :goto_1

    .line 1880
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1885
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprNitThreshold:[I

    aget v2, v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v1

    return v1

    .line 1877
    .end local v0    # "index":I
    :cond_3
    :goto_2
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method private getSceneState()I
    .locals 2

    .line 2417
    const/4 v0, -0x1

    .line 2418
    .local v0, "state":I
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v1, :cond_0

    .line 2419
    invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getCurrentSceneState()I

    move-result v0

    .line 2421
    :cond_0
    return v0
.end method

.method private isAllowedUseSunlightMode()Z
    .locals 1

    .line 607
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSunlightModeDisabledByUser()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isGrayScaleLegal(F)Z
    .locals 1
    .param p1, "grayScale"    # F

    .line 1031
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isHdrScene()Z
    .locals 2

    .line 1047
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v0, :cond_0

    .line 1048
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getHighBrightnessMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 1047
    :goto_1
    return v0
.end method

.method private isHdrVideo()Z
    .locals 1

    .line 1038
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v0, :cond_1

    .line 1039
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->isHdrLayerPresent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    .line 1040
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->isDolbyEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1038
    :goto_0
    return v0
.end method

.method private isInOutdoorCriticalTemperature()Z
    .locals 1

    .line 1414
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v0, :cond_0

    .line 1415
    invoke-virtual {v0}, Lcom/android/server/display/ThermalBrightnessController;->isInOutdoorCriticalTemperature()Z

    move-result v0

    return v0

    .line 1417
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isKeyguardOn()Z
    .locals 1

    .line 1794
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isKeyguardShowingAndNotOccluded()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isManualBrightnessBoostAppEnable()Z
    .locals 1

    .line 710
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    if-eqz v0, :cond_0

    .line 711
    invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->isManualBoostAppEnable()Z

    move-result v0

    return v0

    .line 713
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isOverrideBrightnessPolicyEnable()Z
    .locals 1

    .line 1384
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    if-eqz v0, :cond_0

    .line 1385
    invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->isOverrideBrightnessPolicyEnable()Z

    move-result v0

    return v0

    .line 1387
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isSunlightModeDisabledByUser()Z
    .locals 1

    .line 661
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightController:Lcom/android/server/display/SunlightController;

    invoke-virtual {v0}, Lcom/android/server/display/SunlightController;->isSunlightModeDisabledByUser()Z

    move-result v0

    return v0
.end method

.method private synthetic lambda$init$0()V
    .locals 0

    .line 491
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerForegroundAppUpdater()V

    .line 492
    return-void
.end method

.method private synthetic lambda$init$1(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "folded"    # Ljava/lang/Boolean;

    .line 496
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->setDeviceFolded(Z)V

    return-void
.end method

.method private synthetic lambda$notifyOutDoorHighTempState$4(Z)V
    .locals 0
    .param p1, "changed"    # Z

    .line 2226
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOutDoorHighTempState:Z

    return-void
.end method

.method private synthetic lambda$startDetailThermalUsageStatsOnThermalChanged$5(IFZ)V
    .locals 1
    .param p1, "conditionId"    # I
    .param p2, "temperature"    # F
    .param p3, "brightnessChanged"    # Z

    .line 2258
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentConditionId:I

    .line 2259
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemperature:F

    .line 2260
    iput p2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F

    .line 2261
    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p2, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->startAverageTemperatureStats(FZ)V

    .line 2262
    if-nez p3, :cond_1

    .line 2263
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->startDetailsThermalUsageStats(IF)V

    .line 2265
    :cond_1
    return-void
.end method

.method private synthetic lambda$stop$3()V
    .locals 1

    .line 2020
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    return-void
.end method

.method private synthetic lambda$updateOutdoorThermalAppCategoryList$2(Ljava/util/List;)V
    .locals 1
    .param p1, "outdoorThermalAppList"    # Ljava/util/List;

    .line 1807
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v0, :cond_0

    .line 1808
    invoke-virtual {v0, p1}, Lcom/android/server/display/ThermalBrightnessController;->updateOutdoorThermalAppCategoryList(Ljava/util/List;)V

    .line 1810
    :cond_0
    return-void
.end method

.method private loadSettings()V
    .locals 0

    .line 1644
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateColorInversionEnabled()V

    .line 1645
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAutoBrightnessMode()V

    .line 1646
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCurtainAnimationEnabled()V

    .line 1647
    return-void
.end method

.method private notifyUpdateForegroundApp()V
    .locals 1

    .line 2432
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 2433
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyUpdateForegroundApp()V

    .line 2435
    :cond_0
    return-void
.end method

.method private onAnimateValueChanged(Z)V
    .locals 2
    .param p1, "changed"    # Z

    .line 2497
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2498
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/display/RampRateController;->onAnimateValueChanged(ZZ)V

    .line 2500
    :cond_0
    return-void
.end method

.method private onBrightnessChanged(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 2491
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2492
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/display/RampRateController;->onBrightnessChanged(ZZ)V

    .line 2494
    :cond_0
    return-void
.end method

.method private recalculationForBCBC(F)V
    .locals 7
    .param p1, "coefficient"    # F

    .line 1614
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mInitialBCBCParameters:Z

    if-eqz v0, :cond_0

    .line 1615
    return-void

    .line 1617
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mInitialBCBCParameters:Z

    .line 1619
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeArrayD:[F

    array-length v2, v1

    const/4 v3, 0x4

    const/4 v4, 0x6

    if-ge v0, v2, :cond_1

    .line 1620
    sget-object v2, Lcom/android/server/display/DisplayPowerControllerImpl;->DATA_D:[F

    aget v2, v2, v0

    mul-float/2addr v2, p1

    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1621
    invoke-virtual {v2, v4, v3}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->floatValue()F

    move-result v2

    aput v2, v1, v0

    .line 1619
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1623
    .end local v0    # "i":I
    :cond_1
    sget v0, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_DIFF:F

    mul-float/2addr v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1624
    invoke-virtual {v0, v4, v3}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRealtimeMaxDiff:F

    .line 1625
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->computeBCBCAdjustmentParams()V

    .line 1626
    return-void
.end method

.method private registerBroadcastsReceiver()V
    .locals 5

    .line 2145
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelThreshold:[I

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelBrightnessThreshold:[I

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 2147
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver-IA;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBatteryReceiver:Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;

    .line 2148
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2149
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2150
    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 2151
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBatteryReceiver:Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;

    iget-object v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 2153
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private registerForegroundAppUpdater()V
    .locals 2

    .line 1655
    :try_start_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTaskStackListener:Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 1656
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundWindowListener:Lmiui/process/IForegroundWindowListener;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundWindowListener(Lmiui/process/IForegroundWindowListener;)V

    .line 1659
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundApp()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1662
    goto :goto_0

    .line 1660
    :catch_0
    move-exception v0

    .line 1663
    :goto_0
    return-void
.end method

.method private registerSettingsObserver()V
    .locals 5

    .line 570
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 571
    const-string v1, "accessibility_display_inversion_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    .line 570
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 573
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 574
    const-string v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    .line 573
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 576
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 577
    const-string v1, "curtain_anim_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    .line 576
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 579
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 580
    const-string v1, "is_dynamic_lockscreen_shown"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    .line 579
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 582
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->loadSettings()V

    .line 583
    return-void
.end method

.method private resetBCBCState()V
    .locals 1

    .line 1016
    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F

    .line 1017
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F

    .line 1018
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z

    .line 1019
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBCBCStateIfNeeded()V

    .line 1020
    return-void
.end method

.method private resetBcbcRateModifier(Z)V
    .locals 1
    .param p1, "appliedAutoBrightness"    # Z

    .line 2520
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2521
    if-nez p1, :cond_0

    .line 2522
    invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearBcbcModifier()V

    .line 2525
    :cond_0
    return-void
.end method

.method private resetRateModifierOnAnimateValueChanged()V
    .locals 1

    .line 2534
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2535
    invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearBcbcModifier()V

    .line 2536
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearThermalModifier()V

    .line 2538
    :cond_0
    return-void
.end method

.method private resetScreenGrayscaleState()V
    .locals 2

    .line 1023
    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    .line 1024
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastMaxBrightness:F

    .line 1025
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    .line 1026
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingUpdateBrightness:Z

    .line 1027
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateScreenGrayscaleStateIfNeeded()V

    .line 1028
    return-void
.end method

.method private resetThermalRateModifier()V
    .locals 1

    .line 2528
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2529
    invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearThermalModifier()V

    .line 2531
    :cond_0
    return-void
.end method

.method private sendSurfaceFlingerActualBrightness(I)V
    .locals 6
    .param p1, "brightness"    # I

    .line 788
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    const-string v1, "DisplayPowerControllerImpl"

    if-eqz v0, :cond_0

    .line 789
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendSurfaceFlingerActualBrightness, brightness = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mISurfaceFlinger:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    .line 792
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 793
    .local v0, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 794
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 796
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mISurfaceFlinger:Landroid/os/IBinder;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v5, 0x7980

    invoke-interface {v2, v5, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 801
    nop

    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 802
    goto :goto_2

    .line 801
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 798
    :catch_0
    move-exception v2

    .line 799
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "Failed to send brightness to SurfaceFlinger"

    invoke-static {v1, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 801
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 802
    throw v1

    .line 804
    .end local v0    # "data":Landroid/os/Parcel;
    :cond_1
    :goto_2
    return-void
.end method

.method private setCurrentGrayScale(F)V
    .locals 1
    .param p1, "grayScale"    # F

    .line 1482
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGrayScaleLegal(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 1483
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBrightnessForOpr(F)V

    .line 1484
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBrightnessForPeak(F)V

    .line 1486
    :cond_0
    return-void
.end method

.method private setDeviceFolded(Z)V
    .locals 2
    .param p1, "folded"    # Z

    .line 532
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mFolded:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 533
    return-void

    .line 535
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mFolded:Ljava/lang/Boolean;

    .line 536
    invoke-static {}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getInstance()Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->oneTrackFoldState(Z)V

    .line 537
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v0, :cond_1

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mFolded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mFolded:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisplayPowerControllerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    .line 541
    .local v0, "isInteractive":Z
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationEnabled:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsDynamicLockScreenShowing:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_2

    .line 543
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isFolded()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z

    if-nez v1, :cond_2

    .line 545
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z

    .line 547
    :cond_2
    return-void
.end method

.method private setGrayScale(F)V
    .locals 1
    .param p1, "grayScale"    # F

    .line 1469
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F

    .line 1470
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateGrayScale(F)V

    .line 1471
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1472
    return-void
.end method

.method private shouldManualBoostForCurrentApp()Z
    .locals 1

    .line 717
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isManualBrightnessBoostAppEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 718
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 720
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z

    return v0
.end method

.method private startAverageTemperatureStats(FZ)V
    .locals 1
    .param p1, "temperature"    # F
    .param p2, "needComputed"    # Z

    .line 2285
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2286
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteAverageTemperature(FZ)V

    .line 2288
    :cond_0
    return-void
.end method

.method private startDetailsThermalUsageStats(IF)V
    .locals 1
    .param p1, "conditionId"    # I
    .param p2, "temperature"    # F

    .line 2274
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2275
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteDetailThermalUsage(IF)V

    .line 2277
    :cond_0
    return-void
.end method

.method private startFullSceneThermalUsageStats(FFIFZ)V
    .locals 6
    .param p1, "brightness"    # F
    .param p2, "thermalBrightness"    # F
    .param p3, "currentConditionId"    # I
    .param p4, "temperature"    # F
    .param p5, "outdoorHighTempState"    # Z

    .line 2243
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2244
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->noteFullSceneThermalUsageStats(FFIFZ)V

    .line 2247
    :cond_0
    return-void
.end method

.method private startUpdateThermalStats(FZ)V
    .locals 3
    .param p1, "brightnessState"    # F
    .param p2, "isScreenOn"    # Z

    .line 2215
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_1

    .line 2216
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentTemperature:F

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemperature:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateThermalStats(FZFZ)V

    .line 2219
    :cond_1
    return-void
.end method

.method private updateAmbientLightSensor(Landroid/hardware/Sensor;)V
    .locals 1
    .param p1, "lightSensor"    # Landroid/hardware/Sensor;

    .line 596
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightController:Lcom/android/server/display/SunlightController;

    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {v0, p1}, Lcom/android/server/display/SunlightController;->updateAmbientLightSensor(Landroid/hardware/Sensor;)V

    .line 599
    :cond_0
    return-void
.end method

.method private updateAutoBrightnessMode()V
    .locals 4

    .line 1629
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "screen_brightness_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    .line 1633
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V

    .line 1634
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V

    .line 1636
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-nez v0, :cond_1

    .line 1637
    invoke-virtual {p0, v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBrightnessAnimInfoIfNeeded(Z)V

    goto :goto_1

    .line 1639
    :cond_1
    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    .line 1641
    :goto_1
    return-void
.end method

.method private updateBatteryBrightness(I)V
    .locals 4
    .param p1, "batteryLevel"    # I

    .line 2159
    const/4 v0, 0x0

    .line 2160
    .local v0, "index":I
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2161
    .local v1, "lowBatteryBrightness":F
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelThreshold:[I

    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget v2, v2, v0

    if-le p1, v2, :cond_0

    .line 2163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2165
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelBrightnessThreshold:[I

    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 2166
    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    aget v2, v2, v0

    int-to-float v2, v2

    invoke-virtual {v3, v2}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v1

    .line 2169
    :cond_1
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_2

    .line 2170
    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLimitBrightness:F

    .line 2171
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v2}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 2173
    :cond_2
    return-void
.end method

.method private updateBrightnessForOpr(F)V
    .locals 4
    .param p1, "grayScale"    # F

    .line 1498
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 1500
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F

    move-result v0

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1501
    .local v0, "updateBrightnessForOpr":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 1502
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->getRestrictedOprBrightness(F)F

    move-result v1

    .line 1503
    .local v1, "currentRestrictedBrightness":F
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->getRestrictedOprBrightness(F)F

    move-result v2

    .line 1504
    .local v2, "pendingRestrictedOprBrightness":F
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    .line 1505
    cmpl-float v3, v1, v2

    if-eqz v3, :cond_1

    .line 1506
    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v3}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1509
    .end local v1    # "currentRestrictedBrightness":F
    .end local v2    # "pendingRestrictedOprBrightness":F
    :cond_1
    return-void
.end method

.method private updateBrightnessForPeak(F)V
    .locals 2
    .param p1, "grayScale"    # F

    .line 1489
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    .line 1490
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/android/server/display/DisplayPowerControllerImpl;->PEAK_BRIGHTNESS_GRAY_SCALE_THRESHOLD:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1491
    .local v0, "updateBrightnessForPeak":Z
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingUpdateBrightness:Z

    if-eq v1, v0, :cond_1

    .line 1492
    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingUpdateBrightness:Z

    .line 1493
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1495
    :cond_1
    return-void
.end method

.method private updateColorInversionEnabled()V
    .locals 4

    .line 1205
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "accessibility_display_inversion_enabled"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mColorInversionEnabled:Z

    .line 1208
    return-void
.end method

.method private updateCurtainAnimationEnabled()V
    .locals 5

    .line 1817
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "curtain_anim_enabled"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x0

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationEnabled:Z

    .line 1819
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "is_dynamic_lockscreen_shown"

    invoke-static {v0, v4, v1, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    move v2, v1

    :goto_1
    iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsDynamicLockScreenShowing:Z

    .line 1821
    return-void
.end method

.method private updateDolbyBrightnessIfNeeded(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 1996
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_DOLBY_VERSION_BRIGHTEN:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z

    if-eq v0, p1, :cond_2

    .line 1997
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z

    .line 1998
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 1999
    invoke-virtual {v0, p1}, Lcom/android/server/display/RampRateController;->addHdrRateModifier(Z)V

    .line 2001
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z

    invoke-virtual {v0, v1}, Lcom/android/server/display/HighBrightnessModeController;->setDolbyEnable(Z)V

    .line 2002
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v0, :cond_1

    .line 2003
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V

    .line 2004
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V

    .line 2006
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 2007
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v0, :cond_2

    .line 2008
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyStateEnable:Z

    invoke-virtual {v0, v1}, Lcom/android/server/display/ThermalBrightnessController;->setDolbyEnabled(Z)V

    .line 2011
    :cond_2
    return-void
.end method

.method private updateDolbyState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 1512
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1513
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1514
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1515
    return-void
.end method

.method private updateForegroundApp()V
    .locals 2

    .line 1683
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$4;

    invoke-direct {v1, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$4;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1709
    return-void
.end method

.method private updateForegroundAppSync()V
    .locals 3

    .line 1715
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    .line 1716
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    .line 1717
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v2, :cond_0

    .line 1718
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I

    invoke-virtual {v1, v0, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomSceneState(Ljava/lang/String;I)V

    .line 1720
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v0, :cond_1

    .line 1721
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateBCBCStateIfNeeded()V

    .line 1722
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGameSceneEnable()V

    goto :goto_0

    .line 1724
    :cond_1
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateManualBrightnessBoostState()V

    .line 1726
    :goto_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateThermalBrightnessBoostState()V

    .line 1727
    return-void
.end method

.method private updateGameSceneEnable()V
    .locals 3

    .line 2089
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 2090
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    .line 2091
    invoke-virtual {v1}, Lcom/android/server/display/MiuiDisplayCloudController;->getTouchCoverProtectionGameList()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 2090
    invoke-virtual {v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateGameSceneEnable(Z)V

    .line 2093
    :cond_0
    return-void
.end method

.method private updateManualBrightnessBoostState()V
    .locals 2

    .line 724
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isManualBrightnessBoostAppEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 725
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    .line 726
    invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getManualBoostDisableAppList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 727
    .local v0, "isSupportManualBoostApp":Z
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z

    if-eq v0, v1, :cond_0

    .line 728
    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z

    .line 729
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 732
    .end local v0    # "isSupportManualBoostApp":Z
    :cond_0
    return-void
.end method

.method private updateThermalBrightness(F)V
    .locals 2
    .param p1, "thermalNit"    # F

    .line 1557
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-nez v0, :cond_0

    .line 1558
    const-string v0, "DisplayPowerControllerImpl"

    const-string/jumbo v1, "updateThermalBrightness: no valid display device config!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1559
    return-void

    .line 1561
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v0

    .line 1562
    .local v0, "thermalBrightness":F
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    .line 1563
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    .line 1564
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1565
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetThermalRateModifier()V

    .line 1567
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1569
    :cond_2
    return-void
.end method

.method private updateThermalBrightnessBoostState()V
    .locals 2

    .line 1900
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v0, :cond_0

    .line 1901
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateForegroundApp(Ljava/lang/String;)V

    .line 1903
    :cond_0
    return-void
.end method


# virtual methods
.method public adjustBrightness(FLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 3
    .param p1, "brightness"    # F
    .param p2, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 908
    move v0, p1

    .line 909
    .local v0, "newBrightness":F
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessToPeak(FZLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 910
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z

    if-eqz v2, :cond_0

    .line 911
    invoke-direct {p0, v0, v1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByThermal(FZLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 913
    :cond_0
    invoke-direct {p0, v0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByBattery(FLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 914
    return v0
.end method

.method public adjustSdrBrightness(FZLcom/android/server/display/brightness/BrightnessReason;Z)F
    .locals 4
    .param p1, "brightness"    # F
    .param p2, "useAutoBrightness"    # Z
    .param p3, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;
    .param p4, "appliedAutoBrightness"    # Z

    .line 865
    move v0, p1

    .line 866
    .local v0, "newBrightness":F
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->getDisplayPowerState()Lcom/android/server/display/DisplayPowerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerState;->getScreenState()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne v1, v3, :cond_5

    .line 867
    if-eqz p2, :cond_1

    .line 869
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v1

    if-nez v1, :cond_0

    .line 870
    invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByOpr(FLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 873
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isKeyguardOn()Z

    move-result v1

    if-nez v1, :cond_2

    .line 874
    invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByBcbc(FLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    goto :goto_0

    .line 878
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    .line 879
    invoke-virtual {v1}, Lcom/android/server/display/DisplayPowerController;->getScreenBrightnessSetting()F

    move-result v1

    .line 878
    invoke-virtual {p0, v1, p1, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->canApplyingSunlightBrightness(FFLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 882
    :cond_2
    :goto_0
    invoke-virtual {p3}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v1

    and-int/2addr v1, v3

    if-eqz v1, :cond_3

    .line 883
    invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByPowerSaveMode(FLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 885
    :cond_3
    invoke-direct {p0, v0, v2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessToPeak(FZLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 886
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z

    if-eqz v1, :cond_4

    .line 887
    invoke-direct {p0, v0, v2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByThermal(FZLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 888
    if-eqz p4, :cond_4

    cmpl-float v1, v0, p1

    if-eqz v1, :cond_4

    .line 889
    invoke-virtual {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateFastRateStatus(F)V

    .line 892
    :cond_4
    invoke-direct {p0, v0, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->adjustBrightnessByBattery(FLcom/android/server/display/brightness/BrightnessReason;)F

    move-result v0

    .line 895
    :cond_5
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F

    invoke-static {p1, v1}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result v1

    if-nez v1, :cond_6

    .line 896
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F

    .line 897
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetRateModifierOnAnimateValueChanged()V

    .line 898
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->onBrightnessChanged(Z)V

    goto :goto_1

    .line 900
    :cond_6
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->onBrightnessChanged(Z)V

    .line 902
    :goto_1
    invoke-direct {p0, p4}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBcbcRateModifier(Z)V

    .line 903
    return v0
.end method

.method protected appliedFastRate(FF)Z
    .locals 1
    .param p1, "currentBrightness"    # F
    .param p2, "targetBrightness"    # F

    .line 2513
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 2514
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->shouldUseFastRate(FF)Z

    move-result v0

    return v0

    .line 2516
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public canApplyingSunlightBrightness(FFLcom/android/server/display/brightness/BrightnessReason;)F
    .locals 5
    .param p1, "currentScreenBrightness"    # F
    .param p2, "brightness"    # F
    .param p3, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 613
    invoke-virtual {p3}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I

    move-result v0

    .line 614
    .local v0, "reasonModifier":I
    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_9

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I

    if-eqz v1, :cond_0

    goto/16 :goto_2

    .line 618
    :cond_0
    move v1, p2

    .line 619
    .local v1, "tempBrightness":F
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

    if-eqz v2, :cond_1

    .line 620
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->canApplyManualBrightnessBoost(F)V

    .line 622
    :cond_1
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z

    const-string v3, "DisplayPowerControllerImpl"

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAllowedUseSunlightMode()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 623
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v2}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 624
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_2

    .line 626
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    goto :goto_0

    .line 628
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v2}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v2

    iget v1, v2, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F

    goto :goto_0

    .line 631
    :cond_3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 633
    :goto_0
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z

    if-nez v2, :cond_4

    .line 634
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F

    .line 636
    :cond_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z

    .line 637
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    if-eqz v2, :cond_5

    .line 638
    invoke-interface {v2, v1}, Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;->updateScreenBrightnessSettingDueToSunlight(F)V

    .line 640
    :cond_5
    const/16 v2, 0xb

    invoke-virtual {p3, v2}, Lcom/android/server/display/brightness/BrightnessReason;->setReason(I)V

    .line 641
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v2, :cond_8

    .line 642
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updatePowerState: appling sunlight mode brightness.last brightness:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 645
    :cond_6
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z

    if-eqz v2, :cond_8

    .line 646
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z

    .line 648
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSunlightModeDisabledByUser()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    if-eqz v2, :cond_7

    .line 649
    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F

    invoke-interface {v2, v4}, Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;->updateScreenBrightnessSettingDueToSunlight(F)V

    .line 652
    :cond_7
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v2, :cond_8

    .line 653
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updatePowerState: exit sunlight mode brightness. reset brightness: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    :cond_8
    :goto_1
    return v1

    .line 616
    .end local v1    # "tempBrightness":F
    :cond_9
    :goto_2
    return p2
.end method

.method public clampDozeBrightness(F)F
    .locals 3
    .param p1, "dozeBrightness"    # F

    .line 2464
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2465
    return p1

    .line 2468
    :cond_0
    move v0, p1

    .line 2469
    .local v0, "newDozeBrightness":F
    sget-boolean v1, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z

    if-nez v1, :cond_2

    .line 2470
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    .line 2471
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxDozeBrightnessFloat:F

    goto :goto_0

    .line 2473
    :cond_1
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMinDozeBrightnessFloat:F

    .line 2476
    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v1, :cond_3

    .line 2477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clamp doze brightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DisplayPowerControllerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2480
    :cond_3
    return v0
.end method

.method public convertBrightnessToNit(F)F
    .locals 1
    .param p1, "brightness"    # F

    .line 680
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-nez v0, :cond_0

    .line 681
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0

    .line 683
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1907
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z

    if-eqz v0, :cond_0

    .line 1908
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightController:Lcom/android/server/display/SunlightController;

    invoke-virtual {v0, p1}, Lcom/android/server/display/SunlightController;->dump(Ljava/io/PrintWriter;)V

    .line 1909
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAppliedSunlightMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1910
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLastSettingsBrightnessBeforeApplySunlight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1911
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  SUPPORT_MANUAL_BRIGHTNESS_BOOST="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1912
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mMaxManualBoostBrightness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1913
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mManualBrightnessBoostEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1914
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLastManualBoostBrightness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastManualBoostBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1915
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIsSupportManualBoostApp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSupportManualBoostApp:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1917
    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1918
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BCBC_ENABLE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/server/display/DisplayPowerControllerImpl;->BCBC_ENABLE:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1919
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mGrayScale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1920
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mAppliedBcbc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedBcbc:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1921
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mBcbcBrightness="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBcbcBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1922
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mCurrentGrayScale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGrayScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1923
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mColorInversionEnabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mColorInversionEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1924
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z

    if-eqz v1, :cond_1

    .line 1925
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1926
    const-string v1, "Curtain animation state: "

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1927
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mCurtainAnimationAvailable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1928
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mCurtainAnimationEnabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1929
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mIsDynamicLockScreenShowing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsDynamicLockScreenShowing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1930
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mPendingShowCurtainAnimation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1933
    :cond_1
    sget-boolean v1, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportGalleryHdr:Z

    if-eqz v1, :cond_2

    .line 1934
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1935
    const-string v2, "Gallery Hdr Boost:"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1936
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  mSupportGalleryHdr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1937
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mIsGalleryHdrEnable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1938
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mGalleryHdrThrottled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1939
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mCurrentGalleryHdrBoostFactor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1941
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalObserver:Lcom/android/server/display/ThermalObserver;

    if-eqz v1, :cond_2

    .line 1942
    invoke-virtual {v1, p1}, Lcom/android/server/display/ThermalObserver;->dump(Ljava/io/PrintWriter;)V

    .line 1946
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z

    if-eqz v1, :cond_3

    .line 1947
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1948
    const-string v1, "Dolby Preview Brightness Boost:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1949
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mDolbyPreviewBoostAvailable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1950
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mDolbyPreviewEnable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1951
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mDolbyPreviewBoostRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1954
    :cond_3
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v1, :cond_4

    .line 1955
    invoke-virtual {v1, p1}, Lcom/android/server/display/ThermalBrightnessController;->dump(Ljava/io/PrintWriter;)V

    .line 1956
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mThermalMaxBrightness="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalMaxBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1957
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mBasedSdrBrightness="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedSdrBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1958
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mBasedBrightness="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1961
    :cond_4
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelThreshold:[I

    array-length v1, v1

    if-eqz v1, :cond_5

    .line 1962
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1963
    const-string v1, "Low Battery Level:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1964
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mLowBatteryLevelThreshold="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelThreshold:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1965
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mLowBatteryLevelBrightnessThreshold="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelBrightnessThreshold:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1968
    :cond_5
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z

    if-eqz v1, :cond_6

    .line 1969
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1970
    const-string v1, "Opr Brightness Control:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1971
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mOprBrightnessControlAvailable="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1972
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mOprGrayscaleThreshold="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprGrayscaleThreshold:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1973
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mOprNitThreshold="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprNitThreshold:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1974
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mOprAmbientLuxThreshold="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1975
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mAppliedMaxOprBrightness="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedMaxOprBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1976
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mOprMaxNitThreshold="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprMaxNitThreshold:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1979
    :cond_6
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v1, :cond_7

    .line 1980
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1981
    const-string v0, "Cbm Config: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1982
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->dump(Ljava/io/PrintWriter;)V

    .line 1985
    :cond_7
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_8

    .line 1986
    invoke-virtual {v0, p1}, Lcom/android/server/display/RampRateController;->dump(Ljava/io/PrintWriter;)V

    .line 1989
    :cond_8
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DPC:Z

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    .line 1990
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    if-eqz v0, :cond_9

    .line 1991
    invoke-virtual {v0, p1}, Lcom/android/server/display/MiuiDisplayCloudController;->dump(Ljava/io/PrintWriter;)V

    .line 1993
    :cond_9
    return-void
.end method

.method public getClampedBrightnessForPeak(F)F
    .locals 1
    .param p1, "brightnessValue"    # F

    .line 1783
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v0, :cond_0

    .line 1784
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getCurrentBrightnessMax()F

    move-result v0

    invoke-static {p1, v0}, Landroid/util/MathUtils;->min(FF)F

    move-result p1

    .line 1786
    :cond_0
    return p1
.end method

.method protected getCustomBrightness(FLjava/lang/String;IFFZ)F
    .locals 8
    .param p1, "lux"    # F
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "category"    # I
    .param p4, "oldAutoBrightness"    # F
    .param p5, "newAutoBrightness"    # F
    .param p6, "isManuallySet"    # Z

    .line 2338
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2339
    iget v6, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getCustomBrightness(FLjava/lang/String;IFFIZ)F

    move-result p5

    .line 2344
    :cond_0
    return p5
.end method

.method public getDolbyPreviewBoostRatio()F
    .locals 2

    .line 1235
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_1

    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    goto :goto_0

    .line 1240
    :cond_0
    return v0

    .line 1237
    :cond_1
    :goto_0
    const-string v0, "DisplayPowerControllerImpl"

    const-string v1, "getDolbyPreviewBoostRatio: current dolby preview boost ratio is invalid."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method public getDozeBrightnessThreshold()[F
    .locals 3

    .line 2487
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxDozeBrightnessFloat:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMinDozeBrightnessFloat:F

    aput v2, v0, v1

    return-object v0
.end method

.method protected getGalleryHdrBoostFactor(FF)F
    .locals 6
    .param p1, "sdrBacklight"    # F
    .param p2, "hdrBacklight"    # F

    .line 1293
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1295
    return v1

    .line 1297
    :cond_0
    const-string v0, "null"

    .line 1298
    .local v0, "tempReason":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v1, p2}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v1

    .line 1299
    .local v1, "hdrNit":F
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v2

    .line 1300
    .local v2, "sdrNit":F
    invoke-direct {p0, v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->calculateGalleryHdrBoostFactor(FF)F

    move-result v3

    .line 1302
    .local v3, "factor":F
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isEnterGalleryHdr()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1304
    const-string v0, "enter_gallery_hdr_boost"

    goto :goto_0

    .line 1305
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isExitGalleryHdr()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1307
    const-string v0, "exit_gallery_hdr_boost"

    goto :goto_0

    .line 1308
    :cond_2
    iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1310
    const-string v0, "enter_dim_state"

    .line 1312
    :cond_3
    :goto_0
    iput v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F

    .line 1313
    iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v4, :cond_4

    .line 1314
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getGalleryHdrBoostFactor: reason:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", hdrBrightness: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sdrBrightness: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mCurrentBrightness: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mCurrentSdrBrightness: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentSdrBrightness:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", hdrNit: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sdrNit: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", factor: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DisplayPowerControllerImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1323
    :cond_4
    return v3
.end method

.method getGrayBrightnessFactor()F
    .locals 1

    .line 1178
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGrayBrightnessFactor:F

    return v0
.end method

.method public getHbmDataMinimumLux()F
    .locals 2

    .line 2367
    const v0, 0x45bb8000    # 6000.0f

    .line 2368
    .local v0, "minimumLux":F
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->deviceSupportsHbm()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2370
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->getHbmData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v1

    iget v0, v1, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->minimumLux:F

    .line 2372
    :cond_0
    return v0
.end method

.method public getMaxHbmBrightnessForPeak()F
    .locals 1

    .line 1775
    sget v0, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_HBM_BRIGHTNESS_FOR_PEAK:F

    return v0
.end method

.method public getMaxManualBrightnessBoost()F
    .locals 3

    .line 702
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    const/high16 v1, -0x40800000    # -1.0f

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->shouldManualBoostForCurrentApp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    cmpl-float v2, v0, v1

    if-eqz v2, :cond_0

    .line 704
    return v0

    .line 706
    :cond_0
    return v1
.end method

.method public getMinBrightness()F
    .locals 2

    .line 2383
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-eqz v1, :cond_0

    .line 2384
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getCurrentBrightnessMin()F

    move-result v0

    return v0

    .line 2386
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getNormalMaxBrightness()F
    .locals 2

    .line 2376
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-eqz v1, :cond_0

    .line 2377
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getNormalBrightnessMax()F

    move-result v0

    return v0

    .line 2379
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getProximityNegativeDebounceDelay(I)I
    .locals 1
    .param p1, "delay"    # I

    .line 2573
    const/4 v0, 0x0

    return v0
.end method

.method public init(Lcom/android/server/display/DisplayPowerController;Landroid/content/Context;Landroid/os/Looper;IFFLcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/LogicalDisplay;Lcom/android/server/display/HighBrightnessModeController;)V
    .locals 19
    .param p1, "displayPowerController"    # Lcom/android/server/display/DisplayPowerController;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "looper"    # Landroid/os/Looper;
    .param p4, "displayId"    # I
    .param p5, "brightnessMin"    # F
    .param p6, "brightnessMax"    # F
    .param p7, "displayDeviceConfig"    # Lcom/android/server/display/DisplayDeviceConfig;
    .param p8, "logicalDisplay"    # Lcom/android/server/display/LogicalDisplay;
    .param p9, "hbmController"    # Lcom/android/server/display/HighBrightnessModeController;

    .line 403
    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move/from16 v12, p4

    iput-object v10, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    .line 404
    move-object/from16 v13, p1

    iput-object v13, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    .line 405
    iput v12, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I

    .line 406
    move-object/from16 v14, p7

    iput-object v14, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 407
    move-object/from16 v15, p8

    iput-object v15, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    .line 408
    move-object/from16 v8, p9

    iput-object v8, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    .line 409
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    instance-of v0, v0, Lcom/android/server/display/DisplayManagerServiceImpl;

    if-eqz v0, :cond_0

    .line 410
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl;

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayMangerServiceImpl:Lcom/android/server/display/DisplayManagerServiceImpl;

    .line 412
    :cond_0
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayMangerServiceImpl:Lcom/android/server/display/DisplayManagerServiceImpl;

    if-eqz v0, :cond_1

    iget v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I

    if-nez v1, :cond_1

    .line 413
    invoke-virtual {v0, v9}, Lcom/android/server/display/DisplayManagerServiceImpl;->setUpDisplayPowerControllerImpl(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 415
    :cond_1
    nop

    .line 416
    invoke-static {}, Lcom/android/server/display/DisplayFeatureManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayFeatureManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayFeatureManagerServicImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    .line 417
    iget v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I

    if-nez v0, :cond_2

    .line 418
    const-string v0, "config_sunlight_mode_available"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z

    .line 419
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 420
    const v1, 0x11050016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurtainAnimationAvailable:Z

    .line 421
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 422
    const v1, 0x11050087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z

    .line 423
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 424
    const v1, 0x1105006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z

    .line 425
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 426
    const v1, 0x110b001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I

    .line 427
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1103004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprGrayscaleThreshold:[I

    .line 429
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1103004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprNitThreshold:[I

    .line 431
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 432
    const v1, 0x110b001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprMaxNitThreshold:I

    .line 433
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 434
    const v1, 0x1107002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxManualBoostBrightness:F

    .line 436
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelThreshold:[I

    .line 438
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1103003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mLowBatteryLevelBrightnessThreshold:[I

    .line 441
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1105001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z

    .line 444
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-direct {v0, v9, v11}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Landroid/os/Looper;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    .line 445
    iget-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z

    if-eqz v0, :cond_3

    .line 446
    new-instance v0, Lcom/android/server/display/SunlightController;

    invoke-direct {v0, v10, v9, v11, v12}, Lcom/android/server/display/SunlightController;-><init>(Landroid/content/Context;Lcom/android/server/display/SunlightController$Callback;Landroid/os/Looper;I)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightController:Lcom/android/server/display/SunlightController;

    .line 448
    :cond_3
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mISurfaceFlinger:Landroid/os/IBinder;

    .line 449
    move/from16 v7, p5

    iput v7, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMinimum:F

    .line 450
    move/from16 v6, p6

    iput v6, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenBrightnessRangeMaximum:F

    .line 451
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v0

    iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxDozeBrightnessFloat:F

    .line 452
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v0

    iput v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMinDozeBrightnessFloat:F

    .line 454
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    iget-object v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-direct {v0, v9, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Landroid/os/Handler;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    .line 455
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerSettingsObserver()V

    .line 456
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 457
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;

    invoke-direct {v0, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mTaskStackListener:Lcom/android/server/display/DisplayPowerControllerImpl$TaskStackListenerImpl;

    .line 458
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 459
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mPowerManager:Landroid/os/PowerManager;

    .line 460
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 461
    new-instance v0, Lcom/android/server/display/RampRateController;

    iget-object v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-direct {v0, v10, v9, v1, v11}, Lcom/android/server/display/RampRateController;-><init>(Landroid/content/Context;Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayDeviceConfig;Landroid/os/Looper;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    .line 462
    if-nez v12, :cond_8

    .line 463
    new-instance v5, Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    .line 464
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getCurrentBrightnessMin()F

    move-result v3

    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    .line 465
    invoke-virtual {v0}, Lcom/android/server/display/HighBrightnessModeController;->getNormalBrightnessMax()F

    move-result v4

    .line 466
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getHbmDataMinimumLux()F

    move-result v16

    move-object v0, v5

    move-object/from16 v1, p2

    move-object/from16 v2, p7

    move-object v6, v5

    move/from16 v5, v16

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;-><init>(Landroid/content/Context;Lcom/android/server/display/DisplayDeviceConfig;FFF)V

    iput-object v6, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 467
    new-instance v0, Lcom/android/server/display/MiuiDisplayCloudController;

    invoke-direct {v0, v11, v9, v10}, Lcom/android/server/display/MiuiDisplayCloudController;-><init>(Landroid/os/Looper;Lcom/android/server/display/MiuiDisplayCloudController$Callback;Landroid/content/Context;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    .line 469
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11050033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportIndividualBrightness:Z

    .line 471
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11050017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportCustomBrightness:Z

    .line 473
    iget-boolean v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportIndividualBrightness:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_5

    .line 474
    :cond_4
    new-instance v6, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    iget-object v5, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 476
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getHbmDataMinimumLux()F

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getNormalMaxBrightness()F

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMinBrightness()F

    move-result v18

    move-object v0, v6

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p7

    move-object/from16 v4, p0

    move-object v11, v6

    move/from16 v6, v16

    move/from16 v7, v17

    move/from16 v8, v18

    invoke-direct/range {v0 .. v8}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/statistics/BrightnessDataProcessor;FFF)V

    iput-object v11, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    .line 477
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    invoke-virtual {v0, v11}, Lcom/android/server/display/MiuiDisplayCloudController;->addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V

    .line 478
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;

    const/4 v1, 0x0

    invoke-direct {v0, v9, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher-IA;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationWatcher:Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;

    .line 481
    :cond_5
    new-instance v0, Lcom/android/server/display/ThermalObserver;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, v9}, Lcom/android/server/display/ThermalObserver;-><init>(Landroid/os/Looper;Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalObserver:Lcom/android/server/display/ThermalObserver;

    .line 482
    iget-boolean v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z

    if-eqz v0, :cond_6

    .line 483
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController;

    iget-object v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    iget-object v2, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-virtual {v2}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessCallback:Lcom/android/server/display/ThermalBrightnessController$Callback;

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/android/server/display/ThermalBrightnessController;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/ThermalBrightnessController$Callback;Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    .line 484
    iget-object v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, v1}, Lcom/android/server/display/ThermalBrightnessController;->addThermalListener(Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V

    .line 485
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    iget-object v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    invoke-virtual {v0, v1}, Lcom/android/server/display/MiuiDisplayCloudController;->addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V

    .line 486
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    iget-object v1, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, v1}, Lcom/android/server/display/MiuiDisplayCloudController;->addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V

    .line 487
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;

    invoke-direct {v0, v10}, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;-><init>(Landroid/content/Context;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mInjector:Lcom/android/server/display/DisplayPowerControllerImpl$Injector;

    .line 490
    :cond_6
    iget-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z

    .line 493
    sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->IS_FOLDABLE_DEVICE:Z

    if-eqz v0, :cond_7

    .line 494
    const-class v0, Landroid/hardware/devicestate/DeviceStateManager;

    invoke-virtual {v10, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/devicestate/DeviceStateManager;

    .line 495
    .local v0, "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    new-instance v1, Landroid/os/HandlerExecutor;

    iget-object v2, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-direct {v1, v2}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    new-instance v2, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    new-instance v3, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda1;

    invoke-direct {v3, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    invoke-direct {v2, v10, v3}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V

    invoke-virtual {v0, v1, v2}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    .line 498
    .end local v0    # "deviceStateManager":Landroid/hardware/devicestate/DeviceStateManager;
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerBroadcastsReceiver()V

    .line 499
    new-instance v0, Lcom/android/server/display/BrightnessABTester;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    iget-object v3, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/display/BrightnessABTester;-><init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/server/display/statistics/BrightnessDataProcessor;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessABTester:Lcom/android/server/display/BrightnessABTester;

    .line 503
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->computeBCBCAdjustmentParams()V

    .line 506
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$2;

    invoke-direct {v0, v9}, Lcom/android/server/display/DisplayPowerControllerImpl$2;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    iput-object v0, v9, Lcom/android/server/display/DisplayPowerControllerImpl;->mHdrStateListener:Lcom/android/server/display/HighBrightnessModeController$HdrStateListener;

    .line 529
    return-void
.end method

.method public isAnimating()Z
    .locals 1

    .line 2425
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessRampAnimator:Lcom/android/server/display/RampAnimator$DualRampAnimator;

    if-eqz v0, :cond_0

    .line 2426
    invoke-virtual {v0}, Lcom/android/server/display/RampAnimator$DualRampAnimator;->isAnimating()Z

    move-result v0

    return v0

    .line 2428
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isBrightnessCurveOptimizePolicyDisable()Z
    .locals 1

    .line 2454
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    if-eqz v0, :cond_0

    .line 2455
    invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->isBrightnessCurveOptimizePolicyDisable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2454
    :goto_0
    return v0
.end method

.method public isColorInversionEnabled()Z
    .locals 1

    .line 1201
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mColorInversionEnabled:Z

    return v0
.end method

.method public isCurtainAnimationNeeded()Z
    .locals 1

    .line 551
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z

    return v0
.end method

.method public isDolbyPreviewEnable()Z
    .locals 1

    .line 1226
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z

    return v0
.end method

.method protected isEnterGalleryHdr()Z
    .locals 2

    .line 1274
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSlowChange:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetBrightness:F

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetSdrBrightness:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected isExitGalleryHdr()Z
    .locals 2

    .line 1283
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1284
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentGalleryHdrBoostFactor:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1283
    :goto_0
    return v0
.end method

.method public isFolded()Z
    .locals 1

    .line 562
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mFolded:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 563
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 566
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isGalleryHdrEnable()Z
    .locals 1

    .line 1268
    sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportGalleryHdr:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSupportManualBrightnessBoost()Z
    .locals 1

    .line 698
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

    return v0
.end method

.method public isSupportPeakBrightness()Z
    .locals 2

    .line 1771
    sget v0, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_HBM_BRIGHTNESS_FOR_PEAK:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected isTemporaryDimmingEnabled()Z
    .locals 2

    .line 2295
    const/4 v0, 0x0

    .line 2296
    .local v0, "enable":Z
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v1, :cond_0

    .line 2297
    invoke-virtual {v1}, Lcom/android/server/display/RampRateController;->isTemporaryDimming()Z

    move-result v0

    .line 2299
    :cond_0
    return v0
.end method

.method public mayBeReportUserDisableSunlightTemporary(F)V
    .locals 1
    .param p1, "tempBrightness"    # F

    .line 587
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeAvailable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedSunlightMode:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightController:Lcom/android/server/display/SunlightController;

    invoke-virtual {v0}, Lcom/android/server/display/SunlightController;->setSunlightModeDisabledByUserTemporary()V

    .line 590
    :cond_0
    return-void
.end method

.method public notifyAonFlareEvents(IF)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "preLux"    # F

    .line 2560
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2561
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyAonFlareEvents(IF)V

    .line 2563
    :cond_0
    return-void
.end method

.method public notifyBrightnessChangeIfNeeded(ZFZZFZFFZF)V
    .locals 22
    .param p1, "screenOn"    # Z
    .param p2, "brightness"    # F
    .param p3, "userInitiatedChange"    # Z
    .param p4, "useAutoBrightness"    # Z
    .param p5, "brightnessOverrideFromWindow"    # F
    .param p6, "lowPowerMode"    # Z
    .param p7, "ambientLux"    # F
    .param p8, "userDataPoint"    # F
    .param p9, "defaultConfig"    # Z
    .param p10, "actualBrightness"    # F

    .line 845
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v1, :cond_4

    .line 846
    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->getHighBrightnessMode()I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    .line 847
    invoke-virtual {v1}, Lcom/android/server/display/HighBrightnessModeController;->isDolbyEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move/from16 v17, v2

    goto :goto_0

    :cond_1
    move/from16 v17, v4

    .line 848
    .local v17, "isHdrLayer":Z
    :goto_0
    iget-boolean v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z

    if-nez v1, :cond_3

    iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPreviousDisplayPolicy:I

    if-ne v1, v3, :cond_2

    iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_2

    goto :goto_1

    :cond_2
    move/from16 v18, v4

    goto :goto_2

    :cond_3
    :goto_1
    move/from16 v18, v2

    .line 852
    .local v18, "isDimmingChanged":Z
    :goto_2
    iget-object v5, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-boolean v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z

    move/from16 v16, v1

    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 856
    invoke-virtual {v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getMainFastAmbientLux()F

    move-result v19

    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 857
    invoke-virtual {v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getAssistFastAmbientLux()F

    move-result v20

    .line 858
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getSceneState()I

    move-result v21

    .line 852
    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p10

    move/from16 v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    move/from16 v15, p9

    invoke-virtual/range {v5 .. v21}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyBrightnessEventIfNeeded(ZFFZZFZFFZZZZFFI)V

    .line 860
    .end local v17    # "isHdrLayer":Z
    .end local v18    # "isDimmingChanged":Z
    :cond_4
    return-void
.end method

.method public notifyDisableResetShortTermModel(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 1393
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 1394
    invoke-virtual {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyDisableResetShortTermModel(Z)V

    .line 1396
    :cond_0
    return-void
.end method

.method public notifyDisplaySwapFinished()V
    .locals 2

    .line 2584
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isFolded()Z

    move-result v0

    .line 2585
    .local v0, "foldedState":Z
    sget-boolean v1, Lcom/android/server/display/DisplayPowerControllerImpl;->IS_FOLDABLE_DEVICE:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastFoldedState:Z

    if-eq v0, v1, :cond_0

    .line 2586
    invoke-static {}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getInstance()Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->notifyDisplaySwapFinished()V

    .line 2587
    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastFoldedState:Z

    .line 2589
    :cond_0
    return-void
.end method

.method public notifyFocusedWindowChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "focusedPackageName"    # Ljava/lang/String;

    .line 2579
    invoke-static {}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getInstance()Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->notifyFocusedWindowChanged(Ljava/lang/String;)V

    .line 2580
    return-void
.end method

.method protected notifyOutDoorHighTempState(Z)V
    .locals 2
    .param p1, "changed"    # Z

    .line 2226
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Z)V

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z

    .line 2227
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v0, :cond_0

    .line 2228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyOutDoorState: mOutDoorHighTempState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisplayPowerControllerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2230
    :cond_0
    return-void
.end method

.method public notifySunlightModeChanged(Z)V
    .locals 1
    .param p1, "isSunlightModeEnable"    # Z

    .line 687
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_MANUAL_BRIGHTNESS_BOOST:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z

    if-eq v0, p1, :cond_1

    .line 689
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBootCompleted:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 690
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mManualBrightnessBoostEnable:Z

    .line 692
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsSunlightModeEnable:Z

    .line 693
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 695
    :cond_1
    return-void
.end method

.method public notifySunlightStateChange(Z)V
    .locals 2
    .param p1, "active"    # Z

    .line 666
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v0, :cond_0

    .line 667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifySunlightStateChange: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisplayPowerControllerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z

    .line 670
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    if-eqz v0, :cond_1

    .line 671
    invoke-interface {v0}, Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;->onSunlightStateChange()V

    .line 673
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v0, :cond_2

    .line 674
    invoke-virtual {v0, p1}, Lcom/android/server/display/ThermalBrightnessController;->setSunlightState(Z)V

    .line 676
    :cond_2
    return-void
.end method

.method public notifyThresholdSunlightNitChanged(F)V
    .locals 2
    .param p1, "thresholdSunlightNit"    # F

    .line 1400
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightController:Lcom/android/server/display/SunlightController;

    if-eqz v0, :cond_0

    .line 1401
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/display/SunlightController;->updateThresholdSunlightNit(Ljava/lang/Float;)V

    .line 1403
    :cond_0
    return-void
.end method

.method public notifyUpdateBrightness()V
    .locals 1

    .line 2566
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2567
    invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyUpdateBrightness()V

    .line 2569
    :cond_0
    return-void
.end method

.method public notifyUpdateBrightnessAnimInfo(FFF)V
    .locals 1
    .param p1, "currentBrightnessAnim"    # F
    .param p2, "brightnessAnim"    # F
    .param p3, "targetBrightnessAnim"    # F

    .line 2038
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mUpdateBrightnessAnimInfoEnable:Z

    if-eqz v0, :cond_0

    .line 2040
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isTemporaryDimmingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2041
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyUpdateBrightnessAnimInfo(FFF)V

    .line 2044
    :cond_0
    return-void
.end method

.method public notifyUpdateTempBrightnessTimeStamp(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 2053
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2054
    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyUpdateTempBrightnessTimeStampIfNeeded(Z)V

    .line 2056
    :cond_0
    return-void
.end method

.method public onAnimateValueChanged(F)V
    .locals 1
    .param p1, "animateValue"    # F

    .line 2550
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F

    invoke-static {p1, v0}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2551
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->onAnimateValueChanged(Z)V

    .line 2552
    return-void

    .line 2554
    :cond_0
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBasedBrightness:F

    .line 2555
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetRateModifierOnAnimateValueChanged()V

    .line 2556
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->onAnimateValueChanged(Z)V

    .line 2557
    return-void
.end method

.method public onAnimationEnd()V
    .locals 1

    .line 2542
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2543
    invoke-virtual {v0}, Lcom/android/server/display/RampRateController;->clearAllRateModifier()V

    .line 2545
    :cond_0
    return-void
.end method

.method public onBootCompleted()V
    .locals 1

    .line 2097
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBootCompleted:Z

    .line 2098
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2099
    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->onBootCompleted()V

    .line 2101
    :cond_0
    return-void
.end method

.method public onCurtainAnimationFinished()V
    .locals 1

    .line 556
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z

    if-eqz v0, :cond_0

    .line 557
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPendingShowCurtainAnimation:Z

    .line 559
    :cond_0
    return-void
.end method

.method public receiveNoticeFromDisplayPowerController(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 1447
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1455
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCurrentGrayScale(Landroid/os/Bundle;)V

    .line 1456
    goto :goto_0

    .line 1452
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyState(Landroid/os/Bundle;)V

    .line 1453
    goto :goto_0

    .line 1449
    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGrayScale(Landroid/os/Bundle;)V

    .line 1450
    nop

    .line 1460
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected registerRotationWatcher(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 2312
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportIndividualBrightness:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSupportCustomBrightness:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationWatcher:Lcom/android/server/display/DisplayPowerControllerImpl$RotationWatcher;

    if-eqz v0, :cond_2

    .line 2314
    const-string v1, "DisplayPowerControllerImpl"

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    .line 2315
    iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z

    if-nez v3, :cond_2

    .line 2316
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z

    .line 2317
    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v3, v0, v2}, Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;I)I

    .line 2318
    const-string v0, "Register rotation listener."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2321
    :cond_1
    iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z

    if-eqz v3, :cond_2

    .line 2322
    iput-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRotationListenerEnabled:Z

    .line 2323
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowManagerService;->removeRotationWatcher(Landroid/view/IRotationWatcher;)V

    .line 2324
    const-string v0, "Unregister rotation listener."

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2328
    :cond_2
    :goto_0
    return-void
.end method

.method public resetShortTermModel(Z)V
    .locals 1
    .param p1, "manually"    # Z

    .line 2357
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2358
    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetShortTermModel(Z)V

    .line 2360
    :cond_0
    return-void
.end method

.method public sendBrightnessToSurfaceFlingerIfNeeded(FFZ)V
    .locals 2
    .param p1, "target"    # F
    .param p2, "dozeBrightness"    # F
    .param p3, "isDimming"    # Z

    .line 809
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->getDisplayPowerState()Lcom/android/server/display/DisplayPowerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerState;->getScreenState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_3

    .line 811
    :pswitch_0
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActualScreenOnBrightness:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_4

    if-nez p3, :cond_4

    .line 813
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightModeActive:Z

    if-eqz v0, :cond_0

    .line 814
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSettingsBrightnessBeforeApplySunlight:F

    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    .line 815
    :cond_0
    move v0, p1

    :goto_0
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActualScreenOnBrightness:F

    goto :goto_3

    .line 820
    :pswitch_1
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F

    invoke-static {p2, v0}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result v0

    if-nez v0, :cond_2

    .line 821
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F

    .line 822
    invoke-static {p2}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeInLowBrightness:Z

    .line 825
    :cond_2
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeInLowBrightness:Z

    if-eqz v0, :cond_3

    .line 826
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDozeScreenBrightness:F

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mActualScreenOnBrightness:F

    .line 827
    .local v0, "pendingBrightness":F
    :goto_2
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDesiredBrightness:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_4

    .line 828
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDesiredBrightness:F

    .line 829
    nop

    .line 830
    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDesiredBrightnessInt:I

    .line 831
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->sendSurfaceFlingerActualBrightness(I)V

    .line 837
    .end local v0    # "pendingBrightness":F
    :cond_4
    :goto_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public sendDimStateToSurfaceFlinger(Z)V
    .locals 7
    .param p1, "isDim"    # Z

    .line 2108
    sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_IDLE_DIM:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I

    if-eqz v0, :cond_0

    goto :goto_3

    .line 2111
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    const-string v1, "DisplayPowerControllerImpl"

    if-eqz v0, :cond_1

    .line 2112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendDimStateToSurfaceFlinger is dim "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2114
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mISurfaceFlinger:Landroid/os/IBinder;

    if-eqz v0, :cond_2

    .line 2115
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 2116
    .local v0, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2117
    move v2, p1

    .line 2118
    .local v2, "val":I
    const/16 v3, 0x106

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2119
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120
    const-string/jumbo v3, "system"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2122
    :try_start_0
    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mISurfaceFlinger:Landroid/os/IBinder;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/16 v6, 0x7983

    invoke-interface {v3, v6, v0, v4, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2127
    nop

    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 2128
    goto :goto_2

    .line 2127
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 2124
    :catch_0
    move-exception v3

    .line 2125
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "Failed to send brightness to SurfaceFlinger"

    invoke-static {v1, v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2127
    nop

    .end local v3    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 2128
    throw v1

    .line 2130
    .end local v0    # "data":Landroid/os/Parcel;
    .end local v2    # "val":I
    :cond_2
    :goto_2
    return-void

    .line 2109
    :cond_3
    :goto_3
    return-void
.end method

.method public setAppliedScreenBrightnessOverride(Z)V
    .locals 1
    .param p1, "isApplied"    # Z

    .line 1369
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z

    if-eq v0, p1, :cond_1

    .line 1370
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1371
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V

    .line 1373
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z

    .line 1374
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_1

    .line 1375
    invoke-virtual {v0, p1}, Lcom/android/server/display/RampRateController;->addOverrideRateModifier(Z)V

    .line 1378
    :cond_1
    return-void
.end method

.method public setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V
    .locals 2
    .param p1, "config"    # Landroid/hardware/display/BrightnessConfiguration;

    .line 2363
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/android/server/display/DisplayPowerController;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;Z)V

    .line 2364
    return-void
.end method

.method public setCustomCurveEnabledOnCommand(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 2397
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2398
    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveEnabledOnCommand(Z)V

    .line 2400
    :cond_0
    return-void
.end method

.method public setForceTrainEnabledOnCommand(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 2411
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2412
    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setForceTrainEnabledOnCommand(Z)V

    .line 2414
    :cond_0
    return-void
.end method

.method public setIndividualModelEnabledOnCommand(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 2404
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2405
    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setIndividualModelEnabledOnCommand(Z)V

    .line 2407
    :cond_0
    return-void
.end method

.method public setRampAnimator(Lcom/android/server/display/RampAnimator$DualRampAnimator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/display/RampAnimator$DualRampAnimator<",
            "Lcom/android/server/display/DisplayPowerState;",
            ">;)V"
        }
    .end annotation

    .line 1610
    .local p1, "rampAnimator":Lcom/android/server/display/RampAnimator$DualRampAnimator;, "Lcom/android/server/display/RampAnimator$DualRampAnimator<Lcom/android/server/display/DisplayPowerState;>;"
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessRampAnimator:Lcom/android/server/display/RampAnimator$DualRampAnimator;

    .line 1611
    return-void
.end method

.method public setScreenBrightnessByUser(FFLjava/lang/String;)V
    .locals 4
    .param p1, "lux"    # F
    .param p2, "brightness"    # F
    .param p3, "packageName"    # Ljava/lang/String;

    .line 2348
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->getBrightness(F)F

    move-result v0

    goto :goto_0

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    .line 2349
    .local v0, "unAdjustedBrightness":F
    :goto_0
    cmpg-float v1, v0, p2

    if-gez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 2350
    .local v1, "isBrightening":Z
    :goto_1
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v2, :cond_2

    .line 2351
    iget v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I

    invoke-virtual {v2, p1, v1, p3, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setScreenBrightnessByUser(FZLjava/lang/String;I)V

    .line 2353
    :cond_2
    return-void
.end method

.method public setSunlightListener(Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    .line 603
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSunlightStateListener:Lcom/android/server/display/DisplayPowerControllerStub$SunlightStateChangedListener;

    .line 604
    return-void
.end method

.method public setUpAutoBrightness(Lcom/android/server/display/BrightnessMappingStrategy;Lcom/android/server/display/AutomaticBrightnessControllerStub;Lcom/android/server/display/DisplayDeviceConfig;Landroid/hardware/Sensor;)V
    .locals 4
    .param p1, "brightnessMapper"    # Lcom/android/server/display/BrightnessMappingStrategy;
    .param p2, "stub"    # Lcom/android/server/display/AutomaticBrightnessControllerStub;
    .param p3, "displayDeviceConfig"    # Lcom/android/server/display/DisplayDeviceConfig;
    .param p4, "lightSensor"    # Landroid/hardware/Sensor;

    .line 1575
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    .line 1576
    iput-object p3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 1577
    move-object v0, p2

    check-cast v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 1578
    invoke-virtual {p1, p0}, Lcom/android/server/display/BrightnessMappingStrategy;->setDisplayPowerControllerImpl(Lcom/android/server/display/DisplayPowerControllerStub;)V

    .line 1579
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_4

    .line 1580
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setUpAutoBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/BrightnessMappingStrategy;Lcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/LogicalDisplay;)V

    .line 1582
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 1583
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCloudListener:Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;

    invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setUpCloudControllerListener(Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;)V

    .line 1584
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setDisplayDeviceConfig(Lcom/android/server/display/DisplayDeviceConfig;)V

    .line 1585
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setBrightnessMapper(Lcom/android/server/display/BrightnessMappingStrategy;)V

    .line 1587
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_1

    .line 1588
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setAutoBrightnessComponent(Lcom/android/server/display/BrightnessMappingStrategy;)V

    .line 1590
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 1591
    invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;

    move-result-object v0

    .line 1592
    .local v0, "highBrightnessModeData":Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
    if-eqz v0, :cond_2

    .line 1593
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->recalculationForBCBC(F)V

    .line 1594
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->SUPPORT_HDR_HBM_BRIGHTEN:Z

    invoke-virtual {v1, v2}, Lcom/android/server/display/HighBrightnessModeController;->supportHdrBrightenHbm(Z)V

    .line 1595
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHbmController:Lcom/android/server/display/HighBrightnessModeController;

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHdrStateListener:Lcom/android/server/display/HighBrightnessModeController$HdrStateListener;

    invoke-virtual {v1, v2}, Lcom/android/server/display/HighBrightnessModeController;->registerListener(Lcom/android/server/display/HighBrightnessModeController$HdrStateListener;)V

    .line 1597
    :cond_2
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessABTester:Lcom/android/server/display/BrightnessABTester;

    if-eqz v1, :cond_3

    .line 1598
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessABTester;->setAutomaticBrightnessControllerImpl(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V

    .line 1600
    :cond_3
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    if-eqz v1, :cond_4

    sget v2, Lcom/android/server/display/DisplayPowerControllerImpl;->MAX_POWER_SAVE_MODE_NIT:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_4

    .line 1602
    invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMaxPowerSaveModeBrightness:F

    .line 1605
    .end local v0    # "highBrightnessModeData":Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
    :cond_4
    invoke-direct {p0, p4}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAmbientLightSensor(Landroid/hardware/Sensor;)V

    .line 1606
    return-void
.end method

.method public settingBrightnessStartChange(F)V
    .locals 1
    .param p1, "brightness"    # F

    .line 1407
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z

    if-eqz v0, :cond_0

    .line 1408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z

    .line 1409
    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F

    .line 1411
    :cond_0
    return-void
.end method

.method public showTouchCoverProtectionRect(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .line 2025
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 2026
    invoke-virtual {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->showTouchCoverProtectionRect(Z)V

    .line 2028
    :cond_0
    return-void
.end method

.method protected startCbmStatsJob()V
    .locals 1

    .line 2390
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2391
    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->startCbmStatsJob()V

    .line 2393
    :cond_0
    return-void
.end method

.method protected startDetailThermalUsageStatsOnThermalChanged(IFZ)V
    .locals 2
    .param p1, "conditionId"    # I
    .param p2, "temperature"    # F
    .param p3, "brightnessChanged"    # Z

    .line 2257
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;IFZ)V

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z

    .line 2266
    return-void
.end method

.method public stop()V
    .locals 2

    .line 2015
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mSettingsObserver:Lcom/android/server/display/DisplayPowerControllerImpl$SettingsObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2016
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBatteryReceiver:Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;

    if-eqz v0, :cond_0

    .line 2017
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2019
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->post(Ljava/lang/Runnable;)Z

    .line 2021
    return-void
.end method

.method public temporaryBrightnessStartChange(F)V
    .locals 1
    .param p1, "brightness"    # F

    .line 1348
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isOverrideBrightnessPolicyEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z

    if-eqz v0, :cond_0

    .line 1349
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/wm/WindowManagerServiceStub;->notifySystemBrightnessChange()V

    .line 1350
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedScreenBrightnessOverride:Z

    .line 1353
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessControlAvailable:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayId:I

    if-nez v0, :cond_1

    .line 1355
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isInOutdoorCriticalTemperature()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1358
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mShowOutdoorHighTempToast:Z

    .line 1359
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mInjector:Lcom/android/server/display/DisplayPowerControllerImpl$Injector;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;->showOutdoorHighTempToast()V

    .line 1361
    :cond_1
    iput p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastTemporaryBrightness:F

    .line 1362
    return-void
.end method

.method public updateAutoBrightness()V
    .locals 1

    .line 2331
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 2332
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->update()V

    .line 2334
    :cond_0
    return-void
.end method

.method public updateBCBCStateIfNeeded()V
    .locals 4

    .line 1733
    sget-boolean v0, Lcom/android/server/display/DisplayPowerControllerImpl;->BCBC_ENABLE:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mMiuiDisplayCloudController:Lcom/android/server/display/MiuiDisplayCloudController;

    if-eqz v0, :cond_3

    .line 1736
    nop

    .line 1734
    invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getBCBCAppList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v0, :cond_0

    .line 1735
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z

    if-eqz v0, :cond_0

    .line 1736
    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1737
    .local v0, "state":I
    :goto_0
    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCState:I

    const-string v3, "DisplayPowerControllerImpl"

    if-eq v0, v2, :cond_2

    .line 1738
    iput v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCState:I

    .line 1739
    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayFeatureManagerServicImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    invoke-virtual {v2, v0}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->updateBCBCState(I)V

    .line 1740
    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v2, :cond_3

    .line 1741
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-ne v0, v1, :cond_1

    const-string v1, "Enter "

    goto :goto_1

    :cond_1
    const-string v1, "Exit "

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BCBC State, mForegroundAppPackageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mAutoBrightnessEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1745
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDebug:Z

    if-eqz v1, :cond_3

    .line 1746
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skip BCBC State, mBCBCState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBCBCState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1749
    .end local v0    # "state":I
    :cond_3
    :goto_2
    return-void
.end method

.method public updateBrightnessAnimInfoIfNeeded(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 2065
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mUpdateBrightnessAnimInfoEnable:Z

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    if-eqz v0, :cond_0

    .line 2066
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mUpdateBrightnessAnimInfoEnable:Z

    .line 2067
    if-nez p1, :cond_0

    .line 2068
    invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->notifyResetBrightnessAnimInfo()V

    .line 2071
    :cond_0
    return-void
.end method

.method public updateBrightnessChangeStatus(ZIZZZFFFFILcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;)V
    .locals 18
    .param p1, "animating"    # Z
    .param p2, "displayState"    # I
    .param p3, "slowChange"    # Z
    .param p4, "appliedDimming"    # Z
    .param p5, "appliedLowPower"    # Z
    .param p6, "currentBrightness"    # F
    .param p7, "currentSdrBrightness"    # F
    .param p8, "targetBrightness"    # F
    .param p9, "targetSdrBrightness"    # F
    .param p10, "displayPolicy"    # I
    .param p11, "reason"    # Lcom/android/server/display/brightness/BrightnessReason;
    .param p12, "reasonTemp"    # Lcom/android/server/display/brightness/BrightnessReason;

    .line 1068
    move-object/from16 v0, p0

    move/from16 v14, p1

    move/from16 v15, p2

    move/from16 v13, p3

    move/from16 v12, p4

    move/from16 v11, p5

    move/from16 v10, p6

    move/from16 v9, p7

    move/from16 v8, p8

    move/from16 v7, p9

    move/from16 v6, p10

    iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentSdrBrightness:F

    cmpl-float v1, v9, v1

    if-nez v1, :cond_0

    iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetBrightness:F

    cmpl-float v1, v8, v1

    if-eqz v1, :cond_1

    .line 1070
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateBrightnessChangeStatus: animating: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", displayState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", slowChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appliedDimming: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appliedLowPower: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currentBrightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currentSdrBrightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", targetBrightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", targetSdrBrightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", previousDisplayPolicy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currentDisplayPolicy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DisplayPowerControllerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    :cond_1
    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v1, :cond_2

    .line 1084
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z

    move-result v2

    iget v5, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I

    iget v4, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastDisplayState:I

    move/from16 v3, p1

    move/from16 v16, v4

    move/from16 v4, p3

    move/from16 v17, v5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, v17

    move/from16 v11, v16

    move-object/from16 v12, p12

    move-object/from16 v13, p11

    invoke-virtual/range {v1 .. v13}, Lcom/android/server/display/RampRateController;->updateBrightnessState(ZZZFFFFIIILcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;)V

    .line 1090
    :cond_2
    iput-boolean v14, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastAnimating:Z

    .line 1091
    iput v15, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastDisplayState:I

    .line 1092
    move/from16 v7, p6

    iput v7, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentBrightness:F

    .line 1093
    move/from16 v8, p7

    iput v8, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentSdrBrightness:F

    .line 1094
    move/from16 v9, p3

    iput-boolean v9, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mLastSlowChange:Z

    .line 1095
    move/from16 v10, p4

    iput-boolean v10, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedDimming:Z

    .line 1096
    move/from16 v11, p5

    iput-boolean v11, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAppliedLowPower:Z

    .line 1097
    move/from16 v12, p8

    iput v12, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetBrightness:F

    .line 1098
    move/from16 v13, p9

    iput v13, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mTargetSdrBrightness:F

    .line 1099
    iget v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I

    iput v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mPreviousDisplayPolicy:I

    .line 1100
    move/from16 v6, p10

    iput v6, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCurrentDisplayPolicy:I

    .line 1102
    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v1, :cond_3

    .line 1103
    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move v7, v6

    move/from16 v6, p7

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateSlowChangeStatus(ZZZFF)V

    goto :goto_0

    .line 1102
    :cond_3
    move v7, v6

    .line 1106
    :goto_0
    iget-object v1, v0, Lcom/android/server/display/DisplayPowerControllerImpl;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    if-eqz v1, :cond_4

    .line 1107
    invoke-virtual {v1, v15, v7}, Lcom/android/server/display/ThermalBrightnessController;->updateScreenState(II)V

    .line 1109
    :cond_4
    return-void
.end method

.method protected updateCbmState(Z)V
    .locals 1
    .param p1, "autoBrightnessEnabled"    # Z

    .line 2444
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2445
    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCbmState(Z)V

    .line 2447
    :cond_0
    return-void
.end method

.method public updateCurrentGrayScale(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 1475
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->removeMessages(I)V

    .line 1476
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1477
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1478
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1479
    return-void
.end method

.method protected updateCustomSceneState(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 2438
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mCbmController:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    if-eqz v0, :cond_0

    .line 2439
    iget v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOrientation:I

    invoke-virtual {v0, p1, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomSceneState(Ljava/lang/String;I)V

    .line 2441
    :cond_0
    return-void
.end method

.method public updateDolbyPreviewStateLocked(ZF)V
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "dolbyPreviewBoostRatio"    # F

    .line 1216
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostAvailable:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_1

    .line 1219
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewEnable:Z

    .line 1220
    iput p2, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDolbyPreviewBoostRatio:F

    .line 1221
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1223
    :cond_1
    return-void
.end method

.method public updateFastRateStatus(F)V
    .locals 1
    .param p1, "brightness"    # F

    .line 1799
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_0

    .line 1800
    invoke-virtual {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateFastRateStatus(F)V

    .line 1802
    :cond_0
    return-void
.end method

.method public updateGalleryHdrState(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 1246
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z

    if-eq v0, p1, :cond_0

    .line 1247
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsGalleryHdrEnable:Z

    .line 1248
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1250
    :cond_0
    return-void
.end method

.method public updateGalleryHdrThermalThrottler(Z)V
    .locals 1
    .param p1, "throttled"    # Z

    .line 1257
    iget-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z

    if-eq v0, p1, :cond_0

    .line 1258
    iput-boolean p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mGalleryHdrThrottled:Z

    .line 1259
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->updateBrightness()V

    .line 1261
    :cond_0
    return-void
.end method

.method public updateGrayScale(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 1463
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1464
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1465
    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1466
    return-void
.end method

.method public updateOutdoorThermalAppCategoryList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1806
    .local p1, "outdoorThermalAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mHandler:Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;

    new-instance v1, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;Ljava/util/List;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1811
    return-void
.end method

.method public updateRampRate(Ljava/lang/String;FFF)F
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "currentBrightness"    # F
    .param p3, "targetBrightness"    # F
    .param p4, "rate"    # F

    .line 2505
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mRampRateController:Lcom/android/server/display/RampRateController;

    if-eqz v0, :cond_0

    .line 2506
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/display/RampRateController;->updateBrightnessRate(Ljava/lang/String;FFF)F

    move-result v0

    return v0

    .line 2509
    :cond_0
    return p4
.end method

.method public updateScreenGrayscaleStateIfNeeded()V
    .locals 7

    .line 1755
    invoke-virtual {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1756
    .local v0, "peakState":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprBrightnessControlAvailable:Z

    if-eqz v3, :cond_1

    .line 1757
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isHdrScene()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v3, :cond_1

    .line 1759
    invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getCurrentAmbientLux()F

    move-result v3

    iget v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mOprAmbientLuxThreshold:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v2

    .line 1761
    .local v3, "oprState":Z
    :goto_1
    iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mAutoBrightnessEnable:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z

    if-eqz v4, :cond_3

    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    move v4, v1

    goto :goto_2

    :cond_3
    move v4, v2

    .line 1762
    .local v4, "state":Z
    :goto_2
    iget-boolean v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenGrayscaleState:Z

    if-eq v4, v5, :cond_6

    .line 1763
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v4, :cond_4

    const-string v6, "Starting"

    goto :goto_3

    :cond_4
    const-string v6, "Ending"

    :goto_3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " update screen gray scale."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DisplayPowerControllerImpl"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1764
    iput-boolean v4, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mScreenGrayscaleState:Z

    .line 1765
    iget-object v5, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayFeatureManagerServicImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    if-eqz v4, :cond_5

    .line 1766
    goto :goto_4

    :cond_5
    move v1, v2

    .line 1765
    :goto_4
    invoke-virtual {v5, v1}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->updateScreenGrayscaleState(I)V

    .line 1768
    :cond_6
    return-void
.end method

.method public updateScreenState(FI)V
    .locals 2
    .param p1, "brightnessState"    # F
    .param p2, "policy"    # I

    .line 2198
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mDisplayPowerController:Lcom/android/server/display/DisplayPowerController;

    .line 2199
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerController;->getDisplayPowerState()Lcom/android/server/display/DisplayPowerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerState;->getScreenState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    if-ne p2, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 2201
    .local v0, "state":Z
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z

    if-eq v1, v0, :cond_2

    .line 2202
    iput-boolean v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl;->mIsScreenOn:Z

    .line 2203
    invoke-direct {p0, p1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->startUpdateThermalStats(FZ)V

    .line 2204
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetScreenGrayscaleState()V

    .line 2205
    invoke-direct {p0}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetBCBCState()V

    .line 2207
    :cond_2
    return-void
.end method
