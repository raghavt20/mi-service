.class Lcom/android/server/display/MiuiDisplayCloudController$1;
.super Landroid/database/ContentObserver;
.source "MiuiDisplayCloudController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/display/MiuiDisplayCloudController;->registerMiuiBrightnessCloudDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/MiuiDisplayCloudController;


# direct methods
.method public static synthetic $r8$lambda$_RoDYYPyAT_3JHhMAHObssA36DY(Lcom/android/server/display/MiuiDisplayCloudController$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController$1;->lambda$onChange$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/MiuiDisplayCloudController;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/MiuiDisplayCloudController;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 341
    iput-object p1, p0, Lcom/android/server/display/MiuiDisplayCloudController$1;->this$0:Lcom/android/server/display/MiuiDisplayCloudController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method private synthetic lambda$onChange$0()V
    .locals 1

    .line 347
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController$1;->this$0:Lcom/android/server/display/MiuiDisplayCloudController;

    invoke-static {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->-$$Nest$msyncLocalBackupFromCloud(Lcom/android/server/display/MiuiDisplayCloudController;)V

    .line 348
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 344
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController$1;->this$0:Lcom/android/server/display/MiuiDisplayCloudController;

    invoke-static {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->-$$Nest$mupdateDataFromCloudControl(Lcom/android/server/display/MiuiDisplayCloudController;)Z

    move-result v0

    .line 345
    .local v0, "changed":Z
    if-eqz v0, :cond_0

    .line 346
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/display/MiuiDisplayCloudController$1$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/display/MiuiDisplayCloudController$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/MiuiDisplayCloudController$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 349
    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController$1;->this$0:Lcom/android/server/display/MiuiDisplayCloudController;

    invoke-virtual {v1}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyAllObservers()V

    .line 351
    :cond_0
    return-void
.end method
