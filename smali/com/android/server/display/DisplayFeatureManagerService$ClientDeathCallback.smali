.class Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;
.super Ljava/lang/Object;
.source "DisplayFeatureManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayFeatureManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClientDeathCallback"
.end annotation


# instance fields
.field private mFlag:I

.field private mToken:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/IBinder;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/display/DisplayFeatureManagerService;
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "flag"    # I

    .line 1022
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1023
    iput-object p2, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    .line 1024
    iput p3, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mFlag:I

    .line 1026
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p2, p0, v0}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1029
    goto :goto_0

    .line 1027
    :catch_0
    move-exception v0

    .line 1028
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1030
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 1034
    const-string v0, "DisplayFeatureManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "binderDied: flag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/util/HashMap;

    move-result-object v0

    monitor-enter v0

    .line 1036
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmClientDeathCallbacks(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1037
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1038
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 1039
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mFlag:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mdoDieLocked(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1040
    return-void

    .line 1037
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
