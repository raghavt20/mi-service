class com.android.server.display.MiuiFoldPolicy$1 extends android.hardware.camera2.CameraManager$AvailabilityCallback {
	 /* .source "MiuiFoldPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/MiuiFoldPolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.MiuiFoldPolicy this$0; //synthetic
/* # direct methods */
 com.android.server.display.MiuiFoldPolicy$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/MiuiFoldPolicy; */
/* .line 339 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCameraAvailable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 342 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraAvailable(Ljava/lang/String;)V */
/* .line 343 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 344 */
/* .local v0, "id":I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_0 */
/* .line 345 */
return;
/* .line 347 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.MiuiFoldPolicy .-$$Nest$fgetmOpeningCameraID ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 348 */
return;
} // .end method
public void onCameraUnavailable ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 352 */
/* invoke-super {p0, p1}, Landroid/hardware/camera2/CameraManager$AvailabilityCallback;->onCameraUnavailable(Ljava/lang/String;)V */
/* .line 353 */
v0 = java.lang.Integer .parseInt ( p1 );
/* .line 354 */
/* .local v0, "id":I */
/* const/16 v1, 0x64 */
/* if-lt v0, v1, :cond_0 */
/* .line 355 */
return;
/* .line 357 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.MiuiFoldPolicy .-$$Nest$fgetmOpeningCameraID ( v1 );
java.lang.Integer .valueOf ( v0 );
/* .line 358 */
return;
} // .end method
