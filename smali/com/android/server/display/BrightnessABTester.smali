.class public Lcom/android/server/display/BrightnessABTester;
.super Ljava/lang/Object;
.source "BrightnessABTester.java"


# static fields
.field private static final APP_NAME:Ljava/lang/String; = "AutoBrightness"

.field private static final CONTROL_GROUP:Ljava/lang/String; = "control_group"

.field private static final DELAY_TIME:J = 0x7530L

.field private static final EXPERIMENTAL_GROUP_1:Ljava/lang/String; = "experimental_group_1"

.field private static final EXPERIMENTAL_GROUP_2:Ljava/lang/String; = "experimental_group_2"

.field private static final EXPERIMENTAL_GROUP_3:Ljava/lang/String; = "experimental_group_3"

.field private static final EXPERIMENTAL_GROUP_4:Ljava/lang/String; = "experimental_group_4"

.field private static LAYER_NAME:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BrightnessABTester"

.field private static final THRESHOLD_EXP_KEY:Ljava/lang/String; = "threshold_type"


# instance fields
.field private mABTestHelper:Lcom/android/server/display/ABTestHelper;

.field private mAmbientBrighteningLux:[F

.field private mAmbientBrighteningThresholds:[F

.field private mAmbientDarkeningLux:[F

.field private mAmbientDarkeningThresholds:[F

.field private mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

.field private mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

.field private mCurrentExperiment:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$3j4OlJVSAa-1D8YPwfv29rWak6Y(Lcom/android/server/display/BrightnessABTester;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/BrightnessABTester;->lambda$startExperiment$0(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$DvlmJ9JosBdqmkBwJ0mMJkY-Ufc(Lcom/android/server/display/BrightnessABTester;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessABTester;->startExperiment(Ljava/util/Map;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Wa0wWhj9RwtZChp5hLRmXq-qk2Q(Lcom/android/server/display/BrightnessABTester;Ljava/lang/Void;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessABTester;->endExperiment(Ljava/lang/Void;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 30
    const-string v0, "ro.product.mod_device"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/BrightnessABTester;->LAYER_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;Lcom/android/server/display/statistics/BrightnessDataProcessor;)V
    .locals 11
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "brightnessDataProcessor"    # Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mCurrentExperiment:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/android/server/display/BrightnessABTester;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 42
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 43
    .local v6, "expCondition":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/android/server/display/ABTestHelper;

    const-string v4, "AutoBrightness"

    sget-object v5, Lcom/android/server/display/BrightnessABTester;->LAYER_NAME:Ljava/lang/String;

    new-instance v7, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda1;

    invoke-direct {v7, p0}, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/BrightnessABTester;)V

    new-instance v8, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda2;

    invoke-direct {v8, p0}, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/BrightnessABTester;)V

    const-wide/16 v9, 0x7530

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v10}, Lcom/android/server/display/ABTestHelper;-><init>(Landroid/os/Looper;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/function/Consumer;Ljava/util/function/Consumer;J)V

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mABTestHelper:Lcom/android/server/display/ABTestHelper;

    .line 45
    return-void
.end method

.method private endExperiment(Ljava/lang/Void;)V
    .locals 3
    .param p1, "unused"    # Ljava/lang/Void;

    .line 65
    iget-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mCurrentExperiment:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "threshold_type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 67
    :pswitch_1
    const-string v0, "control_group"

    invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->updateBrightnessThresholdABTest(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setAnimationPolicyDisable(Z)V

    .line 69
    nop

    .line 73
    :goto_2
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mCurrentExperiment:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v1, p0, Lcom/android/server/display/BrightnessABTester;->mABTestHelper:Lcom/android/server/display/ABTestHelper;

    invoke-virtual {v1}, Lcom/android/server/display/ABTestHelper;->getExpId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setExpId(I)V

    .line 75
    return-void

    :pswitch_data_0
    .packed-switch 0x6db46e8e
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private getFloatArray(Landroid/content/res/TypedArray;)[F
    .locals 4
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .line 144
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    .line 145
    .local v0, "N":I
    new-array v1, v0, [F

    .line 146
    .local v1, "vals":[F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 147
    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    aput v3, v1, v2

    .line 146
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 149
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 150
    return-object v1
.end method

.method private getValuesFromXml(IIII)V
    .locals 1
    .param p1, "configAmbientBrighteningLux"    # I
    .param p2, "configAmbientBrighteningThresholds"    # I
    .param p3, "configAmbientDarkeningLux"    # I
    .param p4, "configAmbientDarkeningThresholds"    # I

    .line 79
    nop

    .line 80
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 79
    invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientBrighteningLux:[F

    .line 81
    nop

    .line 82
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 81
    invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientBrighteningThresholds:[F

    .line 83
    nop

    .line 84
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 83
    invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientDarkeningLux:[F

    .line 85
    nop

    .line 86
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 85
    invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientDarkeningThresholds:[F

    .line 87
    return-void
.end method

.method private synthetic lambda$startExperiment$0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "threshold_type"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 55
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/android/server/display/BrightnessABTester;->updateBrightnessThresholdABTest(Ljava/lang/String;)V

    .line 56
    iput-object p1, p0, Lcom/android/server/display/BrightnessABTester;->mCurrentExperiment:Ljava/lang/String;

    .line 57
    nop

    .line 61
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x6db46e8e
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private startExperiment(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 52
    .local p1, "expParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/BrightnessABTester;)V

    invoke-interface {p1, v0}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 62
    return-void
.end method

.method private updateBrightnessThresholdABTest(Ljava/lang/String;)V
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The abtest experiment of brightness target "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BrightnessABTester"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "control_group"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_1
    const-string v0, "experimental_group_4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v0, "experimental_group_3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_3
    const-string v0, "experimental_group_2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_4
    const-string v0, "experimental_group_1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 131
    return-void

    .line 124
    :pswitch_0
    const v0, 0x11030028

    const v2, 0x1103002d

    const v3, 0x1103001e

    const v4, 0x11030023

    invoke-direct {p0, v3, v4, v0, v2}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V

    .line 128
    iget-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setAnimationPolicyDisable(Z)V

    .line 129
    goto :goto_2

    .line 116
    :pswitch_1
    const v0, 0x11030029

    const v1, 0x1103002e

    const v2, 0x1103001f

    const v3, 0x11030024

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V

    .line 120
    goto :goto_2

    .line 109
    :pswitch_2
    const v0, 0x1103002b

    const v1, 0x11030030

    const v2, 0x11030021

    const v3, 0x11030026

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V

    .line 113
    goto :goto_2

    .line 102
    :pswitch_3
    const v0, 0x1103002c

    const v1, 0x11030031

    const v2, 0x11030022

    const v3, 0x11030027

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V

    .line 106
    goto :goto_2

    .line 94
    :pswitch_4
    const v0, 0x1103002a

    const v1, 0x1103002f

    const v2, 0x11030020

    const v3, 0x11030025

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V

    .line 98
    nop

    .line 133
    :goto_2
    iget-object v0, p0, Lcom/android/server/display/BrightnessABTester;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    if-eqz v0, :cond_1

    .line 134
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getHysteresisLevelsImpl()Lcom/android/server/display/HysteresisLevelsStub;

    move-result-object v0

    .line 135
    .local v0, "hysteresisLevelsImpl":Lcom/android/server/display/HysteresisLevelsStub;
    if-eqz v0, :cond_1

    .line 136
    iget-object v1, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientBrighteningLux:[F

    iget-object v2, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientBrighteningThresholds:[F

    iget-object v3, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientDarkeningLux:[F

    iget-object v4, p0, Lcom/android/server/display/BrightnessABTester;->mAmbientDarkeningThresholds:[F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/display/HysteresisLevelsStub;->createHysteresisThresholdSpline([F[F[F[F)V

    .line 138
    iget-object v1, p0, Lcom/android/server/display/BrightnessABTester;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget-object v2, p0, Lcom/android/server/display/BrightnessABTester;->mABTestHelper:Lcom/android/server/display/ABTestHelper;

    invoke-virtual {v2}, Lcom/android/server/display/ABTestHelper;->getExpId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setExpId(I)V

    .line 141
    .end local v0    # "hysteresisLevelsImpl":Lcom/android/server/display/HysteresisLevelsStub;
    :cond_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4ea785e6 -> :sswitch_4
        -0x4ea785e5 -> :sswitch_3
        -0x4ea785e4 -> :sswitch_2
        -0x4ea785e3 -> :sswitch_1
        0x4878851d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public setAutomaticBrightnessControllerImpl(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
    .locals 0
    .param p1, "stub"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 48
    iput-object p1, p0, Lcom/android/server/display/BrightnessABTester;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 49
    return-void
.end method
