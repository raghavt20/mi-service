.class Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;
.super Lcom/android/server/display/BrightnessCurve$Curve;
.source "BrightnessCurve.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/BrightnessCurve;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SecondOutdoorLightCurve"
.end annotation


# instance fields
.field private final mHeadLux:F

.field private final mPullUpAnchorPointLux:F

.field private final mPullUpAnchorPointNit:F

.field private final mTailLux:F

.field final synthetic this$0:Lcom/android/server/display/BrightnessCurve;


# direct methods
.method public constructor <init>(Lcom/android/server/display/BrightnessCurve;)V
    .locals 5
    .param p1, "this$0"    # Lcom/android/server/display/BrightnessCurve;

    .line 532
    iput-object p1, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve$Curve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 533
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmCurrentCurveInterval(Lcom/android/server/display/BrightnessCurve;)[F

    move-result-object v0

    const/4 v1, 0x3

    aget v0, v0, v1

    iput v0, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mHeadLux:F

    .line 534
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmCurrentCurveInterval(Lcom/android/server/display/BrightnessCurve;)[F

    move-result-object v1

    const/4 v2, 0x4

    aget v1, v1, v2

    iput v1, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    .line 535
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmCurrentCurveInterval(Lcom/android/server/display/BrightnessCurve;)[F

    move-result-object v2

    const/4 v3, 0x1

    aget v2, v2, v3

    iput v2, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPullUpAnchorPointLux:F

    .line 536
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/util/Spline;->interpolate(F)F

    move-result v3

    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/util/Spline;->interpolate(F)F

    move-result v4

    sub-float/2addr v3, v4

    sub-float v0, v1, v0

    div-float/2addr v3, v0

    .line 538
    .local v3, "tan":F
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    sub-float/2addr v1, v2

    mul-float/2addr v1, v3

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPullUpAnchorPointNit:F

    .line 540
    return-void
.end method


# virtual methods
.method public connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
    .locals 8
    .param p1, "curve"    # Lcom/android/server/display/BrightnessCurve$Curve;

    .line 555
    iget-object v0, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 556
    iget-object v0, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    iget-object v1, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 557
    .local v0, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v2}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v2

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mHeadLux:F

    invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 558
    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mHeadLux:F

    iget v4, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    iget-object v5, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPointList:Ljava/util/List;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V

    goto :goto_0

    .line 559
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v1

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 560
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->create(FF)V

    goto :goto_0

    .line 562
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v2, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmSecondOutdoorCurveBrightenMinTan(Lcom/android/server/display/BrightnessCurve;)F

    move-result v3

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPointList:Ljava/util/List;

    invoke-static {v1, v0, v2, v3, v4}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullUpConnectTail(Lcom/android/server/display/BrightnessCurve;Landroid/util/Pair;FFLjava/util/List;)V

    .line 565
    .end local v0    # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    :cond_2
    :goto_0
    return-void
.end method

.method public connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
    .locals 8
    .param p1, "curve"    # Lcom/android/server/display/BrightnessCurve$Curve;

    .line 569
    iget-object v0, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 571
    .local v0, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v2}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v2

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 572
    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mHeadLux:F

    iget v4, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    iget-object v5, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPointList:Ljava/util/List;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V

    goto :goto_0

    .line 574
    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->create(FF)V

    .line 577
    .end local v0    # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    :cond_1
    :goto_0
    return-void
.end method

.method public create(FF)V
    .locals 9
    .param p1, "lux"    # F
    .param p2, "changeNit"    # F

    .line 544
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v0}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    cmpl-float v0, v0, p2

    if-lez v0, :cond_0

    .line 545
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPointList:Ljava/util/List;

    iget v5, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mHeadLux:F

    iget v6, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v0}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmSecondOutdoorCurveBrightenMinTan(Lcom/android/server/display/BrightnessCurve;)F

    move-result v7

    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v0}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmSecondOutdoorCurveBrightenMinTan(Lcom/android/server/display/BrightnessCurve;)F

    move-result v8

    move v2, p1

    move v3, p2

    invoke-static/range {v1 .. v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullDownCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V

    goto :goto_0

    .line 548
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPointList:Ljava/util/List;

    iget v5, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mHeadLux:F

    iget v6, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mTailLux:F

    iget v7, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPullUpAnchorPointLux:F

    iget v8, p0, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;->mPullUpAnchorPointNit:F

    move v2, p1

    move v3, p2

    invoke-static/range {v1 .. v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullUpCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V

    .line 551
    :goto_0
    return-void
.end method
