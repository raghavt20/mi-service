class com.android.server.display.DaemonSensorPolicy$2 extends android.content.BroadcastReceiver {
	 /* .source "DaemonSensorPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DaemonSensorPolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.DaemonSensorPolicy this$0; //synthetic
/* # direct methods */
 com.android.server.display.DaemonSensorPolicy$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DaemonSensorPolicy; */
/* .line 156 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 159 */
com.android.server.display.DaemonSensorPolicy .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Device idle changed, mDeviceIdle: "; // const-string v2, "Device idle changed, mDeviceIdle: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.display.DaemonSensorPolicy .-$$Nest$fgetmIsDeviceIdleMode ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 160 */
v0 = this.this$0;
com.android.server.display.DaemonSensorPolicy .-$$Nest$fgetmHandler ( v0 );
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.display.DaemonSensorPolicy$DaemonSensorHandle ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;->sendEmptyMessage(I)Z
/* .line 161 */
return;
} // .end method
