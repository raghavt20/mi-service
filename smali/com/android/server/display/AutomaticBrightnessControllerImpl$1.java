class com.android.server.display.AutomaticBrightnessControllerImpl$1 extends android.hardware.camera2.CameraManager$TorchCallback {
	 /* .source "AutomaticBrightnessControllerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.AutomaticBrightnessControllerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.display.AutomaticBrightnessControllerImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
/* .line 257 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$TorchCallback;-><init>()V */
return;
} // .end method
private java.lang.Integer getRoleIds ( android.hardware.camera2.CameraCharacteristics p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "cameraCharacteristics" # Landroid/hardware/camera2/CameraCharacteristics; */
/* .param p2, "cameraRoleId" # Ljava/lang/String; */
/* .line 329 */
try { // :try_start_0
	 /* new-instance v0, Landroid/hardware/camera2/CameraCharacteristics$Key; */
	 /* const-class v1, [Ljava/lang/Integer; */
	 /* invoke-direct {v0, p2, v1}, Landroid/hardware/camera2/CameraCharacteristics$Key;-><init>(Ljava/lang/String;Ljava/lang/Class;)V */
	 /* .line 330 */
	 (( android.hardware.camera2.CameraCharacteristics ) p1 ).get ( v0 ); // invoke-virtual {p1, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
	 /* check-cast v0, [Ljava/lang/Integer; */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 329 */
	 /* .line 331 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 332 */
	 /* .local v0, "e":Ljava/lang/IllegalArgumentException; */
	 int v1 = 0; // const/4 v1, 0x0
} // .end method
/* # virtual methods */
public void onTorchModeChanged ( java.lang.String p0, Boolean p1 ) {
	 /* .locals 10 */
	 /* .param p1, "cameraId" # Ljava/lang/String; */
	 /* .param p2, "enabled" # Z */
	 /* .line 276 */
	 final String v0 = "AutomaticBrightnessControllerImpl"; // const-string v0, "AutomaticBrightnessControllerImpl"
	 int v1 = 0; // const/4 v1, 0x0
	 try { // :try_start_0
		 v2 = this.this$0;
		 com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmCameraManager ( v2 );
		 /* .line 277 */
		 (( android.hardware.camera2.CameraManager ) v2 ).getCameraCharacteristics ( p1 ); // invoke-virtual {v2, p1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;
		 /* .line 278 */
		 /* .local v2, "cameraCharacteristics":Landroid/hardware/camera2/CameraCharacteristics; */
		 v3 = android.hardware.camera2.CameraCharacteristics.LENS_FACING;
		 /* .line 279 */
		 (( android.hardware.camera2.CameraCharacteristics ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
		 /* check-cast v3, Ljava/lang/Integer; */
		 /* .line 280 */
		 /* .local v3, "facing":Ljava/lang/Integer; */
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 v4 = 			 (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
			 /* if-nez v4, :cond_0 */
			 /* .line 281 */
			 v4 = this.this$0;
			 v5 = android.hardware.camera2.CameraCharacteristics.FLASH_INFO_AVAILABLE;
			 /* .line 282 */
			 (( android.hardware.camera2.CameraCharacteristics ) v2 ).get ( v5 ); // invoke-virtual {v2, v5}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;
			 /* check-cast v5, Ljava/lang/Boolean; */
			 v5 = 			 (( java.lang.Boolean ) v5 ).booleanValue ( ); // invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
			 com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fputmFrontFlashAvailable ( v4,v5 );
			 /* .line 286 */
		 } // :cond_0
		 v4 = this.this$0;
		 v4 = 		 com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmFrontFlashAvailable ( v4 );
		 if ( v4 != null) { // if-eqz v4, :cond_2
			 v4 = this.this$0;
			 com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmExitFacingCameraIds ( v4 );
			 if ( v4 != null) { // if-eqz v4, :cond_2
				 if ( p1 != null) { // if-eqz p1, :cond_2
					 /* .line 289 */
					 v4 = this.this$0;
					 com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmExitFacingCameraIds ( v4 );
					 /* array-length v5, v4 */
					 /* move v6, v1 */
				 } // :goto_0
				 /* if-ge v6, v5, :cond_2 */
				 /* aget-object v7, v4, v6 */
				 /* .line 290 */
				 /* .local v7, "id":Ljava/lang/String; */
				 v8 = 				 (( java.lang.String ) p1 ).equals ( v7 ); // invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v8 != null) { // if-eqz v8, :cond_1
					 /* .line 291 */
					 return;
					 /* .line 289 */
				 } // .end local v7 # "id":Ljava/lang/String;
			 } // :cond_1
			 /* add-int/lit8 v6, v6, 0x1 */
			 /* .line 295 */
		 } // :cond_2
		 int v4 = 0; // const/4 v4, 0x0
		 /* .line 296 */
		 /* .local v4, "roleIds":[Ljava/lang/Integer; */
		 com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$sfgetCAMERA_ROLE_IDS ( );
		 /* array-length v6, v5 */
		 /* move v7, v1 */
	 } // :goto_1
	 /* if-ge v7, v6, :cond_4 */
	 /* aget-object v8, v5, v7 */
	 /* .line 297 */
	 /* .local v8, "cameraRoleId":Ljava/lang/String; */
	 /* invoke-direct {p0, v2, v8}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->getRoleIds(Landroid/hardware/camera2/CameraCharacteristics;Ljava/lang/String;)[Ljava/lang/Integer; */
	 /* move-object v4, v9 */
	 /* .line 298 */
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* .line 299 */
		 /* .line 296 */
	 } // .end local v8 # "cameraRoleId":Ljava/lang/String;
} // :cond_3
/* add-int/lit8 v7, v7, 0x1 */
/* .line 302 */
} // :cond_4
} // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 303 */
java.util.Arrays .asList ( v4 );
/* .line 304 */
/* .local v5, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* const/16 v6, 0x64 */
v6 = java.lang.Integer .valueOf ( v6 );
/* if-nez v6, :cond_5 */
/* const/16 v6, 0x65 */
v6 = java.lang.Integer .valueOf ( v6 );
/* if-nez v6, :cond_5 */
/* .line 305 */
/* const/16 v6, 0x66 */
v6 = java.lang.Integer .valueOf ( v6 );
/* :try_end_0 */
/* .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 306 */
} // :cond_5
return;
/* .line 312 */
} // .end local v2 # "cameraCharacteristics":Landroid/hardware/camera2/CameraCharacteristics;
} // .end local v3 # "facing":Ljava/lang/Integer;
} // .end local v4 # "roleIds":[Ljava/lang/Integer;
} // .end local v5 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catch_0 */
/* move-exception v2 */
/* .line 313 */
/* .local v2, "e":Ljava/lang/IllegalArgumentException; */
final String v3 = "onTorchModeChanged: can\'t get camera characteristics key"; // const-string v3, "onTorchModeChanged: can\'t get camera characteristics key"
android.util.Slog .e ( v0,v3,v2 );
/* .line 309 */
} // .end local v2 # "e":Ljava/lang/IllegalArgumentException;
/* :catch_1 */
/* move-exception v2 */
/* .line 310 */
/* .local v2, "e":Landroid/hardware/camera2/CameraAccessException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onTorchModeChanged: can\'t get characteristics for camera "; // const-string v4, "onTorchModeChanged: can\'t get characteristics for camera "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3,v2 );
/* .line 314 */
} // .end local v2 # "e":Landroid/hardware/camera2/CameraAccessException;
} // :cond_6
/* nop */
/* .line 315 */
} // :goto_3
if ( p2 != null) { // if-eqz p2, :cond_7
/* .line 316 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fputmIsTorchOpen ( v1,v2 );
/* .line 318 */
} // :cond_7
v2 = this.this$0;
v2 = com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmIsTorchOpen ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 319 */
v2 = this.this$0;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fputmTorchCloseTime ( v2,v3,v4 );
/* .line 321 */
} // :cond_8
v2 = this.this$0;
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fputmIsTorchOpen ( v2,v1 );
/* .line 323 */
} // :goto_4
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onTorchModeChanged, mIsTorchOpen="; // const-string v2, "onTorchModeChanged, mIsTorchOpen="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmIsTorchOpen ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 324 */
return;
} // .end method
public void onTorchModeUnavailable ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "cameraId" # Ljava/lang/String; */
/* .line 262 */
v0 = this.this$0;
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmExitFacingCameraIds ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 263 */
v0 = this.this$0;
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fgetmExitFacingCameraIds ( v0 );
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* aget-object v3, v0, v2 */
/* .line 264 */
/* .local v3, "id":Ljava/lang/String; */
v4 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 265 */
return;
/* .line 263 */
} // .end local v3 # "id":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 269 */
} // :cond_1
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$fputmIsTorchOpen ( v0,v1 );
/* .line 270 */
return;
} // .end method
