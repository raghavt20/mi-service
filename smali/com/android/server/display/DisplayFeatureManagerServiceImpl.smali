.class public Lcom/android/server/display/DisplayFeatureManagerServiceImpl;
.super Ljava/lang/Object;
.source "DisplayFeatureManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/display/DisplayFeatureManagerServiceStub;


# instance fields
.field private mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Lcom/android/server/display/DisplayFeatureManagerInternal;)V
    .locals 0
    .param p1, "displayFeatureInternal"    # Lcom/android/server/display/DisplayFeatureManagerInternal;

    .line 12
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    .line 13
    return-void
.end method

.method public setVideoInformation(IZFIIFLandroid/os/IBinder;)V
    .locals 8
    .param p1, "pid"    # I
    .param p2, "bulletChatStatus"    # Z
    .param p3, "frameRate"    # F
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "compressionRatio"    # F
    .param p7, "token"    # Landroid/os/IBinder;

    .line 39
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    if-eqz v0, :cond_0

    .line 40
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/display/DisplayFeatureManagerInternal;->setVideoInformation(IZFIIFLandroid/os/IBinder;)V

    .line 43
    :cond_0
    return-void
.end method

.method public updateBCBCState(I)V
    .locals 1
    .param p1, "state"    # I

    .line 31
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateBCBCState(I)V

    .line 34
    :cond_0
    return-void
.end method

.method public updateDozeBrightness(JI)V
    .locals 1
    .param p1, "physicalDisplayId"    # J
    .param p3, "brightness"    # I

    .line 24
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateDozeBrightness(JI)V

    .line 27
    :cond_0
    return-void
.end method

.method public updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 46
    .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V

    .line 49
    :cond_0
    return-void
.end method

.method public updateScreenEffect(I)V
    .locals 1
    .param p1, "state"    # I

    .line 17
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateScreenEffect(I)V

    .line 20
    :cond_0
    return-void
.end method

.method public updateScreenGrayscaleState(I)V
    .locals 1
    .param p1, "state"    # I

    .line 53
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->mDisplayFeatureInternal:Lcom/android/server/display/DisplayFeatureManagerInternal;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateScreenGrayscaleState(I)V

    .line 56
    :cond_0
    return-void
.end method
