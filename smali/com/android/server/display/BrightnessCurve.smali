.class public Lcom/android/server/display/BrightnessCurve;
.super Ljava/lang/Object;
.source "BrightnessCurve.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/BrightnessCurve$Curve;,
        Lcom/android/server/display/BrightnessCurve$DarkLightCurve;,
        Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;,
        Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;,
        Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;,
        Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;
    }
.end annotation


# static fields
.field private static final DARK_LIGHT_CURVE_HEAD_LUX_INDEX:I = 0x0

.field private static final DARK_LIGHT_CURVE_RADIO:F = 0.7f

.field private static final DARK_LIGHT_CURVE_TAIL_LUX_INDEX:I = 0x1

.field private static final FIRST_OUTDOOR_LIGHT_CURVE_TAIL_INDEX:I = 0x3

.field private static final INDOOR_LIGHT_CURVE_TAIL_LUX_INDEX:I = 0x2

.field private static final SECOND_OUTDOOR_LIGHT_CURVE_TAIL_INDEX:I = 0x4

.field private static final TAG:Ljava/lang/String; = "BrightnessCurve"


# instance fields
.field private mBrightness:[F

.field private final mBrightnessCurveAlgoAvailable:Z

.field private final mBrightnessMinTan:F

.field private mBrightnessToNit:Landroid/util/Spline;

.field private mConfig:Landroid/hardware/display/BrightnessConfiguration;

.field private mCurrentCurveInterval:[F

.field private final mCurveAnchorPointLux:F

.field private final mCustomBrighteningCurveInterval:[F

.field private final mCustomDarkeningCurveInterval:[F

.field private final mDarkLightCurvePullUpLimitNit:F

.field private final mDarkLightCurvePullUpMaxTan:F

.field private mDefault:Landroid/hardware/display/BrightnessConfiguration;

.field private final mDefaultCurveInterval:[F

.field private final mIndoorLightCurveTan:F

.field private mLux:[F

.field private mLuxToNit:Landroid/util/Spline;

.field private mLuxToNitsDefault:Landroid/util/Spline;

.field private mMaxLux:F

.field private mMinNit:F

.field private mNitToBrightness:Landroid/util/Spline;

.field private final mSecondOutdoorCurveBrightenMinTan:F

.field private final mThirdOutdoorCurveBrightenMinTan:F


# direct methods
.method static bridge synthetic -$$Nest$fgetmBrightnessMinTan(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessMinTan:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentCurveInterval(Lcom/android/server/display/BrightnessCurve;)[F
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/BrightnessCurve;->mCurrentCurveInterval:[F

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurveAnchorPointLux(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mCurveAnchorPointLux:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDarkLightCurvePullUpLimitNit(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpLimitNit:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDarkLightCurvePullUpMaxTan(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpMaxTan:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIndoorLightCurveTan(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mIndoorLightCurveTan:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMaxLux(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mMaxLux:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSecondOutdoorCurveBrightenMinTan(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mSecondOutdoorCurveBrightenMinTan:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmThirdOutdoorCurveBrightenMinTan(Lcom/android/server/display/BrightnessCurve;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/BrightnessCurve;->mThirdOutdoorCurveBrightenMinTan:F

    return p0
.end method

.method static bridge synthetic -$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/display/BrightnessCurve;->copyToDefaultSpline(FFLjava/util/List;FF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mpullDownCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/android/server/display/BrightnessCurve;->pullDownCurveCreate(FFLjava/util/List;FFFF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mpullUpConnectTail(Lcom/android/server/display/BrightnessCurve;Landroid/util/Pair;FFLjava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/display/BrightnessCurve;->pullUpConnectTail(Landroid/util/Pair;FFLjava/util/List;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mpullUpCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/android/server/display/BrightnessCurve;->pullUpCurveCreate(FFLjava/util/List;FFFF)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/display/BrightnessConfiguration;Landroid/util/Spline;Landroid/util/Spline;)V
    .locals 3
    .param p1, "defaultConfig"    # Landroid/hardware/display/BrightnessConfiguration;
    .param p2, "nitsToBrightnessSpline"    # Landroid/util/Spline;
    .param p3, "brightnessToNitsSpline"    # Landroid/util/Spline;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    .line 56
    iput-object p1, p0, Lcom/android/server/display/BrightnessCurve;->mDefault:Landroid/hardware/display/BrightnessConfiguration;

    .line 57
    iput-object p2, p0, Lcom/android/server/display/BrightnessCurve;->mNitToBrightness:Landroid/util/Spline;

    .line 58
    iput-object p3, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessToNit:Landroid/util/Spline;

    .line 59
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    .line 60
    .local v0, "resources":Landroid/content/res/Resources;
    nop

    .line 61
    const v1, 0x1103003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 60
    invoke-static {v1}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mDefaultCurveInterval:[F

    .line 62
    nop

    .line 63
    const v2, 0x11030037

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 62
    invoke-static {v2}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mCustomBrighteningCurveInterval:[F

    .line 64
    nop

    .line 65
    const v2, 0x11030038

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 64
    invoke-static {v2}, Lcom/android/server/display/BrightnessMappingStrategy;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mCustomDarkeningCurveInterval:[F

    .line 66
    const v2, 0x11070026

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessMinTan:F

    .line 67
    const v2, 0x11070033

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mSecondOutdoorCurveBrightenMinTan:F

    .line 69
    const v2, 0x11070039

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mThirdOutdoorCurveBrightenMinTan:F

    .line 71
    const v2, 0x11070027

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mCurveAnchorPointLux:F

    .line 73
    const v2, 0x11070029

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpMaxTan:F

    .line 75
    const v2, 0x11070028

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mDarkLightCurvePullUpLimitNit:F

    .line 77
    const v2, 0x1105000e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessCurveAlgoAvailable:Z

    .line 79
    const v2, 0x1107002c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/BrightnessCurve;->mIndoorLightCurveTan:F

    .line 81
    iput-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mCurrentCurveInterval:[F

    .line 82
    return-void
.end method

.method private buildCurve(FF)Ljava/util/List;
    .locals 9
    .param p1, "lux"    # F
    .param p2, "changeNit"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Ljava/util/List<",
            "Lcom/android/server/display/BrightnessCurve$Curve;",
            ">;"
        }
    .end annotation

    .line 162
    new-instance v0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;

    invoke-direct {v0, p0}, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 163
    .local v0, "darkLightCurve":Lcom/android/server/display/BrightnessCurve$Curve;
    new-instance v1, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;

    invoke-direct {v1, p0}, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 164
    .local v1, "indoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve;
    new-instance v2, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;

    invoke-direct {v2, p0}, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 165
    .local v2, "firstOutdoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve;
    new-instance v3, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;

    invoke-direct {v3, p0}, Lcom/android/server/display/BrightnessCurve$SecondOutdoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 166
    .local v3, "secondOutdoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve;
    new-instance v4, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;

    invoke-direct {v4, p0}, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 167
    .local v4, "thirdOutdoorLightCurve":Lcom/android/server/display/BrightnessCurve$Curve;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v5, "curveList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;"
    iget-object v6, p0, Lcom/android/server/display/BrightnessCurve;->mCurrentCurveInterval:[F

    const/4 v7, 0x0

    aget v7, v6, v7

    cmpl-float v7, p1, v7

    const/4 v8, 0x1

    if-ltz v7, :cond_0

    aget v7, v6, v8

    cmpg-float v7, p1, v7

    if-gez v7, :cond_0

    .line 170
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V

    .line 171
    invoke-virtual {v1, v0}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 172
    invoke-virtual {v2, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 173
    invoke-virtual {v3, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 174
    invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    goto :goto_0

    .line 175
    :cond_0
    aget v7, v6, v8

    cmpl-float v7, p1, v7

    const/4 v8, 0x2

    if-ltz v7, :cond_1

    aget v7, v6, v8

    cmpg-float v7, p1, v7

    if-gez v7, :cond_1

    .line 177
    invoke-virtual {v1, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V

    .line 178
    invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 179
    invoke-virtual {v2, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 180
    invoke-virtual {v3, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 181
    invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    goto :goto_0

    .line 182
    :cond_1
    aget v7, v6, v8

    cmpl-float v7, p1, v7

    const/4 v8, 0x3

    if-ltz v7, :cond_2

    aget v7, v6, v8

    cmpg-float v7, p1, v7

    if-gez v7, :cond_2

    .line 184
    invoke-virtual {v2, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V

    .line 185
    invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 186
    invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 187
    invoke-virtual {v3, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 188
    invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    goto :goto_0

    .line 189
    :cond_2
    aget v7, v6, v8

    cmpl-float v7, p1, v7

    const/4 v8, 0x4

    if-ltz v7, :cond_3

    aget v7, v6, v8

    cmpg-float v7, p1, v7

    if-gez v7, :cond_3

    .line 191
    invoke-virtual {v3, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V

    .line 192
    invoke-virtual {v2, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 193
    invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 194
    invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 195
    invoke-virtual {v4, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V

    goto :goto_0

    .line 196
    :cond_3
    aget v6, v6, v8

    cmpl-float v6, p1, v6

    if-ltz v6, :cond_4

    .line 197
    invoke-virtual {v4, p1, p2}, Lcom/android/server/display/BrightnessCurve$Curve;->create(FF)V

    .line 198
    invoke-virtual {v3, v4}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 199
    invoke-virtual {v2, v3}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 200
    invoke-virtual {v1, v2}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 201
    invoke-virtual {v0, v1}, Lcom/android/server/display/BrightnessCurve$Curve;->connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V

    .line 203
    :cond_4
    :goto_0
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    return-object v5
.end method

.method private computeBrightness()V
    .locals 5

    .line 149
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mLux:[F

    array-length v2, v1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightness:[F

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 150
    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mLuxToNit:Landroid/util/Spline;

    aget v1, v1, v0

    invoke-virtual {v2, v1}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    .line 151
    .local v1, "nit":F
    iget v2, p0, Lcom/android/server/display/BrightnessCurve;->mMinNit:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 152
    iget v1, p0, Lcom/android/server/display/BrightnessCurve;->mMinNit:F

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightness:[F

    iget-object v3, p0, Lcom/android/server/display/BrightnessCurve;->mNitToBrightness:Landroid/util/Spline;

    invoke-virtual {v3, v1}, Landroid/util/Spline;->interpolate(F)F

    move-result v3

    aput v3, v2, v0

    .line 155
    if-eqz v0, :cond_1

    .line 156
    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightness:[F

    aget v3, v2, v0

    add-int/lit8 v4, v0, -0x1

    aget v4, v2, v4

    invoke-static {v3, v4}, Landroid/util/MathUtils;->max(FF)F

    move-result v3

    aput v3, v2, v0

    .line 149
    .end local v1    # "nit":F
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method private copyToDefaultSpline(FFLjava/util/List;FF)V
    .locals 6
    .param p1, "startLux"    # F
    .param p2, "endLux"    # F
    .param p4, "diffNit"    # F
    .param p5, "ratio"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;FF)V"
        }
    .end annotation

    .line 213
    .local p3, "mPointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mLux:[F

    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget v1, v1, v0

    cmpg-float v2, v1, p2

    if-gtz v2, :cond_1

    .line 214
    cmpg-float v2, v1, p1

    if-gez v2, :cond_0

    .line 215
    goto :goto_1

    .line 217
    :cond_0
    sub-float v1, p2, v1

    mul-float/2addr v1, p5

    sub-float v2, p2, p1

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v1

    .line 218
    .local v2, "coefficient":F
    new-instance v1, Landroid/util/Pair;

    iget-object v3, p0, Lcom/android/server/display/BrightnessCurve;->mLux:[F

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    iget-object v5, p0, Lcom/android/server/display/BrightnessCurve;->mLux:[F

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Landroid/util/Spline;->interpolate(F)F

    move-result v4

    mul-float v5, p4, v2

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 220
    .local v1, "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    .end local v1    # "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    .end local v2    # "coefficient":F
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private createSpline(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/display/BrightnessCurve$Curve;",
            ">;)V"
        }
    .end annotation

    .line 124
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;"
    invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve;->getSplinePointList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 125
    .local v0, "splinePointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [F

    .line 126
    .local v1, "lux":[F
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [F

    .line 127
    .local v2, "nit":[F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 128
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 129
    .local v4, "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    aput v5, v1, v3

    .line 130
    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    aput v5, v2, v3

    .line 127
    .end local v4    # "point":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 132
    .end local v3    # "i":I
    :cond_0
    invoke-static {v1, v2}, Landroid/util/Spline;->createLinearSpline([F[F)Landroid/util/Spline;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/display/BrightnessCurve;->mLuxToNit:Landroid/util/Spline;

    .line 133
    return-void
.end method

.method private getSplinePointList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/display/BrightnessCurve$Curve;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation

    .line 136
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v0, "splinePointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/display/BrightnessCurve$Curve;

    iget-object v2, v2, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 138
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/display/BrightnessCurve$Curve;

    iget-object v2, v2, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/display/BrightnessCurve$Curve;

    .line 140
    .local v2, "curve":Lcom/android/server/display/BrightnessCurve$Curve;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1
    iget-object v4, v2, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 141
    iget-object v4, v2, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 143
    .end local v2    # "curve":Lcom/android/server/display/BrightnessCurve$Curve;
    .end local v3    # "i":I
    :cond_0
    goto :goto_0

    .line 145
    :cond_1
    return-object v0
.end method

.method private pullDownCurveCreate(FFLjava/util/List;FFFF)V
    .locals 13
    .param p1, "lux"    # F
    .param p2, "changeNit"    # F
    .param p4, "headLux"    # F
    .param p5, "tailLux"    # F
    .param p6, "minTanToHead"    # F
    .param p7, "minTanToTail"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;FFFF)V"
        }
    .end annotation

    .line 226
    .local p3, "pointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;"
    move-object v0, p0

    move-object/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, p5

    iget-object v4, v0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    invoke-virtual {v4, v3}, Landroid/util/Spline;->interpolate(F)F

    move-result v4

    iget-object v5, v0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/util/Spline;->interpolate(F)F

    move-result v5

    sub-float/2addr v4, v5

    div-float/2addr v4, v3

    .line 228
    .local v4, "tan0LuxToTailLux":F
    move/from16 v5, p6

    .line 229
    .local v5, "tanToHeadLux":F
    cmpl-float v7, p1, v2

    if-eqz v7, :cond_0

    .line 230
    iget-object v7, v0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    invoke-virtual {v7, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v7

    sub-float v7, p2, v7

    sub-float v8, p1, v2

    div-float v5, v7, v8

    .line 232
    :cond_0
    cmpg-float v7, v5, p6

    if-gez v7, :cond_1

    .line 233
    move/from16 v5, p6

    .line 235
    :cond_1
    sub-float v7, p1, v2

    mul-float/2addr v7, v5

    sub-float v7, p2, v7

    .line 236
    .local v7, "headPointNit":F
    new-instance v8, Landroid/util/Pair;

    invoke-static/range {p4 .. p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 237
    .local v8, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    cmpl-float v9, p1, v2

    if-eqz v9, :cond_2

    .line 239
    new-instance v9, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 240
    .local v9, "changePoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    .end local v9    # "changePoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    :cond_2
    iget-object v9, v0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    invoke-virtual {v9, v6}, Landroid/util/Spline;->interpolate(F)F

    move-result v6

    sub-float v6, p2, v6

    div-float/2addr v6, p1

    .line 243
    .local v6, "tanToStartPoint":F
    cmpl-float v9, v6, v4

    if-lez v9, :cond_3

    .line 244
    new-instance v9, Landroid/util/Pair;

    invoke-static/range {p5 .. p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    iget-object v11, v0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    invoke-virtual {v11, v3}, Landroid/util/Spline;->interpolate(F)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 245
    .local v9, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    return-void

    .line 248
    .end local v9    # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    :cond_3
    cmpg-float v9, v6, p7

    if-gez v9, :cond_4

    .line 249
    move/from16 v6, p7

    .line 251
    :cond_4
    sub-float v9, v3, p1

    mul-float/2addr v9, v6

    add-float/2addr v9, p2

    .line 252
    .local v9, "tailPointNit":F
    new-instance v10, Landroid/util/Pair;

    invoke-static/range {p5 .. p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 253
    .local v10, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    return-void
.end method

.method private pullUpConnectTail(Landroid/util/Pair;FFLjava/util/List;)V
    .locals 7
    .param p2, "tailLux"    # F
    .param p3, "minTan"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;FF",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;)V"
        }
    .end annotation

    .line 269
    .local p1, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    .local p4, "pointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;"
    invoke-interface {p4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 271
    .local v0, "lux":F
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 272
    .local v1, "changeNit":F
    move v2, p3

    .line 273
    .local v2, "tanToTailPoint":F
    cmpl-float v3, p2, v0

    if-eqz v3, :cond_0

    .line 274
    iget-object v3, p0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    invoke-virtual {v3, p2}, Landroid/util/Spline;->interpolate(F)F

    move-result v3

    sub-float/2addr v3, v1

    sub-float v4, p2, v0

    div-float v2, v3, v4

    .line 276
    :cond_0
    cmpg-float v3, v2, p3

    if-gez v3, :cond_1

    .line 277
    move v2, p3

    .line 279
    :cond_1
    sub-float v3, p2, v0

    mul-float/2addr v3, v2

    add-float/2addr v3, v1

    .line 280
    .local v3, "tailPointNit":F
    new-instance v4, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 281
    .local v4, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {p4, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    return-void
.end method

.method private pullUpCurveCreate(FFLjava/util/List;FFFF)V
    .locals 7
    .param p1, "lux"    # F
    .param p2, "changeNit"    # F
    .param p4, "headLux"    # F
    .param p5, "tailLux"    # F
    .param p6, "anchorPointLux"    # F
    .param p7, "anchorPointNit"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;FFFF)V"
        }
    .end annotation

    .line 258
    .local p3, "pointList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;>;"
    sub-float v0, p2, p7

    sub-float v1, p1, p6

    div-float/2addr v0, v1

    .line 259
    .local v0, "tanToAnchor":F
    sub-float v1, p1, p4

    mul-float/2addr v1, v0

    sub-float v1, p2, v1

    .line 260
    .local v1, "headPointNit":F
    new-instance v2, Landroid/util/Pair;

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 261
    .local v2, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    sub-float v3, p5, p1

    mul-float/2addr v3, v0

    add-float/2addr v3, p2

    .line 263
    .local v3, "tailPointNit":F
    new-instance v4, Landroid/util/Pair;

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 264
    .local v4, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    return-void
.end method

.method private setCurveInterval(Landroid/hardware/display/BrightnessConfiguration;)V
    .locals 2
    .param p1, "config"    # Landroid/hardware/display/BrightnessConfiguration;

    .line 102
    invoke-virtual {p1}, Landroid/hardware/display/BrightnessConfiguration;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "description":Ljava/lang/String;
    const-string v1, "custom_brightness_curve_brightening"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mCustomBrighteningCurveInterval:[F

    iput-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mCurrentCurveInterval:[F

    goto :goto_0

    .line 105
    :cond_0
    const-string v1, "custom_brightness_curve_darkening"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mCustomDarkeningCurveInterval:[F

    iput-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mCurrentCurveInterval:[F

    goto :goto_0

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mDefaultCurveInterval:[F

    iput-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mCurrentCurveInterval:[F

    .line 110
    :goto_0
    return-void
.end method

.method private updateData([F[F)V
    .locals 3
    .param p1, "lux"    # [F
    .param p2, "brightness"    # [F

    .line 92
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessCurve;->setCurveInterval(Landroid/hardware/display/BrightnessConfiguration;)V

    .line 93
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getCurve()Landroid/util/Pair;

    move-result-object v0

    .line 94
    .local v0, "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [F

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, [F

    invoke-static {v1, v2}, Landroid/util/Spline;->createLinearSpline([F[F)Landroid/util/Spline;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mLuxToNitsDefault:Landroid/util/Spline;

    .line 95
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessToNit:Landroid/util/Spline;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/BrightnessCurve;->mMinNit:F

    .line 96
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [F

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, [F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    iput v1, p0, Lcom/android/server/display/BrightnessCurve;->mMaxLux:F

    .line 97
    iput-object p1, p0, Lcom/android/server/display/BrightnessCurve;->mLux:[F

    .line 98
    iput-object p2, p0, Lcom/android/server/display/BrightnessCurve;->mBrightness:[F

    .line 99
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 640
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 641
    const-string v0, "BrightnessCurve Configuration:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBrightnessCurveAlgoAvailable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessCurveAlgoAvailable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 644
    return-void
.end method

.method public isEnable()Z
    .locals 3

    .line 629
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 630
    .local v0, "description":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessCurveAlgoAvailable:Z

    if-eqz v1, :cond_1

    const-string v1, "custom_brightness_curve_default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 631
    const-string v1, "custom_brightness_curve_brightening"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 632
    const-string v1, "custom_brightness_curve_darkening"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mDefault:Landroid/hardware/display/BrightnessConfiguration;

    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    .line 633
    invoke-virtual {v1, v2}, Landroid/hardware/display/BrightnessConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 634
    :cond_0
    const/4 v1, 0x1

    return v1

    .line 636
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public smoothNewCurveV2([F[FI)Landroid/util/Pair;
    .locals 5
    .param p1, "lux"    # [F
    .param p2, "brightness"    # [F
    .param p3, "idx"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F[FI)",
            "Landroid/util/Pair<",
            "[F[F>;"
        }
    .end annotation

    .line 113
    aget v0, p1, p3

    .line 114
    .local v0, "changeLux":F
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessToNit:Landroid/util/Spline;

    aget v2, p2, p3

    invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    .line 115
    .local v1, "changeNit":F
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "smoothNewCurveV2: changeLux: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", changeNit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BrightnessCurve"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/BrightnessCurve;->updateData([F[F)V

    .line 117
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/BrightnessCurve;->buildCurve(FF)Ljava/util/List;

    move-result-object v2

    .line 118
    .local v2, "listPoint":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/BrightnessCurve$Curve;>;"
    invoke-direct {p0, v2}, Lcom/android/server/display/BrightnessCurve;->createSpline(Ljava/util/List;)V

    .line 119
    invoke-direct {p0}, Lcom/android/server/display/BrightnessCurve;->computeBrightness()V

    .line 120
    iget-object v3, p0, Lcom/android/server/display/BrightnessCurve;->mLux:[F

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve;->mBrightness:[F

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    return-object v3
.end method

.method public updateSplineConfig(Landroid/hardware/display/BrightnessConfiguration;Landroid/util/Spline;Landroid/util/Spline;)V
    .locals 0
    .param p1, "config"    # Landroid/hardware/display/BrightnessConfiguration;
    .param p2, "nitsToBrightnessSpline"    # Landroid/util/Spline;
    .param p3, "brightnessToNitsSpline"    # Landroid/util/Spline;

    .line 86
    iput-object p1, p0, Lcom/android/server/display/BrightnessCurve;->mConfig:Landroid/hardware/display/BrightnessConfiguration;

    .line 87
    iput-object p2, p0, Lcom/android/server/display/BrightnessCurve;->mNitToBrightness:Landroid/util/Spline;

    .line 88
    iput-object p3, p0, Lcom/android/server/display/BrightnessCurve;->mBrightnessToNit:Landroid/util/Spline;

    .line 89
    return-void
.end method
