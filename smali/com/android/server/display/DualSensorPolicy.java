public class com.android.server.display.DualSensorPolicy {
	 /* .source "DualSensorPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/DualSensorPolicy$DualSensorPolicyHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ASSISTANT_LIGHT_SENSOR_TYPE;
private static Boolean DEBUG;
private static final Integer MSG_UPDATE_ASSISTANT_LIGHT_SENSOR_AMBIENT_LUX;
private static final java.lang.String TAG;
/* # instance fields */
private Integer mAmbientLightHorizonLong;
private Integer mAmbientLightHorizonShort;
private Boolean mAssistAmbientLuxValid;
private Float mAssistBrighteningThreshold;
private Float mAssistDarkeningThreshold;
private Float mAssistFastAmbientLux;
private android.hardware.Sensor mAssistLightSensor;
private Long mAssistLightSensorBrighteningDebounce;
private Long mAssistLightSensorDarkeningDebounce;
private Boolean mAssistLightSensorEnable;
private Long mAssistLightSensorEnableTime;
private com.android.server.display.AmbientLightRingBuffer mAssistLightSensorRingBuffer;
private Integer mAssistLightSensorWarmUpTime;
private Float mAssistSlowAmbientLux;
private com.android.server.display.AutomaticBrightnessControllerImpl mBrightnessControllerImpl;
private Integer mCurrentAssistLightSensorRate;
private com.android.server.display.HysteresisLevelsStub mHysteresisLevelsImpl;
private Integer mLightSensorRate;
private com.android.server.display.AutomaticBrightnessControllerStub$DualSensorPolicyListener mListener;
private Float mMainFastAmbientLux;
private Float mMainSlowAmbientLux;
private android.os.Handler mPolicyHandler;
private com.android.server.display.SceneDetector mSceneDetector;
private android.hardware.SensorEventListener mSensorListener;
private android.hardware.SensorManager mSensorManager;
private Integer mUseLightSensorFlag;
/* # direct methods */
static void -$$Nest$mhandleAssistLightSensorEvent ( com.android.server.display.DualSensorPolicy p0, Long p1, Float p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DualSensorPolicy;->handleAssistLightSensorEvent(JF)V */
	 return;
} // .end method
static void -$$Nest$mupdateAssistLightSensorAmbientLux ( com.android.server.display.DualSensorPolicy p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/DualSensorPolicy;->updateAssistLightSensorAmbientLux()V */
	 return;
} // .end method
static com.android.server.display.DualSensorPolicy ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.display.DualSensorPolicy.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.display.DualSensorPolicy ( ) {
	 /* .locals 16 */
	 /* .param p1, "looper" # Landroid/os/Looper; */
	 /* .param p2, "sensormanager" # Landroid/hardware/SensorManager; */
	 /* .param p3, "lightSensorWarmUpTime" # I */
	 /* .param p4, "lightSensorRate" # I */
	 /* .param p5, "brighteningLightDebounceConfig" # J */
	 /* .param p7, "darkeningLightDebounceConfig" # J */
	 /* .param p9, "ambientLightHorizonLong" # I */
	 /* .param p10, "ambientLightHorizonShort" # I */
	 /* .param p11, "hysteresisLevelsImpl" # Lcom/android/server/display/HysteresisLevelsStub; */
	 /* .param p12, "listener" # Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener; */
	 /* .param p13, "brightnessControllerImpl" # Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
	 /* .line 70 */
	 /* move-object/from16 v0, p0 */
	 /* move-object/from16 v1, p2 */
	 /* move/from16 v2, p4 */
	 /* move/from16 v3, p9 */
	 /* invoke-direct/range {p0 ..p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 38 */
	 int v4 = -1; // const/4 v4, -0x1
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I */
	 /* .line 56 */
	 /* const/high16 v4, -0x40800000 # -1.0f */
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
	 /* .line 57 */
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
	 /* .line 59 */
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
	 /* .line 60 */
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
	 /* .line 62 */
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
	 /* .line 63 */
	 /* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
	 /* .line 91 */
	 /* new-instance v4, Lcom/android/server/display/DualSensorPolicy$1; */
	 /* invoke-direct {v4, v0}, Lcom/android/server/display/DualSensorPolicy$1;-><init>(Lcom/android/server/display/DualSensorPolicy;)V */
	 this.mSensorListener = v4;
	 /* .line 71 */
	 /* new-instance v4, Lcom/android/server/display/DualSensorPolicy$DualSensorPolicyHandler; */
	 /* move-object/from16 v5, p1 */
	 /* invoke-direct {v4, v0, v5}, Lcom/android/server/display/DualSensorPolicy$DualSensorPolicyHandler;-><init>(Lcom/android/server/display/DualSensorPolicy;Landroid/os/Looper;)V */
	 this.mPolicyHandler = v4;
	 /* .line 72 */
	 this.mSensorManager = v1;
	 /* .line 73 */
	 /* const v4, 0x1fa266f */
	 (( android.hardware.SensorManager ) v1 ).getDefaultSensor ( v4 ); // invoke-virtual {v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
	 this.mAssistLightSensor = v4;
	 /* .line 74 */
	 /* move-object/from16 v4, p12 */
	 this.mListener = v4;
	 /* .line 75 */
	 /* move/from16 v6, p3 */
	 /* iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorWarmUpTime:I */
	 /* .line 76 */
	 /* iput v2, v0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I */
	 /* .line 77 */
	 /* move-wide/from16 v7, p5 */
	 /* iput-wide v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorBrighteningDebounce:J */
	 /* .line 78 */
	 /* move-wide/from16 v9, p7 */
	 /* iput-wide v9, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorDarkeningDebounce:J */
	 /* .line 79 */
	 /* iput v3, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I */
	 /* .line 80 */
	 /* move/from16 v11, p10 */
	 /* iput v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonShort:I */
	 /* .line 81 */
	 /* move-object/from16 v12, p11 */
	 this.mHysteresisLevelsImpl = v12;
	 /* .line 82 */
	 /* new-instance v13, Lcom/android/server/display/AmbientLightRingBuffer; */
	 /* int-to-long v14, v2 */
	 /* invoke-direct {v13, v14, v15, v3}, Lcom/android/server/display/AmbientLightRingBuffer;-><init>(JI)V */
	 this.mAssistLightSensorRingBuffer = v13;
	 /* .line 83 */
	 /* iget-wide v13, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorBrighteningDebounce:J */
	 /* iget-wide v1, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorDarkeningDebounce:J */
	 /* invoke-direct {v0, v13, v14, v1, v2}, Lcom/android/server/display/DualSensorPolicy;->setUpDebounceConfig(JJ)V */
	 /* .line 84 */
	 /* move-object/from16 v1, p13 */
	 this.mBrightnessControllerImpl = v1;
	 /* .line 85 */
	 return;
} // .end method
private void handleAssistLightSensorEvent ( Long p0, Float p1 ) {
	 /* .locals 5 */
	 /* .param p1, "time" # J */
	 /* .param p3, "lux" # F */
	 /* .line 111 */
	 v0 = this.mPolicyHandler;
	 int v1 = 0; // const/4 v1, 0x0
	 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 113 */
	 v0 = this.mAssistLightSensorRingBuffer;
	 v0 = 	 (( com.android.server.display.AmbientLightRingBuffer ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
	 /* if-nez v0, :cond_0 */
	 /* .line 114 */
	 /* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I */
	 /* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I */
	 /* if-eq v0, v1, :cond_0 */
	 /* .line 115 */
	 /* iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I */
	 /* .line 116 */
	 v0 = this.mSensorManager;
	 v1 = this.mSensorListener;
	 (( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
	 /* .line 117 */
	 v0 = this.mSensorManager;
	 v1 = this.mSensorListener;
	 v2 = this.mAssistLightSensor;
	 /* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I */
	 /* mul-int/lit16 v3, v3, 0x3e8 */
	 v4 = this.mPolicyHandler;
	 (( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
	 /* .line 122 */
} // :cond_0
v0 = this.mBrightnessControllerImpl;
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).getIsTorchOpen ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getIsTorchOpen()Z
/* if-nez v0, :cond_2 */
v0 = this.mBrightnessControllerImpl;
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).checkAssistSensorValid ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkAssistSensorValid()Z
/* if-nez v0, :cond_1 */
/* .line 127 */
} // :cond_1
v0 = this.mAssistLightSensorRingBuffer;
/* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I */
/* int-to-long v1, v1 */
/* sub-long v1, p1, v1 */
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).prune ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V
/* .line 128 */
v0 = this.mAssistLightSensorRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).push ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/AmbientLightRingBuffer;->push(JF)V
/* .line 130 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DualSensorPolicy;->updateAssistLightSensorAmbientLux(J)V */
/* .line 131 */
return;
/* .line 123 */
} // :cond_2
} // :goto_0
final String v0 = "DualSensorPolicy"; // const-string v0, "DualSensorPolicy"
final String v1 = "handleAssistantLightSensorEvent: drop assistant light sensor lux due to flash events or within one second of turning off the torch."; // const-string v1, "handleAssistantLightSensorEvent: drop assistant light sensor lux due to flash events or within one second of turning off the torch."
android.util.Slog .d ( v0,v1 );
/* .line 125 */
return;
} // .end method
private Boolean isSkipAmbientBrighteningThreshold ( Float p0 ) {
/* .locals 5 */
/* .param p1, "currentAmbientLux" # F */
/* .line 505 */
v0 = this.mBrightnessControllerImpl;
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).isAnimating ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAnimating()Z
/* .line 506 */
/* .local v0, "isAnimating":Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 507 */
/* .line 509 */
} // :cond_0
v2 = v2 = this.mListener;
/* .line 510 */
/* .local v2, "preAmbientLux":F */
v3 = this.mBrightnessControllerImpl;
v3 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v3 ).isBrighteningDirection ( ); // invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isBrighteningDirection()Z
/* .line 511 */
/* .local v3, "isBrighteningDirection":Z */
/* cmpl-float v4, p1, v2 */
/* if-lez v4, :cond_1 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 512 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isSkipAmbientBrighteningThreshold: currentAmbientLux: "; // const-string v4, "isSkipAmbientBrighteningThreshold: currentAmbientLux: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", preAmbientLux: "; // const-string v4, ", preAmbientLux: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DualSensorPolicy"; // const-string v4, "DualSensorPolicy"
android.util.Slog .d ( v4,v1 );
/* .line 514 */
int v1 = 1; // const/4 v1, 0x1
/* .line 516 */
} // :cond_1
} // .end method
private Boolean isSkipAmbientDarkeningThreshold ( Float p0 ) {
/* .locals 5 */
/* .param p1, "currentAmbientLux" # F */
/* .line 520 */
v0 = this.mBrightnessControllerImpl;
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).isAnimating ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAnimating()Z
/* .line 521 */
/* .local v0, "isAnimating":Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 522 */
/* .line 524 */
} // :cond_0
v2 = v2 = this.mListener;
/* .line 525 */
/* .local v2, "preAmbientLux":F */
v3 = this.mBrightnessControllerImpl;
v3 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v3 ).isBrighteningDirection ( ); // invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isBrighteningDirection()Z
/* .line 526 */
/* .local v3, "isBrighteningDirection":Z */
/* cmpg-float v4, p1, v2 */
/* if-gez v4, :cond_1 */
/* if-nez v3, :cond_1 */
/* .line 527 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "isSkipAmbientDarkeningThreshold: currentAmbientLux: "; // const-string v4, "isSkipAmbientDarkeningThreshold: currentAmbientLux: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", preAmbientLux: "; // const-string v4, ", preAmbientLux: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DualSensorPolicy"; // const-string v4, "DualSensorPolicy"
android.util.Slog .d ( v4,v1 );
/* .line 529 */
int v1 = 1; // const/4 v1, 0x1
/* .line 531 */
} // :cond_1
} // .end method
private void setUpDebounceConfig ( Long p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "brighteningDebounce" # J */
/* .param p3, "darkeningDebounce" # J */
/* .line 163 */
v0 = this.mAssistLightSensorRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).setBrighteningDebounce ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/AmbientLightRingBuffer;->setBrighteningDebounce(J)V
/* .line 164 */
v0 = this.mAssistLightSensorRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).setDarkeningDebounce ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/android/server/display/AmbientLightRingBuffer;->setDarkeningDebounce(J)V
/* .line 165 */
return;
} // .end method
private void updateAssistLightSensorAmbientLux ( ) {
/* .locals 5 */
/* .line 168 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 169 */
/* .local v0, "time":J */
v2 = this.mAssistLightSensorRingBuffer;
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I */
/* int-to-long v3, v3 */
/* sub-long v3, v0, v3 */
(( com.android.server.display.AmbientLightRingBuffer ) v2 ).prune ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V
/* .line 170 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/DualSensorPolicy;->updateAssistLightSensorAmbientLux(J)V */
/* .line 171 */
return;
} // .end method
private void updateAssistLightSensorAmbientLux ( Long p0 ) {
/* .locals 5 */
/* .param p1, "time" # J */
/* .line 174 */
/* iget-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z */
/* if-nez v0, :cond_1 */
/* .line 175 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorWarmUpTime:I */
/* int-to-long v0, v0 */
/* iget-wide v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnableTime:J */
/* add-long/2addr v0, v2 */
/* .line 176 */
/* .local v0, "timeWhenAssistSensorWarmedUp":J */
/* cmp-long v2, p1, v0 */
/* if-gez v2, :cond_0 */
/* .line 177 */
v2 = this.mPolicyHandler;
int v3 = 0; // const/4 v3, 0x0
(( android.os.Handler ) v2 ).sendEmptyMessageAtTime ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z
/* .line 179 */
return;
/* .line 182 */
} // :cond_0
v2 = this.mAssistLightSensorRingBuffer;
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonShort:I */
/* int-to-long v3, v3 */
v2 = (( com.android.server.display.AmbientLightRingBuffer ) v2 ).calculateAmbientLux ( p1, p2, v3, v4 ); // invoke-virtual {v2, p1, p2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F
/* iput v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* .line 183 */
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( com.android.server.display.DualSensorPolicy ) p0 ).setAmbientLuxWhenInvalid ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/android/server/display/DualSensorPolicy;->setAmbientLuxWhenInvalid(IF)V
/* .line 184 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z */
/* .line 186 */
} // .end local v0 # "timeWhenAssistSensorWarmedUp":J
} // :cond_1
(( com.android.server.display.DualSensorPolicy ) p0 ).updateDualSensorPolicy ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/display/DualSensorPolicy;->updateDualSensorPolicy(JI)Z
/* .line 187 */
return;
} // .end method
/* # virtual methods */
protected void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 464 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 465 */
final String v0 = "Dual Sensor Policy State:"; // const-string v0, "Dual Sensor Policy State:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 466 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mUseLightSensorFlag="; // const-string v1, " mUseLightSensorFlag="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 467 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistAmbientLuxValid="; // const-string v1, " mAssistAmbientLuxValid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 468 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistFastAmbientLux="; // const-string v1, " mAssistFastAmbientLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 469 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistSlowAmbientLux="; // const-string v1, " mAssistSlowAmbientLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 470 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistBrighteningThreshold="; // const-string v1, " mAssistBrighteningThreshold="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 471 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistDarkeningThreshold="; // const-string v1, " mAssistDarkeningThreshold="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 472 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistLightSensorRingBuffer="; // const-string v1, " mAssistLightSensorRingBuffer="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAssistLightSensorRingBuffer;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 473 */
final String v0 = "Dual Sensor Policy Configuration:"; // const-string v0, "Dual Sensor Policy Configuration:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 474 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistLightSensorBrighteningDebounce="; // const-string v1, " mAssistLightSensorBrighteningDebounce="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorBrighteningDebounce:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 475 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAssistLightSensorDarkeningDebounce="; // const-string v1, " mAssistLightSensorDarkeningDebounce="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorDarkeningDebounce:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 476 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z */
com.android.server.display.DualSensorPolicy.DEBUG = (v0!= 0);
/* .line 477 */
return;
} // .end method
protected Float getAmbientLux ( Float p0, Float p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "preLux" # F */
/* .param p2, "updateLux" # F */
/* .param p3, "needUpdateLux" # Z */
/* .line 214 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* move v0, p2 */
} // :cond_0
/* move v0, p1 */
} // :goto_0
} // .end method
protected Float getAssistFastAmbientLux ( ) {
/* .locals 1 */
/* .line 487 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
} // .end method
protected android.hardware.Sensor getAssistLightSensor ( ) {
/* .locals 1 */
/* .line 480 */
v0 = this.mAssistLightSensor;
} // .end method
protected Float getMainAmbientLux ( ) {
/* .locals 1 */
/* .line 218 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
} // .end method
protected Float getMainFastAmbientLux ( ) {
/* .locals 1 */
/* .line 501 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
} // .end method
void setAmbientLuxWhenInvalid ( Integer p0, Float p1 ) {
/* .locals 5 */
/* .param p1, "event" # I */
/* .param p2, "lux" # F */
/* .line 427 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v0, :cond_2 */
/* .line 428 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
int v3 = 0; // const/4 v3, 0x0
/* if-eq v0, v2, :cond_1 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-eq v0, v2, :cond_1 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v0, v2, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpl-float v0, p2, v0 */
/* if-ltz v0, :cond_0 */
/* .line 437 */
} // :cond_0
/* iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
/* .line 438 */
/* iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* .line 439 */
v0 = this.mListener;
/* .line 440 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setAmbientLuxWhenInvalid: update brightness using assist light sensor in process, mMainFastAmbientLux: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DualSensorPolicy"; // const-string v1, "DualSensorPolicy"
android.util.Slog .d ( v1,v0 );
/* .line 432 */
} // :cond_1
} // :goto_0
/* iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 433 */
/* iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
/* .line 434 */
/* iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* .line 435 */
v0 = this.mListener;
/* .line 442 */
} // :cond_2
/* if-ne p1, v0, :cond_7 */
/* .line 443 */
/* iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* .line 444 */
/* iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
/* .line 445 */
v0 = v0 = this.mListener;
/* .line 446 */
/* .local v0, "mainBrighteningThreshold":F */
v2 = v2 = this.mListener;
/* .line 447 */
/* .local v2, "useDaemonSensorPolicy":Z */
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v3, v4, :cond_3 */
/* if-nez v2, :cond_5 */
} // :cond_3
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v3, v4, :cond_4 */
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpl-float v3, v3, v0 */
/* if-gtz v3, :cond_5 */
} // :cond_4
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v3, v4, :cond_6 */
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* iget v4, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
/* cmpl-float v4, v3, v4 */
/* if-ltz v4, :cond_6 */
/* iget v4, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* cmpl-float v3, v3, v4 */
/* if-lez v3, :cond_6 */
/* .line 454 */
} // :cond_5
/* iput v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 456 */
v3 = this.mListener;
/* xor-int/lit8 v4, v2, 0x1 */
/* .line 458 */
} // :cond_6
v1 = this.mHysteresisLevelsImpl;
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v1 = (( com.android.server.display.HysteresisLevelsStub ) v1 ).getBrighteningThreshold ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/display/HysteresisLevelsStub;->getBrighteningThreshold(F)F
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
/* .line 459 */
v1 = this.mHysteresisLevelsImpl;
/* iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v1 = (( com.android.server.display.HysteresisLevelsStub ) v1 ).getDarkeningThreshold ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/display/HysteresisLevelsStub;->getDarkeningThreshold(F)F
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
/* .line 461 */
} // .end local v0 # "mainBrighteningThreshold":F
} // .end local v2 # "useDaemonSensorPolicy":Z
} // :cond_7
} // :goto_1
return;
} // .end method
void setSceneDetector ( com.android.server.display.SceneDetector p0 ) {
/* .locals 0 */
/* .param p1, "sceneDetector" # Lcom/android/server/display/SceneDetector; */
/* .line 88 */
this.mSceneDetector = p1;
/* .line 89 */
return;
} // .end method
protected void setSensorEnabled ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .line 134 */
final String v0 = "DualSensorPolicy"; // const-string v0, "DualSensorPolicy"
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z */
/* if-nez v1, :cond_0 */
/* .line 135 */
/* const-string/jumbo v1, "setSensorEnabled: register the assist light sensor." */
android.util.Slog .i ( v0,v1 );
/* .line 136 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z */
/* .line 137 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnableTime:J */
/* .line 138 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I */
/* iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I */
/* .line 139 */
v1 = this.mSensorManager;
v2 = this.mSensorListener;
v3 = this.mAssistLightSensor;
/* mul-int/lit16 v0, v0, 0x3e8 */
v4 = this.mPolicyHandler;
(( android.hardware.SensorManager ) v1 ).registerListener ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 141 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 142 */
/* const-string/jumbo v1, "setSensorEnabled: unregister the assist light sensor." */
android.util.Slog .i ( v0,v1 );
/* .line 143 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z */
/* .line 144 */
/* iput-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z */
/* .line 145 */
v1 = this.mAssistLightSensorRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v1 ).clear ( ); // invoke-virtual {v1}, Lcom/android/server/display/AmbientLightRingBuffer;->clear()V
/* .line 146 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I */
/* .line 147 */
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 148 */
/* const/high16 v1, -0x40800000 # -1.0f */
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* .line 149 */
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
/* .line 150 */
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* .line 151 */
/* iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
/* .line 152 */
v1 = this.mPolicyHandler;
(( android.os.Handler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 153 */
v0 = this.mSensorManager;
v1 = this.mSensorListener;
v2 = this.mAssistLightSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 155 */
} // :cond_1
} // :goto_0
return;
} // .end method
protected Boolean updateBrightnessUsingMainLightSensor ( ) {
/* .locals 2 */
/* .line 237 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected Boolean updateDualSensorPolicy ( Long p0, Integer p1 ) {
/* .locals 26 */
/* .param p1, "time" # J */
/* .param p3, "event" # I */
/* .line 274 */
/* move-object/from16 v0, p0 */
/* move-wide/from16 v1, p1 */
/* move/from16 v3, p3 */
final String v5 = ", mAssistSlowAmbientLux: "; // const-string v5, ", mAssistSlowAmbientLux: "
final String v6 = ", mMainSlowAmbientLux: "; // const-string v6, ", mMainSlowAmbientLux: "
final String v8 = "DualSensorPolicy"; // const-string v8, "DualSensorPolicy"
/* if-ne v3, v4, :cond_12 */
/* .line 275 */
v4 = this.mListener;
/* move-result-wide v10 */
/* .line 276 */
/* .local v10, "nextBrightenTransition":J */
v4 = this.mListener;
/* move-result-wide v12 */
/* .line 277 */
/* .local v12, "nextDarkenTransition":J */
v4 = v4 = this.mListener;
/* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
/* .line 278 */
v4 = v4 = this.mListener;
/* iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* .line 279 */
v4 = v4 = this.mListener;
/* .line 280 */
/* .local v4, "mainBrighteningThreshold":F */
v14 = v14 = this.mListener;
/* .line 281 */
/* .local v14, "mainDarkeningThreshold":F */
/* iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
v15 = /* invoke-direct {v0, v15}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientBrighteningThreshold(F)Z */
/* .line 282 */
/* .local v15, "skipBrighteningThreshold":Z */
/* iget v9, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
v9 = /* invoke-direct {v0, v9}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientDarkeningThreshold(F)Z */
/* .line 283 */
/* .local v9, "skipDarkeningThreshold":Z */
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
/* cmpl-float v16, v7, v4 */
/* move-object/from16 v17, v5 */
final String v5 = ", next darkening threshold of main light sensor: "; // const-string v5, ", next darkening threshold of main light sensor: "
/* move/from16 v18, v9 */
} // .end local v9 # "skipDarkeningThreshold":Z
/* .local v18, "skipDarkeningThreshold":Z */
/* const-string/jumbo v9, "updateDualSensorPolicy: next brightening threshold of main light sensor: " */
/* move-wide/from16 v19, v12 */
} // .end local v12 # "nextDarkenTransition":J
/* .local v19, "nextDarkenTransition":J */
/* const-string/jumbo v12, "updateDualSensorPolicy: update brightness using assist light sensor in process, mMainFastAmbientLux: " */
final String v13 = ", mainDarkeningThreshold: "; // const-string v13, ", mainDarkeningThreshold: "
/* move/from16 v21, v7 */
final String v7 = ", mainBrighteningThreshold: "; // const-string v7, ", mainBrighteningThreshold: "
/* move-object/from16 v22, v5 */
/* const-string/jumbo v5, "updateDualSensorPolicy: update ambient lux using main light sensor, mMainFastAmbientLux: " */
/* move-object/from16 v23, v9 */
final String v9 = ", mAssistFastAmbientLux: "; // const-string v9, ", mAssistFastAmbientLux: "
/* if-ltz v16, :cond_0 */
/* move/from16 v16, v14 */
} // .end local v14 # "mainDarkeningThreshold":F
/* .local v16, "mainDarkeningThreshold":F */
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* cmpl-float v14, v14, v4 */
/* if-ltz v14, :cond_1 */
/* cmp-long v14, v10, v1 */
/* if-lez v14, :cond_2 */
} // .end local v16 # "mainDarkeningThreshold":F
/* .restart local v14 # "mainDarkeningThreshold":F */
} // :cond_0
/* move/from16 v16, v14 */
} // .end local v14 # "mainDarkeningThreshold":F
/* .restart local v16 # "mainDarkeningThreshold":F */
} // :cond_1
} // :goto_0
if ( v15 != null) { // if-eqz v15, :cond_6
/* .line 286 */
} // :cond_2
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* move-wide/from16 v24, v10 */
} // .end local v10 # "nextBrightenTransition":J
/* .local v24, "nextBrightenTransition":J */
/* if-eq v14, v10, :cond_4 */
/* iget v10, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v10, v11, :cond_3 */
/* iget v10, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* iget v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpl-float v10, v10, v11 */
/* if-ltz v10, :cond_3 */
/* .line 302 */
} // :cond_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v12 ); // invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v5 );
/* .line 305 */
v5 = this.mListener;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v7 = 0; // const/4 v7, 0x0
/* move/from16 v10, v16 */
/* .line 289 */
} // :cond_4
} // :goto_1
/* iput v9, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 290 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v10, v16 */
} // .end local v16 # "mainDarkeningThreshold":F
/* .local v10, "mainDarkeningThreshold":F */
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v5 );
/* .line 295 */
v5 = this.mBrightnessControllerImpl;
v5 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v5 ).isAonFlareEnabled ( ); // invoke-virtual {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 296 */
v5 = this.mSceneDetector;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v7 = 0; // const/4 v7, 0x0
(( com.android.server.display.SceneDetector ) v5 ).updateAmbientLux ( v3, v6, v7 ); // invoke-virtual {v5, v3, v6, v7}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V
/* .line 298 */
} // :cond_5
v5 = this.mListener;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v7 = 1; // const/4 v7, 0x1
/* .line 307 */
} // :goto_2
v5 = this.mListener;
/* move-result-wide v5 */
/* .line 308 */
} // .end local v24 # "nextBrightenTransition":J
/* .local v5, "nextBrightenTransition":J */
v7 = this.mListener;
/* move-result-wide v12 */
/* .line 309 */
} // .end local v19 # "nextDarkenTransition":J
/* .restart local v12 # "nextDarkenTransition":J */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v11, v23 */
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = v9 = this.mListener;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
/* move-object/from16 v14, v22 */
(( java.lang.StringBuilder ) v7 ).append ( v14 ); // invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mListener;
v9 = /* .line 310 */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 309 */
android.util.Slog .d ( v8,v7 );
/* move/from16 v16, v15 */
/* goto/16 :goto_5 */
/* .line 311 */
} // .end local v5 # "nextBrightenTransition":J
} // .end local v12 # "nextDarkenTransition":J
/* .local v10, "nextBrightenTransition":J */
/* .restart local v16 # "mainDarkeningThreshold":F */
/* .restart local v19 # "nextDarkenTransition":J */
} // :cond_6
/* move-wide/from16 v24, v10 */
/* move/from16 v10, v16 */
/* move-object/from16 v14, v22 */
/* move-object/from16 v11, v23 */
} // .end local v16 # "mainDarkeningThreshold":F
/* .local v10, "mainDarkeningThreshold":F */
/* .restart local v24 # "nextBrightenTransition":J */
/* cmpg-float v16, v21, v10 */
/* if-gtz v16, :cond_7 */
/* move/from16 v16, v15 */
} // .end local v15 # "skipBrighteningThreshold":Z
/* .local v16, "skipBrighteningThreshold":Z */
/* iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* cmpg-float v15, v15, v10 */
/* if-gtz v15, :cond_8 */
/* cmp-long v15, v19, v1 */
/* if-lez v15, :cond_9 */
} // .end local v16 # "skipBrighteningThreshold":Z
/* .restart local v15 # "skipBrighteningThreshold":Z */
} // :cond_7
/* move/from16 v16, v15 */
} // .end local v15 # "skipBrighteningThreshold":Z
/* .restart local v16 # "skipBrighteningThreshold":Z */
} // :cond_8
} // :goto_3
if ( v18 != null) { // if-eqz v18, :cond_f
/* .line 314 */
} // :cond_9
/* iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* move-object/from16 v22, v14 */
/* if-eq v15, v14, :cond_a */
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v14, v15, :cond_c */
} // :cond_a
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpl-float v14, v14, v15 */
/* if-ltz v14, :cond_c */
/* .line 317 */
/* iput v9, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 318 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v5 );
/* .line 323 */
v5 = this.mBrightnessControllerImpl;
v5 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v5 ).isAonFlareEnabled ( ); // invoke-virtual {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 324 */
v5 = this.mSceneDetector;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v7 = 1; // const/4 v7, 0x1
(( com.android.server.display.SceneDetector ) v5 ).updateAmbientLux ( v3, v6, v7 ); // invoke-virtual {v5, v3, v6, v7}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V
/* goto/16 :goto_4 */
/* .line 326 */
} // :cond_b
int v7 = 1; // const/4 v7, 0x1
v5 = this.mListener;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* goto/16 :goto_4 */
/* .line 328 */
} // :cond_c
/* iget v5, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v5, v7, :cond_e */
/* .line 329 */
/* iput v5, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 330 */
v5 = this.mHysteresisLevelsImpl;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v5 = (( com.android.server.display.HysteresisLevelsStub ) v5 ).getBrighteningThreshold ( v7 ); // invoke-virtual {v5, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getBrighteningThreshold(F)F
/* iput v5, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
/* .line 331 */
v5 = this.mHysteresisLevelsImpl;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v5 = (( com.android.server.display.HysteresisLevelsStub ) v5 ).getDarkeningThreshold ( v7 ); // invoke-virtual {v5, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getDarkeningThreshold(F)F
/* iput v5, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
/* .line 332 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "updateDualSensorPolicy: switch assist light sensor for lux update from main light sensor, mMainFastAmbientLux: " */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
/* move-object/from16 v7, v17 */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v5 );
/* .line 337 */
v5 = this.mBrightnessControllerImpl;
v5 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v5 ).isAonFlareEnabled ( ); // invoke-virtual {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z
if ( v5 != null) { // if-eqz v5, :cond_d
/* .line 338 */
v5 = this.mSceneDetector;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
int v7 = 0; // const/4 v7, 0x0
(( com.android.server.display.SceneDetector ) v5 ).updateAmbientLux ( v3, v6, v7 ); // invoke-virtual {v5, v3, v6, v7}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V
/* .line 340 */
} // :cond_d
v5 = this.mListener;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
int v7 = 1; // const/4 v7, 0x1
/* .line 343 */
} // :cond_e
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v12 ); // invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v5 );
/* .line 346 */
v5 = this.mListener;
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v7 = 0; // const/4 v7, 0x0
/* .line 348 */
} // :goto_4
v5 = this.mListener;
/* move-result-wide v5 */
/* .line 349 */
} // .end local v24 # "nextBrightenTransition":J
/* .restart local v5 # "nextBrightenTransition":J */
v7 = this.mListener;
/* move-result-wide v12 */
/* .line 350 */
} // .end local v19 # "nextDarkenTransition":J
/* .restart local v12 # "nextDarkenTransition":J */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = v9 = this.mListener;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
/* move-object/from16 v9, v22 */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mListener;
v9 = /* .line 351 */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 350 */
android.util.Slog .d ( v8,v7 );
/* .line 311 */
} // .end local v5 # "nextBrightenTransition":J
} // .end local v12 # "nextDarkenTransition":J
/* .restart local v19 # "nextDarkenTransition":J */
/* .restart local v24 # "nextBrightenTransition":J */
} // :cond_f
/* move-wide/from16 v12, v19 */
/* move-wide/from16 v5, v24 */
/* .line 353 */
} // .end local v19 # "nextDarkenTransition":J
} // .end local v24 # "nextBrightenTransition":J
/* .restart local v5 # "nextBrightenTransition":J */
/* .restart local v12 # "nextDarkenTransition":J */
} // :goto_5
java.lang.Math .min ( v12,v13,v5,v6 );
/* move-result-wide v14 */
/* .line 354 */
/* .local v14, "nextTransitionTime":J */
/* cmp-long v7, v14, v1 */
/* if-lez v7, :cond_10 */
/* move v9, v4 */
/* move-wide/from16 v19, v5 */
/* move-wide v4, v14 */
} // :cond_10
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I */
/* move v9, v4 */
/* move-wide/from16 v19, v5 */
} // .end local v4 # "mainBrighteningThreshold":F
} // .end local v5 # "nextBrightenTransition":J
/* .local v9, "mainBrighteningThreshold":F */
/* .local v19, "nextBrightenTransition":J */
/* int-to-long v4, v7 */
/* add-long/2addr v4, v1 */
/* .line 355 */
} // .end local v14 # "nextTransitionTime":J
/* .local v4, "nextTransitionTime":J */
} // :goto_6
/* sget-boolean v6, Lcom/android/server/display/DualSensorPolicy;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_11
/* .line 356 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "updateDualSensorPolicy: next transition time of main light sensor: " */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 357 */
android.util.TimeUtils .formatUptime ( v4,v5 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 356 */
android.util.Slog .d ( v8,v6 );
/* .line 359 */
} // :cond_11
v6 = this.mListener;
} // .end local v4 # "nextTransitionTime":J
} // .end local v9 # "mainBrighteningThreshold":F
} // .end local v10 # "mainDarkeningThreshold":F
} // .end local v12 # "nextDarkenTransition":J
} // .end local v16 # "skipBrighteningThreshold":Z
} // .end local v18 # "skipDarkeningThreshold":Z
} // .end local v19 # "nextBrightenTransition":J
/* goto/16 :goto_a */
/* .line 360 */
} // :cond_12
/* move-object v7, v5 */
/* if-ne v3, v4, :cond_20 */
/* .line 361 */
v4 = this.mAssistLightSensorRingBuffer;
/* iget v5, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
(( com.android.server.display.AmbientLightRingBuffer ) v4 ).nextAmbientLightBrighteningTransition ( v1, v2, v5 ); // invoke-virtual {v4, v1, v2, v5}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightBrighteningTransition(JF)J
/* move-result-wide v4 */
/* .line 362 */
/* .local v4, "nextBrightenTransition":J */
v9 = this.mAssistLightSensorRingBuffer;
/* iget v10, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
(( com.android.server.display.AmbientLightRingBuffer ) v9 ).nextAmbientLightDarkeningTransition ( v1, v2, v10 ); // invoke-virtual {v9, v1, v2, v10}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightDarkeningTransition(JF)J
/* move-result-wide v9 */
/* .line 363 */
/* .local v9, "nextDarkenTransition":J */
v11 = this.mAssistLightSensorRingBuffer;
/* iget v12, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I */
/* int-to-long v12, v12 */
v11 = (( com.android.server.display.AmbientLightRingBuffer ) v11 ).calculateAmbientLux ( v1, v2, v12, v13 ); // invoke-virtual {v11, v1, v2, v12, v13}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F
/* iput v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
/* .line 364 */
v11 = this.mAssistLightSensorRingBuffer;
/* iget v12, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonShort:I */
/* int-to-long v12, v12 */
v11 = (( com.android.server.display.AmbientLightRingBuffer ) v11 ).calculateAmbientLux ( v1, v2, v12, v13 ); // invoke-virtual {v11, v1, v2, v12, v13}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F
/* iput v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* .line 365 */
v11 = v11 = this.mListener;
/* .line 366 */
/* .local v11, "mainBrighteningThreshold":F */
/* iget v12, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v12 = /* invoke-direct {v0, v12}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientBrighteningThreshold(F)Z */
/* .line 367 */
/* .local v12, "skipBrighteningThreshold":Z */
/* iget v13, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v13 = /* invoke-direct {v0, v13}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientDarkeningThreshold(F)Z */
/* .line 368 */
/* .local v13, "skipDarkeningThreshold":Z */
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
/* iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
/* cmpl-float v16, v14, v15 */
/* if-ltz v16, :cond_13 */
/* move-object/from16 v16, v6 */
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpl-float v6, v6, v15 */
/* if-ltz v6, :cond_14 */
/* cmp-long v6, v4, v1 */
/* if-lez v6, :cond_16 */
} // :cond_13
/* move-object/from16 v16, v6 */
} // :cond_14
} // :goto_7
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
/* cmpg-float v14, v14, v6 */
/* if-gtz v14, :cond_15 */
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpg-float v6, v14, v6 */
/* if-gtz v6, :cond_15 */
/* cmp-long v6, v9, v1 */
/* if-lez v6, :cond_16 */
} // :cond_15
/* if-nez v12, :cond_16 */
if ( v13 != null) { // if-eqz v13, :cond_1d
/* .line 374 */
} // :cond_16
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v6, v14, :cond_17 */
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
/* cmpl-float v6, v6, v11 */
/* if-lez v6, :cond_17 */
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* cmpl-float v6, v6, v11 */
/* if-gtz v6, :cond_18 */
} // :cond_17
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v6, v14, :cond_1a */
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* cmpl-float v6, v6, v14 */
/* if-lez v6, :cond_1a */
/* .line 379 */
} // :cond_18
/* iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 380 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v14, "updateDualSensorPolicy: update ambient lux using assist light sensor, mAssistFastAmbientLux: " */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v7 = ", mAssistBrighteningThreshold: "; // const-string v7, ", mAssistBrighteningThreshold: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v7 = ", mAssistDarkeningThreshold: "; // const-string v7, ", mAssistDarkeningThreshold: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v6 );
/* .line 385 */
v6 = this.mBrightnessControllerImpl;
v6 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v6 ).isAonFlareEnabled ( ); // invoke-virtual {v6}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z
if ( v6 != null) { // if-eqz v6, :cond_19
/* .line 386 */
v6 = this.mSceneDetector;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
int v14 = 0; // const/4 v14, 0x0
(( com.android.server.display.SceneDetector ) v6 ).updateAmbientLux ( v3, v7, v14 ); // invoke-virtual {v6, v3, v7, v14}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V
/* .line 388 */
} // :cond_19
v6 = this.mListener;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
int v14 = 1; // const/4 v14, 0x1
/* .line 390 */
} // :cond_1a
/* iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v6, v14, :cond_1c */
/* .line 391 */
/* iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 392 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v14, "updateDualSensorPolicy: switch main light sensor for lux update from assist light sensor, mAssistFastAmbientLux: " */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v7 = ", mMainFastAmbientLux: "; // const-string v7, ", mMainFastAmbientLux: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
/* move-object/from16 v7, v16 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v6 );
/* .line 397 */
v6 = this.mBrightnessControllerImpl;
v6 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) v6 ).isAonFlareEnabled ( ); // invoke-virtual {v6}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z
if ( v6 != null) { // if-eqz v6, :cond_1b
/* .line 398 */
v6 = this.mSceneDetector;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v14 = 0; // const/4 v14, 0x0
(( com.android.server.display.SceneDetector ) v6 ).updateAmbientLux ( v3, v7, v14 ); // invoke-virtual {v6, v3, v7, v14}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V
/* .line 400 */
} // :cond_1b
v6 = this.mListener;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
int v14 = 1; // const/4 v14, 0x1
/* .line 403 */
} // :cond_1c
} // :goto_8
v6 = this.mHysteresisLevelsImpl;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v6 = (( com.android.server.display.HysteresisLevelsStub ) v6 ).getBrighteningThreshold ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getBrighteningThreshold(F)F
/* iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
/* .line 404 */
v6 = this.mHysteresisLevelsImpl;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F */
v6 = (( com.android.server.display.HysteresisLevelsStub ) v6 ).getDarkeningThreshold ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getDarkeningThreshold(F)F
/* iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
/* .line 405 */
v6 = this.mAssistLightSensorRingBuffer;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
(( com.android.server.display.AmbientLightRingBuffer ) v6 ).nextAmbientLightBrighteningTransition ( v1, v2, v7 ); // invoke-virtual {v6, v1, v2, v7}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightBrighteningTransition(JF)J
/* move-result-wide v4 */
/* .line 406 */
v6 = this.mAssistLightSensorRingBuffer;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
(( com.android.server.display.AmbientLightRingBuffer ) v6 ).nextAmbientLightDarkeningTransition ( v1, v2, v7 ); // invoke-virtual {v6, v1, v2, v7}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightDarkeningTransition(JF)J
/* move-result-wide v9 */
/* .line 407 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "updateDualSensorPolicy: next brightening threshold of assist light sensor: " */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v7 = ", next darkening threshold of assist light sensor: "; // const-string v7, ", next darkening threshold of assist light sensor: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v6 );
/* .line 410 */
} // :cond_1d
java.lang.Math .min ( v9,v10,v4,v5 );
/* move-result-wide v6 */
/* .line 411 */
/* .local v6, "nextTransitionTime":J */
/* cmp-long v14, v6, v1 */
/* if-lez v14, :cond_1e */
/* move-wide v14, v6 */
} // :cond_1e
/* iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I */
/* int-to-long v14, v14 */
/* add-long/2addr v14, v1 */
} // :goto_9
/* move-wide v6, v14 */
/* .line 412 */
/* sget-boolean v14, Lcom/android/server/display/DualSensorPolicy;->DEBUG:Z */
if ( v14 != null) { // if-eqz v14, :cond_1f
/* .line 413 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v15, "updateDualSensorPolicy: next transition time of assist light sensor: " */
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v6, v7 ); // invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 414 */
android.util.TimeUtils .formatUptime ( v6,v7 );
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 413 */
android.util.Slog .d ( v8,v14 );
/* .line 416 */
} // :cond_1f
v8 = this.mPolicyHandler;
int v14 = 0; // const/4 v14, 0x0
(( android.os.Handler ) v8 ).sendEmptyMessageAtTime ( v14, v6, v7 ); // invoke-virtual {v8, v14, v6, v7}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z
/* .line 360 */
} // .end local v4 # "nextBrightenTransition":J
} // .end local v6 # "nextTransitionTime":J
} // .end local v9 # "nextDarkenTransition":J
} // .end local v11 # "mainBrighteningThreshold":F
} // .end local v12 # "skipBrighteningThreshold":Z
} // .end local v13 # "skipDarkeningThreshold":Z
} // :cond_20
} // :goto_a
/* nop */
/* .line 418 */
} // :goto_b
int v4 = 1; // const/4 v4, 0x1
} // .end method
protected Boolean updateMainLightSensorAmbientThreshold ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .line 227 */
/* if-eq p1, v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* if-ne v0, v1, :cond_0 */
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
protected void updateMainLuxStatus ( Float p0 ) {
/* .locals 1 */
/* .param p1, "currentLux" # F */
/* .line 491 */
/* iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I */
/* .line 492 */
/* iput p1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F */
/* .line 493 */
/* iput p1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F */
/* .line 494 */
return;
} // .end method
