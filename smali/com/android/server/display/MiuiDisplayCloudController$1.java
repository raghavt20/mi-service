class com.android.server.display.MiuiDisplayCloudController$1 extends android.database.ContentObserver {
	 /* .source "MiuiDisplayCloudController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/MiuiDisplayCloudController;->registerMiuiBrightnessCloudDataObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.MiuiDisplayCloudController this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$_RoDYYPyAT_3JHhMAHObssA36DY ( com.android.server.display.MiuiDisplayCloudController$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController$1;->lambda$onChange$0()V */
return;
} // .end method
 com.android.server.display.MiuiDisplayCloudController$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/MiuiDisplayCloudController; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 341 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
private void lambda$onChange$0 ( ) { //synthethic
/* .locals 1 */
/* .line 347 */
v0 = this.this$0;
com.android.server.display.MiuiDisplayCloudController .-$$Nest$msyncLocalBackupFromCloud ( v0 );
/* .line 348 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 344 */
v0 = this.this$0;
v0 = com.android.server.display.MiuiDisplayCloudController .-$$Nest$mupdateDataFromCloudControl ( v0 );
/* .line 345 */
/* .local v0, "changed":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 346 */
	 com.android.internal.os.BackgroundThread .getHandler ( );
	 /* new-instance v2, Lcom/android/server/display/MiuiDisplayCloudController$1$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/display/MiuiDisplayCloudController$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/MiuiDisplayCloudController$1;)V */
	 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 349 */
	 v1 = this.this$0;
	 (( com.android.server.display.MiuiDisplayCloudController ) v1 ).notifyAllObservers ( ); // invoke-virtual {v1}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyAllObservers()V
	 /* .line 351 */
} // :cond_0
return;
} // .end method
