class com.android.server.display.BrightnessCurve$ThirdOutdoorLightCurve extends com.android.server.display.BrightnessCurve$Curve {
	 /* .source "BrightnessCurve.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/BrightnessCurve; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ThirdOutdoorLightCurve" */
} // .end annotation
/* # instance fields */
private final Float mHeadLux;
private final Float mPullUpAnchorPointLux;
private final Float mPullUpAnchorPointNit;
private final Float mTailLux;
final com.android.server.display.BrightnessCurve this$0; //synthetic
/* # direct methods */
public com.android.server.display.BrightnessCurve$ThirdOutdoorLightCurve ( ) {
/* .locals 5 */
/* .param p1, "this$0" # Lcom/android/server/display/BrightnessCurve; */
/* .line 586 */
this.this$0 = p1;
/* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve$Curve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 587 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v1 = 4; // const/4 v1, 0x4
/* aget v0, v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mHeadLux:F */
/* .line 588 */
v1 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmMaxLux ( p1 );
/* cmpl-float v1, v1, v0 */
/* if-lez v1, :cond_0 */
v1 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmMaxLux ( p1 );
} // :cond_0
/* const/high16 v1, 0x3f800000 # 1.0f */
/* add-float/2addr v1, v0 */
} // :goto_0
/* iput v1, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mTailLux:F */
/* .line 589 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v3 = 1; // const/4 v3, 0x1
/* aget v2, v2, v3 */
/* iput v2, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mPullUpAnchorPointLux:F */
/* .line 590 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( p1 );
v3 = (( android.util.Spline ) v3 ).interpolate ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/Spline;->interpolate(F)F
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( p1 );
v4 = (( android.util.Spline ) v4 ).interpolate ( v0 ); // invoke-virtual {v4, v0}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v3, v4 */
/* sub-float v0, v1, v0 */
/* div-float/2addr v3, v0 */
/* .line 592 */
/* .local v3, "tan":F */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( p1 );
v0 = (( android.util.Spline ) v0 ).interpolate ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v1, v2 */
/* mul-float/2addr v1, v3 */
/* sub-float/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mPullUpAnchorPointNit:F */
/* .line 594 */
return;
} // .end method
/* # virtual methods */
public void connectLeft ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 8 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 610 */
v0 = v0 = this.mPointList;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 611 */
v0 = this.mPointList;
v1 = v1 = this.mPointList;
/* add-int/lit8 v1, v1, -0x1 */
/* check-cast v0, Landroid/util/Pair; */
/* .line 612 */
/* .local v0, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v2 );
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mHeadLux:F */
v2 = (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 613 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mHeadLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 614 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 615 */
v1 = this.first;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
(( com.android.server.display.BrightnessCurve$ThirdOutdoorLightCurve ) p0 ).create ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->create(FF)V
/* .line 617 */
} // :cond_1
v1 = this.this$0;
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mTailLux:F */
v3 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmThirdOutdoorCurveBrightenMinTan ( v1 );
v4 = this.mPointList;
com.android.server.display.BrightnessCurve .-$$Nest$mpullUpConnectTail ( v1,v0,v2,v3,v4 );
/* .line 620 */
} // .end local v0 # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_2
} // :goto_0
return;
} // .end method
public void connectRight ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 0 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 625 */
return;
} // .end method
public void create ( Float p0, Float p1 ) {
/* .locals 9 */
/* .param p1, "lux" # F */
/* .param p2, "changeNit" # F */
/* .line 598 */
v0 = this.this$0;
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmMaxLux ( v0 );
/* cmpl-float v0, p1, v0 */
/* if-lez v0, :cond_0 */
v0 = this.this$0;
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmMaxLux ( v0 );
} // :cond_0
/* move v0, p1 */
} // :goto_0
/* move p1, v0 */
/* .line 599 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v0, v0, p2 */
/* if-lez v0, :cond_1 */
/* .line 600 */
v1 = this.this$0;
v4 = this.mPointList;
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mHeadLux:F */
/* iget v6, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mTailLux:F */
v0 = this.this$0;
v7 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmThirdOutdoorCurveBrightenMinTan ( v0 );
v0 = this.this$0;
v8 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmThirdOutdoorCurveBrightenMinTan ( v0 );
/* move v2, p1 */
/* move v3, p2 */
/* invoke-static/range {v1 ..v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullDownCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V */
/* .line 603 */
} // :cond_1
v1 = this.this$0;
v4 = this.mPointList;
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mHeadLux:F */
/* iget v6, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mTailLux:F */
/* iget v7, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mPullUpAnchorPointLux:F */
/* iget v8, p0, Lcom/android/server/display/BrightnessCurve$ThirdOutdoorLightCurve;->mPullUpAnchorPointNit:F */
/* move v2, p1 */
/* move v3, p2 */
/* invoke-static/range {v1 ..v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullUpCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V */
/* .line 606 */
} // :goto_1
return;
} // .end method
