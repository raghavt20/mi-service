class com.android.server.display.AutomaticBrightnessControllerImpl$2 implements android.hardware.SensorEventListener {
	 /* .source "AutomaticBrightnessControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.AutomaticBrightnessControllerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.display.AutomaticBrightnessControllerImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
/* .line 382 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 400 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 385 */
v0 = this.sensor;
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
/* sparse-switch v0, :sswitch_data_0 */
/* .line 390 */
/* :sswitch_0 */
v0 = this.this$0;
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$monNonUiSensorChanged ( v0,p1 );
/* .line 391 */
/* .line 387 */
/* :sswitch_1 */
v0 = this.this$0;
com.android.server.display.AutomaticBrightnessControllerImpl .-$$Nest$monProximitySensorChanged ( v0,p1 );
/* .line 388 */
/* nop */
/* .line 395 */
} // :goto_0
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x8 -> :sswitch_1 */
/* 0x1fa2653 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
