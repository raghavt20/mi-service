public class com.android.server.display.MiuiBrightnessUtilsImpl extends com.android.server.display.MiuiBrightnessUtilsStub {
	 /* .source "MiuiBrightnessUtilsImpl.java" */
	 /* # static fields */
	 private static final Float A;
	 private static final Float B;
	 private static final Float C;
	 private static final Float R;
	 private static final android.content.res.Resources resources;
	 /* # direct methods */
	 static com.android.server.display.MiuiBrightnessUtilsImpl ( ) {
		 /* .locals 2 */
		 /* .line 27 */
		 android.app.ActivityThread .currentApplication ( );
		 (( android.app.Application ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;
		 /* .line 28 */
		 /* const v1, 0x1107001d */
		 v1 = 		 (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
		 /* .line 30 */
		 /* const v1, 0x1107001a */
		 v1 = 		 (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
		 /* .line 32 */
		 /* const v1, 0x1107001b */
		 v1 = 		 (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
		 /* .line 34 */
		 /* const v1, 0x1107001c */
		 v0 = 		 (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
		 return;
	 } // .end method
	 public com.android.server.display.MiuiBrightnessUtilsImpl ( ) {
		 /* .locals 0 */
		 /* .line 25 */
		 /* invoke-direct {p0}, Lcom/android/server/display/MiuiBrightnessUtilsStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float convertGammaToLinear ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "val" # F */
		 /* .line 46 */
		 /* cmpg-float v1, p1, v0 */
		 /* if-gtz v1, :cond_0 */
		 /* .line 47 */
		 /* div-float v0, p1, v0 */
		 v0 = 		 android.util.MathUtils .sq ( v0 );
		 /* .local v0, "ret":F */
		 /* .line 49 */
	 } // .end local v0 # "ret":F
} // :cond_0
/* sub-float v0, p1, v0 */
/* div-float/2addr v0, v1 */
v0 = android.util.MathUtils .exp ( v0 );
/* add-float/2addr v0, v1 */
/* .line 54 */
/* .restart local v0 # "ret":F */
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* const/high16 v2, 0x41400000 # 12.0f */
v1 = android.util.MathUtils .constrain ( v0,v1,v2 );
/* .line 58 */
/* .local v1, "normalizedRet":F */
/* div-float v2, v1, v2 */
} // .end method
public Float convertLinearToGamma ( Float p0 ) {
/* .locals 3 */
/* .param p1, "val" # F */
/* .line 70 */
/* const/high16 v0, 0x41400000 # 12.0f */
/* mul-float/2addr v0, p1 */
/* .line 72 */
/* .local v0, "normalizedVal":F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpg-float v1, v0, v1 */
/* if-gtz v1, :cond_0 */
/* .line 73 */
v1 = android.util.MathUtils .sqrt ( v0 );
/* mul-float/2addr v1, v2 */
/* .local v1, "ret":F */
/* .line 75 */
} // .end local v1 # "ret":F
} // :cond_0
/* sub-float v2, v0, v2 */
v2 = android.util.MathUtils .log ( v2 );
/* mul-float/2addr v1, v2 */
/* add-float/2addr v1, v2 */
/* .line 77 */
/* .restart local v1 # "ret":F */
} // :goto_0
} // .end method
