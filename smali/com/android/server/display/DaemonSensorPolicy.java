public class com.android.server.display.DaemonSensorPolicy {
	 /* .source "DaemonSensorPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ASSIST_SENSOR_TYPE;
private static final Integer MSG_UPDATE_DEVICE_IDLE;
private static final java.lang.String TAG;
private static final Boolean USE_DAEMON_SENSOR_POLICY;
/* # instance fields */
private com.android.server.display.AutomaticBrightnessControllerImpl mAutomaticBrightnessControllerImpl;
private final android.content.Context mContext;
private Boolean mDaemonLightSensorsEnabled;
private final android.hardware.SensorEventListener mDaemonSensorListener;
private java.util.Map mDaemonSensorValues;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mDaemonSensors;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/hardware/Sensor;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.BroadcastReceiver mDeviceIdleReceiver;
private Integer mDisplayPolicy;
private com.android.server.display.DaemonSensorPolicy$DaemonSensorHandle mHandler;
private Boolean mIsDeviceIdleMode;
private Boolean mIsDeviceIdleReceiverRegistered;
private final android.hardware.Sensor mMainLightSensor;
private final android.os.PowerManager mPowerManager;
private final android.hardware.SensorManager mSensorManager;
/* # direct methods */
public static void $r8$lambda$p0jOgz95rgjsmAJHsCwaO_jP79o ( com.android.server.display.DaemonSensorPolicy p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->lambda$new$0()V */
return;
} // .end method
static java.util.Map -$$Nest$fgetmDaemonSensorValues ( com.android.server.display.DaemonSensorPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDaemonSensorValues;
} // .end method
static com.android.server.display.DaemonSensorPolicy$DaemonSensorHandle -$$Nest$fgetmHandler ( com.android.server.display.DaemonSensorPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsDeviceIdleMode ( com.android.server.display.DaemonSensorPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleMode:Z */
} // .end method
static android.os.PowerManager -$$Nest$fgetmPowerManager ( com.android.server.display.DaemonSensorPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPowerManager;
} // .end method
static void -$$Nest$fputmIsDeviceIdleMode ( com.android.server.display.DaemonSensorPolicy p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleMode:Z */
return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.display.DaemonSensorPolicy.TAG;
} // .end method
static com.android.server.display.DaemonSensorPolicy ( ) {
/* .locals 2 */
/* .line 27 */
/* const-class v0, Lcom/android/server/display/DaemonSensorPolicy; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 35 */
/* nop */
/* .line 36 */
/* const-string/jumbo v0, "use_daemon_sensor_policy" */
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.display.DaemonSensorPolicy.USE_DAEMON_SENSOR_POLICY = (v0!= 0);
/* .line 35 */
return;
} // .end method
public com.android.server.display.DaemonSensorPolicy ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "sensorManager" # Landroid/hardware/SensorManager; */
/* .param p3, "looper" # Landroid/os/Looper; */
/* .param p4, "impl" # Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
/* .param p5, "lightSensor" # Landroid/hardware/Sensor; */
/* .line 51 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 144 */
/* new-instance v0, Lcom/android/server/display/DaemonSensorPolicy$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DaemonSensorPolicy$1;-><init>(Lcom/android/server/display/DaemonSensorPolicy;)V */
this.mDaemonSensorListener = v0;
/* .line 156 */
/* new-instance v0, Lcom/android/server/display/DaemonSensorPolicy$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DaemonSensorPolicy$2;-><init>(Lcom/android/server/display/DaemonSensorPolicy;)V */
this.mDeviceIdleReceiver = v0;
/* .line 52 */
this.mContext = p1;
/* .line 53 */
/* new-instance v0, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle; */
/* invoke-direct {v0, p0, p3}, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;-><init>(Lcom/android/server/display/DaemonSensorPolicy;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 54 */
this.mSensorManager = p2;
/* .line 55 */
final String v0 = "power"; // const-string v0, "power"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 56 */
this.mAutomaticBrightnessControllerImpl = p4;
/* .line 57 */
this.mMainLightSensor = p5;
/* .line 58 */
/* invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->addDaemonSensor()V */
/* .line 62 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DaemonSensorPolicy$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/DaemonSensorPolicy$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/DaemonSensorPolicy;)V */
(( com.android.server.display.DaemonSensorPolicy$DaemonSensorHandle ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;->post(Ljava/lang/Runnable;)Z
/* .line 65 */
return;
} // .end method
private void addDaemonSensor ( ) {
/* .locals 2 */
/* .line 84 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mDaemonSensorValues = v0;
/* .line 85 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDaemonSensors = v0;
/* .line 87 */
v1 = this.mMainLightSensor;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 88 */
/* .line 91 */
} // :cond_0
v0 = this.mSensorManager;
/* const v1, 0x1fa266f */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
/* .line 92 */
/* .local v0, "sensor":Landroid/hardware/Sensor; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 93 */
v1 = this.mDaemonSensors;
/* .line 95 */
} // :cond_1
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 0 */
/* .line 63 */
/* invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->registerReceiver()V */
/* .line 64 */
return;
} // .end method
private void registerReceiver ( ) {
/* .locals 3 */
/* .line 68 */
/* sget-boolean v0, Lcom/android/server/display/DaemonSensorPolicy;->USE_DAEMON_SENSOR_POLICY:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z */
/* if-nez v0, :cond_0 */
/* .line 69 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 70 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.os.action.DEVICE_IDLE_MODE_CHANGED"; // const-string v1, "android.os.action.DEVICE_IDLE_MODE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 71 */
v1 = this.mContext;
v2 = this.mDeviceIdleReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 72 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z */
/* .line 74 */
} // .end local v0 # "intentFilter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
private void unregisterReceiver ( ) {
/* .locals 2 */
/* .line 77 */
/* iget-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 78 */
v0 = this.mContext;
v1 = this.mDeviceIdleReceiver;
(( android.content.Context ) v0 ).unregisterReceiver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z */
/* .line 81 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 165 */
/* sget-boolean v0, Lcom/android/server/display/DaemonSensorPolicy;->USE_DAEMON_SENSOR_POLICY:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 166 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 167 */
final String v0 = "Daemon sensor policy state:"; // const-string v0, "Daemon sensor policy state:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 168 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDaemonLightSensorsEnabled="; // const-string v1, " mDaemonLightSensorsEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 169 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsDeviceIdleMode="; // const-string v1, " mIsDeviceIdleMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 170 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDaemonSensors: size="; // const-string v1, " mDaemonSensors: size="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = v1 = this.mDaemonSensors;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 171 */
v0 = this.mDaemonSensors;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Landroid/hardware/Sensor; */
/* .line 172 */
/* .local v1, "sensor":Landroid/hardware/Sensor; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " sensor name="; // const-string v3, " sensor name="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.hardware.Sensor ) v1 ).getName ( ); // invoke-virtual {v1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", type="; // const-string v3, ", type="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( android.hardware.Sensor ) v1 ).getType ( ); // invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 173 */
} // .end local v1 # "sensor":Landroid/hardware/Sensor;
/* .line 174 */
} // :cond_0
v0 = this.mDaemonSensorValues;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 175 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Float;>;" */
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 176 */
/* .local v2, "type":I */
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* .line 177 */
/* .local v3, "lux":F */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " sensor [type: "; // const-string v5, " sensor [type: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", lux="; // const-string v5, ", lux="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "];"; // const-string v5, "];"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 178 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Float;>;"
} // .end local v2 # "type":I
} // .end local v3 # "lux":F
/* .line 180 */
} // :cond_1
return;
} // .end method
protected Float getDaemonSensorValue ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 124 */
v0 = this.mDaemonSensorValues;
v0 = java.lang.Integer .valueOf ( p1 );
/* if-nez v0, :cond_0 */
/* .line 125 */
v0 = com.android.server.display.DaemonSensorPolicy.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "the sensor of the type " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " is not in daemon sensor list!"; // const-string v2, " is not in daemon sensor list!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 126 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 128 */
} // :cond_0
v0 = this.mDaemonSensorValues;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // .end method
protected Float getMainLightSensorLux ( ) {
/* .locals 1 */
/* .line 117 */
v0 = this.mMainLightSensor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 118 */
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
v0 = (( com.android.server.display.DaemonSensorPolicy ) p0 ).getDaemonSensorValue ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/DaemonSensorPolicy;->getDaemonSensorValue(I)F
/* .line 120 */
} // :cond_0
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
public void notifyRegisterDaemonLightSensor ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .param p2, "displayPolicy" # I */
/* .line 132 */
int v0 = 3; // const/4 v0, 0x3
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v0, :cond_0 */
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 133 */
/* .local v0, "isActive":Z */
} // :goto_0
/* iput p2, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDisplayPolicy:I */
/* .line 134 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 135 */
(( com.android.server.display.DaemonSensorPolicy ) p0 ).setDaemonLightSensorsEnabled ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/display/DaemonSensorPolicy;->setDaemonLightSensorsEnabled(Z)V
/* .line 137 */
} // :cond_1
return;
} // .end method
protected void setDaemonLightSensorsEnabled ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "enable" # Z */
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 99 */
/* iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z */
/* if-nez v1, :cond_2 */
/* .line 100 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z */
/* .line 101 */
v1 = this.mDaemonSensors;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Landroid/hardware/Sensor; */
/* .line 102 */
/* .local v2, "sensor":Landroid/hardware/Sensor; */
v3 = this.mSensorManager;
v4 = this.mDaemonSensorListener;
v5 = this.mHandler;
(( android.hardware.SensorManager ) v3 ).registerListener ( v4, v2, v0, v5 ); // invoke-virtual {v3, v4, v2, v0, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 104 */
} // .end local v2 # "sensor":Landroid/hardware/Sensor;
/* .line 105 */
} // :cond_0
v0 = com.android.server.display.DaemonSensorPolicy.TAG;
final String v1 = "register daemon light sensor."; // const-string v1, "register daemon light sensor."
android.util.Slog .i ( v0,v1 );
/* .line 107 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 108 */
/* iput-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z */
/* .line 109 */
v0 = this.mSensorManager;
v1 = this.mDaemonSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 110 */
v0 = this.mDaemonSensorValues;
/* .line 111 */
v0 = com.android.server.display.DaemonSensorPolicy.TAG;
/* const-string/jumbo v1, "unregister daemon light sensor." */
android.util.Slog .i ( v0,v1 );
/* .line 113 */
} // :cond_2
} // :goto_1
return;
} // .end method
protected void stop ( ) {
/* .locals 1 */
/* .line 140 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.display.DaemonSensorPolicy ) p0 ).setDaemonLightSensorsEnabled ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/DaemonSensorPolicy;->setDaemonLightSensorsEnabled(Z)V
/* .line 141 */
/* invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->unregisterReceiver()V */
/* .line 142 */
return;
} // .end method
