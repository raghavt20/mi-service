public class com.android.server.display.DisplayManagerServiceImpl extends com.android.server.display.DisplayManagerServiceStub {
	 /* .source "DisplayManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.display.DisplayManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/display/DisplayManagerServiceImpl$DisplayManagerStubHandler;, */
/* Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback; */
/* } */
} // .end annotation
/* # static fields */
private static final Integer FLAG_INCREASE_SCREEN_BRIGHTNESS;
private static final Integer FLAG_UPDATE_DOLBY_PREVIEW_STATE;
private static final Boolean IS_FOLDABLE_OR_FLIP_DEVICE;
private static final Integer MSG_RESET_SHORT_MODEL;
private static final java.lang.String TAG;
/* # instance fields */
private Boolean mBootCompleted;
private java.util.HashMap mClientDeathCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Boolean mDebug;
private com.android.server.display.DisplayDevice mDefaultDisplayDevice;
private android.os.IBinder mDefaultDisplayToken;
private com.android.server.display.LocalDisplayAdapter$LocalDisplayDevice mDefaultLocalDisplayDevice;
private com.android.server.display.LogicalDisplay mDefaultLogicalDisplay;
private java.util.List mDisplayDevices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/DisplayDevice;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.display.DisplayFeatureManagerServiceImpl mDisplayFeatureManagerServiceImpl;
private com.android.server.display.DisplayPowerControllerImpl mDisplayPowerControllerImpl;
private android.os.Handler mHandler;
private Boolean mIsResetRate;
private java.lang.Object mLock;
private com.android.server.display.LogicalDisplayMapper mLogicalDisplayMapper;
private com.android.server.display.MiuiFoldPolicy mMiuiFoldPolicy;
private mOpenedReverseDeviceStates;
private mOpenedReversePresentationDeviceStates;
private java.util.List mResolutionSwitchProcessBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mResolutionSwitchProcessProtectList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private mScreenEffectDisplayIndex;
private miui.security.SecurityManagerInternal mSecurityManager;
private com.android.server.tof.TofManagerInternal mTofManagerInternal;
/* # direct methods */
public static void $r8$lambda$_jhiBHpx9t2fFOrtceWal0RGuCg ( com.android.server.display.DisplayManagerServiceImpl p0, java.util.List p1, java.util.List p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->lambda$updateResolutionSwitchList$0(Ljava/util/List;Ljava/util/List;)V */
return;
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.display.DisplayManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static void -$$Nest$mdoDieLocked ( com.android.server.display.DisplayManagerServiceImpl p0, Integer p1, android.os.IBinder p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->doDieLocked(ILandroid/os/IBinder;)V */
return;
} // .end method
static com.android.server.display.DisplayManagerServiceImpl ( ) {
/* .locals 1 */
/* .line 59 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFoldDeviceInside ( );
/* if-nez v0, :cond_1 */
/* .line 60 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
com.android.server.display.DisplayManagerServiceImpl.IS_FOLDABLE_OR_FLIP_DEVICE = (v0!= 0);
/* .line 58 */
return;
} // .end method
public com.android.server.display.DisplayManagerServiceImpl ( ) {
/* .locals 1 */
/* .line 53 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceStub;-><init>()V */
/* .line 64 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mClientDeathCallbacks = v0;
/* .line 70 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mResolutionSwitchProcessProtectList = v0;
/* .line 71 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mResolutionSwitchProcessBlackList = v0;
/* .line 86 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDisplayDevices = v0;
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
/* filled-new-array {v0}, [I */
this.mScreenEffectDisplayIndex = v0;
return;
} // .end method
private Boolean appRequestChangeSceneRefreshRate ( android.os.Parcel p0 ) {
/* .locals 7 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .line 300 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 301 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 302 */
/* .local v0, "callingUid":I */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
/* .line 303 */
/* .local v1, "targetPkgName":Ljava/lang/String; */
v2 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 304 */
/* .local v2, "maxRefreshRate":I */
/* const/16 v3, 0x2710 */
/* if-ge v0, v3, :cond_1 */
/* .line 305 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* .line 307 */
/* .local v3, "token":J */
int v5 = -1; // const/4 v5, -0x1
/* if-ne v2, v5, :cond_0 */
/* .line 308 */
try { // :try_start_0
com.android.server.wm.RefreshRatePolicyStub .getInstance ( );
/* .line 310 */
} // :cond_0
com.android.server.wm.RefreshRatePolicyStub .getInstance ( );
/* int-to-float v6, v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 314 */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 315 */
/* nop */
/* .line 316 */
int v5 = 1; // const/4 v5, 0x1
/* .line 314 */
/* :catchall_0 */
/* move-exception v5 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 315 */
/* throw v5 */
/* .line 318 */
} // .end local v3 # "token":J
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
} // .end method
private void doDieLocked ( Integer p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "flag" # I */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 686 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 687 */
(( com.android.server.display.DisplayManagerServiceImpl ) p0 ).unregisterDeathCallbackLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
/* .line 688 */
v1 = this.mDisplayPowerControllerImpl;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 689 */
(( com.android.server.display.DisplayPowerControllerImpl ) v1 ).updateGalleryHdrState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGalleryHdrState(Z)V
/* .line 691 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_1 */
/* .line 692 */
(( com.android.server.display.DisplayManagerServiceImpl ) p0 ).unregisterDeathCallbackLocked ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
/* .line 693 */
v1 = this.mDisplayPowerControllerImpl;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 694 */
/* const/high16 v2, 0x7fc00000 # Float.NaN */
(( com.android.server.display.DisplayPowerControllerImpl ) v1 ).updateDolbyPreviewStateLocked ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyPreviewStateLocked(ZF)V
/* .line 697 */
} // :cond_1
} // :goto_0
return;
} // .end method
public static com.android.server.display.DisplayManagerServiceImpl getInstance ( ) {
/* .locals 1 */
/* .line 112 */
/* const-class v0, Lcom/android/server/display/DisplayManagerServiceStub; */
com.miui.base.MiuiStubUtil .getImpl ( v0 );
/* check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl; */
} // .end method
private Integer getScreenEffectDisplayIndexInternal ( Long p0 ) {
/* .locals 4 */
/* .param p1, "physicalDisplayId" # J */
/* .line 184 */
com.android.server.display.DisplayControl .getPhysicalDisplayToken ( p1,p2 );
/* .line 185 */
/* .local v0, "displayToken":Landroid/os/IBinder; */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 186 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = v3 = this.mDisplayDevices;
/* if-ge v2, v3, :cond_1 */
/* .line 187 */
v3 = this.mDisplayDevices;
/* check-cast v3, Lcom/android/server/display/DisplayDevice; */
(( com.android.server.display.DisplayDevice ) v3 ).getDisplayTokenLocked ( ); // invoke-virtual {v3}, Lcom/android/server/display/DisplayDevice;->getDisplayTokenLocked()Landroid/os/IBinder;
/* if-ne v0, v3, :cond_0 */
/* .line 188 */
/* monitor-exit v1 */
/* .line 186 */
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 191 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* .line 192 */
int v1 = 0; // const/4 v1, 0x0
/* .line 191 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private Boolean handleGalleryHdrRequest ( android.os.Parcel p0 ) {
/* .locals 3 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .line 541 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 542 */
(( android.os.Parcel ) p1 ).readStrongBinder ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 543 */
/* .local v0, "token":Landroid/os/IBinder; */
v1 = (( android.os.Parcel ) p1 ).readBoolean ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z
/* .line 544 */
/* .local v1, "increaseScreenBrightness":Z */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->requestGalleryHdrBoost(Landroid/os/IBinder;Z)V */
/* .line 545 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
private void lambda$updateResolutionSwitchList$0 ( java.util.List p0, java.util.List p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "resolutionSwitchBlackList" # Ljava/util/List; */
/* .param p2, "resolutionSwitchProtectList" # Ljava/util/List; */
/* .line 406 */
v0 = this.mResolutionSwitchProcessBlackList;
/* .line 407 */
v0 = this.mResolutionSwitchProcessProtectList;
/* .line 408 */
v0 = this.mResolutionSwitchProcessBlackList;
/* .line 409 */
v0 = this.mResolutionSwitchProcessProtectList;
/* .line 410 */
return;
} // .end method
private void requestGalleryHdrBoost ( android.os.IBinder p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "enable" # Z */
/* .line 549 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 550 */
/* .local v0, "ident":J */
v2 = android.os.Binder .getCallingUid ( );
/* .line 551 */
/* .local v2, "callingUid":I */
v3 = android.os.Binder .getCallingPid ( );
/* .line 553 */
/* .local v3, "callingPid":I */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 554 */
try { // :try_start_0
v4 = this.mLock;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 555 */
int v5 = 0; // const/4 v5, 0x0
try { // :try_start_1
/* invoke-direct {p0, p1, v5, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
/* .line 556 */
v5 = this.mDisplayPowerControllerImpl;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 557 */
(( com.android.server.display.DisplayPowerControllerImpl ) v5 ).updateGalleryHdrState ( p2 ); // invoke-virtual {v5, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGalleryHdrState(Z)V
/* .line 559 */
} // :cond_0
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 560 */
try { // :try_start_2
final String v4 = "DisplayManagerServiceImpl"; // const-string v4, "DisplayManagerServiceImpl"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "requestGalleryHdrBoost: callingUid: "; // const-string v6, "requestGalleryHdrBoost: callingUid: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", callingPid: "; // const-string v6, ", callingPid: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", enable: "; // const-string v6, ", enable: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 559 */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_3
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "ident":J
} // .end local v2 # "callingUid":I
} // .end local v3 # "callingPid":I
} // .end local p0 # "this":Lcom/android/server/display/DisplayManagerServiceImpl;
} // .end local p1 # "token":Landroid/os/IBinder;
} // .end local p2 # "enable":Z
try { // :try_start_4
/* throw v5 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 565 */
/* .restart local v0 # "ident":J */
/* .restart local v2 # "callingUid":I */
/* .restart local v3 # "callingPid":I */
/* .restart local p0 # "this":Lcom/android/server/display/DisplayManagerServiceImpl; */
/* .restart local p1 # "token":Landroid/os/IBinder; */
/* .restart local p2 # "enable":Z */
/* :catchall_1 */
/* move-exception v4 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 566 */
/* throw v4 */
/* .line 565 */
} // :cond_1
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 566 */
/* nop */
/* .line 567 */
return;
} // .end method
private void resetAutoBrightnessShortModelInternal ( android.os.Handler p0 ) {
/* .locals 3 */
/* .param p1, "displayControllerHandler" # Landroid/os/Handler; */
/* .line 357 */
v0 = android.os.Binder .getCallingUid ( );
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x3e8 */
/* if-ne v0, v1, :cond_1 */
/* .line 360 */
final String v0 = "DisplayManagerServiceImpl"; // const-string v0, "DisplayManagerServiceImpl"
final String v1 = "reset AutoBrightness ShortModel"; // const-string v1, "reset AutoBrightness ShortModel"
android.util.Slog .d ( v0,v1 );
/* .line 361 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 363 */
/* .local v0, "token":J */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 364 */
/* const/16 v2, 0xff */
try { // :try_start_0
(( android.os.Handler ) p1 ).obtainMessage ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v2 ).sendToTarget ( ); // invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 367 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 368 */
/* throw v2 */
/* .line 367 */
} // :cond_0
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 368 */
/* nop */
/* .line 369 */
return;
/* .line 358 */
} // .end local v0 # "token":J
} // :cond_1
/* new-instance v0, Ljava/lang/SecurityException; */
final String v1 = "Only system uid can reset Short Model!"; // const-string v1, "Only system uid can reset Short Model!"
/* invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private Boolean setBrightnessRate ( android.os.Parcel p0 ) {
/* .locals 6 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .line 372 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 373 */
/* .local v0, "uid":I */
final String v1 = "android.view.android.hardware.display.IDisplayManager"; // const-string v1, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 374 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 376 */
/* .local v1, "token":J */
try { // :try_start_0
v3 = (( android.os.Parcel ) p1 ).readBoolean ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mIsResetRate:Z */
/* .line 377 */
final String v3 = "DisplayManagerServiceImpl"; // const-string v3, "DisplayManagerServiceImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setBrightnessRate, uid: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", mIsResetRate: "; // const-string v5, ", mIsResetRate: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mIsResetRate:Z */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 379 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 380 */
/* nop */
/* .line 381 */
int v3 = 1; // const/4 v3, 0x1
/* .line 379 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 380 */
/* throw v3 */
} // .end method
private Boolean setCustomCurveEnabledOnCommand ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 825 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 827 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mDisplayPowerControllerImpl;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 828 */
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).setCustomCurveEnabledOnCommand ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setCustomCurveEnabledOnCommand(Z)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 831 */
} // :cond_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 832 */
/* nop */
/* .line 833 */
int v2 = 1; // const/4 v2, 0x1
/* .line 831 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 832 */
/* throw v2 */
} // .end method
private void setDeathCallbackLocked ( android.os.IBinder p0, Integer p1, Boolean p2 ) {
/* .locals 0 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .param p3, "enable" # Z */
/* .line 570 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 571 */
(( com.android.server.display.DisplayManagerServiceImpl ) p0 ).registerDeathCallbackLocked ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->registerDeathCallbackLocked(Landroid/os/IBinder;I)V
/* .line 573 */
} // :cond_0
(( com.android.server.display.DisplayManagerServiceImpl ) p0 ).unregisterDeathCallbackLocked ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
/* .line 575 */
} // :goto_0
return;
} // .end method
private Boolean setForceTrainEnabledOnCommand ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 849 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 851 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mDisplayPowerControllerImpl;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 852 */
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).setForceTrainEnabledOnCommand ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setForceTrainEnabledOnCommand(Z)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 855 */
} // :cond_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 856 */
/* nop */
/* .line 857 */
int v2 = 1; // const/4 v2, 0x1
/* .line 855 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 856 */
/* throw v2 */
} // .end method
private Boolean setIndividualModelEnabledOnCommand ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 837 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 839 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mDisplayPowerControllerImpl;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 840 */
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).setIndividualModelEnabledOnCommand ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setIndividualModelEnabledOnCommand(Z)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 843 */
} // :cond_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 844 */
/* nop */
/* .line 845 */
int v2 = 1; // const/4 v2, 0x1
/* .line 843 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 844 */
/* throw v2 */
} // .end method
private Boolean setVideoInformation ( android.os.Parcel p0 ) {
/* .locals 20 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .line 322 */
/* move-object/from16 v1, p0 */
v0 = this.mContext;
final String v2 = "com.miui.permission.VIDEO_INFORMATION"; // const-string v2, "com.miui.permission.VIDEO_INFORMATION"
final String v3 = "Permission required to set video information"; // const-string v3, "Permission required to set video information"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 324 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
/* move-object/from16 v2, p1 */
(( android.os.Parcel ) v2 ).enforceInterface ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 326 */
v11 = android.os.Binder .getCallingPid ( );
/* .line 327 */
/* .local v11, "pid":I */
v12 = /* invoke-virtual/range {p1 ..p1}, Landroid/os/Parcel;->readBoolean()Z */
/* .line 328 */
/* .local v12, "bulletChatStatus":Z */
v13 = /* invoke-virtual/range {p1 ..p1}, Landroid/os/Parcel;->readFloat()F */
/* .line 329 */
/* .local v13, "frameRate":F */
v14 = /* invoke-virtual/range {p1 ..p1}, Landroid/os/Parcel;->readInt()I */
/* .line 330 */
/* .local v14, "width":I */
v15 = /* invoke-virtual/range {p1 ..p1}, Landroid/os/Parcel;->readInt()I */
/* .line 331 */
/* .local v15, "height":I */
v16 = /* invoke-virtual/range {p1 ..p1}, Landroid/os/Parcel;->readFloat()F */
/* .line 332 */
/* .local v16, "compressionRatio":F */
/* invoke-virtual/range {p1 ..p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder; */
/* .line 333 */
/* .local v10, "token":Landroid/os/IBinder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setVideoInformation bulletChatStatus:" */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",pid:"; // const-string v3, ",pid:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ",token:"; // const-string v3, ",token:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DisplayManagerServiceImpl"; // const-string v3, "DisplayManagerServiceImpl"
android.util.Slog .d ( v3,v0 );
/* .line 335 */
/* if-lez v11, :cond_0 */
if ( v10 != null) { // if-eqz v10, :cond_0
/* .line 336 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v17 */
/* .line 338 */
/* .local v17, "ident":J */
try { // :try_start_0
v3 = this.mDisplayFeatureManagerServiceImpl;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move v4, v11 */
/* move v5, v12 */
/* move v6, v13 */
/* move v7, v14 */
/* move v8, v15 */
/* move/from16 v9, v16 */
/* move-object/from16 v19, v10 */
} // .end local v10 # "token":Landroid/os/IBinder;
/* .local v19, "token":Landroid/os/IBinder; */
try { // :try_start_1
/* invoke-virtual/range {v3 ..v10}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->setVideoInformation(IZFIIFLandroid/os/IBinder;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 341 */
/* invoke-static/range {v17 ..v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 342 */
/* nop */
/* .line 343 */
int v0 = 1; // const/4 v0, 0x1
/* .line 341 */
/* :catchall_0 */
/* move-exception v0 */
} // .end local v19 # "token":Landroid/os/IBinder;
/* .restart local v10 # "token":Landroid/os/IBinder; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v19, v10 */
} // .end local v10 # "token":Landroid/os/IBinder;
/* .restart local v19 # "token":Landroid/os/IBinder; */
} // :goto_0
/* invoke-static/range {v17 ..v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V */
/* .line 342 */
/* throw v0 */
/* .line 335 */
} // .end local v17 # "ident":J
} // .end local v19 # "token":Landroid/os/IBinder;
/* .restart local v10 # "token":Landroid/os/IBinder; */
} // :cond_0
/* move-object/from16 v19, v10 */
/* .line 345 */
} // .end local v10 # "token":Landroid/os/IBinder;
/* .restart local v19 # "token":Landroid/os/IBinder; */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean showTouchCoverProtectionRect ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 861 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 863 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mDisplayPowerControllerImpl;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = android.os.Build .isDebuggable ( );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 864 */
v2 = this.mDisplayPowerControllerImpl;
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).showTouchCoverProtectionRect ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->showTouchCoverProtectionRect(Z)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 867 */
} // :cond_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 868 */
/* nop */
/* .line 869 */
int v2 = 1; // const/4 v2, 0x1
/* .line 867 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 868 */
/* throw v2 */
} // .end method
private void updateDefaultDisplayLocked ( ) {
/* .locals 2 */
/* .line 526 */
v0 = this.mLogicalDisplayMapper;
/* if-nez v0, :cond_0 */
return;
/* .line 528 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.LogicalDisplayMapper ) v0 ).getDisplayLocked ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayLocked(I)Lcom/android/server/display/LogicalDisplay;
this.mDefaultLogicalDisplay = v0;
/* .line 529 */
/* if-nez v0, :cond_1 */
/* .line 530 */
final String v0 = "DisplayManagerServiceImpl"; // const-string v0, "DisplayManagerServiceImpl"
final String v1 = "get default display error"; // const-string v1, "get default display error"
android.util.Slog .e ( v0,v1 );
/* .line 532 */
} // :cond_1
v0 = this.mDefaultLogicalDisplay;
(( com.android.server.display.LogicalDisplay ) v0 ).getPrimaryDisplayDeviceLocked ( ); // invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->getPrimaryDisplayDeviceLocked()Lcom/android/server/display/DisplayDevice;
this.mDefaultDisplayDevice = v0;
/* .line 533 */
/* instance-of v1, v0, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 534 */
/* move-object v1, v0 */
/* check-cast v1, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice; */
this.mDefaultLocalDisplayDevice = v1;
/* .line 537 */
} // :cond_2
(( com.android.server.display.DisplayDevice ) v0 ).getDisplayTokenLocked ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayDevice;->getDisplayTokenLocked()Landroid/os/IBinder;
this.mDefaultDisplayToken = v0;
/* .line 538 */
return;
} // .end method
private Boolean updateDolbyPreviewState ( android.os.Parcel p0 ) {
/* .locals 11 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .line 269 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 270 */
(( android.os.Parcel ) p1 ).readStrongBinder ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 272 */
/* .local v0, "token":Landroid/os/IBinder; */
v1 = (( android.os.Parcel ) p1 ).readBoolean ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z
/* .line 274 */
/* .local v1, "isDolbyPreviewEnable":Z */
v2 = (( android.os.Parcel ) p1 ).readFloat ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F
/* .line 275 */
/* .local v2, "dolbyPreviewBoostRatio":F */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v3 */
/* .line 276 */
/* .local v3, "ident":J */
v5 = android.os.Binder .getCallingUid ( );
/* .line 277 */
/* .local v5, "callingUid":I */
v6 = android.os.Binder .getCallingPid ( );
/* .line 279 */
/* .local v6, "callingPid":I */
int v7 = 1; // const/4 v7, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 280 */
try { // :try_start_0
v8 = this.mLock;
/* monitor-enter v8 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 282 */
try { // :try_start_1
/* invoke-direct {p0, v0, v7, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
/* .line 283 */
v9 = this.mDisplayPowerControllerImpl;
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 285 */
(( com.android.server.display.DisplayPowerControllerImpl ) v9 ).updateDolbyPreviewStateLocked ( v1, v2 ); // invoke-virtual {v9, v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyPreviewStateLocked(ZF)V
/* .line 287 */
} // :cond_0
/* monitor-exit v8 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 288 */
try { // :try_start_2
final String v8 = "DisplayManagerServiceImpl"; // const-string v8, "DisplayManagerServiceImpl"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "updateDolbyPreviewStateLocked: callingUid: " */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", callingPid: "; // const-string v10, ", callingPid: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", isDolbyPreviewEnable: "; // const-string v10, ", isDolbyPreviewEnable: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v10 = ", dolbyPreviewBoostRatio: "; // const-string v10, ", dolbyPreviewBoostRatio: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v8,v9 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 287 */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_3
/* monitor-exit v8 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "token":Landroid/os/IBinder;
} // .end local v1 # "isDolbyPreviewEnable":Z
} // .end local v2 # "dolbyPreviewBoostRatio":F
} // .end local v3 # "ident":J
} // .end local v5 # "callingUid":I
} // .end local v6 # "callingPid":I
} // .end local p0 # "this":Lcom/android/server/display/DisplayManagerServiceImpl;
} // .end local p1 # "data":Landroid/os/Parcel;
try { // :try_start_4
/* throw v7 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 294 */
/* .restart local v0 # "token":Landroid/os/IBinder; */
/* .restart local v1 # "isDolbyPreviewEnable":Z */
/* .restart local v2 # "dolbyPreviewBoostRatio":F */
/* .restart local v3 # "ident":J */
/* .restart local v5 # "callingUid":I */
/* .restart local v6 # "callingPid":I */
/* .restart local p0 # "this":Lcom/android/server/display/DisplayManagerServiceImpl; */
/* .restart local p1 # "data":Landroid/os/Parcel; */
/* :catchall_1 */
/* move-exception v7 */
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 295 */
/* throw v7 */
/* .line 294 */
} // :cond_1
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v3,v4 );
/* .line 295 */
/* nop */
/* .line 296 */
} // .end method
private updateScreenEffectDisplayIndexLocked ( ) {
/* .locals 5 */
/* .line 163 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 164 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 166 */
/* .local v1, "displayIndex":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = v3 = this.mDisplayDevices;
/* if-ge v2, v3, :cond_0 */
/* .line 167 */
java.lang.Integer .valueOf ( v2 );
/* .line 166 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 169 */
} // .end local v2 # "i":I
v2 = } // :cond_0
/* new-array v2, v2, [I */
this.mScreenEffectDisplayIndex = v2;
/* .line 170 */
int v2 = 0; // const/4 v2, 0x0
/* .restart local v2 # "i":I */
v3 = } // :goto_1
/* if-ge v2, v3, :cond_1 */
/* .line 171 */
v3 = this.mScreenEffectDisplayIndex;
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* aput v4, v3, v2 */
/* .line 170 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 173 */
} // .end local v2 # "i":I
} // :cond_1
v2 = this.mScreenEffectDisplayIndex;
/* monitor-exit v0 */
/* .line 174 */
} // .end local v1 # "displayIndex":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public void addDisplayGroupManuallyIfNeededLocked ( java.util.concurrent.CopyOnWriteArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/concurrent/CopyOnWriteArrayList<", */
/* "Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 967 */
/* .local p1, "mDisplayGroupListeners":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;>;" */
/* const-string/jumbo v0, "star" */
v1 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 968 */
final String v0 = "DisplayManagerServiceImpl"; // const-string v0, "DisplayManagerServiceImpl"
final String v1 = "initPowerManagement: Manually set display group"; // const-string v1, "initPowerManagement: Manually set display group"
android.util.Slog .d ( v0,v1 );
/* .line 969 */
(( java.util.concurrent.CopyOnWriteArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener; */
/* .line 970 */
/* .local v1, "listener":Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener; */
int v2 = 1; // const/4 v2, 0x1
/* .line 971 */
} // .end local v1 # "listener":Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;
/* .line 973 */
} // :cond_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 727 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 728 */
final String v0 = "DisplayManagerServiceImpl Configuration:"; // const-string v0, "DisplayManagerServiceImpl Configuration:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 729 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DMS:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDebug:Z */
/* .line 730 */
return;
} // .end method
public void finishedGoingToSleep ( ) {
/* .locals 1 */
/* .line 767 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiFoldPolicy;
/* if-nez v0, :cond_0 */
/* .line 770 */
} // :cond_0
(( com.android.server.display.MiuiFoldPolicy ) v0 ).notifyFinishedGoingToSleep ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->notifyFinishedGoingToSleep()V
/* .line 771 */
return;
/* .line 768 */
} // :cond_1
} // :goto_0
return;
} // .end method
public Integer getDefaultSleepFlags ( ) {
/* .locals 1 */
/* .line 991 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Float getDolbyPreviewBoostRatio ( ) {
/* .locals 1 */
/* .line 612 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 613 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).getDolbyPreviewBoostRatio ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getDolbyPreviewBoostRatio()F
/* .line 615 */
} // :cond_0
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
Boolean getDozeBrightnessThreshold ( android.os.Parcel p0, android.os.Parcel p1 ) {
/* .locals 4 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .param p2, "reply" # Landroid/os/Parcel; */
/* .line 224 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 225 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 227 */
/* .local v0, "token":J */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* new-array v2, v2, [F */
/* .line 228 */
/* .local v2, "result":[F */
v3 = this.mDisplayPowerControllerImpl;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 229 */
(( com.android.server.display.DisplayPowerControllerImpl ) v3 ).getDozeBrightnessThreshold ( ); // invoke-virtual {v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->getDozeBrightnessThreshold()[F
/* move-object v2, v3 */
/* .line 231 */
} // :cond_0
/* array-length v3, v2 */
(( android.os.Parcel ) p2 ).writeInt ( v3 ); // invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 232 */
(( android.os.Parcel ) p2 ).writeFloatArray ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->writeFloatArray([F)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 234 */
} // .end local v2 # "result":[F
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 235 */
/* nop */
/* .line 236 */
int v2 = 1; // const/4 v2, 0x1
/* .line 234 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 235 */
/* throw v2 */
} // .end method
public Float getGalleryHdrBoostFactor ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "sdrBacklight" # F */
/* .param p2, "hdrBacklight" # F */
/* .line 662 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 663 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).getGalleryHdrBoostFactor ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->getGalleryHdrBoostFactor(FF)F
/* .line 665 */
} // :cond_0
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public Boolean getIsResetRate ( ) {
/* .locals 1 */
/* .line 386 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mIsResetRate:Z */
} // .end method
public Integer getLogicalDisplayId ( Boolean p0, com.android.server.display.layout.DisplayIdProducer p1 ) {
/* .locals 1 */
/* .param p1, "isDefault" # Z */
/* .param p2, "idProducer" # Lcom/android/server/display/layout/DisplayIdProducer; */
/* .line 996 */
/* sget-boolean v0, Lcom/android/server/display/DisplayManagerServiceImpl;->IS_FOLDABLE_OR_FLIP_DEVICE:Z */
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_0
} // .end method
public Integer getMaskedDensity ( android.graphics.Rect p0, com.android.server.display.DisplayDeviceInfo p1 ) {
/* .locals 3 */
/* .param p1, "maskingInsets" # Landroid/graphics/Rect; */
/* .param p2, "deviceInfo" # Lcom/android/server/display/DisplayDeviceInfo; */
/* .line 953 */
/* iget v0, p2, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I */
/* .line 954 */
/* .local v0, "maskedDensity":I */
v1 = com.android.server.wm.WindowManagerServiceStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 956 */
v1 = com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 957 */
/* .local v1, "density":I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_0 */
/* .line 958 */
/* move v0, v1 */
/* .line 961 */
} // .end local v1 # "density":I
} // :cond_0
} // .end method
public Integer getMaskedHeight ( android.graphics.Rect p0, com.android.server.display.DisplayDeviceInfo p1 ) {
/* .locals 4 */
/* .param p1, "maskingInsets" # Landroid/graphics/Rect; */
/* .param p2, "deviceInfo" # Lcom/android/server/display/DisplayDeviceInfo; */
/* .line 933 */
/* iget v0, p2, Lcom/android/server/display/DisplayDeviceInfo;->height:I */
/* .line 934 */
/* .local v0, "deviceInfoHeight":I */
v1 = com.android.server.wm.WindowManagerServiceStub .get ( );
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I */
/* if-ne v1, v2, :cond_0 */
/* .line 936 */
com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 937 */
/* .local v1, "screenResolution":[I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 938 */
/* aget v0, v1, v2 */
/* .line 943 */
} // .end local v1 # "screenResolution":[I
} // :cond_0
final String v1 = "cetus"; // const-string v1, "cetus"
v3 = android.os.Build.DEVICE;
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I */
/* if-ne v1, v2, :cond_1 */
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
/* const/high16 v2, 0x100000 */
/* and-int/2addr v1, v2 */
/* if-nez v1, :cond_1 */
/* .line 946 */
/* const/16 v0, 0xa50 */
/* .line 948 */
} // :cond_1
/* iget v1, p1, Landroid/graphics/Rect;->top:I */
/* sub-int v1, v0, v1 */
/* iget v2, p1, Landroid/graphics/Rect;->bottom:I */
/* sub-int/2addr v1, v2 */
} // .end method
public Integer getMaskedWidth ( android.graphics.Rect p0, com.android.server.display.DisplayDeviceInfo p1 ) {
/* .locals 4 */
/* .param p1, "maskingInsets" # Landroid/graphics/Rect; */
/* .param p2, "deviceInfo" # Lcom/android/server/display/DisplayDeviceInfo; */
/* .line 913 */
/* iget v0, p2, Lcom/android/server/display/DisplayDeviceInfo;->width:I */
/* .line 914 */
/* .local v0, "deviceInfoWidth":I */
v1 = com.android.server.wm.WindowManagerServiceStub .get ( );
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I */
/* if-ne v1, v2, :cond_0 */
/* .line 916 */
com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 917 */
/* .local v1, "screenResolution":[I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 918 */
int v3 = 0; // const/4 v3, 0x0
/* aget v0, v1, v3 */
/* .line 923 */
} // .end local v1 # "screenResolution":[I
} // :cond_0
final String v1 = "cetus"; // const-string v1, "cetus"
v3 = android.os.Build.DEVICE;
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I */
/* if-ne v1, v2, :cond_1 */
/* iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
/* const/high16 v2, 0x100000 */
/* and-int/2addr v1, v2 */
/* if-nez v1, :cond_1 */
/* .line 926 */
/* const/16 v0, 0x370 */
/* .line 928 */
} // :cond_1
/* iget v1, p1, Landroid/graphics/Rect;->left:I */
/* sub-int v1, v0, v1 */
/* iget v2, p1, Landroid/graphics/Rect;->right:I */
/* sub-int/2addr v1, v2 */
} // .end method
public Float getMaxHbmBrightnessForPeak ( ) {
/* .locals 1 */
/* .line 635 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 636 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).getMaxHbmBrightnessForPeak ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxHbmBrightnessForPeak()F
/* .line 638 */
} // :cond_0
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public Float getMaxManualBrightnessBoost ( ) {
/* .locals 1 */
/* .line 649 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 650 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).getMaxManualBrightnessBoost ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxManualBrightnessBoost()F
/* .line 652 */
} // :cond_0
/* const/high16 v0, -0x40800000 # -1.0f */
} // .end method
Boolean getScreenEffectAvailableDisplay ( android.os.Parcel p0, android.os.Parcel p1 ) {
/* .locals 4 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .param p2, "reply" # Landroid/os/Parcel; */
/* .line 196 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 197 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 199 */
/* .local v0, "token":J */
try { // :try_start_0
(( com.android.server.display.DisplayManagerServiceImpl ) p0 ).getScreenEffectAvailableDisplayInternal ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectAvailableDisplayInternal()[I
/* .line 200 */
/* .local v2, "result":[I */
/* array-length v3, v2 */
(( android.os.Parcel ) p2 ).writeInt ( v3 ); // invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 201 */
(( android.os.Parcel ) p2 ).writeIntArray ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->writeIntArray([I)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 203 */
} // .end local v2 # "result":[I
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 204 */
/* nop */
/* .line 205 */
int v2 = 1; // const/4 v2, 0x1
/* .line 203 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 204 */
/* throw v2 */
} // .end method
public getScreenEffectAvailableDisplayInternal ( ) {
/* .locals 2 */
/* .line 178 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 179 */
try { // :try_start_0
v1 = this.mScreenEffectDisplayIndex;
/* monitor-exit v0 */
/* .line 180 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean getScreenEffectDisplayIndex ( android.os.Parcel p0, android.os.Parcel p1 ) {
/* .locals 4 */
/* .param p1, "data" # Landroid/os/Parcel; */
/* .param p2, "reply" # Landroid/os/Parcel; */
/* .line 209 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p1 ).enforceInterface ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 210 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 212 */
/* .local v0, "token":J */
try { // :try_start_0
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v2 */
v2 = /* invoke-direct {p0, v2, v3}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectDisplayIndexInternal(J)I */
/* .line 213 */
/* .local v2, "result":I */
(( android.os.Parcel ) p2 ).writeInt ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/Parcel;->writeInt(I)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 215 */
} // .end local v2 # "result":I
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 216 */
/* nop */
/* .line 217 */
int v2 = 1; // const/4 v2, 0x1
/* .line 215 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 216 */
/* throw v2 */
} // .end method
protected java.lang.Object getSyncRoot ( ) {
/* .locals 1 */
/* .line 394 */
v0 = this.mLock;
} // .end method
public void init ( java.lang.Object p0, android.content.Context p1, android.os.Looper p2, com.android.server.display.LogicalDisplayMapper p3 ) {
/* .locals 2 */
/* .param p1, "lock" # Ljava/lang/Object; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "looper" # Landroid/os/Looper; */
/* .param p4, "logicalDisplayMapper" # Lcom/android/server/display/LogicalDisplayMapper; */
/* .line 98 */
this.mLock = p1;
/* .line 99 */
this.mContext = p2;
/* .line 100 */
/* new-instance v0, Lcom/android/server/display/DisplayManagerServiceImpl$DisplayManagerStubHandler; */
/* invoke-direct {v0, p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl$DisplayManagerStubHandler;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 101 */
this.mLogicalDisplayMapper = p4;
/* .line 102 */
/* nop */
/* .line 103 */
com.android.server.display.DisplayFeatureManagerServiceStub .getInstance ( );
/* check-cast v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl; */
this.mDisplayFeatureManagerServiceImpl = v0;
/* .line 105 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11050082 */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
/* new-instance v0, Lcom/android/server/display/MiuiFoldPolicy; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/display/MiuiFoldPolicy;-><init>(Landroid/content/Context;)V */
this.mMiuiFoldPolicy = v0;
/* .line 109 */
} // :cond_0
return;
} // .end method
public Boolean isDisplayGroupAlwaysOn ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "groupId" # I */
/* .line 425 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 426 */
/* .line 428 */
} // :cond_0
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 429 */
try { // :try_start_0
v2 = this.mLogicalDisplayMapper;
(( com.android.server.display.LogicalDisplayMapper ) v2 ).getDisplayGroupLocked ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayGroupLocked(I)Lcom/android/server/display/DisplayGroup;
/* .line 431 */
/* .local v2, "displayGroup":Lcom/android/server/display/DisplayGroup; */
int v3 = 0; // const/4 v3, 0x0
/* .line 432 */
/* .local v3, "alwaysOn":Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 433 */
v4 = (( com.android.server.display.DisplayGroup ) v2 ).getSizeLocked ( ); // invoke-virtual {v2}, Lcom/android/server/display/DisplayGroup;->getSizeLocked()I
/* .line 434 */
/* .local v4, "size":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v4, :cond_2 */
/* .line 435 */
v6 = (( com.android.server.display.DisplayGroup ) v2 ).getIdLocked ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/display/DisplayGroup;->getIdLocked(I)I
/* .line 436 */
/* .local v6, "id":I */
v7 = this.mLogicalDisplayMapper;
(( com.android.server.display.LogicalDisplayMapper ) v7 ).getDisplayLocked ( v6, v0 ); // invoke-virtual {v7, v6, v0}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayLocked(IZ)Lcom/android/server/display/LogicalDisplay;
/* .line 437 */
/* .local v7, "display":Lcom/android/server/display/LogicalDisplay; */
if ( v7 != null) { // if-eqz v7, :cond_1
(( com.android.server.display.LogicalDisplay ) v7 ).getDisplayInfoLocked ( ); // invoke-virtual {v7}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;
/* iget v8, v8, Landroid/view/DisplayInfo;->flags:I */
/* and-int/lit16 v8, v8, 0x200 */
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 439 */
int v3 = 1; // const/4 v3, 0x1
/* .line 440 */
/* .line 434 */
} // .end local v6 # "id":I
} // .end local v7 # "display":Lcom/android/server/display/LogicalDisplay;
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 444 */
} // .end local v4 # "size":I
} // .end local v5 # "i":I
} // :cond_2
} // :goto_1
/* monitor-exit v1 */
/* .line 445 */
} // .end local v2 # "displayGroup":Lcom/android/server/display/DisplayGroup;
} // .end local v3 # "alwaysOn":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public Boolean isDolbyPreviewEnable ( ) {
/* .locals 1 */
/* .line 600 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 601 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).isDolbyPreviewEnable ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isDolbyPreviewEnable()Z
/* .line 603 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isGalleryHdrEnable ( ) {
/* .locals 1 */
/* .line 621 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 622 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).isGalleryHdrEnable ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z
/* .line 624 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isInResolutionSwitchBlackList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 420 */
v0 = v0 = this.mResolutionSwitchProcessBlackList;
} // .end method
public Boolean isInResolutionSwitchProtectList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 415 */
v0 = v0 = this.mResolutionSwitchProcessProtectList;
} // .end method
public Boolean isSupportManualBrightnessBoost ( ) {
/* .locals 1 */
/* .line 642 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 643 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).isSupportManualBrightnessBoost ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportManualBrightnessBoost()Z
/* .line 645 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isSupportPeakBrightness ( ) {
/* .locals 1 */
/* .line 628 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 629 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).isSupportPeakBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z
/* .line 631 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void notifyFinishDisplayTransitionLocked ( ) {
/* .locals 1 */
/* .line 743 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiFoldPolicy;
/* if-nez v0, :cond_0 */
/* .line 746 */
} // :cond_0
(( com.android.server.display.MiuiFoldPolicy ) v0 ).dealDisplayTransition ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->dealDisplayTransition()V
/* .line 747 */
return;
/* .line 744 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void notifyHotplugConnectStateChangedLocked ( Long p0, Boolean p1, com.android.server.display.LocalDisplayAdapter$LocalDisplayDevice p2 ) {
/* .locals 11 */
/* .param p1, "physicalDisplayId" # J */
/* .param p3, "isConnected" # Z */
/* .param p4, "device" # Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice; */
/* .line 793 */
int v0 = 0; // const/4 v0, 0x0
/* .line 794 */
/* .local v0, "productName":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 795 */
/* .local v1, "frameRate":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 796 */
/* .local v2, "resolution":Ljava/lang/String; */
if ( p4 != null) { // if-eqz p4, :cond_1
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 797 */
(( com.android.server.display.LocalDisplayAdapter$LocalDisplayDevice ) p4 ).getDisplayDeviceInfoLocked ( ); // invoke-virtual {p4}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;
/* .line 798 */
/* .local v3, "deviceInfo":Lcom/android/server/display/DisplayDeviceInfo; */
v4 = this.deviceProductInfo;
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 799 */
v4 = this.deviceProductInfo;
(( android.hardware.display.DeviceProductInfo ) v4 ).getName ( ); // invoke-virtual {v4}, Landroid/hardware/display/DeviceProductInfo;->getName()Ljava/lang/String;
/* .line 801 */
} // :cond_0
/* iget v4, v3, Lcom/android/server/display/DisplayDeviceInfo;->renderFrameRate:F */
/* float-to-int v1, v4 */
/* .line 802 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v5, v3, Lcom/android/server/display/DisplayDeviceInfo;->width:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " x "; // const-string v5, " x "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, v3, Lcom/android/server/display/DisplayDeviceInfo;->height:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 804 */
} // .end local v3 # "deviceInfo":Lcom/android/server/display/DisplayDeviceInfo;
} // :cond_1
com.android.server.power.PowerManagerServiceStub .get ( );
/* move-wide v5, p1 */
/* move v7, p3 */
/* move-object v8, v0 */
/* move v9, v1 */
/* move-object v10, v2 */
/* invoke-virtual/range {v4 ..v10}, Lcom/android/server/power/PowerManagerServiceStub;->notifyDisplayPortConnectStateChanged(JZLjava/lang/String;ILjava/lang/String;)V */
/* .line 806 */
return;
} // .end method
public void onBootCompleted ( ) {
/* .locals 1 */
/* .line 734 */
/* const-class v0, Lcom/android/server/tof/TofManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/tof/TofManagerInternal; */
this.mTofManagerInternal = v0;
/* .line 735 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
/* .line 736 */
v0 = this.mMiuiFoldPolicy;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 737 */
(( com.android.server.display.MiuiFoldPolicy ) v0 ).initMiuiFoldPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->initMiuiFoldPolicy()V
/* .line 739 */
} // :cond_0
return;
} // .end method
public Boolean onCommand ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 874 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
/* const-string/jumbo v0, "set-individual-model-disable" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_1 */
/* const-string/jumbo v0, "touch-cover-protect-show" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 6; // const/4 v0, 0x6
/* :sswitch_2 */
/* const-string/jumbo v0, "touch-cover-protect-hide" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* :sswitch_3 */
/* const-string/jumbo v0, "set-force-train-enable" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_4 */
/* const-string/jumbo v0, "set-custom-curve-enable" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
/* :sswitch_5 */
/* const-string/jumbo v0, "set-force-train-disable" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_6 */
/* const-string/jumbo v0, "set-individual-model-enable" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_7 */
/* const-string/jumbo v0, "set-custom-curve-disable" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 892 */
/* .line 890 */
/* :pswitch_0 */
v0 = /* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->showTouchCoverProtectionRect(Z)Z */
/* .line 888 */
/* :pswitch_1 */
v0 = /* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->showTouchCoverProtectionRect(Z)Z */
/* .line 886 */
/* :pswitch_2 */
v0 = /* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setForceTrainEnabledOnCommand(Z)Z */
/* .line 884 */
/* :pswitch_3 */
v0 = /* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setForceTrainEnabledOnCommand(Z)Z */
/* .line 882 */
/* :pswitch_4 */
v0 = /* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setIndividualModelEnabledOnCommand(Z)Z */
/* .line 880 */
/* :pswitch_5 */
v0 = /* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setIndividualModelEnabledOnCommand(Z)Z */
/* .line 878 */
/* :pswitch_6 */
v0 = /* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setCustomCurveEnabledOnCommand(Z)Z */
/* .line 876 */
/* :pswitch_7 */
v0 = /* invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setCustomCurveEnabledOnCommand(Z)Z */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5ae158c7 -> :sswitch_7 */
/* -0x4e160510 -> :sswitch_6 */
/* -0x3798252a -> :sswitch_5 */
/* -0x2a45acee -> :sswitch_4 */
/* -0x2922482b -> :sswitch_3 */
/* -0x1c406adc -> :sswitch_2 */
/* -0x1c3b6d21 -> :sswitch_1 */
/* 0x4ee3fb1b -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onEarlyInteractivityChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "interactive" # Z */
/* .line 780 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 781 */
v0 = this.mTofManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 782 */
(( com.android.server.tof.TofManagerInternal ) v0 ).onEarlyInteractivityChange ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/tof/TofManagerInternal;->onEarlyInteractivityChange(Z)V
/* .line 785 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/display/DisplayManagerServiceImpl;->IS_FOLDABLE_OR_FLIP_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 786 */
com.android.server.display.statistics.OneTrackFoldStateHelper .getInstance ( );
(( com.android.server.display.statistics.OneTrackFoldStateHelper ) v0 ).onEarlyInteractivityChange ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->onEarlyInteractivityChange(Z)V
/* .line 789 */
} // :cond_1
return;
} // .end method
public void onHelp ( java.io.PrintWriter p0 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 899 */
v0 = android.os.Build .isDebuggable ( );
/* if-nez v0, :cond_0 */
/* .line 900 */
return;
/* .line 902 */
} // :cond_0
final String v0 = " set-custom-curve-[enable|disable]"; // const-string v0, " set-custom-curve-[enable|disable]"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 903 */
final String v0 = " Enable or disable custom curve"; // const-string v0, " Enable or disable custom curve"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 904 */
final String v0 = " set-individual-model-[enable|disable]"; // const-string v0, " set-individual-model-[enable|disable]"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 905 */
final String v0 = " Enable or disable individual model"; // const-string v0, " Enable or disable individual model"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 906 */
final String v0 = " set-force-train-[enable|disable]"; // const-string v0, " set-force-train-[enable|disable]"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 907 */
final String v0 = " Enable or disable force train"; // const-string v0, " Enable or disable force train"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 908 */
final String v0 = " touch-cover-protect-[show|hide]"; // const-string v0, " touch-cover-protect-[show|hide]"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 909 */
final String v0 = " Show or hide the touch cover protection rect"; // const-string v0, " Show or hide the touch cover protection rect"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 910 */
return;
} // .end method
public Boolean onTransact ( android.os.Handler p0, Integer p1, android.os.Parcel p2, android.os.Parcel p3, Integer p4 ) {
/* .locals 1 */
/* .param p1, "displayControllerHandler" # Landroid/os/Handler; */
/* .param p2, "code" # I */
/* .param p3, "data" # Landroid/os/Parcel; */
/* .param p4, "reply" # Landroid/os/Parcel; */
/* .param p5, "flags" # I */
/* .line 241 */
/* const v0, 0xfffffe */
/* if-ne p2, v0, :cond_0 */
/* .line 242 */
v0 = (( com.android.server.display.DisplayManagerServiceImpl ) p0 ).resetAutoBrightnessShortModel ( p1, p3 ); // invoke-virtual {p0, p1, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->resetAutoBrightnessShortModel(Landroid/os/Handler;Landroid/os/Parcel;)Z
/* .line 243 */
} // :cond_0
/* const v0, 0xfffffd */
/* if-ne p2, v0, :cond_1 */
/* .line 244 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->setBrightnessRate(Landroid/os/Parcel;)Z */
/* .line 245 */
} // :cond_1
/* const v0, 0xfffffc */
/* if-ne p2, v0, :cond_2 */
/* .line 246 */
v0 = (( com.android.server.display.DisplayManagerServiceImpl ) p0 ).getScreenEffectAvailableDisplay ( p3, p4 ); // invoke-virtual {p0, p3, p4}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectAvailableDisplay(Landroid/os/Parcel;Landroid/os/Parcel;)Z
/* .line 247 */
} // :cond_2
/* const v0, 0xfffffb */
/* if-ne p2, v0, :cond_3 */
/* .line 248 */
v0 = (( com.android.server.display.DisplayManagerServiceImpl ) p0 ).getScreenEffectDisplayIndex ( p3, p4 ); // invoke-virtual {p0, p3, p4}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectDisplayIndex(Landroid/os/Parcel;Landroid/os/Parcel;)Z
/* .line 249 */
} // :cond_3
/* const v0, 0xfffffa */
/* if-ne p2, v0, :cond_4 */
/* .line 250 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->setVideoInformation(Landroid/os/Parcel;)Z */
/* .line 251 */
} // :cond_4
/* const v0, 0xfffff9 */
/* if-ne p2, v0, :cond_5 */
/* .line 252 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->handleGalleryHdrRequest(Landroid/os/Parcel;)Z */
/* .line 253 */
} // :cond_5
/* const v0, 0xfffff8 */
/* if-ne p2, v0, :cond_6 */
/* .line 254 */
/* invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->appRequestChangeSceneRefreshRate(Landroid/os/Parcel;)Z */
/* .line 255 */
} // :cond_6
/* const v0, 0xfffff7 */
/* if-ne p2, v0, :cond_7 */
/* .line 256 */
v0 = (( com.android.server.display.DisplayManagerServiceImpl ) p0 ).getDozeBrightnessThreshold ( p3, p4 ); // invoke-virtual {p0, p3, p4}, Lcom/android/server/display/DisplayManagerServiceImpl;->getDozeBrightnessThreshold(Landroid/os/Parcel;Landroid/os/Parcel;)Z
/* .line 257 */
} // :cond_7
/* const v0, 0xfffff6 */
/* if-ne p2, v0, :cond_8 */
/* .line 258 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateDolbyPreviewState(Landroid/os/Parcel;)Z */
/* .line 260 */
} // :cond_8
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void registerDeathCallbackLocked ( android.os.IBinder p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .line 578 */
v0 = this.mClientDeathCallbacks;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 579 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Client token "; // const-string v1, "Client token "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " has already registered."; // const-string v1, " has already registered."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "DisplayManagerServiceImpl"; // const-string v1, "DisplayManagerServiceImpl"
android.util.Slog .w ( v1,v0 );
/* .line 580 */
return;
/* .line 582 */
} // :cond_0
v0 = this.mClientDeathCallbacks;
/* new-instance v1, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/IBinder;I)V */
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 583 */
return;
} // .end method
Boolean resetAutoBrightnessShortModel ( android.os.Handler p0, android.os.Parcel p1 ) {
/* .locals 1 */
/* .param p1, "displayControllerHandler" # Landroid/os/Handler; */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .line 350 */
final String v0 = "android.view.android.hardware.display.IDisplayManager"; // const-string v0, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 351 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerServiceImpl;->resetAutoBrightnessShortModelInternal(Landroid/os/Handler;)V */
/* .line 352 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void screenTurningOff ( ) {
/* .locals 1 */
/* .line 759 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiFoldPolicy;
/* if-nez v0, :cond_0 */
/* .line 762 */
} // :cond_0
(( com.android.server.display.MiuiFoldPolicy ) v0 ).screenTurningOff ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurningOff()V
/* .line 763 */
return;
/* .line 760 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void screenTurningOn ( ) {
/* .locals 1 */
/* .line 751 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiFoldPolicy;
/* if-nez v0, :cond_0 */
/* .line 754 */
} // :cond_0
(( com.android.server.display.MiuiFoldPolicy ) v0 ).screenTurningOn ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurningOn()V
/* .line 755 */
return;
/* .line 752 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setDeviceStateLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 810 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiFoldPolicy;
/* if-nez v0, :cond_0 */
/* .line 813 */
} // :cond_0
(( com.android.server.display.MiuiFoldPolicy ) v0 ).setDeviceStateLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->setDeviceStateLocked(I)V
/* .line 814 */
com.android.server.display.statistics.OneTrackFoldStateHelper .getInstance ( );
(( com.android.server.display.statistics.OneTrackFoldStateHelper ) v0 ).notifyDeviceStateChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->notifyDeviceStateChanged(I)V
/* .line 815 */
return;
/* .line 811 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setSceneMaxRefreshRate ( Integer p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .param p2, "maxFrameRate" # F */
/* .line 450 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 451 */
try { // :try_start_0
v1 = this.mLogicalDisplayMapper;
(( com.android.server.display.LogicalDisplayMapper ) v1 ).getDisplayLocked ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayLocked(I)Lcom/android/server/display/LogicalDisplay;
/* .line 452 */
/* .local v1, "display":Lcom/android/server/display/LogicalDisplay; */
/* if-nez v1, :cond_0 */
/* .line 453 */
/* monitor-exit v0 */
return;
/* .line 455 */
} // :cond_0
com.android.server.display.mode.DisplayModeDirectorStub .getInstance ( );
/* .line 456 */
} // .end local v1 # "display":Lcom/android/server/display/LogicalDisplay;
/* monitor-exit v0 */
/* .line 457 */
return;
/* .line 456 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setUpDisplayPowerControllerImpl ( com.android.server.display.DisplayPowerControllerImpl p0 ) {
/* .locals 0 */
/* .param p1, "impl" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .line 119 */
this.mDisplayPowerControllerImpl = p1;
/* .line 120 */
return;
} // .end method
public void settingBrightnessStartChange ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 680 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 681 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).settingBrightnessStartChange ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->settingBrightnessStartChange(F)V
/* .line 683 */
} // :cond_0
return;
} // .end method
public Boolean shouldDeviceKeepWake ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "deviceState" # I */
/* .line 977 */
v0 = this.mOpenedReverseDeviceStates;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mOpenedReversePresentationDeviceStates;
/* if-nez v0, :cond_1 */
/* .line 978 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030047 */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mOpenedReverseDeviceStates = v0;
/* .line 980 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030048 */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mOpenedReversePresentationDeviceStates = v0;
/* .line 984 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
/* if-eq p1, v0, :cond_3 */
v0 = this.mOpenedReverseDeviceStates;
/* .line 985 */
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p1 );
/* if-nez v0, :cond_3 */
v0 = this.mOpenedReversePresentationDeviceStates;
/* .line 986 */
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :cond_3
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 984 */
} // :goto_1
} // .end method
public void shouldUpdateDisplayModeSpecs ( android.view.SurfaceControl$DesiredDisplayModeSpecs p0 ) {
/* .locals 6 */
/* .param p1, "modeSpecs" # Landroid/view/SurfaceControl$DesiredDisplayModeSpecs; */
/* .line 497 */
v0 = this.mDefaultLocalDisplayDevice;
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = this.mDefaultDisplayToken;
/* if-nez v1, :cond_0 */
/* .line 501 */
} // :cond_0
(( com.android.server.display.LocalDisplayAdapter$LocalDisplayDevice ) v0 ).setDesiredDisplayModeSpecsAsync ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->setDesiredDisplayModeSpecsAsync(Landroid/os/IBinder;Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;)V
/* .line 503 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 504 */
try { // :try_start_0
/* new-instance v1, Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* iget v2, p1, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;->defaultMode:I */
/* iget-boolean v3, p1, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;->allowGroupSwitching:Z */
v4 = this.primaryRanges;
v5 = this.appRequestRanges;
/* invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;-><init>(IZLandroid/view/SurfaceControl$RefreshRateRanges;Landroid/view/SurfaceControl$RefreshRateRanges;)V */
/* .line 511 */
/* .local v1, "desired":Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
v2 = this.mDefaultLocalDisplayDevice;
(( com.android.server.display.LocalDisplayAdapter$LocalDisplayDevice ) v2 ).updateDesiredDisplayModeSpecsLocked ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->updateDesiredDisplayModeSpecsLocked(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)V
/* .line 514 */
v2 = this.mDefaultLogicalDisplay;
(( com.android.server.display.LogicalDisplay ) v2 ).setDesiredDisplayModeSpecsLocked ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/LogicalDisplay;->setDesiredDisplayModeSpecsLocked(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)V
/* .line 517 */
com.android.server.display.mode.DisplayModeDirectorStub .getInstance ( );
v3 = this.mDefaultLogicalDisplay;
/* .line 518 */
v3 = (( com.android.server.display.LogicalDisplay ) v3 ).getDisplayIdLocked ( ); // invoke-virtual {v3}, Lcom/android/server/display/LogicalDisplay;->getDisplayIdLocked()I
/* .line 517 */
int v4 = 0; // const/4 v4, 0x0
/* .line 519 */
} // .end local v1 # "desired":Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;
/* monitor-exit v0 */
/* .line 520 */
return;
/* .line 519 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 498 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void startCbmStatsJob ( ) {
/* .locals 1 */
/* .line 819 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 820 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).startCbmStatsJob ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->startCbmStatsJob()V
/* .line 822 */
} // :cond_0
return;
} // .end method
public void temporaryBrightnessStartChange ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 673 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 674 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).temporaryBrightnessStartChange ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->temporaryBrightnessStartChange(F)V
/* .line 676 */
} // :cond_0
return;
} // .end method
protected void unregisterDeathCallbackLocked ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 586 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 587 */
v0 = this.mClientDeathCallbacks;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback; */
/* .line 588 */
/* .local v0, "deathCallback":Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 589 */
int v1 = 0; // const/4 v1, 0x0
/* .line 592 */
} // .end local v0 # "deathCallback":Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;
} // :cond_0
return;
} // .end method
public android.view.SurfaceControl$DynamicDisplayInfo updateDefaultDisplaySupportMode ( ) {
/* .locals 5 */
/* .line 477 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 478 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateDefaultDisplayLocked()V */
/* .line 479 */
v1 = this.mDefaultLocalDisplayDevice;
/* if-nez v1, :cond_0 */
/* .line 480 */
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 482 */
} // :cond_0
v1 = this.mDefaultLogicalDisplay;
(( com.android.server.display.LogicalDisplay ) v1 ).getDisplayInfoLocked ( ); // invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;
v1 = this.address;
/* .line 483 */
/* .local v1, "address":Landroid/view/DisplayAddress; */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 484 */
/* move-object v0, v1 */
/* check-cast v0, Landroid/view/DisplayAddress$Physical; */
/* .line 485 */
/* .local v0, "physicalAddress":Landroid/view/DisplayAddress$Physical; */
(( android.view.DisplayAddress$Physical ) v0 ).getPhysicalDisplayId ( ); // invoke-virtual {v0}, Landroid/view/DisplayAddress$Physical;->getPhysicalDisplayId()J
/* move-result-wide v2 */
/* .line 486 */
/* .local v2, "physicalDisplayId":J */
android.view.SurfaceControl .getDynamicDisplayInfo ( v2,v3 );
/* .line 483 */
} // .end local v0 # "physicalAddress":Landroid/view/DisplayAddress$Physical;
} // .end local v1 # "address":Landroid/view/DisplayAddress;
} // .end local v2 # "physicalDisplayId":J
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void updateDeviceDisplayChanged ( com.android.server.display.DisplayDevice p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "device" # Lcom/android/server/display/DisplayDevice; */
/* .param p2, "event" # I */
/* .line 124 */
/* instance-of v0, p1, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 125 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 126 */
/* packed-switch p2, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 134 */
/* :pswitch_1 */
try { // :try_start_0
v1 = v1 = this.mDisplayDevices;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 135 */
v1 = this.mDisplayDevices;
/* .line 136 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateScreenEffectDisplayIndexLocked()[I */
/* .line 128 */
/* :pswitch_2 */
v1 = v1 = this.mDisplayDevices;
/* if-nez v1, :cond_0 */
/* .line 129 */
v1 = this.mDisplayDevices;
/* .line 130 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateScreenEffectDisplayIndexLocked()[I */
/* .line 142 */
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 143 */
} // :cond_1
/* instance-of v0, p1, Lcom/android/server/display/VirtualDisplayAdapter$VirtualDisplayDevice; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 144 */
(( com.android.server.display.DisplayDevice ) p1 ).getDisplayDeviceInfoLocked ( ); // invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;
/* .line 145 */
/* .local v0, "info":Lcom/android/server/display/DisplayDeviceInfo; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 146 */
v1 = this.mSecurityManager;
/* if-nez v1, :cond_2 */
/* .line 147 */
/* const-class v1, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManager = v1;
/* .line 149 */
} // :cond_2
v1 = this.mSecurityManager;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 150 */
v2 = this.ownerPackageName;
v3 = this.name;
(( com.android.server.display.DisplayDevice ) p1 ).getDisplayTokenLocked ( ); // invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayTokenLocked()Landroid/os/IBinder;
(( miui.security.SecurityManagerInternal ) v1 ).onDisplayDeviceEvent ( v2, v3, v4, p2 ); // invoke-virtual {v1, v2, v3, v4, p2}, Lmiui/security/SecurityManagerInternal;->onDisplayDeviceEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;I)V
/* .line 152 */
} // :cond_3
com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 155 */
} // .end local v0 # "info":Lcom/android/server/display/DisplayDeviceInfo;
} // :cond_4
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void updateResolutionSwitchList ( java.util.List p0, java.util.List p1 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 405 */
/* .local p1, "resolutionSwitchProtectList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p2, "resolutionSwitchBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/DisplayManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/display/DisplayManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Ljava/util/List;Ljava/util/List;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 411 */
return;
} // .end method
public void updateRhythmicAppCategoryList ( java.util.List p0, java.util.List p1 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 399 */
/* .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mDisplayFeatureManagerServiceImpl;
(( com.android.server.display.DisplayFeatureManagerServiceImpl ) v0 ).updateRhythmicAppCategoryList ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
/* .line 400 */
return;
} // .end method
