.class Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;
.super Landroid/os/Handler;
.source "DaemonSensorPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DaemonSensorPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DaemonSensorHandle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DaemonSensorPolicy;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DaemonSensorPolicy;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DaemonSensorPolicy;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 183
    iput-object p1, p0, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    .line 184
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 185
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 189
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 191
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-static {v0}, Lcom/android/server/display/DaemonSensorPolicy;->-$$Nest$fgetmPowerManager(Lcom/android/server/display/DaemonSensorPolicy;)Landroid/os/PowerManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager;->isDeviceIdleMode()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy;->-$$Nest$fputmIsDeviceIdleMode(Lcom/android/server/display/DaemonSensorPolicy;Z)V

    .line 192
    nop

    .line 196
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
