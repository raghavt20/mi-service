class com.android.server.display.DisplayFeatureManagerService$ClientDeathCallback implements android.os.IBinder$DeathRecipient {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ClientDeathCallback" */
} // .end annotation
/* # instance fields */
private Integer mFlag;
private android.os.IBinder mToken;
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.display.DisplayFeatureManagerService$ClientDeathCallback ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .param p3, "flag" # I */
/* .line 1022 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1023 */
this.mToken = p2;
/* .line 1024 */
/* iput p3, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mFlag:I */
/* .line 1026 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 1029 */
	 /* .line 1027 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 1028 */
	 /* .local v0, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 1030 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 3 */
/* .line 1034 */
final String v0 = "DisplayFeatureManagerService"; // const-string v0, "DisplayFeatureManagerService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "binderDied: flag: "; // const-string v2, "binderDied: flag: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mFlag:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1035 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmClientDeathCallbacks ( v0 );
/* monitor-enter v0 */
/* .line 1036 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmClientDeathCallbacks ( v1 );
v2 = this.mToken;
(( java.util.HashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1037 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1038 */
v0 = this.mToken;
int v1 = 0; // const/4 v1, 0x0
/* .line 1039 */
v0 = this.this$0;
/* iget v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;->mFlag:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mdoDieLocked ( v0,v1 );
/* .line 1040 */
return;
/* .line 1037 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
	 /* monitor-exit v0 */
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* throw v1 */
} // .end method
