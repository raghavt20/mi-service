class com.android.server.display.DisplayManagerServiceImpl$ClientDeathCallback implements android.os.IBinder$DeathRecipient {
	 /* .source "DisplayManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayManagerServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ClientDeathCallback" */
} // .end annotation
/* # instance fields */
private Integer mFlag;
private android.os.IBinder mToken;
final com.android.server.display.DisplayManagerServiceImpl this$0; //synthetic
/* # direct methods */
public com.android.server.display.DisplayManagerServiceImpl$ClientDeathCallback ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayManagerServiceImpl; */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .line 704 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/IBinder;I)V */
/* .line 705 */
return;
} // .end method
public com.android.server.display.DisplayManagerServiceImpl$ClientDeathCallback ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayManagerServiceImpl; */
/* .param p2, "token" # Landroid/os/IBinder; */
/* .param p3, "flag" # I */
/* .line 707 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 708 */
this.mToken = p2;
/* .line 709 */
/* iput p3, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mFlag:I */
/* .line 711 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 714 */
	 /* .line 712 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 713 */
	 /* .local v0, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 715 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 4 */
/* .line 719 */
final String v0 = "DisplayManagerServiceImpl"; // const-string v0, "DisplayManagerServiceImpl"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "binderDied: flag: "; // const-string v2, "binderDied: flag: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mFlag:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 720 */
v0 = this.this$0;
com.android.server.display.DisplayManagerServiceImpl .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 721 */
try { // :try_start_0
v1 = this.this$0;
/* iget v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mFlag:I */
v3 = this.mToken;
com.android.server.display.DisplayManagerServiceImpl .-$$Nest$mdoDieLocked ( v1,v2,v3 );
/* .line 722 */
/* monitor-exit v0 */
/* .line 723 */
return;
/* .line 722 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
