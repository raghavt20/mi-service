class com.android.server.display.DisplayFeatureManagerService$2 extends android.util.IntProperty {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService;->onBootPhase(I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/IntProperty<", */
/* "Lcom/android/server/display/DisplayFeatureManagerService;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayFeatureManagerService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 320 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/util/IntProperty;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public java.lang.Integer get ( com.android.server.display.DisplayFeatureManagerService p0 ) {
/* .locals 1 */
/* .param p1, "object" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .line 340 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Integer .valueOf ( v0 );
} // .end method
public java.lang.Object get ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 320 */
/* check-cast p1, Lcom/android/server/display/DisplayFeatureManagerService; */
(( com.android.server.display.DisplayFeatureManagerService$2 ) p0 ).get ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService$2;->get(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/lang/Integer;
} // .end method
public void setValue ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "object" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .param p2, "value" # I */
/* .line 325 */
v0 = this.this$0;
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmDisplayState ( v0 );
int v1 = 3; // const/4 v1, 0x3
int v2 = 1; // const/4 v2, 0x1
/* if-eq v0, v2, :cond_1 */
/* if-gtz p2, :cond_0 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmPaperModeAnimator ( v0 );
/* .line 326 */
v0 = (( com.android.server.display.MiuiRampAnimator ) v0 ).isAnimating ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiRampAnimator;->isAnimating()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 327 */
} // :cond_0
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetScreenEffect ( p1,v1,p2 );
/* .line 328 */
} // :cond_1
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$sfgetIS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT ( );
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.this$0;
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmDisplayState ( v0 );
/* if-eq v0, v2, :cond_2 */
v0 = this.this$0;
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeEnabled ( v0 );
/* if-nez v0, :cond_2 */
/* .line 334 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetScreenEffect ( p1,v1,v0 );
/* .line 336 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setValue ( java.lang.Object p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 320 */
/* check-cast p1, Lcom/android/server/display/DisplayFeatureManagerService; */
(( com.android.server.display.DisplayFeatureManagerService$2 ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService$2;->setValue(Lcom/android/server/display/DisplayFeatureManagerService;I)V
return;
} // .end method
