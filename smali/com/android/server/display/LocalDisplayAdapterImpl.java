public class com.android.server.display.LocalDisplayAdapterImpl extends com.android.server.display.LocalDisplayAdapterStub {
	 /* .source "LocalDisplayAdapterImpl.java" */
	 /* # static fields */
	 private static final Boolean mIsFlipDevice;
	 private static final Boolean mIsFoldDevice;
	 /* # direct methods */
	 static com.android.server.display.LocalDisplayAdapterImpl ( ) {
		 /* .locals 1 */
		 /* .line 11 */
		 v0 = 		 miui.util.MiuiMultiDisplayTypeInfo .isFoldDeviceInside ( );
		 com.android.server.display.LocalDisplayAdapterImpl.mIsFoldDevice = (v0!= 0);
		 /* .line 12 */
		 v0 = 		 miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
		 com.android.server.display.LocalDisplayAdapterImpl.mIsFlipDevice = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.display.LocalDisplayAdapterImpl ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Lcom/android/server/display/LocalDisplayAdapterStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void setDeviceInfoFlagIfNeeded ( com.android.server.display.DisplayDeviceInfo p0, Boolean p1 ) {
		 /* .locals 2 */
		 /* .param p1, "deviceInfo" # Lcom/android/server/display/DisplayDeviceInfo; */
		 /* .param p2, "isFirstDisplay" # Z */
		 /* .line 16 */
		 /* sget-boolean v0, Lcom/android/server/display/LocalDisplayAdapterImpl;->mIsFoldDevice:Z */
		 /* if-nez v0, :cond_0 */
		 /* sget-boolean v0, Lcom/android/server/display/LocalDisplayAdapterImpl;->mIsFlipDevice:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 17 */
		 } // :cond_0
		 /* iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
		 /* or-int/lit16 v0, v0, 0x80 */
		 /* iput v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
		 /* .line 19 */
	 } // :cond_1
	 /* if-nez p2, :cond_2 */
	 /* const-string/jumbo v0, "star" */
	 v1 = android.os.Build.DEVICE;
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 20 */
		 /* iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
		 /* or-int/lit16 v0, v0, 0x4000 */
		 /* iput v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
		 /* .line 21 */
		 /* iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
		 /* or-int/lit16 v0, v0, 0x80 */
		 /* iput v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I */
		 /* .line 23 */
	 } // :cond_2
	 return;
} // .end method
public void updateScreenEffectIfNeeded ( Integer p0, Boolean p1 ) {
	 /* .locals 2 */
	 /* .param p1, "state" # I */
	 /* .param p2, "isFirstDisplay" # Z */
	 /* .line 27 */
	 /* const-string/jumbo v0, "star" */
	 v1 = android.os.Build.DEVICE;
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 if ( p2 != null) { // if-eqz p2, :cond_1
			 /* .line 28 */
		 } // :cond_0
		 com.android.server.display.DisplayFeatureManagerServiceStub .getInstance ( );
		 /* .line 30 */
	 } // :cond_1
	 return;
} // .end method
