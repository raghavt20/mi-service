public class com.android.server.display.BrightnessABTester {
	 /* .source "BrightnessABTester.java" */
	 /* # static fields */
	 private static final java.lang.String APP_NAME;
	 private static final java.lang.String CONTROL_GROUP;
	 private static final Long DELAY_TIME;
	 private static final java.lang.String EXPERIMENTAL_GROUP_1;
	 private static final java.lang.String EXPERIMENTAL_GROUP_2;
	 private static final java.lang.String EXPERIMENTAL_GROUP_3;
	 private static final java.lang.String EXPERIMENTAL_GROUP_4;
	 private static java.lang.String LAYER_NAME;
	 private static final java.lang.String TAG;
	 private static final java.lang.String THRESHOLD_EXP_KEY;
	 /* # instance fields */
	 private com.android.server.display.ABTestHelper mABTestHelper;
	 private mAmbientBrighteningLux;
	 private mAmbientBrighteningThresholds;
	 private mAmbientDarkeningLux;
	 private mAmbientDarkeningThresholds;
	 private com.android.server.display.AutomaticBrightnessControllerImpl mAutomaticBrightnessControllerImpl;
	 private com.android.server.display.statistics.BrightnessDataProcessor mBrightnessDataProcessor;
	 private java.lang.String mCurrentExperiment;
	 /* # direct methods */
	 public static void $r8$lambda$3j4OlJVSAa-1D8YPwfv29rWak6Y ( com.android.server.display.BrightnessABTester p0, java.lang.String p1, java.lang.String p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/BrightnessABTester;->lambda$startExperiment$0(Ljava/lang/String;Ljava/lang/String;)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$DvlmJ9JosBdqmkBwJ0mMJkY-Ufc ( com.android.server.display.BrightnessABTester p0, java.util.Map p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessABTester;->startExperiment(Ljava/util/Map;)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$Wa0wWhj9RwtZChp5hLRmXq-qk2Q ( com.android.server.display.BrightnessABTester p0, java.lang.Void p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessABTester;->endExperiment(Ljava/lang/Void;)V */
		 return;
	 } // .end method
	 static com.android.server.display.BrightnessABTester ( ) {
		 /* .locals 2 */
		 /* .line 30 */
		 final String v0 = "ro.product.mod_device"; // const-string v0, "ro.product.mod_device"
		 final String v1 = ""; // const-string v1, ""
		 android.os.SystemProperties .get ( v0,v1 );
		 return;
	 } // .end method
	 public com.android.server.display.BrightnessABTester ( ) {
		 /* .locals 11 */
		 /* .param p1, "looper" # Landroid/os/Looper; */
		 /* .param p2, "context" # Landroid/content/Context; */
		 /* .param p3, "brightnessDataProcessor" # Lcom/android/server/display/statistics/BrightnessDataProcessor; */
		 /* .line 40 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 34 */
		 final String v0 = ""; // const-string v0, ""
		 this.mCurrentExperiment = v0;
		 /* .line 41 */
		 this.mBrightnessDataProcessor = p3;
		 /* .line 42 */
		 /* new-instance v6, Ljava/util/HashMap; */
		 /* invoke-direct {v6}, Ljava/util/HashMap;-><init>()V */
		 /* .line 43 */
		 /* .local v6, "expCondition":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
		 /* new-instance v0, Lcom/android/server/display/ABTestHelper; */
		 final String v4 = "AutoBrightness"; // const-string v4, "AutoBrightness"
		 v5 = com.android.server.display.BrightnessABTester.LAYER_NAME;
		 /* new-instance v7, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda1; */
		 /* invoke-direct {v7, p0}, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/BrightnessABTester;)V */
		 /* new-instance v8, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda2; */
		 /* invoke-direct {v8, p0}, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/BrightnessABTester;)V */
		 /* const-wide/16 v9, 0x7530 */
		 /* move-object v1, v0 */
		 /* move-object v2, p1 */
		 /* move-object v3, p2 */
		 /* invoke-direct/range {v1 ..v10}, Lcom/android/server/display/ABTestHelper;-><init>(Landroid/os/Looper;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/function/Consumer;Ljava/util/function/Consumer;J)V */
		 this.mABTestHelper = v0;
		 /* .line 45 */
		 return;
	 } // .end method
	 private void endExperiment ( java.lang.Void p0 ) {
		 /* .locals 3 */
		 /* .param p1, "unused" # Ljava/lang/Void; */
		 /* .line 65 */
		 v0 = this.mCurrentExperiment;
		 v1 = 		 (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
		 int v2 = 0; // const/4 v2, 0x0
		 /* packed-switch v1, :pswitch_data_0 */
	 } // :cond_0
	 /* :pswitch_0 */
	 /* const-string/jumbo v1, "threshold_type" */
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* move v0, v2 */
	 } // :goto_0
	 int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_1 */
/* .line 67 */
/* :pswitch_1 */
final String v0 = "control_group"; // const-string v0, "control_group"
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->updateBrightnessThresholdABTest(Ljava/lang/String;)V */
/* .line 68 */
v0 = this.mAutomaticBrightnessControllerImpl;
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).setAnimationPolicyDisable ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setAnimationPolicyDisable(Z)V
/* .line 69 */
/* nop */
/* .line 73 */
} // :goto_2
final String v0 = ""; // const-string v0, ""
this.mCurrentExperiment = v0;
/* .line 74 */
v0 = this.mBrightnessDataProcessor;
v1 = this.mABTestHelper;
v1 = (( com.android.server.display.ABTestHelper ) v1 ).getExpId ( ); // invoke-virtual {v1}, Lcom/android/server/display/ABTestHelper;->getExpId()I
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).setExpId ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setExpId(I)V
/* .line 75 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x6db46e8e */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private getFloatArray ( android.content.res.TypedArray p0 ) {
/* .locals 4 */
/* .param p1, "array" # Landroid/content/res/TypedArray; */
/* .line 144 */
v0 = (( android.content.res.TypedArray ) p1 ).length ( ); // invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I
/* .line 145 */
/* .local v0, "N":I */
/* new-array v1, v0, [F */
/* .line 146 */
/* .local v1, "vals":[F */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 147 */
/* const/high16 v3, -0x40800000 # -1.0f */
v3 = (( android.content.res.TypedArray ) p1 ).getFloat ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F
/* aput v3, v1, v2 */
/* .line 146 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 149 */
} // .end local v2 # "i":I
} // :cond_0
(( android.content.res.TypedArray ) p1 ).recycle ( ); // invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V
/* .line 150 */
} // .end method
private void getValuesFromXml ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "configAmbientBrighteningLux" # I */
/* .param p2, "configAmbientBrighteningThresholds" # I */
/* .param p3, "configAmbientDarkeningLux" # I */
/* .param p4, "configAmbientDarkeningThresholds" # I */
/* .line 79 */
/* nop */
/* .line 80 */
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v0 ).obtainTypedArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 79 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F */
this.mAmbientBrighteningLux = v0;
/* .line 81 */
/* nop */
/* .line 82 */
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v0 ).obtainTypedArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 81 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F */
this.mAmbientBrighteningThresholds = v0;
/* .line 83 */
/* nop */
/* .line 84 */
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v0 ).obtainTypedArray ( p3 ); // invoke-virtual {v0, p3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 83 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F */
this.mAmbientDarkeningLux = v0;
/* .line 85 */
/* nop */
/* .line 86 */
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v0 ).obtainTypedArray ( p4 ); // invoke-virtual {v0, p4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 85 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessABTester;->getFloatArray(Landroid/content/res/TypedArray;)[F */
this.mAmbientDarkeningThresholds = v0;
/* .line 87 */
return;
} // .end method
private void lambda$startExperiment$0 ( java.lang.String p0, java.lang.String p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 53 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* packed-switch v0, :pswitch_data_0 */
} // :cond_0
/* :pswitch_0 */
/* const-string/jumbo v0, "threshold_type" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_1 */
/* .line 55 */
/* :pswitch_1 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/BrightnessABTester;->updateBrightnessThresholdABTest(Ljava/lang/String;)V */
/* .line 56 */
this.mCurrentExperiment = p1;
/* .line 57 */
/* nop */
/* .line 61 */
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x6db46e8e */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void startExperiment ( java.util.Map p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 52 */
/* .local p1, "expParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v0, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/BrightnessABTester$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/BrightnessABTester;)V */
/* .line 62 */
return;
} // .end method
private void updateBrightnessThresholdABTest ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "value" # Ljava/lang/String; */
/* .line 90 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "The abtest experiment of brightness target "; // const-string v1, "The abtest experiment of brightness target "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BrightnessABTester"; // const-string v1, "BrightnessABTester"
android.util.Slog .i ( v1,v0 );
/* .line 91 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 1; // const/4 v1, 0x1
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "control_group"; // const-string v0, "control_group"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_1 */
final String v0 = "experimental_group_4"; // const-string v0, "experimental_group_4"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_2 */
final String v0 = "experimental_group_3"; // const-string v0, "experimental_group_3"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* :sswitch_3 */
final String v0 = "experimental_group_2"; // const-string v0, "experimental_group_2"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_4 */
final String v0 = "experimental_group_1"; // const-string v0, "experimental_group_1"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 131 */
return;
/* .line 124 */
/* :pswitch_0 */
/* const v0, 0x11030028 */
/* const v2, 0x1103002d */
/* const v3, 0x1103001e */
/* const v4, 0x11030023 */
/* invoke-direct {p0, v3, v4, v0, v2}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V */
/* .line 128 */
v0 = this.mAutomaticBrightnessControllerImpl;
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).setAnimationPolicyDisable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setAnimationPolicyDisable(Z)V
/* .line 129 */
/* .line 116 */
/* :pswitch_1 */
/* const v0, 0x11030029 */
/* const v1, 0x1103002e */
/* const v2, 0x1103001f */
/* const v3, 0x11030024 */
/* invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V */
/* .line 120 */
/* .line 109 */
/* :pswitch_2 */
/* const v0, 0x1103002b */
/* const v1, 0x11030030 */
/* const v2, 0x11030021 */
/* const v3, 0x11030026 */
/* invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V */
/* .line 113 */
/* .line 102 */
/* :pswitch_3 */
/* const v0, 0x1103002c */
/* const v1, 0x11030031 */
/* const v2, 0x11030022 */
/* const v3, 0x11030027 */
/* invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V */
/* .line 106 */
/* .line 94 */
/* :pswitch_4 */
/* const v0, 0x1103002a */
/* const v1, 0x1103002f */
/* const v2, 0x11030020 */
/* const v3, 0x11030025 */
/* invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/display/BrightnessABTester;->getValuesFromXml(IIII)V */
/* .line 98 */
/* nop */
/* .line 133 */
} // :goto_2
v0 = this.mAutomaticBrightnessControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 134 */
(( com.android.server.display.AutomaticBrightnessControllerImpl ) v0 ).getHysteresisLevelsImpl ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getHysteresisLevelsImpl()Lcom/android/server/display/HysteresisLevelsStub;
/* .line 135 */
/* .local v0, "hysteresisLevelsImpl":Lcom/android/server/display/HysteresisLevelsStub; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 136 */
v1 = this.mAmbientBrighteningLux;
v2 = this.mAmbientBrighteningThresholds;
v3 = this.mAmbientDarkeningLux;
v4 = this.mAmbientDarkeningThresholds;
(( com.android.server.display.HysteresisLevelsStub ) v0 ).createHysteresisThresholdSpline ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/display/HysteresisLevelsStub;->createHysteresisThresholdSpline([F[F[F[F)V
/* .line 138 */
v1 = this.mBrightnessDataProcessor;
v2 = this.mABTestHelper;
v2 = (( com.android.server.display.ABTestHelper ) v2 ).getExpId ( ); // invoke-virtual {v2}, Lcom/android/server/display/ABTestHelper;->getExpId()I
(( com.android.server.display.statistics.BrightnessDataProcessor ) v1 ).setExpId ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setExpId(I)V
/* .line 141 */
} // .end local v0 # "hysteresisLevelsImpl":Lcom/android/server/display/HysteresisLevelsStub;
} // :cond_1
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4ea785e6 -> :sswitch_4 */
/* -0x4ea785e5 -> :sswitch_3 */
/* -0x4ea785e4 -> :sswitch_2 */
/* -0x4ea785e3 -> :sswitch_1 */
/* 0x4878851d -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void setAutomaticBrightnessControllerImpl ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) {
/* .locals 0 */
/* .param p1, "stub" # Lcom/android/server/display/AutomaticBrightnessControllerImpl; */
/* .line 48 */
this.mAutomaticBrightnessControllerImpl = p1;
/* .line 49 */
return;
} // .end method
