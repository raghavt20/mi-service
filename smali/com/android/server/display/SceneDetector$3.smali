.class Lcom/android/server/display/SceneDetector$3;
.super Lcom/xiaomi/aon/IMiAONListener$Stub;
.source "SceneDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SceneDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/SceneDetector;


# direct methods
.method public static synthetic $r8$lambda$Lv8E7MnLzpc2AXx-6uW6Za-0UrY(Lcom/android/server/display/SceneDetector$3;[I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector$3;->lambda$onCallbackListener$0([I)V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/SceneDetector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/SceneDetector;

    .line 258
    iput-object p1, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-direct {p0}, Lcom/xiaomi/aon/IMiAONListener$Stub;-><init>()V

    return-void
.end method

.method private synthetic lambda$onCallbackListener$0([I)V
    .locals 4
    .param p1, "data"    # [I

    .line 264
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmAonState(Lcom/android/server/display/SceneDetector;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 265
    const/4 v0, 0x0

    aget v0, p1, v0

    const-string v2, "SceneDetector"

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 266
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmBrightnessControllerImpl(Lcom/android/server/display/SceneDetector;)Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v1}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmPreAmbientLux(Lcom/android/server/display/SceneDetector;)F

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyAonFlareEvents(IF)V

    .line 268
    const-string v0, "aon flare algo suppress this darken event!"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmBrightnessControllerImpl(Lcom/android/server/display/SceneDetector;)Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v3}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmPreAmbientLux(Lcom/android/server/display/SceneDetector;)F

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyAonFlareEvents(IF)V

    .line 272
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmIsMainDarkenEvent(Lcom/android/server/display/SceneDetector;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$mupdateAutoBrightness(Lcom/android/server/display/SceneDetector;)V

    .line 274
    const-string v0, "aon flare algo not suppress this darken event!"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$munregisterAonFlareListener(Lcom/android/server/display/SceneDetector;)V

    .line 279
    :cond_2
    return-void
.end method


# virtual methods
.method public onCallbackListener(I[I)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "data"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 261
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$3;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmSceneDetectorHandler(Lcom/android/server/display/SceneDetector;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/SceneDetector$3$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2}, Lcom/android/server/display/SceneDetector$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector$3;[I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 281
    :cond_0
    return-void
.end method

.method public onImageAvailiable(I)V
    .locals 0
    .param p1, "frameId"    # I

    .line 286
    return-void
.end method
