.class public Lcom/android/server/display/MiuiDisplayCloudController;
.super Ljava/lang/Object;
.source "MiuiDisplayCloudController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/MiuiDisplayCloudController$Callback;,
        Lcom/android/server/display/MiuiDisplayCloudController$Observer;,
        Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;
    }
.end annotation


# static fields
.field private static final APP_CATEGORY_CONFIG:Ljava/lang/String; = "app-category-config"

.field private static final AUTO_BRIGHTNESS_STATISTICS_EVENT_ENABLE:Ljava/lang/String; = "automatic_brightness_statistics_event_enable"

.field private static final AUTO_BRIGHTNESS_STATISTICS_EVENT_MODULE_NAME:Ljava/lang/String; = "AutomaticBrightnessStatisticsEvent"

.field private static final BCBC_APP_CONFIG:Ljava/lang/String; = "bcbc_app_config"

.field private static final BCBC_FEATURE_MODULE_NAME:Ljava/lang/String; = "BCBCFeature"

.field private static final BRIGHTNESS_CURVE_OPTIMIZE_POLICY_DISABLE:Ljava/lang/String; = "brightness_curve_optimize_policy_disable"

.field private static final BRIGHTNESS_CURVE_OPTIMIZE_POLICY_MODULE_NAME:Ljava/lang/String; = "BrightnessCurveOptimizePolicy"

.field private static final BRIGHTNESS_STATISTICS_EVENTS_ENABLE:Ljava/lang/String; = "brightness_statistics_events_enable"

.field private static final BRIGHTNESS_STATISTICS_EVENTS_NAME:Ljava/lang/String; = "brightnessStatisticsEvents"

.field public static final CLOUD_BACKUP_DIR_NAME:Ljava/lang/String; = "displayconfig"

.field private static final CLOUD_BACKUP_FILE_ATTRIBUTE_ENABLE:Ljava/lang/String; = "enabled"

.field private static final CLOUD_BACKUP_FILE_ATTRIBUTE_ITEM:Ljava/lang/String; = "item"

.field private static final CLOUD_BACKUP_FILE_ATTRIBUTE_PACKAGE:Ljava/lang/String; = "package"

.field private static final CLOUD_BACKUP_FILE_ATTRIBUTE_VALUE:Ljava/lang/String; = "value"

.field private static final CLOUD_BACKUP_FILE_AUTO_BRIGHTNESS_STATISTICS_EVENT_ENABLE:Ljava/lang/String; = "automatic-brightness-statistics-event-enable"

.field private static final CLOUD_BACKUP_FILE_AUTO_BRIGHTNESS_STATISTICS_EVENT_ENABLE_TAG:Ljava/lang/String; = "automatic_brightness_statistics_event_enable"

.field private static final CLOUD_BACKUP_FILE_BCBC_TAG:Ljava/lang/String; = "bcbc"

.field private static final CLOUD_BACKUP_FILE_BCBC_TAG_APP:Ljava/lang/String; = "bcbc-app"

.field private static final CLOUD_BACKUP_FILE_BRIGHTNESS_CURVE_OPTIMIZE_POLICY_DISABLE:Ljava/lang/String; = "brightness-curve-optimize-policy-disable"

.field private static final CLOUD_BACKUP_FILE_BRIGHTNESS_CURVE_OPTIMIZE_POLICY_TAG:Ljava/lang/String; = "brightness_curve_optimize_policy_disable"

.field private static final CLOUD_BACKUP_FILE_BRIGHTNESS_STATISTICS_EVENTS_ENABLE:Ljava/lang/String; = "brightness-statistics-events-enable"

.field private static final CLOUD_BACKUP_FILE_BRIGHTNESS_STATISTICS_EVENTS_TAG:Ljava/lang/String; = "brightness_statistics_events"

.field private static final CLOUD_BACKUP_FILE_CATEGORY_TAG_GAME:Ljava/lang/String; = "game-category"

.field private static final CLOUD_BACKUP_FILE_CATEGORY_TAG_IMAGE:Ljava/lang/String; = "image-category"

.field private static final CLOUD_BACKUP_FILE_CATEGORY_TAG_MAP:Ljava/lang/String; = "map-category"

.field private static final CLOUD_BACKUP_FILE_CATEGORY_TAG_READER:Ljava/lang/String; = "reader-category"

.field private static final CLOUD_BACKUP_FILE_CATEGORY_TAG_UNDEFINED:Ljava/lang/String; = "undefined-category"

.field private static final CLOUD_BACKUP_FILE_CATEGORY_TAG_VIDEO:Ljava/lang/String; = "video-category"

.field private static final CLOUD_BACKUP_FILE_CONTACTLESS_GESTURE_COMPONENTS_TAG:Ljava/lang/String; = "gesture_component"

.field private static final CLOUD_BACKUP_FILE_CONTACTLESS_GESTURE_TAG:Ljava/lang/String; = "contactless_gesture"

.field private static final CLOUD_BACKUP_FILE_CUSTOM_CURVE_DISABLE:Ljava/lang/String; = "custom-curve-disable"

.field private static final CLOUD_BACKUP_FILE_CUSTOM_CURVE_TAG:Ljava/lang/String; = "custom_curve"

.field private static final CLOUD_BACKUP_FILE_DISABLE_RESET_SHORT_TERM_MODEL:Ljava/lang/String; = "disable-reset-short-term-model"

.field private static final CLOUD_BACKUP_FILE_DISABLE_RESET_SHORT_TERM_MODEL_TAG:Ljava/lang/String; = "disable_reset_short_term_model"

.field private static final CLOUD_BACKUP_FILE_INDIVIDUAL_MODEL_DISABLE:Ljava/lang/String; = "individual-model-disable"

.field private static final CLOUD_BACKUP_FILE_INDIVIDUAL_MODEL_TAG:Ljava/lang/String; = "individual_model"

.field private static final CLOUD_BACKUP_FILE_MANUAL_BOOST_APP_ENABLE:Ljava/lang/String; = "manual-boost-app-enable"

.field private static final CLOUD_BACKUP_FILE_MANUAL_BOOST_DISABLE_APP_LIST:Ljava/lang/String; = "manual-boost-disable-app-list"

.field private static final CLOUD_BACKUP_FILE_MANUAL_BOOST_DISABLE_APP_TAG:Ljava/lang/String; = "manual_boost_disable_app"

.field private static final CLOUD_BACKUP_FILE_NAME:Ljava/lang/String; = "display_cloud_backup.xml"

.field private static final CLOUD_BACKUP_FILE_OUTDOOR_THERMAL_APP_CATEGORY_TAG:Ljava/lang/String; = "outdoor_thermal_app_category"

.field private static final CLOUD_BACKUP_FILE_OUTDOOR_THERMAL_TAG_APP:Ljava/lang/String; = "outdoor_thermal_app_category_list"

.field private static final CLOUD_BACKUP_FILE_OVERRIDE_BRIGHTNESS_POLICY_ENABLE:Ljava/lang/String; = "override-brightness-policy-enable"

.field private static final CLOUD_BACKUP_FILE_OVERRIDE_BRIGHTNESS_POLICY_TAG:Ljava/lang/String; = "override_brightness_policy_enable"

.field private static final CLOUD_BACKUP_FILE_RESOLUTION_SWITCH_TAG:Ljava/lang/String; = "resolution_switch"

.field private static final CLOUD_BACKUP_FILE_RESOLUTION_SWITCH_TAG_PROCESS_BLACK:Ljava/lang/String; = "resolution_switch_process_black"

.field private static final CLOUD_BACKUP_FILE_RESOLUTION_SWITCH_TAG_PROCESS_PROTECT:Ljava/lang/String; = "resolution_switch_process_protect"

.field private static final CLOUD_BACKUP_FILE_RHYTHMIC_APP_CATEGORY_TAG:Ljava/lang/String; = "rhythmic_app_category"

.field private static final CLOUD_BACKUP_FILE_RHYTHMIC_APP_CATEGORY_TAG_IMAGE:Ljava/lang/String; = "rhythmic_app_category_image"

.field private static final CLOUD_BACKUP_FILE_RHYTHMIC_APP_CATEGORY_TAG_READ:Ljava/lang/String; = "rhythmic_app_category_read"

.field private static final CLOUD_BACKUP_FILE_ROOT_ELEMENT:Ljava/lang/String; = "display-config"

.field private static final CLOUD_BACKUP_FILE_SHORT_TERM_MODEL_ENABLE:Ljava/lang/String; = "short-term-model-enabled"

.field private static final CLOUD_BACKUP_FILE_SHORT_TERM_MODEL_TAG:Ljava/lang/String; = "short-term"

.field private static final CLOUD_BACKUP_FILE_THERMAL_BRIGHTNESS:Ljava/lang/String; = "cloud_thermal_brightness_control.xml"

.field private static final CLOUD_BACKUP_FILE_THRESHOLD_SUNLIGHT_NIT_VALUE:Ljava/lang/String; = "threshold-sunlight-nit-value"

.field private static final CLOUD_BACKUP_FILE_THRESHOLD_SUNLIGHT_NIT_VALUE_TAG:Ljava/lang/String; = "threshold_sunlight_nit_value"

.field private static final CLOUD_BACKUP_FILE_TOUCH_COVER_PROTECTION_GAME_TAG:Ljava/lang/String; = "touch_cover_protection_game"

.field private static final CLOUD_BACKUP_FILE_TOUCH_COVER_PROTECTION_GAME_TAG_APP:Ljava/lang/String; = "touch_cover_protection_game_app"

.field private static final CLOUD_BAKUP_FILE_TEMPERATURE_GAP_TAG:Ljava/lang/String; = "temperature_gap"

.field private static final CLOUD_BAKUP_FILE_TEMPERATURE_GAP_VALUE:Ljava/lang/String; = "temperature-gap-value"

.field public static final CLOUD_EVENTS_APP_CATEGORY:J = 0x8L

.field public static final CLOUD_EVENTS_BRIGHTNESS_STATS:J = 0x1L

.field public static final CLOUD_EVENTS_CUSTOM_CURVE:J = 0x10L

.field public static final CLOUD_EVENTS_INDIVIDUAL_MODEL:J = 0x20L

.field public static final CLOUD_EVENTS_TEMPERATURE_GAP:J = 0x4L

.field public static final CLOUD_EVENTS_THERMAL_CONTROL:J = 0x2L

.field private static final CLOUD_FILE_APP_CATEGORY_CONFIG:Ljava/lang/String; = "cloud_app_brightness_category.xml"

.field private static final CLOUD_THRESHOLD_SUNLIGHT_NIT:Ljava/lang/String; = "threshold_sunlight_nit"

.field private static final CONTACTLESS_GESTURE_COMPONENTS:Ljava/lang/String; = "gestureComponents"

.field private static final CONTACTLESS_GESTURE_MODULE_NAME:Ljava/lang/String; = "ContactlessGestureFeature"

.field private static final CUSTOM_CURVE_DISABLE:Ljava/lang/String; = "custom_curve_disable"

.field private static final CUSTOM_CURVE_DISABLE_MODULE_NAME:Ljava/lang/String; = "CustomCurveDisable"

.field private static final DEBUG:Z

.field private static final DISABLE_RESET_SHORT_TERM_MODEL:Ljava/lang/String; = "disable_reset_short_term_model"

.field private static final DISABLE_RESET_SHORT_TERM_MODEL_MODULE_NAME:Ljava/lang/String; = "DisableResetShortTermModel"

.field private static final INDIVIDUAL_APP_CATEGORY_CONFIG_MODULE_NAME:Ljava/lang/String; = "IndividualAppCategoryConfig"

.field private static final INDIVIDUAL_MODEL_DISABLE:Ljava/lang/String; = "individual_model_disable"

.field private static final INDIVIDUAL_MODEL_DISABLE_MODULE_NAME:Ljava/lang/String; = "IndividualModelDisable"

.field private static final MANUAL_BOOST_APP_ENABLE:Ljava/lang/String; = "manual_boost_app_enable"

.field private static final MANUAL_BOOST_APP_ENABLE_MODULE_NAME:Ljava/lang/String; = "ManualBoostAppEnable"

.field private static final MANUAL_BOOST_DISABLE_APP_LIST:Ljava/lang/String; = "manual_boost_disable_app_list"

.field private static final MANUAL_BOOST_DISABLE_APP_MODULE_NAME:Ljava/lang/String; = "ManualBoostDisableAppList"

.field private static final OUTDOOR_THERMAL_APP_CATEGORY_LIST_BACKUP_FILE:Ljava/lang/String; = "outdoor_thermal_app_category_list_backup.xml"

.field private static final OUTDOOR_THERMAL_APP_LIST:Ljava/lang/String; = "outdoor_thermal_app_list"

.field private static final OUTDOOR_THERMAL_APP_LIST_MODULE_NAME:Ljava/lang/String; = "outdoorThermalAppList"

.field private static final OVERRIDE_BRIGHTNESS_POLICY_ENABLE:Ljava/lang/String; = "override_brightness_policy_enable"

.field private static final OVERRIDE_BRIGHTNESS_POLICY_MODULE_NAME:Ljava/lang/String; = "overrideBrightnessPolicy"

.field private static final PROCESS_RESOLUTION_SWITCH_BLACK_LIST:Ljava/lang/String; = "process_resolution_switch_black_list"

.field private static final PROCESS_RESOLUTION_SWITCH_LIST:Ljava/lang/String; = "process_resolution_switch_list"

.field private static final PROCESS_RESOLUTION_SWITCH_PROTECT_LIST:Ljava/lang/String; = "process_resolution_switch_protect_list"

.field private static final RESOLUTION_SWITCH_PROCESS_LIST_BACKUP_FILE:Ljava/lang/String; = "resolution_switch_process_list_backup.xml"

.field private static final RESOLUTION_SWITCH_PROCESS_LIST_MODEULE_NAME:Ljava/lang/String; = "resolutionSwitchProcessList"

.field private static final RHYTHMIC_APP_CATEGORY_IMAGE_LIST:Ljava/lang/String; = "rhythmic_app_category_image_list"

.field private static final RHYTHMIC_APP_CATEGORY_LIST_BACKUP_FILE:Ljava/lang/String; = "rhythmic_app_category_list_backup.xml"

.field private static final RHYTHMIC_APP_CATEGORY_LIST_MODULE_NAME:Ljava/lang/String; = "rhythmicAppCategoryList"

.field private static final RHYTHMIC_APP_CATEGORY_LIST_NAME:Ljava/lang/String; = "rhythmic_app_category_list"

.field private static final RHYTHMIC_APP_CATEGORY_READ_LIST:Ljava/lang/String; = "rhythmic_app_category_read_list"

.field private static final SHORT_TERM_MODEL_APP_CONFIG:Ljava/lang/String; = "short_term_model_app_config"

.field private static final SHORT_TERM_MODEL_ENABLE:Ljava/lang/String; = "short_term_model_enable"

.field private static final SHORT_TERM_MODEL_GAME_APP_LIST:Ljava/lang/String; = "short_term_model_game_app_list"

.field private static final SHORT_TERM_MODEL_GLOBAL_APP_LIST:Ljava/lang/String; = "short_term_model_global_app_list"

.field private static final SHORT_TERM_MODEL_IMAGE_APP_LIST:Ljava/lang/String; = "short_term_model_image_app_list"

.field private static final SHORT_TERM_MODEL_MAP_APP_LIST:Ljava/lang/String; = "short_term_model_map_app_list"

.field private static final SHORT_TERM_MODEL_MODULE_NAME:Ljava/lang/String; = "shortTermModel"

.field private static final SHORT_TERM_MODEL_READER_APP_LIST:Ljava/lang/String; = "short_term_model_reader_app_list"

.field private static final SHORT_TERM_MODEL_VIDEO_APP_LIST:Ljava/lang/String; = "short_term_model_video_app_list"

.field private static final SHORT_TREM_MODEL_APP_MODULE_NAME:Ljava/lang/String; = "shortTermModelAppList"

.field private static final TAG:Ljava/lang/String; = "MiuiDisplayCloudController"

.field public static final TEMPERATURE_GAP_MODULE_NAME:Ljava/lang/String; = "TemperatureGap"

.field private static final TEMPERATURE_GAP_VALUE:Ljava/lang/String; = "temperature_gap_value"

.field private static final TEMPERATURE_GAP_VALUE_DEFAULT:F = 0.5f

.field private static final THRESHOLD_SUNLIGHT_NIT_DEFAULT:F = 160.0f

.field private static final THRESHOLD_SUNLIGHT_NIT_MODULE_NAME:Ljava/lang/String; = "thresholdSunlightNit"

.field private static final TOUCH_COVER_PROTECTION_GAME_APP_LIST:Ljava/lang/String; = "touch_cover_protection_game_app_list"

.field private static final TOUCH_COVER_PROTECTION_GAME_MODE:Ljava/lang/String; = "TouchCoverProtectionGameMode"


# instance fields
.field private mAppCategoryConfigCloudFile:Landroid/util/AtomicFile;

.field private mAutoBrightnessStatisticsEventEnable:Z

.field private mBCBCAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBrightnessCurveOptimizePolicyDisable:Z

.field private mBrightnessStatsEventsEnable:Z

.field private mCallback:Lcom/android/server/display/MiuiDisplayCloudController$Callback;

.field private mCloudEventsData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCloudEventsSummary:J

.field private mCloudListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCustomCurveDisable:Z

.field private mDisableResetShortTermModel:Z

.field private mFile:Landroid/util/AtomicFile;

.field private mGestureComponents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mIndividualModelDisable:Z

.field private mManualBoostAppEnable:Z

.field private mManualBoostDisableAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mObservers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/display/MiuiDisplayCloudController$Observer;",
            ">;"
        }
    .end annotation
.end field

.field private mOutdoorThermalAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOverrideBrightnessPolicyEnable:Z

.field private mResolutionSwitchProcessBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResolutionSwitchProcessProtectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRhythmicImageAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRhythmicReadAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelAppMapper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mShortTermModelCloudAppCategoryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelEnable:Z

.field private mShortTermModelGameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelGlobalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelImageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelMapList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelReaderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShortTermModelVideoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTemperatureGap:F

.field private mThermalBrightnessCloudFile:Landroid/util/AtomicFile;

.field private mThresholdSunlightNit:F

.field private mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

.field private mTouchCoverProtectionGameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$buBRH27xsKbPmGJbuA3fIB3wMOA(Lcom/android/server/display/MiuiDisplayCloudController;Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/MiuiDisplayCloudController;->lambda$updateDataFromCloudControl$0(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msyncLocalBackupFromCloud(Lcom/android/server/display/MiuiDisplayCloudController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->syncLocalBackupFromCloud()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDataFromCloudControl(Lcom/android/server/display/MiuiDisplayCloudController;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateDataFromCloudControl()Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 62
    const-string v0, "debug.miui.display.cloud.dbg"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    sput-boolean v1, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/android/server/display/MiuiDisplayCloudController$Callback;Landroid/content/Context;)V
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "callback"    # Lcom/android/server/display/MiuiDisplayCloudController$Callback;
    .param p3, "context"    # Landroid/content/Context;

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F

    .line 185
    const/high16 v0, 0x43200000    # 160.0f

    iput v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGameList:Ljava/util/List;

    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelVideoList:Ljava/util/List;

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelMapList:Ljava/util/List;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelImageList:Ljava/util/List;

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelReaderList:Ljava/util/List;

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGlobalList:Ljava/util/List;

    .line 207
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mObservers:Ljava/util/ArrayList;

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudListeners:Ljava/util/List;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    .line 227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    .line 302
    iput-object p3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 303
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mHandler:Landroid/os/Handler;

    .line 304
    iput-object p2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCallback:Lcom/android/server/display/MiuiDisplayCloudController$Callback;

    .line 305
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->initialization()V

    .line 306
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->registerMiuiBrightnessCloudDataObserver()V

    .line 307
    return-void
.end method

.method private getFile(Ljava/lang/String;)Landroid/util/AtomicFile;
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .line 1032
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "displayconfig"

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method private initialization()V
    .locals 4

    .line 315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z

    .line 319
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 323
    .end local v0    # "i":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    const-string/jumbo v1, "short_term_model_game_app_list"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    const-string/jumbo v1, "short_term_model_video_app_list"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    const-string/jumbo v1, "short_term_model_map_app_list"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    const-string/jumbo v1, "short_term_model_image_app_list"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    const-string/jumbo v1, "short_term_model_reader_app_list"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    const-string/jumbo v1, "short_term_model_global_app_list"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    const-string v0, "display_cloud_backup.xml"

    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    .line 332
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadLocalCloudBackup()V

    .line 333
    return-void
.end method

.method private synthetic lambda$updateDataFromCloudControl$0(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V
    .locals 3
    .param p1, "l"    # Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;

    .line 947
    iget-wide v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {p1, v0, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;->onCloudUpdated(JLjava/util/Map;)V

    .line 948
    return-void
.end method

.method private loadLocalCloudBackup()V
    .locals 5

    .line 956
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/util/AtomicFile;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 957
    const/4 v0, 0x0

    .line 959
    .local v0, "inputStream":Ljava/io/FileInputStream;
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v1

    move-object v0, v1

    .line 960
    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->readCloudDataFromXml(Ljava/io/InputStream;)V

    .line 962
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/server/display/MiuiDisplayCloudController;->saveShortTermModelAppComponent(Lorg/json/JSONObject;)V

    .line 963
    invoke-virtual {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyAllObservers()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 968
    nop

    :goto_0
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 969
    goto :goto_1

    .line 968
    :catchall_0
    move-exception v1

    goto :goto_2

    .line 964
    :catch_0
    move-exception v1

    .line 965
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->delete()V

    .line 966
    const-string v2, "MiuiDisplayCloudController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to read local cloud backup"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 968
    nop

    .end local v1    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 970
    .end local v0    # "inputStream":Ljava/io/FileInputStream;
    :goto_1
    goto :goto_3

    .line 968
    .restart local v0    # "inputStream":Ljava/io/FileInputStream;
    :goto_2
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 969
    throw v1

    .line 971
    .end local v0    # "inputStream":Ljava/io/FileInputStream;
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadResolutionSwitchBackUpFileFromXml()V

    .line 972
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadRhythmicAppCategoryBackUpFileFromXml()V

    .line 973
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadOutdoorThermalAppCategoryBackUpFileFromXml()V

    .line 975
    :goto_3
    return-void
.end method

.method private loadOutdoorThermalAppCategoryBackUpFileFromXml()V
    .locals 6

    .line 1010
    const/4 v0, 0x0

    .line 1011
    .local v0, "inputStream":Ljava/io/FileInputStream;
    new-instance v1, Landroid/util/AtomicFile;

    .line 1012
    invoke-static {}, Landroid/os/Environment;->getProductDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "etc/displayconfig"

    const-string v4, "outdoor_thermal_app_category_list_backup.xml"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    .line 1011
    invoke-static {v2, v3}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    .line 1015
    .local v1, "file":Landroid/util/AtomicFile;
    :try_start_0
    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v2

    move-object v0, v2

    .line 1016
    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadOutdoorThermalAppCategoryListFromXml(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1021
    nop

    :goto_0
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 1022
    goto :goto_1

    .line 1021
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 1017
    :catch_0
    move-exception v2

    .line 1018
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v1}, Landroid/util/AtomicFile;->delete()V

    .line 1019
    const-string v3, "MiuiDisplayCloudController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to read local cloud backup"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1021
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 1023
    :goto_1
    return-void

    .line 1021
    :goto_2
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 1022
    throw v2
.end method

.method private loadOutdoorThermalAppCategoryListFromXml(Ljava/io/InputStream;)V
    .locals 6
    .param p1, "stream"    # Ljava/io/InputStream;

    .line 882
    const-string v0, "MiuiDisplayCloudController"

    :try_start_0
    const-string v1, "Start loading app category list from xml."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    invoke-static {p1}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v1

    .line 885
    .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    :cond_0
    :goto_0
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v2

    move v3, v2

    .local v3, "type":I
    const/4 v4, 0x1

    if-eq v2, v4, :cond_3

    .line 886
    const/4 v2, 0x4

    if-eq v3, v2, :cond_0

    const/4 v2, 0x3

    if-ne v3, v2, :cond_1

    .line 887
    goto :goto_0

    .line 889
    :cond_1
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 890
    .local v2, "tag":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_2
    goto :goto_1

    :pswitch_0
    const-string v4, "item"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :goto_1
    const/4 v4, -0x1

    :goto_2
    packed-switch v4, :pswitch_data_1

    goto :goto_3

    .line 892
    :pswitch_1
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 893
    nop

    .line 897
    .end local v2    # "tag":Ljava/lang/String;
    :goto_3
    goto :goto_0

    .line 898
    :cond_3
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyOutdoorThermalAppCategoryListChanged()V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 901
    .end local v1    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v3    # "type":I
    goto :goto_4

    .line 899
    :catch_0
    move-exception v1

    .line 900
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse local cloud backup file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    :pswitch_data_0
    .packed-switch 0x317b13
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private loadResolutionSwitchBackUpFileFromXml()V
    .locals 6

    .line 994
    const/4 v0, 0x0

    .line 995
    .local v0, "inputStream":Ljava/io/FileInputStream;
    new-instance v1, Landroid/util/AtomicFile;

    .line 996
    invoke-static {}, Landroid/os/Environment;->getProductDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "etc/displayconfig"

    const-string v4, "resolution_switch_process_list_backup.xml"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    .line 995
    invoke-static {v2, v3}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    .line 999
    .local v1, "file":Landroid/util/AtomicFile;
    :try_start_0
    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v2

    move-object v0, v2

    .line 1000
    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadResolutionSwitchListFromXml(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    nop

    :goto_0
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 1006
    goto :goto_1

    .line 1005
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 1001
    :catch_0
    move-exception v2

    .line 1002
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v1}, Landroid/util/AtomicFile;->delete()V

    .line 1003
    const-string v3, "MiuiDisplayCloudController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to read local cloud backup"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1005
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 1007
    :goto_1
    return-void

    .line 1005
    :goto_2
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 1006
    throw v2
.end method

.method private loadResolutionSwitchListFromXml(Ljava/io/InputStream;)V
    .locals 8
    .param p1, "stream"    # Ljava/io/InputStream;

    .line 802
    const-string v0, "MiuiDisplayCloudController"

    :try_start_0
    const-string v1, "Start loading resolution switch process list from xml."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-static {p1}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v1

    .line 806
    .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    const/4 v2, 0x0

    .line 807
    .local v2, "currentTag":I
    :cond_0
    :goto_0
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v3

    move v4, v3

    .local v4, "type":I
    const/4 v5, 0x1

    if-eq v3, v5, :cond_5

    .line 808
    const/4 v3, 0x4

    if-eq v4, v3, :cond_0

    const/4 v3, 0x3

    if-ne v4, v3, :cond_1

    .line 809
    goto :goto_0

    .line 811
    :cond_1
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 812
    .local v3, "tag":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/4 v7, 0x2

    sparse-switch v6, :sswitch_data_0

    :cond_2
    goto :goto_1

    :sswitch_0
    const-string v6, "resolution_switch_process_black"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v5

    goto :goto_2

    :sswitch_1
    const-string v6, "item"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v7

    goto :goto_2

    :sswitch_2
    const-string v6, "resolution_switch_process_protect"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    goto :goto_2

    :goto_1
    const/4 v6, -0x1

    :goto_2
    packed-switch v6, :pswitch_data_0

    goto :goto_3

    .line 820
    :pswitch_0
    if-ne v2, v5, :cond_3

    .line 821
    iget-object v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 822
    :cond_3
    if-ne v2, v7, :cond_4

    .line 823
    iget-object v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 817
    :pswitch_1
    const/4 v2, 0x2

    .line 818
    goto :goto_3

    .line 814
    :pswitch_2
    const/4 v2, 0x1

    .line 815
    nop

    .line 829
    .end local v3    # "tag":Ljava/lang/String;
    :cond_4
    :goto_3
    goto :goto_0

    .line 830
    :cond_5
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyResolutionSwitchListChanged()V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 833
    .end local v1    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v2    # "currentTag":I
    .end local v4    # "type":I
    goto :goto_4

    .line 831
    :catch_0
    move-exception v1

    .line 832
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse local cloud backup file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x5da4b919 -> :sswitch_2
        0x317b13 -> :sswitch_1
        0x40eb2617 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private loadRhythmicAppCategoryBackUpFileFromXml()V
    .locals 6

    .line 978
    const/4 v0, 0x0

    .line 979
    .local v0, "inputStream":Ljava/io/FileInputStream;
    new-instance v1, Landroid/util/AtomicFile;

    .line 980
    invoke-static {}, Landroid/os/Environment;->getProductDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "etc/displayconfig"

    const-string v4, "rhythmic_app_category_list_backup.xml"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    .line 979
    invoke-static {v2, v3}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    .line 983
    .local v1, "file":Landroid/util/AtomicFile;
    :try_start_0
    invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v2

    move-object v0, v2

    .line 984
    invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadRhythmicAppCategoryListFromXml(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 989
    nop

    :goto_0
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 990
    goto :goto_1

    .line 989
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 985
    :catch_0
    move-exception v2

    .line 986
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v1}, Landroid/util/AtomicFile;->delete()V

    .line 987
    const-string v3, "MiuiDisplayCloudController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to read local cloud backup"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 989
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 991
    :goto_1
    return-void

    .line 989
    :goto_2
    invoke-static {v0}, Lmiui/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    .line 990
    throw v2
.end method

.method private loadRhythmicAppCategoryListFromXml(Ljava/io/InputStream;)V
    .locals 8
    .param p1, "stream"    # Ljava/io/InputStream;

    .line 842
    const-string v0, "MiuiDisplayCloudController"

    :try_start_0
    const-string v1, "Start loading app category list from xml."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    invoke-static {p1}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v1

    .line 846
    .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    const/4 v2, 0x0

    .line 847
    .local v2, "currentTag":I
    :cond_0
    :goto_0
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v3

    move v4, v3

    .local v4, "type":I
    const/4 v5, 0x1

    if-eq v3, v5, :cond_5

    .line 848
    const/4 v3, 0x4

    if-eq v4, v3, :cond_0

    const/4 v3, 0x3

    if-ne v4, v3, :cond_1

    .line 849
    goto :goto_0

    .line 851
    :cond_1
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 852
    .local v3, "tag":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/4 v7, 0x2

    sparse-switch v6, :sswitch_data_0

    :cond_2
    goto :goto_1

    :sswitch_0
    const-string v6, "rhythmic_app_category_read"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v5

    goto :goto_2

    :sswitch_1
    const-string v6, "item"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v6, v7

    goto :goto_2

    :sswitch_2
    const-string v6, "rhythmic_app_category_image"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    goto :goto_2

    :goto_1
    const/4 v6, -0x1

    :goto_2
    packed-switch v6, :pswitch_data_0

    goto :goto_3

    .line 860
    :pswitch_0
    if-ne v2, v5, :cond_3

    .line 861
    iget-object v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 862
    :cond_3
    if-ne v2, v7, :cond_4

    .line 863
    iget-object v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 857
    :pswitch_1
    const/4 v2, 0x2

    .line 858
    goto :goto_3

    .line 854
    :pswitch_2
    const/4 v2, 0x1

    .line 855
    nop

    .line 869
    .end local v3    # "tag":Ljava/lang/String;
    :cond_4
    :goto_3
    goto :goto_0

    .line 870
    :cond_5
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyRhythmicAppCategoryListChanged()V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 873
    .end local v1    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v2    # "currentTag":I
    .end local v4    # "type":I
    goto :goto_4

    .line 871
    :catch_0
    move-exception v1

    .line 872
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse local cloud backup file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x530f2ad9 -> :sswitch_2
        0x317b13 -> :sswitch_1
        0x3724844a -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private notifyContactlessGestureConfigChanged()V
    .locals 2

    .line 1646
    const-class v0, Lcom/android/server/tof/TofManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/tof/TofManagerInternal;

    iput-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

    .line 1647
    const-string v0, "MiuiDisplayCloudController"

    const-string v1, "notifyContactlessGestureConfigChanged"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1648
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

    if-eqz v0, :cond_0

    .line 1649
    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/android/server/tof/TofManagerInternal;->updateGestureAppConfig(Ljava/util/List;)V

    .line 1651
    :cond_0
    return-void
.end method

.method private notifyOutdoorThermalAppCategoryListChanged()V
    .locals 2

    .line 793
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCallback:Lcom/android/server/display/MiuiDisplayCloudController$Callback;

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/android/server/display/MiuiDisplayCloudController$Callback;->updateOutdoorThermalAppCategoryList(Ljava/util/List;)V

    .line 794
    return-void
.end method

.method private notifyResolutionSwitchListChanged()V
    .locals 3

    .line 1435
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    .line 1436
    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/DisplayManagerServiceStub;->updateResolutionSwitchList(Ljava/util/List;Ljava/util/List;)V

    .line 1438
    return-void
.end method

.method private notifyRhythmicAppCategoryListChanged()V
    .locals 3

    .line 1679
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    .line 1680
    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/DisplayManagerServiceStub;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V

    .line 1681
    return-void
.end method

.method private readCloudDataFromXml(Ljava/io/InputStream;)V
    .locals 11
    .param p1, "stream"    # Ljava/io/InputStream;

    .line 652
    const-string v0, "MiuiDisplayCloudController"

    :try_start_0
    const-string v1, "Start reading cloud data from xml."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    invoke-static {p1}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v1

    .line 655
    .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 656
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 657
    :cond_0
    :goto_0
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v2

    move v3, v2

    .local v3, "type":I
    const/4 v4, 0x1

    if-eq v2, v4, :cond_4

    .line 658
    const/4 v2, 0x3

    if-eq v3, v2, :cond_0

    const/4 v5, 0x4

    if-ne v3, v5, :cond_1

    .line 659
    goto :goto_0

    .line 661
    :cond_1
    invoke-interface {v1}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 662
    .local v6, "tag":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    const/4 v8, 0x0

    sparse-switch v7, :sswitch_data_0

    :cond_2
    goto/16 :goto_1

    :sswitch_0
    const-string v2, "image-category"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v4, v5

    goto/16 :goto_2

    :sswitch_1
    const-string v2, "individual-model-disable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x17

    goto/16 :goto_2

    :sswitch_2
    const-string/jumbo v2, "undefined-category"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v4, 0x6

    goto/16 :goto_2

    :sswitch_3
    const-string v2, "automatic-brightness-statistics-event-enable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0xb

    goto/16 :goto_2

    :sswitch_4
    const-string v2, "disable-reset-short-term-model"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0xc

    goto/16 :goto_2

    :sswitch_5
    const-string v2, "override-brightness-policy-enable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0xa

    goto/16 :goto_2

    :sswitch_6
    const-string v2, "resolution_switch_process_black"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x9

    goto/16 :goto_2

    :sswitch_7
    const-string v2, "rhythmic_app_category_read"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x13

    goto/16 :goto_2

    :sswitch_8
    const-string v2, "reader-category"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v4, 0x5

    goto/16 :goto_2

    :sswitch_9
    const-string v2, "manual-boost-disable-app-list"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x10

    goto/16 :goto_2

    :sswitch_a
    const-string v2, "custom-curve-disable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x18

    goto/16 :goto_2

    :sswitch_b
    const-string v2, "brightness-statistics-events-enable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x14

    goto/16 :goto_2

    :sswitch_c
    const-string/jumbo v2, "temperature-gap-value"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x16

    goto/16 :goto_2

    :sswitch_d
    const-string v2, "bcbc-app"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v4, 0x7

    goto/16 :goto_2

    :sswitch_e
    const-string v2, "brightness-curve-optimize-policy-disable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x19

    goto/16 :goto_2

    :sswitch_f
    const-string/jumbo v2, "touch_cover_protection_game_app"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0xd

    goto/16 :goto_2

    :sswitch_10
    const-string v2, "outdoor_thermal_app_category_list"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x12

    goto/16 :goto_2

    :sswitch_11
    const-string v2, "manual-boost-app-enable"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x11

    goto :goto_2

    :sswitch_12
    const-string/jumbo v2, "threshold-sunlight-nit-value"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x15

    goto :goto_2

    :sswitch_13
    const-string v2, "game-category"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :sswitch_14
    const-string/jumbo v2, "video-category"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    :sswitch_15
    const-string v2, "rhythmic_app_category_image"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0xe

    goto :goto_2

    :sswitch_16
    const-string v2, "gesture_component"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0xf

    goto :goto_2

    :sswitch_17
    const-string v4, "map-category"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v2

    goto :goto_2

    :sswitch_18
    const-string v2, "resolution_switch_process_protect"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v4, 0x8

    goto :goto_2

    :sswitch_19
    const-string/jumbo v2, "short-term-model-enabled"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_2

    move v4, v8

    goto :goto_2

    :goto_1
    const/4 v4, -0x1

    :goto_2
    const-string/jumbo v2, "value"

    const-string v5, "item"

    const-string v7, "enabled"

    const-string v9, "package"

    const/4 v10, 0x0

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_3

    .line 776
    :pswitch_0
    :try_start_1
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z

    .line 778
    goto/16 :goto_3

    .line 769
    :pswitch_1
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z

    .line 771
    if-eqz v2, :cond_3

    .line 772
    iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v7, 0x10

    or-long/2addr v4, v7

    iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    goto/16 :goto_3

    .line 762
    :pswitch_2
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z

    .line 764
    if-eqz v2, :cond_3

    .line 765
    iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v7, 0x20

    or-long/2addr v4, v7

    iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    goto/16 :goto_3

    .line 756
    :pswitch_3
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-interface {v1, v10, v2, v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeFloat(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F

    .line 758
    iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v7, 0x4

    or-long/2addr v4, v7

    iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 759
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    const-string v5, "TemperatureGap"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    goto/16 :goto_3

    .line 751
    :pswitch_4
    const/high16 v4, 0x43200000    # 160.0f

    invoke-interface {v1, v10, v2, v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeFloat(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F

    .line 753
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCallback:Lcom/android/server/display/MiuiDisplayCloudController$Callback;

    invoke-interface {v4, v2}, Lcom/android/server/display/MiuiDisplayCloudController$Callback;->notifyThresholdSunlightNitChanged(F)V

    .line 754
    goto/16 :goto_3

    .line 744
    :pswitch_5
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z

    .line 746
    if-eqz v2, :cond_3

    .line 747
    iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v7, 0x1

    or-long/2addr v4, v7

    iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    goto/16 :goto_3

    .line 740
    :pswitch_6
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 742
    goto/16 :goto_3

    .line 736
    :pswitch_7
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 738
    goto/16 :goto_3

    .line 732
    :pswitch_8
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z

    .line 734
    goto/16 :goto_3

    .line 728
    :pswitch_9
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 730
    goto/16 :goto_3

    .line 724
    :pswitch_a
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 726
    goto :goto_3

    .line 720
    :pswitch_b
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 722
    goto :goto_3

    .line 716
    :pswitch_c
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 718
    goto :goto_3

    .line 712
    :pswitch_d
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z

    .line 714
    goto :goto_3

    .line 708
    :pswitch_e
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z

    .line 710
    goto :goto_3

    .line 704
    :pswitch_f
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z

    .line 706
    goto :goto_3

    .line 700
    :pswitch_10
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 702
    goto :goto_3

    .line 696
    :pswitch_11
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 698
    goto :goto_3

    .line 692
    :pswitch_12
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 694
    goto :goto_3

    .line 688
    :pswitch_13
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGlobalList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 690
    goto :goto_3

    .line 684
    :pswitch_14
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelReaderList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 686
    goto :goto_3

    .line 680
    :pswitch_15
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelImageList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 682
    goto :goto_3

    .line 676
    :pswitch_16
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelMapList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 678
    goto :goto_3

    .line 672
    :pswitch_17
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelVideoList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 674
    goto :goto_3

    .line 668
    :pswitch_18
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGameList:Ljava/util/List;

    invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V

    .line 670
    goto :goto_3

    .line 664
    :pswitch_19
    invoke-interface {v1, v10, v7, v8}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z

    .line 666
    nop

    .line 782
    .end local v6    # "tag":Ljava/lang/String;
    :cond_3
    :goto_3
    goto/16 :goto_0

    .line 783
    :cond_4
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyResolutionSwitchListChanged()V

    .line 784
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyRhythmicAppCategoryListChanged()V

    .line 785
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyOutdoorThermalAppCategoryListChanged()V

    .line 786
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyContactlessGestureConfigChanged()V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 789
    .end local v1    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v3    # "type":I
    goto :goto_4

    .line 787
    :catch_0
    move-exception v1

    .line 788
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse local cloud backup file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6ea54613 -> :sswitch_19
        -0x5da4b919 -> :sswitch_18
        -0x5b891db1 -> :sswitch_17
        -0x5602d279 -> :sswitch_16
        -0x530f2ad9 -> :sswitch_15
        -0x5185f9f0 -> :sswitch_14
        -0x4594bb87 -> :sswitch_13
        -0x3ea26e64 -> :sswitch_12
        -0x3486c9c0 -> :sswitch_11
        -0x29b0eb8a -> :sswitch_10
        -0x2911228e -> :sswitch_f
        -0x1cb94d1d -> :sswitch_e
        -0x19fff5ea -> :sswitch_d
        -0x11b05d5f -> :sswitch_c
        -0x1174fc77 -> :sswitch_b
        -0xa1b552 -> :sswitch_a
        0xdd390e0 -> :sswitch_9
        0x1b3912c8 -> :sswitch_8
        0x3724844a -> :sswitch_7
        0x40eb2617 -> :sswitch_6
        0x466e54e3 -> :sswitch_5
        0x4cf72dbc -> :sswitch_4
        0x6a02e1e6 -> :sswitch_3
        0x6e9b58db -> :sswitch_2
        0x710adc10 -> :sswitch_1
        0x7818ed30 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private registerMiuiBrightnessCloudDataObserver()V
    .locals 4

    .line 339
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 340
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/display/MiuiDisplayCloudController$1;

    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/display/MiuiDisplayCloudController$1;-><init>(Lcom/android/server/display/MiuiDisplayCloudController;Landroid/os/Handler;)V

    .line 339
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 353
    return-void
.end method

.method private saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "parser"    # Lcom/android/modules/utils/TypedXmlPullParser;
    .param p2, "attribute"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/modules/utils/TypedXmlPullParser;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 912
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-interface {p1, v0, p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 913
    invoke-interface {p1, v0, p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 915
    :cond_0
    return-void
.end method

.method private saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1209
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_4

    if-nez p3, :cond_0

    goto :goto_2

    .line 1212
    :cond_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 1213
    .local v0, "appArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_3

    .line 1214
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 1215
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1216
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v2

    .line 1217
    .local v2, "obj":Ljava/lang/Object;
    if-eqz v2, :cond_1

    .line 1218
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1215
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v1    # "i":I
    :cond_2
    goto :goto_1

    .line 1222
    :cond_3
    const-string v1, "MiuiDisplayCloudController"

    const-string v2, "Such category apps are removed."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1224
    :goto_1
    return-void

    .line 1210
    .end local v0    # "appArray":Lorg/json/JSONArray;
    :cond_4
    :goto_2
    return-void
.end method

.method private saveShortTermModelAppComponent(Lorg/json/JSONObject;)V
    .locals 9
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 1163
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelCloudAppCategoryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1164
    .local v1, "str":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x1

    sparse-switch v2, :sswitch_data_0

    :cond_0
    goto :goto_1

    :sswitch_0
    const-string/jumbo v2, "short_term_model_global_app_list"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v4

    goto :goto_2

    :sswitch_1
    const-string/jumbo v2, "short_term_model_image_app_list"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v6

    goto :goto_2

    :sswitch_2
    const-string/jumbo v2, "short_term_model_reader_app_list"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v5

    goto :goto_2

    :sswitch_3
    const-string/jumbo v2, "short_term_model_map_app_list"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v7

    goto :goto_2

    :sswitch_4
    const-string/jumbo v2, "short_term_model_video_app_list"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v8

    goto :goto_2

    :sswitch_5
    const-string/jumbo v2, "short_term_model_game_app_list"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_2

    :goto_1
    const/4 v2, -0x1

    :goto_2
    packed-switch v2, :pswitch_data_0

    goto :goto_3

    .line 1191
    :pswitch_0
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGlobalList:Ljava/util/List;

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1192
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGlobalList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194
    goto :goto_3

    .line 1186
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelReaderList:Ljava/util/List;

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1187
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelReaderList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1189
    goto :goto_3

    .line 1181
    :pswitch_2
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelImageList:Ljava/util/List;

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1182
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelImageList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184
    goto :goto_3

    .line 1176
    :pswitch_3
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelMapList:Ljava/util/List;

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1177
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelMapList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1179
    goto :goto_3

    .line 1171
    :pswitch_4
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelVideoList:Ljava/util/List;

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1172
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelVideoList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1174
    goto :goto_3

    .line 1166
    :pswitch_5
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGameList:Ljava/util/List;

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1167
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGameList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169
    nop

    .line 1198
    .end local v1    # "str":Ljava/lang/String;
    :goto_3
    goto/16 :goto_0

    .line 1199
    :cond_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6ae4e6dd -> :sswitch_5
        -0x64b6c4fa -> :sswitch_4
        -0x6066af3b -> :sswitch_3
        0x39d4f0f2 -> :sswitch_2
        0x64e82226 -> :sswitch_1
        0x78f38212 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private syncLocalBackupFromCloud()V
    .locals 36

    .line 397
    move-object/from16 v8, p0

    const-string v0, "brightness_curve_optimize_policy_disable"

    const-string v9, "custom_curve"

    const-string v10, "individual_model"

    const-string/jumbo v11, "temperature_gap"

    const-string/jumbo v12, "threshold_sunlight_nit_value"

    const-string v13, "brightness_statistics_events"

    const-string v14, "outdoor_thermal_app_category"

    const-string v15, "contactless_gesture"

    const-string v7, "rhythmic_app_category"

    const-string/jumbo v6, "touch_cover_protection_game"

    const-string v5, "disable_reset_short_term_model"

    const-string v4, "automatic_brightness_statistics_event_enable"

    const-string v3, "override_brightness_policy_enable"

    const-string v2, "resolution_switch"

    const-string v1, "bcbc"

    move-object/from16 v16, v0

    const-string/jumbo v0, "short-term"

    move-object/from16 v17, v9

    const-string v9, "display-config"

    move-object/from16 v18, v10

    const-string v10, "MiuiDisplayCloudController"

    move-object/from16 v19, v11

    const-string v11, "manual_boost_disable_app"

    move-object/from16 v20, v1

    iget-object v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    if-nez v1, :cond_0

    .line 398
    return-void

    .line 400
    :cond_0
    const/4 v1, 0x0

    .line 402
    .local v1, "outputStream":Ljava/io/FileOutputStream;
    move-object/from16 v21, v1

    .end local v1    # "outputStream":Ljava/io/FileOutputStream;
    .local v21, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v1, "Start syncing local backup from cloud."

    invoke-static {v10, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    .line 404
    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "outputStream":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-static {v1}, Landroid/util/Xml;->resolveSerializer(Ljava/io/OutputStream;)Lcom/android/modules/utils/TypedXmlSerializer;

    move-result-object v21
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    move-object/from16 v22, v21

    .line 405
    .local v22, "out":Lcom/android/modules/utils/TypedXmlSerializer;
    move-object/from16 v21, v1

    .end local v1    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "outputStream":Ljava/io/FileOutputStream;
    const/4 v1, 0x1

    move-object/from16 v23, v2

    :try_start_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    const/4 v1, 0x0

    move-object/from16 v25, v10

    move-object/from16 v10, v22

    .end local v22    # "out":Lcom/android/modules/utils/TypedXmlSerializer;
    .local v10, "out":Lcom/android/modules/utils/TypedXmlSerializer;
    :try_start_3
    invoke-interface {v10, v1, v2}, Lcom/android/modules/utils/TypedXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 406
    const-string v2, "http://xmlpull.org/v1/doc/features.html#indent-output"

    const/4 v1, 0x1

    invoke-interface {v10, v2, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 407
    const/4 v1, 0x0

    invoke-interface {v10, v1, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 410
    invoke-interface {v10, v1, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 411
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v22, "enabled"

    const-string/jumbo v24, "short-term-model-enabled"

    move-object/from16 v26, v7

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object/from16 v28, v12

    move-object/from16 v27, v21

    move-object v12, v1

    move-object/from16 v35, v20

    move-object/from16 v20, v9

    move-object/from16 v9, v35

    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .local v27, "outputStream":Ljava/io/FileOutputStream;
    move-object/from16 v1, p0

    move-object/from16 v29, v23

    move-object/from16 v30, v3

    move-object/from16 v3, v27

    move-object/from16 v31, v4

    move-object v4, v10

    move-object/from16 v32, v5

    move-object/from16 v5, v22

    move-object/from16 v33, v6

    move-object/from16 v6, v24

    move-object/from16 v34, v26

    :try_start_4
    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 413
    iget-object v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-object/from16 v7, v27

    .end local v27    # "outputStream":Ljava/io/FileOutputStream;
    .local v7, "outputStream":Ljava/io/FileOutputStream;
    :try_start_5
    invoke-direct {v8, v1, v7, v10}, Lcom/android/server/display/MiuiDisplayCloudController;->writeShortTermModelAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;)V

    .line 414
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 417
    invoke-interface {v10, v12, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 418
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    const-string v6, "package"

    const-string v0, "bcbc-app"
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    move-object/from16 v1, p0

    move-object v3, v7

    move-object v4, v10

    move-object/from16 v27, v7

    .end local v7    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v27    # "outputStream":Ljava/io/FileOutputStream;
    move-object v7, v0

    :try_start_6
    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    invoke-interface {v10, v12, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 423
    move-object/from16 v0, v29

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 424
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    const-string v6, "item"

    const-string v7, "resolution_switch_process_protect"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    const-string v6, "item"

    const-string v7, "resolution_switch_process_black"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 440
    move-object/from16 v0, v30

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 441
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "override-brightness-policy-enable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 443
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 446
    move-object/from16 v0, v31

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 447
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "automatic-brightness-statistics-event-enable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 449
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 452
    move-object/from16 v0, v32

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 453
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "disable-reset-short-term-model"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 455
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 458
    move-object/from16 v0, v33

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 459
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    const-string v6, "package"

    const-string/jumbo v7, "touch_cover_protection_game_app"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 464
    move-object/from16 v0, v34

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 465
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    const-string v6, "item"

    const-string v7, "rhythmic_app_category_image"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    const-string v6, "item"

    const-string v7, "rhythmic_app_category_read"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 472
    invoke-interface {v10, v12, v15}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 473
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    const-string v6, "package"

    const-string v7, "gesture_component"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    invoke-interface {v10, v12, v15}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 478
    invoke-interface {v10, v12, v14}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 479
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    const-string v6, "item"

    const-string v7, "outdoor_thermal_app_category_list"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    invoke-interface {v10, v12, v14}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 484
    invoke-interface {v10, v12, v11}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 485
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "manual-boost-app-enable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 487
    invoke-interface {v10, v12, v11}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 490
    invoke-interface {v10, v12, v11}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 491
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    iget-object v5, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    const-string v6, "package"

    const-string v7, "manual-boost-disable-app-list"

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    invoke-interface {v10, v12, v11}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 496
    invoke-interface {v10, v12, v13}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 497
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "brightness-statistics-events-enable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z

    move-object/from16 v1, p0

    move-object/from16 v3, v27

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 499
    iget-object v0, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    move-object/from16 v9, v27

    .end local v27    # "outputStream":Ljava/io/FileOutputStream;
    .local v9, "outputStream":Ljava/io/FileOutputStream;
    :try_start_7
    invoke-direct {v8, v0, v9, v10}, Lcom/android/server/display/MiuiDisplayCloudController;->writeShortTermModelAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;)V

    .line 500
    invoke-interface {v10, v12, v13}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 503
    move-object/from16 v0, v28

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 504
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string/jumbo v5, "value"

    const-string/jumbo v6, "threshold-sunlight-nit-value"

    iget v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F

    .line 505
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    .line 504
    move-object/from16 v1, p0

    move-object v3, v9

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureValueToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Number;)V

    .line 506
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 509
    move-object/from16 v0, v19

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 510
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string/jumbo v5, "value"

    const-string/jumbo v6, "temperature-gap-value"

    iget v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F

    .line 511
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    .line 510
    move-object/from16 v1, p0

    move-object v3, v9

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureValueToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Number;)V

    .line 512
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 515
    move-object/from16 v0, v18

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 516
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "individual-model-disable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z

    move-object/from16 v1, p0

    move-object v3, v9

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 518
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 521
    move-object/from16 v0, v17

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 522
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "custom-curve-disable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z

    move-object/from16 v1, p0

    move-object v3, v9

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 524
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 527
    move-object/from16 v0, v16

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 528
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    const-string v5, "enabled"

    const-string v6, "brightness_curve_optimize_policy_disable"

    iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z

    move-object/from16 v1, p0

    move-object v3, v9

    move-object v4, v10

    invoke-direct/range {v1 .. v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 530
    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 532
    move-object/from16 v0, v20

    invoke-interface {v10, v12, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 533
    invoke-interface {v10}, Lcom/android/modules/utils/TypedXmlSerializer;->endDocument()V

    .line 534
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V

    .line 535
    iget-object v0, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v0, v9}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 539
    .end local v10    # "out":Lcom/android/modules/utils/TypedXmlSerializer;
    move-object v1, v9

    goto :goto_2

    .line 536
    :catch_0
    move-exception v0

    move-object v1, v9

    goto :goto_1

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "outputStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    move-object v9, v7

    move-object v1, v9

    .end local v7    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v27    # "outputStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    move-object/from16 v9, v27

    move-object v1, v9

    .end local v27    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "outputStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v0

    move-object/from16 v25, v10

    :goto_0
    move-object/from16 v9, v21

    move-object v1, v9

    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "outputStream":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v0

    move-object v9, v1

    move-object/from16 v25, v10

    .end local v1    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "outputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v0

    move-object/from16 v25, v10

    move-object/from16 v1, v21

    .line 537
    .end local v21    # "outputStream":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/io/IOException;
    .restart local v1    # "outputStream":Ljava/io/FileOutputStream;
    :goto_1
    iget-object v2, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 538
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to write local backup"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v3, v25

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    return-void
.end method

.method private updateAutoBrightnessStatisticsEventEnableState()Z
    .locals 1

    .line 1098
    sget-boolean v0, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1100
    const/4 v0, 0x0

    return v0

    .line 1102
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateStatisticsAutoBrightnessEventEnable()Z

    move-result v0

    return v0
.end method

.method private updateBCBCAppList()Z
    .locals 6

    .line 1293
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1294
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "BCBCFeature"

    const-string v2, "bcbc_app_config"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1296
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1297
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1298
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 1299
    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1300
    const-string v3, "Update BCBC apps."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1301
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1302
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v3

    .line 1303
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 1304
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1301
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1307
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 1309
    .end local v2    # "appArray":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 1310
    :cond_3
    const-string v2, "Failed to update BCBC apps from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1312
    :goto_1
    return v4
.end method

.method private updateBrightnessCurveOptimizePolicyDisable()Z
    .locals 5

    .line 1867
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1868
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "BrightnessCurveOptimizePolicy"

    const-string v2, "brightness_curve_optimize_policy_disable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1870
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1871
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z

    .line 1873
    const/4 v1, 0x1

    return v1

    .line 1875
    :cond_0
    return v4
.end method

.method private updateBrightnessStatsEventsEnable()Z
    .locals 6

    .line 1465
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1466
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "brightnessStatisticsEvents"

    const-string v2, "brightness_statistics_events_enable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1468
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1469
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z

    .line 1470
    if-eqz v2, :cond_0

    .line 1471
    iget-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v4, 0x1

    or-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 1473
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update brightness statistics events: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1474
    const/4 v1, 0x1

    return v1

    .line 1476
    :cond_1
    const-string v2, "Failed to update brightness statistics events switch from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    return v4
.end method

.method private updateContactlessGestureConfig()Z
    .locals 6

    .line 1622
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1623
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "ContactlessGestureFeature"

    const-string v2, "gestureComponents"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1625
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1626
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1627
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 1628
    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1629
    const-string v3, "Update contactless gesture config."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1630
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1631
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v3

    .line 1632
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 1633
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1630
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1636
    .end local v1    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyContactlessGestureConfigChanged()V

    .line 1637
    const/4 v1, 0x1

    return v1

    .line 1639
    .end local v2    # "appArray":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 1640
    :cond_3
    const-string v2, "Failed to update contactless gesture config."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1642
    :goto_1
    return v4
.end method

.method private updateCustomBrightnessAppConfig()Z
    .locals 21

    .line 1752
    move-object/from16 v1, p0

    const-string v0, "name"

    const-string v2, "MiuiDisplayCloudController"

    iget-object v3, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1753
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "IndividualAppCategoryConfig"

    const-string v5, "app-category-config"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v4, v5, v6, v7}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v3

    .line 1757
    .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1759
    :try_start_0
    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v4

    .line 1760
    .local v4, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 1761
    .local v5, "rootObject":Lorg/json/JSONObject;
    new-instance v6, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;

    invoke-direct {v6}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;-><init>()V

    .line 1762
    .local v6, "config":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;
    invoke-virtual {v6}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;->getCategory()Ljava/util/List;

    move-result-object v8

    .line 1763
    .local v8, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/AppCategory;>;"
    const-string v9, "category"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 1764
    .local v9, "jsonCategoryArray":Lorg/json/JSONArray;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v10, v11, :cond_1

    .line 1765
    new-instance v11, Lcom/android/server/display/aiautobrt/config/AppCategory;

    invoke-direct {v11}, Lcom/android/server/display/aiautobrt/config/AppCategory;-><init>()V

    .line 1766
    .local v11, "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    invoke-virtual {v11}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getPkg()Ljava/util/List;

    move-result-object v12

    .line 1767
    .local v12, "pkgInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/PackageInfo;>;"
    invoke-virtual {v9, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 1768
    .local v13, "categoryItem":Lorg/json/JSONObject;
    const-string v14, "id"

    invoke-virtual {v13, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 1769
    .local v14, "categoryId":I
    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1770
    .local v15, "categoryName":Ljava/lang/String;
    const-string v7, "pkg"

    invoke-virtual {v13, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1771
    .local v7, "pkgArray":Lorg/json/JSONArray;
    const/16 v16, 0x0

    move-object/from16 v17, v3

    move/from16 v3, v16

    .local v3, "j":I
    .local v17, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :goto_1
    move-object/from16 v16, v5

    .end local v5    # "rootObject":Lorg/json/JSONObject;
    .local v16, "rootObject":Lorg/json/JSONObject;
    :try_start_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 1772
    invoke-virtual {v7, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1773
    .local v5, "jsonPkgInfoItem":Lorg/json/JSONObject;
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v19, v18

    .line 1774
    .local v19, "pkgName":Ljava/lang/String;
    new-instance v18, Lcom/android/server/display/aiautobrt/config/PackageInfo;

    invoke-direct/range {v18 .. v18}, Lcom/android/server/display/aiautobrt/config/PackageInfo;-><init>()V

    move-object/from16 v20, v18

    .line 1775
    .local v20, "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
    move-object/from16 v18, v0

    move-object/from16 v0, v20

    .end local v20    # "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
    .local v0, "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
    invoke-virtual {v0, v14}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setCateId(I)V

    .line 1776
    move-object/from16 v20, v5

    move-object/from16 v5, v19

    .end local v19    # "pkgName":Ljava/lang/String;
    .local v5, "pkgName":Ljava/lang/String;
    .local v20, "jsonPkgInfoItem":Lorg/json/JSONObject;
    invoke-virtual {v0, v5}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setName(Ljava/lang/String;)V

    .line 1777
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1771
    nop

    .end local v0    # "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
    .end local v5    # "pkgName":Ljava/lang/String;
    .end local v20    # "jsonPkgInfoItem":Lorg/json/JSONObject;
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v5, v16

    move-object/from16 v0, v18

    goto :goto_1

    :cond_0
    move-object/from16 v18, v0

    .line 1779
    .end local v3    # "j":I
    invoke-virtual {v11, v14}, Lcom/android/server/display/aiautobrt/config/AppCategory;->setId(I)V

    .line 1780
    invoke-virtual {v11, v15}, Lcom/android/server/display/aiautobrt/config/AppCategory;->setName(Ljava/lang/String;)V

    .line 1781
    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1764
    nop

    .end local v7    # "pkgArray":Lorg/json/JSONArray;
    .end local v11    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    .end local v12    # "pkgInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/PackageInfo;>;"
    .end local v13    # "categoryItem":Lorg/json/JSONObject;
    .end local v14    # "categoryId":I
    .end local v15    # "categoryName":Ljava/lang/String;
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v5, v16

    move-object/from16 v3, v17

    move-object/from16 v0, v18

    const/4 v7, 0x0

    goto :goto_0

    .end local v16    # "rootObject":Lorg/json/JSONObject;
    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .local v5, "rootObject":Lorg/json/JSONObject;
    :cond_1
    move-object/from16 v17, v3

    move-object/from16 v16, v5

    .line 1783
    .end local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v5    # "rootObject":Lorg/json/JSONObject;
    .end local v10    # "i":I
    .restart local v16    # "rootObject":Lorg/json/JSONObject;
    .restart local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    sget-boolean v0, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 1784
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update custom app category config json: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1786
    :cond_2
    invoke-direct {v1, v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeAppCategoryConfigToFile(Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;)V

    .line 1787
    iget-wide v10, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v12, 0x8

    or-long/2addr v10, v12

    iput-wide v10, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1788
    const/4 v0, 0x1

    return v0

    .line 1789
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "config":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;
    .end local v8    # "categories":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/AppCategory;>;"
    .end local v9    # "jsonCategoryArray":Lorg/json/JSONArray;
    .end local v16    # "rootObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    goto :goto_2

    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :catch_1
    move-exception v0

    move-object/from16 v17, v3

    .line 1790
    .end local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update  custom app category config exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1791
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 1757
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :cond_3
    move-object/from16 v17, v3

    .line 1794
    .end local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :goto_3
    const/4 v2, 0x0

    return v2
.end method

.method private updateCustomCurveDisable()Z
    .locals 6

    .line 1844
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1845
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "CustomCurveDisable"

    const-string v2, "custom_curve_disable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1848
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1849
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z

    .line 1850
    if-eqz v2, :cond_0

    .line 1851
    iget-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v4, 0x10

    or-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 1853
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update custom curve disable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1854
    const/4 v1, 0x1

    return v1

    .line 1856
    :cond_1
    const-string v2, "Failed to upload custom curve disable from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1857
    return v4
.end method

.method private updateDataFromCloudControl()Z
    .locals 3

    .line 924
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 925
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 926
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateShortTermModelState()Z

    move-result v0

    .line 927
    .local v0, "updated":Z
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateBCBCAppList()Z

    move-result v1

    or-int/2addr v0, v1

    .line 928
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateResolutionSwitchList()Z

    move-result v1

    or-int/2addr v0, v1

    .line 929
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateTouchProtectionGameList()Z

    move-result v1

    or-int/2addr v0, v1

    .line 930
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateManualBoostDisableAppList()Z

    move-result v1

    or-int/2addr v0, v1

    .line 931
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateManualBoostAppEnable()Z

    move-result v1

    or-int/2addr v0, v1

    .line 932
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateOutdoorThermalAppList()Z

    move-result v1

    or-int/2addr v0, v1

    .line 933
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateOverrideBrightnessPolicyEnable()Z

    move-result v1

    or-int/2addr v0, v1

    .line 934
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateAutoBrightnessStatisticsEventEnableState()Z

    move-result v1

    or-int/2addr v0, v1

    .line 935
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateDisableResetShortTermModelState()Z

    move-result v1

    or-int/2addr v0, v1

    .line 936
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateRhythmicAppCategoryList()Z

    move-result v1

    or-int/2addr v0, v1

    .line 937
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateContactlessGestureConfig()Z

    move-result v1

    or-int/2addr v0, v1

    .line 938
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateBrightnessStatsEventsEnable()Z

    move-result v1

    or-int/2addr v0, v1

    .line 939
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateThermalBrightnessConfig()Z

    move-result v1

    or-int/2addr v0, v1

    .line 940
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateThresholdSunlightNitValue()Z

    move-result v1

    or-int/2addr v0, v1

    .line 941
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateTemperatureGap()Z

    move-result v1

    or-int/2addr v0, v1

    .line 942
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateCustomBrightnessAppConfig()Z

    move-result v1

    or-int/2addr v0, v1

    .line 943
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateCustomCurveDisable()Z

    move-result v1

    or-int/2addr v0, v1

    .line 944
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateIndividualModelDisable()Z

    move-result v1

    or-int/2addr v0, v1

    .line 945
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateBrightnessCurveOptimizePolicyDisable()Z

    move-result v1

    or-int/2addr v0, v1

    .line 946
    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudListeners:Ljava/util/List;

    new-instance v2, Lcom/android/server/display/MiuiDisplayCloudController$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/display/MiuiDisplayCloudController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/MiuiDisplayCloudController;)V

    invoke-interface {v1, v2}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 949
    return v0
.end method

.method private updateDisableResetShortTermModel()Z
    .locals 5

    .line 1143
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1144
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "DisableResetShortTermModel"

    const-string v2, "disable_reset_short_term_model"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1147
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1148
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z

    .line 1150
    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCallback:Lcom/android/server/display/MiuiDisplayCloudController$Callback;

    invoke-interface {v2, v1}, Lcom/android/server/display/MiuiDisplayCloudController$Callback;->notifyDisableResetShortTermModel(Z)V

    .line 1151
    const/4 v1, 0x1

    return v1

    .line 1153
    :cond_0
    return v4
.end method

.method private updateDisableResetShortTermModelState()Z
    .locals 1

    .line 1134
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateDisableResetShortTermModel()Z

    move-result v0

    return v0
.end method

.method private updateIndividualModelDisable()Z
    .locals 6

    .line 1883
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1884
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "IndividualModelDisable"

    const-string v2, "individual_model_disable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1887
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1888
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z

    .line 1889
    if-eqz v2, :cond_0

    .line 1890
    iget-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v4, 0x20

    or-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 1892
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update individual model disable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1893
    const/4 v1, 0x1

    return v1

    .line 1895
    :cond_1
    const-string v2, "Failed to upload individual model disable from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1896
    return v4
.end method

.method private updateManualBoostAppEnable()Z
    .locals 5

    .line 1347
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1348
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "ManualBoostAppEnable"

    const-string v2, "manual_boost_app_enable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1350
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1351
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z

    .line 1352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update manual boost app enable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    const/4 v1, 0x1

    return v1

    .line 1355
    :cond_0
    const-string v2, "Failed to update manual boost app enable from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1356
    return v4
.end method

.method private updateManualBoostDisableAppList()Z
    .locals 6

    .line 1385
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1386
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "ManualBoostDisableAppList"

    const-string v2, "manual_boost_disable_app_list"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1388
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1389
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1390
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 1391
    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1392
    const-string v3, "Update manual brightness boost disable apps."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1394
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v3

    .line 1395
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 1396
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1393
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1399
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 1401
    .end local v2    # "appArray":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 1402
    :cond_3
    const-string v2, "Failed to update manual brightness boost disable apps from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1404
    :goto_1
    return v4
.end method

.method private updateOutdoorThermalAppList()Z
    .locals 6

    .line 1361
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1362
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "outdoorThermalAppList"

    const-string v2, "outdoor_thermal_app_list"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1364
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1365
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1366
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 1367
    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1368
    const-string v3, "Update thermal brightness boost enable apps."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1369
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1370
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v3

    .line 1371
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 1372
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1369
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1375
    .end local v1    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyOutdoorThermalAppCategoryListChanged()V

    .line 1376
    const/4 v1, 0x1

    return v1

    .line 1378
    .end local v2    # "appArray":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 1379
    :cond_3
    const-string v2, "Failed to update thermal brightness boost enable apps from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    :goto_1
    return v4
.end method

.method private updateOverrideBrightnessPolicyEnable()Z
    .locals 5

    .line 1446
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1447
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "overrideBrightnessPolicy"

    const-string v2, "override_brightness_policy_enable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1449
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1450
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z

    .line 1451
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update override brightness policy enable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    const/4 v1, 0x1

    return v1

    .line 1454
    :cond_0
    const-string v2, "Failed to update override brightness policy enable from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1455
    return v4
.end method

.method private updateResolutionSwitchList()Z
    .locals 5

    .line 1316
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1317
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "resolutionSwitchProcessList"

    const-string v2, "process_resolution_switch_list"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1320
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1321
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1322
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 1323
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1324
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "Update Resolution switch list from cloud"

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1325
    const-string v1, "process_resolution_switch_protect_list"

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1327
    const-string v1, "process_resolution_switch_black_list"

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1329
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyResolutionSwitchListChanged()V

    .line 1330
    const/4 v1, 0x1

    return v1

    .line 1332
    .end local v2    # "appArray":Lorg/json/JSONArray;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    goto :goto_0

    .line 1333
    :cond_1
    const-string v2, "Failed to update Resolution switch list from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    :goto_0
    return v4
.end method

.method private updateRhythmicAppCategoryList()Z
    .locals 5

    .line 1657
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1658
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "rhythmicAppCategoryList"

    const-string v2, "rhythmic_app_category_list"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1661
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1662
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1663
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 1664
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1665
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "Update app category list from cloud"

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    const-string v1, "rhythmic_app_category_image_list"

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1668
    const-string v1, "rhythmic_app_category_read_list"

    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    .line 1669
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyRhythmicAppCategoryListChanged()V

    .line 1670
    const/4 v1, 0x1

    return v1

    .line 1672
    .end local v2    # "appArray":Lorg/json/JSONArray;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    goto :goto_0

    .line 1673
    :cond_1
    const-string v2, "Failed to update app category list from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1675
    :goto_0
    return v4
.end method

.method private updateShortTermModelAppList()Z
    .locals 5

    .line 1056
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1057
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "shortTermModelAppList"

    const-string/jumbo v2, "short_term_model_app_config"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1059
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1060
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1061
    .local v2, "jsonArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 1062
    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1063
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "Update short term model apps."

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    invoke-direct {p0, v3}, Lcom/android/server/display/MiuiDisplayCloudController;->saveShortTermModelAppComponent(Lorg/json/JSONObject;)V

    .line 1066
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    const/4 v1, 0x1

    return v1

    .line 1068
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    :cond_1
    const-string v2, "Failed to update short term model apps from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    return v4
.end method

.method private updateShortTermModelEnable()Z
    .locals 5

    .line 1079
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1080
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "shortTermModel"

    const-string/jumbo v2, "short_term_model_enable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1082
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1083
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z

    .line 1084
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update short term model enable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    const/4 v1, 0x1

    return v1

    .line 1087
    :cond_0
    const-string v2, "Failed to update short term model enable from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    return v4
.end method

.method private updateShortTermModelState()Z
    .locals 2

    .line 1042
    sget-boolean v0, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1044
    return v1

    .line 1046
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateShortTermModelEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateShortTermModelAppList()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private updateStatisticsAutoBrightnessEventEnable()Z
    .locals 5

    .line 1112
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1113
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "AutomaticBrightnessStatisticsEvent"

    const-string v2, "automatic_brightness_statistics_event_enable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1116
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1117
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z

    .line 1119
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update automatic brightness statistics event enable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    const/4 v1, 0x1

    return v1

    .line 1123
    :cond_0
    const-string v2, "Failed to upload automatic brightness statistics event enable from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    return v4
.end method

.method private updateTemperatureGap()Z
    .locals 9

    .line 1601
    const-string/jumbo v0, "temperature_gap_value"

    const-string v1, "TemperatureGap"

    const-string v2, "MiuiDisplayCloudController"

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1602
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v1, v0, v5, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v4

    .line 1604
    .local v4, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1605
    invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v5

    double-to-float v0, v5

    iput v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F

    .line 1606
    iget-wide v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v7, 0x4

    or-long/2addr v5, v7

    iput-wide v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 1607
    iget-object v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update temperature gap : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1609
    const/4 v0, 0x1

    return v0

    .line 1613
    .end local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :cond_0
    goto :goto_0

    .line 1611
    :catch_0
    move-exception v0

    .line 1612
    .local v0, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update temperature gap exception: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1614
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_0
    return v3
.end method

.method private updateThermalBrightnessConfig()Z
    .locals 22

    .line 1549
    move-object/from16 v1, p0

    const-string/jumbo v0, "thermal-brightness-config"

    const-string v2, "MiuiDisplayCloudController"

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1550
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "temperature_control"

    const/4 v6, 0x0

    invoke-static {v4, v5, v0, v6, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v4

    .line 1553
    .local v4, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1554
    invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    .line 1555
    .local v5, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1556
    .local v0, "rootObject":Lorg/json/JSONObject;
    new-instance v6, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;

    invoke-direct {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;-><init>()V

    .line 1557
    .local v6, "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;->getThermalConditionItem()Ljava/util/List;

    move-result-object v7

    .line 1558
    .local v7, "conditions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;>;"
    const-string/jumbo v8, "thermal-condition-item"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 1559
    .local v8, "conditionArray":Lorg/json/JSONArray;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v9, v10, :cond_2

    .line 1560
    new-instance v10, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-direct {v10}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;-><init>()V

    .line 1561
    .local v10, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 1562
    .local v11, "item":Lorg/json/JSONObject;
    const-string v12, "identifier"

    invoke-virtual {v11, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 1563
    .local v12, "identifier":I
    const-string v13, "description"

    invoke-virtual {v11, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1564
    .local v13, "description":Ljava/lang/String;
    invoke-virtual {v10, v12}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->setIdentifier(I)V

    .line 1565
    invoke-virtual {v10, v13}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->setDescription(Ljava/lang/String;)V

    .line 1567
    invoke-virtual {v10}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v14

    .line 1568
    .local v14, "pairList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    const-string/jumbo v15, "temperature-brightness-pair"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 1569
    .local v15, "pairs":Lorg/json/JSONArray;
    if-eqz v15, :cond_1

    .line 1570
    const/16 v16, 0x0

    move/from16 v3, v16

    .local v3, "j":I
    :goto_1
    move-object/from16 v16, v0

    .end local v0    # "rootObject":Lorg/json/JSONObject;
    .local v16, "rootObject":Lorg/json/JSONObject;
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 1571
    invoke-virtual {v15, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 1572
    .local v0, "pairObj":Lorg/json/JSONObject;
    move-object/from16 v17, v4

    .end local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .local v17, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v4, "min-inclusive"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    .line 1573
    .local v4, "minInclusive":F
    move-object/from16 v18, v8

    .end local v8    # "conditionArray":Lorg/json/JSONArray;
    .local v18, "conditionArray":Lorg/json/JSONArray;
    const-string v8, "max-exclusive"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    .line 1574
    .local v8, "maxExclusive":F
    move-object/from16 v19, v11

    .end local v11    # "item":Lorg/json/JSONObject;
    .local v19, "item":Lorg/json/JSONObject;
    const-string v11, "nit"

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v11

    .line 1576
    .local v11, "nit":F
    new-instance v20, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    invoke-direct/range {v20 .. v20}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;-><init>()V

    move-object/from16 v21, v20

    .line 1577
    .local v21, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    move-object/from16 v20, v0

    move-object/from16 v0, v21

    .end local v21    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .local v0, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .local v20, "pairObj":Lorg/json/JSONObject;
    invoke-virtual {v0, v4}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setMinInclusive(F)V

    .line 1578
    invoke-virtual {v0, v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setMaxExclusive(F)V

    .line 1579
    invoke-virtual {v0, v11}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setNit(F)V

    .line 1580
    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1570
    nop

    .end local v0    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v4    # "minInclusive":F
    .end local v8    # "maxExclusive":F
    .end local v11    # "nit":F
    .end local v20    # "pairObj":Lorg/json/JSONObject;
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v16

    move-object/from16 v4, v17

    move-object/from16 v8, v18

    move-object/from16 v11, v19

    goto :goto_1

    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v18    # "conditionArray":Lorg/json/JSONArray;
    .end local v19    # "item":Lorg/json/JSONObject;
    .local v4, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .local v8, "conditionArray":Lorg/json/JSONArray;
    .local v11, "item":Lorg/json/JSONObject;
    :cond_0
    move-object/from16 v17, v4

    move-object/from16 v18, v8

    move-object/from16 v19, v11

    .end local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v8    # "conditionArray":Lorg/json/JSONArray;
    .end local v11    # "item":Lorg/json/JSONObject;
    .restart local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v18    # "conditionArray":Lorg/json/JSONArray;
    .restart local v19    # "item":Lorg/json/JSONObject;
    goto :goto_2

    .line 1569
    .end local v3    # "j":I
    .end local v16    # "rootObject":Lorg/json/JSONObject;
    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v18    # "conditionArray":Lorg/json/JSONArray;
    .end local v19    # "item":Lorg/json/JSONObject;
    .local v0, "rootObject":Lorg/json/JSONObject;
    .restart local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v8    # "conditionArray":Lorg/json/JSONArray;
    .restart local v11    # "item":Lorg/json/JSONObject;
    :cond_1
    move-object/from16 v16, v0

    move-object/from16 v17, v4

    move-object/from16 v18, v8

    move-object/from16 v19, v11

    .line 1583
    .end local v0    # "rootObject":Lorg/json/JSONObject;
    .end local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v8    # "conditionArray":Lorg/json/JSONArray;
    .end local v11    # "item":Lorg/json/JSONObject;
    .restart local v16    # "rootObject":Lorg/json/JSONObject;
    .restart local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v18    # "conditionArray":Lorg/json/JSONArray;
    .restart local v19    # "item":Lorg/json/JSONObject;
    :goto_2
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1559
    nop

    .end local v10    # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    .end local v12    # "identifier":I
    .end local v13    # "description":Ljava/lang/String;
    .end local v14    # "pairList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    .end local v15    # "pairs":Lorg/json/JSONArray;
    .end local v19    # "item":Lorg/json/JSONObject;
    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, v16

    move-object/from16 v4, v17

    move-object/from16 v8, v18

    const/4 v3, 0x0

    goto/16 :goto_0

    .end local v16    # "rootObject":Lorg/json/JSONObject;
    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v18    # "conditionArray":Lorg/json/JSONArray;
    .restart local v0    # "rootObject":Lorg/json/JSONObject;
    .restart local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v8    # "conditionArray":Lorg/json/JSONArray;
    :cond_2
    move-object/from16 v16, v0

    move-object/from16 v17, v4

    move-object/from16 v18, v8

    .line 1585
    .end local v0    # "rootObject":Lorg/json/JSONObject;
    .end local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v8    # "conditionArray":Lorg/json/JSONArray;
    .end local v9    # "i":I
    .restart local v16    # "rootObject":Lorg/json/JSONObject;
    .restart local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .restart local v18    # "conditionArray":Lorg/json/JSONArray;
    iget-wide v3, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    const-wide/16 v8, 0x2

    or-long/2addr v3, v8

    iput-wide v3, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    .line 1586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update thermal brightness config json: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1587
    invoke-virtual {v1, v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeToFile(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1588
    const/4 v0, 0x1

    return v0

    .line 1553
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
    .end local v7    # "conditions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;>;"
    .end local v16    # "rootObject":Lorg/json/JSONObject;
    .end local v17    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v18    # "conditionArray":Lorg/json/JSONArray;
    .restart local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    :cond_3
    move-object/from16 v17, v4

    .line 1592
    .end local v4    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    goto :goto_3

    .line 1590
    :catch_0
    move-exception v0

    .line 1591
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update thermal brightness config exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1593
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    const/4 v2, 0x0

    return v2
.end method

.method private updateThresholdSunlightNitValue()Z
    .locals 5

    .line 1487
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1488
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "thresholdSunlightNit"

    const-string/jumbo v2, "threshold_sunlight_nit"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1490
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1491
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F

    .line 1492
    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCallback:Lcom/android/server/display/MiuiDisplayCloudController$Callback;

    invoke-interface {v3, v2}, Lcom/android/server/display/MiuiDisplayCloudController$Callback;->notifyThresholdSunlightNitChanged(F)V

    .line 1493
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update threshold of sunlight mode nit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    const/4 v1, 0x1

    return v1

    .line 1496
    :cond_0
    const-string v2, "Failed to update threshold of sunlight mode nit from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    return v4
.end method

.method private updateTouchProtectionGameList()Z
    .locals 6

    .line 1412
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mContext:Landroid/content/Context;

    .line 1413
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "TouchCoverProtectionGameMode"

    const-string/jumbo v2, "touch_cover_protection_game_app_list"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 1415
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiDisplayCloudController"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1416
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1417
    .local v2, "appArray":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 1418
    iget-object v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1419
    const-string v3, "Update game apps."

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1420
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1421
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v3

    .line 1422
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_0

    .line 1423
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    move-object v5, v3

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1420
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1426
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 1428
    .end local v2    # "appArray":Lorg/json/JSONArray;
    :cond_2
    goto :goto_1

    .line 1429
    :cond_3
    const-string v2, "Failed to update game apps from cloud."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    :goto_1
    return v4
.end method

.method private writeAppCategoryConfigToFile(Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;)V
    .locals 18
    .param p1, "config"    # Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;

    .line 1798
    move-object/from16 v1, p0

    const-string v0, "pkg"

    const-string v2, "name"

    const-string v3, "category"

    const-string v4, "app-category-config"

    iget-object v5, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mAppCategoryConfigCloudFile:Landroid/util/AtomicFile;

    if-nez v5, :cond_0

    .line 1799
    const-string v5, "cloud_app_brightness_category.xml"

    invoke-direct {v1, v5}, Lcom/android/server/display/MiuiDisplayCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile;

    move-result-object v5

    iput-object v5, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mAppCategoryConfigCloudFile:Landroid/util/AtomicFile;

    .line 1801
    :cond_0
    const/4 v5, 0x0

    .line 1803
    .local v5, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v6, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mAppCategoryConfigCloudFile:Landroid/util/AtomicFile;

    invoke-virtual {v6}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v6

    move-object v5, v6

    .line 1804
    new-instance v6, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v6}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 1805
    .local v6, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v7, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1806
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v6, v9, v8}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1807
    const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v6, v8, v7}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 1809
    invoke-interface {v6, v9, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1810
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;->getCategory()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/display/aiautobrt/config/AppCategory;

    .line 1811
    .local v8, "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    invoke-interface {v6, v9, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1812
    invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getId()I

    move-result v10

    .line 1813
    .local v10, "id":I
    invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1814
    .local v11, "name":Ljava/lang/String;
    const-string v12, "id"

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v6, v9, v12, v13}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1815
    invoke-interface {v6, v9, v2, v11}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1817
    invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getPkg()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/server/display/aiautobrt/config/PackageInfo;

    .line 1818
    .local v13, "appInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
    invoke-interface {v6, v9, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1819
    invoke-virtual {v13}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->getCateId()I

    move-result v14

    .line 1820
    .local v14, "cateId":I
    invoke-virtual {v13}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->getName()Ljava/lang/String;

    move-result-object v15

    .line 1822
    .local v15, "pkgName":Ljava/lang/String;
    const-string v9, "cateId"

    move-object/from16 v16, v7

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v17, v8

    const/4 v8, 0x0

    .end local v8    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    .local v17, "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    invoke-interface {v6, v8, v9, v7}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1823
    invoke-interface {v6, v8, v2, v15}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1825
    invoke-interface {v6, v8, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1826
    move-object/from16 v7, v16

    move-object/from16 v8, v17

    const/4 v9, 0x0

    .end local v13    # "appInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
    .end local v14    # "cateId":I
    .end local v15    # "pkgName":Ljava/lang/String;
    goto :goto_1

    .line 1827
    .end local v17    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    .restart local v8    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    :cond_1
    move-object/from16 v16, v7

    move-object/from16 v17, v8

    .end local v8    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    .restart local v17    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    const/4 v7, 0x0

    invoke-interface {v6, v7, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1828
    move-object/from16 v7, v16

    const/4 v9, 0x0

    .end local v10    # "id":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v17    # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
    goto :goto_0

    .line 1829
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v6, v0, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1830
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 1831
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    .line 1832
    iget-object v0, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mAppCategoryConfigCloudFile:Landroid/util/AtomicFile;

    invoke-virtual {v0, v5}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1836
    .end local v6    # "out":Lorg/xmlpull/v1/XmlSerializer;
    goto :goto_2

    .line 1833
    :catch_0
    move-exception v0

    .line 1834
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mAppCategoryConfigCloudFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v5}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 1835
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1837
    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    return-void
.end method

.method private writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "writeFile"    # Landroid/util/AtomicFile;
    .param p2, "outStream"    # Ljava/io/FileOutputStream;
    .param p3, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .param p5, "attribute"    # Ljava/lang/String;
    .param p6, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/AtomicFile;",
            "Ljava/io/FileOutputStream;",
            "Lcom/android/modules/utils/TypedXmlSerializer;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 634
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 635
    .local v1, "str":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-interface {p3, v2, p6}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 636
    invoke-interface {p3, v2, p5, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 637
    invoke-interface {p3, v2, p6}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    nop

    .end local v1    # "str":Ljava/lang/String;
    goto :goto_0

    .line 642
    :cond_0
    goto :goto_1

    .line 639
    :catch_0
    move-exception v0

    .line 640
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 641
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write element of app list to xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiDisplayCloudController"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method private writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "writeFile"    # Landroid/util/AtomicFile;
    .param p2, "outStream"    # Ljava/io/FileOutputStream;
    .param p3, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .param p4, "attribute"    # Ljava/lang/String;
    .param p5, "tag"    # Ljava/lang/String;
    .param p6, "enable"    # Z

    .line 556
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 557
    invoke-interface {p3, v0, p4, p6}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Lorg/xmlpull/v1/XmlSerializer;

    .line 558
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    goto :goto_0

    .line 559
    :catch_0
    move-exception v0

    .line 560
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write local backup of feature enable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiDisplayCloudController"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private writeFeatureValueToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Number;)V
    .locals 3
    .param p1, "writeFile"    # Landroid/util/AtomicFile;
    .param p2, "outStream"    # Ljava/io/FileOutputStream;
    .param p3, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .param p4, "attribute"    # Ljava/lang/String;
    .param p5, "tag"    # Ljava/lang/String;
    .param p6, "value"    # Ljava/lang/Number;

    .line 572
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 573
    invoke-virtual {p6}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-interface {p3, v0, p4, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeFloat(Ljava/lang/String;Ljava/lang/String;F)Lorg/xmlpull/v1/XmlSerializer;

    .line 574
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    goto :goto_0

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 577
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write local backup of value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiDisplayCloudController"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private writeShortTermModelAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;)V
    .locals 8
    .param p1, "writeFile"    # Landroid/util/AtomicFile;
    .param p2, "outStream"    # Ljava/io/FileOutputStream;
    .param p3, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;

    .line 588
    const/4 v0, 0x0

    move v7, v0

    .local v7, "category":I
    :goto_0
    const/4 v0, 0x6

    if-ge v7, v0, :cond_0

    .line 589
    packed-switch v7, :pswitch_data_0

    goto/16 :goto_1

    .line 607
    :pswitch_0
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelReaderList:Ljava/util/List;

    const-string v5, "package"

    const-string v6, "reader-category"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    goto :goto_1

    .line 603
    :pswitch_1
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelImageList:Ljava/util/List;

    const-string v5, "package"

    const-string v6, "image-category"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    goto :goto_1

    .line 599
    :pswitch_2
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelMapList:Ljava/util/List;

    const-string v5, "package"

    const-string v6, "map-category"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    goto :goto_1

    .line 595
    :pswitch_3
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelVideoList:Ljava/util/List;

    const-string v5, "package"

    const-string/jumbo v6, "video-category"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    goto :goto_1

    .line 591
    :pswitch_4
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGameList:Ljava/util/List;

    const-string v5, "package"

    const-string v6, "game-category"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    goto :goto_1

    .line 611
    :pswitch_5
    iget-object v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelGlobalList:Ljava/util/List;

    const-string v5, "package"

    const-string/jumbo v6, "undefined-category"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    nop

    .line 588
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 618
    .end local v7    # "category":I
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "out"    # Lorg/xmlpull/v1/XmlSerializer;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1727
    const/4 v0, 0x0

    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1728
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1729
    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1730
    return-void
.end method


# virtual methods
.method protected addCloudListener(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;

    .line 1736
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1737
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1738
    iget-wide v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J

    iget-object v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {p1, v0, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;->onCloudUpdated(JLjava/util/Map;)V

    .line 1740
    :cond_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1264
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1265
    const-string v0, "MiuiDisplayCloudController Configuration:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mShortTermModelEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mShortTermModelAppMapper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBCBCAppList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTouchCoverProtectionGameList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mResolutionSwitchProcessProtectList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mResolutionSwitchProcessBlackList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mRhythmicImageAppList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicImageAppList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mRhythmicReadAppList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mRhythmicReadAppList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mGestureComponents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mGestureComponents:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mOutdoorThermalAppList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOutdoorThermalAppList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mManualBoostAppEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mManualBoostDisableAppList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mOverrideBrightnessPolicyEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAutoBrightnessStatisticsEventEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1280
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDisableResetShortTermModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBrightnessStatsEventsEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mThresholdSunlightNit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCustomCurveDisable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIndividualModelDisable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBrightnessCurveOptimizePolicyDisable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1286
    return-void
.end method

.method protected getBCBCAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1289
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBCBCAppList:Ljava/util/List;

    return-object v0
.end method

.method protected getManualBoostDisableAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1343
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostDisableAppList:Ljava/util/List;

    return-object v0
.end method

.method protected getShortTermModelAppMapper()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1260
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelAppMapper:Ljava/util/Map;

    return-object v0
.end method

.method protected getTouchCoverProtectionGameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1408
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTouchCoverProtectionGameList:Ljava/util/List;

    return-object v0
.end method

.method public isAutoBrightnessStatisticsEventEnable()Z
    .locals 1

    .line 1251
    iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z

    return v0
.end method

.method isBrightnessCurveOptimizePolicyDisable()Z
    .locals 1

    .line 1906
    iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z

    return v0
.end method

.method protected isManualBoostAppEnable()Z
    .locals 1

    .line 1339
    iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z

    return v0
.end method

.method isOverrideBrightnessPolicyEnable()Z
    .locals 1

    .line 1507
    iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z

    return v0
.end method

.method protected isShortTermModelEnable()Z
    .locals 1

    .line 1232
    iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z

    return v0
.end method

.method public notifyAllObservers()V
    .locals 2

    .line 1539
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/display/MiuiDisplayCloudController$Observer;

    .line 1540
    .local v1, "observer":Lcom/android/server/display/MiuiDisplayCloudController$Observer;
    invoke-interface {v1}, Lcom/android/server/display/MiuiDisplayCloudController$Observer;->update()V

    .line 1541
    .end local v1    # "observer":Lcom/android/server/display/MiuiDisplayCloudController$Observer;
    goto :goto_0

    .line 1542
    :cond_0
    return-void
.end method

.method public registerObserver(Lcom/android/server/display/MiuiDisplayCloudController$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/display/MiuiDisplayCloudController$Observer;

    .line 1527
    const-string v0, "observer may not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1528
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1529
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1531
    :cond_0
    return-void
.end method

.method public unregisterObserver(Lcom/android/server/display/MiuiDisplayCloudController$Observer;)V
    .locals 1
    .param p1, "observer"    # Lcom/android/server/display/MiuiDisplayCloudController$Observer;

    .line 1534
    const-string v0, "observer may not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1535
    iget-object v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1536
    return-void
.end method

.method public writeToFile(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;)V
    .locals 17
    .param p1, "config"    # Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;

    .line 1684
    move-object/from16 v1, p0

    const-string/jumbo v0, "temperature-brightness-pair"

    const-string/jumbo v2, "thermal-condition-item"

    const-string/jumbo v3, "thermal-brightness-config"

    iget-object v4, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mThermalBrightnessCloudFile:Landroid/util/AtomicFile;

    if-nez v4, :cond_0

    .line 1685
    const-string v4, "cloud_thermal_brightness_control.xml"

    invoke-direct {v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile;

    move-result-object v4

    iput-object v4, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mThermalBrightnessCloudFile:Landroid/util/AtomicFile;

    .line 1687
    :cond_0
    const/4 v4, 0x0

    .line 1689
    .local v4, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v5, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mThermalBrightnessCloudFile:Landroid/util/AtomicFile;

    invoke-virtual {v5}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v5

    move-object v4, v5

    .line 1690
    new-instance v5, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v5}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 1691
    .local v5, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1692
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v5, v8, v7}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1693
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v5, v7, v6}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 1694
    invoke-interface {v5, v8, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1695
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;->getThermalConditionItem()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 1696
    .local v7, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    invoke-interface {v5, v8, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1697
    invoke-virtual {v7}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v9

    .line 1698
    .local v9, "identifier":I
    invoke-virtual {v7}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;

    move-result-object v10

    .line 1700
    .local v10, "description":Ljava/lang/String;
    const-string v11, "identifier"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-direct {v1, v5, v11, v12}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1701
    const-string v11, "description"

    invoke-direct {v1, v5, v11, v10}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1703
    invoke-virtual {v7}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    .line 1704
    .local v12, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-interface {v5, v8, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1705
    invoke-virtual {v12}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F

    move-result v13

    .line 1706
    .local v13, "minInclusive":F
    invoke-virtual {v12}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F

    move-result v14

    .line 1707
    .local v14, "maxExclusive":F
    invoke-virtual {v12}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F

    move-result v15

    .line 1709
    .local v15, "nit":F
    const-string v8, "min-inclusive"

    move-object/from16 v16, v6

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v1, v5, v8, v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1710
    const-string v6, "max-exclusive"

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-direct {v1, v5, v6, v8}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1711
    const-string v6, "nit"

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-direct {v1, v5, v6, v8}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1712
    const/4 v6, 0x0

    invoke-interface {v5, v6, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1713
    move-object/from16 v6, v16

    const/4 v8, 0x0

    .end local v12    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v13    # "minInclusive":F
    .end local v14    # "maxExclusive":F
    .end local v15    # "nit":F
    goto :goto_1

    .line 1714
    :cond_1
    move-object/from16 v16, v6

    const/4 v6, 0x0

    invoke-interface {v5, v6, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1715
    move-object/from16 v6, v16

    const/4 v8, 0x0

    .end local v7    # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    .end local v9    # "identifier":I
    .end local v10    # "description":Ljava/lang/String;
    goto :goto_0

    .line 1716
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v5, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1717
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 1718
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 1719
    iget-object v0, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mThermalBrightnessCloudFile:Landroid/util/AtomicFile;

    invoke-virtual {v0, v4}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1723
    .end local v5    # "out":Lorg/xmlpull/v1/XmlSerializer;
    goto :goto_2

    .line 1720
    :catch_0
    move-exception v0

    .line 1721
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mThermalBrightnessCloudFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 1722
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1724
    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    return-void
.end method
