.class Lcom/android/server/display/RhythmicEyeCareManager$4;
.super Lcom/android/internal/policy/IKeyguardLockedStateListener$Stub;
.source "RhythmicEyeCareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/RhythmicEyeCareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/RhythmicEyeCareManager;


# direct methods
.method public static synthetic $r8$lambda$HF5eMx2-CxJdcsIMu8fsBL9jhfo(Lcom/android/server/display/RhythmicEyeCareManager$4;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager$4;->lambda$onKeyguardLockedStateChanged$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/RhythmicEyeCareManager;

    .line 96
    iput-object p1, p0, Lcom/android/server/display/RhythmicEyeCareManager$4;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-direct {p0}, Lcom/android/internal/policy/IKeyguardLockedStateListener$Stub;-><init>()V

    return-void
.end method

.method private synthetic lambda$onKeyguardLockedStateChanged$0()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$4;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$mupdateState(Lcom/android/server/display/RhythmicEyeCareManager;Z)V

    return-void
.end method


# virtual methods
.method public onKeyguardLockedStateChanged(Z)V
    .locals 2
    .param p1, "isKeyguardLocked"    # Z

    .line 99
    if-nez p1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$4;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v0}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmHandler(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$4$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$4$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager$4;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 102
    :cond_0
    return-void
.end method
