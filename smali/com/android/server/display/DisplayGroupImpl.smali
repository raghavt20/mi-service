.class public Lcom/android/server/display/DisplayGroupImpl;
.super Lcom/android/server/display/DisplayGroupStub;
.source "DisplayGroupImpl.java"


# instance fields
.field private mAlwaysUnlockedCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/android/server/display/DisplayGroupStub;-><init>()V

    return-void
.end method

.method private hasAlwaysUnLockedFlag(Lcom/android/server/display/LogicalDisplay;)Z
    .locals 2
    .param p1, "display"    # Lcom/android/server/display/LogicalDisplay;

    .line 28
    if-eqz p1, :cond_0

    .line 29
    invoke-virtual {p1}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v0

    iget v0, v0, Landroid/view/DisplayInfo;->flags:I

    .line 30
    .local v0, "flags":I
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_0

    .line 31
    const/4 v1, 0x1

    return v1

    .line 34
    .end local v0    # "flags":I
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public isAlwaysOnLocked()Z
    .locals 1

    .line 24
    iget v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public updateAlwaysUnlockedCountIfNeeded(Lcom/android/server/display/LogicalDisplay;Z)V
    .locals 1
    .param p1, "display"    # Lcom/android/server/display/LogicalDisplay;
    .param p2, "isAdd"    # Z

    .line 13
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayGroupImpl;->hasAlwaysUnLockedFlag(Lcom/android/server/display/LogicalDisplay;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14
    if-eqz p2, :cond_0

    .line 15
    iget v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I

    goto :goto_0

    .line 17
    :cond_0
    iget v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I

    .line 20
    :cond_1
    :goto_0
    return-void
.end method
