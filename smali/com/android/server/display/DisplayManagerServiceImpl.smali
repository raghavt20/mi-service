.class public Lcom/android/server/display/DisplayManagerServiceImpl;
.super Lcom/android/server/display/DisplayManagerServiceStub;
.source "DisplayManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.display.DisplayManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/DisplayManagerServiceImpl$DisplayManagerStubHandler;,
        Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;
    }
.end annotation


# static fields
.field private static final FLAG_INCREASE_SCREEN_BRIGHTNESS:I = 0x0

.field private static final FLAG_UPDATE_DOLBY_PREVIEW_STATE:I = 0x1

.field private static final IS_FOLDABLE_OR_FLIP_DEVICE:Z

.field private static final MSG_RESET_SHORT_MODEL:I = 0xff

.field private static final TAG:Ljava/lang/String; = "DisplayManagerServiceImpl"


# instance fields
.field private mBootCompleted:Z

.field private mClientDeathCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDebug:Z

.field private mDefaultDisplayDevice:Lcom/android/server/display/DisplayDevice;

.field private mDefaultDisplayToken:Landroid/os/IBinder;

.field private mDefaultLocalDisplayDevice:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

.field private mDefaultLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

.field private mDisplayDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/display/DisplayDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayFeatureManagerServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

.field private mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

.field private mHandler:Landroid/os/Handler;

.field private mIsResetRate:Z

.field private mLock:Ljava/lang/Object;

.field private mLogicalDisplayMapper:Lcom/android/server/display/LogicalDisplayMapper;

.field private mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

.field private mOpenedReverseDeviceStates:[I

.field private mOpenedReversePresentationDeviceStates:[I

.field private mResolutionSwitchProcessBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResolutionSwitchProcessProtectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenEffectDisplayIndex:[I

.field private mSecurityManager:Lmiui/security/SecurityManagerInternal;

.field private mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;


# direct methods
.method public static synthetic $r8$lambda$_jhiBHpx9t2fFOrtceWal0RGuCg(Lcom/android/server/display/DisplayManagerServiceImpl;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->lambda$updateResolutionSwitchList$0(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/display/DisplayManagerServiceImpl;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdoDieLocked(Lcom/android/server/display/DisplayManagerServiceImpl;ILandroid/os/IBinder;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->doDieLocked(ILandroid/os/IBinder;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 59
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDeviceInside()Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    sput-boolean v0, Lcom/android/server/display/DisplayManagerServiceImpl;->IS_FOLDABLE_OR_FLIP_DEVICE:Z

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 53
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceStub;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    .line 89
    const/4 v0, 0x0

    filled-new-array {v0}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mScreenEffectDisplayIndex:[I

    return-void
.end method

.method private appRequestChangeSceneRefreshRate(Landroid/os/Parcel;)Z
    .locals 7
    .param p1, "data"    # Landroid/os/Parcel;

    .line 300
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 301
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 302
    .local v0, "callingUid":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, "targetPkgName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 304
    .local v2, "maxRefreshRate":I
    const/16 v3, 0x2710

    if-ge v0, v3, :cond_1

    .line 305
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    .line 307
    .local v3, "token":J
    const/4 v5, -0x1

    if-ne v2, v5, :cond_0

    .line 308
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/RefreshRatePolicyStub;->getInstance()Lcom/android/server/wm/RefreshRatePolicyStub;

    move-result-object v5

    invoke-interface {v5, v1}, Lcom/android/server/wm/RefreshRatePolicyStub;->removeRefreshRateRangeForPackage(Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_0
    invoke-static {}, Lcom/android/server/wm/RefreshRatePolicyStub;->getInstance()Lcom/android/server/wm/RefreshRatePolicyStub;

    move-result-object v5

    int-to-float v6, v2

    invoke-interface {v5, v1, v6}, Lcom/android/server/wm/RefreshRatePolicyStub;->addSceneRefreshRateForPackage(Ljava/lang/String;F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :goto_0
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 315
    nop

    .line 316
    const/4 v5, 0x1

    return v5

    .line 314
    :catchall_0
    move-exception v5

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 315
    throw v5

    .line 318
    .end local v3    # "token":J
    :cond_1
    const/4 v3, 0x0

    return v3
.end method

.method private doDieLocked(ILandroid/os/IBinder;)V
    .locals 3
    .param p1, "flag"    # I
    .param p2, "token"    # Landroid/os/IBinder;

    .line 686
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 687
    invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 688
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v1, :cond_1

    .line 689
    invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGalleryHdrState(Z)V

    goto :goto_0

    .line 691
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 692
    invoke-virtual {p0, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 693
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v1, :cond_1

    .line 694
    const/high16 v2, 0x7fc00000    # Float.NaN

    invoke-virtual {v1, v0, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyPreviewStateLocked(ZF)V

    .line 697
    :cond_1
    :goto_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/display/DisplayManagerServiceImpl;
    .locals 1

    .line 112
    const-class v0, Lcom/android/server/display/DisplayManagerServiceStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl;

    return-object v0
.end method

.method private getScreenEffectDisplayIndexInternal(J)I
    .locals 4
    .param p1, "physicalDisplayId"    # J

    .line 184
    invoke-static {p1, p2}, Lcom/android/server/display/DisplayControl;->getPhysicalDisplayToken(J)Landroid/os/IBinder;

    move-result-object v0

    .line 185
    .local v0, "displayToken":Landroid/os/IBinder;
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 186
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 187
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/display/DisplayDevice;

    invoke-virtual {v3}, Lcom/android/server/display/DisplayDevice;->getDisplayTokenLocked()Landroid/os/IBinder;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 188
    monitor-exit v1

    return v2

    .line 186
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 191
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    .line 192
    const/4 v1, 0x0

    return v1

    .line 191
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private handleGalleryHdrRequest(Landroid/os/Parcel;)Z
    .locals 3
    .param p1, "data"    # Landroid/os/Parcel;

    .line 541
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 542
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 543
    .local v0, "token":Landroid/os/IBinder;
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v1

    .line 544
    .local v1, "increaseScreenBrightness":Z
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->requestGalleryHdrBoost(Landroid/os/IBinder;Z)V

    .line 545
    const/4 v2, 0x1

    return v2
.end method

.method private synthetic lambda$updateResolutionSwitchList$0(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "resolutionSwitchBlackList"    # Ljava/util/List;
    .param p2, "resolutionSwitchProtectList"    # Ljava/util/List;

    .line 406
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 407
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 408
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 409
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 410
    return-void
.end method

.method private requestGalleryHdrBoost(Landroid/os/IBinder;Z)V
    .locals 7
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "enable"    # Z

    .line 549
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 550
    .local v0, "ident":J
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 551
    .local v2, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    .line 553
    .local v3, "callingPid":I
    if-eqz p1, :cond_1

    .line 554
    :try_start_0
    iget-object v4, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 555
    const/4 v5, 0x0

    :try_start_1
    invoke-direct {p0, p1, v5, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    .line 556
    iget-object v5, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v5, :cond_0

    .line 557
    invoke-virtual {v5, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGalleryHdrState(Z)V

    .line 559
    :cond_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 560
    :try_start_2
    const-string v4, "DisplayManagerServiceImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestGalleryHdrBoost: callingUid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", callingPid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", enable: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 559
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "ident":J
    .end local v2    # "callingUid":I
    .end local v3    # "callingPid":I
    .end local p0    # "this":Lcom/android/server/display/DisplayManagerServiceImpl;
    .end local p1    # "token":Landroid/os/IBinder;
    .end local p2    # "enable":Z
    :try_start_4
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 565
    .restart local v0    # "ident":J
    .restart local v2    # "callingUid":I
    .restart local v3    # "callingPid":I
    .restart local p0    # "this":Lcom/android/server/display/DisplayManagerServiceImpl;
    .restart local p1    # "token":Landroid/os/IBinder;
    .restart local p2    # "enable":Z
    :catchall_1
    move-exception v4

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 566
    throw v4

    .line 565
    :cond_1
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 566
    nop

    .line 567
    return-void
.end method

.method private resetAutoBrightnessShortModelInternal(Landroid/os/Handler;)V
    .locals 3
    .param p1, "displayControllerHandler"    # Landroid/os/Handler;

    .line 357
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_1

    .line 360
    const-string v0, "DisplayManagerServiceImpl"

    const-string v1, "reset AutoBrightness ShortModel"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 363
    .local v0, "token":J
    if-eqz p1, :cond_0

    .line 364
    const/16 v2, 0xff

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 367
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 368
    throw v2

    .line 367
    :cond_0
    :goto_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 368
    nop

    .line 369
    return-void

    .line 358
    .end local v0    # "token":J
    :cond_1
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Only system uid can reset Short Model!"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private setBrightnessRate(Landroid/os/Parcel;)Z
    .locals 6
    .param p1, "data"    # Landroid/os/Parcel;

    .line 372
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 373
    .local v0, "uid":I
    const-string v1, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 374
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 376
    .local v1, "token":J
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mIsResetRate:Z

    .line 377
    const-string v3, "DisplayManagerServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setBrightnessRate, uid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mIsResetRate: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mIsResetRate:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 380
    nop

    .line 381
    const/4 v3, 0x1

    return v3

    .line 379
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 380
    throw v3
.end method

.method private setCustomCurveEnabledOnCommand(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .line 825
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 827
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v2, :cond_0

    .line 828
    invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setCustomCurveEnabledOnCommand(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 831
    :cond_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 832
    nop

    .line 833
    const/4 v2, 0x1

    return v2

    .line 831
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 832
    throw v2
.end method

.method private setDeathCallbackLocked(Landroid/os/IBinder;IZ)V
    .locals 0
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I
    .param p3, "enable"    # Z

    .line 570
    if-eqz p3, :cond_0

    .line 571
    invoke-virtual {p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl;->registerDeathCallbackLocked(Landroid/os/IBinder;I)V

    goto :goto_0

    .line 573
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayManagerServiceImpl;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 575
    :goto_0
    return-void
.end method

.method private setForceTrainEnabledOnCommand(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .line 849
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 851
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v2, :cond_0

    .line 852
    invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setForceTrainEnabledOnCommand(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 855
    :cond_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 856
    nop

    .line 857
    const/4 v2, 0x1

    return v2

    .line 855
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 856
    throw v2
.end method

.method private setIndividualModelEnabledOnCommand(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .line 837
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 839
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v2, :cond_0

    .line 840
    invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setIndividualModelEnabledOnCommand(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 843
    :cond_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 844
    nop

    .line 845
    const/4 v2, 0x1

    return v2

    .line 843
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 844
    throw v2
.end method

.method private setVideoInformation(Landroid/os/Parcel;)Z
    .locals 20
    .param p1, "data"    # Landroid/os/Parcel;

    .line 322
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/display/DisplayManagerServiceImpl;->mContext:Landroid/content/Context;

    const-string v2, "com.miui.permission.VIDEO_INFORMATION"

    const-string v3, "Permission required to set video information"

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    move-object/from16 v2, p1

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 326
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v11

    .line 327
    .local v11, "pid":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v12

    .line 328
    .local v12, "bulletChatStatus":Z
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v13

    .line 329
    .local v13, "frameRate":F
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 330
    .local v14, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    .line 331
    .local v15, "height":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readFloat()F

    move-result v16

    .line 332
    .local v16, "compressionRatio":F
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    .line 333
    .local v10, "token":Landroid/os/IBinder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setVideoInformation bulletChatStatus:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",pid:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",token:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "DisplayManagerServiceImpl"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    if-lez v11, :cond_0

    if-eqz v10, :cond_0

    .line 336
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v17

    .line 338
    .local v17, "ident":J
    :try_start_0
    iget-object v3, v1, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayFeatureManagerServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v4, v11

    move v5, v12

    move v6, v13

    move v7, v14

    move v8, v15

    move/from16 v9, v16

    move-object/from16 v19, v10

    .end local v10    # "token":Landroid/os/IBinder;
    .local v19, "token":Landroid/os/IBinder;
    :try_start_1
    invoke-virtual/range {v3 .. v10}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->setVideoInformation(IZFIIFLandroid/os/IBinder;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 342
    nop

    .line 343
    const/4 v0, 0x1

    return v0

    .line 341
    :catchall_0
    move-exception v0

    goto :goto_0

    .end local v19    # "token":Landroid/os/IBinder;
    .restart local v10    # "token":Landroid/os/IBinder;
    :catchall_1
    move-exception v0

    move-object/from16 v19, v10

    .end local v10    # "token":Landroid/os/IBinder;
    .restart local v19    # "token":Landroid/os/IBinder;
    :goto_0
    invoke-static/range {v17 .. v18}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 342
    throw v0

    .line 335
    .end local v17    # "ident":J
    .end local v19    # "token":Landroid/os/IBinder;
    .restart local v10    # "token":Landroid/os/IBinder;
    :cond_0
    move-object/from16 v19, v10

    .line 345
    .end local v10    # "token":Landroid/os/IBinder;
    .restart local v19    # "token":Landroid/os/IBinder;
    const/4 v0, 0x0

    return v0
.end method

.method private showTouchCoverProtectionRect(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .line 861
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 863
    .local v0, "token":J
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/os/Build;->isDebuggable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 864
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {v2, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->showTouchCoverProtectionRect(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 867
    :cond_0
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 868
    nop

    .line 869
    const/4 v2, 0x1

    return v2

    .line 867
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 868
    throw v2
.end method

.method private updateDefaultDisplayLocked()V
    .locals 2

    .line 526
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLogicalDisplayMapper:Lcom/android/server/display/LogicalDisplayMapper;

    if-nez v0, :cond_0

    return-void

    .line 528
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayLocked(I)Lcom/android/server/display/LogicalDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    .line 529
    if-nez v0, :cond_1

    .line 530
    const-string v0, "DisplayManagerServiceImpl"

    const-string v1, "get default display error"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    invoke-virtual {v0}, Lcom/android/server/display/LogicalDisplay;->getPrimaryDisplayDeviceLocked()Lcom/android/server/display/DisplayDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultDisplayDevice:Lcom/android/server/display/DisplayDevice;

    .line 533
    instance-of v1, v0, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    if-eqz v1, :cond_2

    .line 534
    move-object v1, v0

    check-cast v1, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    iput-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLocalDisplayDevice:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    .line 537
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/display/DisplayDevice;->getDisplayTokenLocked()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultDisplayToken:Landroid/os/IBinder;

    .line 538
    return-void
.end method

.method private updateDolbyPreviewState(Landroid/os/Parcel;)Z
    .locals 11
    .param p1, "data"    # Landroid/os/Parcel;

    .line 269
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 272
    .local v0, "token":Landroid/os/IBinder;
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v1

    .line 274
    .local v1, "isDolbyPreviewEnable":Z
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 275
    .local v2, "dolbyPreviewBoostRatio":F
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    .line 276
    .local v3, "ident":J
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 277
    .local v5, "callingUid":I
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    .line 279
    .local v6, "callingPid":I
    const/4 v7, 0x1

    if-eqz v0, :cond_1

    .line 280
    :try_start_0
    iget-object v8, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 282
    :try_start_1
    invoke-direct {p0, v0, v7, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    .line 283
    iget-object v9, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v9, :cond_0

    .line 285
    invoke-virtual {v9, v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateDolbyPreviewStateLocked(ZF)V

    .line 287
    :cond_0
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :try_start_2
    const-string v8, "DisplayManagerServiceImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "updateDolbyPreviewStateLocked: callingUid: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", callingPid: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", isDolbyPreviewEnable: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", dolbyPreviewBoostRatio: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 287
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "token":Landroid/os/IBinder;
    .end local v1    # "isDolbyPreviewEnable":Z
    .end local v2    # "dolbyPreviewBoostRatio":F
    .end local v3    # "ident":J
    .end local v5    # "callingUid":I
    .end local v6    # "callingPid":I
    .end local p0    # "this":Lcom/android/server/display/DisplayManagerServiceImpl;
    .end local p1    # "data":Landroid/os/Parcel;
    :try_start_4
    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 294
    .restart local v0    # "token":Landroid/os/IBinder;
    .restart local v1    # "isDolbyPreviewEnable":Z
    .restart local v2    # "dolbyPreviewBoostRatio":F
    .restart local v3    # "ident":J
    .restart local v5    # "callingUid":I
    .restart local v6    # "callingPid":I
    .restart local p0    # "this":Lcom/android/server/display/DisplayManagerServiceImpl;
    .restart local p1    # "data":Landroid/os/Parcel;
    :catchall_1
    move-exception v7

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 295
    throw v7

    .line 294
    :cond_1
    :goto_0
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 295
    nop

    .line 296
    return v7
.end method

.method private updateScreenEffectDisplayIndexLocked()[I
    .locals 5

    .line 163
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 164
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v1, "displayIndex":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 167
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 169
    .end local v2    # "i":I
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mScreenEffectDisplayIndex:[I

    .line 170
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 171
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mScreenEffectDisplayIndex:[I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v3, v2

    .line 170
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 173
    .end local v2    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mScreenEffectDisplayIndex:[I

    monitor-exit v0

    return-object v2

    .line 174
    .end local v1    # "displayIndex":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public addDisplayGroupManuallyIfNeededLocked(Ljava/util/concurrent/CopyOnWriteArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;",
            ">;)V"
        }
    .end annotation

    .line 967
    .local p1, "mDisplayGroupListeners":Ljava/util/concurrent/CopyOnWriteArrayList;, "Ljava/util/concurrent/CopyOnWriteArrayList<Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;>;"
    const-string/jumbo v0, "star"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 968
    const-string v0, "DisplayManagerServiceImpl"

    const-string v1, "initPowerManagement: Manually set display group"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;

    .line 970
    .local v1, "listener":Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;->onDisplayGroupAdded(I)V

    .line 971
    .end local v1    # "listener":Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;
    goto :goto_0

    .line 973
    :cond_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 727
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 728
    const-string v0, "DisplayManagerServiceImpl Configuration:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 729
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DMS:Z

    iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDebug:Z

    .line 730
    return-void
.end method

.method public finishedGoingToSleep()V
    .locals 1

    .line 767
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    if-nez v0, :cond_0

    goto :goto_0

    .line 770
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->notifyFinishedGoingToSleep()V

    .line 771
    return-void

    .line 768
    :cond_1
    :goto_0
    return-void
.end method

.method public getDefaultSleepFlags()I
    .locals 1

    .line 991
    const/4 v0, 0x0

    return v0
.end method

.method public getDolbyPreviewBoostRatio()F
    .locals 1

    .line 612
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 613
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getDolbyPreviewBoostRatio()F

    move-result v0

    return v0

    .line 615
    :cond_0
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method getDozeBrightnessThreshold(Landroid/os/Parcel;Landroid/os/Parcel;)Z
    .locals 4
    .param p1, "data"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Parcel;

    .line 224
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 225
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 227
    .local v0, "token":J
    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [F

    .line 228
    .local v2, "result":[F
    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v3, :cond_0

    .line 229
    invoke-virtual {v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->getDozeBrightnessThreshold()[F

    move-result-object v3

    move-object v2, v3

    .line 231
    :cond_0
    array-length v3, v2

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 232
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->writeFloatArray([F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    .end local v2    # "result":[F
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 235
    nop

    .line 236
    const/4 v2, 0x1

    return v2

    .line 234
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 235
    throw v2
.end method

.method public getGalleryHdrBoostFactor(FF)F
    .locals 1
    .param p1, "sdrBacklight"    # F
    .param p2, "hdrBacklight"    # F

    .line 662
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 663
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->getGalleryHdrBoostFactor(FF)F

    move-result v0

    return v0

    .line 665
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getIsResetRate()Z
    .locals 1

    .line 386
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mIsResetRate:Z

    return v0
.end method

.method public getLogicalDisplayId(ZLcom/android/server/display/layout/DisplayIdProducer;)I
    .locals 1
    .param p1, "isDefault"    # Z
    .param p2, "idProducer"    # Lcom/android/server/display/layout/DisplayIdProducer;

    .line 996
    sget-boolean v0, Lcom/android/server/display/DisplayManagerServiceImpl;->IS_FOLDABLE_OR_FLIP_DEVICE:Z

    if-eqz v0, :cond_0

    invoke-interface {p2, p1}, Lcom/android/server/display/layout/DisplayIdProducer;->getId(Z)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public getMaskedDensity(Landroid/graphics/Rect;Lcom/android/server/display/DisplayDeviceInfo;)I
    .locals 3
    .param p1, "maskingInsets"    # Landroid/graphics/Rect;
    .param p2, "deviceInfo"    # Lcom/android/server/display/DisplayDeviceInfo;

    .line 953
    iget v0, p2, Lcom/android/server/display/DisplayDeviceInfo;->densityDpi:I

    .line 954
    .local v0, "maskedDensity":I
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->isSupportSetActiveModeSwitchResolution()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 956
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->getDefaultDensity()I

    move-result v1

    .line 957
    .local v1, "density":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 958
    move v0, v1

    .line 961
    .end local v1    # "density":I
    :cond_0
    return v0
.end method

.method public getMaskedHeight(Landroid/graphics/Rect;Lcom/android/server/display/DisplayDeviceInfo;)I
    .locals 4
    .param p1, "maskingInsets"    # Landroid/graphics/Rect;
    .param p2, "deviceInfo"    # Lcom/android/server/display/DisplayDeviceInfo;

    .line 933
    iget v0, p2, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    .line 934
    .local v0, "deviceInfoHeight":I
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->isSupportSetActiveModeSwitchResolution()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    if-ne v1, v2, :cond_0

    .line 936
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->getUserSetResolution()[I

    move-result-object v1

    .line 937
    .local v1, "screenResolution":[I
    if-eqz v1, :cond_0

    .line 938
    aget v0, v1, v2

    .line 943
    .end local v1    # "screenResolution":[I
    :cond_0
    const-string v1, "cetus"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    if-ne v1, v2, :cond_1

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 946
    const/16 v0, 0xa50

    .line 948
    :cond_1
    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int v1, v0, v1

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    return v1
.end method

.method public getMaskedWidth(Landroid/graphics/Rect;Lcom/android/server/display/DisplayDeviceInfo;)I
    .locals 4
    .param p1, "maskingInsets"    # Landroid/graphics/Rect;
    .param p2, "deviceInfo"    # Lcom/android/server/display/DisplayDeviceInfo;

    .line 913
    iget v0, p2, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    .line 914
    .local v0, "deviceInfoWidth":I
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->isSupportSetActiveModeSwitchResolution()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    if-ne v1, v2, :cond_0

    .line 916
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->getUserSetResolution()[I

    move-result-object v1

    .line 917
    .local v1, "screenResolution":[I
    if-eqz v1, :cond_0

    .line 918
    const/4 v3, 0x0

    aget v0, v1, v3

    .line 923
    .end local v1    # "screenResolution":[I
    :cond_0
    const-string v1, "cetus"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->type:I

    if-ne v1, v2, :cond_1

    iget v1, p2, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 926
    const/16 v0, 0x370

    .line 928
    :cond_1
    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int v1, v0, v1

    iget v2, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    return v1
.end method

.method public getMaxHbmBrightnessForPeak()F
    .locals 1

    .line 635
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 636
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxHbmBrightnessForPeak()F

    move-result v0

    return v0

    .line 638
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getMaxManualBrightnessBoost()F
    .locals 1

    .line 649
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 650
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxManualBrightnessBoost()F

    move-result v0

    return v0

    .line 652
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    return v0
.end method

.method getScreenEffectAvailableDisplay(Landroid/os/Parcel;Landroid/os/Parcel;)Z
    .locals 4
    .param p1, "data"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Parcel;

    .line 196
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 199
    .local v0, "token":J
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectAvailableDisplayInternal()[I

    move-result-object v2

    .line 200
    .local v2, "result":[I
    array-length v3, v2

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 201
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->writeIntArray([I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    .end local v2    # "result":[I
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 204
    nop

    .line 205
    const/4 v2, 0x1

    return v2

    .line 203
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 204
    throw v2
.end method

.method public getScreenEffectAvailableDisplayInternal()[I
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mScreenEffectDisplayIndex:[I

    monitor-exit v0

    return-object v1

    .line 180
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getScreenEffectDisplayIndex(Landroid/os/Parcel;Landroid/os/Parcel;)Z
    .locals 4
    .param p1, "data"    # Landroid/os/Parcel;
    .param p2, "reply"    # Landroid/os/Parcel;

    .line 209
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 210
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 212
    .local v0, "token":J
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectDisplayIndexInternal(J)I

    move-result v2

    .line 213
    .local v2, "result":I
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    .end local v2    # "result":I
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 216
    nop

    .line 217
    const/4 v2, 0x1

    return v2

    .line 215
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 216
    throw v2
.end method

.method protected getSyncRoot()Ljava/lang/Object;
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method public init(Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/LogicalDisplayMapper;)V
    .locals 2
    .param p1, "lock"    # Ljava/lang/Object;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "looper"    # Landroid/os/Looper;
    .param p4, "logicalDisplayMapper"    # Lcom/android/server/display/LogicalDisplayMapper;

    .line 98
    iput-object p1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    .line 99
    iput-object p2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 100
    new-instance v0, Lcom/android/server/display/DisplayManagerServiceImpl$DisplayManagerStubHandler;

    invoke-direct {v0, p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl$DisplayManagerStubHandler;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mHandler:Landroid/os/Handler;

    .line 101
    iput-object p4, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLogicalDisplayMapper:Lcom/android/server/display/LogicalDisplayMapper;

    .line 102
    nop

    .line 103
    invoke-static {}, Lcom/android/server/display/DisplayFeatureManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayFeatureManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayFeatureManagerServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    .line 105
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11050082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Lcom/android/server/display/MiuiFoldPolicy;

    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/display/MiuiFoldPolicy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    .line 109
    :cond_0
    return-void
.end method

.method public isDisplayGroupAlwaysOn(I)Z
    .locals 9
    .param p1, "groupId"    # I

    .line 425
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 426
    return v0

    .line 428
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 429
    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLogicalDisplayMapper:Lcom/android/server/display/LogicalDisplayMapper;

    invoke-virtual {v2, p1}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayGroupLocked(I)Lcom/android/server/display/DisplayGroup;

    move-result-object v2

    .line 431
    .local v2, "displayGroup":Lcom/android/server/display/DisplayGroup;
    const/4 v3, 0x0

    .line 432
    .local v3, "alwaysOn":Z
    if-eqz v2, :cond_2

    .line 433
    invoke-virtual {v2}, Lcom/android/server/display/DisplayGroup;->getSizeLocked()I

    move-result v4

    .line 434
    .local v4, "size":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v4, :cond_2

    .line 435
    invoke-virtual {v2, v5}, Lcom/android/server/display/DisplayGroup;->getIdLocked(I)I

    move-result v6

    .line 436
    .local v6, "id":I
    iget-object v7, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLogicalDisplayMapper:Lcom/android/server/display/LogicalDisplayMapper;

    invoke-virtual {v7, v6, v0}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayLocked(IZ)Lcom/android/server/display/LogicalDisplay;

    move-result-object v7

    .line 437
    .local v7, "display":Lcom/android/server/display/LogicalDisplay;
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v8

    iget v8, v8, Landroid/view/DisplayInfo;->flags:I

    and-int/lit16 v8, v8, 0x200

    if-eqz v8, :cond_1

    .line 439
    const/4 v3, 0x1

    .line 440
    goto :goto_1

    .line 434
    .end local v6    # "id":I
    .end local v7    # "display":Lcom/android/server/display/LogicalDisplay;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 444
    .end local v4    # "size":I
    .end local v5    # "i":I
    :cond_2
    :goto_1
    monitor-exit v1

    return v3

    .line 445
    .end local v2    # "displayGroup":Lcom/android/server/display/DisplayGroup;
    .end local v3    # "alwaysOn":Z
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isDolbyPreviewEnable()Z
    .locals 1

    .line 600
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 601
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isDolbyPreviewEnable()Z

    move-result v0

    return v0

    .line 603
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isGalleryHdrEnable()Z
    .locals 1

    .line 621
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 622
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isGalleryHdrEnable()Z

    move-result v0

    return v0

    .line 624
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isInResolutionSwitchBlackList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;

    .line 420
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessBlackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isInResolutionSwitchProtectList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;

    .line 415
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mResolutionSwitchProcessProtectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSupportManualBrightnessBoost()Z
    .locals 1

    .line 642
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 643
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportManualBrightnessBoost()Z

    move-result v0

    return v0

    .line 645
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isSupportPeakBrightness()Z
    .locals 1

    .line 628
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isSupportPeakBrightness()Z

    move-result v0

    return v0

    .line 631
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifyFinishDisplayTransitionLocked()V
    .locals 1

    .line 743
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    if-nez v0, :cond_0

    goto :goto_0

    .line 746
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->dealDisplayTransition()V

    .line 747
    return-void

    .line 744
    :cond_1
    :goto_0
    return-void
.end method

.method public notifyHotplugConnectStateChangedLocked(JZLcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;)V
    .locals 11
    .param p1, "physicalDisplayId"    # J
    .param p3, "isConnected"    # Z
    .param p4, "device"    # Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    .line 793
    const/4 v0, 0x0

    .line 794
    .local v0, "productName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 795
    .local v1, "frameRate":I
    const/4 v2, 0x0

    .line 796
    .local v2, "resolution":Ljava/lang/String;
    if-eqz p4, :cond_1

    if-eqz p3, :cond_1

    .line 797
    invoke-virtual {p4}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    move-result-object v3

    .line 798
    .local v3, "deviceInfo":Lcom/android/server/display/DisplayDeviceInfo;
    iget-object v4, v3, Lcom/android/server/display/DisplayDeviceInfo;->deviceProductInfo:Landroid/hardware/display/DeviceProductInfo;

    if-eqz v4, :cond_0

    .line 799
    iget-object v4, v3, Lcom/android/server/display/DisplayDeviceInfo;->deviceProductInfo:Landroid/hardware/display/DeviceProductInfo;

    invoke-virtual {v4}, Landroid/hardware/display/DeviceProductInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 801
    :cond_0
    iget v4, v3, Lcom/android/server/display/DisplayDeviceInfo;->renderFrameRate:F

    float-to-int v1, v4

    .line 802
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v3, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " x "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 804
    .end local v3    # "deviceInfo":Lcom/android/server/display/DisplayDeviceInfo;
    :cond_1
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v4

    move-wide v5, p1

    move v7, p3

    move-object v8, v0

    move v9, v1

    move-object v10, v2

    invoke-virtual/range {v4 .. v10}, Lcom/android/server/power/PowerManagerServiceStub;->notifyDisplayPortConnectStateChanged(JZLjava/lang/String;ILjava/lang/String;)V

    .line 806
    return-void
.end method

.method public onBootCompleted()V
    .locals 1

    .line 734
    const-class v0, Lcom/android/server/tof/TofManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/tof/TofManagerInternal;

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

    .line 735
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    .line 736
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    if-eqz v0, :cond_0

    .line 737
    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->initMiuiFoldPolicy()V

    .line 739
    :cond_0
    return-void
.end method

.method public onCommand(Ljava/lang/String;)Z
    .locals 3
    .param p1, "cmd"    # Ljava/lang/String;

    .line 874
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "set-individual-model-disable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_1
    const-string/jumbo v0, "touch-cover-protect-show"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_2
    const-string/jumbo v0, "touch-cover-protect-hide"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_3
    const-string/jumbo v0, "set-force-train-enable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_4
    const-string/jumbo v0, "set-custom-curve-enable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :sswitch_5
    const-string/jumbo v0, "set-force-train-disable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string/jumbo v0, "set-individual-model-enable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_7
    const-string/jumbo v0, "set-custom-curve-disable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 892
    return v2

    .line 890
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->showTouchCoverProtectionRect(Z)Z

    move-result v0

    return v0

    .line 888
    :pswitch_1
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->showTouchCoverProtectionRect(Z)Z

    move-result v0

    return v0

    .line 886
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setForceTrainEnabledOnCommand(Z)Z

    move-result v0

    return v0

    .line 884
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setForceTrainEnabledOnCommand(Z)Z

    move-result v0

    return v0

    .line 882
    :pswitch_4
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setIndividualModelEnabledOnCommand(Z)Z

    move-result v0

    return v0

    .line 880
    :pswitch_5
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setIndividualModelEnabledOnCommand(Z)Z

    move-result v0

    return v0

    .line 878
    :pswitch_6
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayManagerServiceImpl;->setCustomCurveEnabledOnCommand(Z)Z

    move-result v0

    return v0

    .line 876
    :pswitch_7
    invoke-direct {p0, v1}, Lcom/android/server/display/DisplayManagerServiceImpl;->setCustomCurveEnabledOnCommand(Z)Z

    move-result v0

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5ae158c7 -> :sswitch_7
        -0x4e160510 -> :sswitch_6
        -0x3798252a -> :sswitch_5
        -0x2a45acee -> :sswitch_4
        -0x2922482b -> :sswitch_3
        -0x1c406adc -> :sswitch_2
        -0x1c3b6d21 -> :sswitch_1
        0x4ee3fb1b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onEarlyInteractivityChange(Z)V
    .locals 1
    .param p1, "interactive"    # Z

    .line 780
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_1

    .line 781
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mTofManagerInternal:Lcom/android/server/tof/TofManagerInternal;

    if-eqz v0, :cond_0

    .line 782
    invoke-virtual {v0, p1}, Lcom/android/server/tof/TofManagerInternal;->onEarlyInteractivityChange(Z)V

    .line 785
    :cond_0
    sget-boolean v0, Lcom/android/server/display/DisplayManagerServiceImpl;->IS_FOLDABLE_OR_FLIP_DEVICE:Z

    if-eqz v0, :cond_1

    .line 786
    invoke-static {}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getInstance()Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->onEarlyInteractivityChange(Z)V

    .line 789
    :cond_1
    return-void
.end method

.method public onHelp(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 899
    invoke-static {}, Landroid/os/Build;->isDebuggable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 900
    return-void

    .line 902
    :cond_0
    const-string v0, "  set-custom-curve-[enable|disable]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 903
    const-string v0, "    Enable or disable custom curve"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 904
    const-string v0, "  set-individual-model-[enable|disable]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 905
    const-string v0, "    Enable or disable individual model"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 906
    const-string v0, "  set-force-train-[enable|disable]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 907
    const-string v0, "    Enable or disable force train"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 908
    const-string v0, "  touch-cover-protect-[show|hide]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 909
    const-string v0, "    Show or hide the touch cover protection rect"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 910
    return-void
.end method

.method public onTransact(Landroid/os/Handler;ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 1
    .param p1, "displayControllerHandler"    # Landroid/os/Handler;
    .param p2, "code"    # I
    .param p3, "data"    # Landroid/os/Parcel;
    .param p4, "reply"    # Landroid/os/Parcel;
    .param p5, "flags"    # I

    .line 241
    const v0, 0xfffffe

    if-ne p2, v0, :cond_0

    .line 242
    invoke-virtual {p0, p1, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->resetAutoBrightnessShortModel(Landroid/os/Handler;Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 243
    :cond_0
    const v0, 0xfffffd

    if-ne p2, v0, :cond_1

    .line 244
    invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->setBrightnessRate(Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 245
    :cond_1
    const v0, 0xfffffc

    if-ne p2, v0, :cond_2

    .line 246
    invoke-virtual {p0, p3, p4}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectAvailableDisplay(Landroid/os/Parcel;Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 247
    :cond_2
    const v0, 0xfffffb

    if-ne p2, v0, :cond_3

    .line 248
    invoke-virtual {p0, p3, p4}, Lcom/android/server/display/DisplayManagerServiceImpl;->getScreenEffectDisplayIndex(Landroid/os/Parcel;Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 249
    :cond_3
    const v0, 0xfffffa

    if-ne p2, v0, :cond_4

    .line 250
    invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->setVideoInformation(Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 251
    :cond_4
    const v0, 0xfffff9

    if-ne p2, v0, :cond_5

    .line 252
    invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->handleGalleryHdrRequest(Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 253
    :cond_5
    const v0, 0xfffff8

    if-ne p2, v0, :cond_6

    .line 254
    invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->appRequestChangeSceneRefreshRate(Landroid/os/Parcel;)Z

    goto :goto_0

    .line 255
    :cond_6
    const v0, 0xfffff7

    if-ne p2, v0, :cond_7

    .line 256
    invoke-virtual {p0, p3, p4}, Lcom/android/server/display/DisplayManagerServiceImpl;->getDozeBrightnessThreshold(Landroid/os/Parcel;Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 257
    :cond_7
    const v0, 0xfffff6

    if-ne p2, v0, :cond_8

    .line 258
    invoke-direct {p0, p3}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateDolbyPreviewState(Landroid/os/Parcel;)Z

    move-result v0

    return v0

    .line 260
    :cond_8
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method protected registerDeathCallbackLocked(Landroid/os/IBinder;I)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I

    .line 578
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Client token "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has already registered."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DisplayManagerServiceImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    return-void

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/IBinder;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    return-void
.end method

.method resetAutoBrightnessShortModel(Landroid/os/Handler;Landroid/os/Parcel;)Z
    .locals 1
    .param p1, "displayControllerHandler"    # Landroid/os/Handler;
    .param p2, "data"    # Landroid/os/Parcel;

    .line 350
    const-string v0, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 351
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayManagerServiceImpl;->resetAutoBrightnessShortModelInternal(Landroid/os/Handler;)V

    .line 352
    const/4 v0, 0x1

    return v0
.end method

.method public screenTurningOff()V
    .locals 1

    .line 759
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    if-nez v0, :cond_0

    goto :goto_0

    .line 762
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurningOff()V

    .line 763
    return-void

    .line 760
    :cond_1
    :goto_0
    return-void
.end method

.method public screenTurningOn()V
    .locals 1

    .line 751
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    if-nez v0, :cond_0

    goto :goto_0

    .line 754
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurningOn()V

    .line 755
    return-void

    .line 752
    :cond_1
    :goto_0
    return-void
.end method

.method public setDeviceStateLocked(I)V
    .locals 1
    .param p1, "state"    # I

    .line 810
    iget-boolean v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mBootCompleted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mMiuiFoldPolicy:Lcom/android/server/display/MiuiFoldPolicy;

    if-nez v0, :cond_0

    goto :goto_0

    .line 813
    :cond_0
    invoke-virtual {v0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->setDeviceStateLocked(I)V

    .line 814
    invoke-static {}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->getInstance()Lcom/android/server/display/statistics/OneTrackFoldStateHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/OneTrackFoldStateHelper;->notifyDeviceStateChanged(I)V

    .line 815
    return-void

    .line 811
    :cond_1
    :goto_0
    return-void
.end method

.method public setSceneMaxRefreshRate(IF)V
    .locals 3
    .param p1, "displayId"    # I
    .param p2, "maxFrameRate"    # F

    .line 450
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 451
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLogicalDisplayMapper:Lcom/android/server/display/LogicalDisplayMapper;

    invoke-virtual {v1, p1}, Lcom/android/server/display/LogicalDisplayMapper;->getDisplayLocked(I)Lcom/android/server/display/LogicalDisplay;

    move-result-object v1

    .line 452
    .local v1, "display":Lcom/android/server/display/LogicalDisplay;
    if-nez v1, :cond_0

    .line 453
    monitor-exit v0

    return-void

    .line 455
    :cond_0
    invoke-static {}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->getInstance()Lcom/android/server/display/mode/DisplayModeDirectorStub;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->setSceneMaxRefreshRate(IF)V

    .line 456
    .end local v1    # "display":Lcom/android/server/display/LogicalDisplay;
    monitor-exit v0

    .line 457
    return-void

    .line 456
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setUpDisplayPowerControllerImpl(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0
    .param p1, "impl"    # Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 119
    iput-object p1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 120
    return-void
.end method

.method public settingBrightnessStartChange(F)V
    .locals 1
    .param p1, "brightness"    # F

    .line 680
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 681
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->settingBrightnessStartChange(F)V

    .line 683
    :cond_0
    return-void
.end method

.method public shouldDeviceKeepWake(I)Z
    .locals 2
    .param p1, "deviceState"    # I

    .line 977
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mOpenedReverseDeviceStates:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mOpenedReversePresentationDeviceStates:[I

    if-nez v0, :cond_1

    .line 978
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mOpenedReverseDeviceStates:[I

    .line 980
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mOpenedReversePresentationDeviceStates:[I

    .line 984
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mOpenedReverseDeviceStates:[I

    .line 985
    invoke-static {v0, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mOpenedReversePresentationDeviceStates:[I

    .line 986
    invoke-static {v0, p1}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    .line 984
    :goto_1
    return v0
.end method

.method public shouldUpdateDisplayModeSpecs(Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;)V
    .locals 6
    .param p1, "modeSpecs"    # Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;

    .line 497
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLocalDisplayDevice:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultDisplayToken:Landroid/os/IBinder;

    if-nez v1, :cond_0

    goto :goto_0

    .line 501
    :cond_0
    invoke-virtual {v0, v1, p1}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->setDesiredDisplayModeSpecsAsync(Landroid/os/IBinder;Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;)V

    .line 503
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 504
    :try_start_0
    new-instance v1, Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;

    iget v2, p1, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;->defaultMode:I

    iget-boolean v3, p1, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;->allowGroupSwitching:Z

    iget-object v4, p1, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;->primaryRanges:Landroid/view/SurfaceControl$RefreshRateRanges;

    iget-object v5, p1, Landroid/view/SurfaceControl$DesiredDisplayModeSpecs;->appRequestRanges:Landroid/view/SurfaceControl$RefreshRateRanges;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;-><init>(IZLandroid/view/SurfaceControl$RefreshRateRanges;Landroid/view/SurfaceControl$RefreshRateRanges;)V

    .line 511
    .local v1, "desired":Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLocalDisplayDevice:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    invoke-virtual {v2, v1}, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;->updateDesiredDisplayModeSpecsLocked(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)V

    .line 514
    iget-object v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    invoke-virtual {v2, v1}, Lcom/android/server/display/LogicalDisplay;->setDesiredDisplayModeSpecsLocked(Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)V

    .line 517
    invoke-static {}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->getInstance()Lcom/android/server/display/mode/DisplayModeDirectorStub;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    .line 518
    invoke-virtual {v3}, Lcom/android/server/display/LogicalDisplay;->getDisplayIdLocked()I

    move-result v3

    .line 517
    const/4 v4, 0x0

    invoke-interface {v2, v3, v1, v4}, Lcom/android/server/display/mode/DisplayModeDirectorStub;->onDesiredDisplayModeSpecsChanged(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)V

    .line 519
    .end local v1    # "desired":Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;
    monitor-exit v0

    .line 520
    return-void

    .line 519
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 498
    :cond_1
    :goto_0
    return-void
.end method

.method public startCbmStatsJob()V
    .locals 1

    .line 819
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 820
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->startCbmStatsJob()V

    .line 822
    :cond_0
    return-void
.end method

.method public temporaryBrightnessStartChange(F)V
    .locals 1
    .param p1, "brightness"    # F

    .line 673
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 674
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->temporaryBrightnessStartChange(F)V

    .line 676
    :cond_0
    return-void
.end method

.method protected unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;

    .line 586
    if-eqz p1, :cond_0

    .line 587
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;

    .line 588
    .local v0, "deathCallback":Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;
    if-eqz v0, :cond_0

    .line 589
    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 592
    .end local v0    # "deathCallback":Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;
    :cond_0
    return-void
.end method

.method public updateDefaultDisplaySupportMode()Landroid/view/SurfaceControl$DynamicDisplayInfo;
    .locals 5

    .line 477
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 478
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateDefaultDisplayLocked()V

    .line 479
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLocalDisplayDevice:Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    if-nez v1, :cond_0

    .line 480
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    .line 482
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDefaultLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    invoke-virtual {v1}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/view/DisplayInfo;->address:Landroid/view/DisplayAddress;

    .line 483
    .local v1, "address":Landroid/view/DisplayAddress;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    move-object v0, v1

    check-cast v0, Landroid/view/DisplayAddress$Physical;

    .line 485
    .local v0, "physicalAddress":Landroid/view/DisplayAddress$Physical;
    invoke-virtual {v0}, Landroid/view/DisplayAddress$Physical;->getPhysicalDisplayId()J

    move-result-wide v2

    .line 486
    .local v2, "physicalDisplayId":J
    invoke-static {v2, v3}, Landroid/view/SurfaceControl;->getDynamicDisplayInfo(J)Landroid/view/SurfaceControl$DynamicDisplayInfo;

    move-result-object v4

    return-object v4

    .line 483
    .end local v0    # "physicalAddress":Landroid/view/DisplayAddress$Physical;
    .end local v1    # "address":Landroid/view/DisplayAddress;
    .end local v2    # "physicalDisplayId":J
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public updateDeviceDisplayChanged(Lcom/android/server/display/DisplayDevice;I)V
    .locals 5
    .param p1, "device"    # Lcom/android/server/display/DisplayDevice;
    .param p2, "event"    # I

    .line 124
    instance-of v0, p1, Lcom/android/server/display/LocalDisplayAdapter$LocalDisplayDevice;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 126
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 134
    :pswitch_1
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 136
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateScreenEffectDisplayIndexLocked()[I

    goto :goto_0

    .line 128
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayDevices:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-direct {p0}, Lcom/android/server/display/DisplayManagerServiceImpl;->updateScreenEffectDisplayIndexLocked()[I

    .line 142
    :cond_0
    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 143
    :cond_1
    instance-of v0, p1, Lcom/android/server/display/VirtualDisplayAdapter$VirtualDisplayDevice;

    if-eqz v0, :cond_4

    .line 144
    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    move-result-object v0

    .line 145
    .local v0, "info":Lcom/android/server/display/DisplayDeviceInfo;
    if-eqz v0, :cond_4

    .line 146
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mSecurityManager:Lmiui/security/SecurityManagerInternal;

    if-nez v1, :cond_2

    .line 147
    const-class v1, Lmiui/security/SecurityManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/security/SecurityManagerInternal;

    iput-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mSecurityManager:Lmiui/security/SecurityManagerInternal;

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mSecurityManager:Lmiui/security/SecurityManagerInternal;

    if-eqz v1, :cond_3

    .line 150
    iget-object v2, v0, Lcom/android/server/display/DisplayDeviceInfo;->ownerPackageName:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/server/display/DisplayDeviceInfo;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayTokenLocked()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, p2}, Lmiui/security/SecurityManagerInternal;->onDisplayDeviceEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;I)V

    .line 152
    :cond_3
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/wm/WindowManagerServiceStub;->updateScreenShareProjectFlag()V

    .line 155
    .end local v0    # "info":Lcom/android/server/display/DisplayDeviceInfo;
    :cond_4
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateResolutionSwitchList(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 405
    .local p1, "resolutionSwitchProtectList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "resolutionSwitchBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/DisplayManagerServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/display/DisplayManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 411
    return-void
.end method

.method public updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 399
    .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl;->mDisplayFeatureManagerServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V

    .line 400
    return-void
.end method
