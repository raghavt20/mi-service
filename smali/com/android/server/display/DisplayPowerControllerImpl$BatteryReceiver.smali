.class final Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DisplayPowerControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayPowerControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayPowerControllerImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0

    .line 2175
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;-><init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 2179
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$BatteryReceiver;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    const-string v1, "level"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateBatteryBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;I)V

    .line 2180
    return-void
.end method
