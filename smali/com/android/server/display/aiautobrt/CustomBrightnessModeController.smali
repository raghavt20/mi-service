.class public Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;
.super Ljava/lang/Object;
.source "CustomBrightnessModeController.java"

# interfaces
.implements Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;
.implements Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;
.implements Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$EngineCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;,
        Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;
    }
.end annotation


# static fields
.field private static final BRIGHT_ROOM_AMBIENT_LUX:F = 600.0f

.field public static final CUSTOM_BRIGHTNESS_CURVE_BRIGHTENING:Ljava/lang/String; = "custom_brightness_curve_brightening"

.field public static final CUSTOM_BRIGHTNESS_CURVE_DARKENING:Ljava/lang/String; = "custom_brightness_curve_darkening"

.field public static final CUSTOM_BRIGHTNESS_CURVE_DEFAULT:Ljava/lang/String; = "custom_brightness_curve_default"

.field public static final CUSTOM_BRIGHTNESS_MODE_CUSTOM:I = 0x1

.field public static final CUSTOM_BRIGHTNESS_MODE_INDIVIDUAL:I = 0x2

.field private static final CUSTOM_BRIGHTNESS_MODE_INVALID:I = -0x1

.field public static final CUSTOM_BRIGHTNESS_MODE_OFF:I = 0x0

.field protected static final CUSTOM_CURVE_STATE_REASON_BACKUP:Ljava/lang/String; = "backup"

.field protected static final CUSTOM_CURVE_STATE_REASON_BEST_INDICATOR:Ljava/lang/String; = "best_indicator"

.field protected static final CUSTOM_CURVE_STATE_REASON_DEFAULT:Ljava/lang/String; = "default"

.field protected static final CUSTOM_CURVE_STATE_REASON_FORCED:Ljava/lang/String; = "forced_operate"

.field protected static final CUSTOM_CURVE_STATE_REASON_USER:Ljava/lang/String; = "user_operate"

.field private static final DARK_ROOM_AMBIENT_LUX:F = 100.0f

.field private static final EXPERIMENT_FLAG_CUSTOM_CURVE:I = 0x1

.field private static final EXPERIMENT_FLAG_INDIVIDUAL_MODEL:I = 0x2

.field private static final EXPERIMENT_FLAG_INVALID:I = -0x1

.field private static final FORCE_TRAIN_DISABLE_VALUE:I = 0x0

.field private static final FORCE_TRAIN_ENABLE_VALUE:I = 0x1

.field private static final FORMAT:Ljava/text/SimpleDateFormat;

.field private static final KEY_CUSTOM_BRIGHTNESS_MODE:Ljava/lang/String; = "custom_brightness_mode"

.field private static final KEY_FORCE_TRAIN_ENABLE:Ljava/lang/String; = "force_train_enable"

.field private static final LUX_GRAD_SMOOTHING:F = 0.25f

.field public static final MAX_CUSTOM_CURVE_VALID_ADJ_TIMES:I = 0x5

.field private static final MAX_GRAD:F = 1.0f

.field private static final MIN_PERMISSIBLE_INCREASE:F = 0.004f

.field private static final MIN_SCOPE_DELTA_NIT:F = 100.0f

.field private static final MSG_CLEAR_PREDICT_PENDING:I = 0x1

.field public static final SCENE_STATE_BRIGHTENING:I = 0x1

.field public static final SCENE_STATE_DARKENING:I = 0x2

.field public static final SCENE_STATE_DEFAULT:I = 0x0

.field public static final SCENE_STATE_INVALID:I = -0x1

.field private static final SHORT_TERM_MODEL_THRESHOLD_RATIO:F = 0.6f

.field protected static final TAG:Ljava/lang/String; = "CbmController"

.field private static final WAIT_FOR_PREDICT_TIMEOUT:I = 0xc8

.field protected static sDebug:Z


# instance fields
.field private final mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

.field private mAutoBrightnessModeEnabled:Z

.field private final mBgHandler:Landroid/os/Handler;

.field private mBrighteningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

.field private final mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

.field private mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

.field private final mBrightnessValidationMapper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "[F>;"
        }
    .end annotation
.end field

.field private mCachedCategoryId:I

.field private mCachedLuxIndex:I

.field private mCbmState:I

.field private final mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentCustomSceneState:I

.field private mCustomCurveCloudDisable:Z

.field private mCustomCurveDisabledByUserChange:Z

.field private volatile mCustomCurveExperimentEnable:Z

.field private mCustomCurveValid:Z

.field private mCustomCurveValidStateReason:Ljava/lang/String;

.field private mDarkeningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

.field private final mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

.field private mDefaultConfiguration:Landroid/hardware/display/BrightnessConfiguration;

.field private final mDefaultLuxLevels:[F

.field private final mDefaultNitsLevels:[F

.field private final mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

.field private final mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

.field private mForcedCustomCurveEnabled:Z

.field private mForcedIndividualBrightnessEnabled:Z

.field private final mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

.field private final mHbmMinimumLux:F

.field private mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

.field private final mIndividualComponentName:Landroid/content/ComponentName;

.field private mIndividualDefaultSpline:Landroid/util/Spline;

.field private mIndividualDisabledByUserChange:Z

.field private mIndividualGameSpline:Landroid/util/Spline;

.field private mIndividualModelCloudDisable:Z

.field private volatile mIndividualModelExperimentEnable:Z

.field private mIndividualVideoSpline:Landroid/util/Spline;

.field private mIsCustomCurveValidReady:Z

.field private mLastModelTrainTimeStamp:J

.field private final mMinBrightness:F

.field private mMinimumBrightnessSpline:Landroid/util/Spline;

.field private mModelTrainTotalTimes:J

.field private final mNormalMaxBrightness:F

.field private mNotifyModelVerificationRunnable:Ljava/lang/Runnable;

.field private mPendingIndividualBrightness:F

.field private final mSettingsObserver:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;

.field private final mShortTermModelTimeout:J

.field private mSupportCustomBrightness:Z

.field private mSupportIndividualBrightness:Z


# direct methods
.method public static synthetic $r8$lambda$8uv2R5DvFjBVNG8lK-B1HeSFpG8(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$setScreenBrightnessByUser$3(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$CffFlsrMBi0uPvyRRVau8Tgi5R8(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$getCustomBrightness$2(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$i8hMZsSLhnGfUotviHlIQKpbjEM(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I[F)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$isMonotonicModel$5(I[F)V

    return-void
.end method

.method public static synthetic $r8$lambda$ioqmpTEEj1h6X-mAuTmiBfSCrpY(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$new$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$p-XPH4bLvZVfyHtFhM0XlR4-2xI(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$onValidatedBrightness$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$wGYYnLzmDPCrE9Rx92gurvvzCT0(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$onBootCompleted$1()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBrightnessDataProcessor(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)Lcom/android/server/display/statistics/BrightnessDataProcessor;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateAutoBrightness(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateAutoBrightness(F)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCustomBrightnessEnabled(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomBrightnessEnabled()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateScreenBrightnessMode(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateScreenBrightnessMode()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 93
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->FORMAT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/statistics/BrightnessDataProcessor;FFF)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "config"    # Lcom/android/server/display/DisplayDeviceConfig;
    .param p4, "dpcImpl"    # Lcom/android/server/display/DisplayPowerControllerImpl;
    .param p5, "processor"    # Lcom/android/server/display/statistics/BrightnessDataProcessor;
    .param p6, "hbmMinimumLux"    # F
    .param p7, "normalMaxBrightness"    # F
    .param p8, "minBrightness"    # F

    .line 220
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p5

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    .line 156
    const/4 v0, -0x1

    iput v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    .line 164
    iput v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    .line 177
    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mPendingIndividualBrightness:F

    .line 190
    const-string v0, "default"

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValidStateReason:Ljava/lang/String;

    .line 202
    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    .line 203
    iput-boolean v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z

    .line 205
    new-instance v0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda1;

    invoke-direct {v0, v7}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNotifyModelVerificationRunnable:Ljava/lang/Runnable;

    .line 221
    iput-object v8, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContext:Landroid/content/Context;

    .line 222
    move-object/from16 v10, p3

    iput-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 223
    move-object/from16 v11, p4

    iput-object v11, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 224
    iput-object v9, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    .line 225
    invoke-virtual {v9, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setModelEventCallback(Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;)V

    .line 226
    move/from16 v12, p6

    iput v12, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F

    .line 227
    move/from16 v13, p7

    iput v13, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNormalMaxBrightness:F

    .line 228
    move/from16 v14, p8

    iput v14, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinBrightness:F

    .line 229
    new-instance v15, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    move-object/from16 v6, p2

    invoke-direct {v15, v7, v6}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;Landroid/os/Looper;)V

    iput-object v15, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    .line 230
    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v5, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    .line 231
    invoke-static {}, Lcom/android/server/display/aiautobrt/AppClassifier;->getInstance()Lcom/android/server/display/aiautobrt/AppClassifier;

    move-result-object v0

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    .line 232
    new-instance v0, Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-direct {v0, v8, v5, v15, v7}, Lcom/android/server/display/aiautobrt/CbmStateTracker;-><init>(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    .line 234
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 235
    .local v4, "resources":Landroid/content/res/Resources;
    const v0, 0x110f00a6

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 238
    .local v3, "componentName":Ljava/lang/String;
    const v0, 0x107001a

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getLuxLevels([I)[F

    move-result-object v2

    iput-object v2, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    .line 240
    const v0, 0x1070014

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultNitsLevels:[F

    .line 242
    const v0, 0x1103003a

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getLuxLevels([I)[F

    move-result-object v1

    .line 244
    .local v1, "customDefaultLuxLevels":[F
    const v0, 0x1103003b

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v0

    .line 246
    .local v0, "customDefaultNitsLevels":[F
    move-object/from16 v16, v2

    const v2, 0x11030034

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-direct {v7, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v2

    .line 248
    .local v2, "brighteningNitsLevels":[F
    move-object/from16 v17, v5

    const v5, 0x11030039

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v5

    invoke-direct {v7, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v5

    .line 250
    .local v5, "darkeningNitsLevels":[F
    const v6, 0x11030041

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v6

    .line 252
    .local v6, "minLuxLevel":[F
    const v8, 0x11030042

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F

    move-result-object v8

    .line 254
    .local v8, "minNitsLevel":[F
    const v10, 0x10e0024

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    int-to-long v10, v10

    iput-wide v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mShortTermModelTimeout:J

    .line 256
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x11050033

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    iput-boolean v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z

    .line 258
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x11050017

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    iput-boolean v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportCustomBrightness:Z

    .line 262
    invoke-direct {v7, v6, v8}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 263
    invoke-static {v6, v8}, Landroid/util/Spline;->createSpline([F[F)Landroid/util/Spline;

    move-result-object v10

    iput-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinimumBrightnessSpline:Landroid/util/Spline;

    .line 267
    :cond_0
    invoke-direct {v7, v1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 268
    invoke-direct {v7, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 269
    invoke-direct {v7, v1, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 270
    const-string v10, "custom_brightness_curve_default"

    invoke-direct {v7, v1, v0, v10}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration;

    move-result-object v10

    iput-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    .line 271
    const-string v10, "custom_brightness_curve_brightening"

    invoke-direct {v7, v1, v2, v10}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration;

    move-result-object v10

    iput-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrighteningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    .line 272
    const-string v10, "custom_brightness_curve_darkening"

    invoke-direct {v7, v1, v5, v10}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration;

    move-result-object v10

    iput-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDarkeningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    .line 275
    :cond_1
    iget-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    iget-object v11, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDarkeningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    move-object/from16 v18, v0

    .end local v0    # "customDefaultNitsLevels":[F
    .local v18, "customDefaultNitsLevels":[F
    iget-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrighteningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v9, v10, v11, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;Landroid/hardware/display/BrightnessConfiguration;Landroid/hardware/display/BrightnessConfiguration;)V

    .line 278
    invoke-direct {v7, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getIndividualComponent(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v10

    iput-object v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualComponentName:Landroid/content/ComponentName;

    .line 279
    new-instance v11, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    iget-object v0, v9, Lcom/android/server/display/statistics/BrightnessDataProcessor;->mIndividualEventNormalizer:Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;

    move-object/from16 v19, v0

    move-object v0, v11

    move-object/from16 v20, v1

    .end local v1    # "customDefaultLuxLevels":[F
    .local v20, "customDefaultLuxLevels":[F
    move-object/from16 v1, p1

    move-object/from16 v21, v8

    move-object/from16 v8, v16

    move-object/from16 v16, v2

    .end local v2    # "brighteningNitsLevels":[F
    .end local v8    # "minNitsLevel":[F
    .local v16, "brighteningNitsLevels":[F
    .local v21, "minNitsLevel":[F
    move-object/from16 v2, v19

    move-object/from16 v19, v3

    .end local v3    # "componentName":Ljava/lang/String;
    .local v19, "componentName":Ljava/lang/String;
    move-object/from16 v3, p2

    move-object/from16 v22, v4

    .end local v4    # "resources":Landroid/content/res/Resources;
    .local v22, "resources":Landroid/content/res/Resources;
    move-object v4, v10

    move-object/from16 v10, v17

    move-object/from16 v17, v5

    .end local v5    # "darkeningNitsLevels":[F
    .local v17, "darkeningNitsLevels":[F
    move-object/from16 v5, p0

    move-object/from16 v23, v6

    .end local v6    # "minLuxLevel":[F
    .local v23, "minLuxLevel":[F
    move-object v6, v10

    invoke-direct/range {v0 .. v6}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;-><init>(Landroid/content/Context;Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;Landroid/os/Looper;Landroid/content/ComponentName;Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$EngineCallback;Landroid/os/Handler;)V

    iput-object v11, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    .line 283
    new-instance v0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;

    invoke-direct {v0, v7, v15}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;Landroid/os/Handler;)V

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSettingsObserver:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;

    .line 284
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    .line 285
    new-instance v0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-direct {v0, v7, v8}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;[F)V

    iput-object v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    .line 286
    iget-object v1, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v1, v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setDataStore(Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;)V

    .line 287
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->registerSettingsObserver()V

    .line 288
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->loadSettings()V

    .line 289
    return-void
.end method

.method private build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration;
    .locals 3
    .param p1, "luxLevels"    # [F
    .param p2, "brightnessLevelsNits"    # [F
    .param p3, "description"    # Ljava/lang/String;

    .line 869
    new-instance v0, Landroid/hardware/display/BrightnessConfiguration$Builder;

    invoke-direct {v0, p1, p2}, Landroid/hardware/display/BrightnessConfiguration$Builder;-><init>([F[F)V

    .line 871
    .local v0, "builder":Landroid/hardware/display/BrightnessConfiguration$Builder;
    iget-wide v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mShortTermModelTimeout:J

    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setShortTermModelTimeoutMillis(J)Landroid/hardware/display/BrightnessConfiguration$Builder;

    .line 872
    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setShortTermModelLowerLuxMultiplier(F)Landroid/hardware/display/BrightnessConfiguration$Builder;

    .line 873
    invoke-virtual {v0, v1}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setShortTermModelUpperLuxMultiplier(F)Landroid/hardware/display/BrightnessConfiguration$Builder;

    .line 874
    invoke-virtual {v0, p3}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setDescription(Ljava/lang/String;)Landroid/hardware/display/BrightnessConfiguration$Builder;

    .line 875
    invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration$Builder;->build()Landroid/hardware/display/BrightnessConfiguration;

    move-result-object v1

    return-object v1
.end method

.method private buildIndividualSpline(I[F)V
    .locals 2
    .param p1, "category"    # I
    .param p2, "nitsArray"    # [F

    .line 823
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    invoke-static {v0, p2}, Landroid/util/Spline;->createSpline([F[F)Landroid/util/Spline;

    move-result-object v0

    .line 824
    .local v0, "spline":Landroid/util/Spline;
    if-nez p1, :cond_0

    .line 825
    iput-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDefaultSpline:Landroid/util/Spline;

    goto :goto_0

    .line 826
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 827
    iput-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualGameSpline:Landroid/util/Spline;

    goto :goto_0

    .line 828
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 829
    iput-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualVideoSpline:Landroid/util/Spline;

    .line 831
    :cond_2
    :goto_0
    return-void
.end method

.method private getFloatArray(Landroid/content/res/TypedArray;)[F
    .locals 4
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .line 938
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    .line 939
    .local v0, "N":I
    new-array v1, v0, [F

    .line 940
    .local v1, "values":[F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 941
    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    aput v3, v1, v2

    .line 940
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 943
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 944
    return-object v1
.end method

.method private getIndividualBrightness(Ljava/lang/String;F)F
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "lux"    # F

    .line 835
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I

    move-result v0

    .line 836
    .local v0, "category":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 837
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualGameSpline:Landroid/util/Spline;

    .local v1, "spline":Landroid/util/Spline;
    goto :goto_0

    .line 838
    .end local v1    # "spline":Landroid/util/Spline;
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 839
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualVideoSpline:Landroid/util/Spline;

    .restart local v1    # "spline":Landroid/util/Spline;
    goto :goto_0

    .line 841
    .end local v1    # "spline":Landroid/util/Spline;
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDefaultSpline:Landroid/util/Spline;

    .line 843
    .restart local v1    # "spline":Landroid/util/Spline;
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v1, p2}, Landroid/util/Spline;->interpolate(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v2

    .line 844
    .local v2, "individualBrt":F
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getIndividualBrightness: category: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", lux: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", individualBrt: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CbmController"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    return v2
.end method

.method private getIndividualComponent(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 340
    if-eqz p1, :cond_0

    .line 341
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0

    .line 343
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private getLuxLevels([I)[F
    .locals 4
    .param p1, "lux"    # [I

    .line 949
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    .line 950
    .local v0, "levels":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 951
    add-int/lit8 v2, v1, 0x1

    aget v3, p1, v1

    int-to-float v3, v3

    aput v3, v0, v2

    .line 950
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 953
    .end local v1    # "i":I
    :cond_0
    return-object v0
.end method

.method private getSceneState(Ljava/lang/String;I)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "orientation"    # I

    .line 468
    invoke-static {}, Ljava/time/LocalDateTime;->now()Ljava/time/LocalDateTime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/time/LocalDateTime;->getHour()I

    move-result v0

    .line 469
    .local v0, "clock":I
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    invoke-virtual {v1, p1}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I

    move-result v1

    .line 470
    .local v1, "category":I
    const/4 v2, 0x0

    .line 471
    .local v2, "scene":I
    invoke-direct {p0, v1, p2, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBrighteningScene(III)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 472
    const/4 v2, 0x1

    goto :goto_0

    .line 473
    :cond_0
    invoke-direct {p0, v1, p2, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isDarkeningScene(III)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 474
    const/4 v2, 0x2

    .line 477
    :cond_1
    :goto_0
    iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    if-eq v3, v2, :cond_2

    .line 478
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSceneState: category: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/android/server/display/aiautobrt/AppClassifier;->categoryToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", scene state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", packageName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", clock: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CbmController"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_2
    return v2
.end method

.method private hasSceneStateChanged(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "orientation"    # I

    .line 452
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getSceneState(Ljava/lang/String;I)I

    move-result v0

    .line 453
    .local v0, "state":I
    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    if-eq v1, v0, :cond_0

    .line 454
    iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    .line 455
    const/4 v1, 0x1

    return v1

    .line 457
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private isBelowMinimumSlope([F)Z
    .locals 8
    .param p1, "nits"    # [F

    .line 1029
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    array-length v2, p1

    array-length v0, v0

    if-eq v2, v0, :cond_0

    goto :goto_4

    .line 1032
    :cond_0
    const/4 v0, 0x0

    .line 1033
    .local v0, "index":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    array-length v4, v3

    const/high16 v5, 0x3f800000    # 1.0f

    if-ge v2, v4, :cond_2

    .line 1034
    move v0, v2

    .line 1035
    aget v4, v3, v2

    iget v6, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F

    sub-float/2addr v6, v5

    cmpl-float v4, v4, v6

    if-lez v4, :cond_1

    .line 1036
    goto :goto_1

    .line 1033
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1039
    .end local v2    # "i":I
    :cond_2
    :goto_1
    if-le v0, v1, :cond_5

    add-int/lit8 v2, v0, -0x1

    aget v2, v3, v2

    const/4 v4, 0x0

    aget v6, v3, v4

    cmpl-float v2, v2, v6

    if-nez v2, :cond_3

    goto :goto_3

    .line 1042
    :cond_3
    add-int/lit8 v2, v0, -0x1

    aget v2, p1, v2

    aget v7, p1, v4

    sub-float/2addr v2, v7

    add-int/lit8 v7, v0, -0x1

    aget v3, v3, v7

    sub-float/2addr v3, v6

    div-float/2addr v2, v3

    .line 1044
    .local v2, "minScope":F
    iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F

    sub-float/2addr v3, v5

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v5, v3

    .line 1045
    .local v5, "permittedMinScope":F
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isBelowMinimumSlope: minScope: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", permittedMinScope: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "CbmController"

    invoke-static {v6, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    cmpg-float v3, v2, v5

    if-gtz v3, :cond_4

    goto :goto_2

    :cond_4
    move v1, v4

    :goto_2
    return v1

    .line 1040
    .end local v2    # "minScope":F
    .end local v5    # "permittedMinScope":F
    :cond_5
    :goto_3
    return v1

    .line 1030
    .end local v0    # "index":I
    :cond_6
    :goto_4
    return v1
.end method

.method private isBelowMinimumSpline([F)Z
    .locals 5
    .param p1, "nits"    # [F

    .line 1016
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinimumBrightnessSpline:Landroid/util/Spline;

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    if-eqz v0, :cond_3

    array-length v2, p1

    array-length v0, v0

    if-eq v2, v0, :cond_0

    goto :goto_1

    .line 1020
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 1021
    aget v3, p1, v0

    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinimumBrightnessSpline:Landroid/util/Spline;

    aget v2, v2, v0

    invoke-virtual {v4, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v2

    cmpg-float v2, v3, v2

    if-gez v2, :cond_1

    .line 1022
    return v1

    .line 1020
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1025
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    return v0

    .line 1018
    :cond_3
    :goto_1
    return v1
.end method

.method private isBrightRoomAdjustment(FLjava/lang/String;IZ)Z
    .locals 3
    .param p1, "lux"    # F
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "orientation"    # I
    .param p4, "isBrightening"    # Z

    .line 1111
    invoke-static {}, Ljava/time/LocalDateTime;->now()Ljava/time/LocalDateTime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/time/LocalDateTime;->getHour()I

    move-result v0

    .line 1112
    .local v0, "clock":I
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    invoke-virtual {v1, p2}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I

    move-result v1

    .line 1113
    .local v1, "category":I
    if-eqz p4, :cond_0

    const/high16 v2, 0x44160000    # 600.0f

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_0

    .line 1114
    invoke-direct {p0, v1, p3, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBrighteningScene(III)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 1113
    :goto_0
    return v2
.end method

.method private isBrighteningScene(III)Z
    .locals 2
    .param p1, "category"    # I
    .param p2, "orientation"    # I
    .param p3, "clock"    # I

    .line 490
    const/4 v0, 0x7

    const/4 v1, 0x1

    if-eq p1, v0, :cond_3

    .line 491
    invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isNoonTime(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isDayTime(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 493
    :cond_0
    invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isHorizontalScreen(I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p1, v1, :cond_3

    const/16 v0, 0x9

    if-eq p1, v0, :cond_3

    .line 496
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isVerticalScreen(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    nop

    .line 490
    :goto_1
    return v1
.end method

.method private isCustomAllowed()Z
    .locals 1

    .line 414
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportCustomBrightness:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveCloudDisable:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isDarkRoomAdjustment(FZ)Z
    .locals 1
    .param p1, "lux"    # F
    .param p2, "isBrightening"    # Z

    .line 1106
    const/high16 v0, 0x42c80000    # 100.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isDarkeningScene(III)Z
    .locals 3
    .param p1, "category"    # I
    .param p2, "orientation"    # I
    .param p3, "clock"    # I

    .line 504
    const/4 v0, 0x4

    if-eq p1, v0, :cond_3

    .line 505
    invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isVerticalScreen(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isNightTime(I)Z

    move-result v0

    const/4 v1, 0x3

    const/16 v2, 0x9

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_3

    if-eq p1, v2, :cond_3

    if-eq p1, v1, :cond_3

    .line 510
    :cond_0
    invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isEarlyMorningTime(I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eq p1, v2, :cond_3

    if-eq p1, v1, :cond_3

    .line 513
    :cond_1
    invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isNoonTime(I)Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne p1, v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    .line 504
    :goto_1
    return v0
.end method

.method private isDayTime(I)Z
    .locals 1
    .param p1, "clock"    # I

    .line 908
    const/16 v0, 0x8

    if-le p1, v0, :cond_0

    const/16 v0, 0xa

    if-le p1, v0, :cond_1

    :cond_0
    const/16 v0, 0xc

    if-le p1, v0, :cond_2

    const/16 v0, 0x14

    if-gt p1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isEarlyMorningTime(I)Z
    .locals 1
    .param p1, "clock"    # I

    .line 900
    const/4 v0, 0x4

    if-le p1, v0, :cond_0

    const/16 v0, 0x8

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isHorizontalScreen(I)Z
    .locals 2
    .param p1, "orientation"    # I

    .line 892
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private isIndividualAllowed()Z
    .locals 1

    .line 424
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualComponentName:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    .line 427
    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelCloudDisable:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    if-eqz v0, :cond_0

    .line 430
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidIndividualSpline()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 424
    :goto_0
    return v0
.end method

.method private isMonotonic([F)Z
    .locals 5
    .param p1, "x"    # [F

    .line 1001
    array-length v0, p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    .line 1002
    return v2

    .line 1004
    :cond_0
    aget v0, p1, v2

    .line 1005
    .local v0, "prev":F
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_3

    .line 1006
    aget v3, p1, v1

    .line 1007
    .local v3, "curr":F
    cmpg-float v4, v3, v0

    if-ltz v4, :cond_2

    const/4 v4, 0x0

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_1

    goto :goto_1

    .line 1010
    :cond_1
    move v0, v3

    .line 1005
    .end local v3    # "curr":F
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1008
    .restart local v3    # "curr":F
    :cond_2
    :goto_1
    return v2

    .line 1012
    .end local v1    # "i":I
    .end local v3    # "curr":F
    :cond_3
    const/4 v1, 0x1

    return v1
.end method

.method private isMonotonicModel()Z
    .locals 9

    .line 760
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 761
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[F>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 762
    .local v3, "key":I
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [F

    .line 763
    .local v4, "array":[F
    invoke-direct {p0, v4}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->smoothCurve([F)[F

    move-result-object v5

    .line 764
    .local v5, "newArray":[F
    const/4 v6, 0x0

    if-nez v5, :cond_0

    .line 765
    return v6

    .line 767
    :cond_0
    invoke-direct {p0, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isMonotonic([F)Z

    move-result v7

    const-string v8, "CbmController"

    if-nez v7, :cond_1

    .line 768
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Model is not monotonic, brightness spline: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    return v6

    .line 771
    :cond_1
    invoke-direct {p0, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBelowMinimumSpline([F)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 772
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Model brightness spline is below the minimum brightness spline, brightness spline: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 773
    invoke-static {v5}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 772
    invoke-static {v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    return v6

    .line 776
    :cond_2
    invoke-direct {p0, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBelowMinimumSlope([F)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 777
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Model brightness spline is below the minimum scope, brightness spline: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 778
    invoke-static {v5}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 777
    invoke-static {v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    return v2

    .line 781
    :cond_3
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    new-instance v6, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda5;

    invoke-direct {v6, p0, v3, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I[F)V

    invoke-virtual {v2, v6}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->post(Ljava/lang/Runnable;)Z

    .line 782
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v2, v3, v5}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeIndividualSpline(I[F)V

    .line 783
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[F>;"
    .end local v3    # "key":I
    .end local v4    # "array":[F
    .end local v5    # "newArray":[F
    goto/16 :goto_0

    .line 784
    :cond_4
    return v2
.end method

.method private isNightTime(I)Z
    .locals 1
    .param p1, "clock"    # I

    .line 896
    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    :cond_0
    const/16 v0, 0x14

    if-le p1, v0, :cond_2

    const/16 v0, 0x17

    if-gt p1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isNoonTime(I)Z
    .locals 1
    .param p1, "clock"    # I

    .line 904
    const/16 v0, 0xa

    if-le p1, v0, :cond_0

    const/16 v0, 0xc

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isValidIndividualSpline()Z
    .locals 1

    .line 851
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDefaultSpline:Landroid/util/Spline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualGameSpline:Landroid/util/Spline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualVideoSpline:Landroid/util/Spline;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isValidMapping([F[F)Z
    .locals 6
    .param p1, "x"    # [F
    .param p2, "y"    # [F

    .line 912
    const/4 v0, 0x0

    if-eqz p1, :cond_9

    if-eqz p2, :cond_9

    array-length v1, p1

    if-eqz v1, :cond_9

    array-length v1, p2

    if-nez v1, :cond_0

    goto :goto_4

    .line 915
    :cond_0
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_1

    .line 916
    return v0

    .line 918
    :cond_1
    array-length v1, p1

    .line 919
    .local v1, "N":I
    aget v2, p1, v0

    .line 920
    .local v2, "prevX":F
    aget v3, p2, v0

    .line 921
    .local v3, "prevY":F
    const/4 v4, 0x0

    cmpg-float v5, v2, v4

    if-ltz v5, :cond_8

    cmpg-float v4, v3, v4

    if-ltz v4, :cond_8

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_3

    .line 924
    :cond_2
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_7

    .line 925
    aget v5, p1, v4

    cmpl-float v5, v2, v5

    if-gez v5, :cond_6

    aget v5, p2, v4

    cmpl-float v5, v3, v5

    if-lez v5, :cond_3

    goto :goto_2

    .line 928
    :cond_3
    aget v5, p1, v4

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_5

    aget v5, p2, v4

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_1

    .line 931
    :cond_4
    aget v2, p1, v4

    .line 932
    aget v3, p2, v4

    .line 924
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 929
    :cond_5
    :goto_1
    return v0

    .line 926
    :cond_6
    :goto_2
    return v0

    .line 934
    .end local v4    # "i":I
    :cond_7
    const/4 v0, 0x1

    return v0

    .line 922
    :cond_8
    :goto_3
    return v0

    .line 913
    .end local v1    # "N":I
    .end local v2    # "prevX":F
    .end local v3    # "prevY":F
    :cond_9
    :goto_4
    return v0
.end method

.method private isVerticalScreen(I)Z
    .locals 1
    .param p1, "orientation"    # I

    .line 888
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method static synthetic lambda$dump$6(Ljava/io/PrintWriter;Ljava/lang/Integer;[F)V
    .locals 2
    .param p0, "pw"    # Ljava/io/PrintWriter;
    .param p1, "k"    # Ljava/lang/Integer;
    .param p2, "v"    # [F

    .line 1200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{cateId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", brtSpl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1201
    invoke-static {p2}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1200
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$getCustomBrightness$2(I)V
    .locals 1
    .param p1, "finalNewCbmState"    # I

    .line 381
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->noteBrightnessAdjustTimesToAggregate(IZ)V

    return-void
.end method

.method private synthetic lambda$isMonotonicModel$5(I[F)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "newArray"    # [F

    .line 781
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildIndividualSpline(I[F)V

    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 2

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    .line 207
    iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    .line 208
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->completeModelValidation()V

    .line 209
    const-string v0, "CbmController"

    const-string v1, "Model cannot complete verification due to predict time out."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    return-void
.end method

.method private synthetic lambda$onBootCompleted$1()V
    .locals 1

    .line 348
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/display/aiautobrt/UserBrightnessStatsJob;->scheduleJob(Landroid/content/Context;)V

    return-void
.end method

.method private synthetic lambda$onValidatedBrightness$4()V
    .locals 3

    .line 688
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    const/4 v1, 0x1

    const-string/jumbo v2, "train_finished"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V

    .line 690
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V

    .line 691
    const-string/jumbo v0, "user_operate"

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V

    .line 692
    invoke-virtual {p0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V

    .line 693
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V

    .line 694
    return-void
.end method

.method private synthetic lambda$setScreenBrightnessByUser$3(I)V
    .locals 1
    .param p1, "finalCbmState"    # I

    .line 568
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->noteBrightnessAdjustTimesToAggregate(IZ)V

    return-void
.end method

.method private loadSettings()V
    .locals 0

    .line 301
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomBrightnessEnabled()V

    .line 302
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateScreenBrightnessMode()V

    .line 303
    return-void
.end method

.method private noteBrightnessAdjustTimesToAggregate(IZ)V
    .locals 1
    .param p1, "cbmState"    # I
    .param p2, "isManuallySet"    # Z

    .line 1132
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->isBrightnessAdjustNoted(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1133
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmBrightnessAdjustTimes(IZ)V

    .line 1135
    :cond_0
    return-void
.end method

.method private permissibleRatio(FF)F
    .locals 2
    .param p1, "currLux"    # F
    .param p2, "prevLux"    # F

    .line 818
    const/high16 v0, 0x3e800000    # 0.25f

    add-float v1, p1, v0

    add-float/2addr v0, p2

    div-float/2addr v1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Landroid/util/MathUtils;->pow(FF)F

    move-result v0

    return v0
.end method

.method private registerSettingsObserver()V
    .locals 5

    .line 292
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    .line 293
    const-string v1, "custom_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSettingsObserver:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;

    .line 292
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 295
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    .line 296
    const-string v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSettingsObserver:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;

    .line 295
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 298
    return-void
.end method

.method private resetCustomCurveValidConditions()V
    .locals 1

    .line 1122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z

    .line 1123
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->resetBrtAdjSceneCount()V

    .line 1124
    return-void
.end method

.method private setPendingToWaitPredict(F)V
    .locals 4
    .param p1, "newAutoBrightness"    # F

    .line 862
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->removeMessages(I)V

    .line 863
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    .line 864
    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 863
    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 865
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 866
    return-void
.end method

.method private smoothCurve([F)[F
    .locals 9
    .param p1, "nitsArray"    # [F

    .line 788
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    array-length v1, v0

    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultNitsLevels:[F

    array-length v2, v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    if-nez v1, :cond_0

    goto :goto_2

    .line 794
    :cond_0
    array-length v2, p1

    new-array v2, v2, [F

    .line 795
    .local v2, "newNitsArray":[F
    const/4 v3, 0x0

    aget v0, v0, v3

    .line 796
    .local v0, "preLux":F
    aget v4, p1, v3

    invoke-virtual {v1, v4}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v1

    iget v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinBrightness:F

    iget v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNormalMaxBrightness:F

    invoke-static {v1, v4, v5}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v1

    .line 798
    .local v1, "preBrightness":F
    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v4, v1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v4

    aput v4, v2, v3

    .line 799
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    array-length v5, v4

    if-ge v3, v5, :cond_2

    .line 800
    aget v5, v4, v3

    iget v6, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_1

    .line 801
    aget v4, v4, v3

    .line 802
    .local v4, "currentLux":F
    iget-object v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    aget v6, p1, v3

    invoke-virtual {v5, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v5

    .line 803
    .local v5, "currentBrightness":F
    nop

    .line 804
    invoke-direct {p0, v4, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->permissibleRatio(FF)F

    move-result v6

    mul-float/2addr v6, v1

    const v7, 0x3b83126f    # 0.004f

    add-float/2addr v7, v1

    .line 803
    invoke-static {v6, v7}, Landroid/util/MathUtils;->max(FF)F

    move-result v6

    iget v7, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNormalMaxBrightness:F

    invoke-static {v6, v7}, Landroid/util/MathUtils;->min(FF)F

    move-result v6

    .line 806
    .local v6, "maxBrightness":F
    invoke-static {v5, v1, v6}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v7

    .line 807
    .local v7, "newBrightness":F
    move v0, v4

    .line 808
    move v1, v7

    .line 809
    iget-object v8, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v8, v7}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F

    move-result v8

    aput v8, v2, v3

    .line 810
    .end local v4    # "currentLux":F
    .end local v5    # "currentBrightness":F
    .end local v6    # "maxBrightness":F
    goto :goto_1

    .line 811
    .end local v7    # "newBrightness":F
    :cond_1
    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultNitsLevels:[F

    aget v4, v4, v3

    aput v4, v2, v3

    .line 799
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 814
    .end local v3    # "i":I
    :cond_2
    return-object v2

    .line 790
    .end local v0    # "preLux":F
    .end local v1    # "preBrightness":F
    .end local v2    # "newNitsArray":[F
    :cond_3
    :goto_2
    const-string v0, "CbmController"

    const-string v1, "Can not smooth individual curve!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    const/4 v0, 0x0

    return-object v0
.end method

.method private updateAutoBrightness(F)V
    .locals 1
    .param p1, "brightness"    # F

    .line 857
    iput p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mPendingIndividualBrightness:F

    .line 858
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAutoBrightness()V

    .line 859
    return-void
.end method

.method private updateCbmState(I)V
    .locals 5
    .param p1, "cbmState"    # I

    .line 401
    iget v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    .line 402
    .local v0, "oldState":I
    iput p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    .line 403
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 404
    .local v1, "now":J
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 405
    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v4, v0, v1, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteStopCbmStateTracking(IJ)V

    .line 407
    :cond_0
    iget v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    if-eq v4, v3, :cond_1

    .line 408
    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteStartCbmStateTracking(IJ)V

    .line 410
    :cond_1
    return-void
.end method

.method private updateCustomBrightnessEnabled()V
    .locals 6

    .line 306
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "custom_brightness_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 308
    .local v0, "value":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v3

    .line 309
    .local v2, "forcedEnableCustomCurve":Z
    :goto_0
    iget-boolean v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedCustomCurveEnabled:Z

    const-string v5, "forced_operate"

    if-eq v4, v2, :cond_1

    .line 310
    iput-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedCustomCurveEnabled:Z

    .line 311
    invoke-virtual {p0, v2, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V

    .line 313
    :cond_1
    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    goto :goto_1

    :cond_2
    move v1, v3

    .line 314
    .local v1, "forcedEnableModel":Z
    :goto_1
    iget-boolean v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z

    if-eq v4, v1, :cond_3

    .line 315
    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z

    .line 316
    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v4, v1, v5}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V

    .line 319
    :cond_3
    if-nez v2, :cond_4

    if-eqz v1, :cond_5

    .line 320
    :cond_4
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V

    .line 322
    :cond_5
    if-eqz v1, :cond_6

    .line 323
    invoke-virtual {p0, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V

    .line 325
    :cond_6
    return-void
.end method

.method private updateScreenBrightnessMode()V
    .locals 4

    .line 328
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "screen_brightness_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAutoBrightnessModeEnabled:Z

    .line 332
    return-void
.end method

.method private uploadValidatedEventToModel()V
    .locals 9

    .line 744
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    if-nez v0, :cond_0

    .line 745
    return-void

    .line 747
    :cond_0
    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 748
    aget v0, v0, v1

    .line 749
    .local v0, "lux":F
    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    iget v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    move v4, v0

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(FIFII)Lcom/xiaomi/aiautobrt/IndividualModelEvent;

    move-result-object v1

    .line 752
    .local v1, "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    invoke-virtual {v1}, Lcom/xiaomi/aiautobrt/IndividualModelEvent;->isValidRawEvent()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z

    if-eqz v2, :cond_1

    .line 753
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->preparePredictBrightness(Lcom/xiaomi/aiautobrt/IndividualModelEvent;)V

    .line 755
    :cond_1
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNotifyModelVerificationRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 757
    .end local v0    # "lux":F
    .end local v1    # "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent;
    :cond_2
    return-void
.end method


# virtual methods
.method protected buildConfigurationFromXml(I[F)V
    .locals 1
    .param p1, "category"    # I
    .param p2, "nits"    # [F

    .line 1155
    invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isMonotonic([F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildIndividualSpline(I[F)V

    .line 1158
    :cond_0
    return-void
.end method

.method protected customCurveConditionsSatisfied()V
    .locals 2

    .line 589
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z

    if-nez v0, :cond_0

    .line 590
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z

    .line 591
    const-string v0, "CbmController"

    const-string v1, "Satisfy valid conditions of custom curve."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    :cond_0
    return-void
.end method

.method protected disableCustomCurve(Z)V
    .locals 3
    .param p1, "disable"    # Z

    .line 523
    const/4 v0, 0x0

    .line 524
    .local v0, "changed":Z
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    if-nez v1, :cond_0

    .line 525
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    .line 527
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V

    .line 528
    const/4 v0, 0x1

    goto :goto_0

    .line 529
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    if-eqz v1, :cond_1

    .line 530
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    .line 531
    const/4 v0, 0x1

    .line 533
    :cond_1
    :goto_0
    if-eqz v0, :cond_3

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    if-eqz v2, :cond_2

    .line 535
    const-string v2, "disable "

    goto :goto_1

    :cond_2
    const-string v2, "enable "

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "custom curve."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 534
    const-string v2, "CbmController"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    :cond_3
    return-void
.end method

.method protected disableIndividualEngine(Z)V
    .locals 3
    .param p1, "disable"    # Z

    .line 540
    const/4 v0, 0x0

    .line 541
    .local v0, "changed":Z
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    if-nez v1, :cond_0

    .line 542
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    .line 543
    const/4 v0, 0x1

    goto :goto_0

    .line 544
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    if-eqz v1, :cond_1

    .line 545
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    .line 546
    const/4 v0, 0x1

    .line 548
    :cond_1
    :goto_0
    if-eqz v0, :cond_3

    .line 549
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    if-eqz v2, :cond_2

    .line 550
    const-string v2, "disable "

    goto :goto_1

    :cond_2
    const-string v2, "enable "

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "individual engine."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 549
    const-string v2, "CbmController"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_3
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1173
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_CBM:Z

    sput-boolean v0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->sDebug:Z

    .line 1174
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->forceReportTrainDataEnabled(Z)V

    .line 1175
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportCustomBrightness:Z

    if-eqz v0, :cond_0

    .line 1176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCustomCurveDisabledByUserChange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mForcedCustomCurveEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedCustomCurveEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentSceneState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDefaultConfiguration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBrighteningConfiguration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrighteningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDarkeningConfiguration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDarkeningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCustomCurveValidStateReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValidStateReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCustomCurveValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIsCustomCurveValidReady="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCustomCurveExperimentEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1188
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z

    if-eqz v0, :cond_2

    .line 1189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mForcedIndividualBrightnessEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIndividualDisabledByUserChange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mPendingIndividualBrightness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mPendingIndividualBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mMinimumBrightnessSpline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinimumBrightnessSpline:Landroid/util/Spline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIndividualModelExperimentEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLastModelTrainTimeStamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mLastModelTrainTimeStamp:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mModelTrainTotalTimes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mModelTrainTotalTimes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1196
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->dump(Ljava/io/PrintWriter;)V

    .line 1197
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->dump(Ljava/io/PrintWriter;)V

    .line 1198
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1199
    const-string v0, "  Brt validation set:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1200
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    new-instance v1, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda6;

    invoke-direct {v1, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda6;-><init>(Ljava/io/PrintWriter;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 1203
    :cond_1
    const-string v0, " Individual Model Spline:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIndividualDefaultSpline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDefaultSpline:Landroid/util/Spline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIndividualGameSpline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualGameSpline:Landroid/util/Spline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIndividualVideoSpline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualVideoSpline:Landroid/util/Spline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1207
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->dump(Ljava/io/PrintWriter;)V

    .line 1209
    :cond_2
    return-void
.end method

.method public getCurrentSceneState()I
    .locals 1

    .line 1118
    iget v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    return v0
.end method

.method public getCustomBrightness(FLjava/lang/String;IFFIZ)F
    .locals 6
    .param p1, "lux"    # F
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "category"    # I
    .param p4, "oldAutoBrightness"    # F
    .param p5, "newAutoBrightness"    # F
    .param p6, "orientation"    # I
    .param p7, "isManuallySet"    # Z

    .line 354
    move v0, p5

    .line 356
    .local v0, "customBrightness":F
    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    .line 357
    .local v1, "newCbmState":I
    const/4 v2, 0x0

    .line 358
    .local v2, "notCount":Z
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isIndividualAllowed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 359
    invoke-direct {p0, p2, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getIndividualBrightness(Ljava/lang/String;F)F

    move-result v0

    .line 360
    const/4 v1, 0x2

    goto :goto_0

    .line 361
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isCustomAllowed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 362
    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    invoke-virtual {v3, p1, p2, p3}, Lcom/android/server/display/BrightnessMappingStrategy;->getBrightness(FLjava/lang/String;I)F

    move-result v0

    .line 363
    const/4 v1, 0x1

    goto :goto_0

    .line 365
    :cond_1
    const/4 v1, 0x0

    .line 368
    :goto_0
    iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    if-eq v1, v3, :cond_2

    .line 369
    invoke-direct {p0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCbmState(I)V

    .line 375
    :cond_2
    if-nez v2, :cond_3

    if-nez p7, :cond_3

    cmpl-float v3, v0, p4

    if-eqz v3, :cond_3

    .line 377
    invoke-static {p4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_3

    .line 378
    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v3, v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteAutoAdjustmentTimes(I)V

    .line 380
    move v3, v1

    .line 381
    .local v3, "finalNewCbmState":I
    iget-object v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    new-instance v5, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda4;

    invoke-direct {v5, p0, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 384
    .end local v3    # "finalNewCbmState":I
    :cond_3
    sget-boolean v3, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->sDebug:Z

    if-nez v3, :cond_4

    cmpl-float v3, v0, p5

    if-eqz v3, :cond_5

    cmpl-float v3, v0, p4

    if-eqz v3, :cond_5

    .line 386
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCustomBrightness: previous: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", config: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", custom: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", packageName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", orientation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CbmController"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_5
    return v0
.end method

.method protected noteBrightnessUsageToAggregate(FI)V
    .locals 1
    .param p1, "duration"    # F
    .param p2, "cbmState"    # I

    .line 1143
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmBrightnessUsageDuration(FI)V

    .line 1144
    return-void
.end method

.method protected notePredictDurationToAggregate(J)V
    .locals 2
    .param p1, "duration"    # J

    .line 1151
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    long-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateModelAvgPredictDuration(F)V

    .line 1152
    return-void
.end method

.method public onAbTestExperimentUpdated(II)V
    .locals 3
    .param p1, "expId"    # I
    .param p2, "flag"    # I

    .line 728
    const/4 v0, -0x1

    const/4 v1, 0x1

    if-ne p2, v0, :cond_0

    .line 729
    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z

    .line 730
    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    goto :goto_2

    .line 732
    :cond_0
    const/4 v0, 0x0

    if-ne p2, v1, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z

    .line 733
    const/4 v2, 0x2

    if-ne p2, v2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v0

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    .line 735
    :goto_2
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateExpId(I)V

    .line 736
    return-void
.end method

.method public onBootCompleted()V
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->onBootCompleted()V

    .line 348
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 349
    return-void
.end method

.method public onBrightnessModelEvent(Lcom/xiaomi/aiautobrt/IndividualModelEvent;)V
    .locals 2
    .param p1, "modelEvent"    # Lcom/xiaomi/aiautobrt/IndividualModelEvent;

    .line 620
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAutoBrightnessModeEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualComponentName:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    invoke-virtual {v0, p1, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->uploadBrightnessModelEvent(Lcom/xiaomi/aiautobrt/IndividualModelEvent;Z)V

    .line 625
    :cond_0
    return-void
.end method

.method public onCloudUpdated(JLjava/util/Map;)V
    .locals 7
    .param p1, "summary"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 880
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-wide/16 v0, 0x8

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 881
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAppClassifier:Lcom/android/server/display/aiautobrt/AppClassifier;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/AppClassifier;->loadAppCategoryConfig()V

    .line 883
    :cond_0
    const-wide/16 v0, 0x10

    and-long/2addr v0, p1

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v4

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveCloudDisable:Z

    .line 884
    const-wide/16 v5, 0x20

    and-long/2addr v5, p1

    cmp-long v0, v5, v2

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v4

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelCloudDisable:Z

    .line 885
    return-void
.end method

.method public onExperimentUpdated(IZ)V
    .locals 1
    .param p1, "expId"    # I
    .param p2, "enable"    # Z

    .line 713
    iput-boolean p2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z

    .line 714
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateExpId(I)V

    .line 715
    return-void
.end method

.method public onPredictFinished(FIF)V
    .locals 6
    .param p1, "lux"    # F
    .param p2, "appId"    # I
    .param p3, "brightness"    # F

    .line 630
    invoke-static {p3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    const-string v1, ", brt: "

    const-string v2, ", appId: "

    const-string v3, "CbmController"

    if-nez v0, :cond_1

    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_0

    goto :goto_0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->removeMessages(I)V

    .line 637
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    invoke-virtual {v0, p3}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F

    move-result v0

    .line 638
    .local v0, "dbv":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPredictFinished: lux: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dbv: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v1, p1, p2, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteIndividualResult(FIF)V

    .line 644
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteStopPredictTracking(J)V

    .line 645
    invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateAutoBrightness(F)V

    .line 646
    return-void

    .line 631
    .end local v0    # "dbv":F
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Predict error: lux: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    return-void
.end method

.method public onTrainIndicatorsFinished(Lcom/xiaomi/aiautobrt/IndividualTrainEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/xiaomi/aiautobrt/IndividualTrainEvent;

    .line 723
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateModelTrainIndicators(Lcom/xiaomi/aiautobrt/IndividualTrainEvent;)V

    .line 724
    return-void
.end method

.method public onValidatedBrightness(F)V
    .locals 7
    .param p1, "brightness"    # F

    .line 666
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNotifyModelVerificationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 667
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isVerificationInProgress()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    if-eqz v0, :cond_6

    array-length v1, v0

    if-nez v1, :cond_0

    goto/16 :goto_2

    .line 672
    :cond_0
    array-length v0, v0

    new-array v0, v0, [F

    .line 673
    .local v0, "nitsArray":[F
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 674
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [F

    .line 676
    :cond_1
    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    aput p1, v0, v1

    .line 677
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 678
    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    .line 679
    iget-object v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultLuxLevels:[F

    array-length v4, v3

    if-ge v1, v4, :cond_2

    .line 680
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->uploadValidatedEventToModel()V

    goto :goto_1

    .line 681
    :cond_2
    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x2

    if-ne v1, v4, :cond_3

    iget v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    if-ge v4, v6, :cond_3

    .line 682
    iput v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    .line 683
    add-int/2addr v4, v2

    iput v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    .line 684
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->uploadValidatedEventToModel()V

    goto :goto_1

    .line 685
    :cond_3
    array-length v3, v3

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    if-ne v1, v6, :cond_5

    .line 686
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isMonotonicModel()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 687
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHandler:Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;

    new-instance v3, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V

    invoke-virtual {v1, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->post(Ljava/lang/Runnable;)Z

    .line 696
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateIndividualModelTrainTimes(Z)V

    goto :goto_0

    .line 699
    :cond_4
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v1, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateIndividualModelTrainTimes(Z)V

    .line 700
    const-string v1, "CbmController"

    const-string v2, "Model cannot complete verification due to non-monotonicity."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->completeModelValidation()V

    .line 704
    :cond_5
    :goto_1
    return-void

    .line 670
    .end local v0    # "nitsArray":[F
    :cond_6
    :goto_2
    return-void
.end method

.method public resetShortTermModel(Z)V
    .locals 3
    .param p1, "manually"    # Z

    .line 600
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    const-string/jumbo v1, "user_operate"

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    .line 601
    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 603
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V

    .line 604
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V

    .line 605
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V

    goto :goto_0

    .line 606
    :cond_0
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 607
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V

    .line 608
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V

    .line 609
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v2, v0, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V

    .line 610
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V

    .line 612
    :cond_2
    :goto_0
    return-void
.end method

.method public setAutoBrightnessComponent(Lcom/android/server/display/BrightnessMappingStrategy;)V
    .locals 0
    .param p1, "mapper"    # Lcom/android/server/display/BrightnessMappingStrategy;

    .line 518
    iput-object p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    .line 519
    return-void
.end method

.method protected setCustomCurveEnabledFromXml(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 1161
    iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    .line 1162
    const-string v0, "backup"

    iput-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValidStateReason:Ljava/lang/String;

    .line 1163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setCustomCurveEnabledFromXml: custom curve is"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1164
    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    if-eqz v1, :cond_0

    const-string v1, " valid"

    goto :goto_0

    :cond_0
    const-string v1, " invalid"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " due to: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValidStateReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1163
    const-string v1, "CbmController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1166
    return-void
.end method

.method public setCustomCurveEnabledOnCommand(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 1091
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    .line 1092
    nop

    .line 1091
    const-string v1, "custom_brightness_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1093
    return-void
.end method

.method protected setCustomCurveValid(ZLjava/lang/String;)V
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "reason"    # Ljava/lang/String;

    .line 1070
    const-string v0, "CbmController"

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    if-nez v1, :cond_0

    .line 1071
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    .line 1072
    iput-object p2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValidStateReason:Ljava/lang/String;

    .line 1073
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeCustomCurveEnabled(Z)V

    .line 1074
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setCustomCurveValid: custom curve is valid due to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1075
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    if-eqz v1, :cond_1

    .line 1076
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    .line 1077
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    .line 1078
    iput-object p2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValidStateReason:Ljava/lang/String;

    .line 1080
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V

    .line 1081
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeCustomCurveEnabled(Z)V

    .line 1082
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setCustomCurveValid: custom curve is invalid due to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    :cond_1
    :goto_0
    return-void
.end method

.method public setForceTrainEnabledOnCommand(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 1101
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    .line 1102
    nop

    .line 1101
    const-string v1, "force_train_enable"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1103
    return-void
.end method

.method protected setIndividualModelEnabledFromXml(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .line 1169
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModeValidFromXml(Z)V

    .line 1170
    return-void
.end method

.method public setIndividualModelEnabledOnCommand(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 1096
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mContentResolver:Landroid/content/ContentResolver;

    .line 1097
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 1096
    :goto_0
    const-string v2, "custom_brightness_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1098
    return-void
.end method

.method public setScreenBrightnessByUser(FZLjava/lang/String;I)V
    .locals 4
    .param p1, "userDataPoint"    # F
    .param p2, "isBrightening"    # Z
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "orientation"    # I

    .line 557
    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->bindServiceDueToBrightnessAdjust(Z)V

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z

    if-eqz v0, :cond_2

    .line 563
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V

    .line 565
    :cond_2
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(I)V

    .line 567
    iget v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    .line 568
    .local v0, "finalCbmState":I
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda7;

    invoke-direct {v3, p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 571
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z

    move-result v2

    if-nez v2, :cond_4

    iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z

    if-eqz v2, :cond_4

    .line 575
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isDarkRoomAdjustment(FZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 576
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    invoke-virtual {v2, v3, v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(II)V

    .line 578
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->startEvaluateCustomCurve()V

    goto :goto_0

    .line 579
    :cond_3
    invoke-direct {p0, p1, p3, p4, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBrightRoomAdjustment(FLjava/lang/String;IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 581
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(II)V

    .line 583
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->startEvaluateCustomCurve()V

    .line 586
    :cond_4
    :goto_0
    return-void
.end method

.method public startCbmStatsJob()V
    .locals 3

    .line 1087
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmStateTracker:Lcom/android/server/display/aiautobrt/CbmStateTracker;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda2;

    invoke-direct {v2, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1088
    return-void
.end method

.method public updateCbmState(Z)V
    .locals 1
    .param p1, "autoBrightnessEnabled"    # Z

    .line 957
    if-nez p1, :cond_0

    .line 958
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCbmState(I)V

    .line 960
    :cond_0
    return-void
.end method

.method public updateCustomSceneState(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "orientation"    # I

    .line 434
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isCustomAllowed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 435
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->hasSceneStateChanged(Ljava/lang/String;I)Z

    move-result v0

    .line 436
    .local v0, "changed":Z
    if-eqz v0, :cond_2

    .line 438
    iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 439
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrighteningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    .local v1, "config":Landroid/hardware/display/BrightnessConfiguration;
    goto :goto_0

    .line 440
    .end local v1    # "config":Landroid/hardware/display/BrightnessConfiguration;
    :cond_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 441
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDarkeningConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    .restart local v1    # "config":Landroid/hardware/display/BrightnessConfiguration;
    goto :goto_0

    .line 443
    .end local v1    # "config":Landroid/hardware/display/BrightnessConfiguration;
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDefaultConfiguration:Landroid/hardware/display/BrightnessConfiguration;

    .line 445
    .restart local v1    # "config":Landroid/hardware/display/BrightnessConfiguration;
    :goto_0
    iget-object v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {v2, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V

    .line 446
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateCustomSceneState: config changed, config: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/hardware/display/BrightnessConfiguration;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CbmController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    .end local v0    # "changed":Z
    .end local v1    # "config":Landroid/hardware/display/BrightnessConfiguration;
    :cond_2
    return-void
.end method

.method protected updateModelValid(IF)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "ratio"    # F

    .line 1050
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    .line 1051
    .local v2, "customCurveValid":Z
    :goto_0
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v0

    .line 1052
    .local v3, "IndividualModelValid":Z
    :goto_1
    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    move v1, v0

    .line 1053
    .local v1, "defaultValid":Z
    :goto_2
    const-string v4, "best_indicator"

    invoke-virtual {p0, v2, v4}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V

    .line 1054
    iget-object v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualBrightnessEngine:Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;

    invoke-virtual {v5, v3, v4}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V

    .line 1055
    if-nez v2, :cond_3

    if-eqz v3, :cond_4

    .line 1056
    :cond_3
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V

    .line 1058
    :cond_4
    if-eqz v3, :cond_5

    .line 1059
    invoke-virtual {p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V

    .line 1061
    :cond_5
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mDataStore:Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;

    invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V

    .line 1062
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateModelValid: custom valid: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", individual model valid: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", default valid: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", best ratio: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "CbmController"

    invoke-static {v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    return-void
.end method

.method public validateModelMonotonicity()V
    .locals 4

    .line 650
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I

    .line 651
    iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I

    .line 652
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessValidationMapper:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 653
    invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->uploadValidatedEventToModel()V

    .line 654
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mLastModelTrainTimeStamp:J

    .line 655
    iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mModelTrainTotalTimes:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mModelTrainTotalTimes:J

    .line 657
    iget-object v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mBrightnessDataProcessor:Lcom/android/server/display/statistics/BrightnessDataProcessor;

    invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateIndividualModelTrainTimes()V

    .line 658
    return-void
.end method
