public abstract class com.android.server.display.aiautobrt.IndividualBrightnessEngine$EngineCallback {
	 /* .source "IndividualBrightnessEngine.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x60c */
/* name = "EngineCallback" */
} // .end annotation
/* # virtual methods */
public abstract void onAbTestExperimentUpdated ( Integer p0, Integer p1 ) {
} // .end method
public abstract void onExperimentUpdated ( Integer p0, Boolean p1 ) {
} // .end method
public abstract void onPredictFinished ( Float p0, Integer p1, Float p2 ) {
} // .end method
public abstract void onTrainIndicatorsFinished ( com.xiaomi.aiautobrt.IndividualTrainEvent p0 ) {
} // .end method
public abstract void onValidatedBrightness ( Float p0 ) {
} // .end method
public abstract void validateModelMonotonicity ( ) {
} // .end method
