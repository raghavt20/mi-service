public class com.android.server.display.aiautobrt.UserBrightnessStatsJob extends android.app.job.JobService {
	 /* .source "UserBrightnessStatsJob.java" */
	 /* # static fields */
	 private static final Long DEBUG_INTERVAL;
	 private static final Integer JOB_ID;
	 private static final java.lang.String TAG;
	 private static final Boolean sDebug;
	 /* # direct methods */
	 static com.android.server.display.aiautobrt.UserBrightnessStatsJob ( ) {
		 /* .locals 3 */
		 /* .line 25 */
		 final String v0 = "debug.miui.display.JobService.dbg"; // const-string v0, "debug.miui.display.JobService.dbg"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v1 = 1; // const/4 v1, 0x1
		 } // :cond_0
		 com.android.server.display.aiautobrt.UserBrightnessStatsJob.sDebug = (v1!= 0);
		 /* .line 27 */
		 v0 = java.util.concurrent.TimeUnit.MINUTES;
		 /* const-wide/16 v1, 0xf */
		 (( java.util.concurrent.TimeUnit ) v0 ).toMillis ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/android/server/display/aiautobrt/UserBrightnessStatsJob;->DEBUG_INTERVAL:J */
		 return;
	 } // .end method
	 public com.android.server.display.aiautobrt.UserBrightnessStatsJob ( ) {
		 /* .locals 0 */
		 /* .line 17 */
		 /* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
		 return;
	 } // .end method
	 public static void scheduleJob ( android.content.Context p0 ) {
		 /* .locals 9 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 30 */
		 final String v0 = "Start schedule job."; // const-string v0, "Start schedule job."
		 final String v1 = "CbmController-StatsJob"; // const-string v1, "CbmController-StatsJob"
		 android.util.Slog .d ( v1,v0 );
		 /* .line 31 */
		 /* const-class v0, Landroid/app/job/JobScheduler; */
		 (( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/app/job/JobScheduler; */
		 /* .line 32 */
		 /* .local v0, "jobScheduler":Landroid/app/job/JobScheduler; */
		 /* const v2, 0x87d1 */
		 (( android.app.job.JobScheduler ) v0 ).getPendingJob ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->getPendingJob(I)Landroid/app/job/JobInfo;
		 /* .line 33 */
		 /* .local v3, "pending":Landroid/app/job/JobInfo; */
		 /* new-instance v4, Landroid/app/job/JobInfo$Builder; */
		 /* new-instance v5, Landroid/content/ComponentName; */
		 /* const-class v6, Lcom/android/server/display/aiautobrt/UserBrightnessStatsJob; */
		 /* invoke-direct {v5, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V */
		 /* invoke-direct {v4, v2, v5}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
		 /* .line 35 */
		 int v5 = 1; // const/4 v5, 0x1
		 (( android.app.job.JobInfo$Builder ) v4 ).setRequiresDeviceIdle ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;
		 /* .line 36 */
		 /* sget-boolean v5, Lcom/android/server/display/aiautobrt/UserBrightnessStatsJob;->sDebug:Z */
		 if ( v5 != null) { // if-eqz v5, :cond_0
			 /* sget-wide v6, Lcom/android/server/display/aiautobrt/UserBrightnessStatsJob;->DEBUG_INTERVAL:J */
		 } // :cond_0
		 v6 = java.util.concurrent.TimeUnit.HOURS;
		 /* const-wide/16 v7, 0x18 */
		 (( java.util.concurrent.TimeUnit ) v6 ).toMillis ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
		 /* move-result-wide v6 */
	 } // :goto_0
	 (( android.app.job.JobInfo$Builder ) v4 ).setPeriodic ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;
	 /* .line 37 */
	 (( android.app.job.JobInfo$Builder ) v4 ).build ( ); // invoke-virtual {v4}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
	 /* .line 39 */
	 /* .local v4, "jobInfo":Landroid/app/job/JobInfo; */
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 v6 = 		 (( android.app.job.JobInfo ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/job/JobInfo;->equals(Ljava/lang/Object;)Z
		 /* if-nez v6, :cond_1 */
		 /* .line 40 */
		 final String v6 = "scheduleJob: cancel."; // const-string v6, "scheduleJob: cancel."
		 android.util.Slog .d ( v1,v6 );
		 /* .line 41 */
		 (( android.app.job.JobScheduler ) v0 ).cancel ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->cancel(I)V
		 /* .line 42 */
		 int v3 = 0; // const/4 v3, 0x0
		 /* .line 45 */
	 } // :cond_1
	 /* if-nez v3, :cond_2 */
	 /* .line 46 */
	 final String v2 = "scheduleJob: schedule."; // const-string v2, "scheduleJob: schedule."
	 android.util.Slog .d ( v1,v2 );
	 /* .line 47 */
	 (( android.app.job.JobScheduler ) v0 ).schedule ( v4 ); // invoke-virtual {v0, v4}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
	 /* .line 50 */
} // :cond_2
if ( v5 != null) { // if-eqz v5, :cond_3
	 /* .line 51 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "Schedule job use debug interval, interval: "; // const-string v5, "Schedule job use debug interval, interval: "
	 (( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* sget-wide v5, Lcom/android/server/display/aiautobrt/UserBrightnessStatsJob;->DEBUG_INTERVAL:J */
	 (( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v2 );
	 /* .line 53 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public Boolean onStartJob ( android.app.job.JobParameters p0 ) {
/* .locals 2 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 57 */
final String v0 = "CbmController-StatsJob"; // const-string v0, "CbmController-StatsJob"
final String v1 = "Start user manual adjustment stats."; // const-string v1, "Start user manual adjustment stats."
android.util.Slog .d ( v0,v1 );
/* .line 58 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).startCbmStatsJob ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceStub;->startCbmStatsJob()V
/* .line 59 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean onStopJob ( android.app.job.JobParameters p0 ) {
/* .locals 2 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 64 */
final String v0 = "CbmController-StatsJob"; // const-string v0, "CbmController-StatsJob"
final String v1 = "Stop job."; // const-string v1, "Stop job."
android.util.Slog .d ( v0,v1 );
/* .line 65 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
