class com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 extends com.xiaomi.aiautobrt.IIndividualCallback$Stub {
	 /* .source "IndividualBrightnessEngine.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.aiautobrt.IndividualBrightnessEngine this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$2vX2s-iHTQGo6YOIxw0Igd_fBs4 ( com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 p0, Integer p1, Boolean p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;->lambda$onUpdateExperiment$2(IZ)V */
return;
} // .end method
public static void $r8$lambda$WIa-geWWlcv8o06qhzQ790Ugg9s ( com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 p0, Integer p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;->lambda$onAbTestExperimentUpdated$4(II)V */
return;
} // .end method
public static void $r8$lambda$Y_6DLxLFzoQGbFtBDb4eEsSN7bE ( com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;->lambda$onTrainFinished$0()V */
return;
} // .end method
public static void $r8$lambda$bZf-fcBIbFYAgPoZGxpwzgUGzd8 ( com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 p0, com.xiaomi.aiautobrt.IndividualTrainEvent p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;->lambda$onTrainIndicatorsFinished$3(Lcom/xiaomi/aiautobrt/IndividualTrainEvent;)V */
return;
} // .end method
public static void $r8$lambda$sSdy3GWeh5nVmZVE4Z7BRu7G3fk ( com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 p0, Float p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;->lambda$onPredictFinished$1(F)V */
return;
} // .end method
 com.android.server.display.aiautobrt.IndividualBrightnessEngine$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine; */
/* .line 173 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/xiaomi/aiautobrt/IIndividualCallback$Stub;-><init>()V */
return;
} // .end method
private void lambda$onAbTestExperimentUpdated$4 ( Integer p0, Integer p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "expId" # I */
/* .param p2, "flag" # I */
/* .line 216 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmEngineCallback ( v0 );
return;
} // .end method
private void lambda$onPredictFinished$1 ( Float p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 189 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmEngineCallback ( v0 );
return;
} // .end method
private void lambda$onTrainFinished$0 ( ) { //synthethic
/* .locals 2 */
/* .line 178 */
final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
final String v1 = "Model train is finished."; // const-string v1, "Model train is finished."
android.util.Slog .d ( v0,v1 );
/* .line 179 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fputmModelValidationInProgress ( v0,v1 );
/* .line 180 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmEngineCallback ( v0 );
/* .line 181 */
return;
} // .end method
private void lambda$onTrainIndicatorsFinished$3 ( com.xiaomi.aiautobrt.IndividualTrainEvent p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "event" # Lcom/xiaomi/aiautobrt/IndividualTrainEvent; */
/* .line 205 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmEngineCallback ( v0 );
return;
} // .end method
private void lambda$onUpdateExperiment$2 ( Integer p0, Boolean p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "expId" # I */
/* .param p2, "enable" # Z */
/* .line 195 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmEngineCallback ( v0 );
return;
} // .end method
/* # virtual methods */
public void onAbTestExperimentUpdated ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "expId" # I */
/* .param p2, "flag" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 216 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 217 */
return;
} // .end method
public void onPredictFinished ( Float p0, Float p1, Float p2 ) {
/* .locals 2 */
/* .param p1, "lux" # F */
/* .param p2, "appId" # F */
/* .param p3, "brightness" # F */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 187 */
v0 = this.this$0;
v0 = com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmModelValidationInProgress ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 189 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmBgHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p3}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;F)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 191 */
} // :cond_0
return;
} // .end method
public void onTrainFinished ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 177 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmBgHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 182 */
return;
} // .end method
public void onTrainIndicatorsFinished ( com.xiaomi.aiautobrt.IndividualTrainEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Lcom/xiaomi/aiautobrt/IndividualTrainEvent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 205 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmBgHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;Lcom/xiaomi/aiautobrt/IndividualTrainEvent;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 206 */
return;
} // .end method
public void onUpdateExperiment ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "expId" # I */
/* .param p2, "enable" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 195 */
v0 = this.this$0;
com.android.server.display.aiautobrt.IndividualBrightnessEngine .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;IZ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 196 */
return;
} // .end method
