public class com.android.server.display.aiautobrt.AppClassifier {
	 /* .source "AppClassifier.java" */
	 /* # static fields */
	 private static final java.lang.String APP_CATEGORY_CONFIG_DIR;
	 public static final Integer CATEGORY_FINANCE_LEARN;
	 public static final Integer CATEGORY_GAME;
	 public static final Integer CATEGORY_MAX;
	 public static final Integer CATEGORY_MUSIC_READ;
	 public static final Integer CATEGORY_NEWS;
	 public static final Integer CATEGORY_PHOTO;
	 public static final Integer CATEGORY_SHOPPING;
	 public static final Integer CATEGORY_SOCIAL;
	 public static final Integer CATEGORY_TRAVEL;
	 public static final Integer CATEGORY_UNDEFINED;
	 public static final Integer CATEGORY_VIDEO;
	 private static final java.lang.String CLOUD_BACKUP_CONFIG_FILE;
	 private static final java.lang.String DEFAULT_CONFIG_FILE;
	 private static final java.lang.String ETC_DIR;
	 private static final java.lang.String TAG;
	 private static final android.util.Singleton sInstance;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/Singleton<", */
	 /* "Lcom/android/server/display/aiautobrt/AppClassifier;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.util.List mAppCategoryInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/aiautobrt/config/PackageInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.HashMap mCachedAppCategoryInfo;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.display.aiautobrt.AppClassifier ( ) {
/* .locals 1 */
/* .line 69 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/AppClassifier$1; */
/* invoke-direct {v0}, Lcom/android/server/display/aiautobrt/AppClassifier$1;-><init>()V */
return;
} // .end method
private com.android.server.display.aiautobrt.AppClassifier ( ) {
/* .locals 1 */
/* .line 80 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 66 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAppCategoryInfo = v0;
/* .line 67 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCachedAppCategoryInfo = v0;
/* .line 81 */
(( com.android.server.display.aiautobrt.AppClassifier ) p0 ).loadAppCategoryConfig ( ); // invoke-virtual {p0}, Lcom/android/server/display/aiautobrt/AppClassifier;->loadAppCategoryConfig()V
/* .line 82 */
return;
} // .end method
 com.android.server.display.aiautobrt.AppClassifier ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/AppClassifier;-><init>()V */
return;
} // .end method
public static java.lang.String categoryToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "category" # I */
/* .line 158 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 179 */
final String v0 = "default"; // const-string v0, "default"
/* .line 160 */
/* :pswitch_0 */
/* const-string/jumbo v0, "social" */
/* .line 170 */
/* :pswitch_1 */
/* const-string/jumbo v0, "travel" */
/* .line 168 */
/* :pswitch_2 */
final String v0 = "photo"; // const-string v0, "photo"
/* .line 172 */
/* :pswitch_3 */
/* const-string/jumbo v0, "shopping" */
/* .line 176 */
/* :pswitch_4 */
final String v0 = "news"; // const-string v0, "news"
/* .line 174 */
/* :pswitch_5 */
final String v0 = "finance_learn"; // const-string v0, "finance_learn"
/* .line 166 */
/* :pswitch_6 */
final String v0 = "music_read"; // const-string v0, "music_read"
/* .line 162 */
/* :pswitch_7 */
/* const-string/jumbo v0, "video" */
/* .line 164 */
/* :pswitch_8 */
final String v0 = "game"; // const-string v0, "game"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static com.android.server.display.aiautobrt.AppClassifier getInstance ( ) {
/* .locals 1 */
/* .line 77 */
v0 = com.android.server.display.aiautobrt.AppClassifier.sInstance;
(( android.util.Singleton ) v0 ).get ( ); // invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/display/aiautobrt/AppClassifier; */
} // .end method
private com.android.server.display.aiautobrt.config.AppCategoryConfig loadConfigFromFile ( ) {
/* .locals 4 */
/* .line 102 */
android.os.Environment .getProductDirectory ( );
final String v1 = "app_brightness_category.xml"; // const-string v1, "app_brightness_category.xml"
final String v2 = "etc"; // const-string v2, "etc"
final String v3 = "displayconfig"; // const-string v3, "displayconfig"
/* filled-new-array {v2, v3, v1}, [Ljava/lang/String; */
/* .line 101 */
android.os.Environment .buildPath ( v0,v1 );
/* .line 106 */
/* .local v0, "defaultFile":Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v2 = "cloud_app_brightness_category.xml"; // const-string v2, "cloud_app_brightness_category.xml"
/* filled-new-array {v3, v2}, [Ljava/lang/String; */
/* .line 105 */
android.os.Environment .buildPath ( v1,v2 );
/* .line 109 */
/* .local v1, "cloudFile":Ljava/io/File; */
/* invoke-direct {p0, v1}, Lcom/android/server/display/aiautobrt/AppClassifier;->readFromConfig(Ljava/io/File;)Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
/* .line 110 */
/* .local v2, "config":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 111 */
/* .line 114 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/AppClassifier;->readFromConfig(Ljava/io/File;)Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
/* .line 115 */
} // .end method
private com.android.server.display.aiautobrt.config.AppCategoryConfig readFromConfig ( java.io.File p0 ) {
/* .locals 4 */
/* .param p1, "configFile" # Ljava/io/File; */
/* .line 122 */
v0 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 123 */
/* .line 126 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedInputStream; */
(( java.io.File ) p1 ).toPath ( ); // invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Ljava/nio/file/OpenOption; */
java.nio.file.Files .newInputStream ( v2,v3 );
/* invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 127 */
/* .local v0, "in":Ljava/io/InputStream; */
try { // :try_start_1
com.android.server.display.aiautobrt.config.XmlParser .read ( v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 128 */
try { // :try_start_2
(( java.io.InputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 127 */
/* .line 126 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.InputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/display/aiautobrt/AppClassifier;
} // .end local p1 # "configFile":Ljava/io/File;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 128 */
} // .end local v0 # "in":Ljava/io/InputStream;
/* .restart local p0 # "this":Lcom/android/server/display/aiautobrt/AppClassifier; */
/* .restart local p1 # "configFile":Ljava/io/File; */
/* :catch_0 */
/* move-exception v0 */
/* .line 129 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 131 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
/* # virtual methods */
public Integer getAppCategoryId ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 139 */
v0 = v0 = this.mAppCategoryInfo;
/* if-nez v0, :cond_4 */
/* if-nez p1, :cond_0 */
/* .line 142 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 143 */
/* .local v0, "category":I */
v1 = this.mCachedAppCategoryInfo;
v1 = (( java.util.HashMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 144 */
v1 = this.mCachedAppCategoryInfo;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 147 */
} // :cond_1
v1 = this.mAppCategoryInfo;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lcom/android/server/display/aiautobrt/config/PackageInfo; */
/* .line 148 */
/* .local v2, "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo; */
(( com.android.server.display.aiautobrt.config.PackageInfo ) v2 ).getName ( ); // invoke-virtual {v2}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->getName()Ljava/lang/String;
v3 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 149 */
v0 = (( com.android.server.display.aiautobrt.config.PackageInfo ) v2 ).getCateId ( ); // invoke-virtual {v2}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->getCateId()I
/* .line 150 */
/* .line 152 */
} // .end local v2 # "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
} // :cond_2
/* .line 153 */
} // :cond_3
} // :goto_1
v1 = this.mCachedAppCategoryInfo;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 154 */
/* .line 140 */
} // .end local v0 # "category":I
} // :cond_4
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void loadAppCategoryConfig ( ) {
/* .locals 5 */
/* .line 85 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/AppClassifier;->loadConfigFromFile()Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
/* .line 86 */
/* .local v0, "config":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 87 */
final String v1 = "CbmController-AppClassifier"; // const-string v1, "CbmController-AppClassifier"
final String v2 = "Update custom app category config."; // const-string v2, "Update custom app category config."
android.util.Slog .d ( v1,v2 );
/* .line 88 */
(( com.android.server.display.aiautobrt.config.AppCategoryConfig ) v0 ).getCategory ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;->getCategory()Ljava/util/List;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/display/aiautobrt/config/AppCategory; */
/* .line 89 */
/* .local v2, "category":Lcom/android/server/display/aiautobrt/config/AppCategory; */
v3 = this.mAppCategoryInfo;
(( com.android.server.display.aiautobrt.config.AppCategory ) v2 ).getPkg ( ); // invoke-virtual {v2}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getPkg()Ljava/util/List;
/* .line 90 */
} // .end local v2 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
/* .line 92 */
} // :cond_0
return;
} // .end method
