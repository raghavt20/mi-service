class com.android.server.display.aiautobrt.CbmStateTracker$ResultRecord {
	 /* .source "CbmStateTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/aiautobrt/CbmStateTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ResultRecord" */
} // .end annotation
/* # instance fields */
private Integer appId;
private Float brightness;
private Float lux;
private Long time;
/* # direct methods */
public com.android.server.display.aiautobrt.CbmStateTracker$ResultRecord ( ) {
/* .locals 0 */
/* .param p1, "lux" # F */
/* .param p2, "appId" # I */
/* .param p3, "brightness" # F */
/* .param p4, "time" # J */
/* .line 435 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 436 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->lux:F */
/* .line 437 */
/* iput p2, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->appId:I */
/* .line 438 */
/* iput p3, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->brightness:F */
/* .line 439 */
/* iput-wide p4, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->time:J */
/* .line 440 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 452 */
/* instance-of v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 453 */
/* .line 455 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
/* .line 456 */
/* .local v0, "record":Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->lux:F */
/* iget v3, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->lux:F */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->appId:I */
/* iget v3, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->appId:I */
/* if-ne v2, v3, :cond_1 */
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->brightness:F */
/* iget v3, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->brightness:F */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 444 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
com.android.server.display.aiautobrt.CbmStateTracker .-$$Nest$sfgetFORMAT ( );
/* new-instance v2, Ljava/util/Date; */
/* iget-wide v3, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->time:J */
/* invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", l: "; // const-string v1, ", l: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->lux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", a: "; // const-string v1, ", a: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->appId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", b: "; // const-string v1, ", b: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->brightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
