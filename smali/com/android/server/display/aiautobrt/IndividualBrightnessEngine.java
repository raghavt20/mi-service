public class com.android.server.display.aiautobrt.IndividualBrightnessEngine {
	 /* .source "IndividualBrightnessEngine.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$ModelBindRecord;, */
	 /* Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$EngineCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_CLIENT_SERVICE;
protected static final java.lang.String MODEL_STATE_BEST_INDICATOR;
protected static final java.lang.String MODEL_STATE_REASON_BACKUP;
protected static final java.lang.String MODEL_STATE_REASON_DEFAULT;
protected static final java.lang.String MODEL_STATE_REASON_FORCED;
protected static final java.lang.String MODEL_STATE_REASON_TRAIN_FINISHED;
protected static final java.lang.String MODEL_STATE_REASON_USER;
private static final java.lang.String TAG;
private static Boolean sDebug;
/* # instance fields */
private final android.os.Handler mBgHandler;
private final android.content.Context mContext;
private com.android.server.display.aiautobrt.CustomPersistentDataStore mDataStore;
private final com.android.server.display.aiautobrt.IndividualBrightnessEngine$EngineCallback mEngineCallback;
private final android.os.Handler mHandler;
private final com.xiaomi.aiautobrt.IIndividualCallback mIndividualCallback;
private final com.android.server.display.aiautobrt.IndividualEventNormalizer mIndividualEventNormalizer;
private final com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord mModelBindRecord;
private final android.content.ComponentName mModelComponent;
private volatile com.xiaomi.aiautobrt.IIndividualBrightnessService mModelService;
private Boolean mModelValid;
private java.lang.String mModelValidStateReason;
private volatile Boolean mModelValidationInProgress;
private volatile Boolean mNeedBindService;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmBgHandler ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBgHandler;
} // .end method
static com.android.server.display.aiautobrt.IndividualBrightnessEngine$EngineCallback -$$Nest$fgetmEngineCallback ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mEngineCallback;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmModelValidationInProgress ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValidationInProgress:Z */
} // .end method
static void -$$Nest$fputmModelValidationInProgress ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValidationInProgress:Z */
	 return;
} // .end method
static void -$$Nest$mattach ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0, android.os.IBinder p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->attach(Landroid/os/IBinder;)V */
	 return;
} // .end method
static void -$$Nest$mdetach ( com.android.server.display.aiautobrt.IndividualBrightnessEngine p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->detach()V */
	 return;
} // .end method
public com.android.server.display.aiautobrt.IndividualBrightnessEngine ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "normalizer" # Lcom/android/server/display/aiautobrt/IndividualEventNormalizer; */
	 /* .param p3, "looper" # Landroid/os/Looper; */
	 /* .param p4, "component" # Landroid/content/ComponentName; */
	 /* .param p5, "callback" # Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$EngineCallback; */
	 /* .param p6, "bgHandler" # Landroid/os/Handler; */
	 /* .line 69 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 61 */
	 final String v0 = "default"; // const-string v0, "default"
	 this.mModelValidStateReason = v0;
	 /* .line 173 */
	 /* new-instance v0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$1;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;)V */
	 this.mIndividualCallback = v0;
	 /* .line 70 */
	 this.mContext = p1;
	 /* .line 71 */
	 this.mIndividualEventNormalizer = p2;
	 /* .line 72 */
	 /* new-instance v0, Landroid/os/Handler; */
	 /* invoke-direct {v0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 73 */
	 this.mBgHandler = p6;
	 /* .line 74 */
	 /* new-instance v0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$ModelBindRecord; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$ModelBindRecord;-><init>(Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$ModelBindRecord-IA;)V */
	 this.mModelBindRecord = v0;
	 /* .line 75 */
	 this.mModelComponent = p4;
	 /* .line 76 */
	 this.mEngineCallback = p5;
	 /* .line 77 */
	 return;
} // .end method
private void attach ( android.os.IBinder p0 ) {
	 /* .locals 4 */
	 /* .param p1, "service" # Landroid/os/IBinder; */
	 /* .line 141 */
	 final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
	 try { // :try_start_0
		 com.xiaomi.aiautobrt.IIndividualBrightnessService$Stub .asInterface ( p1 );
		 this.mModelService = v1;
		 /* .line 142 */
		 v1 = this.mModelService;
		 v2 = this.mModelBindRecord;
		 int v3 = 0; // const/4 v3, 0x0
		 /* .line 143 */
		 v1 = this.mModelService;
		 v2 = this.mIndividualCallback;
		 /* .line 144 */
		 v1 = this.mModelService;
		 int v2 = 2; // const/4 v2, 0x2
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 147 */
		 /* .line 145 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 146 */
		 /* .local v1, "e":Landroid/os/RemoteException; */
		 final String v2 = "Failed to bind service bound!"; // const-string v2, "Failed to bind service bound!"
		 android.util.Slog .e ( v0,v2 );
		 /* .line 148 */
	 } // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
v1 = this.mModelBindRecord;
int v2 = 1; // const/4 v2, 0x1
com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord .-$$Nest$fputmIsBound ( v1,v2 );
/* .line 149 */
final String v1 = "Service has bound successfully."; // const-string v1, "Service has bound successfully."
android.util.Slog .w ( v0,v1 );
/* .line 150 */
return;
} // .end method
private void detach ( ) {
/* .locals 5 */
/* .line 153 */
final String v0 = "Process of service has died, detach from it."; // const-string v0, "Process of service has died, detach from it."
final String v1 = "CbmController-IndividualEngine"; // const-string v1, "CbmController-IndividualEngine"
v2 = this.mModelService;
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 155 */
	 int v2 = 0; // const/4 v2, 0x0
	 try { // :try_start_0
		 v3 = this.mModelService;
		 v4 = this.mModelBindRecord;
		 /* .line 156 */
		 v3 = this.mModelService;
		 /* :try_end_0 */
		 /* .catch Ljava/util/NoSuchElementException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 159 */
		 /* .line 157 */
		 /* :catch_0 */
		 /* move-exception v3 */
		 /* .line 158 */
		 /* .local v3, "e":Ljava/lang/Exception; */
		 android.util.Slog .e ( v1,v0 );
		 /* .line 161 */
	 } // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
v3 = this.mContext;
v4 = this.mModelBindRecord;
(( android.content.Context ) v3 ).unbindService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 162 */
v3 = this.mModelBindRecord;
com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord .-$$Nest$fputmIsBound ( v3,v2 );
/* .line 163 */
int v3 = 0; // const/4 v3, 0x0
this.mModelService = v3;
/* .line 164 */
/* iput-boolean v2, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mNeedBindService:Z */
/* .line 165 */
android.util.Slog .w ( v1,v0 );
/* .line 167 */
} // :cond_0
return;
} // .end method
private void tryToBindModelService ( ) {
/* .locals 7 */
/* .line 119 */
v0 = this.mModelComponent;
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mNeedBindService:Z */
/* if-nez v0, :cond_0 */
/* .line 123 */
} // :cond_0
v0 = this.mModelBindRecord;
v0 = com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord .-$$Nest$fgetmIsBound ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mModelService;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 125 */
	 return;
	 /* .line 127 */
} // :cond_1
/* const-string/jumbo v0, "tryToBindModelService: try to bind model service." */
final String v1 = "CbmController-IndividualEngine"; // const-string v1, "CbmController-IndividualEngine"
android.util.Slog .i ( v1,v0 );
/* .line 128 */
v0 = android.app.ActivityManager .getCurrentUser ( );
/* .line 129 */
/* .local v0, "userId":I */
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "com.miui.aiautobrt.service.AiService"; // const-string v3, "com.miui.aiautobrt.service.AiService"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 131 */
/* .local v2, "intent":Landroid/content/Intent; */
v3 = this.mModelComponent;
(( android.content.Intent ) v2 ).setComponent ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 132 */
/* const/high16 v3, 0x800000 */
(( android.content.Intent ) v2 ).addFlags ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 133 */
v3 = this.mContext;
v4 = this.mModelBindRecord;
/* new-instance v5, Landroid/os/UserHandle; */
/* invoke-direct {v5, v0}, Landroid/os/UserHandle;-><init>(I)V */
/* const v6, 0x4000001 */
v3 = (( android.content.Context ) v3 ).bindServiceAsUser ( v2, v4, v6, v5 ); // invoke-virtual {v3, v2, v4, v6, v5}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
/* if-nez v3, :cond_2 */
/* .line 135 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Unable to bind service: bindService failed "; // const-string v4, "Unable to bind service: bindService failed "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 137 */
} // :cond_2
return;
/* .line 121 */
} // .end local v0 # "userId":I
} // .end local v2 # "intent":Landroid/content/Intent;
} // :cond_3
} // :goto_0
return;
} // .end method
/* # virtual methods */
protected void bindServiceDueToBrightnessAdjust ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "needBindService" # Z */
/* .line 302 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mNeedBindService:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 303 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mNeedBindService:Z */
/* .line 304 */
final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
final String v1 = "Try to bind service due to brightness adjust."; // const-string v1, "Try to bind service due to brightness adjust."
android.util.Slog .d ( v0,v1 );
/* .line 305 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->tryToBindModelService()V */
/* .line 307 */
} // :cond_0
return;
} // .end method
protected void completeModelValidation ( ) {
/* .locals 1 */
/* .line 294 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValidationInProgress:Z */
/* .line 295 */
return;
} // .end method
protected void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 310 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_CBM:Z */
com.android.server.display.aiautobrt.IndividualBrightnessEngine.sDebug = (v0!= 0);
/* .line 311 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mModelService="; // const-string v1, " mModelService="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mModelService;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 312 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isBound="; // const-string v1, " isBound="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mModelBindRecord;
v1 = com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord .-$$Nest$fgetmIsBound ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 313 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mModelValid="; // const-string v1, " mModelValid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 314 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mModelValidStateReason="; // const-string v1, " mModelValidStateReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mModelValidStateReason;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 315 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mModelValidationInProgress="; // const-string v1, " mModelValidationInProgress="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValidationInProgress:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 316 */
return;
} // .end method
protected Boolean isModelValid ( ) {
/* .locals 1 */
/* .line 245 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
} // .end method
protected Boolean isVerificationInProgress ( ) {
/* .locals 1 */
/* .line 298 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValidationInProgress:Z */
} // .end method
public void onBootCompleted ( ) {
/* .locals 2 */
/* .line 170 */
final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
final String v1 = "onBootCompleted: boot completed."; // const-string v1, "onBootCompleted: boot completed."
android.util.Slog .i ( v0,v1 );
/* .line 171 */
return;
} // .end method
public void preparePredictBrightness ( com.xiaomi.aiautobrt.IndividualModelEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
/* .line 104 */
final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
v1 = this.mModelService;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.mModelBindRecord;
v1 = com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord .-$$Nest$fgetmIsBound ( v1 );
/* if-nez v1, :cond_0 */
/* .line 109 */
} // :cond_0
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValidationInProgress:Z */
/* if-nez v1, :cond_1 */
/* .line 110 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preparePredictBrightness: event: "; // const-string v2, "preparePredictBrightness: event: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 112 */
} // :cond_1
v1 = this.mModelService;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 115 */
/* .line 113 */
/* :catch_0 */
/* move-exception v1 */
/* .line 114 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 116 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
/* .line 105 */
} // :cond_2
} // :goto_1
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->tryToBindModelService()V */
/* .line 106 */
return;
} // .end method
protected void setDataStore ( com.android.server.display.aiautobrt.CustomPersistentDataStore p0 ) {
/* .locals 0 */
/* .param p1, "dataStore" # Lcom/android/server/display/aiautobrt/CustomPersistentDataStore; */
/* .line 281 */
this.mDataStore = p1;
/* .line 282 */
return;
} // .end method
protected void setModeValidFromXml ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .line 273 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
/* .line 274 */
final String v0 = "backup"; // const-string v0, "backup"
this.mModelValidStateReason = v0;
/* .line 275 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setModeValidFromXml: model is " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 276 */
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
final String v2 = " valid"; // const-string v2, " valid"
} // :cond_0
final String v2 = " invalid"; // const-string v2, " invalid"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " due to: "; // const-string v2, " due to: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 275 */
final String v1 = "CbmController-IndividualEngine"; // const-string v1, "CbmController-IndividualEngine"
android.util.Slog .d ( v1,v0 );
/* .line 278 */
return;
} // .end method
protected void setModelValid ( Boolean p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "valid" # Z */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 255 */
final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
/* if-nez v1, :cond_1 */
/* .line 256 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
/* .line 257 */
this.mModelValidStateReason = p2;
/* .line 258 */
v2 = this.mDataStore;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 259 */
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v2 ).storeIndividualModelEnabled ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeIndividualModelEnabled(Z)V
/* .line 261 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setModelValid: model is valid due to: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 262 */
} // :cond_1
/* if-nez p1, :cond_3 */
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 263 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->mModelValid:Z */
/* .line 264 */
this.mModelValidStateReason = p2;
/* .line 265 */
v2 = this.mDataStore;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 266 */
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v2 ).storeIndividualModelEnabled ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeIndividualModelEnabled(Z)V
/* .line 268 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setModelInvalid: model is invalid due to: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 270 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void uploadBrightnessModelEvent ( com.xiaomi.aiautobrt.IndividualModelEvent p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
/* .param p2, "enable" # Z */
/* .line 84 */
final String v0 = "CbmController-IndividualEngine"; // const-string v0, "CbmController-IndividualEngine"
v1 = this.mModelService;
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.mModelBindRecord;
v1 = com.android.server.display.aiautobrt.IndividualBrightnessEngine$ModelBindRecord .-$$Nest$fgetmIsBound ( v1 );
/* if-nez v1, :cond_0 */
/* .line 89 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 90 */
try { // :try_start_0
v1 = this.mModelService;
/* .line 91 */
/* sget-boolean v1, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->sDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 92 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "uploadBrightnessModelEvent: event: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 95 */
/* :catch_0 */
/* move-exception v1 */
/* .line 96 */
/* .local v1, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 97 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
/* nop */
/* .line 98 */
} // :goto_1
return;
/* .line 85 */
} // :cond_2
} // :goto_2
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->tryToBindModelService()V */
/* .line 86 */
return;
} // .end method
