public class com.android.server.display.aiautobrt.CustomBrightnessModeController implements com.android.server.display.statistics.BrightnessDataProcessor$ModelEventCallback implements com.android.server.display.MiuiDisplayCloudController$CloudListener implements com.android.server.display.aiautobrt.IndividualBrightnessEngine$EngineCallback {
	 /* .source "CustomBrightnessModeController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;, */
	 /* Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Float BRIGHT_ROOM_AMBIENT_LUX;
public static final java.lang.String CUSTOM_BRIGHTNESS_CURVE_BRIGHTENING;
public static final java.lang.String CUSTOM_BRIGHTNESS_CURVE_DARKENING;
public static final java.lang.String CUSTOM_BRIGHTNESS_CURVE_DEFAULT;
public static final Integer CUSTOM_BRIGHTNESS_MODE_CUSTOM;
public static final Integer CUSTOM_BRIGHTNESS_MODE_INDIVIDUAL;
private static final Integer CUSTOM_BRIGHTNESS_MODE_INVALID;
public static final Integer CUSTOM_BRIGHTNESS_MODE_OFF;
protected static final java.lang.String CUSTOM_CURVE_STATE_REASON_BACKUP;
protected static final java.lang.String CUSTOM_CURVE_STATE_REASON_BEST_INDICATOR;
protected static final java.lang.String CUSTOM_CURVE_STATE_REASON_DEFAULT;
protected static final java.lang.String CUSTOM_CURVE_STATE_REASON_FORCED;
protected static final java.lang.String CUSTOM_CURVE_STATE_REASON_USER;
private static final Float DARK_ROOM_AMBIENT_LUX;
private static final Integer EXPERIMENT_FLAG_CUSTOM_CURVE;
private static final Integer EXPERIMENT_FLAG_INDIVIDUAL_MODEL;
private static final Integer EXPERIMENT_FLAG_INVALID;
private static final Integer FORCE_TRAIN_DISABLE_VALUE;
private static final Integer FORCE_TRAIN_ENABLE_VALUE;
private static final java.text.SimpleDateFormat FORMAT;
private static final java.lang.String KEY_CUSTOM_BRIGHTNESS_MODE;
private static final java.lang.String KEY_FORCE_TRAIN_ENABLE;
private static final Float LUX_GRAD_SMOOTHING;
public static final Integer MAX_CUSTOM_CURVE_VALID_ADJ_TIMES;
private static final Float MAX_GRAD;
private static final Float MIN_PERMISSIBLE_INCREASE;
private static final Float MIN_SCOPE_DELTA_NIT;
private static final Integer MSG_CLEAR_PREDICT_PENDING;
public static final Integer SCENE_STATE_BRIGHTENING;
public static final Integer SCENE_STATE_DARKENING;
public static final Integer SCENE_STATE_DEFAULT;
public static final Integer SCENE_STATE_INVALID;
private static final Float SHORT_TERM_MODEL_THRESHOLD_RATIO;
protected static final java.lang.String TAG;
private static final Integer WAIT_FOR_PREDICT_TIMEOUT;
protected static Boolean sDebug;
/* # instance fields */
private final com.android.server.display.aiautobrt.AppClassifier mAppClassifier;
private Boolean mAutoBrightnessModeEnabled;
private final android.os.Handler mBgHandler;
private android.hardware.display.BrightnessConfiguration mBrighteningConfiguration;
private final com.android.server.display.statistics.BrightnessDataProcessor mBrightnessDataProcessor;
private com.android.server.display.BrightnessMappingStrategy mBrightnessMapper;
private final java.util.Map mBrightnessValidationMapper;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "[F>;" */
/* } */
} // .end annotation
} // .end field
private Integer mCachedCategoryId;
private Integer mCachedLuxIndex;
private Integer mCbmState;
private final com.android.server.display.aiautobrt.CbmStateTracker mCbmStateTracker;
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private Integer mCurrentCustomSceneState;
private Boolean mCustomCurveCloudDisable;
private Boolean mCustomCurveDisabledByUserChange;
private volatile Boolean mCustomCurveExperimentEnable;
private Boolean mCustomCurveValid;
private java.lang.String mCustomCurveValidStateReason;
private android.hardware.display.BrightnessConfiguration mDarkeningConfiguration;
private final com.android.server.display.aiautobrt.CustomPersistentDataStore mDataStore;
private android.hardware.display.BrightnessConfiguration mDefaultConfiguration;
private final mDefaultLuxLevels;
private final mDefaultNitsLevels;
private final com.android.server.display.DisplayDeviceConfig mDisplayDeviceConfig;
private final com.android.server.display.DisplayPowerControllerImpl mDisplayPowerControllerImpl;
private Boolean mForcedCustomCurveEnabled;
private Boolean mForcedIndividualBrightnessEnabled;
private final com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler mHandler;
private final Float mHbmMinimumLux;
private com.android.server.display.aiautobrt.IndividualBrightnessEngine mIndividualBrightnessEngine;
private final android.content.ComponentName mIndividualComponentName;
private android.util.Spline mIndividualDefaultSpline;
private Boolean mIndividualDisabledByUserChange;
private android.util.Spline mIndividualGameSpline;
private Boolean mIndividualModelCloudDisable;
private volatile Boolean mIndividualModelExperimentEnable;
private android.util.Spline mIndividualVideoSpline;
private Boolean mIsCustomCurveValidReady;
private Long mLastModelTrainTimeStamp;
private final Float mMinBrightness;
private android.util.Spline mMinimumBrightnessSpline;
private Long mModelTrainTotalTimes;
private final Float mNormalMaxBrightness;
private java.lang.Runnable mNotifyModelVerificationRunnable;
private Float mPendingIndividualBrightness;
private final com.android.server.display.aiautobrt.CustomBrightnessModeController$SettingsObserver mSettingsObserver;
private final Long mShortTermModelTimeout;
private Boolean mSupportCustomBrightness;
private Boolean mSupportIndividualBrightness;
/* # direct methods */
public static void $r8$lambda$8uv2R5DvFjBVNG8lK-B1HeSFpG8 ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$setScreenBrightnessByUser$3(I)V */
return;
} // .end method
public static void $r8$lambda$CffFlsrMBi0uPvyRRVau8Tgi5R8 ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$getCustomBrightness$2(I)V */
return;
} // .end method
public static void $r8$lambda$i8hMZsSLhnGfUotviHlIQKpbjEM ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0, Integer p1, Float[] p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$isMonotonicModel$5(I[F)V */
return;
} // .end method
public static void $r8$lambda$ioqmpTEEj1h6X-mAuTmiBfSCrpY ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$new$0()V */
return;
} // .end method
public static void $r8$lambda$p-XPH4bLvZVfyHtFhM0XlR4-2xI ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$onValidatedBrightness$4()V */
return;
} // .end method
public static void $r8$lambda$wGYYnLzmDPCrE9Rx92gurvvzCT0 ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->lambda$onBootCompleted$1()V */
return;
} // .end method
static com.android.server.display.statistics.BrightnessDataProcessor -$$Nest$fgetmBrightnessDataProcessor ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBrightnessDataProcessor;
} // .end method
static void -$$Nest$mupdateAutoBrightness ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateAutoBrightness(F)V */
return;
} // .end method
static void -$$Nest$mupdateCustomBrightnessEnabled ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomBrightnessEnabled()V */
return;
} // .end method
static void -$$Nest$mupdateScreenBrightnessMode ( com.android.server.display.aiautobrt.CustomBrightnessModeController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateScreenBrightnessMode()V */
return;
} // .end method
static com.android.server.display.aiautobrt.CustomBrightnessModeController ( ) {
/* .locals 2 */
/* .line 93 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v1 = "MM-dd HH:mm:ss.SSS"; // const-string v1, "MM-dd HH:mm:ss.SSS"
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
return;
} // .end method
public com.android.server.display.aiautobrt.CustomBrightnessModeController ( ) {
/* .locals 24 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .param p3, "config" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .param p4, "dpcImpl" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .param p5, "processor" # Lcom/android/server/display/statistics/BrightnessDataProcessor; */
/* .param p6, "hbmMinimumLux" # F */
/* .param p7, "normalMaxBrightness" # F */
/* .param p8, "minBrightness" # F */
/* .line 220 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
/* move-object/from16 v9, p5 */
/* invoke-direct/range {p0 ..p0}, Ljava/lang/Object;-><init>()V */
/* .line 120 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mBrightnessValidationMapper = v0;
/* .line 156 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* .line 164 */
/* iput v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
/* .line 177 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* iput v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mPendingIndividualBrightness:F */
/* .line 190 */
final String v0 = "default"; // const-string v0, "default"
this.mCustomCurveValidStateReason = v0;
/* .line 202 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
/* .line 203 */
/* iput-boolean v0, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z */
/* .line 205 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, v7}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V */
this.mNotifyModelVerificationRunnable = v0;
/* .line 221 */
this.mContext = v8;
/* .line 222 */
/* move-object/from16 v10, p3 */
this.mDisplayDeviceConfig = v10;
/* .line 223 */
/* move-object/from16 v11, p4 */
this.mDisplayPowerControllerImpl = v11;
/* .line 224 */
this.mBrightnessDataProcessor = v9;
/* .line 225 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v9 ).setModelEventCallback ( v7 ); // invoke-virtual {v9, v7}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setModelEventCallback(Lcom/android/server/display/statistics/BrightnessDataProcessor$ModelEventCallback;)V
/* .line 226 */
/* move/from16 v12, p6 */
/* iput v12, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F */
/* .line 227 */
/* move/from16 v13, p7 */
/* iput v13, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNormalMaxBrightness:F */
/* .line 228 */
/* move/from16 v14, p8 */
/* iput v14, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinBrightness:F */
/* .line 229 */
/* new-instance v15, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler; */
/* move-object/from16 v6, p2 */
/* invoke-direct {v15, v7, v6}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;Landroid/os/Looper;)V */
this.mHandler = v15;
/* .line 230 */
/* new-instance v5, Landroid/os/Handler; */
com.android.internal.os.BackgroundThread .getHandler ( );
(( android.os.Handler ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v5, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mBgHandler = v5;
/* .line 231 */
com.android.server.display.aiautobrt.AppClassifier .getInstance ( );
this.mAppClassifier = v0;
/* .line 232 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/CbmStateTracker; */
/* invoke-direct {v0, v8, v5, v15, v7}, Lcom/android/server/display/aiautobrt/CbmStateTracker;-><init>(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Handler;Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V */
this.mCbmStateTracker = v0;
/* .line 234 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* .line 235 */
/* .local v4, "resources":Landroid/content/res/Resources; */
/* const v0, 0x110f00a6 */
(( android.content.res.Resources ) v4 ).getString ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 238 */
/* .local v3, "componentName":Ljava/lang/String; */
/* const v0, 0x107001a */
(( android.content.res.Resources ) v4 ).getIntArray ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getIntArray(I)[I
/* invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getLuxLevels([I)[F */
this.mDefaultLuxLevels = v2;
/* .line 240 */
/* const v0, 0x1070014 */
(( android.content.res.Resources ) v4 ).obtainTypedArray ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F */
this.mDefaultNitsLevels = v0;
/* .line 242 */
/* const v0, 0x1103003a */
(( android.content.res.Resources ) v4 ).getIntArray ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getIntArray(I)[I
/* invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getLuxLevels([I)[F */
/* .line 244 */
/* .local v1, "customDefaultLuxLevels":[F */
/* const v0, 0x1103003b */
(( android.content.res.Resources ) v4 ).obtainTypedArray ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* invoke-direct {v7, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F */
/* .line 246 */
/* .local v0, "customDefaultNitsLevels":[F */
/* move-object/from16 v16, v2 */
/* const v2, 0x11030034 */
(( android.content.res.Resources ) v4 ).obtainTypedArray ( v2 ); // invoke-virtual {v4, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* invoke-direct {v7, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F */
/* .line 248 */
/* .local v2, "brighteningNitsLevels":[F */
/* move-object/from16 v17, v5 */
/* const v5, 0x11030039 */
(( android.content.res.Resources ) v4 ).obtainTypedArray ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* invoke-direct {v7, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F */
/* .line 250 */
/* .local v5, "darkeningNitsLevels":[F */
/* const v6, 0x11030041 */
(( android.content.res.Resources ) v4 ).obtainTypedArray ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* invoke-direct {v7, v6}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F */
/* .line 252 */
/* .local v6, "minLuxLevel":[F */
/* const v8, 0x11030042 */
(( android.content.res.Resources ) v4 ).obtainTypedArray ( v8 ); // invoke-virtual {v4, v8}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* invoke-direct {v7, v8}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getFloatArray(Landroid/content/res/TypedArray;)[F */
/* .line 254 */
/* .local v8, "minNitsLevel":[F */
/* const v10, 0x10e0024 */
v10 = (( android.content.res.Resources ) v4 ).getInteger ( v10 ); // invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getInteger(I)I
/* int-to-long v10, v10 */
/* iput-wide v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mShortTermModelTimeout:J */
/* .line 256 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* const v11, 0x11050033 */
v10 = (( android.content.res.Resources ) v10 ).getBoolean ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z */
/* .line 258 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources; */
/* const v11, 0x11050017 */
v10 = (( android.content.res.Resources ) v10 ).getBoolean ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v10, v7, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportCustomBrightness:Z */
/* .line 262 */
v10 = /* invoke-direct {v7, v6, v8}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z */
if ( v10 != null) { // if-eqz v10, :cond_0
/* .line 263 */
android.util.Spline .createSpline ( v6,v8 );
this.mMinimumBrightnessSpline = v10;
/* .line 267 */
} // :cond_0
v10 = /* invoke-direct {v7, v1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z */
if ( v10 != null) { // if-eqz v10, :cond_1
/* .line 268 */
v10 = /* invoke-direct {v7, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z */
if ( v10 != null) { // if-eqz v10, :cond_1
	 /* .line 269 */
	 v10 = 	 /* invoke-direct {v7, v1, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidMapping([F[F)Z */
	 if ( v10 != null) { // if-eqz v10, :cond_1
		 /* .line 270 */
		 final String v10 = "custom_brightness_curve_default"; // const-string v10, "custom_brightness_curve_default"
		 /* invoke-direct {v7, v1, v0, v10}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration; */
		 this.mDefaultConfiguration = v10;
		 /* .line 271 */
		 final String v10 = "custom_brightness_curve_brightening"; // const-string v10, "custom_brightness_curve_brightening"
		 /* invoke-direct {v7, v1, v2, v10}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration; */
		 this.mBrighteningConfiguration = v10;
		 /* .line 272 */
		 final String v10 = "custom_brightness_curve_darkening"; // const-string v10, "custom_brightness_curve_darkening"
		 /* invoke-direct {v7, v1, v5, v10}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->build([F[FLjava/lang/String;)Landroid/hardware/display/BrightnessConfiguration; */
		 this.mDarkeningConfiguration = v10;
		 /* .line 275 */
	 } // :cond_1
	 v10 = this.mDefaultConfiguration;
	 v11 = this.mDarkeningConfiguration;
	 /* move-object/from16 v18, v0 */
} // .end local v0 # "customDefaultNitsLevels":[F
/* .local v18, "customDefaultNitsLevels":[F */
v0 = this.mBrighteningConfiguration;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v9 ).setBrightnessConfiguration ( v10, v11, v0 ); // invoke-virtual {v9, v10, v11, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;Landroid/hardware/display/BrightnessConfiguration;Landroid/hardware/display/BrightnessConfiguration;)V
/* .line 278 */
/* invoke-direct {v7, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getIndividualComponent(Ljava/lang/String;)Landroid/content/ComponentName; */
this.mIndividualComponentName = v10;
/* .line 279 */
/* new-instance v11, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine; */
v0 = this.mIndividualEventNormalizer;
/* move-object/from16 v19, v0 */
/* move-object v0, v11 */
/* move-object/from16 v20, v1 */
} // .end local v1 # "customDefaultLuxLevels":[F
/* .local v20, "customDefaultLuxLevels":[F */
/* move-object/from16 v1, p1 */
/* move-object/from16 v21, v8 */
/* move-object/from16 v8, v16 */
/* move-object/from16 v16, v2 */
} // .end local v2 # "brighteningNitsLevels":[F
} // .end local v8 # "minNitsLevel":[F
/* .local v16, "brighteningNitsLevels":[F */
/* .local v21, "minNitsLevel":[F */
/* move-object/from16 v2, v19 */
/* move-object/from16 v19, v3 */
} // .end local v3 # "componentName":Ljava/lang/String;
/* .local v19, "componentName":Ljava/lang/String; */
/* move-object/from16 v3, p2 */
/* move-object/from16 v22, v4 */
} // .end local v4 # "resources":Landroid/content/res/Resources;
/* .local v22, "resources":Landroid/content/res/Resources; */
/* move-object v4, v10 */
/* move-object/from16 v10, v17 */
/* move-object/from16 v17, v5 */
} // .end local v5 # "darkeningNitsLevels":[F
/* .local v17, "darkeningNitsLevels":[F */
/* move-object/from16 v5, p0 */
/* move-object/from16 v23, v6 */
} // .end local v6 # "minLuxLevel":[F
/* .local v23, "minLuxLevel":[F */
/* move-object v6, v10 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;-><init>(Landroid/content/Context;Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;Landroid/os/Looper;Landroid/content/ComponentName;Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine$EngineCallback;Landroid/os/Handler;)V */
this.mIndividualBrightnessEngine = v11;
/* .line 283 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver; */
/* invoke-direct {v0, v7, v15}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$SettingsObserver;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 284 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
this.mContentResolver = v0;
/* .line 285 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore; */
/* invoke-direct {v0, v7, v8}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;[F)V */
this.mDataStore = v0;
/* .line 286 */
v1 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v1 ).setDataStore ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setDataStore(Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;)V
/* .line 287 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->registerSettingsObserver()V */
/* .line 288 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->loadSettings()V */
/* .line 289 */
return;
} // .end method
private android.hardware.display.BrightnessConfiguration build ( Float[] p0, Float[] p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "luxLevels" # [F */
/* .param p2, "brightnessLevelsNits" # [F */
/* .param p3, "description" # Ljava/lang/String; */
/* .line 869 */
/* new-instance v0, Landroid/hardware/display/BrightnessConfiguration$Builder; */
/* invoke-direct {v0, p1, p2}, Landroid/hardware/display/BrightnessConfiguration$Builder;-><init>([F[F)V */
/* .line 871 */
/* .local v0, "builder":Landroid/hardware/display/BrightnessConfiguration$Builder; */
/* iget-wide v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mShortTermModelTimeout:J */
(( android.hardware.display.BrightnessConfiguration$Builder ) v0 ).setShortTermModelTimeoutMillis ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setShortTermModelTimeoutMillis(J)Landroid/hardware/display/BrightnessConfiguration$Builder;
/* .line 872 */
/* const v1, 0x3f19999a # 0.6f */
(( android.hardware.display.BrightnessConfiguration$Builder ) v0 ).setShortTermModelLowerLuxMultiplier ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setShortTermModelLowerLuxMultiplier(F)Landroid/hardware/display/BrightnessConfiguration$Builder;
/* .line 873 */
(( android.hardware.display.BrightnessConfiguration$Builder ) v0 ).setShortTermModelUpperLuxMultiplier ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setShortTermModelUpperLuxMultiplier(F)Landroid/hardware/display/BrightnessConfiguration$Builder;
/* .line 874 */
(( android.hardware.display.BrightnessConfiguration$Builder ) v0 ).setDescription ( p3 ); // invoke-virtual {v0, p3}, Landroid/hardware/display/BrightnessConfiguration$Builder;->setDescription(Ljava/lang/String;)Landroid/hardware/display/BrightnessConfiguration$Builder;
/* .line 875 */
(( android.hardware.display.BrightnessConfiguration$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration$Builder;->build()Landroid/hardware/display/BrightnessConfiguration;
} // .end method
private void buildIndividualSpline ( Integer p0, Float[] p1 ) {
/* .locals 2 */
/* .param p1, "category" # I */
/* .param p2, "nitsArray" # [F */
/* .line 823 */
v0 = this.mDefaultLuxLevels;
android.util.Spline .createSpline ( v0,p2 );
/* .line 824 */
/* .local v0, "spline":Landroid/util/Spline; */
/* if-nez p1, :cond_0 */
/* .line 825 */
this.mIndividualDefaultSpline = v0;
/* .line 826 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_1 */
/* .line 827 */
this.mIndividualGameSpline = v0;
/* .line 828 */
} // :cond_1
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_2 */
/* .line 829 */
this.mIndividualVideoSpline = v0;
/* .line 831 */
} // :cond_2
} // :goto_0
return;
} // .end method
private getFloatArray ( android.content.res.TypedArray p0 ) {
/* .locals 4 */
/* .param p1, "array" # Landroid/content/res/TypedArray; */
/* .line 938 */
v0 = (( android.content.res.TypedArray ) p1 ).length ( ); // invoke-virtual {p1}, Landroid/content/res/TypedArray;->length()I
/* .line 939 */
/* .local v0, "N":I */
/* new-array v1, v0, [F */
/* .line 940 */
/* .local v1, "values":[F */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 941 */
/* const/high16 v3, -0x40800000 # -1.0f */
v3 = (( android.content.res.TypedArray ) p1 ).getFloat ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F
/* aput v3, v1, v2 */
/* .line 940 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 943 */
} // .end local v2 # "i":I
} // :cond_0
(( android.content.res.TypedArray ) p1 ).recycle ( ); // invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V
/* .line 944 */
} // .end method
private Float getIndividualBrightness ( java.lang.String p0, Float p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "lux" # F */
/* .line 835 */
v0 = this.mAppClassifier;
v0 = (( com.android.server.display.aiautobrt.AppClassifier ) v0 ).getAppCategoryId ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I
/* .line 836 */
/* .local v0, "category":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 837 */
v1 = this.mIndividualGameSpline;
/* .local v1, "spline":Landroid/util/Spline; */
/* .line 838 */
} // .end local v1 # "spline":Landroid/util/Spline;
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
/* .line 839 */
v1 = this.mIndividualVideoSpline;
/* .restart local v1 # "spline":Landroid/util/Spline; */
/* .line 841 */
} // .end local v1 # "spline":Landroid/util/Spline;
} // :cond_1
v1 = this.mIndividualDefaultSpline;
/* .line 843 */
/* .restart local v1 # "spline":Landroid/util/Spline; */
} // :goto_0
v2 = this.mDisplayDeviceConfig;
v3 = (( android.util.Spline ) v1 ).interpolate ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/Spline;->interpolate(F)F
v2 = (( com.android.server.display.DisplayDeviceConfig ) v2 ).getBrightnessFromNit ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 844 */
/* .local v2, "individualBrt":F */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getIndividualBrightness: category: "; // const-string v4, "getIndividualBrightness: category: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", lux: "; // const-string v4, ", lux: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", individualBrt: "; // const-string v4, ", individualBrt: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "CbmController"; // const-string v4, "CbmController"
android.util.Slog .d ( v4,v3 );
/* .line 847 */
} // .end method
private android.content.ComponentName getIndividualComponent ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 340 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 341 */
android.content.ComponentName .unflattenFromString ( p1 );
/* .line 343 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private getLuxLevels ( Integer[] p0 ) {
/* .locals 4 */
/* .param p1, "lux" # [I */
/* .line 949 */
/* array-length v0, p1 */
/* add-int/lit8 v0, v0, 0x1 */
/* new-array v0, v0, [F */
/* .line 950 */
/* .local v0, "levels":[F */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v1, v2, :cond_0 */
/* .line 951 */
/* add-int/lit8 v2, v1, 0x1 */
/* aget v3, p1, v1 */
/* int-to-float v3, v3 */
/* aput v3, v0, v2 */
/* .line 950 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 953 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
private Integer getSceneState ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "orientation" # I */
/* .line 468 */
java.time.LocalDateTime .now ( );
v0 = (( java.time.LocalDateTime ) v0 ).getHour ( ); // invoke-virtual {v0}, Ljava/time/LocalDateTime;->getHour()I
/* .line 469 */
/* .local v0, "clock":I */
v1 = this.mAppClassifier;
v1 = (( com.android.server.display.aiautobrt.AppClassifier ) v1 ).getAppCategoryId ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I
/* .line 470 */
/* .local v1, "category":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 471 */
/* .local v2, "scene":I */
v3 = /* invoke-direct {p0, v1, p2, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBrighteningScene(III)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 472 */
int v2 = 1; // const/4 v2, 0x1
/* .line 473 */
} // :cond_0
v3 = /* invoke-direct {p0, v1, p2, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isDarkeningScene(III)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 474 */
int v2 = 2; // const/4 v2, 0x2
/* .line 477 */
} // :cond_1
} // :goto_0
/* iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
/* if-eq v3, v2, :cond_2 */
/* .line 478 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getSceneState: category: "; // const-string v4, "getSceneState: category: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.display.aiautobrt.AppClassifier .categoryToString ( v1 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", scene state: "; // const-string v4, ", scene state: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", packageName: "; // const-string v4, ", packageName: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", clock: "; // const-string v4, ", clock: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "CbmController"; // const-string v4, "CbmController"
android.util.Slog .d ( v4,v3 );
/* .line 483 */
} // :cond_2
} // .end method
private Boolean hasSceneStateChanged ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "orientation" # I */
/* .line 452 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getSceneState(Ljava/lang/String;I)I */
/* .line 453 */
/* .local v0, "state":I */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
/* if-eq v1, v0, :cond_0 */
/* .line 454 */
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
/* .line 455 */
int v1 = 1; // const/4 v1, 0x1
/* .line 457 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isBelowMinimumSlope ( Float[] p0 ) {
/* .locals 8 */
/* .param p1, "nits" # [F */
/* .line 1029 */
v0 = this.mDefaultLuxLevels;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_6
/* array-length v2, p1 */
/* array-length v0, v0 */
/* if-eq v2, v0, :cond_0 */
/* .line 1032 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1033 */
/* .local v0, "index":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mDefaultLuxLevels;
/* array-length v4, v3 */
/* const/high16 v5, 0x3f800000 # 1.0f */
/* if-ge v2, v4, :cond_2 */
/* .line 1034 */
/* move v0, v2 */
/* .line 1035 */
/* aget v4, v3, v2 */
/* iget v6, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F */
/* sub-float/2addr v6, v5 */
/* cmpl-float v4, v4, v6 */
/* if-lez v4, :cond_1 */
/* .line 1036 */
/* .line 1033 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1039 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_1
/* if-le v0, v1, :cond_5 */
/* add-int/lit8 v2, v0, -0x1 */
/* aget v2, v3, v2 */
int v4 = 0; // const/4 v4, 0x0
/* aget v6, v3, v4 */
/* cmpl-float v2, v2, v6 */
/* if-nez v2, :cond_3 */
/* .line 1042 */
} // :cond_3
/* add-int/lit8 v2, v0, -0x1 */
/* aget v2, p1, v2 */
/* aget v7, p1, v4 */
/* sub-float/2addr v2, v7 */
/* add-int/lit8 v7, v0, -0x1 */
/* aget v3, v3, v7 */
/* sub-float/2addr v3, v6 */
/* div-float/2addr v2, v3 */
/* .line 1044 */
/* .local v2, "minScope":F */
/* iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F */
/* sub-float/2addr v3, v5 */
/* const/high16 v5, 0x42c80000 # 100.0f */
/* div-float/2addr v5, v3 */
/* .line 1045 */
/* .local v5, "permittedMinScope":F */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "isBelowMinimumSlope: minScope: "; // const-string v6, "isBelowMinimumSlope: minScope: "
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v6 = ", permittedMinScope: "; // const-string v6, ", permittedMinScope: "
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "CbmController"; // const-string v6, "CbmController"
android.util.Slog .d ( v6,v3 );
/* .line 1046 */
/* cmpg-float v3, v2, v5 */
/* if-gtz v3, :cond_4 */
} // :cond_4
/* move v1, v4 */
} // :goto_2
/* .line 1040 */
} // .end local v2 # "minScope":F
} // .end local v5 # "permittedMinScope":F
} // :cond_5
} // :goto_3
/* .line 1030 */
} // .end local v0 # "index":I
} // :cond_6
} // :goto_4
} // .end method
private Boolean isBelowMinimumSpline ( Float[] p0 ) {
/* .locals 5 */
/* .param p1, "nits" # [F */
/* .line 1016 */
v0 = this.mMinimumBrightnessSpline;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mDefaultLuxLevels;
if ( v0 != null) { // if-eqz v0, :cond_3
/* array-length v2, p1 */
/* array-length v0, v0 */
/* if-eq v2, v0, :cond_0 */
/* .line 1020 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v2 = this.mDefaultLuxLevels;
/* array-length v3, v2 */
/* if-ge v0, v3, :cond_2 */
/* .line 1021 */
/* aget v3, p1, v0 */
v4 = this.mMinimumBrightnessSpline;
/* aget v2, v2, v0 */
v2 = (( android.util.Spline ) v4 ).interpolate ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/Spline;->interpolate(F)F
/* cmpg-float v2, v3, v2 */
/* if-gez v2, :cond_1 */
/* .line 1022 */
/* .line 1020 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1025 */
} // .end local v0 # "i":I
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 1018 */
} // :cond_3
} // :goto_1
} // .end method
private Boolean isBrightRoomAdjustment ( Float p0, java.lang.String p1, Integer p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "orientation" # I */
/* .param p4, "isBrightening" # Z */
/* .line 1111 */
java.time.LocalDateTime .now ( );
v0 = (( java.time.LocalDateTime ) v0 ).getHour ( ); // invoke-virtual {v0}, Ljava/time/LocalDateTime;->getHour()I
/* .line 1112 */
/* .local v0, "clock":I */
v1 = this.mAppClassifier;
v1 = (( com.android.server.display.aiautobrt.AppClassifier ) v1 ).getAppCategoryId ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/display/aiautobrt/AppClassifier;->getAppCategoryId(Ljava/lang/String;)I
/* .line 1113 */
/* .local v1, "category":I */
if ( p4 != null) { // if-eqz p4, :cond_0
/* const/high16 v2, 0x44160000 # 600.0f */
/* cmpl-float v2, p1, v2 */
/* if-ltz v2, :cond_0 */
/* .line 1114 */
v2 = /* invoke-direct {p0, v1, p3, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBrighteningScene(III)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 1113 */
} // :goto_0
} // .end method
private Boolean isBrighteningScene ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "category" # I */
/* .param p2, "orientation" # I */
/* .param p3, "clock" # I */
/* .line 490 */
int v0 = 7; // const/4 v0, 0x7
int v1 = 1; // const/4 v1, 0x1
/* if-eq p1, v0, :cond_3 */
/* .line 491 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isNoonTime(I)Z */
/* if-nez v0, :cond_0 */
/* .line 492 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isDayTime(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 493 */
} // :cond_0
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isHorizontalScreen(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-eq p1, v1, :cond_3 */
/* const/16 v0, 0x9 */
/* if-eq p1, v0, :cond_3 */
/* .line 496 */
} // :cond_1
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isVerticalScreen(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_2 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :cond_3
} // :goto_0
/* nop */
/* .line 490 */
} // :goto_1
} // .end method
private Boolean isCustomAllowed ( ) {
/* .locals 1 */
/* .line 414 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportCustomBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
/* if-nez v0, :cond_0 */
v0 = this.mBrightnessMapper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveCloudDisable:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isDarkRoomAdjustment ( Float p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "lux" # F */
/* .param p2, "isBrightening" # Z */
/* .line 1106 */
/* const/high16 v0, 0x42c80000 # 100.0f */
/* cmpg-float v0, p1, v0 */
/* if-gtz v0, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isDarkeningScene ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "category" # I */
/* .param p2, "orientation" # I */
/* .param p3, "clock" # I */
/* .line 504 */
int v0 = 4; // const/4 v0, 0x4
/* if-eq p1, v0, :cond_3 */
/* .line 505 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isVerticalScreen(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 506 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isNightTime(I)Z */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v2, 0x9 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* if-eq p1, v0, :cond_3 */
/* if-eq p1, v2, :cond_3 */
/* if-eq p1, v1, :cond_3 */
/* .line 510 */
} // :cond_0
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isEarlyMorningTime(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-eq p1, v2, :cond_3 */
/* if-eq p1, v1, :cond_3 */
/* .line 513 */
} // :cond_1
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isNoonTime(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-ne p1, v2, :cond_2 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :cond_3
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 504 */
} // :goto_1
} // .end method
private Boolean isDayTime ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "clock" # I */
/* .line 908 */
/* const/16 v0, 0x8 */
/* if-le p1, v0, :cond_0 */
/* const/16 v0, 0xa */
/* if-le p1, v0, :cond_1 */
} // :cond_0
/* const/16 v0, 0xc */
/* if-le p1, v0, :cond_2 */
/* const/16 v0, 0x14 */
/* if-gt p1, v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isEarlyMorningTime ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "clock" # I */
/* .line 900 */
int v0 = 4; // const/4 v0, 0x4
/* if-le p1, v0, :cond_0 */
/* const/16 v0, 0x8 */
/* if-gt p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isHorizontalScreen ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "orientation" # I */
/* .line 892 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_1 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne p1, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
} // .end method
private Boolean isIndividualAllowed ( ) {
/* .locals 1 */
/* .line 424 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mIndividualComponentName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
/* if-nez v0, :cond_0 */
v0 = this.mIndividualBrightnessEngine;
/* .line 427 */
v0 = (( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).isModelValid ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelCloudDisable:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 430 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isValidIndividualSpline()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 424 */
} // :goto_0
} // .end method
private Boolean isMonotonic ( Float[] p0 ) {
/* .locals 5 */
/* .param p1, "x" # [F */
/* .line 1001 */
/* array-length v0, p1 */
int v1 = 2; // const/4 v1, 0x2
int v2 = 0; // const/4 v2, 0x0
/* if-ge v0, v1, :cond_0 */
/* .line 1002 */
/* .line 1004 */
} // :cond_0
/* aget v0, p1, v2 */
/* .line 1005 */
/* .local v0, "prev":F */
int v1 = 1; // const/4 v1, 0x1
/* .local v1, "i":I */
} // :goto_0
/* array-length v3, p1 */
/* if-ge v1, v3, :cond_3 */
/* .line 1006 */
/* aget v3, p1, v1 */
/* .line 1007 */
/* .local v3, "curr":F */
/* cmpg-float v4, v3, v0 */
/* if-ltz v4, :cond_2 */
int v4 = 0; // const/4 v4, 0x0
/* cmpg-float v4, v0, v4 */
/* if-gtz v4, :cond_1 */
/* .line 1010 */
} // :cond_1
/* move v0, v3 */
/* .line 1005 */
} // .end local v3 # "curr":F
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1008 */
/* .restart local v3 # "curr":F */
} // :cond_2
} // :goto_1
/* .line 1012 */
} // .end local v1 # "i":I
} // .end local v3 # "curr":F
} // :cond_3
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean isMonotonicModel ( ) {
/* .locals 9 */
/* .line 760 */
v0 = this.mBrightnessValidationMapper;
v1 = } // :goto_0
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_4
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 761 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[F>;" */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 762 */
/* .local v3, "key":I */
/* check-cast v4, [F */
/* .line 763 */
/* .local v4, "array":[F */
/* invoke-direct {p0, v4}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->smoothCurve([F)[F */
/* .line 764 */
/* .local v5, "newArray":[F */
int v6 = 0; // const/4 v6, 0x0
/* if-nez v5, :cond_0 */
/* .line 765 */
/* .line 767 */
} // :cond_0
v7 = /* invoke-direct {p0, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isMonotonic([F)Z */
final String v8 = "CbmController"; // const-string v8, "CbmController"
/* if-nez v7, :cond_1 */
/* .line 768 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Model is not monotonic, brightness spline: "; // const-string v2, "Model is not monotonic, brightness spline: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v8,v0 );
/* .line 769 */
/* .line 771 */
} // :cond_1
v7 = /* invoke-direct {p0, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBelowMinimumSpline([F)Z */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 772 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Model brightness spline is below the minimum brightness spline, brightness spline: "; // const-string v2, "Model brightness spline is below the minimum brightness spline, brightness spline: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 773 */
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 772 */
android.util.Slog .e ( v8,v0 );
/* .line 774 */
/* .line 776 */
} // :cond_2
v6 = /* invoke-direct {p0, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBelowMinimumSlope([F)Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 777 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Model brightness spline is below the minimum scope, brightness spline: "; // const-string v6, "Model brightness spline is below the minimum scope, brightness spline: "
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 778 */
java.util.Arrays .toString ( v5 );
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 777 */
android.util.Slog .e ( v8,v0 );
/* .line 779 */
/* .line 781 */
} // :cond_3
v2 = this.mHandler;
/* new-instance v6, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda5; */
/* invoke-direct {v6, p0, v3, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I[F)V */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler ) v2 ).post ( v6 ); // invoke-virtual {v2, v6}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->post(Ljava/lang/Runnable;)Z
/* .line 782 */
v2 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v2 ).storeIndividualSpline ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeIndividualSpline(I[F)V
/* .line 783 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[F>;"
} // .end local v3 # "key":I
} // .end local v4 # "array":[F
} // .end local v5 # "newArray":[F
/* goto/16 :goto_0 */
/* .line 784 */
} // :cond_4
} // .end method
private Boolean isNightTime ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "clock" # I */
/* .line 896 */
/* if-ltz p1, :cond_0 */
int v0 = 4; // const/4 v0, 0x4
/* if-le p1, v0, :cond_1 */
} // :cond_0
/* const/16 v0, 0x14 */
/* if-le p1, v0, :cond_2 */
/* const/16 v0, 0x17 */
/* if-gt p1, v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isNoonTime ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "clock" # I */
/* .line 904 */
/* const/16 v0, 0xa */
/* if-le p1, v0, :cond_0 */
/* const/16 v0, 0xc */
/* if-gt p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isValidIndividualSpline ( ) {
/* .locals 1 */
/* .line 851 */
v0 = this.mIndividualDefaultSpline;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mIndividualGameSpline;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mIndividualVideoSpline;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isValidMapping ( Float[] p0, Float[] p1 ) {
/* .locals 6 */
/* .param p1, "x" # [F */
/* .param p2, "y" # [F */
/* .line 912 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_9
if ( p2 != null) { // if-eqz p2, :cond_9
/* array-length v1, p1 */
if ( v1 != null) { // if-eqz v1, :cond_9
/* array-length v1, p2 */
/* if-nez v1, :cond_0 */
/* .line 915 */
} // :cond_0
/* array-length v1, p1 */
/* array-length v2, p2 */
/* if-eq v1, v2, :cond_1 */
/* .line 916 */
/* .line 918 */
} // :cond_1
/* array-length v1, p1 */
/* .line 919 */
/* .local v1, "N":I */
/* aget v2, p1, v0 */
/* .line 920 */
/* .local v2, "prevX":F */
/* aget v3, p2, v0 */
/* .line 921 */
/* .local v3, "prevY":F */
int v4 = 0; // const/4 v4, 0x0
/* cmpg-float v5, v2, v4 */
/* if-ltz v5, :cond_8 */
/* cmpg-float v4, v3, v4 */
/* if-ltz v4, :cond_8 */
v4 = java.lang.Float .isNaN ( v2 );
/* if-nez v4, :cond_8 */
v4 = java.lang.Float .isNaN ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 924 */
} // :cond_2
int v4 = 1; // const/4 v4, 0x1
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v1, :cond_7 */
/* .line 925 */
/* aget v5, p1, v4 */
/* cmpl-float v5, v2, v5 */
/* if-gez v5, :cond_6 */
/* aget v5, p2, v4 */
/* cmpl-float v5, v3, v5 */
/* if-lez v5, :cond_3 */
/* .line 928 */
} // :cond_3
/* aget v5, p1, v4 */
v5 = java.lang.Float .isNaN ( v5 );
/* if-nez v5, :cond_5 */
/* aget v5, p2, v4 */
v5 = java.lang.Float .isNaN ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 931 */
} // :cond_4
/* aget v2, p1, v4 */
/* .line 932 */
/* aget v3, p2, v4 */
/* .line 924 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 929 */
} // :cond_5
} // :goto_1
/* .line 926 */
} // :cond_6
} // :goto_2
/* .line 934 */
} // .end local v4 # "i":I
} // :cond_7
int v0 = 1; // const/4 v0, 0x1
/* .line 922 */
} // :cond_8
} // :goto_3
/* .line 913 */
} // .end local v1 # "N":I
} // .end local v2 # "prevX":F
} // .end local v3 # "prevY":F
} // :cond_9
} // :goto_4
} // .end method
private Boolean isVerticalScreen ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "orientation" # I */
/* .line 888 */
if ( p1 != null) { // if-eqz p1, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
static void lambda$dump$6 ( java.io.PrintWriter p0, java.lang.Integer p1, Float[] p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "k" # Ljava/lang/Integer; */
/* .param p2, "v" # [F */
/* .line 1200 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{cateId=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", brtSpl="; // const-string v1, ", brtSpl="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1201 */
java.util.Arrays .toString ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1200 */
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
return;
} // .end method
private void lambda$getCustomBrightness$2 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "finalNewCbmState" # I */
/* .line 381 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->noteBrightnessAdjustTimesToAggregate(IZ)V */
return;
} // .end method
private void lambda$isMonotonicModel$5 ( Integer p0, Float[] p1 ) { //synthethic
/* .locals 0 */
/* .param p1, "key" # I */
/* .param p2, "newArray" # [F */
/* .line 781 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildIndividualSpline(I[F)V */
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 2 */
/* .line 206 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
/* .line 207 */
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
/* .line 208 */
v0 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).completeModelValidation ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->completeModelValidation()V
/* .line 209 */
final String v0 = "CbmController"; // const-string v0, "CbmController"
final String v1 = "Model cannot complete verification due to predict time out."; // const-string v1, "Model cannot complete verification due to predict time out."
android.util.Slog .e ( v0,v1 );
/* .line 210 */
return;
} // .end method
private void lambda$onBootCompleted$1 ( ) { //synthethic
/* .locals 1 */
/* .line 348 */
v0 = this.mContext;
com.android.server.display.aiautobrt.UserBrightnessStatsJob .scheduleJob ( v0 );
return;
} // .end method
private void lambda$onValidatedBrightness$4 ( ) { //synthethic
/* .locals 3 */
/* .line 688 */
v0 = this.mIndividualBrightnessEngine;
int v1 = 1; // const/4 v1, 0x1
/* const-string/jumbo v2, "train_finished" */
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).setModelValid ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V
/* .line 690 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V */
/* .line 691 */
/* const-string/jumbo v0, "user_operate" */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).setCustomCurveValid ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V
/* .line 692 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).disableIndividualEngine ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V
/* .line 693 */
v0 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v0 ).startWrite ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V
/* .line 694 */
return;
} // .end method
private void lambda$setScreenBrightnessByUser$3 ( Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "finalCbmState" # I */
/* .line 568 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->noteBrightnessAdjustTimesToAggregate(IZ)V */
return;
} // .end method
private void loadSettings ( ) {
/* .locals 0 */
/* .line 301 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomBrightnessEnabled()V */
/* .line 302 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateScreenBrightnessMode()V */
/* .line 303 */
return;
} // .end method
private void noteBrightnessAdjustTimesToAggregate ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "cbmState" # I */
/* .param p2, "isManuallySet" # Z */
/* .line 1132 */
v0 = this.mCbmStateTracker;
v0 = (( com.android.server.display.aiautobrt.CbmStateTracker ) v0 ).isBrightnessAdjustNoted ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->isBrightnessAdjustNoted(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1133 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).aggregateCbmBrightnessAdjustTimes ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmBrightnessAdjustTimes(IZ)V
/* .line 1135 */
} // :cond_0
return;
} // .end method
private Float permissibleRatio ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "currLux" # F */
/* .param p2, "prevLux" # F */
/* .line 818 */
/* const/high16 v0, 0x3e800000 # 0.25f */
/* add-float v1, p1, v0 */
/* add-float/2addr v0, p2 */
/* div-float/2addr v1, v0 */
/* const/high16 v0, 0x3f800000 # 1.0f */
v0 = android.util.MathUtils .pow ( v1,v0 );
} // .end method
private void registerSettingsObserver ( ) {
/* .locals 5 */
/* .line 292 */
v0 = this.mContentResolver;
/* .line 293 */
final String v1 = "custom_brightness_mode"; // const-string v1, "custom_brightness_mode"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 292 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 295 */
v0 = this.mContentResolver;
/* .line 296 */
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 295 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 298 */
return;
} // .end method
private void resetCustomCurveValidConditions ( ) {
/* .locals 1 */
/* .line 1122 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z */
/* .line 1123 */
v0 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v0 ).resetBrtAdjSceneCount ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->resetBrtAdjSceneCount()V
/* .line 1124 */
return;
} // .end method
private void setPendingToWaitPredict ( Float p0 ) {
/* .locals 4 */
/* .param p1, "newAutoBrightness" # F */
/* .line 862 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->removeMessages(I)V
/* .line 863 */
v0 = this.mHandler;
/* .line 864 */
v2 = java.lang.Float .floatToIntBits ( p1 );
java.lang.Integer .valueOf ( v2 );
/* .line 863 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 865 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0xc8 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 866 */
return;
} // .end method
private smoothCurve ( Float[] p0 ) {
/* .locals 9 */
/* .param p1, "nitsArray" # [F */
/* .line 788 */
v0 = this.mDefaultLuxLevels;
/* array-length v1, v0 */
v2 = this.mDefaultNitsLevels;
/* array-length v2, v2 */
/* if-ne v1, v2, :cond_3 */
v1 = this.mDisplayDeviceConfig;
/* if-nez v1, :cond_0 */
/* .line 794 */
} // :cond_0
/* array-length v2, p1 */
/* new-array v2, v2, [F */
/* .line 795 */
/* .local v2, "newNitsArray":[F */
int v3 = 0; // const/4 v3, 0x0
/* aget v0, v0, v3 */
/* .line 796 */
/* .local v0, "preLux":F */
/* aget v4, p1, v3 */
v1 = (( com.android.server.display.DisplayDeviceConfig ) v1 ).getBrightnessFromNit ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* iget v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mMinBrightness:F */
/* iget v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNormalMaxBrightness:F */
v1 = android.util.MathUtils .constrain ( v1,v4,v5 );
/* .line 798 */
/* .local v1, "preBrightness":F */
v4 = this.mDisplayDeviceConfig;
v4 = (( com.android.server.display.DisplayDeviceConfig ) v4 ).getNitsFromBacklight ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* aput v4, v2, v3 */
/* .line 799 */
int v3 = 1; // const/4 v3, 0x1
/* .local v3, "i":I */
} // :goto_0
v4 = this.mDefaultLuxLevels;
/* array-length v5, v4 */
/* if-ge v3, v5, :cond_2 */
/* .line 800 */
/* aget v5, v4, v3 */
/* iget v6, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mHbmMinimumLux:F */
/* const/high16 v7, 0x3f800000 # 1.0f */
/* sub-float/2addr v6, v7 */
/* cmpg-float v5, v5, v6 */
/* if-gtz v5, :cond_1 */
/* .line 801 */
/* aget v4, v4, v3 */
/* .line 802 */
/* .local v4, "currentLux":F */
v5 = this.mDisplayDeviceConfig;
/* aget v6, p1, v3 */
v5 = (( com.android.server.display.DisplayDeviceConfig ) v5 ).getBrightnessFromNit ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 803 */
/* .local v5, "currentBrightness":F */
/* nop */
/* .line 804 */
v6 = /* invoke-direct {p0, v4, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->permissibleRatio(FF)F */
/* mul-float/2addr v6, v1 */
/* const v7, 0x3b83126f # 0.004f */
/* add-float/2addr v7, v1 */
/* .line 803 */
v6 = android.util.MathUtils .max ( v6,v7 );
/* iget v7, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mNormalMaxBrightness:F */
v6 = android.util.MathUtils .min ( v6,v7 );
/* .line 806 */
/* .local v6, "maxBrightness":F */
v7 = android.util.MathUtils .constrain ( v5,v1,v6 );
/* .line 807 */
/* .local v7, "newBrightness":F */
/* move v0, v4 */
/* .line 808 */
/* move v1, v7 */
/* .line 809 */
v8 = this.mDisplayDeviceConfig;
v8 = (( com.android.server.display.DisplayDeviceConfig ) v8 ).getNitsFromBacklight ( v7 ); // invoke-virtual {v8, v7}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* aput v8, v2, v3 */
/* .line 810 */
} // .end local v4 # "currentLux":F
} // .end local v5 # "currentBrightness":F
} // .end local v6 # "maxBrightness":F
/* .line 811 */
} // .end local v7 # "newBrightness":F
} // :cond_1
v4 = this.mDefaultNitsLevels;
/* aget v4, v4, v3 */
/* aput v4, v2, v3 */
/* .line 799 */
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 814 */
} // .end local v3 # "i":I
} // :cond_2
/* .line 790 */
} // .end local v0 # "preLux":F
} // .end local v1 # "preBrightness":F
} // .end local v2 # "newNitsArray":[F
} // :cond_3
} // :goto_2
final String v0 = "CbmController"; // const-string v0, "CbmController"
final String v1 = "Can not smooth individual curve!"; // const-string v1, "Can not smooth individual curve!"
android.util.Slog .e ( v0,v1 );
/* .line 791 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void updateAutoBrightness ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 857 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mPendingIndividualBrightness:F */
/* .line 858 */
v0 = this.mDisplayPowerControllerImpl;
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).updateAutoBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateAutoBrightness()V
/* .line 859 */
return;
} // .end method
private void updateCbmState ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "cbmState" # I */
/* .line 401 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* .line 402 */
/* .local v0, "oldState":I */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* .line 403 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v1 */
/* .line 404 */
/* .local v1, "now":J */
int v3 = -1; // const/4 v3, -0x1
/* if-eq v0, v3, :cond_0 */
/* .line 405 */
v4 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v4 ).noteStopCbmStateTracking ( v0, v1, v2 ); // invoke-virtual {v4, v0, v1, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteStopCbmStateTracking(IJ)V
/* .line 407 */
} // :cond_0
/* iget v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* if-eq v4, v3, :cond_1 */
/* .line 408 */
v3 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v3 ).noteStartCbmStateTracking ( p1, v1, v2 ); // invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteStartCbmStateTracking(IJ)V
/* .line 410 */
} // :cond_1
return;
} // .end method
private void updateCustomBrightnessEnabled ( ) {
/* .locals 6 */
/* .line 306 */
v0 = this.mContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "custom_brightness_mode"; // const-string v2, "custom_brightness_mode"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
/* .line 308 */
/* .local v0, "value":I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* move v2, v3 */
/* .line 309 */
/* .local v2, "forcedEnableCustomCurve":Z */
} // :goto_0
/* iget-boolean v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedCustomCurveEnabled:Z */
final String v5 = "forced_operate"; // const-string v5, "forced_operate"
/* if-eq v4, v2, :cond_1 */
/* .line 310 */
/* iput-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedCustomCurveEnabled:Z */
/* .line 311 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).setCustomCurveValid ( v2, v5 ); // invoke-virtual {p0, v2, v5}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V
/* .line 313 */
} // :cond_1
int v4 = 2; // const/4 v4, 0x2
/* if-ne v0, v4, :cond_2 */
} // :cond_2
/* move v1, v3 */
/* .line 314 */
/* .local v1, "forcedEnableModel":Z */
} // :goto_1
/* iget-boolean v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z */
/* if-eq v4, v1, :cond_3 */
/* .line 315 */
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z */
/* .line 316 */
v4 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v4 ).setModelValid ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V
/* .line 319 */
} // :cond_3
/* if-nez v2, :cond_4 */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 320 */
} // :cond_4
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V */
/* .line 322 */
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 323 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).disableIndividualEngine ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V
/* .line 325 */
} // :cond_6
return;
} // .end method
private void updateScreenBrightnessMode ( ) {
/* .locals 4 */
/* .line 328 */
v0 = this.mContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "screen_brightness_mode"; // const-string v2, "screen_brightness_mode"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAutoBrightnessModeEnabled:Z */
/* .line 332 */
return;
} // .end method
private void uploadValidatedEventToModel ( ) {
/* .locals 9 */
/* .line 744 */
v0 = this.mDefaultLuxLevels;
/* if-nez v0, :cond_0 */
/* .line 745 */
return;
/* .line 747 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_2 */
/* .line 748 */
/* aget v0, v0, v1 */
/* .line 749 */
/* .local v0, "lux":F */
v3 = this.mBrightnessDataProcessor;
/* iget v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = -1; // const/4 v8, -0x1
/* move v4, v0 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->createModelEvent(FIFII)Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
/* .line 752 */
/* .local v1, "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
v2 = (( com.xiaomi.aiautobrt.IndividualModelEvent ) v1 ).isValidRawEvent ( ); // invoke-virtual {v1}, Lcom/xiaomi/aiautobrt/IndividualModelEvent;->isValidRawEvent()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 753 */
v2 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v2 ).preparePredictBrightness ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->preparePredictBrightness(Lcom/xiaomi/aiautobrt/IndividualModelEvent;)V
/* .line 755 */
} // :cond_1
v2 = this.mBgHandler;
v3 = this.mNotifyModelVerificationRunnable;
/* const-wide/16 v4, 0xc8 */
(( android.os.Handler ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 757 */
} // .end local v0 # "lux":F
} // .end local v1 # "event":Lcom/xiaomi/aiautobrt/IndividualModelEvent;
} // :cond_2
return;
} // .end method
/* # virtual methods */
protected void buildConfigurationFromXml ( Integer p0, Float[] p1 ) {
/* .locals 1 */
/* .param p1, "category" # I */
/* .param p2, "nits" # [F */
/* .line 1155 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isMonotonic([F)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1156 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildIndividualSpline(I[F)V */
/* .line 1158 */
} // :cond_0
return;
} // .end method
protected void customCurveConditionsSatisfied ( ) {
/* .locals 2 */
/* .line 589 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z */
/* if-nez v0, :cond_0 */
/* .line 590 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z */
/* .line 591 */
final String v0 = "CbmController"; // const-string v0, "CbmController"
final String v1 = "Satisfy valid conditions of custom curve."; // const-string v1, "Satisfy valid conditions of custom curve."
android.util.Slog .d ( v0,v1 );
/* .line 593 */
} // :cond_0
return;
} // .end method
protected void disableCustomCurve ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "disable" # Z */
/* .line 523 */
int v0 = 0; // const/4 v0, 0x0
/* .line 524 */
/* .local v0, "changed":Z */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
/* if-nez v1, :cond_0 */
/* .line 525 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
/* .line 527 */
v1 = this.mDisplayPowerControllerImpl;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.display.DisplayPowerControllerImpl ) v1 ).setBrightnessConfiguration ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V
/* .line 528 */
int v0 = 1; // const/4 v0, 0x1
/* .line 529 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 530 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
/* .line 531 */
int v0 = 1; // const/4 v0, 0x1
/* .line 533 */
} // :cond_1
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 534 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 535 */
final String v2 = "disable "; // const-string v2, "disable "
} // :cond_2
final String v2 = "enable "; // const-string v2, "enable "
} // :goto_1
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "custom curve."; // const-string v2, "custom curve."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 534 */
final String v2 = "CbmController"; // const-string v2, "CbmController"
android.util.Slog .d ( v2,v1 );
/* .line 537 */
} // :cond_3
return;
} // .end method
protected void disableIndividualEngine ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "disable" # Z */
/* .line 540 */
int v0 = 0; // const/4 v0, 0x0
/* .line 541 */
/* .local v0, "changed":Z */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
/* if-nez v1, :cond_0 */
/* .line 542 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
/* .line 543 */
int v0 = 1; // const/4 v0, 0x1
/* .line 544 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 545 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
/* .line 546 */
int v0 = 1; // const/4 v0, 0x1
/* .line 548 */
} // :cond_1
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 549 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 550 */
final String v2 = "disable "; // const-string v2, "disable "
} // :cond_2
final String v2 = "enable "; // const-string v2, "enable "
} // :goto_1
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "individual engine."; // const-string v2, "individual engine."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 549 */
final String v2 = "CbmController"; // const-string v2, "CbmController"
android.util.Slog .d ( v2,v1 );
/* .line 552 */
} // :cond_3
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1173 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_CBM:Z */
com.android.server.display.aiautobrt.CustomBrightnessModeController.sDebug = (v0!= 0);
/* .line 1174 */
v1 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v1 ).forceReportTrainDataEnabled ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->forceReportTrainDataEnabled(Z)V
/* .line 1175 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportCustomBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1176 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCustomCurveDisabledByUserChange="; // const-string v1, " mCustomCurveDisabledByUserChange="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveDisabledByUserChange:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1177 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mForcedCustomCurveEnabled="; // const-string v1, " mForcedCustomCurveEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedCustomCurveEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1178 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentSceneState="; // const-string v1, " mCurrentSceneState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1179 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDefaultConfiguration="; // const-string v1, " mDefaultConfiguration="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDefaultConfiguration;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1180 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBrighteningConfiguration="; // const-string v1, " mBrighteningConfiguration="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBrighteningConfiguration;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1181 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDarkeningConfiguration="; // const-string v1, " mDarkeningConfiguration="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDarkeningConfiguration;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1182 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCustomCurveValidStateReason="; // const-string v1, " mCustomCurveValidStateReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCustomCurveValidStateReason;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1183 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCustomCurveValid="; // const-string v1, " mCustomCurveValid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1184 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsCustomCurveValidReady="; // const-string v1, " mIsCustomCurveValidReady="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1185 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCustomCurveExperimentEnable="; // const-string v1, " mCustomCurveExperimentEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1188 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1189 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mForcedIndividualBrightnessEnabled="; // const-string v1, " mForcedIndividualBrightnessEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1190 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualDisabledByUserChange="; // const-string v1, " mIndividualDisabledByUserChange="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualDisabledByUserChange:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1191 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mPendingIndividualBrightness="; // const-string v1, " mPendingIndividualBrightness="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mPendingIndividualBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1192 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMinimumBrightnessSpline="; // const-string v1, " mMinimumBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mMinimumBrightnessSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1193 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualModelExperimentEnable="; // const-string v1, " mIndividualModelExperimentEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1194 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLastModelTrainTimeStamp="; // const-string v1, " mLastModelTrainTimeStamp="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = com.android.server.display.aiautobrt.CustomBrightnessModeController.FORMAT;
/* new-instance v2, Ljava/util/Date; */
/* iget-wide v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mLastModelTrainTimeStamp:J */
/* invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1195 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mModelTrainTotalTimes="; // const-string v1, " mModelTrainTotalTimes="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mModelTrainTotalTimes:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1196 */
v0 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->dump(Ljava/io/PrintWriter;)V
/* .line 1197 */
v0 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->dump(Ljava/io/PrintWriter;)V
/* .line 1198 */
v0 = v0 = this.mBrightnessValidationMapper;
/* if-nez v0, :cond_1 */
/* .line 1199 */
final String v0 = " Brt validation set:"; // const-string v0, " Brt validation set:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1200 */
v0 = this.mBrightnessValidationMapper;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda6; */
/* invoke-direct {v1, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda6;-><init>(Ljava/io/PrintWriter;)V */
/* .line 1203 */
} // :cond_1
final String v0 = " Individual Model Spline:"; // const-string v0, " Individual Model Spline:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1204 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualDefaultSpline="; // const-string v1, " mIndividualDefaultSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIndividualDefaultSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1205 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualGameSpline="; // const-string v1, " mIndividualGameSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIndividualGameSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1206 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualVideoSpline="; // const-string v1, " mIndividualVideoSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIndividualVideoSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1207 */
v0 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->dump(Ljava/io/PrintWriter;)V
/* .line 1209 */
} // :cond_2
return;
} // .end method
public Integer getCurrentSceneState ( ) {
/* .locals 1 */
/* .line 1118 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
} // .end method
public Float getCustomBrightness ( Float p0, java.lang.String p1, Integer p2, Float p3, Float p4, Integer p5, Boolean p6 ) {
/* .locals 6 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "category" # I */
/* .param p4, "oldAutoBrightness" # F */
/* .param p5, "newAutoBrightness" # F */
/* .param p6, "orientation" # I */
/* .param p7, "isManuallySet" # Z */
/* .line 354 */
/* move v0, p5 */
/* .line 356 */
/* .local v0, "customBrightness":F */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* .line 357 */
/* .local v1, "newCbmState":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 358 */
/* .local v2, "notCount":Z */
v3 = /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isIndividualAllowed()Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 359 */
v0 = /* invoke-direct {p0, p2, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->getIndividualBrightness(Ljava/lang/String;F)F */
/* .line 360 */
int v1 = 2; // const/4 v1, 0x2
/* .line 361 */
} // :cond_0
v3 = /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isCustomAllowed()Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 362 */
v3 = this.mBrightnessMapper;
v0 = (( com.android.server.display.BrightnessMappingStrategy ) v3 ).getBrightness ( p1, p2, p3 ); // invoke-virtual {v3, p1, p2, p3}, Lcom/android/server/display/BrightnessMappingStrategy;->getBrightness(FLjava/lang/String;I)F
/* .line 363 */
int v1 = 1; // const/4 v1, 0x1
/* .line 365 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 368 */
} // :goto_0
/* iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* if-eq v1, v3, :cond_2 */
/* .line 369 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCbmState(I)V */
/* .line 375 */
} // :cond_2
/* if-nez v2, :cond_3 */
/* if-nez p7, :cond_3 */
/* cmpl-float v3, v0, p4 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 377 */
v3 = java.lang.Float .isNaN ( p4 );
/* if-nez v3, :cond_3 */
/* .line 378 */
v3 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v3 ).noteAutoAdjustmentTimes ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteAutoAdjustmentTimes(I)V
/* .line 380 */
/* move v3, v1 */
/* .line 381 */
/* .local v3, "finalNewCbmState":I */
v4 = this.mBgHandler;
/* new-instance v5, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda4; */
/* invoke-direct {v5, p0, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I)V */
(( android.os.Handler ) v4 ).post ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 384 */
} // .end local v3 # "finalNewCbmState":I
} // :cond_3
/* sget-boolean v3, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->sDebug:Z */
/* if-nez v3, :cond_4 */
/* cmpl-float v3, v0, p5 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* cmpl-float v3, v0, p4 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 386 */
} // :cond_4
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getCustomBrightness: previous: "; // const-string v4, "getCustomBrightness: previous: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", config: "; // const-string v4, ", config: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p5 ); // invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", custom: "; // const-string v4, ", custom: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", packageName: "; // const-string v4, ", packageName: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", orientation: "; // const-string v4, ", orientation: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p6 ); // invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "CbmController"; // const-string v4, "CbmController"
android.util.Slog .i ( v4,v3 );
/* .line 393 */
} // :cond_5
} // .end method
protected void noteBrightnessUsageToAggregate ( Float p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "duration" # F */
/* .param p2, "cbmState" # I */
/* .line 1143 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).aggregateCbmBrightnessUsageDuration ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateCbmBrightnessUsageDuration(FI)V
/* .line 1144 */
return;
} // .end method
protected void notePredictDurationToAggregate ( Long p0 ) {
/* .locals 2 */
/* .param p1, "duration" # J */
/* .line 1151 */
v0 = this.mBrightnessDataProcessor;
/* long-to-float v1, p1 */
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).aggregateModelAvgPredictDuration ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateModelAvgPredictDuration(F)V
/* .line 1152 */
return;
} // .end method
public void onAbTestExperimentUpdated ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "expId" # I */
/* .param p2, "flag" # I */
/* .line 728 */
int v0 = -1; // const/4 v0, -0x1
int v1 = 1; // const/4 v1, 0x1
/* if-ne p2, v0, :cond_0 */
/* .line 729 */
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z */
/* .line 730 */
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
/* .line 732 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* if-ne p2, v1, :cond_1 */
/* move v2, v1 */
} // :cond_1
/* move v2, v0 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z */
/* .line 733 */
int v2 = 2; // const/4 v2, 0x2
/* if-ne p2, v2, :cond_2 */
} // :cond_2
/* move v1, v0 */
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
/* .line 735 */
} // :goto_2
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).updateExpId ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateExpId(I)V
/* .line 736 */
return;
} // .end method
public void onBootCompleted ( ) {
/* .locals 2 */
/* .line 347 */
v0 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).onBootCompleted ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->onBootCompleted()V
/* .line 348 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 349 */
return;
} // .end method
public void onBrightnessModelEvent ( com.xiaomi.aiautobrt.IndividualModelEvent p0 ) {
/* .locals 2 */
/* .param p1, "modelEvent" # Lcom/xiaomi/aiautobrt/IndividualModelEvent; */
/* .line 620 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mAutoBrightnessModeEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mIndividualComponentName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 622 */
v0 = this.mIndividualBrightnessEngine;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).uploadBrightnessModelEvent ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->uploadBrightnessModelEvent(Lcom/xiaomi/aiautobrt/IndividualModelEvent;Z)V
/* .line 625 */
} // :cond_0
return;
} // .end method
public void onCloudUpdated ( Long p0, java.util.Map p1 ) {
/* .locals 7 */
/* .param p1, "summary" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 880 */
/* .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;" */
/* const-wide/16 v0, 0x8 */
/* and-long/2addr v0, p1 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 881 */
v0 = this.mAppClassifier;
(( com.android.server.display.aiautobrt.AppClassifier ) v0 ).loadAppCategoryConfig ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/AppClassifier;->loadAppCategoryConfig()V
/* .line 883 */
} // :cond_0
/* const-wide/16 v0, 0x10 */
/* and-long/2addr v0, p1 */
/* cmp-long v0, v0, v2 */
int v1 = 1; // const/4 v1, 0x1
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v1 */
} // :cond_1
/* move v0, v4 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveCloudDisable:Z */
/* .line 884 */
/* const-wide/16 v5, 0x20 */
/* and-long/2addr v5, p1 */
/* cmp-long v0, v5, v2 */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
/* move v1, v4 */
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelCloudDisable:Z */
/* .line 885 */
return;
} // .end method
public void onExperimentUpdated ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "expId" # I */
/* .param p2, "enable" # Z */
/* .line 713 */
/* iput-boolean p2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIndividualModelExperimentEnable:Z */
/* .line 714 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).updateExpId ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->updateExpId(I)V
/* .line 715 */
return;
} // .end method
public void onPredictFinished ( Float p0, Integer p1, Float p2 ) {
/* .locals 6 */
/* .param p1, "lux" # F */
/* .param p2, "appId" # I */
/* .param p3, "brightness" # F */
/* .line 630 */
v0 = java.lang.Float .isNaN ( p3 );
final String v1 = ", brt: "; // const-string v1, ", brt: "
final String v2 = ", appId: "; // const-string v2, ", appId: "
final String v3 = "CbmController"; // const-string v3, "CbmController"
/* if-nez v0, :cond_1 */
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v0, p3, v0 */
/* if-gez v0, :cond_0 */
/* .line 636 */
} // :cond_0
v0 = this.mHandler;
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler ) v0 ).removeMessages ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->removeMessages(I)V
/* .line 637 */
v0 = this.mDisplayDeviceConfig;
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getBrightnessFromNit ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
/* .line 638 */
/* .local v0, "dbv":F */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onPredictFinished: lux: "; // const-string v5, "onPredictFinished: lux: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", dbv: "; // const-string v2, ", dbv: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 642 */
v1 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v1 ).noteIndividualResult ( p1, p2, v0 ); // invoke-virtual {v1, p1, p2, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteIndividualResult(FIF)V
/* .line 644 */
v1 = this.mCbmStateTracker;
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
(( com.android.server.display.aiautobrt.CbmStateTracker ) v1 ).noteStopPredictTracking ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteStopPredictTracking(J)V
/* .line 645 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateAutoBrightness(F)V */
/* .line 646 */
return;
/* .line 631 */
} // .end local v0 # "dbv":F
} // :cond_1
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Predict error: lux: "; // const-string v4, "Predict error: lux: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 634 */
return;
} // .end method
public void onTrainIndicatorsFinished ( com.xiaomi.aiautobrt.IndividualTrainEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Lcom/xiaomi/aiautobrt/IndividualTrainEvent; */
/* .line 723 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).aggregateModelTrainIndicators ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateModelTrainIndicators(Lcom/xiaomi/aiautobrt/IndividualTrainEvent;)V
/* .line 724 */
return;
} // .end method
public void onValidatedBrightness ( Float p0 ) {
/* .locals 7 */
/* .param p1, "brightness" # F */
/* .line 666 */
v0 = this.mBgHandler;
v1 = this.mNotifyModelVerificationRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 667 */
v0 = this.mIndividualBrightnessEngine;
v0 = (( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).isVerificationInProgress ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isVerificationInProgress()Z
if ( v0 != null) { // if-eqz v0, :cond_6
v0 = this.mDefaultLuxLevels;
if ( v0 != null) { // if-eqz v0, :cond_6
/* array-length v1, v0 */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_2 */
/* .line 672 */
} // :cond_0
/* array-length v0, v0 */
/* new-array v0, v0, [F */
/* .line 673 */
/* .local v0, "nitsArray":[F */
v1 = this.mBrightnessValidationMapper;
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
v1 = java.lang.Integer .valueOf ( v2 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 674 */
v1 = this.mBrightnessValidationMapper;
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
java.lang.Integer .valueOf ( v2 );
/* move-object v0, v1 */
/* check-cast v0, [F */
/* .line 676 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
/* aput p1, v0, v1 */
/* .line 677 */
v1 = this.mBrightnessValidationMapper;
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
java.lang.Integer .valueOf ( v2 );
/* .line 678 */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v1, v2 */
/* iput v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
/* .line 679 */
v3 = this.mDefaultLuxLevels;
/* array-length v4, v3 */
/* if-ge v1, v4, :cond_2 */
/* .line 680 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->uploadValidatedEventToModel()V */
/* .line 681 */
} // :cond_2
/* array-length v4, v3 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 2; // const/4 v6, 0x2
/* if-ne v1, v4, :cond_3 */
/* iget v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
/* if-ge v4, v6, :cond_3 */
/* .line 682 */
/* iput v5, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
/* .line 683 */
/* add-int/2addr v4, v2 */
/* iput v4, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
/* .line 684 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->uploadValidatedEventToModel()V */
/* .line 685 */
} // :cond_3
/* array-length v3, v3 */
/* if-ne v1, v3, :cond_5 */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
/* if-ne v1, v6, :cond_5 */
/* .line 686 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isMonotonicModel()Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 687 */
v1 = this.mHandler;
/* new-instance v3, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController$CbmHandler ) v1 ).post ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$CbmHandler;->post(Ljava/lang/Runnable;)Z
/* .line 696 */
v1 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v1 ).aggregateIndividualModelTrainTimes ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateIndividualModelTrainTimes(Z)V
/* .line 699 */
} // :cond_4
v1 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v1 ).aggregateIndividualModelTrainTimes ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateIndividualModelTrainTimes(Z)V
/* .line 700 */
final String v1 = "CbmController"; // const-string v1, "CbmController"
final String v2 = "Model cannot complete verification due to non-monotonicity."; // const-string v2, "Model cannot complete verification due to non-monotonicity."
android.util.Slog .e ( v1,v2 );
/* .line 702 */
} // :goto_0
v1 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v1 ).completeModelValidation ( ); // invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->completeModelValidation()V
/* .line 704 */
} // :cond_5
} // :goto_1
return;
/* .line 670 */
} // .end local v0 # "nitsArray":[F
} // :cond_6
} // :goto_2
return;
} // .end method
public void resetShortTermModel ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "manually" # Z */
/* .line 600 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* const-string/jumbo v1, "user_operate" */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mIndividualBrightnessEngine;
/* .line 601 */
v0 = (( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).isModelValid ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z
/* if-nez v0, :cond_0 */
/* if-nez p1, :cond_0 */
/* .line 603 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).setCustomCurveValid ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V
/* .line 604 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V */
/* .line 605 */
v0 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v0 ).startWrite ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V
/* .line 606 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* if-nez v0, :cond_1 */
v0 = this.mIndividualBrightnessEngine;
v0 = (( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).isModelValid ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 607 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).setCustomCurveValid ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V
/* .line 608 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V */
/* .line 609 */
v2 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v2 ).setModelValid ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V
/* .line 610 */
v0 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v0 ).startWrite ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V
/* .line 612 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void setAutoBrightnessComponent ( com.android.server.display.BrightnessMappingStrategy p0 ) {
/* .locals 0 */
/* .param p1, "mapper" # Lcom/android/server/display/BrightnessMappingStrategy; */
/* .line 518 */
this.mBrightnessMapper = p1;
/* .line 519 */
return;
} // .end method
protected void setCustomCurveEnabledFromXml ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 1161 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* .line 1162 */
final String v0 = "backup"; // const-string v0, "backup"
this.mCustomCurveValidStateReason = v0;
/* .line 1163 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setCustomCurveEnabledFromXml: custom curve is" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1164 */
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = " valid"; // const-string v1, " valid"
} // :cond_0
final String v1 = " invalid"; // const-string v1, " invalid"
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " due to: "; // const-string v1, " due to: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCustomCurveValidStateReason;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1163 */
final String v1 = "CbmController"; // const-string v1, "CbmController"
android.util.Slog .d ( v1,v0 );
/* .line 1166 */
return;
} // .end method
public void setCustomCurveEnabledOnCommand ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 1091 */
v0 = this.mContentResolver;
/* .line 1092 */
/* nop */
/* .line 1091 */
final String v1 = "custom_brightness_mode"; // const-string v1, "custom_brightness_mode"
android.provider.Settings$Secure .putInt ( v0,v1,p1 );
/* .line 1093 */
return;
} // .end method
protected void setCustomCurveValid ( Boolean p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 1070 */
final String v0 = "CbmController"; // const-string v0, "CbmController"
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* if-nez v1, :cond_0 */
/* .line 1071 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* .line 1072 */
this.mCustomCurveValidStateReason = p2;
/* .line 1073 */
v2 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v2 ).storeCustomCurveEnabled ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeCustomCurveEnabled(Z)V
/* .line 1074 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setCustomCurveValid: custom curve is valid due to " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1075 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1076 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* .line 1077 */
int v2 = -1; // const/4 v2, -0x1
/* iput v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
/* .line 1078 */
this.mCustomCurveValidStateReason = p2;
/* .line 1080 */
v2 = this.mDisplayPowerControllerImpl;
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).setBrightnessConfiguration ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayPowerControllerImpl;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V
/* .line 1081 */
v2 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v2 ).storeCustomCurveEnabled ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->storeCustomCurveEnabled(Z)V
/* .line 1082 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setCustomCurveValid: custom curve is invalid due to " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1084 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setForceTrainEnabledOnCommand ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 1101 */
v0 = this.mContentResolver;
/* .line 1102 */
/* nop */
/* .line 1101 */
final String v1 = "force_train_enable"; // const-string v1, "force_train_enable"
android.provider.Settings$Secure .putInt ( v0,v1,p1 );
/* .line 1103 */
return;
} // .end method
protected void setIndividualModelEnabledFromXml ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .line 1169 */
v0 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).setModeValidFromXml ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModeValidFromXml(Z)V
/* .line 1170 */
return;
} // .end method
public void setIndividualModelEnabledOnCommand ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 1096 */
v0 = this.mContentResolver;
/* .line 1097 */
if ( p1 != null) { // if-eqz p1, :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1096 */
} // :goto_0
final String v2 = "custom_brightness_mode"; // const-string v2, "custom_brightness_mode"
android.provider.Settings$Secure .putInt ( v0,v2,v1 );
/* .line 1098 */
return;
} // .end method
public void setScreenBrightnessByUser ( Float p0, Boolean p1, java.lang.String p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "userDataPoint" # F */
/* .param p2, "isBrightening" # Z */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "orientation" # I */
/* .line 557 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mSupportIndividualBrightness:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 558 */
v0 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).bindServiceDueToBrightnessAdjust ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->bindServiceDueToBrightnessAdjust(Z)V
/* .line 562 */
} // :cond_0
v0 = this.mIndividualBrightnessEngine;
v0 = (( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v0 ).isModelValid ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mForcedIndividualBrightnessEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 563 */
} // :cond_1
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).disableIndividualEngine ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V
/* .line 565 */
} // :cond_2
v0 = this.mCbmStateTracker;
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
(( com.android.server.display.aiautobrt.CbmStateTracker ) v0 ).noteManualAdjustmentTimes ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(I)V
/* .line 567 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* .line 568 */
/* .local v0, "finalCbmState":I */
v2 = this.mBgHandler;
/* new-instance v3, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda7; */
/* invoke-direct {v3, p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;I)V */
(( android.os.Handler ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 571 */
v2 = this.mIndividualBrightnessEngine;
v2 = (( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v2 ).isModelValid ( ); // invoke-virtual {v2}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->isModelValid()Z
/* if-nez v2, :cond_4 */
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
/* if-nez v2, :cond_4 */
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveValid:Z */
/* if-nez v2, :cond_4 */
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mIsCustomCurveValidReady:Z */
/* if-nez v2, :cond_4 */
/* iget-boolean v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCustomCurveExperimentEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 575 */
v2 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isDarkRoomAdjustment(FZ)Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 576 */
v2 = this.mCbmStateTracker;
/* iget v3, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
(( com.android.server.display.aiautobrt.CbmStateTracker ) v2 ).noteManualAdjustmentTimes ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(II)V
/* .line 578 */
v1 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v1 ).startEvaluateCustomCurve ( ); // invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->startEvaluateCustomCurve()V
/* .line 579 */
} // :cond_3
v1 = /* invoke-direct {p0, p1, p3, p4, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isBrightRoomAdjustment(FLjava/lang/String;IZ)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 581 */
v1 = this.mCbmStateTracker;
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCbmState:I */
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.display.aiautobrt.CbmStateTracker ) v1 ).noteManualAdjustmentTimes ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(II)V
/* .line 583 */
v1 = this.mCbmStateTracker;
(( com.android.server.display.aiautobrt.CbmStateTracker ) v1 ).startEvaluateCustomCurve ( ); // invoke-virtual {v1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->startEvaluateCustomCurve()V
/* .line 586 */
} // :cond_4
} // :goto_0
return;
} // .end method
public void startCbmStatsJob ( ) {
/* .locals 3 */
/* .line 1087 */
v0 = this.mBgHandler;
v1 = this.mCbmStateTracker;
java.util.Objects .requireNonNull ( v1 );
/* new-instance v2, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1088 */
return;
} // .end method
public void updateCbmState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "autoBrightnessEnabled" # Z */
/* .line 957 */
/* if-nez p1, :cond_0 */
/* .line 958 */
int v0 = -1; // const/4 v0, -0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCbmState(I)V */
/* .line 960 */
} // :cond_0
return;
} // .end method
public void updateCustomSceneState ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "orientation" # I */
/* .line 434 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->isCustomAllowed()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 435 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->hasSceneStateChanged(Ljava/lang/String;I)Z */
/* .line 436 */
/* .local v0, "changed":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 438 */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCurrentCustomSceneState:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* .line 439 */
v1 = this.mBrighteningConfiguration;
/* .local v1, "config":Landroid/hardware/display/BrightnessConfiguration; */
/* .line 440 */
} // .end local v1 # "config":Landroid/hardware/display/BrightnessConfiguration;
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_1 */
/* .line 441 */
v1 = this.mDarkeningConfiguration;
/* .restart local v1 # "config":Landroid/hardware/display/BrightnessConfiguration; */
/* .line 443 */
} // .end local v1 # "config":Landroid/hardware/display/BrightnessConfiguration;
} // :cond_1
v1 = this.mDefaultConfiguration;
/* .line 445 */
/* .restart local v1 # "config":Landroid/hardware/display/BrightnessConfiguration; */
} // :goto_0
v2 = this.mDisplayPowerControllerImpl;
(( com.android.server.display.DisplayPowerControllerImpl ) v2 ).setBrightnessConfiguration ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->setBrightnessConfiguration(Landroid/hardware/display/BrightnessConfiguration;)V
/* .line 446 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateCustomSceneState: config changed, config: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.hardware.display.BrightnessConfiguration ) v1 ).toString ( ); // invoke-virtual {v1}, Landroid/hardware/display/BrightnessConfiguration;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "CbmController"; // const-string v3, "CbmController"
android.util.Slog .d ( v3,v2 );
/* .line 449 */
} // .end local v0 # "changed":Z
} // .end local v1 # "config":Landroid/hardware/display/BrightnessConfiguration;
} // :cond_2
return;
} // .end method
protected void updateModelValid ( Integer p0, Float p1 ) {
/* .locals 6 */
/* .param p1, "type" # I */
/* .param p2, "ratio" # F */
/* .line 1050 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* move v2, v0 */
/* .line 1051 */
/* .local v2, "customCurveValid":Z */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ne p1, v3, :cond_1 */
/* move v3, v1 */
} // :cond_1
/* move v3, v0 */
/* .line 1052 */
/* .local v3, "IndividualModelValid":Z */
} // :goto_1
/* if-nez p1, :cond_2 */
} // :cond_2
/* move v1, v0 */
/* .line 1053 */
/* .local v1, "defaultValid":Z */
} // :goto_2
final String v4 = "best_indicator"; // const-string v4, "best_indicator"
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).setCustomCurveValid ( v2, v4 ); // invoke-virtual {p0, v2, v4}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveValid(ZLjava/lang/String;)V
/* .line 1054 */
v5 = this.mIndividualBrightnessEngine;
(( com.android.server.display.aiautobrt.IndividualBrightnessEngine ) v5 ).setModelValid ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Lcom/android/server/display/aiautobrt/IndividualBrightnessEngine;->setModelValid(ZLjava/lang/String;)V
/* .line 1055 */
/* if-nez v2, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1056 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->resetCustomCurveValidConditions()V */
/* .line 1058 */
} // :cond_4
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 1059 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) p0 ).disableIndividualEngine ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->disableIndividualEngine(Z)V
/* .line 1061 */
} // :cond_5
v0 = this.mDataStore;
(( com.android.server.display.aiautobrt.CustomPersistentDataStore ) v0 ).startWrite ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->startWrite()V
/* .line 1062 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateModelValid: custom valid: " */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", individual model valid: "; // const-string v4, ", individual model valid: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", default valid: "; // const-string v4, ", default valid: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", best ratio: "; // const-string v4, ", best ratio: "
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "CbmController"; // const-string v4, "CbmController"
android.util.Slog .d ( v4,v0 );
/* .line 1067 */
return;
} // .end method
public void validateModelMonotonicity ( ) {
/* .locals 4 */
/* .line 650 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedCategoryId:I */
/* .line 651 */
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mCachedLuxIndex:I */
/* .line 652 */
v0 = this.mBrightnessValidationMapper;
/* .line 653 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->uploadValidatedEventToModel()V */
/* .line 654 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mLastModelTrainTimeStamp:J */
/* .line 655 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mModelTrainTotalTimes:J */
/* const-wide/16 v2, 0x1 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->mModelTrainTotalTimes:J */
/* .line 657 */
v0 = this.mBrightnessDataProcessor;
(( com.android.server.display.statistics.BrightnessDataProcessor ) v0 ).aggregateIndividualModelTrainTimes ( ); // invoke-virtual {v0}, Lcom/android/server/display/statistics/BrightnessDataProcessor;->aggregateIndividualModelTrainTimes()V
/* .line 658 */
return;
} // .end method
