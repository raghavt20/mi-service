public class com.android.server.display.aiautobrt.config.AppCategory {
	 /* .source "AppCategory.java" */
	 /* # instance fields */
	 private java.lang.Integer id;
	 private java.lang.String name;
	 private java.util.List pkg;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/android/server/display/aiautobrt/config/PackageInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.aiautobrt.config.AppCategory ( ) {
/* .locals 0 */
/* .line 3 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static com.android.server.display.aiautobrt.config.AppCategory read ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 7 */
/* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException;, */
/* Ljavax/xml/datatype/DatatypeConfigurationException; */
/* } */
} // .end annotation
/* .line 49 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/config/AppCategory; */
/* invoke-direct {v0}, Lcom/android/server/display/aiautobrt/config/AppCategory;-><init>()V */
/* .line 50 */
/* .local v0, "_instance":Lcom/android/server/display/aiautobrt/config/AppCategory; */
int v1 = 0; // const/4 v1, 0x0
/* .line 51 */
/* .local v1, "_raw":Ljava/lang/String; */
final String v2 = "id"; // const-string v2, "id"
int v3 = 0; // const/4 v3, 0x0
/* .line 52 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 53 */
v2 = java.lang.Integer .parseInt ( v1 );
/* .line 54 */
/* .local v2, "_value":I */
(( com.android.server.display.aiautobrt.config.AppCategory ) v0 ).setId ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/config/AppCategory;->setId(I)V
/* .line 56 */
} // .end local v2 # "_value":I
} // :cond_0
final String v2 = "name"; // const-string v2, "name"
/* .line 57 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 58 */
/* move-object v2, v1 */
/* .line 59 */
/* .local v2, "_value":Ljava/lang/String; */
(( com.android.server.display.aiautobrt.config.AppCategory ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/config/AppCategory;->setName(Ljava/lang/String;)V
/* .line 61 */
} // .end local v2 # "_value":Ljava/lang/String;
v2 = } // :cond_1
/* .line 63 */
/* .local v2, "outerDepth":I */
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 3; // const/4 v6, 0x3
/* if-eq v3, v5, :cond_4 */
/* if-eq v4, v6, :cond_4 */
v3 = /* .line 65 */
int v5 = 2; // const/4 v5, 0x2
/* if-eq v3, v5, :cond_2 */
/* .line 66 */
} // :cond_2
/* .line 67 */
/* .local v3, "_tagName":Ljava/lang/String; */
final String v5 = "pkg"; // const-string v5, "pkg"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 68 */
com.android.server.display.aiautobrt.config.PackageInfo .read ( p0 );
/* .line 69 */
/* .local v5, "_value":Lcom/android/server/display/aiautobrt/config/PackageInfo; */
(( com.android.server.display.aiautobrt.config.AppCategory ) v0 ).getPkg ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getPkg()Ljava/util/List;
/* .line 70 */
} // .end local v5 # "_value":Lcom/android/server/display/aiautobrt/config/PackageInfo;
/* .line 71 */
} // :cond_3
com.android.server.display.aiautobrt.config.XmlParser .skip ( p0 );
/* .line 73 */
} // .end local v3 # "_tagName":Ljava/lang/String;
} // :goto_1
/* .line 74 */
} // :cond_4
/* if-ne v4, v6, :cond_5 */
/* .line 77 */
/* .line 75 */
} // :cond_5
/* new-instance v3, Ljavax/xml/datatype/DatatypeConfigurationException; */
final String v5 = "AppCategory is not closed"; // const-string v5, "AppCategory is not closed"
/* invoke-direct {v3, v5}, Ljavax/xml/datatype/DatatypeConfigurationException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
} // .end method
/* # virtual methods */
public Integer getId ( ) {
/* .locals 1 */
/* .line 16 */
v0 = this.id;
/* if-nez v0, :cond_0 */
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
/* .line 19 */
} // :cond_0
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 34 */
v0 = this.name;
} // .end method
public java.util.List getPkg ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/aiautobrt/config/PackageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 9 */
v0 = this.pkg;
/* if-nez v0, :cond_0 */
/* .line 10 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.pkg = v0;
/* .line 12 */
} // :cond_0
v0 = this.pkg;
} // .end method
Boolean hasId ( ) {
/* .locals 1 */
/* .line 23 */
v0 = this.id;
/* if-nez v0, :cond_0 */
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
/* .line 26 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean hasName ( ) {
/* .locals 1 */
/* .line 38 */
v0 = this.name;
/* if-nez v0, :cond_0 */
/* .line 39 */
int v0 = 0; // const/4 v0, 0x0
/* .line 41 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void setId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "id" # I */
/* .line 30 */
java.lang.Integer .valueOf ( p1 );
this.id = v0;
/* .line 31 */
return;
} // .end method
public void setName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 45 */
this.name = p1;
/* .line 46 */
return;
} // .end method
