public class com.android.server.display.aiautobrt.config.AppCategoryConfig {
	 /* .source "AppCategoryConfig.java" */
	 /* # instance fields */
	 private java.util.List category;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/android/server/display/aiautobrt/config/AppCategory;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.aiautobrt.config.AppCategoryConfig ( ) {
/* .locals 0 */
/* .line 3 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static com.android.server.display.aiautobrt.config.AppCategoryConfig read ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 7 */
/* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/xmlpull/v1/XmlPullParserException;, */
/* Ljava/io/IOException;, */
/* Ljavax/xml/datatype/DatatypeConfigurationException; */
/* } */
} // .end annotation
/* .line 14 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
/* invoke-direct {v0}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;-><init>()V */
/* .line 15 */
/* .local v0, "_instance":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
int v1 = 0; // const/4 v1, 0x0
/* .line 16 */
v2 = /* .local v1, "_raw":Ljava/lang/String; */
/* .line 18 */
/* .local v2, "outerDepth":I */
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 3; // const/4 v6, 0x3
/* if-eq v3, v5, :cond_2 */
/* if-eq v4, v6, :cond_2 */
v3 = /* .line 20 */
int v5 = 2; // const/4 v5, 0x2
/* if-eq v3, v5, :cond_0 */
/* .line 21 */
} // :cond_0
/* .line 22 */
/* .local v3, "_tagName":Ljava/lang/String; */
final String v5 = "category"; // const-string v5, "category"
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 23 */
com.android.server.display.aiautobrt.config.AppCategory .read ( p0 );
/* .line 24 */
/* .local v5, "_value":Lcom/android/server/display/aiautobrt/config/AppCategory; */
(( com.android.server.display.aiautobrt.config.AppCategoryConfig ) v0 ).getCategory ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;->getCategory()Ljava/util/List;
/* .line 25 */
} // .end local v5 # "_value":Lcom/android/server/display/aiautobrt/config/AppCategory;
/* .line 26 */
} // :cond_1
com.android.server.display.aiautobrt.config.XmlParser .skip ( p0 );
/* .line 28 */
} // .end local v3 # "_tagName":Ljava/lang/String;
} // :goto_1
/* .line 29 */
} // :cond_2
/* if-ne v4, v6, :cond_3 */
/* .line 32 */
/* .line 30 */
} // :cond_3
/* new-instance v3, Ljavax/xml/datatype/DatatypeConfigurationException; */
final String v5 = "AppCategoryConfig is not closed"; // const-string v5, "AppCategoryConfig is not closed"
/* invoke-direct {v3, v5}, Ljavax/xml/datatype/DatatypeConfigurationException;-><init>(Ljava/lang/String;)V */
/* throw v3 */
} // .end method
/* # virtual methods */
public java.util.List getCategory ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/aiautobrt/config/AppCategory;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 7 */
v0 = this.category;
/* if-nez v0, :cond_0 */
/* .line 8 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.category = v0;
/* .line 10 */
} // :cond_0
v0 = this.category;
} // .end method
