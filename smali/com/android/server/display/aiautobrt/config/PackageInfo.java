public class com.android.server.display.aiautobrt.config.PackageInfo {
	 /* .source "PackageInfo.java" */
	 /* # instance fields */
	 private java.lang.Integer cateId;
	 private java.lang.String name;
	 private java.lang.Boolean top;
	 /* # direct methods */
	 public com.android.server.display.aiautobrt.config.PackageInfo ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static com.android.server.display.aiautobrt.config.PackageInfo read ( org.xmlpull.v1.XmlPullParser p0 ) {
		 /* .locals 4 */
		 /* .param p0, "_parser" # Lorg/xmlpull/v1/XmlPullParser; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Lorg/xmlpull/v1/XmlPullParserException;, */
		 /* Ljava/io/IOException;, */
		 /* Ljavax/xml/datatype/DatatypeConfigurationException; */
		 /* } */
	 } // .end annotation
	 /* .line 60 */
	 /* new-instance v0, Lcom/android/server/display/aiautobrt/config/PackageInfo; */
	 /* invoke-direct {v0}, Lcom/android/server/display/aiautobrt/config/PackageInfo;-><init>()V */
	 /* .line 61 */
	 /* .local v0, "_instance":Lcom/android/server/display/aiautobrt/config/PackageInfo; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 62 */
	 /* .local v1, "_raw":Ljava/lang/String; */
	 final String v2 = "name"; // const-string v2, "name"
	 int v3 = 0; // const/4 v3, 0x0
	 /* .line 63 */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 64 */
		 /* move-object v2, v1 */
		 /* .line 65 */
		 /* .local v2, "_value":Ljava/lang/String; */
		 (( com.android.server.display.aiautobrt.config.PackageInfo ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setName(Ljava/lang/String;)V
		 /* .line 67 */
	 } // .end local v2 # "_value":Ljava/lang/String;
} // :cond_0
final String v2 = "cateId"; // const-string v2, "cateId"
/* .line 68 */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 69 */
	 v2 = 	 java.lang.Integer .parseInt ( v1 );
	 /* .line 70 */
	 /* .local v2, "_value":I */
	 (( com.android.server.display.aiautobrt.config.PackageInfo ) v0 ).setCateId ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setCateId(I)V
	 /* .line 72 */
} // .end local v2 # "_value":I
} // :cond_1
/* const-string/jumbo v2, "top" */
/* .line 73 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 74 */
v2 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 75 */
/* .local v2, "_value":Z */
(( com.android.server.display.aiautobrt.config.PackageInfo ) v0 ).setTop ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setTop(Z)V
/* .line 77 */
} // .end local v2 # "_value":Z
} // :cond_2
com.android.server.display.aiautobrt.config.XmlParser .skip ( p0 );
/* .line 78 */
} // .end method
/* # virtual methods */
public Integer getCateId ( ) {
/* .locals 1 */
/* .line 24 */
v0 = this.cateId;
/* if-nez v0, :cond_0 */
/* .line 25 */
int v0 = 0; // const/4 v0, 0x0
/* .line 27 */
} // :cond_0
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 9 */
v0 = this.name;
} // .end method
Boolean hasCateId ( ) {
/* .locals 1 */
/* .line 31 */
v0 = this.cateId;
/* if-nez v0, :cond_0 */
/* .line 32 */
int v0 = 0; // const/4 v0, 0x0
/* .line 34 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean hasName ( ) {
/* .locals 1 */
/* .line 13 */
v0 = this.name;
/* if-nez v0, :cond_0 */
/* .line 14 */
int v0 = 0; // const/4 v0, 0x0
/* .line 16 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
Boolean hasTop ( ) {
/* .locals 1 */
/* .line 49 */
v0 = this.top;
/* if-nez v0, :cond_0 */
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isTop ( ) {
/* .locals 1 */
/* .line 42 */
v0 = this.top;
/* if-nez v0, :cond_0 */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
/* .line 45 */
} // :cond_0
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
} // .end method
public void setCateId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "cateId" # I */
/* .line 38 */
java.lang.Integer .valueOf ( p1 );
this.cateId = v0;
/* .line 39 */
return;
} // .end method
public void setName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 20 */
this.name = p1;
/* .line 21 */
return;
} // .end method
public void setTop ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "top" # Z */
/* .line 56 */
java.lang.Boolean .valueOf ( p1 );
this.top = v0;
/* .line 57 */
return;
} // .end method
