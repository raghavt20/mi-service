public class com.android.server.display.aiautobrt.IndividualEventNormalizer {
	 /* .source "IndividualEventNormalizer.java" */
	 /* # static fields */
	 private static final Float MAX_NORMALIZATION_VALUE;
	 private static final Float MIN_NORMALIZATION_VALUE;
	 private static final Integer MIXED_ORIENTATION_APP_MAX;
	 private static final Integer MIXED_ORIENTATION_APP_MIN;
	 /* # instance fields */
	 private final Integer mAppIdMax;
	 private final Integer mAppIdMin;
	 private final Float mBrightnessMax;
	 private final Float mBrightnessMin;
	 private final Float mBrightnessSpanMax;
	 private final Float mBrightnessSpanMin;
	 private final Float mLuxMax;
	 private final Float mLuxMin;
	 private final Float mLuxSpanMax;
	 private final Float mLuxSpanMin;
	 private final Float mMixedOrientationAppMax;
	 private final Float mMixedOrientationAppMin;
	 /* # direct methods */
	 public com.android.server.display.aiautobrt.IndividualEventNormalizer ( ) {
		 /* .locals 1 */
		 /* .param p1, "brightnessMin" # F */
		 /* .param p2, "normalBrightnessMax" # F */
		 /* .param p3, "luxMin" # F */
		 /* .param p4, "luxMax" # F */
		 /* .param p5, "brightnessSpanMin" # F */
		 /* .param p6, "brightnessSpanMax" # F */
		 /* .param p7, "luxSpanMin" # F */
		 /* .param p8, "luxSpanMax" # F */
		 /* .line 36 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 37 */
		 /* iput p1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMin:F */
		 /* .line 38 */
		 /* iput p2, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMax:F */
		 /* .line 39 */
		 /* iput p3, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMin:F */
		 /* .line 40 */
		 /* iput p4, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMax:F */
		 /* .line 41 */
		 /* iput p5, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessSpanMin:F */
		 /* .line 42 */
		 /* iput p6, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessSpanMax:F */
		 /* .line 43 */
		 /* iput p7, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxSpanMin:F */
		 /* .line 44 */
		 /* iput p8, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxSpanMax:F */
		 /* .line 45 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMin:I */
		 /* .line 46 */
		 /* const/16 v0, 0x9 */
		 /* iput v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMax:I */
		 /* .line 47 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mMixedOrientationAppMin:F */
		 /* .line 48 */
		 /* const/high16 v0, 0x40000000 # 2.0f */
		 /* iput v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mMixedOrientationAppMax:F */
		 /* .line 49 */
		 return;
	 } // .end method
	 private Float constrain ( Float p0 ) {
		 /* .locals 2 */
		 /* .param p1, "amount" # F */
		 /* .line 120 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* const/high16 v1, 0x3f800000 # 1.0f */
		 v0 = 		 android.util.MathUtils .constrain ( p1,v0,v1 );
	 } // .end method
	 /* # virtual methods */
	 public Integer antiNormalizeAppId ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "appId" # F */
		 /* .line 110 */
		 /* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMin:I */
		 /* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMax:I */
		 v0 = 		 android.util.MathUtils .lerp ( v0,v1,p1 );
		 v0 = 		 java.lang.Math .round ( v0 );
		 /* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMin:I */
		 /* iget v2, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMax:I */
		 v0 = 		 android.util.MathUtils .constrain ( v0,v1,v2 );
	 } // .end method
	 public Float antiNormalizeBrightness ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "brightness" # F */
		 /* .line 115 */
		 /* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMin:F */
		 /* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMax:F */
		 v0 = 		 android.util.MathUtils .lerp ( v0,v1,p1 );
		 /* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMin:F */
		 /* iget v2, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMax:F */
		 v0 = 		 android.util.MathUtils .constrain ( v0,v1,v2 );
	 } // .end method
	 public Float antiNormalizeLux ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "lux" # F */
		 /* .line 106 */
		 /* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMin:F */
		 /* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMax:F */
		 v0 = 		 android.util.MathUtils .lerp ( v0,v1,p1 );
		 /* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMin:F */
		 /* iget v2, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMax:F */
		 v0 = 		 android.util.MathUtils .constrain ( v0,v1,v2 );
	 } // .end method
	 public Float getMixedOrientApp ( Integer p0, Integer p1 ) {
		 /* .locals 3 */
		 /* .param p1, "appId" # I */
		 /* .param p2, "orientation" # I */
		 /* .line 90 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 91 */
		 /* .local v0, "mixed":F */
		 int v1 = 1; // const/4 v1, 0x1
		 /* if-eq p2, v1, :cond_1 */
		 int v2 = 3; // const/4 v2, 0x3
		 /* if-ne p2, v2, :cond_0 */
		 /* .line 98 */
	 } // :cond_0
	 /* const/16 v1, 0x9 */
	 /* if-ne p1, v1, :cond_3 */
	 /* .line 99 */
	 /* const/high16 v0, 0x40000000 # 2.0f */
	 /* .line 93 */
} // :cond_1
} // :goto_0
/* if-eq p1, v1, :cond_2 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_3 */
/* .line 94 */
} // :cond_2
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 102 */
} // :cond_3
} // :goto_1
} // .end method
public Float normalizeAppId ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "appId" # I */
/* .line 81 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMin:I */
/* int-to-float v0, v0 */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mAppIdMax:I */
/* int-to-float v1, v1 */
/* int-to-float v2, p1 */
v0 = android.util.MathUtils .norm ( v0,v1,v2 );
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->constrain(F)F */
} // .end method
public Float normalizeLux ( Float p0 ) {
/* .locals 2 */
/* .param p1, "lux" # F */
/* .line 52 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 53 */
/* const/high16 p1, -0x40800000 # -1.0f */
/* .line 55 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMax:F */
p1 = java.lang.Math .min ( p1,v0 );
/* .line 56 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMin:F */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxMax:F */
v0 = android.util.MathUtils .norm ( v0,v1,p1 );
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->constrain(F)F */
} // .end method
public Float normalizeLuxSpan ( Float p0 ) {
/* .locals 2 */
/* .param p1, "luxSpan" # F */
/* .line 60 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 61 */
/* const/high16 p1, -0x40800000 # -1.0f */
/* .line 63 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxSpanMin:F */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mLuxSpanMax:F */
v0 = android.util.MathUtils .norm ( v0,v1,p1 );
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->constrain(F)F */
} // .end method
public Float normalizeNit ( Float p0 ) {
/* .locals 2 */
/* .param p1, "nit" # F */
/* .line 67 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 68 */
/* const/high16 p1, -0x40800000 # -1.0f */
/* .line 70 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMin:F */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessMax:F */
v0 = android.util.MathUtils .norm ( v0,v1,p1 );
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->constrain(F)F */
} // .end method
public Float normalizeNitSpan ( Float p0 ) {
/* .locals 2 */
/* .param p1, "nitSpan" # F */
/* .line 74 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 75 */
/* const/high16 p1, -0x40800000 # -1.0f */
/* .line 77 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessSpanMin:F */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mBrightnessSpanMax:F */
v0 = android.util.MathUtils .norm ( v0,v1,p1 );
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->constrain(F)F */
} // .end method
public Float normalizedMixedOrientationAppId ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "appId" # I */
/* .param p2, "orientation" # I */
/* .line 85 */
v0 = (( com.android.server.display.aiautobrt.IndividualEventNormalizer ) p0 ).getMixedOrientApp ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->getMixedOrientApp(II)F
/* .line 86 */
/* .local v0, "mixed":F */
/* iget v1, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mMixedOrientationAppMin:F */
/* iget v2, p0, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->mMixedOrientationAppMax:F */
v1 = android.util.MathUtils .norm ( v1,v2,v0 );
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/display/aiautobrt/IndividualEventNormalizer;->constrain(F)F */
} // .end method
