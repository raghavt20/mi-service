public class com.android.server.display.aiautobrt.CustomPersistentDataStore {
	 /* .source "CustomPersistentDataStore.java" */
	 /* # static fields */
	 private static final java.lang.String ATTRIBUTE_ENABLED_TAG;
	 private static final java.lang.String ATTRIBUTE_NITS_TAG;
	 public static final java.lang.String CUSTOM_BACKUP_DIR_NAME;
	 public static final java.lang.String CUSTOM_BACKUP_FILE_NAME;
	 private static final java.lang.String CUSTOM_BACKUP_FILE_ROOT_ELEMENT;
	 private static final java.lang.String CUSTOM_CURVE_ENABLED_TAG;
	 private static final java.lang.String CUSTOM_CURVE_TAG;
	 private static final java.lang.String INDIVIDUAL_DEFAULT_POINT_TAG;
	 private static final java.lang.String INDIVIDUAL_DEFAULT_SPLINE_TAG;
	 private static final java.lang.String INDIVIDUAL_GAME_POINT_TAG;
	 private static final java.lang.String INDIVIDUAL_GAME_SPLINE_TAG;
	 private static final java.lang.String INDIVIDUAL_MODEL_ENABLED_TAG;
	 private static final java.lang.String INDIVIDUAL_MODEL_TAG;
	 private static final java.lang.String INDIVIDUAL_VIDEO_POINT_TAG;
	 private static final java.lang.String INDIVIDUAL_VIDEO_SPLINE_TAG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.os.Handler mBgHandler;
	 private final com.android.server.display.aiautobrt.CustomBrightnessModeController mCbmController;
	 private Boolean mCustomCurveEnabled;
	 private final mDefaultLux;
	 private mDefaultNits;
	 private final android.util.AtomicFile mFile;
	 private mGameNits;
	 private Boolean mIndividualModelEnabled;
	 private final java.lang.Object mLock;
	 private mVideoNits;
	 /* # direct methods */
	 public static void $r8$lambda$3YX-wo8IF8Wh82tyVFXoFU9RK4s ( com.android.server.display.aiautobrt.CustomPersistentDataStore p0, Integer p1, Float[] p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->lambda$storeIndividualSpline$2(I[F)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$AlV2jckyrkgXG48Ydc64yASaMWQ ( com.android.server.display.aiautobrt.CustomPersistentDataStore p0, Boolean p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->lambda$storeCustomCurveEnabled$0(Z)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$UKXyxo0vTqCXYhW-caDrHsl6VdQ ( com.android.server.display.aiautobrt.CustomPersistentDataStore p0, Boolean p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->lambda$storeIndividualModelEnabled$1(Z)V */
		 return;
	 } // .end method
	 public com.android.server.display.aiautobrt.CustomPersistentDataStore ( ) {
		 /* .locals 2 */
		 /* .param p1, "controller" # Lcom/android/server/display/aiautobrt/CustomBrightnessModeController; */
		 /* .param p2, "defaultLux" # [F */
		 /* .line 82 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 71 */
		 /* new-instance v0, Ljava/lang/Object; */
		 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
		 this.mLock = v0;
		 /* .line 83 */
		 this.mCbmController = p1;
		 /* .line 84 */
		 /* new-instance v0, Landroid/os/Handler; */
		 com.android.internal.os.BackgroundThread .getHandler ( );
		 (( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
		 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
		 this.mBgHandler = v0;
		 /* .line 85 */
		 /* array-length v0, p2 */
		 java.util.Arrays .copyOf ( p2,v0 );
		 this.mDefaultLux = v0;
		 /* .line 86 */
		 /* array-length v0, p2 */
		 /* new-array v0, v0, [F */
		 this.mDefaultNits = v0;
		 /* .line 87 */
		 /* array-length v0, p2 */
		 /* new-array v0, v0, [F */
		 this.mGameNits = v0;
		 /* .line 88 */
		 /* array-length v0, p2 */
		 /* new-array v0, v0, [F */
		 this.mVideoNits = v0;
		 /* .line 89 */
		 /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->getFile()Landroid/util/AtomicFile; */
		 this.mFile = v0;
		 /* .line 90 */
		 /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->loadFromXml()V */
		 /* .line 91 */
		 return;
	 } // .end method
	 private android.util.AtomicFile getFile ( ) {
		 /* .locals 4 */
		 /* .line 259 */
		 /* new-instance v0, Landroid/util/AtomicFile; */
		 /* new-instance v1, Ljava/io/File; */
		 android.os.Environment .getDataSystemDirectory ( );
		 final String v3 = "displayconfig"; // const-string v3, "displayconfig"
		 /* filled-new-array {v3}, [Ljava/lang/String; */
		 android.os.Environment .buildPath ( v2,v3 );
		 final String v3 = "custom_brightness_backup.xml"; // const-string v3, "custom_brightness_backup.xml"
		 /* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
	 } // .end method
	 private Boolean isNonZeroArray ( Float[] p0 ) {
		 /* .locals 5 */
		 /* .param p1, "array" # [F */
		 /* .line 264 */
		 /* array-length v0, p1 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* move v2, v1 */
	 } // :goto_0
	 /* if-ge v2, v0, :cond_1 */
	 /* aget v3, p1, v2 */
	 /* .line 265 */
	 /* .local v3, "v":F */
	 int v4 = 0; // const/4 v4, 0x0
	 /* cmpl-float v4, v3, v4 */
	 if ( v4 != null) { // if-eqz v4, :cond_0
		 /* .line 266 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 264 */
	 } // .end local v3 # "v":F
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 269 */
} // :cond_1
} // .end method
private void lambda$storeCustomCurveEnabled$0 ( Boolean p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "enabled" # Z */
/* .line 140 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mCustomCurveEnabled:Z */
return;
} // .end method
private void lambda$storeIndividualModelEnabled$1 ( Boolean p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "enabled" # Z */
/* .line 144 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mIndividualModelEnabled:Z */
return;
} // .end method
private void lambda$storeIndividualSpline$2 ( Integer p0, Float[] p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "category" # I */
/* .param p2, "nits" # [F */
/* .line 153 */
/* if-nez p1, :cond_0 */
/* .line 154 */
/* array-length v0, p2 */
java.util.Arrays .copyOf ( p2,v0 );
this.mDefaultNits = v0;
/* .line 155 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_1 */
/* .line 156 */
/* array-length v0, p2 */
java.util.Arrays .copyOf ( p2,v0 );
this.mGameNits = v0;
/* .line 157 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_2 */
/* .line 158 */
/* array-length v0, p2 */
java.util.Arrays .copyOf ( p2,v0 );
this.mVideoNits = v0;
/* .line 160 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void listToArray ( Float[] p0, java.util.List p1 ) {
/* .locals 2 */
/* .param p1, "array" # [F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([F", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 250 */
/* .local p2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
v1 = /* array-length v0, p1 */
/* if-eq v0, v1, :cond_0 */
/* .line 251 */
return;
/* .line 253 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
v1 = } // :goto_0
/* if-ge v0, v1, :cond_1 */
/* .line 254 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* aput v1, p1, v0 */
/* .line 253 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 256 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private void loadCustomBrightnessConfig ( java.util.List p0, java.util.List p1, java.util.List p2 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 239 */
/* .local p1, "defaultNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
/* .local p2, "gameNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
/* .local p3, "videoNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
v0 = this.mDefaultNits;
/* invoke-direct {p0, v0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->listToArray([FLjava/util/List;)V */
/* .line 240 */
v0 = this.mGameNits;
/* invoke-direct {p0, v0, p2}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->listToArray([FLjava/util/List;)V */
/* .line 241 */
v0 = this.mVideoNits;
/* invoke-direct {p0, v0, p3}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->listToArray([FLjava/util/List;)V */
/* .line 242 */
v0 = this.mCbmController;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mCustomCurveEnabled:Z */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).setCustomCurveEnabledFromXml ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setCustomCurveEnabledFromXml(Z)V
/* .line 243 */
v0 = this.mCbmController;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mIndividualModelEnabled:Z */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).setIndividualModelEnabledFromXml ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->setIndividualModelEnabledFromXml(Z)V
/* .line 244 */
v0 = this.mCbmController;
int v1 = 0; // const/4 v1, 0x0
v2 = this.mDefaultNits;
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).buildConfigurationFromXml ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildConfigurationFromXml(I[F)V
/* .line 245 */
v0 = this.mCbmController;
int v1 = 1; // const/4 v1, 0x1
v2 = this.mGameNits;
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).buildConfigurationFromXml ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildConfigurationFromXml(I[F)V
/* .line 246 */
v0 = this.mCbmController;
int v1 = 2; // const/4 v1, 0x2
v2 = this.mVideoNits;
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).buildConfigurationFromXml ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->buildConfigurationFromXml(I[F)V
/* .line 247 */
return;
} // .end method
private void loadFromXml ( ) {
/* .locals 13 */
/* .line 190 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 191 */
try { // :try_start_0
v1 = this.mFile;
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = (( android.util.AtomicFile ) v1 ).exists ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->exists()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 192 */
int v1 = 0; // const/4 v1, 0x0
/* .line 194 */
/* .local v1, "inputStream":Ljava/io/FileInputStream; */
try { // :try_start_1
v2 = this.mFile;
(( android.util.AtomicFile ) v2 ).openRead ( ); // invoke-virtual {v2}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v1, v2 */
/* .line 195 */
final String v2 = "CbmController-CustomPersistentDataStore"; // const-string v2, "CbmController-CustomPersistentDataStore"
final String v3 = "Start reading custom brightness config from xml."; // const-string v3, "Start reading custom brightness config from xml."
android.util.Slog .d ( v2,v3 );
/* .line 196 */
android.util.Xml .resolvePullParser ( v1 );
/* .line 198 */
/* .local v2, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 199 */
/* .local v3, "defaultNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 200 */
/* .local v4, "gameNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 201 */
/* .local v5, "videoNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;" */
} // :cond_0
v6 = } // :goto_0
/* move v7, v6 */
/* .local v7, "type":I */
int v8 = 1; // const/4 v8, 0x1
/* if-eq v6, v8, :cond_3 */
/* .line 202 */
int v6 = 3; // const/4 v6, 0x3
/* if-eq v7, v6, :cond_0 */
int v9 = 4; // const/4 v9, 0x4
/* if-ne v7, v9, :cond_1 */
/* .line 203 */
/* .line 205 */
} // :cond_1
/* .line 206 */
/* .local v10, "tag":Ljava/lang/String; */
v11 = (( java.lang.String ) v10 ).hashCode ( ); // invoke-virtual {v10}, Ljava/lang/String;->hashCode()I
int v12 = 0; // const/4 v12, 0x0
/* sparse-switch v11, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
/* const-string/jumbo v6, "video_point" */
v6 = (( java.lang.String ) v10 ).equals ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* move v8, v9 */
/* :sswitch_1 */
final String v6 = "default_point"; // const-string v6, "default_point"
v6 = (( java.lang.String ) v10 ).equals ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
int v8 = 2; // const/4 v8, 0x2
/* :sswitch_2 */
final String v8 = "game_point"; // const-string v8, "game_point"
v8 = (( java.lang.String ) v10 ).equals ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* move v8, v6 */
/* :sswitch_3 */
final String v6 = "individual_model_enabled"; // const-string v6, "individual_model_enabled"
v6 = (( java.lang.String ) v10 ).equals ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* :sswitch_4 */
final String v6 = "custom_curve_enabled"; // const-string v6, "custom_curve_enabled"
v6 = (( java.lang.String ) v10 ).equals ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* move v8, v12 */
} // :goto_1
int v8 = -1; // const/4 v8, -0x1
} // :goto_2
int v6 = 0; // const/4 v6, 0x0
int v9 = 0; // const/4 v9, 0x0
/* packed-switch v8, :pswitch_data_0 */
/* .line 220 */
/* :pswitch_0 */
v6 = final String v8 = "nit"; // const-string v8, "nit"
java.lang.Float .valueOf ( v6 );
/* .line 221 */
/* .line 217 */
/* :pswitch_1 */
v6 = final String v8 = "nit"; // const-string v8, "nit"
java.lang.Float .valueOf ( v6 );
/* .line 218 */
/* .line 214 */
/* :pswitch_2 */
v6 = final String v8 = "nit"; // const-string v8, "nit"
java.lang.Float .valueOf ( v6 );
/* .line 215 */
/* .line 211 */
/* :pswitch_3 */
v6 = final String v6 = "enabled"; // const-string v6, "enabled"
/* iput-boolean v6, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mIndividualModelEnabled:Z */
/* .line 212 */
/* .line 208 */
/* :pswitch_4 */
v6 = final String v6 = "enabled"; // const-string v6, "enabled"
/* iput-boolean v6, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mCustomCurveEnabled:Z */
/* .line 209 */
/* nop */
/* .line 225 */
} // .end local v10 # "tag":Ljava/lang/String;
} // :goto_3
/* goto/16 :goto_0 */
/* .line 226 */
} // :cond_3
/* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->loadCustomBrightnessConfig(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 231 */
} // .end local v2 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v3 # "defaultNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
} // .end local v4 # "gameNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
} // .end local v5 # "videoNitsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
} // .end local v7 # "type":I
try { // :try_start_2
miui.io.IOUtils .closeQuietly ( v1 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 227 */
/* :catch_0 */
/* move-exception v2 */
/* .line 228 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_3
v3 = this.mFile;
(( android.util.AtomicFile ) v3 ).delete ( ); // invoke-virtual {v3}, Landroid/util/AtomicFile;->delete()V
/* .line 229 */
final String v3 = "CbmController-CustomPersistentDataStore"; // const-string v3, "CbmController-CustomPersistentDataStore"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Failed to read custom brightness backup"; // const-string v5, "Failed to read custom brightness backup"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 231 */
} // .end local v2 # "e":Ljava/lang/Exception;
try { // :try_start_4
miui.io.IOUtils .closeQuietly ( v1 );
/* .line 232 */
} // :goto_4
/* .line 231 */
} // :goto_5
miui.io.IOUtils .closeQuietly ( v1 );
/* .line 232 */
/* nop */
} // .end local p0 # "this":Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;
/* throw v2 */
/* .line 234 */
} // .end local v1 # "inputStream":Ljava/io/FileInputStream;
/* .restart local p0 # "this":Lcom/android/server/display/aiautobrt/CustomPersistentDataStore; */
} // :cond_4
} // :goto_6
/* monitor-exit v0 */
/* .line 235 */
return;
/* .line 234 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5ce4285d -> :sswitch_4 */
/* 0x14c86905 -> :sswitch_3 */
/* 0x39aac703 -> :sswitch_2 */
/* 0x4e4b1a12 -> :sswitch_1 */
/* 0x526a44cc -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void writeArrayToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.lang.String p3, Float[] p4 ) {
/* .locals 4 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p4, "tag" # Ljava/lang/String; */
/* .param p5, "nits" # [F */
/* .line 178 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
try { // :try_start_0
v1 = this.mDefaultLux;
/* array-length v1, v1 */
/* if-ge v0, v1, :cond_0 */
/* .line 179 */
int v1 = 0; // const/4 v1, 0x0
/* .line 180 */
final String v2 = "nit"; // const-string v2, "nit"
/* aget v3, p5, v0 */
/* .line 181 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 178 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 186 */
} // .end local v0 # "i":I
} // :cond_0
/* .line 183 */
/* :catch_0 */
/* move-exception v0 */
/* .line 184 */
/* .local v0, "e":Ljava/lang/Exception; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 185 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write backup of nits"; // const-string v2, "Failed to write backup of nits"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CbmController-CustomPersistentDataStore"; // const-string v2, "CbmController-CustomPersistentDataStore"
android.util.Slog .e ( v2,v1 );
/* .line 187 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void writeFeatureEnabledToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.lang.String p3, java.lang.String p4, Boolean p5 ) {
/* .locals 3 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p4, "attribute" # Ljava/lang/String; */
/* .param p5, "tag" # Ljava/lang/String; */
/* .param p6, "enabled" # Z */
/* .line 166 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 167 */
/* .line 168 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 172 */
/* .line 169 */
/* :catch_0 */
/* move-exception v0 */
/* .line 170 */
/* .local v0, "e":Ljava/lang/Exception; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 171 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write backup of feature enabled"; // const-string v2, "Failed to write backup of feature enabled"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CbmController-CustomPersistentDataStore"; // const-string v2, "CbmController-CustomPersistentDataStore"
android.util.Slog .e ( v2,v1 );
/* .line 173 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
/* # virtual methods */
protected void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 273 */
final String v0 = " CustomPersistentDataStore:"; // const-string v0, " CustomPersistentDataStore:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 274 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCustomCurveEnabled="; // const-string v1, " mCustomCurveEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mCustomCurveEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 275 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualModelEnabled="; // const-string v1, " mIndividualModelEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mIndividualModelEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 276 */
v0 = this.mDefaultNits;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->isNonZeroArray([F)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 277 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDefaultNits="; // const-string v1, " mDefaultNits="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDefaultNits;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 279 */
} // :cond_0
v0 = this.mGameNits;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->isNonZeroArray([F)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 280 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mGameNits="; // const-string v1, " mGameNits="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mGameNits;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 282 */
} // :cond_1
v0 = this.mVideoNits;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->isNonZeroArray([F)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 283 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mVideoNits="; // const-string v1, " mVideoNits="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mVideoNits;
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 285 */
} // :cond_2
return;
} // .end method
public void saveToXml ( ) {
/* .locals 11 */
/* .line 94 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 95 */
int v1 = 0; // const/4 v1, 0x0
/* .line 97 */
/* .local v1, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_0
final String v2 = "CbmController-CustomPersistentDataStore"; // const-string v2, "CbmController-CustomPersistentDataStore"
final String v3 = "Save custom brightness config to xml."; // const-string v3, "Save custom brightness config to xml."
android.util.Slog .d ( v2,v3 );
/* .line 98 */
v2 = this.mFile;
(( android.util.AtomicFile ) v2 ).startWrite ( ); // invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v1, v2 */
/* .line 99 */
android.util.Xml .resolveSerializer ( v1 );
/* .line 100 */
/* .local v2, "out":Lcom/android/modules/utils/TypedXmlSerializer; */
int v3 = 1; // const/4 v3, 0x1
java.lang.Boolean .valueOf ( v3 );
int v10 = 0; // const/4 v10, 0x0
/* .line 101 */
final String v4 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 102 */
final String v3 = "custom-config"; // const-string v3, "custom-config"
/* .line 104 */
final String v3 = "custom_curve"; // const-string v3, "custom_curve"
/* .line 105 */
v4 = this.mFile;
final String v7 = "enabled"; // const-string v7, "enabled"
final String v8 = "custom_curve_enabled"; // const-string v8, "custom_curve_enabled"
/* iget-boolean v9, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mCustomCurveEnabled:Z */
/* move-object v3, p0 */
/* move-object v5, v1 */
/* move-object v6, v2 */
/* invoke-direct/range {v3 ..v9}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->writeFeatureEnabledToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 107 */
final String v3 = "custom_curve"; // const-string v3, "custom_curve"
/* .line 109 */
final String v3 = "individual_model"; // const-string v3, "individual_model"
/* .line 110 */
v4 = this.mFile;
final String v7 = "enabled"; // const-string v7, "enabled"
final String v8 = "individual_model_enabled"; // const-string v8, "individual_model_enabled"
/* iget-boolean v9, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mIndividualModelEnabled:Z */
/* move-object v3, p0 */
/* move-object v5, v1 */
/* move-object v6, v2 */
/* invoke-direct/range {v3 ..v9}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->writeFeatureEnabledToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 112 */
final String v3 = "individual_model"; // const-string v3, "individual_model"
/* .line 114 */
/* iget-boolean v3, p0, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->mIndividualModelEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 115 */
final String v3 = "individual_default_spline"; // const-string v3, "individual_default_spline"
/* .line 116 */
v4 = this.mFile;
final String v7 = "default_point"; // const-string v7, "default_point"
v8 = this.mDefaultNits;
/* move-object v3, p0 */
/* move-object v5, v1 */
/* move-object v6, v2 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->writeArrayToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;[F)V */
/* .line 117 */
final String v3 = "individual_default_spline"; // const-string v3, "individual_default_spline"
/* .line 119 */
final String v3 = "individual_game_spline"; // const-string v3, "individual_game_spline"
/* .line 120 */
v4 = this.mFile;
final String v7 = "game_point"; // const-string v7, "game_point"
v8 = this.mGameNits;
/* move-object v3, p0 */
/* move-object v5, v1 */
/* move-object v6, v2 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->writeArrayToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;[F)V */
/* .line 121 */
final String v3 = "individual_game_spline"; // const-string v3, "individual_game_spline"
/* .line 123 */
final String v3 = "individual_video_spline"; // const-string v3, "individual_video_spline"
/* .line 124 */
v4 = this.mFile;
/* const-string/jumbo v7, "video_point" */
v8 = this.mVideoNits;
/* move-object v3, p0 */
/* move-object v5, v1 */
/* move-object v6, v2 */
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;->writeArrayToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;[F)V */
/* .line 125 */
final String v3 = "individual_video_spline"; // const-string v3, "individual_video_spline"
/* .line 128 */
} // :cond_0
final String v3 = "custom-config"; // const-string v3, "custom-config"
/* .line 129 */
/* .line 130 */
(( java.io.FileOutputStream ) v1 ).flush ( ); // invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
/* .line 131 */
v3 = this.mFile;
(( android.util.AtomicFile ) v3 ).finishWrite ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 135 */
} // .end local v2 # "out":Lcom/android/modules/utils/TypedXmlSerializer;
/* .line 136 */
} // .end local v1 # "outputStream":Ljava/io/FileOutputStream;
/* :catchall_0 */
/* move-exception v1 */
/* .line 132 */
/* .restart local v1 # "outputStream":Ljava/io/FileOutputStream; */
/* :catch_0 */
/* move-exception v2 */
/* .line 133 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
v3 = this.mFile;
(( android.util.AtomicFile ) v3 ).failWrite ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 134 */
final String v3 = "CbmController-CustomPersistentDataStore"; // const-string v3, "CbmController-CustomPersistentDataStore"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Failed to write custom brightness config"; // const-string v5, "Failed to write custom brightness config"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 136 */
} // .end local v1 # "outputStream":Ljava/io/FileOutputStream;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 137 */
return;
/* .line 136 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void startWrite ( ) {
/* .locals 2 */
/* .line 148 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 149 */
return;
} // .end method
public void storeCustomCurveEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 140 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 141 */
return;
} // .end method
public void storeIndividualModelEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 144 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 145 */
return;
} // .end method
public void storeIndividualSpline ( Integer p0, Float[] p1 ) {
/* .locals 2 */
/* .param p1, "category" # I */
/* .param p2, "nits" # [F */
/* .line 152 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomPersistentDataStore$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/aiautobrt/CustomPersistentDataStore;I[F)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 161 */
return;
} // .end method
