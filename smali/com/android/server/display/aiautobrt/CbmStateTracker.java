public class com.android.server.display.aiautobrt.CbmStateTracker {
	 /* .source "CbmStateTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;, */
	 /* Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
	 /* } */
} // .end annotation
/* # static fields */
protected static final Integer CUSTOM_BRT_ADJ_SCENE_IN_BRIGHT_ROOM;
protected static final Integer CUSTOM_BRT_ADJ_SCENE_IN_DARK_ROOM;
protected static final Integer CUSTOM_BRT_ADJ_SCENE_IN_DEFAULT;
private static final java.text.SimpleDateFormat FORMAT;
private static final Integer MAX_HISTORY_CBM_EVENTS_CAPACITY;
private static final Long MAX_PREDICT_DURATION;
private static final Integer MAX_RECORD_CAPACITY;
private static final Integer MINIMUM_AUTO_ADJUST_TIMES;
private static final Integer MINIMUM_COMPARED_MODEL_NUM;
private static final java.lang.String TAG;
private static Boolean sDebug;
/* # instance fields */
private final android.os.Handler mBgHandler;
private final java.util.Map mCbmEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private final com.android.server.display.aiautobrt.CustomBrightnessModeController mCustomController;
private final android.os.Handler mHandler;
private final java.util.ArrayDeque mHistoryCbmEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayDeque<", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;", */
/* ">;>;>;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Runnable mNoteMaxPredictDurationRunnable;
private final java.util.ArrayDeque mResultRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayDeque<", */
/* "Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$23rpsK-YgamWAbUruIzj7kkuv_c ( com.android.server.display.aiautobrt.CbmStateTracker p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteMaxPredictDuration()V */
return;
} // .end method
public static void $r8$lambda$2j4xP79ZnZF1IZCnkZJSbVaM9Ts ( com.android.server.display.aiautobrt.CbmStateTracker p0, Integer p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteManualAdjustmentTimes$1(II)V */
return;
} // .end method
public static void $r8$lambda$BwrB5psgyp_EiKONyrpiZKMknvg ( com.android.server.display.aiautobrt.CbmStateTracker p0, Integer p1, Long p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteStopCbmStateTracking$3(IJ)V */
return;
} // .end method
public static void $r8$lambda$HJXSGKaR_Qgs2LJm3Wkblvb207g ( com.android.server.display.aiautobrt.CbmStateTracker p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$resetBrtAdjSceneCount$9()V */
return;
} // .end method
public static void $r8$lambda$RuiZ3YKKhiSEGwrEhhG-CQwNg_w ( com.android.server.display.aiautobrt.CbmStateTracker p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$startEvaluateCustomCurve$8()V */
return;
} // .end method
public static void $r8$lambda$UiQAVHCxiRO_69GLYDkkiKUAlRc ( com.android.server.display.aiautobrt.CbmStateTracker p0, Integer p1, Long p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteStartCbmStateTracking$2(IJ)V */
return;
} // .end method
public static void $r8$lambda$XHr2jBJ3H0FbA9jcb6cxGSTWK4Q ( com.android.server.display.aiautobrt.CbmStateTracker p0, Float p1, Integer p2, Float p3, Long p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteIndividualResult$6(FIFJ)V */
return;
} // .end method
public static void $r8$lambda$Z4jKnEG_H-__EAYCbA0gwmPDERg ( com.android.server.display.aiautobrt.CbmStateTracker p0, Integer p1, Float p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$startCbmStats$7(IF)V */
return;
} // .end method
public static void $r8$lambda$_2pyPhTL6QTsGiFRu0vupPTdOHE ( com.android.server.display.aiautobrt.CbmStateTracker p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteAutoAdjustmentTimes$0(I)V */
return;
} // .end method
public static void $r8$lambda$iF7fmxV0wTEbzMqXE-lEDP7whCY ( com.android.server.display.aiautobrt.CbmStateTracker p0, Long p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteStopPredictTracking$5(J)V */
return;
} // .end method
public static void $r8$lambda$mfRHy5QqE5igs-JPMQeu9wM_T1g ( com.android.server.display.aiautobrt.CbmStateTracker p0, Long p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->lambda$noteStartPredictTracking$4(J)V */
return;
} // .end method
static java.text.SimpleDateFormat -$$Nest$sfgetFORMAT ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.display.aiautobrt.CbmStateTracker.FORMAT;
} // .end method
static Boolean -$$Nest$sfgetsDebug ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
} // .end method
static com.android.server.display.aiautobrt.CbmStateTracker ( ) {
/* .locals 2 */
/* .line 28 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v1 = "MM-dd HH:mm:ss.SSS"; // const-string v1, "MM-dd HH:mm:ss.SSS"
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
return;
} // .end method
public com.android.server.display.aiautobrt.CbmStateTracker ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "bgHandler" # Landroid/os/Handler; */
/* .param p3, "handler" # Landroid/os/Handler; */
/* .param p4, "customController" # Lcom/android/server/display/aiautobrt/CustomBrightnessModeController; */
/* .line 56 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 45 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCbmEvents = v0;
/* .line 46 */
/* new-instance v0, Ljava/util/ArrayDeque; */
/* const/16 v1, 0xc8 */
/* invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V */
this.mResultRecords = v0;
/* .line 47 */
/* new-instance v0, Ljava/util/ArrayDeque; */
/* const/16 v1, 0x1e */
/* invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V */
this.mHistoryCbmEvents = v0;
/* .line 53 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda11; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda11;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;)V */
this.mNoteMaxPredictDurationRunnable = v0;
/* .line 57 */
this.mContext = p1;
/* .line 58 */
this.mBgHandler = p2;
/* .line 59 */
this.mHandler = p3;
/* .line 60 */
this.mCustomController = p4;
/* .line 61 */
return;
} // .end method
private com.android.server.display.aiautobrt.CbmStateTracker$StateRecord getCbmEvent ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "state" # I */
/* .line 64 */
v0 = this.mCbmEvents;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 65 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* if-nez v0, :cond_0 */
/* .line 66 */
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* invoke-direct {v1, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;-><init>(I)V */
/* move-object v0, v1 */
/* .line 67 */
v1 = this.mCbmEvents;
java.lang.Integer .valueOf ( p1 );
/* .line 69 */
} // :cond_0
} // .end method
static void lambda$dump$10 ( java.io.PrintWriter p0, com.android.server.display.aiautobrt.CbmStateTracker$ResultRecord p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "resultRecord" # Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
/* .line 314 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
return;
} // .end method
static void lambda$dump$11 ( java.io.PrintWriter p0, java.lang.Integer p1, com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "type" # Ljava/lang/Integer; */
/* .param p2, "stateRecord" # Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 316 */
(( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) p2 ).toString ( ); // invoke-virtual {p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
return;
} // .end method
static void lambda$dump$12 ( java.io.PrintWriter p0, java.lang.Integer p1, com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "type" # Ljava/lang/Integer; */
/* .param p2, "stateRecord" # Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 322 */
(( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) p2 ).toString ( ); // invoke-virtual {p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
return;
} // .end method
static void lambda$dump$13 ( java.io.PrintWriter p0, java.lang.String p1, java.util.Map p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "time" # Ljava/lang/String; */
/* .param p2, "historyMap" # Ljava/util/Map; */
/* .line 321 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ":"; // const-string v1, ":"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 322 */
/* new-instance v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda9; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda9;-><init>(Ljava/io/PrintWriter;)V */
/* .line 323 */
return;
} // .end method
private void lambda$noteAutoAdjustmentTimes$0 ( Integer p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "cbmState" # I */
/* .line 81 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 82 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* iget-boolean v1, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* if-nez v1, :cond_0 */
/* .line 83 */
return;
/* .line 85 */
} // :cond_0
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetautoAdjustTimes ( v0 );
/* const/high16 v2, 0x3f800000 # 1.0f */
/* add-float/2addr v1, v2 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputautoAdjustTimes ( v0,v1 );
/* .line 86 */
/* sget-boolean v1, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 87 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "noteAutoAdjustmentTimes: cbm state: "; // const-string v2, "noteAutoAdjustmentTimes: cbm state: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", auto adjust times: "; // const-string v2, ", auto adjust times: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetautoAdjustTimes ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CbmController-Tracker"; // const-string v2, "CbmController-Tracker"
android.util.Slog .i ( v2,v1 );
/* .line 90 */
} // :cond_1
return;
} // .end method
private void lambda$noteIndividualResult$6 ( Float p0, Integer p1, Float p2, Long p3 ) { //synthethic
/* .locals 7 */
/* .param p1, "lux" # F */
/* .param p2, "appId" # I */
/* .param p3, "brightness" # F */
/* .param p4, "now" # J */
/* .line 212 */
/* new-instance v6, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
/* move-object v0, v6 */
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move-wide v4, p4 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;-><init>(FIFJ)V */
/* .line 213 */
/* .local v0, "resultRecord":Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
v1 = this.mResultRecords;
v1 = (( java.util.ArrayDeque ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I
/* const/16 v2, 0xc8 */
/* if-ne v1, v2, :cond_0 */
/* .line 214 */
v1 = this.mResultRecords;
(( java.util.ArrayDeque ) v1 ).pollLast ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->pollLast()Ljava/lang/Object;
/* .line 216 */
} // :cond_0
v1 = this.mResultRecords;
(( java.util.ArrayDeque ) v1 ).peekFirst ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
/* .line 217 */
/* .local v1, "peekFirst":Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = (( com.android.server.display.aiautobrt.CbmStateTracker$ResultRecord ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$ResultRecord;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 218 */
return;
/* .line 220 */
} // :cond_1
v2 = this.mResultRecords;
(( java.util.ArrayDeque ) v2 ).addFirst ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V
/* .line 221 */
return;
} // .end method
private void lambda$noteManualAdjustmentTimes$1 ( Integer p0, Integer p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "cbmState" # I */
/* .param p2, "brtAdjSceneState" # I */
/* .line 99 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 100 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* iget-boolean v1, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* if-nez v1, :cond_0 */
/* .line 101 */
return;
/* .line 103 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p2, v1, :cond_1 */
/* .line 104 */
v2 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetdarkRoomAdjTimes ( v0 );
/* add-int/2addr v2, v1 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputdarkRoomAdjTimes ( v0,v2 );
/* .line 105 */
} // :cond_1
int v2 = 2; // const/4 v2, 0x2
/* if-ne p2, v2, :cond_2 */
/* .line 106 */
v2 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetbrightRoomAdjTimes ( v0 );
/* add-int/2addr v2, v1 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputbrightRoomAdjTimes ( v0,v2 );
/* .line 108 */
} // :cond_2
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetmanualAdjustTimes ( v0 );
/* const/high16 v2, 0x3f800000 # 1.0f */
/* add-float/2addr v1, v2 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputmanualAdjustTimes ( v0,v1 );
/* .line 110 */
} // :goto_0
/* sget-boolean v1, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 111 */
final String v1 = "CbmController-Tracker"; // const-string v1, "CbmController-Tracker"
/* if-nez p2, :cond_3 */
/* .line 112 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "noteManualAdjustmentTimes: cbm state: "; // const-string v3, "noteManualAdjustmentTimes: cbm state: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", manually adjust times: "; // const-string v3, ", manually adjust times: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetmanualAdjustTimes ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 115 */
} // :cond_3
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "noteManualAdjustmentTimes: brt scene state: "; // const-string v3, "noteManualAdjustmentTimes: brt scene state: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", dark room adjust times: "; // const-string v3, ", dark room adjust times: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetdarkRoomAdjTimes ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", bright room adjust times: "; // const-string v3, ", bright room adjust times: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetbrightRoomAdjTimes ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 120 */
} // :cond_4
} // :goto_1
return;
} // .end method
private void lambda$noteStartCbmStateTracking$2 ( Integer p0, Long p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "state" # I */
/* .param p2, "timeMills" # J */
/* .line 125 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 126 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetinitialize ( v0 );
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_0 */
/* .line 127 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputinitialize ( v0,v2 );
/* .line 128 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputstartTimeMills ( v0,p2,p3 );
/* .line 130 */
} // :cond_0
/* iget-boolean v1, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 131 */
return;
/* .line 133 */
} // :cond_1
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputstartTimeMills ( v0,p2,p3 );
/* .line 134 */
/* iput-boolean v2, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* .line 135 */
/* sget-boolean v1, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 136 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "noteStartCbmStateTracking: state: "; // const-string v2, "noteStartCbmStateTracking: state: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", usage: "; // const-string v2, ", usage: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetusageDurations ( v0 );
/* move-result-wide v2 */
/* .line 137 */
android.util.TimeUtils .formatDuration ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 136 */
final String v2 = "CbmController-Tracker"; // const-string v2, "CbmController-Tracker"
android.util.Slog .i ( v2,v1 );
/* .line 139 */
} // :cond_2
return;
} // .end method
private void lambda$noteStartPredictTracking$4 ( Long p0 ) { //synthethic
/* .locals 5 */
/* .param p1, "timeMills" # J */
/* .line 171 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 172 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetpredictTracking ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 173 */
return;
/* .line 175 */
} // :cond_0
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputstartPredictTimeMills ( v0,p1,p2 );
/* .line 176 */
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputpredictTracking ( v0,v1 );
/* .line 178 */
v1 = this.mBgHandler;
v2 = this.mNoteMaxPredictDurationRunnable;
(( android.os.Handler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 179 */
v1 = this.mBgHandler;
v2 = this.mNoteMaxPredictDurationRunnable;
/* const-wide/16 v3, 0x3e8 */
(( android.os.Handler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 180 */
/* sget-boolean v1, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 181 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "noteStartCbmPredictTracking: duration: "; // const-string v2, "noteStartCbmPredictTracking: duration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 182 */
(( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) v0 ).getPredictDurations ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->getPredictDurations()J
/* move-result-wide v2 */
android.util.TimeUtils .formatDuration ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 181 */
final String v2 = "CbmController-Tracker"; // const-string v2, "CbmController-Tracker"
android.util.Slog .i ( v2,v1 );
/* .line 184 */
} // :cond_1
return;
} // .end method
private void lambda$noteStopCbmStateTracking$3 ( Integer p0, Long p1 ) { //synthethic
/* .locals 6 */
/* .param p1, "state" # I */
/* .param p2, "timeMills" # J */
/* .line 144 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 145 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetinitialize ( v0 );
/* if-nez v1, :cond_0 */
/* .line 146 */
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputinitialize ( v0,v1 );
/* .line 147 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputstartTimeMills ( v0,p2,p3 );
/* .line 149 */
} // :cond_0
/* iget-boolean v1, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* if-nez v1, :cond_1 */
/* .line 150 */
return;
/* .line 152 */
} // :cond_1
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetstartTimeMills ( v0 );
/* move-result-wide v1 */
/* sub-long v1, p2, v1 */
/* .line 153 */
/* .local v1, "duration":J */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetusageDurations ( v0 );
/* move-result-wide v3 */
/* add-long/2addr v3, v1 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputusageDurations ( v0,v3,v4 );
/* .line 154 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* .line 156 */
v3 = this.mCustomController;
/* long-to-float v4, v1 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v3 ).noteBrightnessUsageToAggregate ( v4, p1 ); // invoke-virtual {v3, v4, p1}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->noteBrightnessUsageToAggregate(FI)V
/* .line 157 */
/* sget-boolean v3, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 158 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "noteStopCbmStateTracking: state: "; // const-string v4, "noteStopCbmStateTracking: state: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", duration: "; // const-string v4, ", duration: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* long-to-float v4, v1 */
/* const/high16 v5, 0x447a0000 # 1000.0f */
/* div-float/2addr v4, v5 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", usage: "; // const-string v4, ", usage: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 160 */
(( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) v0 ).getUsageDuration ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->getUsageDuration()J
/* move-result-wide v4 */
android.util.TimeUtils .formatDuration ( v4,v5 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 158 */
final String v4 = "CbmController-Tracker"; // const-string v4, "CbmController-Tracker"
android.util.Slog .i ( v4,v3 );
/* .line 162 */
} // :cond_2
return;
} // .end method
private void lambda$noteStopPredictTracking$5 ( Long p0 ) { //synthethic
/* .locals 5 */
/* .param p1, "timeMills" # J */
/* .line 193 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 194 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetpredictTracking ( v0 );
/* if-nez v1, :cond_0 */
/* .line 195 */
return;
/* .line 197 */
} // :cond_0
v1 = this.mBgHandler;
v2 = this.mNoteMaxPredictDurationRunnable;
(( android.os.Handler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 198 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetpredictDurations ( v0 );
/* move-result-wide v1 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetstartPredictTimeMills ( v0 );
/* move-result-wide v3 */
/* sub-long v3, p1, v3 */
/* add-long/2addr v1, v3 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputpredictDurations ( v0,v1,v2 );
/* .line 199 */
int v1 = 0; // const/4 v1, 0x0
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputpredictTracking ( v0,v1 );
/* .line 200 */
/* sget-boolean v1, Lcom/android/server/display/aiautobrt/CbmStateTracker;->sDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 201 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "noteStopPredictTracking: duration: "; // const-string v2, "noteStopPredictTracking: duration: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 202 */
(( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) v0 ).getPredictDurations ( ); // invoke-virtual {v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->getPredictDurations()J
/* move-result-wide v2 */
android.util.TimeUtils .formatDuration ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 201 */
final String v2 = "CbmController-Tracker"; // const-string v2, "CbmController-Tracker"
android.util.Slog .i ( v2,v1 );
/* .line 204 */
} // :cond_1
v1 = this.mCustomController;
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fgetpredictDurations ( v0 );
/* move-result-wide v2 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v1 ).notePredictDurationToAggregate ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->notePredictDurationToAggregate(J)V
/* .line 205 */
/* const-wide/16 v1, 0x0 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputpredictDurations ( v0,v1,v2 );
/* .line 206 */
return;
} // .end method
private void lambda$resetBrtAdjSceneCount$9 ( ) { //synthethic
/* .locals 2 */
/* .line 283 */
v0 = this.mCbmEvents;
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* check-cast v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 284 */
/* .local v0, "stateRecord":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 285 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$mresetBrtAdjSceneCount ( v0 );
/* .line 287 */
} // :cond_0
return;
} // .end method
private void lambda$startCbmStats$7 ( Integer p0, Float p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "minimumType" # I */
/* .param p2, "minimumRatio" # F */
/* .line 251 */
v0 = this.mCustomController;
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v0 ).updateModelValid ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateModelValid(IF)V
return;
} // .end method
private void lambda$startEvaluateCustomCurve$8 ( ) { //synthethic
/* .locals 4 */
/* .line 274 */
v0 = this.mCbmEvents;
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* check-cast v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 275 */
/* .local v0, "stateRecord":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$msatisfyMaxBrtAdjTimes ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 276 */
v1 = this.mHandler;
v2 = this.mCustomController;
java.util.Objects .requireNonNull ( v2 );
/* new-instance v3, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, v2}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;)V */
(( android.os.Handler ) v1 ).post ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 278 */
} // :cond_0
return;
} // .end method
private void noteMaxPredictDuration ( ) {
/* .locals 4 */
/* .line 304 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 305 */
/* .local v0, "event":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
int v1 = 0; // const/4 v1, 0x0
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputpredictTracking ( v0,v1 );
/* .line 306 */
v1 = this.mCustomController;
/* const-wide/16 v2, 0x3e8 */
(( com.android.server.display.aiautobrt.CustomBrightnessModeController ) v1 ).notePredictDurationToAggregate ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->notePredictDurationToAggregate(J)V
/* .line 307 */
/* const-wide/16 v1, 0x0 */
com.android.server.display.aiautobrt.CbmStateTracker$StateRecord .-$$Nest$fputpredictDurations ( v0,v1,v2 );
/* .line 308 */
return;
} // .end method
private void storeHistoryCbmEvents ( ) {
/* .locals 6 */
/* .line 261 */
v0 = v0 = this.mCbmEvents;
/* if-nez v0, :cond_1 */
/* .line 262 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 263 */
/* .local v0, "historyStateRecordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;>;" */
v1 = this.mHistoryCbmEvents;
v1 = (( java.util.ArrayDeque ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I
/* const/16 v2, 0x1e */
/* if-ne v1, v2, :cond_0 */
/* .line 264 */
v1 = this.mHistoryCbmEvents;
(( java.util.ArrayDeque ) v1 ).pollLast ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->pollLast()Ljava/lang/Object;
/* .line 266 */
} // :cond_0
/* new-instance v1, Ljava/util/HashMap; */
v2 = this.mCbmEvents;
/* invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V */
/* .line 267 */
/* .local v1, "historyEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;" */
v2 = com.android.server.display.aiautobrt.CbmStateTracker.FORMAT;
/* new-instance v3, Ljava/util/Date; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v3 ); // invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 268 */
v2 = this.mHistoryCbmEvents;
(( java.util.ArrayDeque ) v2 ).addFirst ( v0 ); // invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V
/* .line 270 */
} // .end local v0 # "historyStateRecordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;>;"
} // .end local v1 # "historyEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;"
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 311 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_CBM:Z */
com.android.server.display.aiautobrt.CbmStateTracker.sDebug = (v0!= 0);
/* .line 312 */
v0 = this.mResultRecords;
v0 = (( java.util.ArrayDeque ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I
/* .line 313 */
/* .local v0, "size":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " Latest "; // const-string v2, " Latest "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " individual events: "; // const-string v2, " individual events: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 314 */
v1 = this.mResultRecords;
/* new-instance v2, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda12; */
/* invoke-direct {v2, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda12;-><init>(Ljava/io/PrintWriter;)V */
(( java.util.ArrayDeque ) v1 ).forEach ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->forEach(Ljava/util/function/Consumer;)V
/* .line 315 */
final String v1 = " Brt adj times stats: "; // const-string v1, " Brt adj times stats: "
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 316 */
v1 = this.mCbmEvents;
/* new-instance v2, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda13; */
/* invoke-direct {v2, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda13;-><init>(Ljava/io/PrintWriter;)V */
/* .line 317 */
v1 = this.mHistoryCbmEvents;
v1 = (( java.util.ArrayDeque ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z
/* if-nez v1, :cond_0 */
/* .line 318 */
final String v1 = " History brt adj times stats: "; // const-string v1, " History brt adj times stats: "
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 319 */
v1 = this.mHistoryCbmEvents;
(( java.util.ArrayDeque ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map; */
/* .line 320 */
/* .local v2, "events":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;>;" */
/* new-instance v3, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda14; */
/* invoke-direct {v3, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda14;-><init>(Ljava/io/PrintWriter;)V */
/* .line 324 */
} // .end local v2 # "events":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;>;"
/* .line 326 */
} // :cond_0
return;
} // .end method
protected Boolean isBrightnessAdjustNoted ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "cbmState" # I */
/* .line 296 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->getCbmEvent(I)Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* iget-boolean v0, v0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
} // .end method
protected void noteAutoAdjustmentTimes ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "cbmState" # I */
/* .line 80 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda6; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 91 */
return;
} // .end method
public void noteIndividualResult ( Float p0, Integer p1, Float p2 ) {
/* .locals 11 */
/* .param p1, "lux" # F */
/* .param p2, "appId" # I */
/* .param p3, "brightness" # F */
/* .line 210 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* .line 211 */
/* .local v7, "now":J */
v9 = this.mBgHandler;
/* new-instance v10, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda5; */
/* move-object v0, v10 */
/* move-object v1, p0 */
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-wide v5, v7 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;FIFJ)V */
(( android.os.Handler ) v9 ).post ( v10 ); // invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 222 */
return;
} // .end method
protected void noteManualAdjustmentTimes ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "cbmState" # I */
/* .line 94 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.display.aiautobrt.CbmStateTracker ) p0 ).noteManualAdjustmentTimes ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->noteManualAdjustmentTimes(II)V
/* .line 95 */
return;
} // .end method
protected void noteManualAdjustmentTimes ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "cbmState" # I */
/* .param p2, "brtAdjSceneState" # I */
/* .line 98 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda10; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 121 */
return;
} // .end method
protected void noteStartCbmStateTracking ( Integer p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .param p2, "timeMills" # J */
/* .line 124 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda7; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;IJ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 140 */
return;
} // .end method
protected void noteStartPredictTracking ( Long p0 ) {
/* .locals 2 */
/* .param p1, "timeMills" # J */
/* .line 170 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;J)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 185 */
return;
} // .end method
protected void noteStopCbmStateTracking ( Integer p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .param p2, "timeMills" # J */
/* .line 143 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda15; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda15;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;IJ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 163 */
return;
} // .end method
protected void noteStopPredictTracking ( Long p0 ) {
/* .locals 2 */
/* .param p1, "timeMills" # J */
/* .line 192 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda8; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;J)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 207 */
return;
} // .end method
protected void resetBrtAdjSceneCount ( ) {
/* .locals 2 */
/* .line 282 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 288 */
return;
} // .end method
protected void startCbmStats ( ) {
/* .locals 11 */
/* .line 225 */
int v0 = 0; // const/4 v0, 0x0
/* .line 226 */
/* .local v0, "minType":I */
/* const/high16 v1, -0x40800000 # -1.0f */
/* .line 227 */
/* .local v1, "minRatio":F */
int v2 = 0; // const/4 v2, 0x0
/* .line 228 */
/* .local v2, "minComparedModelNum":I */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker;->storeHistoryCbmEvents()V */
/* .line 229 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 230 */
/* .local v3, "tempCbmEvents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;" */
v4 = this.mCbmEvents;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 231 */
/* .local v5, "stateRecord":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;" */
/* new-instance v6, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* invoke-direct {v6}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;-><init>()V */
/* .line 232 */
/* .local v6, "tempRecord":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 233 */
/* .local v7, "type":I */
/* check-cast v8, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 234 */
/* .local v8, "record":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
v9 = (( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) v8 ).getManualAdjustRatio ( ); // invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->getManualAdjustRatio()F
/* .line 235 */
/* .local v9, "ratio":F */
v10 = java.lang.Float .isNaN ( v9 );
/* if-nez v10, :cond_1 */
/* .line 236 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 237 */
/* const/high16 v10, -0x40800000 # -1.0f */
/* cmpl-float v10, v1, v10 */
/* if-nez v10, :cond_0 */
/* move v10, v9 */
} // :cond_0
/* move v10, v1 */
} // :goto_1
/* move v1, v10 */
/* .line 238 */
/* cmpg-float v10, v9, v1 */
/* if-gtz v10, :cond_1 */
/* .line 239 */
/* move v0, v7 */
/* .line 240 */
/* move v1, v9 */
/* .line 243 */
} // :cond_1
(( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ) v6 ).copyFrom ( v8 ); // invoke-virtual {v6, v8}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->copyFrom(Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;)V
/* .line 244 */
java.lang.Integer .valueOf ( v7 );
/* .line 245 */
} // .end local v5 # "stateRecord":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;>;"
} // .end local v6 # "tempRecord":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;
} // .end local v7 # "type":I
} // .end local v8 # "record":Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;
} // .end local v9 # "ratio":F
/* .line 246 */
} // :cond_2
v4 = this.mCbmEvents;
/* .line 247 */
v4 = this.mCbmEvents;
/* .line 248 */
int v4 = 2; // const/4 v4, 0x2
/* if-lt v2, v4, :cond_3 */
/* .line 249 */
/* move v4, v0 */
/* .line 250 */
/* .local v4, "minimumType":I */
/* move v5, v1 */
/* .line 251 */
/* .local v5, "minimumRatio":F */
v6 = this.mHandler;
/* new-instance v7, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda4; */
/* invoke-direct {v7, p0, v4, v5}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;IF)V */
(( android.os.Handler ) v6 ).post ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 252 */
} // .end local v4 # "minimumType":I
} // .end local v5 # "minimumRatio":F
/* .line 253 */
} // :cond_3
final String v4 = "CbmController-Tracker"; // const-string v4, "CbmController-Tracker"
final String v5 = "Model switch cannot be satisfied."; // const-string v5, "Model switch cannot be satisfied."
android.util.Slog .w ( v4,v5 );
/* .line 255 */
} // :goto_2
return;
} // .end method
protected void startEvaluateCustomCurve ( ) {
/* .locals 2 */
/* .line 273 */
v0 = this.mBgHandler;
/* new-instance v1, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/aiautobrt/CbmStateTracker;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 279 */
return;
} // .end method
