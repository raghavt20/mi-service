class com.android.server.display.aiautobrt.CbmStateTracker$StateRecord {
	 /* .source "CbmStateTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/aiautobrt/CbmStateTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "StateRecord" */
} // .end annotation
/* # instance fields */
private Float autoAdjustTimes;
private Integer brightRoomAdjTimes;
private Integer darkRoomAdjTimes;
private Boolean initialize;
private Float manualAdjustTimes;
private Long predictDurations;
private Boolean predictTracking;
private Long startPredictTimeMills;
private Long startTimeMills;
protected Boolean tracking;
protected Integer type;
private Long usageDurations;
/* # direct methods */
static Float -$$Nest$fgetautoAdjustTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->autoAdjustTimes:F */
} // .end method
static Integer -$$Nest$fgetbrightRoomAdjTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->brightRoomAdjTimes:I */
} // .end method
static Integer -$$Nest$fgetdarkRoomAdjTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->darkRoomAdjTimes:I */
} // .end method
static Boolean -$$Nest$fgetinitialize ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->initialize:Z */
} // .end method
static Float -$$Nest$fgetmanualAdjustTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->manualAdjustTimes:F */
} // .end method
static Long -$$Nest$fgetpredictDurations ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictDurations:J */
/* return-wide v0 */
} // .end method
static Boolean -$$Nest$fgetpredictTracking ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictTracking:Z */
} // .end method
static Long -$$Nest$fgetstartPredictTimeMills ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startPredictTimeMills:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetstartTimeMills ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startTimeMills:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetusageDurations ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->usageDurations:J */
/* return-wide v0 */
} // .end method
static void -$$Nest$fputautoAdjustTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->autoAdjustTimes:F */
return;
} // .end method
static void -$$Nest$fputbrightRoomAdjTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->brightRoomAdjTimes:I */
return;
} // .end method
static void -$$Nest$fputdarkRoomAdjTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->darkRoomAdjTimes:I */
return;
} // .end method
static void -$$Nest$fputinitialize ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->initialize:Z */
return;
} // .end method
static void -$$Nest$fputmanualAdjustTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->manualAdjustTimes:F */
return;
} // .end method
static void -$$Nest$fputpredictDurations ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictDurations:J */
return;
} // .end method
static void -$$Nest$fputpredictTracking ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictTracking:Z */
return;
} // .end method
static void -$$Nest$fputstartPredictTimeMills ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startPredictTimeMills:J */
return;
} // .end method
static void -$$Nest$fputstartTimeMills ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startTimeMills:J */
return;
} // .end method
static void -$$Nest$fputusageDurations ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->usageDurations:J */
return;
} // .end method
static void -$$Nest$mresetBrtAdjSceneCount ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->resetBrtAdjSceneCount()V */
return;
} // .end method
static Boolean -$$Nest$msatisfyMaxBrtAdjTimes ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->satisfyMaxBrtAdjTimes()Z */
} // .end method
public com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ( ) {
/* .locals 0 */
/* .line 352 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 353 */
return;
} // .end method
public com.android.server.display.aiautobrt.CbmStateTracker$StateRecord ( ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 346 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 347 */
/* iput p1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
/* .line 348 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startTimeMills:J */
/* .line 349 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* .line 350 */
return;
} // .end method
private void resetBrtAdjSceneCount ( ) {
/* .locals 1 */
/* .line 408 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->darkRoomAdjTimes:I */
/* .line 409 */
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->brightRoomAdjTimes:I */
/* .line 410 */
return;
} // .end method
private Boolean satisfyMaxBrtAdjTimes ( ) {
/* .locals 2 */
/* .line 413 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->darkRoomAdjTimes:I */
int v1 = 5; // const/4 v1, 0x5
/* if-ge v0, v1, :cond_1 */
/* iget v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->brightRoomAdjTimes:I */
/* if-lt v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
/* # virtual methods */
public void copyFrom ( com.android.server.display.aiautobrt.CbmStateTracker$StateRecord p0 ) {
/* .locals 2 */
/* .param p1, "record" # Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord; */
/* .line 356 */
/* iget v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
/* .line 357 */
/* iget-wide v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startTimeMills:J */
/* iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startTimeMills:J */
/* .line 358 */
/* iget-boolean v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->tracking:Z */
/* .line 359 */
/* iget-wide v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startPredictTimeMills:J */
/* iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->startPredictTimeMills:J */
/* .line 360 */
/* iget-boolean v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictTracking:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictTracking:Z */
/* .line 361 */
/* iget-boolean v0, p1, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->initialize:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->initialize:Z */
/* .line 362 */
return;
} // .end method
protected Float getManualAdjustRatio ( ) {
/* .locals 3 */
/* .line 378 */
v0 = com.android.server.display.aiautobrt.CbmStateTracker .-$$Nest$sfgetsDebug ( );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
/* const/high16 v0, 0x42c80000 # 100.0f */
/* .line 379 */
/* .local v0, "minimumAdjustTimes":F */
} // :goto_0
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->autoAdjustTimes:F */
/* cmpg-float v2, v1, v0 */
/* if-gtz v2, :cond_1 */
/* .line 380 */
/* const/high16 v1, 0x7fc00000 # Float.NaN */
/* .line 382 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->manualAdjustTimes:F */
/* div-float/2addr v2, v1 */
} // .end method
protected Float getManualAdjustTimesPerHour ( ) {
/* .locals 4 */
/* .line 368 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->usageDurations:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
/* if-nez v2, :cond_0 */
/* .line 369 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 371 */
} // :cond_0
/* iget v2, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->manualAdjustTimes:F */
/* long-to-float v0, v0 */
/* div-float/2addr v2, v0 */
} // .end method
protected Long getPredictDurations ( ) {
/* .locals 2 */
/* .line 397 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->predictDurations:J */
/* return-wide v0 */
} // .end method
protected Long getUsageDuration ( ) {
/* .locals 2 */
/* .line 389 */
/* iget-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->usageDurations:J */
/* return-wide v0 */
} // .end method
protected void reCount ( ) {
/* .locals 2 */
/* .line 401 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->manualAdjustTimes:F */
/* .line 402 */
/* iput v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->autoAdjustTimes:F */
/* .line 403 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->usageDurations:J */
/* .line 404 */
/* invoke-direct {p0}, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->resetBrtAdjSceneCount()V */
/* .line 405 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 419 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->type:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", manual adj times: "; // const-string v1, ", manual adj times: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->manualAdjustTimes:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", auto adj times: "; // const-string v1, ", auto adj times: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->autoAdjustTimes:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", usage duration: "; // const-string v1, ", usage duration: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/aiautobrt/CbmStateTracker$StateRecord;->usageDurations:J */
/* .line 422 */
android.util.TimeUtils .formatDuration ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 419 */
} // .end method
