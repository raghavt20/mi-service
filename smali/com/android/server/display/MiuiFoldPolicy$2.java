class com.android.server.display.MiuiFoldPolicy$2 implements android.hardware.SensorEventListener {
	 /* .source "MiuiFoldPolicy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/MiuiFoldPolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.MiuiFoldPolicy this$0; //synthetic
/* # direct methods */
 com.android.server.display.MiuiFoldPolicy$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/MiuiFoldPolicy; */
/* .line 482 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 495 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 485 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.display.MiuiFoldPolicy .-$$Nest$fputmNeedOffDueToFoldGesture ( v0,v1 );
/* .line 486 */
v0 = this.values;
int v1 = 4; // const/4 v1, 0x4
/* aget v0, v0, v1 */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
/* .line 487 */
v0 = this.values;
/* const/16 v1, 0xb */
/* aget v0, v0, v1 */
v1 = this.this$0;
v1 = com.android.server.display.MiuiFoldPolicy .-$$Nest$fgetmFoldGestureAngleThreshold ( v1 );
/* int-to-float v1, v1 */
/* cmpg-float v0, v0, v1 */
/* if-gez v0, :cond_0 */
/* .line 488 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.MiuiFoldPolicy .-$$Nest$fputmNeedOffDueToFoldGesture ( v0,v1 );
/* .line 491 */
} // :cond_0
return;
} // .end method
