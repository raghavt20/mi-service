public abstract class com.android.server.display.MiuiDisplayCloudController$Callback {
	 /* .source "MiuiDisplayCloudController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/MiuiDisplayCloudController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "Callback" */
} // .end annotation
/* # virtual methods */
public abstract void notifyDisableResetShortTermModel ( Boolean p0 ) {
} // .end method
public abstract void notifyThresholdSunlightNitChanged ( Float p0 ) {
} // .end method
public abstract void updateOutdoorThermalAppCategoryList ( java.util.List p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
} // .end method
