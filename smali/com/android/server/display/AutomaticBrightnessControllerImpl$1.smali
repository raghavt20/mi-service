.class Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;
.super Landroid/hardware/camera2/CameraManager$TorchCallback;
.source "AutomaticBrightnessControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/AutomaticBrightnessControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 257
    iput-object p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraManager$TorchCallback;-><init>()V

    return-void
.end method

.method private getRoleIds(Landroid/hardware/camera2/CameraCharacteristics;Ljava/lang/String;)[Ljava/lang/Integer;
    .locals 2
    .param p1, "cameraCharacteristics"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p2, "cameraRoleId"    # Ljava/lang/String;

    .line 329
    :try_start_0
    new-instance v0, Landroid/hardware/camera2/CameraCharacteristics$Key;

    const-class v1, [Ljava/lang/Integer;

    invoke-direct {v0, p2, v1}, Landroid/hardware/camera2/CameraCharacteristics$Key;-><init>(Ljava/lang/String;Ljava/lang/Class;)V

    .line 330
    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    return-object v0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    return-object v1
.end method


# virtual methods
.method public onTorchModeChanged(Ljava/lang/String;Z)V
    .locals 10
    .param p1, "cameraId"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .line 276
    const-string v0, "AutomaticBrightnessControllerImpl"

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmCameraManager(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Landroid/hardware/camera2/CameraManager;

    move-result-object v2

    .line 277
    invoke-virtual {v2, p1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v2

    .line 278
    .local v2, "cameraCharacteristics":Landroid/hardware/camera2/CameraCharacteristics;
    sget-object v3, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 279
    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 280
    .local v3, "facing":Ljava/lang/Integer;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_0

    .line 281
    iget-object v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    sget-object v5, Landroid/hardware/camera2/CameraCharacteristics;->FLASH_INFO_AVAILABLE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 282
    invoke-virtual {v2, v5}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmFrontFlashAvailable(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V

    .line 286
    :cond_0
    iget-object v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v4}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmFrontFlashAvailable(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v4}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmExitFacingCameraIds(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    if-eqz p1, :cond_2

    .line 289
    iget-object v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v4}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmExitFacingCameraIds(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v6, v1

    :goto_0
    if-ge v6, v5, :cond_2

    aget-object v7, v4, v6

    .line 290
    .local v7, "id":Ljava/lang/String;
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 291
    return-void

    .line 289
    .end local v7    # "id":Ljava/lang/String;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 295
    :cond_2
    const/4 v4, 0x0

    .line 296
    .local v4, "roleIds":[Ljava/lang/Integer;
    invoke-static {}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$sfgetCAMERA_ROLE_IDS()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v7, v1

    :goto_1
    if-ge v7, v6, :cond_4

    aget-object v8, v5, v7

    .line 297
    .local v8, "cameraRoleId":Ljava/lang/String;
    invoke-direct {p0, v2, v8}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->getRoleIds(Landroid/hardware/camera2/CameraCharacteristics;Ljava/lang/String;)[Ljava/lang/Integer;

    move-result-object v9

    move-object v4, v9

    .line 298
    if-eqz v4, :cond_3

    .line 299
    goto :goto_2

    .line 296
    .end local v8    # "cameraRoleId":Ljava/lang/String;
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 302
    :cond_4
    :goto_2
    if-eqz v4, :cond_6

    .line 303
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 304
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    const/16 v6, 0x65

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 305
    const/16 v6, 0x66

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_6

    .line 306
    :cond_5
    return-void

    .line 312
    .end local v2    # "cameraCharacteristics":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v3    # "facing":Ljava/lang/Integer;
    .end local v4    # "roleIds":[Ljava/lang/Integer;
    .end local v5    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v2

    .line 313
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "onTorchModeChanged: can\'t get camera characteristics key"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 309
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 310
    .local v2, "e":Landroid/hardware/camera2/CameraAccessException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTorchModeChanged: can\'t get characteristics for camera "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 314
    .end local v2    # "e":Landroid/hardware/camera2/CameraAccessException;
    :cond_6
    nop

    .line 315
    :goto_3
    if-eqz p2, :cond_7

    .line 316
    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V

    goto :goto_4

    .line 318
    :cond_7
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 319
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmTorchCloseTime(Lcom/android/server/display/AutomaticBrightnessControllerImpl;J)V

    .line 321
    :cond_8
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v2, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V

    .line 323
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTorchModeChanged, mIsTorchOpen="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    return-void
.end method

.method public onTorchModeUnavailable(Ljava/lang/String;)V
    .locals 5
    .param p1, "cameraId"    # Ljava/lang/String;

    .line 262
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmExitFacingCameraIds(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 263
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmExitFacingCameraIds(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 264
    .local v3, "id":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 265
    return-void

    .line 263
    .end local v3    # "id":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V

    .line 270
    return-void
.end method
