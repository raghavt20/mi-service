class com.android.server.display.ThermalObserver$TemperatureObserver extends android.os.FileObserver {
	 /* .source "ThermalObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/ThermalObserver; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "TemperatureObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.ThermalObserver this$0; //synthetic
/* # direct methods */
public com.android.server.display.ThermalObserver$TemperatureObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/ThermalObserver; */
/* .param p2, "file" # Ljava/io/File; */
/* .line 48 */
this.this$0 = p1;
/* .line 49 */
/* invoke-direct {p0, p2}, Landroid/os/FileObserver;-><init>(Ljava/io/File;)V */
/* .line 50 */
return;
} // .end method
/* # virtual methods */
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 54 */
/* and-int/lit16 v0, p1, 0xfff */
/* .line 55 */
/* .local v0, "type":I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 56 */
v1 = this.this$0;
com.android.server.display.ThermalObserver .-$$Nest$fgetmHandler ( v1 );
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.display.ThermalObserver$TemperatureControllerHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/ThermalObserver$TemperatureControllerHandler;->removeMessages(I)V
/* .line 57 */
v1 = this.this$0;
com.android.server.display.ThermalObserver .-$$Nest$fgetmHandler ( v1 );
(( com.android.server.display.ThermalObserver$TemperatureControllerHandler ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/ThermalObserver$TemperatureControllerHandler;->sendEmptyMessage(I)Z
/* .line 59 */
} // :cond_0
return;
} // .end method
