class com.android.server.display.BrightnessCurve$FirstOutdoorLightCurve extends com.android.server.display.BrightnessCurve$Curve {
	 /* .source "BrightnessCurve.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/BrightnessCurve; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "FirstOutdoorLightCurve" */
} // .end annotation
/* # instance fields */
private final Float mAnchorPointLux;
private final Float mHeadLux;
private final Float mPullUpAnchorPointLux;
private final Float mPullUpAnchorPointNit;
private final Float mTailLux;
final com.android.server.display.BrightnessCurve this$0; //synthetic
/* # direct methods */
public com.android.server.display.BrightnessCurve$FirstOutdoorLightCurve ( ) {
/* .locals 5 */
/* .param p1, "this$0" # Lcom/android/server/display/BrightnessCurve; */
/* .line 461 */
this.this$0 = p1;
/* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve$Curve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 462 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v1 = 2; // const/4 v1, 0x2
/* aget v0, v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mHeadLux:F */
/* .line 463 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v2 = 3; // const/4 v2, 0x3
/* aget v1, v1, v2 */
/* iput v1, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
/* .line 464 */
v2 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurveAnchorPointLux ( p1 );
/* iput v2, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
/* .line 465 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v3 = 1; // const/4 v3, 0x1
/* aget v2, v2, v3 */
/* iput v2, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mPullUpAnchorPointLux:F */
/* .line 466 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( p1 );
v3 = (( android.util.Spline ) v3 ).interpolate ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/Spline;->interpolate(F)F
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( p1 );
v4 = (( android.util.Spline ) v4 ).interpolate ( v0 ); // invoke-virtual {v4, v0}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v3, v4 */
/* sub-float v0, v1, v0 */
/* div-float/2addr v3, v0 */
/* .line 468 */
/* .local v3, "tan":F */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( p1 );
v0 = (( android.util.Spline ) v0 ).interpolate ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v1, v2 */
/* mul-float/2addr v1, v3 */
/* sub-float/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mPullUpAnchorPointNit:F */
/* .line 470 */
return;
} // .end method
private void pullUpConnectLeft ( android.util.Pair p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 511 */
/* .local p1, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v0 = this.mPointList;
/* .line 512 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
v0 = (( android.util.Spline ) v0 ).interpolate ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
/* .line 513 */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v1, v2 */
/* div-float/2addr v0, v1 */
/* .line 514 */
/* .local v0, "tanToAnchorPoint":F */
v1 = this.this$0;
v1 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_0 */
/* .line 515 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 517 */
} // :cond_0
v1 = this.this$0;
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* .line 518 */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
v3 = this.first;
/* check-cast v3, Ljava/lang/Float; */
/* .line 519 */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v2, v3 */
/* mul-float/2addr v2, v0 */
/* add-float/2addr v1, v2 */
/* .line 520 */
/* .local v1, "anchorPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
java.lang.Float .valueOf ( v3 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 521 */
/* .local v2, "anchorPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v3 = this.this$0;
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v5 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v3 );
v6 = this.mPointList;
com.android.server.display.BrightnessCurve .-$$Nest$mpullUpConnectTail ( v3,v2,v4,v5,v6 );
/* .line 523 */
} // .end local v1 # "anchorPointNit":F
} // .end local v2 # "anchorPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void connectLeft ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 8 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 485 */
v0 = v0 = this.mPointList;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 486 */
v0 = this.mPointList;
v1 = v1 = this.mPointList;
/* add-int/lit8 v1, v1, -0x1 */
/* check-cast v0, Landroid/util/Pair; */
/* .line 487 */
/* .local v0, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v2 );
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mHeadLux:F */
v2 = (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 488 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mHeadLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 489 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 490 */
v1 = this.mPointList;
/* .line 491 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mAnchorPointLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 493 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->pullUpConnectLeft(Landroid/util/Pair;)V */
/* .line 496 */
} // .end local v0 # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_2
} // :goto_0
return;
} // .end method
public void connectRight ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 8 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 500 */
v0 = v0 = this.mPointList;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 501 */
v0 = this.mPointList;
int v1 = 0; // const/4 v1, 0x0
/* check-cast v0, Landroid/util/Pair; */
/* .line 502 */
/* .local v0, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v2 );
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v2 = (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 503 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mHeadLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 505 */
} // :cond_0
v1 = this.first;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
(( com.android.server.display.BrightnessCurve$FirstOutdoorLightCurve ) p0 ).create ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->create(FF)V
/* .line 508 */
} // .end local v0 # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_1
} // :goto_0
return;
} // .end method
public void create ( Float p0, Float p1 ) {
/* .locals 9 */
/* .param p1, "lux" # F */
/* .param p2, "changeNit" # F */
/* .line 474 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v0, v0, p2 */
/* if-lez v0, :cond_0 */
/* .line 475 */
v1 = this.this$0;
v4 = this.mPointList;
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mHeadLux:F */
/* iget v6, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
v0 = this.this$0;
v7 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v0 );
v0 = this.this$0;
v8 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v0 );
/* move v2, p1 */
/* move v3, p2 */
/* invoke-static/range {v1 ..v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullDownCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V */
/* .line 478 */
} // :cond_0
v1 = this.this$0;
v4 = this.mPointList;
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mHeadLux:F */
/* iget v6, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mTailLux:F */
/* iget v7, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mPullUpAnchorPointLux:F */
/* iget v8, p0, Lcom/android/server/display/BrightnessCurve$FirstOutdoorLightCurve;->mPullUpAnchorPointNit:F */
/* move v2, p1 */
/* move v3, p2 */
/* invoke-static/range {v1 ..v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullUpCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V */
/* .line 481 */
} // :goto_0
return;
} // .end method
