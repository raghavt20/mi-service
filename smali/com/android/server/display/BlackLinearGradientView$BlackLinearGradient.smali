.class Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;
.super Landroid/graphics/LinearGradient;
.source "BlackLinearGradientView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/BlackLinearGradientView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BlackLinearGradient"
.end annotation


# instance fields
.field endX:F

.field endY:F

.field startX:F

.field startY:F

.field final synthetic this$0:Lcom/android/server/display/BlackLinearGradientView;


# direct methods
.method public constructor <init>(Lcom/android/server/display/BlackLinearGradientView;FFFFIILandroid/graphics/Shader$TileMode;)V
    .locals 9
    .param p2, "x0"    # F
    .param p3, "y0"    # F
    .param p4, "x1"    # F
    .param p5, "y1"    # F
    .param p6, "color0"    # I
    .param p7, "color1"    # I
    .param p8, "tile"    # Landroid/graphics/Shader$TileMode;

    .line 104
    move-object v8, p0

    move-object v0, p1

    iput-object v0, v8, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->this$0:Lcom/android/server/display/BlackLinearGradientView;

    .line 105
    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Lcom/android/server/display/BlackLinearGradientView;FFFF[I[FLandroid/graphics/Shader$TileMode;)V
    .locals 9
    .param p2, "x0"    # F
    .param p3, "y0"    # F
    .param p4, "x1"    # F
    .param p5, "y1"    # F
    .param p6, "colors"    # [I
    .param p7, "positions"    # [F
    .param p8, "tile"    # Landroid/graphics/Shader$TileMode;

    .line 99
    move-object v8, p0

    move-object v0, p1

    iput-object v0, v8, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->this$0:Lcom/android/server/display/BlackLinearGradientView;

    .line 100
    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 101
    return-void
.end method


# virtual methods
.method public getEndX()F
    .locals 1

    .line 125
    iget v0, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->endX:F

    return v0
.end method

.method public getEndY()F
    .locals 1

    .line 133
    iget v0, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->endY:F

    return v0
.end method

.method public getStartX()F
    .locals 1

    .line 109
    iget v0, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->startX:F

    return v0
.end method

.method public getStartY()F
    .locals 1

    .line 117
    iget v0, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->startY:F

    return v0
.end method

.method public setEndX(F)V
    .locals 0
    .param p1, "endX"    # F

    .line 129
    iput p1, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->endX:F

    .line 130
    return-void
.end method

.method public setEndY(F)V
    .locals 0
    .param p1, "endY"    # F

    .line 137
    iput p1, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->endY:F

    .line 138
    return-void
.end method

.method public setStartX(F)V
    .locals 0
    .param p1, "startX"    # F

    .line 113
    iput p1, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->startX:F

    .line 114
    return-void
.end method

.method public setStartY(F)V
    .locals 0
    .param p1, "startY"    # F

    .line 121
    iput p1, p0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;->startY:F

    .line 122
    return-void
.end method
