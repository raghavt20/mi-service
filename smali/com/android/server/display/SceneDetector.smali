.class public Lcom/android/server/display/SceneDetector;
.super Ljava/lang/Object;
.source "SceneDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/SceneDetector$SceneDetectorHandler;
    }
.end annotation


# static fields
.field private static final ACTION_CLIENT_SERVICE:Ljava/lang/String; = "com.xiaomi.aon.AonFlareService"

.field private static final AON_WAIT_DATA_STATE:I = 0x2

.field private static final AON_WAIT_REGISTER_STATE:I = 0x1

.field private static final DEFAULT_FRAMES_PER_SECOND:F = 1.0f

.field private static final DEFAULT_MAX_DELAY_TIME:I = 0x1388

.field private static final MSG_CHECK_AON_DATA_RETURN:I = 0x1

.field private static final MSG_UPDATE_BIND_AON_STATUS:I = 0x2

.field private static final MSG_UPDATE_UNBIND_AON_STATUS:I = 0x3

.field private static final SUPPRESS_DARKEN_EVENT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SceneDetector"

.field private static final TYPE_AON_FLARE:I = 0x5

.field private static sDEBUG:Z


# instance fields
.field private mAmbientLux:F

.field private mAonFlareMaxDelayTime:I

.field private mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

.field private mAonState:I

.field private mAutomaticBrightnessEnable:Z

.field private mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

.field private mContext:Landroid/content/Context;

.field private mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private mDualSensorPolicyListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

.field private mIsMainDarkenEvent:Z

.field private mMainHandler:Landroid/os/Handler;

.field private mMaxAonFlareEnableLux:F

.field private mMiAONListener:Lcom/xiaomi/aon/IMiAONListener;

.field private mMinAonFlareEnableLux:F

.field private mPreAmbientLux:F

.field private mSceneDetectorHandler:Landroid/os/Handler;

.field private final mServiceComponent:Landroid/content/ComponentName;

.field private mServiceConnected:Z

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public static synthetic $r8$lambda$h3y05PBjFEj-uL3MWDfuyyhV-og(Lcom/android/server/display/SceneDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->lambda$updateAutoBrightness$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$inSCfs7rsK1izZETiUec96Nmay0(Lcom/android/server/display/SceneDetector;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector;->lambda$updateAmbientLux$0(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$yK3nBKl5psr65swE9Q3PcvYCogE(Lcom/android/server/display/SceneDetector;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector;->lambda$updateAmbientLux$1(F)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAonState(Lcom/android/server/display/SceneDetector;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmBrightnessControllerImpl(Lcom/android/server/display/SceneDetector;)Lcom/android/server/display/AutomaticBrightnessControllerImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SceneDetector;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Lcom/android/server/display/SceneDetector;)Landroid/os/IBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SceneDetector;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsMainDarkenEvent(Lcom/android/server/display/SceneDetector;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreAmbientLux(Lcom/android/server/display/SceneDetector;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/SceneDetector;->mPreAmbientLux:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSceneDetectorHandler(Lcom/android/server/display/SceneDetector;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAonFlareService(Lcom/android/server/display/SceneDetector;Lcom/xiaomi/aon/IAONFlareService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAonState(Lcom/android/server/display/SceneDetector;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsMainDarkenEvent(Lcom/android/server/display/SceneDetector;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmServiceConnected(Lcom/android/server/display/SceneDetector;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetServiceConnectedStatus(Lcom/android/server/display/SceneDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->resetServiceConnectedStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtryToBindAonFlareService(Lcom/android/server/display/SceneDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->tryToBindAonFlareService()V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterAonFlareListener(Lcom/android/server/display/SceneDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->unregisterAonFlareListener()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAutoBrightness(Lcom/android/server/display/SceneDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->updateAutoBrightness()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/display/SceneDetector;->sDEBUG:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 3
    .param p1, "listener"    # Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;
    .param p2, "brightnessControllerImpl"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "context"    # Landroid/content/Context;

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    .line 208
    new-instance v0, Lcom/android/server/display/SceneDetector$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/SceneDetector$1;-><init>(Lcom/android/server/display/SceneDetector;)V

    iput-object v0, p0, Lcom/android/server/display/SceneDetector;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    .line 230
    new-instance v0, Lcom/android/server/display/SceneDetector$2;

    invoke-direct {v0, p0}, Lcom/android/server/display/SceneDetector$2;-><init>(Lcom/android/server/display/SceneDetector;)V

    iput-object v0, p0, Lcom/android/server/display/SceneDetector;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 258
    new-instance v0, Lcom/android/server/display/SceneDetector$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/SceneDetector$3;-><init>(Lcom/android/server/display/SceneDetector;)V

    iput-object v0, p0, Lcom/android/server/display/SceneDetector;->mMiAONListener:Lcom/xiaomi/aon/IMiAONListener;

    .line 66
    iput-object p3, p0, Lcom/android/server/display/SceneDetector;->mMainHandler:Landroid/os/Handler;

    .line 67
    iput-object p4, p0, Lcom/android/server/display/SceneDetector;->mContext:Landroid/content/Context;

    .line 68
    iput-object p1, p0, Lcom/android/server/display/SceneDetector;->mDualSensorPolicyListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    .line 69
    new-instance v0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;-><init>(Lcom/android/server/display/SceneDetector;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    .line 70
    iput-object p2, p0, Lcom/android/server/display/SceneDetector;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 71
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 72
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x1107002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/SceneDetector;->mMaxAonFlareEnableLux:F

    .line 74
    const v1, 0x11070031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/SceneDetector;->mMinAonFlareEnableLux:F

    .line 76
    const v1, 0x110b000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareMaxDelayTime:I

    .line 78
    const v1, 0x110f00a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "componentName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/server/display/SceneDetector;->getAonFlareServiceComponent(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/display/SceneDetector;->mServiceComponent:Landroid/content/ComponentName;

    .line 81
    return-void
.end method

.method private checkAonFlareStatus(F)V
    .locals 2
    .param p1, "preLux"    # F

    .line 150
    sget-boolean v0, Lcom/android/server/display/SceneDetector;->sDEBUG:Z

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkAonFlareStatus mIsMainDarkenEvent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mAonState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SceneDetector"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    iget v0, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 155
    iput p1, p0, Lcom/android/server/display/SceneDetector;->mPreAmbientLux:F

    .line 156
    iget-boolean v0, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    if-eqz v0, :cond_1

    .line 157
    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->registerAonFlareListener()V

    goto :goto_0

    .line 159
    :cond_1
    invoke-direct {p0}, Lcom/android/server/display/SceneDetector;->updateAutoBrightness()V

    .line 162
    :cond_2
    :goto_0
    return-void
.end method

.method private getAonFlareServiceComponent(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 134
    if-eqz p1, :cond_0

    .line 135
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0

    .line 137
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private synthetic lambda$updateAmbientLux$0(Z)V
    .locals 1
    .param p1, "isMainDarkenEvent"    # Z

    .line 94
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyUpdateBrightness()V

    .line 95
    iput-boolean p1, p0, Lcom/android/server/display/SceneDetector;->mIsMainDarkenEvent:Z

    .line 96
    return-void
.end method

.method private synthetic lambda$updateAmbientLux$1(F)V
    .locals 0
    .param p1, "preLux"    # F

    .line 101
    invoke-direct {p0, p1}, Lcom/android/server/display/SceneDetector;->checkAonFlareStatus(F)V

    .line 102
    return-void
.end method

.method private synthetic lambda$updateAutoBrightness$2()V
    .locals 4

    .line 143
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mDualSensorPolicyListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    sget v1, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_MAIN_LUX_EVENT:I

    iget v2, p0, Lcom/android/server/display/SceneDetector;->mAmbientLux:F

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 145
    return-void
.end method

.method private registerAonFlareListener()V
    .locals 6

    .line 167
    const-string v0, "SceneDetector"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mMiAONListener:Lcom/xiaomi/aon/IMiAONListener;

    const/4 v3, 0x5

    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v5, 0x1388

    invoke-interface {v1, v3, v4, v5, v2}, Lcom/xiaomi/aon/IAONFlareService;->registerListener(IFILcom/xiaomi/aon/IMiAONListener;)V

    .line 169
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/android/server/display/SceneDetector;->mAonFlareMaxDelayTime:I

    int-to-long v2, v2

    const/4 v4, 0x1

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 171
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    .line 172
    const-string v1, "registerAonFlareListener: register aon flare listener success."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    goto :goto_0

    .line 173
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "registerAonFlareListener: register aon flare listener failed."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private resetServiceConnectedStatus()V
    .locals 3

    .line 222
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z

    if-eqz v1, :cond_0

    .line 223
    invoke-interface {v0}, Lcom/xiaomi/aon/IAONFlareService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 224
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    .line 226
    iput-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z

    .line 228
    :cond_0
    return-void
.end method

.method private tryToBindAonFlareService()V
    .locals 6

    .line 109
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mServiceComponent:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    .line 111
    return-void

    .line 113
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/SceneDetector;->mServiceConnected:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    if-eqz v0, :cond_1

    .line 115
    return-void

    .line 117
    :cond_1
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    .line 118
    .local v0, "userId":I
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.xiaomi.aon.AonFlareService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mServiceComponent:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 120
    const/high16 v2, 0x800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 121
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/display/SceneDetector;->mServiceConnection:Landroid/content/ServiceConnection;

    new-instance v4, Landroid/os/UserHandle;

    invoke-direct {v4, v0}, Landroid/os/UserHandle;-><init>(I)V

    const/4 v5, 0x1

    invoke-virtual {v2, v1, v3, v5, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unable to bind service: bindService failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SceneDetector"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_2
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyUpdateBrightness()V

    .line 126
    return-void
.end method

.method private unregisterAonFlareListener()V
    .locals 4

    .line 199
    const-string v0, "SceneDetector"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareService:Lcom/xiaomi/aon/IAONFlareService;

    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mMiAONListener:Lcom/xiaomi/aon/IMiAONListener;

    const/4 v3, 0x5

    invoke-interface {v1, v3, v2}, Lcom/xiaomi/aon/IAONFlareService;->unregisterListener(ILcom/xiaomi/aon/IMiAONListener;)V

    .line 200
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 201
    iput v2, p0, Lcom/android/server/display/SceneDetector;->mAonState:I

    .line 202
    const-string/jumbo v1, "unregisterAonFlareListener: unregister aon flare listener success."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    goto :goto_0

    .line 203
    :catch_0
    move-exception v1

    .line 204
    .local v1, "e":Landroid/os/RemoteException;
    const-string/jumbo v2, "unregisterAonFlareListener: unregister aon flare listener failed."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private updateAutoBrightness()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 146
    return-void
.end method


# virtual methods
.method public configure(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 179
    const/4 v0, 0x3

    const/4 v1, 0x2

    if-nez p1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z

    if-eqz v2, :cond_0

    .line 180
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z

    .line 182
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 183
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 184
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 185
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 186
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z

    if-nez v2, :cond_1

    .line 187
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/display/SceneDetector;->mAutomaticBrightnessEnable:Z

    .line 189
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 190
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 191
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 192
    .restart local v0    # "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 186
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    nop

    .line 194
    :goto_1
    return-void
.end method

.method protected dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 324
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 325
    const-string v0, "Scene Detector State:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mMaxAonFlareEnableLux="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SceneDetector;->mMaxAonFlareEnableLux:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mMinAonFlareEnableLux="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SceneDetector;->mMinAonFlareEnableLux:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAonFlareMaxDelayTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/SceneDetector;->mAonFlareMaxDelayTime:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 329
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z

    sput-boolean v0, Lcom/android/server/display/SceneDetector;->sDEBUG:Z

    .line 330
    return-void
.end method

.method public updateAmbientLux(IFZ)V
    .locals 4
    .param p1, "event"    # I
    .param p2, "lux"    # F
    .param p3, "isMainDarkenEvent"    # Z

    .line 90
    iget-object v0, p0, Lcom/android/server/display/SceneDetector;->mDualSensorPolicyListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v0}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getAmbientLux()F

    move-result v0

    .line 91
    .local v0, "preLux":F
    iput p2, p0, Lcom/android/server/display/SceneDetector;->mAmbientLux:F

    .line 93
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, p3}, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/SceneDetector;Z)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    const/4 v1, 0x1

    if-eqz p3, :cond_0

    iget v2, p0, Lcom/android/server/display/SceneDetector;->mMaxAonFlareEnableLux:F

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    iget v2, p0, Lcom/android/server/display/SceneDetector;->mMinAonFlareEnableLux:F

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mDualSensorPolicyListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    const/4 v3, 0x0

    invoke-interface {v2, p1, p2, v1, v3}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 100
    iget-object v1, p0, Lcom/android/server/display/SceneDetector;->mSceneDetectorHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0, v0}, Lcom/android/server/display/SceneDetector$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/SceneDetector;F)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 104
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/SceneDetector;->mDualSensorPolicyListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v2, p1, p2, v1, v1}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 106
    :goto_0
    return-void
.end method
