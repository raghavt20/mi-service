public class com.android.server.display.DisplayFeatureManagerServiceImpl implements com.android.server.display.DisplayFeatureManagerServiceStub {
	 /* .source "DisplayFeatureManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private com.android.server.display.DisplayFeatureManagerInternal mDisplayFeatureInternal;
	 /* # direct methods */
	 public com.android.server.display.DisplayFeatureManagerServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void init ( com.android.server.display.DisplayFeatureManagerInternal p0 ) {
		 /* .locals 0 */
		 /* .param p1, "displayFeatureInternal" # Lcom/android/server/display/DisplayFeatureManagerInternal; */
		 /* .line 12 */
		 this.mDisplayFeatureInternal = p1;
		 /* .line 13 */
		 return;
	 } // .end method
	 public void setVideoInformation ( Integer p0, Boolean p1, Float p2, Integer p3, Integer p4, Float p5, android.os.IBinder p6 ) {
		 /* .locals 8 */
		 /* .param p1, "pid" # I */
		 /* .param p2, "bulletChatStatus" # Z */
		 /* .param p3, "frameRate" # F */
		 /* .param p4, "width" # I */
		 /* .param p5, "height" # I */
		 /* .param p6, "compressionRatio" # F */
		 /* .param p7, "token" # Landroid/os/IBinder; */
		 /* .line 39 */
		 v0 = this.mDisplayFeatureInternal;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 40 */
			 /* move v1, p1 */
			 /* move v2, p2 */
			 /* move v3, p3 */
			 /* move v4, p4 */
			 /* move v5, p5 */
			 /* move v6, p6 */
			 /* move-object v7, p7 */
			 /* invoke-virtual/range {v0 ..v7}, Lcom/android/server/display/DisplayFeatureManagerInternal;->setVideoInformation(IZFIIFLandroid/os/IBinder;)V */
			 /* .line 43 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void updateBCBCState ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "state" # I */
		 /* .line 31 */
		 v0 = this.mDisplayFeatureInternal;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 32 */
			 (( com.android.server.display.DisplayFeatureManagerInternal ) v0 ).updateBCBCState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateBCBCState(I)V
			 /* .line 34 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void updateDozeBrightness ( Long p0, Integer p1 ) {
		 /* .locals 1 */
		 /* .param p1, "physicalDisplayId" # J */
		 /* .param p3, "brightness" # I */
		 /* .line 24 */
		 v0 = this.mDisplayFeatureInternal;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 25 */
			 (( com.android.server.display.DisplayFeatureManagerInternal ) v0 ).updateDozeBrightness ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateDozeBrightness(JI)V
			 /* .line 27 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void updateRhythmicAppCategoryList ( java.util.List p0, java.util.List p1 ) {
		 /* .locals 1 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
	 /* .line 46 */
	 /* .local p1, "imageAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 /* .local p2, "readAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 v0 = this.mDisplayFeatureInternal;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 47 */
		 (( com.android.server.display.DisplayFeatureManagerInternal ) v0 ).updateRhythmicAppCategoryList ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
		 /* .line 49 */
	 } // :cond_0
	 return;
} // .end method
public void updateScreenEffect ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "state" # I */
	 /* .line 17 */
	 v0 = this.mDisplayFeatureInternal;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 18 */
		 (( com.android.server.display.DisplayFeatureManagerInternal ) v0 ).updateScreenEffect ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateScreenEffect(I)V
		 /* .line 20 */
	 } // :cond_0
	 return;
} // .end method
public void updateScreenGrayscaleState ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "state" # I */
	 /* .line 53 */
	 v0 = this.mDisplayFeatureInternal;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 54 */
		 (( com.android.server.display.DisplayFeatureManagerInternal ) v0 ).updateScreenGrayscaleState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerInternal;->updateScreenGrayscaleState(I)V
		 /* .line 56 */
	 } // :cond_0
	 return;
} // .end method
