class com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DisplayFeatureManagerWrapper" */
} // .end annotation
/* # static fields */
private static final Integer AIDL_TRANSACTION_registerCallback;
private static final Integer AIDL_TRANSACTION_setFeature;
private static final Integer HIDL_TRANSACTION_interfaceDescriptor;
private static final Integer HIDL_TRANSACTION_registerCallback;
private static final Integer HIDL_TRANSACTION_setFeature;
private static final java.lang.String HWBINDER_BASE_INTERFACE_DESCRIPTOR;
private static final java.lang.String HWBINDER_INTERFACE_DESCRIPTOR;
private static final java.lang.String INTERFACE_DESCRIPTOR;
/* # instance fields */
private java.lang.String mDescriptor;
private android.os.IHwBinder mHwService;
private android.os.IBinder mService;
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .param p2, "service" # Ljava/lang/Object; */
/* .line 1620 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1621 */
/* instance-of v0, p2, Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 1622 */
	 /* move-object v0, p2 */
	 /* check-cast v0, Landroid/os/IBinder; */
	 this.mService = v0;
	 /* .line 1624 */
	 try { // :try_start_0
		 this.mDescriptor = v0;
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 1626 */
		 /* .line 1625 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 1627 */
	 } // :goto_0
	 v0 = this.mDescriptor;
	 v0 = 	 android.text.TextUtils .isEmpty ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 1628 */
		 /* const-string/jumbo v0, "vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature" */
		 this.mDescriptor = v0;
		 /* .line 1630 */
	 } // :cond_0
	 /* instance-of v0, p2, Landroid/os/IHwBinder; */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 1631 */
		 /* move-object v0, p2 */
		 /* check-cast v0, Landroid/os/IHwBinder; */
		 this.mHwService = v0;
		 /* .line 1633 */
		 try { // :try_start_1
			 (( com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->interfaceDescriptor()Ljava/lang/String;
			 this.mDescriptor = v0;
			 /* :try_end_1 */
			 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
			 /* .line 1635 */
			 /* .line 1634 */
			 /* :catch_1 */
			 /* move-exception v0 */
			 /* .line 1636 */
		 } // :goto_1
		 v0 = this.mDescriptor;
		 v0 = 		 android.text.TextUtils .isEmpty ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 1637 */
			 /* const-string/jumbo v0, "vendor.xiaomi.hardware.displayfeature@1.0::IDisplayFeature" */
			 this.mDescriptor = v0;
			 /* .line 1640 */
		 } // :cond_1
	 } // :goto_2
	 return;
} // .end method
private void callBinderTransact ( Integer p0, Integer p1, java.lang.Object...p2 ) {
	 /* .locals 6 */
	 /* .param p1, "transactId" # I */
	 /* .param p2, "flag" # I */
	 /* .param p3, "params" # [Ljava/lang/Object; */
	 /* .line 1678 */
	 android.os.Parcel .obtain ( );
	 /* .line 1679 */
	 /* .local v0, "data":Landroid/os/Parcel; */
	 android.os.Parcel .obtain ( );
	 /* .line 1681 */
	 /* .local v1, "reply":Landroid/os/Parcel; */
	 try { // :try_start_0
		 v2 = this.mDescriptor;
		 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
		 /* .line 1682 */
		 /* array-length v2, p3 */
		 int v3 = 0; // const/4 v3, 0x0
	 } // :goto_0
	 /* if-ge v3, v2, :cond_2 */
	 /* aget-object v4, p3, v3 */
	 /* .line 1683 */
	 /* .local v4, "param":Ljava/lang/Object; */
	 /* instance-of v5, v4, Ljava/lang/Integer; */
	 if ( v5 != null) { // if-eqz v5, :cond_0
		 /* .line 1684 */
		 /* move-object v5, v4 */
		 /* check-cast v5, Ljava/lang/Integer; */
		 v5 = 		 (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
		 (( android.os.Parcel ) v0 ).writeInt ( v5 ); // invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V
		 /* .line 1685 */
	 } // :cond_0
	 /* instance-of v5, v4, Landroid/os/IInterface; */
	 if ( v5 != null) { // if-eqz v5, :cond_1
		 /* .line 1686 */
		 /* move-object v5, v4 */
		 /* check-cast v5, Landroid/os/IInterface; */
		 (( android.os.Parcel ) v0 ).writeStrongBinder ( v5 ); // invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V
		 /* .line 1682 */
	 } // .end local v4 # "param":Ljava/lang/Object;
} // :cond_1
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1689 */
} // :cond_2
v2 = this.mService;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1690 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1695 */
} // :cond_3
/* nop */
} // :goto_2
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1696 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1697 */
/* .line 1695 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1692 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1693 */
/* .local v2, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "callBinderTransact transact fail."; // const-string v5, "callBinderTransact transact fail."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1695 */
/* nop */
} // .end local v2 # "e":Landroid/os/RemoteException;
/* .line 1698 */
} // :goto_3
return;
/* .line 1695 */
} // :goto_4
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 1696 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 1697 */
/* throw v2 */
} // .end method
private void callHwBinderTransact ( Integer p0, Integer p1, java.lang.Object...p2 ) {
/* .locals 6 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "flag" # I */
/* .param p3, "params" # [Ljava/lang/Object; */
/* .line 1701 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 1703 */
/* .local v0, "hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 1704 */
/* .local v1, "hidl_request":Landroid/os/HwParcel; */
v2 = this.mDescriptor;
(( android.os.HwParcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1705 */
/* array-length v2, p3 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, p3, v3 */
/* .line 1706 */
/* .local v4, "param":Ljava/lang/Object; */
/* instance-of v5, v4, Ljava/lang/Integer; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1707 */
/* move-object v5, v4 */
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
(( android.os.HwParcel ) v1 ).writeInt32 ( v5 ); // invoke-virtual {v1, v5}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1708 */
} // :cond_0
/* instance-of v5, v4, Landroid/os/IHwInterface; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1709 */
/* move-object v5, v4 */
/* check-cast v5, Landroid/os/IHwInterface; */
(( android.os.HwParcel ) v1 ).writeStrongBinder ( v5 ); // invoke-virtual {v1, v5}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 1705 */
} // .end local v4 # "param":Ljava/lang/Object;
} // :cond_1
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1712 */
} // :cond_2
v2 = this.mHwService;
/* .line 1713 */
(( android.os.HwParcel ) v0 ).verifySuccess ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 1714 */
(( android.os.HwParcel ) v1 ).releaseTemporaryStorage ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1718 */
} // .end local v1 # "hidl_request":Landroid/os/HwParcel;
/* :catchall_0 */
/* move-exception v1 */
/* .line 1715 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1716 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_1
final String v2 = "DisplayFeatureManagerService"; // const-string v2, "DisplayFeatureManagerService"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "callHwBinderTransact transact fail."; // const-string v4, "callHwBinderTransact transact fail."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1718 */
/* nop */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_2
(( android.os.HwParcel ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->release()V
/* .line 1719 */
/* nop */
/* .line 1720 */
return;
/* .line 1718 */
} // :goto_3
(( android.os.HwParcel ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->release()V
/* .line 1719 */
/* throw v1 */
} // .end method
/* # virtual methods */
public java.lang.String interfaceDescriptor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1660 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 1661 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 1663 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 1665 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mHwService;
/* const v3, 0xf445343 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1667 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 1668 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 1670 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1671 */
/* .local v2, "_hidl_out_descriptor":Ljava/lang/String; */
/* nop */
/* .line 1673 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1671 */
/* .line 1673 */
} // .end local v2 # "_hidl_out_descriptor":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 1674 */
/* throw v2 */
} // .end method
void registerCallback ( Integer p0, java.lang.Object p1 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .param p2, "callback" # Ljava/lang/Object; */
/* .line 1651 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z */
int v1 = 0; // const/4 v1, 0x0
int v2 = 2; // const/4 v2, 0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1652 */
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v0, p2}, [Ljava/lang/Object; */
/* invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callHwBinderTransact(II[Ljava/lang/Object;)V */
/* .line 1654 */
} // :cond_0
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v0, p2}, [Ljava/lang/Object; */
/* invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callBinderTransact(II[Ljava/lang/Object;)V */
/* .line 1656 */
} // :goto_0
return;
} // .end method
void setFeature ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "displayId" # I */
/* .param p2, "mode" # I */
/* .param p3, "value" # I */
/* .param p4, "cookie" # I */
/* .line 1643 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1644 */
java.lang.Integer .valueOf ( p1 );
java.lang.Integer .valueOf ( p2 );
java.lang.Integer .valueOf ( p3 );
java.lang.Integer .valueOf ( p4 );
/* filled-new-array {v0, v2, v3, v4}, [Ljava/lang/Object; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callHwBinderTransact(II[Ljava/lang/Object;)V */
/* .line 1646 */
} // :cond_0
java.lang.Integer .valueOf ( p1 );
java.lang.Integer .valueOf ( p2 );
java.lang.Integer .valueOf ( p3 );
java.lang.Integer .valueOf ( p4 );
/* filled-new-array {v0, v2, v3, v4}, [Ljava/lang/Object; */
int v2 = 7; // const/4 v2, 0x7
/* invoke-direct {p0, v2, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->callBinderTransact(II[Ljava/lang/Object;)V */
/* .line 1648 */
} // :goto_0
return;
} // .end method
