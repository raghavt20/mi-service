public class com.android.server.display.ABTestHelper {
	 /* .source "ABTestHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/ABTestHelper$AbTestHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long DELAY_TIME;
private static final java.lang.String PATH;
private static final java.lang.String TAG;
private static final Integer UPDATE_EXPERIMENTS;
private static final Long UPDATE_EXPERIMENTS_DELAY;
/* # instance fields */
private com.xiaomi.abtest.ABTest mABTest;
private java.lang.String mAppName;
private android.content.Context mContext;
private java.util.function.Consumer mEndExperiment;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/lang/Void;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mExpCondition;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mExpId;
private android.os.Handler mHandler;
private java.lang.String mLayerName;
private java.util.function.Consumer mStartExperiment;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.telephony.TelephonyManager mTelephonyManager;
/* # direct methods */
public static void $r8$lambda$iPdUacJN3I7AdaYXuIJUW2un4Sk ( com.android.server.display.ABTestHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->lambda$new$0()V */
return;
} // .end method
static void -$$Nest$mupdateABTest ( com.android.server.display.ABTestHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->updateABTest()V */
return;
} // .end method
public com.android.server.display.ABTestHelper ( ) {
/* .locals 2 */
/* .param p1, "looper" # Landroid/os/Looper; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "appName" # Ljava/lang/String; */
/* .param p4, "layerName" # Ljava/lang/String; */
/* .param p8, "delay" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/Looper;", */
/* "Landroid/content/Context;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;", */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/lang/Void;", */
/* ">;J)V" */
/* } */
} // .end annotation
/* .line 49 */
/* .local p5, "expCondition":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* .local p6, "startExperiment":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;" */
/* .local p7, "endExperiment":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Ljava/lang/Void;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
/* .line 50 */
/* new-instance v0, Lcom/android/server/display/ABTestHelper$AbTestHandler; */
/* invoke-direct {v0, p0, p1}, Lcom/android/server/display/ABTestHelper$AbTestHandler;-><init>(Lcom/android/server/display/ABTestHelper;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 51 */
this.mContext = p2;
/* .line 52 */
this.mAppName = p3;
/* .line 53 */
this.mLayerName = p4;
/* .line 54 */
this.mExpCondition = p5;
/* .line 55 */
this.mStartExperiment = p6;
/* .line 56 */
this.mEndExperiment = p7;
/* .line 57 */
final String v0 = "phone"; // const-string v0, "phone"
(( android.content.Context ) p2 ).getSystemService ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telephony/TelephonyManager; */
this.mTelephonyManager = v0;
/* .line 58 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/ABTestHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/ABTestHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/ABTestHelper;)V */
(( android.os.Handler ) v0 ).postDelayed ( v1, p8, p9 ); // invoke-virtual {v0, v1, p8, p9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 61 */
return;
} // .end method
private java.lang.String getImei ( ) {
/* .locals 2 */
/* .line 110 */
int v0 = 0; // const/4 v0, 0x0
/* .line 111 */
/* .local v0, "imei":Ljava/lang/String; */
v1 = this.mTelephonyManager;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 112 */
(( android.telephony.TelephonyManager ) v1 ).getDeviceId ( ); // invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
/* .line 114 */
} // :cond_0
} // .end method
private void init ( ) {
/* .locals 7 */
/* .line 64 */
/* invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->getImei()Ljava/lang/String; */
/* .line 65 */
/* .local v0, "imei":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 66 */
final String v1 = "ABTestHelper"; // const-string v1, "ABTestHelper"
final String v2 = "failed to init, imei = null !!"; // const-string v2, "failed to init, imei = null !!"
android.util.Slog .e ( v1,v2 );
/* .line 67 */
return;
/* .line 69 */
} // :cond_0
/* new-instance v1, Lcom/xiaomi/abtest/ABTestConfig$Builder; */
/* invoke-direct {v1}, Lcom/xiaomi/abtest/ABTestConfig$Builder;-><init>()V */
/* .line 70 */
/* .local v1, "builder":Lcom/xiaomi/abtest/ABTestConfig$Builder; */
v2 = this.mAppName;
/* .line 71 */
(( com.xiaomi.abtest.ABTestConfig$Builder ) v1 ).setAppName ( v2 ); // invoke-virtual {v1, v2}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setAppName(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;
v3 = this.mLayerName;
/* .line 72 */
(( com.xiaomi.abtest.ABTestConfig$Builder ) v2 ).setLayerName ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setLayerName(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;
/* .line 73 */
(( com.xiaomi.abtest.ABTestConfig$Builder ) v2 ).setDeviceId ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setDeviceId(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;
/* .line 74 */
(( com.xiaomi.abtest.ABTestConfig$Builder ) v2 ).setUserId ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->setUserId(Ljava/lang/String;)Lcom/xiaomi/abtest/ABTestConfig$Builder;
/* .line 75 */
(( com.xiaomi.abtest.ABTestConfig$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Lcom/xiaomi/abtest/ABTestConfig$Builder;->build()Lcom/xiaomi/abtest/ABTestConfig;
/* .line 76 */
/* .local v2, "conf":Lcom/xiaomi/abtest/ABTestConfig; */
v3 = this.mContext;
com.xiaomi.abtest.ABTest .abTestWithConfig ( v3,v2 );
this.mABTest = v3;
/* .line 77 */
int v3 = 1; // const/4 v3, 0x1
com.xiaomi.abtest.ABTest .setIsLoadConfigWhenBackground ( v3 );
/* .line 78 */
v3 = this.mHandler;
int v4 = 0; // const/4 v4, 0x0
/* const-wide/16 v5, 0x1388 */
(( android.os.Handler ) v3 ).sendEmptyMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 79 */
return;
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 0 */
/* .line 59 */
/* invoke-direct {p0}, Lcom/android/server/display/ABTestHelper;->init()V */
/* .line 60 */
return;
} // .end method
private void updateABTest ( ) {
/* .locals 10 */
/* .line 82 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 83 */
v0 = this.mABTest;
v2 = this.mExpCondition;
(( com.xiaomi.abtest.ABTest ) v0 ).getExperiments ( v2 ); // invoke-virtual {v0, v2}, Lcom/xiaomi/abtest/ABTest;->getExperiments(Ljava/util/Map;)Ljava/util/Map;
/* .line 84 */
/* .local v0, "experiments":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/xiaomi/abtest/ExperimentInfo;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "experiments: "; // const-string v3, "experiments: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ABTestHelper"; // const-string v3, "ABTestHelper"
android.util.Slog .i ( v3,v2 );
/* .line 85 */
final String v2 = "/ExpLayer/ExpDomain/"; // const-string v2, "/ExpLayer/ExpDomain/"
final String v4 = "/"; // const-string v4, "/"
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mAppName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mLayerName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 86 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mAppName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mLayerName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* check-cast v5, Lcom/xiaomi/abtest/ExperimentInfo; */
/* .line 87 */
/* .local v5, "experimentInfo":Lcom/xiaomi/abtest/ExperimentInfo; */
(( com.xiaomi.abtest.ExperimentInfo ) v5 ).getParams ( ); // invoke-virtual {v5}, Lcom/xiaomi/abtest/ExperimentInfo;->getParams()Ljava/util/Map;
/* .line 88 */
/* .local v6, "expParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v7 = (( com.xiaomi.abtest.ExperimentInfo ) v5 ).getExpId ( ); // invoke-virtual {v5}, Lcom/xiaomi/abtest/ExperimentInfo;->getExpId()I
/* .line 89 */
/* .local v7, "expId":I */
/* iput v7, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
/* .line 90 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 91 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "start experiment mExpId: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mAppName: "; // const-string v9, ", mAppName: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mAppName;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ", mLayerName: "; // const-string v9, ", mLayerName: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mLayerName;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ", expParams: "; // const-string v9, ", expParams: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v8 );
/* .line 93 */
v8 = this.mStartExperiment;
/* .line 96 */
} // .end local v5 # "experimentInfo":Lcom/xiaomi/abtest/ExperimentInfo;
} // .end local v6 # "expParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v7 # "expId":I
} // :cond_0
/* iget v5, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
int v6 = -1; // const/4 v6, -0x1
/* if-eq v5, v6, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mAppName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mLayerName;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* if-nez v2, :cond_2 */
/* .line 98 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "end experiment mExpId: "; // const-string v4, "end experiment mExpId: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 99 */
/* iput v6, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
/* .line 100 */
v2 = this.mEndExperiment;
int v3 = 0; // const/4 v3, 0x0
/* .line 102 */
} // :cond_2
v2 = this.mHandler;
/* const-wide/32 v3, 0x927c0 */
(( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 103 */
return;
} // .end method
/* # virtual methods */
public Integer getExpId ( ) {
/* .locals 1 */
/* .line 106 */
/* iget v0, p0, Lcom/android/server/display/ABTestHelper;->mExpId:I */
} // .end method
