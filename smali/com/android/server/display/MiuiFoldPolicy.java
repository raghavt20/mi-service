public class com.android.server.display.MiuiFoldPolicy {
	 /* .source "MiuiFoldPolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;, */
	 /* Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;, */
	 /* Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOSE_LID_DISPLAY_SETTING;
private static final java.lang.Boolean DEBUG;
private static final Integer FOLD_GESTURE_ANGLE_THRESHOLD;
private static final java.lang.String MIUI_OPTIMIZATION;
private static final Integer MSG_RELEASE_WINDOW_BY_SCREEN_OFF;
private static final Integer MSG_SCREEN_TURNING_OFF;
private static final Integer MSG_SCREEN_TURNING_ON;
private static final Integer MSG_SHOW_OR_RELEASE_SWIPE_UP_WINDOW;
private static final Integer MSG_UPDATE_DEVICE_STATE;
private static final Integer MSG_USER_SWITCH;
private static final Integer SETTING_EVENT_INVALID;
private static final Integer SETTING_EVENT_KEEP_ON;
private static final Integer SETTING_EVENT_SCREEN_OFF;
private static final Integer SETTING_EVENT_SMART;
private static final Integer SETTING_EVENT_SWIPE_UP;
public static final java.lang.String TAG;
public static final Integer TYPE_HINGE_STATE;
private static final Integer VIRTUAL_CAMERA_BOUNDARY;
/* # instance fields */
private android.media.AudioManager mAudioManager;
private final android.hardware.camera2.CameraManager$AvailabilityCallback mAvailabilityCallback;
private android.hardware.camera2.CameraManager mCameraManager;
private android.content.Context mContext;
private Integer mFoldGestureAngleThreshold;
private android.os.Handler mHandler;
private Boolean mIsCtsMode;
private Boolean mIsDeviceProvisioned;
private android.hardware.Sensor mMiHingeAngleSensor;
private Boolean mMiHingeAngleSensorEnabled;
private final android.hardware.SensorEventListener mMiHingeAngleSensorListener;
private Boolean mNeedOffDueToFoldGesture;
private Boolean mNeedReleaseByScreenTurningOn;
private Boolean mNeedReleaseSwipeUpWindow;
private Boolean mNeedShowSwipeUpWindow;
private final java.util.Set mOpeningCameraID;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mPreState;
private Integer mScreenStateAfterFold;
private android.hardware.SensorManager mSensorManager;
private com.android.server.display.MiuiFoldPolicy$SettingsObserver mSettingsObserver;
private Integer mState;
private final mStrictFoldedDeviceStates;
private com.android.server.display.SwipeUpWindow mSwipeUpWindow;
private android.telecom.TelecomManager mTelecomManager;
private final mTentDeviceStates;
private com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
/* # direct methods */
static Integer -$$Nest$fgetmFoldGestureAngleThreshold ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mFoldGestureAngleThreshold:I */
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.util.Set -$$Nest$fgetmOpeningCameraID ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mOpeningCameraID;
} // .end method
static void -$$Nest$fputmNeedOffDueToFoldGesture ( com.android.server.display.MiuiFoldPolicy p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedOffDueToFoldGesture:Z */
return;
} // .end method
static void -$$Nest$mreleaseSwipeUpWindow ( com.android.server.display.MiuiFoldPolicy p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->releaseSwipeUpWindow(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mshowOrReleaseSwipeUpWindow ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->showOrReleaseSwipeUpWindow()V */
return;
} // .end method
static void -$$Nest$mupdateCtsMode ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateCtsMode()V */
return;
} // .end method
static void -$$Nest$mupdateDeviceProVisioned ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateDeviceProVisioned()V */
return;
} // .end method
static void -$$Nest$mupdateFoldGestureAngleThreshold ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateFoldGestureAngleThreshold()V */
return;
} // .end method
static void -$$Nest$mupdateScreenStateAfterFold ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateScreenStateAfterFold()V */
return;
} // .end method
static void -$$Nest$mupdateSettings ( com.android.server.display.MiuiFoldPolicy p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateSettings()V */
return;
} // .end method
static com.android.server.display.MiuiFoldPolicy ( ) {
/* .locals 1 */
/* .line 42 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Boolean .valueOf ( v0 );
return;
} // .end method
public com.android.server.display.MiuiFoldPolicy ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 92 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 66 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I */
/* .line 67 */
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
/* .line 68 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mOpeningCameraID = v0;
/* .line 85 */
/* const/16 v0, 0x52 */
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mFoldGestureAngleThreshold:I */
/* .line 338 */
/* new-instance v0, Lcom/android/server/display/MiuiFoldPolicy$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/MiuiFoldPolicy$1;-><init>(Lcom/android/server/display/MiuiFoldPolicy;)V */
this.mAvailabilityCallback = v0;
/* .line 482 */
/* new-instance v0, Lcom/android/server/display/MiuiFoldPolicy$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/MiuiFoldPolicy$2;-><init>(Lcom/android/server/display/MiuiFoldPolicy;)V */
this.mMiHingeAngleSensorListener = v0;
/* .line 93 */
this.mContext = p1;
/* .line 94 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x10700bb */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mStrictFoldedDeviceStates = v0;
/* .line 96 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030056 */
(( android.content.res.Resources ) v0 ).getIntArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I
this.mTentDeviceStates = v0;
/* .line 98 */
return;
} // .end method
private Boolean isCtsScene ( ) {
/* .locals 3 */
/* .line 447 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsCtsMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v0, v2, :cond_1 */
} // :cond_0
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move v0, v1 */
/* .line 449 */
/* .local v0, "isCtsScene":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 450 */
final String v1 = "MiuiFoldPolicy"; // const-string v1, "MiuiFoldPolicy"
final String v2 = "running cts, skip fold policy."; // const-string v2, "running cts, skip fold policy."
android.util.Slog .i ( v1,v2 );
/* .line 452 */
} // :cond_2
} // .end method
private Boolean isFolded ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 206 */
v0 = this.mStrictFoldedDeviceStates;
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p1 );
} // .end method
private Boolean isFoldedOrTent ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 214 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->isFolded(I)Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->isTent(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean isHoldScreenOn ( ) {
/* .locals 4 */
/* .line 313 */
final String v0 = ""; // const-string v0, ""
/* .line 314 */
/* .local v0, "reason":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 317 */
/* .local v1, "isHoldScreenOn":Z */
v2 = this.mTelecomManager;
v2 = (( android.telecom.TelecomManager ) v2 ).isInCall ( ); // invoke-virtual {v2}, Landroid/telecom/TelecomManager;->isInCall()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 318 */
int v1 = 1; // const/4 v1, 0x1
/* .line 319 */
final String v0 = "in call"; // const-string v0, "in call"
/* .line 321 */
} // :cond_0
v2 = v2 = this.mOpeningCameraID;
/* if-nez v2, :cond_1 */
/* .line 322 */
int v1 = 1; // const/4 v1, 0x1
/* .line 323 */
final String v0 = "camera using"; // const-string v0, "camera using"
/* .line 325 */
} // :cond_1
v2 = this.mAudioManager;
v2 = (( android.media.AudioManager ) v2 ).getMode ( ); // invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I
int v3 = 2; // const/4 v3, 0x2
/* if-eq v2, v3, :cond_2 */
v2 = this.mAudioManager;
/* .line 326 */
v2 = (( android.media.AudioManager ) v2 ).getMode ( ); // invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I
int v3 = 3; // const/4 v3, 0x3
/* if-ne v2, v3, :cond_3 */
/* .line 327 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
/* .line 328 */
final String v0 = "audio using"; // const-string v0, "audio using"
/* .line 331 */
} // :cond_3
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 332 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "hold screen on reason : "; // const-string v3, "hold screen on reason : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFoldPolicy"; // const-string v3, "MiuiFoldPolicy"
android.util.Slog .i ( v3,v2 );
/* .line 334 */
} // :cond_4
} // .end method
private Boolean isKeepScreenOnAfterFolded ( ) {
/* .locals 2 */
/* .line 187 */
int v0 = 0; // const/4 v0, 0x0
/* .line 188 */
/* .local v0, "isKeepScreenOn":Z */
/* iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 190 */
/* :pswitch_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 191 */
/* .line 196 */
/* :pswitch_1 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isHoldScreenOn()Z */
/* if-nez v1, :cond_1 */
v1 = this.mWindowManagerPolicy;
v1 = /* .line 197 */
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* move v0, v1 */
/* .line 198 */
/* .line 193 */
/* :pswitch_2 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isHoldScreenOn()Z */
/* .line 194 */
/* nop */
/* .line 202 */
} // :goto_2
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean isTent ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 210 */
v0 = this.mTentDeviceStates;
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p1 );
} // .end method
private void registerContentObserver ( ) {
/* .locals 5 */
/* .line 396 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 397 */
final String v1 = "close_lid_display_setting"; // const-string v1, "close_lid_display_setting"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 396 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 400 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 401 */
final String v1 = "fold_gesture_angle_threshold"; // const-string v1, "fold_gesture_angle_threshold"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 400 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 404 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 405 */
final String v1 = "miui_optimization"; // const-string v1, "miui_optimization"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 404 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 408 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 409 */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
android.provider.Settings$Global .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 408 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 412 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateSettings()V */
/* .line 413 */
return;
} // .end method
private void registerMiHingeAngleSensorListener ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 468 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 469 */
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z */
/* if-nez v1, :cond_1 */
/* .line 470 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z */
/* .line 471 */
v1 = this.mSensorManager;
v2 = this.mMiHingeAngleSensorListener;
v3 = this.mMiHingeAngleSensor;
(( android.hardware.SensorManager ) v1 ).registerListener ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 475 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 476 */
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mMiHingeAngleSensorEnabled:Z */
/* .line 477 */
v0 = this.mSensorManager;
v1 = this.mMiHingeAngleSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 480 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void releaseSwipeUpWindow ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 239 */
v0 = this.mSwipeUpWindow;
(( com.android.server.display.SwipeUpWindow ) v0 ).releaseSwipeWindow ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/SwipeUpWindow;->releaseSwipeWindow(Ljava/lang/String;)V
/* .line 240 */
return;
} // .end method
private void screenTurnOff ( ) {
/* .locals 5 */
/* .line 307 */
v0 = this.mContext;
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
/* .line 308 */
/* .local v0, "powerManager":Landroid/os/PowerManager; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
(( android.os.PowerManager ) v0 ).goToSleep ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->goToSleep(JII)V
/* .line 310 */
return;
} // .end method
private void showOrReleaseSwipeUpWindow ( ) {
/* .locals 2 */
/* .line 218 */
v0 = com.android.server.display.MiuiFoldPolicy.DEBUG;
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 219 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "showOrReleaseSwipeUpWindow: fold?" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/display/MiuiFoldPolicy;->isFoldedOrTent(I)Z */
/* xor-int/lit8 v1, v1, 0x1 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", mNeedShowSwipeUpWindow:"; // const-string v1, ", mNeedShowSwipeUpWindow:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFoldPolicy"; // const-string v1, "MiuiFoldPolicy"
android.util.Slog .i ( v1,v0 );
/* .line 222 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z */
/* .line 224 */
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z */
/* if-nez v1, :cond_3 */
/* iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/display/MiuiFoldPolicy;->isFoldedOrTent(I)Z */
/* if-nez v1, :cond_1 */
/* .line 231 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 232 */
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z */
/* .line 233 */
v0 = this.mSwipeUpWindow;
(( com.android.server.display.SwipeUpWindow ) v0 ).showSwipeUpWindow ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow;->showSwipeUpWindow()V
/* .line 235 */
} // :cond_2
return;
/* .line 225 */
} // :cond_3
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z */
/* .line 226 */
final String v0 = "device state"; // const-string v0, "device state"
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->releaseSwipeUpWindow(Ljava/lang/String;)V */
/* .line 227 */
return;
} // .end method
private void updateCtsMode ( ) {
/* .locals 2 */
/* .line 442 */
/* nop */
/* .line 443 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 442 */
/* xor-int/lit8 v0, v0, 0x1 */
final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* xor-int/lit8 v0, v0, 0x1 */
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsCtsMode:Z */
/* .line 444 */
return;
} // .end method
private void updateDeviceProVisioned ( ) {
/* .locals 3 */
/* .line 456 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsDeviceProvisioned:Z */
/* .line 458 */
return;
} // .end method
private void updateFoldGestureAngleThreshold ( ) {
/* .locals 4 */
/* .line 416 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const/16 v1, 0x52 */
int v2 = -2; // const/4 v2, -0x2
final String v3 = "fold_gesture_angle_threshold"; // const-string v3, "fold_gesture_angle_threshold"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mFoldGestureAngleThreshold:I */
/* .line 419 */
return;
} // .end method
private void updateScreenStateAfterFold ( ) {
/* .locals 5 */
/* .line 422 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "close_lid_display_setting"; // const-string v1, "close_lid_display_setting"
int v2 = -1; // const/4 v2, -0x1
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
/* .line 425 */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v0, v2, :cond_1 */
/* .line 427 */
final String v0 = "cetus"; // const-string v0, "cetus"
v2 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 428 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
/* .line 430 */
} // :cond_0
/* iput v4, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
/* .line 433 */
} // :goto_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* iget v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
android.provider.Settings$System .putIntForUser ( v0,v1,v2,v3 );
/* .line 438 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_2 */
} // :cond_2
int v4 = 0; // const/4 v4, 0x0
} // :goto_1
/* invoke-direct {p0, v4}, Lcom/android/server/display/MiuiFoldPolicy;->registerMiHingeAngleSensorListener(Z)V */
/* .line 439 */
return;
} // .end method
private void updateSettings ( ) {
/* .locals 0 */
/* .line 389 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateScreenStateAfterFold()V */
/* .line 390 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateFoldGestureAngleThreshold()V */
/* .line 391 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateCtsMode()V */
/* .line 392 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->updateDeviceProVisioned()V */
/* .line 393 */
return;
} // .end method
/* # virtual methods */
public void dealDisplayTransition ( ) {
/* .locals 2 */
/* .line 130 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 131 */
return;
/* .line 134 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 135 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 136 */
return;
} // .end method
public void handleDeviceStateChanged ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "state" # I */
/* .line 154 */
/* iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
/* if-ne p1, v0, :cond_0 */
/* .line 155 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "the new state is equal the old(" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ") skip"; // const-string v1, ") skip"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFoldPolicy"; // const-string v1, "MiuiFoldPolicy"
android.util.Slog .i ( v1,v0 );
/* .line 156 */
return;
/* .line 159 */
} // :cond_0
/* iput v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I */
/* .line 160 */
/* iput p1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
/* .line 162 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/MiuiFoldPolicy;->isFoldedOrTent(I)Z */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isKeepScreenOnAfterFolded()Z */
/* if-nez v0, :cond_5 */
/* .line 163 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->isCtsScene()Z */
/* if-nez v0, :cond_5 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mIsDeviceProvisioned:Z */
/* if-nez v0, :cond_1 */
/* .line 171 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mState:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->isFolded(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->isTent(I)Z */
/* if-nez v0, :cond_4 */
/* .line 172 */
/* iget v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mScreenStateAfterFold:I */
/* if-nez v0, :cond_2 */
/* iget v3, p0, Lcom/android/server/display/MiuiFoldPolicy;->mPreState:I */
int v4 = -1; // const/4 v4, -0x1
/* if-eq v3, v4, :cond_2 */
/* .line 174 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurnOff()V */
/* .line 175 */
} // :cond_2
int v3 = 3; // const/4 v3, 0x3
/* if-ne v0, v3, :cond_3 */
/* .line 176 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedOffDueToFoldGesture:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 177 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->screenTurnOff()V */
/* .line 179 */
} // :cond_3
/* if-ne v0, v2, :cond_4 */
/* .line 180 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z */
/* .line 181 */
/* iput-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z */
/* .line 184 */
} // :cond_4
} // :goto_0
return;
/* .line 164 */
} // :cond_5
} // :goto_1
v0 = this.mSwipeUpWindow;
(( com.android.server.display.SwipeUpWindow ) v0 ).cancelScreenOffDelay ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow;->cancelScreenOffDelay()V
/* .line 165 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseSwipeUpWindow:Z */
/* .line 166 */
/* iput-boolean v1, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedShowSwipeUpWindow:Z */
/* .line 167 */
return;
} // .end method
public void handleScreenTurningOff ( ) {
/* .locals 1 */
/* .line 303 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z */
/* .line 304 */
return;
} // .end method
public void handleScreenTurningOn ( ) {
/* .locals 1 */
/* .line 296 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 297 */
final String v0 = "screen on"; // const-string v0, "screen on"
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiFoldPolicy;->releaseSwipeUpWindow(Ljava/lang/String;)V */
/* .line 298 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiFoldPolicy;->mNeedReleaseByScreenTurningOn:Z */
/* .line 300 */
} // :cond_0
return;
} // .end method
public void initMiuiFoldPolicy ( ) {
/* .locals 5 */
/* .line 101 */
/* new-instance v0, Lcom/android/server/ServiceThread; */
int v1 = -4; // const/4 v1, -0x4
int v2 = 0; // const/4 v2, 0x0
final String v3 = "MiuiFoldPolicy"; // const-string v3, "MiuiFoldPolicy"
/* invoke-direct {v0, v3, v1, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
/* .line 102 */
/* .local v0, "handlerThread":Lcom/android/server/ServiceThread; */
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 103 */
/* new-instance v1, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler; */
(( com.android.server.ServiceThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;-><init>(Lcom/android/server/display/MiuiFoldPolicy;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 104 */
v1 = this.mContext;
/* const-string/jumbo v2, "telecom" */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/telecom/TelecomManager; */
this.mTelecomManager = v1;
/* .line 105 */
v1 = this.mContext;
final String v2 = "audio"; // const-string v2, "audio"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/media/AudioManager; */
this.mAudioManager = v1;
/* .line 106 */
v1 = this.mContext;
final String v2 = "camera"; // const-string v2, "camera"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/camera2/CameraManager; */
this.mCameraManager = v1;
/* .line 107 */
v1 = this.mContext;
/* const-string/jumbo v2, "sensor" */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/SensorManager; */
this.mSensorManager = v1;
/* .line 108 */
/* const-class v1, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v1;
/* .line 110 */
v1 = this.mCameraManager;
v2 = this.mAvailabilityCallback;
v3 = this.mHandler;
(( android.hardware.camera2.CameraManager ) v1 ).registerAvailabilityCallback ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/hardware/camera2/CameraManager;->registerAvailabilityCallback(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V
/* .line 112 */
v1 = this.mSensorManager;
/* const v2, 0x1fa268f */
(( android.hardware.SensorManager ) v1 ).getDefaultSensor ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mMiHingeAngleSensor = v1;
/* .line 114 */
/* new-instance v1, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver; */
v2 = this.mHandler;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;-><init>(Lcom/android/server/display/MiuiFoldPolicy;Landroid/os/Handler;)V */
this.mSettingsObserver = v1;
/* .line 115 */
/* new-instance v1, Lcom/android/server/display/SwipeUpWindow; */
v2 = this.mContext;
(( com.android.server.ServiceThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2, v3}, Lcom/android/server/display/SwipeUpWindow;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mSwipeUpWindow = v1;
/* .line 117 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiFoldPolicy;->registerContentObserver()V */
/* .line 119 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver;-><init>(Lcom/android/server/display/MiuiFoldPolicy;Lcom/android/server/display/MiuiFoldPolicy$UserSwitchReceiver-IA;)V */
/* new-instance v3, Landroid/content/IntentFilter; */
final String v4 = "android.intent.action.USER_SWITCHED"; // const-string v4, "android.intent.action.USER_SWITCHED"
/* invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 121 */
return;
} // .end method
public void notifyFinishedGoingToSleep ( ) {
/* .locals 2 */
/* .line 289 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 290 */
return;
/* .line 292 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 293 */
return;
} // .end method
public void screenTurningOff ( ) {
/* .locals 2 */
/* .line 282 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 283 */
return;
/* .line 285 */
} // :cond_0
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 286 */
return;
} // .end method
public void screenTurningOn ( ) {
/* .locals 2 */
/* .line 275 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 276 */
return;
/* .line 278 */
} // :cond_0
int v1 = 5; // const/4 v1, 0x5
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 279 */
return;
} // .end method
public void setDeviceStateLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "state" # I */
/* .line 139 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
/* .line 140 */
return;
/* .line 143 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 144 */
/* .local v0, "msg":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 145 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 147 */
v1 = com.android.server.display.MiuiFoldPolicy.DEBUG;
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 148 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setDeviceStateLocked: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFoldPolicy"; // const-string v2, "MiuiFoldPolicy"
android.util.Slog .i ( v2,v1 );
/* .line 150 */
} // :cond_1
return;
} // .end method
