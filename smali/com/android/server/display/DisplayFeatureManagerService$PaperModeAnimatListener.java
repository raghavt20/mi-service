class com.android.server.display.DisplayFeatureManagerService$PaperModeAnimatListener implements com.android.server.display.MiuiRampAnimator$Listener {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "PaperModeAnimatListener" */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayFeatureManagerService$PaperModeAnimatListener ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .line 1542 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAnimationEnd ( ) {
/* .locals 2 */
/* .line 1545 */
v0 = this.this$0;
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmDisplayState ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
v0 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$sfgetIS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT ( );
/* if-nez v0, :cond_0 */
/* .line 1546 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleScreenSchemeChange ( v0,v1 );
/* .line 1548 */
} // :cond_0
return;
} // .end method
