class com.android.server.display.RhythmicEyeCareManager$2 extends android.content.BroadcastReceiver {
	 /* .source "RhythmicEyeCareManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/RhythmicEyeCareManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.RhythmicEyeCareManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$AuETwViZxc8mEov7hm27f8iZjiQ ( com.android.server.display.RhythmicEyeCareManager$2 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager$2;->lambda$onReceive$0()V */
return;
} // .end method
 com.android.server.display.RhythmicEyeCareManager$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/RhythmicEyeCareManager; */
/* .line 70 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
private void lambda$onReceive$0 ( ) { //synthethic
/* .locals 2 */
/* .line 81 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.RhythmicEyeCareManager .-$$Nest$mupdateState ( v0,v1 );
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 73 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 74 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.android.server.display.RhythmicEyeCareManager .-$$Nest$fgetmIsEnabled ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 75 */
	 final String v1 = "android.intent.action.TIMEZONE_CHANGED"; // const-string v1, "android.intent.action.TIMEZONE_CHANGED"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v1, :cond_0 */
	 /* .line 76 */
	 final String v1 = "android.intent.action.TIME_SET"; // const-string v1, "android.intent.action.TIME_SET"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v1, :cond_0 */
	 /* .line 77 */
	 final String v1 = "android.intent.action.DATE_CHANGED"; // const-string v1, "android.intent.action.DATE_CHANGED"
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 78 */
	 } // :cond_0
	 v1 = this.this$0;
	 com.android.server.display.RhythmicEyeCareManager .-$$Nest$fgetmOnAlarmListener ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 79 */
		 v1 = this.this$0;
		 com.android.server.display.RhythmicEyeCareManager .-$$Nest$fgetmAlarmManager ( v1 );
		 v2 = this.this$0;
		 com.android.server.display.RhythmicEyeCareManager .-$$Nest$fgetmOnAlarmListener ( v2 );
		 (( android.app.AlarmManager ) v1 ).cancel ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/AlarmManager$OnAlarmListener;)V
		 /* .line 81 */
	 } // :cond_1
	 v1 = this.this$0;
	 com.android.server.display.RhythmicEyeCareManager .-$$Nest$fgetmHandler ( v1 );
	 /* new-instance v2, Lcom/android/server/display/RhythmicEyeCareManager$2$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/display/RhythmicEyeCareManager$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager$2;)V */
	 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 82 */
	 v1 = this.this$0;
	 com.android.server.display.RhythmicEyeCareManager .-$$Nest$mscheduleRhythmicAlarm ( v1 );
	 /* .line 85 */
} // :cond_2
return;
} // .end method
