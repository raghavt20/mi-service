.class public Lcom/android/server/display/TouchCoverProtectionHelper;
.super Ljava/lang/Object;
.source "TouchCoverProtectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/TouchCoverProtectionHelper$Injector;,
        Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field private static final GAME_TOUCH_EVENT_INTERVAL:I = 0x2bf20

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBorderRect:Landroid/graphics/Rect;

.field private mContext:Landroid/content/Context;

.field private mCurrentBorderRect:Landroid/graphics/Rect;

.field private mDeviceStateManager:Landroid/hardware/devicestate/DeviceStateManager;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayListenerEnabled:Z

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mDisplayManagerServiceImpl:Lcom/android/server/display/DisplayManagerServiceImpl;

.field private mFoldStateListener:Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

.field private mHandler:Landroid/os/Handler;

.field private mInjector:Lcom/android/server/display/TouchCoverProtectionHelper$Injector;

.field private mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

.field private mLogicalHeight:I

.field private mLogicalWidth:I

.field private mRotation:I

.field private mSecondDisplayBorderRect:Landroid/graphics/Rect;

.field private mSyncRoot:Ljava/lang/Object;

.field private mTouchAreaEnabled:Z

.field private final mTouchCoverProtectionRect:Landroid/graphics/Rect;

.field private mTouchEventDebounce:I

.field private mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

.field private mTouchTrackingEnabled:Z

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method public static synthetic $r8$lambda$d80CvqLEszP8mTzTNFJKHuIMvKw(Lcom/android/server/display/TouchCoverProtectionHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->lambda$showTouchCoverProtectionRect$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$gyDFvXjtaGxAXT4-76TKF2NnMWc(Lcom/android/server/display/TouchCoverProtectionHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->lambda$showTouchCoverProtectionRect$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$t9WMKeqeoE0goZnzEVv7-txU8KE(Lcom/android/server/display/TouchCoverProtectionHelper;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->lambda$new$0(Ljava/lang/Boolean;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/display/TouchCoverProtectionHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLogicalDisplay(Lcom/android/server/display/TouchCoverProtectionHelper;)Lcom/android/server/display/LogicalDisplay;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTouchCoverProtectionRect(Lcom/android/server/display/TouchCoverProtectionHelper;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchCoverProtectionRect:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateTouchCoverProtectionRect(Lcom/android/server/display/TouchCoverProtectionHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->updateTouchCoverProtectionRect()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 39
    const-class v0, Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchCoverProtectionRect:Landroid/graphics/Rect;

    .line 282
    new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$1;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 76
    iput-object p1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mContext:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mHandler:Landroid/os/Handler;

    .line 78
    new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper$Injector;

    invoke-direct {v0, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$Injector;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mInjector:Lcom/android/server/display/TouchCoverProtectionHelper$Injector;

    .line 79
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 80
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 81
    invoke-static {}, Lcom/android/server/display/DisplayManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayManagerServiceImpl;

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayManagerServiceImpl:Lcom/android/server/display/DisplayManagerServiceImpl;

    .line 82
    invoke-virtual {v0}, Lcom/android/server/display/DisplayManagerServiceImpl;->getSyncRoot()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mSyncRoot:Ljava/lang/Object;

    .line 83
    new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker-IA;)V

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 85
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x11050083

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z

    .line 88
    const v1, 0x11030058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setBorderRect([I)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mBorderRect:Landroid/graphics/Rect;

    .line 90
    const v1, 0x11030050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setBorderRect([I)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mSecondDisplayBorderRect:Landroid/graphics/Rect;

    .line 92
    const v1, 0x110b0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchEventDebounce:I

    .line 94
    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mBorderRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    .line 96
    new-instance v1, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    new-instance v2, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V

    invoke-direct {v1, p1, v2}, Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;-><init>(Landroid/content/Context;Ljava/util/function/Consumer;)V

    iput-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mFoldStateListener:Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    .line 97
    const-class v1, Landroid/hardware/devicestate/DeviceStateManager;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/devicestate/DeviceStateManager;

    iput-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDeviceStateManager:Landroid/hardware/devicestate/DeviceStateManager;

    .line 98
    new-instance v2, Landroid/os/HandlerExecutor;

    iget-object v3, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, v3}, Landroid/os/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    iget-object v3, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mFoldStateListener:Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    invoke-virtual {v1, v2, v3}, Landroid/hardware/devicestate/DeviceStateManager;->registerCallback(Ljava/util/concurrent/Executor;Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    .line 99
    return-void
.end method

.method private synthetic lambda$new$0(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "folded"    # Ljava/lang/Boolean;

    .line 96
    invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->updateBorderRect(Ljava/lang/Boolean;)V

    return-void
.end method

.method private synthetic lambda$showTouchCoverProtectionRect$1()V
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mInjector:Lcom/android/server/display/TouchCoverProtectionHelper$Injector;

    invoke-static {v0}, Lcom/android/server/display/TouchCoverProtectionHelper$Injector;->-$$Nest$mshow(Lcom/android/server/display/TouchCoverProtectionHelper$Injector;)V

    return-void
.end method

.method private synthetic lambda$showTouchCoverProtectionRect$2()V
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mInjector:Lcom/android/server/display/TouchCoverProtectionHelper$Injector;

    invoke-static {v0}, Lcom/android/server/display/TouchCoverProtectionHelper$Injector;->-$$Nest$mhide(Lcom/android/server/display/TouchCoverProtectionHelper$Injector;)V

    return-void
.end method

.method private setBorderRect([I)Landroid/graphics/Rect;
    .locals 5
    .param p1, "configArray"    # [I

    .line 262
    array-length v0, p1

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    .line 263
    sget-object v0, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    const-string v1, "The touch cover array configuration must be four."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0

    .line 266
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    aget v1, p1, v2

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    const/4 v4, 0x3

    aget v4, p1, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private setDisplayListenerEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 250
    if-eqz p1, :cond_0

    .line 251
    iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z

    if-nez v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    iget-object v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z

    goto :goto_0

    .line 255
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDisplayListenerEnabled:Z

    .line 259
    :cond_1
    :goto_0
    return-void
.end method

.method private setTouchTrackingEnabled(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 270
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 271
    iget-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z

    if-nez v1, :cond_1

    .line 272
    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z

    goto :goto_0

    .line 275
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z

    if-eqz v1, :cond_1

    .line 277
    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    invoke-virtual {v1, v2, v0}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 278
    iput-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchTrackingEnabled:Z

    .line 280
    :cond_1
    :goto_0
    return-void
.end method

.method private updateBorderRect(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "folded"    # Ljava/lang/Boolean;

    .line 114
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mSecondDisplayBorderRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    goto :goto_0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mBorderRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    .line 119
    :goto_0
    return-void
.end method

.method private updateTouchCoverProtectionRect()V
    .locals 15

    .line 137
    iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z

    if-nez v0, :cond_0

    .line 139
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    if-nez v0, :cond_1

    .line 143
    return-void

    .line 146
    :cond_1
    const/4 v0, 0x0

    .line 147
    .local v0, "physicalWidth":F
    const/4 v1, 0x0

    .line 149
    .local v1, "physicalHeight":F
    const/4 v2, 0x0

    .line 150
    .local v2, "logicalWidth":I
    const/4 v3, 0x0

    .line 151
    .local v3, "logicalHeight":I
    const/4 v4, 0x0

    .line 154
    .local v4, "rotation":I
    iget-object v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mSyncRoot:Ljava/lang/Object;

    monitor-enter v5

    .line 155
    :try_start_0
    iget-object v6, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    invoke-virtual {v6}, Lcom/android/server/display/LogicalDisplay;->getPrimaryDisplayDeviceLocked()Lcom/android/server/display/DisplayDevice;

    move-result-object v6

    .line 156
    .local v6, "displayDevice":Lcom/android/server/display/DisplayDevice;
    if-nez v6, :cond_2

    .line 157
    sget-object v7, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    const-string v8, "current display device is not exist"

    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    monitor-exit v5

    return-void

    .line 161
    :cond_2
    invoke-virtual {v6}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;

    move-result-object v7

    .line 162
    .local v7, "displayDeviceInfo":Lcom/android/server/display/DisplayDeviceInfo;
    iget v8, v7, Lcom/android/server/display/DisplayDeviceInfo;->width:I

    int-to-float v0, v8

    .line 163
    iget v8, v7, Lcom/android/server/display/DisplayDeviceInfo;->height:I

    int-to-float v1, v8

    .line 166
    iget-object v8, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    invoke-virtual {v8}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;

    move-result-object v8

    .line 167
    .local v8, "displayInfo":Landroid/view/DisplayInfo;
    iget v9, v8, Landroid/view/DisplayInfo;->logicalWidth:I

    move v2, v9

    .line 168
    iget v9, v8, Landroid/view/DisplayInfo;->logicalHeight:I

    move v3, v9

    .line 169
    iget v9, v8, Landroid/view/DisplayInfo;->rotation:I

    move v4, v9

    .line 170
    .end local v6    # "displayDevice":Lcom/android/server/display/DisplayDevice;
    .end local v7    # "displayDeviceInfo":Lcom/android/server/display/DisplayDeviceInfo;
    .end local v8    # "displayInfo":Landroid/view/DisplayInfo;
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalWidth:I

    if-ne v5, v2, :cond_3

    iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalHeight:I

    if-ne v5, v3, :cond_3

    iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mRotation:I

    if-ne v4, v5, :cond_3

    .line 174
    return-void

    .line 177
    :cond_3
    iput v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalWidth:I

    .line 178
    iput v3, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalHeight:I

    .line 179
    iput v4, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mRotation:I

    .line 182
    const/4 v5, 0x0

    .line 183
    .local v5, "left":I
    const/4 v6, 0x0

    .line 184
    .local v6, "right":I
    const/4 v7, 0x0

    .line 185
    .local v7, "top":I
    const/4 v8, 0x0

    .line 186
    .local v8, "bottom":I
    const/4 v9, 0x3

    const/4 v10, 0x1

    if-ne v4, v10, :cond_4

    .line 187
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v5, v11, Landroid/graphics/Rect;->top:I

    .line 188
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v6, v11, Landroid/graphics/Rect;->bottom:I

    .line 189
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v11, v11

    sub-float v11, v0, v11

    float-to-int v7, v11

    .line 190
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    sub-float v11, v0, v11

    float-to-int v8, v11

    goto :goto_0

    .line 191
    :cond_4
    if-ne v4, v9, :cond_5

    .line 192
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    int-to-float v11, v11

    sub-float v11, v1, v11

    float-to-int v5, v11

    .line 193
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    sub-float v11, v1, v11

    float-to-int v6, v11

    .line 194
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v7, v11, Landroid/graphics/Rect;->left:I

    .line 195
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v8, v11, Landroid/graphics/Rect;->right:I

    .line 197
    :cond_5
    :goto_0
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchCoverProtectionRect:Landroid/graphics/Rect;

    invoke-virtual {v11, v5, v7, v6, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 200
    if-eq v4, v10, :cond_7

    if-ne v4, v9, :cond_6

    goto :goto_1

    :cond_6
    const/4 v10, 0x0

    :cond_7
    :goto_1
    move v9, v10

    .line 201
    .local v9, "rotated":Z
    if-eqz v9, :cond_8

    move v10, v3

    goto :goto_2

    :cond_8
    move v10, v2

    :goto_2
    int-to-float v10, v10

    div-float/2addr v10, v0

    .line 202
    .local v10, "scale":F
    float-to-double v11, v10

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v11, v13

    if-eqz v11, :cond_9

    .line 203
    iget-object v11, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchCoverProtectionRect:Landroid/graphics/Rect;

    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->scale(F)V

    .line 206
    :cond_9
    sget-object v11, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "updateTouchCoverProtectionRect, mLogicalWidth: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalWidth:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", mLogicalHeight: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalHeight:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", physicalWidth: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", physicalHeight: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", scale: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    return-void

    .line 170
    .end local v5    # "left":I
    .end local v6    # "right":I
    .end local v7    # "top":I
    .end local v8    # "bottom":I
    .end local v9    # "rotated":Z
    .end local v10    # "scale":F
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method


# virtual methods
.method protected configure(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 102
    iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z

    if-eqz v0, :cond_0

    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setTouchTrackingEnabled(Z)V

    .line 104
    invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setDisplayListenerEnabled(Z)V

    .line 106
    :cond_0
    return-void
.end method

.method protected dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 317
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z

    sput-boolean v0, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z

    .line 318
    const-string v0, "Touch cover protection state:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTouchAreaEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  isTouchCoverProtectionActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    invoke-virtual {v1}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->isTouchingInArea()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBorderLeft="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBorderTop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBorderRight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mBorderBottom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mCurrentBorderRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTouchCoverProtectionRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchCoverProtectionRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTouchEventDebounce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchEventDebounce:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method protected isGameSceneWithinTouchTime()Z
    .locals 6

    .line 236
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 237
    .local v0, "now":J
    iget-object v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    invoke-static {v2}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->-$$Nest$fgetmLastObservedTouchTime(Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/32 v4, 0x2bf20

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 238
    sget-boolean v2, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 239
    sget-object v2, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    const-string v3, "Time of light sensor event is within given time of touch event in game scene."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 243
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method protected isTouchCoverProtectionActive()Z
    .locals 8

    .line 218
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    invoke-virtual {v0}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->isTouchingInArea()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 219
    return v1

    .line 221
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 222
    .local v2, "now":J
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchPositionTracker:Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;

    invoke-static {v0}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->-$$Nest$fgetmLastObservedTouchTime(Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;)J

    move-result-wide v4

    sub-long v4, v2, v4

    iget v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchEventDebounce:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    .line 223
    sget-boolean v0, Lcom/android/server/display/TouchCoverProtectionHelper;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 224
    sget-object v0, Lcom/android/server/display/TouchCoverProtectionHelper;->TAG:Ljava/lang/String;

    const-string v4, "Time of light sensor event is within given time of touch event."

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_1
    return v1

    .line 228
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public setUpLogicalDisplay(Lcom/android/server/display/LogicalDisplay;)V
    .locals 0
    .param p1, "logicalDisplay"    # Lcom/android/server/display/LogicalDisplay;

    .line 109
    iput-object p1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mLogicalDisplay:Lcom/android/server/display/LogicalDisplay;

    .line 110
    invoke-direct {p0}, Lcom/android/server/display/TouchCoverProtectionHelper;->updateTouchCoverProtectionRect()V

    .line 111
    return-void
.end method

.method protected showTouchCoverProtectionRect(Z)V
    .locals 2
    .param p1, "isShow"    # Z

    .line 309
    if-eqz p1, :cond_0

    .line 310
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/display/TouchCoverProtectionHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 314
    :goto_0
    return-void
.end method

.method protected stop()V
    .locals 2

    .line 125
    iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mTouchAreaEnabled:Z

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->setTouchTrackingEnabled(Z)V

    .line 127
    invoke-direct {p0, v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->setDisplayListenerEnabled(Z)V

    .line 128
    invoke-virtual {p0, v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->showTouchCoverProtectionRect(Z)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mDeviceStateManager:Landroid/hardware/devicestate/DeviceStateManager;

    iget-object v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper;->mFoldStateListener:Landroid/hardware/devicestate/DeviceStateManager$FoldStateListener;

    invoke-virtual {v0, v1}, Landroid/hardware/devicestate/DeviceStateManager;->unregisterCallback(Landroid/hardware/devicestate/DeviceStateManager$DeviceStateCallback;)V

    .line 131
    return-void
.end method
