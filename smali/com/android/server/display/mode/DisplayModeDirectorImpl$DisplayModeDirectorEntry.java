class com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry {
	 /* .source "DisplayModeDirectorImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/mode/DisplayModeDirectorImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DisplayModeDirectorEntry" */
} // .end annotation
/* # instance fields */
private com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs desiredDisplayModeSpecs;
private Integer displayId;
final com.android.server.display.mode.DisplayModeDirectorImpl this$0; //synthetic
private Long timesTamp;
private android.util.SparseArray votes;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/display/mode/Vote;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$fputdesiredDisplayModeSpecs ( com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1 ) { //bridge//synthethic
/* .locals 0 */
this.desiredDisplayModeSpecs = p1;
return;
} // .end method
static void -$$Nest$fputdisplayId ( com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->displayId:I */
return;
} // .end method
static void -$$Nest$fputtimesTamp ( com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->timesTamp:J */
return;
} // .end method
static void -$$Nest$fputvotes ( com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry p0, android.util.SparseArray p1 ) { //bridge//synthethic
/* .locals 0 */
this.votes = p1;
return;
} // .end method
public com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/display/mode/DisplayModeDirectorImpl; */
/* .param p2, "displayId" # I */
/* .param p3, "desiredDisplayModeSpecs" # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;", */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/display/mode/Vote;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 151 */
/* .local p4, "votes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/display/mode/Vote;>;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 152 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->timesTamp:J */
/* .line 153 */
/* iput p2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->displayId:I */
/* .line 154 */
this.desiredDisplayModeSpecs = p3;
/* .line 155 */
this.votes = p4;
/* .line 156 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 6 */
/* .line 159 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 160 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.this$0;
com.android.server.display.mode.DisplayModeDirectorImpl .-$$Nest$fgetmDateFormat ( v2 );
/* iget-wide v3, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->timesTamp:J */
java.lang.Long .valueOf ( v3,v4 );
(( java.text.SimpleDateFormat ) v2 ).format ( v3 ); // invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " Display "; // const-string v2, " Display "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->displayId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ":\n"; // const-string v2, ":\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 162 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mDesiredDisplayModeSpecs:"; // const-string v2, " mDesiredDisplayModeSpecs:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.desiredDisplayModeSpecs;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 163 */
final String v1 = " mVotes:\n"; // const-string v1, " mVotes:\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 165 */
/* const/16 v1, 0x11 */
/* .local v1, "p":I */
} // :goto_0
/* if-ltz v1, :cond_1 */
/* .line 166 */
v3 = this.votes;
(( android.util.SparseArray ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/display/mode/Vote; */
/* .line 167 */
/* .local v3, "vote":Lcom/android/server/display/mode/Vote; */
/* if-nez v3, :cond_0 */
/* .line 168 */
/* .line 170 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.display.mode.Vote .priorityToString ( v1 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " -> "; // const-string v5, " -> "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 165 */
} // .end local v3 # "vote":Lcom/android/server/display/mode/Vote;
} // :goto_1
/* add-int/lit8 v1, v1, -0x1 */
/* .line 173 */
} // .end local v1 # "p":I
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
