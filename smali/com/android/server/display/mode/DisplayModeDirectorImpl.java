public class com.android.server.display.mode.DisplayModeDirectorImpl implements com.android.server.display.mode.DisplayModeDirectorStub {
	 /* .source "DisplayModeDirectorImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String EXTERNAL_DISPLAY_CONNECTED;
private static final Integer HISTORY_COUNT_FOR_HIGH_RAM_DEVICE;
private static final Integer HISTORY_COUNT_FOR_LOW_RAM_DEVICE;
public static final java.lang.String MIUI_OPTIMIZATION;
public static final java.lang.String MIUI_OPTIMIZATION_PROP;
public static final java.lang.String MIUI_REFRESH_RATE;
public static final java.lang.String MIUI_THERMAL_LIMIT_REFRESH_RATE;
public static final java.lang.String MIUI_USER_REFRESH_RATE;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.display.mode.DisplayModeDirector$BrightnessObserver mBrightnessObserver;
private android.content.Context mContext;
private final java.text.SimpleDateFormat mDateFormat;
private com.android.server.display.mode.DisplayModeDirector mDisplayModeDirector;
private com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry mDisplayModeDirectorHistory;
private Boolean mExternalDisplayConnected;
private final android.net.Uri mExternalDisplayRefreshRateSetting;
private Integer mHistoryCount;
private Integer mHistoryIndex;
private java.lang.Object mLock;
private final android.net.Uri mMiuiOptimizationSetting;
private Boolean mMiuiRefreshRateEnable;
private final android.net.Uri mMiuiRefreshRateSetting;
private Float mSceneMaxRefreshRate;
private com.android.server.display.mode.DisplayModeDirector$SettingsObserver mSettingsObserver;
private final android.net.Uri mThermalRefreshRateSetting;
private com.android.server.display.mode.VotesStorage mVotesStorage;
/* # direct methods */
static java.text.SimpleDateFormat -$$Nest$fgetmDateFormat ( com.android.server.display.mode.DisplayModeDirectorImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDateFormat;
} // .end method
public com.android.server.display.mode.DisplayModeDirectorImpl ( ) {
	 /* .locals 2 */
	 /* .line 25 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 27 */
	 /* new-instance v0, Ljava/text/SimpleDateFormat; */
	 final String v1 = "MM-dd HH:mm:ss.SSS"; // const-string v1, "MM-dd HH:mm:ss.SSS"
	 /* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
	 this.mDateFormat = v0;
	 /* .line 32 */
	 v0 = 	 android.app.ActivityManager .isLowRamDeviceStatic ( );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 33 */
		 /* const/16 v0, 0xa */
	 } // :cond_0
	 /* const/16 v0, 0x1e */
} // :goto_0
/* iput v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I */
/* .line 34 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
/* .line 42 */
final String v0 = "miui_refresh_rate"; // const-string v0, "miui_refresh_rate"
android.provider.Settings$Secure .getUriFor ( v0 );
this.mMiuiRefreshRateSetting = v0;
/* .line 43 */
/* nop */
/* .line 44 */
/* const-string/jumbo v0, "thermal_limit_refresh_rate" */
android.provider.Settings$System .getUriFor ( v0 );
this.mThermalRefreshRateSetting = v0;
/* .line 45 */
final String v0 = "miui_optimization"; // const-string v0, "miui_optimization"
android.provider.Settings$Secure .getUriFor ( v0 );
this.mMiuiOptimizationSetting = v0;
/* .line 47 */
/* nop */
/* .line 48 */
final String v0 = "external_display_connected"; // const-string v0, "external_display_connected"
android.provider.Settings$System .getUriFor ( v0 );
this.mExternalDisplayRefreshRateSetting = v0;
/* .line 47 */
return;
} // .end method
private com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry addToHistory ( Integer p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1, android.util.SparseArray p2 ) {
/* .locals 4 */
/* .param p1, "displayId" # I */
/* .param p2, "desiredDisplayModeSpecs" # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;", */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/display/mode/Vote;", */
/* ">;)", */
/* "Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;" */
/* } */
} // .end annotation
/* .line 117 */
/* .local p3, "votes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/display/mode/Vote;>;" */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 118 */
try { // :try_start_0
v1 = this.mDisplayModeDirectorHistory;
/* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
/* aget-object v3, v1, v2 */
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* .line 119 */
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v1 */
	 com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry .-$$Nest$fputtimesTamp ( v3,v1,v2 );
	 /* .line 120 */
	 v1 = this.mDisplayModeDirectorHistory;
	 /* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
	 /* aget-object v1, v1, v2 */
	 com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry .-$$Nest$fputdisplayId ( v1,p1 );
	 /* .line 121 */
	 v1 = this.mDisplayModeDirectorHistory;
	 /* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
	 /* aget-object v1, v1, v2 */
	 com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry .-$$Nest$fputdesiredDisplayModeSpecs ( v1,p2 );
	 /* .line 122 */
	 v1 = this.mDisplayModeDirectorHistory;
	 /* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
	 /* aget-object v1, v1, v2 */
	 com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry .-$$Nest$fputvotes ( v1,p3 );
	 /* .line 124 */
} // :cond_0
/* new-instance v3, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry; */
/* invoke-direct {v3, p0, p1, p2, p3}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;-><init>(Lcom/android/server/display/mode/DisplayModeDirectorImpl;ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)V */
/* aput-object v3, v1, v2 */
/* .line 127 */
} // :goto_0
v1 = this.mDisplayModeDirectorHistory;
/* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
/* aget-object v1, v1, v2 */
/* .line 128 */
/* .local v1, "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry; */
/* add-int/lit8 v2, v2, 0x1 */
/* iget v3, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I */
/* rem-int/2addr v2, v3 */
/* iput v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
/* .line 129 */
/* monitor-exit v0 */
/* .line 130 */
} // .end local v1 # "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void clearAospSettingVote ( ) {
/* .locals 3 */
/* .line 243 */
v0 = this.mVotesStorage;
int v1 = 7; // const/4 v1, 0x7
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.display.mode.VotesStorage ) v0 ).updateGlobalVote ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 245 */
v0 = this.mVotesStorage;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.mode.VotesStorage ) v0 ).updateGlobalVote ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 247 */
v0 = this.mVotesStorage;
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.display.mode.VotesStorage ) v0 ).updateGlobalVote ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 249 */
return;
} // .end method
private void clearMiuiSettingVote ( ) {
/* .locals 3 */
/* .line 236 */
v0 = this.mVotesStorage;
/* const/16 v1, 0x10 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.display.mode.VotesStorage ) v0 ).updateGlobalVote ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 238 */
v0 = this.mVotesStorage;
/* const/16 v1, 0xf */
(( com.android.server.display.mode.VotesStorage ) v0 ).updateGlobalVote ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 240 */
return;
} // .end method
private com.android.server.display.mode.Vote getMiuiRefreshRateVote ( ) {
/* .locals 4 */
/* .line 213 */
/* iget-boolean v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayConnected:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 214 */
/* const/high16 v0, 0x42700000 # 60.0f */
com.android.server.display.mode.Vote .forRenderFrameRates ( v1,v0 );
/* .local v0, "miuiVote":Lcom/android/server/display/mode/Vote; */
/* .line 215 */
} // .end local v0 # "miuiVote":Lcom/android/server/display/mode/Vote;
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F */
/* cmpl-float v0, v0, v1 */
/* if-lez v0, :cond_2 */
/* .line 216 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "user_refresh_rate" */
/* const/high16 v3, -0x40800000 # -1.0f */
v0 = android.provider.Settings$Secure .getFloat ( v0,v2,v3 );
/* .line 218 */
/* .local v0, "userSetRefreshRate":F */
/* cmpl-float v2, v0, v1 */
/* if-lez v2, :cond_1 */
/* .line 219 */
/* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F */
v2 = java.lang.Math .min ( v2,v0 );
} // :cond_1
/* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F */
} // :goto_0
/* iput v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F */
/* .line 220 */
com.android.server.display.mode.Vote .forRenderFrameRates ( v1,v2 );
/* .line 221 */
/* .local v0, "miuiVote":Lcom/android/server/display/mode/Vote; */
/* .line 222 */
} // .end local v0 # "miuiVote":Lcom/android/server/display/mode/Vote;
} // :cond_2
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 223 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
final String v2 = "miui_refresh_rate"; // const-string v2, "miui_refresh_rate"
v2 = android.provider.Settings$Secure .getFloat ( v0,v2,v1 );
/* .line 224 */
/* .local v2, "miuiRefreshRate":F */
/* cmpl-float v3, v2, v1 */
/* if-nez v3, :cond_3 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_3
com.android.server.display.mode.Vote .forRenderFrameRates ( v1,v2 );
} // :goto_1
/* move-object v0, v1 */
/* .line 226 */
} // .end local v2 # "miuiRefreshRate":F
/* .local v0, "miuiVote":Lcom/android/server/display/mode/Vote; */
} // :goto_2
} // .end method
private com.android.server.display.mode.Vote getThermalLimitRefreshRateVote ( ) {
/* .locals 3 */
/* .line 230 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "thermal_limit_refresh_rate" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* .line 232 */
/* .local v0, "thermalRefreshRate":I */
/* if-nez v0, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* int-to-float v2, v0 */
com.android.server.display.mode.Vote .forRenderFrameRates ( v1,v2 );
} // :goto_0
} // .end method
private void updateExternalDisplayConnectedLocked ( ) {
/* .locals 3 */
/* .line 252 */
v0 = this.mContext;
/* .line 253 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 252 */
final String v1 = "external_display_connected"; // const-string v1, "external_display_connected"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* iput-boolean v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayConnected:Z */
/* .line 254 */
return;
} // .end method
private void updateMiuiRefreshRateState ( ) {
/* .locals 2 */
/* .line 139 */
/* nop */
/* .line 140 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 139 */
/* xor-int/lit8 v0, v0, 0x1 */
final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
/* iput-boolean v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateEnable:Z */
/* .line 141 */
return;
} // .end method
/* # virtual methods */
public void dumpLocked ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 91 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSceneMaxRefreshRate: "; // const-string v1, " mSceneMaxRefreshRate: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 92 */
final String v0 = "History of DisplayMoDirector"; // const-string v0, "History of DisplayMoDirector"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 93 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* iget v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I */
/* if-ge v0, v1, :cond_1 */
/* .line 94 */
/* iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I */
/* add-int/2addr v2, v0 */
/* rem-int/2addr v2, v1 */
/* .line 95 */
/* .local v2, "index":I */
v1 = this.mDisplayModeDirectorHistory;
/* aget-object v1, v1, v2 */
/* .line 97 */
/* .local v1, "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry; */
/* if-nez v1, :cond_0 */
/* .line 98 */
/* .line 100 */
} // :cond_0
(( com.android.server.display.mode.DisplayModeDirectorImpl$DisplayModeDirectorEntry ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 93 */
} // .end local v1 # "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
} // .end local v2 # "index":I
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 102 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
public android.util.SparseArray getVotes ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/display/mode/Vote;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 300 */
v0 = this.mVotesStorage;
(( com.android.server.display.mode.VotesStorage ) v0 ).getVotes ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/mode/VotesStorage;->getVotes(I)Landroid/util/SparseArray;
} // .end method
public void init ( com.android.server.display.mode.DisplayModeDirector p0, java.lang.Object p1, com.android.server.display.mode.DisplayModeDirector$BrightnessObserver p2, com.android.server.display.mode.DisplayModeDirector$SettingsObserver p3, android.content.Context p4, com.android.server.display.mode.VotesStorage p5 ) {
/* .locals 1 */
/* .param p1, "modeDirector" # Lcom/android/server/display/mode/DisplayModeDirector; */
/* .param p2, "lock" # Ljava/lang/Object; */
/* .param p3, "brightnessObserver" # Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver; */
/* .param p4, "settingsObserver" # Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver; */
/* .param p5, "context" # Landroid/content/Context; */
/* .param p6, "votesStorage" # Lcom/android/server/display/mode/VotesStorage; */
/* .line 69 */
this.mLock = p2;
/* .line 70 */
this.mDisplayModeDirector = p1;
/* .line 71 */
/* iget v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I */
/* new-array v0, v0, [Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry; */
this.mDisplayModeDirectorHistory = v0;
/* .line 72 */
this.mBrightnessObserver = p3;
/* .line 73 */
this.mSettingsObserver = p4;
/* .line 74 */
this.mContext = p5;
/* .line 75 */
this.mVotesStorage = p6;
/* .line 76 */
return;
} // .end method
public void notifyDisplayModeSpecsChanged ( ) {
/* .locals 1 */
/* .line 109 */
v0 = this.mDisplayModeDirector;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 110 */
(( com.android.server.display.mode.DisplayModeDirector ) v0 ).notifyDesiredDisplayModeSpecsChanged ( ); // invoke-virtual {v0}, Lcom/android/server/display/mode/DisplayModeDirector;->notifyDesiredDisplayModeSpecsChanged()V
/* .line 112 */
} // :cond_0
return;
} // .end method
public void onDesiredDisplayModeSpecsChanged ( Integer p0, com.android.server.display.mode.DisplayModeDirector$DesiredDisplayModeSpecs p1, android.util.SparseArray p2 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .param p2, "desiredDisplayModeSpecs" # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;", */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/display/mode/Vote;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 82 */
/* .local p3, "votes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/display/mode/Vote;>;" */
if ( p3 != null) { // if-eqz p3, :cond_1
v0 = (( android.util.SparseArray ) p3 ).size ( ); // invoke-virtual {p3}, Landroid/util/SparseArray;->size()I
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 83 */
/* .local v0, "noVotes":Z */
} // :goto_1
/* if-nez v0, :cond_2 */
/* .line 84 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->addToHistory(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry; */
/* .line 86 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onDesiredDisplayModeSpecsChanged:"; // const-string v2, "onDesiredDisplayModeSpecsChanged:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " noVotes="; // const-string v2, " noVotes="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DisplayModeDirectorImpl"; // const-string v2, "DisplayModeDirectorImpl"
android.util.Slog .i ( v2,v1 );
/* .line 87 */
return;
} // .end method
public void registerMiuiContentObserver ( android.content.ContentResolver p0, android.database.ContentObserver p1 ) {
/* .locals 2 */
/* .param p1, "cr" # Landroid/content/ContentResolver; */
/* .param p2, "observer" # Landroid/database/ContentObserver; */
/* .line 280 */
v0 = this.mMiuiRefreshRateSetting;
int v1 = 0; // const/4 v1, 0x0
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v1, p2, v1 ); // invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 282 */
v0 = this.mMiuiOptimizationSetting;
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v1, p2, v1 ); // invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 285 */
/* const-string/jumbo v0, "thermal_limit_refresh_rate" */
v0 = android.provider.Settings$System .getInt ( p1,v0,v1 );
/* if-lez v0, :cond_0 */
/* .line 286 */
/* const-string/jumbo v0, "thermal_limit_refresh_rate" */
android.provider.Settings$System .putInt ( p1,v0,v1 );
/* .line 288 */
} // :cond_0
v0 = this.mThermalRefreshRateSetting;
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v1, p2, v1 ); // invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 290 */
v0 = this.mExternalDisplayRefreshRateSetting;
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v1, p2, v1 ); // invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 292 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 293 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateExternalDisplayConnectedLocked()V */
/* .line 294 */
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateMiuiRefreshRateState()V */
/* .line 295 */
/* monitor-exit v0 */
/* .line 296 */
return;
/* .line 295 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setSceneMaxRefreshRate ( Integer p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .param p2, "maxFrameRate" # F */
/* .line 272 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 273 */
try { // :try_start_0
/* iput p2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F */
/* .line 274 */
v1 = this.mSettingsObserver;
(( com.android.server.display.mode.DisplayModeDirector$SettingsObserver ) v1 ).updateMiuiRefreshRateSettingLocked ( ); // invoke-virtual {v1}, Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;->updateMiuiRefreshRateSettingLocked()V
/* .line 275 */
/* monitor-exit v0 */
/* .line 276 */
return;
/* .line 275 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateDisplaySize ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "displayId" # I */
/* .param p2, "width" # I */
/* .param p3, "height" # I */
/* .line 179 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 180 */
try { // :try_start_0
com.android.server.display.mode.Vote .forSize ( p2,p3 );
/* .line 181 */
/* .local v1, "sizeVote":Lcom/android/server/display/mode/Vote; */
v2 = this.mVotesStorage;
/* const/16 v3, 0x11 */
(( com.android.server.display.mode.VotesStorage ) v2 ).updateVote ( p1, v3, v1 ); // invoke-virtual {v2, p1, v3, v1}, Lcom/android/server/display/mode/VotesStorage;->updateVote(IILcom/android/server/display/mode/Vote;)V
/* .line 182 */
} // .end local v1 # "sizeVote":Lcom/android/server/display/mode/Vote;
/* monitor-exit v0 */
/* .line 183 */
return;
/* .line 182 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean updateMiuiRefreshRateEnhance ( android.net.Uri p0 ) {
/* .locals 2 */
/* .param p1, "uri" # Landroid/net/Uri; */
/* .line 258 */
v0 = this.mMiuiOptimizationSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateMiuiRefreshRateState()V */
/* .line 260 */
/* .line 261 */
} // :cond_0
v0 = this.mExternalDisplayRefreshRateSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 262 */
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateExternalDisplayConnectedLocked()V */
/* .line 263 */
/* .line 264 */
} // :cond_1
v0 = this.mThermalRefreshRateSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
v0 = this.mMiuiRefreshRateSetting;
v0 = (( android.net.Uri ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 267 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 265 */
} // :cond_3
} // :goto_0
} // .end method
public Boolean updateMiuiRefreshRateSettingLocked ( Float p0 ) {
/* .locals 7 */
/* .param p1, "minRefreshRate" # F */
/* .line 187 */
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->getMiuiRefreshRateVote()Lcom/android/server/display/mode/Vote; */
/* .line 189 */
/* .local v0, "miuiRefreshVote":Lcom/android/server/display/mode/Vote; */
/* iget-boolean v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateEnable:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 190 */
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->getThermalLimitRefreshRateVote()Lcom/android/server/display/mode/Vote; */
/* .line 191 */
/* .local v1, "thermalVote":Lcom/android/server/display/mode/Vote; */
v3 = this.mVotesStorage;
/* const/16 v4, 0x10 */
(( com.android.server.display.mode.VotesStorage ) v3 ).updateGlobalVote ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 193 */
v3 = this.mVotesStorage;
/* const/16 v4, 0xf */
(( com.android.server.display.mode.VotesStorage ) v3 ).updateGlobalVote ( v4, v0 ); // invoke-virtual {v3, v4, v0}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V
/* .line 195 */
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->clearAospSettingVote()V */
/* .line 196 */
int p1 = 0; // const/4 p1, 0x0
/* .line 197 */
/* const/high16 v3, 0x7f800000 # Float.POSITIVE_INFINITY */
/* if-nez v1, :cond_0 */
/* move v4, v3 */
/* .line 198 */
} // :cond_0
v4 = this.refreshRateRanges;
v4 = this.render;
/* iget v4, v4, Landroid/view/SurfaceControl$RefreshRateRange;->max:F */
} // :goto_0
/* nop */
/* .line 199 */
/* .local v4, "thermalRefreshRate":F */
/* if-nez v0, :cond_1 */
/* .line 200 */
} // :cond_1
v3 = this.refreshRateRanges;
v3 = this.render;
/* iget v3, v3, Landroid/view/SurfaceControl$RefreshRateRange;->max:F */
} // :goto_1
/* nop */
/* .line 201 */
/* .local v3, "miuiRefresh":F */
v5 = java.lang.Math .min ( v4,v3 );
/* .line 202 */
/* .local v5, "miuiMaxRefreshRate":F */
v6 = this.mBrightnessObserver;
(( com.android.server.display.mode.DisplayModeDirector$BrightnessObserver ) v6 ).onRefreshRateSettingChangedLocked ( p1, v5 ); // invoke-virtual {v6, p1, v5}, Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;->onRefreshRateSettingChangedLocked(FF)V
/* .line 203 */
if ( v0 != null) { // if-eqz v0, :cond_2
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
/* .line 205 */
} // .end local v1 # "thermalVote":Lcom/android/server/display/mode/Vote;
} // .end local v3 # "miuiRefresh":F
} // .end local v4 # "thermalRefreshRate":F
} // .end local v5 # "miuiMaxRefreshRate":F
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->clearMiuiSettingVote()V */
/* .line 206 */
} // .end method
