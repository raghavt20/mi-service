.class public Lcom/android/server/display/mode/DisplayModeDirectorImpl;
.super Ljava/lang/Object;
.source "DisplayModeDirectorImpl.java"

# interfaces
.implements Lcom/android/server/display/mode/DisplayModeDirectorStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
    }
.end annotation


# static fields
.field private static final EXTERNAL_DISPLAY_CONNECTED:Ljava/lang/String; = "external_display_connected"

.field private static final HISTORY_COUNT_FOR_HIGH_RAM_DEVICE:I = 0x1e

.field private static final HISTORY_COUNT_FOR_LOW_RAM_DEVICE:I = 0xa

.field public static final MIUI_OPTIMIZATION:Ljava/lang/String; = "miui_optimization"

.field public static final MIUI_OPTIMIZATION_PROP:Ljava/lang/String; = "persist.sys.miui_optimization"

.field public static final MIUI_REFRESH_RATE:Ljava/lang/String; = "miui_refresh_rate"

.field public static final MIUI_THERMAL_LIMIT_REFRESH_RATE:Ljava/lang/String; = "thermal_limit_refresh_rate"

.field public static final MIUI_USER_REFRESH_RATE:Ljava/lang/String; = "user_refresh_rate"

.field private static final TAG:Ljava/lang/String; = "DisplayModeDirectorImpl"


# instance fields
.field private mBrightnessObserver:Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;

.field private mContext:Landroid/content/Context;

.field private final mDateFormat:Ljava/text/SimpleDateFormat;

.field private mDisplayModeDirector:Lcom/android/server/display/mode/DisplayModeDirector;

.field private mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

.field private mExternalDisplayConnected:Z

.field private final mExternalDisplayRefreshRateSetting:Landroid/net/Uri;

.field private mHistoryCount:I

.field private mHistoryIndex:I

.field private mLock:Ljava/lang/Object;

.field private final mMiuiOptimizationSetting:Landroid/net/Uri;

.field private mMiuiRefreshRateEnable:Z

.field private final mMiuiRefreshRateSetting:Landroid/net/Uri;

.field private mSceneMaxRefreshRate:F

.field private mSettingsObserver:Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;

.field private final mThermalRefreshRateSetting:Landroid/net/Uri;

.field private mVotesStorage:Lcom/android/server/display/mode/VotesStorage;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDateFormat(Lcom/android/server/display/mode/DisplayModeDirectorImpl;)Ljava/text/SimpleDateFormat;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDateFormat:Ljava/text/SimpleDateFormat;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDateFormat:Ljava/text/SimpleDateFormat;

    .line 32
    invoke-static {}, Landroid/app/ActivityManager;->isLowRamDeviceStatic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    const/16 v0, 0x1e

    :goto_0
    iput v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    .line 42
    const-string v0, "miui_refresh_rate"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateSetting:Landroid/net/Uri;

    .line 43
    nop

    .line 44
    const-string/jumbo v0, "thermal_limit_refresh_rate"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mThermalRefreshRateSetting:Landroid/net/Uri;

    .line 45
    const-string v0, "miui_optimization"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiOptimizationSetting:Landroid/net/Uri;

    .line 47
    nop

    .line 48
    const-string v0, "external_display_connected"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayRefreshRateSetting:Landroid/net/Uri;

    .line 47
    return-void
.end method

.method private addToHistory(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
    .locals 4
    .param p1, "displayId"    # I
    .param p2, "desiredDisplayModeSpecs"    # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;",
            "Landroid/util/SparseArray<",
            "Lcom/android/server/display/mode/Vote;",
            ">;)",
            "Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;"
        }
    .end annotation

    .line 117
    .local p3, "votes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/display/mode/Vote;>;"
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 118
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    aget-object v3, v1, v2

    if-eqz v3, :cond_0

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v3, v1, v2}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->-$$Nest$fputtimesTamp(Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;J)V

    .line 120
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    aget-object v1, v1, v2

    invoke-static {v1, p1}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->-$$Nest$fputdisplayId(Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;I)V

    .line 121
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    aget-object v1, v1, v2

    invoke-static {v1, p2}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->-$$Nest$fputdesiredDisplayModeSpecs(Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;)V

    .line 122
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    aget-object v1, v1, v2

    invoke-static {v1, p3}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->-$$Nest$fputvotes(Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;Landroid/util/SparseArray;)V

    goto :goto_0

    .line 124
    :cond_0
    new-instance v3, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;-><init>(Lcom/android/server/display/mode/DisplayModeDirectorImpl;ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)V

    aput-object v3, v1, v2

    .line 127
    :goto_0
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    aget-object v1, v1, v2

    .line 128
    .local v1, "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    .line 129
    monitor-exit v0

    return-object v1

    .line 130
    .end local v1    # "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private clearAospSettingVote()V
    .locals 3

    .line 243
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 245
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 247
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 249
    return-void
.end method

.method private clearMiuiSettingVote()V
    .locals 3

    .line 236
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 238
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 240
    return-void
.end method

.method private getMiuiRefreshRateVote()Lcom/android/server/display/mode/Vote;
    .locals 4

    .line 213
    iget-boolean v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayConnected:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 214
    const/high16 v0, 0x42700000    # 60.0f

    invoke-static {v1, v0}, Lcom/android/server/display/mode/Vote;->forRenderFrameRates(FF)Lcom/android/server/display/mode/Vote;

    move-result-object v0

    .local v0, "miuiVote":Lcom/android/server/display/mode/Vote;
    goto :goto_2

    .line 215
    .end local v0    # "miuiVote":Lcom/android/server/display/mode/Vote;
    :cond_0
    iget v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "user_refresh_rate"

    const/high16 v3, -0x40800000    # -1.0f

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    .line 218
    .local v0, "userSetRefreshRate":F
    cmpl-float v2, v0, v1

    if-lez v2, :cond_1

    .line 219
    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F

    :goto_0
    iput v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F

    .line 220
    invoke-static {v1, v2}, Lcom/android/server/display/mode/Vote;->forRenderFrameRates(FF)Lcom/android/server/display/mode/Vote;

    move-result-object v0

    .line 221
    .local v0, "miuiVote":Lcom/android/server/display/mode/Vote;
    goto :goto_2

    .line 222
    .end local v0    # "miuiVote":Lcom/android/server/display/mode/Vote;
    :cond_2
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 223
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "miui_refresh_rate"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v2

    .line 224
    .local v2, "miuiRefreshRate":F
    cmpl-float v3, v2, v1

    if-nez v3, :cond_3

    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    invoke-static {v1, v2}, Lcom/android/server/display/mode/Vote;->forRenderFrameRates(FF)Lcom/android/server/display/mode/Vote;

    move-result-object v1

    :goto_1
    move-object v0, v1

    .line 226
    .end local v2    # "miuiRefreshRate":F
    .local v0, "miuiVote":Lcom/android/server/display/mode/Vote;
    :goto_2
    return-object v0
.end method

.method private getThermalLimitRefreshRateVote()Lcom/android/server/display/mode/Vote;
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "thermal_limit_refresh_rate"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 232
    .local v0, "thermalRefreshRate":I
    if-nez v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-static {v1, v2}, Lcom/android/server/display/mode/Vote;->forRenderFrameRates(FF)Lcom/android/server/display/mode/Vote;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method private updateExternalDisplayConnectedLocked()V
    .locals 3

    .line 252
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mContext:Landroid/content/Context;

    .line 253
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 252
    const-string v1, "external_display_connected"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayConnected:Z

    .line 254
    return-void
.end method

.method private updateMiuiRefreshRateState()V
    .locals 2

    .line 139
    nop

    .line 140
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 139
    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateEnable:Z

    .line 141
    return-void
.end method


# virtual methods
.method public dumpLocked(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mSceneMaxRefreshRate: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 92
    const-string v0, "History of DisplayMoDirector"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 93
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I

    if-ge v0, v1, :cond_1

    .line 94
    iget v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryIndex:I

    add-int/2addr v2, v0

    rem-int/2addr v2, v1

    .line 95
    .local v2, "index":I
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    aget-object v1, v1, v2

    .line 97
    .local v1, "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
    if-nez v1, :cond_0

    .line 98
    goto :goto_1

    .line 100
    :cond_0
    invoke-virtual {v1}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 93
    .end local v1    # "displayModeDirectorEntry":Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;
    .end local v2    # "index":I
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public getVotes(I)Landroid/util/SparseArray;
    .locals 1
    .param p1, "displayId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/SparseArray<",
            "Lcom/android/server/display/mode/Vote;",
            ">;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    invoke-virtual {v0, p1}, Lcom/android/server/display/mode/VotesStorage;->getVotes(I)Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/android/server/display/mode/DisplayModeDirector;Ljava/lang/Object;Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;Landroid/content/Context;Lcom/android/server/display/mode/VotesStorage;)V
    .locals 1
    .param p1, "modeDirector"    # Lcom/android/server/display/mode/DisplayModeDirector;
    .param p2, "lock"    # Ljava/lang/Object;
    .param p3, "brightnessObserver"    # Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;
    .param p4, "settingsObserver"    # Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "votesStorage"    # Lcom/android/server/display/mode/VotesStorage;

    .line 69
    iput-object p2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mLock:Ljava/lang/Object;

    .line 70
    iput-object p1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirector:Lcom/android/server/display/mode/DisplayModeDirector;

    .line 71
    iget v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mHistoryCount:I

    new-array v0, v0, [Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    iput-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirectorHistory:[Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    .line 72
    iput-object p3, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mBrightnessObserver:Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;

    .line 73
    iput-object p4, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSettingsObserver:Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;

    .line 74
    iput-object p5, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mContext:Landroid/content/Context;

    .line 75
    iput-object p6, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    .line 76
    return-void
.end method

.method public notifyDisplayModeSpecsChanged()V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mDisplayModeDirector:Lcom/android/server/display/mode/DisplayModeDirector;

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Lcom/android/server/display/mode/DisplayModeDirector;->notifyDesiredDisplayModeSpecsChanged()V

    .line 112
    :cond_0
    return-void
.end method

.method public onDesiredDisplayModeSpecsChanged(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)V
    .locals 3
    .param p1, "displayId"    # I
    .param p2, "desiredDisplayModeSpecs"    # Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;",
            "Landroid/util/SparseArray<",
            "Lcom/android/server/display/mode/Vote;",
            ">;)V"
        }
    .end annotation

    .line 82
    .local p3, "votes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/display/mode/Vote;>;"
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 83
    .local v0, "noVotes":Z
    :goto_1
    if-nez v0, :cond_2

    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->addToHistory(ILcom/android/server/display/mode/DisplayModeDirector$DesiredDisplayModeSpecs;Landroid/util/SparseArray;)Lcom/android/server/display/mode/DisplayModeDirectorImpl$DisplayModeDirectorEntry;

    .line 86
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDesiredDisplayModeSpecsChanged:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " noVotes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DisplayModeDirectorImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method public registerMiuiContentObserver(Landroid/content/ContentResolver;Landroid/database/ContentObserver;)V
    .locals 2
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .line 280
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateSetting:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 282
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiOptimizationSetting:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 285
    const-string/jumbo v0, "thermal_limit_refresh_rate"

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    .line 286
    const-string/jumbo v0, "thermal_limit_refresh_rate"

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mThermalRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 290
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1, p2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 292
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 293
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateExternalDisplayConnectedLocked()V

    .line 294
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateMiuiRefreshRateState()V

    .line 295
    monitor-exit v0

    .line 296
    return-void

    .line 295
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setSceneMaxRefreshRate(IF)V
    .locals 2
    .param p1, "displayId"    # I
    .param p2, "maxFrameRate"    # F

    .line 272
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 273
    :try_start_0
    iput p2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSceneMaxRefreshRate:F

    .line 274
    iget-object v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mSettingsObserver:Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;

    invoke-virtual {v1}, Lcom/android/server/display/mode/DisplayModeDirector$SettingsObserver;->updateMiuiRefreshRateSettingLocked()V

    .line 275
    monitor-exit v0

    .line 276
    return-void

    .line 275
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateDisplaySize(III)V
    .locals 4
    .param p1, "displayId"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .line 179
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 180
    :try_start_0
    invoke-static {p2, p3}, Lcom/android/server/display/mode/Vote;->forSize(II)Lcom/android/server/display/mode/Vote;

    move-result-object v1

    .line 181
    .local v1, "sizeVote":Lcom/android/server/display/mode/Vote;
    iget-object v2, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/16 v3, 0x11

    invoke-virtual {v2, p1, v3, v1}, Lcom/android/server/display/mode/VotesStorage;->updateVote(IILcom/android/server/display/mode/Vote;)V

    .line 182
    .end local v1    # "sizeVote":Lcom/android/server/display/mode/Vote;
    monitor-exit v0

    .line 183
    return-void

    .line 182
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateMiuiRefreshRateEnhance(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .line 258
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiOptimizationSetting:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateMiuiRefreshRateState()V

    .line 260
    return v1

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mExternalDisplayRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->updateExternalDisplayConnectedLocked()V

    .line 263
    return v1

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mThermalRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateSetting:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 267
    :cond_2
    const/4 v0, 0x0

    return v0

    .line 265
    :cond_3
    :goto_0
    return v1
.end method

.method public updateMiuiRefreshRateSettingLocked(F)Z
    .locals 7
    .param p1, "minRefreshRate"    # F

    .line 187
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->getMiuiRefreshRateVote()Lcom/android/server/display/mode/Vote;

    move-result-object v0

    .line 189
    .local v0, "miuiRefreshVote":Lcom/android/server/display/mode/Vote;
    iget-boolean v1, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mMiuiRefreshRateEnable:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 190
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->getThermalLimitRefreshRateVote()Lcom/android/server/display/mode/Vote;

    move-result-object v1

    .line 191
    .local v1, "thermalVote":Lcom/android/server/display/mode/Vote;
    iget-object v3, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/16 v4, 0x10

    invoke-virtual {v3, v4, v1}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 193
    iget-object v3, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mVotesStorage:Lcom/android/server/display/mode/VotesStorage;

    const/16 v4, 0xf

    invoke-virtual {v3, v4, v0}, Lcom/android/server/display/mode/VotesStorage;->updateGlobalVote(ILcom/android/server/display/mode/Vote;)V

    .line 195
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->clearAospSettingVote()V

    .line 196
    const/4 p1, 0x0

    .line 197
    const/high16 v3, 0x7f800000    # Float.POSITIVE_INFINITY

    if-nez v1, :cond_0

    move v4, v3

    goto :goto_0

    .line 198
    :cond_0
    iget-object v4, v1, Lcom/android/server/display/mode/Vote;->refreshRateRanges:Landroid/view/SurfaceControl$RefreshRateRanges;

    iget-object v4, v4, Landroid/view/SurfaceControl$RefreshRateRanges;->render:Landroid/view/SurfaceControl$RefreshRateRange;

    iget v4, v4, Landroid/view/SurfaceControl$RefreshRateRange;->max:F

    :goto_0
    nop

    .line 199
    .local v4, "thermalRefreshRate":F
    if-nez v0, :cond_1

    goto :goto_1

    .line 200
    :cond_1
    iget-object v3, v0, Lcom/android/server/display/mode/Vote;->refreshRateRanges:Landroid/view/SurfaceControl$RefreshRateRanges;

    iget-object v3, v3, Landroid/view/SurfaceControl$RefreshRateRanges;->render:Landroid/view/SurfaceControl$RefreshRateRange;

    iget v3, v3, Landroid/view/SurfaceControl$RefreshRateRange;->max:F

    :goto_1
    nop

    .line 201
    .local v3, "miuiRefresh":F
    invoke-static {v4, v3}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 202
    .local v5, "miuiMaxRefreshRate":F
    iget-object v6, p0, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->mBrightnessObserver:Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;

    invoke-virtual {v6, p1, v5}, Lcom/android/server/display/mode/DisplayModeDirector$BrightnessObserver;->onRefreshRateSettingChangedLocked(FF)V

    .line 203
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :cond_2
    return v2

    .line 205
    .end local v1    # "thermalVote":Lcom/android/server/display/mode/Vote;
    .end local v3    # "miuiRefresh":F
    .end local v4    # "thermalRefreshRate":F
    .end local v5    # "miuiMaxRefreshRate":F
    :cond_3
    invoke-direct {p0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl;->clearMiuiSettingVote()V

    .line 206
    return v2
.end method
