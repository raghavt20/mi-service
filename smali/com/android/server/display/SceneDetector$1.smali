.class Lcom/android/server/display/SceneDetector$1;
.super Ljava/lang/Object;
.source "SceneDetector.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SceneDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/SceneDetector;


# direct methods
.method public static synthetic $r8$lambda$D-6GL5NhYZgtOrYsKjHuisJpv_U(Lcom/android/server/display/SceneDetector$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SceneDetector$1;->lambda$binderDied$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/SceneDetector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/SceneDetector;

    .line 208
    iput-object p1, p0, Lcom/android/server/display/SceneDetector$1;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$binderDied$0()V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$1;->this$0:Lcom/android/server/display/SceneDetector;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/display/SceneDetector;->-$$Nest$fputmAonState(Lcom/android/server/display/SceneDetector;I)V

    .line 214
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$1;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$mresetServiceConnectedStatus(Lcom/android/server/display/SceneDetector;)V

    .line 215
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$1;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$mtryToBindAonFlareService(Lcom/android/server/display/SceneDetector;)V

    .line 216
    const-string v0, "SceneDetector"

    const-string v1, "Process of service has died, try to bind it."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$1;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmSceneDetectorHandler(Lcom/android/server/display/SceneDetector;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/SceneDetector$1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/SceneDetector$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 218
    return-void
.end method
