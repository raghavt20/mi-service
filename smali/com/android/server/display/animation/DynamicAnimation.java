public abstract class com.android.server.display.animation.DynamicAnimation implements com.android.server.display.animation.AnimationHandler$AnimationFrameCallback {
	 /* .source "DynamicAnimation.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;, */
	 /* Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;, */
	 /* Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener;, */
	 /* Lcom/android/server/display/animation/DynamicAnimation$MassState; */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Lcom/android/server/display/animation/DynamicAnimation<", */
/* "TT;>;>", */
/* "Ljava/lang/Object;", */
/* "Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;" */
/* } */
} // .end annotation
/* # static fields */
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty ALPHA;
public static final Float MIN_VISIBLE_CHANGE_ALPHA;
public static final Float MIN_VISIBLE_CHANGE_PIXELS;
public static final Float MIN_VISIBLE_CHANGE_ROTATION_DEGREES;
public static final Float MIN_VISIBLE_CHANGE_SCALE;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty ROTATION;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty ROTATION_X;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty ROTATION_Y;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty SCALE_X;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty SCALE_Y;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty SCROLL_X;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty SCROLL_Y;
private static final Float THRESHOLD_MULTIPLIER;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty TRANSLATION_X;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty TRANSLATION_Y;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty TRANSLATION_Z;
private static final Float UNSET;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty X;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty Y;
public static final com.android.server.display.animation.DynamicAnimation$ViewProperty Z;
/* # instance fields */
private final java.util.ArrayList mEndListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mLastFrameTime;
Float mMaxValue;
Float mMinValue;
private Float mMinVisibleChange;
final com.android.server.display.animation.FloatPropertyCompat mProperty;
Boolean mRunning;
Boolean mStartValueIsSet;
final java.lang.Object mTarget;
private final java.util.ArrayList mUpdateListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Float mValue;
Float mVelocity;
/* # direct methods */
static com.android.server.display.animation.DynamicAnimation ( ) {
/* .locals 2 */
/* .line 55 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$1; */
/* const-string/jumbo v1, "translationX" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$1;-><init>(Ljava/lang/String;)V */
/* .line 70 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$2; */
/* const-string/jumbo v1, "translationY" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$2;-><init>(Ljava/lang/String;)V */
/* .line 85 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$3; */
/* const-string/jumbo v1, "translationZ" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$3;-><init>(Ljava/lang/String;)V */
/* .line 100 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$4; */
final String v1 = "scaleX"; // const-string v1, "scaleX"
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$4;-><init>(Ljava/lang/String;)V */
/* .line 115 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$5; */
final String v1 = "scaleY"; // const-string v1, "scaleY"
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$5;-><init>(Ljava/lang/String;)V */
/* .line 130 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$6; */
final String v1 = "rotation"; // const-string v1, "rotation"
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$6;-><init>(Ljava/lang/String;)V */
/* .line 145 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$7; */
final String v1 = "rotationX"; // const-string v1, "rotationX"
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$7;-><init>(Ljava/lang/String;)V */
/* .line 160 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$8; */
final String v1 = "rotationY"; // const-string v1, "rotationY"
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$8;-><init>(Ljava/lang/String;)V */
/* .line 175 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$9; */
/* const-string/jumbo v1, "x" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$9;-><init>(Ljava/lang/String;)V */
/* .line 190 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$10; */
/* const-string/jumbo v1, "y" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$10;-><init>(Ljava/lang/String;)V */
/* .line 205 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$11; */
/* const-string/jumbo v1, "z" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$11;-><init>(Ljava/lang/String;)V */
/* .line 220 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$12; */
final String v1 = "alpha"; // const-string v1, "alpha"
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$12;-><init>(Ljava/lang/String;)V */
/* .line 236 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$13; */
/* const-string/jumbo v1, "scrollX" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$13;-><init>(Ljava/lang/String;)V */
/* .line 251 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$14; */
/* const-string/jumbo v1, "scrollY" */
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/DynamicAnimation$14;-><init>(Ljava/lang/String;)V */
return;
} // .end method
 com.android.server.display.animation.DynamicAnimation ( ) {
/* .locals 2 */
/* .param p1, "floatValueHolder" # Lcom/android/server/display/animation/FloatValueHolder; */
/* .line 331 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 287 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mVelocity:F */
/* .line 290 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* .line 294 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 303 */
/* iput-boolean v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
/* .line 306 */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMaxValue:F */
/* .line 307 */
/* neg-float v0, v0 */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinValue:F */
/* .line 310 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mLastFrameTime:J */
/* .line 315 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mEndListeners = v0;
/* .line 318 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mUpdateListeners = v0;
/* .line 332 */
int v0 = 0; // const/4 v0, 0x0
this.mTarget = v0;
/* .line 333 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$15; */
final String v1 = "FloatValueHolder"; // const-string v1, "FloatValueHolder"
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/display/animation/DynamicAnimation$15;-><init>(Lcom/android/server/display/animation/DynamicAnimation;Ljava/lang/String;Lcom/android/server/display/animation/FloatValueHolder;)V */
this.mProperty = v0;
/* .line 344 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* .line 345 */
return;
} // .end method
 com.android.server.display.animation.DynamicAnimation ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<K:", */
/* "Ljava/lang/Object;", */
/* ">(TK;", */
/* "Lcom/android/server/display/animation/FloatPropertyCompat<", */
/* "TK;>;)V" */
/* } */
} // .end annotation
/* .line 354 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* .local p1, "object":Ljava/lang/Object;, "TK;" */
/* .local p2, "property":Lcom/android/server/display/animation/FloatPropertyCompat;, "Lcom/android/server/display/animation/FloatPropertyCompat<TK;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 287 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mVelocity:F */
/* .line 290 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* .line 294 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 303 */
/* iput-boolean v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
/* .line 306 */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMaxValue:F */
/* .line 307 */
/* neg-float v0, v0 */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinValue:F */
/* .line 310 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mLastFrameTime:J */
/* .line 315 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mEndListeners = v0;
/* .line 318 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mUpdateListeners = v0;
/* .line 355 */
this.mTarget = p1;
/* .line 356 */
this.mProperty = p2;
/* .line 357 */
v0 = com.android.server.display.animation.DynamicAnimation.ROTATION;
/* if-eq p2, v0, :cond_4 */
v0 = com.android.server.display.animation.DynamicAnimation.ROTATION_X;
/* if-eq p2, v0, :cond_4 */
v0 = com.android.server.display.animation.DynamicAnimation.ROTATION_Y;
/* if-ne p2, v0, :cond_0 */
/* .line 360 */
} // :cond_0
v0 = com.android.server.display.animation.DynamicAnimation.ALPHA;
/* const/high16 v1, 0x3b800000 # 0.00390625f */
/* if-ne p2, v0, :cond_1 */
/* .line 361 */
/* iput v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* .line 362 */
} // :cond_1
v0 = com.android.server.display.animation.DynamicAnimation.SCALE_X;
/* if-eq p2, v0, :cond_3 */
v0 = com.android.server.display.animation.DynamicAnimation.SCALE_Y;
/* if-ne p2, v0, :cond_2 */
/* .line 365 */
} // :cond_2
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* .line 363 */
} // :cond_3
} // :goto_0
/* iput v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* .line 359 */
} // :cond_4
} // :goto_1
/* const v0, 0x3dcccccd # 0.1f */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* .line 367 */
} // :goto_2
return;
} // .end method
private void endAnimationInternal ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "canceled" # Z */
/* .line 671 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
/* .line 672 */
com.android.server.display.animation.AnimationHandler .getInstance ( );
(( com.android.server.display.animation.AnimationHandler ) v1 ).removeCallback ( p0 ); // invoke-virtual {v1, p0}, Lcom/android/server/display/animation/AnimationHandler;->removeCallback(Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;)V
/* .line 673 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mLastFrameTime:J */
/* .line 674 */
/* iput-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 675 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mEndListeners;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 676 */
v1 = this.mEndListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 677 */
v1 = this.mEndListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener; */
/* iget v2, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* iget v3, p0, Lcom/android/server/display/animation/DynamicAnimation;->mVelocity:F */
/* .line 675 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 680 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mEndListeners;
com.android.server.display.animation.DynamicAnimation .removeNullEntries ( v0 );
/* .line 681 */
return;
} // .end method
private Float getPropertyValue ( ) {
/* .locals 2 */
/* .line 707 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
v0 = this.mProperty;
v1 = this.mTarget;
v0 = (( com.android.server.display.animation.FloatPropertyCompat ) v0 ).getValue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/animation/FloatPropertyCompat;->getValue(Ljava/lang/Object;)F
} // .end method
private static void removeEntry ( java.util.ArrayList p0, java.lang.Object p1 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/util/ArrayList<", */
/* "TT;>;TT;)V" */
/* } */
} // .end annotation
/* .line 556 */
/* .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;" */
/* .local p1, "entry":Ljava/lang/Object;, "TT;" */
v0 = (( java.util.ArrayList ) p0 ).indexOf ( p1 ); // invoke-virtual {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 557 */
/* .local v0, "id":I */
/* if-ltz v0, :cond_0 */
/* .line 558 */
int v1 = 0; // const/4 v1, 0x0
(( java.util.ArrayList ) p0 ).set ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 560 */
} // :cond_0
return;
} // .end method
private static void removeNullEntries ( java.util.ArrayList p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/util/ArrayList<", */
/* "TT;>;)V" */
/* } */
} // .end annotation
/* .line 545 */
/* .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;" */
v0 = (( java.util.ArrayList ) p0 ).size ( ); // invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 546 */
(( java.util.ArrayList ) p0 ).get ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 547 */
(( java.util.ArrayList ) p0 ).remove ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 545 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 550 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private void startAnimationInternal ( ) {
/* .locals 3 */
/* .line 611 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
/* if-nez v0, :cond_2 */
/* .line 612 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
/* .line 613 */
/* iget-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mStartValueIsSet:Z */
/* if-nez v0, :cond_0 */
/* .line 614 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/animation/DynamicAnimation;->getPropertyValue()F */
/* iput v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* .line 617 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* iget v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMaxValue:F */
/* cmpl-float v1, v0, v1 */
/* if-gtz v1, :cond_1 */
/* iget v1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinValue:F */
/* cmpg-float v0, v0, v1 */
/* if-ltz v0, :cond_1 */
/* .line 621 */
com.android.server.display.animation.AnimationHandler .getInstance ( );
/* const-wide/16 v1, 0x0 */
(( com.android.server.display.animation.AnimationHandler ) v0 ).addAnimationFrameCallback ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Lcom/android/server/display/animation/AnimationHandler;->addAnimationFrameCallback(Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;J)V
/* .line 618 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Starting value need to be in between min value and max value"; // const-string v1, "Starting value need to be in between min value and max value"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 623 */
} // :cond_2
} // :goto_0
return;
} // .end method
/* # virtual methods */
public com.android.server.display.animation.DynamicAnimation addEndListener ( com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 443 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
v0 = this.mEndListeners;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 444 */
v0 = this.mEndListeners;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 446 */
} // :cond_0
} // .end method
public com.android.server.display.animation.DynamicAnimation addUpdateListener ( com.android.server.display.animation.DynamicAnimation$OnAnimationUpdateListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 471 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
v0 = (( com.android.server.display.animation.DynamicAnimation ) p0 ).isRunning ( ); // invoke-virtual {p0}, Lcom/android/server/display/animation/DynamicAnimation;->isRunning()Z
/* if-nez v0, :cond_1 */
/* .line 477 */
v0 = this.mUpdateListeners;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 478 */
v0 = this.mUpdateListeners;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 480 */
} // :cond_0
/* .line 474 */
} // :cond_1
/* new-instance v0, Ljava/lang/UnsupportedOperationException; */
final String v1 = "Error: Update listeners must be added beforethe animation."; // const-string v1, "Error: Update listeners must be added beforethe animation."
/* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void cancel ( ) {
/* .locals 1 */
/* .line 592 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 593 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/animation/DynamicAnimation;->endAnimationInternal(Z)V */
/* .line 595 */
} // :cond_0
return;
} // .end method
public Boolean doAnimationFrame ( Long p0 ) {
/* .locals 6 */
/* .param p1, "frameTime" # J */
/* .line 636 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget-wide v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mLastFrameTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 638 */
/* iput-wide p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mLastFrameTime:J */
/* .line 639 */
/* iget v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
(( com.android.server.display.animation.DynamicAnimation ) p0 ).setPropertyValue ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/animation/DynamicAnimation;->setPropertyValue(F)V
/* .line 640 */
/* .line 642 */
} // :cond_0
/* sub-long v0, p1, v0 */
/* .line 643 */
/* .local v0, "deltaT":J */
/* iput-wide p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mLastFrameTime:J */
/* .line 644 */
v2 = (( com.android.server.display.animation.DynamicAnimation ) p0 ).updateValueAndVelocity ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/display/animation/DynamicAnimation;->updateValueAndVelocity(J)Z
/* .line 646 */
/* .local v2, "finished":Z */
/* iget v4, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* iget v5, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMaxValue:F */
v4 = java.lang.Math .min ( v4,v5 );
/* iput v4, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* .line 647 */
/* iget v5, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinValue:F */
v4 = java.lang.Math .max ( v4,v5 );
/* iput v4, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* .line 649 */
(( com.android.server.display.animation.DynamicAnimation ) p0 ).setPropertyValue ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/display/animation/DynamicAnimation;->setPropertyValue(F)V
/* .line 651 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 652 */
/* invoke-direct {p0, v3}, Lcom/android/server/display/animation/DynamicAnimation;->endAnimationInternal(Z)V */
/* .line 654 */
} // :cond_1
} // .end method
abstract Float getAcceleration ( Float p0, Float p1 ) {
} // .end method
public Float getMinimumVisibleChange ( ) {
/* .locals 1 */
/* .line 537 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
} // .end method
Float getValueThreshold ( ) {
/* .locals 2 */
/* .line 700 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* const/high16 v1, 0x3f400000 # 0.75f */
/* mul-float/2addr v0, v1 */
} // .end method
abstract Boolean isAtEquilibrium ( Float p0, Float p1 ) {
} // .end method
public Boolean isRunning ( ) {
/* .locals 1 */
/* .line 603 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
} // .end method
public void removeEndListener ( com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener; */
/* .line 455 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
v0 = this.mEndListeners;
com.android.server.display.animation.DynamicAnimation .removeEntry ( v0,p1 );
/* .line 456 */
return;
} // .end method
public void removeUpdateListener ( com.android.server.display.animation.DynamicAnimation$OnAnimationUpdateListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener; */
/* .line 490 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
v0 = this.mUpdateListeners;
com.android.server.display.animation.DynamicAnimation .removeEntry ( v0,p1 );
/* .line 491 */
return;
} // .end method
public com.android.server.display.animation.DynamicAnimation setMaxValue ( Float p0 ) {
/* .locals 0 */
/* .param p1, "max" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 418 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMaxValue:F */
/* .line 419 */
} // .end method
public com.android.server.display.animation.DynamicAnimation setMinValue ( Float p0 ) {
/* .locals 0 */
/* .param p1, "min" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 431 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinValue:F */
/* .line 432 */
} // .end method
public com.android.server.display.animation.DynamicAnimation setMinimumVisibleChange ( Float p0 ) {
/* .locals 2 */
/* .param p1, "minimumVisibleChange" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 522 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 525 */
/* iput p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mMinVisibleChange:F */
/* .line 526 */
/* const/high16 v0, 0x3f400000 # 0.75f */
/* mul-float/2addr v0, p1 */
(( com.android.server.display.animation.DynamicAnimation ) p0 ).setValueThreshold ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/animation/DynamicAnimation;->setValueThreshold(F)V
/* .line 527 */
/* .line 523 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Minimum visible change must be positive."; // const-string v1, "Minimum visible change must be positive."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
void setPropertyValue ( Float p0 ) {
/* .locals 4 */
/* .param p1, "value" # F */
/* .line 687 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
v0 = this.mProperty;
v1 = this.mTarget;
(( com.android.server.display.animation.FloatPropertyCompat ) v0 ).setValue ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/display/animation/FloatPropertyCompat;->setValue(Ljava/lang/Object;F)V
/* .line 688 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mUpdateListeners;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 689 */
v1 = this.mUpdateListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 690 */
v1 = this.mUpdateListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener; */
/* iget v2, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* iget v3, p0, Lcom/android/server/display/animation/DynamicAnimation;->mVelocity:F */
/* .line 688 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 693 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mUpdateListeners;
com.android.server.display.animation.DynamicAnimation .removeNullEntries ( v0 );
/* .line 694 */
return;
} // .end method
public com.android.server.display.animation.DynamicAnimation setStartValue ( Float p0 ) {
/* .locals 1 */
/* .param p1, "startValue" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 377 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mValue:F */
/* .line 378 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 379 */
} // .end method
public com.android.server.display.animation.DynamicAnimation setStartVelocity ( Float p0 ) {
/* .locals 0 */
/* .param p1, "startVelocity" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 401 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lcom/android/server/display/animation/DynamicAnimation;->mVelocity:F */
/* .line 402 */
} // .end method
abstract void setValueThreshold ( Float p0 ) {
} // .end method
public void start ( ) {
/* .locals 1 */
/* .line 577 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation;, "Lcom/android/server/display/animation/DynamicAnimation<TT;>;" */
/* iget-boolean v0, p0, Lcom/android/server/display/animation/DynamicAnimation;->mRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 578 */
/* invoke-direct {p0}, Lcom/android/server/display/animation/DynamicAnimation;->startAnimationInternal()V */
/* .line 580 */
} // :cond_0
return;
} // .end method
abstract Boolean updateValueAndVelocity ( Long p0 ) {
} // .end method
