class com.android.server.display.animation.AnimationHandler$FrameCallbackProvider16 extends com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "FrameCallbackProvider16" */
} // .end annotation
/* # instance fields */
private final android.view.Choreographer mChoreographer;
private final android.view.Choreographer$FrameCallback mChoreographerCallback;
/* # direct methods */
 com.android.server.display.animation.AnimationHandler$FrameCallbackProvider16 ( ) {
/* .locals 1 */
/* .param p1, "dispatcher" # Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher; */
/* .line 200 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;-><init>(Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;)V */
/* .line 196 */
android.view.Choreographer .getInstance ( );
this.mChoreographer = v0;
/* .line 201 */
/* new-instance v0, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider16$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider16$1;-><init>(Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider16;)V */
this.mChoreographerCallback = v0;
/* .line 207 */
return;
} // .end method
/* # virtual methods */
void postFrameCallback ( ) {
/* .locals 2 */
/* .line 211 */
v0 = this.mChoreographer;
v1 = this.mChoreographerCallback;
(( android.view.Choreographer ) v0 ).postFrameCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V
/* .line 212 */
return;
} // .end method
