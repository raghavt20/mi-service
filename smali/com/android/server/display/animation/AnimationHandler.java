class com.android.server.display.animation.AnimationHandler {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;, */
	 /* Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;, */
	 /* Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider16;, */
	 /* Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14;, */
	 /* Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long FRAME_DELAY_MS;
public static final java.lang.ThreadLocal sAnimatorHandler;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ThreadLocal<", */
/* "Lcom/android/server/display/animation/AnimationHandler;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
final java.util.ArrayList mAnimationCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.display.animation.AnimationHandler$AnimationCallbackDispatcher mCallbackDispatcher;
Long mCurrentFrameTime;
private final android.util.ArrayMap mDelayedCallbackStartTime;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mListDirty;
private com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider mProvider;
/* # direct methods */
static com.android.server.display.animation.AnimationHandler ( ) {
/* .locals 1 */
/* .line 67 */
/* new-instance v0, Ljava/lang/ThreadLocal; */
/* invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V */
return;
} // .end method
 com.android.server.display.animation.AnimationHandler ( ) {
/* .locals 2 */
/* .line 38 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 73 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mDelayedCallbackStartTime = v0;
/* .line 75 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAnimationCallbacks = v0;
/* .line 77 */
/* new-instance v0, Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;-><init>(Lcom/android/server/display/animation/AnimationHandler;)V */
this.mCallbackDispatcher = v0;
/* .line 81 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/display/animation/AnimationHandler;->mCurrentFrameTime:J */
/* .line 83 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/animation/AnimationHandler;->mListDirty:Z */
return;
} // .end method
private void cleanUpList ( ) {
/* .locals 2 */
/* .line 181 */
/* iget-boolean v0, p0, Lcom/android/server/display/animation/AnimationHandler;->mListDirty:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 182 */
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 183 */
v1 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 184 */
v1 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 182 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 187 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/animation/AnimationHandler;->mListDirty:Z */
/* .line 189 */
} // :cond_2
return;
} // .end method
public static Long getFrameTime ( ) {
/* .locals 2 */
/* .line 93 */
v0 = com.android.server.display.animation.AnimationHandler.sAnimatorHandler;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 94 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
/* .line 96 */
} // :cond_0
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/display/animation/AnimationHandler; */
/* iget-wide v0, v0, Lcom/android/server/display/animation/AnimationHandler;->mCurrentFrameTime:J */
/* return-wide v0 */
} // .end method
public static com.android.server.display.animation.AnimationHandler getInstance ( ) {
/* .locals 2 */
/* .line 86 */
v0 = com.android.server.display.animation.AnimationHandler.sAnimatorHandler;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 87 */
/* new-instance v1, Lcom/android/server/display/animation/AnimationHandler; */
/* invoke-direct {v1}, Lcom/android/server/display/animation/AnimationHandler;-><init>()V */
(( java.lang.ThreadLocal ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
/* .line 89 */
} // :cond_0
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/display/animation/AnimationHandler; */
} // .end method
private Boolean isCallbackDue ( com.android.server.display.animation.AnimationHandler$AnimationFrameCallback p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "callback" # Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback; */
/* .param p2, "currentTime" # J */
/* .line 169 */
v0 = this.mDelayedCallbackStartTime;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
/* .line 170 */
/* .local v0, "startTime":Ljava/lang/Long; */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 171 */
/* .line 173 */
} // :cond_0
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* cmp-long v2, v2, p2 */
/* if-gez v2, :cond_1 */
/* .line 174 */
v2 = this.mDelayedCallbackStartTime;
(( android.util.ArrayMap ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 175 */
/* .line 177 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
/* # virtual methods */
public void addAnimationFrameCallback ( com.android.server.display.animation.AnimationHandler$AnimationFrameCallback p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback; */
/* .param p2, "delay" # J */
/* .line 123 */
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-nez v0, :cond_0 */
/* .line 124 */
(( com.android.server.display.animation.AnimationHandler ) p0 ).getProvider ( ); // invoke-virtual {p0}, Lcom/android/server/display/animation/AnimationHandler;->getProvider()Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;
(( com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider ) v0 ).postFrameCallback ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;->postFrameCallback()V
/* .line 126 */
} // :cond_0
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 127 */
v0 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 130 */
} // :cond_1
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p2, v0 */
/* if-lez v0, :cond_2 */
/* .line 131 */
v0 = this.mDelayedCallbackStartTime;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* add-long/2addr v1, p2 */
java.lang.Long .valueOf ( v1,v2 );
(( android.util.ArrayMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 133 */
} // :cond_2
return;
} // .end method
void doAnimationFrame ( Long p0 ) {
/* .locals 5 */
/* .param p1, "frameTime" # J */
/* .line 149 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 150 */
/* .local v0, "currentTime":J */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mAnimationCallbacks;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 151 */
v3 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback; */
/* .line 152 */
/* .local v3, "callback":Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback; */
/* if-nez v3, :cond_0 */
/* .line 153 */
/* .line 155 */
} // :cond_0
v4 = /* invoke-direct {p0, v3, v0, v1}, Lcom/android/server/display/animation/AnimationHandler;->isCallbackDue(Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;J)Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 156 */
/* .line 150 */
} // .end local v3 # "callback":Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback;
} // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 159 */
} // .end local v2 # "i":I
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/display/animation/AnimationHandler;->cleanUpList()V */
/* .line 160 */
return;
} // .end method
com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider getProvider ( ) {
/* .locals 2 */
/* .line 109 */
v0 = this.mProvider;
/* if-nez v0, :cond_0 */
/* .line 110 */
/* nop */
/* .line 111 */
/* new-instance v0, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider16; */
v1 = this.mCallbackDispatcher;
/* invoke-direct {v0, v1}, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider16;-><init>(Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;)V */
this.mProvider = v0;
/* .line 116 */
} // :cond_0
v0 = this.mProvider;
} // .end method
public void removeCallback ( com.android.server.display.animation.AnimationHandler$AnimationFrameCallback p0 ) {
/* .locals 3 */
/* .param p1, "callback" # Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallback; */
/* .line 139 */
v0 = this.mDelayedCallbackStartTime;
(( android.util.ArrayMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 140 */
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).indexOf ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 141 */
/* .local v0, "id":I */
/* if-ltz v0, :cond_0 */
/* .line 142 */
v1 = this.mAnimationCallbacks;
int v2 = 0; // const/4 v2, 0x0
(( java.util.ArrayList ) v1 ).set ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 143 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/display/animation/AnimationHandler;->mListDirty:Z */
/* .line 145 */
} // :cond_0
return;
} // .end method
public void setProvider ( com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider p0 ) {
/* .locals 0 */
/* .param p1, "provider" # Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider; */
/* .line 104 */
this.mProvider = p1;
/* .line 105 */
return;
} // .end method
