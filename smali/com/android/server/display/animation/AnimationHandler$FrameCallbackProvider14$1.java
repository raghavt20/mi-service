class com.android.server.display.animation.AnimationHandler$FrameCallbackProvider14$1 implements java.lang.Runnable {
	 /* .source "AnimationHandler.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14;-><init>(Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.animation.AnimationHandler$FrameCallbackProvider14 this$0; //synthetic
/* # direct methods */
 com.android.server.display.animation.AnimationHandler$FrameCallbackProvider14$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14; */
/* .line 227 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 230 */
v0 = this.this$0;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14;->mLastFrameTime:J */
/* .line 231 */
v0 = this.this$0;
v0 = this.mDispatcher;
(( com.android.server.display.animation.AnimationHandler$AnimationCallbackDispatcher ) v0 ).dispatchAnimationFrame ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;->dispatchAnimationFrame()V
/* .line 232 */
return;
} // .end method
