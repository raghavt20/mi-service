class com.android.server.display.animation.AnimationHandler$FrameCallbackProvider14 extends com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "FrameCallbackProvider14" */
} // .end annotation
/* # instance fields */
private final android.os.Handler mHandler;
Long mLastFrameTime;
private final java.lang.Runnable mRunnable;
/* # direct methods */
 com.android.server.display.animation.AnimationHandler$FrameCallbackProvider14 ( ) {
/* .locals 2 */
/* .param p1, "dispatcher" # Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher; */
/* .line 226 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;-><init>(Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher;)V */
/* .line 223 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14;->mLastFrameTime:J */
/* .line 227 */
/* new-instance v0, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14$1;-><init>(Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14;)V */
this.mRunnable = v0;
/* .line 234 */
/* new-instance v0, Landroid/os/Handler; */
android.os.Looper .myLooper ( );
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 235 */
return;
} // .end method
/* # virtual methods */
void postFrameCallback ( ) {
/* .locals 4 */
/* .line 239 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/display/animation/AnimationHandler$FrameCallbackProvider14;->mLastFrameTime:J */
/* sub-long/2addr v0, v2 */
/* const-wide/16 v2, 0xa */
/* sub-long/2addr v2, v0 */
/* .line 240 */
/* .local v2, "delay":J */
/* const-wide/16 v0, 0x0 */
java.lang.Math .max ( v2,v3,v0,v1 );
/* move-result-wide v0 */
/* .line 241 */
} // .end local v2 # "delay":J
/* .local v0, "delay":J */
v2 = this.mHandler;
v3 = this.mRunnable;
(( android.os.Handler ) v2 ).postDelayed ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 242 */
return;
} // .end method
