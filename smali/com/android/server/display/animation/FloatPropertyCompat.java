public abstract class com.android.server.display.animation.FloatPropertyCompat {
	 /* .source "FloatPropertyCompat.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # instance fields */
final java.lang.String mPropertyName;
/* # direct methods */
public com.android.server.display.animation.FloatPropertyCompat ( ) {
	 /* .locals 0 */
	 /* .param p1, "name" # Ljava/lang/String; */
	 /* .line 40 */
	 /* .local p0, "this":Lcom/android/server/display/animation/FloatPropertyCompat;, "Lcom/android/server/display/animation/FloatPropertyCompat<TT;>;" */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 41 */
	 this.mPropertyName = p1;
	 /* .line 42 */
	 return;
} // .end method
public static com.android.server.display.animation.FloatPropertyCompat createFloatPropertyCompat ( android.util.FloatProperty p0 ) {
	 /* .locals 2 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">(", */
	 /* "Landroid/util/FloatProperty<", */
	 /* "TT;>;)", */
	 /* "Lcom/android/server/display/animation/FloatPropertyCompat<", */
	 /* "TT;>;" */
	 /* } */
} // .end annotation
/* .line 55 */
/* .local p0, "property":Landroid/util/FloatProperty;, "Landroid/util/FloatProperty<TT;>;" */
/* new-instance v0, Lcom/android/server/display/animation/FloatPropertyCompat$1; */
(( android.util.FloatProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Landroid/util/FloatProperty;->getName()Ljava/lang/String;
/* invoke-direct {v0, v1, p0}, Lcom/android/server/display/animation/FloatPropertyCompat$1;-><init>(Ljava/lang/String;Landroid/util/FloatProperty;)V */
} // .end method
/* # virtual methods */
public abstract Float getValue ( java.lang.Object p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)F" */
/* } */
} // .end annotation
} // .end method
public abstract void setValue ( java.lang.Object p0, Float p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;F)V" */
/* } */
} // .end annotation
} // .end method
