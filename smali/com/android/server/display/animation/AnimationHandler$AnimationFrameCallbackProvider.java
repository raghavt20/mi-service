abstract class com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x408 */
/* name = "AnimationFrameCallbackProvider" */
} // .end annotation
/* # instance fields */
final com.android.server.display.animation.AnimationHandler$AnimationCallbackDispatcher mDispatcher;
/* # direct methods */
 com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider ( ) {
/* .locals 0 */
/* .param p1, "dispatcher" # Lcom/android/server/display/animation/AnimationHandler$AnimationCallbackDispatcher; */
/* .line 253 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 254 */
this.mDispatcher = p1;
/* .line 255 */
return;
} // .end method
/* # virtual methods */
abstract void postFrameCallback ( ) {
} // .end method
