class com.android.server.display.animation.DynamicAnimation$2 extends com.android.server.display.animation.DynamicAnimation$ViewProperty {
	 /* .source "DynamicAnimation.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/DynamicAnimation; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.android.server.display.animation.DynamicAnimation$2 ( ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 70 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;-><init>(Ljava/lang/String;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty-IA;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 78 */
v0 = (( android.view.View ) p1 ).getTranslationY ( ); // invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 70 */
/* check-cast p1, Landroid/view/View; */
p1 = (( com.android.server.display.animation.DynamicAnimation$2 ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/display/animation/DynamicAnimation$2;->getValue(Landroid/view/View;)F
} // .end method
public void setValue ( android.view.View p0, Float p1 ) {
/* .locals 0 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "value" # F */
/* .line 73 */
(( android.view.View ) p1 ).setTranslationY ( p2 ); // invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V
/* .line 74 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 70 */
/* check-cast p1, Landroid/view/View; */
(( com.android.server.display.animation.DynamicAnimation$2 ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/animation/DynamicAnimation$2;->setValue(Landroid/view/View;F)V
return;
} // .end method
