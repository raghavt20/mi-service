class com.android.server.display.animation.DynamicAnimation$13 extends com.android.server.display.animation.DynamicAnimation$ViewProperty {
	 /* .source "DynamicAnimation.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/DynamicAnimation; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.android.server.display.animation.DynamicAnimation$13 ( ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 236 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;-><init>(Ljava/lang/String;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty-IA;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 244 */
v0 = (( android.view.View ) p1 ).getScrollX ( ); // invoke-virtual {p1}, Landroid/view/View;->getScrollX()I
/* int-to-float v0, v0 */
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 236 */
/* check-cast p1, Landroid/view/View; */
p1 = (( com.android.server.display.animation.DynamicAnimation$13 ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/display/animation/DynamicAnimation$13;->getValue(Landroid/view/View;)F
} // .end method
public void setValue ( android.view.View p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "value" # F */
/* .line 239 */
/* float-to-int v0, p2 */
(( android.view.View ) p1 ).setScrollX ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setScrollX(I)V
/* .line 240 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 236 */
/* check-cast p1, Landroid/view/View; */
(( com.android.server.display.animation.DynamicAnimation$13 ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/animation/DynamicAnimation$13;->setValue(Landroid/view/View;F)V
return;
} // .end method
