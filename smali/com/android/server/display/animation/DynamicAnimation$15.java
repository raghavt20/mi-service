class com.android.server.display.animation.DynamicAnimation$15 extends com.android.server.display.animation.FloatPropertyCompat {
	 /* .source "DynamicAnimation.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/animation/DynamicAnimation;-><init>(Lcom/android/server/display/animation/FloatValueHolder;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.animation.DynamicAnimation this$0; //synthetic
final com.android.server.display.animation.FloatValueHolder val$floatValueHolder; //synthetic
/* # direct methods */
 com.android.server.display.animation.DynamicAnimation$15 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/animation/DynamicAnimation; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 333 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation$15;, "Lcom/android/server/display/animation/DynamicAnimation$15;" */
this.this$0 = p1;
this.val$floatValueHolder = p3;
/* invoke-direct {p0, p2}, Lcom/android/server/display/animation/FloatPropertyCompat;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "object" # Ljava/lang/Object; */
/* .line 336 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation$15;, "Lcom/android/server/display/animation/DynamicAnimation$15;" */
v0 = this.val$floatValueHolder;
v0 = (( com.android.server.display.animation.FloatValueHolder ) v0 ).getValue ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/FloatValueHolder;->getValue()F
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "object" # Ljava/lang/Object; */
/* .param p2, "value" # F */
/* .line 341 */
/* .local p0, "this":Lcom/android/server/display/animation/DynamicAnimation$15;, "Lcom/android/server/display/animation/DynamicAnimation$15;" */
v0 = this.val$floatValueHolder;
(( com.android.server.display.animation.FloatValueHolder ) v0 ).setValue ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/display/animation/FloatValueHolder;->setValue(F)V
/* .line 342 */
return;
} // .end method
