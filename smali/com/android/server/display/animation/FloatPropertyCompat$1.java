class com.android.server.display.animation.FloatPropertyCompat$1 extends com.android.server.display.animation.FloatPropertyCompat {
	 /* .source "FloatPropertyCompat.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/animation/FloatPropertyCompat;->createFloatPropertyCompat(Landroid/util/FloatProperty;)Lcom/android/server/display/animation/FloatPropertyCompat; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/android/server/display/animation/FloatPropertyCompat<", */
/* "TT;>;" */
/* } */
} // .end annotation
/* # instance fields */
final android.util.FloatProperty val$property; //synthetic
/* # direct methods */
 com.android.server.display.animation.FloatPropertyCompat$1 ( ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 55 */
this.val$property = p2;
/* invoke-direct {p0, p1}, Lcom/android/server/display/animation/FloatPropertyCompat;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( java.lang.Object p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)F" */
/* } */
} // .end annotation
/* .line 58 */
/* .local p1, "object":Ljava/lang/Object;, "TT;" */
v0 = this.val$property;
(( android.util.FloatProperty ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/FloatProperty;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) {
/* .locals 1 */
/* .param p2, "value" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;F)V" */
/* } */
} // .end annotation
/* .line 63 */
/* .local p1, "object":Ljava/lang/Object;, "TT;" */
v0 = this.val$property;
(( android.util.FloatProperty ) v0 ).setValue ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/FloatProperty;->setValue(Ljava/lang/Object;F)V
/* .line 64 */
return;
} // .end method
