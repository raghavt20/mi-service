class inal implements com.android.server.display.animation.Force {
	 /* .source "FlingAnimation.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/FlingAnimation; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x18 */
/* name = "DragForce" */
} // .end annotation
/* # static fields */
private static final Float DEFAULT_FRICTION;
private static final Float VELOCITY_THRESHOLD_MULTIPLIER;
/* # instance fields */
private Float mFriction;
private final com.android.server.display.animation.DynamicAnimation$MassState mMassState;
private Float mVelocityThreshold;
/* # direct methods */
 inal ( ) {
/* .locals 1 */
/* .line 194 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 202 */
/* const v0, -0x3f79999a # -4.2f */
/* iput v0, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mFriction:F */
/* .line 206 */
/* new-instance v0, Lcom/android/server/display/animation/DynamicAnimation$MassState; */
/* invoke-direct {v0}, Lcom/android/server/display/animation/DynamicAnimation$MassState;-><init>()V */
this.mMassState = v0;
return;
} // .end method
/* # virtual methods */
public Float getAcceleration ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "position" # F */
/* .param p2, "velocity" # F */
/* .line 228 */
/* iget v0, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mFriction:F */
/* mul-float/2addr v0, p2 */
} // .end method
Float getFrictionScalar ( ) {
/* .locals 2 */
/* .line 213 */
/* iget v0, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mFriction:F */
/* const v1, -0x3f79999a # -4.2f */
/* div-float/2addr v0, v1 */
} // .end method
public Boolean isAtEquilibrium ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "value" # F */
/* .param p2, "velocity" # F */
/* .line 233 */
v0 = java.lang.Math .abs ( p2 );
/* iget v1, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mVelocityThreshold:F */
/* cmpg-float v0, v0, v1 */
/* if-gez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
void setFrictionScalar ( Float p0 ) {
/* .locals 1 */
/* .param p1, "frictionScalar" # F */
/* .line 209 */
/* const v0, -0x3f79999a # -4.2f */
/* mul-float/2addr v0, p1 */
/* iput v0, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mFriction:F */
/* .line 210 */
return;
} // .end method
void setValueThreshold ( Float p0 ) {
/* .locals 1 */
/* .param p1, "threshold" # F */
/* .line 237 */
/* const/high16 v0, 0x427a0000 # 62.5f */
/* mul-float/2addr v0, p1 */
/* iput v0, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mVelocityThreshold:F */
/* .line 238 */
return;
} // .end method
com.android.server.display.animation.DynamicAnimation$MassState updateValueAndVelocity ( Float p0, Float p1, Long p2 ) {
/* .locals 9 */
/* .param p1, "value" # F */
/* .param p2, "velocity" # F */
/* .param p3, "deltaT" # J */
/* .line 217 */
v0 = this.mMassState;
/* float-to-double v1, p2 */
/* long-to-float v3, p3 */
/* const/high16 v4, 0x447a0000 # 1000.0f */
/* div-float/2addr v3, v4 */
/* iget v5, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mFriction:F */
/* mul-float/2addr v3, v5 */
/* float-to-double v5, v3 */
java.lang.Math .exp ( v5,v6 );
/* move-result-wide v5 */
/* mul-double/2addr v1, v5 */
/* double-to-float v1, v1 */
/* iput v1, v0, Lcom/android/server/display/animation/DynamicAnimation$MassState;->mVelocity:F */
/* .line 218 */
v0 = this.mMassState;
/* iget v1, p0, Lcom/android/server/display/animation/FlingAnimation$DragForce;->mFriction:F */
/* div-float v2, p2, v1 */
/* sub-float v2, p1, v2 */
/* float-to-double v2, v2 */
/* div-float v5, p2, v1 */
/* float-to-double v5, v5 */
/* long-to-float v7, p3 */
/* mul-float/2addr v1, v7 */
/* div-float/2addr v1, v4 */
/* float-to-double v7, v1 */
/* .line 219 */
java.lang.Math .exp ( v7,v8 );
/* move-result-wide v7 */
/* mul-double/2addr v5, v7 */
/* add-double/2addr v2, v5 */
/* double-to-float v1, v2 */
/* iput v1, v0, Lcom/android/server/display/animation/DynamicAnimation$MassState;->mValue:F */
/* .line 220 */
v0 = this.mMassState;
/* iget v0, v0, Lcom/android/server/display/animation/DynamicAnimation$MassState;->mValue:F */
v1 = this.mMassState;
/* iget v1, v1, Lcom/android/server/display/animation/DynamicAnimation$MassState;->mVelocity:F */
v0 = (( com.android.server.display.animation.FlingAnimation$DragForce ) p0 ).isAtEquilibrium ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/display/animation/FlingAnimation$DragForce;->isAtEquilibrium(FF)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 221 */
v0 = this.mMassState;
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lcom/android/server/display/animation/DynamicAnimation$MassState;->mVelocity:F */
/* .line 223 */
} // :cond_0
v0 = this.mMassState;
} // .end method
