class com.android.server.display.animation.AnimationHandler$AnimationCallbackDispatcher {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/animation/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "AnimationCallbackDispatcher" */
} // .end annotation
/* # instance fields */
final com.android.server.display.animation.AnimationHandler this$0; //synthetic
/* # direct methods */
 com.android.server.display.animation.AnimationHandler$AnimationCallbackDispatcher ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/animation/AnimationHandler; */
/* .line 56 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
void dispatchAnimationFrame ( ) {
/* .locals 3 */
/* .line 58 */
v0 = this.this$0;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/display/animation/AnimationHandler;->mCurrentFrameTime:J */
/* .line 59 */
v0 = this.this$0;
/* iget-wide v1, v0, Lcom/android/server/display/animation/AnimationHandler;->mCurrentFrameTime:J */
(( com.android.server.display.animation.AnimationHandler ) v0 ).doAnimationFrame ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/animation/AnimationHandler;->doAnimationFrame(J)V
/* .line 60 */
v0 = this.this$0;
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v0, :cond_0 */
/* .line 61 */
v0 = this.this$0;
(( com.android.server.display.animation.AnimationHandler ) v0 ).getProvider ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/AnimationHandler;->getProvider()Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;
(( com.android.server.display.animation.AnimationHandler$AnimationFrameCallbackProvider ) v0 ).postFrameCallback ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/AnimationHandler$AnimationFrameCallbackProvider;->postFrameCallback()V
/* .line 63 */
} // :cond_0
return;
} // .end method
