class com.android.server.display.DisplayFeatureManagerService$DisplayFeatureHandler extends android.os.Handler {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DisplayFeatureHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.display.DisplayFeatureManagerService$DisplayFeatureHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1170 */
this.this$0 = p1;
/* .line 1171 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1172 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1176 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 1177 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const/high16 v1, 0x3f800000 # 1.0f */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_0 */
/* .line 1252 */
/* :pswitch_1 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msendMuraState ( v0,v1 );
/* goto/16 :goto_0 */
/* .line 1224 */
/* :pswitch_2 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msendHbmState ( v0,v1 );
/* .line 1225 */
/* goto/16 :goto_0 */
/* .line 1227 */
/* :pswitch_3 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetSDR2HDR ( v0,v1 );
/* .line 1228 */
/* goto/16 :goto_0 */
/* .line 1231 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmResolver ( v0 );
final String v1 = "screen_game_mode"; // const-string v1, "screen_game_mode"
android.provider.Settings$System .putInt ( v0,v1,v2 );
/* .line 1232 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mloadSettings ( v0 );
/* .line 1233 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetScreenEffectAll ( v0,v3 );
/* .line 1234 */
/* goto/16 :goto_0 */
/* .line 1244 */
/* :pswitch_5 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
int v4 = 2; // const/4 v4, 0x2
/* if-ne v1, v4, :cond_0 */
/* move v2, v3 */
} // :cond_0
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fputmDolbyState ( v0,v2 );
/* .line 1246 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = com.android.server.display.DisplayFeatureManagerService.BRIGHTNESS_THROTTLER_STATUS;
/* .line 1248 */
v2 = this.this$0;
v2 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmDolbyState ( v2 );
/* .line 1246 */
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .putIntForUser ( v0,v1,v2,v3 );
/* .line 1249 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifyHdrStateChanged ( v0 );
/* .line 1250 */
/* goto/16 :goto_0 */
/* .line 1221 */
/* :pswitch_6 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleTrueToneModeChange ( v0 );
/* .line 1222 */
/* goto/16 :goto_0 */
/* .line 1182 */
/* :pswitch_7 */
v0 = this.this$0;
v1 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeEnabled ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.this$0;
v2 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
} // :cond_1
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetPaperColors ( v0,v2 );
/* .line 1183 */
/* goto/16 :goto_0 */
/* .line 1218 */
/* :pswitch_8 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifySFSecondaryFrameRate ( v0,v1 );
/* .line 1219 */
/* goto/16 :goto_0 */
/* .line 1215 */
/* :pswitch_9 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifySFDCParseState ( v0,v1 );
/* .line 1216 */
/* goto/16 :goto_0 */
/* .line 1209 */
/* :pswitch_a */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifySFColorMode ( v0,v1 );
/* .line 1210 */
/* goto/16 :goto_0 */
/* .line 1212 */
/* :pswitch_b */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetExpertScreenMode ( v0 );
/* .line 1213 */
/* goto/16 :goto_0 */
/* .line 1192 */
/* :pswitch_c */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->what:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifySFDfpsMode ( v0,v1,v2 );
/* .line 1193 */
/* goto/16 :goto_0 */
/* .line 1203 */
/* :pswitch_d */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmContext ( v0 );
v2 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmContext ( v2 );
v2 = com.android.server.display.DisplayFeatureManagerService .-$$Nest$misDarkModeEnable ( v2,v3 );
com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetDarkModeEnable ( v0,v1,v2 );
/* .line 1204 */
/* goto/16 :goto_0 */
/* .line 1206 */
/* :pswitch_e */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleUnlimitedColorLevelChange ( v0,v1 );
/* .line 1207 */
/* .line 1195 */
/* :pswitch_f */
v0 = this.obj;
/* check-cast v0, Lcom/android/internal/os/SomeArgs; */
/* .line 1196 */
/* .local v0, "args":Lcom/android/internal/os/SomeArgs; */
v1 = this.arg1;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* .line 1197 */
/* .local v1, "red":F */
v2 = this.arg2;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* .line 1198 */
/* .local v2, "green":F */
v3 = this.arg3;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* .line 1199 */
/* .local v3, "blue":F */
v4 = this.this$0;
/* iget v5, p1, Landroid/os/Message;->arg1:I */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifySFPccLevel ( v4,v5,v1,v2,v3 );
/* .line 1200 */
(( com.android.internal.os.SomeArgs ) v0 ).recycle ( ); // invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V
/* .line 1201 */
/* .line 1188 */
} // .end local v0 # "args":Lcom/android/internal/os/SomeArgs;
} // .end local v1 # "red":F
} // .end local v2 # "green":F
} // .end local v3 # "blue":F
/* :pswitch_10 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* if-ne v1, v3, :cond_2 */
/* move v2, v3 */
} // :cond_2
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifySFWcgState ( v0,v2 );
/* .line 1189 */
/* .line 1240 */
/* :pswitch_11 */
v0 = this.this$0;
/* iget v2, p1, Landroid/os/Message;->arg1:I */
/* int-to-float v2, v2 */
/* mul-float/2addr v2, v1 */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fputmCurrentGrayScale ( v0,v2 );
/* .line 1241 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifyCurrentGrayScaleChanged ( v0 );
/* .line 1242 */
/* .line 1236 */
/* :pswitch_12 */
v0 = this.this$0;
/* iget v2, p1, Landroid/os/Message;->arg1:I */
/* int-to-float v2, v2 */
/* mul-float/2addr v2, v1 */
/* const/high16 v1, 0x437f0000 # 255.0f */
/* div-float/2addr v2, v1 */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fputmGrayScale ( v0,v2 );
/* .line 1237 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mnotifyGrayScaleChanged ( v0 );
/* .line 1238 */
/* .line 1185 */
/* :pswitch_13 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleScreenSchemeChange ( v0,v1 );
/* .line 1186 */
/* .line 1179 */
/* :pswitch_14 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleReadingModeChange ( v0,v1 );
/* .line 1180 */
/* nop */
/* .line 1255 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_0 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_0 */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_c */
/* :pswitch_0 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
