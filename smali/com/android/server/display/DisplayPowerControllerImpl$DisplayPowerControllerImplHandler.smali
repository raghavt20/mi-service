.class final Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;
.super Landroid/os/Handler;
.source "DisplayPowerControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayPowerControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DisplayPowerControllerImplHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayPowerControllerImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayPowerControllerImpl;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1518
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 1519
    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p2, p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 1520
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 1524
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 1545
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fputmOrientation(Lcom/android/server/display/DisplayPowerControllerImpl;I)V

    .line 1546
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmCbmController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmAutoBrightnessEnable(Lcom/android/server/display/DisplayPowerControllerImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1547
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmCbmController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmForegroundAppPackageName(Lcom/android/server/display/DisplayPowerControllerImpl;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmOrientation(Lcom/android/server/display/DisplayPowerControllerImpl;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/aiautobrt/CustomBrightnessModeController;->updateCustomSceneState(Ljava/lang/String;I)V

    goto :goto_0

    .line 1529
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "current_gray_scale"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$msetCurrentGrayScale(Lcom/android/server/display/DisplayPowerControllerImpl;F)V

    .line 1530
    goto :goto_0

    .line 1542
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateThermalBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;F)V

    .line 1543
    goto :goto_0

    .line 1535
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateForegroundApp(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 1536
    goto :goto_0

    .line 1532
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateForegroundAppSync(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 1533
    goto :goto_0

    .line 1538
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "dolby_version_state"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mupdateDolbyBrightnessIfNeeded(Lcom/android/server/display/DisplayPowerControllerImpl;Z)V

    .line 1540
    goto :goto_0

    .line 1526
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "gray_scale"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$msetGrayScale(Lcom/android/server/display/DisplayPowerControllerImpl;F)V

    .line 1527
    nop

    .line 1553
    :cond_0
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
