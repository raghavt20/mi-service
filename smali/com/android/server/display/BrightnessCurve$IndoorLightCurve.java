class com.android.server.display.BrightnessCurve$IndoorLightCurve extends com.android.server.display.BrightnessCurve$Curve {
	 /* .source "BrightnessCurve.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/BrightnessCurve; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "IndoorLightCurve" */
} // .end annotation
/* # instance fields */
private final Float mAnchorPointLux;
private final Float mHeadLux;
private final Float mTailLux;
final com.android.server.display.BrightnessCurve this$0; //synthetic
/* # direct methods */
public com.android.server.display.BrightnessCurve$IndoorLightCurve ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/display/BrightnessCurve; */
/* .line 366 */
this.this$0 = p1;
/* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve$Curve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 367 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v1 = 1; // const/4 v1, 0x1
/* aget v0, v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
/* .line 368 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v1 = 2; // const/4 v1, 0x2
/* aget v0, v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
/* .line 369 */
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurveAnchorPointLux ( p1 );
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
/* .line 370 */
return;
} // .end method
private void pullDownConnectLeft ( android.util.Pair p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 433 */
/* .local p1, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v0 = this.mPointList;
/* .line 434 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
v0 = (( android.util.Spline ) v0 ).interpolate ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
/* .line 435 */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v1, v2 */
/* div-float/2addr v0, v1 */
/* .line 436 */
/* .local v0, "tanToAnchorPoint":F */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
v3 = this.first;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v2, v3 */
/* mul-float/2addr v2, v0 */
/* add-float/2addr v1, v2 */
/* .line 437 */
/* .local v1, "tailPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
java.lang.Float .valueOf ( v3 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 438 */
/* .local v2, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v3 = this.mPointList;
/* .line 439 */
return;
} // .end method
private void pullDownConnectRight ( android.util.Pair p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 442 */
/* .local p1, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v0 = this.second;
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
/* sub-float/2addr v1, v2 */
/* div-float/2addr v0, v1 */
/* .line 444 */
/* .local v0, "tanToHead":F */
v1 = this.this$0;
v1 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_0 */
/* .line 445 */
v1 = this.this$0;
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* .line 447 */
} // :cond_0
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
/* sub-float/2addr v2, v3 */
/* mul-float/2addr v2, v0 */
/* sub-float/2addr v1, v2 */
/* .line 448 */
/* .local v1, "headPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
java.lang.Float .valueOf ( v3 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 449 */
/* .local v2, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v3 = this.mPointList;
/* .line 450 */
v3 = this.mPointList;
/* .line 451 */
return;
} // .end method
private void pullUpConnectLeft ( android.util.Pair p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 421 */
/* .local p1, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v0 = this.mPointList;
/* .line 422 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
v0 = (( android.util.Spline ) v0 ).interpolate ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
/* .line 423 */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v1, v2 */
/* div-float/2addr v0, v1 */
/* .line 424 */
/* .local v0, "tanToAnchorPoint":F */
v1 = this.this$0;
v1 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_0 */
/* .line 425 */
v1 = this.this$0;
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* .line 427 */
} // :cond_0
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
v3 = this.first;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v2, v3 */
/* mul-float/2addr v2, v0 */
/* add-float/2addr v1, v2 */
/* .line 428 */
/* .local v1, "tailPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
java.lang.Float .valueOf ( v3 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 429 */
/* .local v2, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v3 = this.mPointList;
/* .line 430 */
return;
} // .end method
/* # virtual methods */
public void connectLeft ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 8 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 391 */
v0 = v0 = this.mPointList;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 392 */
v0 = this.mPointList;
v1 = v1 = this.mPointList;
/* add-int/lit8 v1, v1, -0x1 */
/* check-cast v0, Landroid/util/Pair; */
/* .line 393 */
/* .local v0, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v2 );
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
v2 = (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 394 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 395 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 396 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->pullDownConnectLeft(Landroid/util/Pair;)V */
/* .line 398 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->pullUpConnectLeft(Landroid/util/Pair;)V */
/* .line 401 */
} // .end local v0 # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_2
} // :goto_0
return;
} // .end method
public void connectRight ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 8 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 405 */
v0 = v0 = this.mPointList;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 406 */
v0 = this.mPointList;
int v1 = 0; // const/4 v1, 0x0
/* check-cast v0, Landroid/util/Pair; */
/* .line 407 */
/* .local v0, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v1 = this.second;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v2 );
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
v2 = (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v1, v1, v2 */
/* if-nez v1, :cond_0 */
/* .line 408 */
v2 = this.this$0;
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
v5 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 409 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 410 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->pullDownConnectRight(Landroid/util/Pair;)V */
/* .line 412 */
} // :cond_1
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
/* .line 413 */
/* .local v1, "headPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
java.lang.Float .valueOf ( v3 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 414 */
/* .local v2, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v3 = this.mPointList;
/* .line 415 */
v3 = this.mPointList;
/* .line 418 */
} // .end local v0 # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // .end local v1 # "headPointNit":F
} // .end local v2 # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // :cond_2
} // :goto_0
return;
} // .end method
public void create ( Float p0, Float p1 ) {
/* .locals 8 */
/* .param p1, "lux" # F */
/* .param p2, "changeNit" # F */
/* .line 374 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
v0 = (( android.util.Spline ) v0 ).interpolate ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v0, p2 */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mAnchorPointLux:F */
/* sub-float/2addr v1, p1 */
/* div-float/2addr v0, v1 */
/* .line 376 */
/* .local v0, "tanToAnchorPoint":F */
v1 = this.this$0;
v1 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_0 */
/* .line 377 */
v1 = this.this$0;
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* .line 379 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
/* sub-float v1, p1, v1 */
v2 = this.this$0;
v2 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmIndoorLightCurveTan ( v2 );
/* mul-float/2addr v1, v2 */
/* sub-float v1, p2, v1 */
/* .line 380 */
/* .local v1, "headPointNit":F */
/* new-instance v2, Landroid/util/Pair; */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mHeadLux:F */
java.lang.Float .valueOf ( v3 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 381 */
/* .local v2, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v3 = this.mPointList;
/* .line 382 */
/* new-instance v3, Landroid/util/Pair; */
java.lang.Float .valueOf ( p1 );
java.lang.Float .valueOf ( p2 );
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 383 */
/* .local v3, "changePoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v4 = this.mPointList;
/* .line 384 */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
/* sub-float/2addr v4, p1 */
/* mul-float/2addr v4, v0 */
/* add-float/2addr v4, p2 */
/* .line 385 */
/* .local v4, "tailPointNit":F */
/* new-instance v5, Landroid/util/Pair; */
/* iget v6, p0, Lcom/android/server/display/BrightnessCurve$IndoorLightCurve;->mTailLux:F */
java.lang.Float .valueOf ( v6 );
java.lang.Float .valueOf ( v4 );
/* invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 386 */
/* .local v5, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v6 = this.mPointList;
/* .line 387 */
return;
} // .end method
