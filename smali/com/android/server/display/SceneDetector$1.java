class com.android.server.display.SceneDetector$1 implements android.os.IBinder$DeathRecipient {
	 /* .source "SceneDetector.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SceneDetector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.SceneDetector this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$D-6GL5NhYZgtOrYsKjHuisJpv_U ( com.android.server.display.SceneDetector$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/SceneDetector$1;->lambda$binderDied$0()V */
return;
} // .end method
 com.android.server.display.SceneDetector$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/SceneDetector; */
/* .line 208 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$binderDied$0 ( ) { //synthethic
/* .locals 2 */
/* .line 213 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.SceneDetector .-$$Nest$fputmAonState ( v0,v1 );
/* .line 214 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$mresetServiceConnectedStatus ( v0 );
/* .line 215 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$mtryToBindAonFlareService ( v0 );
/* .line 216 */
final String v0 = "SceneDetector"; // const-string v0, "SceneDetector"
final String v1 = "Process of service has died, try to bind it."; // const-string v1, "Process of service has died, try to bind it."
android.util.Slog .w ( v0,v1 );
/* .line 217 */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 2 */
/* .line 212 */
v0 = this.this$0;
com.android.server.display.SceneDetector .-$$Nest$fgetmSceneDetectorHandler ( v0 );
/* new-instance v1, Lcom/android/server/display/SceneDetector$1$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/SceneDetector$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/SceneDetector$1;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 218 */
return;
} // .end method
