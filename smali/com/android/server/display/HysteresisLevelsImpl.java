public class com.android.server.display.HysteresisLevelsImpl extends com.android.server.display.HysteresisLevelsStub {
	 /* .source "HysteresisLevelsImpl.java" */
	 /* # static fields */
	 private static final Float HBM_MINIMUM_LUX;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.display.HysteresisLevels mAmbientBrightnessThresholds;
	 private com.android.server.display.HighBrightnessModeController mHbmController;
	 private android.util.Spline mHysteresisBrightSpline;
	 private android.util.Spline mHysteresisDarkSpline;
	 /* # direct methods */
	 public com.android.server.display.HysteresisLevelsImpl ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Lcom/android/server/display/HysteresisLevelsStub;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void createHysteresisThresholdSpline ( Float[] p0, Float[] p1, Float[] p2, Float[] p3 ) {
		 /* .locals 2 */
		 /* .param p1, "ambientBrighteningLux" # [F */
		 /* .param p2, "ambientBrighteningThresholds" # [F */
		 /* .param p3, "ambientDarkeningLux" # [F */
		 /* .param p4, "ambientDarkeningThresholds" # [F */
		 /* .line 43 */
		 /* array-length v0, p1 */
		 /* array-length v1, p2 */
		 /* if-ne v0, v1, :cond_2 */
		 /* array-length v0, p3 */
		 /* array-length v1, p4 */
		 /* if-eq v0, v1, :cond_0 */
		 /* .line 48 */
	 } // :cond_0
	 /* array-length v0, p1 */
	 /* if-lez v0, :cond_1 */
	 /* array-length v0, p2 */
	 /* if-lez v0, :cond_1 */
	 /* array-length v0, p3 */
	 /* if-lez v0, :cond_1 */
	 /* array-length v0, p4 */
	 /* if-lez v0, :cond_1 */
	 /* .line 50 */
	 android.util.Spline .createLinearSpline ( p1,p2 );
	 this.mHysteresisBrightSpline = v0;
	 /* .line 51 */
	 android.util.Spline .createLinearSpline ( p3,p4 );
	 this.mHysteresisDarkSpline = v0;
	 /* .line 53 */
} // :cond_1
return;
/* .line 45 */
} // :cond_2
} // :goto_0
final String v0 = "HysteresisLevelsImpl"; // const-string v0, "HysteresisLevelsImpl"
final String v1 = "Mismatch between hysteresis array lengths."; // const-string v1, "Mismatch between hysteresis array lengths."
android.util.Slog .e ( v0,v1 );
/* .line 46 */
return;
} // .end method
void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 78 */
final String v0 = "MiuiHysteresisLevels:"; // const-string v0, "MiuiHysteresisLevels:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 79 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mHysteresisBrightSpline="; // const-string v1, " mHysteresisBrightSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHysteresisBrightSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 80 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mHysteresisDarkSpline="; // const-string v1, " mHysteresisDarkSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHysteresisDarkSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 81 */
return;
} // .end method
public Float getBrighteningThreshold ( Float p0 ) {
/* .locals 3 */
/* .param p1, "value" # F */
/* .line 57 */
v0 = this.mHysteresisBrightSpline;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 58 */
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 59 */
/* .local v0, "brighteningThreshold":F */
/* const v1, 0x45bb8000 # 6000.0f */
/* .line 60 */
/* .local v1, "hbmMinValue":F */
v2 = this.mHbmController;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = (( com.android.server.display.HighBrightnessModeController ) v2 ).deviceSupportsHbm ( ); // invoke-virtual {v2}, Lcom/android/server/display/HighBrightnessModeController;->deviceSupportsHbm()Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 61 */
	 v2 = this.mHbmController;
	 (( com.android.server.display.HighBrightnessModeController ) v2 ).getHbmData ( ); // invoke-virtual {v2}, Lcom/android/server/display/HighBrightnessModeController;->getHbmData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
	 /* iget v1, v2, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->minimumLux:F */
	 /* .line 64 */
} // :cond_0
/* cmpl-float v2, p1, v1 */
/* if-lez v2, :cond_1 */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* add-float/2addr v2, p1 */
} // :cond_1
/* move v2, v0 */
} // :goto_0
/* .line 66 */
} // .end local v0 # "brighteningThreshold":F
} // .end local v1 # "hbmMinValue":F
} // :cond_2
v0 = this.mAmbientBrightnessThresholds;
v0 = (( com.android.server.display.HysteresisLevels ) v0 ).getBrighteningThreshold ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/HysteresisLevels;->getBrighteningThreshold(F)F
} // .end method
public Float getDarkeningThreshold ( Float p0 ) {
/* .locals 1 */
/* .param p1, "value" # F */
/* .line 71 */
v0 = this.mHysteresisDarkSpline;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 74 */
} // :cond_0
v0 = this.mAmbientBrightnessThresholds;
v0 = (( com.android.server.display.HysteresisLevels ) v0 ).getDarkeningThreshold ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/HysteresisLevels;->getDarkeningThreshold(F)F
} // .end method
public void initialize ( com.android.server.display.HighBrightnessModeController p0, com.android.server.display.HysteresisLevels p1 ) {
/* .locals 5 */
/* .param p1, "hbmController" # Lcom/android/server/display/HighBrightnessModeController; */
/* .param p2, "ambientBrightnessThresholds" # Lcom/android/server/display/HysteresisLevels; */
/* .line 22 */
this.mHbmController = p1;
/* .line 23 */
this.mAmbientBrightnessThresholds = p2;
/* .line 25 */
android.content.res.Resources .getSystem ( );
/* const v1, 0x1103001e */
(( android.content.res.Resources ) v0 ).obtainTypedArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 24 */
com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v0 );
/* .line 28 */
/* .local v0, "ambientBrighteningLux":[F */
android.content.res.Resources .getSystem ( );
/* const v2, 0x11030023 */
(( android.content.res.Resources ) v1 ).obtainTypedArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 27 */
com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v1 );
/* .line 31 */
/* .local v1, "ambientBrighteningThresholds":[F */
android.content.res.Resources .getSystem ( );
/* const v3, 0x11030028 */
(( android.content.res.Resources ) v2 ).obtainTypedArray ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 30 */
com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v2 );
/* .line 34 */
/* .local v2, "ambientDarkeningLux":[F */
android.content.res.Resources .getSystem ( );
/* const v4, 0x1103002d */
(( android.content.res.Resources ) v3 ).obtainTypedArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;
/* .line 33 */
com.android.server.display.BrightnessMappingStrategy .getFloatArray ( v3 );
/* .line 36 */
/* .local v3, "ambientDarkeningThresholds":[F */
(( com.android.server.display.HysteresisLevelsImpl ) p0 ).createHysteresisThresholdSpline ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/display/HysteresisLevelsImpl;->createHysteresisThresholdSpline([F[F[F[F)V
/* .line 38 */
return;
} // .end method
