class com.android.server.display.BrightnessCurve$DarkLightCurve extends com.android.server.display.BrightnessCurve$Curve {
	 /* .source "BrightnessCurve.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/BrightnessCurve; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DarkLightCurve" */
} // .end annotation
/* # instance fields */
private final Float mHeadLux;
private final Float mPullUpLimitNit;
private final Float mPullUpMaxTan;
private final Float mTailLux;
final com.android.server.display.BrightnessCurve this$0; //synthetic
/* # direct methods */
public com.android.server.display.BrightnessCurve$DarkLightCurve ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/display/BrightnessCurve; */
/* .line 304 */
this.this$0 = p1;
/* invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve$Curve;-><init>(Lcom/android/server/display/BrightnessCurve;)V */
/* .line 305 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
/* .line 306 */
com.android.server.display.BrightnessCurve .-$$Nest$fgetmCurrentCurveInterval ( p1 );
int v1 = 1; // const/4 v1, 0x1
/* aget v0, v0, v1 */
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
/* .line 307 */
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmDarkLightCurvePullUpMaxTan ( p1 );
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpMaxTan:F */
/* .line 308 */
v0 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmDarkLightCurvePullUpLimitNit ( p1 );
/* iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpLimitNit:F */
/* .line 309 */
return;
} // .end method
/* # virtual methods */
public void connectLeft ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 0 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 343 */
return;
} // .end method
public void connectRight ( com.android.server.display.BrightnessCurve$Curve p0 ) {
/* .locals 9 */
/* .param p1, "curve" # Lcom/android/server/display/BrightnessCurve$Curve; */
/* .line 347 */
v0 = v0 = this.mPointList;
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 348 */
	 v0 = this.mPointList;
	 int v1 = 0; // const/4 v1, 0x0
	 /* check-cast v0, Landroid/util/Pair; */
	 /* .line 349 */
	 /* .local v0, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
	 v1 = this.second;
	 /* check-cast v1, Ljava/lang/Float; */
	 v1 = 	 (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
	 v2 = this.this$0;
	 com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v2 );
	 /* iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
	 v2 = 	 (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
	 /* cmpl-float v1, v1, v2 */
	 /* if-nez v1, :cond_0 */
	 /* .line 350 */
	 v2 = this.this$0;
	 /* iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
	 /* iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
	 v5 = this.mPointList;
	 int v6 = 0; // const/4 v6, 0x0
	 int v7 = 0; // const/4 v7, 0x0
	 /* invoke-static/range {v2 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
	 /* .line 351 */
} // :cond_0
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 352 */
v1 = this.first;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
(( com.android.server.display.BrightnessCurve$DarkLightCurve ) p0 ).create ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->create(FF)V
/* .line 354 */
} // :cond_1
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
v2 = this.first;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
v2 = this.second;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v1, v2 */
/* .line 355 */
/* .local v1, "diffNit":F */
v3 = this.this$0;
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
v6 = this.mPointList;
/* const v8, 0x3f333333 # 0.7f */
/* move v7, v1 */
/* invoke-static/range {v3 ..v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 358 */
} // .end local v0 # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // .end local v1 # "diffNit":F
} // :cond_2
} // :goto_0
return;
} // .end method
public void create ( Float p0, Float p1 ) {
/* .locals 9 */
/* .param p1, "lux" # F */
/* .param p2, "changeNit" # F */
/* .line 313 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* cmpl-float v0, v0, p2 */
/* if-lez v0, :cond_1 */
/* .line 314 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v0, p1, v0 */
/* if-ltz v0, :cond_0 */
/* const/high16 v0, 0x40400000 # 3.0f */
/* cmpg-float v0, p1, v0 */
/* if-gtz v0, :cond_0 */
/* .line 315 */
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v0, p2 */
/* .line 316 */
/* .local v0, "diffNit":F */
v1 = this.this$0;
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
v4 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
/* move v5, v0 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 317 */
} // .end local v0 # "diffNit":F
/* goto/16 :goto_0 */
/* .line 318 */
} // :cond_0
v0 = this.this$0;
v3 = this.mPointList;
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
v1 = this.this$0;
v6 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
v1 = this.this$0;
v7 = com.android.server.display.BrightnessCurve .-$$Nest$fgetmBrightnessMinTan ( v1 );
/* move v1, p1 */
/* move v2, p2 */
/* invoke-static/range {v0 ..v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullDownCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V */
/* .line 322 */
} // :cond_1
v0 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v0 );
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* sub-float/2addr v0, p2 */
/* .line 323 */
/* .restart local v0 # "diffNit":F */
v1 = this.this$0;
com.android.server.display.BrightnessCurve .-$$Nest$fgetmLuxToNitsDefault ( v1 );
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
v1 = (( android.util.Spline ) v1 ).interpolate ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F
/* sub-float v7, v1, v0 */
/* .line 324 */
/* .local v7, "tailPointNit":F */
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpLimitNit:F */
/* sub-float v2, v1, p2 */
/* iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
/* sub-float v4, v3, p1 */
/* div-float v8, v2, v4 */
/* .line 325 */
/* .local v8, "tanToLimitPoint":F */
/* cmpg-float v1, v7, v1 */
/* if-gez v1, :cond_2 */
/* .line 326 */
v1 = this.this$0;
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
v4 = this.mPointList;
int v6 = 0; // const/4 v6, 0x0
/* move v5, v0 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V */
/* .line 327 */
return;
/* .line 328 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpMaxTan:F */
/* cmpg-float v1, v8, v1 */
/* if-gez v1, :cond_3 */
/* .line 329 */
/* iget v8, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpMaxTan:F */
/* .line 331 */
} // :cond_3
/* sub-float/2addr v3, p1 */
/* mul-float/2addr v3, v8 */
/* add-float v1, p2, v3 */
/* .line 332 */
} // .end local v7 # "tailPointNit":F
/* .local v1, "tailPointNit":F */
/* iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
/* sub-float v2, p1, v2 */
/* mul-float/2addr v2, v8 */
/* sub-float v2, p2, v2 */
/* .line 333 */
/* .local v2, "headPointNit":F */
/* new-instance v3, Landroid/util/Pair; */
/* iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F */
java.lang.Float .valueOf ( v4 );
java.lang.Float .valueOf ( v2 );
/* invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 334 */
/* .local v3, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v4 = this.mPointList;
/* .line 335 */
/* new-instance v4, Landroid/util/Pair; */
/* iget v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F */
java.lang.Float .valueOf ( v5 );
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 336 */
/* .local v4, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;" */
v5 = this.mPointList;
/* .line 338 */
} // .end local v0 # "diffNit":F
} // .end local v1 # "tailPointNit":F
} // .end local v2 # "headPointNit":F
} // .end local v3 # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // .end local v4 # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
} // .end local v8 # "tanToLimitPoint":F
} // :goto_0
return;
} // .end method
