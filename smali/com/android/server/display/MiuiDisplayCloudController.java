public class com.android.server.display.MiuiDisplayCloudController {
	 /* .source "MiuiDisplayCloudController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/MiuiDisplayCloudController$Callback;, */
	 /* Lcom/android/server/display/MiuiDisplayCloudController$Observer;, */
	 /* Lcom/android/server/display/MiuiDisplayCloudController$CloudListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_CATEGORY_CONFIG;
private static final java.lang.String AUTO_BRIGHTNESS_STATISTICS_EVENT_ENABLE;
private static final java.lang.String AUTO_BRIGHTNESS_STATISTICS_EVENT_MODULE_NAME;
private static final java.lang.String BCBC_APP_CONFIG;
private static final java.lang.String BCBC_FEATURE_MODULE_NAME;
private static final java.lang.String BRIGHTNESS_CURVE_OPTIMIZE_POLICY_DISABLE;
private static final java.lang.String BRIGHTNESS_CURVE_OPTIMIZE_POLICY_MODULE_NAME;
private static final java.lang.String BRIGHTNESS_STATISTICS_EVENTS_ENABLE;
private static final java.lang.String BRIGHTNESS_STATISTICS_EVENTS_NAME;
public static final java.lang.String CLOUD_BACKUP_DIR_NAME;
private static final java.lang.String CLOUD_BACKUP_FILE_ATTRIBUTE_ENABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_ATTRIBUTE_ITEM;
private static final java.lang.String CLOUD_BACKUP_FILE_ATTRIBUTE_PACKAGE;
private static final java.lang.String CLOUD_BACKUP_FILE_ATTRIBUTE_VALUE;
private static final java.lang.String CLOUD_BACKUP_FILE_AUTO_BRIGHTNESS_STATISTICS_EVENT_ENABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_AUTO_BRIGHTNESS_STATISTICS_EVENT_ENABLE_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_BCBC_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_BCBC_TAG_APP;
private static final java.lang.String CLOUD_BACKUP_FILE_BRIGHTNESS_CURVE_OPTIMIZE_POLICY_DISABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_BRIGHTNESS_CURVE_OPTIMIZE_POLICY_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_BRIGHTNESS_STATISTICS_EVENTS_ENABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_BRIGHTNESS_STATISTICS_EVENTS_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_CATEGORY_TAG_GAME;
private static final java.lang.String CLOUD_BACKUP_FILE_CATEGORY_TAG_IMAGE;
private static final java.lang.String CLOUD_BACKUP_FILE_CATEGORY_TAG_MAP;
private static final java.lang.String CLOUD_BACKUP_FILE_CATEGORY_TAG_READER;
private static final java.lang.String CLOUD_BACKUP_FILE_CATEGORY_TAG_UNDEFINED;
private static final java.lang.String CLOUD_BACKUP_FILE_CATEGORY_TAG_VIDEO;
private static final java.lang.String CLOUD_BACKUP_FILE_CONTACTLESS_GESTURE_COMPONENTS_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_CONTACTLESS_GESTURE_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_CUSTOM_CURVE_DISABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_CUSTOM_CURVE_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_DISABLE_RESET_SHORT_TERM_MODEL;
private static final java.lang.String CLOUD_BACKUP_FILE_DISABLE_RESET_SHORT_TERM_MODEL_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_INDIVIDUAL_MODEL_DISABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_INDIVIDUAL_MODEL_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_MANUAL_BOOST_APP_ENABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_MANUAL_BOOST_DISABLE_APP_LIST;
private static final java.lang.String CLOUD_BACKUP_FILE_MANUAL_BOOST_DISABLE_APP_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_NAME;
private static final java.lang.String CLOUD_BACKUP_FILE_OUTDOOR_THERMAL_APP_CATEGORY_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_OUTDOOR_THERMAL_TAG_APP;
private static final java.lang.String CLOUD_BACKUP_FILE_OVERRIDE_BRIGHTNESS_POLICY_ENABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_OVERRIDE_BRIGHTNESS_POLICY_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_RESOLUTION_SWITCH_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_RESOLUTION_SWITCH_TAG_PROCESS_BLACK;
private static final java.lang.String CLOUD_BACKUP_FILE_RESOLUTION_SWITCH_TAG_PROCESS_PROTECT;
private static final java.lang.String CLOUD_BACKUP_FILE_RHYTHMIC_APP_CATEGORY_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_RHYTHMIC_APP_CATEGORY_TAG_IMAGE;
private static final java.lang.String CLOUD_BACKUP_FILE_RHYTHMIC_APP_CATEGORY_TAG_READ;
private static final java.lang.String CLOUD_BACKUP_FILE_ROOT_ELEMENT;
private static final java.lang.String CLOUD_BACKUP_FILE_SHORT_TERM_MODEL_ENABLE;
private static final java.lang.String CLOUD_BACKUP_FILE_SHORT_TERM_MODEL_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_THERMAL_BRIGHTNESS;
private static final java.lang.String CLOUD_BACKUP_FILE_THRESHOLD_SUNLIGHT_NIT_VALUE;
private static final java.lang.String CLOUD_BACKUP_FILE_THRESHOLD_SUNLIGHT_NIT_VALUE_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_TOUCH_COVER_PROTECTION_GAME_TAG;
private static final java.lang.String CLOUD_BACKUP_FILE_TOUCH_COVER_PROTECTION_GAME_TAG_APP;
private static final java.lang.String CLOUD_BAKUP_FILE_TEMPERATURE_GAP_TAG;
private static final java.lang.String CLOUD_BAKUP_FILE_TEMPERATURE_GAP_VALUE;
public static final Long CLOUD_EVENTS_APP_CATEGORY;
public static final Long CLOUD_EVENTS_BRIGHTNESS_STATS;
public static final Long CLOUD_EVENTS_CUSTOM_CURVE;
public static final Long CLOUD_EVENTS_INDIVIDUAL_MODEL;
public static final Long CLOUD_EVENTS_TEMPERATURE_GAP;
public static final Long CLOUD_EVENTS_THERMAL_CONTROL;
private static final java.lang.String CLOUD_FILE_APP_CATEGORY_CONFIG;
private static final java.lang.String CLOUD_THRESHOLD_SUNLIGHT_NIT;
private static final java.lang.String CONTACTLESS_GESTURE_COMPONENTS;
private static final java.lang.String CONTACTLESS_GESTURE_MODULE_NAME;
private static final java.lang.String CUSTOM_CURVE_DISABLE;
private static final java.lang.String CUSTOM_CURVE_DISABLE_MODULE_NAME;
private static final Boolean DEBUG;
private static final java.lang.String DISABLE_RESET_SHORT_TERM_MODEL;
private static final java.lang.String DISABLE_RESET_SHORT_TERM_MODEL_MODULE_NAME;
private static final java.lang.String INDIVIDUAL_APP_CATEGORY_CONFIG_MODULE_NAME;
private static final java.lang.String INDIVIDUAL_MODEL_DISABLE;
private static final java.lang.String INDIVIDUAL_MODEL_DISABLE_MODULE_NAME;
private static final java.lang.String MANUAL_BOOST_APP_ENABLE;
private static final java.lang.String MANUAL_BOOST_APP_ENABLE_MODULE_NAME;
private static final java.lang.String MANUAL_BOOST_DISABLE_APP_LIST;
private static final java.lang.String MANUAL_BOOST_DISABLE_APP_MODULE_NAME;
private static final java.lang.String OUTDOOR_THERMAL_APP_CATEGORY_LIST_BACKUP_FILE;
private static final java.lang.String OUTDOOR_THERMAL_APP_LIST;
private static final java.lang.String OUTDOOR_THERMAL_APP_LIST_MODULE_NAME;
private static final java.lang.String OVERRIDE_BRIGHTNESS_POLICY_ENABLE;
private static final java.lang.String OVERRIDE_BRIGHTNESS_POLICY_MODULE_NAME;
private static final java.lang.String PROCESS_RESOLUTION_SWITCH_BLACK_LIST;
private static final java.lang.String PROCESS_RESOLUTION_SWITCH_LIST;
private static final java.lang.String PROCESS_RESOLUTION_SWITCH_PROTECT_LIST;
private static final java.lang.String RESOLUTION_SWITCH_PROCESS_LIST_BACKUP_FILE;
private static final java.lang.String RESOLUTION_SWITCH_PROCESS_LIST_MODEULE_NAME;
private static final java.lang.String RHYTHMIC_APP_CATEGORY_IMAGE_LIST;
private static final java.lang.String RHYTHMIC_APP_CATEGORY_LIST_BACKUP_FILE;
private static final java.lang.String RHYTHMIC_APP_CATEGORY_LIST_MODULE_NAME;
private static final java.lang.String RHYTHMIC_APP_CATEGORY_LIST_NAME;
private static final java.lang.String RHYTHMIC_APP_CATEGORY_READ_LIST;
private static final java.lang.String SHORT_TERM_MODEL_APP_CONFIG;
private static final java.lang.String SHORT_TERM_MODEL_ENABLE;
private static final java.lang.String SHORT_TERM_MODEL_GAME_APP_LIST;
private static final java.lang.String SHORT_TERM_MODEL_GLOBAL_APP_LIST;
private static final java.lang.String SHORT_TERM_MODEL_IMAGE_APP_LIST;
private static final java.lang.String SHORT_TERM_MODEL_MAP_APP_LIST;
private static final java.lang.String SHORT_TERM_MODEL_MODULE_NAME;
private static final java.lang.String SHORT_TERM_MODEL_READER_APP_LIST;
private static final java.lang.String SHORT_TERM_MODEL_VIDEO_APP_LIST;
private static final java.lang.String SHORT_TREM_MODEL_APP_MODULE_NAME;
private static final java.lang.String TAG;
public static final java.lang.String TEMPERATURE_GAP_MODULE_NAME;
private static final java.lang.String TEMPERATURE_GAP_VALUE;
private static final Float TEMPERATURE_GAP_VALUE_DEFAULT;
private static final Float THRESHOLD_SUNLIGHT_NIT_DEFAULT;
private static final java.lang.String THRESHOLD_SUNLIGHT_NIT_MODULE_NAME;
private static final java.lang.String TOUCH_COVER_PROTECTION_GAME_APP_LIST;
private static final java.lang.String TOUCH_COVER_PROTECTION_GAME_MODE;
/* # instance fields */
private android.util.AtomicFile mAppCategoryConfigCloudFile;
private Boolean mAutoBrightnessStatisticsEventEnable;
private java.util.List mBCBCAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mBrightnessCurveOptimizePolicyDisable;
private Boolean mBrightnessStatsEventsEnable;
private com.android.server.display.MiuiDisplayCloudController$Callback mCallback;
private java.util.Map mCloudEventsData;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mCloudEventsSummary;
private java.util.List mCloudListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Boolean mCustomCurveDisable;
private Boolean mDisableResetShortTermModel;
private android.util.AtomicFile mFile;
private java.util.List mGestureComponents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private Boolean mIndividualModelDisable;
private Boolean mManualBoostAppEnable;
private java.util.List mManualBoostDisableAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.ArrayList mObservers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/display/MiuiDisplayCloudController$Observer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mOutdoorThermalAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mOverrideBrightnessPolicyEnable;
private java.util.List mResolutionSwitchProcessBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mResolutionSwitchProcessProtectList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mRhythmicImageAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mRhythmicReadAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mShortTermModelAppMapper;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mShortTermModelCloudAppCategoryList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mShortTermModelEnable;
private java.util.List mShortTermModelGameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mShortTermModelGlobalList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mShortTermModelImageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mShortTermModelMapList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mShortTermModelReaderList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mShortTermModelVideoList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float mTemperatureGap;
private android.util.AtomicFile mThermalBrightnessCloudFile;
private Float mThresholdSunlightNit;
private com.android.server.tof.TofManagerInternal mTofManagerInternal;
private java.util.List mTouchCoverProtectionGameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$buBRH27xsKbPmGJbuA3fIB3wMOA ( com.android.server.display.MiuiDisplayCloudController p0, com.android.server.display.MiuiDisplayCloudController$CloudListener p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/MiuiDisplayCloudController;->lambda$updateDataFromCloudControl$0(Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;)V */
return;
} // .end method
static void -$$Nest$msyncLocalBackupFromCloud ( com.android.server.display.MiuiDisplayCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->syncLocalBackupFromCloud()V */
return;
} // .end method
static Boolean -$$Nest$mupdateDataFromCloudControl ( com.android.server.display.MiuiDisplayCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateDataFromCloudControl()Z */
} // .end method
static com.android.server.display.MiuiDisplayCloudController ( ) {
/* .locals 2 */
/* .line 62 */
final String v0 = "debug.miui.display.cloud.dbg"; // const-string v0, "debug.miui.display.cloud.dbg"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
com.android.server.display.MiuiDisplayCloudController.DEBUG = (v1!= 0);
return;
} // .end method
public com.android.server.display.MiuiDisplayCloudController ( ) {
/* .locals 1 */
/* .param p1, "looper" # Landroid/os/Looper; */
/* .param p2, "callback" # Lcom/android/server/display/MiuiDisplayCloudController$Callback; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 301 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 176 */
/* const/high16 v0, 0x3f000000 # 0.5f */
/* iput v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F */
/* .line 185 */
/* const/high16 v0, 0x43200000 # 160.0f */
/* iput v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F */
/* .line 200 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelGameList = v0;
/* .line 201 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelVideoList = v0;
/* .line 202 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelMapList = v0;
/* .line 203 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelImageList = v0;
/* .line 204 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelReaderList = v0;
/* .line 205 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelGlobalList = v0;
/* .line 207 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mShortTermModelAppMapper = v0;
/* .line 208 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortTermModelCloudAppCategoryList = v0;
/* .line 209 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mBCBCAppList = v0;
/* .line 210 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTouchCoverProtectionGameList = v0;
/* .line 212 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mManualBoostDisableAppList = v0;
/* .line 217 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mObservers = v0;
/* .line 218 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCloudListeners = v0;
/* .line 220 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mResolutionSwitchProcessProtectList = v0;
/* .line 221 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mResolutionSwitchProcessBlackList = v0;
/* .line 224 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mRhythmicImageAppList = v0;
/* .line 225 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mRhythmicReadAppList = v0;
/* .line 227 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mGestureComponents = v0;
/* .line 231 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mOutdoorThermalAppList = v0;
/* .line 302 */
this.mContext = p3;
/* .line 303 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 304 */
this.mCallback = p2;
/* .line 305 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->initialization()V */
/* .line 306 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->registerMiuiBrightnessCloudDataObserver()V */
/* .line 307 */
return;
} // .end method
private android.util.AtomicFile getFile ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .line 1032 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v3 = "displayconfig"; // const-string v3, "displayconfig"
/* filled-new-array {v3}, [Ljava/lang/String; */
android.os.Environment .buildPath ( v2,v3 );
/* invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
} // .end method
private void initialization ( ) {
/* .locals 4 */
/* .line 315 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCloudEventsData = v0;
/* .line 317 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
/* .line 319 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 6; // const/4 v1, 0x6
/* if-ge v0, v1, :cond_0 */
/* .line 320 */
v1 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v0 );
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 319 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 323 */
} // .end local v0 # "i":I
} // :cond_0
v0 = this.mShortTermModelCloudAppCategoryList;
/* const-string/jumbo v1, "short_term_model_game_app_list" */
/* .line 324 */
v0 = this.mShortTermModelCloudAppCategoryList;
/* const-string/jumbo v1, "short_term_model_video_app_list" */
/* .line 325 */
v0 = this.mShortTermModelCloudAppCategoryList;
/* const-string/jumbo v1, "short_term_model_map_app_list" */
/* .line 326 */
v0 = this.mShortTermModelCloudAppCategoryList;
/* const-string/jumbo v1, "short_term_model_image_app_list" */
/* .line 327 */
v0 = this.mShortTermModelCloudAppCategoryList;
/* const-string/jumbo v1, "short_term_model_reader_app_list" */
/* .line 328 */
v0 = this.mShortTermModelCloudAppCategoryList;
/* const-string/jumbo v1, "short_term_model_global_app_list" */
/* .line 330 */
final String v0 = "display_cloud_backup.xml"; // const-string v0, "display_cloud_backup.xml"
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile; */
this.mFile = v0;
/* .line 332 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadLocalCloudBackup()V */
/* .line 333 */
return;
} // .end method
private void lambda$updateDataFromCloudControl$0 ( com.android.server.display.MiuiDisplayCloudController$CloudListener p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "l" # Lcom/android/server/display/MiuiDisplayCloudController$CloudListener; */
/* .line 947 */
/* iget-wide v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
v2 = this.mCloudEventsData;
/* .line 948 */
return;
} // .end method
private void loadLocalCloudBackup ( ) {
/* .locals 5 */
/* .line 956 */
v0 = this.mFile;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( android.util.AtomicFile ) v0 ).exists ( ); // invoke-virtual {v0}, Landroid/util/AtomicFile;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 957 */
int v0 = 0; // const/4 v0, 0x0
/* .line 959 */
/* .local v0, "inputStream":Ljava/io/FileInputStream; */
try { // :try_start_0
v1 = this.mFile;
(( android.util.AtomicFile ) v1 ).openRead ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v0, v1 */
/* .line 960 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->readCloudDataFromXml(Ljava/io/InputStream;)V */
/* .line 962 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1}, Lcom/android/server/display/MiuiDisplayCloudController;->saveShortTermModelAppComponent(Lorg/json/JSONObject;)V */
/* .line 963 */
(( com.android.server.display.MiuiDisplayCloudController ) p0 ).notifyAllObservers ( ); // invoke-virtual {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyAllObservers()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 968 */
/* nop */
} // :goto_0
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 969 */
/* .line 968 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 964 */
/* :catch_0 */
/* move-exception v1 */
/* .line 965 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_1
v2 = this.mFile;
(( android.util.AtomicFile ) v2 ).delete ( ); // invoke-virtual {v2}, Landroid/util/AtomicFile;->delete()V
/* .line 966 */
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to read local cloud backup"; // const-string v4, "Failed to read local cloud backup"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 968 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
/* .line 970 */
} // .end local v0 # "inputStream":Ljava/io/FileInputStream;
} // :goto_1
/* .line 968 */
/* .restart local v0 # "inputStream":Ljava/io/FileInputStream; */
} // :goto_2
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 969 */
/* throw v1 */
/* .line 971 */
} // .end local v0 # "inputStream":Ljava/io/FileInputStream;
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadResolutionSwitchBackUpFileFromXml()V */
/* .line 972 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadRhythmicAppCategoryBackUpFileFromXml()V */
/* .line 973 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadOutdoorThermalAppCategoryBackUpFileFromXml()V */
/* .line 975 */
} // :goto_3
return;
} // .end method
private void loadOutdoorThermalAppCategoryBackUpFileFromXml ( ) {
/* .locals 6 */
/* .line 1010 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1011 */
/* .local v0, "inputStream":Ljava/io/FileInputStream; */
/* new-instance v1, Landroid/util/AtomicFile; */
/* .line 1012 */
android.os.Environment .getProductDirectory ( );
final String v3 = "etc/displayconfig"; // const-string v3, "etc/displayconfig"
final String v4 = "outdoor_thermal_app_category_list_backup.xml"; // const-string v4, "outdoor_thermal_app_category_list_backup.xml"
/* filled-new-array {v3, v4}, [Ljava/lang/String; */
/* .line 1011 */
android.os.Environment .buildPath ( v2,v3 );
/* invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
/* .line 1015 */
/* .local v1, "file":Landroid/util/AtomicFile; */
try { // :try_start_0
(( android.util.AtomicFile ) v1 ).openRead ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v0, v2 */
/* .line 1016 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadOutdoorThermalAppCategoryListFromXml(Ljava/io/InputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1021 */
/* nop */
} // :goto_0
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 1022 */
/* .line 1021 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1017 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1018 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
(( android.util.AtomicFile ) v1 ).delete ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->delete()V
/* .line 1019 */
final String v3 = "MiuiDisplayCloudController"; // const-string v3, "MiuiDisplayCloudController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Failed to read local cloud backup"; // const-string v5, "Failed to read local cloud backup"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1021 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 1023 */
} // :goto_1
return;
/* .line 1021 */
} // :goto_2
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 1022 */
/* throw v2 */
} // .end method
private void loadOutdoorThermalAppCategoryListFromXml ( java.io.InputStream p0 ) {
/* .locals 6 */
/* .param p1, "stream" # Ljava/io/InputStream; */
/* .line 882 */
final String v0 = "MiuiDisplayCloudController"; // const-string v0, "MiuiDisplayCloudController"
try { // :try_start_0
final String v1 = "Start loading app category list from xml."; // const-string v1, "Start loading app category list from xml."
android.util.Slog .d ( v0,v1 );
/* .line 883 */
android.util.Xml .resolvePullParser ( p1 );
/* .line 885 */
/* .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
} // :cond_0
v2 = } // :goto_0
/* move v3, v2 */
/* .local v3, "type":I */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v2, v4, :cond_3 */
/* .line 886 */
int v2 = 4; // const/4 v2, 0x4
/* if-eq v3, v2, :cond_0 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v3, v2, :cond_1 */
/* .line 887 */
/* .line 889 */
} // :cond_1
/* .line 890 */
/* .local v2, "tag":Ljava/lang/String; */
v4 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
/* packed-switch v4, :pswitch_data_0 */
} // :cond_2
/* :pswitch_0 */
final String v4 = "item"; // const-string v4, "item"
v4 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
int v4 = 0; // const/4 v4, 0x0
} // :goto_1
int v4 = -1; // const/4 v4, -0x1
} // :goto_2
/* packed-switch v4, :pswitch_data_1 */
/* .line 892 */
/* :pswitch_1 */
v4 = this.mOutdoorThermalAppList;
/* .line 893 */
/* nop */
/* .line 897 */
} // .end local v2 # "tag":Ljava/lang/String;
} // :goto_3
/* .line 898 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyOutdoorThermalAppCategoryListChanged()V */
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 901 */
} // .end local v1 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v3 # "type":I
/* .line 899 */
/* :catch_0 */
/* move-exception v1 */
/* .line 900 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parse local cloud backup file"; // const-string v3, "Failed to parse local cloud backup file"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 902 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
return;
/* :pswitch_data_0 */
/* .packed-switch 0x317b13 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void loadResolutionSwitchBackUpFileFromXml ( ) {
/* .locals 6 */
/* .line 994 */
int v0 = 0; // const/4 v0, 0x0
/* .line 995 */
/* .local v0, "inputStream":Ljava/io/FileInputStream; */
/* new-instance v1, Landroid/util/AtomicFile; */
/* .line 996 */
android.os.Environment .getProductDirectory ( );
final String v3 = "etc/displayconfig"; // const-string v3, "etc/displayconfig"
final String v4 = "resolution_switch_process_list_backup.xml"; // const-string v4, "resolution_switch_process_list_backup.xml"
/* filled-new-array {v3, v4}, [Ljava/lang/String; */
/* .line 995 */
android.os.Environment .buildPath ( v2,v3 );
/* invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
/* .line 999 */
/* .local v1, "file":Landroid/util/AtomicFile; */
try { // :try_start_0
(( android.util.AtomicFile ) v1 ).openRead ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v0, v2 */
/* .line 1000 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadResolutionSwitchListFromXml(Ljava/io/InputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1005 */
/* nop */
} // :goto_0
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 1006 */
/* .line 1005 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1001 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1002 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
(( android.util.AtomicFile ) v1 ).delete ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->delete()V
/* .line 1003 */
final String v3 = "MiuiDisplayCloudController"; // const-string v3, "MiuiDisplayCloudController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Failed to read local cloud backup"; // const-string v5, "Failed to read local cloud backup"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1005 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 1007 */
} // :goto_1
return;
/* .line 1005 */
} // :goto_2
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 1006 */
/* throw v2 */
} // .end method
private void loadResolutionSwitchListFromXml ( java.io.InputStream p0 ) {
/* .locals 8 */
/* .param p1, "stream" # Ljava/io/InputStream; */
/* .line 802 */
final String v0 = "MiuiDisplayCloudController"; // const-string v0, "MiuiDisplayCloudController"
try { // :try_start_0
final String v1 = "Start loading resolution switch process list from xml."; // const-string v1, "Start loading resolution switch process list from xml."
android.util.Slog .d ( v0,v1 );
/* .line 803 */
android.util.Xml .resolvePullParser ( p1 );
/* .line 806 */
/* .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
int v2 = 0; // const/4 v2, 0x0
/* .line 807 */
/* .local v2, "currentTag":I */
} // :cond_0
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
/* if-eq v3, v5, :cond_5 */
/* .line 808 */
int v3 = 4; // const/4 v3, 0x4
/* if-eq v4, v3, :cond_0 */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v4, v3, :cond_1 */
/* .line 809 */
/* .line 811 */
} // :cond_1
/* .line 812 */
/* .local v3, "tag":Ljava/lang/String; */
v6 = (( java.lang.String ) v3 ).hashCode ( ); // invoke-virtual {v3}, Ljava/lang/String;->hashCode()I
int v7 = 2; // const/4 v7, 0x2
/* sparse-switch v6, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
final String v6 = "resolution_switch_process_black"; // const-string v6, "resolution_switch_process_black"
v6 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* move v6, v5 */
/* :sswitch_1 */
final String v6 = "item"; // const-string v6, "item"
v6 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* move v6, v7 */
/* :sswitch_2 */
final String v6 = "resolution_switch_process_protect"; // const-string v6, "resolution_switch_process_protect"
v6 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
int v6 = 0; // const/4 v6, 0x0
} // :goto_1
int v6 = -1; // const/4 v6, -0x1
} // :goto_2
/* packed-switch v6, :pswitch_data_0 */
/* .line 820 */
/* :pswitch_0 */
/* if-ne v2, v5, :cond_3 */
/* .line 821 */
v5 = this.mResolutionSwitchProcessProtectList;
/* .line 822 */
} // :cond_3
/* if-ne v2, v7, :cond_4 */
/* .line 823 */
v5 = this.mResolutionSwitchProcessBlackList;
/* .line 817 */
/* :pswitch_1 */
int v2 = 2; // const/4 v2, 0x2
/* .line 818 */
/* .line 814 */
/* :pswitch_2 */
int v2 = 1; // const/4 v2, 0x1
/* .line 815 */
/* nop */
/* .line 829 */
} // .end local v3 # "tag":Ljava/lang/String;
} // :cond_4
} // :goto_3
/* .line 830 */
} // :cond_5
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyResolutionSwitchListChanged()V */
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 833 */
} // .end local v1 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v2 # "currentTag":I
} // .end local v4 # "type":I
/* .line 831 */
/* :catch_0 */
/* move-exception v1 */
/* .line 832 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parse local cloud backup file"; // const-string v3, "Failed to parse local cloud backup file"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 834 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5da4b919 -> :sswitch_2 */
/* 0x317b13 -> :sswitch_1 */
/* 0x40eb2617 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void loadRhythmicAppCategoryBackUpFileFromXml ( ) {
/* .locals 6 */
/* .line 978 */
int v0 = 0; // const/4 v0, 0x0
/* .line 979 */
/* .local v0, "inputStream":Ljava/io/FileInputStream; */
/* new-instance v1, Landroid/util/AtomicFile; */
/* .line 980 */
android.os.Environment .getProductDirectory ( );
final String v3 = "etc/displayconfig"; // const-string v3, "etc/displayconfig"
final String v4 = "rhythmic_app_category_list_backup.xml"; // const-string v4, "rhythmic_app_category_list_backup.xml"
/* filled-new-array {v3, v4}, [Ljava/lang/String; */
/* .line 979 */
android.os.Environment .buildPath ( v2,v3 );
/* invoke-direct {v1, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
/* .line 983 */
/* .local v1, "file":Landroid/util/AtomicFile; */
try { // :try_start_0
(( android.util.AtomicFile ) v1 ).openRead ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v0, v2 */
/* .line 984 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiDisplayCloudController;->loadRhythmicAppCategoryListFromXml(Ljava/io/InputStream;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 989 */
/* nop */
} // :goto_0
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 990 */
/* .line 989 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 985 */
/* :catch_0 */
/* move-exception v2 */
/* .line 986 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
(( android.util.AtomicFile ) v1 ).delete ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->delete()V
/* .line 987 */
final String v3 = "MiuiDisplayCloudController"; // const-string v3, "MiuiDisplayCloudController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Failed to read local cloud backup"; // const-string v5, "Failed to read local cloud backup"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 989 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 991 */
} // :goto_1
return;
/* .line 989 */
} // :goto_2
miui.io.IOUtils .closeQuietly ( v0 );
/* .line 990 */
/* throw v2 */
} // .end method
private void loadRhythmicAppCategoryListFromXml ( java.io.InputStream p0 ) {
/* .locals 8 */
/* .param p1, "stream" # Ljava/io/InputStream; */
/* .line 842 */
final String v0 = "MiuiDisplayCloudController"; // const-string v0, "MiuiDisplayCloudController"
try { // :try_start_0
final String v1 = "Start loading app category list from xml."; // const-string v1, "Start loading app category list from xml."
android.util.Slog .d ( v0,v1 );
/* .line 843 */
android.util.Xml .resolvePullParser ( p1 );
/* .line 846 */
/* .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
int v2 = 0; // const/4 v2, 0x0
/* .line 847 */
/* .local v2, "currentTag":I */
} // :cond_0
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
/* if-eq v3, v5, :cond_5 */
/* .line 848 */
int v3 = 4; // const/4 v3, 0x4
/* if-eq v4, v3, :cond_0 */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v4, v3, :cond_1 */
/* .line 849 */
/* .line 851 */
} // :cond_1
/* .line 852 */
/* .local v3, "tag":Ljava/lang/String; */
v6 = (( java.lang.String ) v3 ).hashCode ( ); // invoke-virtual {v3}, Ljava/lang/String;->hashCode()I
int v7 = 2; // const/4 v7, 0x2
/* sparse-switch v6, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
final String v6 = "rhythmic_app_category_read"; // const-string v6, "rhythmic_app_category_read"
v6 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* move v6, v5 */
/* :sswitch_1 */
final String v6 = "item"; // const-string v6, "item"
v6 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* move v6, v7 */
/* :sswitch_2 */
final String v6 = "rhythmic_app_category_image"; // const-string v6, "rhythmic_app_category_image"
v6 = (( java.lang.String ) v3 ).equals ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
int v6 = 0; // const/4 v6, 0x0
} // :goto_1
int v6 = -1; // const/4 v6, -0x1
} // :goto_2
/* packed-switch v6, :pswitch_data_0 */
/* .line 860 */
/* :pswitch_0 */
/* if-ne v2, v5, :cond_3 */
/* .line 861 */
v5 = this.mRhythmicImageAppList;
/* .line 862 */
} // :cond_3
/* if-ne v2, v7, :cond_4 */
/* .line 863 */
v5 = this.mRhythmicReadAppList;
/* .line 857 */
/* :pswitch_1 */
int v2 = 2; // const/4 v2, 0x2
/* .line 858 */
/* .line 854 */
/* :pswitch_2 */
int v2 = 1; // const/4 v2, 0x1
/* .line 855 */
/* nop */
/* .line 869 */
} // .end local v3 # "tag":Ljava/lang/String;
} // :cond_4
} // :goto_3
/* .line 870 */
} // :cond_5
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyRhythmicAppCategoryListChanged()V */
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 873 */
} // .end local v1 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v2 # "currentTag":I
} // .end local v4 # "type":I
/* .line 871 */
/* :catch_0 */
/* move-exception v1 */
/* .line 872 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parse local cloud backup file"; // const-string v3, "Failed to parse local cloud backup file"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 874 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x530f2ad9 -> :sswitch_2 */
/* 0x317b13 -> :sswitch_1 */
/* 0x3724844a -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void notifyContactlessGestureConfigChanged ( ) {
/* .locals 2 */
/* .line 1646 */
/* const-class v0, Lcom/android/server/tof/TofManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/tof/TofManagerInternal; */
this.mTofManagerInternal = v0;
/* .line 1647 */
final String v0 = "MiuiDisplayCloudController"; // const-string v0, "MiuiDisplayCloudController"
final String v1 = "notifyContactlessGestureConfigChanged"; // const-string v1, "notifyContactlessGestureConfigChanged"
android.util.Slog .e ( v0,v1 );
/* .line 1648 */
v0 = this.mTofManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1649 */
v1 = this.mGestureComponents;
(( com.android.server.tof.TofManagerInternal ) v0 ).updateGestureAppConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/tof/TofManagerInternal;->updateGestureAppConfig(Ljava/util/List;)V
/* .line 1651 */
} // :cond_0
return;
} // .end method
private void notifyOutdoorThermalAppCategoryListChanged ( ) {
/* .locals 2 */
/* .line 793 */
v0 = this.mCallback;
v1 = this.mOutdoorThermalAppList;
/* .line 794 */
return;
} // .end method
private void notifyResolutionSwitchListChanged ( ) {
/* .locals 3 */
/* .line 1435 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
v1 = this.mResolutionSwitchProcessProtectList;
v2 = this.mResolutionSwitchProcessBlackList;
/* .line 1436 */
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).updateResolutionSwitchList ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/DisplayManagerServiceStub;->updateResolutionSwitchList(Ljava/util/List;Ljava/util/List;)V
/* .line 1438 */
return;
} // .end method
private void notifyRhythmicAppCategoryListChanged ( ) {
/* .locals 3 */
/* .line 1679 */
com.android.server.display.DisplayManagerServiceStub .getInstance ( );
v1 = this.mRhythmicImageAppList;
v2 = this.mRhythmicReadAppList;
/* .line 1680 */
(( com.android.server.display.DisplayManagerServiceStub ) v0 ).updateRhythmicAppCategoryList ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/DisplayManagerServiceStub;->updateRhythmicAppCategoryList(Ljava/util/List;Ljava/util/List;)V
/* .line 1681 */
return;
} // .end method
private void readCloudDataFromXml ( java.io.InputStream p0 ) {
/* .locals 11 */
/* .param p1, "stream" # Ljava/io/InputStream; */
/* .line 652 */
final String v0 = "MiuiDisplayCloudController"; // const-string v0, "MiuiDisplayCloudController"
try { // :try_start_0
final String v1 = "Start reading cloud data from xml."; // const-string v1, "Start reading cloud data from xml."
android.util.Slog .d ( v0,v1 );
/* .line 653 */
android.util.Xml .resolvePullParser ( p1 );
/* .line 655 */
/* .local v1, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 656 */
v2 = this.mCloudEventsData;
/* .line 657 */
} // :cond_0
v2 = } // :goto_0
/* move v3, v2 */
/* .local v3, "type":I */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v2, v4, :cond_4 */
/* .line 658 */
int v2 = 3; // const/4 v2, 0x3
/* if-eq v3, v2, :cond_0 */
int v5 = 4; // const/4 v5, 0x4
/* if-ne v3, v5, :cond_1 */
/* .line 659 */
/* .line 661 */
} // :cond_1
/* .line 662 */
/* .local v6, "tag":Ljava/lang/String; */
v7 = (( java.lang.String ) v6 ).hashCode ( ); // invoke-virtual {v6}, Ljava/lang/String;->hashCode()I
int v8 = 0; // const/4 v8, 0x0
/* sparse-switch v7, :sswitch_data_0 */
} // :cond_2
/* goto/16 :goto_1 */
/* :sswitch_0 */
final String v2 = "image-category"; // const-string v2, "image-category"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* move v4, v5 */
/* goto/16 :goto_2 */
/* :sswitch_1 */
final String v2 = "individual-model-disable"; // const-string v2, "individual-model-disable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x17 */
/* goto/16 :goto_2 */
/* :sswitch_2 */
/* const-string/jumbo v2, "undefined-category" */
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v4 = 6; // const/4 v4, 0x6
/* goto/16 :goto_2 */
/* :sswitch_3 */
final String v2 = "automatic-brightness-statistics-event-enable"; // const-string v2, "automatic-brightness-statistics-event-enable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0xb */
/* goto/16 :goto_2 */
/* :sswitch_4 */
final String v2 = "disable-reset-short-term-model"; // const-string v2, "disable-reset-short-term-model"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0xc */
/* goto/16 :goto_2 */
/* :sswitch_5 */
final String v2 = "override-brightness-policy-enable"; // const-string v2, "override-brightness-policy-enable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0xa */
/* goto/16 :goto_2 */
/* :sswitch_6 */
final String v2 = "resolution_switch_process_black"; // const-string v2, "resolution_switch_process_black"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x9 */
/* goto/16 :goto_2 */
/* :sswitch_7 */
final String v2 = "rhythmic_app_category_read"; // const-string v2, "rhythmic_app_category_read"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x13 */
/* goto/16 :goto_2 */
/* :sswitch_8 */
final String v2 = "reader-category"; // const-string v2, "reader-category"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v4 = 5; // const/4 v4, 0x5
/* goto/16 :goto_2 */
/* :sswitch_9 */
final String v2 = "manual-boost-disable-app-list"; // const-string v2, "manual-boost-disable-app-list"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x10 */
/* goto/16 :goto_2 */
/* :sswitch_a */
final String v2 = "custom-curve-disable"; // const-string v2, "custom-curve-disable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x18 */
/* goto/16 :goto_2 */
/* :sswitch_b */
final String v2 = "brightness-statistics-events-enable"; // const-string v2, "brightness-statistics-events-enable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x14 */
/* goto/16 :goto_2 */
/* :sswitch_c */
/* const-string/jumbo v2, "temperature-gap-value" */
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x16 */
/* goto/16 :goto_2 */
/* :sswitch_d */
final String v2 = "bcbc-app"; // const-string v2, "bcbc-app"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v4 = 7; // const/4 v4, 0x7
/* goto/16 :goto_2 */
/* :sswitch_e */
final String v2 = "brightness-curve-optimize-policy-disable"; // const-string v2, "brightness-curve-optimize-policy-disable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x19 */
/* goto/16 :goto_2 */
/* :sswitch_f */
/* const-string/jumbo v2, "touch_cover_protection_game_app" */
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0xd */
/* goto/16 :goto_2 */
/* :sswitch_10 */
final String v2 = "outdoor_thermal_app_category_list"; // const-string v2, "outdoor_thermal_app_category_list"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x12 */
/* goto/16 :goto_2 */
/* :sswitch_11 */
final String v2 = "manual-boost-app-enable"; // const-string v2, "manual-boost-app-enable"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x11 */
/* :sswitch_12 */
/* const-string/jumbo v2, "threshold-sunlight-nit-value" */
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x15 */
/* :sswitch_13 */
final String v2 = "game-category"; // const-string v2, "game-category"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* :sswitch_14 */
/* const-string/jumbo v2, "video-category" */
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
int v4 = 2; // const/4 v4, 0x2
/* :sswitch_15 */
final String v2 = "rhythmic_app_category_image"; // const-string v2, "rhythmic_app_category_image"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0xe */
/* :sswitch_16 */
final String v2 = "gesture_component"; // const-string v2, "gesture_component"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0xf */
/* :sswitch_17 */
final String v4 = "map-category"; // const-string v4, "map-category"
v4 = (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* move v4, v2 */
/* :sswitch_18 */
final String v2 = "resolution_switch_process_protect"; // const-string v2, "resolution_switch_process_protect"
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v4, 0x8 */
/* :sswitch_19 */
/* const-string/jumbo v2, "short-term-model-enabled" */
v2 = (( java.lang.String ) v6 ).equals ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* move v4, v8 */
} // :goto_1
int v4 = -1; // const/4 v4, -0x1
} // :goto_2
/* const-string/jumbo v2, "value" */
final String v5 = "item"; // const-string v5, "item"
final String v7 = "enabled"; // const-string v7, "enabled"
final String v9 = "package"; // const-string v9, "package"
int v10 = 0; // const/4 v10, 0x0
/* packed-switch v4, :pswitch_data_0 */
/* goto/16 :goto_3 */
/* .line 776 */
/* :pswitch_0 */
v2 = try { // :try_start_1
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z */
/* .line 778 */
/* goto/16 :goto_3 */
/* .line 769 */
v2 = /* :pswitch_1 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z */
/* .line 771 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 772 */
/* iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v7, 0x10 */
/* or-long/2addr v4, v7 */
/* iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* goto/16 :goto_3 */
/* .line 762 */
v2 = /* :pswitch_2 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z */
/* .line 764 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 765 */
/* iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v7, 0x20 */
/* or-long/2addr v4, v7 */
/* iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* goto/16 :goto_3 */
/* .line 756 */
/* :pswitch_3 */
v2 = /* const/high16 v4, 0x3f000000 # 0.5f */
/* iput v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F */
/* .line 758 */
/* iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v7, 0x4 */
/* or-long/2addr v4, v7 */
/* iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 759 */
v4 = this.mCloudEventsData;
final String v5 = "TemperatureGap"; // const-string v5, "TemperatureGap"
java.lang.Float .valueOf ( v2 );
/* .line 760 */
/* goto/16 :goto_3 */
/* .line 751 */
/* :pswitch_4 */
v2 = /* const/high16 v4, 0x43200000 # 160.0f */
/* iput v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F */
/* .line 753 */
v4 = this.mCallback;
/* .line 754 */
/* goto/16 :goto_3 */
/* .line 744 */
v2 = /* :pswitch_5 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z */
/* .line 746 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 747 */
/* iget-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v7, 0x1 */
/* or-long/2addr v4, v7 */
/* iput-wide v4, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* goto/16 :goto_3 */
/* .line 740 */
/* :pswitch_6 */
v2 = this.mRhythmicReadAppList;
/* invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 742 */
/* goto/16 :goto_3 */
/* .line 736 */
/* :pswitch_7 */
v2 = this.mOutdoorThermalAppList;
/* invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 738 */
/* goto/16 :goto_3 */
/* .line 732 */
v2 = /* :pswitch_8 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z */
/* .line 734 */
/* goto/16 :goto_3 */
/* .line 728 */
/* :pswitch_9 */
v2 = this.mManualBoostDisableAppList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 730 */
/* goto/16 :goto_3 */
/* .line 724 */
/* :pswitch_a */
v2 = this.mGestureComponents;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 726 */
/* .line 720 */
/* :pswitch_b */
v2 = this.mRhythmicImageAppList;
/* invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 722 */
/* .line 716 */
/* :pswitch_c */
v2 = this.mTouchCoverProtectionGameList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 718 */
/* .line 712 */
v2 = /* :pswitch_d */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z */
/* .line 714 */
/* .line 708 */
v2 = /* :pswitch_e */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z */
/* .line 710 */
/* .line 704 */
v2 = /* :pswitch_f */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z */
/* .line 706 */
/* .line 700 */
/* :pswitch_10 */
v2 = this.mResolutionSwitchProcessBlackList;
/* invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 702 */
/* .line 696 */
/* :pswitch_11 */
v2 = this.mResolutionSwitchProcessProtectList;
/* invoke-direct {p0, v1, v5, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 698 */
/* .line 692 */
/* :pswitch_12 */
v2 = this.mBCBCAppList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 694 */
/* .line 688 */
/* :pswitch_13 */
v2 = this.mShortTermModelGlobalList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 690 */
/* .line 684 */
/* :pswitch_14 */
v2 = this.mShortTermModelReaderList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 686 */
/* .line 680 */
/* :pswitch_15 */
v2 = this.mShortTermModelImageList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 682 */
/* .line 676 */
/* :pswitch_16 */
v2 = this.mShortTermModelMapList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 678 */
/* .line 672 */
/* :pswitch_17 */
v2 = this.mShortTermModelVideoList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 674 */
/* .line 668 */
/* :pswitch_18 */
v2 = this.mShortTermModelGameList;
/* invoke-direct {p0, v1, v9, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveAppListFromXml(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;Ljava/util/List;)V */
/* .line 670 */
/* .line 664 */
v2 = /* :pswitch_19 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
/* .line 666 */
/* nop */
/* .line 782 */
} // .end local v6 # "tag":Ljava/lang/String;
} // :cond_3
} // :goto_3
/* goto/16 :goto_0 */
/* .line 783 */
} // :cond_4
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyResolutionSwitchListChanged()V */
/* .line 784 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyRhythmicAppCategoryListChanged()V */
/* .line 785 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyOutdoorThermalAppCategoryListChanged()V */
/* .line 786 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyContactlessGestureConfigChanged()V */
/* :try_end_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 789 */
} // .end local v1 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v3 # "type":I
/* .line 787 */
/* :catch_0 */
/* move-exception v1 */
/* .line 788 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parse local cloud backup file"; // const-string v3, "Failed to parse local cloud backup file"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 790 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6ea54613 -> :sswitch_19 */
/* -0x5da4b919 -> :sswitch_18 */
/* -0x5b891db1 -> :sswitch_17 */
/* -0x5602d279 -> :sswitch_16 */
/* -0x530f2ad9 -> :sswitch_15 */
/* -0x5185f9f0 -> :sswitch_14 */
/* -0x4594bb87 -> :sswitch_13 */
/* -0x3ea26e64 -> :sswitch_12 */
/* -0x3486c9c0 -> :sswitch_11 */
/* -0x29b0eb8a -> :sswitch_10 */
/* -0x2911228e -> :sswitch_f */
/* -0x1cb94d1d -> :sswitch_e */
/* -0x19fff5ea -> :sswitch_d */
/* -0x11b05d5f -> :sswitch_c */
/* -0x1174fc77 -> :sswitch_b */
/* -0xa1b552 -> :sswitch_a */
/* 0xdd390e0 -> :sswitch_9 */
/* 0x1b3912c8 -> :sswitch_8 */
/* 0x3724844a -> :sswitch_7 */
/* 0x40eb2617 -> :sswitch_6 */
/* 0x466e54e3 -> :sswitch_5 */
/* 0x4cf72dbc -> :sswitch_4 */
/* 0x6a02e1e6 -> :sswitch_3 */
/* 0x6e9b58db -> :sswitch_2 */
/* 0x710adc10 -> :sswitch_1 */
/* 0x7818ed30 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_19 */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void registerMiuiBrightnessCloudDataObserver ( ) {
/* .locals 4 */
/* .line 339 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 340 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/display/MiuiDisplayCloudController$1; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/display/MiuiDisplayCloudController$1;-><init>(Lcom/android/server/display/MiuiDisplayCloudController;Landroid/os/Handler;)V */
/* .line 339 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 353 */
return;
} // .end method
private void saveAppListFromXml ( com.android.modules.utils.TypedXmlPullParser p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 2 */
/* .param p1, "parser" # Lcom/android/modules/utils/TypedXmlPullParser; */
/* .param p2, "attribute" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/modules/utils/TypedXmlPullParser;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 912 */
/* .local p3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v0 = 0; // const/4 v0, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 913 */
/* .line 915 */
} // :cond_0
return;
} // .end method
private void saveObjectAsListIfNeeded ( org.json.JSONObject p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 4 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "str" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lorg/json/JSONObject;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1209 */
/* .local p3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez p3, :cond_0 */
/* .line 1212 */
} // :cond_0
(( org.json.JSONObject ) p1 ).optJSONArray ( p2 ); // invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1213 */
/* .local v0, "appArray":Lorg/json/JSONArray; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1214 */
/* .line 1215 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v2, :cond_2 */
/* .line 1216 */
(( org.json.JSONArray ) v0 ).opt ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;
/* .line 1217 */
/* .local v2, "obj":Ljava/lang/Object; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1218 */
/* move-object v3, v2 */
/* check-cast v3, Ljava/lang/String; */
/* .line 1215 */
} // .end local v2 # "obj":Ljava/lang/Object;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
} // .end local v1 # "i":I
} // :cond_2
/* .line 1222 */
} // :cond_3
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
final String v2 = "Such category apps are removed."; // const-string v2, "Such category apps are removed."
android.util.Slog .d ( v1,v2 );
/* .line 1224 */
} // :goto_1
return;
/* .line 1210 */
} // .end local v0 # "appArray":Lorg/json/JSONArray;
} // :cond_4
} // :goto_2
return;
} // .end method
private void saveShortTermModelAppComponent ( org.json.JSONObject p0 ) {
/* .locals 9 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 1163 */
v0 = this.mShortTermModelCloudAppCategoryList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 1164 */
/* .local v1, "str":Ljava/lang/String; */
v2 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
int v3 = 0; // const/4 v3, 0x0
int v4 = 5; // const/4 v4, 0x5
int v5 = 4; // const/4 v5, 0x4
int v6 = 3; // const/4 v6, 0x3
int v7 = 2; // const/4 v7, 0x2
int v8 = 1; // const/4 v8, 0x1
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
/* const-string/jumbo v2, "short_term_model_global_app_list" */
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v4 */
/* :sswitch_1 */
/* const-string/jumbo v2, "short_term_model_image_app_list" */
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v6 */
/* :sswitch_2 */
/* const-string/jumbo v2, "short_term_model_reader_app_list" */
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v5 */
/* :sswitch_3 */
/* const-string/jumbo v2, "short_term_model_map_app_list" */
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v7 */
/* :sswitch_4 */
/* const-string/jumbo v2, "short_term_model_video_app_list" */
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v8 */
/* :sswitch_5 */
/* const-string/jumbo v2, "short_term_model_game_app_list" */
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v3 */
} // :goto_1
int v2 = -1; // const/4 v2, -0x1
} // :goto_2
/* packed-switch v2, :pswitch_data_0 */
/* .line 1191 */
/* :pswitch_0 */
v2 = this.mShortTermModelGlobalList;
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1192 */
v2 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v3 );
v4 = this.mShortTermModelGlobalList;
/* .line 1194 */
/* .line 1186 */
/* :pswitch_1 */
v2 = this.mShortTermModelReaderList;
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1187 */
v2 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v4 );
v4 = this.mShortTermModelReaderList;
/* .line 1189 */
/* .line 1181 */
/* :pswitch_2 */
v2 = this.mShortTermModelImageList;
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1182 */
v2 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v5 );
v4 = this.mShortTermModelImageList;
/* .line 1184 */
/* .line 1176 */
/* :pswitch_3 */
v2 = this.mShortTermModelMapList;
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1177 */
v2 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v6 );
v4 = this.mShortTermModelMapList;
/* .line 1179 */
/* .line 1171 */
/* :pswitch_4 */
v2 = this.mShortTermModelVideoList;
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1172 */
v2 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v7 );
v4 = this.mShortTermModelVideoList;
/* .line 1174 */
/* .line 1166 */
/* :pswitch_5 */
v2 = this.mShortTermModelGameList;
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1167 */
v2 = this.mShortTermModelAppMapper;
java.lang.Integer .valueOf ( v8 );
v4 = this.mShortTermModelGameList;
/* .line 1169 */
/* nop */
/* .line 1198 */
} // .end local v1 # "str":Ljava/lang/String;
} // :goto_3
/* goto/16 :goto_0 */
/* .line 1199 */
} // :cond_1
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6ae4e6dd -> :sswitch_5 */
/* -0x64b6c4fa -> :sswitch_4 */
/* -0x6066af3b -> :sswitch_3 */
/* 0x39d4f0f2 -> :sswitch_2 */
/* 0x64e82226 -> :sswitch_1 */
/* 0x78f38212 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void syncLocalBackupFromCloud ( ) {
/* .locals 36 */
/* .line 397 */
/* move-object/from16 v8, p0 */
final String v0 = "brightness_curve_optimize_policy_disable"; // const-string v0, "brightness_curve_optimize_policy_disable"
final String v9 = "custom_curve"; // const-string v9, "custom_curve"
final String v10 = "individual_model"; // const-string v10, "individual_model"
/* const-string/jumbo v11, "temperature_gap" */
/* const-string/jumbo v12, "threshold_sunlight_nit_value" */
final String v13 = "brightness_statistics_events"; // const-string v13, "brightness_statistics_events"
final String v14 = "outdoor_thermal_app_category"; // const-string v14, "outdoor_thermal_app_category"
final String v15 = "contactless_gesture"; // const-string v15, "contactless_gesture"
final String v7 = "rhythmic_app_category"; // const-string v7, "rhythmic_app_category"
/* const-string/jumbo v6, "touch_cover_protection_game" */
final String v5 = "disable_reset_short_term_model"; // const-string v5, "disable_reset_short_term_model"
final String v4 = "automatic_brightness_statistics_event_enable"; // const-string v4, "automatic_brightness_statistics_event_enable"
final String v3 = "override_brightness_policy_enable"; // const-string v3, "override_brightness_policy_enable"
final String v2 = "resolution_switch"; // const-string v2, "resolution_switch"
final String v1 = "bcbc"; // const-string v1, "bcbc"
/* move-object/from16 v16, v0 */
/* const-string/jumbo v0, "short-term" */
/* move-object/from16 v17, v9 */
final String v9 = "display-config"; // const-string v9, "display-config"
/* move-object/from16 v18, v10 */
final String v10 = "MiuiDisplayCloudController"; // const-string v10, "MiuiDisplayCloudController"
/* move-object/from16 v19, v11 */
final String v11 = "manual_boost_disable_app"; // const-string v11, "manual_boost_disable_app"
/* move-object/from16 v20, v1 */
v1 = this.mFile;
/* if-nez v1, :cond_0 */
/* .line 398 */
return;
/* .line 400 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 402 */
/* .local v1, "outputStream":Ljava/io/FileOutputStream; */
/* move-object/from16 v21, v1 */
} // .end local v1 # "outputStream":Ljava/io/FileOutputStream;
/* .local v21, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_0
final String v1 = "Start syncing local backup from cloud."; // const-string v1, "Start syncing local backup from cloud."
android.util.Slog .d ( v10,v1 );
/* .line 403 */
v1 = this.mFile;
(( android.util.AtomicFile ) v1 ).startWrite ( ); // invoke-virtual {v1}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_6 */
/* .line 404 */
} // .end local v21 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v1 # "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_1
android.util.Xml .resolveSerializer ( v1 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_5 */
/* move-object/from16 v22, v21 */
/* .line 405 */
/* .local v22, "out":Lcom/android/modules/utils/TypedXmlSerializer; */
/* move-object/from16 v21, v1 */
} // .end local v1 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v21 # "outputStream":Ljava/io/FileOutputStream; */
int v1 = 1; // const/4 v1, 0x1
/* move-object/from16 v23, v2 */
try { // :try_start_2
java.lang.Boolean .valueOf ( v1 );
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_4 */
int v1 = 0; // const/4 v1, 0x0
/* move-object/from16 v25, v10 */
/* move-object/from16 v10, v22 */
} // .end local v22 # "out":Lcom/android/modules/utils/TypedXmlSerializer;
/* .local v10, "out":Lcom/android/modules/utils/TypedXmlSerializer; */
try { // :try_start_3
/* .line 406 */
final String v2 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v2, "http://xmlpull.org/v1/doc/features.html#indent-output"
int v1 = 1; // const/4 v1, 0x1
/* .line 407 */
int v1 = 0; // const/4 v1, 0x0
/* .line 410 */
/* .line 411 */
v2 = this.mFile;
final String v22 = "enabled"; // const-string v22, "enabled"
/* const-string/jumbo v24, "short-term-model-enabled" */
/* move-object/from16 v26, v7 */
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_3 */
/* move-object/from16 v28, v12 */
/* move-object/from16 v27, v21 */
/* move-object v12, v1 */
/* move-object/from16 v35, v20 */
/* move-object/from16 v20, v9 */
/* move-object/from16 v9, v35 */
} // .end local v21 # "outputStream":Ljava/io/FileOutputStream;
/* .local v27, "outputStream":Ljava/io/FileOutputStream; */
/* move-object/from16 v1, p0 */
/* move-object/from16 v29, v23 */
/* move-object/from16 v30, v3 */
/* move-object/from16 v3, v27 */
/* move-object/from16 v31, v4 */
/* move-object v4, v10 */
/* move-object/from16 v32, v5 */
/* move-object/from16 v5, v22 */
/* move-object/from16 v33, v6 */
/* move-object/from16 v6, v24 */
/* move-object/from16 v34, v26 */
try { // :try_start_4
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 413 */
v1 = this.mFile;
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* move-object/from16 v7, v27 */
} // .end local v27 # "outputStream":Ljava/io/FileOutputStream;
/* .local v7, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_5
/* invoke-direct {v8, v1, v7, v10}, Lcom/android/server/display/MiuiDisplayCloudController;->writeShortTermModelAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;)V */
/* .line 414 */
/* .line 417 */
/* .line 418 */
v2 = this.mFile;
v5 = this.mBCBCAppList;
final String v6 = "package"; // const-string v6, "package"
final String v0 = "bcbc-app"; // const-string v0, "bcbc-app"
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_1 */
/* move-object/from16 v1, p0 */
/* move-object v3, v7 */
/* move-object v4, v10 */
/* move-object/from16 v27, v7 */
} // .end local v7 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v27 # "outputStream":Ljava/io/FileOutputStream; */
/* move-object v7, v0 */
try { // :try_start_6
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 420 */
/* .line 423 */
/* move-object/from16 v0, v29 */
/* .line 424 */
v2 = this.mFile;
v5 = this.mResolutionSwitchProcessProtectList;
final String v6 = "item"; // const-string v6, "item"
final String v7 = "resolution_switch_process_protect"; // const-string v7, "resolution_switch_process_protect"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 426 */
v2 = this.mFile;
v5 = this.mResolutionSwitchProcessBlackList;
final String v6 = "item"; // const-string v6, "item"
final String v7 = "resolution_switch_process_black"; // const-string v7, "resolution_switch_process_black"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 428 */
/* .line 440 */
/* move-object/from16 v0, v30 */
/* .line 441 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "override-brightness-policy-enable"; // const-string v6, "override-brightness-policy-enable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z */
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 443 */
/* .line 446 */
/* move-object/from16 v0, v31 */
/* .line 447 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "automatic-brightness-statistics-event-enable"; // const-string v6, "automatic-brightness-statistics-event-enable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z */
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 449 */
/* .line 452 */
/* move-object/from16 v0, v32 */
/* .line 453 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "disable-reset-short-term-model"; // const-string v6, "disable-reset-short-term-model"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z */
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 455 */
/* .line 458 */
/* move-object/from16 v0, v33 */
/* .line 459 */
v2 = this.mFile;
v5 = this.mTouchCoverProtectionGameList;
final String v6 = "package"; // const-string v6, "package"
/* const-string/jumbo v7, "touch_cover_protection_game_app" */
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 461 */
/* .line 464 */
/* move-object/from16 v0, v34 */
/* .line 465 */
v2 = this.mFile;
v5 = this.mRhythmicImageAppList;
final String v6 = "item"; // const-string v6, "item"
final String v7 = "rhythmic_app_category_image"; // const-string v7, "rhythmic_app_category_image"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 467 */
v2 = this.mFile;
v5 = this.mRhythmicReadAppList;
final String v6 = "item"; // const-string v6, "item"
final String v7 = "rhythmic_app_category_read"; // const-string v7, "rhythmic_app_category_read"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 469 */
/* .line 472 */
/* .line 473 */
v2 = this.mFile;
v5 = this.mGestureComponents;
final String v6 = "package"; // const-string v6, "package"
final String v7 = "gesture_component"; // const-string v7, "gesture_component"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 475 */
/* .line 478 */
/* .line 479 */
v2 = this.mFile;
v5 = this.mOutdoorThermalAppList;
final String v6 = "item"; // const-string v6, "item"
final String v7 = "outdoor_thermal_app_category_list"; // const-string v7, "outdoor_thermal_app_category_list"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 481 */
/* .line 484 */
/* .line 485 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "manual-boost-app-enable"; // const-string v6, "manual-boost-app-enable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z */
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 487 */
/* .line 490 */
/* .line 491 */
v2 = this.mFile;
v5 = this.mManualBoostDisableAppList;
final String v6 = "package"; // const-string v6, "package"
final String v7 = "manual-boost-disable-app-list"; // const-string v7, "manual-boost-disable-app-list"
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 493 */
/* .line 496 */
/* .line 497 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "brightness-statistics-events-enable"; // const-string v6, "brightness-statistics-events-enable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z */
/* move-object/from16 v1, p0 */
/* move-object/from16 v3, v27 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 499 */
v0 = this.mFile;
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_2 */
/* move-object/from16 v9, v27 */
} // .end local v27 # "outputStream":Ljava/io/FileOutputStream;
/* .local v9, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_7
/* invoke-direct {v8, v0, v9, v10}, Lcom/android/server/display/MiuiDisplayCloudController;->writeShortTermModelAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;)V */
/* .line 500 */
/* .line 503 */
/* move-object/from16 v0, v28 */
/* .line 504 */
v2 = this.mFile;
/* const-string/jumbo v5, "value" */
/* const-string/jumbo v6, "threshold-sunlight-nit-value" */
/* iget v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F */
/* .line 505 */
java.lang.Float .valueOf ( v1 );
/* .line 504 */
/* move-object/from16 v1, p0 */
/* move-object v3, v9 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureValueToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Number;)V */
/* .line 506 */
/* .line 509 */
/* move-object/from16 v0, v19 */
/* .line 510 */
v2 = this.mFile;
/* const-string/jumbo v5, "value" */
/* const-string/jumbo v6, "temperature-gap-value" */
/* iget v1, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F */
/* .line 511 */
java.lang.Float .valueOf ( v1 );
/* .line 510 */
/* move-object/from16 v1, p0 */
/* move-object v3, v9 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureValueToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Number;)V */
/* .line 512 */
/* .line 515 */
/* move-object/from16 v0, v18 */
/* .line 516 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "individual-model-disable"; // const-string v6, "individual-model-disable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z */
/* move-object/from16 v1, p0 */
/* move-object v3, v9 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 518 */
/* .line 521 */
/* move-object/from16 v0, v17 */
/* .line 522 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "custom-curve-disable"; // const-string v6, "custom-curve-disable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z */
/* move-object/from16 v1, p0 */
/* move-object v3, v9 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 524 */
/* .line 527 */
/* move-object/from16 v0, v16 */
/* .line 528 */
v2 = this.mFile;
final String v5 = "enabled"; // const-string v5, "enabled"
final String v6 = "brightness_curve_optimize_policy_disable"; // const-string v6, "brightness_curve_optimize_policy_disable"
/* iget-boolean v7, v8, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z */
/* move-object/from16 v1, p0 */
/* move-object v3, v9 */
/* move-object v4, v10 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/display/MiuiDisplayCloudController;->writeFeatureEnableToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 530 */
/* .line 532 */
/* move-object/from16 v0, v20 */
/* .line 533 */
/* .line 534 */
(( java.io.FileOutputStream ) v9 ).flush ( ); // invoke-virtual {v9}, Ljava/io/FileOutputStream;->flush()V
/* .line 535 */
v0 = this.mFile;
(( android.util.AtomicFile ) v0 ).finishWrite ( v9 ); // invoke-virtual {v0, v9}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_0 */
/* .line 539 */
} // .end local v10 # "out":Lcom/android/modules/utils/TypedXmlSerializer;
/* move-object v1, v9 */
/* .line 536 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v1, v9 */
} // .end local v9 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v7 # "outputStream":Ljava/io/FileOutputStream; */
/* :catch_1 */
/* move-exception v0 */
/* move-object v9, v7 */
/* move-object v1, v9 */
} // .end local v7 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v9 # "outputStream":Ljava/io/FileOutputStream; */
} // .end local v9 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v27 # "outputStream":Ljava/io/FileOutputStream; */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v9, v27 */
/* move-object v1, v9 */
} // .end local v27 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v9 # "outputStream":Ljava/io/FileOutputStream; */
} // .end local v9 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v21 # "outputStream":Ljava/io/FileOutputStream; */
/* :catch_3 */
/* move-exception v0 */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v25, v10 */
} // :goto_0
/* move-object/from16 v9, v21 */
/* move-object v1, v9 */
} // .end local v21 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v9 # "outputStream":Ljava/io/FileOutputStream; */
} // .end local v9 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v1 # "outputStream":Ljava/io/FileOutputStream; */
/* :catch_5 */
/* move-exception v0 */
/* move-object v9, v1 */
/* move-object/from16 v25, v10 */
} // .end local v1 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v9 # "outputStream":Ljava/io/FileOutputStream; */
} // .end local v9 # "outputStream":Ljava/io/FileOutputStream;
/* .restart local v21 # "outputStream":Ljava/io/FileOutputStream; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v25, v10 */
/* move-object/from16 v1, v21 */
/* .line 537 */
} // .end local v21 # "outputStream":Ljava/io/FileOutputStream;
/* .local v0, "e":Ljava/io/IOException; */
/* .restart local v1 # "outputStream":Ljava/io/FileOutputStream; */
} // :goto_1
v2 = this.mFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 538 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to write local backup"; // const-string v3, "Failed to write local backup"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object/from16 v3, v25 */
android.util.Slog .e ( v3,v2 );
/* .line 540 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
private Boolean updateAutoBrightnessStatisticsEventEnableState ( ) {
/* .locals 1 */
/* .line 1098 */
/* sget-boolean v0, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1100 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1102 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateStatisticsAutoBrightnessEventEnable()Z */
} // .end method
private Boolean updateBCBCAppList ( ) {
/* .locals 6 */
/* .line 1293 */
v0 = this.mContext;
/* .line 1294 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "BCBCFeature"; // const-string v1, "BCBCFeature"
final String v2 = "bcbc_app_config"; // const-string v2, "bcbc_app_config"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1296 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1297 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1298 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1299 */
v3 = this.mBCBCAppList;
/* .line 1300 */
final String v3 = "Update BCBC apps."; // const-string v3, "Update BCBC apps."
android.util.Slog .d ( v1,v3 );
/* .line 1301 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v3, :cond_1 */
/* .line 1302 */
(( org.json.JSONArray ) v2 ).opt ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;
/* .line 1303 */
/* .local v3, "obj":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1304 */
v4 = this.mBCBCAppList;
/* move-object v5, v3 */
/* check-cast v5, Ljava/lang/String; */
/* .line 1301 */
} // .end local v3 # "obj":Ljava/lang/Object;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1307 */
} // .end local v1 # "i":I
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 1309 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // :cond_2
/* .line 1310 */
} // :cond_3
final String v2 = "Failed to update BCBC apps from cloud."; // const-string v2, "Failed to update BCBC apps from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1312 */
} // :goto_1
} // .end method
private Boolean updateBrightnessCurveOptimizePolicyDisable ( ) {
/* .locals 5 */
/* .line 1867 */
v0 = this.mContext;
/* .line 1868 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "BrightnessCurveOptimizePolicy"; // const-string v1, "BrightnessCurveOptimizePolicy"
final String v2 = "brightness_curve_optimize_policy_disable"; // const-string v2, "brightness_curve_optimize_policy_disable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1870 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1871 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v1 = (( org.json.JSONObject ) v1 ).optBoolean ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z */
/* .line 1873 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1875 */
} // :cond_0
} // .end method
private Boolean updateBrightnessStatsEventsEnable ( ) {
/* .locals 6 */
/* .line 1465 */
v0 = this.mContext;
/* .line 1466 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "brightnessStatisticsEvents"; // const-string v1, "brightnessStatisticsEvents"
final String v2 = "brightness_statistics_events_enable"; // const-string v2, "brightness_statistics_events_enable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1468 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1469 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z */
/* .line 1470 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1471 */
/* iget-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v4, 0x1 */
/* or-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 1473 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update brightness statistics events: "; // const-string v3, "Update brightness statistics events: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1474 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1476 */
} // :cond_1
final String v2 = "Failed to update brightness statistics events switch from cloud."; // const-string v2, "Failed to update brightness statistics events switch from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1477 */
} // .end method
private Boolean updateContactlessGestureConfig ( ) {
/* .locals 6 */
/* .line 1622 */
v0 = this.mContext;
/* .line 1623 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "ContactlessGestureFeature"; // const-string v1, "ContactlessGestureFeature"
final String v2 = "gestureComponents"; // const-string v2, "gestureComponents"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1625 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1626 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1627 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1628 */
v3 = this.mGestureComponents;
/* .line 1629 */
final String v3 = "Update contactless gesture config."; // const-string v3, "Update contactless gesture config."
android.util.Slog .d ( v1,v3 );
/* .line 1630 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v3, :cond_1 */
/* .line 1631 */
(( org.json.JSONArray ) v2 ).opt ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;
/* .line 1632 */
/* .local v3, "obj":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1633 */
v4 = this.mGestureComponents;
/* move-object v5, v3 */
/* check-cast v5, Ljava/lang/String; */
/* .line 1630 */
} // .end local v3 # "obj":Ljava/lang/Object;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1636 */
} // .end local v1 # "i":I
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyContactlessGestureConfigChanged()V */
/* .line 1637 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1639 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // :cond_2
/* .line 1640 */
} // :cond_3
final String v2 = "Failed to update contactless gesture config."; // const-string v2, "Failed to update contactless gesture config."
android.util.Slog .w ( v1,v2 );
/* .line 1642 */
} // :goto_1
} // .end method
private Boolean updateCustomBrightnessAppConfig ( ) {
/* .locals 21 */
/* .line 1752 */
/* move-object/from16 v1, p0 */
final String v0 = "name"; // const-string v0, "name"
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
v3 = this.mContext;
/* .line 1753 */
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "IndividualAppCategoryConfig"; // const-string v4, "IndividualAppCategoryConfig"
final String v5 = "app-category-config"; // const-string v5, "app-category-config"
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v3,v4,v5,v6,v7 );
/* .line 1757 */
/* .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v3 != null) { // if-eqz v3, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).json ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1759 */
try { // :try_start_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).json ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 1760 */
/* .local v4, "jsonObject":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v4 ).getJSONObject ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 1761 */
/* .local v5, "rootObject":Lorg/json/JSONObject; */
/* new-instance v6, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
/* invoke-direct {v6}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;-><init>()V */
/* .line 1762 */
/* .local v6, "config":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
(( com.android.server.display.aiautobrt.config.AppCategoryConfig ) v6 ).getCategory ( ); // invoke-virtual {v6}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;->getCategory()Ljava/util/List;
/* .line 1763 */
/* .local v8, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/AppCategory;>;" */
final String v9 = "category"; // const-string v9, "category"
(( org.json.JSONObject ) v5 ).getJSONArray ( v9 ); // invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1764 */
/* .local v9, "jsonCategoryArray":Lorg/json/JSONArray; */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "i":I */
} // :goto_0
v11 = (( org.json.JSONArray ) v9 ).length ( ); // invoke-virtual {v9}, Lorg/json/JSONArray;->length()I
/* if-ge v10, v11, :cond_1 */
/* .line 1765 */
/* new-instance v11, Lcom/android/server/display/aiautobrt/config/AppCategory; */
/* invoke-direct {v11}, Lcom/android/server/display/aiautobrt/config/AppCategory;-><init>()V */
/* .line 1766 */
/* .local v11, "category":Lcom/android/server/display/aiautobrt/config/AppCategory; */
(( com.android.server.display.aiautobrt.config.AppCategory ) v11 ).getPkg ( ); // invoke-virtual {v11}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getPkg()Ljava/util/List;
/* .line 1767 */
/* .local v12, "pkgInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/PackageInfo;>;" */
(( org.json.JSONArray ) v9 ).getJSONObject ( v10 ); // invoke-virtual {v9, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 1768 */
/* .local v13, "categoryItem":Lorg/json/JSONObject; */
final String v14 = "id"; // const-string v14, "id"
v14 = (( org.json.JSONObject ) v13 ).getInt ( v14 ); // invoke-virtual {v13, v14}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 1769 */
/* .local v14, "categoryId":I */
(( org.json.JSONObject ) v13 ).getString ( v0 ); // invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 1770 */
/* .local v15, "categoryName":Ljava/lang/String; */
final String v7 = "pkg"; // const-string v7, "pkg"
(( org.json.JSONObject ) v13 ).getJSONArray ( v7 ); // invoke-virtual {v13, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 1771 */
/* .local v7, "pkgArray":Lorg/json/JSONArray; */
/* const/16 v16, 0x0 */
/* move-object/from16 v17, v3 */
/* move/from16 v3, v16 */
/* .local v3, "j":I */
/* .local v17, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
} // :goto_1
/* move-object/from16 v16, v5 */
} // .end local v5 # "rootObject":Lorg/json/JSONObject;
/* .local v16, "rootObject":Lorg/json/JSONObject; */
try { // :try_start_1
v5 = (( org.json.JSONArray ) v7 ).length ( ); // invoke-virtual {v7}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v5, :cond_0 */
/* .line 1772 */
(( org.json.JSONArray ) v7 ).getJSONObject ( v3 ); // invoke-virtual {v7, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 1773 */
/* .local v5, "jsonPkgInfoItem":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).optString ( v0 ); // invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* move-object/from16 v19, v18 */
/* .line 1774 */
/* .local v19, "pkgName":Ljava/lang/String; */
/* new-instance v18, Lcom/android/server/display/aiautobrt/config/PackageInfo; */
/* invoke-direct/range {v18 ..v18}, Lcom/android/server/display/aiautobrt/config/PackageInfo;-><init>()V */
/* move-object/from16 v20, v18 */
/* .line 1775 */
/* .local v20, "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo; */
/* move-object/from16 v18, v0 */
/* move-object/from16 v0, v20 */
} // .end local v20 # "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
/* .local v0, "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo; */
(( com.android.server.display.aiautobrt.config.PackageInfo ) v0 ).setCateId ( v14 ); // invoke-virtual {v0, v14}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setCateId(I)V
/* .line 1776 */
/* move-object/from16 v20, v5 */
/* move-object/from16 v5, v19 */
} // .end local v19 # "pkgName":Ljava/lang/String;
/* .local v5, "pkgName":Ljava/lang/String; */
/* .local v20, "jsonPkgInfoItem":Lorg/json/JSONObject; */
(( com.android.server.display.aiautobrt.config.PackageInfo ) v0 ).setName ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->setName(Ljava/lang/String;)V
/* .line 1777 */
/* .line 1771 */
/* nop */
} // .end local v0 # "pkgInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
} // .end local v5 # "pkgName":Ljava/lang/String;
} // .end local v20 # "jsonPkgInfoItem":Lorg/json/JSONObject;
/* add-int/lit8 v3, v3, 0x1 */
/* move-object/from16 v5, v16 */
/* move-object/from16 v0, v18 */
} // :cond_0
/* move-object/from16 v18, v0 */
/* .line 1779 */
} // .end local v3 # "j":I
(( com.android.server.display.aiautobrt.config.AppCategory ) v11 ).setId ( v14 ); // invoke-virtual {v11, v14}, Lcom/android/server/display/aiautobrt/config/AppCategory;->setId(I)V
/* .line 1780 */
(( com.android.server.display.aiautobrt.config.AppCategory ) v11 ).setName ( v15 ); // invoke-virtual {v11, v15}, Lcom/android/server/display/aiautobrt/config/AppCategory;->setName(Ljava/lang/String;)V
/* .line 1781 */
/* .line 1764 */
/* nop */
} // .end local v7 # "pkgArray":Lorg/json/JSONArray;
} // .end local v11 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
} // .end local v12 # "pkgInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/PackageInfo;>;"
} // .end local v13 # "categoryItem":Lorg/json/JSONObject;
} // .end local v14 # "categoryId":I
} // .end local v15 # "categoryName":Ljava/lang/String;
/* add-int/lit8 v10, v10, 0x1 */
/* move-object/from16 v5, v16 */
/* move-object/from16 v3, v17 */
/* move-object/from16 v0, v18 */
int v7 = 0; // const/4 v7, 0x0
} // .end local v16 # "rootObject":Lorg/json/JSONObject;
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .local v5, "rootObject":Lorg/json/JSONObject; */
} // :cond_1
/* move-object/from16 v17, v3 */
/* move-object/from16 v16, v5 */
/* .line 1783 */
} // .end local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v5 # "rootObject":Lorg/json/JSONObject;
} // .end local v10 # "i":I
/* .restart local v16 # "rootObject":Lorg/json/JSONObject; */
/* .restart local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* sget-boolean v0, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1784 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update custom app category config json: "; // const-string v3, "Update custom app category config json: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1786 */
} // :cond_2
/* invoke-direct {v1, v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeAppCategoryConfigToFile(Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;)V */
/* .line 1787 */
/* iget-wide v10, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v12, 0x8 */
/* or-long/2addr v10, v12 */
/* iput-wide v10, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1788 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1789 */
} // .end local v4 # "jsonObject":Lorg/json/JSONObject;
} // .end local v6 # "config":Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;
} // .end local v8 # "categories":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/aiautobrt/config/AppCategory;>;"
} // .end local v9 # "jsonCategoryArray":Lorg/json/JSONArray;
} // .end local v16 # "rootObject":Lorg/json/JSONObject;
/* :catch_0 */
/* move-exception v0 */
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .restart local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v17, v3 */
/* .line 1790 */
} // .end local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .local v0, "e":Lorg/json/JSONException; */
/* .restart local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
} // :goto_2
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update custom app category config exception: "; // const-string v4, "Update custom app category config exception: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 1791 */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 1757 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .restart local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
} // :cond_3
/* move-object/from16 v17, v3 */
/* .line 1794 */
} // .end local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .restart local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
} // :goto_3
int v2 = 0; // const/4 v2, 0x0
} // .end method
private Boolean updateCustomCurveDisable ( ) {
/* .locals 6 */
/* .line 1844 */
v0 = this.mContext;
/* .line 1845 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "CustomCurveDisable"; // const-string v1, "CustomCurveDisable"
final String v2 = "custom_curve_disable"; // const-string v2, "custom_curve_disable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1848 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1849 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z */
/* .line 1850 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1851 */
/* iget-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v4, 0x10 */
/* or-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 1853 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update custom curve disable: "; // const-string v3, "Update custom curve disable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1854 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1856 */
} // :cond_1
final String v2 = "Failed to upload custom curve disable from cloud."; // const-string v2, "Failed to upload custom curve disable from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1857 */
} // .end method
private Boolean updateDataFromCloudControl ( ) {
/* .locals 3 */
/* .line 924 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 925 */
v0 = this.mCloudEventsData;
/* .line 926 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateShortTermModelState()Z */
/* .line 927 */
/* .local v0, "updated":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateBCBCAppList()Z */
/* or-int/2addr v0, v1 */
/* .line 928 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateResolutionSwitchList()Z */
/* or-int/2addr v0, v1 */
/* .line 929 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateTouchProtectionGameList()Z */
/* or-int/2addr v0, v1 */
/* .line 930 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateManualBoostDisableAppList()Z */
/* or-int/2addr v0, v1 */
/* .line 931 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateManualBoostAppEnable()Z */
/* or-int/2addr v0, v1 */
/* .line 932 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateOutdoorThermalAppList()Z */
/* or-int/2addr v0, v1 */
/* .line 933 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateOverrideBrightnessPolicyEnable()Z */
/* or-int/2addr v0, v1 */
/* .line 934 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateAutoBrightnessStatisticsEventEnableState()Z */
/* or-int/2addr v0, v1 */
/* .line 935 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateDisableResetShortTermModelState()Z */
/* or-int/2addr v0, v1 */
/* .line 936 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateRhythmicAppCategoryList()Z */
/* or-int/2addr v0, v1 */
/* .line 937 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateContactlessGestureConfig()Z */
/* or-int/2addr v0, v1 */
/* .line 938 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateBrightnessStatsEventsEnable()Z */
/* or-int/2addr v0, v1 */
/* .line 939 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateThermalBrightnessConfig()Z */
/* or-int/2addr v0, v1 */
/* .line 940 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateThresholdSunlightNitValue()Z */
/* or-int/2addr v0, v1 */
/* .line 941 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateTemperatureGap()Z */
/* or-int/2addr v0, v1 */
/* .line 942 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateCustomBrightnessAppConfig()Z */
/* or-int/2addr v0, v1 */
/* .line 943 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateCustomCurveDisable()Z */
/* or-int/2addr v0, v1 */
/* .line 944 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateIndividualModelDisable()Z */
/* or-int/2addr v0, v1 */
/* .line 945 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateBrightnessCurveOptimizePolicyDisable()Z */
/* or-int/2addr v0, v1 */
/* .line 946 */
v1 = this.mCloudListeners;
/* new-instance v2, Lcom/android/server/display/MiuiDisplayCloudController$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/display/MiuiDisplayCloudController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/MiuiDisplayCloudController;)V */
/* .line 949 */
} // .end method
private Boolean updateDisableResetShortTermModel ( ) {
/* .locals 5 */
/* .line 1143 */
v0 = this.mContext;
/* .line 1144 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "DisableResetShortTermModel"; // const-string v1, "DisableResetShortTermModel"
final String v2 = "disable_reset_short_term_model"; // const-string v2, "disable_reset_short_term_model"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1147 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1148 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v1 = (( org.json.JSONObject ) v1 ).optBoolean ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z */
/* .line 1150 */
v2 = this.mCallback;
/* .line 1151 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1153 */
} // :cond_0
} // .end method
private Boolean updateDisableResetShortTermModelState ( ) {
/* .locals 1 */
/* .line 1134 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateDisableResetShortTermModel()Z */
} // .end method
private Boolean updateIndividualModelDisable ( ) {
/* .locals 6 */
/* .line 1883 */
v0 = this.mContext;
/* .line 1884 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "IndividualModelDisable"; // const-string v1, "IndividualModelDisable"
final String v2 = "individual_model_disable"; // const-string v2, "individual_model_disable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1887 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1888 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z */
/* .line 1889 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1890 */
/* iget-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v4, 0x20 */
/* or-long/2addr v2, v4 */
/* iput-wide v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 1892 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update individual model disable: "; // const-string v3, "Update individual model disable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1893 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1895 */
} // :cond_1
final String v2 = "Failed to upload individual model disable from cloud."; // const-string v2, "Failed to upload individual model disable from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1896 */
} // .end method
private Boolean updateManualBoostAppEnable ( ) {
/* .locals 5 */
/* .line 1347 */
v0 = this.mContext;
/* .line 1348 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "ManualBoostAppEnable"; // const-string v1, "ManualBoostAppEnable"
final String v2 = "manual_boost_app_enable"; // const-string v2, "manual_boost_app_enable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1350 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1351 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z */
/* .line 1352 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update manual boost app enable: "; // const-string v3, "Update manual boost app enable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1353 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1355 */
} // :cond_0
final String v2 = "Failed to update manual boost app enable from cloud."; // const-string v2, "Failed to update manual boost app enable from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1356 */
} // .end method
private Boolean updateManualBoostDisableAppList ( ) {
/* .locals 6 */
/* .line 1385 */
v0 = this.mContext;
/* .line 1386 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "ManualBoostDisableAppList"; // const-string v1, "ManualBoostDisableAppList"
final String v2 = "manual_boost_disable_app_list"; // const-string v2, "manual_boost_disable_app_list"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1388 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1389 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1390 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1391 */
v3 = this.mManualBoostDisableAppList;
/* .line 1392 */
final String v3 = "Update manual brightness boost disable apps."; // const-string v3, "Update manual brightness boost disable apps."
android.util.Slog .d ( v1,v3 );
/* .line 1393 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v3, :cond_1 */
/* .line 1394 */
(( org.json.JSONArray ) v2 ).opt ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;
/* .line 1395 */
/* .local v3, "obj":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1396 */
v4 = this.mManualBoostDisableAppList;
/* move-object v5, v3 */
/* check-cast v5, Ljava/lang/String; */
/* .line 1393 */
} // .end local v3 # "obj":Ljava/lang/Object;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1399 */
} // .end local v1 # "i":I
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 1401 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // :cond_2
/* .line 1402 */
} // :cond_3
final String v2 = "Failed to update manual brightness boost disable apps from cloud."; // const-string v2, "Failed to update manual brightness boost disable apps from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1404 */
} // :goto_1
} // .end method
private Boolean updateOutdoorThermalAppList ( ) {
/* .locals 6 */
/* .line 1361 */
v0 = this.mContext;
/* .line 1362 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "outdoorThermalAppList"; // const-string v1, "outdoorThermalAppList"
final String v2 = "outdoor_thermal_app_list"; // const-string v2, "outdoor_thermal_app_list"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1364 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1365 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1366 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1367 */
v3 = this.mOutdoorThermalAppList;
/* .line 1368 */
final String v3 = "Update thermal brightness boost enable apps."; // const-string v3, "Update thermal brightness boost enable apps."
android.util.Slog .d ( v1,v3 );
/* .line 1369 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v3, :cond_1 */
/* .line 1370 */
(( org.json.JSONArray ) v2 ).opt ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;
/* .line 1371 */
/* .local v3, "obj":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1372 */
v4 = this.mOutdoorThermalAppList;
/* move-object v5, v3 */
/* check-cast v5, Ljava/lang/String; */
/* .line 1369 */
} // .end local v3 # "obj":Ljava/lang/Object;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1375 */
} // .end local v1 # "i":I
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyOutdoorThermalAppCategoryListChanged()V */
/* .line 1376 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1378 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // :cond_2
/* .line 1379 */
} // :cond_3
final String v2 = "Failed to update thermal brightness boost enable apps from cloud."; // const-string v2, "Failed to update thermal brightness boost enable apps from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1381 */
} // :goto_1
} // .end method
private Boolean updateOverrideBrightnessPolicyEnable ( ) {
/* .locals 5 */
/* .line 1446 */
v0 = this.mContext;
/* .line 1447 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "overrideBrightnessPolicy"; // const-string v1, "overrideBrightnessPolicy"
final String v2 = "override_brightness_policy_enable"; // const-string v2, "override_brightness_policy_enable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1449 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1450 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z */
/* .line 1451 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update override brightness policy enable: "; // const-string v3, "Update override brightness policy enable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1452 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1454 */
} // :cond_0
final String v2 = "Failed to update override brightness policy enable from cloud."; // const-string v2, "Failed to update override brightness policy enable from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1455 */
} // .end method
private Boolean updateResolutionSwitchList ( ) {
/* .locals 5 */
/* .line 1316 */
v0 = this.mContext;
/* .line 1317 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "resolutionSwitchProcessList"; // const-string v1, "resolutionSwitchProcessList"
final String v2 = "process_resolution_switch_list"; // const-string v2, "process_resolution_switch_list"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1320 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1321 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1322 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1323 */
(( org.json.JSONArray ) v2 ).optJSONObject ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* .line 1324 */
/* .local v3, "jsonObject":Lorg/json/JSONObject; */
final String v4 = "Update Resolution switch list from cloud"; // const-string v4, "Update Resolution switch list from cloud"
android.util.Slog .d ( v1,v4 );
/* .line 1325 */
final String v1 = "process_resolution_switch_protect_list"; // const-string v1, "process_resolution_switch_protect_list"
v4 = this.mResolutionSwitchProcessProtectList;
/* invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1327 */
final String v1 = "process_resolution_switch_black_list"; // const-string v1, "process_resolution_switch_black_list"
v4 = this.mResolutionSwitchProcessBlackList;
/* invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1329 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyResolutionSwitchListChanged()V */
/* .line 1330 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1332 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // .end local v3 # "jsonObject":Lorg/json/JSONObject;
} // :cond_0
/* .line 1333 */
} // :cond_1
final String v2 = "Failed to update Resolution switch list from cloud."; // const-string v2, "Failed to update Resolution switch list from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1335 */
} // :goto_0
} // .end method
private Boolean updateRhythmicAppCategoryList ( ) {
/* .locals 5 */
/* .line 1657 */
v0 = this.mContext;
/* .line 1658 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "rhythmicAppCategoryList"; // const-string v1, "rhythmicAppCategoryList"
final String v2 = "rhythmic_app_category_list"; // const-string v2, "rhythmic_app_category_list"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1661 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1662 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1663 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1664 */
(( org.json.JSONArray ) v2 ).optJSONObject ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* .line 1665 */
/* .local v3, "jsonObject":Lorg/json/JSONObject; */
final String v4 = "Update app category list from cloud"; // const-string v4, "Update app category list from cloud"
android.util.Slog .d ( v1,v4 );
/* .line 1667 */
final String v1 = "rhythmic_app_category_image_list"; // const-string v1, "rhythmic_app_category_image_list"
v4 = this.mRhythmicImageAppList;
/* invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1668 */
final String v1 = "rhythmic_app_category_read_list"; // const-string v1, "rhythmic_app_category_read_list"
v4 = this.mRhythmicReadAppList;
/* invoke-direct {p0, v3, v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->saveObjectAsListIfNeeded(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V */
/* .line 1669 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->notifyRhythmicAppCategoryListChanged()V */
/* .line 1670 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1672 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // .end local v3 # "jsonObject":Lorg/json/JSONObject;
} // :cond_0
/* .line 1673 */
} // :cond_1
final String v2 = "Failed to update app category list from cloud."; // const-string v2, "Failed to update app category list from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1675 */
} // :goto_0
} // .end method
private Boolean updateShortTermModelAppList ( ) {
/* .locals 5 */
/* .line 1056 */
v0 = this.mContext;
/* .line 1057 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "shortTermModelAppList" */
/* const-string/jumbo v2, "short_term_model_app_config" */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1059 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1060 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1061 */
/* .local v2, "jsonArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1062 */
(( org.json.JSONArray ) v2 ).optJSONObject ( v4 ); // invoke-virtual {v2, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* .line 1063 */
/* .local v3, "jsonObject":Lorg/json/JSONObject; */
final String v4 = "Update short term model apps."; // const-string v4, "Update short term model apps."
android.util.Slog .d ( v1,v4 );
/* .line 1064 */
/* invoke-direct {p0, v3}, Lcom/android/server/display/MiuiDisplayCloudController;->saveShortTermModelAppComponent(Lorg/json/JSONObject;)V */
/* .line 1066 */
} // .end local v3 # "jsonObject":Lorg/json/JSONObject;
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 1068 */
} // .end local v2 # "jsonArray":Lorg/json/JSONArray;
} // :cond_1
final String v2 = "Failed to update short term model apps from cloud."; // const-string v2, "Failed to update short term model apps from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1069 */
} // .end method
private Boolean updateShortTermModelEnable ( ) {
/* .locals 5 */
/* .line 1079 */
v0 = this.mContext;
/* .line 1080 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "shortTermModel" */
/* const-string/jumbo v2, "short_term_model_enable" */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1082 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1083 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
/* .line 1084 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update short term model enable: "; // const-string v3, "Update short term model enable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1085 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1087 */
} // :cond_0
final String v2 = "Failed to update short term model enable from cloud."; // const-string v2, "Failed to update short term model enable from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1088 */
} // .end method
private Boolean updateShortTermModelState ( ) {
/* .locals 2 */
/* .line 1042 */
/* sget-boolean v0, Lcom/android/server/display/MiuiDisplayCloudController;->DEBUG:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1044 */
/* .line 1046 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateShortTermModelEnable()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/display/MiuiDisplayCloudController;->updateShortTermModelAppList()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private Boolean updateStatisticsAutoBrightnessEventEnable ( ) {
/* .locals 5 */
/* .line 1112 */
v0 = this.mContext;
/* .line 1113 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "AutomaticBrightnessStatisticsEvent"; // const-string v1, "AutomaticBrightnessStatisticsEvent"
final String v2 = "automatic_brightness_statistics_event_enable"; // const-string v2, "automatic_brightness_statistics_event_enable"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1116 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1117 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
v2 = (( org.json.JSONObject ) v3 ).optBoolean ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z */
/* .line 1119 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update automatic brightness statistics event enable: "; // const-string v3, "Update automatic brightness statistics event enable: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1121 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1123 */
} // :cond_0
final String v2 = "Failed to upload automatic brightness statistics event enable from cloud."; // const-string v2, "Failed to upload automatic brightness statistics event enable from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1124 */
} // .end method
private Boolean updateTemperatureGap ( ) {
/* .locals 9 */
/* .line 1601 */
/* const-string/jumbo v0, "temperature_gap_value" */
final String v1 = "TemperatureGap"; // const-string v1, "TemperatureGap"
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = this.mContext;
/* .line 1602 */
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v5 = 0; // const/4 v5, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v4,v1,v0,v5,v3 );
/* .line 1604 */
/* .local v4, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v4 != null) { // if-eqz v4, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v4 ).json ( ); // invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1605 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v4 ).json ( ); // invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v5 ).getDouble ( v0 ); // invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v5 */
/* double-to-float v0, v5 */
/* iput v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F */
/* .line 1606 */
/* iget-wide v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v7, 0x4 */
/* or-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 1607 */
v5 = this.mCloudEventsData;
java.lang.Float .valueOf ( v0 );
/* .line 1608 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Update temperature gap : "; // const-string v1, "Update temperature gap : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mTemperatureGap:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1609 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1613 */
} // .end local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // :cond_0
/* .line 1611 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1612 */
/* .local v0, "e":Lorg/json/JSONException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update temperature gap exception: "; // const-string v4, "Update temperature gap exception: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1614 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_0
} // .end method
private Boolean updateThermalBrightnessConfig ( ) {
/* .locals 22 */
/* .line 1549 */
/* move-object/from16 v1, p0 */
/* const-string/jumbo v0, "thermal-brightness-config" */
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = this.mContext;
/* .line 1550 */
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v5, "temperature_control" */
int v6 = 0; // const/4 v6, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v4,v5,v0,v6,v3 );
/* .line 1553 */
/* .local v4, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v4 != null) { // if-eqz v4, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v4 ).json ( ); // invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 1554 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v4 ).json ( ); // invoke-virtual {v4}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 1555 */
/* .local v5, "jsonObject":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v5 ).getJSONObject ( v0 ); // invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 1556 */
/* .local v0, "rootObject":Lorg/json/JSONObject; */
/* new-instance v6, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
/* invoke-direct {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;-><init>()V */
/* .line 1557 */
/* .local v6, "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig ) v6 ).getThermalConditionItem ( ); // invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;->getThermalConditionItem()Ljava/util/List;
/* .line 1558 */
/* .local v7, "conditions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;>;" */
/* const-string/jumbo v8, "thermal-condition-item" */
(( org.json.JSONObject ) v0 ).getJSONArray ( v8 ); // invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1559 */
/* .local v8, "conditionArray":Lorg/json/JSONArray; */
int v9 = 0; // const/4 v9, 0x0
/* .local v9, "i":I */
} // :goto_0
v10 = (( org.json.JSONArray ) v8 ).length ( ); // invoke-virtual {v8}, Lorg/json/JSONArray;->length()I
/* if-ge v9, v10, :cond_2 */
/* .line 1560 */
/* new-instance v10, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* invoke-direct {v10}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;-><init>()V */
/* .line 1561 */
/* .local v10, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
(( org.json.JSONArray ) v8 ).getJSONObject ( v9 ); // invoke-virtual {v8, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 1562 */
/* .local v11, "item":Lorg/json/JSONObject; */
final String v12 = "identifier"; // const-string v12, "identifier"
v12 = (( org.json.JSONObject ) v11 ).getInt ( v12 ); // invoke-virtual {v11, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 1563 */
/* .local v12, "identifier":I */
final String v13 = "description"; // const-string v13, "description"
(( org.json.JSONObject ) v11 ).getString ( v13 ); // invoke-virtual {v11, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 1564 */
/* .local v13, "description":Ljava/lang/String; */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v10 ).setIdentifier ( v12 ); // invoke-virtual {v10, v12}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->setIdentifier(I)V
/* .line 1565 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v10 ).setDescription ( v13 ); // invoke-virtual {v10, v13}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->setDescription(Ljava/lang/String;)V
/* .line 1567 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v10 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v10}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
/* .line 1568 */
/* .local v14, "pairList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;" */
/* const-string/jumbo v15, "temperature-brightness-pair" */
(( org.json.JSONObject ) v11 ).optJSONArray ( v15 ); // invoke-virtual {v11, v15}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1569 */
/* .local v15, "pairs":Lorg/json/JSONArray; */
if ( v15 != null) { // if-eqz v15, :cond_1
/* .line 1570 */
/* const/16 v16, 0x0 */
/* move/from16 v3, v16 */
/* .local v3, "j":I */
} // :goto_1
/* move-object/from16 v16, v0 */
} // .end local v0 # "rootObject":Lorg/json/JSONObject;
/* .local v16, "rootObject":Lorg/json/JSONObject; */
v0 = (( org.json.JSONArray ) v15 ).length ( ); // invoke-virtual {v15}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v0, :cond_0 */
/* .line 1571 */
(( org.json.JSONArray ) v15 ).getJSONObject ( v3 ); // invoke-virtual {v15, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 1572 */
/* .local v0, "pairObj":Lorg/json/JSONObject; */
/* move-object/from16 v17, v4 */
} // .end local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .local v17, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v4 = "min-inclusive"; // const-string v4, "min-inclusive"
(( org.json.JSONObject ) v0 ).getString ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v4 = java.lang.Float .parseFloat ( v4 );
/* .line 1573 */
/* .local v4, "minInclusive":F */
/* move-object/from16 v18, v8 */
} // .end local v8 # "conditionArray":Lorg/json/JSONArray;
/* .local v18, "conditionArray":Lorg/json/JSONArray; */
final String v8 = "max-exclusive"; // const-string v8, "max-exclusive"
(( org.json.JSONObject ) v0 ).getString ( v8 ); // invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v8 = java.lang.Float .parseFloat ( v8 );
/* .line 1574 */
/* .local v8, "maxExclusive":F */
/* move-object/from16 v19, v11 */
} // .end local v11 # "item":Lorg/json/JSONObject;
/* .local v19, "item":Lorg/json/JSONObject; */
final String v11 = "nit"; // const-string v11, "nit"
(( org.json.JSONObject ) v0 ).getString ( v11 ); // invoke-virtual {v0, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v11 = java.lang.Float .parseFloat ( v11 );
/* .line 1576 */
/* .local v11, "nit":F */
/* new-instance v20, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* invoke-direct/range {v20 ..v20}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;-><init>()V */
/* move-object/from16 v21, v20 */
/* .line 1577 */
/* .local v21, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* move-object/from16 v20, v0 */
/* move-object/from16 v0, v21 */
} // .end local v21 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
/* .local v0, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .local v20, "pairObj":Lorg/json/JSONObject; */
(( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v0 ).setMinInclusive ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setMinInclusive(F)V
/* .line 1578 */
(( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v0 ).setMaxExclusive ( v8 ); // invoke-virtual {v0, v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setMaxExclusive(F)V
/* .line 1579 */
(( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v0 ).setNit ( v11 ); // invoke-virtual {v0, v11}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->setNit(F)V
/* .line 1580 */
/* .line 1570 */
/* nop */
} // .end local v0 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v4 # "minInclusive":F
} // .end local v8 # "maxExclusive":F
} // .end local v11 # "nit":F
} // .end local v20 # "pairObj":Lorg/json/JSONObject;
/* add-int/lit8 v3, v3, 0x1 */
/* move-object/from16 v0, v16 */
/* move-object/from16 v4, v17 */
/* move-object/from16 v8, v18 */
/* move-object/from16 v11, v19 */
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v18 # "conditionArray":Lorg/json/JSONArray;
} // .end local v19 # "item":Lorg/json/JSONObject;
/* .local v4, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .local v8, "conditionArray":Lorg/json/JSONArray; */
/* .local v11, "item":Lorg/json/JSONObject; */
} // :cond_0
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v8 */
/* move-object/from16 v19, v11 */
} // .end local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v8 # "conditionArray":Lorg/json/JSONArray;
} // .end local v11 # "item":Lorg/json/JSONObject;
/* .restart local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .restart local v18 # "conditionArray":Lorg/json/JSONArray; */
/* .restart local v19 # "item":Lorg/json/JSONObject; */
/* .line 1569 */
} // .end local v3 # "j":I
} // .end local v16 # "rootObject":Lorg/json/JSONObject;
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v18 # "conditionArray":Lorg/json/JSONArray;
} // .end local v19 # "item":Lorg/json/JSONObject;
/* .local v0, "rootObject":Lorg/json/JSONObject; */
/* .restart local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .restart local v8 # "conditionArray":Lorg/json/JSONArray; */
/* .restart local v11 # "item":Lorg/json/JSONObject; */
} // :cond_1
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v8 */
/* move-object/from16 v19, v11 */
/* .line 1583 */
} // .end local v0 # "rootObject":Lorg/json/JSONObject;
} // .end local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v8 # "conditionArray":Lorg/json/JSONArray;
} // .end local v11 # "item":Lorg/json/JSONObject;
/* .restart local v16 # "rootObject":Lorg/json/JSONObject; */
/* .restart local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .restart local v18 # "conditionArray":Lorg/json/JSONArray; */
/* .restart local v19 # "item":Lorg/json/JSONObject; */
} // :goto_2
/* .line 1559 */
/* nop */
} // .end local v10 # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
} // .end local v12 # "identifier":I
} // .end local v13 # "description":Ljava/lang/String;
} // .end local v14 # "pairList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
} // .end local v15 # "pairs":Lorg/json/JSONArray;
} // .end local v19 # "item":Lorg/json/JSONObject;
/* add-int/lit8 v9, v9, 0x1 */
/* move-object/from16 v0, v16 */
/* move-object/from16 v4, v17 */
/* move-object/from16 v8, v18 */
int v3 = 0; // const/4 v3, 0x0
/* goto/16 :goto_0 */
} // .end local v16 # "rootObject":Lorg/json/JSONObject;
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v18 # "conditionArray":Lorg/json/JSONArray;
/* .restart local v0 # "rootObject":Lorg/json/JSONObject; */
/* .restart local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .restart local v8 # "conditionArray":Lorg/json/JSONArray; */
} // :cond_2
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v4 */
/* move-object/from16 v18, v8 */
/* .line 1585 */
} // .end local v0 # "rootObject":Lorg/json/JSONObject;
} // .end local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v8 # "conditionArray":Lorg/json/JSONArray;
} // .end local v9 # "i":I
/* .restart local v16 # "rootObject":Lorg/json/JSONObject; */
/* .restart local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .restart local v18 # "conditionArray":Lorg/json/JSONArray; */
/* iget-wide v3, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v8, 0x2 */
/* or-long/2addr v3, v8 */
/* iput-wide v3, v1, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
/* .line 1586 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update thermal brightness config json: "; // const-string v3, "Update thermal brightness config json: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 1587 */
(( com.android.server.display.MiuiDisplayCloudController ) v1 ).writeToFile ( v6 ); // invoke-virtual {v1, v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeToFile(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1588 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1553 */
} // .end local v5 # "jsonObject":Lorg/json/JSONObject;
} // .end local v6 # "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
} // .end local v7 # "conditions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;>;"
} // .end local v16 # "rootObject":Lorg/json/JSONObject;
} // .end local v17 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v18 # "conditionArray":Lorg/json/JSONArray;
/* .restart local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
} // :cond_3
/* move-object/from16 v17, v4 */
/* .line 1592 */
} // .end local v4 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .line 1590 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1591 */
/* .local v0, "e":Lorg/json/JSONException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update thermal brightness config exception: "; // const-string v4, "Update thermal brightness config exception: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1593 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_3
int v2 = 0; // const/4 v2, 0x0
} // .end method
private Boolean updateThresholdSunlightNitValue ( ) {
/* .locals 5 */
/* .line 1487 */
v0 = this.mContext;
/* .line 1488 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "thresholdSunlightNit" */
/* const-string/jumbo v2, "threshold_sunlight_nit" */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1490 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1491 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optDouble ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D
/* move-result-wide v2 */
/* double-to-float v2, v2 */
/* iput v2, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F */
/* .line 1492 */
v3 = this.mCallback;
/* .line 1493 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Update threshold of sunlight mode nit: "; // const-string v3, "Update threshold of sunlight mode nit: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1494 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1496 */
} // :cond_0
final String v2 = "Failed to update threshold of sunlight mode nit from cloud."; // const-string v2, "Failed to update threshold of sunlight mode nit from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1497 */
} // .end method
private Boolean updateTouchProtectionGameList ( ) {
/* .locals 6 */
/* .line 1412 */
v0 = this.mContext;
/* .line 1413 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "TouchCoverProtectionGameMode"; // const-string v1, "TouchCoverProtectionGameMode"
/* const-string/jumbo v2, "touch_cover_protection_game_app_list" */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 1415 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiDisplayCloudController"; // const-string v1, "MiuiDisplayCloudController"
if ( v0 != null) { // if-eqz v0, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1416 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v3 ).optJSONArray ( v2 ); // invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 1417 */
/* .local v2, "appArray":Lorg/json/JSONArray; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1418 */
v3 = this.mTouchCoverProtectionGameList;
/* .line 1419 */
final String v3 = "Update game apps."; // const-string v3, "Update game apps."
android.util.Slog .d ( v1,v3 );
/* .line 1420 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v3, :cond_1 */
/* .line 1421 */
(( org.json.JSONArray ) v2 ).opt ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;
/* .line 1422 */
/* .local v3, "obj":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1423 */
v4 = this.mTouchCoverProtectionGameList;
/* move-object v5, v3 */
/* check-cast v5, Ljava/lang/String; */
/* .line 1420 */
} // .end local v3 # "obj":Ljava/lang/Object;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1426 */
} // .end local v1 # "i":I
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 1428 */
} // .end local v2 # "appArray":Lorg/json/JSONArray;
} // :cond_2
/* .line 1429 */
} // :cond_3
final String v2 = "Failed to update game apps from cloud."; // const-string v2, "Failed to update game apps from cloud."
android.util.Slog .w ( v1,v2 );
/* .line 1431 */
} // :goto_1
} // .end method
private void writeAppCategoryConfigToFile ( com.android.server.display.aiautobrt.config.AppCategoryConfig p0 ) {
/* .locals 18 */
/* .param p1, "config" # Lcom/android/server/display/aiautobrt/config/AppCategoryConfig; */
/* .line 1798 */
/* move-object/from16 v1, p0 */
final String v0 = "pkg"; // const-string v0, "pkg"
final String v2 = "name"; // const-string v2, "name"
final String v3 = "category"; // const-string v3, "category"
final String v4 = "app-category-config"; // const-string v4, "app-category-config"
v5 = this.mAppCategoryConfigCloudFile;
/* if-nez v5, :cond_0 */
/* .line 1799 */
final String v5 = "cloud_app_brightness_category.xml"; // const-string v5, "cloud_app_brightness_category.xml"
/* invoke-direct {v1, v5}, Lcom/android/server/display/MiuiDisplayCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile; */
this.mAppCategoryConfigCloudFile = v5;
/* .line 1801 */
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* .line 1803 */
/* .local v5, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_0
v6 = this.mAppCategoryConfigCloudFile;
(( android.util.AtomicFile ) v6 ).startWrite ( ); // invoke-virtual {v6}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v5, v6 */
/* .line 1804 */
/* new-instance v6, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v6}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 1805 */
/* .local v6, "out":Lorg/xmlpull/v1/XmlSerializer; */
v7 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v7 ).name ( ); // invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 1806 */
int v7 = 1; // const/4 v7, 0x1
java.lang.Boolean .valueOf ( v7 );
int v9 = 0; // const/4 v9, 0x0
/* .line 1807 */
final String v8 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 1809 */
/* .line 1810 */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/aiautobrt/config/AppCategoryConfig;->getCategory()Ljava/util/List; */
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_2
/* check-cast v8, Lcom/android/server/display/aiautobrt/config/AppCategory; */
/* .line 1811 */
/* .local v8, "category":Lcom/android/server/display/aiautobrt/config/AppCategory; */
/* .line 1812 */
v10 = (( com.android.server.display.aiautobrt.config.AppCategory ) v8 ).getId ( ); // invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getId()I
/* .line 1813 */
/* .local v10, "id":I */
(( com.android.server.display.aiautobrt.config.AppCategory ) v8 ).getName ( ); // invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getName()Ljava/lang/String;
/* .line 1814 */
/* .local v11, "name":Ljava/lang/String; */
final String v12 = "id"; // const-string v12, "id"
java.lang.Integer .toString ( v10 );
/* .line 1815 */
/* .line 1817 */
(( com.android.server.display.aiautobrt.config.AppCategory ) v8 ).getPkg ( ); // invoke-virtual {v8}, Lcom/android/server/display/aiautobrt/config/AppCategory;->getPkg()Ljava/util/List;
v13 = } // :goto_1
if ( v13 != null) { // if-eqz v13, :cond_1
/* check-cast v13, Lcom/android/server/display/aiautobrt/config/PackageInfo; */
/* .line 1818 */
/* .local v13, "appInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo; */
/* .line 1819 */
v14 = (( com.android.server.display.aiautobrt.config.PackageInfo ) v13 ).getCateId ( ); // invoke-virtual {v13}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->getCateId()I
/* .line 1820 */
/* .local v14, "cateId":I */
(( com.android.server.display.aiautobrt.config.PackageInfo ) v13 ).getName ( ); // invoke-virtual {v13}, Lcom/android/server/display/aiautobrt/config/PackageInfo;->getName()Ljava/lang/String;
/* .line 1822 */
/* .local v15, "pkgName":Ljava/lang/String; */
final String v9 = "cateId"; // const-string v9, "cateId"
/* move-object/from16 v16, v7 */
java.lang.Integer .toString ( v14 );
/* move-object/from16 v17, v8 */
int v8 = 0; // const/4 v8, 0x0
} // .end local v8 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
/* .local v17, "category":Lcom/android/server/display/aiautobrt/config/AppCategory; */
/* .line 1823 */
/* .line 1825 */
/* .line 1826 */
/* move-object/from16 v7, v16 */
/* move-object/from16 v8, v17 */
int v9 = 0; // const/4 v9, 0x0
} // .end local v13 # "appInfo":Lcom/android/server/display/aiautobrt/config/PackageInfo;
} // .end local v14 # "cateId":I
} // .end local v15 # "pkgName":Ljava/lang/String;
/* .line 1827 */
} // .end local v17 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
/* .restart local v8 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory; */
} // :cond_1
/* move-object/from16 v16, v7 */
/* move-object/from16 v17, v8 */
} // .end local v8 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
/* .restart local v17 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory; */
int v7 = 0; // const/4 v7, 0x0
/* .line 1828 */
/* move-object/from16 v7, v16 */
int v9 = 0; // const/4 v9, 0x0
} // .end local v10 # "id":I
} // .end local v11 # "name":Ljava/lang/String;
} // .end local v17 # "category":Lcom/android/server/display/aiautobrt/config/AppCategory;
/* .line 1829 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 1830 */
/* .line 1831 */
(( java.io.FileOutputStream ) v5 ).flush ( ); // invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V
/* .line 1832 */
v0 = this.mAppCategoryConfigCloudFile;
(( android.util.AtomicFile ) v0 ).finishWrite ( v5 ); // invoke-virtual {v0, v5}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1836 */
} // .end local v6 # "out":Lorg/xmlpull/v1/XmlSerializer;
/* .line 1833 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1834 */
/* .local v0, "e":Ljava/io/IOException; */
v2 = this.mAppCategoryConfigCloudFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v5 ); // invoke-virtual {v2, v5}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 1835 */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 1837 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
private void writeElementOfAppListToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.util.List p3, java.lang.String p4, java.lang.String p5 ) {
/* .locals 3 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p5, "attribute" # Ljava/lang/String; */
/* .param p6, "tag" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/AtomicFile;", */
/* "Ljava/io/FileOutputStream;", */
/* "Lcom/android/modules/utils/TypedXmlSerializer;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 634 */
/* .local p4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
try { // :try_start_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/String; */
/* .line 635 */
/* .local v1, "str":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 636 */
/* .line 637 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 638 */
/* nop */
} // .end local v1 # "str":Ljava/lang/String;
/* .line 642 */
} // :cond_0
/* .line 639 */
/* :catch_0 */
/* move-exception v0 */
/* .line 640 */
/* .local v0, "e":Ljava/io/IOException; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 641 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write element of app list to xml"; // const-string v2, "Failed to write element of app list to xml"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
android.util.Slog .e ( v2,v1 );
/* .line 643 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
private void writeFeatureEnableToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.lang.String p3, java.lang.String p4, Boolean p5 ) {
/* .locals 3 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p4, "attribute" # Ljava/lang/String; */
/* .param p5, "tag" # Ljava/lang/String; */
/* .param p6, "enable" # Z */
/* .line 556 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 557 */
/* .line 558 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 562 */
/* .line 559 */
/* :catch_0 */
/* move-exception v0 */
/* .line 560 */
/* .local v0, "e":Ljava/io/IOException; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 561 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write local backup of feature enable"; // const-string v2, "Failed to write local backup of feature enable"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
android.util.Slog .e ( v2,v1 );
/* .line 563 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void writeFeatureValueToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.lang.String p3, java.lang.String p4, java.lang.Number p5 ) {
/* .locals 3 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p4, "attribute" # Ljava/lang/String; */
/* .param p5, "tag" # Ljava/lang/String; */
/* .param p6, "value" # Ljava/lang/Number; */
/* .line 572 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 573 */
v1 = (( java.lang.Number ) p6 ).floatValue ( ); // invoke-virtual {p6}, Ljava/lang/Number;->floatValue()F
/* .line 574 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 578 */
/* .line 575 */
/* :catch_0 */
/* move-exception v0 */
/* .line 576 */
/* .local v0, "e":Ljava/io/IOException; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 577 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write local backup of value"; // const-string v2, "Failed to write local backup of value"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiDisplayCloudController"; // const-string v2, "MiuiDisplayCloudController"
android.util.Slog .e ( v2,v1 );
/* .line 579 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void writeShortTermModelAppListToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2 ) {
/* .locals 8 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .line 588 */
int v0 = 0; // const/4 v0, 0x0
/* move v7, v0 */
/* .local v7, "category":I */
} // :goto_0
int v0 = 6; // const/4 v0, 0x6
/* if-ge v7, v0, :cond_0 */
/* .line 589 */
/* packed-switch v7, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 607 */
/* :pswitch_0 */
v4 = this.mShortTermModelReaderList;
final String v5 = "package"; // const-string v5, "package"
final String v6 = "reader-category"; // const-string v6, "reader-category"
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 609 */
/* .line 603 */
/* :pswitch_1 */
v4 = this.mShortTermModelImageList;
final String v5 = "package"; // const-string v5, "package"
final String v6 = "image-category"; // const-string v6, "image-category"
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 605 */
/* .line 599 */
/* :pswitch_2 */
v4 = this.mShortTermModelMapList;
final String v5 = "package"; // const-string v5, "package"
final String v6 = "map-category"; // const-string v6, "map-category"
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 601 */
/* .line 595 */
/* :pswitch_3 */
v4 = this.mShortTermModelVideoList;
final String v5 = "package"; // const-string v5, "package"
/* const-string/jumbo v6, "video-category" */
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 597 */
/* .line 591 */
/* :pswitch_4 */
v4 = this.mShortTermModelGameList;
final String v5 = "package"; // const-string v5, "package"
final String v6 = "game-category"; // const-string v6, "game-category"
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 593 */
/* .line 611 */
/* :pswitch_5 */
v4 = this.mShortTermModelGlobalList;
final String v5 = "package"; // const-string v5, "package"
/* const-string/jumbo v6, "undefined-category" */
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeElementOfAppListToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 613 */
/* nop */
/* .line 588 */
} // :goto_1
/* add-int/lit8 v7, v7, 0x1 */
/* .line 618 */
} // .end local v7 # "category":I
} // :cond_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void writeTagToXml ( org.xmlpull.v1.XmlSerializer p0, java.lang.String p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "out" # Lorg/xmlpull/v1/XmlSerializer; */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 1727 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1728 */
java.lang.String .valueOf ( p3 );
/* .line 1729 */
/* .line 1730 */
return;
} // .end method
/* # virtual methods */
protected void addCloudListener ( com.android.server.display.MiuiDisplayCloudController$CloudListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/android/server/display/MiuiDisplayCloudController$CloudListener; */
/* .line 1736 */
v0 = v0 = this.mCloudListeners;
/* if-nez v0, :cond_0 */
/* .line 1737 */
v0 = this.mCloudListeners;
/* .line 1738 */
/* iget-wide v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCloudEventsSummary:J */
v2 = this.mCloudEventsData;
/* .line 1740 */
} // :cond_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1264 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 1265 */
final String v0 = "MiuiDisplayCloudController Configuration:"; // const-string v0, "MiuiDisplayCloudController Configuration:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1266 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mShortTermModelEnable="; // const-string v1, " mShortTermModelEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1267 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mShortTermModelAppMapper="; // const-string v1, " mShortTermModelAppMapper="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mShortTermModelAppMapper;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1268 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBCBCAppList="; // const-string v1, " mBCBCAppList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBCBCAppList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1269 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mTouchCoverProtectionGameList="; // const-string v1, " mTouchCoverProtectionGameList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTouchCoverProtectionGameList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1270 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mResolutionSwitchProcessProtectList="; // const-string v1, " mResolutionSwitchProcessProtectList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mResolutionSwitchProcessProtectList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1271 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mResolutionSwitchProcessBlackList="; // const-string v1, " mResolutionSwitchProcessBlackList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mResolutionSwitchProcessBlackList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1272 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mRhythmicImageAppList="; // const-string v1, " mRhythmicImageAppList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRhythmicImageAppList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1273 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mRhythmicReadAppList="; // const-string v1, " mRhythmicReadAppList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRhythmicReadAppList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1274 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mGestureComponents="; // const-string v1, " mGestureComponents="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mGestureComponents;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1275 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mOutdoorThermalAppList="; // const-string v1, " mOutdoorThermalAppList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mOutdoorThermalAppList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1276 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mManualBoostAppEnable="; // const-string v1, " mManualBoostAppEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1277 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mManualBoostDisableAppList="; // const-string v1, " mManualBoostDisableAppList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mManualBoostDisableAppList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1278 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mOverrideBrightnessPolicyEnable="; // const-string v1, " mOverrideBrightnessPolicyEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1279 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAutoBrightnessStatisticsEventEnable="; // const-string v1, " mAutoBrightnessStatisticsEventEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1280 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDisableResetShortTermModel="; // const-string v1, " mDisableResetShortTermModel="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mDisableResetShortTermModel:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1281 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBrightnessStatsEventsEnable="; // const-string v1, " mBrightnessStatsEventsEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessStatsEventsEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1282 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mThresholdSunlightNit="; // const-string v1, " mThresholdSunlightNit="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mThresholdSunlightNit:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1283 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCustomCurveDisable="; // const-string v1, " mCustomCurveDisable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mCustomCurveDisable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1284 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIndividualModelDisable="; // const-string v1, " mIndividualModelDisable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mIndividualModelDisable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1285 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBrightnessCurveOptimizePolicyDisable="; // const-string v1, " mBrightnessCurveOptimizePolicyDisable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1286 */
return;
} // .end method
protected java.util.List getBCBCAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1289 */
v0 = this.mBCBCAppList;
} // .end method
protected java.util.List getManualBoostDisableAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1343 */
v0 = this.mManualBoostDisableAppList;
} // .end method
protected java.util.Map getShortTermModelAppMapper ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 1260 */
v0 = this.mShortTermModelAppMapper;
} // .end method
protected java.util.List getTouchCoverProtectionGameList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1408 */
v0 = this.mTouchCoverProtectionGameList;
} // .end method
public Boolean isAutoBrightnessStatisticsEventEnable ( ) {
/* .locals 1 */
/* .line 1251 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mAutoBrightnessStatisticsEventEnable:Z */
} // .end method
Boolean isBrightnessCurveOptimizePolicyDisable ( ) {
/* .locals 1 */
/* .line 1906 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mBrightnessCurveOptimizePolicyDisable:Z */
} // .end method
protected Boolean isManualBoostAppEnable ( ) {
/* .locals 1 */
/* .line 1339 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mManualBoostAppEnable:Z */
} // .end method
Boolean isOverrideBrightnessPolicyEnable ( ) {
/* .locals 1 */
/* .line 1507 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mOverrideBrightnessPolicyEnable:Z */
} // .end method
protected Boolean isShortTermModelEnable ( ) {
/* .locals 1 */
/* .line 1232 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiDisplayCloudController;->mShortTermModelEnable:Z */
} // .end method
public void notifyAllObservers ( ) {
/* .locals 2 */
/* .line 1539 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/display/MiuiDisplayCloudController$Observer; */
/* .line 1540 */
/* .local v1, "observer":Lcom/android/server/display/MiuiDisplayCloudController$Observer; */
/* .line 1541 */
} // .end local v1 # "observer":Lcom/android/server/display/MiuiDisplayCloudController$Observer;
/* .line 1542 */
} // :cond_0
return;
} // .end method
public void registerObserver ( com.android.server.display.MiuiDisplayCloudController$Observer p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Lcom/android/server/display/MiuiDisplayCloudController$Observer; */
/* .line 1527 */
final String v0 = "observer may not be null"; // const-string v0, "observer may not be null"
java.util.Objects .requireNonNull ( p1,v0 );
/* .line 1528 */
v0 = this.mObservers;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 1529 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1531 */
} // :cond_0
return;
} // .end method
public void unregisterObserver ( com.android.server.display.MiuiDisplayCloudController$Observer p0 ) {
/* .locals 1 */
/* .param p1, "observer" # Lcom/android/server/display/MiuiDisplayCloudController$Observer; */
/* .line 1534 */
final String v0 = "observer may not be null"; // const-string v0, "observer may not be null"
java.util.Objects .requireNonNull ( p1,v0 );
/* .line 1535 */
v0 = this.mObservers;
(( java.util.ArrayList ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1536 */
return;
} // .end method
public void writeToFile ( com.android.server.display.thermalbrightnesscondition.config.ThermalBrightnessConfig p0 ) {
/* .locals 17 */
/* .param p1, "config" # Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig; */
/* .line 1684 */
/* move-object/from16 v1, p0 */
/* const-string/jumbo v0, "temperature-brightness-pair" */
/* const-string/jumbo v2, "thermal-condition-item" */
/* const-string/jumbo v3, "thermal-brightness-config" */
v4 = this.mThermalBrightnessCloudFile;
/* if-nez v4, :cond_0 */
/* .line 1685 */
final String v4 = "cloud_thermal_brightness_control.xml"; // const-string v4, "cloud_thermal_brightness_control.xml"
/* invoke-direct {v1, v4}, Lcom/android/server/display/MiuiDisplayCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile; */
this.mThermalBrightnessCloudFile = v4;
/* .line 1687 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .line 1689 */
/* .local v4, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_0
v5 = this.mThermalBrightnessCloudFile;
(( android.util.AtomicFile ) v5 ).startWrite ( ); // invoke-virtual {v5}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v4, v5 */
/* .line 1690 */
/* new-instance v5, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v5}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 1691 */
/* .local v5, "out":Lorg/xmlpull/v1/XmlSerializer; */
v6 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v6 ).name ( ); // invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 1692 */
int v6 = 1; // const/4 v6, 0x1
java.lang.Boolean .valueOf ( v6 );
int v8 = 0; // const/4 v8, 0x0
/* .line 1693 */
final String v7 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 1694 */
/* .line 1695 */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;->getThermalConditionItem()Ljava/util/List; */
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_2
/* check-cast v7, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 1696 */
/* .local v7, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem; */
/* .line 1697 */
v9 = (( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v7 ).getIdentifier ( ); // invoke-virtual {v7}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I
/* .line 1698 */
/* .local v9, "identifier":I */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v7 ).getDescription ( ); // invoke-virtual {v7}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;
/* .line 1700 */
/* .local v10, "description":Ljava/lang/String; */
final String v11 = "identifier"; // const-string v11, "identifier"
java.lang.Integer .valueOf ( v9 );
/* invoke-direct {v1, v5, v11, v12}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 1701 */
final String v11 = "description"; // const-string v11, "description"
/* invoke-direct {v1, v5, v11, v10}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 1703 */
(( com.android.server.display.thermalbrightnesscondition.config.ThermalConditionItem ) v7 ).getTemperatureBrightnessPair ( ); // invoke-virtual {v7}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;
v12 = } // :goto_1
if ( v12 != null) { // if-eqz v12, :cond_1
/* check-cast v12, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 1704 */
/* .local v12, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair; */
/* .line 1705 */
v13 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v12 ).getMinInclusive ( ); // invoke-virtual {v12}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F
/* .line 1706 */
/* .local v13, "minInclusive":F */
v14 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v12 ).getMaxExclusive ( ); // invoke-virtual {v12}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F
/* .line 1707 */
/* .local v14, "maxExclusive":F */
v15 = (( com.android.server.display.thermalbrightnesscondition.config.TemperatureBrightnessPair ) v12 ).getNit ( ); // invoke-virtual {v12}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F
/* .line 1709 */
/* .local v15, "nit":F */
final String v8 = "min-inclusive"; // const-string v8, "min-inclusive"
/* move-object/from16 v16, v6 */
java.lang.Float .valueOf ( v13 );
/* invoke-direct {v1, v5, v8, v6}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 1710 */
final String v6 = "max-exclusive"; // const-string v6, "max-exclusive"
java.lang.Float .valueOf ( v14 );
/* invoke-direct {v1, v5, v6, v8}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 1711 */
final String v6 = "nit"; // const-string v6, "nit"
java.lang.Float .valueOf ( v15 );
/* invoke-direct {v1, v5, v6, v8}, Lcom/android/server/display/MiuiDisplayCloudController;->writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V */
/* .line 1712 */
int v6 = 0; // const/4 v6, 0x0
/* .line 1713 */
/* move-object/from16 v6, v16 */
int v8 = 0; // const/4 v8, 0x0
} // .end local v12 # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
} // .end local v13 # "minInclusive":F
} // .end local v14 # "maxExclusive":F
} // .end local v15 # "nit":F
/* .line 1714 */
} // :cond_1
/* move-object/from16 v16, v6 */
int v6 = 0; // const/4 v6, 0x0
/* .line 1715 */
/* move-object/from16 v6, v16 */
int v8 = 0; // const/4 v8, 0x0
} // .end local v7 # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
} // .end local v9 # "identifier":I
} // .end local v10 # "description":Ljava/lang/String;
/* .line 1716 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 1717 */
/* .line 1718 */
(( java.io.FileOutputStream ) v4 ).flush ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
/* .line 1719 */
v0 = this.mThermalBrightnessCloudFile;
(( android.util.AtomicFile ) v0 ).finishWrite ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1723 */
} // .end local v5 # "out":Lorg/xmlpull/v1/XmlSerializer;
/* .line 1720 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1721 */
/* .local v0, "e":Ljava/io/IOException; */
v2 = this.mThermalBrightnessCloudFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v4 ); // invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 1722 */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 1724 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
