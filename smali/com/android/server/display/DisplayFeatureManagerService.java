public class com.android.server.display.DisplayFeatureManagerService extends com.android.server.SystemService {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$LocalService;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$BinderService;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$PaperModeAnimatListener;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;, */
	 /* Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AIDL_SERVICENAME_DEFAULT;
public static java.lang.String BRIGHTNESS_THROTTLER_STATUS;
private static final Integer CLASSIC_READING_MODE;
private static final Integer CONFIG_SERVICENAME_RESOURCEID;
private static final Integer FLAG_SET_SCREEN_EFFECT;
private static final Integer FLAG_SET_VIEDEO_INFOMATION;
private static final Boolean FPS_SWITCH_DEFAULT;
private static final java.lang.String HIDL_SERVICENAME_DEFAULT;
private static final Boolean IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT;
private static final Integer MSG_SDR_TO_HDR;
private static final Integer MSG_SEND_HBM_STATE;
private static final Integer MSG_SEND_MURA_STATE;
private static final Integer MSG_SET_COLOR_MODE;
private static final Integer MSG_SET_CURRENT_GRAY_VALUE;
private static final Integer MSG_SET_DC_PARSE_STATE;
private static final Integer MSG_SET_GRAY_VALUE;
private static final Integer MSG_SET_PAPER_COLOR_TYPE;
private static final Integer MSG_SET_SECONDARY_FRAME_RATE;
private static final Integer MSG_SWITCH_DARK_MODE;
private static final Integer MSG_UPDATE_COLOR_SCHEME;
private static final Integer MSG_UPDATE_DFPS_MODE;
private static final Integer MSG_UPDATE_EXPERT_MODE;
private static final Integer MSG_UPDATE_HDR_STATE;
private static final Integer MSG_UPDATE_PCC_LEVEL;
private static final Integer MSG_UPDATE_READING_MODE;
private static final Integer MSG_UPDATE_SMART_DFPS_MODE;
private static final Integer MSG_UPDATE_TRUETONE;
private static final Integer MSG_UPDATE_UNLIMITED_COLOR_LEVEL;
private static final Integer MSG_UPDATE_USERCHANGE;
private static final Integer MSG_UPDATE_WCG_STATE;
private static final Integer NEW_CLASSIC_READING_MODE;
private static final Float PAPER_MODE_MIN_LEVEL;
private static final Integer PAPER_READING_MODE;
private static final java.lang.String PERSISTENT_PROPERTY_DISPLAY_COLOR;
private static final Integer RHYTHMIC_READING_MODE;
private static final Integer SCREEN_DEFAULT_FPS;
private static final Boolean SUPPORT_DISPLAY_EXPERT_MODE;
private static final Boolean SUPPORT_MULTIPLE_AOD_BRIGHTNESS;
private static final Boolean SUPPORT_SET_FEATURE;
private static final Boolean SUPPORT_UNLIMITED_COLOR_MODE;
private static final java.lang.String SURFACE_FLINGER;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_DC_PARSE_STATE;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_DFPS;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_FPS_VIDEO_INFO;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_PCC;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SDR2HDR;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SECONDARY_FRAME_RATE;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SET_MODE;
private static final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SMART_DFPS;
private static final java.lang.String TAG;
private static final Integer TEMP_PAPER_MODE_LEVEL;
private static final java.lang.String THREAD_TAG;
/* # instance fields */
private Boolean mAutoAdjustEnable;
private android.os.IBinder$DeathRecipient mBinderDeathHandler;
private Boolean mBootCompleted;
private java.util.HashMap mClientDeathCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mColorSchemeCTLevel;
private Integer mColorSchemeModeType;
private android.content.Context mContext;
private Float mCurrentGrayScale;
private Boolean mDeskTopModeEnabled;
private com.android.server.display.DisplayFeatureManagerServiceImpl mDisplayFeatureServiceImpl;
private android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
private Integer mDisplayState;
private Boolean mDolbyState;
private Boolean mForceDisableEyeCare;
private Boolean mGameHdrEnabled;
private Float mGrayScale;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private android.os.IHwBinder$DeathRecipient mHwBinderDeathHandler;
private final Boolean mIsFoldOrFlip;
public com.android.server.display.DisplayFeatureManagerInternal mLocalService;
private final java.lang.Object mLock;
private Integer mMaxDozeBrightnessInt;
private Integer mMinDozeBrightnessInt;
private Integer mMinimumBrightnessInt;
private Integer mPaperColorType;
private com.android.server.display.MiuiRampAnimator mPaperModeAnimator;
private Integer mPaperModeMinRate;
private android.os.PowerManager mPowerManager;
private Integer mReadingModeCTLevel;
private Boolean mReadingModeEnabled;
private Integer mReadingModeType;
private android.content.ContentResolver mResolver;
private final com.android.server.display.RhythmicEyeCareManager$RhythmicEyeCareListener mRhythmicEyeCareListener;
private com.android.server.display.RhythmicEyeCareManager mRhythmicEyeCareManager;
private com.android.server.display.DisplayFeatureManagerService$SettingsObserver mSettingsObserver;
private Integer mTrueToneModeEnabled;
private com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper mWrapper;
/* # direct methods */
static Boolean -$$Nest$fgetmAutoAdjustEnable ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmBootCompleted ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z */
} // .end method
static java.util.HashMap -$$Nest$fgetmClientDeathCallbacks ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mClientDeathCallbacks;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmDisplayState ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I */
} // .end method
static Boolean -$$Nest$fgetmDolbyState ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDolbyState:Z */
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static com.android.server.display.MiuiRampAnimator -$$Nest$fgetmPaperModeAnimator ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPaperModeAnimator;
} // .end method
static Boolean -$$Nest$fgetmReadingModeEnabled ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
} // .end method
static Integer -$$Nest$fgetmReadingModeType ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
} // .end method
static android.content.ContentResolver -$$Nest$fgetmResolver ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mResolver;
} // .end method
static com.android.server.display.RhythmicEyeCareManager -$$Nest$fgetmRhythmicEyeCareManager ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mRhythmicEyeCareManager;
} // .end method
static void -$$Nest$fputmCurrentGrayScale ( com.android.server.display.DisplayFeatureManagerService p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mCurrentGrayScale:F */
return;
} // .end method
static void -$$Nest$fputmDisplayState ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I */
return;
} // .end method
static void -$$Nest$fputmDolbyState ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDolbyState:Z */
return;
} // .end method
static void -$$Nest$fputmGrayScale ( com.android.server.display.DisplayFeatureManagerService p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGrayScale:F */
return;
} // .end method
static void -$$Nest$fputmWrapper ( com.android.server.display.DisplayFeatureManagerService p0, com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper p1 ) { //bridge//synthethic
/* .locals 0 */
this.mWrapper = p1;
return;
} // .end method
static void -$$Nest$mdoDieLocked ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->doDieLocked(I)V */
return;
} // .end method
static void -$$Nest$mhandleAutoAdjustChange ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleAutoAdjustChange()V */
return;
} // .end method
static void -$$Nest$mhandleDisplayFeatureInfoChanged ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, java.lang.Object[] p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->handleDisplayFeatureInfoChanged(I[Ljava/lang/Object;)V */
return;
} // .end method
static void -$$Nest$mhandleGameModeChange ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleGameModeChange()V */
return;
} // .end method
static void -$$Nest$mhandleReadingModeChange ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->handleReadingModeChange(Z)V */
return;
} // .end method
static void -$$Nest$mhandleScreenSchemeChange ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->handleScreenSchemeChange(Z)V */
return;
} // .end method
static void -$$Nest$mhandleTrueToneModeChange ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleTrueToneModeChange()V */
return;
} // .end method
static void -$$Nest$mhandleUnlimitedColorLevelChange ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->handleUnlimitedColorLevelChange(Z)V */
return;
} // .end method
static Boolean -$$Nest$misDarkModeEnable ( com.android.server.display.DisplayFeatureManagerService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->isDarkModeEnable(Landroid/content/Context;)Z */
} // .end method
static Boolean -$$Nest$misSupportSmartEyeCare ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->isSupportSmartEyeCare()Z */
} // .end method
static void -$$Nest$mloadSettings ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->loadSettings()V */
return;
} // .end method
static void -$$Nest$mnotifyCurrentGrayScaleChanged ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->notifyCurrentGrayScaleChanged()V */
return;
} // .end method
static void -$$Nest$mnotifyGrayScaleChanged ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->notifyGrayScaleChanged()V */
return;
} // .end method
static void -$$Nest$mnotifyHdrStateChanged ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->notifyHdrStateChanged()V */
return;
} // .end method
static void -$$Nest$mnotifySFColorMode ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFColorMode(I)V */
return;
} // .end method
static void -$$Nest$mnotifySFDCParseState ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFDCParseState(I)V */
return;
} // .end method
static void -$$Nest$mnotifySFDfpsMode ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFDfpsMode(II)V */
return;
} // .end method
static void -$$Nest$mnotifySFPccLevel ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, Float p2, Float p3, Float p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFPccLevel(IFFF)V */
return;
} // .end method
static void -$$Nest$mnotifySFSecondaryFrameRate ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFSecondaryFrameRate(I)V */
return;
} // .end method
static void -$$Nest$mnotifySFWcgState ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFWcgState(Z)V */
return;
} // .end method
static void -$$Nest$msendHbmState ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->sendHbmState(I)V */
return;
} // .end method
static void -$$Nest$msendMuraState ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->sendMuraState(I)V */
return;
} // .end method
static void -$$Nest$msetDarkModeEnable ( com.android.server.display.DisplayFeatureManagerService p0, android.content.Context p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setDarkModeEnable(Landroid/content/Context;Z)V */
return;
} // .end method
static void -$$Nest$msetDeathCallbackLocked ( com.android.server.display.DisplayFeatureManagerService p0, android.os.IBinder p1, Integer p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
return;
} // .end method
static void -$$Nest$msetExpertScreenMode ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->setExpertScreenMode()V */
return;
} // .end method
static void -$$Nest$msetPaperColors ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V */
return;
} // .end method
static void -$$Nest$msetRhythmicColor ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setRhythmicColor(II)V */
return;
} // .end method
static void -$$Nest$msetSDR2HDR ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setSDR2HDR(I)V */
return;
} // .end method
static void -$$Nest$msetScreenEffect ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
return;
} // .end method
static void -$$Nest$msetScreenEffectAll ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectAll(Z)V */
return;
} // .end method
static void -$$Nest$msetScreenEffectColor ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectColor(Z)V */
return;
} // .end method
static void -$$Nest$msetScreenEffectInternal ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V */
return;
} // .end method
static void -$$Nest$mupdateAutoAdjustEnable ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateAutoAdjustEnable()V */
return;
} // .end method
static void -$$Nest$mupdateColorSchemeCTLevel ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeCTLevel()V */
return;
} // .end method
static void -$$Nest$mupdateColorSchemeModeType ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeModeType()V */
return;
} // .end method
static void -$$Nest$mupdateDeskTopMode ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateDeskTopMode()V */
return;
} // .end method
static void -$$Nest$mupdatePaperColorType ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperColorType()V */
return;
} // .end method
static void -$$Nest$mupdatePaperMode ( com.android.server.display.DisplayFeatureManagerService p0, Boolean p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V */
return;
} // .end method
static void -$$Nest$mupdateReadingModeCTLevel ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeCTLevel()V */
return;
} // .end method
static void -$$Nest$mupdateReadingModeEnable ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeEnable()V */
return;
} // .end method
static void -$$Nest$mupdateReadingModeType ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeType()V */
return;
} // .end method
static void -$$Nest$mupdateTrueToneModeEnable ( com.android.server.display.DisplayFeatureManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateTrueToneModeEnable()V */
return;
} // .end method
static void -$$Nest$mupdateVideoInformationIfNeeded ( com.android.server.display.DisplayFeatureManagerService p0, Integer p1, Boolean p2, Float p3, Integer p4, Integer p5, Float p6, android.os.IBinder p7 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p7}, Lcom/android/server/display/DisplayFeatureManagerService;->updateVideoInformationIfNeeded(IZFIIFLandroid/os/IBinder;)V */
return;
} // .end method
static Boolean -$$Nest$sfgetIS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
} // .end method
static Boolean -$$Nest$sfgetSUPPORT_UNLIMITED_COLOR_MODE ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_UNLIMITED_COLOR_MODE:Z */
} // .end method
static com.android.server.display.DisplayFeatureManagerService ( ) {
/* .locals 5 */
/* .line 123 */
/* nop */
/* .line 124 */
final String v0 = "is_compatible_paper_and_screen_effect"; // const-string v0, "is_compatible_paper_and_screen_effect"
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
com.android.server.display.DisplayFeatureManagerService.IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT = (v0!= 0);
/* .line 128 */
/* nop */
/* .line 129 */
final String v0 = "paper_mode_min_level"; // const-string v0, "paper_mode_min_level"
/* const/high16 v2, 0x3f800000 # 1.0f */
miui.util.FeatureParser .getFloat ( v0,v2 );
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 133 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_UNLIMITED_COLOR_MODE:Z */
com.android.server.display.DisplayFeatureManagerService.SUPPORT_UNLIMITED_COLOR_MODE = (v0!= 0);
/* .line 136 */
/* sget-boolean v0, Lcom/android/server/display/expertmode/ExpertData;->SUPPORT_DISPLAY_EXPERT_MODE:Z */
com.android.server.display.DisplayFeatureManagerService.SUPPORT_DISPLAY_EXPERT_MODE = (v0!= 0);
/* .line 139 */
/* nop */
/* .line 140 */
final String v0 = "defaultFps"; // const-string v0, "defaultFps"
v0 = miui.util.FeatureParser .getInteger ( v0,v1 );
/* .line 141 */
/* nop */
/* .line 142 */
final String v0 = "ro.vendor.fps.switch.default"; // const-string v0, "ro.vendor.fps.switch.default"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.display.DisplayFeatureManagerService.FPS_SWITCH_DEFAULT = (v0!= 0);
/* .line 194 */
final String v0 = "brightness_throttler_status"; // const-string v0, "brightness_throttler_status"
/* .line 211 */
/* nop */
/* .line 212 */
/* const-string/jumbo v0, "support_screen_effect" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* xor-int/lit8 v0, v0, 0x1 */
com.android.server.display.DisplayFeatureManagerService.SUPPORT_SET_FEATURE = (v0!= 0);
/* .line 218 */
android.content.res.Resources .getSystem ( );
/* const-string/jumbo v2, "string" */
final String v3 = "android"; // const-string v3, "android"
final String v4 = "config_displayFeatureHidlServiceName"; // const-string v4, "config_displayFeatureHidlServiceName"
v0 = (( android.content.res.Resources ) v0 ).getIdentifier ( v4, v2, v3 ); // invoke-virtual {v0, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 222 */
/* nop */
/* .line 223 */
final String v0 = "ro.vendor.aod.brightness.cust"; // const-string v0, "ro.vendor.aod.brightness.cust"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.display.DisplayFeatureManagerService.SUPPORT_MULTIPLE_AOD_BRIGHTNESS = (v0!= 0);
/* .line 222 */
return;
} // .end method
public com.android.server.display.DisplayFeatureManagerService ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 245 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 178 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I */
/* .line 185 */
/* const/high16 v1, 0x7fc00000 # Float.NaN */
/* iput v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGrayScale:F */
/* .line 188 */
/* iput v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mCurrentGrayScale:F */
/* .line 202 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.mClientDeathCallbacks = v1;
/* .line 207 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mLock = v1;
/* .line 208 */
v2 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
/* if-nez v2, :cond_0 */
/* .line 209 */
v2 = miui.util.MiuiMultiDisplayTypeInfo .isFoldDeviceInside ( );
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mIsFoldOrFlip:Z */
/* .line 236 */
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$1;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
this.mRhythmicEyeCareListener = v0;
/* .line 246 */
this.mContext = p1;
/* .line 247 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v2;
/* .line 248 */
/* new-instance v2, Landroid/os/HandlerThread; */
final String v3 = "DisplayFeatureThread"; // const-string v3, "DisplayFeatureThread"
/* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v2;
/* .line 249 */
(( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
/* .line 250 */
/* new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 251 */
v2 = this.mContext;
final String v3 = "power"; // const-string v3, "power"
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/os/PowerManager; */
this.mPowerManager = v2;
/* .line 252 */
v2 = (( android.os.PowerManager ) v2 ).getMinimumScreenBrightnessSetting ( ); // invoke-virtual {v2}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I
/* iput v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMinimumBrightnessInt:I */
/* .line 253 */
/* const-class v2, Landroid/hardware/display/DisplayManagerInternal; */
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).getLocalService ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->getLocalService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v2, Landroid/hardware/display/DisplayManagerInternal; */
this.mDisplayManagerInternal = v2;
/* .line 254 */
/* new-instance v2, Lcom/android/server/display/RhythmicEyeCareManager; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p1, v3}, Lcom/android/server/display/RhythmicEyeCareManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mRhythmicEyeCareManager = v2;
/* .line 255 */
(( com.android.server.display.RhythmicEyeCareManager ) v2 ).setRhythmicEyeCareListener ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->setRhythmicEyeCareListener(Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;)V
/* .line 256 */
/* nop */
/* .line 257 */
com.android.server.display.DisplayFeatureManagerServiceStub .getInstance ( );
/* check-cast v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl; */
this.mDisplayFeatureServiceImpl = v0;
/* .line 258 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initServiceDeathRecipient()V */
/* .line 259 */
/* monitor-enter v1 */
/* .line 260 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initWrapperLocked()V */
/* .line 261 */
/* monitor-exit v1 */
/* .line 262 */
return;
/* .line 261 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
private void doDieLocked ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "flag" # I */
/* .line 1044 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
/* .line 1045 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFVideoInformation(ZFIIF)V */
/* .line 1047 */
} // :cond_0
return;
} // .end method
private Integer getEffectedDisplayIndex ( Long p0 ) {
/* .locals 6 */
/* .param p1, "physicalDisplayId" # J */
/* .line 526 */
int v0 = 0; // const/4 v0, 0x0
/* .line 527 */
/* .local v0, "result":I */
/* iget-boolean v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mIsFoldOrFlip:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 528 */
android.os.Parcel .obtain ( );
/* .line 529 */
/* .local v1, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 531 */
/* .local v2, "reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v3 = "android.view.android.hardware.display.IDisplayManager"; // const-string v3, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 532 */
(( android.os.Parcel ) v1 ).writeLong ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V
/* .line 533 */
final String v3 = "display"; // const-string v3, "display"
android.os.ServiceManager .getService ( v3 );
/* .line 534 */
/* .local v3, "b":Landroid/os/IBinder; */
/* const v4, 0xfffffb */
int v5 = 0; // const/4 v5, 0x0
/* .line 535 */
v4 = (( android.os.Parcel ) v2 ).readInt ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v0, v4 */
/* .line 539 */
} // .end local v3 # "b":Landroid/os/IBinder;
} // :goto_0
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 540 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 541 */
/* .line 539 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 536 */
/* :catch_0 */
/* move-exception v3 */
/* .line 537 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* .line 539 */
} // :goto_1
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 540 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 541 */
/* throw v3 */
/* .line 543 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // .end local v2 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
} // .end method
private getEffectedDisplayIndex ( ) {
/* .locals 6 */
/* .line 504 */
int v0 = 0; // const/4 v0, 0x0
/* filled-new-array {v0}, [I */
/* .line 505 */
/* .local v1, "result":[I */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mIsFoldOrFlip:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 506 */
android.os.Parcel .obtain ( );
/* .line 507 */
/* .local v2, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 509 */
/* .local v3, "reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v4 = "android.view.android.hardware.display.IDisplayManager"; // const-string v4, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) v2 ).writeInterfaceToken ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 510 */
final String v4 = "display"; // const-string v4, "display"
android.os.ServiceManager .getService ( v4 );
/* .line 511 */
/* .local v4, "b":Landroid/os/IBinder; */
/* const v5, 0xfffffc */
/* .line 512 */
v0 = (( android.os.Parcel ) v3 ).readInt ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I
/* .line 513 */
/* .local v0, "count":I */
/* new-array v5, v0, [I */
/* move-object v1, v5 */
/* .line 514 */
(( android.os.Parcel ) v3 ).readIntArray ( v1 ); // invoke-virtual {v3, v1}, Landroid/os/Parcel;->readIntArray([I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
} // .end local v0 # "count":I
} // .end local v4 # "b":Landroid/os/IBinder;
/* .line 518 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 515 */
/* :catch_0 */
/* move-exception v0 */
/* .line 516 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 518 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 519 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 520 */
/* .line 518 */
} // :goto_1
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 519 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 520 */
/* throw v0 */
/* .line 522 */
} // .end local v2 # "data":Landroid/os/Parcel;
} // .end local v3 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
} // .end method
private com.android.server.display.expertmode.ExpertData getExpertData ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1573 */
com.android.server.display.expertmode.ExpertData .getFromDatabase ( p1 );
/* .line 1575 */
/* .local v0, "data":Lcom/android/server/display/expertmode/ExpertData; */
/* if-nez v0, :cond_0 */
/* .line 1576 */
com.android.server.display.expertmode.ExpertData .getDefaultValue ( );
/* .line 1578 */
} // :cond_0
} // .end method
private Integer getScreenDpiMode ( ) {
/* .locals 2 */
/* .line 444 */
final String v0 = "persist.vendor.dfps.level"; // const-string v0, "persist.vendor.dfps.level"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
private void handleAutoAdjustChange ( ) {
/* .locals 2 */
/* .line 1275 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1276 */
int v0 = 3; // const/4 v0, 0x3
/* const/16 v1, 0x100 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1279 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeCTLevel()V */
/* .line 1280 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V */
/* .line 1282 */
} // :goto_0
return;
} // .end method
private void handleDisplayFeatureInfoChanged ( Integer p0, java.lang.Object...p1 ) {
/* .locals 6 */
/* .param p1, "caseId" # I */
/* .param p2, "params" # [Ljava/lang/Object; */
/* .line 380 */
/* array-length v0, p2 */
int v1 = 3; // const/4 v1, 0x3
int v2 = 4; // const/4 v2, 0x4
int v3 = 0; // const/4 v3, 0x0
/* if-lez v0, :cond_b */
/* .line 381 */
/* const/16 v0, 0x2710 */
/* if-ne p1, v0, :cond_0 */
/* .line 382 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 383 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 382 */
int v5 = 6; // const/4 v5, 0x6
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 383 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 385 */
} // :cond_0
/* const/16 v0, 0x2733 */
/* if-ne p1, v0, :cond_1 */
/* .line 386 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 387 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 386 */
/* const/16 v5, 0xb */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 387 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 389 */
} // :cond_1
/* const/16 v0, 0x2735 */
/* if-ne p1, v0, :cond_2 */
/* .line 390 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 391 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 390 */
/* const/16 v5, 0x11 */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 391 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 393 */
} // :cond_2
/* if-nez p1, :cond_3 */
/* .line 394 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 395 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 394 */
(( android.os.Handler ) v0 ).obtainMessage ( v1, v4, v3 ); // invoke-virtual {v0, v1, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 395 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 397 */
} // :cond_3
/* const/16 v0, 0x7530 */
/* if-ne p1, v0, :cond_4 */
/* .line 398 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 399 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 398 */
/* const/16 v5, 0xd */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 399 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 401 */
} // :cond_4
/* const/16 v0, 0x7531 */
/* if-ne p1, v0, :cond_5 */
/* .line 402 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 403 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 402 */
/* const/16 v5, 0x16 */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 403 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 405 */
} // :cond_5
/* const v0, 0x9c40 */
/* if-ne p1, v0, :cond_6 */
/* .line 406 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 407 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 406 */
/* const/16 v5, 0xe */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 407 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 409 */
} // :cond_6
/* const v0, 0xc350 */
/* if-ne p1, v0, :cond_7 */
/* .line 410 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 411 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 410 */
/* const/16 v5, 0x14 */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 411 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 413 */
} // :cond_7
/* const/16 v0, 0x2734 */
/* if-ne p1, v0, :cond_8 */
/* .line 414 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 415 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 414 */
/* const/16 v5, 0xf */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 415 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 418 */
} // :cond_8
/* const v0, 0xea60 */
/* if-ne p1, v0, :cond_9 */
/* .line 419 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* const/16 v5, 0x1e */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 422 */
} // :cond_9
/* const v0, 0x11170 */
/* if-ne p1, v0, :cond_a */
/* .line 423 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* const/16 v5, 0x28 */
(( android.os.Handler ) v0 ).obtainMessage ( v5, v4, v3 ); // invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 426 */
} // :cond_a
/* const v0, 0x17318 */
/* if-ne p1, v0, :cond_b */
/* .line 427 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 428 */
v0 = this.mHandler;
/* aget-object v4, p2, v3 */
/* check-cast v4, Ljava/lang/Integer; */
/* .line 429 */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 428 */
(( android.os.Handler ) v0 ).obtainMessage ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 429 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 432 */
} // :cond_b
/* const/16 v0, 0x4e20 */
/* if-ne p1, v0, :cond_c */
/* array-length v0, p2 */
/* if-lt v0, v2, :cond_c */
/* .line 434 */
com.android.internal.os.SomeArgs .obtain ( );
/* .line 435 */
/* .local v0, "args":Lcom/android/internal/os/SomeArgs; */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v2, p2, v2 */
this.arg1 = v2;
/* .line 436 */
int v2 = 2; // const/4 v2, 0x2
/* aget-object v2, p2, v2 */
this.arg2 = v2;
/* .line 437 */
/* aget-object v1, p2, v1 */
this.arg3 = v1;
/* .line 438 */
v1 = this.mHandler;
/* aget-object v2, p2, v3 */
/* check-cast v2, Ljava/lang/Integer; */
/* .line 439 */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 438 */
int v4 = 7; // const/4 v4, 0x7
(( android.os.Handler ) v1 ).obtainMessage ( v4, v2, v3, v0 ); // invoke-virtual {v1, v4, v2, v3, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 439 */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 441 */
} // .end local v0 # "args":Lcom/android/internal/os/SomeArgs;
} // :cond_c
return;
} // .end method
private void handleGameModeChange ( ) {
/* .locals 9 */
/* .line 1383 */
v0 = this.mResolver;
final String v1 = "screen_game_mode"; // const-string v1, "screen_game_mode"
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
/* .line 1385 */
/* .local v0, "gameMode":I */
v1 = this.mResolver;
final String v4 = "game_hdr_level"; // const-string v4, "game_hdr_level"
v1 = android.provider.Settings$System .getIntForUser ( v1,v4,v2,v3 );
/* .line 1387 */
/* .local v1, "gameHdrLevel":I */
/* and-int/lit8 v3, v0, 0x2 */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_0
/* move v3, v4 */
} // :cond_0
/* move v3, v2 */
/* .line 1388 */
/* .local v3, "gameHdrEnabled":Z */
} // :goto_0
/* and-int/lit8 v5, v0, 0x1 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* move v5, v4 */
} // :cond_1
/* move v5, v2 */
/* .line 1390 */
/* .local v5, "forceDisableEyecare":Z */
} // :goto_1
/* iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z */
/* if-eq v6, v5, :cond_2 */
/* .line 1391 */
/* iput-boolean v5, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z */
/* .line 1394 */
} // :cond_2
/* iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGameHdrEnabled:Z */
int v7 = 2; // const/4 v7, 0x2
/* if-eq v6, v3, :cond_8 */
/* .line 1395 */
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGameHdrEnabled:Z */
/* .line 1396 */
/* const/16 v6, 0x13 */
/* if-nez v3, :cond_3 */
/* .line 1398 */
/* invoke-direct {p0, v6, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1399 */
/* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->handleScreenSchemeChange(Z)V */
/* .line 1403 */
} // :cond_3
/* sget-boolean v8, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
/* if-nez v8, :cond_5 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* iget-boolean v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
/* if-nez v8, :cond_5 */
} // :cond_4
/* iget-boolean v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
/* if-nez v8, :cond_8 */
/* .line 1407 */
} // :cond_5
if ( v5 != null) { // if-eqz v5, :cond_7
/* iget-boolean v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 1408 */
/* iget v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
/* if-ne v8, v7, :cond_6 */
/* .line 1409 */
v8 = this.mRhythmicEyeCareManager;
(( com.android.server.display.RhythmicEyeCareManager ) v8 ).setModeEnable ( v2 ); // invoke-virtual {v8, v2}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V
/* .line 1411 */
} // :cond_6
/* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V */
/* .line 1413 */
} // :goto_2
/* invoke-direct {p0, v2, v4}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V */
/* .line 1415 */
} // :cond_7
/* invoke-direct {p0, v6, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1422 */
} // :cond_8
} // :goto_3
/* sget-boolean v6, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
/* if-nez v6, :cond_9 */
/* if-nez v3, :cond_10 */
} // :cond_9
/* iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
if ( v6 != null) { // if-eqz v6, :cond_10
/* .line 1423 */
/* iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z */
if ( v6 != null) { // if-eqz v6, :cond_b
/* .line 1424 */
/* if-nez v5, :cond_a */
/* .line 1425 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleAutoAdjustChange()V */
/* .line 1427 */
} // :cond_a
/* invoke-direct {p0, v2, v4}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V */
/* .line 1433 */
} // :cond_b
/* iget v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
/* if-ne v6, v7, :cond_d */
/* .line 1434 */
v6 = this.mRhythmicEyeCareManager;
/* if-nez v5, :cond_c */
/* move v7, v4 */
} // :cond_c
/* move v7, v2 */
} // :goto_4
(( com.android.server.display.RhythmicEyeCareManager ) v6 ).setModeEnable ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V
/* .line 1436 */
} // :cond_d
if ( v5 != null) { // if-eqz v5, :cond_e
/* move v6, v2 */
} // :cond_e
/* invoke-direct {p0, v6}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V */
/* .line 1438 */
} // :goto_5
/* if-nez v5, :cond_f */
} // :cond_f
/* move v4, v2 */
} // :goto_6
/* invoke-direct {p0, v4, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V */
/* .line 1441 */
} // :cond_10
} // :goto_7
return;
} // .end method
private void handleReadingModeChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "immediate" # Z */
/* .line 1148 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
int v1 = 2; // const/4 v1, 0x2
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1149 */
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 1150 */
/* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V */
/* .line 1151 */
v0 = this.mRhythmicEyeCareManager;
(( com.android.server.display.RhythmicEyeCareManager ) v0 ).setModeEnable ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V
/* .line 1153 */
} // :cond_0
/* invoke-direct {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V */
/* .line 1155 */
} // :goto_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1156 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleAutoAdjustChange()V */
/* .line 1158 */
} // :cond_1
/* invoke-direct {p0, v3, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V */
/* .line 1161 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
/* if-ne v0, v1, :cond_3 */
/* .line 1162 */
v0 = this.mRhythmicEyeCareManager;
(( com.android.server.display.RhythmicEyeCareManager ) v0 ).setModeEnable ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V
/* .line 1164 */
} // :cond_3
/* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V */
/* .line 1165 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z */
/* invoke-direct {p0, v2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V */
/* .line 1167 */
} // :goto_1
return;
} // .end method
private void handleScreenSchemeChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "userChange" # Z */
/* .line 1355 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGameHdrEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z */
/* if-nez v0, :cond_1 */
/* .line 1357 */
return;
/* .line 1360 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeCTLevel:I */
/* .line 1361 */
/* .local v0, "value":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 1363 */
/* .local v1, "mode":I */
/* iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeModeType:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 1364 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1365 */
} // :cond_2
int v3 = 3; // const/4 v3, 0x3
/* if-ne v2, v3, :cond_3 */
/* .line 1366 */
int v1 = 2; // const/4 v1, 0x2
/* .line 1367 */
} // :cond_3
int v3 = 4; // const/4 v3, 0x4
/* if-ne v2, v3, :cond_6 */
/* .line 1373 */
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 1374 */
} // :cond_4
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->setExpertScreenMode()V */
/* .line 1376 */
} // :cond_5
return;
/* .line 1378 */
} // :cond_6
} // :goto_0
/* invoke-direct {p0, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1379 */
return;
} // .end method
private void handleTrueToneModeChange ( ) {
/* .locals 2 */
/* .line 1453 */
/* const/16 v0, 0x20 */
/* iget v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mTrueToneModeEnabled:I */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1455 */
return;
} // .end method
private void handleUnlimitedColorLevelChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "userChange" # Z */
/* .line 1445 */
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeModeType:I */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 1447 */
} // :cond_0
/* const/16 v0, 0x17 */
/* iget v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeCTLevel:I */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1450 */
} // :cond_1
return;
} // .end method
private void initServiceDeathRecipient ( ) {
/* .locals 1 */
/* .line 632 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 633 */
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$5; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$5;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
this.mHwBinderDeathHandler = v0;
/* .line 643 */
} // :cond_0
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$6; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$6;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
this.mBinderDeathHandler = v0;
/* .line 653 */
} // :goto_0
return;
} // .end method
private void initWrapperLocked ( ) {
/* .locals 6 */
/* .line 595 */
final String v0 = "DisplayFeatureManagerService"; // const-string v0, "DisplayFeatureManagerService"
try { // :try_start_0
/* sget-boolean v1, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 597 */
/* if-nez v1, :cond_0 */
/* .line 598 */
/* const-string/jumbo v2, "vendor.xiaomi.hardware.displayfeature@1.0::IDisplayFeature" */
} // :cond_0
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v2 ).getString ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 599 */
/* .local v2, "hidlServiceName":Ljava/lang/String; */
} // :goto_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initProxyLocked CONFIG_SERVICENAME_RESOURCEID: "; // const-string v4, "initProxyLocked CONFIG_SERVICENAME_RESOURCEID: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " hidlServiceName: "; // const-string v3, " hidlServiceName: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 601 */
final String v1 = "default"; // const-string v1, "default"
android.os.HwBinder .getService ( v2,v1 );
/* .line 602 */
/* .local v1, "hb":Landroid/os/IHwBinder; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 603 */
v3 = this.mHwBinderDeathHandler;
/* const-wide/16 v4, 0x2711 */
/* .line 604 */
/* new-instance v3, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper; */
/* invoke-direct {v3, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/Object;)V */
this.mWrapper = v3;
/* .line 606 */
} // .end local v1 # "hb":Landroid/os/IHwBinder;
} // .end local v2 # "hidlServiceName":Ljava/lang/String;
} // :cond_1
/* .line 607 */
} // :cond_2
final String v1 = "initProxyLocked aidlServiceName: vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature/default"; // const-string v1, "initProxyLocked aidlServiceName: vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature/default"
android.util.Slog .d ( v0,v1 );
/* .line 608 */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature/default" */
android.os.ServiceManager .getService ( v1 );
/* .line 609 */
/* .local v1, "b":Landroid/os/IBinder; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 610 */
v2 = this.mBinderDeathHandler;
int v3 = 0; // const/4 v3, 0x0
/* .line 611 */
/* new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper; */
/* invoke-direct {v2, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/Object;)V */
this.mWrapper = v2;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/util/NoSuchElementException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 616 */
} // .end local v1 # "b":Landroid/os/IBinder;
} // :cond_3
} // :goto_1
/* .line 614 */
/* :catch_0 */
/* move-exception v1 */
/* .line 615 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "initProxyLocked failed."; // const-string v2, "initProxyLocked failed."
android.util.Slog .e ( v0,v2,v1 );
/* .line 617 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private Boolean isDarkModeEnable ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .line 1484 */
/* nop */
/* .line 1485 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1484 */
/* const-string/jumbo v1, "ui_night_mode" */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
int v1 = 2; // const/4 v1, 0x2
/* if-ne v1, v0, :cond_0 */
} // :cond_0
/* move v2, v3 */
} // :goto_0
} // .end method
private Boolean isForegroundApp ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 980 */
/* const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
/* .line 981 */
/* .local v0, "mAtmInternal":Lcom/android/server/wm/ActivityTaskManagerInternal; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.wm.ActivityTaskManagerInternal ) v0 ).getTopApp ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 982 */
/* .local v1, "wpc":Lcom/android/server/wm/WindowProcessController; */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
v3 = (( com.android.server.wm.WindowProcessController ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I
} // :cond_1
/* move v3, v2 */
/* .line 983 */
/* .local v3, "topAppPid":I */
} // :goto_1
/* if-ne p1, v3, :cond_2 */
/* .line 984 */
int v2 = 1; // const/4 v2, 0x1
/* .line 986 */
} // :cond_2
} // .end method
private Boolean isSupportSmartEyeCare ( ) {
/* .locals 2 */
/* .line 1285 */
/* const-string/jumbo v0, "support_smart_eyecare" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
} // .end method
private void loadSettings ( ) {
/* .locals 1 */
/* .line 818 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeEnable()V */
/* .line 819 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeType()V */
/* .line 820 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperColorType()V */
/* .line 821 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->resetLocalPaperLevelIfNeed()V */
/* .line 824 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateAutoAdjustEnable()V */
/* .line 827 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeModeType()V */
/* .line 828 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeCTLevel()V */
/* .line 831 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_TRUETONE_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 832 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateTrueToneModeEnable()V */
/* .line 834 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateDeskTopMode()V */
/* .line 835 */
return;
} // .end method
private void notifyCurrentGrayScaleChanged ( ) {
/* .locals 4 */
/* .line 1559 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1560 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "current_gray_scale"; // const-string v1, "current_gray_scale"
/* iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mCurrentGrayScale:F */
(( android.os.Bundle ) v0 ).putFloat ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V
/* .line 1561 */
v1 = this.mDisplayManagerInternal;
int v2 = 0; // const/4 v2, 0x0
int v3 = 5; // const/4 v3, 0x5
(( android.hardware.display.DisplayManagerInternal ) v1 ).notifyDisplayManager ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/display/DisplayManagerInternal;->notifyDisplayManager(IILandroid/os/Bundle;)V
/* .line 1563 */
return;
} // .end method
private void notifyGrayScaleChanged ( ) {
/* .locals 4 */
/* .line 1552 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1553 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "gray_scale"; // const-string v1, "gray_scale"
/* iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGrayScale:F */
(( android.os.Bundle ) v0 ).putFloat ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V
/* .line 1554 */
v1 = this.mDisplayManagerInternal;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
(( android.hardware.display.DisplayManagerInternal ) v1 ).notifyDisplayManager ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/display/DisplayManagerInternal;->notifyDisplayManager(IILandroid/os/Bundle;)V
/* .line 1556 */
return;
} // .end method
private void notifyHdrStateChanged ( ) {
/* .locals 4 */
/* .line 1566 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1567 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "dolby_version_state"; // const-string v1, "dolby_version_state"
/* iget-boolean v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDolbyState:Z */
(( android.os.Bundle ) v0 ).putBoolean ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1568 */
v1 = this.mDisplayManagerInternal;
int v2 = 0; // const/4 v2, 0x0
int v3 = 3; // const/4 v3, 0x3
(( android.hardware.display.DisplayManagerInternal ) v1 ).notifyDisplayManager ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/display/DisplayManagerInternal;->notifyDisplayManager(IILandroid/os/Bundle;)V
/* .line 1570 */
return;
} // .end method
private void notifySFColorMode ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mode" # I */
/* .line 717 */
v0 = this.mResolver;
final String v1 = "display_color_mode"; // const-string v1, "display_color_mode"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .putIntForUser ( v0,v1,p1,v2 );
/* .line 719 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 720 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 721 */
android.os.Parcel .obtain ( );
/* .line 722 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 723 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 725 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x792f */
try { // :try_start_0
/* .line 727 */
final String v2 = "persist.sys.sf.native_mode"; // const-string v2, "persist.sys.sf.native_mode"
java.lang.Integer .toString ( p1 );
android.os.SystemProperties .set ( v2,v3 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 731 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 732 */
/* .line 731 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 728 */
/* :catch_0 */
/* move-exception v2 */
/* .line 729 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to notify dfps mode to SurfaceFlinger"; // const-string v4, "Failed to notify dfps mode to SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 731 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 732 */
/* throw v2 */
/* .line 734 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void notifySFDCParseState ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "state" # I */
/* .line 754 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 755 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 756 */
android.os.Parcel .obtain ( );
/* .line 757 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 758 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 760 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x793c */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 765 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 766 */
/* .line 765 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 762 */
/* :catch_0 */
/* move-exception v2 */
/* .line 763 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to notify dc parse state to SurfaceFlinger"; // const-string v4, "Failed to notify dc parse state to SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 765 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 766 */
/* throw v2 */
/* .line 768 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void notifySFDfpsMode ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "mode" # I */
/* .param p2, "msg" # I */
/* .line 673 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 674 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 675 */
android.os.Parcel .obtain ( );
/* .line 676 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 677 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 679 */
/* const/16 v2, 0x11 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
/* if-ne p2, v2, :cond_0 */
/* .line 680 */
/* const/16 v2, 0x793d */
try { // :try_start_0
/* .line 683 */
} // :cond_0
/* const/16 v2, 0x793b */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 689 */
} // :goto_0
/* nop */
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 690 */
/* .line 689 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 686 */
/* :catch_0 */
/* move-exception v2 */
/* .line 687 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to notify dfps mode to SurfaceFlinger"; // const-string v4, "Failed to notify dfps mode to SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 689 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_2
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 690 */
/* throw v2 */
/* .line 692 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_1
} // :goto_3
return;
} // .end method
private void notifySFPccLevel ( Integer p0, Float p1, Float p2, Float p3 ) {
/* .locals 5 */
/* .param p1, "level" # I */
/* .param p2, "red" # F */
/* .param p3, "green" # F */
/* .param p4, "blue" # F */
/* .line 695 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 696 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 697 */
android.os.Parcel .obtain ( );
/* .line 698 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 699 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 700 */
(( android.os.Parcel ) v1 ).writeFloat ( p2 ); // invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 701 */
(( android.os.Parcel ) v1 ).writeFloat ( p3 ); // invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 702 */
(( android.os.Parcel ) v1 ).writeFloat ( p4 ); // invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 704 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x797d */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 709 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 710 */
/* .line 709 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 706 */
/* :catch_0 */
/* move-exception v2 */
/* .line 707 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to notifySurfaceFlinger"; // const-string v4, "Failed to notifySurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 709 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 710 */
/* throw v2 */
/* .line 712 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void notifySFSecondaryFrameRate ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "rateState" # I */
/* .line 771 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 772 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 773 */
android.os.Parcel .obtain ( );
/* .line 774 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 775 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 777 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x7991 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 782 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 783 */
/* .line 782 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 779 */
/* :catch_0 */
/* move-exception v2 */
/* .line 780 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to notify dc parse state to SurfaceFlinger"; // const-string v4, "Failed to notify dc parse state to SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 782 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 783 */
/* throw v2 */
/* .line 785 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void notifySFVideoInformation ( Boolean p0, Float p1, Integer p2, Integer p3, Float p4 ) {
/* .locals 7 */
/* .param p1, "bulletChatStatus" # Z */
/* .param p2, "frameRate" # F */
/* .param p3, "width" # I */
/* .param p4, "height" # I */
/* .param p5, "compressionRatio" # F */
/* .line 789 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 790 */
/* .local v0, "surfaceFlinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 791 */
android.os.Parcel .obtain ( );
/* .line 792 */
/* .local v1, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 793 */
/* .local v2, "reply":Landroid/os/Parcel; */
final String v3 = "android.ui.ISurfaceComposer"; // const-string v3, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 794 */
(( android.os.Parcel ) v1 ).writeBoolean ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 795 */
(( android.os.Parcel ) v1 ).writeFloat ( p2 ); // invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 796 */
(( android.os.Parcel ) v1 ).writeInt ( p3 ); // invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 797 */
(( android.os.Parcel ) v1 ).writeInt ( p4 ); // invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 798 */
(( android.os.Parcel ) v1 ).writeFloat ( p5 ); // invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 799 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "notifySFVideoInformation bulletChatStatus:"; // const-string v4, "notifySFVideoInformation bulletChatStatus:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", frameRate:"; // const-string v4, ", frameRate:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", resolution:"; // const-string v4, ", resolution:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v4, "x" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", compressionRatio:"; // const-string v4, ", compressionRatio:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p5 ); // invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DisplayFeatureManagerService"; // const-string v4, "DisplayFeatureManagerService"
android.util.Slog .d ( v4,v3 );
/* .line 805 */
/* const/16 v3, 0x798c */
int v5 = 0; // const/4 v5, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 810 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 811 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 812 */
/* .line 810 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 807 */
/* :catch_0 */
/* move-exception v3 */
/* .line 808 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "notifySFVideoInformation RemoteException:"; // const-string v6, "notifySFVideoInformation RemoteException:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 810 */
/* nop */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 811 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 812 */
/* throw v3 */
/* .line 814 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // .end local v2 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void notifySFWcgState ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .line 656 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 657 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 658 */
android.os.Parcel .obtain ( );
/* .line 659 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 660 */
(( android.os.Parcel ) v1 ).writeBoolean ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 662 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x797c */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 667 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 668 */
/* .line 667 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 664 */
/* :catch_0 */
/* move-exception v2 */
/* .line 665 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to notifySurfaceFlinger"; // const-string v4, "Failed to notifySurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 667 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 668 */
/* throw v2 */
/* .line 670 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void resetLocalPaperLevelIfNeed ( ) {
/* .locals 5 */
/* .line 1053 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* int-to-float v0, v0 */
/* cmpg-float v0, v0, v1 */
/* if-gez v0, :cond_0 */
/* .line 1054 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_paper_mode_level" */
int v2 = -1; // const/4 v2, -0x1
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
/* .line 1058 */
/* .local v0, "tempValue":I */
/* if-eq v0, v2, :cond_0 */
/* .line 1059 */
/* iput v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* .line 1060 */
v4 = this.mResolver;
android.provider.Settings$System .putIntForUser ( v4,v1,v2,v3 );
/* .line 1065 */
} // .end local v0 # "tempValue":I
} // :cond_0
return;
} // .end method
private void sendHbmState ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "type" # I */
/* .line 1260 */
android.cameracovered.MiuiCameraCoveredManager .hbmCoveredAnimation ( p1 );
/* .line 1261 */
return;
} // .end method
private void sendMuraState ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "type" # I */
/* .line 1264 */
android.cameracovered.MiuiCameraCoveredManager .cupMuraCoveredAnimation ( p1 );
/* .line 1265 */
return;
} // .end method
private void setDarkModeEnable ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "enable" # Z */
/* .line 1476 */
/* const-class v0, Landroid/app/UiModeManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/UiModeManager; */
/* .line 1477 */
/* .local v0, "manager":Landroid/app/UiModeManager; */
/* if-nez v0, :cond_0 */
/* .line 1478 */
return;
/* .line 1480 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
int v1 = 2; // const/4 v1, 0x2
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :goto_0
(( android.app.UiModeManager ) v0 ).setNightMode ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V
/* .line 1481 */
return;
} // .end method
private void setDeathCallbackLocked ( android.os.IBinder p0, Integer p1, Boolean p2 ) {
/* .locals 0 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .param p3, "register" # Z */
/* .line 990 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 991 */
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).registerDeathCallbackLocked ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->registerDeathCallbackLocked(Landroid/os/IBinder;I)V
/* .line 993 */
} // :cond_0
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).unregisterDeathCallbackLocked ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
/* .line 995 */
} // :goto_0
return;
} // .end method
private void setDisplayFeature ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "displayId" # I */
/* .param p2, "mode" # I */
/* .param p3, "value" # I */
/* .param p4, "cookie" # I */
/* .line 491 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 492 */
try { // :try_start_0
final String v1 = "DisplayFeatureManagerService"; // const-string v1, "DisplayFeatureManagerService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setScreenEffect displayId=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mode="; // const-string v3, " mode="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " value="; // const-string v3, " value="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " cookie="; // const-string v3, " cookie="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " from pid="; // const-string v3, " from pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 493 */
v3 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 492 */
android.util.Slog .d ( v1,v2 );
/* .line 494 */
v1 = this.mWrapper;
/* if-nez v1, :cond_0 */
/* .line 495 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initWrapperLocked()V */
/* .line 497 */
} // :cond_0
v1 = this.mWrapper;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 498 */
(( com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper ) v1 ).setFeature ( p1, p2, p3, p4 ); // invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->setFeature(IIII)V
/* .line 500 */
} // :cond_1
/* monitor-exit v0 */
/* .line 501 */
return;
/* .line 500 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setExpertScreenMode ( ) {
/* .locals 4 */
/* .line 1582 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_DISPLAY_EXPERT_MODE:Z */
/* if-nez v0, :cond_0 */
/* .line 1583 */
final String v0 = "DisplayFeatureManagerService"; // const-string v0, "DisplayFeatureManagerService"
final String v1 = "device don\'t support DISPLAY_EXPERT_MODE"; // const-string v1, "device don\'t support DISPLAY_EXPERT_MODE"
android.util.Slog .w ( v0,v1 );
/* .line 1586 */
} // :cond_0
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->getExpertData(Landroid/content/Context;)Lcom/android/server/display/expertmode/ExpertData; */
/* .line 1587 */
/* .local v0, "data":Lcom/android/server/display/expertmode/ExpertData; */
/* if-nez v0, :cond_1 */
/* .line 1588 */
return;
/* .line 1590 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 1591 */
/* .local v1, "cookie":I */
} // :goto_0
/* const/16 v2, 0x9 */
/* if-ge v1, v2, :cond_2 */
/* .line 1592 */
/* nop */
/* .line 1593 */
v2 = (( com.android.server.display.expertmode.ExpertData ) v0 ).getByCookie ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/expertmode/ExpertData;->getByCookie(I)I
/* .line 1592 */
/* const/16 v3, 0x1a */
/* invoke-direct {p0, v3, v2, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V */
/* .line 1591 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1595 */
} // .end local v1 # "cookie":I
} // :cond_2
return;
} // .end method
private void setPaperColors ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 1296 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* if-eq p1, v1, :cond_1 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne p1, v2, :cond_0 */
} // :cond_0
/* move v1, v0 */
/* .line 1297 */
/* .local v1, "isPaperColorType":Z */
} // :cond_1
} // :goto_0
/* nop */
/* .line 1298 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperColorType:I */
/* .line 1297 */
} // :cond_2
/* const/16 v2, 0x1f */
/* invoke-direct {p0, v2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1299 */
return;
} // .end method
private void setRhythmicColor ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "category" # I */
/* .param p2, "time" # I */
/* .line 1305 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z */
/* if-nez v0, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 1306 */
/* .local v0, "modeEnable":Z */
} // :goto_0
/* nop */
/* .line 1307 */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
/* move v1, v2 */
/* .line 1306 */
} // :goto_1
/* const/16 v2, 0x36 */
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).setRhythmicScreenEffect ( v2, v1, p1, p2 ); // invoke-virtual {p0, v2, v1, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setRhythmicScreenEffect(IIII)V
/* .line 1308 */
return;
} // .end method
private void setSDR2HDR ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mode" # I */
/* .line 737 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 738 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 739 */
android.os.Parcel .obtain ( );
/* .line 740 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 741 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 743 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const v4, 0x84d0 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 748 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 749 */
/* .line 748 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 745 */
/* :catch_0 */
/* move-exception v2 */
/* .line 746 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
final String v4 = "Failed to set sdr2hdr to SurfaceFlinger"; // const-string v4, "Failed to set sdr2hdr to SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 748 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 749 */
/* throw v2 */
/* .line 751 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void setScreenEffect ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .param p2, "value" # I */
/* .line 480 */
/* const/16 v0, 0xff */
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V */
/* .line 481 */
return;
} // .end method
private void setScreenEffectAll ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "userChange" # Z */
/* .line 455 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectColor(Z)V */
/* .line 458 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_TRUETONE_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 459 */
v0 = this.mHandler;
/* const/16 v1, 0x13 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 463 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
java.lang.Boolean .valueOf ( v1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 464 */
return;
} // .end method
private void setScreenEffectColor ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "userChange" # Z */
/* .line 469 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_UNLIMITED_COLOR_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 470 */
v1 = this.mHandler;
/* const/16 v2, 0x8 */
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 474 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
/* if-nez v1, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 475 */
} // :cond_1
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 477 */
} // :cond_2
return;
} // .end method
private void setScreenEffectInternal ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "mode" # I */
/* .param p2, "value" # I */
/* .param p3, "cookie" # I */
/* .line 484 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->getEffectedDisplayIndex()[I */
/* .line 485 */
/* .local v0, "displays":[I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_0 */
/* .line 486 */
/* invoke-direct {p0, v1, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDisplayFeature(IIII)V */
/* .line 485 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 488 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
private void setScreenEyeCare ( Boolean p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "enabled" # Z */
/* .param p2, "immediate" # Z */
/* .line 1326 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_PAPERMODE_ANIMATION:Z */
int v1 = 3; // const/4 v1, 0x3
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1328 */
v0 = this.mPaperModeAnimator;
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p2, :cond_0 */
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I */
int v3 = 1; // const/4 v3, 0x1
/* if-eq v0, v3, :cond_3 */
/* .line 1330 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* move v0, v2 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* mul-int/lit8 v0, v0, 0x2 */
/* div-int/2addr v0, v1 */
/* iget v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeMinRate:I */
v0 = java.lang.Math .max ( v0,v3 );
/* .line 1331 */
/* .local v0, "rate":I */
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_2
/* iget v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
} // :cond_2
/* move v3, v2 */
/* .line 1332 */
/* .local v3, "targetLevel":I */
} // :goto_1
v4 = this.mPaperModeAnimator;
v4 = (( com.android.server.display.MiuiRampAnimator ) v4 ).animateTo ( v3, v0 ); // invoke-virtual {v4, v3, v0}, Lcom/android/server/display/MiuiRampAnimator;->animateTo(II)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1333 */
return;
/* .line 1338 */
} // .end local v0 # "rate":I
} // .end local v3 # "targetLevel":I
} // :cond_3
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 1339 */
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* invoke-direct {p0, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1342 */
} // :cond_4
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1343 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V */
/* .line 1345 */
} // :cond_5
/* invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->handleScreenSchemeChange(Z)V */
/* .line 1348 */
} // :goto_2
return;
} // .end method
private void updateAutoAdjustEnable ( ) {
/* .locals 4 */
/* .line 881 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->isSupportSmartEyeCare()Z */
/* if-nez v0, :cond_0 */
/* .line 882 */
return;
/* .line 884 */
} // :cond_0
v0 = this.mResolver;
final String v1 = "screen_auto_adjust"; // const-string v1, "screen_auto_adjust"
int v2 = -2; // const/4 v2, -0x2
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v3,v2 );
if ( v0 != null) { // if-eqz v0, :cond_1
int v3 = 1; // const/4 v3, 0x1
} // :cond_1
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z */
/* .line 887 */
return;
} // .end method
private void updateClassicReadingModeCTLevel ( ) {
/* .locals 4 */
/* .line 919 */
v0 = this.mResolver;
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "screen_paper_mode_level" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* .line 922 */
return;
} // .end method
private void updateColorSchemeCTLevel ( ) {
/* .locals 4 */
/* .line 860 */
v0 = this.mResolver;
int v1 = 2; // const/4 v1, 0x2
int v2 = -2; // const/4 v2, -0x2
final String v3 = "screen_color_level"; // const-string v3, "screen_color_level"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeCTLevel:I */
/* .line 863 */
return;
} // .end method
private void updateColorSchemeModeType ( ) {
/* .locals 4 */
/* .line 869 */
v0 = this.mResolver;
int v2 = -2; // const/4 v2, -0x2
final String v3 = "screen_optimize_mode"; // const-string v3, "screen_optimize_mode"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeModeType:I */
/* .line 872 */
return;
} // .end method
private void updateDeskTopMode ( ) {
/* .locals 4 */
/* .line 974 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_dkt_mode"; // const-string v2, "miui_dkt_mode"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDeskTopModeEnabled:Z */
/* .line 976 */
v0 = this.mRhythmicEyeCareManager;
(( com.android.server.display.RhythmicEyeCareManager ) v0 ).updateDeskTopMode ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/display/RhythmicEyeCareManager;->updateDeskTopMode(Z)V
/* .line 977 */
return;
} // .end method
private void updatePaperColorType ( ) {
/* .locals 4 */
/* .line 851 */
v0 = this.mResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "screen_texture_color_type" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperColorType:I */
/* .line 854 */
return;
} // .end method
private void updatePaperMode ( Boolean p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .param p2, "immediate" # Z */
/* .line 1268 */
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1269 */
return;
/* .line 1271 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V */
/* .line 1272 */
return;
} // .end method
private void updatePaperReadingModeCTLevel ( ) {
/* .locals 4 */
/* .line 928 */
v0 = this.mResolver;
/* float-to-int v1, v1 */
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "screen_paper_texture_level" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* .line 931 */
return;
} // .end method
private void updateReadingModeCTLevel ( ) {
/* .locals 1 */
/* .line 947 */
/* iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 956 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateRhythmicModeCTLevel()V */
/* .line 957 */
/* .line 953 */
/* :pswitch_1 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperReadingModeCTLevel()V */
/* .line 954 */
/* .line 949 */
/* :pswitch_2 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateClassicReadingModeCTLevel()V */
/* .line 950 */
/* nop */
/* .line 961 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void updateReadingModeEnable ( ) {
/* .locals 4 */
/* .line 893 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "screen_paper_mode_enabled" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z */
/* .line 897 */
return;
} // .end method
private void updateReadingModeType ( ) {
/* .locals 4 */
/* .line 907 */
v0 = this.mResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
final String v3 = "screen_mode_type"; // const-string v3, "screen_mode_type"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I */
/* .line 912 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeCTLevel()V */
/* .line 913 */
return;
} // .end method
private void updateRhythmicModeCTLevel ( ) {
/* .locals 4 */
/* .line 937 */
v0 = this.mResolver;
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "screen_rhythmic_mode_level" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I */
/* .line 940 */
return;
} // .end method
private void updateTrueToneModeEnable ( ) {
/* .locals 4 */
/* .line 842 */
v0 = this.mResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "screen_true_tone" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mTrueToneModeEnabled:I */
/* .line 845 */
return;
} // .end method
private void updateVideoInformationIfNeeded ( Integer p0, Boolean p1, Float p2, Integer p3, Integer p4, Float p5, android.os.IBinder p6 ) {
/* .locals 6 */
/* .param p1, "pid" # I */
/* .param p2, "bulletChatStatus" # Z */
/* .param p3, "frameRate" # F */
/* .param p4, "width" # I */
/* .param p5, "height" # I */
/* .param p6, "compressionRatio" # F */
/* .param p7, "token" # Landroid/os/IBinder; */
/* .line 966 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->isForegroundApp(I)Z */
/* if-nez v0, :cond_0 */
/* .line 967 */
return;
/* .line 969 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p7, v0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V */
/* .line 970 */
/* move-object v0, p0 */
/* move v1, p2 */
/* move v2, p3 */
/* move v3, p4 */
/* move v4, p5 */
/* move v5, p6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFVideoInformation(ZFIIF)V */
/* .line 971 */
return;
} // .end method
/* # virtual methods */
public void loadDozeBrightnessThreshold ( ) {
/* .locals 6 */
/* .line 556 */
android.os.Parcel .obtain ( );
/* .line 557 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 559 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
final String v2 = "android.view.android.hardware.display.IDisplayManager"; // const-string v2, "android.view.android.hardware.display.IDisplayManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 560 */
final String v2 = "display"; // const-string v2, "display"
android.os.ServiceManager .getService ( v2 );
/* .line 561 */
/* .local v2, "b":Landroid/os/IBinder; */
/* const v3, 0xfffff7 */
int v4 = 0; // const/4 v4, 0x0
/* .line 562 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* .line 563 */
/* .local v3, "length":I */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v3, v5, :cond_0 */
/* .line 564 */
/* new-array v5, v3, [F */
/* .line 565 */
/* .local v5, "result":[F */
(( android.os.Parcel ) v1 ).readFloatArray ( v5 ); // invoke-virtual {v1, v5}, Landroid/os/Parcel;->readFloatArray([F)V
/* .line 566 */
/* aget v4, v5, v4 */
v4 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v4 );
/* iput v4, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMaxDozeBrightnessInt:I */
/* .line 567 */
int v4 = 1; // const/4 v4, 0x1
/* aget v4, v5, v4 */
v4 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v4 );
/* iput v4, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMinDozeBrightnessInt:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 572 */
} // .end local v2 # "b":Landroid/os/IBinder;
} // .end local v3 # "length":I
} // .end local v5 # "result":[F
/* :catchall_0 */
/* move-exception v2 */
/* .line 569 */
/* :catch_0 */
/* move-exception v2 */
/* .line 570 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 572 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 573 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 574 */
/* nop */
/* .line 575 */
return;
/* .line 572 */
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 573 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 574 */
/* throw v2 */
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "phase" # I */
/* .line 274 */
/* const/16 v0, 0x1f4 */
/* if-ne p1, v0, :cond_5 */
/* .line 276 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_PAPERMODE_ANIMATION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 277 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110b001f */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeMinRate:I */
/* .line 280 */
} // :cond_0
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
this.mSettingsObserver = v0;
/* .line 281 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_paper_mode_enabled" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 284 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_paper_mode_level" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 287 */
v0 = this.mResolver;
final String v1 = "screen_optimize_mode"; // const-string v1, "screen_optimize_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 290 */
v0 = this.mResolver;
final String v1 = "screen_color_level"; // const-string v1, "screen_color_level"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 293 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_texture_color_type" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 296 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_paper_texture_level" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 299 */
v0 = this.mResolver;
final String v1 = "screen_auto_adjust"; // const-string v1, "screen_auto_adjust"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 302 */
v0 = this.mResolver;
final String v1 = "screen_mode_type"; // const-string v1, "screen_mode_type"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 305 */
v0 = this.mResolver;
final String v1 = "screen_game_mode"; // const-string v1, "screen_game_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 308 */
v0 = this.mResolver;
final String v1 = "miui_dkt_mode"; // const-string v1, "miui_dkt_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 310 */
v0 = this.mContext;
/* new-instance v1, Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver-IA;)V */
/* new-instance v2, Landroid/content/IntentFilter; */
final String v5 = "android.intent.action.USER_SWITCHED"; // const-string v5, "android.intent.action.USER_SWITCHED"
/* invoke-direct {v2, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v0 ).registerReceiver ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 312 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_TRUETONE_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 313 */
v0 = this.mResolver;
/* const-string/jumbo v1, "screen_true_tone" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 318 */
} // :cond_1
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_PAPERMODE_ANIMATION:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 319 */
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$2; */
final String v1 = "papermode"; // const-string v1, "papermode"
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$2;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/String;)V */
/* .line 343 */
/* .local v0, "paperMode":Landroid/util/IntProperty;, "Landroid/util/IntProperty<Lcom/android/server/display/DisplayFeatureManagerService;>;" */
/* new-instance v1, Lcom/android/server/display/MiuiRampAnimator; */
/* invoke-direct {v1, p0, v0}, Lcom/android/server/display/MiuiRampAnimator;-><init>(Ljava/lang/Object;Landroid/util/IntProperty;)V */
this.mPaperModeAnimator = v1;
/* .line 344 */
/* new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$PaperModeAnimatListener; */
/* invoke-direct {v2, p0}, Lcom/android/server/display/DisplayFeatureManagerService$PaperModeAnimatListener;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
(( com.android.server.display.MiuiRampAnimator ) v1 ).setListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/MiuiRampAnimator;->setListener(Lcom/android/server/display/MiuiRampAnimator$Listener;)V
/* .line 347 */
} // .end local v0 # "paperMode":Landroid/util/IntProperty;, "Landroid/util/IntProperty<Lcom/android/server/display/DisplayFeatureManagerService;>;"
} // :cond_2
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_CALLBACK:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 348 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 349 */
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$3;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).registerCallback ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->registerCallback(Ljava/lang/Object;)V
/* .line 358 */
} // :cond_3
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$4; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$4;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).registerCallback ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->registerCallback(Ljava/lang/Object;)V
/* .line 369 */
} // :cond_4
} // :goto_0
v0 = this.mHandler;
/* const/16 v1, 0x15 */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 370 */
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).loadDozeBrightnessThreshold ( ); // invoke-virtual {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->loadDozeBrightnessThreshold()V
/* .line 371 */
} // :cond_5
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_6 */
/* .line 372 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z */
/* .line 373 */
/* sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->FPS_SWITCH_DEFAULT:Z */
/* if-nez v0, :cond_6 */
/* .line 374 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->getScreenDpiMode()I */
/* const/16 v1, 0x11 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFDfpsMode(II)V */
/* .line 377 */
} // :cond_6
} // :goto_1
return;
} // .end method
public void onStart ( ) {
/* .locals 2 */
/* .line 266 */
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V */
this.mLocalService = v0;
/* .line 267 */
v1 = this.mDisplayFeatureServiceImpl;
(( com.android.server.display.DisplayFeatureManagerServiceImpl ) v1 ).init ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->init(Lcom/android/server/display/DisplayFeatureManagerInternal;)V
/* .line 268 */
/* new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$BinderService; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Lcom/android/server/display/DisplayFeatureManagerService$BinderService-IA;)V */
final String v1 = "displayfeature"; // const-string v1, "displayfeature"
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).publishBinderService ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 269 */
/* const-class v0, Lcom/android/server/display/DisplayFeatureManagerInternal; */
v1 = this.mLocalService;
(( com.android.server.display.DisplayFeatureManagerService ) p0 ).publishLocalService ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 270 */
return;
} // .end method
public void registerCallback ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "callback" # Ljava/lang/Object; */
/* .line 621 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 622 */
try { // :try_start_0
v1 = this.mWrapper;
/* if-nez v1, :cond_0 */
/* .line 623 */
/* invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initWrapperLocked()V */
/* .line 625 */
} // :cond_0
v1 = this.mWrapper;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 626 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.display.DisplayFeatureManagerService$DisplayFeatureManagerWrapper ) v1 ).registerCallback ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->registerCallback(ILjava/lang/Object;)V
/* .line 628 */
} // :cond_1
/* monitor-exit v0 */
/* .line 629 */
return;
/* .line 628 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected void registerDeathCallbackLocked ( android.os.IBinder p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "flag" # I */
/* .line 998 */
v0 = this.mClientDeathCallbacks;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 999 */
final String v0 = "DisplayFeatureManagerService"; // const-string v0, "DisplayFeatureManagerService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Client token "; // const-string v2, "Client token "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " has already registered."; // const-string v2, " has already registered."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1000 */
return;
/* .line 1002 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1003 */
try { // :try_start_0
v1 = this.mClientDeathCallbacks;
/* new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/IBinder;I)V */
(( java.util.HashMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1004 */
/* monitor-exit v0 */
/* .line 1005 */
return;
/* .line 1004 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setDozeBrightness ( Long p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "physicalDisplayId" # J */
/* .param p3, "brightness" # I */
/* .line 578 */
/* move v0, p3 */
/* .line 579 */
/* .local v0, "value":I */
/* sget-boolean v1, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z */
/* if-nez v1, :cond_2 */
/* .line 580 */
v1 = com.android.server.display.DisplayFeatureManagerService$DozeBrightnessMode.DOZE_TO_NORMAL;
/* .line 581 */
/* .local v1, "modeId":Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode; */
/* iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMaxDozeBrightnessInt:I */
/* if-lt p3, v2, :cond_0 */
/* .line 582 */
v1 = com.android.server.display.DisplayFeatureManagerService$DozeBrightnessMode.DOZE_BRIGHTNESS_HBM;
/* .line 583 */
} // :cond_0
/* if-lez p3, :cond_1 */
/* .line 584 */
v1 = com.android.server.display.DisplayFeatureManagerService$DozeBrightnessMode.DOZE_BRIGHTNESS_LBM;
/* .line 586 */
} // :cond_1
} // :goto_0
v0 = (( com.android.server.display.DisplayFeatureManagerService$DozeBrightnessMode ) v1 ).ordinal ( ); // invoke-virtual {v1}, Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;->ordinal()I
/* .line 587 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setDozeBrightness, brightness: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", value: "; // const-string v3, ", value: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DisplayFeatureManagerService"; // const-string v3, "DisplayFeatureManagerService"
android.util.Slog .d ( v3,v2 );
/* .line 589 */
} // .end local v1 # "modeId":Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;
} // :cond_2
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->getEffectedDisplayIndex(J)I */
/* .line 590 */
/* .local v1, "index":I */
/* const/16 v2, 0x19 */
/* const/16 v3, 0xff */
/* invoke-direct {p0, v1, v2, v0, v3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDisplayFeature(IIII)V */
/* .line 591 */
return;
} // .end method
public void setRhythmicScreenEffect ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "screenModeType" # I */
/* .param p2, "screenModeValue" # I */
/* .param p3, "category" # I */
/* .param p4, "time" # I */
/* .line 1320 */
/* shl-int/lit8 v0, p3, 0x10 */
/* or-int/2addr v0, p4 */
/* .line 1321 */
/* .local v0, "cookie":I */
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V */
/* .line 1322 */
return;
} // .end method
protected void unregisterDeathCallbackLocked ( android.os.IBinder p0 ) {
/* .locals 3 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .line 1008 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 1009 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1010 */
try { // :try_start_0
v1 = this.mClientDeathCallbacks;
(( java.util.HashMap ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback; */
/* .line 1011 */
/* .local v1, "deathCallback":Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1012 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1014 */
} // .end local v1 # "deathCallback":Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1016 */
} // :cond_1
} // :goto_0
return;
} // .end method
