.class final Lcom/android/server/display/DisplayFeatureManagerService$BinderService;
.super Lmiui/hardware/display/IDisplayFeatureManager$Stub;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayFeatureManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BinderService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    .line 1532
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-direct {p0}, Lmiui/hardware/display/IDisplayFeatureManager$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;Lcom/android/server/display/DisplayFeatureManagerService$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    return-void
.end method


# virtual methods
.method public setScreenEffect(IIILandroid/os/IBinder;)V
    .locals 3
    .param p1, "mode"    # I
    .param p2, "value"    # I
    .param p3, "cookie"    # I
    .param p4, "token"    # Landroid/os/IBinder;

    .line 1535
    if-eqz p4, :cond_0

    .line 1536
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p4, v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetDeathCallbackLocked(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/IBinder;IZ)V

    .line 1538
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffectInternal(Lcom/android/server/display/DisplayFeatureManagerService;III)V

    .line 1539
    return-void
.end method
