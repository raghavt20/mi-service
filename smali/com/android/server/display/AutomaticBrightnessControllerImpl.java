public class com.android.server.display.AutomaticBrightnessControllerImpl extends com.android.server.display.AutomaticBrightnessControllerStub {
	 /* .source "AutomaticBrightnessControllerImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;, */
	 /* Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer BRIGHTENING;
private static final java.lang.String CAMERA_ROLE_IDS;
private static final Integer DARKENING;
private static final Integer DO_NOT_REDUCE_BRIGHTNESS_INTERVAL;
private static final Boolean IS_LIGHT_FOV_OPTIMIZATION_POLICY_ENABLE;
private static final Integer MSG_RESET_FAST_RATE;
private static final Integer MSG_UPDATE_OUT_PACKET_TIME;
private static final Long NON_UI_FAST_UPDATE_BRIGHTNESS_TIME;
private static final Float NON_UI_NOT_IN_POCKET;
private static final Integer PARALLEL_VIRTUAL_ROLE_ID;
private static final Integer SENSOR_TYPE_ASSIST;
private static final Integer SENSOR_TYPE_LIGHT_FOV;
private static final Integer SENSOR_TYPE_NON_UI;
private static final java.lang.String TAG;
private static final Integer TORCH_CLOSE_DELAY;
private static final Float TYPICAL_PROXIMITY_THRESHOLD;
private static final Integer VIRTUAL_BACK_ROLE_ID;
private static final Integer VIRTUAL_FRONT_ROLE_ID;
/* # instance fields */
private Float mAllowFastRateRatio;
private Integer mAllowFastRateTime;
private Float mAllowFastRateValue;
private Float mAmbientLux;
private Integer mAmbientLuxDirection;
private Boolean mAppliedDimming;
private Boolean mApplyingFastRate;
private com.android.server.display.AutomaticBrightnessController mAutomaticBrightnessController;
private Boolean mAutomaticBrightnessEnable;
private com.android.server.display.BrightnessMappingStrategy mBrightnessMapper;
private android.hardware.camera2.CameraManager mCameraManager;
private android.content.Context mContext;
private com.android.server.display.DaemonSensorPolicy mDaemonSensorPolicy;
private Boolean mDaemonSensorPolicyEnabled;
private Boolean mDebug;
private Boolean mDisableResetShortTermModel;
private com.android.server.display.DisplayDeviceConfig mDisplayDeviceConfig;
private com.android.server.display.DisplayPowerControllerImpl mDisplayPowerControllerImpl;
private com.android.server.display.DualSensorPolicy mDualSensorPolicy;
private Long mEnterGameTime;
private java.lang.String mExitFacingCameraIds;
private Boolean mFrontFlashAvailable;
private android.os.Handler mHandler;
private com.android.server.display.HysteresisLevelsImpl mHysteresisLevelsImpl;
private Boolean mIsAnimatePolicyDisable;
private Boolean mIsGameSceneEnable;
private Boolean mIsTorchOpen;
private Float mLastBrightness;
private android.hardware.Sensor mLightFovSensor;
private Long mLightSensorEnableTime;
private Float mNeedUseFastRateBrightness;
private android.hardware.Sensor mNonUiSensor;
private Float mNonUiSensorData;
private Boolean mNonUiSensorEnabled;
private Long mNotInPocketTime;
private Boolean mPendingUseFastRateDueToFirstAutoBrightness;
private Boolean mProximityPositive;
private android.hardware.Sensor mProximitySensor;
private Boolean mProximitySensorEnabled;
private Float mProximityThreshold;
private Integer mResetFastRateTime;
private com.android.server.display.SceneDetector mSceneDetector;
private android.hardware.SensorEventListener mSensorListener;
private android.hardware.SensorManager mSensorManager;
private Float mSkipTransitionLuxValue;
private Boolean mSlowChange;
private Float mStartBrightness;
private Float mStartSdrBrightness;
private Integer mState;
private final android.hardware.camera2.CameraManager$TorchCallback mTorchCallback;
private Long mTorchCloseTime;
private com.android.server.display.TouchCoverProtectionHelper mTouchAreaHelper;
private Boolean mUseAonFlareEnabled;
private Boolean mUseAssistSensorEnabled;
private Boolean mUseFastRateForVirtualSensor;
private Boolean mUseNonUiEnabled;
private Boolean mUseProximityEnabled;
/* # direct methods */
public static void $r8$lambda$5WxsJfFSpJVKPfZQVr86vmYf3Jk ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->lambda$stop$0()V */
	 return;
} // .end method
static Boolean -$$Nest$fgetmApplyingFastRate ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
} // .end method
static android.hardware.camera2.CameraManager -$$Nest$fgetmCameraManager ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mCameraManager;
} // .end method
static java.lang.String -$$Nest$fgetmExitFacingCameraIds ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mExitFacingCameraIds;
} // .end method
static Boolean -$$Nest$fgetmFrontFlashAvailable ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mFrontFlashAvailable:Z */
} // .end method
static Boolean -$$Nest$fgetmIsTorchOpen ( com.android.server.display.AutomaticBrightnessControllerImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z */
} // .end method
static void -$$Nest$fputmApplyingFastRate ( com.android.server.display.AutomaticBrightnessControllerImpl p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
	 return;
} // .end method
static void -$$Nest$fputmFrontFlashAvailable ( com.android.server.display.AutomaticBrightnessControllerImpl p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mFrontFlashAvailable:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsTorchOpen ( com.android.server.display.AutomaticBrightnessControllerImpl p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z */
	 return;
} // .end method
static void -$$Nest$fputmNeedUseFastRateBrightness ( com.android.server.display.AutomaticBrightnessControllerImpl p0, Float p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F */
	 return;
} // .end method
static void -$$Nest$fputmNotInPocketTime ( com.android.server.display.AutomaticBrightnessControllerImpl p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J */
	 return;
} // .end method
static void -$$Nest$fputmTorchCloseTime ( com.android.server.display.AutomaticBrightnessControllerImpl p0, Long p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-wide p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCloseTime:J */
	 return;
} // .end method
static void -$$Nest$monNonUiSensorChanged ( com.android.server.display.AutomaticBrightnessControllerImpl p0, android.hardware.SensorEvent p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->onNonUiSensorChanged(Landroid/hardware/SensorEvent;)V */
	 return;
} // .end method
static void -$$Nest$monProximitySensorChanged ( com.android.server.display.AutomaticBrightnessControllerImpl p0, android.hardware.SensorEvent p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->onProximitySensorChanged(Landroid/hardware/SensorEvent;)V */
	 return;
} // .end method
static java.lang.String -$$Nest$sfgetCAMERA_ROLE_IDS ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.display.AutomaticBrightnessControllerImpl.CAMERA_ROLE_IDS;
} // .end method
static com.android.server.display.AutomaticBrightnessControllerImpl ( ) {
	 /* .locals 3 */
	 /* .line 37 */
	 /* nop */
	 /* .line 38 */
	 final String v0 = "ro.miui.support.light.fov"; // const-string v0, "ro.miui.support.light.fov"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getInt ( v0,v1 );
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v0, v2, :cond_0 */
	 /* move v1, v2 */
} // :cond_0
com.android.server.display.AutomaticBrightnessControllerImpl.IS_LIGHT_FOV_OPTIMIZATION_POLICY_ENABLE = (v1!= 0);
/* .line 45 */
final String v0 = "com.xiaomi.cameraid.role.cameraIds"; // const-string v0, "com.xiaomi.cameraid.role.cameraIds"
final String v1 = "com.xiaomi.cameraid.role.cameraId"; // const-string v1, "com.xiaomi.cameraid.role.cameraId"
/* filled-new-array {v0, v1}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.display.AutomaticBrightnessControllerImpl ( ) {
/* .locals 2 */
/* .line 30 */
/* invoke-direct {p0}, Lcom/android/server/display/AutomaticBrightnessControllerStub;-><init>()V */
/* .line 94 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartBrightness:F */
/* .line 95 */
/* iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartSdrBrightness:F */
/* .line 107 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J */
/* .line 126 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I */
/* .line 256 */
/* new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V */
this.mTorchCallback = v0;
/* .line 382 */
/* new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V */
this.mSensorListener = v0;
return;
} // .end method
private Float convertToBrightness ( Float p0 ) {
/* .locals 1 */
/* .param p1, "nit" # F */
/* .line 576 */
v0 = this.mBrightnessMapper;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 577 */
	 v0 = 	 (( com.android.server.display.BrightnessMappingStrategy ) v0 ).convertToBrightness ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F
	 /* .line 579 */
} // :cond_0
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
private Float convertToNit ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 569 */
v0 = this.mBrightnessMapper;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 570 */
	 v0 = 	 (( com.android.server.display.BrightnessMappingStrategy ) v0 ).convertToNits ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToNits(F)F
	 /* .line 572 */
} // :cond_0
/* const/high16 v0, 0x7fc00000 # Float.NaN */
} // .end method
private void lambda$stop$0 ( ) { //synthethic
/* .locals 1 */
/* .line 640 */
v0 = this.mTouchAreaHelper;
(( com.android.server.display.TouchCoverProtectionHelper ) v0 ).stop ( ); // invoke-virtual {v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->stop()V
/* .line 641 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setRotationListenerEnable(Z)V */
/* .line 642 */
return;
} // .end method
private void loadConfiguration ( ) {
/* .locals 3 */
/* .line 181 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 182 */
/* .local v0, "resources":Landroid/content/res/Resources; */
/* const v1, 0x11050013 */
v1 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z */
/* .line 183 */
/* const v1, 0x110b0008 */
v1 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateTime:I */
/* .line 185 */
/* const v1, 0x110b0020 */
v1 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mResetFastRateTime:I */
/* .line 187 */
/* const v1, 0x1107001e */
v1 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateRatio:F */
/* .line 188 */
/* const v1, 0x1107001f */
v1 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateValue:F */
/* .line 189 */
/* const v1, 0x11050012 */
v1 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z */
/* .line 190 */
/* const v1, 0x11050010 */
v1 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
/* .line 193 */
/* const v1, 0x11050011 */
v1 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z */
/* .line 195 */
/* const v1, 0x11070034 */
v1 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSkipTransitionLuxValue:F */
/* .line 197 */
/* nop */
/* .line 198 */
/* const-string/jumbo v1, "use_daemon_sensor_policy" */
int v2 = 1; // const/4 v2, 0x1
v1 = miui.util.FeatureParser .getBoolean ( v1,v2 );
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicyEnabled:Z */
/* .line 199 */
/* const v1, 0x11050089 */
v1 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseFastRateForVirtualSensor:Z */
/* .line 201 */
/* const v1, 0x1103003e */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
this.mExitFacingCameraIds = v1;
/* .line 203 */
return;
} // .end method
private void onNonUiSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 411 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 412 */
	 v0 = this.values;
	 int v1 = 0; // const/4 v1, 0x0
	 /* aget v0, v0, v1 */
	 /* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorData:F */
	 /* cmpl-float v0, v0, v2 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 413 */
		 v0 = this.values;
		 /* aget v0, v0, v1 */
		 /* iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorData:F */
		 /* .line 414 */
		 v0 = this.values;
		 /* aget v0, v0, v1 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* cmpl-float v0, v0, v1 */
		 /* if-nez v0, :cond_0 */
		 /* .line 415 */
		 v0 = this.mHandler;
		 int v1 = 1; // const/4 v1, 0x1
		 (( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
		 /* .line 419 */
	 } // :cond_0
	 return;
} // .end method
private void onProximitySensorChanged ( android.hardware.SensorEvent p0 ) {
	 /* .locals 3 */
	 /* .param p1, "event" # Landroid/hardware/SensorEvent; */
	 /* .line 404 */
	 /* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 405 */
		 v0 = this.values;
		 int v1 = 0; // const/4 v1, 0x0
		 /* aget v0, v0, v1 */
		 /* .line 406 */
		 /* .local v0, "distance":F */
		 int v2 = 0; // const/4 v2, 0x0
		 /* cmpl-float v2, v0, v2 */
		 /* if-ltz v2, :cond_0 */
		 /* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityThreshold:F */
		 /* cmpg-float v2, v0, v2 */
		 /* if-gez v2, :cond_0 */
		 int v1 = 1; // const/4 v1, 0x1
	 } // :cond_0
	 /* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityPositive:Z */
	 /* .line 408 */
} // .end local v0 # "distance":F
} // :cond_1
return;
} // .end method
private void setNonUiSensorEnabled ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .line 367 */
final String v0 = "AutomaticBrightnessControllerImpl"; // const-string v0, "AutomaticBrightnessControllerImpl"
int v1 = 1; // const/4 v1, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z */
/* if-nez v2, :cond_0 */
v2 = this.mNonUiSensor;
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 368 */
	 /* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z */
	 /* .line 369 */
	 v1 = this.mSensorManager;
	 v3 = this.mSensorListener;
	 int v4 = 3; // const/4 v4, 0x3
	 (( android.hardware.SensorManager ) v1 ).registerListener ( v3, v2, v4 ); // invoke-virtual {v1, v3, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
	 /* .line 371 */
	 /* const-string/jumbo v1, "setNonUiSensorEnabled enable" */
	 android.util.Slog .i ( v0,v1 );
	 /* .line 372 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 373 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* iput-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z */
	 /* .line 374 */
	 /* const-wide/16 v2, -0x1 */
	 /* iput-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J */
	 /* .line 375 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* iput v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorData:F */
	 /* .line 376 */
	 v2 = this.mHandler;
	 (( android.os.Handler ) v2 ).removeMessages ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 377 */
	 v1 = this.mSensorManager;
	 v2 = this.mSensorListener;
	 v3 = this.mNonUiSensor;
	 (( android.hardware.SensorManager ) v1 ).unregisterListener ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
	 /* .line 378 */
	 /* const-string/jumbo v1, "setNonUiSensorEnabled disable" */
	 android.util.Slog .i ( v0,v1 );
	 /* .line 380 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void setProximitySensorEnabled ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 350 */
final String v0 = "AutomaticBrightnessControllerImpl"; // const-string v0, "AutomaticBrightnessControllerImpl"
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z */
/* if-nez v1, :cond_0 */
/* .line 353 */
/* const-string/jumbo v1, "setProximitySensorEnabled enable" */
android.util.Slog .i ( v0,v1 );
/* .line 354 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z */
/* .line 355 */
v0 = this.mSensorManager;
v1 = this.mSensorListener;
v2 = this.mProximitySensor;
int v3 = 3; // const/4 v3, 0x3
(( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 357 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 360 */
/* const-string/jumbo v1, "setProximitySensorEnabled disable" */
android.util.Slog .i ( v0,v1 );
/* .line 361 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z */
/* .line 362 */
v0 = this.mSensorManager;
v1 = this.mSensorListener;
v2 = this.mProximitySensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 364 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void setRotationListenerEnable ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 778 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 779 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).registerRotationWatcher ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerRotationWatcher(Z)V
/* .line 781 */
} // :cond_0
return;
} // .end method
private void setSensorEnabled ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 338 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 339 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setProximitySensorEnabled(Z)V */
/* .line 341 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 342 */
v0 = this.mDualSensorPolicy;
(( com.android.server.display.DualSensorPolicy ) v0 ).setSensorEnabled ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DualSensorPolicy;->setSensorEnabled(Z)V
/* .line 344 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 345 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setNonUiSensorEnabled(Z)V */
/* .line 347 */
} // :cond_2
return;
} // .end method
private void setUpDisplayDeviceConfig ( com.android.server.display.DisplayDeviceConfig p0 ) {
/* .locals 0 */
/* .param p1, "deviceConfig" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .line 215 */
this.mDisplayDeviceConfig = p1;
/* .line 216 */
return;
} // .end method
private void setUpLogicalDisplay ( com.android.server.display.LogicalDisplay p0 ) {
/* .locals 1 */
/* .param p1, "logicalDisplay" # Lcom/android/server/display/LogicalDisplay; */
/* .line 219 */
v0 = this.mTouchAreaHelper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 220 */
(( com.android.server.display.TouchCoverProtectionHelper ) v0 ).setUpLogicalDisplay ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setUpLogicalDisplay(Lcom/android/server/display/LogicalDisplay;)V
/* .line 222 */
} // :cond_0
return;
} // .end method
private void updateCbmState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "autoBrightnessEnabled" # Z */
/* .line 851 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 852 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).updateCbmState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCbmState(Z)V
/* .line 854 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
protected Boolean checkAssistSensorValid ( ) {
/* .locals 4 */
/* .line 455 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z */
/* if-nez v0, :cond_0 */
/* .line 457 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCloseTime:J */
/* sub-long/2addr v0, v2 */
/* const-wide/16 v2, 0x708 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 458 */
int v0 = 1; // const/4 v0, 0x1
/* .line 460 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 461 */
final String v0 = "AutomaticBrightnessControllerImpl"; // const-string v0, "AutomaticBrightnessControllerImpl"
final String v1 = "drop assist light data due to within 1s of turning off the torch."; // const-string v1, "drop assist light data due to within 1s of turning off the torch."
android.util.Slog .d ( v0,v1 );
/* .line 463 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean checkFastRateStatus ( ) {
/* .locals 6 */
/* .line 509 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 510 */
/* .local v0, "currentTime":J */
/* iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightSensorEnableTime:J */
/* iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateTime:I */
/* int-to-long v4, v4 */
/* add-long/2addr v2, v4 */
/* cmp-long v2, v0, v2 */
/* if-lez v2, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J */
/* const-wide/16 v4, 0x7d0 */
/* add-long/2addr v2, v4 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :cond_1
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
} // :goto_1
} // .end method
public void configure ( Integer p0, Float p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "state" # I */
/* .param p2, "screenAutoBrightness" # F */
/* .param p3, "displayPolicy" # I */
/* .line 226 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* if-eq p3, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* move v2, v0 */
/* .line 228 */
/* .local v2, "enable":Z */
} // :goto_0
/* invoke-direct {p0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setSensorEnabled(Z)V */
/* .line 230 */
/* iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicyEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 231 */
v3 = this.mDaemonSensorPolicy;
(( com.android.server.display.DaemonSensorPolicy ) v3 ).notifyRegisterDaemonLightSensor ( p1, p3 ); // invoke-virtual {v3, p1, p3}, Lcom/android/server/display/DaemonSensorPolicy;->notifyRegisterDaemonLightSensor(II)V
/* .line 233 */
} // :cond_1
/* iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 234 */
v3 = this.mSceneDetector;
(( com.android.server.display.SceneDetector ) v3 ).configure ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/display/SceneDetector;->configure(Z)V
/* .line 236 */
} // :cond_2
v3 = this.mTouchAreaHelper;
(( com.android.server.display.TouchCoverProtectionHelper ) v3 ).configure ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/display/TouchCoverProtectionHelper;->configure(Z)V
/* .line 237 */
/* invoke-direct {p0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setRotationListenerEnable(Z)V */
/* .line 239 */
int v3 = 2; // const/4 v3, 0x2
/* if-nez v2, :cond_3 */
/* iget-boolean v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 240 */
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z */
/* .line 241 */
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
/* .line 242 */
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z */
/* .line 243 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).removeMessages ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 244 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateCbmState(Z)V */
/* .line 245 */
} // :cond_3
if ( v2 != null) { // if-eqz v2, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z */
/* if-nez v0, :cond_5 */
/* .line 246 */
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z */
/* .line 247 */
/* iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mState:I */
/* .line 249 */
/* .local v0, "oldState":I */
/* if-eq v0, v3, :cond_4 */
/* iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseFastRateForVirtualSensor:Z */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 250 */
} // :cond_4
/* iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z */
/* .line 253 */
} // .end local v0 # "oldState":I
} // :cond_5
} // :goto_1
/* iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mState:I */
/* .line 254 */
return;
} // .end method
public Boolean dropAmbientLuxIfNeeded ( ) {
/* .locals 3 */
/* .line 423 */
v0 = this.mTouchAreaHelper;
v0 = (( com.android.server.display.TouchCoverProtectionHelper ) v0 ).isTouchCoverProtectionActive ( ); // invoke-virtual {v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->isTouchCoverProtectionActive()Z
int v1 = 1; // const/4 v1, 0x1
final String v2 = "AutomaticBrightnessControllerImpl"; // const-string v2, "AutomaticBrightnessControllerImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 424 */
final String v0 = "drop the ambient lux due to touch events."; // const-string v0, "drop the ambient lux due to touch events."
android.util.Slog .d ( v2,v0 );
/* .line 425 */
/* .line 426 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityPositive:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 427 */
final String v0 = "drop the ambient lux due to proximity events."; // const-string v0, "drop the ambient lux due to proximity events."
android.util.Slog .d ( v2,v0 );
/* .line 428 */
/* .line 430 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean dropDecreaseLuxIfNeeded ( ) {
/* .locals 6 */
/* .line 439 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 440 */
/* .local v0, "now":J */
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z */
/* if-nez v2, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsGameSceneEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mEnterGameTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/32 v4, 0xea60 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_0 */
v2 = this.mTouchAreaHelper;
/* .line 442 */
v2 = (( com.android.server.display.TouchCoverProtectionHelper ) v2 ).isGameSceneWithinTouchTime ( ); // invoke-virtual {v2}, Lcom/android/server/display/TouchCoverProtectionHelper;->isGameSceneWithinTouchTime()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 443 */
} // :cond_0
final String v2 = "AutomaticBrightnessControllerImpl"; // const-string v2, "AutomaticBrightnessControllerImpl"
final String v3 = "drop the ambient lux due to game scene enable."; // const-string v3, "drop the ambient lux due to game scene enable."
android.util.Slog .d ( v2,v3 );
/* .line 444 */
int v2 = 1; // const/4 v2, 0x1
/* .line 446 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 584 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mUseProximityEnabled="; // const-string v1, " mUseProximityEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 585 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLightFovSensor="; // const-string v1, " mLightFovSensor="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mLightFovSensor;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 586 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mUseNonUiEnabled="; // const-string v1, " mUseNonUiEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 587 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mUseAonFlareEnabled="; // const-string v1, " mUseAonFlareEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 588 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mIsGameSceneEnable="; // const-string v1, " mIsGameSceneEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsGameSceneEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 589 */
v0 = this.mTouchAreaHelper;
(( com.android.server.display.TouchCoverProtectionHelper ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->dump(Ljava/io/PrintWriter;)V
/* .line 590 */
v0 = this.mDaemonSensorPolicy;
(( com.android.server.display.DaemonSensorPolicy ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DaemonSensorPolicy;->dump(Ljava/io/PrintWriter;)V
/* .line 591 */
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) p0 ).supportDualSensorPolicy ( ); // invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->supportDualSensorPolicy()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 592 */
v0 = this.mDualSensorPolicy;
(( com.android.server.display.DualSensorPolicy ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DualSensorPolicy;->dump(Ljava/io/PrintWriter;)V
/* .line 594 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 595 */
v0 = this.mSceneDetector;
(( com.android.server.display.SceneDetector ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/SceneDetector;->dump(Ljava/io/PrintWriter;)V
/* .line 597 */
} // :cond_1
v0 = this.mHysteresisLevelsImpl;
(( com.android.server.display.HysteresisLevelsImpl ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/HysteresisLevelsImpl;->dump(Ljava/io/PrintWriter;)V
/* .line 598 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z */
/* .line 599 */
return;
} // .end method
public android.util.SparseArray fillInLuxFromDaemonSensor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 606 */
/* new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V */
/* .line 613 */
/* .local v0, "daemonSensorArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Float;>;" */
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicyEnabled:Z */
/* if-nez v1, :cond_0 */
/* .line 614 */
/* .line 617 */
} // :cond_0
v1 = this.mDaemonSensorPolicy;
v1 = (( com.android.server.display.DaemonSensorPolicy ) v1 ).getMainLightSensorLux ( ); // invoke-virtual {v1}, Lcom/android/server/display/DaemonSensorPolicy;->getMainLightSensorLux()F
/* .line 618 */
/* .local v1, "mainLux":F */
v2 = this.mDaemonSensorPolicy;
/* const v3, 0x1fa266f */
v2 = (( com.android.server.display.DaemonSensorPolicy ) v2 ).getDaemonSensorValue ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/DaemonSensorPolicy;->getDaemonSensorValue(I)F
/* .line 620 */
/* .local v2, "assistLux":F */
/* iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) p0 ).checkAssistSensorValid ( ); // invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkAssistSensorValid()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* cmpl-float v3, v2, v1 */
/* if-lez v3, :cond_1 */
/* .line 621 */
java.lang.Float .valueOf ( v2 );
(( android.util.SparseArray ) v0 ).put ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 623 */
} // :cond_1
java.lang.Float .valueOf ( v1 );
(( android.util.SparseArray ) v0 ).put ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 626 */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 627 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fillInLuxFromDaemonSensor: mainLux: "; // const-string v4, "fillInLuxFromDaemonSensor: mainLux: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", assistLux: "; // const-string v4, ", assistLux: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "AutomaticBrightnessControllerImpl"; // const-string v4, "AutomaticBrightnessControllerImpl"
android.util.Slog .d ( v4,v3 );
/* .line 629 */
} // :cond_2
} // .end method
public Float getAmbientLux ( Integer p0, Float p1, Float p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "event" # I */
/* .param p2, "preLux" # F */
/* .param p3, "updateLux" # F */
/* .param p4, "needUpdateLux" # Z */
/* .line 667 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 668 */
/* iput p3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLux:F */
/* .line 670 */
} // :cond_0
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).getAmbientLux ( p2, p3, p4 ); // invoke-virtual {v0, p2, p3, p4}, Lcom/android/server/display/DualSensorPolicy;->getAmbientLux(FFZ)F
} // .end method
protected Float getAssistFastAmbientLux ( ) {
/* .locals 1 */
/* .line 774 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).getAssistFastAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getAssistFastAmbientLux()F
} // .end method
public Float getCurrentAmbientLux ( ) {
/* .locals 1 */
/* .line 690 */
v0 = this.mAutomaticBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 691 */
v0 = (( com.android.server.display.AutomaticBrightnessController ) v0 ).getAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessController;->getAmbientLux()F
/* .line 693 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLux:F */
} // .end method
public Float getCustomBrightness ( Float p0, java.lang.String p1, Integer p2, Float p3, Float p4, Boolean p5 ) {
/* .locals 7 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "category" # I */
/* .param p4, "oldAutoBrightness" # F */
/* .param p5, "newAutoBrightness" # F */
/* .param p6, "isManuallySet" # Z */
/* .line 791 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 792 */
/* move v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* move v6, p6 */
p5 = /* invoke-virtual/range {v0 ..v6}, Lcom/android/server/display/DisplayPowerControllerImpl;->getCustomBrightness(FLjava/lang/String;IFFZ)F */
/* .line 795 */
} // :cond_0
} // .end method
public com.android.server.display.HysteresisLevelsStub getHysteresisLevelsImpl ( ) {
/* .locals 1 */
/* .line 834 */
v0 = this.mHysteresisLevelsImpl;
} // .end method
protected Boolean getIsTorchOpen ( ) {
/* .locals 1 */
/* .line 646 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z */
} // .end method
public Float getMainAmbientLux ( ) {
/* .locals 1 */
/* .line 697 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).getMainAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getMainAmbientLux()F
} // .end method
protected Float getMainFastAmbientLux ( ) {
/* .locals 1 */
/* .line 770 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).getMainFastAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getMainFastAmbientLux()F
} // .end method
public void initialize ( android.hardware.SensorManager p0, android.content.Context p1, android.os.Looper p2, android.hardware.Sensor p3, Integer p4, Integer p5, Long p6, Long p7, Integer p8, Integer p9, com.android.server.display.HysteresisLevelsStub p10, com.android.server.display.AutomaticBrightnessControllerStub$DualSensorPolicyListener p11, com.android.server.display.AutomaticBrightnessController p12 ) {
/* .locals 16 */
/* .param p1, "sensorManager" # Landroid/hardware/SensorManager; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "looper" # Landroid/os/Looper; */
/* .param p4, "lightSensor" # Landroid/hardware/Sensor; */
/* .param p5, "lightSensorWarmUpTime" # I */
/* .param p6, "lightSensorRate" # I */
/* .param p7, "brighteningLightDebounceConfig" # J */
/* .param p9, "darkeningLightDebounceConfig" # J */
/* .param p11, "ambientLightHorizonLong" # I */
/* .param p12, "ambientLightHorizonShort" # I */
/* .param p13, "hysteresisLevelsImpl" # Lcom/android/server/display/HysteresisLevelsStub; */
/* .param p14, "listener" # Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener; */
/* .param p15, "controller" # Lcom/android/server/display/AutomaticBrightnessController; */
/* .line 142 */
/* move-object/from16 v14, p0 */
/* move-object/from16 v15, p2 */
/* move-object/from16 v13, p3 */
this.mContext = v15;
/* .line 143 */
/* move-object/from16 v12, p1 */
this.mSensorManager = v12;
/* .line 144 */
/* move-object/from16 v11, p15 */
this.mAutomaticBrightnessController = v11;
/* .line 145 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->loadConfiguration()V */
/* .line 147 */
/* new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler; */
/* invoke-direct {v0, v14, v13}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 148 */
/* new-instance v6, Lcom/android/server/display/DaemonSensorPolicy; */
v1 = this.mContext;
/* move-object v0, v6 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p3 */
/* move-object/from16 v4, p0 */
/* move-object/from16 v5, p4 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/DaemonSensorPolicy;-><init>(Landroid/content/Context;Landroid/hardware/SensorManager;Landroid/os/Looper;Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/Sensor;)V */
this.mDaemonSensorPolicy = v6;
/* .line 149 */
/* new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper; */
v1 = this.mContext;
/* invoke-direct {v0, v1, v13}, Lcom/android/server/display/TouchCoverProtectionHelper;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
this.mTouchAreaHelper = v0;
/* .line 150 */
/* new-instance v10, Lcom/android/server/display/DualSensorPolicy; */
/* move-object v0, v10 */
/* move-object/from16 v1, p3 */
/* move/from16 v3, p5 */
/* move/from16 v4, p6 */
/* move-wide/from16 v5, p7 */
/* move-wide/from16 v7, p9 */
/* move/from16 v9, p11 */
/* move-object v15, v10 */
/* move/from16 v10, p12 */
/* move-object/from16 v11, p13 */
/* move-object/from16 v12, p14 */
/* move-object/from16 v13, p0 */
/* invoke-direct/range {v0 ..v13}, Lcom/android/server/display/DualSensorPolicy;-><init>(Landroid/os/Looper;Landroid/hardware/SensorManager;IIJJIILcom/android/server/display/HysteresisLevelsStub;Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V */
this.mDualSensorPolicy = v15;
/* .line 154 */
/* iget-boolean v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 155 */
/* new-instance v0, Lcom/android/server/display/SceneDetector; */
v1 = this.mHandler;
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p14 */
/* invoke-direct {v0, v3, v14, v1, v2}, Lcom/android/server/display/SceneDetector;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/os/Handler;Landroid/content/Context;)V */
this.mSceneDetector = v0;
/* .line 156 */
v1 = this.mDualSensorPolicy;
(( com.android.server.display.DualSensorPolicy ) v1 ).setSceneDetector ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/DualSensorPolicy;->setSceneDetector(Lcom/android/server/display/SceneDetector;)V
/* .line 154 */
} // :cond_0
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p14 */
/* .line 158 */
} // :goto_0
v0 = this.mSensorManager;
/* const v1, 0x1fa2a8f */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mLightFovSensor = v0;
/* .line 159 */
/* move-object/from16 v0, p13 */
/* check-cast v0, Lcom/android/server/display/HysteresisLevelsImpl; */
this.mHysteresisLevelsImpl = v0;
/* .line 161 */
/* iget-boolean v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 162 */
v0 = this.mSensorManager;
/* const/16 v1, 0x8 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mProximitySensor = v0;
/* .line 163 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 164 */
v0 = (( android.hardware.Sensor ) v0 ).getMaximumRange ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F
/* const/high16 v1, 0x40a00000 # 5.0f */
v0 = java.lang.Math .min ( v0,v1 );
/* iput v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityThreshold:F */
/* .line 169 */
} // :cond_1
/* iget-boolean v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 170 */
v0 = this.mSensorManager;
/* const v1, 0x1fa2653 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mNonUiSensor = v0;
/* .line 171 */
/* if-nez v0, :cond_2 */
/* .line 172 */
v0 = this.mSensorManager;
int v4 = 1; // const/4 v4, 0x1
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mNonUiSensor = v0;
/* .line 176 */
} // :cond_2
v0 = this.mContext;
final String v1 = "camera"; // const-string v1, "camera"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/camera2/CameraManager; */
this.mCameraManager = v0;
/* .line 177 */
v1 = this.mTorchCallback;
v4 = this.mHandler;
(( android.hardware.camera2.CameraManager ) v0 ).registerTorchCallback ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/hardware/camera2/CameraManager;->registerTorchCallback(Landroid/hardware/camera2/CameraManager$TorchCallback;Landroid/os/Handler;)V
/* .line 178 */
return;
} // .end method
public Boolean isAnimating ( ) {
/* .locals 3 */
/* .line 823 */
v0 = this.mDisplayPowerControllerImpl;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 824 */
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsAnimatePolicyDisable:Z */
/* if-nez v2, :cond_0 */
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).isAnimating ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
/* if-nez v0, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 826 */
} // :cond_1
} // .end method
protected Boolean isAonFlareEnabled ( ) {
/* .locals 1 */
/* .line 650 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
} // .end method
public Boolean isBrighteningDirection ( ) {
/* .locals 2 */
/* .line 819 */
/* iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Boolean isDisableResetShortTermModel ( ) {
/* .locals 2 */
/* .line 763 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 764 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).resetShortTermModel ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetShortTermModel(Z)V
/* .line 766 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisableResetShortTermModel:Z */
} // .end method
public void notifyAonFlareEvents ( Integer p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "preLux" # F */
/* .line 857 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 858 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).notifyAonFlareEvents ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyAonFlareEvents(IF)V
/* .line 860 */
} // :cond_0
return;
} // .end method
protected void notifyDisableResetShortTermModel ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 755 */
/* iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisableResetShortTermModel:Z */
/* .line 756 */
return;
} // .end method
public void notifyUnregisterDaemonSensor ( ) {
/* .locals 2 */
/* .line 727 */
v0 = this.mDaemonSensorPolicy;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.DaemonSensorPolicy ) v0 ).setDaemonLightSensorsEnabled ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy;->setDaemonLightSensorsEnabled(Z)V
/* .line 728 */
return;
} // .end method
public void notifyUpdateBrightness ( ) {
/* .locals 1 */
/* .line 863 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 864 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).notifyUpdateBrightness ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyUpdateBrightness()V
/* .line 866 */
} // :cond_0
return;
} // .end method
protected void notifyUpdateForegroundApp ( ) {
/* .locals 1 */
/* .line 838 */
v0 = this.mAutomaticBrightnessController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 839 */
(( com.android.server.display.AutomaticBrightnessController ) v0 ).updateForegroundAppWindowChanged ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessController;->updateForegroundAppWindowChanged()V
/* .line 841 */
} // :cond_0
return;
} // .end method
public void setAmbientLuxWhenInvalid ( Integer p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "event" # I */
/* .param p2, "lux" # F */
/* .line 732 */
v0 = this.mDualSensorPolicy;
(( com.android.server.display.DualSensorPolicy ) v0 ).setAmbientLuxWhenInvalid ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DualSensorPolicy;->setAmbientLuxWhenInvalid(IF)V
/* .line 733 */
return;
} // .end method
public void setAnimationPolicyDisable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isDisable" # Z */
/* .line 830 */
/* iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsAnimatePolicyDisable:Z */
/* .line 831 */
return;
} // .end method
public void setScreenBrightnessByUser ( Float p0, Float p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "lux" # F */
/* .param p2, "brightness" # F */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 800 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 801 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).setScreenBrightnessByUser ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->setScreenBrightnessByUser(FFLjava/lang/String;)V
/* .line 803 */
} // :cond_0
return;
} // .end method
protected void setUpAutoBrightness ( com.android.server.display.DisplayPowerControllerImpl p0, com.android.server.display.BrightnessMappingStrategy p1, com.android.server.display.DisplayDeviceConfig p2, com.android.server.display.LogicalDisplay p3 ) {
/* .locals 0 */
/* .param p1, "dpcImpl" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .param p2, "brightnessMapper" # Lcom/android/server/display/BrightnessMappingStrategy; */
/* .param p3, "deviceConfig" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .param p4, "logicalDisplay" # Lcom/android/server/display/LogicalDisplay; */
/* .line 208 */
this.mDisplayPowerControllerImpl = p1;
/* .line 209 */
this.mBrightnessMapper = p2;
/* .line 210 */
/* invoke-direct {p0, p3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setUpDisplayDeviceConfig(Lcom/android/server/display/DisplayDeviceConfig;)V */
/* .line 211 */
/* invoke-direct {p0, p4}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setUpLogicalDisplay(Lcom/android/server/display/LogicalDisplay;)V */
/* .line 212 */
return;
} // .end method
public Boolean shouldSkipBrighteningTransition ( Long p0, Float p1, Float p2, Float p3 ) {
/* .locals 3 */
/* .param p1, "sensorEnableTime" # J */
/* .param p3, "currentLux" # F */
/* .param p4, "ambientLux" # F */
/* .param p5, "brighteningThreshold" # F */
/* .line 487 */
/* iput-wide p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightSensorEnableTime:J */
/* .line 488 */
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) p0 ).checkFastRateStatus ( ); // invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkFastRateStatus()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* cmpl-float v0, p3, p5 */
/* if-ltz v0, :cond_3 */
/* iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSkipTransitionLuxValue:F */
/* add-float/2addr v0, p4 */
/* cmpl-float v0, p3, v0 */
/* if-ltz v0, :cond_3 */
/* iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateRatio:F */
/* mul-float/2addr v0, p4 */
/* add-float/2addr v0, p4 */
/* cmpl-float v0, p3, v0 */
/* if-ltz v0, :cond_3 */
/* .line 491 */
v0 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) p0 ).supportDualSensorPolicy ( ); // invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->supportDualSensorPolicy()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 492 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).updateBrightnessUsingMainLightSensor ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->updateBrightnessUsingMainLightSensor()Z
/* if-nez v0, :cond_0 */
v0 = this.mDualSensorPolicy;
/* .line 493 */
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).getAssistFastAmbientLux ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getAssistFastAmbientLux()F
/* cmpg-float v0, p3, v0 */
/* if-gez v0, :cond_0 */
/* .line 494 */
/* .line 496 */
} // :cond_0
v0 = this.mDualSensorPolicy;
(( com.android.server.display.DualSensorPolicy ) v0 ).updateMainLuxStatus ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/display/DualSensorPolicy;->updateMainLuxStatus(F)V
/* .line 498 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Skip brightening transition, currentLux:"; // const-string v2, "Skip brightening transition, currentLux:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", ambientLux:"; // const-string v2, ", ambientLux:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AutomaticBrightnessControllerImpl"; // const-string v2, "AutomaticBrightnessControllerImpl"
android.util.Slog .i ( v2,v0 );
/* .line 499 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 500 */
v0 = this.mSceneDetector;
(( com.android.server.display.SceneDetector ) v0 ).updateAmbientLux ( v2, p3, v1 ); // invoke-virtual {v0, v2, p3, v1}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V
/* .line 501 */
/* .line 503 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 505 */
} // :cond_3
} // .end method
protected Boolean shouldUseFastRate ( Float p0, Float p1 ) {
/* .locals 7 */
/* .param p1, "currBrightness" # F */
/* .param p2, "tgtBrightness" # F */
/* .line 516 */
/* iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->convertToNit(F)F */
/* .line 517 */
/* .local v0, "nit":F */
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->convertToNit(F)F */
/* .line 523 */
/* .local v1, "currentNit":F */
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
final String v3 = "AutomaticBrightnessControllerImpl"; // const-string v3, "AutomaticBrightnessControllerImpl"
/* if-nez v2, :cond_0 */
v2 = (( com.android.server.display.AutomaticBrightnessControllerImpl ) p0 ).checkFastRateStatus ( ); // invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkFastRateStatus()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateRatio:F */
/* mul-float/2addr v2, v1 */
/* add-float/2addr v2, v1 */
/* cmpl-float v2, v0, v2 */
/* if-ltz v2, :cond_0 */
/* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateValue:F */
/* add-float/2addr v2, v1 */
/* cmpl-float v2, v0, v2 */
/* if-ltz v2, :cond_0 */
/* .line 525 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
/* .line 526 */
/* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F */
/* iput v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F */
/* .line 527 */
v2 = this.mHandler;
int v4 = 2; // const/4 v4, 0x2
(( android.os.Handler ) v2 ).removeMessages ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 528 */
v2 = this.mHandler;
/* iget v5, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mResetFastRateTime:I */
/* int-to-long v5, v5 */
(( android.os.Handler ) v2 ).sendEmptyMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 529 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Use fast rate due to large change in brightness, mLastBrightness:"; // const-string v4, "Use fast rate due to large change in brightness, mLastBrightness:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", currBrightness:"; // const-string v4, ", currBrightness:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 540 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F */
/* iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F */
/* cmpg-float v5, v2, v4 */
/* if-ltz v5, :cond_1 */
/* cmpl-float v2, v2, v4 */
/* if-lez v2, :cond_2 */
/* cmpl-float v2, p1, v4 */
/* if-ltz v2, :cond_2 */
/* .line 543 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "shouldUseFastRate: mLastBrightness: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", tgtBrightness: "; // const-string v4, ", tgtBrightness: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", currBrightness: "; // const-string v4, ", currBrightness: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", mNeedUseFastRateBrightness: "; // const-string v4, ", mNeedUseFastRateBrightness: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 546 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
/* .line 547 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F */
/* .line 549 */
} // :cond_2
/* iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
} // .end method
public void showTouchCoverProtectionRect ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isShow" # Z */
/* .line 655 */
v0 = this.mTouchAreaHelper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 656 */
(( com.android.server.display.TouchCoverProtectionHelper ) v0 ).showTouchCoverProtectionRect ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->showTouchCoverProtectionRect(Z)V
/* .line 658 */
} // :cond_0
return;
} // .end method
public void stop ( ) {
/* .locals 2 */
/* .line 634 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setSensorEnabled(Z)V */
/* .line 635 */
v0 = this.mDaemonSensorPolicy;
(( com.android.server.display.DaemonSensorPolicy ) v0 ).stop ( ); // invoke-virtual {v0}, Lcom/android/server/display/DaemonSensorPolicy;->stop()V
/* .line 636 */
v0 = this.mCameraManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 637 */
v1 = this.mTorchCallback;
(( android.hardware.camera2.CameraManager ) v0 ).unregisterTorchCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->unregisterTorchCallback(Landroid/hardware/camera2/CameraManager$TorchCallback;)V
/* .line 639 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/display/AutomaticBrightnessControllerImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 643 */
return;
} // .end method
public Boolean supportDualSensorPolicy ( ) {
/* .locals 1 */
/* .line 662 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mDualSensorPolicy;
(( com.android.server.display.DualSensorPolicy ) v0 ).getAssistLightSensor ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getAssistLightSensor()Landroid/hardware/Sensor;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public android.hardware.Sensor switchLightSensor ( android.hardware.Sensor p0 ) {
/* .locals 1 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .line 748 */
/* sget-boolean v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->IS_LIGHT_FOV_OPTIMIZATION_POLICY_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mLightFovSensor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 749 */
/* .line 751 */
} // :cond_0
} // .end method
public void update ( ) {
/* .locals 1 */
/* .line 784 */
v0 = this.mAutomaticBrightnessController;
(( com.android.server.display.AutomaticBrightnessController ) v0 ).update ( ); // invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessController;->update()V
/* .line 785 */
return;
} // .end method
public void updateAmbientLuxDirection ( Boolean p0, Float p1, Float p2 ) {
/* .locals 1 */
/* .param p1, "needUpdateBrightness" # Z */
/* .param p2, "currentAmbientLux" # F */
/* .param p3, "preAmbientLux" # F */
/* .line 807 */
/* if-nez p1, :cond_0 */
/* .line 808 */
return;
/* .line 810 */
} // :cond_0
/* cmpl-float v0, p2, p3 */
/* if-lez v0, :cond_1 */
/* .line 811 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I */
/* .line 813 */
} // :cond_1
/* cmpg-float v0, p2, p3 */
/* if-gez v0, :cond_2 */
/* .line 814 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I */
/* .line 816 */
} // :cond_2
return;
} // .end method
public Boolean updateBrightnessUsingMainLightSensor ( ) {
/* .locals 1 */
/* .line 680 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).updateBrightnessUsingMainLightSensor ( ); // invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->updateBrightnessUsingMainLightSensor()Z
} // .end method
public void updateCustomSceneState ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 845 */
v0 = this.mDisplayPowerControllerImpl;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 846 */
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).updateCustomSceneState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCustomSceneState(Ljava/lang/String;)V
/* .line 848 */
} // :cond_0
return;
} // .end method
public Boolean updateDualSensorPolicy ( Long p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "time" # J */
/* .param p3, "event" # I */
/* .line 685 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).updateDualSensorPolicy ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DualSensorPolicy;->updateDualSensorPolicy(JI)Z
} // .end method
public void updateFastRateStatus ( Float p0 ) {
/* .locals 4 */
/* .param p1, "brightness" # F */
/* .line 472 */
/* iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F */
/* .line 473 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 474 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z */
/* .line 475 */
/* iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F */
/* .line 476 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z */
/* .line 478 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 479 */
v0 = this.mHandler;
/* iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mResetFastRateTime:I */
/* int-to-long v2, v2 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 480 */
final String v0 = "AutomaticBrightnessControllerImpl"; // const-string v0, "AutomaticBrightnessControllerImpl"
final String v1 = "Use fast rate due to first auto brightness."; // const-string v1, "Use fast rate due to first auto brightness."
android.util.Slog .i ( v0,v1 );
/* .line 482 */
} // :cond_0
return;
} // .end method
protected void updateGameSceneEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 450 */
/* iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsGameSceneEnable:Z */
/* .line 451 */
if ( p1 != null) { // if-eqz p1, :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mEnterGameTime:J */
} // :goto_0
/* iput-wide v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mEnterGameTime:J */
/* .line 452 */
return;
} // .end method
public Boolean updateMainLightSensorAmbientThreshold ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "event" # I */
/* .line 675 */
v0 = this.mDualSensorPolicy;
v0 = (( com.android.server.display.DualSensorPolicy ) v0 ).updateMainLightSensorAmbientThreshold ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DualSensorPolicy;->updateMainLightSensorAmbientThreshold(I)Z
} // .end method
public void updateSlowChangeStatus ( Boolean p0, Boolean p1, Boolean p2, Float p3, Float p4 ) {
/* .locals 2 */
/* .param p1, "slowChange" # Z */
/* .param p2, "appliedDimming" # Z */
/* .param p3, "appliedLowPower" # Z */
/* .param p4, "startBrightness" # F */
/* .param p5, "startSdrBrightness" # F */
/* .line 555 */
/* xor-int/lit8 v0, p2, 0x1 */
/* and-int/2addr v0, p1 */
/* iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSlowChange:Z */
/* .line 556 */
/* iput-boolean p2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAppliedDimming:Z */
/* .line 557 */
/* iput p4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartBrightness:F */
/* .line 558 */
/* iput p5, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartSdrBrightness:F */
/* .line 559 */
/* iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 560 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateSlowChangeStatus: mSlowChange: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSlowChange:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", appliedDimming: "; // const-string v1, ", appliedDimming: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAppliedDimming:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", appliedLowPower: "; // const-string v1, ", appliedLowPower: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", startBrightness: "; // const-string v1, ", startBrightness: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", mStartSdrBrightness: "; // const-string v1, ", mStartSdrBrightness: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartSdrBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AutomaticBrightnessControllerImpl"; // const-string v1, "AutomaticBrightnessControllerImpl"
android.util.Slog .d ( v1,v0 );
/* .line 566 */
} // :cond_0
return;
} // .end method
