class com.android.server.display.DisplayPowerControllerImpl$2 implements com.android.server.display.HighBrightnessModeController$HdrStateListener {
	 /* .source "DisplayPowerControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/DisplayPowerControllerImpl;->init(Lcom/android/server/display/DisplayPowerController;Landroid/content/Context;Landroid/os/Looper;IFFLcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/LogicalDisplay;Lcom/android/server/display/HighBrightnessModeController;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayPowerControllerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayPowerControllerImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .line 506 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onHbmModeChanged ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "newMode" # I */
/* .param p2, "oldMode" # I */
/* .line 520 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmRampRateController ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 521 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* if-ne p1, v0, :cond_0 */
	 /* .line 522 */
	 v0 = this.this$0;
	 com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmRampRateController ( v0 );
	 int v1 = 1; // const/4 v1, 0x1
	 (( com.android.server.display.RampRateController ) v0 ).addHdrRateModifier ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/RampRateController;->addHdrRateModifier(Z)V
	 /* .line 523 */
} // :cond_0
/* if-ne p2, v0, :cond_1 */
/* .line 524 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmRampRateController ( v0 );
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.RampRateController ) v0 ).addHdrRateModifier ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/RampRateController;->addHdrRateModifier(Z)V
/* .line 527 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onHdrStateChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isHdrLayerPresent" # Z */
/* .line 509 */
v0 = this.this$0;
v0 = com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmAutoBrightnessEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 510 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$mresetBCBCState ( v0 );
/* .line 511 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$mresetScreenGrayscaleState ( v0 );
/* .line 513 */
} // :cond_0
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmThermalBrightnessController ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 514 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmThermalBrightnessController ( v0 );
(( com.android.server.display.ThermalBrightnessController ) v0 ).setHdrLayerPresent ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/ThermalBrightnessController;->setHdrLayerPresent(Z)V
/* .line 516 */
} // :cond_1
return;
} // .end method
