.class public Lcom/android/server/display/DaemonSensorPolicy;
.super Ljava/lang/Object;
.source "DaemonSensorPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;
    }
.end annotation


# static fields
.field private static final ASSIST_SENSOR_TYPE:I = 0x1fa266f

.field private static final MSG_UPDATE_DEVICE_IDLE:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final USE_DAEMON_SENSOR_POLICY:Z


# instance fields
.field private mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

.field private final mContext:Landroid/content/Context;

.field private mDaemonLightSensorsEnabled:Z

.field private final mDaemonSensorListener:Landroid/hardware/SensorEventListener;

.field private mDaemonSensorValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mDaemonSensors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/hardware/Sensor;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceIdleReceiver:Landroid/content/BroadcastReceiver;

.field private mDisplayPolicy:I

.field private mHandler:Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

.field private mIsDeviceIdleMode:Z

.field private mIsDeviceIdleReceiverRegistered:Z

.field private final mMainLightSensor:Landroid/hardware/Sensor;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private final mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public static synthetic $r8$lambda$p0jOgz95rgjsmAJHsCwaO_jP79o(Lcom/android/server/display/DaemonSensorPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->lambda$new$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmDaemonSensorValues(Lcom/android/server/display/DaemonSensorPolicy;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorValues:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/DaemonSensorPolicy;)Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mHandler:Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsDeviceIdleMode(Lcom/android/server/display/DaemonSensorPolicy;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerManager(Lcom/android/server/display/DaemonSensorPolicy;)Landroid/os/PowerManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mPowerManager:Landroid/os/PowerManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsDeviceIdleMode(Lcom/android/server/display/DaemonSensorPolicy;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/display/DaemonSensorPolicy;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 27
    const-class v0, Lcom/android/server/display/DaemonSensorPolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/DaemonSensorPolicy;->TAG:Ljava/lang/String;

    .line 35
    nop

    .line 36
    const-string/jumbo v0, "use_daemon_sensor_policy"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DaemonSensorPolicy;->USE_DAEMON_SENSOR_POLICY:Z

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/hardware/SensorManager;Landroid/os/Looper;Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/Sensor;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensorManager"    # Landroid/hardware/SensorManager;
    .param p3, "looper"    # Landroid/os/Looper;
    .param p4, "impl"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;
    .param p5, "lightSensor"    # Landroid/hardware/Sensor;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Lcom/android/server/display/DaemonSensorPolicy$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/DaemonSensorPolicy$1;-><init>(Lcom/android/server/display/DaemonSensorPolicy;)V

    iput-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorListener:Landroid/hardware/SensorEventListener;

    .line 156
    new-instance v0, Lcom/android/server/display/DaemonSensorPolicy$2;

    invoke-direct {v0, p0}, Lcom/android/server/display/DaemonSensorPolicy$2;-><init>(Lcom/android/server/display/DaemonSensorPolicy;)V

    iput-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDeviceIdleReceiver:Landroid/content/BroadcastReceiver;

    .line 52
    iput-object p1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mContext:Landroid/content/Context;

    .line 53
    new-instance v0, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

    invoke-direct {v0, p0, p3}, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;-><init>(Lcom/android/server/display/DaemonSensorPolicy;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mHandler:Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

    .line 54
    iput-object p2, p0, Lcom/android/server/display/DaemonSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    .line 55
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mPowerManager:Landroid/os/PowerManager;

    .line 56
    iput-object p4, p0, Lcom/android/server/display/DaemonSensorPolicy;->mAutomaticBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 57
    iput-object p5, p0, Lcom/android/server/display/DaemonSensorPolicy;->mMainLightSensor:Landroid/hardware/Sensor;

    .line 58
    invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->addDaemonSensor()V

    .line 62
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mHandler:Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

    new-instance v1, Lcom/android/server/display/DaemonSensorPolicy$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/DaemonSensorPolicy$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/DaemonSensorPolicy;)V

    invoke-virtual {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;->post(Ljava/lang/Runnable;)Z

    .line 65
    return-void
.end method

.method private addDaemonSensor()V
    .locals 2

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorValues:Ljava/util/Map;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensors:Ljava/util/List;

    .line 87
    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mMainLightSensor:Landroid/hardware/Sensor;

    if-eqz v1, :cond_0

    .line 88
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa266f

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 92
    .local v0, "sensor":Landroid/hardware/Sensor;
    if-eqz v0, :cond_1

    .line 93
    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensors:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_1
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 0

    .line 63
    invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->registerReceiver()V

    .line 64
    return-void
.end method

.method private registerReceiver()V
    .locals 3

    .line 68
    sget-boolean v0, Lcom/android/server/display/DaemonSensorPolicy;->USE_DAEMON_SENSOR_POLICY:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 70
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.os.action.DEVICE_IDLE_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDeviceIdleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 72
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z

    .line 74
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private unregisterReceiver()V
    .locals 2

    .line 77
    iget-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDeviceIdleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleReceiverRegistered:Z

    .line 81
    :cond_0
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 165
    sget-boolean v0, Lcom/android/server/display/DaemonSensorPolicy;->USE_DAEMON_SENSOR_POLICY:Z

    if-eqz v0, :cond_1

    .line 166
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 167
    const-string v0, "Daemon sensor policy state:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDaemonLightSensorsEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIsDeviceIdleMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mIsDeviceIdleMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDaemonSensors: size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Sensor;

    .line 172
    .local v1, "sensor":Landroid/hardware/Sensor;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "      sensor name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 173
    .end local v1    # "sensor":Landroid/hardware/Sensor;
    goto :goto_0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorValues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 175
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Float;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 176
    .local v2, "type":I
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 177
    .local v3, "lux":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "      sensor [type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", lux="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "];"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 178
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Float;>;"
    .end local v2    # "type":I
    .end local v3    # "lux":F
    goto :goto_1

    .line 180
    :cond_1
    return-void
.end method

.method protected getDaemonSensorValue(I)F
    .locals 3
    .param p1, "type"    # I

    .line 124
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorValues:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    sget-object v0, Lcom/android/server/display/DaemonSensorPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "the sensor of the type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not in daemon sensor list!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorValues:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method protected getMainLightSensorLux()F
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mMainLightSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/display/DaemonSensorPolicy;->getDaemonSensorValue(I)F

    move-result v0

    return v0

    .line 120
    :cond_0
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method public notifyRegisterDaemonLightSensor(II)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "displayPolicy"    # I

    .line 132
    const/4 v0, 0x3

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 133
    .local v0, "isActive":Z
    :goto_0
    iput p2, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDisplayPolicy:I

    .line 134
    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p0, v1}, Lcom/android/server/display/DaemonSensorPolicy;->setDaemonLightSensorsEnabled(Z)V

    .line 137
    :cond_1
    return-void
.end method

.method protected setDaemonLightSensorsEnabled(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .line 98
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 99
    iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z

    if-nez v1, :cond_2

    .line 100
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z

    .line 101
    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Sensor;

    .line 102
    .local v2, "sensor":Landroid/hardware/Sensor;
    iget-object v3, p0, Lcom/android/server/display/DaemonSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v5, p0, Lcom/android/server/display/DaemonSensorPolicy;->mHandler:Lcom/android/server/display/DaemonSensorPolicy$DaemonSensorHandle;

    invoke-virtual {v3, v4, v2, v0, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 104
    .end local v2    # "sensor":Landroid/hardware/Sensor;
    goto :goto_0

    .line 105
    :cond_0
    sget-object v0, Lcom/android/server/display/DaemonSensorPolicy;->TAG:Ljava/lang/String;

    const-string v1, "register daemon light sensor."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 107
    :cond_1
    iget-boolean v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z

    if-eqz v1, :cond_2

    .line 108
    iput-boolean v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonLightSensorsEnabled:Z

    .line 109
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 110
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy;->mDaemonSensorValues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 111
    sget-object v0, Lcom/android/server/display/DaemonSensorPolicy;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unregister daemon light sensor."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_2
    :goto_1
    return-void
.end method

.method protected stop()V
    .locals 1

    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/display/DaemonSensorPolicy;->setDaemonLightSensorsEnabled(Z)V

    .line 141
    invoke-direct {p0}, Lcom/android/server/display/DaemonSensorPolicy;->unregisterReceiver()V

    .line 142
    return-void
.end method
