class com.android.server.display.BlackLinearGradientView extends android.view.View {
	 /* .source "BlackLinearGradientView.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
public static final java.lang.String TAG;
/* # instance fields */
private final Integer DEFAULT_COLOR;
com.android.server.display.BlackLinearGradientView$BlackLinearGradient mBlackLinearGradient;
private Integer mEndColor;
private android.graphics.Paint mPaint;
private android.graphics.RectF mRectF;
private Integer mStartColor;
/* # direct methods */
public com.android.server.display.BlackLinearGradientView ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 28 */
	 /* invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V */
	 /* .line 17 */
	 /* const/high16 v0, -0x1000000 */
	 /* iput v0, p0, Lcom/android/server/display/BlackLinearGradientView;->DEFAULT_COLOR:I */
	 /* .line 19 */
	 /* iput v0, p0, Lcom/android/server/display/BlackLinearGradientView;->mStartColor:I */
	 /* .line 20 */
	 /* iput v0, p0, Lcom/android/server/display/BlackLinearGradientView;->mEndColor:I */
	 /* .line 29 */
	 /* invoke-direct {p0}, Lcom/android/server/display/BlackLinearGradientView;->initPaint()V */
	 /* .line 30 */
	 return;
} // .end method
private void initPaint ( ) {
	 /* .locals 4 */
	 /* .line 33 */
	 /* new-instance v0, Landroid/graphics/Paint; */
	 /* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
	 this.mPaint = v0;
	 /* .line 34 */
	 int v1 = 1; // const/4 v1, 0x1
	 (( android.graphics.Paint ) v0 ).setAntiAlias ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V
	 /* .line 35 */
	 v0 = this.mPaint;
	 v1 = android.graphics.Paint$Style.FILL;
	 (( android.graphics.Paint ) v0 ).setStyle ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
	 /* .line 36 */
	 /* new-instance v0, Landroid/graphics/RectF; */
	 v1 = 	 (( com.android.server.display.BlackLinearGradientView ) p0 ).getWidth ( ); // invoke-virtual {p0}, Lcom/android/server/display/BlackLinearGradientView;->getWidth()I
	 /* int-to-float v1, v1 */
	 v2 = 	 (( com.android.server.display.BlackLinearGradientView ) p0 ).getHeight ( ); // invoke-virtual {p0}, Lcom/android/server/display/BlackLinearGradientView;->getHeight()I
	 /* int-to-float v2, v2 */
	 int v3 = 0; // const/4 v3, 0x0
	 /* invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V */
	 this.mRectF = v0;
	 /* .line 37 */
	 return;
} // .end method
/* # virtual methods */
protected void onDraw ( android.graphics.Canvas p0 ) {
	 /* .locals 2 */
	 /* .param p1, "canvas" # Landroid/graphics/Canvas; */
	 /* .line 56 */
	 /* invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V */
	 /* .line 57 */
	 v0 = this.mPaint;
	 v1 = this.mBlackLinearGradient;
	 (( android.graphics.Paint ) v0 ).setShader ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
	 /* .line 58 */
	 v0 = this.mRectF;
	 v1 = this.mPaint;
	 (( android.graphics.Canvas ) p1 ).drawRect ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
	 /* .line 59 */
	 return;
} // .end method
protected void onSizeChanged ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
	 /* .locals 13 */
	 /* .param p1, "width" # I */
	 /* .param p2, "height" # I */
	 /* .param p3, "oldWidth" # I */
	 /* .param p4, "oldHeight" # I */
	 /* .line 41 */
	 /* move-object v9, p0 */
	 /* move v10, p2 */
	 /* invoke-super/range {p0 ..p4}, Landroid/view/View;->onSizeChanged(IIII)V */
	 /* .line 43 */
	 /* new-instance v0, Landroid/graphics/RectF; */
	 /* move v11, p1 */
	 /* int-to-float v1, v11 */
	 /* int-to-float v2, v10 */
	 int v3 = 0; // const/4 v3, 0x0
	 /* invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V */
	 this.mRectF = v0;
	 /* .line 44 */
	 /* new-instance v12, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient; */
	 int v2 = 0; // const/4 v2, 0x0
	 int v4 = 0; // const/4 v4, 0x0
	 /* int-to-float v5, v10 */
	 /* const/high16 v0, -0x1000000 */
	 /* filled-new-array {v0, v0}, [I */
	 int v0 = 2; // const/4 v0, 0x2
	 /* new-array v7, v0, [F */
	 /* fill-array-data v7, :array_0 */
	 v8 = android.graphics.Shader$TileMode.CLAMP;
	 /* move-object v0, v12 */
	 /* move-object v1, p0 */
	 /* invoke-direct/range {v0 ..v8}, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;-><init>(Lcom/android/server/display/BlackLinearGradientView;FFFF[I[FLandroid/graphics/Shader$TileMode;)V */
	 this.mBlackLinearGradient = v12;
	 /* .line 52 */
	 return;
	 /* nop */
	 /* :array_0 */
	 /* .array-data 4 */
	 /* 0x0 */
	 /* 0x3f800000 # 1.0f */
} // .end array-data
} // .end method
public void setLinearGradientColor ( Integer[] p0 ) {
/* .locals 1 */
/* .param p1, "colors" # [I */
/* .line 62 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [F */
/* fill-array-data v0, :array_0 */
(( com.android.server.display.BlackLinearGradientView ) p0 ).setLinearGradientColor ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/display/BlackLinearGradientView;->setLinearGradientColor([I[F)V
/* .line 63 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x0 */
/* 0x3f800000 # 1.0f */
} // .end array-data
} // .end method
public void setLinearGradientColor ( Integer[] p0, Float[] p1 ) {
/* .locals 10 */
/* .param p1, "colors" # [I */
/* .param p2, "positions" # [F */
/* .line 66 */
/* array-length v0, p1 */
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_1 */
/* array-length v0, p2 */
/* if-ge v0, v1, :cond_0 */
/* .line 70 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* aget v0, p1, v0 */
/* iput v0, p0, Lcom/android/server/display/BlackLinearGradientView;->mStartColor:I */
/* .line 71 */
int v0 = 1; // const/4 v0, 0x1
/* aget v0, p1, v0 */
/* iput v0, p0, Lcom/android/server/display/BlackLinearGradientView;->mEndColor:I */
/* .line 73 */
/* new-instance v0, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient; */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
v1 = this.mRectF;
/* iget v6, v1, Landroid/graphics/RectF;->bottom:F */
v9 = android.graphics.Shader$TileMode.CLAMP;
/* move-object v1, v0 */
/* move-object v2, p0 */
/* move-object v7, p1 */
/* move-object v8, p2 */
/* invoke-direct/range {v1 ..v9}, Lcom/android/server/display/BlackLinearGradientView$BlackLinearGradient;-><init>(Lcom/android/server/display/BlackLinearGradientView;FFFF[I[FLandroid/graphics/Shader$TileMode;)V */
this.mBlackLinearGradient = v0;
/* .line 77 */
(( com.android.server.display.BlackLinearGradientView ) p0 ).postInvalidate ( ); // invoke-virtual {p0}, Lcom/android/server/display/BlackLinearGradientView;->postInvalidate()V
/* .line 81 */
return;
/* .line 67 */
} // :cond_1
} // :goto_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 85 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "LinearGradientView{ mStartColor="; // const-string v1, "LinearGradientView{ mStartColor="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "0x"; // const-string v1, "0x"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/BlackLinearGradientView;->mStartColor:I */
/* .line 86 */
java.lang.Integer .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "%08x"; // const-string v3, "%08x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", mEndColor="; // const-string v2, ", mEndColor="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/BlackLinearGradientView;->mEndColor:I */
/* .line 87 */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mRectF="; // const-string v1, ", mRectF="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRectF;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 85 */
} // .end method
