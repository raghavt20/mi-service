class com.android.server.display.DisplayPowerControllerImpl$3 implements com.android.server.display.ThermalBrightnessController$Callback {
	 /* .source "DisplayPowerControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayPowerControllerImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayPowerControllerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayPowerControllerImpl$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .line 1005 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onThermalBrightnessChanged ( Float p0 ) {
/* .locals 3 */
/* .param p1, "thermalBrightness" # F */
/* .line 1008 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmHandler ( v0 );
int v1 = 5; // const/4 v1, 0x5
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->removeMessages(I)V
/* .line 1009 */
v0 = this.this$0;
com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmHandler ( v0 );
/* .line 1010 */
v2 = java.lang.Float .floatToIntBits ( p1 );
java.lang.Integer .valueOf ( v2 );
/* .line 1009 */
(( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1011 */
/* .local v0, "msg":Landroid/os/Message; */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 1012 */
return;
} // .end method
