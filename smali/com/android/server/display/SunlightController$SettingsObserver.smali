.class Lcom/android/server/display/SunlightController$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "SunlightController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SunlightController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/SunlightController;


# direct methods
.method public constructor <init>(Lcom/android/server/display/SunlightController;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 517
    iput-object p1, p0, Lcom/android/server/display/SunlightController$SettingsObserver;->this$0:Lcom/android/server/display/SunlightController;

    .line 518
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 519
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 523
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 524
    .local v0, "lastPathSegment":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "screen_brightness"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_1
    const-string v1, "screen_brightness_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "low_power_level_state"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_3
    const-string/jumbo v1, "sunlight_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 531
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/display/SunlightController$SettingsObserver;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v1}, Lcom/android/server/display/SunlightController;->-$$Nest$mupdateLowPowerState(Lcom/android/server/display/SunlightController;)V

    .line 532
    goto :goto_2

    .line 528
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/display/SunlightController$SettingsObserver;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v1}, Lcom/android/server/display/SunlightController;->-$$Nest$mupdateSunlightModeSettings(Lcom/android/server/display/SunlightController;)V

    .line 529
    nop

    .line 537
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x69203588 -> :sswitch_3
        -0x2a02884f -> :sswitch_2
        -0x294f7102 -> :sswitch_1
        0x67748604 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
