.class final Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;
.super Landroid/os/Handler;
.source "MiuiFoldPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/MiuiFoldPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MiuiFoldPolicyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/MiuiFoldPolicy;


# direct methods
.method public constructor <init>(Lcom/android/server/display/MiuiFoldPolicy;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 243
    iput-object p1, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    .line 244
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 245
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 249
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 263
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->handleScreenTurningOff()V

    .line 264
    goto :goto_0

    .line 266
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/MiuiFoldPolicy;->handleScreenTurningOn()V

    .line 267
    goto :goto_0

    .line 260
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v0}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mupdateSettings(Lcom/android/server/display/MiuiFoldPolicy;)V

    .line 261
    goto :goto_0

    .line 257
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/server/display/MiuiFoldPolicy;->handleDeviceStateChanged(I)V

    .line 258
    goto :goto_0

    .line 254
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    const-string v1, "screen off"

    invoke-static {v0, v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mreleaseSwipeUpWindow(Lcom/android/server/display/MiuiFoldPolicy;Ljava/lang/String;)V

    .line 255
    goto :goto_0

    .line 251
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/display/MiuiFoldPolicy$MiuiFoldPolicyHandler;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v0}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mshowOrReleaseSwipeUpWindow(Lcom/android/server/display/MiuiFoldPolicy;)V

    .line 252
    nop

    .line 271
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
