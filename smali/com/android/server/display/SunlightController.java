public class com.android.server.display.SunlightController {
	 /* .source "SunlightController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/SunlightController$Callback;, */
	 /* Lcom/android/server/display/SunlightController$SunlightModeHandler;, */
	 /* Lcom/android/server/display/SunlightController$NotificationHelper;, */
	 /* Lcom/android/server/display/SunlightController$SettingsObserver;, */
	 /* Lcom/android/server/display/SunlightController$ScreenOnReceiver;, */
	 /* Lcom/android/server/display/SunlightController$ScreenHangUpReceiver;, */
	 /* Lcom/android/server/display/SunlightController$UserSwitchObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static Boolean DEBUG;
private static final java.lang.String ENABLE_SENSOR_REASON_DEFAULT;
private static final java.lang.String ENABLE_SENSOR_REASON_NOTIFICATION;
private static final Integer MSG_INITIALIZE;
private static final Integer MSG_SCREEN_HANG_UP_RECEIVE;
private static final Integer MSG_SCREEN_ON_OFF_RECEIVE;
private static final Integer MSG_UPDATE_SUNLIGHT_MODE;
private static final Integer RESET_USER_DISABLE_DURATION;
private static final Integer SUNLIGHT_AMBIENT_LIGHT_HORIZON;
private static final Integer SUNLIGHT_LIGHT_SENSOR_RATE;
private static final java.lang.String TAG;
private static final Integer THRESHOLD_ENTER_SUNLIGHT_DURATION;
private static final Integer THRESHOLD_EXIT_SUNLIGHT_DURATION;
private static final Integer THRESHOLD_SUNLIGHT_LUX;
private static final Float THRESHOLD_SUNLIGHT_NIT_DEFAULT;
/* # instance fields */
private com.android.server.display.AmbientLightRingBuffer mAmbientLightRingBuffer;
private Boolean mAutoBrightnessSettingsEnable;
private Boolean mBelowThresholdNit;
private com.android.server.display.SunlightController$Callback mCallback;
private android.content.Context mContext;
private Float mCurrentAmbientLux;
private Integer mCurrentScreenBrightnessSettings;
private Integer mDisplayId;
private com.android.server.display.SunlightController$SunlightModeHandler mHandler;
private Float mLastObservedLux;
private Long mLastObservedLuxTime;
private Long mLastScreenOffTime;
private Boolean mLastSunlightSettingsEnable;
private android.hardware.Sensor mLightSensor;
private final android.hardware.SensorEventListener mLightSensorListener;
private Boolean mLowPowerState;
private com.android.server.display.SunlightController$NotificationHelper mNotificationHelper;
private android.os.PowerManager mPowerManager;
private Boolean mPreparedForNotification;
private Integer mScreenBrightnessDefaultSettings;
private Boolean mScreenIsHangUp;
private Boolean mScreenOn;
private android.hardware.SensorManager mSensorManager;
private com.android.server.display.SunlightController$SettingsObserver mSettingsObserver;
private Boolean mSunlightModeActive;
private Boolean mSunlightModeDisabledByUser;
private Boolean mSunlightModeEnable;
private Long mSunlightSensorEnableTime;
private Boolean mSunlightSensorEnabled;
private java.lang.String mSunlightSensorEnabledReason;
private Boolean mSunlightSettingsEnable;
private Float mThresholdSunlightNit;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static com.android.server.display.SunlightController$SunlightModeHandler -$$Nest$fgetmHandler ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmSunlightSensorEnabled ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
} // .end method
static void -$$Nest$mhandleLightSensorEvent ( com.android.server.display.SunlightController p0, Long p1, Float p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/SunlightController;->handleLightSensorEvent(JF)V */
	 return;
} // .end method
static void -$$Nest$mregister ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->register()V */
	 return;
} // .end method
static void -$$Nest$mupdateAmbientLux ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateAmbientLux()V */
	 return;
} // .end method
static void -$$Nest$mupdateHangUpState ( com.android.server.display.SunlightController p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController;->updateHangUpState(Z)V */
	 return;
} // .end method
static void -$$Nest$mupdateLowPowerState ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateLowPowerState()V */
	 return;
} // .end method
static void -$$Nest$mupdateScreenState ( com.android.server.display.SunlightController p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController;->updateScreenState(Z)V */
	 return;
} // .end method
static void -$$Nest$mupdateSunlightModeSettings ( com.android.server.display.SunlightController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeSettings()V */
	 return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 /* sget-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z */
} // .end method
static com.android.server.display.SunlightController ( ) {
	 /* .locals 1 */
	 /* .line 46 */
	 int v0 = 0; // const/4 v0, 0x0
	 com.android.server.display.SunlightController.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.display.SunlightController ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "callback" # Lcom/android/server/display/SunlightController$Callback; */
	 /* .param p3, "looper" # Landroid/os/Looper; */
	 /* .param p4, "displayId" # I */
	 /* .line 109 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 101 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
	 /* .line 105 */
	 /* const/high16 v0, 0x43200000 # 160.0f */
	 /* iput v0, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F */
	 /* .line 299 */
	 /* new-instance v0, Lcom/android/server/display/SunlightController$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SunlightController$1;-><init>(Lcom/android/server/display/SunlightController;)V */
	 this.mLightSensorListener = v0;
	 /* .line 110 */
	 /* if-nez p4, :cond_0 */
	 /* .line 114 */
	 this.mContext = p1;
	 /* .line 115 */
	 this.mCallback = p2;
	 /* .line 116 */
	 /* iput p4, p0, Lcom/android/server/display/SunlightController;->mDisplayId:I */
	 /* .line 117 */
	 /* new-instance v0, Lcom/android/server/display/SunlightController$SunlightModeHandler; */
	 /* invoke-direct {v0, p0, p3}, Lcom/android/server/display/SunlightController$SunlightModeHandler;-><init>(Lcom/android/server/display/SunlightController;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 118 */
	 /* new-instance v0, Lcom/android/server/display/SunlightController$NotificationHelper; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SunlightController$NotificationHelper;-><init>(Lcom/android/server/display/SunlightController;)V */
	 this.mNotificationHelper = v0;
	 /* .line 119 */
	 /* new-instance v0, Landroid/hardware/SystemSensorManager; */
	 v1 = this.mContext;
	 v2 = this.mHandler;
	 (( com.android.server.display.SunlightController$SunlightModeHandler ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1, v2}, Landroid/hardware/SystemSensorManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V */
	 this.mSensorManager = v0;
	 /* .line 120 */
	 int v1 = 5; // const/4 v1, 0x5
	 (( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
	 this.mLightSensor = v0;
	 /* .line 121 */
	 /* new-instance v0, Lcom/android/server/display/AmbientLightRingBuffer; */
	 /* const-wide/16 v1, 0xfa */
	 /* const/16 v3, 0x2710 */
	 /* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/display/AmbientLightRingBuffer;-><init>(JI)V */
	 this.mAmbientLightRingBuffer = v0;
	 /* .line 122 */
	 /* new-instance v0, Lcom/android/server/display/SunlightController$SettingsObserver; */
	 v1 = this.mHandler;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/display/SunlightController$SettingsObserver;-><init>(Lcom/android/server/display/SunlightController;Landroid/os/Handler;)V */
	 this.mSettingsObserver = v0;
	 /* .line 123 */
	 v0 = this.mContext;
	 final String v1 = "power"; // const-string v1, "power"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/os/PowerManager; */
	 this.mPowerManager = v0;
	 /* .line 124 */
	 v0 = 	 (( android.os.PowerManager ) v0 ).getDefaultScreenBrightnessSetting ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I
	 /* iput v0, p0, Lcom/android/server/display/SunlightController;->mScreenBrightnessDefaultSettings:I */
	 /* .line 125 */
	 v0 = this.mHandler;
	 int v1 = 4; // const/4 v1, 0x4
	 (( com.android.server.display.SunlightController$SunlightModeHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->sendEmptyMessage(I)Z
	 /* .line 126 */
	 return;
	 /* .line 111 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Sunlight mode can only be used on the default display."; // const-string v1, "Sunlight mode can only be used on the default display."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private void applyLightSensorMeasurement ( Long p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "time" # J */
/* .param p3, "lux" # F */
/* .line 367 */
v0 = this.mAmbientLightRingBuffer;
/* const-wide/16 v1, 0x2710 */
/* sub-long v1, p1, v1 */
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).prune ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V
/* .line 368 */
v0 = this.mAmbientLightRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).push ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/AmbientLightRingBuffer;->push(JF)V
/* .line 370 */
/* iput p3, p0, Lcom/android/server/display/SunlightController;->mLastObservedLux:F */
/* .line 371 */
/* iput-wide p1, p0, Lcom/android/server/display/SunlightController;->mLastObservedLuxTime:J */
/* .line 372 */
return;
} // .end method
private void clearAmbientLightRingBuffer ( ) {
/* .locals 1 */
/* .line 484 */
v0 = this.mAmbientLightRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).clear ( ); // invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->clear()V
/* .line 485 */
return;
} // .end method
private void handleLightSensorEvent ( Long p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "time" # J */
/* .param p3, "lux" # F */
/* .line 316 */
/* sget-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 317 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "handleLightSensorEvent: lux = "; // const-string v1, "handleLightSensorEvent: lux = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "SunlightController"; // const-string v1, "SunlightController"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 319 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.display.SunlightController$SunlightModeHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->removeMessages(I)V
/* .line 320 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/SunlightController;->applyLightSensorMeasurement(JF)V */
/* .line 321 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/SunlightController;->updateAmbientLux(J)V */
/* .line 322 */
return;
} // .end method
private Long nextEnterSunlightModeTransition ( Long p0 ) {
/* .locals 6 */
/* .param p1, "time" # J */
/* .line 375 */
v0 = this.mAmbientLightRingBuffer;
v0 = (( com.android.server.display.AmbientLightRingBuffer ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
/* .line 376 */
/* .local v0, "N":I */
/* move-wide v1, p1 */
/* .line 377 */
/* .local v1, "earliestValidTime":J */
/* add-int/lit8 v3, v0, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* .line 378 */
v4 = this.mAmbientLightRingBuffer;
v4 = (( com.android.server.display.AmbientLightRingBuffer ) v4 ).getLux ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* const v5, 0x463b8000 # 12000.0f */
/* cmpg-float v4, v4, v5 */
/* if-gtz v4, :cond_0 */
/* .line 379 */
/* .line 381 */
} // :cond_0
v4 = this.mAmbientLightRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v4 ).getTime ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v1 */
/* .line 377 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 383 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* const-wide/16 v3, 0x1388 */
/* add-long/2addr v3, v1 */
/* return-wide v3 */
} // .end method
private Long nextExitSunlightModeTransition ( Long p0 ) {
/* .locals 6 */
/* .param p1, "time" # J */
/* .line 387 */
v0 = this.mAmbientLightRingBuffer;
v0 = (( com.android.server.display.AmbientLightRingBuffer ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
/* .line 388 */
/* .local v0, "N":I */
/* move-wide v1, p1 */
/* .line 389 */
/* .local v1, "earliestValidTime":J */
/* add-int/lit8 v3, v0, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* .line 390 */
v4 = this.mAmbientLightRingBuffer;
v4 = (( com.android.server.display.AmbientLightRingBuffer ) v4 ).getLux ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* const v5, 0x463b8000 # 12000.0f */
/* cmpl-float v4, v4, v5 */
/* if-ltz v4, :cond_0 */
/* .line 391 */
/* .line 393 */
} // :cond_0
v4 = this.mAmbientLightRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v4 ).getTime ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v1 */
/* .line 389 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 395 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* const-wide/16 v3, 0x7d0 */
/* add-long/2addr v3, v1 */
/* return-wide v3 */
} // .end method
private void register ( ) {
/* .locals 0 */
/* .line 130 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerSettingsObserver()V */
/* .line 131 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerScreenOnReceiver()V */
/* .line 132 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerHangUpReceiver()V */
/* .line 133 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->registerUserSwitchObserver()V */
/* .line 134 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeSettings()V */
/* .line 135 */
return;
} // .end method
private void registerHangUpReceiver ( ) {
/* .locals 4 */
/* .line 165 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 166 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "miui.intent.action.HANG_UP_CHANGED"; // const-string v1, "miui.intent.action.HANG_UP_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 167 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/display/SunlightController$ScreenHangUpReceiver; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/display/SunlightController$ScreenHangUpReceiver;-><init>(Lcom/android/server/display/SunlightController;Lcom/android/server/display/SunlightController$ScreenHangUpReceiver-IA;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 168 */
return;
} // .end method
private void registerScreenOnReceiver ( ) {
/* .locals 4 */
/* .line 157 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 158 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 159 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 160 */
/* const/16 v1, 0x3e8 */
(( android.content.IntentFilter ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V
/* .line 161 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/display/SunlightController$ScreenOnReceiver; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/android/server/display/SunlightController$ScreenOnReceiver;-><init>(Lcom/android/server/display/SunlightController;Lcom/android/server/display/SunlightController$ScreenOnReceiver-IA;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 162 */
return;
} // .end method
private void registerSettingsObserver ( ) {
/* .locals 5 */
/* .line 138 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 139 */
/* const-string/jumbo v1, "sunlight_mode" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 138 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 142 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 143 */
final String v1 = "screen_brightness_mode"; // const-string v1, "screen_brightness_mode"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 142 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 146 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 147 */
final String v1 = "screen_brightness"; // const-string v1, "screen_brightness"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 146 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 150 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 151 */
final String v1 = "low_power_level_state"; // const-string v1, "low_power_level_state"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mSettingsObserver;
/* .line 150 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 154 */
return;
} // .end method
private void registerUserSwitchObserver ( ) {
/* .locals 3 */
/* .line 172 */
try { // :try_start_0
/* new-instance v0, Lcom/android/server/display/SunlightController$UserSwitchObserver; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/SunlightController$UserSwitchObserver;-><init>(Lcom/android/server/display/SunlightController;Lcom/android/server/display/SunlightController$UserSwitchObserver-IA;)V */
/* .line 173 */
/* .local v0, "observer":Lcom/android/server/display/SunlightController$UserSwitchObserver; */
android.app.ActivityManager .getService ( );
final String v2 = "SunlightController"; // const-string v2, "SunlightController"
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 176 */
} // .end local v0 # "observer":Lcom/android/server/display/SunlightController$UserSwitchObserver;
/* .line 174 */
/* :catch_0 */
/* move-exception v0 */
/* .line 177 */
} // :goto_0
return;
} // .end method
private void resetUserDisableTemporaryData ( ) {
/* .locals 2 */
/* .line 476 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 477 */
final String v0 = "SunlightController"; // const-string v0, "SunlightController"
final String v1 = "Reset user slide operation."; // const-string v1, "Reset user slide operation."
android.util.Slog .d ( v0,v1 );
/* .line 478 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
/* .line 479 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z */
/* .line 481 */
} // :cond_0
return;
} // .end method
private Boolean setLightSensorEnabled ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .line 265 */
/* const-string/jumbo v0, "sunlight_mode" */
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabled(ZLjava/lang/String;)Z */
} // .end method
private Boolean setLightSensorEnabled ( Boolean p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "enabled" # Z */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 269 */
/* sget-boolean v0, Lcom/android/server/display/SunlightController;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 270 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setLightSensorEnabled: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", enabled reason: "; // const-string v1, ", enabled reason: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SunlightController"; // const-string v1, "SunlightController"
android.util.Slog .d ( v1,v0 );
/* .line 272 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 273 */
v2 = this.mSunlightSensorEnabledReason;
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 274 */
this.mSunlightSensorEnabledReason = p2;
/* .line 276 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
/* if-nez v2, :cond_2 */
/* .line 277 */
/* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
/* .line 278 */
v2 = this.mSensorManager;
v3 = this.mLightSensorListener;
v4 = this.mLightSensor;
v5 = this.mHandler;
(( android.hardware.SensorManager ) v2 ).registerListener ( v3, v4, v1, v5 ); // invoke-virtual {v2, v3, v4, v1, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 280 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnableTime:J */
/* .line 281 */
/* .line 283 */
} // :cond_2
/* .line 285 */
} // :cond_3
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 286 */
/* iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
/* .line 287 */
/* iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z */
/* .line 288 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnableTime:J */
/* .line 289 */
v2 = this.mHandler;
(( com.android.server.display.SunlightController$SunlightModeHandler ) v2 ).removeMessages ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->removeMessages(I)V
/* .line 290 */
v2 = this.mSensorManager;
v3 = this.mLightSensorListener;
(( android.hardware.SensorManager ) v2 ).unregisterListener ( v3 ); // invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 292 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/SunlightController;->setSunLightModeActive(Z)V */
/* .line 293 */
/* .line 295 */
} // :cond_4
} // .end method
private void setLightSensorEnabledForNotification ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 217 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 218 */
/* iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z */
/* .line 219 */
final String v0 = "prepare_for_notifaction"; // const-string v0, "prepare_for_notifaction"
/* invoke-direct {p0, p1, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabled(ZLjava/lang/String;)Z */
/* .line 221 */
} // :cond_0
return;
} // .end method
private void setSunLightModeActive ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "active" # Z */
/* .line 359 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeActive:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 360 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setSunLightModeActive: active: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SunlightController"; // const-string v1, "SunlightController"
android.util.Slog .d ( v1,v0 );
/* .line 361 */
/* iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeActive:Z */
/* .line 362 */
v0 = this.mCallback;
/* .line 364 */
} // :cond_0
return;
} // .end method
private void shouldPrepareToNotify ( ) {
/* .locals 1 */
/* .line 210 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z */
/* if-nez v0, :cond_0 */
v0 = this.mNotificationHelper;
v0 = com.android.server.display.SunlightController$NotificationHelper .-$$Nest$fgetmHasReachedLimitTimes ( v0 );
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 213 */
/* .local v0, "enable":Z */
} // :goto_0
/* invoke-direct {p0, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabledForNotification(Z)V */
/* .line 214 */
return;
} // .end method
private void updateAmbientLux ( ) {
/* .locals 5 */
/* .line 325 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 326 */
/* .local v0, "time":J */
v2 = this.mAmbientLightRingBuffer;
/* const-wide/16 v3, 0x2710 */
/* sub-long v3, v0, v3 */
(( com.android.server.display.AmbientLightRingBuffer ) v2 ).prune ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V
/* .line 327 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/SunlightController;->updateAmbientLux(J)V */
/* .line 328 */
return;
} // .end method
private void updateAmbientLux ( Long p0 ) {
/* .locals 10 */
/* .param p1, "time" # J */
/* .line 331 */
v0 = this.mAmbientLightRingBuffer;
/* const-wide/16 v1, 0x2710 */
v0 = (( com.android.server.display.AmbientLightRingBuffer ) v0 ).calculateAmbientLux ( p1, p2, v1, v2 ); // invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F
/* iput v0, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F */
/* .line 332 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/SunlightController;->nextEnterSunlightModeTransition(J)J */
/* move-result-wide v0 */
/* .line 333 */
/* .local v0, "nextEnterSunlightModeTime":J */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/SunlightController;->nextExitSunlightModeTransition(J)J */
/* move-result-wide v2 */
/* .line 335 */
/* .local v2, "nextExitSunlightModeTime":J */
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z */
/* if-nez v4, :cond_1 */
/* .line 336 */
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mLowPowerState:Z */
/* if-nez v4, :cond_0 */
/* .line 337 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateNotificationState()V */
/* .line 339 */
} // :cond_0
return;
/* .line 342 */
} // :cond_1
/* iget v4, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F */
/* const v5, 0x463b8000 # 12000.0f */
/* cmpl-float v6, v4, v5 */
int v7 = 1; // const/4 v7, 0x1
/* if-ltz v6, :cond_2 */
/* cmp-long v6, v0, p1 */
/* if-gtz v6, :cond_2 */
/* iget-boolean v6, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
/* iget-boolean v6, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 344 */
/* invoke-direct {p0, v7}, Lcom/android/server/display/SunlightController;->setSunLightModeActive(Z)V */
/* .line 345 */
} // :cond_2
/* cmpg-float v4, v4, v5 */
/* if-gez v4, :cond_3 */
/* cmp-long v4, v2, p1 */
/* if-gtz v4, :cond_3 */
/* .line 346 */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {p0, v4}, Lcom/android/server/display/SunlightController;->setSunLightModeActive(Z)V */
/* .line 348 */
} // :cond_3
} // :goto_0
java.lang.Math .min ( v0,v1,v2,v3 );
/* move-result-wide v4 */
/* .line 350 */
/* .local v4, "nextTransitionTime":J */
/* cmp-long v6, v4, p1 */
/* if-lez v6, :cond_4 */
/* move-wide v8, v4 */
} // :cond_4
/* const-wide/16 v8, 0x3e8 */
/* add-long/2addr v8, p1 */
} // :goto_1
/* move-wide v4, v8 */
/* .line 351 */
/* sget-boolean v6, Lcom/android/server/display/SunlightController;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 352 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "updateAmbientLux: Scheduling lux update for " */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 353 */
android.util.TimeUtils .formatUptime ( v4,v5 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 352 */
final String v8 = "SunlightController"; // const-string v8, "SunlightController"
android.util.Slog .d ( v8,v6 );
/* .line 355 */
} // :cond_5
v6 = this.mHandler;
(( com.android.server.display.SunlightController$SunlightModeHandler ) v6 ).sendEmptyMessageAtTime ( v7, v4, v5 ); // invoke-virtual {v6, v7, v4, v5}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->sendEmptyMessageAtTime(IJ)Z
/* .line 356 */
return;
} // .end method
private void updateHangUpState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "screenIsHangUp" # Z */
/* .line 438 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 439 */
/* iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z */
/* .line 440 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z */
/* .line 442 */
} // :cond_0
return;
} // .end method
private void updateLowPowerState ( ) {
/* .locals 5 */
/* .line 462 */
v0 = this.mContext;
/* .line 463 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 462 */
final String v1 = "low_power_level_state"; // const-string v1, "low_power_level_state"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mLowPowerState:Z */
/* .line 464 */
/* const-string/jumbo v3, "sunlight_mode" */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 465 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mLastSunlightSettingsEnable:Z */
/* .line 466 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putInt ( v0,v3,v2 );
/* .line 469 */
} // :cond_1
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 470 */
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mLastSunlightSettingsEnable:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* move v2, v1 */
/* .line 471 */
} // :cond_2
/* nop */
/* .line 469 */
} // :goto_1
android.provider.Settings$System .putInt ( v0,v3,v2 );
/* .line 473 */
} // :goto_2
return;
} // .end method
private void updateNotificationState ( ) {
/* .locals 2 */
/* .line 246 */
/* iget v0, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F */
/* const v1, 0x463b8000 # 12000.0f */
/* cmpl-float v0, v0, v1 */
/* if-ltz v0, :cond_0 */
/* .line 247 */
v0 = this.mNotificationHelper;
v0 = com.android.server.display.SunlightController$NotificationHelper .-$$Nest$mshowNotificationIfNecessary ( v0 );
/* if-nez v0, :cond_0 */
/* .line 248 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/display/SunlightController;->setLightSensorEnabledForNotification(Z)V */
/* .line 251 */
} // :cond_0
return;
} // .end method
private void updateScreenState ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "screenOn" # Z */
/* .line 445 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
/* if-eq p1, v0, :cond_2 */
/* .line 446 */
/* iput-boolean p1, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
/* .line 447 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 448 */
/* .local v0, "currentTime":J */
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 449 */
/* iget-wide v2, p0, Lcom/android/server/display/SunlightController;->mLastScreenOffTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/32 v4, 0x493e0 */
/* cmp-long v2, v2, v4 */
/* if-ltz v2, :cond_1 */
/* .line 450 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->resetUserDisableTemporaryData()V */
/* .line 453 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->clearAmbientLightRingBuffer()V */
/* .line 454 */
/* iput-wide v0, p0, Lcom/android/server/display/SunlightController;->mLastScreenOffTime:J */
/* .line 456 */
} // :cond_1
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z */
/* .line 457 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->shouldPrepareToNotify()V */
/* .line 459 */
} // .end local v0 # "currentTime":J
} // :cond_2
return;
} // .end method
private Boolean updateSunlightModeCondition ( ) {
/* .locals 5 */
/* .line 224 */
v0 = this.mCallback;
/* iget v1, p0, Lcom/android/server/display/SunlightController;->mCurrentScreenBrightnessSettings:I */
/* .line 225 */
v1 = com.android.internal.display.BrightnessSynchronizerStub .brightnessIntToFloatForLowLevel ( v1 );
v0 = /* .line 224 */
/* .line 227 */
/* .local v0, "currentScreenNit":F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v1, v0, v1 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F */
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* move v1, v3 */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z */
/* .line 228 */
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z */
/* if-nez v1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
/* if-nez v1, :cond_1 */
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z */
/* if-nez v1, :cond_1 */
} // :cond_1
/* move v2, v3 */
} // :goto_1
/* move v1, v2 */
/* .line 230 */
/* .local v1, "enable":Z */
/* sget-boolean v2, Lcom/android/server/display/SunlightController;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 231 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateSunlightModeCondition: mSunlightModeEnable: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", enable: "; // const-string v4, ", enable: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", mScreenOn: "; // const-string v4, ", mScreenOn: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mScreenOn:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", mSunlightModeDisabledByUser: "; // const-string v4, ", mSunlightModeDisabledByUser: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", mBelowThresholdNit: "; // const-string v4, ", mBelowThresholdNit: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", mScreenIsHangUp"; // const-string v4, ", mScreenIsHangUp"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "SunlightController"; // const-string v4, "SunlightController"
android.util.Slog .d ( v4,v2 );
/* .line 237 */
} // :cond_2
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z */
/* if-eq v1, v2, :cond_3 */
/* .line 238 */
/* iput-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeEnable:Z */
/* .line 239 */
/* iput-boolean v3, p0, Lcom/android/server/display/SunlightController;->mPreparedForNotification:Z */
/* .line 240 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/SunlightController;->setLightSensorEnabled(Z)Z */
/* .line 242 */
} // :cond_3
} // .end method
private void updateSunlightModeSettings ( ) {
/* .locals 6 */
/* .line 180 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "sunlight_mode" */
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v3 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 184 */
/* .local v0, "sunlightSettingsChanged":Z */
} // :goto_0
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "screen_brightness_mode"; // const-string v5, "screen_brightness_mode"
v4 = android.provider.Settings$System .getIntForUser ( v4,v5,v2,v3 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* move v2, v1 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z */
/* .line 188 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "screen_brightness"; // const-string v2, "screen_brightness"
/* iget v4, p0, Lcom/android/server/display/SunlightController;->mScreenBrightnessDefaultSettings:I */
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v4,v3 );
/* iput v1, p0, Lcom/android/server/display/SunlightController;->mCurrentScreenBrightnessSettings:I */
/* .line 192 */
/* if-nez v0, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 193 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->resetUserDisableTemporaryData()V */
/* .line 195 */
} // :cond_2
/* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
/* .line 196 */
/* sget-boolean v1, Lcom/android/server/display/SunlightController;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 197 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateSunlightModeSettings: mSunlightSettingsEnable=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mAutoBrightnessSettingsEnable="; // const-string v2, ", mAutoBrightnessSettingsEnable="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mAutoBrightnessSettingsEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mCurrentScreenBrightnessSettings="; // const-string v2, ", mCurrentScreenBrightnessSettings="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/display/SunlightController;->mCurrentScreenBrightnessSettings:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SunlightController"; // const-string v2, "SunlightController"
android.util.Slog .d ( v2,v1 );
/* .line 202 */
} // :cond_3
v1 = this.mCallback;
/* iget-boolean v2, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
/* .line 204 */
v1 = /* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z */
/* if-nez v1, :cond_4 */
/* .line 205 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->shouldPrepareToNotify()V */
/* .line 207 */
} // :cond_4
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 690 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 691 */
final String v0 = "Sunlight Controller Configuration:"; // const-string v0, "Sunlight Controller Configuration:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 692 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSunlightSettingsEnable="; // const-string v1, " mSunlightSettingsEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSettingsEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 693 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSunlightSensorEnableTime="; // const-string v1, " mSunlightSensorEnableTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnableTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 694 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLastObservedLux="; // const-string v1, " mLastObservedLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/SunlightController;->mLastObservedLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 695 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mLastObservedLuxTime="; // const-string v1, " mLastObservedLuxTime="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/server/display/SunlightController;->mLastObservedLuxTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 696 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentAmbientLux="; // const-string v1, " mCurrentAmbientLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/SunlightController;->mCurrentAmbientLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 697 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSunlightSensorEnabled="; // const-string v1, " mSunlightSensorEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 698 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 699 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSunlightSensorEnabledReason="; // const-string v1, " mSunlightSensorEnabledReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSunlightSensorEnabledReason;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 701 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBelowThresholdNit="; // const-string v1, " mBelowThresholdNit="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mBelowThresholdNit:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 702 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSunlightModeActive="; // const-string v1, " mSunlightModeActive="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 703 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mSunlightModeDisabledByUser="; // const-string v1, " mSunlightModeDisabledByUser="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 704 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAmbientLightRingBuffer="; // const-string v1, " mAmbientLightRingBuffer="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAmbientLightRingBuffer;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 705 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mScreenIsHangUp="; // const-string v1, " mScreenIsHangUp="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/display/SunlightController;->mScreenIsHangUp:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 706 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mThresholdSunlightNit="; // const-string v1, " mThresholdSunlightNit="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 707 */
v0 = this.mNotificationHelper;
com.android.server.display.SunlightController$NotificationHelper .-$$Nest$mdump ( v0,p1 );
/* .line 708 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_SC:Z */
com.android.server.display.SunlightController.DEBUG = (v0!= 0);
/* .line 709 */
return;
} // .end method
public Boolean isSunlightModeDisabledByUser ( ) {
/* .locals 1 */
/* .line 407 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
} // .end method
public void setSunlightModeDisabledByUserTemporary ( ) {
/* .locals 2 */
/* .line 399 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
/* if-nez v0, :cond_0 */
/* .line 400 */
final String v0 = "SunlightController"; // const-string v0, "SunlightController"
final String v1 = "Disable sunlight mode temporarily due to user slide bar."; // const-string v1, "Disable sunlight mode temporarily due to user slide bar."
android.util.Slog .d ( v0,v1 );
/* .line 401 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightModeDisabledByUser:Z */
/* .line 402 */
/* invoke-direct {p0}, Lcom/android/server/display/SunlightController;->updateSunlightModeCondition()Z */
/* .line 404 */
} // :cond_0
return;
} // .end method
public void updateAmbientLightSensor ( android.hardware.Sensor p0 ) {
/* .locals 5 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .line 254 */
v0 = this.mLightSensor;
/* if-eq v0, p1, :cond_0 */
/* .line 255 */
this.mLightSensor = p1;
/* .line 256 */
/* iget-boolean v0, p0, Lcom/android/server/display/SunlightController;->mSunlightSensorEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 257 */
v0 = this.mSensorManager;
v1 = this.mLightSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 258 */
v0 = this.mSensorManager;
v1 = this.mLightSensorListener;
v2 = this.mLightSensor;
int v3 = 0; // const/4 v3, 0x0
v4 = this.mHandler;
(( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 262 */
} // :cond_0
return;
} // .end method
public void updateThresholdSunlightNit ( java.lang.Float p0 ) {
/* .locals 1 */
/* .param p1, "thresholdSunlightNit" # Ljava/lang/Float; */
/* .line 558 */
v0 = (( java.lang.Float ) p1 ).floatValue ( ); // invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F
/* iput v0, p0, Lcom/android/server/display/SunlightController;->mThresholdSunlightNit:F */
/* .line 559 */
return;
} // .end method
