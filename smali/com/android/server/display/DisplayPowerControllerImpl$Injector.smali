.class Lcom/android/server/display/DisplayPowerControllerImpl$Injector;
.super Ljava/lang/Object;
.source "DisplayPowerControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayPowerControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Injector"
.end annotation


# instance fields
.field private mOutdoorHighTempToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 2185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2186
    const v0, 0x110f0232

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;->mOutdoorHighTempToast:Landroid/widget/Toast;

    .line 2188
    return-void
.end method


# virtual methods
.method public showOutdoorHighTempToast()V
    .locals 1

    .line 2191
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;->mOutdoorHighTempToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 2192
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$Injector;->mOutdoorHighTempToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2193
    return-void
.end method
