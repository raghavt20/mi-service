.class public Lcom/android/server/display/DualSensorPolicy;
.super Ljava/lang/Object;
.source "DualSensorPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/DualSensorPolicy$DualSensorPolicyHandler;
    }
.end annotation


# static fields
.field private static final ASSISTANT_LIGHT_SENSOR_TYPE:I = 0x1fa266f

.field private static DEBUG:Z = false

.field private static final MSG_UPDATE_ASSISTANT_LIGHT_SENSOR_AMBIENT_LUX:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DualSensorPolicy"


# instance fields
.field private mAmbientLightHorizonLong:I

.field private mAmbientLightHorizonShort:I

.field private mAssistAmbientLuxValid:Z

.field private mAssistBrighteningThreshold:F

.field private mAssistDarkeningThreshold:F

.field private mAssistFastAmbientLux:F

.field private mAssistLightSensor:Landroid/hardware/Sensor;

.field private mAssistLightSensorBrighteningDebounce:J

.field private mAssistLightSensorDarkeningDebounce:J

.field private mAssistLightSensorEnable:Z

.field private mAssistLightSensorEnableTime:J

.field private mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

.field private mAssistLightSensorWarmUpTime:I

.field private mAssistSlowAmbientLux:F

.field private mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

.field private mCurrentAssistLightSensorRate:I

.field private mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

.field private mLightSensorRate:I

.field private mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

.field private mMainFastAmbientLux:F

.field private mMainSlowAmbientLux:F

.field private mPolicyHandler:Landroid/os/Handler;

.field private mSceneDetector:Lcom/android/server/display/SceneDetector;

.field private mSensorListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mUseLightSensorFlag:I


# direct methods
.method static bridge synthetic -$$Nest$mhandleAssistLightSensorEvent(Lcom/android/server/display/DualSensorPolicy;JF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DualSensorPolicy;->handleAssistLightSensorEvent(JF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAssistLightSensorAmbientLux(Lcom/android/server/display/DualSensorPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DualSensorPolicy;->updateAssistLightSensorAmbientLux()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 25
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/display/DualSensorPolicy;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/hardware/SensorManager;IIJJIILcom/android/server/display/HysteresisLevelsStub;Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
    .locals 16
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "sensormanager"    # Landroid/hardware/SensorManager;
    .param p3, "lightSensorWarmUpTime"    # I
    .param p4, "lightSensorRate"    # I
    .param p5, "brighteningLightDebounceConfig"    # J
    .param p7, "darkeningLightDebounceConfig"    # J
    .param p9, "ambientLightHorizonLong"    # I
    .param p10, "ambientLightHorizonShort"    # I
    .param p11, "hysteresisLevelsImpl"    # Lcom/android/server/display/HysteresisLevelsStub;
    .param p12, "listener"    # Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;
    .param p13, "brightnessControllerImpl"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 70
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p4

    move/from16 v3, p9

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v4, -0x1

    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I

    .line 56
    const/high16 v4, -0x40800000    # -1.0f

    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    .line 57
    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    .line 59
    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    .line 60
    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    .line 62
    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    .line 63
    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    .line 91
    new-instance v4, Lcom/android/server/display/DualSensorPolicy$1;

    invoke-direct {v4, v0}, Lcom/android/server/display/DualSensorPolicy$1;-><init>(Lcom/android/server/display/DualSensorPolicy;)V

    iput-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mSensorListener:Landroid/hardware/SensorEventListener;

    .line 71
    new-instance v4, Lcom/android/server/display/DualSensorPolicy$DualSensorPolicyHandler;

    move-object/from16 v5, p1

    invoke-direct {v4, v0, v5}, Lcom/android/server/display/DualSensorPolicy$DualSensorPolicyHandler;-><init>(Lcom/android/server/display/DualSensorPolicy;Landroid/os/Looper;)V

    iput-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    .line 72
    iput-object v1, v0, Lcom/android/server/display/DualSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    .line 73
    const v4, 0x1fa266f

    invoke-virtual {v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v4

    iput-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensor:Landroid/hardware/Sensor;

    .line 74
    move-object/from16 v4, p12

    iput-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    .line 75
    move/from16 v6, p3

    iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorWarmUpTime:I

    .line 76
    iput v2, v0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I

    .line 77
    move-wide/from16 v7, p5

    iput-wide v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorBrighteningDebounce:J

    .line 78
    move-wide/from16 v9, p7

    iput-wide v9, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorDarkeningDebounce:J

    .line 79
    iput v3, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I

    .line 80
    move/from16 v11, p10

    iput v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonShort:I

    .line 81
    move-object/from16 v12, p11

    iput-object v12, v0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    .line 82
    new-instance v13, Lcom/android/server/display/AmbientLightRingBuffer;

    int-to-long v14, v2

    invoke-direct {v13, v14, v15, v3}, Lcom/android/server/display/AmbientLightRingBuffer;-><init>(JI)V

    iput-object v13, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    .line 83
    iget-wide v13, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorBrighteningDebounce:J

    iget-wide v1, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorDarkeningDebounce:J

    invoke-direct {v0, v13, v14, v1, v2}, Lcom/android/server/display/DualSensorPolicy;->setUpDebounceConfig(JJ)V

    .line 84
    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 85
    return-void
.end method

.method private handleAssistLightSensorEvent(JF)V
    .locals 5
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .line 111
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 113
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I

    if-eq v0, v1, :cond_0

    .line 115
    iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I

    .line 116
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 117
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensor:Landroid/hardware/Sensor;

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I

    mul-int/lit16 v3, v3, 0x3e8

    iget-object v4, p0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->getIsTorchOpen()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkAssistSensorValid()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I

    int-to-long v1, v1

    sub-long v1, p1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V

    .line 128
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/AmbientLightRingBuffer;->push(JF)V

    .line 130
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DualSensorPolicy;->updateAssistLightSensorAmbientLux(J)V

    .line 131
    return-void

    .line 123
    :cond_2
    :goto_0
    const-string v0, "DualSensorPolicy"

    const-string v1, "handleAssistantLightSensorEvent: drop assistant light sensor lux due to flash events or within one second of turning off the torch."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    return-void
.end method

.method private isSkipAmbientBrighteningThreshold(F)Z
    .locals 5
    .param p1, "currentAmbientLux"    # F

    .line 505
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAnimating()Z

    move-result v0

    .line 506
    .local v0, "isAnimating":Z
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 507
    return v1

    .line 509
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getAmbientLux()F

    move-result v2

    .line 510
    .local v2, "preAmbientLux":F
    iget-object v3, p0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isBrighteningDirection()Z

    move-result v3

    .line 511
    .local v3, "isBrighteningDirection":Z
    cmpl-float v4, p1, v2

    if-lez v4, :cond_1

    if-eqz v3, :cond_1

    .line 512
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSkipAmbientBrighteningThreshold: currentAmbientLux: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", preAmbientLux: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "DualSensorPolicy"

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const/4 v1, 0x1

    return v1

    .line 516
    :cond_1
    return v1
.end method

.method private isSkipAmbientDarkeningThreshold(F)Z
    .locals 5
    .param p1, "currentAmbientLux"    # F

    .line 520
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAnimating()Z

    move-result v0

    .line 521
    .local v0, "isAnimating":Z
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 522
    return v1

    .line 524
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getAmbientLux()F

    move-result v2

    .line 525
    .local v2, "preAmbientLux":F
    iget-object v3, p0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isBrighteningDirection()Z

    move-result v3

    .line 526
    .local v3, "isBrighteningDirection":Z
    cmpg-float v4, p1, v2

    if-gez v4, :cond_1

    if-nez v3, :cond_1

    .line 527
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSkipAmbientDarkeningThreshold: currentAmbientLux: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", preAmbientLux: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "DualSensorPolicy"

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const/4 v1, 0x1

    return v1

    .line 531
    :cond_1
    return v1
.end method

.method private setUpDebounceConfig(JJ)V
    .locals 1
    .param p1, "brighteningDebounce"    # J
    .param p3, "darkeningDebounce"    # J

    .line 163
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/AmbientLightRingBuffer;->setBrighteningDebounce(J)V

    .line 164
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, p3, p4}, Lcom/android/server/display/AmbientLightRingBuffer;->setDarkeningDebounce(J)V

    .line 165
    return-void
.end method

.method private updateAssistLightSensorAmbientLux()V
    .locals 5

    .line 168
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 169
    .local v0, "time":J
    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I

    int-to-long v3, v3

    sub-long v3, v0, v3

    invoke-virtual {v2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V

    .line 170
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/DualSensorPolicy;->updateAssistLightSensorAmbientLux(J)V

    .line 171
    return-void
.end method

.method private updateAssistLightSensorAmbientLux(J)V
    .locals 5
    .param p1, "time"    # J

    .line 174
    iget-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z

    if-nez v0, :cond_1

    .line 175
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorWarmUpTime:I

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnableTime:J

    add-long/2addr v0, v2

    .line 176
    .local v0, "timeWhenAssistSensorWarmedUp":J
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 179
    return-void

    .line 182
    :cond_0
    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonShort:I

    int-to-long v3, v3

    invoke-virtual {v2, p1, p2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F

    move-result v2

    iput v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    .line 183
    sget v2, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_ASSIST_LUX_EVENT:I

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {p0, v2, v3}, Lcom/android/server/display/DualSensorPolicy;->setAmbientLuxWhenInvalid(IF)V

    .line 184
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z

    .line 186
    .end local v0    # "timeWhenAssistSensorWarmedUp":J
    :cond_1
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_ASSIST_LUX_EVENT:I

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/display/DualSensorPolicy;->updateDualSensorPolicy(JI)Z

    .line 187
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 464
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 465
    const-string v0, "Dual Sensor Policy State:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 466
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mUseLightSensorFlag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistAmbientLuxValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistFastAmbientLux="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistSlowAmbientLux="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistBrighteningThreshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 471
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistDarkeningThreshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistLightSensorRingBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 473
    const-string v0, "Dual Sensor Policy Configuration:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistLightSensorBrighteningDebounce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorBrighteningDebounce:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mAssistLightSensorDarkeningDebounce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorDarkeningDebounce:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 476
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z

    sput-boolean v0, Lcom/android/server/display/DualSensorPolicy;->DEBUG:Z

    .line 477
    return-void
.end method

.method protected getAmbientLux(FFZ)F
    .locals 1
    .param p1, "preLux"    # F
    .param p2, "updateLux"    # F
    .param p3, "needUpdateLux"    # Z

    .line 214
    if-eqz p3, :cond_0

    move v0, p2

    goto :goto_0

    :cond_0
    move v0, p1

    :goto_0
    return v0
.end method

.method protected getAssistFastAmbientLux()F
    .locals 1

    .line 487
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    return v0
.end method

.method protected getAssistLightSensor()Landroid/hardware/Sensor;
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method protected getMainAmbientLux()F
    .locals 1

    .line 218
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    return v0
.end method

.method protected getMainFastAmbientLux()F
    .locals 1

    .line 501
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    return v0
.end method

.method setAmbientLuxWhenInvalid(IF)V
    .locals 5
    .param p1, "event"    # I
    .param p2, "lux"    # F

    .line 427
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_MAIN_LUX_EVENT:I

    const/4 v1, 0x1

    if-ne p1, v0, :cond_2

    .line 428
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v2, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    const/4 v3, 0x0

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v2, Lcom/android/server/display/AutomaticBrightnessControllerStub;->DUAL_SENSOR_LUX_INVALID:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v2, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    goto :goto_0

    .line 437
    :cond_0
    iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    .line 438
    iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    .line 439
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v0, p1, p2, v3, v3}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setAmbientLuxWhenInvalid: update brightness using assist light sensor in process, mMainFastAmbientLux: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DualSensorPolicy"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 432
    :cond_1
    :goto_0
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 433
    iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    .line 434
    iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    .line 435
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v0, p1, p2, v1, v3}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    goto :goto_1

    .line 442
    :cond_2
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_ASSIST_LUX_EVENT:I

    if-ne p1, v0, :cond_7

    .line 443
    iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    .line 444
    iput p2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    .line 445
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v0}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getBrighteningThreshold()F

    move-result v0

    .line 446
    .local v0, "mainBrighteningThreshold":F
    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->useDaemonSensorPolicyInProgress()Z

    move-result v2

    .line 447
    .local v2, "useDaemonSensorPolicy":Z
    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v4, Lcom/android/server/display/AutomaticBrightnessControllerStub;->DUAL_SENSOR_LUX_INVALID:I

    if-ne v3, v4, :cond_3

    if-nez v2, :cond_5

    :cond_3
    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v4, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpl-float v3, v3, v0

    if-gtz v3, :cond_5

    :cond_4
    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v4, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    if-ne v3, v4, :cond_6

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    iget v4, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_6

    iget v4, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6

    .line 454
    :cond_5
    sget v3, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    iput v3, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 456
    iget-object v3, p0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    xor-int/lit8 v4, v2, 0x1

    invoke-interface {v3, p1, p2, v1, v4}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 458
    :cond_6
    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v1, v3}, Lcom/android/server/display/HysteresisLevelsStub;->getBrighteningThreshold(F)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    .line 459
    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    iget v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v1, v3}, Lcom/android/server/display/HysteresisLevelsStub;->getDarkeningThreshold(F)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    .line 461
    .end local v0    # "mainBrighteningThreshold":F
    .end local v2    # "useDaemonSensorPolicy":Z
    :cond_7
    :goto_1
    return-void
.end method

.method setSceneDetector(Lcom/android/server/display/SceneDetector;)V
    .locals 0
    .param p1, "sceneDetector"    # Lcom/android/server/display/SceneDetector;

    .line 88
    iput-object p1, p0, Lcom/android/server/display/DualSensorPolicy;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    .line 89
    return-void
.end method

.method protected setSensorEnabled(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .line 134
    const-string v0, "DualSensorPolicy"

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z

    if-nez v1, :cond_0

    .line 135
    const-string/jumbo v1, "setSensorEnabled: register the assist light sensor."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z

    .line 137
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnableTime:J

    .line 138
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I

    iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I

    .line 139
    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensor:Landroid/hardware/Sensor;

    mul-int/lit16 v0, v0, 0x3e8

    iget-object v4, p0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    goto :goto_0

    .line 141
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z

    if-eqz v1, :cond_1

    .line 142
    const-string/jumbo v1, "setSensorEnabled: unregister the assist light sensor."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorEnable:Z

    .line 144
    iput-boolean v0, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistAmbientLuxValid:Z

    .line 145
    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v1}, Lcom/android/server/display/AmbientLightRingBuffer;->clear()V

    .line 146
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mCurrentAssistLightSensorRate:I

    .line 147
    sget v1, Lcom/android/server/display/AutomaticBrightnessControllerStub;->DUAL_SENSOR_LUX_INVALID:I

    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 148
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    .line 149
    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    .line 150
    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    .line 151
    iput v1, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    .line 152
    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 153
    iget-object v0, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/DualSensorPolicy;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 155
    :cond_1
    :goto_0
    return-void
.end method

.method protected updateBrightnessUsingMainLightSensor()Z
    .locals 2

    .line 237
    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v1, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected updateDualSensorPolicy(JI)Z
    .locals 26
    .param p1, "time"    # J
    .param p3, "event"    # I

    .line 274
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, p3

    sget v4, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_MAIN_LUX_EVENT:I

    const-string v5, ", mAssistSlowAmbientLux: "

    const-string v6, ", mMainSlowAmbientLux: "

    const-string v8, "DualSensorPolicy"

    if-ne v3, v4, :cond_12

    .line 275
    iget-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v4, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateNextBrighteningTransition(J)J

    move-result-wide v10

    .line 276
    .local v10, "nextBrightenTransition":J
    iget-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v4, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateNextDarkeningTransition(J)J

    move-result-wide v12

    .line 277
    .local v12, "nextDarkenTransition":J
    iget-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v4, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->calculateSlowAmbientLux(J)F

    move-result v4

    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    .line 278
    iget-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v4, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->calculateFastAmbientLux(J)F

    move-result v4

    iput v4, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    .line 279
    iget-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v4}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getBrighteningThreshold()F

    move-result v4

    .line 280
    .local v4, "mainBrighteningThreshold":F
    iget-object v14, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v14}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getDarkeningThreshold()F

    move-result v14

    .line 281
    .local v14, "mainDarkeningThreshold":F
    iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-direct {v0, v15}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientBrighteningThreshold(F)Z

    move-result v15

    .line 282
    .local v15, "skipBrighteningThreshold":Z
    iget v9, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-direct {v0, v9}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientDarkeningThreshold(F)Z

    move-result v9

    .line 283
    .local v9, "skipDarkeningThreshold":Z
    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    cmpl-float v16, v7, v4

    move-object/from16 v17, v5

    const-string v5, ", next darkening threshold of main light sensor: "

    move/from16 v18, v9

    .end local v9    # "skipDarkeningThreshold":Z
    .local v18, "skipDarkeningThreshold":Z
    const-string/jumbo v9, "updateDualSensorPolicy: next brightening threshold of main light sensor: "

    move-wide/from16 v19, v12

    .end local v12    # "nextDarkenTransition":J
    .local v19, "nextDarkenTransition":J
    const-string/jumbo v12, "updateDualSensorPolicy: update brightness using assist light sensor in process, mMainFastAmbientLux: "

    const-string v13, ", mainDarkeningThreshold: "

    move/from16 v21, v7

    const-string v7, ", mainBrighteningThreshold: "

    move-object/from16 v22, v5

    const-string/jumbo v5, "updateDualSensorPolicy: update ambient lux using main light sensor, mMainFastAmbientLux: "

    move-object/from16 v23, v9

    const-string v9, ", mAssistFastAmbientLux: "

    if-ltz v16, :cond_0

    move/from16 v16, v14

    .end local v14    # "mainDarkeningThreshold":F
    .local v16, "mainDarkeningThreshold":F
    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    cmpl-float v14, v14, v4

    if-ltz v14, :cond_1

    cmp-long v14, v10, v1

    if-lez v14, :cond_2

    goto :goto_0

    .end local v16    # "mainDarkeningThreshold":F
    .restart local v14    # "mainDarkeningThreshold":F
    :cond_0
    move/from16 v16, v14

    .end local v14    # "mainDarkeningThreshold":F
    .restart local v16    # "mainDarkeningThreshold":F
    :cond_1
    :goto_0
    if-eqz v15, :cond_6

    .line 286
    :cond_2
    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    move-wide/from16 v24, v10

    .end local v10    # "nextBrightenTransition":J
    .local v24, "nextBrightenTransition":J
    sget v10, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-eq v14, v10, :cond_4

    iget v10, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v11, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    if-ne v10, v11, :cond_3

    iget v10, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    iget v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpl-float v10, v10, v11

    if-ltz v10, :cond_3

    goto :goto_1

    .line 302
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v7, 0x0

    invoke-interface {v5, v3, v6, v7, v7}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    move/from16 v10, v16

    goto :goto_2

    .line 289
    :cond_4
    :goto_1
    sget v9, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    iput v9, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 290
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v9, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v10, v16

    .end local v16    # "mainDarkeningThreshold":F
    .local v10, "mainDarkeningThreshold":F
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 296
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v6, v7}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V

    goto :goto_2

    .line 298
    :cond_5
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v7, 0x1

    invoke-interface {v5, v3, v6, v7, v7}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 307
    :goto_2
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v5, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateNextBrighteningTransition(J)J

    move-result-wide v5

    .line 308
    .end local v24    # "nextBrightenTransition":J
    .local v5, "nextBrightenTransition":J
    iget-object v7, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v7, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateNextDarkeningTransition(J)J

    move-result-wide v12

    .line 309
    .end local v19    # "nextDarkenTransition":J
    .restart local v12    # "nextDarkenTransition":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v11, v23

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v9}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getBrighteningThreshold()F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v14, v22

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    .line 310
    invoke-interface {v9}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getDarkeningThreshold()F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 309
    invoke-static {v8, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v16, v15

    goto/16 :goto_5

    .line 311
    .end local v5    # "nextBrightenTransition":J
    .end local v12    # "nextDarkenTransition":J
    .local v10, "nextBrightenTransition":J
    .restart local v16    # "mainDarkeningThreshold":F
    .restart local v19    # "nextDarkenTransition":J
    :cond_6
    move-wide/from16 v24, v10

    move/from16 v10, v16

    move-object/from16 v14, v22

    move-object/from16 v11, v23

    .end local v16    # "mainDarkeningThreshold":F
    .local v10, "mainDarkeningThreshold":F
    .restart local v24    # "nextBrightenTransition":J
    cmpg-float v16, v21, v10

    if-gtz v16, :cond_7

    move/from16 v16, v15

    .end local v15    # "skipBrighteningThreshold":Z
    .local v16, "skipBrighteningThreshold":Z
    iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    cmpg-float v15, v15, v10

    if-gtz v15, :cond_8

    cmp-long v15, v19, v1

    if-lez v15, :cond_9

    goto :goto_3

    .end local v16    # "skipBrighteningThreshold":Z
    .restart local v15    # "skipBrighteningThreshold":Z
    :cond_7
    move/from16 v16, v15

    .end local v15    # "skipBrighteningThreshold":Z
    .restart local v16    # "skipBrighteningThreshold":Z
    :cond_8
    :goto_3
    if-eqz v18, :cond_f

    .line 314
    :cond_9
    iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    move-object/from16 v22, v14

    sget v14, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-eq v15, v14, :cond_a

    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v15, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    if-ne v14, v15, :cond_c

    :cond_a
    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpl-float v14, v14, v15

    if-ltz v14, :cond_c

    .line 317
    sget v9, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    iput v9, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 318
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v9, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 324
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v7, 0x1

    invoke-virtual {v5, v3, v6, v7}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V

    goto/16 :goto_4

    .line 326
    :cond_b
    const/4 v7, 0x1

    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-interface {v5, v3, v6, v7, v7}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    goto/16 :goto_4

    .line 328
    :cond_c
    iget v5, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v7, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-ne v5, v7, :cond_e

    .line 329
    sget v5, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    iput v5, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 330
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v5, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getBrighteningThreshold(F)F

    move-result v5

    iput v5, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    .line 331
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v5, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getDarkeningThreshold(F)F

    move-result v5

    iput v5, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    .line 332
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDualSensorPolicy: switch assist light sensor for lux update from main light sensor, mMainFastAmbientLux: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v7, v17

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v5}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 338
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v6, v7}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V

    goto :goto_4

    .line 340
    :cond_d
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    const/4 v7, 0x1

    invoke-interface {v5, v3, v6, v7, v7}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    goto :goto_4

    .line 343
    :cond_e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v7, 0x0

    invoke-interface {v5, v3, v6, v7, v7}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 348
    :goto_4
    iget-object v5, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v5, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateNextBrighteningTransition(J)J

    move-result-wide v5

    .line 349
    .end local v24    # "nextBrightenTransition":J
    .restart local v5    # "nextBrightenTransition":J
    iget-object v7, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v7, v1, v2}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateNextDarkeningTransition(J)J

    move-result-wide v12

    .line 350
    .end local v19    # "nextDarkenTransition":J
    .restart local v12    # "nextDarkenTransition":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v9}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getBrighteningThreshold()F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v9, v22

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    .line 351
    invoke-interface {v9}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getDarkeningThreshold()F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 350
    invoke-static {v8, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 311
    .end local v5    # "nextBrightenTransition":J
    .end local v12    # "nextDarkenTransition":J
    .restart local v19    # "nextDarkenTransition":J
    .restart local v24    # "nextBrightenTransition":J
    :cond_f
    move-wide/from16 v12, v19

    move-wide/from16 v5, v24

    .line 353
    .end local v19    # "nextDarkenTransition":J
    .end local v24    # "nextBrightenTransition":J
    .restart local v5    # "nextBrightenTransition":J
    .restart local v12    # "nextDarkenTransition":J
    :goto_5
    invoke-static {v12, v13, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    .line 354
    .local v14, "nextTransitionTime":J
    cmp-long v7, v14, v1

    if-lez v7, :cond_10

    move v9, v4

    move-wide/from16 v19, v5

    move-wide v4, v14

    goto :goto_6

    :cond_10
    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I

    move v9, v4

    move-wide/from16 v19, v5

    .end local v4    # "mainBrighteningThreshold":F
    .end local v5    # "nextBrightenTransition":J
    .local v9, "mainBrighteningThreshold":F
    .local v19, "nextBrightenTransition":J
    int-to-long v4, v7

    add-long/2addr v4, v1

    .line 355
    .end local v14    # "nextTransitionTime":J
    .local v4, "nextTransitionTime":J
    :goto_6
    sget-boolean v6, Lcom/android/server/display/DualSensorPolicy;->DEBUG:Z

    if-eqz v6, :cond_11

    .line 356
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDualSensorPolicy: next transition time of main light sensor: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 357
    invoke-static {v4, v5}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 356
    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_11
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v6, v4, v5}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->sendUpdateAmbientLuxMessage(J)V

    .end local v4    # "nextTransitionTime":J
    .end local v9    # "mainBrighteningThreshold":F
    .end local v10    # "mainDarkeningThreshold":F
    .end local v12    # "nextDarkenTransition":J
    .end local v16    # "skipBrighteningThreshold":Z
    .end local v18    # "skipDarkeningThreshold":Z
    .end local v19    # "nextBrightenTransition":J
    goto/16 :goto_a

    .line 360
    :cond_12
    move-object v7, v5

    sget v4, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_ASSIST_LUX_EVENT:I

    if-ne v3, v4, :cond_20

    .line 361
    iget-object v4, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v5, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    invoke-virtual {v4, v1, v2, v5}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightBrighteningTransition(JF)J

    move-result-wide v4

    .line 362
    .local v4, "nextBrightenTransition":J
    iget-object v9, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v10, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    invoke-virtual {v9, v1, v2, v10}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightDarkeningTransition(JF)J

    move-result-wide v9

    .line 363
    .local v9, "nextDarkenTransition":J
    iget-object v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v12, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonLong:I

    int-to-long v12, v12

    invoke-virtual {v11, v1, v2, v12, v13}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F

    move-result v11

    iput v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    .line 364
    iget-object v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v12, v0, Lcom/android/server/display/DualSensorPolicy;->mAmbientLightHorizonShort:I

    int-to-long v12, v12

    invoke-virtual {v11, v1, v2, v12, v13}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F

    move-result v11

    iput v11, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    .line 365
    iget-object v11, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    invoke-interface {v11}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->getBrighteningThreshold()F

    move-result v11

    .line 366
    .local v11, "mainBrighteningThreshold":F
    iget v12, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-direct {v0, v12}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientBrighteningThreshold(F)Z

    move-result v12

    .line 367
    .local v12, "skipBrighteningThreshold":Z
    iget v13, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-direct {v0, v13}, Lcom/android/server/display/DualSensorPolicy;->isSkipAmbientDarkeningThreshold(F)Z

    move-result v13

    .line 368
    .local v13, "skipDarkeningThreshold":Z
    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    iget v15, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    cmpl-float v16, v14, v15

    if-ltz v16, :cond_13

    move-object/from16 v16, v6

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpl-float v6, v6, v15

    if-ltz v6, :cond_14

    cmp-long v6, v4, v1

    if-lez v6, :cond_16

    goto :goto_7

    :cond_13
    move-object/from16 v16, v6

    :cond_14
    :goto_7
    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    cmpg-float v14, v14, v6

    if-gtz v14, :cond_15

    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpg-float v6, v14, v6

    if-gtz v6, :cond_15

    cmp-long v6, v9, v1

    if-lez v6, :cond_16

    :cond_15
    if-nez v12, :cond_16

    if-eqz v13, :cond_1d

    .line 374
    :cond_16
    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v14, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-ne v6, v14, :cond_17

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    cmpl-float v6, v6, v11

    if-lez v6, :cond_17

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    cmpl-float v6, v6, v11

    if-gtz v6, :cond_18

    :cond_17
    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v14, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    if-ne v6, v14, :cond_1a

    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    cmpl-float v6, v6, v14

    if-lez v6, :cond_1a

    .line 379
    :cond_18
    sget v6, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 380
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "updateDualSensorPolicy: update ambient lux using assist light sensor, mAssistFastAmbientLux: "

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mAssistBrighteningThreshold: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mAssistDarkeningThreshold: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v6}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 386
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    const/4 v14, 0x0

    invoke-virtual {v6, v3, v7, v14}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V

    goto :goto_8

    .line 388
    :cond_19
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    const/4 v14, 0x1

    invoke-interface {v6, v3, v7, v14, v14}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    goto :goto_8

    .line 390
    :cond_1a
    iget v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v14, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_ASSIST_LIGHT_SENSOR:I

    if-ne v6, v14, :cond_1c

    .line 391
    sget v6, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 392
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "updateDualSensorPolicy: switch main light sensor for lux update from assist light sensor, mAssistFastAmbientLux: "

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistSlowAmbientLux:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mMainFastAmbientLux: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v7, v16

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mBrightnessControllerImpl:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-virtual {v6}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->isAonFlareEnabled()Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 398
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v14, 0x0

    invoke-virtual {v6, v3, v7, v14}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V

    goto :goto_8

    .line 400
    :cond_1b
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mListener:Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    const/4 v14, 0x1

    invoke-interface {v6, v3, v7, v14, v14}, Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;->updateAmbientLux(IFZZ)V

    .line 403
    :cond_1c
    :goto_8
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v6, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getBrighteningThreshold(F)F

    move-result v6

    iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    .line 404
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsStub;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistFastAmbientLux:F

    invoke-virtual {v6, v7}, Lcom/android/server/display/HysteresisLevelsStub;->getDarkeningThreshold(F)F

    move-result v6

    iput v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    .line 405
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    invoke-virtual {v6, v1, v2, v7}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightBrighteningTransition(JF)J

    move-result-wide v4

    .line 406
    iget-object v6, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistLightSensorRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    invoke-virtual {v6, v1, v2, v7}, Lcom/android/server/display/AmbientLightRingBuffer;->nextAmbientLightDarkeningTransition(JF)J

    move-result-wide v9

    .line 407
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDualSensorPolicy: next brightening threshold of assist light sensor: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistBrighteningThreshold:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", next darkening threshold of assist light sensor: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/display/DualSensorPolicy;->mAssistDarkeningThreshold:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_1d
    invoke-static {v9, v10, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 411
    .local v6, "nextTransitionTime":J
    cmp-long v14, v6, v1

    if-lez v14, :cond_1e

    move-wide v14, v6

    goto :goto_9

    :cond_1e
    iget v14, v0, Lcom/android/server/display/DualSensorPolicy;->mLightSensorRate:I

    int-to-long v14, v14

    add-long/2addr v14, v1

    :goto_9
    move-wide v6, v14

    .line 412
    sget-boolean v14, Lcom/android/server/display/DualSensorPolicy;->DEBUG:Z

    if-eqz v14, :cond_1f

    .line 413
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "updateDualSensorPolicy: next transition time of assist light sensor: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 414
    invoke-static {v6, v7}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 413
    invoke-static {v8, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_1f
    iget-object v8, v0, Lcom/android/server/display/DualSensorPolicy;->mPolicyHandler:Landroid/os/Handler;

    const/4 v14, 0x0

    invoke-virtual {v8, v14, v6, v7}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_b

    .line 360
    .end local v4    # "nextBrightenTransition":J
    .end local v6    # "nextTransitionTime":J
    .end local v9    # "nextDarkenTransition":J
    .end local v11    # "mainBrighteningThreshold":F
    .end local v12    # "skipBrighteningThreshold":Z
    .end local v13    # "skipDarkeningThreshold":Z
    :cond_20
    :goto_a
    nop

    .line 418
    :goto_b
    const/4 v4, 0x1

    return v4
.end method

.method protected updateMainLightSensorAmbientThreshold(I)Z
    .locals 2
    .param p1, "event"    # I

    .line 227
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_MAIN_LUX_EVENT:I

    if-eq p1, v0, :cond_1

    iget v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    sget v1, Lcom/android/server/display/AutomaticBrightnessControllerStub;->USE_MAIN_LIGHT_SENSOR:I

    if-ne v0, v1, :cond_0

    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_ASSIST_LUX_EVENT:I

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method protected updateMainLuxStatus(F)V
    .locals 1
    .param p1, "currentLux"    # F

    .line 491
    sget v0, Lcom/android/server/display/AutomaticBrightnessControllerStub;->HANDLE_MAIN_LUX_EVENT:I

    iput v0, p0, Lcom/android/server/display/DualSensorPolicy;->mUseLightSensorFlag:I

    .line 492
    iput p1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainSlowAmbientLux:F

    .line 493
    iput p1, p0, Lcom/android/server/display/DualSensorPolicy;->mMainFastAmbientLux:F

    .line 494
    return-void
.end method
