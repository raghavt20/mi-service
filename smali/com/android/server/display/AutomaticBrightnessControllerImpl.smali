.class public Lcom/android/server/display/AutomaticBrightnessControllerImpl;
.super Lcom/android/server/display/AutomaticBrightnessControllerStub;
.source "AutomaticBrightnessControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;,
        Lcom/android/server/display/AutomaticBrightnessControllerImpl$CloudControllerListener;
    }
.end annotation


# static fields
.field private static final BRIGHTENING:I = 0x1

.field private static final CAMERA_ROLE_IDS:[Ljava/lang/String;

.field private static final DARKENING:I = 0x0

.field private static final DO_NOT_REDUCE_BRIGHTNESS_INTERVAL:I = 0xea60

.field private static final IS_LIGHT_FOV_OPTIMIZATION_POLICY_ENABLE:Z

.field private static final MSG_RESET_FAST_RATE:I = 0x2

.field private static final MSG_UPDATE_OUT_PACKET_TIME:I = 0x1

.field private static final NON_UI_FAST_UPDATE_BRIGHTNESS_TIME:J = 0x7d0L

.field private static final NON_UI_NOT_IN_POCKET:F = 0.0f

.field private static final PARALLEL_VIRTUAL_ROLE_ID:I = 0x66

.field private static final SENSOR_TYPE_ASSIST:I = 0x1fa266f

.field private static final SENSOR_TYPE_LIGHT_FOV:I = 0x1fa2a8f

.field private static final SENSOR_TYPE_NON_UI:I = 0x1fa2653

.field private static final TAG:Ljava/lang/String; = "AutomaticBrightnessControllerImpl"

.field private static final TORCH_CLOSE_DELAY:I = 0x708

.field private static final TYPICAL_PROXIMITY_THRESHOLD:F = 5.0f

.field private static final VIRTUAL_BACK_ROLE_ID:I = 0x64

.field private static final VIRTUAL_FRONT_ROLE_ID:I = 0x65


# instance fields
.field private mAllowFastRateRatio:F

.field private mAllowFastRateTime:I

.field private mAllowFastRateValue:F

.field private mAmbientLux:F

.field private mAmbientLuxDirection:I

.field private mAppliedDimming:Z

.field private mApplyingFastRate:Z

.field private mAutomaticBrightnessController:Lcom/android/server/display/AutomaticBrightnessController;

.field private mAutomaticBrightnessEnable:Z

.field private mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

.field private mCameraManager:Landroid/hardware/camera2/CameraManager;

.field private mContext:Landroid/content/Context;

.field private mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

.field private mDaemonSensorPolicyEnabled:Z

.field private mDebug:Z

.field private mDisableResetShortTermModel:Z

.field private mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

.field private mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

.field private mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

.field private mEnterGameTime:J

.field private mExitFacingCameraIds:[Ljava/lang/String;

.field private mFrontFlashAvailable:Z

.field private mHandler:Landroid/os/Handler;

.field private mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsImpl;

.field private mIsAnimatePolicyDisable:Z

.field private mIsGameSceneEnable:Z

.field private mIsTorchOpen:Z

.field private mLastBrightness:F

.field private mLightFovSensor:Landroid/hardware/Sensor;

.field private mLightSensorEnableTime:J

.field private mNeedUseFastRateBrightness:F

.field private mNonUiSensor:Landroid/hardware/Sensor;

.field private mNonUiSensorData:F

.field private mNonUiSensorEnabled:Z

.field private mNotInPocketTime:J

.field private mPendingUseFastRateDueToFirstAutoBrightness:Z

.field private mProximityPositive:Z

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mProximitySensorEnabled:Z

.field private mProximityThreshold:F

.field private mResetFastRateTime:I

.field private mSceneDetector:Lcom/android/server/display/SceneDetector;

.field private mSensorListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSkipTransitionLuxValue:F

.field private mSlowChange:Z

.field private mStartBrightness:F

.field private mStartSdrBrightness:F

.field private mState:I

.field private final mTorchCallback:Landroid/hardware/camera2/CameraManager$TorchCallback;

.field private mTorchCloseTime:J

.field private mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

.field private mUseAonFlareEnabled:Z

.field private mUseAssistSensorEnabled:Z

.field private mUseFastRateForVirtualSensor:Z

.field private mUseNonUiEnabled:Z

.field private mUseProximityEnabled:Z


# direct methods
.method public static synthetic $r8$lambda$5WxsJfFSpJVKPfZQVr86vmYf3Jk(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->lambda$stop$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmApplyingFastRate(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCameraManager(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Landroid/hardware/camera2/CameraManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmExitFacingCameraIds(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mExitFacingCameraIds:[Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrontFlashAvailable(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mFrontFlashAvailable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmApplyingFastRate(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFrontFlashAvailable(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mFrontFlashAvailable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsTorchOpen(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmNeedUseFastRateBrightness(Lcom/android/server/display/AutomaticBrightnessControllerImpl;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmNotInPocketTime(Lcom/android/server/display/AutomaticBrightnessControllerImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTorchCloseTime(Lcom/android/server/display/AutomaticBrightnessControllerImpl;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCloseTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$monNonUiSensorChanged(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/SensorEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->onNonUiSensorChanged(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monProximitySensorChanged(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/SensorEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->onProximitySensorChanged(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetCAMERA_ROLE_IDS()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->CAMERA_ROLE_IDS:[Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 37
    nop

    .line 38
    const-string v0, "ro.miui.support.light.fov"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->IS_LIGHT_FOV_OPTIMIZATION_POLICY_ENABLE:Z

    .line 45
    const-string v0, "com.xiaomi.cameraid.role.cameraIds"

    const-string v1, "com.xiaomi.cameraid.role.cameraId"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->CAMERA_ROLE_IDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 30
    invoke-direct {p0}, Lcom/android/server/display/AutomaticBrightnessControllerStub;-><init>()V

    .line 94
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartBrightness:F

    .line 95
    iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartSdrBrightness:F

    .line 107
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I

    .line 256
    new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$1;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V

    iput-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCallback:Landroid/hardware/camera2/CameraManager$TorchCallback;

    .line 382
    new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V

    iput-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorListener:Landroid/hardware/SensorEventListener;

    return-void
.end method

.method private convertToBrightness(F)F
    .locals 1
    .param p1, "nit"    # F

    .line 576
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToBrightness(F)F

    move-result v0

    return v0

    .line 579
    :cond_0
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method private convertToNit(F)F
    .locals 1
    .param p1, "brightness"    # F

    .line 569
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    if-eqz v0, :cond_0

    .line 570
    invoke-virtual {v0, p1}, Lcom/android/server/display/BrightnessMappingStrategy;->convertToNits(F)F

    move-result v0

    return v0

    .line 572
    :cond_0
    const/high16 v0, 0x7fc00000    # Float.NaN

    return v0
.end method

.method private synthetic lambda$stop$0()V
    .locals 1

    .line 640
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-virtual {v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->stop()V

    .line 641
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setRotationListenerEnable(Z)V

    .line 642
    return-void
.end method

.method private loadConfiguration()V
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 182
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x11050013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z

    .line 183
    const v1, 0x110b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateTime:I

    .line 185
    const v1, 0x110b0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mResetFastRateTime:I

    .line 187
    const v1, 0x1107001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateRatio:F

    .line 188
    const v1, 0x1107001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateValue:F

    .line 189
    const v1, 0x11050012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z

    .line 190
    const v1, 0x11050010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    .line 193
    const v1, 0x11050011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z

    .line 195
    const v1, 0x11070034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSkipTransitionLuxValue:F

    .line 197
    nop

    .line 198
    const-string/jumbo v1, "use_daemon_sensor_policy"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicyEnabled:Z

    .line 199
    const v1, 0x11050089

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseFastRateForVirtualSensor:Z

    .line 201
    const v1, 0x1103003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mExitFacingCameraIds:[Ljava/lang/String;

    .line 203
    return-void
.end method

.method private onNonUiSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 411
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorData:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v1

    iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorData:F

    .line 414
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 419
    :cond_0
    return-void
.end method

.method private onProximitySensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 404
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z

    if-eqz v0, :cond_1

    .line 405
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 406
    .local v0, "distance":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityThreshold:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityPositive:Z

    .line 408
    .end local v0    # "distance":F
    :cond_1
    return-void
.end method

.method private setNonUiSensorEnabled(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .line 367
    const-string v0, "AutomaticBrightnessControllerImpl"

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_0

    .line 368
    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z

    .line 369
    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 371
    const-string/jumbo v1, "setNonUiSensorEnabled enable"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 372
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z

    if-eqz v2, :cond_1

    .line 373
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorEnabled:Z

    .line 374
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J

    .line 375
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensorData:F

    .line 376
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 377
    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 378
    const-string/jumbo v1, "setNonUiSensorEnabled disable"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_1
    :goto_0
    return-void
.end method

.method private setProximitySensorEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 350
    const-string v0, "AutomaticBrightnessControllerImpl"

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z

    if-nez v1, :cond_0

    .line 353
    const-string/jumbo v1, "setProximitySensorEnabled enable"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z

    .line 355
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 357
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z

    if-eqz v1, :cond_1

    .line 360
    const-string/jumbo v1, "setProximitySensorEnabled disable"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensorEnabled:Z

    .line 362
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 364
    :cond_1
    :goto_0
    return-void
.end method

.method private setRotationListenerEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 778
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 779
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->registerRotationWatcher(Z)V

    .line 781
    :cond_0
    return-void
.end method

.method private setSensorEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 338
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z

    if-eqz v0, :cond_0

    .line 339
    invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setProximitySensorEnabled(Z)V

    .line 341
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/display/DualSensorPolicy;->setSensorEnabled(Z)V

    .line 344
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z

    if-eqz v0, :cond_2

    .line 345
    invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setNonUiSensorEnabled(Z)V

    .line 347
    :cond_2
    return-void
.end method

.method private setUpDisplayDeviceConfig(Lcom/android/server/display/DisplayDeviceConfig;)V
    .locals 0
    .param p1, "deviceConfig"    # Lcom/android/server/display/DisplayDeviceConfig;

    .line 215
    iput-object p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayDeviceConfig:Lcom/android/server/display/DisplayDeviceConfig;

    .line 216
    return-void
.end method

.method private setUpLogicalDisplay(Lcom/android/server/display/LogicalDisplay;)V
    .locals 1
    .param p1, "logicalDisplay"    # Lcom/android/server/display/LogicalDisplay;

    .line 219
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {v0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->setUpLogicalDisplay(Lcom/android/server/display/LogicalDisplay;)V

    .line 222
    :cond_0
    return-void
.end method

.method private updateCbmState(Z)V
    .locals 1
    .param p1, "autoBrightnessEnabled"    # Z

    .line 851
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 852
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCbmState(Z)V

    .line 854
    :cond_0
    return-void
.end method


# virtual methods
.method protected checkAssistSensorValid()Z
    .locals 4

    .line 455
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z

    if-nez v0, :cond_0

    .line 457
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCloseTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x708

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 458
    const/4 v0, 0x1

    return v0

    .line 460
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z

    if-eqz v0, :cond_1

    .line 461
    const-string v0, "AutomaticBrightnessControllerImpl"

    const-string v1, "drop assist light data due to within 1s of turning off the torch."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method protected checkFastRateStatus()Z
    .locals 6

    .line 509
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 510
    .local v0, "currentTime":J
    iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightSensorEnableTime:J

    iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateTime:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNotInPocketTime:J

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2
.end method

.method public configure(IFI)V
    .locals 5
    .param p1, "state"    # I
    .param p2, "screenAutoBrightness"    # F
    .param p3, "displayPolicy"    # I

    .line 226
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    if-eq p3, v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    .line 228
    .local v2, "enable":Z
    :goto_0
    invoke-direct {p0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setSensorEnabled(Z)V

    .line 230
    iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicyEnabled:Z

    if-eqz v3, :cond_1

    .line 231
    iget-object v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-virtual {v3, p1, p3}, Lcom/android/server/display/DaemonSensorPolicy;->notifyRegisterDaemonLightSensor(II)V

    .line 233
    :cond_1
    iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    if-eqz v3, :cond_2

    .line 234
    iget-object v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    invoke-virtual {v3, v2}, Lcom/android/server/display/SceneDetector;->configure(Z)V

    .line 236
    :cond_2
    iget-object v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-virtual {v3, v2}, Lcom/android/server/display/TouchCoverProtectionHelper;->configure(Z)V

    .line 237
    invoke-direct {p0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setRotationListenerEnable(Z)V

    .line 239
    const/4 v3, 0x2

    if-nez v2, :cond_3

    iget-boolean v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z

    if-eqz v4, :cond_3

    .line 240
    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z

    .line 241
    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    .line 242
    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z

    .line 243
    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 244
    invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->updateCbmState(Z)V

    goto :goto_1

    .line 245
    :cond_3
    if-eqz v2, :cond_5

    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z

    if-nez v0, :cond_5

    .line 246
    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessEnable:Z

    .line 247
    iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mState:I

    .line 249
    .local v0, "oldState":I
    if-eq v0, v3, :cond_4

    iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseFastRateForVirtualSensor:Z

    if-eqz v3, :cond_5

    .line 250
    :cond_4
    iput-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z

    .line 253
    .end local v0    # "oldState":I
    :cond_5
    :goto_1
    iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mState:I

    .line 254
    return-void
.end method

.method public dropAmbientLuxIfNeeded()Z
    .locals 3

    .line 423
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-virtual {v0}, Lcom/android/server/display/TouchCoverProtectionHelper;->isTouchCoverProtectionActive()Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "AutomaticBrightnessControllerImpl"

    if-eqz v0, :cond_0

    .line 424
    const-string v0, "drop the ambient lux due to touch events."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    return v1

    .line 426
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityPositive:Z

    if-eqz v0, :cond_1

    .line 427
    const-string v0, "drop the ambient lux due to proximity events."

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    return v1

    .line 430
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public dropDecreaseLuxIfNeeded()Z
    .locals 6

    .line 439
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 440
    .local v0, "now":J
    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsGameSceneEnable:Z

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mEnterGameTime:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    .line 442
    invoke-virtual {v2}, Lcom/android/server/display/TouchCoverProtectionHelper;->isGameSceneWithinTouchTime()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 443
    :cond_0
    const-string v2, "AutomaticBrightnessControllerImpl"

    const-string v3, "drop the ambient lux due to game scene enable."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v2, 0x1

    return v2

    .line 446
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mUseProximityEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLightFovSensor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightFovSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mUseNonUiEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mUseAonFlareEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIsGameSceneEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsGameSceneEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->dump(Ljava/io/PrintWriter;)V

    .line 590
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/display/DaemonSensorPolicy;->dump(Ljava/io/PrintWriter;)V

    .line 591
    invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->supportDualSensorPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/display/DualSensorPolicy;->dump(Ljava/io/PrintWriter;)V

    .line 594
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    invoke-virtual {v0, p1}, Lcom/android/server/display/SceneDetector;->dump(Ljava/io/PrintWriter;)V

    .line 597
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/display/HysteresisLevelsImpl;->dump(Ljava/io/PrintWriter;)V

    .line 598
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_ABC:Z

    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z

    .line 599
    return-void
.end method

.method public fillInLuxFromDaemonSensor()Landroid/util/SparseArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 606
    new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$3;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V

    .line 613
    .local v0, "daemonSensorArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Float;>;"
    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicyEnabled:Z

    if-nez v1, :cond_0

    .line 614
    return-object v0

    .line 617
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-virtual {v1}, Lcom/android/server/display/DaemonSensorPolicy;->getMainLightSensorLux()F

    move-result v1

    .line 618
    .local v1, "mainLux":F
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    const v3, 0x1fa266f

    invoke-virtual {v2, v3}, Lcom/android/server/display/DaemonSensorPolicy;->getDaemonSensorValue(I)F

    move-result v2

    .line 620
    .local v2, "assistLux":F
    iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkAssistSensorValid()Z

    move-result v3

    if-eqz v3, :cond_1

    cmpl-float v3, v2, v1

    if-lez v3, :cond_1

    .line 621
    sget v3, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->HANDLE_ASSIST_LUX_EVENT:I

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 623
    :cond_1
    sget v3, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->HANDLE_MAIN_LUX_EVENT:I

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 626
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z

    if-eqz v3, :cond_2

    .line 627
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fillInLuxFromDaemonSensor: mainLux: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", assistLux: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AutomaticBrightnessControllerImpl"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    :cond_2
    return-object v0
.end method

.method public getAmbientLux(IFFZ)F
    .locals 1
    .param p1, "event"    # I
    .param p2, "preLux"    # F
    .param p3, "updateLux"    # F
    .param p4, "needUpdateLux"    # Z

    .line 667
    if-eqz p4, :cond_0

    .line 668
    iput p3, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLux:F

    .line 670
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p2, p3, p4}, Lcom/android/server/display/DualSensorPolicy;->getAmbientLux(FFZ)F

    move-result v0

    return v0
.end method

.method protected getAssistFastAmbientLux()F
    .locals 1

    .line 774
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getAssistFastAmbientLux()F

    move-result v0

    return v0
.end method

.method public getCurrentAmbientLux()F
    .locals 1

    .line 690
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessController:Lcom/android/server/display/AutomaticBrightnessController;

    if-eqz v0, :cond_0

    .line 691
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessController;->getAmbientLux()F

    move-result v0

    return v0

    .line 693
    :cond_0
    iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLux:F

    return v0
.end method

.method public getCustomBrightness(FLjava/lang/String;IFFZ)F
    .locals 7
    .param p1, "lux"    # F
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "category"    # I
    .param p4, "oldAutoBrightness"    # F
    .param p5, "newAutoBrightness"    # F
    .param p6, "isManuallySet"    # Z

    .line 791
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 792
    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/server/display/DisplayPowerControllerImpl;->getCustomBrightness(FLjava/lang/String;IFFZ)F

    move-result p5

    .line 795
    :cond_0
    return p5
.end method

.method public getHysteresisLevelsImpl()Lcom/android/server/display/HysteresisLevelsStub;
    .locals 1

    .line 834
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsImpl;

    return-object v0
.end method

.method protected getIsTorchOpen()Z
    .locals 1

    .line 646
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsTorchOpen:Z

    return v0
.end method

.method public getMainAmbientLux()F
    .locals 1

    .line 697
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getMainAmbientLux()F

    move-result v0

    return v0
.end method

.method protected getMainFastAmbientLux()F
    .locals 1

    .line 770
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getMainFastAmbientLux()F

    move-result v0

    return v0
.end method

.method public initialize(Landroid/hardware/SensorManager;Landroid/content/Context;Landroid/os/Looper;Landroid/hardware/Sensor;IIJJIILcom/android/server/display/HysteresisLevelsStub;Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessController;)V
    .locals 16
    .param p1, "sensorManager"    # Landroid/hardware/SensorManager;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "looper"    # Landroid/os/Looper;
    .param p4, "lightSensor"    # Landroid/hardware/Sensor;
    .param p5, "lightSensorWarmUpTime"    # I
    .param p6, "lightSensorRate"    # I
    .param p7, "brighteningLightDebounceConfig"    # J
    .param p9, "darkeningLightDebounceConfig"    # J
    .param p11, "ambientLightHorizonLong"    # I
    .param p12, "ambientLightHorizonShort"    # I
    .param p13, "hysteresisLevelsImpl"    # Lcom/android/server/display/HysteresisLevelsStub;
    .param p14, "listener"    # Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;
    .param p15, "controller"    # Lcom/android/server/display/AutomaticBrightnessController;

    .line 142
    move-object/from16 v14, p0

    move-object/from16 v15, p2

    move-object/from16 v13, p3

    iput-object v15, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mContext:Landroid/content/Context;

    .line 143
    move-object/from16 v12, p1

    iput-object v12, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    .line 144
    move-object/from16 v11, p15

    iput-object v11, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessController:Lcom/android/server/display/AutomaticBrightnessController;

    .line 145
    invoke-direct/range {p0 .. p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->loadConfiguration()V

    .line 147
    new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;

    invoke-direct {v0, v14, v13}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/os/Looper;)V

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    .line 148
    new-instance v6, Lcom/android/server/display/DaemonSensorPolicy;

    iget-object v1, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mContext:Landroid/content/Context;

    move-object v0, v6

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p0

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/DaemonSensorPolicy;-><init>(Landroid/content/Context;Landroid/hardware/SensorManager;Landroid/os/Looper;Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/Sensor;)V

    iput-object v6, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    .line 149
    new-instance v0, Lcom/android/server/display/TouchCoverProtectionHelper;

    iget-object v1, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v13}, Lcom/android/server/display/TouchCoverProtectionHelper;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    .line 150
    new-instance v10, Lcom/android/server/display/DualSensorPolicy;

    move-object v0, v10

    move-object/from16 v1, p3

    move/from16 v3, p5

    move/from16 v4, p6

    move-wide/from16 v5, p7

    move-wide/from16 v7, p9

    move/from16 v9, p11

    move-object v15, v10

    move/from16 v10, p12

    move-object/from16 v11, p13

    move-object/from16 v12, p14

    move-object/from16 v13, p0

    invoke-direct/range {v0 .. v13}, Lcom/android/server/display/DualSensorPolicy;-><init>(Landroid/os/Looper;Landroid/hardware/SensorManager;IIJJIILcom/android/server/display/HysteresisLevelsStub;Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V

    iput-object v15, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    .line 154
    iget-boolean v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Lcom/android/server/display/SceneDetector;

    iget-object v1, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    move-object/from16 v2, p2

    move-object/from16 v3, p14

    invoke-direct {v0, v3, v14, v1, v2}, Lcom/android/server/display/SceneDetector;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerStub$DualSensorPolicyListener;Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    .line 156
    iget-object v1, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v1, v0}, Lcom/android/server/display/DualSensorPolicy;->setSceneDetector(Lcom/android/server/display/SceneDetector;)V

    goto :goto_0

    .line 154
    :cond_0
    move-object/from16 v2, p2

    move-object/from16 v3, p14

    .line 158
    :goto_0
    iget-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa2a8f

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightFovSensor:Landroid/hardware/Sensor;

    .line 159
    move-object/from16 v0, p13

    check-cast v0, Lcom/android/server/display/HysteresisLevelsImpl;

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHysteresisLevelsImpl:Lcom/android/server/display/HysteresisLevelsImpl;

    .line 161
    iget-boolean v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseProximityEnabled:Z

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximitySensor:Landroid/hardware/Sensor;

    .line 163
    if-eqz v0, :cond_1

    .line 164
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v0

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mProximityThreshold:F

    .line 169
    :cond_1
    iget-boolean v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseNonUiEnabled:Z

    if-eqz v0, :cond_2

    .line 170
    iget-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa2653

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensor:Landroid/hardware/Sensor;

    .line 171
    if-nez v0, :cond_2

    .line 172
    iget-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNonUiSensor:Landroid/hardware/Sensor;

    .line 176
    :cond_2
    iget-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mContext:Landroid/content/Context;

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    .line 177
    iget-object v1, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCallback:Landroid/hardware/camera2/CameraManager$TorchCallback;

    iget-object v4, v14, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v4}, Landroid/hardware/camera2/CameraManager;->registerTorchCallback(Landroid/hardware/camera2/CameraManager$TorchCallback;Landroid/os/Handler;)V

    .line 178
    return-void
.end method

.method public isAnimating()Z
    .locals 3

    .line 823
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 824
    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsAnimatePolicyDisable:Z

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 826
    :cond_1
    return v1
.end method

.method protected isAonFlareEnabled()Z
    .locals 1

    .line 650
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    return v0
.end method

.method public isBrighteningDirection()Z
    .locals 2

    .line 819
    iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isDisableResetShortTermModel()Z
    .locals 2

    .line 763
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 764
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->resetShortTermModel(Z)V

    .line 766
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisableResetShortTermModel:Z

    return v0
.end method

.method public notifyAonFlareEvents(IF)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "preLux"    # F

    .line 857
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 858
    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyAonFlareEvents(IF)V

    .line 860
    :cond_0
    return-void
.end method

.method protected notifyDisableResetShortTermModel(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 755
    iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisableResetShortTermModel:Z

    .line 756
    return-void
.end method

.method public notifyUnregisterDaemonSensor()V
    .locals 2

    .line 727
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/display/DaemonSensorPolicy;->setDaemonLightSensorsEnabled(Z)V

    .line 728
    return-void
.end method

.method public notifyUpdateBrightness()V
    .locals 1

    .line 863
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 864
    invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyUpdateBrightness()V

    .line 866
    :cond_0
    return-void
.end method

.method protected notifyUpdateForegroundApp()V
    .locals 1

    .line 838
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessController:Lcom/android/server/display/AutomaticBrightnessController;

    if-eqz v0, :cond_0

    .line 839
    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessController;->updateForegroundAppWindowChanged()V

    .line 841
    :cond_0
    return-void
.end method

.method public setAmbientLuxWhenInvalid(IF)V
    .locals 1
    .param p1, "event"    # I
    .param p2, "lux"    # F

    .line 732
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DualSensorPolicy;->setAmbientLuxWhenInvalid(IF)V

    .line 733
    return-void
.end method

.method public setAnimationPolicyDisable(Z)V
    .locals 0
    .param p1, "isDisable"    # Z

    .line 830
    iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsAnimatePolicyDisable:Z

    .line 831
    return-void
.end method

.method public setScreenBrightnessByUser(FFLjava/lang/String;)V
    .locals 1
    .param p1, "lux"    # F
    .param p2, "brightness"    # F
    .param p3, "packageName"    # Ljava/lang/String;

    .line 800
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 801
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DisplayPowerControllerImpl;->setScreenBrightnessByUser(FFLjava/lang/String;)V

    .line 803
    :cond_0
    return-void
.end method

.method protected setUpAutoBrightness(Lcom/android/server/display/DisplayPowerControllerImpl;Lcom/android/server/display/BrightnessMappingStrategy;Lcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/LogicalDisplay;)V
    .locals 0
    .param p1, "dpcImpl"    # Lcom/android/server/display/DisplayPowerControllerImpl;
    .param p2, "brightnessMapper"    # Lcom/android/server/display/BrightnessMappingStrategy;
    .param p3, "deviceConfig"    # Lcom/android/server/display/DisplayDeviceConfig;
    .param p4, "logicalDisplay"    # Lcom/android/server/display/LogicalDisplay;

    .line 208
    iput-object p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 209
    iput-object p2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mBrightnessMapper:Lcom/android/server/display/BrightnessMappingStrategy;

    .line 210
    invoke-direct {p0, p3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setUpDisplayDeviceConfig(Lcom/android/server/display/DisplayDeviceConfig;)V

    .line 211
    invoke-direct {p0, p4}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setUpLogicalDisplay(Lcom/android/server/display/LogicalDisplay;)V

    .line 212
    return-void
.end method

.method public shouldSkipBrighteningTransition(JFFF)Z
    .locals 3
    .param p1, "sensorEnableTime"    # J
    .param p3, "currentLux"    # F
    .param p4, "ambientLux"    # F
    .param p5, "brighteningThreshold"    # F

    .line 487
    iput-wide p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightSensorEnableTime:J

    .line 488
    invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkFastRateStatus()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    cmpl-float v0, p3, p5

    if-ltz v0, :cond_3

    iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSkipTransitionLuxValue:F

    add-float/2addr v0, p4

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_3

    iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateRatio:F

    mul-float/2addr v0, p4

    add-float/2addr v0, p4

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_3

    .line 491
    invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->supportDualSensorPolicy()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->updateBrightnessUsingMainLightSensor()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    .line 493
    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getAssistFastAmbientLux()F

    move-result v0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_0

    .line 494
    return v1

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p3}, Lcom/android/server/display/DualSensorPolicy;->updateMainLuxStatus(F)V

    .line 498
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skip brightening transition, currentLux:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ambientLux:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AutomaticBrightnessControllerImpl"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAonFlareEnabled:Z

    if-eqz v0, :cond_2

    .line 500
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSceneDetector:Lcom/android/server/display/SceneDetector;

    sget v2, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->HANDLE_MAIN_LUX_EVENT:I

    invoke-virtual {v0, v2, p3, v1}, Lcom/android/server/display/SceneDetector;->updateAmbientLux(IFZ)V

    .line 501
    return v1

    .line 503
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 505
    :cond_3
    return v1
.end method

.method protected shouldUseFastRate(FF)Z
    .locals 7
    .param p1, "currBrightness"    # F
    .param p2, "tgtBrightness"    # F

    .line 516
    iget v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F

    invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->convertToNit(F)F

    move-result v0

    .line 517
    .local v0, "nit":F
    invoke-direct {p0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->convertToNit(F)F

    move-result v1

    .line 523
    .local v1, "currentNit":F
    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    const-string v3, "AutomaticBrightnessControllerImpl"

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->checkFastRateStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateRatio:F

    mul-float/2addr v2, v1

    add-float/2addr v2, v1

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAllowFastRateValue:F

    add-float/2addr v2, v1

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    .line 525
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    .line 526
    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F

    iput v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F

    .line 527
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 528
    iget-object v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    iget v5, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mResetFastRateTime:I

    int-to-long v5, v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 529
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Use fast rate due to large change in brightness, mLastBrightness:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", currBrightness:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F

    iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F

    cmpg-float v5, v2, v4

    if-ltz v5, :cond_1

    cmpl-float v2, v2, v4

    if-lez v2, :cond_2

    cmpl-float v2, p1, v4

    if-ltz v2, :cond_2

    .line 543
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "shouldUseFastRate: mLastBrightness: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", tgtBrightness: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", currBrightness: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", mNeedUseFastRateBrightness: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    .line 547
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F

    .line 549
    :cond_2
    iget-boolean v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    return v2
.end method

.method public showTouchCoverProtectionRect(Z)V
    .locals 1
    .param p1, "isShow"    # Z

    .line 655
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTouchAreaHelper:Lcom/android/server/display/TouchCoverProtectionHelper;

    if-eqz v0, :cond_0

    .line 656
    invoke-virtual {v0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper;->showTouchCoverProtectionRect(Z)V

    .line 658
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .line 634
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->setSensorEnabled(Z)V

    .line 635
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDaemonSensorPolicy:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DaemonSensorPolicy;->stop()V

    .line 636
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mCameraManager:Landroid/hardware/camera2/CameraManager;

    if-eqz v0, :cond_0

    .line 637
    iget-object v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mTorchCallback:Landroid/hardware/camera2/CameraManager$TorchCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->unregisterTorchCallback(Landroid/hardware/camera2/CameraManager$TorchCallback;)V

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/display/AutomaticBrightnessControllerImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 643
    return-void
.end method

.method public supportDualSensorPolicy()Z
    .locals 1

    .line 662
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mUseAssistSensorEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->getAssistLightSensor()Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public switchLightSensor(Landroid/hardware/Sensor;)Landroid/hardware/Sensor;
    .locals 1
    .param p1, "sensor"    # Landroid/hardware/Sensor;

    .line 748
    sget-boolean v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->IS_LIGHT_FOV_OPTIMIZATION_POLICY_ENABLE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLightFovSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    .line 749
    return-object v0

    .line 751
    :cond_0
    return-object p1
.end method

.method public update()V
    .locals 1

    .line 784
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAutomaticBrightnessController:Lcom/android/server/display/AutomaticBrightnessController;

    invoke-virtual {v0}, Lcom/android/server/display/AutomaticBrightnessController;->update()V

    .line 785
    return-void
.end method

.method public updateAmbientLuxDirection(ZFF)V
    .locals 1
    .param p1, "needUpdateBrightness"    # Z
    .param p2, "currentAmbientLux"    # F
    .param p3, "preAmbientLux"    # F

    .line 807
    if-nez p1, :cond_0

    .line 808
    return-void

    .line 810
    :cond_0
    cmpl-float v0, p2, p3

    if-lez v0, :cond_1

    .line 811
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I

    .line 813
    :cond_1
    cmpg-float v0, p2, p3

    if-gez v0, :cond_2

    .line 814
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAmbientLuxDirection:I

    .line 816
    :cond_2
    return-void
.end method

.method public updateBrightnessUsingMainLightSensor()Z
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0}, Lcom/android/server/display/DualSensorPolicy;->updateBrightnessUsingMainLightSensor()Z

    move-result v0

    return v0
.end method

.method public updateCustomSceneState(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 845
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    if-eqz v0, :cond_0

    .line 846
    invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateCustomSceneState(Ljava/lang/String;)V

    .line 848
    :cond_0
    return-void
.end method

.method public updateDualSensorPolicy(JI)Z
    .locals 1
    .param p1, "time"    # J
    .param p3, "event"    # I

    .line 685
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/DualSensorPolicy;->updateDualSensorPolicy(JI)Z

    move-result v0

    return v0
.end method

.method public updateFastRateStatus(F)V
    .locals 4
    .param p1, "brightness"    # F

    .line 472
    iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mLastBrightness:F

    .line 473
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z

    if-eqz v0, :cond_0

    .line 474
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mApplyingFastRate:Z

    .line 475
    iput p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mNeedUseFastRateBrightness:F

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mPendingUseFastRateDueToFirstAutoBrightness:Z

    .line 478
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 479
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mResetFastRateTime:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 480
    const-string v0, "AutomaticBrightnessControllerImpl"

    const-string v1, "Use fast rate due to first auto brightness."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_0
    return-void
.end method

.method protected updateGameSceneEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 450
    iput-boolean p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mIsGameSceneEnable:Z

    .line 451
    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mEnterGameTime:J

    :goto_0
    iput-wide v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mEnterGameTime:J

    .line 452
    return-void
.end method

.method public updateMainLightSensorAmbientThreshold(I)Z
    .locals 1
    .param p1, "event"    # I

    .line 675
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDualSensorPolicy:Lcom/android/server/display/DualSensorPolicy;

    invoke-virtual {v0, p1}, Lcom/android/server/display/DualSensorPolicy;->updateMainLightSensorAmbientThreshold(I)Z

    move-result v0

    return v0
.end method

.method public updateSlowChangeStatus(ZZZFF)V
    .locals 2
    .param p1, "slowChange"    # Z
    .param p2, "appliedDimming"    # Z
    .param p3, "appliedLowPower"    # Z
    .param p4, "startBrightness"    # F
    .param p5, "startSdrBrightness"    # F

    .line 555
    xor-int/lit8 v0, p2, 0x1

    and-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSlowChange:Z

    .line 556
    iput-boolean p2, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAppliedDimming:Z

    .line 557
    iput p4, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartBrightness:F

    .line 558
    iput p5, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartSdrBrightness:F

    .line 559
    iget-boolean v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mDebug:Z

    if-eqz v0, :cond_0

    .line 560
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateSlowChangeStatus: mSlowChange: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mSlowChange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appliedDimming: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mAppliedDimming:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appliedLowPower: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startBrightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mStartSdrBrightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->mStartSdrBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AutomaticBrightnessControllerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    :cond_0
    return-void
.end method
