.class public final Lcom/android/server/display/DisplayManagerServiceStub$$;
.super Ljava/lang/Object;
.source "DisplayManagerServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 19
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.AutomaticBrightnessControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/display/DisplayAdapterImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/DisplayAdapterImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.DisplayAdapterStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/display/DisplayManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/DisplayManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.DisplayManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/display/HysteresisLevelsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/HysteresisLevelsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.HysteresisLevelsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/display/BrightnessMappingStrategyImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/BrightnessMappingStrategyImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.BrightnessMappingStrategyStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.DisplayFeatureManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/android/server/display/DisplayPowerControllerImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/DisplayPowerControllerImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.DisplayPowerControllerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/android/server/display/LocalDisplayAdapterImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/LocalDisplayAdapterImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.LocalDisplayAdapterStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/android/server/display/MiuiBrightnessUtilsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/MiuiBrightnessUtilsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.MiuiBrightnessUtilsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/android/server/display/mode/DisplayModeDirectorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/mode/DisplayModeDirectorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.mode.DisplayModeDirectorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v0, Lcom/android/server/display/DisplayGroupImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/display/DisplayGroupImpl$Provider;-><init>()V

    const-string v1, "com.android.server.display.DisplayGroupStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    return-void
.end method
