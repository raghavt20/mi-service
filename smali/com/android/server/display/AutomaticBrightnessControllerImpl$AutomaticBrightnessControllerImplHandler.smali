.class final Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;
.super Landroid/os/Handler;
.source "AutomaticBrightnessControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/AutomaticBrightnessControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutomaticBrightnessControllerImplHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .line 701
    iput-object p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 702
    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p2, p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 703
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 707
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "AutomaticBrightnessControllerImpl"

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 713
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fgetmApplyingFastRate(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmApplyingFastRate(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Z)V

    .line 715
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmNeedUseFastRateBrightness(Lcom/android/server/display/AutomaticBrightnessControllerImpl;F)V

    .line 716
    const-string v0, "Reset apply fast rate."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 709
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$AutomaticBrightnessControllerImplHandler;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$fputmNotInPocketTime(Lcom/android/server/display/AutomaticBrightnessControllerImpl;J)V

    .line 710
    const-string/jumbo v0, "take phone out of pocket at the current time!"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    nop

    .line 722
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
