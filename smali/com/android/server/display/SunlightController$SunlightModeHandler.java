class com.android.server.display.SunlightController$SunlightModeHandler extends android.os.Handler {
	 /* .source "SunlightController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SunlightController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SunlightModeHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.display.SunlightController this$0; //synthetic
/* # direct methods */
public com.android.server.display.SunlightController$SunlightModeHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 412 */
this.this$0 = p1;
/* .line 413 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 414 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 418 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 429 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.display.SunlightController .-$$Nest$mregister ( v0 );
/* .line 430 */
/* .line 426 */
/* :pswitch_1 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.SunlightController .-$$Nest$mupdateHangUpState ( v0,v1 );
/* .line 427 */
/* .line 423 */
/* :pswitch_2 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.display.SunlightController .-$$Nest$mupdateScreenState ( v0,v1 );
/* .line 424 */
/* .line 420 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.display.SunlightController .-$$Nest$mupdateAmbientLux ( v0 );
/* .line 421 */
/* nop */
/* .line 434 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
