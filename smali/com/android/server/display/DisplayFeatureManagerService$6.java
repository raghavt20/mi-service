class com.android.server.display.DisplayFeatureManagerService$6 implements android.os.IBinder$DeathRecipient {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService;->initServiceDeathRecipient()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayFeatureManagerService$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayFeatureManagerService; */
/* .line 643 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 3 */
/* .line 646 */
v0 = this.this$0;
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 647 */
try { // :try_start_0
	 final String v1 = "DisplayFeatureManagerService"; // const-string v1, "DisplayFeatureManagerService"
	 final String v2 = "binder service binderDied!"; // const-string v2, "binder service binderDied!"
	 android.util.Slog .v ( v1,v2 );
	 /* .line 648 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fputmWrapper ( v1,v2 );
	 /* .line 649 */
	 /* monitor-exit v0 */
	 /* .line 650 */
	 return;
	 /* .line 649 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
