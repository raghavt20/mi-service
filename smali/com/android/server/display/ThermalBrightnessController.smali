.class public Lcom/android/server/display/ThermalBrightnessController;
.super Ljava/lang/Object;
.source "ThermalBrightnessController.java"

# interfaces
.implements Lcom/android/server/display/MiuiDisplayCloudController$CloudListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/ThermalBrightnessController$Callback;,
        Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;,
        Lcom/android/server/display/ThermalBrightnessController$ThermalListener;
    }
.end annotation


# static fields
.field private static final CLOUD_BACKUP_CONFIG_FILE:Ljava/lang/String; = "cloud_thermal_brightness_control.xml"

.field private static final CLOUD_BACKUP_FILE_NAME:Ljava/lang/String; = "display_cloud_backup.xml"

.field private static final CLOUD_BAKUP_FILE_TEMP_GAP_VALUE:Ljava/lang/String; = "temperature-gap-value"

.field private static final CONDITION_ID_FOR_NTC_THERMAL:I = -0x3

.field protected static DEBUG:Z = false

.field private static final DEFAULT_CONFIG_FILE:Ljava/lang/String; = "thermal_brightness_control.xml"

.field private static final DOLBY_OVERRIDE_DESC:Ljava/lang/String; = "DOLBY-VISION"

.field private static final ETC_DIR:Ljava/lang/String; = "etc"

.field private static final EVENT_CONDITION_CHANGE:I = 0x2

.field private static final EVENT_NTC_TEMPERATURE_CHANGE:I = 0x10

.field private static final EVENT_OUTDOOR_CHANGE:I = 0x4

.field private static final EVENT_SAFETY_BRIGHTNESS_CHANGE:I = 0x8

.field private static final EVENT_TEMPERATURE_CHANGE:I = 0x1

.field private static final FILE_NTC_TEMPERATURE:Ljava/lang/String; = "display_therm_temp"

.field private static final FILE_SAFETY_BRIGHTNESS:Ljava/lang/String; = "thermal_max_brightness"

.field private static final FILE_SKIN_TEMPERATURE:Ljava/lang/String; = "board_sensor_temp"

.field private static final FILE_THERMAL_CONDITION_ID:Ljava/lang/String; = "sconfig"

.field private static final MAX_BRIGHTNESS_FILE:Ljava/lang/String; = "/sys/class/thermal/thermal_message/board_sensor_temp"

.field private static final NTC_TEMPERATURE_GAP:I = 0x1

.field private static final OUTDOOR_THERMAL_ID:I = -0x4

.field private static final TAG:Ljava/lang/String; = "ThermalBrightnessController"

.field private static final THERMAL_BRIGHTNESS_CONFIG_DIR:Ljava/lang/String; = "displayconfig"

.field private static final THERMAL_CONFIG_DIR:Ljava/lang/String; = "/sys/class/thermal/thermal_message"


# instance fields
.field private mBackgroundHandler:Landroid/os/Handler;

.field private mCacheInfo:Landroid/util/SparseArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArrayMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lcom/android/server/display/ThermalBrightnessController$Callback;

.field private mConditionIdMaps:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private volatile mCurrentActualThermalConditionId:I

.field private volatile mCurrentAppliedNtcTemperature:F

.field private volatile mCurrentAppliedTemperature:F

.field private volatile mCurrentAppliedThermalConditionId:I

.field private mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

.field private mCurrentMaxThermalBrightness:F

.field private volatile mCurrentNtcTemperature:F

.field private volatile mCurrentTemperature:F

.field private mDefaultCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

.field private mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

.field private mForegroundAppPackageName:Ljava/lang/String;

.field private volatile mIsDolbyEnabled:Z

.field private volatile mIsHdrLayerPresent:Z

.field private volatile mIsInOutdoorHighTempState:Z

.field private mIsThermalBrightnessBoostEnable:Z

.field private final mLoadFileRunnable:Ljava/lang/Runnable;

.field private mLoadedFileFrom:Ljava/lang/String;

.field private final mMinBrightnessInOutdoorHighTemperature:F

.field private mMiniTemperatureThresholds:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mNeedOverrideCondition:Z

.field private mNeedThermalBrightnessBoost:Z

.field private mOutdoorDetector:Lcom/android/server/display/OutdoorDetector;

.field private mOutdoorThermalAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOverrideCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

.field private volatile mScreenOn:Z

.field private final mSettingsObserver:Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;

.field private mSunlightModeActive:Z

.field private mTemperatureGap:F

.field private final mTemperatureLevelCritical:F

.field private final mTemperatureLevelEmergency:F

.field private final mThermalBrightnessControlNtcAvailable:Z

.field protected mThermalConditions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;",
            ">;"
        }
    .end annotation
.end field

.field private mThermalListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/display/ThermalBrightnessController$ThermalListener;",
            ">;"
        }
    .end annotation
.end field

.field private mThermalSafetyBrightness:F

.field private final mThresholdLuxEnterOutdoor:F

.field private final mUpdateConditionRunnable:Ljava/lang/Runnable;

.field private final mUpdateNtcTemperatureRunnable:Ljava/lang/Runnable;

.field private final mUpdateOutdoorRunnable:Ljava/lang/Runnable;

.field private final mUpdateOverrideConditionRunnable:Ljava/lang/Runnable;

.field private final mUpdateSafetyBrightnessRunnable:Ljava/lang/Runnable;

.field private final mUpdateTemperatureRunnable:Ljava/lang/Runnable;

.field private mUseAutoBrightness:Z


# direct methods
.method public static synthetic $r8$lambda$3iYN90ot-OIokKpGlYHa4LZho9k(Lcom/android/server/display/ThermalBrightnessController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$FtypDuq4GmSt8ostCcfZC1q7pN8(Lcom/android/server/display/ThermalBrightnessController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$O01v36ukOCBTPjYfLnM7YyQ0qQg(Lcom/android/server/display/ThermalBrightnessController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateOverrideCondition()V

    return-void
.end method

.method public static synthetic $r8$lambda$SZExmViQJCLUesVQIetdi6Kw-s4(Lcom/android/server/display/ThermalBrightnessController;Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/ThermalBrightnessController;->lambda$loadThermalConfig$0(Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V

    return-void
.end method

.method public static synthetic $r8$lambda$fn6uzVMbXafkmh4Vuc_e_CzYnX4(Lcom/android/server/display/ThermalBrightnessController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$nhkyG9-ejBfhl-OiobIMzG7eovg(Lcom/android/server/display/ThermalBrightnessController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->lambda$new$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$uy36Q3EV5YGq7Y7Ale1g419nQG8(Lcom/android/server/display/ThermalBrightnessController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->loadThermalConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBackgroundHandler(Lcom/android/server/display/ThermalBrightnessController;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/display/ThermalBrightnessController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUpdateOutdoorRunnable(Lcom/android/server/display/ThermalBrightnessController;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOutdoorRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUseAutoBrightness(Lcom/android/server/display/ThermalBrightnessController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmUseAutoBrightness(Lcom/android/server/display/ThermalBrightnessController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/display/ThermalBrightnessController$Callback;Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "callback"    # Lcom/android/server/display/ThermalBrightnessController$Callback;
    .param p4, "displayPowerControllerImpl"    # Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const-string v0, "null"

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadedFileFrom:Ljava/lang/String;

    .line 101
    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    .line 117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalConditions:Ljava/util/List;

    .line 119
    new-instance v1, Landroid/util/SparseArrayMap;

    invoke-direct {v1}, Landroid/util/SparseArrayMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCacheInfo:Landroid/util/SparseArrayMap;

    .line 121
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMiniTemperatureThresholds:Landroid/util/SparseArray;

    .line 123
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mConditionIdMaps:Landroid/util/SparseArray;

    .line 149
    iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    .line 155
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F

    .line 156
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalListener:Ljava/util/ArrayList;

    .line 462
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadFileRunnable:Ljava/lang/Runnable;

    .line 464
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOverrideConditionRunnable:Ljava/lang/Runnable;

    .line 466
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOutdoorRunnable:Ljava/lang/Runnable;

    .line 467
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateConditionRunnable:Ljava/lang/Runnable;

    .line 468
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateTemperatureRunnable:Ljava/lang/Runnable;

    .line 469
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateSafetyBrightnessRunnable:Ljava/lang/Runnable;

    .line 470
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda6;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateNtcTemperatureRunnable:Ljava/lang/Runnable;

    .line 176
    iput-object p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    .line 177
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    .line 178
    iput-object p3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCallback:Lcom/android/server/display/ThermalBrightnessController$Callback;

    .line 179
    iput-object p4, p0, Lcom/android/server/display/ThermalBrightnessController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 180
    new-instance v0, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;-><init>(Lcom/android/server/display/ThermalBrightnessController;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mSettingsObserver:Lcom/android/server/display/ThermalBrightnessController$SettingsObserver;

    .line 182
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 183
    const v2, 0x11070038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F

    .line 184
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 185
    const v2, 0x11070037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F

    .line 186
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 187
    const v2, 0x11070035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMinBrightnessInOutdoorHighTemperature:F

    .line 188
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 189
    const v2, 0x1107003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThresholdLuxEnterOutdoor:F

    .line 190
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 191
    const v3, 0x11050088

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalBrightnessControlNtcAvailable:Z

    .line 192
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 193
    const v3, 0x11050086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z

    .line 195
    new-instance v2, Lcom/android/server/display/OutdoorDetector;

    invoke-direct {v2, p1, p0, v1}, Lcom/android/server/display/OutdoorDetector;-><init>(Landroid/content/Context;Lcom/android/server/display/ThermalBrightnessController;F)V

    iput-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorDetector:Lcom/android/server/display/OutdoorDetector;

    .line 196
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    .line 197
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 196
    const-string v2, "screen_brightness_mode"

    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 199
    .local v1, "screenBrightnessModeSetting":I
    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    goto :goto_0

    :cond_0
    move v4, v3

    :goto_0
    iput-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z

    .line 202
    iget-object v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v5, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0;

    invoke-direct {v5, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 203
    iget-object v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 204
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 203
    const/4 v5, -0x1

    invoke-virtual {v4, v2, v3, v0, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 207
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->nativeInit()V

    .line 208
    return-void
.end method

.method private formatDumpTemperatureBrightnessPair(Ljava/util/List;Ljava/io/PrintWriter;)V
    .locals 13
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;",
            ">;",
            "Ljava/io/PrintWriter;",
            ")V"
        }
    .end annotation

    .line 839
    .local p1, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    const-string v0, "  temperature-brightness pair: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 840
    const/4 v0, 0x0

    .line 841
    .local v0, "sbTemperature":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 842
    .local v1, "sbNit":Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    .line 843
    .local v2, "needsHeaders":Z
    const-string v3, ""

    .line 844
    .local v3, "separator":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 845
    .local v4, "size":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 846
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    .line 847
    .local v6, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    if-eqz v2, :cond_0

    .line 848
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "    temperature: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, v7

    .line 849
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "            nit: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v1, v7

    .line 850
    const/4 v2, 0x0

    .line 853
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 854
    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 855
    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F

    move-result v8

    invoke-static {v8}, Lcom/android/server/display/ThermalBrightnessController;->toStrFloatForDump(F)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 856
    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 857
    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F

    move-result v8

    invoke-static {v8}, Lcom/android/server/display/ThermalBrightnessController;->toStrFloatForDump(F)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 858
    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 859
    .local v7, "strTemperature":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F

    move-result v8

    invoke-static {v8}, Lcom/android/server/display/ThermalBrightnessController;->toStrFloatForDump(F)Ljava/lang/String;

    move-result-object v8

    .line 860
    .local v8, "strNit":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 861
    .local v9, "maxLen":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "%"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "s"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 862
    .local v10, "format":Ljava/lang/String;
    const-string v3, ", "

    .line 863
    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/text/TextUtils;->formatSimple(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 864
    filled-new-array {v8}, [Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/text/TextUtils;->formatSimple(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 866
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    const/16 v12, 0x50

    if-gt v11, v12, :cond_1

    add-int/lit8 v11, v4, -0x1

    if-ne v5, v11, :cond_2

    .line 867
    :cond_1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 868
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 869
    const/4 v2, 0x1

    .line 870
    const-string v3, ""

    .line 845
    .end local v6    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v7    # "strTemperature":Ljava/lang/String;
    .end local v8    # "strNit":Ljava/lang/String;
    .end local v9    # "maxLen":I
    .end local v10    # "format":Ljava/lang/String;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 873
    .end local v5    # "i":I
    :cond_3
    return-void
.end method

.method private synthetic lambda$loadThermalConfig$0(Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V
    .locals 1
    .param p1, "l"    # Lcom/android/server/display/ThermalBrightnessController$ThermalListener;

    .line 222
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalConditions:Ljava/util/List;

    invoke-interface {p1, v0}, Lcom/android/server/display/ThermalBrightnessController$ThermalListener;->thermalConfigChanged(Ljava/util/List;)V

    .line 223
    return-void
.end method

.method private synthetic lambda$new$1()V
    .locals 1

    .line 467
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    return-void
.end method

.method private synthetic lambda$new$2()V
    .locals 1

    .line 468
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    return-void
.end method

.method private synthetic lambda$new$3()V
    .locals 1

    .line 469
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    return-void
.end method

.method private synthetic lambda$new$4()V
    .locals 1

    .line 470
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    return-void
.end method

.method private loadConfigFromFile()Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
    .locals 4

    .line 666
    invoke-static {}, Landroid/os/Environment;->getProductDirectory()Ljava/io/File;

    move-result-object v0

    const-string/jumbo v1, "thermal_brightness_control.xml"

    const-string v2, "etc"

    const-string v3, "displayconfig"

    filled-new-array {v2, v3, v1}, [Ljava/lang/String;

    move-result-object v1

    .line 665
    invoke-static {v0, v1}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 670
    .local v0, "defaultFile":Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "cloud_thermal_brightness_control.xml"

    filled-new-array {v3, v2}, [Ljava/lang/String;

    move-result-object v2

    .line 669
    invoke-static {v1, v2}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 672
    .local v1, "cloudFile":Ljava/io/File;
    invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->parseConfig(Ljava/io/File;)Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;

    move-result-object v2

    .line 673
    .local v2, "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
    if-eqz v2, :cond_0

    .line 674
    const-string v3, "cloud_file"

    iput-object v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadedFileFrom:Ljava/lang/String;

    .line 675
    return-object v2

    .line 678
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->parseConfig(Ljava/io/File;)Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;

    move-result-object v2

    .line 679
    if-eqz v2, :cond_1

    .line 680
    const-string/jumbo v3, "static_file"

    iput-object v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadedFileFrom:Ljava/lang/String;

    .line 681
    return-object v2

    .line 683
    :cond_1
    const/4 v3, 0x0

    return-object v3
.end method

.method private loadThermalConfig()V
    .locals 11

    .line 211
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalConditions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 212
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->loadConfigFromFile()Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;

    move-result-object v0

    .line 213
    .local v0, "config":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
    const-string v1, "ThermalBrightnessController"

    if-nez v0, :cond_0

    .line 214
    const-string v2, "config file was not found!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    return-void

    .line 217
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load thermal config from: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadedFileFrom:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalConditions:Ljava/util/List;

    invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;->getThermalConditionItem()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 221
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalListener:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda7;

    invoke-direct {v2, p0}, Lcom/android/server/display/ThermalBrightnessController$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/display/ThermalBrightnessController;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->forEach(Ljava/util/function/Consumer;)V

    .line 224
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalConditions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 226
    .local v2, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    invoke-virtual {v2}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v3

    .line 227
    .local v3, "id":I
    invoke-virtual {v2}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;

    move-result-object v4

    .line 229
    .local v4, "description":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 230
    iput-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mDefaultCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 234
    :cond_1
    const-string v5, "DOLBY-VISION"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 235
    iput-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mOverrideCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 238
    :cond_2
    iget-object v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mConditionIdMaps:Landroid/util/SparseArray;

    invoke-virtual {v5, v3, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 240
    invoke-virtual {v2}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v5

    .line 243
    .local v5, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    const/high16 v6, 0x7fc00000    # Float.NaN

    .line 244
    .local v6, "miniTemperature":F
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    .line 245
    .local v8, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-virtual {v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F

    move-result v9

    .line 246
    .local v9, "minTemp":F
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v10

    if-nez v10, :cond_3

    cmpg-float v10, v9, v6

    if-gez v10, :cond_4

    .line 247
    :cond_3
    move v6, v9

    .line 249
    .end local v8    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v9    # "minTemp":F
    :cond_4
    goto :goto_1

    .line 250
    :cond_5
    iget-object v7, p0, Lcom/android/server/display/ThermalBrightnessController;->mMiniTemperatureThresholds:Landroid/util/SparseArray;

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v7, v3, v8}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 251
    .end local v2    # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    .end local v3    # "id":I
    .end local v4    # "description":Ljava/lang/String;
    .end local v5    # "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    .end local v6    # "miniTemperature":F
    goto :goto_0

    .line 252
    :cond_6
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    .line 253
    return-void
.end method

.method private native nativeInit()V
.end method

.method private parseConfig(Ljava/io/File;)Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;
    .locals 4
    .param p1, "configFile"    # Ljava/io/File;

    .line 690
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 691
    return-object v1

    .line 693
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    .local v0, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/XmlParser;->read(Ljava/io/InputStream;)Lcom/android/server/display/thermalbrightnesscondition/config/ThermalBrightnessConfig;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 695
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0

    .line 694
    return-object v2

    .line 693
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/display/ThermalBrightnessController;
    .end local p1    # "configFile":Ljava/io/File;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljavax/xml/datatype/DatatypeConfigurationException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0

    .line 695
    .end local v0    # "in":Ljava/io/InputStream;
    .restart local p0    # "this":Lcom/android/server/display/ThermalBrightnessController;
    .restart local p1    # "configFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 696
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 698
    .end local v0    # "e":Ljava/lang/Exception;
    return-object v1
.end method

.method private readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "file"    # Ljava/lang/String;

    .line 633
    const/4 v0, 0x0

    .line 634
    .local v0, "reader":Ljava/io/BufferedReader;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 637
    .local v1, "builder":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    new-instance v4, Ljava/io/File;

    const-string v5, "/sys/class/thermal/thermal_message"

    invoke-direct {v4, v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v0, v2

    .line 638
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .local v3, "info":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 639
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 641
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 648
    nop

    .line 649
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 653
    goto :goto_1

    .line 651
    :catch_0
    move-exception v4

    .line 652
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 641
    .end local v4    # "e":Ljava/io/IOException;
    :goto_1
    return-object v2

    .line 647
    .end local v3    # "info":Ljava/lang/String;
    :catchall_0
    move-exception v2

    goto :goto_4

    .line 644
    :catch_1
    move-exception v2

    .line 645
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 648
    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v0, :cond_1

    .line 649
    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 651
    :catch_2
    move-exception v2

    .line 652
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 654
    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_3

    .line 653
    :cond_1
    :goto_2
    goto :goto_3

    .line 642
    :catch_3
    move-exception v2

    .line 643
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 648
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    if-eqz v0, :cond_1

    .line 649
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 655
    :goto_3
    const/4 v2, 0x0

    return-object v2

    .line 648
    :goto_4
    if-eqz v0, :cond_2

    .line 649
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_5

    .line 651
    :catch_4
    move-exception v3

    .line 652
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 653
    .end local v3    # "e":Ljava/io/IOException;
    :cond_2
    :goto_5
    nop

    .line 654
    :goto_6
    throw v2
.end method

.method private static toStrFloatForDump(F)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # F

    .line 889
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-nez v0, :cond_0

    .line 890
    const-string v0, "0"

    return-object v0

    .line 891
    :cond_0
    const v0, 0x3dcccccd    # 0.1f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_1

    .line 892
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%.3f"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 893
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_2

    .line 894
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%.2f"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 895
    :cond_2
    const/high16 v0, 0x41200000    # 10.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_3

    .line 896
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%.1f"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 898
    :cond_3
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%d"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->formatSimple(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toStringCondition(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;)Ljava/lang/String;
    .locals 3
    .param p0, "condition"    # Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 876
    if-nez p0, :cond_0

    .line 877
    const-string v0, "null"

    return-object v0

    .line 879
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 880
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{identifier: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 881
    invoke-virtual {p0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 882
    const-string v2, ", description: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 883
    invoke-virtual {p0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 884
    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 885
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private updateConditionState(I)V
    .locals 5
    .param p1, "event"    # I

    .line 337
    const/4 v0, 0x0

    .line 338
    .local v0, "changed":Z
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 339
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateSkinTemperature()Z

    move-result v0

    goto :goto_0

    .line 340
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 341
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalCondition()Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 342
    :cond_1
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 343
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateOutdoorState()Z

    move-result v1

    or-int/2addr v0, v1

    .line 344
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    iget-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    invoke-virtual {v1, v2}, Lcom/android/server/display/DisplayPowerControllerImpl;->notifyOutDoorHighTempState(Z)V

    goto :goto_0

    .line 345
    :cond_2
    const/16 v1, 0x8

    if-ne p1, v1, :cond_3

    .line 346
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateSafetyBrightness()Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_0

    .line 347
    :cond_3
    const/16 v1, 0x10

    if-ne p1, v1, :cond_4

    .line 348
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateNtcTemperature()Z

    move-result v1

    or-int/2addr v0, v1

    .line 350
    :cond_4
    :goto_0
    if-eqz v0, :cond_5

    .line 351
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalBrightnessIfNeeded()Z

    move-result v1

    .line 353
    .local v1, "thermalBrightnessChanged":Z
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mDisplayPowerControllerImpl:Lcom/android/server/display/DisplayPowerControllerImpl;

    iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I

    iget v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    invoke-virtual {v2, v3, v4, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->startDetailThermalUsageStatsOnThermalChanged(IFZ)V

    .line 358
    if-eqz v1, :cond_5

    .line 359
    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCallback:Lcom/android/server/display/ThermalBrightnessController$Callback;

    iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    invoke-interface {v2, v3}, Lcom/android/server/display/ThermalBrightnessController$Callback;->onThermalBrightnessChanged(F)V

    .line 362
    .end local v1    # "thermalBrightnessChanged":Z
    :cond_5
    return-void
.end method

.method private updateMaxNtcTempBrightness()F
    .locals 9

    .line 610
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mConditionIdMaps:Landroid/util/SparseArray;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 611
    .local v0, "ntcTempCondition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    const/high16 v1, 0x7fc00000    # Float.NaN

    if-nez v0, :cond_0

    .line 612
    return v1

    .line 614
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v2

    .line 615
    .local v2, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    const/4 v3, 0x0

    .line 616
    .local v3, "target":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    .line 617
    .local v5, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-virtual {v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F

    move-result v6

    .line 618
    .local v6, "minTemp":F
    invoke-virtual {v5}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F

    move-result v7

    .line 619
    .local v7, "maxTemp":F
    iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F

    cmpl-float v8, v8, v6

    if-ltz v8, :cond_1

    iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F

    cmpg-float v8, v8, v7

    if-gez v8, :cond_1

    .line 620
    move-object v3, v5

    .line 621
    goto :goto_1

    .line 623
    .end local v5    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v6    # "minTemp":F
    .end local v7    # "maxTemp":F
    :cond_1
    goto :goto_0

    .line 624
    :cond_2
    :goto_1
    if-nez v3, :cond_3

    .line 625
    return v1

    .line 627
    :cond_3
    invoke-virtual {v3}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F

    move-result v1

    .line 628
    .local v1, "value":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateMaxNtcTempBrightness: get brightness threshold: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ThermalBrightnessController"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    return v1
.end method

.method private updateMaxThermalBrightness()F
    .locals 12

    .line 556
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    const/high16 v1, 0x7fc00000    # Float.NaN

    const-string v2, "ThermalBrightnessController"

    if-nez v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mDefaultCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    if-eqz v0, :cond_0

    .line 558
    iput-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 559
    const-string/jumbo v0, "updateMaxThermalBrightness: mCurrentCondition is null, initialized with default condition."

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 562
    :cond_0
    const-string/jumbo v0, "updateMaxThermalBrightness: no valid conditions!"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    return v1

    .line 566
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-virtual {v0}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v0

    .line 567
    .local v0, "conditionId":I
    iget-object v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mMiniTemperatureThresholds:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    .line 568
    .local v3, "miniTemperature":Ljava/lang/Float;
    if-eqz v3, :cond_3

    iget v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 569
    sget-boolean v4, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 570
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateMaxThermalBrightness: Current skin temperature is less than the minimum temperature threshold: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    :cond_2
    return v1

    .line 575
    :cond_3
    iget-object v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCacheInfo:Landroid/util/SparseArrayMap;

    iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    float-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/util/SparseArrayMap;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    .line 576
    .local v4, "cachedNit":Ljava/lang/Float;
    if-eqz v4, :cond_5

    .line 577
    sget-boolean v1, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 578
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateMaxThermalBrightness: Max thermal brightness already cached: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_4
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    return v1

    .line 583
    :cond_5
    iget-object v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-virtual {v5}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v5

    .line 584
    .local v5, "pairs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;>;"
    const/4 v6, 0x0

    .line 585
    .local v6, "target":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;

    .line 586
    .local v8, "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    invoke-virtual {v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMinInclusive()F

    move-result v9

    .line 587
    .local v9, "minTemp":F
    invoke-virtual {v8}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getMaxExclusive()F

    move-result v10

    .line 588
    .local v10, "maxTemp":F
    iget v11, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    cmpl-float v11, v11, v9

    if-ltz v11, :cond_6

    iget v11, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    cmpg-float v11, v11, v10

    if-gez v11, :cond_6

    .line 589
    move-object v6, v8

    .line 590
    goto :goto_2

    .line 592
    .end local v8    # "pair":Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;
    .end local v9    # "minTemp":F
    .end local v10    # "maxTemp":F
    :cond_6
    goto :goto_1

    .line 594
    :cond_7
    :goto_2
    if-nez v6, :cond_8

    .line 595
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Current temperature "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " does not match in config: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    return v1

    .line 599
    :cond_8
    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/TemperatureBrightnessPair;->getNit()F

    move-result v1

    .line 600
    .local v1, "value":F
    iget-object v7, p0, Lcom/android/server/display/ThermalBrightnessController;->mCacheInfo:Landroid/util/SparseArrayMap;

    iget v8, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v0, v8, v9}, Landroid/util/SparseArrayMap;->add(ILjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "updateMaxThermalBrightness: get brightness threshold: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    return v1
.end method

.method private updateNtcTemperature()Z
    .locals 6

    .line 430
    const-string v0, "display_therm_temp"

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 432
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 433
    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    .line 434
    .local v1, "temperature":F
    sget-boolean v2, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "ThermalBrightnessController"

    if-eqz v2, :cond_0

    .line 435
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateNtcTemperature: temperature: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_0
    iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentNtcTemperature:F

    .line 438
    iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v2, v4}, Lcom/android/server/display/ThermalBrightnessController;->asSameTemperature(FFZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 439
    iput v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F

    .line 440
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateNtcTemperature: actual temperature: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentNtcTemperature:F

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", applied temperature: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 442
    return v4

    .line 445
    .end local v1    # "temperature":F
    :catch_0
    move-exception v1

    .line 446
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 447
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    nop

    .line 448
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method private updateOutdoorState()Z
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorDetector:Lcom/android/server/display/OutdoorDetector;

    invoke-virtual {v0}, Lcom/android/server/display/OutdoorDetector;->isOutdoorState()Z

    move-result v0

    .line 478
    .local v0, "state":Z
    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    if-eq v0, v1, :cond_0

    .line 479
    iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    .line 480
    const/4 v1, 0x1

    return v1

    .line 482
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private updateOverrideCondition()V
    .locals 2

    .line 744
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsHdrLayerPresent:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsDolbyEnabled:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 745
    .local v0, "needOverride":Z
    :goto_1
    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z

    if-eq v1, v0, :cond_2

    .line 746
    iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z

    .line 747
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    .line 749
    :cond_2
    return-void
.end method

.method private updateSafetyBrightness()Z
    .locals 3

    .line 415
    const-string/jumbo v0, "thermal_max_brightness"

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 416
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 417
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 418
    .local v1, "nit":F
    iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_1

    .line 419
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    const/high16 v2, 0x7fc00000    # Float.NaN

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    iput v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    .line 420
    const/4 v2, 0x1

    return v2

    .line 423
    .end local v1    # "nit":F
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private updateSkinTemperature()Z
    .locals 6

    .line 308
    const-string v0, "board_sensor_temp"

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "value":Ljava/lang/String;
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 311
    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 312
    .local v2, "temperature":F
    sget-boolean v3, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v4, "ThermalBrightnessController"

    if-eqz v3, :cond_0

    .line 313
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateSkinTemperature: temperature: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    iput v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    .line 316
    iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    invoke-virtual {p0, v2, v3, v1}, Lcom/android/server/display/ThermalBrightnessController;->asSameTemperature(FFZ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 317
    iput v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    .line 319
    invoke-virtual {p0}, Lcom/android/server/display/ThermalBrightnessController;->checkOutdoorState()V

    .line 320
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalBrightnessBoostCondition()V

    .line 321
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateSkinTemperature: actual temperature: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", applied temperature: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 323
    const/4 v1, 0x1

    return v1

    .line 326
    .end local v2    # "temperature":F
    :catch_0
    move-exception v2

    .line 327
    .local v2, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 328
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    nop

    .line 329
    :goto_0
    return v1
.end method

.method private updateThermalBrightnessBoostCondition()V
    .locals 2

    .line 775
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorThermalAppList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorThermalAppList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mForegroundAppPackageName:Ljava/lang/String;

    .line 776
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 777
    .local v0, "needThermalBrightnessBoost":Z
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z

    if-eq v1, v0, :cond_1

    .line 778
    iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z

    .line 779
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    .line 781
    :cond_1
    return-void
.end method

.method private updateThermalBrightnessIfNeeded()Z
    .locals 4

    .line 515
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateMaxThermalBrightness()F

    move-result v0

    .line 516
    .local v0, "restrictedBrightness":F
    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalBrightnessControlNtcAvailable:Z

    if-eqz v1, :cond_1

    .line 517
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateMaxNtcTempBrightness()F

    move-result v1

    .line 518
    .local v1, "restrictedNtcBrightness":F
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 519
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v1

    goto :goto_0

    .line 520
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    :goto_0
    move v0, v2

    .line 523
    .end local v1    # "restrictedNtcBrightness":F
    :cond_1
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 524
    return v2

    .line 529
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_3

    .line 531
    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMinBrightnessInOutdoorHighTemperature:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 535
    :cond_3
    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_4

    .line 536
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_4

    .line 537
    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 539
    :cond_4
    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_5

    .line 540
    iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    .line 541
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateThermalBrightnessState: condition id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", temperature: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", maximum thermal brightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", outdoor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", safety thermal brightness: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ThermalBrightnessController"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const/4 v1, 0x1

    return v1

    .line 548
    :cond_5
    return v2
.end method

.method private updateThermalCondition()Z
    .locals 7

    .line 368
    const-string v0, ", name="

    const-string v1, "ThermalBrightnessController"

    const-string v2, "sconfig"

    invoke-direct {p0, v2}, Lcom/android/server/display/ThermalBrightnessController;->readThermalConfigFromNode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 370
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 371
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 372
    .local v3, "id":I
    iget v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I

    if-ne v4, v3, :cond_0

    iget-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z

    if-eqz v4, :cond_5

    .line 373
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateThermalCondition: condition changed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    iput v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentActualThermalConditionId:I

    .line 377
    iget-boolean v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z

    if-eqz v4, :cond_1

    .line 378
    const/4 v3, -0x4

    .line 381
    :cond_1
    iget-object v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mConditionIdMaps:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 383
    .local v4, "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    iget-boolean v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z

    if-eqz v5, :cond_2

    .line 384
    iget-object v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mOverrideCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    move-object v4, v5

    .line 387
    :cond_2
    if-nez v4, :cond_3

    .line 388
    iget-object v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mDefaultCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    if-eqz v5, :cond_3

    .line 389
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Thermal condition (id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentActualThermalConditionId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") is not configured in file, apply default condition!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mDefaultCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    move-object v4, v5

    .line 395
    :cond_3
    iget-object v5, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    if-eq v4, v5, :cond_5

    .line 396
    if-eqz v5, :cond_4

    if-eqz v4, :cond_4

    .line 397
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "condition changed from: [id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 398
    invoke-virtual {v6}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] to [id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 399
    invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 400
    invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "]"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 397
    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    :cond_4
    iput-object v4, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 403
    invoke-virtual {v4}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v0

    iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    const/4 v0, 0x1

    return v0

    .line 408
    .end local v3    # "id":I
    .end local v4    # "condition":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 410
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    nop

    .line 411
    :goto_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public addThermalListener(Lcom/android/server/display/ThermalBrightnessController$ThermalListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/android/server/display/ThermalBrightnessController$ThermalListener;

    .line 903
    if-eqz p1, :cond_0

    .line 904
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 906
    :cond_0
    return-void
.end method

.method public asSameTemperature(FFZ)Z
    .locals 4
    .param p1, "a"    # F
    .param p2, "b"    # F
    .param p3, "isNtcTemperatureGap"    # Z

    .line 702
    cmpl-float v0, p1, p2

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 703
    return v1

    .line 704
    :cond_0
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 705
    return v1

    .line 706
    :cond_1
    float-to-int v0, p1

    float-to-int v2, p2

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ge v0, v1, :cond_2

    .line 707
    return v1

    .line 709
    :cond_2
    const/4 v0, 0x0

    if-eqz p3, :cond_4

    .line 710
    sub-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    goto :goto_0

    :cond_3
    move v1, v0

    :goto_0
    return v1

    .line 712
    :cond_4
    sub-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    goto :goto_1

    :cond_5
    move v1, v0

    :goto_1
    return v1
.end method

.method public checkOutdoorState()V
    .locals 2

    .line 494
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mUseAutoBrightness:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mSunlightModeActive:Z

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 498
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorDetector:Lcom/android/server/display/OutdoorDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/display/OutdoorDetector;->detect(Z)Z

    goto :goto_0

    .line 500
    :cond_2
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorDetector:Lcom/android/server/display/OutdoorDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/display/OutdoorDetector;->detect(Z)Z

    .line 502
    :goto_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 800
    sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DPC:Z

    sput-boolean v0, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z

    .line 801
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 802
    const-string v0, "Thermal Brightness Controller: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 803
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentCondition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-static {v1}, Lcom/android/server/display/ThermalBrightnessController;->toStringCondition(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 804
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mDefaultCondition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mDefaultCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-static {v1}, Lcom/android/server/display/ThermalBrightnessController;->toStringCondition(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mOverrideCondition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mOverrideCondition:Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    invoke-static {v1}, Lcom/android/server/display/ThermalBrightnessController;->toStringCondition(Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 806
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentAppliedThermalConditionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedThermalConditionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 807
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentActualThermalConditionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentActualThermalConditionId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 808
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentAppliedTemperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentTemperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentTemperature:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 810
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentThermalMaxBrightness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentMaxThermalBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 811
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mMiniTemperatureThresholds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMiniTemperatureThresholds:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 812
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIsInOutdoorHighTempState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 813
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mIsThermalBrightnessBoostEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 814
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mNeedThermalBrightnessBoost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedThermalBrightnessBoost:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 815
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mScreenOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 816
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mNeedOverrideCondition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mNeedOverrideCondition:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 817
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTemperatureLevelEmergency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 818
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTemperatureLevelCritical="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 819
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mMinBrightnessInOutdoorHighTemperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mMinBrightnessInOutdoorHighTemperature:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 820
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mThresholdLuxEnterOutdoor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThresholdLuxEnterOutdoor:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 821
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mThermalSafetyBrightness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalSafetyBrightness:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 822
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mTemperatureGap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 823
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mThermalBrightnessControlNtcAvailable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalBrightnessControlNtcAvailable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 824
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentNtcTemperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentNtcTemperature:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 825
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mCurrentAppliedNtcTemperature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedNtcTemperature:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 827
    const-string v0, "  Thermal brightness config:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  mLoadedFileFrom:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadedFileFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 829
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mThermalConditions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;

    .line 830
    .local v1, "conditionItem":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  identifier: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getIdentifier()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 831
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  description: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 832
    invoke-virtual {v1}, Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;->getTemperatureBrightnessPair()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/android/server/display/ThermalBrightnessController;->formatDumpTemperatureBrightnessPair(Ljava/util/List;Ljava/io/PrintWriter;)V

    .line 833
    .end local v1    # "conditionItem":Lcom/android/server/display/thermalbrightnesscondition/config/ThermalConditionItem;
    goto :goto_0

    .line 836
    :cond_0
    return-void
.end method

.method protected isInOutdoorCriticalTemperature()Z
    .locals 2

    .line 509
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsInOutdoorHighTempState:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelCritical:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mCurrentAppliedTemperature:F

    iget v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureLevelEmergency:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onCloudUpdated(JLjava/util/Map;)V
    .locals 4
    .param p1, "summary"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 453
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-wide/16 v0, 0x2

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadFileRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 455
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mLoadFileRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 457
    :cond_0
    const-wide/16 v0, 0x4

    and-long/2addr v0, p1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 458
    const-string v0, "TemperatureGap"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mTemperatureGap:F

    .line 460
    :cond_1
    return-void
.end method

.method protected onConditionEvent(I)V
    .locals 2
    .param p1, "event"    # I

    .line 277
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateConditionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 279
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateConditionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 281
    :cond_0
    return-void
.end method

.method protected onNtcTemperatureEvent(I)V
    .locals 2
    .param p1, "event"    # I

    .line 297
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateNtcTemperatureRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 299
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateNtcTemperatureRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 301
    :cond_0
    return-void
.end method

.method protected onSafetyBrightnessEvent(I)V
    .locals 2
    .param p1, "event"    # I

    .line 287
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateSafetyBrightnessRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 289
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateSafetyBrightnessRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 291
    :cond_0
    return-void
.end method

.method protected onTemperatureEvent(I)V
    .locals 2
    .param p1, "event"    # I

    .line 266
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateTemperatureRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 268
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateTemperatureRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 271
    :cond_0
    return-void
.end method

.method public outDoorStateChanged()V
    .locals 1

    .line 473
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/server/display/ThermalBrightnessController;->updateConditionState(I)V

    .line 474
    return-void
.end method

.method public setDolbyEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 736
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsDolbyEnabled:Z

    if-eq v0, p1, :cond_0

    .line 737
    iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsDolbyEnabled:Z

    .line 738
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOverrideConditionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 739
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOverrideConditionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 741
    :cond_0
    return-void
.end method

.method public setHdrLayerPresent(Z)V
    .locals 2
    .param p1, "isHdrLayerPresent"    # Z

    .line 728
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsHdrLayerPresent:Z

    if-eq v0, p1, :cond_0

    .line 729
    iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsHdrLayerPresent:Z

    .line 730
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOverrideConditionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 731
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOverrideConditionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 733
    :cond_0
    return-void
.end method

.method public setSunlightState(Z)V
    .locals 2
    .param p1, "active"    # Z

    .line 788
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mSunlightModeActive:Z

    if-eq p1, v0, :cond_0

    .line 789
    iput-boolean p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mSunlightModeActive:Z

    .line 790
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOutdoorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 791
    iget-object v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOutdoorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 793
    :cond_0
    return-void
.end method

.method public updateForegroundApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "foregroundAppPackageName"    # Ljava/lang/String;

    .line 764
    iget-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mIsThermalBrightnessBoostEnable:Z

    if-eqz v0, :cond_0

    .line 765
    iput-object p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mForegroundAppPackageName:Ljava/lang/String;

    .line 766
    invoke-direct {p0}, Lcom/android/server/display/ThermalBrightnessController;->updateThermalBrightnessBoostCondition()V

    .line 768
    :cond_0
    return-void
.end method

.method public updateOutdoorThermalAppCategoryList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 756
    .local p1, "outdoorThermalAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/android/server/display/ThermalBrightnessController;->mOutdoorThermalAppList:Ljava/util/List;

    .line 757
    return-void
.end method

.method protected updateScreenState(II)V
    .locals 3
    .param p1, "displayState"    # I
    .param p2, "displayPolicy"    # I

    .line 717
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    const/4 v1, 0x3

    if-eq p2, v1, :cond_0

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 720
    .local v0, "screenOn":Z
    :goto_0
    iget-boolean v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z

    if-eq v0, v1, :cond_2

    .line 721
    iput-boolean v0, p0, Lcom/android/server/display/ThermalBrightnessController;->mScreenOn:Z

    .line 722
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOutdoorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 723
    iget-object v1, p0, Lcom/android/server/display/ThermalBrightnessController;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/display/ThermalBrightnessController;->mUpdateOutdoorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 725
    :cond_2
    return-void
.end method
