public class com.android.server.display.RampRateController {
	 /* .source "RampRateController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/RampRateController$RateStateRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ANIMATION_RATE_TYPE_DEFAULT;
private static final Integer ANIMATION_RATE_TYPE_DIM;
private static final Integer ANIMATION_RATE_TYPE_HDR_BRIGHTNESS;
private static final Integer ANIMATION_RATE_TYPE_MANUAL_BRIGHTNESS;
private static final Integer ANIMATION_RATE_TYPE_TEMPORARY_DIMMING;
private static final Integer BRIGHTNESS_12BIT;
private static final Boolean IS_UMI_0B_DISPLAY_PANEL;
private static final Float NIT_LEVEL;
private static final Float NIT_LEVEL1;
private static final Float NIT_LEVEL2;
private static final Float NIT_LEVEL3;
private static final java.lang.String OLED_PANEL_ID;
private static final Integer RATE_LEVEL;
private static final Integer REASON_AUTO_BRIGHTNESS_RATE;
private static final Integer REASON_BCBC_BRIGHTNESS_RATE;
private static final Integer REASON_DIM_BRIGHTNESS_RATE;
private static final Integer REASON_DOZE_BRIGHTNESS_RATE;
private static final Integer REASON_FAST_BRIGHTNESS_ADJUSTMENT_RATE;
private static final Integer REASON_FAST_RATE;
private static final Integer REASON_HDR_BRIGHTNESS_RATE;
private static final Integer REASON_MANUAL_BRIGHTNESS_RATE;
private static final Integer REASON_OVERRIDE_BRIGHTNESS_RATE;
private static final Integer REASON_SLOW_FAST;
private static final Integer REASON_TEMPORARY_BRIGHTNESS_RATE;
private static final Integer REASON_THERMAL_BRIGHTNESS_RATE;
private static final Integer REASON_ZERO_RATE;
private static final java.lang.String TAG;
private static final Float TIME_1;
private static final Float TIME_2;
private static final Float TIME_3;
private static final Float TIME_4;
private static final Float TIME_5;
/* # instance fields */
private final mALevels;
private Boolean mAnimateValueChanged;
private final Integer mAnimationDurationDim;
private final Integer mAnimationDurationDozeDimming;
private final Integer mAnimationDurationHdrBrightness;
private final Integer mAnimationDurationManualBrightness;
private final Integer mAnimationDurationTemporaryDimming;
private Boolean mAppliedHdrRateDueToEntry;
private Boolean mAppliedHdrRateDueToExit;
private Boolean mAppliedOverrideRateDueToEntry;
private Boolean mAppliedOverrideRateDueToExit;
private final mBLevels;
private Float mBrightness;
private Boolean mBrightnessChanged;
private Float mBrightnessMaxNit;
private final Float mBrightnessRampRateFast;
private final Float mBrightnessRampRateSlow;
private final com.android.server.display.brightness.BrightnessReason mBrightnessReason;
private final com.android.server.display.brightness.BrightnessReason mBrightnessReasonTemp;
private Integer mCurrentPolicy;
private Boolean mDebug;
private final com.android.server.display.DisplayDeviceConfig mDisplayDeviceConfig;
private final com.android.server.display.DisplayPowerControllerImpl mDisplayPowerControllerImpl;
private final android.os.Handler mHandler;
private Float mHbmMaxNit;
private Float mHbmTransitionPointNit;
private final com.android.server.display.DisplayDeviceConfig$HighBrightnessModeData mHighBrightnessModeData;
private final Float mMinimumAdjustRate;
private final mNitsLevels;
private Integer mPreviousDisplayState;
private Integer mPreviousPolicy;
private com.android.server.display.RampRateController$RateStateRecord mRateRecord;
private final Integer mScreenBrightnessMaximumInt;
private Float mSdrBrightness;
private Boolean mSlowChange;
private final Float mSlowLinearRampRate;
private final Boolean mSupportManualDimming;
private Float mTargetBrightness;
private Float mTargetSdrBrightness;
/* # direct methods */
static Integer -$$Nest$fgetmAnimationDurationDozeDimming ( com.android.server.display.RampRateController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDozeDimming:I */
} // .end method
static Integer -$$Nest$fgetmAnimationDurationTemporaryDimming ( com.android.server.display.RampRateController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationTemporaryDimming:I */
} // .end method
static Boolean -$$Nest$fgetmSupportManualDimming ( com.android.server.display.RampRateController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/RampRateController;->mSupportManualDimming:Z */
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.display.RampRateController.TAG;
} // .end method
static com.android.server.display.RampRateController ( ) {
	 /* .locals 2 */
	 /* .line 45 */
	 /* const-class v0, Lcom/android/server/display/RampRateController; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 /* .line 68 */
	 /* nop */
	 /* .line 69 */
	 final String v0 = "ro.boot.oled_panel_id"; // const-string v0, "ro.boot.oled_panel_id"
	 final String v1 = ""; // const-string v1, ""
	 android.os.SystemProperties .get ( v0,v1 );
	 /* .line 70 */
	 final String v1 = "0B"; // const-string v1, "0B"
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v0 = android.os.Build.DEVICE;
		 /* .line 71 */
		 /* const-string/jumbo v1, "umi" */
		 v0 = 		 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* if-nez v0, :cond_0 */
		 /* const-string/jumbo v0, "umiin" */
		 v1 = android.os.Build.DEVICE;
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
		 } // :cond_0
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_1
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
com.android.server.display.RampRateController.IS_UMI_0B_DISPLAY_PANEL = (v0!= 0);
/* .line 70 */
return;
} // .end method
public com.android.server.display.RampRateController ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "impl" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .param p3, "deviceConfig" # Lcom/android/server/display/DisplayDeviceConfig; */
/* .param p4, "looper" # Landroid/os/Looper; */
/* .line 131 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 83 */
/* const/16 v0, 0x9 */
/* new-array v1, v0, [F */
/* fill-array-data v1, :array_0 */
this.mNitsLevels = v1;
/* .line 84 */
/* new-array v1, v0, [F */
/* fill-array-data v1, :array_1 */
this.mALevels = v1;
/* .line 86 */
/* new-array v0, v0, [F */
/* fill-array-data v0, :array_2 */
this.mBLevels = v0;
/* .line 102 */
/* new-instance v0, Lcom/android/server/display/brightness/BrightnessReason; */
/* invoke-direct {v0}, Lcom/android/server/display/brightness/BrightnessReason;-><init>()V */
this.mBrightnessReason = v0;
/* .line 103 */
/* new-instance v0, Lcom/android/server/display/brightness/BrightnessReason; */
/* invoke-direct {v0}, Lcom/android/server/display/brightness/BrightnessReason;-><init>()V */
this.mBrightnessReasonTemp = v0;
/* .line 112 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
/* .line 122 */
/* const/high16 v1, 0x7fc00000 # Float.NaN */
/* iput v1, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F */
/* .line 123 */
/* iput v1, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F */
/* .line 124 */
/* iput v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F */
/* .line 132 */
this.mDisplayPowerControllerImpl = p2;
/* .line 133 */
this.mDisplayDeviceConfig = p3;
/* .line 134 */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1, p4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 135 */
(( com.android.server.display.DisplayDeviceConfig ) p3 ).getHighBrightnessModeData ( ); // invoke-virtual {p3}, Lcom/android/server/display/DisplayDeviceConfig;->getHighBrightnessModeData()Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;
this.mHighBrightnessModeData = v1;
/* .line 136 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 137 */
/* .local v1, "resources":Landroid/content/res/Resources; */
/* const v2, 0x10500b8 */
v2 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateSlow:F */
/* .line 139 */
/* const v2, 0x10500b7 */
v2 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateFast:F */
/* .line 141 */
/* const v2, 0x110b000a */
v2 = (( android.content.res.Resources ) v1 ).getInteger ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDim:I */
/* .line 143 */
/* const v2, 0x110b000e */
v2 = (( android.content.res.Resources ) v1 ).getInteger ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationTemporaryDimming:I */
/* .line 145 */
/* const v2, 0x110b000d */
v2 = (( android.content.res.Resources ) v1 ).getInteger ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationManualBrightness:I */
/* .line 147 */
/* const v2, 0x110b000c */
v2 = (( android.content.res.Resources ) v1 ).getInteger ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationHdrBrightness:I */
/* .line 149 */
/* const v2, 0x110b000b */
v2 = (( android.content.res.Resources ) v1 ).getInteger ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDozeDimming:I */
/* .line 151 */
/* const v2, 0x11070036 */
v2 = (( android.content.res.Resources ) v1 ).getFloat ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mSlowLinearRampRate:F */
/* .line 152 */
/* const v2, 0x10e00f3 */
v2 = (( android.content.res.Resources ) v1 ).getInteger ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I
/* iput v2, p0, Lcom/android/server/display/RampRateController;->mScreenBrightnessMaximumInt:I */
/* .line 154 */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* int-to-float v2, v2 */
/* div-float/2addr v3, v2 */
/* iput v3, p0, Lcom/android/server/display/RampRateController;->mMinimumAdjustRate:F */
/* .line 155 */
/* const-string/jumbo v2, "support_manual_dimming" */
v0 = miui.util.FeatureParser .getBoolean ( v2,v0 );
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mSupportManualDimming:Z */
/* .line 156 */
/* new-instance v0, Lcom/android/server/display/RampRateController$RateStateRecord; */
/* invoke-direct {v0, p0}, Lcom/android/server/display/RampRateController$RateStateRecord;-><init>(Lcom/android/server/display/RampRateController;)V */
this.mRateRecord = v0;
/* .line 157 */
/* invoke-direct {p0}, Lcom/android/server/display/RampRateController;->updateDeviceConfigData()V */
/* .line 158 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x44480000 # 800.0f */
/* 0x437b0000 # 251.0f */
/* 0x43160000 # 150.0f */
/* 0x42c80000 # 100.0f */
/* 0x428c0000 # 70.0f */
/* 0x42480000 # 50.0f */
/* 0x42200000 # 40.0f */
/* 0x41f00000 # 30.0f */
/* 0x41e40000 # 28.5f */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x44480000 # 800.0f */
/* 0x440e5eb8 */
/* 0x43ac71ec */
/* 0x436dc000 # 237.75f */
/* 0x4333b5c3 # 179.71f */
/* 0x430730a4 # 135.19f */
/* 0x42e32e14 # 113.59f */
/* 0x427b5c29 # 62.84f */
/* 0x442937ae # 676.87f */
} // .end array-data
/* :array_2 */
/* .array-data 4 */
/* 0x3f7d1b71 # 0.9887f */
/* 0x3f7df3b6 # 0.992f */
/* 0x3f7eb852 # 0.995f */
/* 0x3f7f1aa0 # 0.9965f */
/* 0x3f7f4f0e # 0.9973f */
/* 0x3f7f7660 # 0.9979f */
/* 0x3f7f8a09 # 0.9982f */
/* 0x3f7fbe77 # 0.999f */
/* 0x3f7ef9db # 0.996f */
} // .end array-data
} // .end method
private Boolean appliedFastRate ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "currentBrightness" # F */
/* .param p2, "targetBrightness" # F */
/* .line 607 */
v0 = this.mDisplayPowerControllerImpl;
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).appliedFastRate ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/display/DisplayPowerControllerImpl;->appliedFastRate(FF)Z
} // .end method
private void clearHdrRateModifier ( ) {
/* .locals 2 */
/* .line 623 */
v0 = this.mRateRecord;
/* const/16 v1, 0x8 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 624 */
return;
} // .end method
private void clearOverrideRateModifier ( ) {
/* .locals 2 */
/* .line 627 */
v0 = this.mRateRecord;
/* const/16 v1, 0x10 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 628 */
return;
} // .end method
private Float convertToBrightness ( Float p0 ) {
/* .locals 1 */
/* .param p1, "nit" # F */
/* .line 576 */
v0 = this.mDisplayDeviceConfig;
/* if-nez v0, :cond_0 */
/* .line 577 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 579 */
} // :cond_0
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getBrightnessFromNit ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getBrightnessFromNit(F)F
} // .end method
private Float convertToNit ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 569 */
v0 = this.mDisplayDeviceConfig;
/* if-nez v0, :cond_0 */
/* .line 570 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 572 */
} // :cond_0
v0 = (( com.android.server.display.DisplayDeviceConfig ) v0 ).getNitsFromBacklight ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
} // .end method
private Float getBrighteningLogRate ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 5 */
/* .param p1, "startBrightness" # F */
/* .param p2, "currentBrightness" # F */
/* .param p3, "targetBrightness" # F */
/* .param p4, "duration" # F */
/* .line 523 */
/* nop */
/* .line 524 */
v0 = android.util.MathUtils .exp ( p3 );
v1 = android.util.MathUtils .exp ( p1 );
/* sub-float/2addr v0, v1 */
/* const/high16 v1, 0x447a0000 # 1000.0f */
/* mul-float/2addr v0, v1 */
/* div-float/2addr v0, p4 */
/* .line 525 */
/* .local v0, "coefficient":F */
v1 = android.util.MathUtils .exp ( p2 );
/* div-float v1, v0, v1 */
/* .line 526 */
/* .local v1, "rate":F */
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 527 */
v2 = com.android.server.display.RampRateController.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getBrighteningLogRate: rate: "; // const-string v4, "getBrighteningLogRate: rate: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", startBrightness: "; // const-string v4, ", startBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", currentBrightness: "; // const-string v4, ", currentBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", targetBrightness: "; // const-string v4, ", targetBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", coefficient: "; // const-string v4, ", coefficient: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 534 */
} // :cond_0
} // .end method
private Float getBrighteningRate ( Float p0, Float p1, Float p2 ) {
/* .locals 9 */
/* .param p1, "brightness" # F */
/* .param p2, "startBrightness" # F */
/* .param p3, "tgtBrightness" # F */
/* .line 382 */
v6 = /* invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* .line 383 */
/* .local v6, "nit":F */
v7 = /* invoke-direct {p0, p2}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* .line 384 */
/* .local v7, "startNit":F */
v8 = /* invoke-direct {p0, p3}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* .line 386 */
/* .local v8, "targetNit":F */
/* const/high16 v0, 0x420c0000 # 35.0f */
/* cmpg-float v1, v7, v0 */
/* const v2, 0x42aee667 # 87.450005f */
/* const v3, 0x43848000 # 265.0f */
/* if-gez v1, :cond_5 */
/* .line 387 */
/* cmpg-float v1, v8, v0 */
/* if-gez v1, :cond_0 */
/* .line 388 */
/* sub-float v0, v8, v7 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F */
/* const/high16 v1, 0x40800000 # 4.0f */
/* div-float/2addr v0, v1 */
/* .local v0, "rate":F */
/* goto/16 :goto_0 */
/* .line 389 */
} // .end local v0 # "rate":F
} // :cond_0
/* cmpg-float v1, v8, v3 */
/* if-gez v1, :cond_2 */
/* .line 390 */
/* cmpg-float v1, v6, v0 */
/* if-gez v1, :cond_1 */
/* .line 391 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F */
/* sub-float/2addr v0, p2 */
/* const v1, 0x3fe66666 # 1.8f */
/* div-float/2addr v0, v1 */
/* .restart local v0 # "rate":F */
/* goto/16 :goto_0 */
/* .line 393 */
} // .end local v0 # "rate":F
} // :cond_1
/* const/high16 v1, 0x420c0000 # 35.0f */
/* const v4, 0x3fe66666 # 1.8f */
/* const/high16 v5, 0x40800000 # 4.0f */
/* move-object v0, p0 */
/* move v2, v8 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .restart local v0 # "rate":F */
/* goto/16 :goto_0 */
/* .line 396 */
} // .end local v0 # "rate":F
} // :cond_2
/* cmpg-float v1, v6, v0 */
/* if-gez v1, :cond_3 */
/* .line 397 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F */
/* sub-float/2addr v0, p2 */
/* const v1, 0x3f4ccccd # 0.8f */
/* div-float/2addr v0, v1 */
/* .restart local v0 # "rate":F */
/* goto/16 :goto_0 */
/* .line 398 */
} // .end local v0 # "rate":F
} // :cond_3
/* cmpg-float v0, v6, v2 */
/* if-gez v0, :cond_4 */
/* .line 399 */
/* const/high16 v1, 0x420c0000 # 35.0f */
/* const v2, 0x42aee667 # 87.450005f */
/* const v4, 0x3f4ccccd # 0.8f */
/* const v5, 0x3fe66666 # 1.8f */
/* move-object v0, p0 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .restart local v0 # "rate":F */
/* goto/16 :goto_0 */
/* .line 401 */
} // .end local v0 # "rate":F
} // :cond_4
/* const v1, 0x42aee667 # 87.450005f */
/* const v4, 0x3fe66666 # 1.8f */
/* const/high16 v5, 0x40800000 # 4.0f */
/* move-object v0, p0 */
/* move v2, v8 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .restart local v0 # "rate":F */
/* .line 404 */
} // .end local v0 # "rate":F
} // :cond_5
/* cmpg-float v0, v7, v3 */
/* if-gez v0, :cond_8 */
/* .line 405 */
/* cmpg-float v0, v8, v3 */
/* if-gez v0, :cond_6 */
/* .line 406 */
int v4 = 0; // const/4 v4, 0x0
/* const/high16 v5, 0x40800000 # 4.0f */
/* move-object v0, p0 */
/* move v1, v7 */
/* move v2, v8 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .restart local v0 # "rate":F */
/* .line 408 */
} // .end local v0 # "rate":F
} // :cond_6
/* cmpg-float v0, v6, v2 */
/* if-gez v0, :cond_7 */
/* .line 409 */
/* const v2, 0x42aee667 # 87.450005f */
int v4 = 0; // const/4 v4, 0x0
/* const v5, 0x3fe66666 # 1.8f */
/* move-object v0, p0 */
/* move v1, v7 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .restart local v0 # "rate":F */
/* .line 411 */
} // .end local v0 # "rate":F
} // :cond_7
/* const v1, 0x42aee667 # 87.450005f */
/* const v4, 0x3fe66666 # 1.8f */
/* const/high16 v5, 0x40800000 # 4.0f */
/* move-object v0, p0 */
/* move v2, v8 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .restart local v0 # "rate":F */
/* .line 415 */
} // .end local v0 # "rate":F
} // :cond_8
int v4 = 0; // const/4 v4, 0x0
/* const/high16 v5, 0x40800000 # 4.0f */
/* move-object v0, p0 */
/* move v1, v7 */
/* move v2, v8 */
/* move v3, v6 */
v0 = /* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/RampRateController;->getExpRate(FFFFF)F */
/* .line 417 */
/* .restart local v0 # "rate":F */
} // :goto_0
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mMinimumAdjustRate:F */
v0 = android.util.MathUtils .max ( v0,v1 );
/* .line 418 */
/* iget-boolean v1, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 419 */
v1 = com.android.server.display.RampRateController.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getBrighteningRate: rate: "; // const-string v3, "getBrighteningRate: rate: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", nit: "; // const-string v3, ", nit: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", startNit: "; // const-string v3, ", startNit: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", targetNit: "; // const-string v3, ", targetNit: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 424 */
} // :cond_9
} // .end method
private Float getDarkeningExpRate ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 5 */
/* .param p1, "startBrightness" # F */
/* .param p2, "currentBrightness" # F */
/* .param p3, "targetBrightness" # F */
/* .param p4, "duration" # F */
/* .line 539 */
/* div-float v0, p3, p1 */
v0 = android.util.MathUtils .log ( v0 );
/* const/high16 v1, 0x447a0000 # 1000.0f */
/* mul-float/2addr v0, v1 */
/* div-float/2addr v0, p4 */
/* .line 540 */
/* .local v0, "coefficient":F */
/* mul-float v1, v0, p2 */
v1 = android.util.MathUtils .abs ( v1 );
/* .line 541 */
/* .local v1, "rate":F */
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 542 */
v2 = com.android.server.display.RampRateController.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getDarkeningExpRate: rate: "; // const-string v4, "getDarkeningExpRate: rate: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", startBrightness: "; // const-string v4, ", startBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", currentBrightness: "; // const-string v4, ", currentBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", targetBrightness: "; // const-string v4, ", targetBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", coefficient: "; // const-string v4, ", coefficient: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 549 */
} // :cond_0
} // .end method
private Float getDarkeningRate ( Float p0 ) {
/* .locals 10 */
/* .param p1, "brightness" # F */
/* .line 437 */
v0 = this.mHighBrightnessModeData;
/* const/high16 v1, 0x3f800000 # 1.0f */
/* if-nez v0, :cond_0 */
/* move v0, v1 */
/* .line 438 */
} // :cond_0
/* iget v0, v0, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F */
} // :goto_0
/* nop */
/* .line 439 */
/* .local v0, "normalMaxBrightnessFloat":F */
v2 = this.mDisplayDeviceConfig;
v2 = (( com.android.server.display.DisplayDeviceConfig ) v2 ).getNitsFromBacklight ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/display/DisplayDeviceConfig;->getNitsFromBacklight(F)F
/* .line 440 */
/* .local v2, "normalNit":F */
/* const/high16 v3, -0x40800000 # -1.0f */
/* cmpl-float v3, v2, v3 */
/* if-nez v3, :cond_1 */
/* .line 441 */
/* const/high16 v2, 0x43fa0000 # 500.0f */
/* .line 443 */
} // :cond_1
v3 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v0 );
/* .line 444 */
/* .local v3, "normalMaxBrightnessInt":I */
v4 = /* invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* .line 445 */
/* .local v4, "nit":F */
v5 = /* invoke-direct {p0, v4}, Lcom/android/server/display/RampRateController;->getIndex(F)I */
/* .line 446 */
/* .local v5, "index":I */
v6 = this.mALevels;
/* aget v6, v6, v5 */
/* const/high16 v7, 0x41c00000 # 24.0f */
/* mul-float/2addr v6, v7 */
v8 = this.mBLevels;
/* aget v8, v8, v5 */
/* .line 447 */
v9 = /* invoke-direct {p0, v4, v5}, Lcom/android/server/display/RampRateController;->getTime(FI)F */
/* mul-float/2addr v9, v7 */
v7 = android.util.MathUtils .pow ( v8,v9 );
/* mul-float/2addr v6, v7 */
v7 = this.mBLevels;
/* aget v7, v7, v5 */
/* .line 448 */
v7 = android.util.MathUtils .log ( v7 );
/* mul-float/2addr v6, v7 */
/* .line 446 */
v6 = android.util.MathUtils .abs ( v6 );
/* .line 449 */
/* .local v6, "rate":F */
/* iget v7, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F */
/* cmpl-float v7, v4, v7 */
/* if-lez v7, :cond_2 */
/* .line 450 */
v1 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( v1 );
/* .line 451 */
/* .local v1, "maxBrightnessInt":I */
/* sub-int v7, v1, v3 */
v7 = com.android.internal.display.BrightnessSynchronizer .brightnessIntToFloat ( v7 );
/* mul-float/2addr v7, v6 */
/* iget v8, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F */
/* iget v9, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F */
/* sub-float/2addr v8, v9 */
/* div-float/2addr v7, v8 */
/* .line 453 */
} // .end local v1 # "maxBrightnessInt":I
} // .end local v6 # "rate":F
/* .local v7, "rate":F */
/* .line 454 */
} // .end local v7 # "rate":F
/* .restart local v6 # "rate":F */
} // :cond_2
/* mul-float v1, v6, v0 */
/* div-float v7, v1, v2 */
/* .line 456 */
} // .end local v6 # "rate":F
/* .restart local v7 # "rate":F */
} // :goto_1
/* const/16 v1, 0xfff */
/* const/high16 v6, 0x42200000 # 40.0f */
/* if-ge v3, v1, :cond_3 */
/* cmpg-float v1, v4, v6 */
/* if-gtz v1, :cond_3 */
/* .line 457 */
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mScreenBrightnessMaximumInt:I */
/* int-to-float v1, v1 */
/* div-float v7, v6, v1 */
/* .line 458 */
} // :cond_3
/* sget-boolean v1, Lcom/android/server/display/RampRateController;->IS_UMI_0B_DISPLAY_PANEL:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* cmpg-float v1, v4, v6 */
/* if-gtz v1, :cond_4 */
/* .line 459 */
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mScreenBrightnessMaximumInt:I */
/* int-to-float v1, v1 */
/* const/high16 v6, 0x42a00000 # 80.0f */
/* div-float v7, v6, v1 */
/* .line 461 */
} // :cond_4
} // :goto_2
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mMinimumAdjustRate:F */
v1 = android.util.MathUtils .max ( v7,v1 );
/* .line 462 */
} // .end local v7 # "rate":F
/* .local v1, "rate":F */
/* iget-boolean v6, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 463 */
v6 = com.android.server.display.RampRateController.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "getDarkeningRate: rate: "; // const-string v8, "getDarkeningRate: rate: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v8 = ", nit: "; // const-string v8, ", nit: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 465 */
} // :cond_5
} // .end method
private Float getExpRate ( Float p0, Float p1, Float p2, Float p3, Float p4 ) {
/* .locals 5 */
/* .param p1, "startNit" # F */
/* .param p2, "targetNit" # F */
/* .param p3, "currentNit" # F */
/* .param p4, "startTime" # F */
/* .param p5, "targetTime" # F */
/* .line 429 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F */
/* .line 430 */
/* .local v0, "beginDbv":F */
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F */
/* .line 431 */
/* .local v1, "endDbv":F */
v2 = /* invoke-direct {p0, p3}, Lcom/android/server/display/RampRateController;->convertToBrightness(F)F */
/* .line 432 */
/* .local v2, "curDbv":F */
/* div-float v3, v1, v0 */
v3 = android.util.MathUtils .log ( v3 );
/* sub-float v4, p5, p4 */
/* div-float/2addr v3, v4 */
/* .line 433 */
/* .local v3, "a":F */
/* mul-float v4, v3, v2 */
} // .end method
private Integer getIndex ( Float p0 ) {
/* .locals 4 */
/* .param p1, "nit" # F */
/* .line 469 */
int v0 = 1; // const/4 v0, 0x1
/* .line 470 */
/* .local v0, "index":I */
} // :goto_0
v1 = this.mNitsLevels;
/* array-length v2, v1 */
/* if-le v2, v0, :cond_0 */
/* aget v1, v1, v0 */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_0 */
/* .line 471 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 473 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 474 */
v1 = com.android.server.display.RampRateController.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getIndex: nit: "; // const-string v3, "getIndex: nit: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ", index: "; // const-string v3, ", index: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 476 */
} // :cond_1
/* add-int/lit8 v1, v0, -0x1 */
} // .end method
private Float getRampRate ( Integer p0, Boolean p1, Float p2, Float p3, Float p4, Float p5 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "increase" # Z */
/* .param p3, "startBrightness" # F */
/* .param p4, "currentBrightness" # F */
/* .param p5, "targetBrightness" # F */
/* .param p6, "rate" # F */
/* .line 508 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/RampRateController;->getRateDuration(I)F */
/* .line 509 */
/* .local v0, "duration":F */
v1 = java.lang.Float .isNaN ( v0 );
/* if-nez v1, :cond_2 */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, p5, v1 */
/* if-nez v1, :cond_0 */
/* .line 515 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 516 */
v1 = /* invoke-direct {p0, p3, p4, p5, v0}, Lcom/android/server/display/RampRateController;->getBrighteningLogRate(FFFF)F */
/* .line 518 */
} // :cond_1
v1 = /* invoke-direct {p0, p3, p4, p5, v0}, Lcom/android/server/display/RampRateController;->getDarkeningExpRate(FFFF)F */
/* .line 510 */
} // :cond_2
} // :goto_0
/* iget-boolean v1, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 511 */
v1 = com.android.server.display.RampRateController.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getRampRate: rate: "; // const-string v3, "getRampRate: rate: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p6 ); // invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 513 */
} // :cond_3
} // .end method
private Float getRampRate ( Boolean p0, Float p1, Float p2, Float p3 ) {
/* .locals 1 */
/* .param p1, "increase" # Z */
/* .param p2, "startBrightness" # F */
/* .param p3, "currentBrightness" # F */
/* .param p4, "targetBrightness" # F */
/* .line 375 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 376 */
v0 = /* invoke-direct {p0, p3, p2, p4}, Lcom/android/server/display/RampRateController;->getBrighteningRate(FFF)F */
/* .line 378 */
} // :cond_0
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/RampRateController;->getDarkeningRate(F)F */
} // .end method
private Float getRateDuration ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 553 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 564 */
/* const/high16 v0, 0x7fc00000 # Float.NaN */
/* .line 559 */
/* :pswitch_0 */
/* iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationHdrBrightness:I */
/* int-to-float v0, v0 */
/* .line 561 */
/* :pswitch_1 */
/* iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationManualBrightness:I */
/* int-to-float v0, v0 */
/* .line 557 */
/* :pswitch_2 */
/* iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationTemporaryDimming:I */
/* int-to-float v0, v0 */
/* .line 555 */
/* :pswitch_3 */
/* iget v0, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDim:I */
/* int-to-float v0, v0 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Float getTime ( Float p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "nit" # F */
/* .param p2, "index" # I */
/* .line 480 */
v0 = this.mALevels;
/* aget v0, v0, p2 */
/* .line 481 */
/* .local v0, "a":F */
v1 = this.mBLevels;
/* aget v1, v1, p2 */
/* .line 482 */
/* .local v1, "b":F */
/* div-float v2, p1, v0 */
v2 = android.util.MathUtils .log ( v2 );
v3 = android.util.MathUtils .log ( v1 );
/* div-float/2addr v2, v3 */
/* const/high16 v3, 0x41c00000 # 24.0f */
/* div-float/2addr v2, v3 */
/* .line 483 */
/* .local v2, "time":F */
/* iget-boolean v3, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 484 */
v3 = com.android.server.display.RampRateController.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getTime: time: "; // const-string v5, "getTime: time: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", a: "; // const-string v5, ", a: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", b: "; // const-string v5, ", b: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 486 */
} // :cond_0
v3 = android.util.MathUtils .abs ( v2 );
} // .end method
private java.lang.String reasonToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "reason" # I */
/* .line 655 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 683 */
/* const-string/jumbo v0, "unknown" */
/* .line 681 */
/* :pswitch_0 */
final String v0 = "doze_rate"; // const-string v0, "doze_rate"
/* .line 679 */
/* :pswitch_1 */
final String v0 = "bcbc_rate"; // const-string v0, "bcbc_rate"
/* .line 677 */
/* :pswitch_2 */
/* const-string/jumbo v0, "thermal_rate" */
/* .line 675 */
/* :pswitch_3 */
final String v0 = "manual_brightness_rate"; // const-string v0, "manual_brightness_rate"
/* .line 673 */
/* :pswitch_4 */
final String v0 = "auto_brightness_rate"; // const-string v0, "auto_brightness_rate"
/* .line 671 */
/* :pswitch_5 */
final String v0 = "fast_brightness_adj"; // const-string v0, "fast_brightness_adj"
/* .line 669 */
/* :pswitch_6 */
final String v0 = "hdr_brightness_rate"; // const-string v0, "hdr_brightness_rate"
/* .line 667 */
/* :pswitch_7 */
final String v0 = "override_brightness_rate"; // const-string v0, "override_brightness_rate"
/* .line 665 */
/* :pswitch_8 */
/* const-string/jumbo v0, "temporary_brightness_rate" */
/* .line 663 */
/* :pswitch_9 */
final String v0 = "dim_rate"; // const-string v0, "dim_rate"
/* .line 661 */
/* :pswitch_a */
final String v0 = "fast_rate"; // const-string v0, "fast_rate"
/* .line 659 */
/* :pswitch_b */
/* const-string/jumbo v0, "slow_fast" */
/* .line 657 */
/* :pswitch_c */
/* const-string/jumbo v0, "zero_rate" */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void updateDeviceConfigData ( ) {
/* .locals 3 */
/* .line 490 */
v0 = this.mHighBrightnessModeData;
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.mDisplayPowerControllerImpl;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 491 */
/* iget v0, v0, Lcom/android/server/display/DisplayDeviceConfig$HighBrightnessModeData;->transitionPoint:F */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* iput v0, p0, Lcom/android/server/display/RampRateController;->mHbmTransitionPointNit:F */
/* .line 492 */
/* const/high16 v0, 0x3f800000 # 1.0f */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* iput v0, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F */
/* .line 493 */
v0 = this.mDisplayPowerControllerImpl;
v0 = (( com.android.server.display.DisplayPowerControllerImpl ) v0 ).getMaxHbmBrightnessForPeak ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->getMaxHbmBrightnessForPeak()F
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/display/RampRateController;->convertToNit(F)F */
/* iput v0, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F */
/* .line 495 */
} // :cond_0
v0 = this.mDisplayDeviceConfig;
(( com.android.server.display.DisplayDeviceConfig ) v0 ).getNits ( ); // invoke-virtual {v0}, Lcom/android/server/display/DisplayDeviceConfig;->getNits()[F
/* .line 496 */
/* .local v0, "nits":[F */
if ( v0 != null) { // if-eqz v0, :cond_1
/* array-length v1, v0 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 497 */
/* array-length v1, v0 */
/* add-int/lit8 v1, v1, -0x1 */
/* aget v1, v0, v1 */
/* iput v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F */
/* .line 499 */
} // :cond_1
/* const/high16 v1, 0x43fa0000 # 500.0f */
/* iput v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F */
/* .line 500 */
v1 = com.android.server.display.RampRateController.TAG;
final String v2 = "The max nit of the device is not adapted."; // const-string v2, "The max nit of the device is not adapted."
android.util.Slog .e ( v1,v2 );
/* .line 502 */
} // :goto_0
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mBrightnessMaxNit:F */
/* iput v1, p0, Lcom/android/server/display/RampRateController;->mHbmMaxNit:F */
/* .line 504 */
} // .end local v0 # "nits":[F
} // :goto_1
return;
} // .end method
private void updateRatePriority ( com.android.server.display.brightness.BrightnessReason p0, com.android.server.display.brightness.BrightnessReason p1, Float p2, Float p3, Boolean p4, Boolean p5 ) {
/* .locals 20 */
/* .param p1, "reasonTemp" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .param p2, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .param p3, "targetBrightness" # F */
/* .param p4, "targetSdrBrightness" # F */
/* .param p5, "animating" # Z */
/* .param p6, "readyToAnimate" # Z */
/* .line 181 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p3 */
/* move/from16 v2, p4 */
/* iget v3, v0, Lcom/android/server/display/RampRateController;->mCurrentPolicy:I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 3; // const/4 v6, 0x3
/* if-ne v3, v6, :cond_0 */
/* move v7, v5 */
} // :cond_0
int v7 = 0; // const/4 v7, 0x0
/* .line 182 */
/* .local v7, "isPolicyBright":Z */
} // :goto_0
int v8 = 2; // const/4 v8, 0x2
/* if-ne v3, v8, :cond_1 */
/* move v3, v5 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 183 */
/* .local v3, "isPolicyDim":Z */
} // :goto_1
v9 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/brightness/BrightnessReason;->getReason()I */
/* if-ne v9, v5, :cond_2 */
/* move v9, v5 */
} // :cond_2
int v9 = 0; // const/4 v9, 0x0
/* .line 184 */
/* .local v9, "isReasonManual":Z */
} // :goto_2
v10 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I */
/* const/16 v11, 0x10 */
/* and-int/2addr v10, v11 */
if ( v10 != null) { // if-eqz v10, :cond_3
/* move v10, v5 */
} // :cond_3
int v10 = 0; // const/4 v10, 0x0
/* .line 185 */
/* .local v10, "isModifierBcbc":Z */
} // :goto_3
v12 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I */
/* and-int/2addr v12, v11 */
if ( v12 != null) { // if-eqz v12, :cond_4
/* move v12, v5 */
} // :cond_4
int v12 = 0; // const/4 v12, 0x0
/* .line 186 */
/* .local v12, "isModifierTempBcbc":Z */
} // :goto_4
v13 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I */
/* const/16 v14, 0x40 */
/* and-int/2addr v13, v14 */
if ( v13 != null) { // if-eqz v13, :cond_5
/* move v13, v5 */
} // :cond_5
int v13 = 0; // const/4 v13, 0x0
/* .line 187 */
/* .local v13, "isModifierThermal":Z */
} // :goto_5
v15 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I */
/* and-int/2addr v15, v14 */
if ( v15 != null) { // if-eqz v15, :cond_6
/* move v15, v5 */
} // :cond_6
int v15 = 0; // const/4 v15, 0x0
/* .line 188 */
/* .local v15, "isModifierTempThermal":Z */
} // :goto_6
v16 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/brightness/BrightnessReason;->getModifier()I */
int v4 = 4; // const/4 v4, 0x4
/* and-int/lit8 v16, v16, 0x4 */
if ( v16 != null) { // if-eqz v16, :cond_7
/* move/from16 v16, v5 */
} // :cond_7
/* const/16 v16, 0x0 */
/* .line 189 */
/* .local v16, "isModifierTempHdr":Z */
} // :goto_7
v14 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/brightness/BrightnessReason;->getReason()I */
int v4 = 6; // const/4 v4, 0x6
/* if-ne v14, v4, :cond_8 */
/* move v4, v5 */
} // :cond_8
int v4 = 0; // const/4 v4, 0x0
/* .line 190 */
/* .local v4, "isReasonTempOverride":Z */
} // :goto_8
v14 = /* invoke-virtual/range {p1 ..p1}, Lcom/android/server/display/brightness/BrightnessReason;->getReason()I */
int v11 = 7; // const/4 v11, 0x7
/* if-ne v14, v11, :cond_9 */
/* move/from16 v17, v5 */
} // :cond_9
/* const/16 v17, 0x0 */
} // :goto_9
/* move/from16 v11, v17 */
/* .line 192 */
/* .local v11, "isReasonTemporary":Z */
if ( v7 != null) { // if-eqz v7, :cond_a
/* if-nez v12, :cond_b */
/* if-nez v10, :cond_b */
} // :cond_a
v14 = this.mRateRecord;
/* .line 194 */
v14 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedBcbcDimming ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_e
/* .line 195 */
} // :cond_b
/* iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z */
/* if-nez v14, :cond_c */
/* if-nez p5, :cond_c */
/* .line 196 */
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v14,v5 );
/* .line 197 */
} // :cond_c
v14 = this.mRateRecord;
v14 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedBcbcDimming ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_e
/* iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z */
/* if-nez v14, :cond_d */
if ( v9 != null) { // if-eqz v9, :cond_e
/* .line 199 */
} // :cond_d
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v14,v5 );
/* .line 203 */
} // :cond_e
} // :goto_a
if ( v7 != null) { // if-eqz v7, :cond_f
/* if-nez v15, :cond_10 */
/* if-nez v13, :cond_10 */
} // :cond_f
v14 = this.mRateRecord;
/* .line 205 */
v14 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedThermalDimming ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_13
/* .line 206 */
} // :cond_10
/* iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z */
/* if-nez v14, :cond_11 */
/* iget-boolean v14, v0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z */
/* if-nez v14, :cond_11 */
/* if-nez p5, :cond_11 */
/* .line 207 */
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v14,v5 );
/* .line 208 */
v5 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v5,v8 );
/* .line 209 */
} // :cond_11
v5 = this.mRateRecord;
v5 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedThermalDimming ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_13
/* iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z */
/* if-nez v5, :cond_12 */
/* iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z */
if ( v5 != null) { // if-eqz v5, :cond_13
/* .line 211 */
} // :cond_12
v5 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v5,v8 );
/* .line 215 */
} // :cond_13
} // :goto_b
if ( v7 != null) { // if-eqz v7, :cond_15
v5 = this.mRateRecord;
/* .line 216 */
v5 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedHdrDimming ( v5 );
/* if-nez v5, :cond_14 */
if ( v16 != null) { // if-eqz v16, :cond_15
/* iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z */
if ( v5 != null) { // if-eqz v5, :cond_15
/* .line 218 */
} // :cond_14
v5 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v5,v6 );
/* .line 220 */
v5 = this.mRateRecord;
/* const/16 v6, 0x8 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v5,v6 );
/* .line 223 */
} // :cond_15
if ( v7 != null) { // if-eqz v7, :cond_17
v5 = this.mRateRecord;
/* .line 224 */
v5 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedOverrideDimming ( v5 );
/* if-nez v5, :cond_16 */
if ( v4 != null) { // if-eqz v4, :cond_17
/* iget-boolean v5, v0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z */
if ( v5 != null) { // if-eqz v5, :cond_17
/* .line 226 */
} // :cond_16
v5 = this.mRateRecord;
/* const/16 v6, 0xb */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v5,v6 );
/* .line 229 */
v5 = this.mRateRecord;
/* const/16 v6, 0x10 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v5,v6 );
/* .line 232 */
} // :cond_17
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v5 */
/* .line 233 */
/* .local v5, "currentTime":J */
if ( v7 != null) { // if-eqz v7, :cond_18
/* iget v14, v0, Lcom/android/server/display/RampRateController;->mPreviousDisplayState:I */
/* .line 234 */
v14 = android.view.Display .isDozeState ( v14 );
if ( v14 != null) { // if-eqz v14, :cond_18
/* iget v14, v0, Lcom/android/server/display/RampRateController;->mBrightness:F */
/* cmpl-float v14, v1, v14 */
/* if-gtz v14, :cond_19 */
} // :cond_18
v14 = this.mRateRecord;
/* .line 236 */
v14 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedDozeDimming ( v14,v5,v6 );
if ( v14 != null) { // if-eqz v14, :cond_1a
/* .line 237 */
} // :cond_19
v14 = this.mRateRecord;
/* const/16 v8, 0x1b */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v14,v8 );
/* .line 241 */
if ( p6 != null) { // if-eqz p6, :cond_1b
/* .line 242 */
v8 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetDozeDimmingTimeMills ( v8,v5,v6 );
/* .line 243 */
v8 = this.mRateRecord;
int v14 = 4; // const/4 v14, 0x4
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v8,v14 );
/* .line 245 */
} // :cond_1a
int v14 = 4; // const/4 v14, 0x4
v8 = this.mRateRecord;
v8 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedDozeDimming ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_1b
/* .line 246 */
v8 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v8,v14 );
/* .line 249 */
} // :cond_1b
} // :goto_c
if ( v7 != null) { // if-eqz v7, :cond_1c
/* if-nez v11, :cond_1d */
} // :cond_1c
v8 = this.mRateRecord;
/* .line 250 */
v8 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedTemporaryDimming ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_20
/* .line 251 */
} // :cond_1d
/* move v8, v4 */
/* move-wide/from16 v18, v5 */
} // .end local v4 # "isReasonTempOverride":Z
} // .end local v5 # "currentTime":J
/* .local v8, "isReasonTempOverride":Z */
/* .local v18, "currentTime":J */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
/* .line 252 */
/* .local v4, "now":J */
v6 = this.mRateRecord;
/* const/16 v14, 0x1f */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v6,v14 );
/* .line 257 */
/* iget-boolean v6, v0, Lcom/android/server/display/RampRateController;->mSupportManualDimming:Z */
if ( v6 != null) { // if-eqz v6, :cond_21
/* .line 258 */
/* const/16 v6, 0x20 */
/* if-nez v11, :cond_1f */
/* .line 259 */
v14 = this.mRateRecord;
v14 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedTemporaryDimming ( v14,v4,v5,v1,v2 );
if ( v14 != null) { // if-eqz v14, :cond_1e
/* .line 261 */
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v14,v6 );
/* .line 263 */
} // :cond_1e
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v14,v6 );
/* .line 266 */
} // :cond_1f
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetStartTemporaryDimmingTimeMills ( v14,v4,v5 );
/* .line 267 */
v14 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v14,v6 );
/* .line 250 */
} // .end local v8 # "isReasonTempOverride":Z
} // .end local v18 # "currentTime":J
/* .local v4, "isReasonTempOverride":Z */
/* .restart local v5 # "currentTime":J */
} // :cond_20
/* move v8, v4 */
/* move-wide/from16 v18, v5 */
/* .line 272 */
} // .end local v4 # "isReasonTempOverride":Z
} // .end local v5 # "currentTime":J
/* .restart local v8 # "isReasonTempOverride":Z */
/* .restart local v18 # "currentTime":J */
} // :cond_21
} // :goto_d
/* if-nez v3, :cond_23 */
/* iget v4, v0, Lcom/android/server/display/RampRateController;->mPreviousPolicy:I */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v5, :cond_22 */
if ( v7 != null) { // if-eqz v7, :cond_22
/* .line 281 */
} // :cond_22
v4 = this.mRateRecord;
v4 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedDimDimming ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_24
/* .line 282 */
v4 = this.mRateRecord;
/* const/16 v5, 0x40 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v4,v5 );
/* .line 274 */
} // :cond_23
} // :goto_e
v4 = this.mRateRecord;
/* const/16 v5, 0x3f */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v4,v5 );
/* .line 280 */
v4 = this.mRateRecord;
/* const/16 v5, 0x40 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v4,v5 );
/* .line 285 */
} // :cond_24
} // :goto_f
int v4 = 0; // const/4 v4, 0x0
/* cmpl-float v5, v1, v4 */
if ( v5 != null) { // if-eqz v5, :cond_25
/* cmpl-float v5, v2, v4 */
if ( v5 != null) { // if-eqz v5, :cond_25
/* iget v5, v0, Lcom/android/server/display/RampRateController;->mBrightness:F */
/* cmpl-float v5, v5, v4 */
if ( v5 != null) { // if-eqz v5, :cond_25
/* iget v5, v0, Lcom/android/server/display/RampRateController;->mSdrBrightness:F */
/* cmpl-float v4, v5, v4 */
/* if-nez v4, :cond_26 */
/* .line 287 */
} // :cond_25
v4 = this.mRateRecord;
/* const/16 v5, 0x7f */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v4,v5 );
/* .line 289 */
} // :cond_26
return;
} // .end method
/* # virtual methods */
protected void addHdrRateModifier ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEntry" # Z */
/* .line 635 */
v0 = this.mRateRecord;
/* const/16 v1, 0x7f */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 636 */
v0 = this.mRateRecord;
/* const/16 v1, 0x8 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v0,v1 );
/* .line 637 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 638 */
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToEntry:Z */
/* .line 640 */
} // :cond_0
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToExit:Z */
/* .line 642 */
} // :goto_0
return;
} // .end method
protected void addOverrideRateModifier ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEntry" # Z */
/* .line 645 */
v0 = this.mRateRecord;
/* const/16 v1, 0x7f */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 646 */
v0 = this.mRateRecord;
/* const/16 v1, 0x10 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$maddRateModifier ( v0,v1 );
/* .line 647 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 648 */
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToEntry:Z */
/* .line 650 */
} // :cond_0
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToExit:Z */
/* .line 652 */
} // :goto_0
return;
} // .end method
protected void clearAllRateModifier ( ) {
/* .locals 2 */
/* .line 631 */
v0 = this.mRateRecord;
/* const/16 v1, 0x7f */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 632 */
return;
} // .end method
protected void clearBcbcModifier ( ) {
/* .locals 2 */
/* .line 615 */
v0 = this.mRateRecord;
int v1 = 1; // const/4 v1, 0x1
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 616 */
return;
} // .end method
protected void clearThermalModifier ( ) {
/* .locals 2 */
/* .line 619 */
v0 = this.mRateRecord;
int v1 = 2; // const/4 v1, 0x2
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mclearRateModifier ( v0,v1 );
/* .line 620 */
return;
} // .end method
protected void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 688 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DPC:Z */
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
/* .line 689 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 690 */
final String v0 = "Ramp Rate Controller:"; // const-string v0, "Ramp Rate Controller:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 691 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mRateType="; // const-string v1, " mRateType="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRateRecord;
v1 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetRateType ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 692 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentRateReason="; // const-string v1, " mCurrentRateReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$fgetSCREEN_BRIGHTNESS_FLOAT_NAME ( v1 );
/* .line 693 */
v1 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetCurrentRateReason ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 692 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 694 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mPreviousRateReason="; // const-string v1, " mPreviousRateReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$fgetSCREEN_BRIGHTNESS_FLOAT_NAME ( v1 );
/* .line 695 */
v1 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetPreviousRateReason ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 694 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 696 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentSdrRateReason="; // const-string v1, " mCurrentSdrRateReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$fgetSCREEN_SDR_BRIGHTNESS_FLOAT_NAME ( v1 );
/* .line 697 */
v1 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetCurrentRateReason ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 696 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 698 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mPreviousSdrRateReason="; // const-string v1, " mPreviousSdrRateReason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$fgetSCREEN_SDR_BRIGHTNESS_FLOAT_NAME ( v1 );
/* .line 699 */
v1 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetPreviousRateReason ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 698 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 700 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mRateModifier="; // const-string v1, " mRateModifier="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRateRecord;
v1 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetRateModifier ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 701 */
return;
} // .end method
protected Boolean isTemporaryDimming ( ) {
/* .locals 1 */
/* .line 611 */
v0 = this.mRateRecord;
v0 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedTemporaryDimming ( v0 );
} // .end method
protected void onAnimateValueChanged ( Boolean p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "changed" # Z */
/* .param p2, "isAnimating" # Z */
/* .line 595 */
/* if-nez p1, :cond_1 */
/* if-nez p2, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToEntry:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToExit:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = this.mRateRecord;
/* .line 598 */
v0 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedHdrDimming ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 599 */
/* invoke-direct {p0}, Lcom/android/server/display/RampRateController;->clearHdrRateModifier()V */
/* .line 601 */
} // :cond_1
/* iput-boolean p1, p0, Lcom/android/server/display/RampRateController;->mAnimateValueChanged:Z */
/* .line 602 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToEntry:Z */
/* .line 603 */
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedHdrRateDueToExit:Z */
/* .line 604 */
return;
} // .end method
protected void onBrightnessChanged ( Boolean p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "changed" # Z */
/* .param p2, "isAnimating" # Z */
/* .line 583 */
/* if-nez p1, :cond_1 */
/* if-nez p2, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToEntry:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToExit:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
v0 = this.mRateRecord;
/* .line 586 */
v0 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedOverrideDimming ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 587 */
/* invoke-direct {p0}, Lcom/android/server/display/RampRateController;->clearOverrideRateModifier()V */
/* .line 589 */
} // :cond_1
/* iput-boolean p1, p0, Lcom/android/server/display/RampRateController;->mBrightnessChanged:Z */
/* .line 590 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToEntry:Z */
/* .line 591 */
/* iput-boolean v0, p0, Lcom/android/server/display/RampRateController;->mAppliedOverrideRateDueToExit:Z */
/* .line 592 */
return;
} // .end method
protected Float updateBrightnessRate ( java.lang.String p0, Float p1, Float p2, Float p3 ) {
/* .locals 11 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "currentBrightness" # F */
/* .param p3, "targetBrightness" # F */
/* .param p4, "rate" # F */
/* .line 293 */
/* move v0, p4 */
/* .line 294 */
/* .local v0, "finalRate":F */
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mBrightness:F */
/* .line 295 */
/* .local v1, "startBrightness":F */
v2 = com.android.server.display.DisplayPowerState.SCREEN_SDR_BRIGHTNESS_FLOAT;
(( android.util.FloatProperty ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/util/FloatProperty;->getName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 296 */
/* iget v1, p0, Lcom/android/server/display/RampRateController;->mSdrBrightness:F */
/* .line 298 */
} // :cond_0
/* cmpl-float v2, p3, v1 */
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* if-lez v2, :cond_1 */
/* move v2, v3 */
} // :cond_1
/* move v2, v4 */
} // :goto_0
/* move v9, v2 */
/* .line 299 */
/* .local v9, "increase":Z */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mresetRateType ( v2 );
/* .line 300 */
v2 = this.mRateRecord;
v5 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetCurrentRateReason ( v2,p1 );
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetPreviousRateReason ( v2,v5,p1 );
/* .line 301 */
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z */
int v5 = 2; // const/4 v5, 0x2
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 302 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 304 */
} // :cond_2
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v5,p1 );
/* .line 307 */
} // :goto_1
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedDimDimming ( v2 );
int v6 = 0; // const/4 v6, 0x0
int v7 = 3; // const/4 v7, 0x3
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 308 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateTypeIfNeeded ( v2,v3 );
/* .line 309 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateReasonIfNeeded ( v2,v7,p1 );
/* goto/16 :goto_2 */
/* .line 310 */
} // :cond_3
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedDozeDimming ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 311 */
v2 = this.mRateRecord;
/* const/16 v3, 0xc */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 312 */
/* sub-float v2, p3, v1 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* mul-float/2addr v2, v3 */
/* iget v3, p0, Lcom/android/server/display/RampRateController;->mAnimationDurationDozeDimming:I */
/* int-to-float v3, v3 */
/* div-float v0, v2, v3 */
/* goto/16 :goto_2 */
/* .line 313 */
} // :cond_4
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedTemporaryDimming ( v2 );
int v3 = 4; // const/4 v3, 0x4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 314 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v4,p1 );
/* .line 315 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateTypeIfNeeded ( v2,v5 );
/* .line 316 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateReasonIfNeeded ( v2,v3,p1 );
/* goto/16 :goto_2 */
/* .line 317 */
} // :cond_5
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedOverrideDimming ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 318 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateTypeIfNeeded ( v2,v7 );
/* .line 319 */
v2 = this.mRateRecord;
int v3 = 5; // const/4 v3, 0x5
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateReasonIfNeeded ( v2,v3,p1 );
/* goto/16 :goto_2 */
/* .line 320 */
} // :cond_6
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedHdrDimming ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 321 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateType ( v2,v3 );
/* .line 322 */
v2 = this.mRateRecord;
int v3 = 6; // const/4 v3, 0x6
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 323 */
} // :cond_7
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedThermalDimming ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 324 */
v2 = this.mRateRecord;
/* const/16 v3, 0xa */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 325 */
/* iget v0, p0, Lcom/android/server/display/RampRateController;->mSlowLinearRampRate:F */
/* .line 326 */
} // :cond_8
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z */
if ( v2 != null) { // if-eqz v2, :cond_9
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mappliedBcbcDimming ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 327 */
v2 = this.mRateRecord;
/* const/16 v3, 0xb */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 328 */
/* iget v0, p0, Lcom/android/server/display/RampRateController;->mSlowLinearRampRate:F */
/* .line 329 */
} // :cond_9
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z */
if ( v2 != null) { // if-eqz v2, :cond_a
v2 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/display/RampRateController;->appliedFastRate(FF)Z */
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 330 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateTypeIfNeeded ( v2,v7 );
/* .line 331 */
v2 = this.mRateRecord;
int v3 = 7; // const/4 v3, 0x7
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 332 */
} // :cond_a
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z */
if ( v2 != null) { // if-eqz v2, :cond_b
/* cmpl-float v2, v0, v6 */
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 333 */
v2 = this.mRateRecord;
/* const/16 v3, 0x8 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v3,p1 );
/* .line 334 */
v0 = /* invoke-direct {p0, v9, v1, p2, p3}, Lcom/android/server/display/RampRateController;->getRampRate(ZFFF)F */
/* .line 335 */
} // :cond_b
/* cmpl-float v2, v0, v6 */
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 336 */
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateTypeIfNeeded ( v2,v7 );
/* .line 337 */
v2 = this.mRateRecord;
/* const/16 v3, 0x9 */
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetRateReasonIfNeeded ( v2,v3,p1 );
/* .line 339 */
} // :cond_c
v2 = this.mRateRecord;
com.android.server.display.RampRateController$RateStateRecord .-$$Nest$msetCurrentRateReason ( v2,v4,p1 );
/* .line 344 */
} // :goto_2
v2 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p2 );
/* .line 345 */
v3 = com.android.internal.display.BrightnessSynchronizer .brightnessFloatToInt ( p3 );
/* const-string/jumbo v10, "updateBrightnessRate: " */
/* if-ne v2, v3, :cond_f */
/* .line 346 */
/* cmpl-float v2, v0, v6 */
if ( v2 != null) { // if-eqz v2, :cond_f
/* .line 347 */
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mSlowChange:Z */
if ( v2 != null) { // if-eqz v2, :cond_d
/* iget v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateSlow:F */
} // :cond_d
/* iget v2, p0, Lcom/android/server/display/RampRateController;->mBrightnessRampRateFast:F */
} // :goto_3
/* move v0, v2 */
/* .line 348 */
/* iget-boolean v2, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v2 != null) { // if-eqz v2, :cond_e
/* .line 349 */
v2 = com.android.server.display.RampRateController.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": using current rate to avoid frequent animation execution: rate: "; // const-string v4, ": using current rate to avoid frequent animation execution: rate: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", currentBrightness: "; // const-string v4, ", currentBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", targetBrightness: "; // const-string v4, ", targetBrightness: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 355 */
} // :cond_e
/* .line 359 */
} // :cond_f
v2 = this.mRateRecord;
v3 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetRateType ( v2 );
/* move-object v2, p0 */
/* move v4, v9 */
/* move v5, v1 */
/* move v6, p2 */
/* move v7, p3 */
/* move v8, v0 */
v0 = /* invoke-direct/range {v2 ..v8}, Lcom/android/server/display/RampRateController;->getRampRate(IZFFFF)F */
/* .line 361 */
v2 = this.mRateRecord;
v2 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetPreviousRateReason ( v2,p1 );
/* .line 362 */
/* .local v2, "previousRateReason":I */
v3 = this.mRateRecord;
v3 = com.android.server.display.RampRateController$RateStateRecord .-$$Nest$mgetCurrentRateReason ( v3,p1 );
/* .line 363 */
/* .local v3, "currentRateReason":I */
/* if-ne v3, v2, :cond_10 */
/* iget-boolean v4, p0, Lcom/android/server/display/RampRateController;->mDebug:Z */
if ( v4 != null) { // if-eqz v4, :cond_11
/* .line 364 */
} // :cond_10
v4 = com.android.server.display.RampRateController.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ": brightness rate changing from ["; // const-string v6, ": brightness rate changing from ["
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 366 */
/* invoke-direct {p0, v2}, Lcom/android/server/display/RampRateController;->reasonToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "] to ["; // const-string v6, "] to ["
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 367 */
/* invoke-direct {p0, v3}, Lcom/android/server/display/RampRateController;->reasonToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "], rate: "; // const-string v6, "], rate: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 364 */
android.util.Slog .d ( v4,v5 );
/* .line 370 */
} // :cond_11
} // .end method
protected void updateBrightnessState ( Boolean p0, Boolean p1, Boolean p2, Float p3, Float p4, Float p5, Float p6, Integer p7, Integer p8, Integer p9, com.android.server.display.brightness.BrightnessReason p10, com.android.server.display.brightness.BrightnessReason p11 ) {
/* .locals 16 */
/* .param p1, "animating" # Z */
/* .param p2, "readyToAnimate" # Z */
/* .param p3, "slowChange" # Z */
/* .param p4, "brightness" # F */
/* .param p5, "sdrBrightness" # F */
/* .param p6, "targetBrightness" # F */
/* .param p7, "targetSdrBrightness" # F */
/* .param p8, "currentPolicy" # I */
/* .param p9, "previousPolicy" # I */
/* .param p10, "oldDisplayState" # I */
/* .param p11, "reasonTemp" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .param p12, "reason" # Lcom/android/server/display/brightness/BrightnessReason; */
/* .line 164 */
/* move-object/from16 v7, p0 */
/* move/from16 v8, p3 */
/* iput-boolean v8, v7, Lcom/android/server/display/RampRateController;->mSlowChange:Z */
/* .line 165 */
/* move/from16 v9, p4 */
/* iput v9, v7, Lcom/android/server/display/RampRateController;->mBrightness:F */
/* .line 166 */
/* move/from16 v10, p5 */
/* iput v10, v7, Lcom/android/server/display/RampRateController;->mSdrBrightness:F */
/* .line 167 */
/* move/from16 v11, p6 */
/* iput v11, v7, Lcom/android/server/display/RampRateController;->mTargetBrightness:F */
/* .line 168 */
/* move/from16 v12, p7 */
/* iput v12, v7, Lcom/android/server/display/RampRateController;->mTargetSdrBrightness:F */
/* .line 169 */
/* move/from16 v13, p8 */
/* iput v13, v7, Lcom/android/server/display/RampRateController;->mCurrentPolicy:I */
/* .line 170 */
/* move/from16 v14, p9 */
/* iput v14, v7, Lcom/android/server/display/RampRateController;->mPreviousPolicy:I */
/* .line 171 */
/* move/from16 v15, p10 */
/* iput v15, v7, Lcom/android/server/display/RampRateController;->mPreviousDisplayState:I */
/* .line 172 */
v0 = this.mBrightnessReasonTemp;
/* move-object/from16 v6, p11 */
(( com.android.server.display.brightness.BrightnessReason ) v0 ).set ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/display/brightness/BrightnessReason;->set(Lcom/android/server/display/brightness/BrightnessReason;)V
/* .line 173 */
v0 = this.mBrightnessReason;
/* move-object/from16 v5, p12 */
(( com.android.server.display.brightness.BrightnessReason ) v0 ).set ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/display/brightness/BrightnessReason;->set(Lcom/android/server/display/brightness/BrightnessReason;)V
/* .line 174 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p11 */
/* move-object/from16 v2, p12 */
/* move/from16 v3, p6 */
/* move/from16 v4, p7 */
/* move/from16 v5, p1 */
/* move/from16 v6, p2 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/display/RampRateController;->updateRatePriority(Lcom/android/server/display/brightness/BrightnessReason;Lcom/android/server/display/brightness/BrightnessReason;FFZZ)V */
/* .line 176 */
return;
} // .end method
