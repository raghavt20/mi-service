public class com.android.server.display.DisplayAdapterImpl extends com.android.server.display.DisplayAdapterStub {
	 /* .source "DisplayAdapterImpl.java" */
	 /* # static fields */
	 private static final java.lang.String EXTERNAL_DISPLAY_CONNECTED;
	 /* # instance fields */
	 private android.app.ActivityManagerInternal mActivityManagerInternal;
	 private Boolean mSystemReady;
	 /* # direct methods */
	 public com.android.server.display.DisplayAdapterImpl ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Lcom/android/server/display/DisplayAdapterStub;-><init>()V */
		 return;
	 } // .end method
	 private Boolean isSystemReady ( ) {
		 /* .locals 1 */
		 /* .line 32 */
		 /* iget-boolean v0, p0, Lcom/android/server/display/DisplayAdapterImpl;->mSystemReady:Z */
		 /* if-nez v0, :cond_1 */
		 /* .line 33 */
		 v0 = this.mActivityManagerInternal;
		 /* if-nez v0, :cond_0 */
		 /* .line 34 */
		 /* const-class v0, Landroid/app/ActivityManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Landroid/app/ActivityManagerInternal; */
		 this.mActivityManagerInternal = v0;
		 /* .line 36 */
	 } // :cond_0
	 v0 = this.mActivityManagerInternal;
	 v0 = 	 (( android.app.ActivityManagerInternal ) v0 ).isSystemReady ( ); // invoke-virtual {v0}, Landroid/app/ActivityManagerInternal;->isSystemReady()Z
	 /* iput-boolean v0, p0, Lcom/android/server/display/DisplayAdapterImpl;->mSystemReady:Z */
	 /* .line 38 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/display/DisplayAdapterImpl;->mSystemReady:Z */
} // .end method
/* # virtual methods */
public void updateExternalDisplayStatus ( com.android.server.display.DisplayDevice p0, Integer p1, android.content.Context p2 ) {
/* .locals 3 */
/* .param p1, "device" # Lcom/android/server/display/DisplayDevice; */
/* .param p2, "event" # I */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 22 */
(( com.android.server.display.DisplayDevice ) p1 ).getDisplayDeviceInfoLocked ( ); // invoke-virtual {p1}, Lcom/android/server/display/DisplayDevice;->getDisplayDeviceInfoLocked()Lcom/android/server/display/DisplayDeviceInfo;
/* iget v0, v0, Lcom/android/server/display/DisplayDeviceInfo;->type:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_1 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/DisplayAdapterImpl;->isSystemReady()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 23 */
	 final String v0 = "external_display_connected"; // const-string v0, "external_display_connected"
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne p2, v1, :cond_0 */
	 /* .line 24 */
	 (( android.content.Context ) p3 ).getContentResolver ( ); // invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 android.provider.Settings$System .putInt ( v2,v0,v1 );
	 /* .line 25 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
/* if-ne p2, v1, :cond_1 */
/* .line 26 */
(( android.content.Context ) p3 ).getContentResolver ( ); // invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 0; // const/4 v2, 0x0
android.provider.Settings$System .putInt ( v1,v0,v2 );
/* .line 29 */
} // :cond_1
} // :goto_0
return;
} // .end method
