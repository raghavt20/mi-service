.class Lcom/android/server/display/RhythmicEyeCareManager$5;
.super Landroid/app/IProcessObserver$Stub;
.source "RhythmicEyeCareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/RhythmicEyeCareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/RhythmicEyeCareManager;


# direct methods
.method public static synthetic $r8$lambda$EeU80twLzppGHOM39SUzkgu3vOg(Lcom/android/server/display/RhythmicEyeCareManager$5;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager$5;->lambda$onProcessDied$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$NroLgTucSkuaNNJd6xlZ4aoamNg(Lcom/android/server/display/RhythmicEyeCareManager$5;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager$5;->lambda$onForegroundActivitiesChanged$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/RhythmicEyeCareManager;

    .line 105
    iput-object p1, p0, Lcom/android/server/display/RhythmicEyeCareManager$5;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method

.method private synthetic lambda$onForegroundActivitiesChanged$0()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$5;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$mupdateState(Lcom/android/server/display/RhythmicEyeCareManager;Z)V

    return-void
.end method

.method private synthetic lambda$onProcessDied$1()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$5;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$mupdateState(Lcom/android/server/display/RhythmicEyeCareManager;Z)V

    return-void
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foregroundActivities"    # Z

    .line 109
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$5;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v0}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmHandler(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$5$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$5$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager$5;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 110
    return-void
.end method

.method public onForegroundServicesChanged(III)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "serviceTypes"    # I

    .line 117
    return-void
.end method

.method public onProcessDied(II)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 121
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$5;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v0}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmHandler(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/display/RhythmicEyeCareManager$5$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/display/RhythmicEyeCareManager$5$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/display/RhythmicEyeCareManager$5;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 122
    return-void
.end method
