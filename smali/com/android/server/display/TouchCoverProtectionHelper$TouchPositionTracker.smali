.class final Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;
.super Ljava/lang/Object;
.source "TouchCoverProtectionHelper.java"

# interfaces
.implements Landroid/view/WindowManagerPolicyConstants$PointerEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/TouchCoverProtectionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TouchPositionTracker"
.end annotation


# instance fields
.field private volatile mIsTouchingInArea:Z

.field private volatile mLastObservedTouchTime:J

.field private mPointerIndexTriggerBitMask:I

.field final synthetic this$0:Lcom/android/server/display/TouchCoverProtectionHelper;


# direct methods
.method static bridge synthetic -$$Nest$fgetmLastObservedTouchTime(Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mLastObservedTouchTime:J

    return-wide v0
.end method

.method private constructor <init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->this$0:Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/display/TouchCoverProtectionHelper;Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;-><init>(Lcom/android/server/display/TouchCoverProtectionHelper;)V

    return-void
.end method

.method private updateTouchStatus(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 371
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 372
    .local v0, "pointerCount":I
    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 373
    .local v1, "pointerCoords":Landroid/view/MotionEvent$PointerCoords;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 374
    invoke-virtual {p1, v2, v1}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 375
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 376
    .local v3, "pointerId":I
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v4

    .line 377
    .local v4, "x":F
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v6

    .line 378
    .local v6, "y":F
    iget-object v7, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->this$0:Lcom/android/server/display/TouchCoverProtectionHelper;

    invoke-static {v7}, Lcom/android/server/display/TouchCoverProtectionHelper;->-$$Nest$fgetmTouchCoverProtectionRect(Lcom/android/server/display/TouchCoverProtectionHelper;)Landroid/graphics/Rect;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 379
    iget v7, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    shl-int/2addr v5, v3

    or-int/2addr v5, v7

    iput v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    goto :goto_1

    .line 381
    :cond_0
    iget v7, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    shl-int/2addr v5, v3

    not-int v5, v5

    and-int/2addr v5, v7

    iput v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    .line 373
    .end local v3    # "pointerId":I
    .end local v4    # "x":F
    .end local v6    # "y":F
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 384
    .end local v2    # "i":I
    :cond_1
    return-void
.end method


# virtual methods
.method isTouchingInArea()Z
    .locals 1

    .line 387
    iget-boolean v0, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mIsTouchingInArea:Z

    return v0
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 336
    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 337
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 338
    .local v0, "action":I
    and-int/lit16 v1, v0, 0xff

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 349
    :pswitch_1
    const v1, 0xff00

    and-int/2addr v1, v0

    shr-int/lit8 v1, v1, 0x8

    .line 350
    .local v1, "index":I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    .line 351
    .local v4, "pointerId":I
    iget v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    shl-int v6, v2, v4

    not-int v6, v6

    and-int/2addr v5, v6

    iput v5, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    .line 352
    goto :goto_0

    .line 346
    .end local v1    # "index":I
    .end local v4    # "pointerId":I
    :pswitch_2
    iput v3, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    .line 347
    goto :goto_0

    .line 342
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->updateTouchStatus(Landroid/view/MotionEvent;)V

    .line 343
    nop

    .line 358
    :goto_0
    iget v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:I

    if-eqz v1, :cond_0

    .line 359
    iput-boolean v2, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mIsTouchingInArea:Z

    .line 360
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mLastObservedTouchTime:J

    .line 361
    invoke-static {}, Lcom/android/server/display/TouchCoverProtectionHelper;->-$$Nest$sfgetDEBUG()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 362
    invoke-static {}, Lcom/android/server/display/TouchCoverProtectionHelper;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPointerEvent: touch events occurred in area"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 365
    :cond_0
    iput-boolean v3, p0, Lcom/android/server/display/TouchCoverProtectionHelper$TouchPositionTracker;->mIsTouchingInArea:Z

    .line 368
    .end local v0    # "action":I
    :cond_1
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
