.class Lcom/android/server/display/DisplayPowerControllerImpl$2;
.super Ljava/lang/Object;
.source "DisplayPowerControllerImpl.java"

# interfaces
.implements Lcom/android/server/display/HighBrightnessModeController$HdrStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/display/DisplayPowerControllerImpl;->init(Lcom/android/server/display/DisplayPowerController;Landroid/content/Context;Landroid/os/Looper;IFFLcom/android/server/display/DisplayDeviceConfig;Lcom/android/server/display/LogicalDisplay;Lcom/android/server/display/HighBrightnessModeController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayPowerControllerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/display/DisplayPowerControllerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DisplayPowerControllerImpl;

    .line 506
    iput-object p1, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHbmModeChanged(II)V
    .locals 2
    .param p1, "newMode"    # I
    .param p2, "oldMode"    # I

    .line 520
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmRampRateController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/RampRateController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 521
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmRampRateController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/RampRateController;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/display/RampRateController;->addHdrRateModifier(Z)V

    goto :goto_0

    .line 523
    :cond_0
    if-ne p2, v0, :cond_1

    .line 524
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmRampRateController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/RampRateController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/display/RampRateController;->addHdrRateModifier(Z)V

    .line 527
    :cond_1
    :goto_0
    return-void
.end method

.method public onHdrStateChanged(Z)V
    .locals 1
    .param p1, "isHdrLayerPresent"    # Z

    .line 509
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmAutoBrightnessEnable(Lcom/android/server/display/DisplayPowerControllerImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mresetBCBCState(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 511
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$mresetScreenGrayscaleState(Lcom/android/server/display/DisplayPowerControllerImpl;)V

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmThermalBrightnessController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/ThermalBrightnessController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/android/server/display/DisplayPowerControllerImpl$2;->this$0:Lcom/android/server/display/DisplayPowerControllerImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayPowerControllerImpl;->-$$Nest$fgetmThermalBrightnessController(Lcom/android/server/display/DisplayPowerControllerImpl;)Lcom/android/server/display/ThermalBrightnessController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/display/ThermalBrightnessController;->setHdrLayerPresent(Z)V

    .line 516
    :cond_1
    return-void
.end method
