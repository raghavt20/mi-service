.class public Lcom/android/server/display/AmbientLightRingBuffer;
.super Ljava/lang/Object;
.source "AmbientLightRingBuffer.java"


# static fields
.field private static final AMBIENT_LIGHT_PREDICTION_TIME_MILLIS:J = 0x64L

.field private static final BUFFER_SLACK:F = 1.5f


# instance fields
.field private mBrighteningLightDebounceConfig:J

.field private mCapacity:I

.field private mCount:I

.field private mDarkeningLightDebounceConfig:J

.field private mEnd:I

.field private mRingLux:[F

.field private mRingTime:[J

.field private mStart:I

.field private final mWeightingIntercept:I


# direct methods
.method public constructor <init>(JI)V
    .locals 2
    .param p1, "lightSensorRate"    # J
    .param p3, "ambientLightHorizon"    # I

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    int-to-float v0, p3

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    long-to-float v1, p1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    .line 36
    new-array v1, v0, [F

    iput-object v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingLux:[F

    .line 37
    new-array v0, v0, [J

    iput-object v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    .line 38
    iput p3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mWeightingIntercept:I

    .line 39
    return-void
.end method

.method private calculateWeight(JJ)F
    .locals 2
    .param p1, "startDelta"    # J
    .param p3, "endDelta"    # J

    .line 188
    invoke-direct {p0, p3, p4}, Lcom/android/server/display/AmbientLightRingBuffer;->weightIntegral(J)F

    move-result v0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/AmbientLightRingBuffer;->weightIntegral(J)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method private offsetOf(I)I
    .locals 1
    .param p1, "index"    # I

    .line 137
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    if-ge p1, v0, :cond_1

    if-ltz p1, :cond_1

    .line 140
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    add-int/2addr p1, v0

    .line 141
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    if-lt p1, v0, :cond_0

    .line 142
    sub-int/2addr p1, v0

    .line 144
    :cond_0
    return p1

    .line 138
    :cond_1
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0
.end method

.method private weightIntegral(J)F
    .locals 3
    .param p1, "x"    # J

    .line 194
    long-to-float v0, p1

    long-to-float v1, p1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mWeightingIntercept:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public calculateAmbientLux(JJ)F
    .locals 17
    .param p1, "now"    # J
    .param p3, "horizon"    # J

    .line 149
    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v1

    .line 150
    .local v1, "N":I
    if-nez v1, :cond_0

    .line 151
    const/high16 v2, -0x40800000    # -1.0f

    return v2

    .line 155
    :cond_0
    const/4 v2, 0x0

    .line 156
    .local v2, "endIndex":I
    sub-long v3, p1, p3

    .line 157
    .local v3, "horizonStartTime":J
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    add-int/lit8 v6, v1, -0x1

    if-ge v5, v6, :cond_1

    .line 158
    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v0, v6}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v6

    cmp-long v6, v6, v3

    if-gtz v6, :cond_1

    .line 159
    add-int/lit8 v2, v2, 0x1

    .line 157
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 165
    .end local v5    # "i":I
    :cond_1
    const/4 v5, 0x0

    .line 166
    .local v5, "sum":F
    const/4 v6, 0x0

    .line 167
    .local v6, "totalWeight":F
    const-wide/16 v7, 0x64

    .line 168
    .local v7, "endTime":J
    add-int/lit8 v9, v1, -0x1

    .local v9, "i":I
    :goto_1
    if-lt v9, v2, :cond_3

    .line 169
    invoke-virtual {v0, v9}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v10

    .line 170
    .local v10, "eventTime":J
    if-ne v9, v2, :cond_2

    cmp-long v12, v10, v3

    if-gez v12, :cond_2

    .line 173
    move-wide v10, v3

    .line 175
    :cond_2
    sub-long v12, v10, p1

    .line 176
    .local v12, "startTime":J
    invoke-direct {v0, v12, v13, v7, v8}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateWeight(JJ)F

    move-result v14

    .line 177
    .local v14, "weight":F
    invoke-virtual {v0, v9}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v15

    .line 179
    .local v15, "lux":F
    add-float/2addr v6, v14

    .line 180
    mul-float v16, v15, v14

    add-float v5, v5, v16

    .line 181
    move-wide v7, v12

    .line 168
    .end local v10    # "eventTime":J
    .end local v12    # "startTime":J
    .end local v14    # "weight":F
    .end local v15    # "lux":F
    add-int/lit8 v9, v9, -0x1

    goto :goto_1

    .line 184
    .end local v9    # "i":I
    :cond_3
    div-float v9, v5, v6

    return v9
.end method

.method public clear()V
    .locals 1

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    .line 114
    iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I

    .line 115
    iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    .line 116
    return-void
.end method

.method public getLux(I)F
    .locals 2
    .param p1, "index"    # I

    .line 42
    iget-object v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingLux:[F

    invoke-direct {p0, p1}, Lcom/android/server/display/AmbientLightRingBuffer;->offsetOf(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public getTime(I)J
    .locals 2
    .param p1, "index"    # I

    .line 46
    iget-object v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    invoke-direct {p0, p1}, Lcom/android/server/display/AmbientLightRingBuffer;->offsetOf(I)I

    move-result v1

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method nextAmbientLightBrighteningTransition(JF)J
    .locals 5
    .param p1, "time"    # J
    .param p3, "brighteningThreshold"    # F

    .line 198
    invoke-virtual {p0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    .line 199
    .local v0, "N":I
    move-wide v1, p1

    .line 200
    .local v1, "earliestValidTime":J
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 201
    invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    cmpg-float v4, v4, p3

    if-gtz v4, :cond_0

    .line 202
    goto :goto_1

    .line 204
    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v1

    .line 200
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 206
    .end local v3    # "i":I
    :cond_1
    :goto_1
    iget-wide v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mBrighteningLightDebounceConfig:J

    add-long/2addr v3, v1

    return-wide v3
.end method

.method nextAmbientLightDarkeningTransition(JF)J
    .locals 5
    .param p1, "time"    # J
    .param p3, "darkeningThreshold"    # F

    .line 210
    invoke-virtual {p0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    .line 211
    .local v0, "N":I
    move-wide v1, p1

    .line 212
    .local v1, "earliestValidTime":J
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 213
    invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    cmpl-float v4, v4, p3

    if-ltz v4, :cond_0

    .line 214
    goto :goto_1

    .line 216
    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v1

    .line 212
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 219
    .end local v3    # "i":I
    :cond_1
    :goto_1
    iget-wide v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mDarkeningLightDebounceConfig:J

    add-long/2addr v3, v1

    return-wide v3
.end method

.method public prune(J)V
    .locals 5
    .param p1, "horizon"    # J

    .line 80
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    if-nez v0, :cond_0

    .line 81
    return-void

    .line 84
    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 85
    iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    add-int/2addr v2, v1

    .line 86
    .local v2, "next":I
    iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    if-lt v2, v1, :cond_1

    .line 87
    sub-int/2addr v2, v1

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    aget-wide v3, v1, v2

    cmp-long v1, v3, p1

    if-lez v1, :cond_2

    .line 97
    goto :goto_1

    .line 99
    :cond_2
    iput v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    .line 100
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    .line 101
    .end local v2    # "next":I
    goto :goto_0

    .line 103
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    aget-wide v2, v0, v1

    cmp-long v2, v2, p1

    if-gez v2, :cond_4

    .line 104
    aput-wide p1, v0, v1

    .line 106
    :cond_4
    return-void
.end method

.method public push(JF)V
    .locals 8
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .line 50
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I

    .line 51
    .local v0, "next":I
    iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    .line 52
    mul-int/lit8 v1, v2, 0x2

    .line 54
    .local v1, "newSize":I
    new-array v4, v1, [F

    .line 55
    .local v4, "newRingLux":[F
    new-array v5, v1, [J

    .line 56
    .local v5, "newRingTime":[J
    iget v6, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    sub-int/2addr v2, v6

    .line 57
    .local v2, "length":I
    iget-object v7, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingLux:[F

    invoke-static {v7, v6, v4, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    iget-object v6, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    iget v7, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    invoke-static {v6, v7, v5, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    iget v6, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    if-eqz v6, :cond_0

    .line 60
    iget-object v7, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingLux:[F

    invoke-static {v7, v3, v4, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 61
    iget-object v6, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    iget v7, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    invoke-static {v6, v3, v5, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    :cond_0
    iput-object v4, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingLux:[F

    .line 64
    iput-object v5, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    .line 66
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    .line 67
    iput v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    .line 68
    iput v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I

    .line 70
    .end local v1    # "newSize":I
    .end local v2    # "length":I
    .end local v4    # "newRingLux":[F
    .end local v5    # "newRingTime":[J
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingTime:[J

    aput-wide p1, v1, v0

    .line 71
    iget-object v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mRingLux:[F

    aput p3, v1, v0

    .line 72
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I

    .line 73
    iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I

    if-ne v1, v2, :cond_2

    .line 74
    iput v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I

    .line 76
    :cond_2
    iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    .line 77
    return-void
.end method

.method setBrighteningDebounce(J)V
    .locals 0
    .param p1, "brighteningDebounce"    # J

    .line 223
    iput-wide p1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mBrighteningLightDebounceConfig:J

    .line 224
    return-void
.end method

.method setDarkeningDebounce(J)V
    .locals 0
    .param p1, "darkeningDebounce"    # J

    .line 227
    iput-wide p1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mDarkeningLightDebounceConfig:J

    .line 228
    return-void
.end method

.method public size()I
    .locals 1

    .line 109
    iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 120
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 121
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 122
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I

    if-ge v1, v2, :cond_2

    .line 123
    add-int/lit8 v3, v1, 0x1

    if-ge v3, v2, :cond_0

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v2

    goto :goto_1

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 124
    .local v2, "next":J
    :goto_1
    if-eqz v1, :cond_1

    .line 125
    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;

    .line 128
    const-string v4, " / "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {p0, v1}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 130
    const-string v4, "ms"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    .end local v2    # "next":J
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v1    # "i":I
    :cond_2
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 133
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
