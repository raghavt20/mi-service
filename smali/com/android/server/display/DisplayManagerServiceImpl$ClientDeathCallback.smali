.class Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;
.super Ljava/lang/Object;
.source "DisplayManagerServiceImpl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClientDeathCallback"
.end annotation


# instance fields
.field private mFlag:I

.field private mToken:Landroid/os/IBinder;

.field final synthetic this$0:Lcom/android/server/display/DisplayManagerServiceImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/display/DisplayManagerServiceImpl;
    .param p2, "token"    # Landroid/os/IBinder;

    .line 704
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;-><init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/IBinder;I)V

    .line 705
    return-void
.end method

.method public constructor <init>(Lcom/android/server/display/DisplayManagerServiceImpl;Landroid/os/IBinder;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/display/DisplayManagerServiceImpl;
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "flag"    # I

    .line 707
    iput-object p1, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayManagerServiceImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 708
    iput-object p2, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    .line 709
    iput p3, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mFlag:I

    .line 711
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p2, p0, v0}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 714
    goto :goto_0

    .line 712
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 715
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 4

    .line 719
    const-string v0, "DisplayManagerServiceImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "binderDied: flag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    iget-object v0, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/display/DisplayManagerServiceImpl;->-$$Nest$fgetmLock(Lcom/android/server/display/DisplayManagerServiceImpl;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 721
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->this$0:Lcom/android/server/display/DisplayManagerServiceImpl;

    iget v2, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mFlag:I

    iget-object v3, p0, Lcom/android/server/display/DisplayManagerServiceImpl$ClientDeathCallback;->mToken:Landroid/os/IBinder;

    invoke-static {v1, v2, v3}, Lcom/android/server/display/DisplayManagerServiceImpl;->-$$Nest$mdoDieLocked(Lcom/android/server/display/DisplayManagerServiceImpl;ILandroid/os/IBinder;)V

    .line 722
    monitor-exit v0

    .line 723
    return-void

    .line 722
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
