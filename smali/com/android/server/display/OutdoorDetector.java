public class com.android.server.display.OutdoorDetector {
	 /* .source "OutdoorDetector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/OutdoorDetector$MyHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DEBOUNCE_ENTER_OUTDOOR_DURATION;
private static final Integer DEBOUNCE_EXIT_OUTDOOR_DURATION;
private static Boolean DEBUG;
private static final Integer MSG_UPDATE_OUTDOOR_STATE;
private static final Integer OUTDOOR_AMBIENT_LIGHT_HORIZON;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.display.AmbientLightRingBuffer mAmbientLightRingBuffer;
private Float mCurrentAmbientLux;
private android.os.Handler mHandler;
private Boolean mIsInOutDoor;
private Float mLastObservedLux;
private Long mLastObservedLuxTime;
private android.hardware.Sensor mLightSensor;
private Boolean mSensorEnabled;
private android.hardware.SensorEventListener mSensorListener;
private android.hardware.SensorManager mSensorManager;
private com.android.server.display.ThermalBrightnessController mThermalBrightnessController;
private final Float mThresholdLuxEnterOutdoor;
/* # direct methods */
static void -$$Nest$mhandleLightSensorEvent ( com.android.server.display.OutdoorDetector p0, Long p1, Float p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/OutdoorDetector;->handleLightSensorEvent(JF)V */
	 return;
} // .end method
static void -$$Nest$mupdateAmbientLux ( com.android.server.display.OutdoorDetector p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/OutdoorDetector;->updateAmbientLux()V */
	 return;
} // .end method
static com.android.server.display.OutdoorDetector ( ) {
	 /* .locals 1 */
	 /* .line 19 */
	 /* sget-boolean v0, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z */
	 com.android.server.display.OutdoorDetector.DEBUG = (v0!= 0);
	 return;
} // .end method
public com.android.server.display.OutdoorDetector ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "thermalController" # Lcom/android/server/display/ThermalBrightnessController; */
	 /* .param p3, "threshold" # F */
	 /* .line 42 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 65 */
	 /* new-instance v0, Lcom/android/server/display/OutdoorDetector$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/OutdoorDetector$1;-><init>(Lcom/android/server/display/OutdoorDetector;)V */
	 this.mSensorListener = v0;
	 /* .line 43 */
	 /* const-class v0, Landroid/hardware/SensorManager; */
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/SensorManager; */
	 this.mSensorManager = v0;
	 /* .line 44 */
	 int v1 = 5; // const/4 v1, 0x5
	 int v2 = 0; // const/4 v2, 0x0
	 (( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
	 this.mLightSensor = v0;
	 /* .line 45 */
	 /* new-instance v0, Lcom/android/server/display/OutdoorDetector$MyHandler; */
	 com.android.internal.os.BackgroundThread .getHandler ( );
	 (( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/display/OutdoorDetector$MyHandler;-><init>(Lcom/android/server/display/OutdoorDetector;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 46 */
	 /* new-instance v0, Lcom/android/server/display/AmbientLightRingBuffer; */
	 /* const-wide/16 v1, 0xfa */
	 /* const/16 v3, 0xbb8 */
	 /* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/display/AmbientLightRingBuffer;-><init>(JI)V */
	 this.mAmbientLightRingBuffer = v0;
	 /* .line 47 */
	 this.mThermalBrightnessController = p2;
	 /* .line 48 */
	 /* iput p3, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F */
	 /* .line 49 */
	 return;
} // .end method
private void applyLightSensorMeasurement ( Long p0, Float p1 ) {
	 /* .locals 3 */
	 /* .param p1, "time" # J */
	 /* .param p3, "lux" # F */
	 /* .line 90 */
	 v0 = this.mAmbientLightRingBuffer;
	 /* const-wide/16 v1, 0xbb8 */
	 /* sub-long v1, p1, v1 */
	 (( com.android.server.display.AmbientLightRingBuffer ) v0 ).prune ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V
	 /* .line 91 */
	 v0 = this.mAmbientLightRingBuffer;
	 (( com.android.server.display.AmbientLightRingBuffer ) v0 ).push ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/AmbientLightRingBuffer;->push(JF)V
	 /* .line 93 */
	 /* iput p3, p0, Lcom/android/server/display/OutdoorDetector;->mLastObservedLux:F */
	 /* .line 94 */
	 /* iput-wide p1, p0, Lcom/android/server/display/OutdoorDetector;->mLastObservedLuxTime:J */
	 /* .line 95 */
	 return;
} // .end method
private void handleLightSensorEvent ( Long p0, Float p1 ) {
	 /* .locals 2 */
	 /* .param p1, "time" # J */
	 /* .param p3, "lux" # F */
	 /* .line 80 */
	 /* sget-boolean v0, Lcom/android/server/display/OutdoorDetector;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 81 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "handleLightSensorEvent: lux = "; // const-string v1, "handleLightSensorEvent: lux = "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "ThermalBrightnessController.OutdoorDetector"; // const-string v1, "ThermalBrightnessController.OutdoorDetector"
		 android.util.Slog .d ( v1,v0 );
		 /* .line 83 */
	 } // :cond_0
	 v0 = this.mHandler;
	 int v1 = 1; // const/4 v1, 0x1
	 (( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
	 /* .line 84 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/OutdoorDetector;->applyLightSensorMeasurement(JF)V */
	 /* .line 85 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/OutdoorDetector;->updateAmbientLux(J)V */
	 /* .line 86 */
	 return;
} // .end method
private Long nextEnterOutdoorModeTransition ( Long p0 ) {
	 /* .locals 6 */
	 /* .param p1, "time" # J */
	 /* .line 140 */
	 v0 = this.mAmbientLightRingBuffer;
	 v0 = 	 (( com.android.server.display.AmbientLightRingBuffer ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
	 /* .line 141 */
	 /* .local v0, "N":I */
	 /* move-wide v1, p1 */
	 /* .line 142 */
	 /* .local v1, "earliestValidTime":J */
	 /* add-int/lit8 v3, v0, -0x1 */
	 /* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* .line 143 */
v4 = this.mAmbientLightRingBuffer;
v4 = (( com.android.server.display.AmbientLightRingBuffer ) v4 ).getLux ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* iget v5, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F */
/* cmpg-float v4, v4, v5 */
/* if-gtz v4, :cond_0 */
/* .line 144 */
/* .line 146 */
} // :cond_0
v4 = this.mAmbientLightRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v4 ).getTime ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v1 */
/* .line 142 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 148 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* const-wide/16 v3, 0xbb8 */
/* add-long/2addr v3, v1 */
/* return-wide v3 */
} // .end method
private Long nextExitOutdoorModeTransition ( Long p0 ) {
/* .locals 6 */
/* .param p1, "time" # J */
/* .line 152 */
v0 = this.mAmbientLightRingBuffer;
v0 = (( com.android.server.display.AmbientLightRingBuffer ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
/* .line 153 */
/* .local v0, "N":I */
/* move-wide v1, p1 */
/* .line 154 */
/* .local v1, "earliestValidTime":J */
/* add-int/lit8 v3, v0, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* .line 155 */
v4 = this.mAmbientLightRingBuffer;
v4 = (( com.android.server.display.AmbientLightRingBuffer ) v4 ).getLux ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* iget v5, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F */
/* cmpl-float v4, v4, v5 */
/* if-ltz v4, :cond_0 */
/* .line 156 */
/* .line 158 */
} // :cond_0
v4 = this.mAmbientLightRingBuffer;
(( com.android.server.display.AmbientLightRingBuffer ) v4 ).getTime ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v1 */
/* .line 154 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 160 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* const-wide/16 v3, 0xbb8 */
/* add-long/2addr v3, v1 */
/* return-wide v3 */
} // .end method
private void setOutdoorActive ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "active" # Z */
/* .line 128 */
/* iget-boolean v0, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 129 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setOutDoorActive: active: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ThermalBrightnessController.OutdoorDetector"; // const-string v1, "ThermalBrightnessController.OutdoorDetector"
android.util.Slog .d ( v1,v0 );
/* .line 130 */
/* iput-boolean p1, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z */
/* .line 131 */
v0 = this.mThermalBrightnessController;
(( com.android.server.display.ThermalBrightnessController ) v0 ).outDoorStateChanged ( ); // invoke-virtual {v0}, Lcom/android/server/display/ThermalBrightnessController;->outDoorStateChanged()V
/* .line 133 */
} // :cond_0
return;
} // .end method
private void updateAmbientLux ( ) {
/* .locals 5 */
/* .line 99 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 100 */
/* .local v0, "time":J */
v2 = this.mAmbientLightRingBuffer;
/* const-wide/16 v3, 0xbb8 */
/* sub-long v3, v0, v3 */
(( com.android.server.display.AmbientLightRingBuffer ) v2 ).prune ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V
/* .line 101 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/display/OutdoorDetector;->updateAmbientLux(J)V */
/* .line 102 */
return;
} // .end method
private void updateAmbientLux ( Long p0 ) {
/* .locals 10 */
/* .param p1, "time" # J */
/* .line 105 */
v0 = this.mAmbientLightRingBuffer;
/* const-wide/16 v1, 0xbb8 */
v0 = (( com.android.server.display.AmbientLightRingBuffer ) v0 ).calculateAmbientLux ( p1, p2, v1, v2 ); // invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F
/* iput v0, p0, Lcom/android/server/display/OutdoorDetector;->mCurrentAmbientLux:F */
/* .line 106 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/OutdoorDetector;->nextEnterOutdoorModeTransition(J)J */
/* move-result-wide v0 */
/* .line 107 */
/* .local v0, "nextEnterOutdoorModeTime":J */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/display/OutdoorDetector;->nextExitOutdoorModeTransition(J)J */
/* move-result-wide v2 */
/* .line 109 */
/* .local v2, "nextExitOutdoorModeTime":J */
/* iget v4, p0, Lcom/android/server/display/OutdoorDetector;->mCurrentAmbientLux:F */
/* iget v5, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F */
/* cmpl-float v6, v4, v5 */
int v7 = 1; // const/4 v7, 0x1
/* if-ltz v6, :cond_0 */
/* cmp-long v6, v0, p1 */
/* if-gtz v6, :cond_0 */
/* iget-boolean v6, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z */
/* if-nez v6, :cond_0 */
/* .line 112 */
/* invoke-direct {p0, v7}, Lcom/android/server/display/OutdoorDetector;->setOutdoorActive(Z)V */
/* .line 113 */
} // :cond_0
/* cmpg-float v4, v4, v5 */
/* if-gez v4, :cond_1 */
/* cmp-long v4, v2, p1 */
/* if-gtz v4, :cond_1 */
/* .line 115 */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {p0, v4}, Lcom/android/server/display/OutdoorDetector;->setOutdoorActive(Z)V */
/* .line 117 */
} // :cond_1
} // :goto_0
java.lang.Math .min ( v0,v1,v2,v3 );
/* move-result-wide v4 */
/* .line 118 */
/* .local v4, "nextTransitionTime":J */
/* cmp-long v6, v4, p1 */
/* if-lez v6, :cond_2 */
/* move-wide v8, v4 */
} // :cond_2
/* const-wide/16 v8, 0x3e8 */
/* add-long/2addr v8, p1 */
} // :goto_1
/* move-wide v4, v8 */
/* .line 119 */
/* sget-boolean v6, Lcom/android/server/display/OutdoorDetector;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 120 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "updateAmbientLux: Scheduling lux update for " */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 121 */
android.util.TimeUtils .formatUptime ( v4,v5 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ", mAmbientLightRingBuffer: "; // const-string v8, ", mAmbientLightRingBuffer: "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 120 */
final String v8 = "ThermalBrightnessController.OutdoorDetector"; // const-string v8, "ThermalBrightnessController.OutdoorDetector"
android.util.Slog .d ( v8,v6 );
/* .line 124 */
} // :cond_3
v6 = this.mHandler;
(( android.os.Handler ) v6 ).sendEmptyMessageAtTime ( v7, v4, v5 ); // invoke-virtual {v6, v7, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z
/* .line 125 */
return;
} // .end method
/* # virtual methods */
protected Boolean detect ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .line 52 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z */
/* if-nez v2, :cond_0 */
/* .line 53 */
/* iput-boolean v0, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z */
/* .line 54 */
v0 = this.mSensorManager;
v2 = this.mSensorListener;
v3 = this.mLightSensor;
v4 = this.mHandler;
(( android.hardware.SensorManager ) v0 ).registerListener ( v2, v3, v1, v4 ); // invoke-virtual {v0, v2, v3, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 56 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 57 */
/* iput-boolean v1, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z */
/* .line 58 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).removeMessages ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V
/* .line 59 */
v0 = this.mSensorManager;
v2 = this.mSensorListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v2 ); // invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 60 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/OutdoorDetector;->setOutdoorActive(Z)V */
/* .line 62 */
} // :cond_1
} // :goto_0
} // .end method
protected Boolean isOutdoorState ( ) {
/* .locals 1 */
/* .line 136 */
/* iget-boolean v0, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z */
} // .end method
