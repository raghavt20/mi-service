.class Lcom/android/server/display/SunlightController$SunlightModeHandler;
.super Landroid/os/Handler;
.source "SunlightController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SunlightController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SunlightModeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/SunlightController;


# direct methods
.method public constructor <init>(Lcom/android/server/display/SunlightController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 412
    iput-object p1, p0, Lcom/android/server/display/SunlightController$SunlightModeHandler;->this$0:Lcom/android/server/display/SunlightController;

    .line 413
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 414
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 418
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 429
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/SunlightController$SunlightModeHandler;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v0}, Lcom/android/server/display/SunlightController;->-$$Nest$mregister(Lcom/android/server/display/SunlightController;)V

    .line 430
    goto :goto_0

    .line 426
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/SunlightController$SunlightModeHandler;->this$0:Lcom/android/server/display/SunlightController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/SunlightController;->-$$Nest$mupdateHangUpState(Lcom/android/server/display/SunlightController;Z)V

    .line 427
    goto :goto_0

    .line 423
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/SunlightController$SunlightModeHandler;->this$0:Lcom/android/server/display/SunlightController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/SunlightController;->-$$Nest$mupdateScreenState(Lcom/android/server/display/SunlightController;Z)V

    .line 424
    goto :goto_0

    .line 420
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/SunlightController$SunlightModeHandler;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v0}, Lcom/android/server/display/SunlightController;->-$$Nest$mupdateAmbientLux(Lcom/android/server/display/SunlightController;)V

    .line 421
    nop

    .line 434
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
