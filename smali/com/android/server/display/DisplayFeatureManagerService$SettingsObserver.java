class com.android.server.display.DisplayFeatureManagerService$SettingsObserver extends android.database.ContentObserver {
	 /* .source "DisplayFeatureManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/DisplayFeatureManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayFeatureManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.display.DisplayFeatureManagerService$SettingsObserver ( ) {
/* .locals 0 */
/* .line 1068 */
this.this$0 = p1;
/* .line 1069 */
com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmHandler ( p1 );
/* invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 1070 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1074 */
(( android.net.Uri ) p2 ).getLastPathSegment ( ); // invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
/* .line 1075 */
/* .local v0, "lastPathSegment":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 2; // const/4 v2, 0x2
int v3 = 3; // const/4 v3, 0x3
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
/* const-string/jumbo v1, "screen_paper_mode_enabled" */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v5 */
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v1 = "miui_dkt_mode"; // const-string v1, "miui_dkt_mode"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* const/16 v1, 0xa */
	 /* goto/16 :goto_1 */
	 /* :sswitch_2 */
	 final String v1 = "screen_optimize_mode"; // const-string v1, "screen_optimize_mode"
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* move v1, v3 */
		 /* :sswitch_3 */
		 final String v1 = "screen_auto_adjust"; // const-string v1, "screen_auto_adjust"
		 v1 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v1 = 6; // const/4 v1, 0x6
			 /* :sswitch_4 */
			 final String v1 = "screen_color_level"; // const-string v1, "screen_color_level"
			 v1 = 			 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 int v1 = 4; // const/4 v1, 0x4
				 /* :sswitch_5 */
				 /* const-string/jumbo v1, "screen_texture_color_type" */
				 v1 = 				 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 int v1 = 7; // const/4 v1, 0x7
					 /* :sswitch_6 */
					 final String v1 = "screen_mode_type"; // const-string v1, "screen_mode_type"
					 v1 = 					 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v1 != null) { // if-eqz v1, :cond_0
						 /* const/16 v1, 0x8 */
						 /* :sswitch_7 */
						 /* const-string/jumbo v1, "screen_true_tone" */
						 v1 = 						 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v1 != null) { // if-eqz v1, :cond_0
							 /* const/16 v1, 0x9 */
							 /* :sswitch_8 */
							 final String v1 = "screen_game_mode"; // const-string v1, "screen_game_mode"
							 v1 = 							 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
							 if ( v1 != null) { // if-eqz v1, :cond_0
								 int v1 = 5; // const/4 v1, 0x5
								 /* :sswitch_9 */
								 /* const-string/jumbo v1, "screen_paper_mode_level" */
								 v1 = 								 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
								 if ( v1 != null) { // if-eqz v1, :cond_0
									 /* move v1, v4 */
									 /* :sswitch_a */
									 /* const-string/jumbo v1, "screen_paper_texture_level" */
									 v1 = 									 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
									 if ( v1 != null) { // if-eqz v1, :cond_0
										 /* move v1, v2 */
									 } // :goto_0
									 int v1 = -1; // const/4 v1, -0x1
								 } // :goto_1
								 /* packed-switch v1, :pswitch_data_0 */
								 /* goto/16 :goto_2 */
								 /* .line 1139 */
								 /* :pswitch_0 */
								 v1 = this.this$0;
								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateDeskTopMode ( v1 );
								 /* .line 1140 */
								 /* goto/16 :goto_2 */
								 /* .line 1135 */
								 /* :pswitch_1 */
								 v1 = this.this$0;
								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateTrueToneModeEnable ( v1 );
								 /* .line 1136 */
								 v1 = this.this$0;
								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleTrueToneModeChange ( v1 );
								 /* .line 1137 */
								 /* goto/16 :goto_2 */
								 /* .line 1120 */
								 /* :pswitch_2 */
								 v1 = this.this$0;
								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateReadingModeType ( v1 );
								 /* .line 1121 */
								 v1 = this.this$0;
								 v1 = 								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeEnabled ( v1 );
								 if ( v1 != null) { // if-eqz v1, :cond_6
									 /* .line 1122 */
									 v1 = this.this$0;
									 v1 = 									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmAutoAdjustEnable ( v1 );
									 /* if-nez v1, :cond_1 */
									 /* .line 1123 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdatePaperMode ( v1,v4,v5 );
									 /* .line 1125 */
								 } // :cond_1
								 v1 = this.this$0;
								 v1 = 								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
								 /* if-ne v1, v2, :cond_2 */
								 /* .line 1126 */
								 v1 = this.this$0;
								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetPaperColors ( v1,v5 );
								 /* .line 1127 */
								 v1 = this.this$0;
								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmRhythmicEyeCareManager ( v1 );
								 (( com.android.server.display.RhythmicEyeCareManager ) v1 ).setModeEnable ( v4 ); // invoke-virtual {v1, v4}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V
								 /* goto/16 :goto_2 */
								 /* .line 1129 */
							 } // :cond_2
							 v1 = this.this$0;
							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmRhythmicEyeCareManager ( v1 );
							 (( com.android.server.display.RhythmicEyeCareManager ) v1 ).setModeEnable ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V
							 /* .line 1130 */
							 v1 = this.this$0;
							 v2 = 							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetPaperColors ( v1,v2 );
							 /* goto/16 :goto_2 */
							 /* .line 1112 */
							 /* :pswitch_3 */
							 v1 = this.this$0;
							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdatePaperColorType ( v1 );
							 /* .line 1113 */
							 v1 = this.this$0;
							 v1 = 							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeEnabled ( v1 );
							 if ( v1 != null) { // if-eqz v1, :cond_6
								 v1 = this.this$0;
								 v1 = 								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
								 /* if-eq v1, v4, :cond_3 */
								 v1 = this.this$0;
								 v1 = 								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
								 /* if-ne v1, v3, :cond_6 */
								 /* .line 1116 */
							 } // :cond_3
							 v1 = this.this$0;
							 v2 = 							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$msetPaperColors ( v1,v2 );
							 /* goto/16 :goto_2 */
							 /* .line 1106 */
							 /* :pswitch_4 */
							 v1 = this.this$0;
							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateAutoAdjustEnable ( v1 );
							 /* .line 1107 */
							 v1 = this.this$0;
							 v1 = 							 com.android.server.display.DisplayFeatureManagerService .-$$Nest$misSupportSmartEyeCare ( v1 );
							 if ( v1 != null) { // if-eqz v1, :cond_6
								 v1 = this.this$0;
								 v1 = 								 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeEnabled ( v1 );
								 if ( v1 != null) { // if-eqz v1, :cond_6
									 /* .line 1108 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleAutoAdjustChange ( v1 );
									 /* .line 1103 */
									 /* :pswitch_5 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleGameModeChange ( v1 );
									 /* .line 1104 */
									 /* .line 1095 */
									 /* :pswitch_6 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateColorSchemeCTLevel ( v1 );
									 /* .line 1096 */
									 v1 = 									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$sfgetSUPPORT_UNLIMITED_COLOR_MODE ( );
									 if ( v1 != null) { // if-eqz v1, :cond_4
										 /* .line 1097 */
										 v1 = this.this$0;
										 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleUnlimitedColorLevelChange ( v1,v5 );
										 /* .line 1099 */
									 } // :cond_4
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleScreenSchemeChange ( v1,v5 );
									 /* .line 1101 */
									 /* .line 1091 */
									 /* :pswitch_7 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateColorSchemeModeType ( v1 );
									 /* .line 1092 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleScreenSchemeChange ( v1,v5 );
									 /* .line 1093 */
									 /* .line 1082 */
									 /* :pswitch_8 */
									 v1 = this.this$0;
									 v1 = 									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeEnabled ( v1 );
									 if ( v1 != null) { // if-eqz v1, :cond_6
										 v1 = this.this$0;
										 v1 = 										 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
										 /* if-eq v1, v4, :cond_5 */
										 v1 = this.this$0;
										 v1 = 										 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
										 /* if-eq v1, v3, :cond_5 */
										 v1 = this.this$0;
										 v1 = 										 com.android.server.display.DisplayFeatureManagerService .-$$Nest$fgetmReadingModeType ( v1 );
										 /* if-nez v1, :cond_6 */
										 /* .line 1086 */
									 } // :cond_5
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateReadingModeCTLevel ( v1 );
									 /* .line 1087 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdatePaperMode ( v1,v4,v4 );
									 /* .line 1077 */
									 /* :pswitch_9 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mupdateReadingModeEnable ( v1 );
									 /* .line 1078 */
									 v1 = this.this$0;
									 com.android.server.display.DisplayFeatureManagerService .-$$Nest$mhandleReadingModeChange ( v1,v5 );
									 /* .line 1079 */
									 /* nop */
									 /* .line 1144 */
								 } // :cond_6
							 } // :goto_2
							 return;
							 /* :sswitch_data_0 */
							 /* .sparse-switch */
							 /* -0x63a18e66 -> :sswitch_a */
							 /* -0x6076b212 -> :sswitch_9 */
							 /* -0x56e48e43 -> :sswitch_8 */
							 /* -0x4241e690 -> :sswitch_7 */
							 /* -0xb0e3fbd -> :sswitch_6 */
							 /* 0x25b806ed -> :sswitch_5 */
							 /* 0x2807b455 -> :sswitch_4 */
							 /* 0x6fff40cc -> :sswitch_3 */
							 /* 0x74fb4732 -> :sswitch_2 */
							 /* 0x75414804 -> :sswitch_1 */
							 /* 0x7e544b2b -> :sswitch_0 */
						 } // .end sparse-switch
						 /* :pswitch_data_0 */
						 /* .packed-switch 0x0 */
						 /* :pswitch_9 */
						 /* :pswitch_8 */
						 /* :pswitch_8 */
						 /* :pswitch_7 */
						 /* :pswitch_6 */
						 /* :pswitch_5 */
						 /* :pswitch_4 */
						 /* :pswitch_3 */
						 /* :pswitch_2 */
						 /* :pswitch_1 */
						 /* :pswitch_0 */
					 } // .end packed-switch
				 } // .end method
