public class com.android.server.display.DisplayGroupImpl extends com.android.server.display.DisplayGroupStub {
	 /* .source "DisplayGroupImpl.java" */
	 /* # instance fields */
	 private Integer mAlwaysUnlockedCount;
	 /* # direct methods */
	 public com.android.server.display.DisplayGroupImpl ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Lcom/android/server/display/DisplayGroupStub;-><init>()V */
		 return;
	 } // .end method
	 private Boolean hasAlwaysUnLockedFlag ( com.android.server.display.LogicalDisplay p0 ) {
		 /* .locals 2 */
		 /* .param p1, "display" # Lcom/android/server/display/LogicalDisplay; */
		 /* .line 28 */
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* .line 29 */
			 (( com.android.server.display.LogicalDisplay ) p1 ).getDisplayInfoLocked ( ); // invoke-virtual {p1}, Lcom/android/server/display/LogicalDisplay;->getDisplayInfoLocked()Landroid/view/DisplayInfo;
			 /* iget v0, v0, Landroid/view/DisplayInfo;->flags:I */
			 /* .line 30 */
			 /* .local v0, "flags":I */
			 /* and-int/lit16 v1, v0, 0x200 */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 31 */
				 int v1 = 1; // const/4 v1, 0x1
				 /* .line 34 */
			 } // .end local v0 # "flags":I
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 /* # virtual methods */
	 public Boolean isAlwaysOnLocked ( ) {
		 /* .locals 1 */
		 /* .line 24 */
		 /* iget v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I */
		 /* if-lez v0, :cond_0 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void updateAlwaysUnlockedCountIfNeeded ( com.android.server.display.LogicalDisplay p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "display" # Lcom/android/server/display/LogicalDisplay; */
/* .param p2, "isAdd" # Z */
/* .line 13 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/DisplayGroupImpl;->hasAlwaysUnLockedFlag(Lcom/android/server/display/LogicalDisplay;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 14 */
	 if ( p2 != null) { // if-eqz p2, :cond_0
		 /* .line 15 */
		 /* iget v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I */
		 /* add-int/lit8 v0, v0, 0x1 */
		 /* iput v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I */
		 /* .line 17 */
	 } // :cond_0
	 /* iget v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I */
	 /* add-int/lit8 v0, v0, -0x1 */
	 /* iput v0, p0, Lcom/android/server/display/DisplayGroupImpl;->mAlwaysUnlockedCount:I */
	 /* .line 20 */
} // :cond_1
} // :goto_0
return;
} // .end method
