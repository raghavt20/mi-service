public class com.android.server.display.AmbientLightRingBuffer {
	 /* .source "AmbientLightRingBuffer.java" */
	 /* # static fields */
	 private static final Long AMBIENT_LIGHT_PREDICTION_TIME_MILLIS;
	 private static final Float BUFFER_SLACK;
	 /* # instance fields */
	 private Long mBrighteningLightDebounceConfig;
	 private Integer mCapacity;
	 private Integer mCount;
	 private Long mDarkeningLightDebounceConfig;
	 private Integer mEnd;
	 private mRingLux;
	 private mRingTime;
	 private Integer mStart;
	 private final Integer mWeightingIntercept;
	 /* # direct methods */
	 public com.android.server.display.AmbientLightRingBuffer ( ) {
		 /* .locals 2 */
		 /* .param p1, "lightSensorRate" # J */
		 /* .param p3, "ambientLightHorizon" # I */
		 /* .line 34 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 35 */
		 /* int-to-float v0, p3 */
		 /* const/high16 v1, 0x3fc00000 # 1.5f */
		 /* mul-float/2addr v0, v1 */
		 /* long-to-float v1, p1 */
		 /* div-float/2addr v0, v1 */
		 /* float-to-double v0, v0 */
		 java.lang.Math .ceil ( v0,v1 );
		 /* move-result-wide v0 */
		 /* double-to-int v0, v0 */
		 /* iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
		 /* .line 36 */
		 /* new-array v1, v0, [F */
		 this.mRingLux = v1;
		 /* .line 37 */
		 /* new-array v0, v0, [J */
		 this.mRingTime = v0;
		 /* .line 38 */
		 /* iput p3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mWeightingIntercept:I */
		 /* .line 39 */
		 return;
	 } // .end method
	 private Float calculateWeight ( Long p0, Long p1 ) {
		 /* .locals 2 */
		 /* .param p1, "startDelta" # J */
		 /* .param p3, "endDelta" # J */
		 /* .line 188 */
		 v0 = 		 /* invoke-direct {p0, p3, p4}, Lcom/android/server/display/AmbientLightRingBuffer;->weightIntegral(J)F */
		 v1 = 		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/display/AmbientLightRingBuffer;->weightIntegral(J)F */
		 /* sub-float/2addr v0, v1 */
	 } // .end method
	 private Integer offsetOf ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "index" # I */
		 /* .line 137 */
		 /* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
		 /* if-ge p1, v0, :cond_1 */
		 /* if-ltz p1, :cond_1 */
		 /* .line 140 */
		 /* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
		 /* add-int/2addr p1, v0 */
		 /* .line 141 */
		 /* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
		 /* if-lt p1, v0, :cond_0 */
		 /* .line 142 */
		 /* sub-int/2addr p1, v0 */
		 /* .line 144 */
	 } // :cond_0
	 /* .line 138 */
} // :cond_1
/* new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException; */
/* invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V */
/* throw v0 */
} // .end method
private Float weightIntegral ( Long p0 ) {
/* .locals 3 */
/* .param p1, "x" # J */
/* .line 194 */
/* long-to-float v0, p1 */
/* long-to-float v1, p1 */
/* const/high16 v2, 0x3f000000 # 0.5f */
/* mul-float/2addr v1, v2 */
/* iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mWeightingIntercept:I */
/* int-to-float v2, v2 */
/* add-float/2addr v1, v2 */
/* mul-float/2addr v0, v1 */
} // .end method
/* # virtual methods */
public Float calculateAmbientLux ( Long p0, Long p1 ) {
/* .locals 17 */
/* .param p1, "now" # J */
/* .param p3, "horizon" # J */
/* .line 149 */
/* move-object/from16 v0, p0 */
v1 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I */
/* .line 150 */
/* .local v1, "N":I */
/* if-nez v1, :cond_0 */
/* .line 151 */
/* const/high16 v2, -0x40800000 # -1.0f */
/* .line 155 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 156 */
/* .local v2, "endIndex":I */
/* sub-long v3, p1, p3 */
/* .line 157 */
/* .local v3, "horizonStartTime":J */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* add-int/lit8 v6, v1, -0x1 */
/* if-ge v5, v6, :cond_1 */
/* .line 158 */
/* add-int/lit8 v6, v5, 0x1 */
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).getTime ( v6 ); // invoke-virtual {v0, v6}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v6 */
/* cmp-long v6, v6, v3 */
/* if-gtz v6, :cond_1 */
/* .line 159 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 157 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 165 */
} // .end local v5 # "i":I
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
/* .line 166 */
/* .local v5, "sum":F */
int v6 = 0; // const/4 v6, 0x0
/* .line 167 */
/* .local v6, "totalWeight":F */
/* const-wide/16 v7, 0x64 */
/* .line 168 */
/* .local v7, "endTime":J */
/* add-int/lit8 v9, v1, -0x1 */
/* .local v9, "i":I */
} // :goto_1
/* if-lt v9, v2, :cond_3 */
/* .line 169 */
(( com.android.server.display.AmbientLightRingBuffer ) v0 ).getTime ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v10 */
/* .line 170 */
/* .local v10, "eventTime":J */
/* if-ne v9, v2, :cond_2 */
/* cmp-long v12, v10, v3 */
/* if-gez v12, :cond_2 */
/* .line 173 */
/* move-wide v10, v3 */
/* .line 175 */
} // :cond_2
/* sub-long v12, v10, p1 */
/* .line 176 */
/* .local v12, "startTime":J */
v14 = /* invoke-direct {v0, v12, v13, v7, v8}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateWeight(JJ)F */
/* .line 177 */
/* .local v14, "weight":F */
v15 = (( com.android.server.display.AmbientLightRingBuffer ) v0 ).getLux ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* .line 179 */
/* .local v15, "lux":F */
/* add-float/2addr v6, v14 */
/* .line 180 */
/* mul-float v16, v15, v14 */
/* add-float v5, v5, v16 */
/* .line 181 */
/* move-wide v7, v12 */
/* .line 168 */
} // .end local v10 # "eventTime":J
} // .end local v12 # "startTime":J
} // .end local v14 # "weight":F
} // .end local v15 # "lux":F
/* add-int/lit8 v9, v9, -0x1 */
/* .line 184 */
} // .end local v9 # "i":I
} // :cond_3
/* div-float v9, v5, v6 */
} // .end method
public void clear ( ) {
/* .locals 1 */
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
/* .line 114 */
/* iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I */
/* .line 115 */
/* iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* .line 116 */
return;
} // .end method
public Float getLux ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "index" # I */
/* .line 42 */
v0 = this.mRingLux;
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/display/AmbientLightRingBuffer;->offsetOf(I)I */
/* aget v0, v0, v1 */
} // .end method
public Long getTime ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "index" # I */
/* .line 46 */
v0 = this.mRingTime;
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/display/AmbientLightRingBuffer;->offsetOf(I)I */
/* aget-wide v0, v0, v1 */
/* return-wide v0 */
} // .end method
Long nextAmbientLightBrighteningTransition ( Long p0, Float p1 ) {
/* .locals 5 */
/* .param p1, "time" # J */
/* .param p3, "brighteningThreshold" # F */
/* .line 198 */
v0 = (( com.android.server.display.AmbientLightRingBuffer ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
/* .line 199 */
/* .local v0, "N":I */
/* move-wide v1, p1 */
/* .line 200 */
/* .local v1, "earliestValidTime":J */
/* add-int/lit8 v3, v0, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* .line 201 */
v4 = (( com.android.server.display.AmbientLightRingBuffer ) p0 ).getLux ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* cmpg-float v4, v4, p3 */
/* if-gtz v4, :cond_0 */
/* .line 202 */
/* .line 204 */
} // :cond_0
(( com.android.server.display.AmbientLightRingBuffer ) p0 ).getTime ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v1 */
/* .line 200 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 206 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* iget-wide v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mBrighteningLightDebounceConfig:J */
/* add-long/2addr v3, v1 */
/* return-wide v3 */
} // .end method
Long nextAmbientLightDarkeningTransition ( Long p0, Float p1 ) {
/* .locals 5 */
/* .param p1, "time" # J */
/* .param p3, "darkeningThreshold" # F */
/* .line 210 */
v0 = (( com.android.server.display.AmbientLightRingBuffer ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I
/* .line 211 */
/* .local v0, "N":I */
/* move-wide v1, p1 */
/* .line 212 */
/* .local v1, "earliestValidTime":J */
/* add-int/lit8 v3, v0, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* .line 213 */
v4 = (( com.android.server.display.AmbientLightRingBuffer ) p0 ).getLux ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
/* cmpl-float v4, v4, p3 */
/* if-ltz v4, :cond_0 */
/* .line 214 */
/* .line 216 */
} // :cond_0
(( com.android.server.display.AmbientLightRingBuffer ) p0 ).getTime ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v1 */
/* .line 212 */
/* add-int/lit8 v3, v3, -0x1 */
/* .line 219 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* iget-wide v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mDarkeningLightDebounceConfig:J */
/* add-long/2addr v3, v1 */
/* return-wide v3 */
} // .end method
public void prune ( Long p0 ) {
/* .locals 5 */
/* .param p1, "horizon" # J */
/* .line 80 */
/* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* if-nez v0, :cond_0 */
/* .line 81 */
return;
/* .line 84 */
} // :cond_0
} // :goto_0
/* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
int v1 = 1; // const/4 v1, 0x1
/* if-le v0, v1, :cond_3 */
/* .line 85 */
/* iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
/* add-int/2addr v2, v1 */
/* .line 86 */
/* .local v2, "next":I */
/* iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
/* if-lt v2, v1, :cond_1 */
/* .line 87 */
/* sub-int/2addr v2, v1 */
/* .line 89 */
} // :cond_1
v1 = this.mRingTime;
/* aget-wide v3, v1, v2 */
/* cmp-long v1, v3, p1 */
/* if-lez v1, :cond_2 */
/* .line 97 */
/* .line 99 */
} // :cond_2
/* iput v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
/* .line 100 */
/* add-int/lit8 v0, v0, -0x1 */
/* iput v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* .line 101 */
} // .end local v2 # "next":I
/* .line 103 */
} // :cond_3
} // :goto_1
v0 = this.mRingTime;
/* iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
/* aget-wide v2, v0, v1 */
/* cmp-long v2, v2, p1 */
/* if-gez v2, :cond_4 */
/* .line 104 */
/* aput-wide p1, v0, v1 */
/* .line 106 */
} // :cond_4
return;
} // .end method
public void push ( Long p0, Float p1 ) {
/* .locals 8 */
/* .param p1, "time" # J */
/* .param p3, "lux" # F */
/* .line 50 */
/* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I */
/* .line 51 */
/* .local v0, "next":I */
/* iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
int v3 = 0; // const/4 v3, 0x0
/* if-ne v1, v2, :cond_1 */
/* .line 52 */
/* mul-int/lit8 v1, v2, 0x2 */
/* .line 54 */
/* .local v1, "newSize":I */
/* new-array v4, v1, [F */
/* .line 55 */
/* .local v4, "newRingLux":[F */
/* new-array v5, v1, [J */
/* .line 56 */
/* .local v5, "newRingTime":[J */
/* iget v6, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
/* sub-int/2addr v2, v6 */
/* .line 57 */
/* .local v2, "length":I */
v7 = this.mRingLux;
java.lang.System .arraycopy ( v7,v6,v4,v3,v2 );
/* .line 58 */
v6 = this.mRingTime;
/* iget v7, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
java.lang.System .arraycopy ( v6,v7,v5,v3,v2 );
/* .line 59 */
/* iget v6, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 60 */
v7 = this.mRingLux;
java.lang.System .arraycopy ( v7,v3,v4,v2,v6 );
/* .line 61 */
v6 = this.mRingTime;
/* iget v7, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
java.lang.System .arraycopy ( v6,v3,v5,v2,v7 );
/* .line 63 */
} // :cond_0
this.mRingLux = v4;
/* .line 64 */
this.mRingTime = v5;
/* .line 66 */
/* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
/* .line 67 */
/* iput v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
/* .line 68 */
/* iput v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mStart:I */
/* .line 70 */
} // .end local v1 # "newSize":I
} // .end local v2 # "length":I
} // .end local v4 # "newRingLux":[F
} // .end local v5 # "newRingTime":[J
} // :cond_1
v1 = this.mRingTime;
/* aput-wide p1, v1, v0 */
/* .line 71 */
v1 = this.mRingLux;
/* aput p3, v1, v0 */
/* .line 72 */
/* add-int/lit8 v1, v0, 0x1 */
/* iput v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I */
/* .line 73 */
/* iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCapacity:I */
/* if-ne v1, v2, :cond_2 */
/* .line 74 */
/* iput v3, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mEnd:I */
/* .line 76 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* .line 77 */
return;
} // .end method
void setBrighteningDebounce ( Long p0 ) {
/* .locals 0 */
/* .param p1, "brighteningDebounce" # J */
/* .line 223 */
/* iput-wide p1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mBrighteningLightDebounceConfig:J */
/* .line 224 */
return;
} // .end method
void setDarkeningDebounce ( Long p0 ) {
/* .locals 0 */
/* .param p1, "darkeningDebounce" # J */
/* .line 227 */
/* iput-wide p1, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mDarkeningLightDebounceConfig:J */
/* .line 228 */
return;
} // .end method
public Integer size ( ) {
/* .locals 1 */
/* .line 109 */
/* iget v0, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
} // .end method
public java.lang.String toString ( ) {
/* .locals 6 */
/* .line 120 */
/* new-instance v0, Ljava/lang/StringBuffer; */
/* invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V */
/* .line 121 */
/* .local v0, "buf":Ljava/lang/StringBuffer; */
/* const/16 v1, 0x5b */
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
/* .line 122 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* iget v2, p0, Lcom/android/server/display/AmbientLightRingBuffer;->mCount:I */
/* if-ge v1, v2, :cond_2 */
/* .line 123 */
/* add-int/lit8 v3, v1, 0x1 */
/* if-ge v3, v2, :cond_0 */
/* add-int/lit8 v2, v1, 0x1 */
(( com.android.server.display.AmbientLightRingBuffer ) p0 ).getTime ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v2 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 124 */
/* .local v2, "next":J */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 125 */
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuffer ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 127 */
} // :cond_1
v4 = (( com.android.server.display.AmbientLightRingBuffer ) p0 ).getLux ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F
(( java.lang.StringBuffer ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(F)Ljava/lang/StringBuffer;
/* .line 128 */
final String v4 = " / "; // const-string v4, " / "
(( java.lang.StringBuffer ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 129 */
(( com.android.server.display.AmbientLightRingBuffer ) p0 ).getTime ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J
/* move-result-wide v4 */
/* sub-long v4, v2, v4 */
(( java.lang.StringBuffer ) v0 ).append ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;
/* .line 130 */
final String v4 = "ms"; // const-string v4, "ms"
(( java.lang.StringBuffer ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
/* .line 122 */
} // .end local v2 # "next":J
/* add-int/lit8 v1, v1, 0x1 */
/* .line 132 */
} // .end local v1 # "i":I
} // :cond_2
/* const/16 v1, 0x5d */
(( java.lang.StringBuffer ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
/* .line 133 */
(( java.lang.StringBuffer ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
} // .end method
