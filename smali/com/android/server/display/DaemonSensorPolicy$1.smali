.class Lcom/android/server/display/DaemonSensorPolicy$1;
.super Ljava/lang/Object;
.source "DaemonSensorPolicy.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DaemonSensorPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DaemonSensorPolicy;


# direct methods
.method constructor <init>(Lcom/android/server/display/DaemonSensorPolicy;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DaemonSensorPolicy;

    .line 144
    iput-object p1, p0, Lcom/android/server/display/DaemonSensorPolicy$1;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 153
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 147
    iget-object v0, p0, Lcom/android/server/display/DaemonSensorPolicy$1;->this$0:Lcom/android/server/display/DaemonSensorPolicy;

    invoke-static {v0}, Lcom/android/server/display/DaemonSensorPolicy;->-$$Nest$fgetmDaemonSensorValues(Lcom/android/server/display/DaemonSensorPolicy;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-void
.end method
