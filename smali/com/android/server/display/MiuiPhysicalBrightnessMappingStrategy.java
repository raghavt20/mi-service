public class com.android.server.display.MiuiPhysicalBrightnessMappingStrategy extends com.android.server.display.BrightnessMappingStrategy {
	 /* .source "MiuiPhysicalBrightnessMappingStrategy.java" */
	 /* # static fields */
	 public static final Integer CATEGORY_GAME;
	 public static final Integer CATEGORY_IMAGE;
	 public static final Integer CATEGORY_MAPS;
	 public static final Integer CATEGORY_READERS;
	 public static final Integer CATEGORY_SUM;
	 public static final Integer CATEGORY_UNDEFINED;
	 public static final Integer CATEGORY_VIDEO;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.util.HashMap mAdjustedSplineMapper;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/lang/Boolean;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Integer mApplicationCategory;
private Float mAutoBrightnessAdjustment;
private final mBrightness;
private com.android.server.display.BrightnessMappingStrategyStub mBrightnessMappingStrategyImpl;
private Boolean mBrightnessRangeAdjustmentApplied;
private android.util.Spline mBrightnessSpline;
private android.util.Spline mBrightnessToAdjustedNitsSpline;
private android.util.Spline mBrightnessToNitsSpline;
private android.hardware.display.BrightnessConfiguration mConfig;
private final android.hardware.display.BrightnessConfiguration mDefaultConfig;
private Boolean mIsGlobalAdjustment;
private final Float mMaxGamma;
private com.android.server.display.MiuiDisplayCloudController mMiuiDisplayCloudController;
private final mNits;
private android.util.Spline mNitsToBrightnessSpline;
private Float mShortTermModelUserBrightness;
private Float mShortTermModelUserLux;
private java.util.HashMap mSplineGroup;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Landroid/util/Spline;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.display.MiuiPhysicalBrightnessMappingStrategy ( ) {
/* .locals 4 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .param p2, "nits" # [F */
/* .param p3, "brightness" # [F */
/* .param p4, "maxGamma" # F */
/* .line 71 */
/* invoke-direct {p0}, Lcom/android/server/display/BrightnessMappingStrategy;-><init>()V */
/* .line 31 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mSplineGroup = v0;
/* .line 33 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAdjustedSplineMapper = v0;
/* .line 73 */
/* array-length v0, p2 */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, p3 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
final String v3 = "Nits and brightness arrays must not be empty!"; // const-string v3, "Nits and brightness arrays must not be empty!"
com.android.internal.util.Preconditions .checkArgument ( v0,v3 );
/* .line 76 */
/* array-length v0, p2 */
/* array-length v3, p3 */
/* if-ne v0, v3, :cond_1 */
} // :cond_1
/* move v1, v2 */
} // :goto_1
final String v0 = "Nits and brightness arrays must be the same length!"; // const-string v0, "Nits and brightness arrays must be the same length!"
com.android.internal.util.Preconditions .checkArgument ( v1,v0 );
/* .line 78 */
java.util.Objects .requireNonNull ( p1 );
/* .line 79 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
final String v1 = "nits"; // const-string v1, "nits"
int v3 = 0; // const/4 v3, 0x0
com.android.internal.util.Preconditions .checkArrayElementsInRange ( p2,v3,v0,v1 );
/* .line 80 */
/* const/high16 v0, 0x3f800000 # 1.0f */
final String v1 = "brightness"; // const-string v1, "brightness"
com.android.internal.util.Preconditions .checkArrayElementsInRange ( p3,v3,v0,v1 );
/* .line 82 */
com.android.server.display.BrightnessMappingStrategyStub .getInstance ( );
this.mBrightnessMappingStrategyImpl = v0;
/* .line 83 */
/* iput p4, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mMaxGamma:F */
/* .line 84 */
/* iput v3, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
/* .line 85 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* iput v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* .line 86 */
/* iput v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserBrightness:F */
/* .line 87 */
/* iput v2, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mApplicationCategory:I */
/* .line 88 */
/* iput-boolean v2, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mIsGlobalAdjustment:Z */
/* .line 89 */
this.mNits = p2;
/* .line 90 */
this.mBrightness = p3;
/* .line 91 */
/* invoke-direct {p0, p2}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeNitsBrightnessSplines([F)V */
/* .line 92 */
this.mDefaultConfig = p1;
/* .line 93 */
this.mConfig = p1;
/* .line 94 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeSpline()V */
/* .line 95 */
return;
} // .end method
private void adjustSplineGroup ( Integer p0, android.util.Spline p1 ) {
/* .locals 4 */
/* .param p1, "category" # I */
/* .param p2, "spline" # Landroid/util/Spline; */
/* .line 105 */
/* iget-boolean v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mIsGlobalAdjustment:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 6; // const/4 v1, 0x6
/* if-ge v0, v1, :cond_0 */
/* .line 107 */
v1 = this.mSplineGroup;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).put ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 106 */
/* add-int/lit8 v0, v0, 0x1 */
} // .end local v0 # "i":I
} // :cond_0
/* .line 109 */
} // :cond_1
/* if-nez p1, :cond_4 */
/* .line 110 */
v0 = this.mAdjustedSplineMapper;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 111 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Boolean;>;" */
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* if-nez v2, :cond_2 */
/* .line 112 */
v2 = this.mSplineGroup;
/* check-cast v3, Ljava/lang/Integer; */
(( java.util.HashMap ) v2 ).put ( v3, p2 ); // invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 114 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Boolean;>;"
} // :cond_2
} // :cond_3
/* .line 116 */
} // :cond_4
v0 = this.mSplineGroup;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mApplicationCategory:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 117 */
v0 = this.mAdjustedSplineMapper;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mApplicationCategory:I */
java.lang.Integer .valueOf ( v1 );
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 119 */
} // :goto_2
return;
} // .end method
private Integer applicationCategoryInfo ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 318 */
v0 = this.mMiuiDisplayCloudController;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 319 */
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).getShortTermModelAppMapper ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getShortTermModelAppMapper()Ljava/util/Map;
/* .line 320 */
/* .local v0, "appMapper":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;" */
if ( v0 != null) { // if-eqz v0, :cond_4
v1 = this.mMiuiDisplayCloudController;
v1 = (( com.android.server.display.MiuiDisplayCloudController ) v1 ).isShortTermModelEnable ( ); // invoke-virtual {v1}, Lcom/android/server/display/MiuiDisplayCloudController;->isShortTermModelEnable()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 321 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
/* check-cast v2, Ljava/util/List; */
java.util.Objects .requireNonNull ( v2 );
v2 = /* check-cast v2, Ljava/util/List; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 322 */
/* .line 323 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
java.lang.Integer .valueOf ( v1 );
/* check-cast v2, Ljava/util/List; */
java.util.Objects .requireNonNull ( v2 );
v2 = /* check-cast v2, Ljava/util/List; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 324 */
/* .line 325 */
} // :cond_1
int v1 = 3; // const/4 v1, 0x3
java.lang.Integer .valueOf ( v1 );
/* check-cast v2, Ljava/util/List; */
java.util.Objects .requireNonNull ( v2 );
v2 = /* check-cast v2, Ljava/util/List; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 326 */
/* .line 327 */
} // :cond_2
int v1 = 4; // const/4 v1, 0x4
java.lang.Integer .valueOf ( v1 );
/* check-cast v2, Ljava/util/List; */
java.util.Objects .requireNonNull ( v2 );
v2 = /* check-cast v2, Ljava/util/List; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 328 */
/* .line 329 */
} // :cond_3
int v1 = 5; // const/4 v1, 0x5
java.lang.Integer .valueOf ( v1 );
/* check-cast v2, Ljava/util/List; */
java.util.Objects .requireNonNull ( v2 );
v2 = /* check-cast v2, Ljava/util/List; */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 330 */
/* .line 334 */
} // .end local v0 # "appMapper":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void computeNitsBrightnessSplines ( Float[] p0 ) {
/* .locals 1 */
/* .param p1, "nits" # [F */
/* .line 338 */
v0 = this.mBrightness;
android.util.Spline .createLinearSpline ( p1,v0 );
this.mNitsToBrightnessSpline = v0;
/* .line 340 */
v0 = this.mBrightness;
android.util.Spline .createLinearSpline ( v0,p1 );
this.mBrightnessToNitsSpline = v0;
/* .line 342 */
return;
} // .end method
private void computeSpline ( ) {
/* .locals 11 */
/* .line 272 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getCurve ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getCurve()Landroid/util/Pair;
/* .line 273 */
/* .local v0, "defaultCurve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;" */
v1 = this.first;
/* check-cast v1, [F */
/* .line 274 */
/* .local v1, "defaultLux":[F */
v2 = this.second;
/* move-object v9, v2 */
/* check-cast v9, [F */
/* .line 275 */
/* .local v9, "defaultNits":[F */
/* array-length v2, v9 */
/* new-array v10, v2, [F */
/* .line 276 */
/* .local v10, "defaultBrightness":[F */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v10 */
/* if-ge v2, v3, :cond_0 */
/* .line 277 */
v3 = this.mNitsToBrightnessSpline;
/* aget v4, v9, v2 */
v3 = (( android.util.Spline ) v3 ).interpolate ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/Spline;->interpolate(F)F
/* aput v3, v10, v2 */
/* .line 276 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 279 */
} // .end local v2 # "i":I
} // :cond_0
/* iget v5, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* iget v6, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserBrightness:F */
/* iget v7, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
/* iget v8, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mMaxGamma:F */
/* move-object v2, p0 */
/* move-object v3, v1 */
/* move-object v4, v10 */
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->getAdjustedCurve([F[FFFFF)Landroid/util/Pair; */
/* .line 281 */
/* .local v2, "curve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;" */
v3 = this.first;
/* check-cast v3, [F */
/* .line 282 */
/* .local v3, "lux":[F */
v4 = this.second;
/* check-cast v4, [F */
/* .line 283 */
/* .local v4, "brightness":[F */
/* array-length v5, v4 */
/* new-array v5, v5, [F */
/* .line 284 */
/* .local v5, "nits":[F */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_1
/* array-length v7, v5 */
/* if-ge v6, v7, :cond_1 */
/* .line 285 */
v7 = this.mBrightnessToNitsSpline;
/* aget v8, v4, v6 */
v7 = (( android.util.Spline ) v7 ).interpolate ( v8 ); // invoke-virtual {v7, v8}, Landroid/util/Spline;->interpolate(F)F
/* aput v7, v5, v6 */
/* .line 284 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 287 */
} // .end local v6 # "i":I
} // :cond_1
android.util.Spline .createSpline ( v3,v5 );
this.mBrightnessSpline = v6;
/* .line 288 */
/* iget v7, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* const/high16 v8, -0x40800000 # -1.0f */
/* cmpl-float v7, v7, v8 */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 289 */
/* iget v7, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mApplicationCategory:I */
/* invoke-direct {p0, v7, v6}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->adjustSplineGroup(ILandroid/util/Spline;)V */
/* .line 291 */
} // :cond_2
/* invoke-direct {p0, v6}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->initializeSplineGroup(Landroid/util/Spline;)V */
/* .line 293 */
} // :goto_2
return;
} // .end method
private Float correctBrightness ( Float p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "brightness" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "category" # I */
/* .line 302 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 303 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getCorrectionByPackageName ( p2 ); // invoke-virtual {v0, p2}, Landroid/hardware/display/BrightnessConfiguration;->getCorrectionByPackageName(Ljava/lang/String;)Landroid/hardware/display/BrightnessCorrection;
/* .line 304 */
/* .local v0, "correction":Landroid/hardware/display/BrightnessCorrection; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 305 */
v1 = (( android.hardware.display.BrightnessCorrection ) v0 ).apply ( p1 ); // invoke-virtual {v0, p1}, Landroid/hardware/display/BrightnessCorrection;->apply(F)F
/* .line 308 */
} // .end local v0 # "correction":Landroid/hardware/display/BrightnessCorrection;
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
/* if-eq p3, v0, :cond_1 */
/* .line 309 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getCorrectionByCategory ( p3 ); // invoke-virtual {v0, p3}, Landroid/hardware/display/BrightnessConfiguration;->getCorrectionByCategory(I)Landroid/hardware/display/BrightnessCorrection;
/* .line 310 */
/* .restart local v0 # "correction":Landroid/hardware/display/BrightnessCorrection; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 311 */
v1 = (( android.hardware.display.BrightnessCorrection ) v0 ).apply ( p1 ); // invoke-virtual {v0, p1}, Landroid/hardware/display/BrightnessCorrection;->apply(F)F
/* .line 314 */
} // .end local v0 # "correction":Landroid/hardware/display/BrightnessCorrection;
} // :cond_1
} // .end method
private Float getUnadjustedBrightness ( Float p0 ) {
/* .locals 4 */
/* .param p1, "lux" # F */
/* .line 296 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getCurve ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getCurve()Landroid/util/Pair;
/* .line 297 */
/* .local v0, "curve":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;" */
v1 = this.first;
/* check-cast v1, [F */
v2 = this.second;
/* check-cast v2, [F */
android.util.Spline .createSpline ( v1,v2 );
/* .line 298 */
/* .local v1, "spline":Landroid/util/Spline; */
v2 = this.mNitsToBrightnessSpline;
v3 = (( android.util.Spline ) v1 ).interpolate ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/Spline;->interpolate(F)F
v2 = (( android.util.Spline ) v2 ).interpolate ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F
} // .end method
private void initializeSplineGroup ( android.util.Spline p0 ) {
/* .locals 4 */
/* .param p1, "spline" # Landroid/util/Spline; */
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 6; // const/4 v1, 0x6
/* if-ge v0, v1, :cond_0 */
/* .line 99 */
v1 = this.mSplineGroup;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).put ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 100 */
v1 = this.mAdjustedSplineMapper;
java.lang.Integer .valueOf ( v0 );
int v3 = 0; // const/4 v3, 0x0
java.lang.Boolean .valueOf ( v3 );
(( java.util.HashMap ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 98 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 102 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void isGlobalAdjustment ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 349 */
v0 = this.mMiuiDisplayCloudController;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 350 */
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).getShortTermModelAppMapper ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getShortTermModelAppMapper()Ljava/util/Map;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMiuiDisplayCloudController;
/* .line 351 */
(( com.android.server.display.MiuiDisplayCloudController ) v0 ).getShortTermModelAppMapper ( ); // invoke-virtual {v0}, Lcom/android/server/display/MiuiDisplayCloudController;->getShortTermModelAppMapper()Ljava/util/Map;
/* .line 352 */
java.lang.Integer .valueOf ( v1 );
/* check-cast v0, Ljava/util/List; */
/* .line 351 */
java.util.Objects .requireNonNull ( v0 );
/* check-cast v0, Ljava/util/List; */
v0 = /* .line 352 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* nop */
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mIsGlobalAdjustment:Z */
/* .line 353 */
return;
} // .end method
/* # virtual methods */
public void addUserDataPoint ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "lux" # F */
/* .param p2, "brightness" # F */
/* .line 196 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.display.MiuiPhysicalBrightnessMappingStrategy ) p0 ).addUserDataPoint ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->addUserDataPoint(FFLjava/lang/String;)V
/* .line 197 */
return;
} // .end method
public void addUserDataPoint ( Float p0, Float p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "lux" # F */
/* .param p2, "brightness" # F */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 200 */
/* invoke-direct {p0, p3}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->isGlobalAdjustment(Ljava/lang/String;)V */
/* .line 201 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->applicationCategoryInfo(Ljava/lang/String;)I */
/* iput v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mApplicationCategory:I */
/* .line 202 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "User add data point: lux = "; // const-string v1, "User add data point: lux = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", brightness = "; // const-string v1, ", brightness = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", packageName = "; // const-string v1, ", packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", category ="; // const-string v1, ", category ="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mApplicationCategory:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiBrightnessMappingStrategy"; // const-string v1, "MiuiBrightnessMappingStrategy"
android.util.Slog .d ( v1,v0 );
/* .line 204 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->getUnadjustedBrightness(F)F */
/* .line 205 */
/* .local v0, "unadjustedBrightness":F */
v1 = this.mBrightnessMappingStrategyImpl;
/* .line 206 */
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mMaxGamma:F */
v1 = (( com.android.server.display.MiuiPhysicalBrightnessMappingStrategy ) p0 ).inferAutoBrightnessAdjustment ( v1, p2, v0 ); // invoke-virtual {p0, v1, p2, v0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->inferAutoBrightnessAdjustment(FFF)F
/* .line 209 */
/* .local v1, "adjustment":F */
/* iput v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
/* .line 210 */
/* iput p1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* .line 211 */
/* iput p2, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserBrightness:F */
/* .line 212 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeSpline()V */
/* .line 213 */
return;
} // .end method
public void clearUserDataPoints ( ) {
/* .locals 3 */
/* .line 217 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 218 */
final String v0 = "MiuiBrightnessMappingStrategy"; // const-string v0, "MiuiBrightnessMappingStrategy"
final String v2 = "Clear user data points."; // const-string v2, "Clear user data points."
android.util.Slog .d ( v0,v2 );
/* .line 219 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
/* .line 220 */
/* iput v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* .line 221 */
/* iput v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserBrightness:F */
/* .line 222 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeSpline()V */
/* .line 224 */
} // :cond_0
return;
} // .end method
public Float convertToAdjustedNits ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 186 */
v0 = this.mBrightnessToAdjustedNitsSpline;
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
} // .end method
public Float convertToBrightness ( Float p0 ) {
/* .locals 1 */
/* .param p1, "nit" # F */
/* .line 191 */
v0 = this.mNitsToBrightnessSpline;
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
} // .end method
public Float convertToFloatScale ( Float p0 ) {
/* .locals 1 */
/* .param p1, "nits" # F */
/* .line 367 */
v0 = this.mNitsToBrightnessSpline;
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
} // .end method
public Float convertToNits ( Float p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # F */
/* .line 181 */
v0 = this.mBrightnessToNitsSpline;
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
} // .end method
public void dump ( java.io.PrintWriter p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "hbmTransition" # F */
/* .line 372 */
final String v0 = "MiuiPhysicalMappingStrategy Configuration:"; // const-string v0, "MiuiPhysicalMappingStrategy Configuration:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 373 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mConfig="; // const-string v1, " mConfig="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mConfig;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 374 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mBrightnessSpline="; // const-string v1, " mBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBrightnessSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 375 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mNitsToBacklightSpline="; // const-string v1, " mNitsToBacklightSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mNitsToBrightnessSpline;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 376 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mMaxGamma="; // const-string v1, " mMaxGamma="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mMaxGamma:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 377 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mAutoBrightnessAdjustment="; // const-string v1, " mAutoBrightnessAdjustment="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 378 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mShortTermModelUserLux="; // const-string v1, " mShortTermModelUserLux="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 379 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mShortTermModelUserBrightness="; // const-string v1, " mShortTermModelUserBrightness="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserBrightness:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 380 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mDefaultConfig="; // const-string v1, " mDefaultConfig="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDefaultConfig;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 381 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " UndefinedBrightnessSpline="; // const-string v1, " UndefinedBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplineGroup;
int v2 = 0; // const/4 v2, 0x0
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 382 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " GameBrightnessSpline="; // const-string v1, " GameBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplineGroup;
int v2 = 1; // const/4 v2, 0x1
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 383 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " VideoBrightnessSpline="; // const-string v1, " VideoBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplineGroup;
int v2 = 2; // const/4 v2, 0x2
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 384 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " MapsBrightnessSpline="; // const-string v1, " MapsBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplineGroup;
int v2 = 3; // const/4 v2, 0x3
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 385 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " ImageBrightnessSpline="; // const-string v1, " ImageBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplineGroup;
int v2 = 4; // const/4 v2, 0x4
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 386 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " ReadersBrightnessSpline="; // const-string v1, " ReadersBrightnessSpline="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSplineGroup;
int v2 = 5; // const/4 v2, 0x5
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 387 */
return;
} // .end method
public Float getAutoBrightnessAdjustment ( ) {
/* .locals 1 */
/* .line 165 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
} // .end method
public Float getBrightness ( Float p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 155 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->applicationCategoryInfo(Ljava/lang/String;)I */
/* .line 156 */
/* .local v0, "category":I */
v1 = this.mSplineGroup;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/util/Spline; */
java.util.Objects .requireNonNull ( v1 );
/* check-cast v1, Landroid/util/Spline; */
v1 = (( android.util.Spline ) v1 ).interpolate ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 157 */
/* .local v1, "nits":F */
v2 = this.mNitsToBrightnessSpline;
v2 = (( android.util.Spline ) v2 ).interpolate ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/Spline;->interpolate(F)F
/* .line 158 */
/* .local v2, "backlight":F */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "autobrightness adjustment by applications is applied, lux = "; // const-string v4, "autobrightness adjustment by applications is applied, lux = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", brightness = "; // const-string v4, ", brightness = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = ", appPackageName = "; // const-string v4, ", appPackageName = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiBrightnessMappingStrategy"; // const-string v4, "MiuiBrightnessMappingStrategy"
android.util.Slog .d ( v4,v3 );
/* .line 160 */
} // .end method
public Float getBrightness ( Float p0, java.lang.String p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "lux" # F */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "category" # I */
/* .line 142 */
v0 = this.mBrightnessSpline;
v0 = (( android.util.Spline ) v0 ).interpolate ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F
/* .line 143 */
/* .local v0, "nits":F */
v1 = this.mNitsToBrightnessSpline;
v1 = (( android.util.Spline ) v1 ).interpolate ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/Spline;->interpolate(F)F
/* .line 146 */
/* .local v1, "brightness":F */
/* iget v2, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* const/high16 v3, -0x40800000 # -1.0f */
/* cmpl-float v2, v2, v3 */
/* if-nez v2, :cond_0 */
/* .line 147 */
v1 = /* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->correctBrightness(FLjava/lang/String;I)F */
/* .line 148 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mLoggingEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 149 */
final String v2 = "MiuiBrightnessMappingStrategy"; // const-string v2, "MiuiBrightnessMappingStrategy"
/* const-string/jumbo v3, "user point set, correction not applied" */
android.util.Slog .d ( v2,v3 );
/* .line 151 */
} // :cond_1
} // :goto_0
} // .end method
public android.hardware.display.BrightnessConfiguration getBrightnessConfiguration ( ) {
/* .locals 1 */
/* .line 136 */
v0 = this.mConfig;
} // .end method
public android.util.Spline getBrightnessSpline ( ) {
/* .locals 1 */
/* .line 254 */
v0 = this.mBrightnessSpline;
} // .end method
public android.hardware.display.BrightnessConfiguration getDefaultConfig ( ) {
/* .locals 1 */
/* .line 238 */
v0 = this.mDefaultConfig;
} // .end method
public Long getShortTermModelTimeout ( ) {
/* .locals 4 */
/* .line 264 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getShortTermModelTimeoutMillis ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getShortTermModelTimeoutMillis()J
/* move-result-wide v0 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-ltz v0, :cond_0 */
/* .line 265 */
v0 = this.mConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getShortTermModelTimeoutMillis ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getShortTermModelTimeoutMillis()J
/* move-result-wide v0 */
/* return-wide v0 */
/* .line 267 */
} // :cond_0
v0 = this.mDefaultConfig;
(( android.hardware.display.BrightnessConfiguration ) v0 ).getShortTermModelTimeoutMillis ( ); // invoke-virtual {v0}, Landroid/hardware/display/BrightnessConfiguration;->getShortTermModelTimeoutMillis()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
Float getUserBrightness ( ) {
/* .locals 1 */
/* .line 362 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserBrightness:F */
} // .end method
public Float getUserDataPoint ( ) {
/* .locals 1 */
/* .line 249 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
} // .end method
Float getUserLux ( ) {
/* .locals 1 */
/* .line 357 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
} // .end method
public Boolean hasUserDataPoints ( ) {
/* .locals 2 */
/* .line 228 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mShortTermModelUserLux:F */
/* const/high16 v1, -0x40800000 # -1.0f */
/* cmpl-float v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isDefaultConfig ( ) {
/* .locals 2 */
/* .line 233 */
v0 = this.mDefaultConfig;
v1 = this.mConfig;
v0 = (( android.hardware.display.BrightnessConfiguration ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/BrightnessConfiguration;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isForIdleMode ( ) {
/* .locals 1 */
/* .line 259 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void recalculateSplines ( Boolean p0, Float[] p1 ) {
/* .locals 1 */
/* .param p1, "applyAdjustment" # Z */
/* .param p2, "adjustedNits" # [F */
/* .line 243 */
/* iput-boolean p1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mBrightnessRangeAdjustmentApplied:Z */
/* .line 244 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-object v0, p2 */
} // :cond_0
v0 = this.mNits;
} // :goto_0
/* invoke-direct {p0, v0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeNitsBrightnessSplines([F)V */
/* .line 245 */
return;
} // .end method
public Boolean setAutoBrightnessAdjustment ( Float p0 ) {
/* .locals 2 */
/* .param p1, "adjustment" # F */
/* .line 170 */
/* const/high16 v0, -0x40800000 # -1.0f */
/* const/high16 v1, 0x3f800000 # 1.0f */
p1 = android.util.MathUtils .constrain ( p1,v0,v1 );
/* .line 171 */
/* iget v0, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
/* cmpl-float v0, p1, v0 */
/* if-nez v0, :cond_0 */
/* .line 172 */
int v0 = 0; // const/4 v0, 0x0
/* .line 174 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->mAutoBrightnessAdjustment:F */
/* .line 175 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeSpline()V */
/* .line 176 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean setBrightnessConfiguration ( android.hardware.display.BrightnessConfiguration p0 ) {
/* .locals 1 */
/* .param p1, "config" # Landroid/hardware/display/BrightnessConfiguration; */
/* .line 123 */
/* if-nez p1, :cond_0 */
/* .line 124 */
p1 = this.mDefaultConfig;
/* .line 126 */
} // :cond_0
v0 = this.mConfig;
v0 = (( android.hardware.display.BrightnessConfiguration ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/hardware/display/BrightnessConfiguration;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 127 */
int v0 = 0; // const/4 v0, 0x0
/* .line 129 */
} // :cond_1
this.mConfig = p1;
/* .line 130 */
/* invoke-direct {p0}, Lcom/android/server/display/MiuiPhysicalBrightnessMappingStrategy;->computeSpline()V */
/* .line 131 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
protected void setMiuiDisplayCloudController ( com.android.server.display.MiuiDisplayCloudController p0 ) {
/* .locals 0 */
/* .param p1, "cloudController" # Lcom/android/server/display/MiuiDisplayCloudController; */
/* .line 345 */
this.mMiuiDisplayCloudController = p1;
/* .line 346 */
return;
} // .end method
