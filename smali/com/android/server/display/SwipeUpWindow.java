public class com.android.server.display.SwipeUpWindow {
	 /* .source "SwipeUpWindow.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;, */
	 /* Lcom/android/server/display/SwipeUpWindow$AnimationState;, */
	 /* Lcom/android/server/display/SwipeUpWindow$SwipeUpFrameLayout;, */
	 /* Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Float BG_INIT_ALPHA;
private static final Boolean DEBUG;
private static final Float DEFAULT_DAMPING;
private static final Float DEFAULT_STIFFNESS;
private static final Float DISTANCE_DEBOUNCE;
private static final Integer ICON_HEIGHT;
private static final Float ICON_OFFSET;
private static final Float ICON_TIP_SHOW_DAMPING;
private static final Float ICON_TIP_SHOW_STIFFNESS;
private static final Float ICON_VIEW_BOTTOM;
private static final Integer ICON_WIDTH;
private static final Integer LOCK_SATE_START_DELAY;
private static final Float LOCK_STATE_LONG_DAMPING;
private static final Float LOCK_STATE_LONG_STIFFNESS;
private static final Integer MSG_ICON_LOCK_ANIMATION;
private static final Integer MSG_ICON_TIP_HIDE_ANIMATION;
private static final Integer MSG_LOCK_STATE_WITH_LONG_ANIMATION;
private static final Integer MSG_RELEASE_WINDOW;
private static final Integer MSG_SCREEN_OFF;
private static final Integer SCREEN_HEIGHT;
private static final Integer SCREEN_OFF_DELAY;
private static final Integer SCREEN_WIDTH;
private static final Float SCROLL_DAMPING;
private static final Float SCROLL_STIFFNESS;
private static final Float SHOW_STATE_DAMPING;
private static final Float SHOW_STATE_STIFFNESS;
public static final java.lang.String TAG;
private static final Integer TIP_HEIGHT;
private static final Float TIP_INIT_ALPHA;
private static final Float TIP_OFFSET;
private static final Integer TIP_TEXT_SIZE;
private static final Float TIP_VIEW_BOTTOM;
/* # instance fields */
private com.android.server.display.SwipeUpWindow$AnimationState mAnimationState;
private com.android.server.display.BlackLinearGradientView mBlackLinearGradientView;
private android.content.Context mContext;
private com.android.server.display.animation.SpringAnimation mGradientShadowSpringAnimation;
private android.os.Handler mHandler;
private android.graphics.drawable.AnimatedVectorDrawable mIconDrawable;
private com.android.server.display.SwipeUpWindow$DualSpringAnimation mIconSpringAnimation;
private android.widget.ImageView mIconView;
private com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener mLockStateLongAnimationEndListener;
private com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener mLockStateShortAnimationEndListener;
private android.view.View$OnTouchListener mOnTouchListener;
private android.os.PowerManager mPowerManager;
private Integer mPreBlurLevel;
private Integer mPreBottomColor;
private com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener mPreOnAnimationEndListener;
private Integer mPreTopColor;
private Integer mScreenHeight;
private Integer mScreenWidth;
private Boolean mScrollAnimationNeedInit;
private Float mStartPer;
private android.widget.FrameLayout mSwipeUpFrameLayout;
private android.view.WindowManager$LayoutParams mSwipeUpLayoutParams;
private Boolean mSwipeUpWindowShowing;
private com.android.server.display.SwipeUpWindow$DualSpringAnimation mTipSpringAnimation;
private android.widget.TextView mTipView;
private Float mUnlockDistance;
private com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener mUnlockStateAnimationEndListener;
private android.view.VelocityTracker mVelocityTracker;
private com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener mWakeStateAnimationEndListener;
private android.view.WindowManager mWindowManager;
/* # direct methods */
static com.android.server.display.SwipeUpWindow$AnimationState -$$Nest$fgetmAnimationState ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mAnimationState;
} // .end method
static com.android.server.display.animation.SpringAnimation -$$Nest$fgetmGradientShadowSpringAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mGradientShadowSpringAnimation;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static com.android.server.display.SwipeUpWindow$DualSpringAnimation -$$Nest$fgetmIconSpringAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mIconSpringAnimation;
} // .end method
static android.widget.ImageView -$$Nest$fgetmIconView ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mIconView;
} // .end method
static Integer -$$Nest$fgetmScreenHeight ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
} // .end method
static Boolean -$$Nest$fgetmScrollAnimationNeedInit ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/display/SwipeUpWindow;->mScrollAnimationNeedInit:Z */
} // .end method
static Float -$$Nest$fgetmStartPer ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/SwipeUpWindow;->mStartPer:F */
} // .end method
static com.android.server.display.SwipeUpWindow$DualSpringAnimation -$$Nest$fgetmTipSpringAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mTipSpringAnimation;
} // .end method
static android.widget.TextView -$$Nest$fgetmTipView ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mTipView;
} // .end method
static Float -$$Nest$fgetmUnlockDistance ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/display/SwipeUpWindow;->mUnlockDistance:F */
} // .end method
static android.view.VelocityTracker -$$Nest$fgetmVelocityTracker ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mVelocityTracker;
} // .end method
static void -$$Nest$fputmIconSpringAnimation ( com.android.server.display.SwipeUpWindow p0, com.android.server.display.SwipeUpWindow$DualSpringAnimation p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mIconSpringAnimation = p1;
	 return;
} // .end method
static void -$$Nest$fputmScrollAnimationNeedInit ( com.android.server.display.SwipeUpWindow p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/display/SwipeUpWindow;->mScrollAnimationNeedInit:Z */
	 return;
} // .end method
static void -$$Nest$fputmStartPer ( com.android.server.display.SwipeUpWindow p0, Float p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/display/SwipeUpWindow;->mStartPer:F */
	 return;
} // .end method
static void -$$Nest$fputmTipSpringAnimation ( com.android.server.display.SwipeUpWindow p0, com.android.server.display.SwipeUpWindow$DualSpringAnimation p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mTipSpringAnimation = p1;
	 return;
} // .end method
static void -$$Nest$fputmVelocityTracker ( com.android.server.display.SwipeUpWindow p0, android.view.VelocityTracker p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mVelocityTracker = p1;
	 return;
} // .end method
static com.android.server.display.animation.SpringAnimation -$$Nest$mcreatSpringAnimation ( com.android.server.display.SwipeUpWindow p0, android.view.View p1, com.android.server.display.animation.DynamicAnimation$ViewProperty p2, Float p3, Float p4, Float p5 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct/range {p0 ..p5}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
} // .end method
static void -$$Nest$mhandleScreenOff ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->handleScreenOff()V */
	 return;
} // .end method
static void -$$Nest$minitVelocityTrackerIfNotExists ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initVelocityTrackerIfNotExists()V */
	 return;
} // .end method
static void -$$Nest$mplayIconAndTipHideAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipHideAnimation()V */
	 return;
} // .end method
static void -$$Nest$mplayIconAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAnimation()V */
	 return;
} // .end method
static void -$$Nest$mrecycleVelocityTracker ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->recycleVelocityTracker()V */
	 return;
} // .end method
static void -$$Nest$mresetIconAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->resetIconAnimation()V */
	 return;
} // .end method
static void -$$Nest$msetLockStateWithLongAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setLockStateWithLongAnimation()V */
	 return;
} // .end method
static void -$$Nest$msetLockStateWithShortAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setLockStateWithShortAnimation()V */
	 return;
} // .end method
static void -$$Nest$msetUnlockState ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setUnlockState()V */
	 return;
} // .end method
static void -$$Nest$msetWakeState ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setWakeState()V */
	 return;
} // .end method
static void -$$Nest$mstartGradientShadowAnimation ( com.android.server.display.SwipeUpWindow p0, Float p1, Float p2, Float p3 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFF)V */
	 return;
} // .end method
static void -$$Nest$mstartSwipeUpAnimation ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->startSwipeUpAnimation()V */
	 return;
} // .end method
static void -$$Nest$mupdateBlackView ( com.android.server.display.SwipeUpWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->updateBlackView()V */
	 return;
} // .end method
public com.android.server.display.SwipeUpWindow ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "looper" # Landroid/os/Looper; */
	 /* .line 129 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 119 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I */
	 /* .line 120 */
	 /* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I */
	 /* .line 121 */
	 /* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I */
	 /* .line 122 */
	 /* const/high16 v0, 0x3f800000 # 1.0f */
	 /* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mStartPer:F */
	 /* .line 525 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$3; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$3;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
	 this.mWakeStateAnimationEndListener = v0;
	 /* .line 539 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$4; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$4;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
	 this.mLockStateLongAnimationEndListener = v0;
	 /* .line 554 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$5; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$5;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
	 this.mLockStateShortAnimationEndListener = v0;
	 /* .line 566 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$6; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$6;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
	 this.mUnlockStateAnimationEndListener = v0;
	 /* .line 578 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$7; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$7;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
	 this.mOnTouchListener = v0;
	 /* .line 130 */
	 /* const v0, 0x7fffffff */
	 /* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
	 /* .line 131 */
	 /* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I */
	 /* .line 132 */
	 this.mContext = p1;
	 /* .line 133 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler; */
	 /* invoke-direct {v0, p0, p2}, Lcom/android/server/display/SwipeUpWindow$SwipeUpWindowHandler;-><init>(Lcom/android/server/display/SwipeUpWindow;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 134 */
	 /* new-instance v0, Lcom/android/server/display/SwipeUpWindow$AnimationState; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/display/SwipeUpWindow$AnimationState;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
	 this.mAnimationState = v0;
	 /* .line 136 */
	 v0 = this.mContext;
	 /* const-string/jumbo v1, "window" */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/view/WindowManager; */
	 this.mWindowManager = v0;
	 /* .line 137 */
	 v0 = this.mContext;
	 final String v1 = "power"; // const-string v1, "power"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/os/PowerManager; */
	 this.mPowerManager = v0;
	 /* .line 139 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->updateSettings(Z)V */
	 /* .line 140 */
	 /* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->updateSizeInfo()V */
	 /* .line 141 */
	 return;
} // .end method
private Integer calculateBlackAlpha ( Float p0 ) {
	 /* .locals 2 */
	 /* .param p1, "alpha" # F */
	 /* .line 423 */
	 /* const/high16 v0, 0x437f0000 # 255.0f */
	 /* mul-float/2addr v0, p1 */
	 /* float-to-int v0, v0 */
	 /* .line 425 */
	 /* .local v0, "blackAlpha":I */
	 /* shl-int/lit8 v1, v0, 0x18 */
	 /* or-int/lit8 v1, v1, 0x0 */
} // .end method
private Float constraintAlpha ( Float p0 ) {
	 /* .locals 2 */
	 /* .param p1, "alpha" # F */
	 /* .line 455 */
	 /* const/high16 v0, 0x3f800000 # 1.0f */
	 /* cmpl-float v1, p1, v0 */
	 /* if-lez v1, :cond_0 */
} // :goto_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v1, p1, v0 */
/* if-gez v1, :cond_1 */
} // :cond_1
/* move v0, p1 */
} // :goto_1
} // .end method
private Float constraintBlur ( Float p0 ) {
/* .locals 2 */
/* .param p1, "level" # F */
/* .line 459 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* cmpl-float v1, p1, v0 */
/* if-lez v1, :cond_0 */
} // :goto_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v1, p1, v0 */
/* if-gez v1, :cond_1 */
} // :cond_1
/* move v0, p1 */
} // :goto_1
} // .end method
private com.android.server.display.animation.SpringAnimation creatSpringAnimation ( android.view.View p0, com.android.server.display.animation.DynamicAnimation$ViewProperty p1, Float p2 ) {
/* .locals 6 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "viewProperty" # Lcom/android/server/display/animation/DynamicAnimation$ViewProperty; */
/* .param p3, "finalPosition" # F */
/* .line 782 */
/* const v3, 0x43a1228f */
/* const v4, 0x3f733333 # 0.95f */
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v5, p3 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
} // .end method
private com.android.server.display.animation.SpringAnimation creatSpringAnimation ( android.view.View p0, com.android.server.display.animation.DynamicAnimation$ViewProperty p1, Float p2, Float p3, Float p4 ) {
/* .locals 3 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "viewProperty" # Lcom/android/server/display/animation/DynamicAnimation$ViewProperty; */
/* .param p3, "stiffness" # F */
/* .param p4, "damping" # F */
/* .param p5, "finalPosition" # F */
/* .line 789 */
/* new-instance v0, Lcom/android/server/display/animation/SpringAnimation; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/display/animation/SpringAnimation;-><init>(Ljava/lang/Object;Lcom/android/server/display/animation/FloatPropertyCompat;)V */
/* .line 790 */
/* .local v0, "springAnimation":Lcom/android/server/display/animation/SpringAnimation; */
/* new-instance v1, Lcom/android/server/display/animation/SpringForce; */
/* invoke-direct {v1, p5}, Lcom/android/server/display/animation/SpringForce;-><init>(F)V */
/* .line 791 */
/* .local v1, "springForce":Lcom/android/server/display/animation/SpringForce; */
(( com.android.server.display.animation.SpringForce ) v1 ).setStiffness ( p3 ); // invoke-virtual {v1, p3}, Lcom/android/server/display/animation/SpringForce;->setStiffness(F)Lcom/android/server/display/animation/SpringForce;
/* .line 792 */
(( com.android.server.display.animation.SpringForce ) v1 ).setDampingRatio ( p4 ); // invoke-virtual {v1, p4}, Lcom/android/server/display/animation/SpringForce;->setDampingRatio(F)Lcom/android/server/display/animation/SpringForce;
/* .line 793 */
(( com.android.server.display.animation.SpringAnimation ) v0 ).setSpring ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/animation/SpringAnimation;->setSpring(Lcom/android/server/display/animation/SpringForce;)Lcom/android/server/display/animation/SpringAnimation;
/* .line 794 */
/* const/high16 v2, 0x3b800000 # 0.00390625f */
(( com.android.server.display.animation.SpringAnimation ) v0 ).setMinimumVisibleChange ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/animation/SpringAnimation;->setMinimumVisibleChange(F)Lcom/android/server/display/animation/DynamicAnimation;
/* .line 795 */
} // .end method
private void handleScreenOff ( ) {
/* .locals 5 */
/* .line 486 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 487 */
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
(( android.os.PowerManager ) v0 ).goToSleep ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->goToSleep(JII)V
/* .line 489 */
return;
} // .end method
private void initBlackLinearGradientView ( ) {
/* .locals 3 */
/* .line 239 */
/* new-instance v0, Landroid/widget/FrameLayout$LayoutParams; */
int v1 = -1; // const/4 v1, -0x1
/* const/16 v2, 0x11 */
/* invoke-direct {v0, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V */
/* .line 243 */
/* .local v0, "backViewParams":Landroid/widget/FrameLayout$LayoutParams; */
/* new-instance v1, Lcom/android/server/display/BlackLinearGradientView; */
v2 = this.mContext;
/* invoke-direct {v1, v2}, Lcom/android/server/display/BlackLinearGradientView;-><init>(Landroid/content/Context;)V */
this.mBlackLinearGradientView = v1;
/* .line 244 */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.display.BlackLinearGradientView ) v1 ).setBackgroundResource ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/BlackLinearGradientView;->setBackgroundResource(I)V
/* .line 245 */
v1 = this.mSwipeUpFrameLayout;
v2 = this.mBlackLinearGradientView;
(( android.widget.FrameLayout ) v1 ).addView ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
/* .line 246 */
return;
} // .end method
private void initGradientShadowAnimation ( ) {
/* .locals 4 */
/* .line 287 */
/* new-instance v0, Lcom/android/server/display/animation/SpringAnimation; */
v1 = this.mAnimationState;
/* new-instance v2, Lcom/android/server/display/SwipeUpWindow$1; */
final String v3 = "perState"; // const-string v3, "perState"
/* invoke-direct {v2, p0, v3}, Lcom/android/server/display/SwipeUpWindow$1;-><init>(Lcom/android/server/display/SwipeUpWindow;Ljava/lang/String;)V */
/* invoke-direct {v0, v1, v2}, Lcom/android/server/display/animation/SpringAnimation;-><init>(Ljava/lang/Object;Lcom/android/server/display/animation/FloatPropertyCompat;)V */
this.mGradientShadowSpringAnimation = v0;
/* .line 300 */
/* new-instance v0, Lcom/android/server/display/animation/SpringForce; */
/* invoke-direct {v0}, Lcom/android/server/display/animation/SpringForce;-><init>()V */
/* .line 301 */
/* .local v0, "springForce":Lcom/android/server/display/animation/SpringForce; */
/* const v1, 0x43a1228f */
(( com.android.server.display.animation.SpringForce ) v0 ).setStiffness ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/animation/SpringForce;->setStiffness(F)Lcom/android/server/display/animation/SpringForce;
/* .line 302 */
/* const v1, 0x3f733333 # 0.95f */
(( com.android.server.display.animation.SpringForce ) v0 ).setDampingRatio ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/animation/SpringForce;->setDampingRatio(F)Lcom/android/server/display/animation/SpringForce;
/* .line 303 */
v1 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v1 ).setSpring ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/animation/SpringAnimation;->setSpring(Lcom/android/server/display/animation/SpringForce;)Lcom/android/server/display/animation/SpringAnimation;
/* .line 304 */
v1 = this.mGradientShadowSpringAnimation;
/* const/high16 v2, 0x3b800000 # 0.00390625f */
(( com.android.server.display.animation.SpringAnimation ) v1 ).setMinimumVisibleChange ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/animation/SpringAnimation;->setMinimumVisibleChange(F)Lcom/android/server/display/animation/DynamicAnimation;
/* .line 307 */
v1 = this.mGradientShadowSpringAnimation;
/* new-instance v2, Lcom/android/server/display/SwipeUpWindow$2; */
/* invoke-direct {v2, p0}, Lcom/android/server/display/SwipeUpWindow$2;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
(( com.android.server.display.animation.SpringAnimation ) v1 ).addUpdateListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/display/animation/SpringAnimation;->addUpdateListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationUpdateListener;)Lcom/android/server/display/animation/DynamicAnimation;
/* .line 314 */
return;
} // .end method
private void initIconView ( ) {
/* .locals 5 */
/* .line 249 */
/* new-instance v0, Landroid/widget/ImageView; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V */
this.mIconView = v0;
/* .line 250 */
/* const v1, 0x1108019b */
(( android.widget.ImageView ) v0 ).setImageResource ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
/* .line 251 */
v0 = this.mIconView;
v1 = android.widget.ImageView$ScaleType.CENTER_CROP;
(( android.widget.ImageView ) v0 ).setScaleType ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V
/* .line 252 */
v0 = this.mIconView;
int v1 = 0; // const/4 v1, 0x0
(( android.widget.ImageView ) v0 ).setAlpha ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V
/* .line 253 */
v0 = this.mIconView;
int v1 = 0; // const/4 v1, 0x0
(( android.widget.ImageView ) v0 ).setBackgroundResource ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V
/* .line 254 */
/* new-instance v0, Landroid/widget/FrameLayout$LayoutParams; */
/* .line 255 */
/* const/high16 v2, 0x41e00000 # 28.0f */
v3 = (( com.android.server.display.SwipeUpWindow ) p0 ).transformDpToPx ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I
/* .line 256 */
v2 = (( com.android.server.display.SwipeUpWindow ) p0 ).transformDpToPx ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I
/* const/16 v4, 0x31 */
/* invoke-direct {v0, v3, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V */
/* .line 258 */
/* .local v0, "iconViewParams":Landroid/widget/FrameLayout$LayoutParams; */
/* iget v2, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
/* const/high16 v3, 0x43110000 # 145.0f */
v3 = (( com.android.server.display.SwipeUpWindow ) p0 ).transformDpToPx ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I
/* sub-int/2addr v2, v3 */
(( android.widget.FrameLayout$LayoutParams ) v0 ).setMargins ( v1, v2, v1, v1 ); // invoke-virtual {v0, v1, v2, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V
/* .line 259 */
v1 = this.mSwipeUpFrameLayout;
v2 = this.mIconView;
(( android.widget.FrameLayout ) v1 ).addView ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
/* .line 261 */
v1 = this.mIconView;
(( android.widget.ImageView ) v1 ).getDrawable ( ); // invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;
/* instance-of v1, v1, Landroid/graphics/drawable/AnimatedVectorDrawable; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 262 */
v1 = this.mIconView;
(( android.widget.ImageView ) v1 ).getDrawable ( ); // invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;
/* check-cast v1, Landroid/graphics/drawable/AnimatedVectorDrawable; */
this.mIconDrawable = v1;
/* .line 264 */
} // :cond_0
final String v1 = "SwipeUpWindow"; // const-string v1, "SwipeUpWindow"
final String v2 = "icon drawable get incompatible class"; // const-string v2, "icon drawable get incompatible class"
android.util.Slog .i ( v1,v2 );
/* .line 266 */
} // :goto_0
return;
} // .end method
private void initSwipeUpWindow ( ) {
/* .locals 10 */
/* .line 210 */
/* new-instance v0, Lcom/android/server/display/SwipeUpWindow$SwipeUpFrameLayout; */
/* new-instance v1, Landroid/view/ContextThemeWrapper; */
v2 = this.mContext;
/* const v3, 0x1030007 */
/* invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/display/SwipeUpWindow$SwipeUpFrameLayout;-><init>(Lcom/android/server/display/SwipeUpWindow;Landroid/content/Context;)V */
this.mSwipeUpFrameLayout = v0;
/* .line 212 */
/* new-instance v0, Landroid/view/WindowManager$LayoutParams; */
int v5 = -1; // const/4 v5, -0x1
int v6 = -1; // const/4 v6, -0x1
/* const/16 v7, 0x7ea */
/* const v8, 0x1820104 */
int v9 = -3; // const/4 v9, -0x3
/* move-object v4, v0 */
/* invoke-direct/range {v4 ..v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V */
this.mSwipeUpLayoutParams = v0;
/* .line 222 */
/* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I */
/* or-int/lit8 v1, v1, 0x2 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I */
/* .line 224 */
v0 = this.mSwipeUpLayoutParams;
int v1 = 3; // const/4 v1, 0x3
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 225 */
v0 = this.mSwipeUpLayoutParams;
/* const v1, 0x800033 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 226 */
v0 = this.mSwipeUpLayoutParams;
final String v1 = "SwipeUpWindow"; // const-string v1, "SwipeUpWindow"
(( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 227 */
v0 = this.mSwipeUpLayoutParams;
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I */
/* .line 228 */
v0 = this.mSwipeUpFrameLayout;
v2 = this.mOnTouchListener;
(( android.widget.FrameLayout ) v0 ).setOnTouchListener ( v2 ); // invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
/* .line 229 */
v0 = this.mSwipeUpFrameLayout;
(( android.widget.FrameLayout ) v0 ).setLongClickable ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLongClickable(Z)V
/* .line 230 */
v0 = this.mSwipeUpFrameLayout;
(( android.widget.FrameLayout ) v0 ).setFocusable ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setFocusable(Z)V
/* .line 232 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initBlackLinearGradientView()V */
/* .line 233 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initIconView()V */
/* .line 234 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initTipView()V */
/* .line 235 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initGradientShadowAnimation()V */
/* .line 236 */
return;
} // .end method
private void initTipView ( ) {
/* .locals 5 */
/* .line 269 */
/* new-instance v0, Landroid/widget/TextView; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V */
this.mTipView = v0;
/* .line 270 */
/* const/16 v1, 0x11 */
(( android.widget.TextView ) v0 ).setGravity ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V
/* .line 271 */
v0 = this.mTipView;
/* const v1, 0x110f03b1 */
(( android.widget.TextView ) v0 ).setText ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V
/* .line 272 */
v0 = this.mTipView;
int v1 = 2; // const/4 v1, 0x2
/* const/high16 v2, 0x41980000 # 19.0f */
(( android.widget.TextView ) v0 ).setTextSize ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V
/* .line 273 */
final String v0 = "mipro-medium"; // const-string v0, "mipro-medium"
int v1 = 0; // const/4 v1, 0x0
android.graphics.Typeface .create ( v0,v1 );
/* .line 274 */
/* .local v0, "typeface":Landroid/graphics/Typeface; */
v2 = this.mTipView;
(( android.widget.TextView ) v2 ).setTypeface ( v0 ); // invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
/* .line 275 */
v2 = this.mTipView;
/* const v3, 0x66ffffff */
(( android.widget.TextView ) v2 ).setTextColor ( v3 ); // invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V
/* .line 276 */
v2 = this.mTipView;
int v3 = 0; // const/4 v3, 0x0
(( android.widget.TextView ) v2 ).setAlpha ( v3 ); // invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V
/* .line 277 */
v2 = this.mTipView;
(( android.widget.TextView ) v2 ).setBackgroundResource ( v1 ); // invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V
/* .line 278 */
/* new-instance v2, Landroid/widget/FrameLayout$LayoutParams; */
int v3 = -2; // const/4 v3, -0x2
/* const/16 v4, 0x31 */
/* invoke-direct {v2, v3, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V */
/* .line 282 */
/* .local v2, "tipViewParams":Landroid/widget/FrameLayout$LayoutParams; */
/* iget v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
/* const/high16 v4, 0x42b00000 # 88.0f */
v4 = (( com.android.server.display.SwipeUpWindow ) p0 ).transformDpToPx ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I
/* sub-int/2addr v3, v4 */
(( android.widget.FrameLayout$LayoutParams ) v2 ).setMargins ( v1, v3, v1, v1 ); // invoke-virtual {v2, v1, v3, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V
/* .line 283 */
v1 = this.mSwipeUpFrameLayout;
v3 = this.mTipView;
(( android.widget.FrameLayout ) v1 ).addView ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
/* .line 284 */
return;
} // .end method
private void initVelocityTrackerIfNotExists ( ) {
/* .locals 1 */
/* .line 677 */
v0 = this.mVelocityTracker;
/* if-nez v0, :cond_0 */
/* .line 678 */
android.view.VelocityTracker .obtain ( );
this.mVelocityTracker = v0;
/* .line 680 */
} // :cond_0
return;
} // .end method
private void playIconAndTipHideAnimation ( ) {
/* .locals 5 */
/* .line 760 */
v0 = this.mIconSpringAnimation;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 761 */
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V
/* .line 763 */
} // :cond_0
v0 = this.mTipSpringAnimation;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 764 */
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V
/* .line 767 */
} // :cond_1
/* new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
v1 = this.mIconView;
v2 = com.android.server.display.animation.SpringAnimation.Y;
v3 = this.mIconView;
/* .line 768 */
v3 = (( android.widget.ImageView ) v3 ).getTop ( ); // invoke-virtual {v3}, Landroid/widget/ImageView;->getTop()I
/* int-to-float v3, v3 */
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation; */
v2 = this.mIconView;
v3 = com.android.server.display.animation.SpringAnimation.ALPHA;
/* .line 769 */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation; */
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V */
this.mIconSpringAnimation = v0;
/* .line 771 */
/* new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
v1 = this.mTipView;
v2 = com.android.server.display.animation.SpringAnimation.Y;
v3 = this.mTipView;
/* .line 772 */
v3 = (( android.widget.TextView ) v3 ).getTop ( ); // invoke-virtual {v3}, Landroid/widget/TextView;->getTop()I
/* int-to-float v3, v3 */
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation; */
v2 = this.mTipView;
v3 = com.android.server.display.animation.SpringAnimation.ALPHA;
/* .line 773 */
/* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;F)Lcom/android/server/display/animation/SpringAnimation; */
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V */
this.mTipSpringAnimation = v0;
/* .line 775 */
v0 = this.mIconSpringAnimation;
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V
/* .line 776 */
v0 = this.mTipSpringAnimation;
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V
/* .line 777 */
return;
} // .end method
private void playIconAndTipShowAnimation ( ) {
/* .locals 8 */
/* .line 736 */
v0 = this.mIconSpringAnimation;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 737 */
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V
/* .line 739 */
} // :cond_0
v0 = this.mTipSpringAnimation;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 740 */
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->cancel()V
/* .line 743 */
} // :cond_1
/* new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
v2 = this.mIconView;
v3 = com.android.server.display.animation.SpringAnimation.Y;
/* const v4, 0x421de9e6 */
/* const/high16 v5, 0x3f800000 # 1.0f */
v1 = this.mIconView;
/* .line 745 */
v1 = (( android.widget.ImageView ) v1 ).getTop ( ); // invoke-virtual {v1}, Landroid/widget/ImageView;->getTop()I
/* int-to-float v6, v1 */
/* .line 744 */
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
v3 = this.mIconView;
v4 = com.android.server.display.animation.SpringAnimation.ALPHA;
/* const v5, 0x421de9e6 */
/* const/high16 v6, 0x3f800000 # 1.0f */
/* const/high16 v7, 0x3f800000 # 1.0f */
/* .line 746 */
/* move-object v2, p0 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V */
this.mIconSpringAnimation = v0;
/* .line 749 */
/* new-instance v0, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation; */
v2 = this.mTipView;
v3 = com.android.server.display.animation.SpringAnimation.Y;
/* const v4, 0x421de9e6 */
/* const/high16 v5, 0x3f800000 # 1.0f */
v1 = this.mTipView;
/* .line 751 */
v1 = (( android.widget.TextView ) v1 ).getTop ( ); // invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I
/* int-to-float v6, v1 */
/* .line 750 */
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
v3 = this.mTipView;
v4 = com.android.server.display.animation.SpringAnimation.ALPHA;
/* const v5, 0x421de9e6 */
/* const/high16 v6, 0x3f800000 # 1.0f */
/* .line 752 */
/* move-object v2, p0 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/display/SwipeUpWindow;->creatSpringAnimation(Landroid/view/View;Lcom/android/server/display/animation/DynamicAnimation$ViewProperty;FFF)Lcom/android/server/display/animation/SpringAnimation; */
/* invoke-direct {v0, p0, v1, v2}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;-><init>(Lcom/android/server/display/SwipeUpWindow;Lcom/android/server/display/animation/SpringAnimation;Lcom/android/server/display/animation/SpringAnimation;)V */
this.mTipSpringAnimation = v0;
/* .line 755 */
v0 = this.mIconSpringAnimation;
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V
/* .line 756 */
v0 = this.mTipSpringAnimation;
(( com.android.server.display.SwipeUpWindow$DualSpringAnimation ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$DualSpringAnimation;->start()V
/* .line 757 */
return;
} // .end method
private void playIconAnimation ( ) {
/* .locals 2 */
/* .line 702 */
v0 = this.mIconDrawable;
/* if-nez v0, :cond_0 */
/* .line 703 */
return;
/* .line 706 */
} // :cond_0
/* new-instance v1, Lcom/android/server/display/SwipeUpWindow$8; */
/* invoke-direct {v1, p0}, Lcom/android/server/display/SwipeUpWindow$8;-><init>(Lcom/android/server/display/SwipeUpWindow;)V */
(( android.graphics.drawable.AnimatedVectorDrawable ) v0 ).registerAnimationCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimatedVectorDrawable;->registerAnimationCallback(Landroid/graphics/drawable/Animatable2$AnimationCallback;)V
/* .line 714 */
v0 = this.mIconDrawable;
(( android.graphics.drawable.AnimatedVectorDrawable ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V
/* .line 715 */
return;
} // .end method
private void prepareIconAndTipAnimation ( ) {
/* .locals 4 */
/* .line 728 */
v0 = this.mIconView;
v1 = (( android.widget.ImageView ) v0 ).getTop ( ); // invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I
/* const/high16 v2, 0x41f00000 # 30.0f */
v2 = (( com.android.server.display.SwipeUpWindow ) p0 ).transformDpToPx ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I
/* add-int/2addr v1, v2 */
/* int-to-float v1, v1 */
(( android.widget.ImageView ) v0 ).setY ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V
/* .line 729 */
v0 = this.mIconView;
int v1 = 0; // const/4 v1, 0x0
(( android.widget.ImageView ) v0 ).setAlpha ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V
/* .line 731 */
v0 = this.mTipView;
v2 = (( android.widget.TextView ) v0 ).getTop ( ); // invoke-virtual {v0}, Landroid/widget/TextView;->getTop()I
/* const/high16 v3, 0x42480000 # 50.0f */
v3 = (( com.android.server.display.SwipeUpWindow ) p0 ).transformDpToPx ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->transformDpToPx(F)I
/* add-int/2addr v2, v3 */
/* int-to-float v2, v2 */
(( android.widget.TextView ) v0 ).setY ( v2 ); // invoke-virtual {v0, v2}, Landroid/widget/TextView;->setY(F)V
/* .line 732 */
v0 = this.mTipView;
(( android.widget.TextView ) v0 ).setAlpha ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V
/* .line 733 */
return;
} // .end method
private void recycleVelocityTracker ( ) {
/* .locals 1 */
/* .line 683 */
v0 = this.mVelocityTracker;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 684 */
(( android.view.VelocityTracker ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V
/* .line 685 */
int v0 = 0; // const/4 v0, 0x0
this.mVelocityTracker = v0;
/* .line 687 */
} // :cond_0
return;
} // .end method
private void resetIconAnimation ( ) {
/* .locals 1 */
/* .line 718 */
v0 = this.mIconDrawable;
/* if-nez v0, :cond_0 */
/* .line 719 */
return;
/* .line 722 */
} // :cond_0
/* nop */
/* .line 723 */
v0 = this.mIconDrawable;
(( android.graphics.drawable.AnimatedVectorDrawable ) v0 ).reset ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->reset()V
/* .line 725 */
return;
} // .end method
private void setBackgroudBlur ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "blurRadius" # I */
/* .line 433 */
v0 = this.mSwipeUpFrameLayout;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v1, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I */
/* if-ne v1, p1, :cond_0 */
/* .line 440 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I */
/* .line 442 */
(( android.widget.FrameLayout ) v0 ).getViewRootImpl ( ); // invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewRootImpl()Landroid/view/ViewRootImpl;
/* .line 443 */
/* .local v0, "viewRootImpl":Landroid/view/ViewRootImpl; */
/* if-nez v0, :cond_1 */
/* .line 444 */
final String v1 = "SwipeUpWindow"; // const-string v1, "SwipeUpWindow"
final String v2 = "mViewRootImpl is null"; // const-string v2, "mViewRootImpl is null"
android.util.Slog .d ( v1,v2 );
/* .line 445 */
return;
/* .line 447 */
} // :cond_1
(( android.view.ViewRootImpl ) v0 ).getSurfaceControl ( ); // invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;
/* .line 448 */
/* .local v1, "surfaceControl":Landroid/view/SurfaceControl; */
/* new-instance v2, Landroid/view/SurfaceControl$Transaction; */
/* invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V */
/* .line 449 */
/* .local v2, "transaction":Landroid/view/SurfaceControl$Transaction; */
(( android.view.SurfaceControl$Transaction ) v2 ).setBackgroundBlurRadius ( v1, p1 ); // invoke-virtual {v2, v1, p1}, Landroid/view/SurfaceControl$Transaction;->setBackgroundBlurRadius(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
/* .line 450 */
(( android.view.SurfaceControl$Transaction ) v2 ).show ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 451 */
(( android.view.SurfaceControl$Transaction ) v2 ).apply ( ); // invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 452 */
return;
/* .line 434 */
} // .end local v0 # "viewRootImpl":Landroid/view/ViewRootImpl;
} // .end local v1 # "surfaceControl":Landroid/view/SurfaceControl;
} // .end local v2 # "transaction":Landroid/view/SurfaceControl$Transaction;
} // :cond_2
} // :goto_0
return;
} // .end method
private void setLockStateWithLongAnimation ( ) {
/* .locals 6 */
/* .line 348 */
/* const v1, 0x40db851f # 6.86f */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* const/high16 v3, 0x3f800000 # 1.0f */
v4 = this.mLockStateLongAnimationEndListener;
/* const/high16 v5, 0x3b800000 # 0.00390625f */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V */
/* .line 351 */
return;
} // .end method
private void setLockStateWithShortAnimation ( ) {
/* .locals 6 */
/* .line 354 */
/* const v1, 0x43a1228f */
/* const v2, 0x3f733333 # 0.95f */
/* const/high16 v3, 0x3f800000 # 1.0f */
v4 = this.mLockStateShortAnimationEndListener;
/* const/high16 v5, 0x3b800000 # 0.00390625f */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V */
/* .line 357 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipHideAnimation()V */
/* .line 358 */
return;
} // .end method
private void setUnlockState ( ) {
/* .locals 8 */
/* .line 361 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 362 */
/* const v3, 0x43a1228f */
/* const v4, 0x3f733333 # 0.95f */
/* const/high16 v5, -0x40800000 # -1.0f */
v6 = this.mUnlockStateAnimationEndListener;
/* const/high16 v7, 0x3f800000 # 1.0f */
/* move-object v2, p0 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V */
/* .line 365 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipHideAnimation()V */
/* .line 366 */
return;
} // .end method
private void setWakeState ( ) {
/* .locals 6 */
/* .line 335 */
/* const v1, 0x431de9e6 */
/* const/high16 v2, 0x3f800000 # 1.0f */
int v3 = 0; // const/4 v3, 0x0
v4 = this.mWakeStateAnimationEndListener;
/* const/high16 v5, 0x3b800000 # 0.00390625f */
/* move-object v0, p0 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V */
/* .line 339 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->resetIconAnimation()V */
/* .line 340 */
v0 = this.mIconView;
v0 = (( android.widget.ImageView ) v0 ).getAlpha ( ); // invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F
int v1 = 0; // const/4 v1, 0x0
/* cmpg-float v0, v0, v1 */
/* if-gtz v0, :cond_0 */
/* .line 341 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->prepareIconAndTipAnimation()V */
/* .line 344 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->playIconAndTipShowAnimation()V */
/* .line 345 */
return;
} // .end method
private void startGradientShadowAnimation ( Float p0 ) {
/* .locals 2 */
/* .param p1, "finalPosition" # F */
/* .line 369 */
/* const v0, 0x43a1228f */
/* const v1, 0x3f733333 # 0.95f */
/* invoke-direct {p0, v0, v1, p1}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFF)V */
/* .line 370 */
return;
} // .end method
private void startGradientShadowAnimation ( Float p0, Float p1, Float p2 ) {
/* .locals 6 */
/* .param p1, "stiffness" # F */
/* .param p2, "dampingRatio" # F */
/* .param p3, "finalPosition" # F */
/* .line 374 */
int v4 = 0; // const/4 v4, 0x0
/* const/high16 v5, 0x3b800000 # 0.00390625f */
/* move-object v0, p0 */
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/display/SwipeUpWindow;->startGradientShadowAnimation(FFFLcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;F)V */
/* .line 376 */
return;
} // .end method
private void startGradientShadowAnimation ( Float p0, Float p1, Float p2, com.android.server.display.animation.DynamicAnimation$OnAnimationEndListener p3, Float p4 ) {
/* .locals 3 */
/* .param p1, "stiffness" # F */
/* .param p2, "dampingRatio" # F */
/* .param p3, "finalPosition" # F */
/* .param p4, "onAnimationEndListener" # Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener; */
/* .param p5, "minimumVisibleChange" # F */
/* .line 381 */
v0 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V
/* .line 382 */
/* new-instance v0, Lcom/android/server/display/animation/SpringForce; */
/* invoke-direct {v0}, Lcom/android/server/display/animation/SpringForce;-><init>()V */
/* .line 383 */
/* .local v0, "springForce":Lcom/android/server/display/animation/SpringForce; */
(( com.android.server.display.animation.SpringForce ) v0 ).setStiffness ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/display/animation/SpringForce;->setStiffness(F)Lcom/android/server/display/animation/SpringForce;
/* .line 384 */
(( com.android.server.display.animation.SpringForce ) v0 ).setDampingRatio ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/display/animation/SpringForce;->setDampingRatio(F)Lcom/android/server/display/animation/SpringForce;
/* .line 385 */
(( com.android.server.display.animation.SpringForce ) v0 ).setFinalPosition ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/display/animation/SpringForce;->setFinalPosition(F)Lcom/android/server/display/animation/SpringForce;
/* .line 386 */
v1 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v1 ).setSpring ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/display/animation/SpringAnimation;->setSpring(Lcom/android/server/display/animation/SpringForce;)Lcom/android/server/display/animation/SpringAnimation;
/* .line 388 */
v1 = this.mPreOnAnimationEndListener;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 389 */
v2 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v2 ).removeEndListener ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/display/animation/SpringAnimation;->removeEndListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;)V
/* .line 391 */
} // :cond_0
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 392 */
v1 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v1 ).addEndListener ( p4 ); // invoke-virtual {v1, p4}, Lcom/android/server/display/animation/SpringAnimation;->addEndListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;)Lcom/android/server/display/animation/DynamicAnimation;
/* .line 394 */
} // :cond_1
this.mPreOnAnimationEndListener = p4;
/* .line 396 */
v1 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v1 ).setMinimumVisibleChange ( p5 ); // invoke-virtual {v1, p5}, Lcom/android/server/display/animation/SpringAnimation;->setMinimumVisibleChange(F)Lcom/android/server/display/animation/DynamicAnimation;
/* .line 398 */
v1 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v1 ).start ( ); // invoke-virtual {v1}, Lcom/android/server/display/animation/SpringAnimation;->start()V
/* .line 399 */
return;
} // .end method
private void startSwipeUpAnimation ( ) {
/* .locals 4 */
/* .line 317 */
/* iget-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z */
/* if-nez v0, :cond_0 */
/* .line 318 */
return;
/* .line 321 */
} // :cond_0
/* const/high16 v0, -0x1000000 */
/* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I */
/* .line 322 */
/* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I */
/* .line 323 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBlurLevel:I */
/* .line 324 */
v0 = this.mAnimationState;
/* const/high16 v1, 0x3f800000 # 1.0f */
(( com.android.server.display.SwipeUpWindow$AnimationState ) v0 ).setPerState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->setPerState(F)V
/* .line 325 */
/* invoke-direct {p0, v1}, Lcom/android/server/display/SwipeUpWindow;->updateBlur(F)V */
/* .line 327 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->setWakeState()V */
/* .line 329 */
v0 = this.mHandler;
/* const/16 v1, 0x68 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 330 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 331 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x1770 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 332 */
return;
} // .end method
private void updateBlackView ( ) {
/* .locals 8 */
/* .line 402 */
v0 = this.mAnimationState;
v0 = (( com.android.server.display.SwipeUpWindow$AnimationState ) v0 ).getPerState ( ); // invoke-virtual {v0}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getPerState()F
/* .line 403 */
/* .local v0, "per":F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* add-float v2, v0, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->constraintAlpha(F)F */
/* .line 404 */
/* .local v2, "topAlpha":F */
/* const v3, 0x3f19999a # 0.6f */
/* add-float/2addr v3, v0 */
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->constraintAlpha(F)F */
/* .line 406 */
/* .local v3, "bottomAlpha":F */
v4 = /* invoke-direct {p0, v2}, Lcom/android/server/display/SwipeUpWindow;->calculateBlackAlpha(F)I */
/* .line 407 */
/* .local v4, "topColor":I */
v5 = /* invoke-direct {p0, v3}, Lcom/android/server/display/SwipeUpWindow;->calculateBlackAlpha(F)I */
/* .line 408 */
/* .local v5, "bottomColor":I */
/* iget v6, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I */
/* if-ne v4, v6, :cond_0 */
/* iget v6, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I */
/* if-eq v5, v6, :cond_1 */
/* .line 409 */
} // :cond_0
/* iput v4, p0, Lcom/android/server/display/SwipeUpWindow;->mPreTopColor:I */
/* .line 410 */
/* iput v5, p0, Lcom/android/server/display/SwipeUpWindow;->mPreBottomColor:I */
/* .line 411 */
v6 = this.mBlackLinearGradientView;
/* filled-new-array {v4, v5}, [I */
(( com.android.server.display.BlackLinearGradientView ) v6 ).setLinearGradientColor ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/display/BlackLinearGradientView;->setLinearGradientColor([I)V
/* .line 418 */
} // :cond_1
int v6 = 0; // const/4 v6, 0x0
v6 = android.util.MathUtils .min ( v6,v0 );
/* add-float/2addr v6, v1 */
v1 = /* invoke-direct {p0, v6}, Lcom/android/server/display/SwipeUpWindow;->constraintBlur(F)F */
/* .line 419 */
/* .local v1, "blurLevel":F */
/* invoke-direct {p0, v1}, Lcom/android/server/display/SwipeUpWindow;->updateBlur(F)V */
/* .line 420 */
return;
} // .end method
private void updateBlur ( Float p0 ) {
/* .locals 1 */
/* .param p1, "level" # F */
/* .line 429 */
/* const/high16 v0, 0x42c80000 # 100.0f */
/* mul-float/2addr v0, p1 */
/* float-to-int v0, v0 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->setBackgroudBlur(I)V */
/* .line 430 */
return;
} // .end method
private void updateSettings ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "bool" # Z */
/* .line 492 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 493 */
/* nop */
/* .line 492 */
/* const-string/jumbo v1, "swipe_up_is_showing" */
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$Secure .putIntForUser ( v0,v1,p1,v2 );
/* .line 494 */
return;
} // .end method
private void updateSizeInfo ( ) {
/* .locals 8 */
/* .line 144 */
v0 = this.mContext;
final String v1 = "display"; // const-string v1, "display"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
/* .line 146 */
/* .local v0, "displayManager":Landroid/hardware/display/DisplayManager; */
final String v1 = "android.hardware.display.category.ALL_INCLUDING_DISABLED"; // const-string v1, "android.hardware.display.category.ALL_INCLUDING_DISABLED"
(( android.hardware.display.DisplayManager ) v0 ).getDisplays ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;
/* .line 147 */
/* .local v1, "displays":[Landroid/view/Display; */
/* new-instance v2, Landroid/view/DisplayInfo; */
/* invoke-direct {v2}, Landroid/view/DisplayInfo;-><init>()V */
/* .line 148 */
/* .local v2, "outDisplayInfo":Landroid/view/DisplayInfo; */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* aget-object v5, v1, v4 */
/* .line 149 */
/* .local v5, "display":Landroid/view/Display; */
(( android.view.Display ) v5 ).getDisplayInfo ( v2 ); // invoke-virtual {v5, v2}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z
/* .line 150 */
/* iget v6, v2, Landroid/view/DisplayInfo;->type:I */
int v7 = 1; // const/4 v7, 0x1
/* if-ne v6, v7, :cond_0 */
/* .line 151 */
/* iget v6, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I */
/* iget v7, v2, Landroid/view/DisplayInfo;->logicalWidth:I */
/* if-le v6, v7, :cond_0 */
/* .line 152 */
/* iget v6, v2, Landroid/view/DisplayInfo;->logicalWidth:I */
/* iput v6, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I */
/* .line 153 */
/* iget v6, v2, Landroid/view/DisplayInfo;->logicalHeight:I */
/* iput v6, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
/* .line 148 */
} // .end local v5 # "display":Landroid/view/Display;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 158 */
} // :cond_1
/* iget v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I */
/* const v4, 0x7fffffff */
/* if-ne v3, v4, :cond_2 */
/* .line 159 */
/* const/16 v3, 0x438 */
/* iput v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I */
/* .line 160 */
/* const/16 v3, 0x9d8 */
/* iput v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
/* .line 162 */
} // :cond_2
/* iget v3, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
/* int-to-float v3, v3 */
/* const v4, 0x3e4ccccd # 0.2f */
/* mul-float/2addr v3, v4 */
/* iput v3, p0, Lcom/android/server/display/SwipeUpWindow;->mUnlockDistance:F */
/* .line 163 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateSizeInfo: (h=" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenHeight:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", w="; // const-string v4, ", w="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/display/SwipeUpWindow;->mScreenWidth:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ")"; // const-string v4, ")"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "SwipeUpWindow"; // const-string v4, "SwipeUpWindow"
android.util.Slog .i ( v4,v3 );
/* .line 164 */
return;
} // .end method
/* # virtual methods */
public Float afterFrictionValue ( Float p0, Float p1 ) {
/* .locals 4 */
/* .param p1, "value" # F */
/* .param p2, "range" # F */
/* .line 691 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v1, p2, v0 */
/* if-nez v1, :cond_0 */
/* .line 692 */
/* .line 694 */
} // :cond_0
/* cmpl-float v0, p1, v0 */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* if-ltz v0, :cond_1 */
/* move v0, v1 */
} // :cond_1
/* const/high16 v0, -0x40800000 # -1.0f */
/* .line 695 */
/* .local v0, "t":F */
} // :goto_0
p1 = android.util.MathUtils .abs ( p1 );
/* .line 696 */
/* div-float v2, p1, p2 */
v1 = android.util.MathUtils .min ( v2,v1 );
/* .line 697 */
/* .local v1, "per":F */
/* mul-float v2, v1, v1 */
/* mul-float/2addr v2, v1 */
/* const/high16 v3, 0x40400000 # 3.0f */
/* div-float/2addr v2, v3 */
/* mul-float v3, v1, v1 */
/* sub-float/2addr v2, v3 */
/* add-float/2addr v2, v1 */
/* mul-float/2addr v2, v0 */
/* mul-float/2addr v2, p2 */
} // .end method
public void cancelScreenOffDelay ( ) {
/* .locals 3 */
/* .line 475 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 476 */
v0 = this.mGradientShadowSpringAnimation;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 477 */
v2 = this.mPreOnAnimationEndListener;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 478 */
(( com.android.server.display.animation.SpringAnimation ) v0 ).removeEndListener ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/display/animation/SpringAnimation;->removeEndListener(Lcom/android/server/display/animation/DynamicAnimation$OnAnimationEndListener;)V
/* .line 479 */
this.mPreOnAnimationEndListener = v1;
/* .line 481 */
} // :cond_0
v0 = this.mGradientShadowSpringAnimation;
(( com.android.server.display.animation.SpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V
/* .line 483 */
} // :cond_1
return;
} // .end method
public void releaseSwipeWindow ( ) {
/* .locals 1 */
/* .line 171 */
/* const-string/jumbo v0, "unknow" */
(( com.android.server.display.SwipeUpWindow ) p0 ).releaseSwipeWindow ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->releaseSwipeWindow(Ljava/lang/String;)V
/* .line 172 */
return;
} // .end method
public void releaseSwipeWindow ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 175 */
/* iget-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z */
/* if-nez v0, :cond_0 */
/* .line 176 */
return;
/* .line 178 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "release swipe up window: "; // const-string v1, "release swipe up window: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "SwipeUpWindow"; // const-string v1, "SwipeUpWindow"
android.util.Slog .i ( v1,v0 );
/* .line 179 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z */
/* .line 181 */
/* invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->updateSettings(Z)V */
/* .line 182 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 184 */
v0 = this.mGradientShadowSpringAnimation;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 185 */
(( com.android.server.display.animation.SpringAnimation ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/display/animation/SpringAnimation;->cancel()V
/* .line 186 */
this.mGradientShadowSpringAnimation = v1;
/* .line 189 */
} // :cond_1
v0 = this.mSwipeUpFrameLayout;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 190 */
v2 = this.mWindowManager;
/* .line 191 */
this.mSwipeUpFrameLayout = v1;
/* .line 193 */
} // :cond_2
return;
} // .end method
public void removeSwipeUpWindow ( ) {
/* .locals 0 */
/* .line 168 */
return;
} // .end method
public void showSwipeUpWindow ( ) {
/* .locals 3 */
/* .line 196 */
/* iget-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 197 */
return;
/* .line 200 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/display/SwipeUpWindow;->updateSettings(Z)V */
/* .line 201 */
v1 = this.mSwipeUpFrameLayout;
/* if-nez v1, :cond_1 */
/* .line 202 */
/* invoke-direct {p0}, Lcom/android/server/display/SwipeUpWindow;->initSwipeUpWindow()V */
/* .line 204 */
} // :cond_1
/* iput-boolean v0, p0, Lcom/android/server/display/SwipeUpWindow;->mSwipeUpWindowShowing:Z */
/* .line 205 */
v0 = this.mWindowManager;
v1 = this.mSwipeUpFrameLayout;
v2 = this.mSwipeUpLayoutParams;
/* .line 206 */
final String v0 = "SwipeUpWindow"; // const-string v0, "SwipeUpWindow"
/* const-string/jumbo v1, "show swipe up window" */
android.util.Slog .i ( v0,v1 );
/* .line 207 */
return;
} // .end method
public Integer transformDpToPx ( Float p0 ) {
/* .locals 2 */
/* .param p1, "dp" # F */
/* .line 468 */
v0 = this.mContext;
/* .line 469 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* .line 468 */
int v1 = 1; // const/4 v1, 0x1
v0 = android.util.TypedValue .applyDimension ( v1,p1,v0 );
/* float-to-int v0, v0 */
} // .end method
public Integer transformDpToPx ( android.content.Context p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "ctx" # Landroid/content/Context; */
/* .param p2, "dp" # F */
/* .line 463 */
/* nop */
/* .line 464 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* .line 463 */
int v1 = 1; // const/4 v1, 0x1
v0 = android.util.TypedValue .applyDimension ( v1,p2,v0 );
/* float-to-int v0, v0 */
} // .end method
