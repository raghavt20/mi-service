class com.android.server.display.DisplayPowerControllerImpl$4 implements java.lang.Runnable {
	 /* .source "DisplayPowerControllerImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/DisplayPowerControllerImpl;->updateForegroundApp()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.display.DisplayPowerControllerImpl this$0; //synthetic
/* # direct methods */
 com.android.server.display.DisplayPowerControllerImpl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/DisplayPowerControllerImpl; */
/* .line 1683 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 1687 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmActivityTaskManager ( v0 );
	 /* .line 1688 */
	 /* .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
	 if ( v0 != null) { // if-eqz v0, :cond_4
		 v1 = this.topActivity;
		 /* if-nez v1, :cond_0 */
		 /* .line 1691 */
	 } // :cond_0
	 v1 = 	 (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
	 int v2 = 5; // const/4 v2, 0x5
	 /* if-eq v1, v2, :cond_3 */
	 /* .line 1692 */
	 v1 = 	 (( android.app.ActivityTaskManager$RootTaskInfo ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Landroid/app/ActivityTaskManager$RootTaskInfo;->getWindowingMode()I
	 int v2 = 6; // const/4 v2, 0x6
	 /* if-eq v1, v2, :cond_3 */
	 v1 = this.this$0;
	 com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmActivityTaskManager ( v1 );
	 v1 = 	 /* .line 1693 */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 1696 */
	 } // :cond_1
	 v1 = this.topActivity;
	 (( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
	 /* .line 1698 */
	 /* .local v1, "packageName":Ljava/lang/String; */
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 v2 = this.this$0;
		 com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmForegroundAppPackageName ( v2 );
		 /* .line 1699 */
		 v2 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_2
			 /* .line 1700 */
			 return;
			 /* .line 1702 */
		 } // :cond_2
		 v2 = this.this$0;
		 com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fputmPendingForegroundAppPackageName ( v2,v1 );
		 /* .line 1703 */
		 v2 = this.this$0;
		 com.android.server.display.DisplayPowerControllerImpl .-$$Nest$fgetmHandler ( v2 );
		 int v3 = 3; // const/4 v3, 0x3
		 (( com.android.server.display.DisplayPowerControllerImpl$DisplayPowerControllerImplHandler ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/display/DisplayPowerControllerImpl$DisplayPowerControllerImplHandler;->sendEmptyMessage(I)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 1706 */
		 /* nop */
	 } // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v1 # "packageName":Ljava/lang/String;
/* .line 1694 */
/* .restart local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_3
} // :goto_0
return;
/* .line 1689 */
} // :cond_4
} // :goto_1
return;
/* .line 1704 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 1707 */
} // :goto_2
return;
} // .end method
