.class Lcom/android/server/display/RhythmicEyeCareManager$2;
.super Landroid/content/BroadcastReceiver;
.source "RhythmicEyeCareManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/RhythmicEyeCareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/RhythmicEyeCareManager;


# direct methods
.method public static synthetic $r8$lambda$AuETwViZxc8mEov7hm27f8iZjiQ(Lcom/android/server/display/RhythmicEyeCareManager$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/RhythmicEyeCareManager$2;->lambda$onReceive$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/display/RhythmicEyeCareManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/RhythmicEyeCareManager;

    .line 70
    iput-object p1, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private synthetic lambda$onReceive$0()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$mupdateState(Lcom/android/server/display/RhythmicEyeCareManager;Z)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 73
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmIsEnabled(Lcom/android/server/display/RhythmicEyeCareManager;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmOnAlarmListener(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/app/AlarmManager$OnAlarmListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 79
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmAlarmManager(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/app/AlarmManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v2}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmOnAlarmListener(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/app/AlarmManager$OnAlarmListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/AlarmManager$OnAlarmListener;)V

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$fgetmHandler(Lcom/android/server/display/RhythmicEyeCareManager;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/display/RhythmicEyeCareManager$2$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/display/RhythmicEyeCareManager$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/display/RhythmicEyeCareManager$2;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 82
    iget-object v1, p0, Lcom/android/server/display/RhythmicEyeCareManager$2;->this$0:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-static {v1}, Lcom/android/server/display/RhythmicEyeCareManager;->-$$Nest$mscheduleRhythmicAlarm(Lcom/android/server/display/RhythmicEyeCareManager;)V

    .line 85
    :cond_2
    return-void
.end method
