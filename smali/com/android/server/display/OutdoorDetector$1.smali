.class Lcom/android/server/display/OutdoorDetector$1;
.super Ljava/lang/Object;
.source "OutdoorDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/OutdoorDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/OutdoorDetector;


# direct methods
.method constructor <init>(Lcom/android/server/display/OutdoorDetector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/OutdoorDetector;

    .line 65
    iput-object p1, p0, Lcom/android/server/display/OutdoorDetector$1;->this$0:Lcom/android/server/display/OutdoorDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 76
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 68
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 69
    .local v0, "time":J
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    .line 70
    .local v2, "lux":F
    iget-object v3, p0, Lcom/android/server/display/OutdoorDetector$1;->this$0:Lcom/android/server/display/OutdoorDetector;

    invoke-static {v3, v0, v1, v2}, Lcom/android/server/display/OutdoorDetector;->-$$Nest$mhandleLightSensorEvent(Lcom/android/server/display/OutdoorDetector;JF)V

    .line 71
    return-void
.end method
