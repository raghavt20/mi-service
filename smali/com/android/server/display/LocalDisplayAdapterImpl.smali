.class public Lcom/android/server/display/LocalDisplayAdapterImpl;
.super Lcom/android/server/display/LocalDisplayAdapterStub;
.source "LocalDisplayAdapterImpl.java"


# static fields
.field private static final mIsFlipDevice:Z

.field private static final mIsFoldDevice:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDeviceInside()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/LocalDisplayAdapterImpl;->mIsFoldDevice:Z

    .line 12
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/LocalDisplayAdapterImpl;->mIsFlipDevice:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/android/server/display/LocalDisplayAdapterStub;-><init>()V

    return-void
.end method


# virtual methods
.method public setDeviceInfoFlagIfNeeded(Lcom/android/server/display/DisplayDeviceInfo;Z)V
    .locals 2
    .param p1, "deviceInfo"    # Lcom/android/server/display/DisplayDeviceInfo;
    .param p2, "isFirstDisplay"    # Z

    .line 16
    sget-boolean v0, Lcom/android/server/display/LocalDisplayAdapterImpl;->mIsFoldDevice:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/display/LocalDisplayAdapterImpl;->mIsFlipDevice:Z

    if-eqz v0, :cond_1

    .line 17
    :cond_0
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    .line 19
    :cond_1
    if-nez p2, :cond_2

    const-string/jumbo v0, "star"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    .line 21
    iget v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p1, Lcom/android/server/display/DisplayDeviceInfo;->flags:I

    .line 23
    :cond_2
    return-void
.end method

.method public updateScreenEffectIfNeeded(IZ)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "isFirstDisplay"    # Z

    .line 27
    const-string/jumbo v0, "star"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    .line 28
    :cond_0
    invoke-static {}, Lcom/android/server/display/DisplayFeatureManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayFeatureManagerServiceStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/display/DisplayFeatureManagerServiceStub;->updateScreenEffect(I)V

    .line 30
    :cond_1
    return-void
.end method
