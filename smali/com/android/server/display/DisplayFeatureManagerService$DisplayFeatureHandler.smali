.class Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;
.super Landroid/os/Handler;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayFeatureManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisplayFeatureHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1170
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    .line 1171
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1172
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 1176
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1177
    iget v0, p1, Landroid/os/Message;->what:I

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1252
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msendMuraState(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    goto/16 :goto_0

    .line 1224
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msendHbmState(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1225
    goto/16 :goto_0

    .line 1227
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetSDR2HDR(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1228
    goto/16 :goto_0

    .line 1231
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmResolver(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_game_mode"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1232
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mloadSettings(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1233
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0, v3}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffectAll(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1234
    goto/16 :goto_0

    .line 1244
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    move v2, v3

    :cond_0
    invoke-static {v0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fputmDolbyState(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1246
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmContext(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/server/display/DisplayFeatureManagerService;->BRIGHTNESS_THROTTLER_STATUS:Ljava/lang/String;

    .line 1248
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmDolbyState(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v2

    .line 1246
    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1249
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifyHdrStateChanged(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1250
    goto/16 :goto_0

    .line 1221
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleTrueToneModeChange(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1222
    goto/16 :goto_0

    .line 1182
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v2

    :cond_1
    invoke-static {v0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetPaperColors(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1183
    goto/16 :goto_0

    .line 1218
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifySFSecondaryFrameRate(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1219
    goto/16 :goto_0

    .line 1215
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifySFDCParseState(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1216
    goto/16 :goto_0

    .line 1209
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifySFColorMode(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1210
    goto/16 :goto_0

    .line 1212
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetExpertScreenMode(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1213
    goto/16 :goto_0

    .line 1192
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v0, v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifySFDfpsMode(Lcom/android/server/display/DisplayFeatureManagerService;II)V

    .line 1193
    goto/16 :goto_0

    .line 1203
    :pswitch_d
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmContext(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmContext(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$misDarkModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/content/Context;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetDarkModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/content/Context;Z)V

    .line 1204
    goto/16 :goto_0

    .line 1206
    :pswitch_e
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleUnlimitedColorLevelChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1207
    goto :goto_0

    .line 1195
    :pswitch_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/os/SomeArgs;

    .line 1196
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    iget-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1197
    .local v1, "red":F
    iget-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 1198
    .local v2, "green":F
    iget-object v3, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 1199
    .local v3, "blue":F
    iget-object v4, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-static {v4, v5, v1, v2, v3}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifySFPccLevel(Lcom/android/server/display/DisplayFeatureManagerService;IFFF)V

    .line 1200
    invoke-virtual {v0}, Lcom/android/internal/os/SomeArgs;->recycle()V

    .line 1201
    goto :goto_0

    .line 1188
    .end local v0    # "args":Lcom/android/internal/os/SomeArgs;
    .end local v1    # "red":F
    .end local v2    # "green":F
    .end local v3    # "blue":F
    :pswitch_10
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v3, :cond_2

    move v2, v3

    :cond_2
    invoke-static {v0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifySFWcgState(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1189
    goto :goto_0

    .line 1240
    :pswitch_11
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v2, p1, Landroid/os/Message;->arg1:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fputmCurrentGrayScale(Lcom/android/server/display/DisplayFeatureManagerService;F)V

    .line 1241
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifyCurrentGrayScaleChanged(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1242
    goto :goto_0

    .line 1236
    :pswitch_12
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget v2, p1, Landroid/os/Message;->arg1:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v2, v1

    invoke-static {v0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fputmGrayScale(Lcom/android/server/display/DisplayFeatureManagerService;F)V

    .line 1237
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mnotifyGrayScaleChanged(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1238
    goto :goto_0

    .line 1185
    :pswitch_13
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleScreenSchemeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1186
    goto :goto_0

    .line 1179
    :pswitch_14
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleReadingModeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1180
    nop

    .line 1255
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_0
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_c
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
