abstract class com.android.server.display.BrightnessCurve$Curve {
	 /* .source "BrightnessCurve.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/BrightnessCurve; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x402 */
/* name = "Curve" */
} // .end annotation
/* # instance fields */
public java.util.List mPointList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Float;", */
/* "Ljava/lang/Float;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.display.BrightnessCurve this$0; //synthetic
/* # direct methods */
public com.android.server.display.BrightnessCurve$Curve ( ) {
/* .locals 0 */
/* .line 287 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 288 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.mPointList = p1;
/* .line 289 */
return;
} // .end method
/* # virtual methods */
public abstract void connectLeft ( com.android.server.display.BrightnessCurve$Curve p0 ) {
} // .end method
public abstract void connectRight ( com.android.server.display.BrightnessCurve$Curve p0 ) {
} // .end method
public abstract void create ( Float p0, Float p1 ) {
} // .end method
