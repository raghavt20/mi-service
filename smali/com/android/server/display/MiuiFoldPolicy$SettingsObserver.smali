.class final Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiFoldPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/MiuiFoldPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/MiuiFoldPolicy;


# direct methods
.method public constructor <init>(Lcom/android/server/display/MiuiFoldPolicy;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 362
    iput-object p1, p0, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    .line 363
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 364
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 368
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 369
    .local v0, "lastPathSegment":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "device_provisioned"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_1
    const-string v1, "close_lid_display_setting"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "miui_optimization"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "fold_gesture_angle_threshold"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 380
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mupdateDeviceProVisioned(Lcom/android/server/display/MiuiFoldPolicy;)V

    .line 381
    goto :goto_2

    .line 377
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mupdateCtsMode(Lcom/android/server/display/MiuiFoldPolicy;)V

    .line 378
    goto :goto_2

    .line 374
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mupdateScreenStateAfterFold(Lcom/android/server/display/MiuiFoldPolicy;)V

    .line 375
    goto :goto_2

    .line 371
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/display/MiuiFoldPolicy$SettingsObserver;->this$0:Lcom/android/server/display/MiuiFoldPolicy;

    invoke-static {v1}, Lcom/android/server/display/MiuiFoldPolicy;->-$$Nest$mupdateFoldGestureAngleThreshold(Lcom/android/server/display/MiuiFoldPolicy;)V

    .line 372
    nop

    .line 385
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x503af535 -> :sswitch_3
        -0x2c813be4 -> :sswitch_2
        0x495cb554 -> :sswitch_1
        0x527f6fcb -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
