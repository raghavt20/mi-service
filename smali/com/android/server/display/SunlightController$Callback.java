public abstract class com.android.server.display.SunlightController$Callback {
	 /* .source "SunlightController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SunlightController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "Callback" */
} // .end annotation
/* # virtual methods */
public abstract Float convertBrightnessToNit ( Float p0 ) {
} // .end method
public abstract void notifySunlightModeChanged ( Boolean p0 ) {
} // .end method
public abstract void notifySunlightStateChange ( Boolean p0 ) {
} // .end method
