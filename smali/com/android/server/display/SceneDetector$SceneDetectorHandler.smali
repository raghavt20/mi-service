.class final Lcom/android/server/display/SceneDetector$SceneDetectorHandler;
.super Landroid/os/Handler;
.source "SceneDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SceneDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SceneDetectorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/SceneDetector;


# direct methods
.method public constructor <init>(Lcom/android/server/display/SceneDetector;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .line 290
    iput-object p1, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    .line 291
    const/4 p1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p2, p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 292
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 296
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 311
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmAonState(Lcom/android/server/display/SceneDetector;)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$munregisterAonFlareListener(Lcom/android/server/display/SceneDetector;)V

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/display/SceneDetector;->-$$Nest$fputmIsMainDarkenEvent(Lcom/android/server/display/SceneDetector;Z)V

    .line 315
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$mresetServiceConnectedStatus(Lcom/android/server/display/SceneDetector;)V

    .line 316
    goto :goto_0

    .line 308
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$mtryToBindAonFlareService(Lcom/android/server/display/SceneDetector;)V

    .line 309
    goto :goto_0

    .line 298
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmAonState(Lcom/android/server/display/SceneDetector;)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 299
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmBrightnessControllerImpl(Lcom/android/server/display/SceneDetector;)Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v1}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmPreAmbientLux(Lcom/android/server/display/SceneDetector;)F

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->notifyAonFlareEvents(IF)V

    .line 301
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$fgetmIsMainDarkenEvent(Lcom/android/server/display/SceneDetector;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$mupdateAutoBrightness(Lcom/android/server/display/SceneDetector;)V

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/SceneDetector$SceneDetectorHandler;->this$0:Lcom/android/server/display/SceneDetector;

    invoke-static {v0}, Lcom/android/server/display/SceneDetector;->-$$Nest$munregisterAonFlareListener(Lcom/android/server/display/SceneDetector;)V

    .line 320
    :cond_2
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
