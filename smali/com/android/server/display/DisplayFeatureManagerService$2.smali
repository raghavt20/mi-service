.class Lcom/android/server/display/DisplayFeatureManagerService$2;
.super Landroid/util/IntProperty;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/display/DisplayFeatureManagerService;->onBootPhase(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/IntProperty<",
        "Lcom/android/server/display/DisplayFeatureManagerService;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/DisplayFeatureManagerService;
    .param p2, "name"    # Ljava/lang/String;

    .line 320
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$2;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-direct {p0, p2}, Landroid/util/IntProperty;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/lang/Integer;
    .locals 1
    .param p1, "object"    # Lcom/android/server/display/DisplayFeatureManagerService;

    .line 340
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 320
    check-cast p1, Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService$2;->get(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public setValue(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 3
    .param p1, "object"    # Lcom/android/server/display/DisplayFeatureManagerService;
    .param p2, "value"    # I

    .line 325
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$2;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmDisplayState(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-gtz p2, :cond_0

    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$2;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmPaperModeAnimator(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/MiuiRampAnimator;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Lcom/android/server/display/MiuiRampAnimator;->isAnimating()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    :cond_0
    invoke-static {p1, v1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffect(Lcom/android/server/display/DisplayFeatureManagerService;II)V

    goto :goto_0

    .line 328
    :cond_1
    invoke-static {}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$sfgetIS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$2;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmDisplayState(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v0

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService$2;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 334
    const/4 v0, 0x0

    invoke-static {p1, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetScreenEffect(Lcom/android/server/display/DisplayFeatureManagerService;II)V

    .line 336
    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;I)V
    .locals 0

    .line 320
    check-cast p1, Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService$2;->setValue(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    return-void
.end method
