.class final Lcom/android/server/display/SunlightController$NotificationHelper;
.super Ljava/lang/Object;
.source "SunlightController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/SunlightController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "NotificationHelper"
.end annotation


# static fields
.field private static final AUTO_BRIGHTNESS_ACTION:Ljava/lang/String; = "com.android.settings/com.android.settings.display.BrightnessActivity"

.field private static final DEFAULT_NOTIFICATION_LIMIT:I = 0x2

.field private static final KEY_NOTIFICATION_LAST_SHOW_TIME:Ljava/lang/String; = "last_show_time"

.field private static final KEY_NOTIFICATION_LIMIT:Ljava/lang/String; = "shown_times"

.field private static final MINI_INTERVAL_NOTIFICATION:I = 0x36ee80

.field private static final NOTIFICATION_ID:I = 0x3e8

.field private static final NOTIFICATION_TAG:Ljava/lang/String; = "SUNLIGHT_NOTIFY"

.field private static final PREFS_SUNLIGHT_FILE:Ljava/lang/String; = "sunlight_notification.xml"


# instance fields
.field private mHasReachedLimitTimes:Z

.field private mLastShowNotificationTime:J

.field private mNotification:Landroid/app/Notification;

.field private mNotificationLimitTimesPrefs:Landroid/content/SharedPreferences;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field final synthetic this$0:Lcom/android/server/display/SunlightController;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHasReachedLimitTimes(Lcom/android/server/display/SunlightController$NotificationHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mHasReachedLimitTimes:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$mdump(Lcom/android/server/display/SunlightController$NotificationHelper;Ljava/io/PrintWriter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController$NotificationHelper;->dump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowNotificationIfNecessary(Lcom/android/server/display/SunlightController$NotificationHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->showNotificationIfNecessary()Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Lcom/android/server/display/SunlightController;)V
    .locals 3

    .line 579
    iput-object p1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->this$0:Lcom/android/server/display/SunlightController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 580
    invoke-static {p1}, Lcom/android/server/display/SunlightController;->-$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;

    move-result-object v0

    .line 581
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationManager:Landroid/app/NotificationManager;

    .line 582
    invoke-static {p1}, Lcom/android/server/display/SunlightController;->-$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Ljava/io/File;

    .line 584
    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->getSystemDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "sunlight_notification.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 583
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationLimitTimesPrefs:Landroid/content/SharedPreferences;

    .line 585
    const-string v0, "last_show_time"

    const-wide/16 v1, 0x0

    invoke-interface {p1, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mLastShowNotificationTime:J

    .line 586
    return-void
.end method

.method private buildNotification()V
    .locals 5

    .line 650
    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->getBrightnessActivityIntent()Landroid/content/Intent;

    move-result-object v0

    .line 651
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 652
    const-string v1, "SunlightController"

    const-string v2, "Build notification failed."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    return-void

    .line 655
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v1}, Lcom/android/server/display/SunlightController;->-$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x14000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 657
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v3}, Lcom/android/server/display/SunlightController;->-$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/android/internal/notification/SystemNotificationChannels;->ALERTS:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 658
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v3}, Lcom/android/server/display/SunlightController;->-$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;

    move-result-object v3

    .line 659
    const v4, 0x110f03ad

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->this$0:Lcom/android/server/display/SunlightController;

    invoke-static {v3}, Lcom/android/server/display/SunlightController;->-$$Nest$fgetmContext(Lcom/android/server/display/SunlightController;)Landroid/content/Context;

    move-result-object v3

    .line 660
    const v4, 0x110f03ac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 661
    const v3, 0x108089a

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 662
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 663
    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotification:Landroid/app/Notification;

    .line 664
    return-void
.end method

.method private constrainedByInterval()Z
    .locals 8

    .line 617
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 618
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mLastShowNotificationTime:J

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    .line 619
    return v5

    .line 621
    :cond_0
    sub-long v2, v0, v2

    .line 622
    .local v2, "interval":J
    invoke-static {}, Lcom/android/server/display/SunlightController;->-$$Nest$sfgetDEBUG()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 623
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "constrainedByInterval, interval="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "SunlightController"

    invoke-static {v6, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    :cond_1
    const-wide/32 v6, 0x36ee80

    cmp-long v4, v2, v6

    if-gtz v4, :cond_2

    const/4 v5, 0x1

    :cond_2
    return v5
.end method

.method private dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 683
    const-string v0, "  Sunlight Controller Noticationcation Helper:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    mHasReachedLimitTimes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mHasReachedLimitTimes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 685
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    mLastShowNotificationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mLastShowNotificationTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 686
    return-void
.end method

.method private getBrightnessActivityIntent()Landroid/content/Intent;
    .locals 3

    .line 667
    const-string v0, "com.android.settings/com.android.settings.display.BrightnessActivity"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 668
    .local v0, "component":Landroid/content/ComponentName;
    if-nez v0, :cond_0

    .line 669
    const/4 v1, 0x0

    return-object v1

    .line 672
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 673
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 674
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 675
    return-object v1
.end method

.method private getSystemDir()Ljava/io/File;
    .locals 3

    .line 679
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private hasReachedLimitTimes()Z
    .locals 3

    .line 629
    iget-object v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationLimitTimesPrefs:Landroid/content/SharedPreferences;

    const/4 v1, 0x2

    const-string/jumbo v2, "shown_times"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 631
    .local v0, "times":I
    if-lez v0, :cond_0

    .line 632
    iget-object v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationLimitTimesPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    add-int/lit8 v0, v0, -0x1

    .line 633
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 634
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 635
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mHasReachedLimitTimes:Z

    .line 636
    return v1

    .line 638
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mHasReachedLimitTimes:Z

    .line 639
    return v1
.end method

.method private isNotificationActive()Z
    .locals 7

    .line 603
    iget-object v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getActiveNotifications()[Landroid/service/notification/StatusBarNotification;

    move-result-object v0

    .line 604
    .local v0, "activeNotifications":[Landroid/service/notification/StatusBarNotification;
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 605
    .local v4, "sbn":Landroid/service/notification/StatusBarNotification;
    const/16 v5, 0x3e8

    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v6

    if-ne v5, v6, :cond_0

    const-string v5, "SUNLIGHT_NOTIFY"

    invoke-virtual {v4}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 606
    const/4 v1, 0x1

    return v1

    .line 604
    .end local v4    # "sbn":Landroid/service/notification/StatusBarNotification;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 609
    :cond_1
    return v2
.end method

.method private showNotificationIfNecessary()Z
    .locals 4

    .line 591
    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->isNotificationActive()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->constrainedByInterval()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->hasReachedLimitTimes()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotification:Landroid/app/Notification;

    if-nez v0, :cond_1

    .line 595
    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->buildNotification()V

    .line 597
    :cond_1
    invoke-direct {p0}, Lcom/android/server/display/SunlightController$NotificationHelper;->updateLastShowTimePrefs()V

    .line 598
    iget-object v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotification:Landroid/app/Notification;

    const-string v3, "SUNLIGHT_NOTIFY"

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 599
    const/4 v0, 0x1

    return v0

    .line 592
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private updateLastShowTimePrefs()V
    .locals 4

    .line 643
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mLastShowNotificationTime:J

    .line 644
    iget-object v0, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mNotificationLimitTimesPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/server/display/SunlightController$NotificationHelper;->mLastShowNotificationTime:J

    .line 645
    const-string v3, "last_show_time"

    invoke-interface {v0, v3, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 646
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 647
    return-void
.end method
