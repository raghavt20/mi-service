class com.android.server.display.SunlightController$ScreenOnReceiver extends android.content.BroadcastReceiver {
	 /* .source "SunlightController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/display/SunlightController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ScreenOnReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.display.SunlightController this$0; //synthetic
/* # direct methods */
private com.android.server.display.SunlightController$ScreenOnReceiver ( ) {
/* .locals 0 */
/* .line 487 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.display.SunlightController$ScreenOnReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/display/SunlightController$ScreenOnReceiver;-><init>(Lcom/android/server/display/SunlightController;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 491 */
int v0 = 0; // const/4 v0, 0x0
/* .line 492 */
/* .local v0, "screenOn":Z */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v2 = "android.intent.action.SCREEN_ON"; // const-string v2, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 493 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 495 */
} // :cond_0
v1 = com.android.server.display.SunlightController .-$$Nest$sfgetDEBUG ( );
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 496 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Receive screen on/off broadcast: "; // const-string v2, "Receive screen on/off broadcast: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 497 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 final String v2 = "screen on."; // const-string v2, "screen on."
	 } // :cond_1
	 final String v2 = "screen off."; // const-string v2, "screen off."
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 496 */
final String v2 = "SunlightController"; // const-string v2, "SunlightController"
android.util.Slog .d ( v2,v1 );
/* .line 499 */
} // :cond_2
v1 = this.this$0;
com.android.server.display.SunlightController .-$$Nest$fgetmHandler ( v1 );
int v2 = 2; // const/4 v2, 0x2
java.lang.Boolean .valueOf ( v0 );
(( com.android.server.display.SunlightController$SunlightModeHandler ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/display/SunlightController$SunlightModeHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 500 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 501 */
return;
} // .end method
