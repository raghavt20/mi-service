.class Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/DisplayFeatureManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/DisplayFeatureManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    .line 1068
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    .line 1069
    invoke-static {p1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/os/Handler;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1070
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1074
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1075
    .local v0, "lastPathSegment":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    goto/16 :goto_1

    :sswitch_1
    const-string v1, "miui_dkt_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    goto/16 :goto_1

    :sswitch_2
    const-string v1, "screen_optimize_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_1

    :sswitch_3
    const-string v1, "screen_auto_adjust"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_4
    const-string v1, "screen_color_level"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_5
    const-string/jumbo v1, "screen_texture_color_type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    goto :goto_1

    :sswitch_6
    const-string v1, "screen_mode_type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    goto :goto_1

    :sswitch_7
    const-string/jumbo v1, "screen_true_tone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    goto :goto_1

    :sswitch_8
    const-string v1, "screen_game_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_9
    const-string/jumbo v1, "screen_paper_mode_level"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_1

    :sswitch_a
    const-string/jumbo v1, "screen_paper_texture_level"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto/16 :goto_2

    .line 1139
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateDeskTopMode(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1140
    goto/16 :goto_2

    .line 1135
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateTrueToneModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1136
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleTrueToneModeChange(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1137
    goto/16 :goto_2

    .line 1120
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1121
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1122
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmAutoAdjustEnable(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1123
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v4, v5}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdatePaperMode(Lcom/android/server/display/DisplayFeatureManagerService;ZZ)V

    .line 1125
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 1126
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v5}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetPaperColors(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    .line 1127
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmRhythmicEyeCareManager(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/RhythmicEyeCareManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V

    goto/16 :goto_2

    .line 1129
    :cond_2
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmRhythmicEyeCareManager(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/RhythmicEyeCareManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V

    .line 1130
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetPaperColors(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    goto/16 :goto_2

    .line 1112
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdatePaperColorType(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1113
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v1

    if-eq v1, v4, :cond_3

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v1

    if-ne v1, v3, :cond_6

    .line 1116
    :cond_3
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$msetPaperColors(Lcom/android/server/display/DisplayFeatureManagerService;I)V

    goto/16 :goto_2

    .line 1106
    :pswitch_4
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateAutoAdjustEnable(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1107
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$misSupportSmartEyeCare(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1108
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleAutoAdjustChange(Lcom/android/server/display/DisplayFeatureManagerService;)V

    goto :goto_2

    .line 1103
    :pswitch_5
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleGameModeChange(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1104
    goto :goto_2

    .line 1095
    :pswitch_6
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateColorSchemeCTLevel(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1096
    invoke-static {}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$sfgetSUPPORT_UNLIMITED_COLOR_MODE()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1097
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v5}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleUnlimitedColorLevelChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    goto :goto_2

    .line 1099
    :cond_4
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v5}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleScreenSchemeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1101
    goto :goto_2

    .line 1091
    :pswitch_7
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateColorSchemeModeType(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1092
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v5}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleScreenSchemeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1093
    goto :goto_2

    .line 1082
    :pswitch_8
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v1

    if-eq v1, v4, :cond_5

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v1

    if-eq v1, v3, :cond_5

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I

    move-result v1

    if-nez v1, :cond_6

    .line 1086
    :cond_5
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateReadingModeCTLevel(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1087
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v4, v4}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdatePaperMode(Lcom/android/server/display/DisplayFeatureManagerService;ZZ)V

    goto :goto_2

    .line 1077
    :pswitch_9
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mupdateReadingModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;)V

    .line 1078
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;->this$0:Lcom/android/server/display/DisplayFeatureManagerService;

    invoke-static {v1, v5}, Lcom/android/server/display/DisplayFeatureManagerService;->-$$Nest$mhandleReadingModeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V

    .line 1079
    nop

    .line 1144
    :cond_6
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x63a18e66 -> :sswitch_a
        -0x6076b212 -> :sswitch_9
        -0x56e48e43 -> :sswitch_8
        -0x4241e690 -> :sswitch_7
        -0xb0e3fbd -> :sswitch_6
        0x25b806ed -> :sswitch_5
        0x2807b455 -> :sswitch_4
        0x6fff40cc -> :sswitch_3
        0x74fb4732 -> :sswitch_2
        0x75414804 -> :sswitch_1
        0x7e544b2b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
