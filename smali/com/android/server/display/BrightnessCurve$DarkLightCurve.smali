.class Lcom/android/server/display/BrightnessCurve$DarkLightCurve;
.super Lcom/android/server/display/BrightnessCurve$Curve;
.source "BrightnessCurve.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/BrightnessCurve;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DarkLightCurve"
.end annotation


# instance fields
.field private final mHeadLux:F

.field private final mPullUpLimitNit:F

.field private final mPullUpMaxTan:F

.field private final mTailLux:F

.field final synthetic this$0:Lcom/android/server/display/BrightnessCurve;


# direct methods
.method public constructor <init>(Lcom/android/server/display/BrightnessCurve;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/display/BrightnessCurve;

    .line 304
    iput-object p1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-direct {p0, p1}, Lcom/android/server/display/BrightnessCurve$Curve;-><init>(Lcom/android/server/display/BrightnessCurve;)V

    .line 305
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmCurrentCurveInterval(Lcom/android/server/display/BrightnessCurve;)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    .line 306
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmCurrentCurveInterval(Lcom/android/server/display/BrightnessCurve;)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    .line 307
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmDarkLightCurvePullUpMaxTan(Lcom/android/server/display/BrightnessCurve;)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpMaxTan:F

    .line 308
    invoke-static {p1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmDarkLightCurvePullUpLimitNit(Lcom/android/server/display/BrightnessCurve;)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpLimitNit:F

    .line 309
    return-void
.end method


# virtual methods
.method public connectLeft(Lcom/android/server/display/BrightnessCurve$Curve;)V
    .locals 0
    .param p1, "curve"    # Lcom/android/server/display/BrightnessCurve$Curve;

    .line 343
    return-void
.end method

.method public connectRight(Lcom/android/server/display/BrightnessCurve$Curve;)V
    .locals 9
    .param p1, "curve"    # Lcom/android/server/display/BrightnessCurve$Curve;

    .line 347
    iget-object v0, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 348
    iget-object v0, p1, Lcom/android/server/display/BrightnessCurve$Curve;->mPointList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 349
    .local v0, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v2}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v2

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    invoke-virtual {v2, v3}, Landroid/util/Spline;->interpolate(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 350
    iget-object v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    iget-object v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V

    goto :goto_0

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v1

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 352
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->create(FF)V

    goto :goto_0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v1

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float/2addr v1, v2

    .line 355
    .local v1, "diffNit":F
    iget-object v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    iget v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    iget-object v6, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    const v8, 0x3f333333    # 0.7f

    move v7, v1

    invoke-static/range {v3 .. v8}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V

    .line 358
    .end local v0    # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    .end local v1    # "diffNit":F
    :cond_2
    :goto_0
    return-void
.end method

.method public create(FF)V
    .locals 9
    .param p1, "lux"    # F
    .param p2, "changeNit"    # F

    .line 313
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v0}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    cmpl-float v0, v0, p2

    if-lez v0, :cond_1

    .line 314
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x40400000    # 3.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v0}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    sub-float/2addr v0, p2

    .line 316
    .local v0, "diffNit":F
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    const/4 v6, 0x0

    move v5, v0

    invoke-static/range {v1 .. v6}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V

    .line 317
    .end local v0    # "diffNit":F
    goto/16 :goto_0

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget-object v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    iget v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmBrightnessMinTan(Lcom/android/server/display/BrightnessCurve;)F

    move-result v6

    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmBrightnessMinTan(Lcom/android/server/display/BrightnessCurve;)F

    move-result v7

    move v1, p1

    move v2, p2

    invoke-static/range {v0 .. v7}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mpullDownCurveCreate(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FFFF)V

    goto :goto_0

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v0}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/Spline;->interpolate(F)F

    move-result v0

    sub-float/2addr v0, p2

    .line 323
    .restart local v0    # "diffNit":F
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    invoke-static {v1}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$fgetmLuxToNitsDefault(Lcom/android/server/display/BrightnessCurve;)Landroid/util/Spline;

    move-result-object v1

    iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    invoke-virtual {v1, v2}, Landroid/util/Spline;->interpolate(F)F

    move-result v1

    sub-float v7, v1, v0

    .line 324
    .local v7, "tailPointNit":F
    iget v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpLimitNit:F

    sub-float v2, v1, p2

    iget v3, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    sub-float v4, v3, p1

    div-float v8, v2, v4

    .line 325
    .local v8, "tanToLimitPoint":F
    cmpg-float v1, v7, v1

    if-gez v1, :cond_2

    .line 326
    iget-object v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->this$0:Lcom/android/server/display/BrightnessCurve;

    iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    const/4 v6, 0x0

    move v5, v0

    invoke-static/range {v1 .. v6}, Lcom/android/server/display/BrightnessCurve;->-$$Nest$mcopyToDefaultSpline(Lcom/android/server/display/BrightnessCurve;FFLjava/util/List;FF)V

    .line 327
    return-void

    .line 328
    :cond_2
    iget v1, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpMaxTan:F

    cmpg-float v1, v8, v1

    if-gez v1, :cond_3

    .line 329
    iget v8, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPullUpMaxTan:F

    .line 331
    :cond_3
    sub-float/2addr v3, p1

    mul-float/2addr v3, v8

    add-float v1, p2, v3

    .line 332
    .end local v7    # "tailPointNit":F
    .local v1, "tailPointNit":F
    iget v2, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    sub-float v2, p1, v2

    mul-float/2addr v2, v8

    sub-float v2, p2, v2

    .line 333
    .local v2, "headPointNit":F
    new-instance v3, Landroid/util/Pair;

    iget v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mHeadLux:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 334
    .local v3, "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v4, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    new-instance v4, Landroid/util/Pair;

    iget v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mTailLux:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 336
    .local v4, "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v5, p0, Lcom/android/server/display/BrightnessCurve$DarkLightCurve;->mPointList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    .end local v0    # "diffNit":F
    .end local v1    # "tailPointNit":F
    .end local v2    # "headPointNit":F
    .end local v3    # "headPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    .end local v4    # "tailPoint":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    .end local v8    # "tanToLimitPoint":F
    :goto_0
    return-void
.end method
