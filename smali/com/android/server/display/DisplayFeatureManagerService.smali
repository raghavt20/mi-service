.class public Lcom/android/server/display/DisplayFeatureManagerService;
.super Lcom/android/server/SystemService;
.source "DisplayFeatureManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;,
        Lcom/android/server/display/DisplayFeatureManagerService$LocalService;,
        Lcom/android/server/display/DisplayFeatureManagerService$BinderService;,
        Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;,
        Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver;,
        Lcom/android/server/display/DisplayFeatureManagerService$PaperModeAnimatListener;,
        Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;,
        Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;,
        Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;
    }
.end annotation


# static fields
.field private static final AIDL_SERVICENAME_DEFAULT:Ljava/lang/String; = "vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature/default"

.field public static BRIGHTNESS_THROTTLER_STATUS:Ljava/lang/String; = null

.field private static final CLASSIC_READING_MODE:I = 0x0

.field private static final CONFIG_SERVICENAME_RESOURCEID:I

.field private static final FLAG_SET_SCREEN_EFFECT:I = 0x0

.field private static final FLAG_SET_VIEDEO_INFOMATION:I = 0x1

.field private static final FPS_SWITCH_DEFAULT:Z

.field private static final HIDL_SERVICENAME_DEFAULT:Ljava/lang/String; = "vendor.xiaomi.hardware.displayfeature@1.0::IDisplayFeature"

.field private static final IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

.field private static final MSG_SDR_TO_HDR:I = 0x16

.field private static final MSG_SEND_HBM_STATE:I = 0x1e

.field private static final MSG_SEND_MURA_STATE:I = 0x28

.field private static final MSG_SET_COLOR_MODE:I = 0xd

.field private static final MSG_SET_CURRENT_GRAY_VALUE:I = 0x4

.field private static final MSG_SET_DC_PARSE_STATE:I = 0xe

.field private static final MSG_SET_GRAY_VALUE:I = 0x3

.field private static final MSG_SET_PAPER_COLOR_TYPE:I = 0x10

.field private static final MSG_SET_SECONDARY_FRAME_RATE:I = 0xf

.field private static final MSG_SWITCH_DARK_MODE:I = 0xa

.field private static final MSG_UPDATE_COLOR_SCHEME:I = 0x2

.field private static final MSG_UPDATE_DFPS_MODE:I = 0xb

.field private static final MSG_UPDATE_EXPERT_MODE:I = 0xc

.field private static final MSG_UPDATE_HDR_STATE:I = 0x14

.field private static final MSG_UPDATE_PCC_LEVEL:I = 0x7

.field private static final MSG_UPDATE_READING_MODE:I = 0x1

.field private static final MSG_UPDATE_SMART_DFPS_MODE:I = 0x11

.field private static final MSG_UPDATE_TRUETONE:I = 0x13

.field private static final MSG_UPDATE_UNLIMITED_COLOR_LEVEL:I = 0x8

.field private static final MSG_UPDATE_USERCHANGE:I = 0x15

.field private static final MSG_UPDATE_WCG_STATE:I = 0x6

.field private static final NEW_CLASSIC_READING_MODE:I = 0x3

.field private static final PAPER_MODE_MIN_LEVEL:F

.field private static final PAPER_READING_MODE:I = 0x1

.field private static final PERSISTENT_PROPERTY_DISPLAY_COLOR:Ljava/lang/String; = "persist.sys.sf.native_mode"

.field private static final RHYTHMIC_READING_MODE:I = 0x2

.field private static final SCREEN_DEFAULT_FPS:I

.field private static final SUPPORT_DISPLAY_EXPERT_MODE:Z

.field private static final SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z

.field private static final SUPPORT_SET_FEATURE:Z

.field private static final SUPPORT_UNLIMITED_COLOR_MODE:Z

.field private static final SURFACE_FLINGER:Ljava/lang/String; = "SurfaceFlinger"

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE:I = 0x797c

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_DC_PARSE_STATE:I = 0x793c

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_DFPS:I = 0x793b

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_FPS_VIDEO_INFO:I = 0x798c

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_PCC:I = 0x797d

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SDR2HDR:I = 0x84d0

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SECONDARY_FRAME_RATE:I = 0x7991

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SET_MODE:I = 0x792f

.field private static final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE_SMART_DFPS:I = 0x793d

.field private static final TAG:Ljava/lang/String; = "DisplayFeatureManagerService"

.field private static final TEMP_PAPER_MODE_LEVEL:I = -0x1

.field private static final THREAD_TAG:Ljava/lang/String; = "DisplayFeatureThread"


# instance fields
.field private mAutoAdjustEnable:Z

.field private mBinderDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private mBootCompleted:Z

.field private mClientDeathCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/os/IBinder;",
            "Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mColorSchemeCTLevel:I

.field private mColorSchemeModeType:I

.field private mContext:Landroid/content/Context;

.field private mCurrentGrayScale:F

.field private mDeskTopModeEnabled:Z

.field private mDisplayFeatureServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

.field private mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private mDisplayState:I

.field private mDolbyState:Z

.field private mForceDisableEyeCare:Z

.field private mGameHdrEnabled:Z

.field private mGrayScale:F

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHwBinderDeathHandler:Landroid/os/IHwBinder$DeathRecipient;

.field private final mIsFoldOrFlip:Z

.field public mLocalService:Lcom/android/server/display/DisplayFeatureManagerInternal;

.field private final mLock:Ljava/lang/Object;

.field private mMaxDozeBrightnessInt:I

.field private mMinDozeBrightnessInt:I

.field private mMinimumBrightnessInt:I

.field private mPaperColorType:I

.field private mPaperModeAnimator:Lcom/android/server/display/MiuiRampAnimator;

.field private mPaperModeMinRate:I

.field private mPowerManager:Landroid/os/PowerManager;

.field private mReadingModeCTLevel:I

.field private mReadingModeEnabled:Z

.field private mReadingModeType:I

.field private mResolver:Landroid/content/ContentResolver;

.field private final mRhythmicEyeCareListener:Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

.field private mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

.field private mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

.field private mTrueToneModeEnabled:I

.field private mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAutoAdjustEnable(Lcom/android/server/display/DisplayFeatureManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmBootCompleted(Lcom/android/server/display/DisplayFeatureManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmClientDeathCallbacks(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/util/HashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayState(Lcom/android/server/display/DisplayFeatureManagerService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDolbyState(Lcom/android/server/display/DisplayFeatureManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDolbyState:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/display/DisplayFeatureManagerService;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPaperModeAnimator(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/MiuiRampAnimator;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeAnimator:Lcom/android/server/display/MiuiRampAnimator;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmReadingModeEnabled(Lcom/android/server/display/DisplayFeatureManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmResolver(Lcom/android/server/display/DisplayFeatureManagerService;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRhythmicEyeCareManager(Lcom/android/server/display/DisplayFeatureManagerService;)Lcom/android/server/display/RhythmicEyeCareManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentGrayScale(Lcom/android/server/display/DisplayFeatureManagerService;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mCurrentGrayScale:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDisplayState(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDolbyState(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDolbyState:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmGrayScale(Lcom/android/server/display/DisplayFeatureManagerService;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGrayScale:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWrapper(Lcom/android/server/display/DisplayFeatureManagerService;Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoDieLocked(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->doDieLocked(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleAutoAdjustChange(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleAutoAdjustChange()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleDisplayFeatureInfoChanged(Lcom/android/server/display/DisplayFeatureManagerService;I[Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->handleDisplayFeatureInfoChanged(I[Ljava/lang/Object;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleGameModeChange(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleGameModeChange()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleReadingModeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->handleReadingModeChange(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleScreenSchemeChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->handleScreenSchemeChange(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleTrueToneModeChange(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleTrueToneModeChange()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleUnlimitedColorLevelChange(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->handleUnlimitedColorLevelChange(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misDarkModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/content/Context;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misSupportSmartEyeCare(Lcom/android/server/display/DisplayFeatureManagerService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->isSupportSmartEyeCare()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mloadSettings(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->loadSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyCurrentGrayScaleChanged(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->notifyCurrentGrayScaleChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyGrayScaleChanged(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->notifyGrayScaleChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyHdrStateChanged(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->notifyHdrStateChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySFColorMode(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFColorMode(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySFDCParseState(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFDCParseState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySFDfpsMode(Lcom/android/server/display/DisplayFeatureManagerService;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFDfpsMode(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySFPccLevel(Lcom/android/server/display/DisplayFeatureManagerService;IFFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFPccLevel(IFFF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySFSecondaryFrameRate(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFSecondaryFrameRate(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifySFWcgState(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFWcgState(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendHbmState(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->sendHbmState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendMuraState(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->sendMuraState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetDarkModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setDarkModeEnable(Landroid/content/Context;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetDeathCallbackLocked(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/IBinder;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetExpertScreenMode(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->setExpertScreenMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetPaperColors(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetRhythmicColor(Lcom/android/server/display/DisplayFeatureManagerService;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setRhythmicColor(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSDR2HDR(Lcom/android/server/display/DisplayFeatureManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setSDR2HDR(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetScreenEffect(Lcom/android/server/display/DisplayFeatureManagerService;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetScreenEffectAll(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectAll(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetScreenEffectColor(Lcom/android/server/display/DisplayFeatureManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectColor(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetScreenEffectInternal(Lcom/android/server/display/DisplayFeatureManagerService;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAutoAdjustEnable(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateAutoAdjustEnable()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateColorSchemeCTLevel(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeCTLevel()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateColorSchemeModeType(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeModeType()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDeskTopMode(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateDeskTopMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePaperColorType(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperColorType()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePaperMode(Lcom/android/server/display/DisplayFeatureManagerService;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateReadingModeCTLevel(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeCTLevel()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateReadingModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeEnable()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateReadingModeType(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeType()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateTrueToneModeEnable(Lcom/android/server/display/DisplayFeatureManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateTrueToneModeEnable()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateVideoInformationIfNeeded(Lcom/android/server/display/DisplayFeatureManagerService;IZFIIFLandroid/os/IBinder;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/android/server/display/DisplayFeatureManagerService;->updateVideoInformationIfNeeded(IZFIIFLandroid/os/IBinder;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetIS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetSUPPORT_UNLIMITED_COLOR_MODE()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_UNLIMITED_COLOR_MODE:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 123
    nop

    .line 124
    const-string v0, "is_compatible_paper_and_screen_effect"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    .line 128
    nop

    .line 129
    const-string v0, "paper_mode_min_level"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getFloat(Ljava/lang/String;F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/android/server/display/DisplayFeatureManagerService;->PAPER_MODE_MIN_LEVEL:F

    .line 133
    sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_UNLIMITED_COLOR_MODE:Z

    sput-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_UNLIMITED_COLOR_MODE:Z

    .line 136
    sget-boolean v0, Lcom/android/server/display/expertmode/ExpertData;->SUPPORT_DISPLAY_EXPERT_MODE:Z

    sput-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_DISPLAY_EXPERT_MODE:Z

    .line 139
    nop

    .line 140
    const-string v0, "defaultFps"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/display/DisplayFeatureManagerService;->SCREEN_DEFAULT_FPS:I

    .line 141
    nop

    .line 142
    const-string v0, "ro.vendor.fps.switch.default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->FPS_SWITCH_DEFAULT:Z

    .line 194
    const-string v0, "brightness_throttler_status"

    sput-object v0, Lcom/android/server/display/DisplayFeatureManagerService;->BRIGHTNESS_THROTTLER_STATUS:Ljava/lang/String;

    .line 211
    nop

    .line 212
    const-string/jumbo v0, "support_screen_effect"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_SET_FEATURE:Z

    .line 218
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v2, "string"

    const-string v3, "android"

    const-string v4, "config_displayFeatureHidlServiceName"

    invoke-virtual {v0, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/server/display/DisplayFeatureManagerService;->CONFIG_SERVICENAME_RESOURCEID:I

    .line 222
    nop

    .line 223
    const-string v0, "ro.vendor.aod.brightness.cust"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 245
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 178
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I

    .line 185
    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGrayScale:F

    .line 188
    iput v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mCurrentGrayScale:F

    .line 202
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    .line 207
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLock:Ljava/lang/Object;

    .line 208
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v2

    if-nez v2, :cond_0

    .line 209
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDeviceInside()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mIsFoldOrFlip:Z

    .line 236
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$1;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareListener:Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;

    .line 246
    iput-object p1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    .line 247
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    .line 248
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "DisplayFeatureThread"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 249
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 250
    new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;

    iget-object v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureHandler;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    .line 251
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPowerManager:Landroid/os/PowerManager;

    .line 252
    invoke-virtual {v2}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    move-result v2

    iput v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMinimumBrightnessInt:I

    .line 253
    const-class v2, Landroid/hardware/display/DisplayManagerInternal;

    invoke-virtual {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->getLocalService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 254
    new-instance v2, Lcom/android/server/display/RhythmicEyeCareManager;

    iget-object v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/android/server/display/RhythmicEyeCareManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    .line 255
    invoke-virtual {v2, v0}, Lcom/android/server/display/RhythmicEyeCareManager;->setRhythmicEyeCareListener(Lcom/android/server/display/RhythmicEyeCareManager$RhythmicEyeCareListener;)V

    .line 256
    nop

    .line 257
    invoke-static {}, Lcom/android/server/display/DisplayFeatureManagerServiceStub;->getInstance()Lcom/android/server/display/DisplayFeatureManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayFeatureServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    .line 258
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initServiceDeathRecipient()V

    .line 259
    monitor-enter v1

    .line 260
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initWrapperLocked()V

    .line 261
    monitor-exit v1

    .line 262
    return-void

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private doDieLocked(I)V
    .locals 7
    .param p1, "flag"    # I

    .line 1044
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1045
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFVideoInformation(ZFIIF)V

    .line 1047
    :cond_0
    return-void
.end method

.method private getEffectedDisplayIndex(J)I
    .locals 6
    .param p1, "physicalDisplayId"    # J

    .line 526
    const/4 v0, 0x0

    .line 527
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mIsFoldOrFlip:Z

    if-eqz v1, :cond_0

    .line 528
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 529
    .local v1, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 531
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 532
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 533
    const-string v3, "display"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    .line 534
    .local v3, "b":Landroid/os/IBinder;
    const v4, 0xfffffb

    const/4 v5, 0x0

    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 535
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v4

    .line 539
    .end local v3    # "b":Landroid/os/IBinder;
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 540
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 541
    goto :goto_2

    .line 539
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 536
    :catch_0
    move-exception v3

    .line 537
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v3    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 539
    :goto_1
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 540
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 541
    throw v3

    .line 543
    .end local v1    # "data":Landroid/os/Parcel;
    .end local v2    # "reply":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return v0
.end method

.method private getEffectedDisplayIndex()[I
    .locals 6

    .line 504
    const/4 v0, 0x0

    filled-new-array {v0}, [I

    move-result-object v1

    .line 505
    .local v1, "result":[I
    iget-boolean v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mIsFoldOrFlip:Z

    if-eqz v2, :cond_0

    .line 506
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 507
    .local v2, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 509
    .local v3, "reply":Landroid/os/Parcel;
    :try_start_0
    const-string v4, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 510
    const-string v4, "display"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    .line 511
    .local v4, "b":Landroid/os/IBinder;
    const v5, 0xfffffc

    invoke-interface {v4, v5, v2, v3, v0}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 512
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 513
    .local v0, "count":I
    new-array v5, v0, [I

    move-object v1, v5

    .line 514
    invoke-virtual {v3, v1}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v0    # "count":I
    .end local v4    # "b":Landroid/os/IBinder;
    goto :goto_0

    .line 518
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 518
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 519
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 520
    goto :goto_2

    .line 518
    :goto_1
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 519
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 520
    throw v0

    .line 522
    .end local v2    # "data":Landroid/os/Parcel;
    .end local v3    # "reply":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-object v1
.end method

.method private getExpertData(Landroid/content/Context;)Lcom/android/server/display/expertmode/ExpertData;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 1573
    invoke-static {p1}, Lcom/android/server/display/expertmode/ExpertData;->getFromDatabase(Landroid/content/Context;)Lcom/android/server/display/expertmode/ExpertData;

    move-result-object v0

    .line 1575
    .local v0, "data":Lcom/android/server/display/expertmode/ExpertData;
    if-nez v0, :cond_0

    .line 1576
    invoke-static {}, Lcom/android/server/display/expertmode/ExpertData;->getDefaultValue()Lcom/android/server/display/expertmode/ExpertData;

    move-result-object v0

    .line 1578
    :cond_0
    return-object v0
.end method

.method private getScreenDpiMode()I
    .locals 2

    .line 444
    const-string v0, "persist.vendor.dfps.level"

    sget v1, Lcom/android/server/display/DisplayFeatureManagerService;->SCREEN_DEFAULT_FPS:I

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private handleAutoAdjustChange()V
    .locals 2

    .line 1275
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z

    if-eqz v0, :cond_0

    .line 1276
    const/4 v0, 0x3

    const/16 v1, 0x100

    invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    goto :goto_0

    .line 1279
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeCTLevel()V

    .line 1280
    const/4 v0, 0x1

    invoke-direct {p0, v0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V

    .line 1282
    :goto_0
    return-void
.end method

.method private varargs handleDisplayFeatureInfoChanged(I[Ljava/lang/Object;)V
    .locals 6
    .param p1, "caseId"    # I
    .param p2, "params"    # [Ljava/lang/Object;

    .line 380
    array-length v0, p2

    const/4 v1, 0x3

    const/4 v2, 0x4

    const/4 v3, 0x0

    if-lez v0, :cond_b

    .line 381
    const/16 v0, 0x2710

    if-ne p1, v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 383
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 382
    const/4 v5, 0x6

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 383
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 385
    :cond_0
    const/16 v0, 0x2733

    if-ne p1, v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 387
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 386
    const/16 v5, 0xb

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 387
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 389
    :cond_1
    const/16 v0, 0x2735

    if-ne p1, v0, :cond_2

    .line 390
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 391
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 390
    const/16 v5, 0x11

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 393
    :cond_2
    if-nez p1, :cond_3

    .line 394
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 395
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 394
    invoke-virtual {v0, v1, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 397
    :cond_3
    const/16 v0, 0x7530

    if-ne p1, v0, :cond_4

    .line 398
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 399
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 398
    const/16 v5, 0xd

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 401
    :cond_4
    const/16 v0, 0x7531

    if-ne p1, v0, :cond_5

    .line 402
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 403
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 402
    const/16 v5, 0x16

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 405
    :cond_5
    const v0, 0x9c40

    if-ne p1, v0, :cond_6

    .line 406
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 407
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 406
    const/16 v5, 0xe

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 407
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 409
    :cond_6
    const v0, 0xc350

    if-ne p1, v0, :cond_7

    .line 410
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 411
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 410
    const/16 v5, 0x14

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 411
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 413
    :cond_7
    const/16 v0, 0x2734

    if-ne p1, v0, :cond_8

    .line 414
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 415
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 414
    const/16 v5, 0xf

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 415
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 418
    :cond_8
    const v0, 0xea60

    if-ne p1, v0, :cond_9

    .line 419
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x1e

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 422
    :cond_9
    const v0, 0x11170

    if-ne p1, v0, :cond_a

    .line 423
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/16 v5, 0x28

    invoke-virtual {v0, v5, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 426
    :cond_a
    const v0, 0x17318

    if-ne p1, v0, :cond_b

    .line 427
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 428
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v4, p2, v3

    check-cast v4, Ljava/lang/Integer;

    .line 429
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 428
    invoke-virtual {v0, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 432
    :cond_b
    const/16 v0, 0x4e20

    if-ne p1, v0, :cond_c

    array-length v0, p2

    if-lt v0, v2, :cond_c

    .line 434
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v0

    .line 435
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    const/4 v2, 0x1

    aget-object v2, p2, v2

    iput-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    .line 436
    const/4 v2, 0x2

    aget-object v2, p2, v2

    iput-object v2, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    .line 437
    aget-object v1, p2, v1

    iput-object v1, v0, Lcom/android/internal/os/SomeArgs;->arg3:Ljava/lang/Object;

    .line 438
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    aget-object v2, p2, v3

    check-cast v2, Ljava/lang/Integer;

    .line 439
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 438
    const/4 v4, 0x7

    invoke-virtual {v1, v4, v2, v3, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 439
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 441
    .end local v0    # "args":Lcom/android/internal/os/SomeArgs;
    :cond_c
    return-void
.end method

.method private handleGameModeChange()V
    .locals 9

    .line 1383
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_game_mode"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 1385
    .local v0, "gameMode":I
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "game_hdr_level"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 1387
    .local v1, "gameHdrLevel":I
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    move v3, v4

    goto :goto_0

    :cond_0
    move v3, v2

    .line 1388
    .local v3, "gameHdrEnabled":Z
    :goto_0
    and-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_1

    move v5, v4

    goto :goto_1

    :cond_1
    move v5, v2

    .line 1390
    .local v5, "forceDisableEyecare":Z
    :goto_1
    iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z

    if-eq v6, v5, :cond_2

    .line 1391
    iput-boolean v5, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z

    .line 1394
    :cond_2
    iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGameHdrEnabled:Z

    const/4 v7, 0x2

    if-eq v6, v3, :cond_8

    .line 1395
    iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGameHdrEnabled:Z

    .line 1396
    const/16 v6, 0x13

    if-nez v3, :cond_3

    .line 1398
    invoke-direct {p0, v6, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    .line 1399
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->handleScreenSchemeChange(Z)V

    goto :goto_3

    .line 1403
    :cond_3
    sget-boolean v8, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    if-nez v8, :cond_5

    if-eqz v5, :cond_4

    iget-boolean v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    if-nez v8, :cond_5

    :cond_4
    iget-boolean v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    if-nez v8, :cond_8

    .line 1407
    :cond_5
    if-eqz v5, :cond_7

    iget-boolean v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    if-eqz v8, :cond_7

    .line 1408
    iget v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    if-ne v8, v7, :cond_6

    .line 1409
    iget-object v8, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-virtual {v8, v2}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V

    goto :goto_2

    .line 1411
    :cond_6
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V

    .line 1413
    :goto_2
    invoke-direct {p0, v2, v4}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V

    .line 1415
    :cond_7
    invoke-direct {p0, v6, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    .line 1422
    :cond_8
    :goto_3
    sget-boolean v6, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    if-nez v6, :cond_9

    if-nez v3, :cond_10

    :cond_9
    iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    if-eqz v6, :cond_10

    .line 1423
    iget-boolean v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z

    if-eqz v6, :cond_b

    .line 1424
    if-nez v5, :cond_a

    .line 1425
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleAutoAdjustChange()V

    goto :goto_7

    .line 1427
    :cond_a
    invoke-direct {p0, v2, v4}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V

    goto :goto_7

    .line 1433
    :cond_b
    iget v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    if-ne v6, v7, :cond_d

    .line 1434
    iget-object v6, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    if-nez v5, :cond_c

    move v7, v4

    goto :goto_4

    :cond_c
    move v7, v2

    :goto_4
    invoke-virtual {v6, v7}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V

    goto :goto_5

    .line 1436
    :cond_d
    if-eqz v5, :cond_e

    move v6, v2

    :cond_e
    invoke-direct {p0, v6}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V

    .line 1438
    :goto_5
    if-nez v5, :cond_f

    goto :goto_6

    :cond_f
    move v4, v2

    :goto_6
    invoke-direct {p0, v4, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V

    .line 1441
    :cond_10
    :goto_7
    return-void
.end method

.method private handleReadingModeChange(Z)V
    .locals 4
    .param p1, "immediate"    # Z

    .line 1148
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 1149
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    .line 1150
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V

    .line 1151
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-virtual {v0, v3}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V

    goto :goto_0

    .line 1153
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V

    .line 1155
    :goto_0
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z

    if-eqz v0, :cond_1

    .line 1156
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->handleAutoAdjustChange()V

    goto :goto_1

    .line 1158
    :cond_1
    invoke-direct {p0, v3, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V

    goto :goto_1

    .line 1161
    :cond_2
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    if-ne v0, v1, :cond_3

    .line 1162
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-virtual {v0, v2}, Lcom/android/server/display/RhythmicEyeCareManager;->setModeEnable(Z)V

    .line 1164
    :cond_3
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setPaperColors(I)V

    .line 1165
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z

    invoke-direct {p0, v2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperMode(ZZ)V

    .line 1167
    :goto_1
    return-void
.end method

.method private handleScreenSchemeChange(Z)V
    .locals 4
    .param p1, "userChange"    # Z

    .line 1355
    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGameHdrEnabled:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z

    if-nez v0, :cond_1

    .line 1357
    return-void

    .line 1360
    :cond_1
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeCTLevel:I

    .line 1361
    .local v0, "value":I
    const/4 v1, 0x0

    .line 1363
    .local v1, "mode":I
    iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeModeType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1364
    const/4 v1, 0x1

    goto :goto_0

    .line 1365
    :cond_2
    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 1366
    const/4 v1, 0x2

    goto :goto_0

    .line 1367
    :cond_3
    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    .line 1373
    iget-boolean v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z

    if-eqz v2, :cond_4

    if-eqz p1, :cond_5

    .line 1374
    :cond_4
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->setExpertScreenMode()V

    .line 1376
    :cond_5
    return-void

    .line 1378
    :cond_6
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    .line 1379
    return-void
.end method

.method private handleTrueToneModeChange()V
    .locals 2

    .line 1453
    const/16 v0, 0x20

    iget v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mTrueToneModeEnabled:I

    invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    .line 1455
    return-void
.end method

.method private handleUnlimitedColorLevelChange(Z)V
    .locals 2
    .param p1, "userChange"    # Z

    .line 1445
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeModeType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 1447
    :cond_0
    const/16 v0, 0x17

    iget v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeCTLevel:I

    invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    .line 1450
    :cond_1
    return-void
.end method

.method private initServiceDeathRecipient()V
    .locals 1

    .line 632
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z

    if-eqz v0, :cond_0

    .line 633
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$5;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$5;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHwBinderDeathHandler:Landroid/os/IHwBinder$DeathRecipient;

    goto :goto_0

    .line 643
    :cond_0
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$6;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$6;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBinderDeathHandler:Landroid/os/IBinder$DeathRecipient;

    .line 653
    :goto_0
    return-void
.end method

.method private initWrapperLocked()V
    .locals 6

    .line 595
    const-string v0, "DisplayFeatureManagerService"

    :try_start_0
    sget-boolean v1, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z

    if-eqz v1, :cond_2

    .line 597
    sget v1, Lcom/android/server/display/DisplayFeatureManagerService;->CONFIG_SERVICENAME_RESOURCEID:I

    if-nez v1, :cond_0

    .line 598
    const-string/jumbo v2, "vendor.xiaomi.hardware.displayfeature@1.0::IDisplayFeature"

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 599
    .local v2, "hidlServiceName":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initProxyLocked CONFIG_SERVICENAME_RESOURCEID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " hidlServiceName: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    const-string v1, "default"

    invoke-static {v2, v1}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v1

    .line 602
    .local v1, "hb":Landroid/os/IHwBinder;
    if-eqz v1, :cond_1

    .line 603
    iget-object v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHwBinderDeathHandler:Landroid/os/IHwBinder$DeathRecipient;

    const-wide/16 v4, 0x2711

    invoke-interface {v1, v3, v4, v5}, Landroid/os/IHwBinder;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z

    .line 604
    new-instance v3, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    invoke-direct {v3, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    .line 606
    .end local v1    # "hb":Landroid/os/IHwBinder;
    .end local v2    # "hidlServiceName":Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 607
    :cond_2
    const-string v1, "initProxyLocked aidlServiceName: vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature/default"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const-string/jumbo v1, "vendor.xiaomi.hardware.displayfeature_aidl.IDisplayFeature/default"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 609
    .local v1, "b":Landroid/os/IBinder;
    if-eqz v1, :cond_3

    .line 610
    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBinderDeathHandler:Landroid/os/IBinder$DeathRecipient;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 611
    new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    invoke-direct {v2, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 616
    .end local v1    # "b":Landroid/os/IBinder;
    :cond_3
    :goto_1
    goto :goto_2

    .line 614
    :catch_0
    move-exception v1

    .line 615
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "initProxyLocked failed."

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 617
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private isDarkModeEnable(Landroid/content/Context;)Z
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;

    .line 1484
    nop

    .line 1485
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1484
    const-string/jumbo v1, "ui_night_mode"

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x2

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    return v2
.end method

.method private isForegroundApp(I)Z
    .locals 4
    .param p1, "pid"    # I

    .line 980
    const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal;

    .line 981
    .local v0, "mAtmInternal":Lcom/android/server/wm/ActivityTaskManagerInternal;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 982
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/server/wm/WindowProcessController;->getPid()I

    move-result v3

    goto :goto_1

    :cond_1
    move v3, v2

    .line 983
    .local v3, "topAppPid":I
    :goto_1
    if-ne p1, v3, :cond_2

    .line 984
    const/4 v2, 0x1

    return v2

    .line 986
    :cond_2
    return v2
.end method

.method private isSupportSmartEyeCare()Z
    .locals 2

    .line 1285
    const-string/jumbo v0, "support_smart_eyecare"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private loadSettings()V
    .locals 1

    .line 818
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeEnable()V

    .line 819
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeType()V

    .line 820
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperColorType()V

    .line 821
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->resetLocalPaperLevelIfNeed()V

    .line 824
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateAutoAdjustEnable()V

    .line 827
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeModeType()V

    .line 828
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateColorSchemeCTLevel()V

    .line 831
    sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_TRUETONE_MODE:Z

    if-eqz v0, :cond_0

    .line 832
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateTrueToneModeEnable()V

    .line 834
    :cond_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateDeskTopMode()V

    .line 835
    return-void
.end method

.method private notifyCurrentGrayScaleChanged()V
    .locals 4

    .line 1559
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1560
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "current_gray_scale"

    iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mCurrentGrayScale:F

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1561
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    const/4 v2, 0x0

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/display/DisplayManagerInternal;->notifyDisplayManager(IILandroid/os/Bundle;)V

    .line 1563
    return-void
.end method

.method private notifyGrayScaleChanged()V
    .locals 4

    .line 1552
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1553
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "gray_scale"

    iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mGrayScale:F

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1554
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/display/DisplayManagerInternal;->notifyDisplayManager(IILandroid/os/Bundle;)V

    .line 1556
    return-void
.end method

.method private notifyHdrStateChanged()V
    .locals 4

    .line 1566
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1567
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "dolby_version_state"

    iget-boolean v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDolbyState:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1568
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/display/DisplayManagerInternal;->notifyDisplayManager(IILandroid/os/Bundle;)V

    .line 1570
    return-void
.end method

.method private notifySFColorMode(I)V
    .locals 5
    .param p1, "mode"    # I

    .line 717
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "display_color_mode"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 719
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 720
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 721
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 722
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 723
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 725
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x792f

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 727
    const-string v2, "persist.sys.sf.native_mode"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 731
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 732
    goto :goto_2

    .line 731
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 728
    :catch_0
    move-exception v2

    .line 729
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to notify dfps mode to SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 731
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 732
    throw v2

    .line 734
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private notifySFDCParseState(I)V
    .locals 5
    .param p1, "state"    # I

    .line 754
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 755
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 756
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 757
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 758
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 760
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x793c

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 765
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 766
    goto :goto_2

    .line 765
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 762
    :catch_0
    move-exception v2

    .line 763
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to notify dc parse state to SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 765
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 766
    throw v2

    .line 768
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private notifySFDfpsMode(II)V
    .locals 5
    .param p1, "mode"    # I
    .param p2, "msg"    # I

    .line 673
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 674
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_1

    .line 675
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 676
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 677
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 679
    const/16 v2, 0x11

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-ne p2, v2, :cond_0

    .line 680
    const/16 v2, 0x793d

    :try_start_0
    invoke-interface {v0, v2, v1, v4, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    goto :goto_0

    .line 683
    :cond_0
    const/16 v2, 0x793b

    invoke-interface {v0, v2, v1, v4, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 689
    :goto_0
    nop

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 690
    goto :goto_3

    .line 689
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 686
    :catch_0
    move-exception v2

    .line 687
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to notify dfps mode to SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 689
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_1

    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 690
    throw v2

    .line 692
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_1
    :goto_3
    return-void
.end method

.method private notifySFPccLevel(IFFF)V
    .locals 5
    .param p1, "level"    # I
    .param p2, "red"    # F
    .param p3, "green"    # F
    .param p4, "blue"    # F

    .line 695
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 696
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 697
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 698
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 699
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 700
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 701
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeFloat(F)V

    .line 702
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeFloat(F)V

    .line 704
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x797d

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 709
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 710
    goto :goto_2

    .line 709
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 706
    :catch_0
    move-exception v2

    .line 707
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to notifySurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 709
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 710
    throw v2

    .line 712
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private notifySFSecondaryFrameRate(I)V
    .locals 5
    .param p1, "rateState"    # I

    .line 771
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 772
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 773
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 774
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 775
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 777
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x7991

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 782
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 783
    goto :goto_2

    .line 782
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 779
    :catch_0
    move-exception v2

    .line 780
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to notify dc parse state to SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 782
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 783
    throw v2

    .line 785
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private notifySFVideoInformation(ZFIIF)V
    .locals 7
    .param p1, "bulletChatStatus"    # Z
    .param p2, "frameRate"    # F
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "compressionRatio"    # F

    .line 789
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 790
    .local v0, "surfaceFlinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 791
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 792
    .local v1, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 793
    .local v2, "reply":Landroid/os/Parcel;
    const-string v3, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 794
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 795
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 796
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 797
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 798
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeFloat(F)V

    .line 799
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "notifySFVideoInformation bulletChatStatus:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", frameRate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", resolution:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", compressionRatio:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DisplayFeatureManagerService"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    const/16 v3, 0x798c

    const/4 v5, 0x0

    :try_start_0
    invoke-interface {v0, v3, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 810
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 811
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 812
    goto :goto_2

    .line 810
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 807
    :catch_0
    move-exception v3

    .line 808
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifySFVideoInformation RemoteException:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 810
    nop

    .end local v3    # "e":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 811
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 812
    throw v3

    .line 814
    .end local v1    # "data":Landroid/os/Parcel;
    .end local v2    # "reply":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private notifySFWcgState(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .line 656
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 657
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 658
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 659
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 660
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 662
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x797c

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 668
    goto :goto_2

    .line 667
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 664
    :catch_0
    move-exception v2

    .line 665
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to notifySurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 667
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 668
    throw v2

    .line 670
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private resetLocalPaperLevelIfNeed()V
    .locals 5

    .line 1053
    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    int-to-float v0, v0

    sget v1, Lcom/android/server/display/DisplayFeatureManagerService;->PAPER_MODE_MIN_LEVEL:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_paper_mode_level"

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 1058
    .local v0, "tempValue":I
    if-eq v0, v2, :cond_0

    .line 1059
    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    iput v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    .line 1060
    iget-object v4, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v4, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1065
    .end local v0    # "tempValue":I
    :cond_0
    return-void
.end method

.method private sendHbmState(I)V
    .locals 0
    .param p1, "type"    # I

    .line 1260
    invoke-static {p1}, Landroid/cameracovered/MiuiCameraCoveredManager;->hbmCoveredAnimation(I)V

    .line 1261
    return-void
.end method

.method private sendMuraState(I)V
    .locals 0
    .param p1, "type"    # I

    .line 1264
    invoke-static {p1}, Landroid/cameracovered/MiuiCameraCoveredManager;->cupMuraCoveredAnimation(I)V

    .line 1265
    return-void
.end method

.method private setDarkModeEnable(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "enable"    # Z

    .line 1476
    const-class v0, Landroid/app/UiModeManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 1477
    .local v0, "manager":Landroid/app/UiModeManager;
    if-nez v0, :cond_0

    .line 1478
    return-void

    .line 1480
    :cond_0
    if-eqz p2, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    .line 1481
    return-void
.end method

.method private setDeathCallbackLocked(Landroid/os/IBinder;IZ)V
    .locals 0
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I
    .param p3, "register"    # Z

    .line 990
    if-eqz p3, :cond_0

    .line 991
    invoke-virtual {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->registerDeathCallbackLocked(Landroid/os/IBinder;I)V

    goto :goto_0

    .line 993
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->unregisterDeathCallbackLocked(Landroid/os/IBinder;)V

    .line 995
    :goto_0
    return-void
.end method

.method private setDisplayFeature(IIII)V
    .locals 4
    .param p1, "displayId"    # I
    .param p2, "mode"    # I
    .param p3, "value"    # I
    .param p4, "cookie"    # I

    .line 491
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 492
    :try_start_0
    const-string v1, "DisplayFeatureManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setScreenEffect displayId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cookie="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 493
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 492
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    if-nez v1, :cond_0

    .line 495
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initWrapperLocked()V

    .line 497
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    if-eqz v1, :cond_1

    .line 498
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->setFeature(IIII)V

    .line 500
    :cond_1
    monitor-exit v0

    .line 501
    return-void

    .line 500
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setExpertScreenMode()V
    .locals 4

    .line 1582
    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_DISPLAY_EXPERT_MODE:Z

    if-nez v0, :cond_0

    .line 1583
    const-string v0, "DisplayFeatureManagerService"

    const-string v1, "device don\'t support DISPLAY_EXPERT_MODE"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->getExpertData(Landroid/content/Context;)Lcom/android/server/display/expertmode/ExpertData;

    move-result-object v0

    .line 1587
    .local v0, "data":Lcom/android/server/display/expertmode/ExpertData;
    if-nez v0, :cond_1

    .line 1588
    return-void

    .line 1590
    :cond_1
    const/4 v1, 0x0

    .line 1591
    .local v1, "cookie":I
    :goto_0
    const/16 v2, 0x9

    if-ge v1, v2, :cond_2

    .line 1592
    nop

    .line 1593
    invoke-virtual {v0, v1}, Lcom/android/server/display/expertmode/ExpertData;->getByCookie(I)I

    move-result v2

    .line 1592
    const/16 v3, 0x1a

    invoke-direct {p0, v3, v2, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V

    .line 1591
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1595
    .end local v1    # "cookie":I
    :cond_2
    return-void
.end method

.method private setPaperColors(I)V
    .locals 3
    .param p1, "type"    # I

    .line 1296
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v1, v0

    .line 1297
    .local v1, "isPaperColorType":Z
    :cond_1
    :goto_0
    nop

    .line 1298
    if-eqz v1, :cond_2

    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperColorType:I

    .line 1297
    :cond_2
    const/16 v2, 0x1f

    invoke-direct {p0, v2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    .line 1299
    return-void
.end method

.method private setRhythmicColor(II)V
    .locals 4
    .param p1, "category"    # I
    .param p2, "time"    # I

    .line 1305
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1306
    .local v0, "modeEnable":Z
    :goto_0
    nop

    .line 1307
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 1306
    :goto_1
    const/16 v2, 0x36

    invoke-virtual {p0, v2, v1, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setRhythmicScreenEffect(IIII)V

    .line 1308
    return-void
.end method

.method private setSDR2HDR(I)V
    .locals 5
    .param p1, "mode"    # I

    .line 737
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 738
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 739
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 740
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 741
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 743
    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x84d0

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 748
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 749
    goto :goto_2

    .line 748
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 745
    :catch_0
    move-exception v2

    .line 746
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DisplayFeatureManagerService"

    const-string v4, "Failed to set sdr2hdr to SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 748
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 749
    throw v2

    .line 751
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private setScreenEffect(II)V
    .locals 1
    .param p1, "mode"    # I
    .param p2, "value"    # I

    .line 480
    const/16 v0, 0xff

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V

    .line 481
    return-void
.end method

.method private setScreenEffectAll(Z)V
    .locals 3
    .param p1, "userChange"    # Z

    .line 455
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectColor(Z)V

    .line 458
    sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_TRUETONE_MODE:Z

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 464
    return-void
.end method

.method private setScreenEffectColor(Z)V
    .locals 4
    .param p1, "userChange"    # Z

    .line 469
    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_UNLIMITED_COLOR_MODE:Z

    if-eqz v0, :cond_0

    .line 470
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 474
    :cond_0
    sget-boolean v1, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 477
    :cond_2
    return-void
.end method

.method private setScreenEffectInternal(III)V
    .locals 3
    .param p1, "mode"    # I
    .param p2, "value"    # I
    .param p3, "cookie"    # I

    .line 484
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->getEffectedDisplayIndex()[I

    move-result-object v0

    .line 485
    .local v0, "displays":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 486
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDisplayFeature(IIII)V

    .line 485
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 488
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method private setScreenEyeCare(ZZ)V
    .locals 5
    .param p1, "enabled"    # Z
    .param p2, "immediate"    # Z

    .line 1326
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_PAPERMODE_ANIMATION:Z

    const/4 v1, 0x3

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 1328
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeAnimator:Lcom/android/server/display/MiuiRampAnimator;

    if-eqz v0, :cond_3

    if-nez p2, :cond_0

    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayState:I

    const/4 v3, 0x1

    if-eq v0, v3, :cond_3

    .line 1330
    :cond_0
    if-eqz p2, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    mul-int/lit8 v0, v0, 0x2

    div-int/2addr v0, v1

    iget v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeMinRate:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1331
    .local v0, "rate":I
    :goto_0
    if-eqz p1, :cond_2

    iget v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    goto :goto_1

    :cond_2
    move v3, v2

    .line 1332
    .local v3, "targetLevel":I
    :goto_1
    iget-object v4, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeAnimator:Lcom/android/server/display/MiuiRampAnimator;

    invoke-virtual {v4, v3, v0}, Lcom/android/server/display/MiuiRampAnimator;->animateTo(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1333
    return-void

    .line 1338
    .end local v0    # "rate":I
    .end local v3    # "targetLevel":I
    :cond_3
    if-eqz p1, :cond_4

    .line 1339
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    invoke-direct {p0, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    goto :goto_2

    .line 1342
    :cond_4
    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->IS_COMPATIBLE_PAPER_AND_SCREEN_EFFECT:Z

    if-eqz v0, :cond_5

    .line 1343
    invoke-direct {p0, v1, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffect(II)V

    goto :goto_2

    .line 1345
    :cond_5
    invoke-direct {p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService;->handleScreenSchemeChange(Z)V

    .line 1348
    :goto_2
    return-void
.end method

.method private updateAutoAdjustEnable()V
    .locals 4

    .line 881
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->isSupportSmartEyeCare()Z

    move-result v0

    if-nez v0, :cond_0

    .line 882
    return-void

    .line 884
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_auto_adjust"

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v3, 0x1

    :cond_1
    iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mAutoAdjustEnable:Z

    .line 887
    return-void
.end method

.method private updateClassicReadingModeCTLevel()V
    .locals 4

    .line 919
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    sget v1, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    const/4 v2, -0x2

    const-string/jumbo v3, "screen_paper_mode_level"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    .line 922
    return-void
.end method

.method private updateColorSchemeCTLevel()V
    .locals 4

    .line 860
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x2

    const/4 v2, -0x2

    const-string v3, "screen_color_level"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeCTLevel:I

    .line 863
    return-void
.end method

.method private updateColorSchemeModeType()V
    .locals 4

    .line 869
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    sget v1, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_SCREEN_OPTIMIZE_MODE:I

    const/4 v2, -0x2

    const-string v3, "screen_optimize_mode"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mColorSchemeModeType:I

    .line 872
    return-void
.end method

.method private updateDeskTopMode()V
    .locals 4

    .line 974
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_dkt_mode"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDeskTopModeEnabled:Z

    .line 976
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mRhythmicEyeCareManager:Lcom/android/server/display/RhythmicEyeCareManager;

    invoke-virtual {v0, v3}, Lcom/android/server/display/RhythmicEyeCareManager;->updateDeskTopMode(Z)V

    .line 977
    return-void
.end method

.method private updatePaperColorType()V
    .locals 4

    .line 851
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string/jumbo v3, "screen_texture_color_type"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperColorType:I

    .line 854
    return-void
.end method

.method private updatePaperMode(ZZ)V
    .locals 1
    .param p1, "enabled"    # Z
    .param p2, "immediate"    # Z

    .line 1268
    iget-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mForceDisableEyeCare:Z

    if-eqz v0, :cond_0

    .line 1269
    return-void

    .line 1271
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEyeCare(ZZ)V

    .line 1272
    return-void
.end method

.method private updatePaperReadingModeCTLevel()V
    .locals 4

    .line 928
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    sget v1, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_TEXTURE_MODE_LEVEL:F

    float-to-int v1, v1

    const/4 v2, -0x2

    const-string/jumbo v3, "screen_paper_texture_level"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    .line 931
    return-void
.end method

.method private updateReadingModeCTLevel()V
    .locals 1

    .line 947
    iget v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 956
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateRhythmicModeCTLevel()V

    .line 957
    goto :goto_0

    .line 953
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updatePaperReadingModeCTLevel()V

    .line 954
    goto :goto_0

    .line 949
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateClassicReadingModeCTLevel()V

    .line 950
    nop

    .line 961
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateReadingModeEnable()V
    .locals 4

    .line 893
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string/jumbo v2, "screen_paper_mode_enabled"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeEnabled:Z

    .line 897
    return-void
.end method

.method private updateReadingModeType()V
    .locals 4

    .line 907
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string v3, "screen_mode_type"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeType:I

    .line 912
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->updateReadingModeCTLevel()V

    .line 913
    return-void
.end method

.method private updateRhythmicModeCTLevel()V
    .locals 4

    .line 937
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    sget v1, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_RHYTHMIC_EYECARE_LEVEL:I

    const/4 v2, -0x2

    const-string/jumbo v3, "screen_rhythmic_mode_level"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mReadingModeCTLevel:I

    .line 940
    return-void
.end method

.method private updateTrueToneModeEnable()V
    .locals 4

    .line 842
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string/jumbo v3, "screen_true_tone"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mTrueToneModeEnabled:I

    .line 845
    return-void
.end method

.method private updateVideoInformationIfNeeded(IZFIIFLandroid/os/IBinder;)V
    .locals 6
    .param p1, "pid"    # I
    .param p2, "bulletChatStatus"    # Z
    .param p3, "frameRate"    # F
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "compressionRatio"    # F
    .param p7, "token"    # Landroid/os/IBinder;

    .line 966
    invoke-direct {p0, p1}, Lcom/android/server/display/DisplayFeatureManagerService;->isForegroundApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 967
    return-void

    .line 969
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p7, v0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setDeathCallbackLocked(Landroid/os/IBinder;IZ)V

    .line 970
    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFVideoInformation(ZFIIF)V

    .line 971
    return-void
.end method


# virtual methods
.method public loadDozeBrightnessThreshold()V
    .locals 6

    .line 556
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 557
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 559
    .local v1, "reply":Landroid/os/Parcel;
    :try_start_0
    const-string v2, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 560
    const-string v2, "display"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 561
    .local v2, "b":Landroid/os/IBinder;
    const v3, 0xfffff7

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 562
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 563
    .local v3, "length":I
    const/4 v5, 0x2

    if-ne v3, v5, :cond_0

    .line 564
    new-array v5, v3, [F

    .line 565
    .local v5, "result":[F
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 566
    aget v4, v5, v4

    invoke-static {v4}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v4

    iput v4, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMaxDozeBrightnessInt:I

    .line 567
    const/4 v4, 0x1

    aget v4, v5, v4

    invoke-static {v4}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToInt(F)I

    move-result v4

    iput v4, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMinDozeBrightnessInt:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 572
    .end local v2    # "b":Landroid/os/IBinder;
    .end local v3    # "length":I
    .end local v5    # "result":[F
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 569
    :catch_0
    move-exception v2

    .line 570
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 573
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 574
    nop

    .line 575
    return-void

    .line 572
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 573
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 574
    throw v2
.end method

.method public onBootPhase(I)V
    .locals 6
    .param p1, "phase"    # I

    .line 274
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_5

    .line 276
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_PAPERMODE_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110b001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeMinRate:I

    .line 280
    :cond_0
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    .line 281
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_paper_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 284
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_paper_mode_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 287
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_optimize_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 290
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_color_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 293
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_texture_color_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 296
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_paper_texture_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 299
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_auto_adjust"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 302
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_mode_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 305
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_game_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 308
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "miui_dkt_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 310
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Lcom/android/server/display/DisplayFeatureManagerService$UserSwitchReceiver-IA;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.USER_SWITCHED"

    invoke-direct {v2, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 312
    sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->SUPPORT_TRUETONE_MODE:Z

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "screen_true_tone"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mSettingsObserver:Lcom/android/server/display/DisplayFeatureManagerService$SettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 318
    :cond_1
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_PAPERMODE_ANIMATION:Z

    if-eqz v0, :cond_2

    .line 319
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$2;

    const-string v1, "papermode"

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$2;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Ljava/lang/String;)V

    .line 343
    .local v0, "paperMode":Landroid/util/IntProperty;, "Landroid/util/IntProperty<Lcom/android/server/display/DisplayFeatureManagerService;>;"
    new-instance v1, Lcom/android/server/display/MiuiRampAnimator;

    invoke-direct {v1, p0, v0}, Lcom/android/server/display/MiuiRampAnimator;-><init>(Ljava/lang/Object;Landroid/util/IntProperty;)V

    iput-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mPaperModeAnimator:Lcom/android/server/display/MiuiRampAnimator;

    .line 344
    new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$PaperModeAnimatListener;

    invoke-direct {v2, p0}, Lcom/android/server/display/DisplayFeatureManagerService$PaperModeAnimatListener;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    invoke-virtual {v1, v2}, Lcom/android/server/display/MiuiRampAnimator;->setListener(Lcom/android/server/display/MiuiRampAnimator$Listener;)V

    .line 347
    .end local v0    # "paperMode":Landroid/util/IntProperty;, "Landroid/util/IntProperty<Lcom/android/server/display/DisplayFeatureManagerService;>;"
    :cond_2
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_CALLBACK:Z

    if-eqz v0, :cond_4

    .line 348
    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_DISPLAYFEATURE_HIDL:Z

    if-eqz v0, :cond_3

    .line 349
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$3;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$3;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    invoke-virtual {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->registerCallback(Ljava/lang/Object;)V

    goto :goto_0

    .line 358
    :cond_3
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$4;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$4;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    invoke-virtual {p0, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->registerCallback(Ljava/lang/Object;)V

    .line 369
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 370
    invoke-virtual {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->loadDozeBrightnessThreshold()V

    goto :goto_1

    .line 371
    :cond_5
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_6

    .line 372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mBootCompleted:Z

    .line 373
    sget-boolean v0, Lcom/android/server/display/DisplayFeatureManagerService;->FPS_SWITCH_DEFAULT:Z

    if-nez v0, :cond_6

    .line 374
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->getScreenDpiMode()I

    move-result v0

    const/16 v1, 0x11

    invoke-direct {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->notifySFDfpsMode(II)V

    .line 377
    :cond_6
    :goto_1
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 266
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;

    invoke-direct {v0, p0}, Lcom/android/server/display/DisplayFeatureManagerService$LocalService;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;)V

    iput-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLocalService:Lcom/android/server/display/DisplayFeatureManagerInternal;

    .line 267
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mDisplayFeatureServiceImpl:Lcom/android/server/display/DisplayFeatureManagerServiceImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/display/DisplayFeatureManagerServiceImpl;->init(Lcom/android/server/display/DisplayFeatureManagerInternal;)V

    .line 268
    new-instance v0, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/DisplayFeatureManagerService$BinderService;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Lcom/android/server/display/DisplayFeatureManagerService$BinderService-IA;)V

    const-string v1, "displayfeature"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 269
    const-class v0, Lcom/android/server/display/DisplayFeatureManagerInternal;

    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLocalService:Lcom/android/server/display/DisplayFeatureManagerInternal;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/display/DisplayFeatureManagerService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 270
    return-void
.end method

.method public registerCallback(Ljava/lang/Object;)V
    .locals 3
    .param p1, "callback"    # Ljava/lang/Object;

    .line 621
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 622
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    if-nez v1, :cond_0

    .line 623
    invoke-direct {p0}, Lcom/android/server/display/DisplayFeatureManagerService;->initWrapperLocked()V

    .line 625
    :cond_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mWrapper:Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;

    if-eqz v1, :cond_1

    .line 626
    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Lcom/android/server/display/DisplayFeatureManagerService$DisplayFeatureManagerWrapper;->registerCallback(ILjava/lang/Object;)V

    .line 628
    :cond_1
    monitor-exit v0

    .line 629
    return-void

    .line 628
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected registerDeathCallbackLocked(Landroid/os/IBinder;I)V
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "flag"    # I

    .line 998
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 999
    const-string v0, "DisplayFeatureManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Client token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has already registered."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    return-void

    .line 1002
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1003
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    new-instance v2, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;-><init>(Lcom/android/server/display/DisplayFeatureManagerService;Landroid/os/IBinder;I)V

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1004
    monitor-exit v0

    .line 1005
    return-void

    .line 1004
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setDozeBrightness(JI)V
    .locals 4
    .param p1, "physicalDisplayId"    # J
    .param p3, "brightness"    # I

    .line 578
    move v0, p3

    .line 579
    .local v0, "value":I
    sget-boolean v1, Lcom/android/server/display/DisplayFeatureManagerService;->SUPPORT_MULTIPLE_AOD_BRIGHTNESS:Z

    if-nez v1, :cond_2

    .line 580
    sget-object v1, Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;->DOZE_TO_NORMAL:Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;

    .line 581
    .local v1, "modeId":Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;
    iget v2, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mMaxDozeBrightnessInt:I

    if-lt p3, v2, :cond_0

    .line 582
    sget-object v1, Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;->DOZE_BRIGHTNESS_HBM:Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;

    goto :goto_0

    .line 583
    :cond_0
    if-lez p3, :cond_1

    .line 584
    sget-object v1, Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;->DOZE_BRIGHTNESS_LBM:Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;

    .line 586
    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;->ordinal()I

    move-result v0

    .line 587
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setDozeBrightness, brightness: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DisplayFeatureManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    .end local v1    # "modeId":Lcom/android/server/display/DisplayFeatureManagerService$DozeBrightnessMode;
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/DisplayFeatureManagerService;->getEffectedDisplayIndex(J)I

    move-result v1

    .line 590
    .local v1, "index":I
    const/16 v2, 0x19

    const/16 v3, 0xff

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/android/server/display/DisplayFeatureManagerService;->setDisplayFeature(IIII)V

    .line 591
    return-void
.end method

.method public setRhythmicScreenEffect(IIII)V
    .locals 1
    .param p1, "screenModeType"    # I
    .param p2, "screenModeValue"    # I
    .param p3, "category"    # I
    .param p4, "time"    # I

    .line 1320
    shl-int/lit8 v0, p3, 0x10

    or-int/2addr v0, p4

    .line 1321
    .local v0, "cookie":I
    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/display/DisplayFeatureManagerService;->setScreenEffectInternal(III)V

    .line 1322
    return-void
.end method

.method protected unregisterDeathCallbackLocked(Landroid/os/IBinder;)V
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;

    .line 1008
    if-eqz p1, :cond_1

    .line 1009
    iget-object v0, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1010
    :try_start_0
    iget-object v1, p0, Lcom/android/server/display/DisplayFeatureManagerService;->mClientDeathCallbacks:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;

    .line 1011
    .local v1, "deathCallback":Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;
    if-eqz v1, :cond_0

    .line 1012
    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 1014
    .end local v1    # "deathCallback":Lcom/android/server/display/DisplayFeatureManagerService$ClientDeathCallback;
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1016
    :cond_1
    :goto_0
    return-void
.end method
