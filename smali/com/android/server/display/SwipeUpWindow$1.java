class com.android.server.display.SwipeUpWindow$1 extends com.android.server.display.animation.FloatPropertyCompat {
	 /* .source "SwipeUpWindow.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/display/SwipeUpWindow;->initGradientShadowAnimation()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/android/server/display/animation/FloatPropertyCompat<", */
/* "Lcom/android/server/display/SwipeUpWindow$AnimationState;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
final com.android.server.display.SwipeUpWindow this$0; //synthetic
/* # direct methods */
 com.android.server.display.SwipeUpWindow$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/display/SwipeUpWindow; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 288 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Lcom/android/server/display/animation/FloatPropertyCompat;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( com.android.server.display.SwipeUpWindow$AnimationState p0 ) {
/* .locals 1 */
/* .param p1, "object" # Lcom/android/server/display/SwipeUpWindow$AnimationState; */
/* .line 291 */
v0 = (( com.android.server.display.SwipeUpWindow$AnimationState ) p1 ).getPerState ( ); // invoke-virtual {p1}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->getPerState()F
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 288 */
/* check-cast p1, Lcom/android/server/display/SwipeUpWindow$AnimationState; */
p1 = (( com.android.server.display.SwipeUpWindow$1 ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/display/SwipeUpWindow$1;->getValue(Lcom/android/server/display/SwipeUpWindow$AnimationState;)F
} // .end method
public void setValue ( com.android.server.display.SwipeUpWindow$AnimationState p0, Float p1 ) {
/* .locals 0 */
/* .param p1, "object" # Lcom/android/server/display/SwipeUpWindow$AnimationState; */
/* .param p2, "value" # F */
/* .line 296 */
(( com.android.server.display.SwipeUpWindow$AnimationState ) p1 ).setPerState ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/display/SwipeUpWindow$AnimationState;->setPerState(F)V
/* .line 297 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 288 */
/* check-cast p1, Lcom/android/server/display/SwipeUpWindow$AnimationState; */
(( com.android.server.display.SwipeUpWindow$1 ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/display/SwipeUpWindow$1;->setValue(Lcom/android/server/display/SwipeUpWindow$AnimationState;F)V
return;
} // .end method
