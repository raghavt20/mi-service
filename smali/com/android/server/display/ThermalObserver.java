public class com.android.server.display.ThermalObserver {
	 /* .source "ThermalObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/display/ThermalObserver$TemperatureControllerHandler;, */
	 /* Lcom/android/server/display/ThermalObserver$TemperatureObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static Boolean DEBUG;
private static final Float DISABLE_GALLERY_HDR_TEMPERATURE;
private static final Float ENABLE_GALLERY_TEMPERATURE;
private static final Integer MSG_UPDATE_TEMP;
private static final java.lang.String TAG;
private static final java.lang.String TEMPERATURE_PATH;
private static final Boolean mSupportGalleryHDR;
/* # instance fields */
private Float mCurrentTemperature;
private com.android.server.display.DisplayPowerControllerImpl mDisplayPowerControllerImpl;
private com.android.server.display.ThermalObserver$TemperatureControllerHandler mHandler;
private com.android.server.display.ThermalObserver$TemperatureObserver mTemperatureObserver;
/* # direct methods */
static com.android.server.display.ThermalObserver$TemperatureControllerHandler -$$Nest$fgetmHandler ( com.android.server.display.ThermalObserver p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$mupdateTemperature ( com.android.server.display.ThermalObserver p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/display/ThermalObserver;->updateTemperature()V */
	 return;
} // .end method
static com.android.server.display.ThermalObserver ( ) {
	 /* .locals 2 */
	 /* .line 28 */
	 /* const-string/jumbo v0, "support_gallery_hdr" */
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 miui.util.FeatureParser .getBoolean ( v0,v1 );
	 com.android.server.display.ThermalObserver.mSupportGalleryHDR = (v0!= 0);
	 return;
} // .end method
public com.android.server.display.ThermalObserver ( ) {
	 /* .locals 3 */
	 /* .param p1, "looper" # Landroid/os/Looper; */
	 /* .param p2, "impl" # Lcom/android/server/display/DisplayPowerControllerImpl; */
	 /* .line 38 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 32 */
	 /* const/high16 v0, -0x40800000 # -1.0f */
	 /* iput v0, p0, Lcom/android/server/display/ThermalObserver;->mCurrentTemperature:F */
	 /* .line 39 */
	 /* new-instance v0, Lcom/android/server/display/ThermalObserver$TemperatureControllerHandler; */
	 /* invoke-direct {v0, p0, p1}, Lcom/android/server/display/ThermalObserver$TemperatureControllerHandler;-><init>(Lcom/android/server/display/ThermalObserver;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 40 */
	 this.mDisplayPowerControllerImpl = p2;
	 /* .line 41 */
	 /* new-instance v0, Lcom/android/server/display/ThermalObserver$TemperatureObserver; */
	 /* new-instance v1, Ljava/io/File; */
	 final String v2 = "/sys/class/thermal/thermal_message/board_sensor_temp"; // const-string v2, "/sys/class/thermal/thermal_message/board_sensor_temp"
	 /* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/display/ThermalObserver$TemperatureObserver;-><init>(Lcom/android/server/display/ThermalObserver;Ljava/io/File;)V */
	 this.mTemperatureObserver = v0;
	 /* .line 42 */
	 /* sget-boolean v1, Lcom/android/server/display/ThermalObserver;->mSupportGalleryHDR:Z */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 43 */
		 (( com.android.server.display.ThermalObserver$TemperatureObserver ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/display/ThermalObserver$TemperatureObserver;->startWatching()V
		 /* .line 45 */
	 } // :cond_0
	 return;
} // .end method
private Float getBoardTemperature ( ) {
	 /* .locals 5 */
	 /* .line 118 */
	 final String v0 = "ThermalObserver"; // const-string v0, "ThermalObserver"
	 final String v1 = "/sys/class/thermal/thermal_message/board_sensor_temp"; // const-string v1, "/sys/class/thermal/thermal_message/board_sensor_temp"
	 com.android.server.display.ThermalObserver .readSysNodeInfo ( v1 );
	 /* .line 120 */
	 /* .local v1, "node":Ljava/lang/String; */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 121 */
		 try { // :try_start_0
			 /* sget-boolean v2, Lcom/android/server/display/ThermalObserver;->DEBUG:Z */
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 /* .line 122 */
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "read BoardTemp node: "; // const-string v3, "read BoardTemp node: "
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v3 = 				 java.lang.Float .parseFloat ( v1 );
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .d ( v0,v2 );
				 /* .line 124 */
			 } // :cond_0
			 v0 = 			 java.lang.Float .parseFloat ( v1 );
			 /* :try_end_0 */
			 /* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 126 */
			 /* :catch_0 */
			 /* move-exception v2 */
			 /* .line 127 */
			 /* .local v2, "e":Ljava/lang/NumberFormatException; */
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v4 = "read BoardTemp error node: "; // const-string v4, "read BoardTemp error node: "
			 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .e ( v0,v3 );
			 /* .line 128 */
		 } // .end local v2 # "e":Ljava/lang/NumberFormatException;
	 } // :cond_1
	 /* nop */
	 /* .line 129 */
} // :goto_0
/* const/high16 v0, -0x40800000 # -1.0f */
} // .end method
private static java.lang.String readSysNodeInfo ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p0, "nodePath" # Ljava/lang/String; */
/* .line 92 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 93 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_2
	 (( java.io.File ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/io/File;->length()J
	 /* move-result-wide v1 */
	 /* const-wide/16 v3, 0x0 */
	 /* cmp-long v1, v1, v3 */
	 /* if-gtz v1, :cond_0 */
	 /* .line 97 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 99 */
/* .local v1, "info":Ljava/lang/StringBuilder; */
try { // :try_start_0
	 /* new-instance v2, Ljava/io/BufferedReader; */
	 /* new-instance v3, Ljava/io/FileReader; */
	 /* invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
	 /* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 100 */
	 /* .local v2, "reader":Ljava/io/BufferedReader; */
	 try { // :try_start_1
		 (( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
		 /* .line 101 */
		 /* .local v3, "temp":Ljava/lang/String; */
	 } // :goto_0
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 102 */
		 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 103 */
		 (( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
		 /* move-object v3, v4 */
		 /* .line 104 */
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 105 */
			 final String v4 = "\n"; // const-string v4, "\n"
			 (( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 110 */
		 } // :cond_1
		 try { // :try_start_2
			 (( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
			 /* :try_end_2 */
			 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
			 /* .line 112 */
		 } // .end local v2 # "reader":Ljava/io/BufferedReader;
		 /* .line 99 */
	 } // .end local v3 # "temp":Ljava/lang/String;
	 /* .restart local v2 # "reader":Ljava/io/BufferedReader; */
	 /* :catchall_0 */
	 /* move-exception v3 */
	 try { // :try_start_3
		 (( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
		 /* :try_end_3 */
		 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
		 /* :catchall_1 */
		 /* move-exception v4 */
		 try { // :try_start_4
			 (( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
		 } // .end local v0 # "file":Ljava/io/File;
	 } // .end local v1 # "info":Ljava/lang/StringBuilder;
} // .end local p0 # "nodePath":Ljava/lang/String;
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 110 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local v1 # "info":Ljava/lang/StringBuilder; */
/* .restart local p0 # "nodePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 113 */
} // :goto_2
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 94 */
} // .end local v1 # "info":Ljava/lang/StringBuilder;
} // :cond_2
} // :goto_3
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void updateTemperature ( ) {
/* .locals 3 */
/* .line 80 */
v0 = /* invoke-direct {p0}, Lcom/android/server/display/ThermalObserver;->getBoardTemperature()F */
/* iput v0, p0, Lcom/android/server/display/ThermalObserver;->mCurrentTemperature:F */
/* .line 81 */
/* const v1, 0x47108800 # 37000.0f */
/* cmpl-float v1, v0, v1 */
final String v2 = "ThermalObserver"; // const-string v2, "ThermalObserver"
/* if-ltz v1, :cond_0 */
/* .line 82 */
final String v0 = "Gallery hdr is disable by thermal."; // const-string v0, "Gallery hdr is disable by thermal."
android.util.Slog .i ( v2,v0 );
/* .line 83 */
v0 = this.mDisplayPowerControllerImpl;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).updateGalleryHdrThermalThrottler ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGalleryHdrThermalThrottler(Z)V
/* .line 84 */
} // :cond_0
/* const v1, 0x4708b800 # 35000.0f */
/* cmpg-float v0, v0, v1 */
/* if-gtz v0, :cond_1 */
/* .line 85 */
final String v0 = "Temperature control has been released."; // const-string v0, "Temperature control has been released."
android.util.Slog .i ( v2,v0 );
/* .line 86 */
v0 = this.mDisplayPowerControllerImpl;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.display.DisplayPowerControllerImpl ) v0 ).updateGalleryHdrThermalThrottler ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/display/DisplayPowerControllerImpl;->updateGalleryHdrThermalThrottler(Z)V
/* .line 88 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 133 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 134 */
final String v0 = "Thermal Observer State:"; // const-string v0, "Thermal Observer State:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 135 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " mCurrentTemperature="; // const-string v1, " mCurrentTemperature="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/display/ThermalObserver;->mCurrentTemperature:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 136 */
/* sget-boolean v0, Lcom/android/server/display/DisplayDebugConfig;->DEBUG_DMS:Z */
com.android.server.display.ThermalObserver.DEBUG = (v0!= 0);
/* .line 137 */
return;
} // .end method
