.class public Lcom/android/server/display/OutdoorDetector;
.super Ljava/lang/Object;
.source "OutdoorDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/OutdoorDetector$MyHandler;
    }
.end annotation


# static fields
.field private static final DEBOUNCE_ENTER_OUTDOOR_DURATION:I = 0xbb8

.field private static final DEBOUNCE_EXIT_OUTDOOR_DURATION:I = 0xbb8

.field private static DEBUG:Z = false

.field private static final MSG_UPDATE_OUTDOOR_STATE:I = 0x1

.field private static final OUTDOOR_AMBIENT_LIGHT_HORIZON:I = 0xbb8

.field private static final TAG:Ljava/lang/String; = "ThermalBrightnessController.OutdoorDetector"


# instance fields
.field private mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

.field private mCurrentAmbientLux:F

.field private mHandler:Landroid/os/Handler;

.field private mIsInOutDoor:Z

.field private mLastObservedLux:F

.field private mLastObservedLuxTime:J

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mSensorEnabled:Z

.field private mSensorListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

.field private final mThresholdLuxEnterOutdoor:F


# direct methods
.method static bridge synthetic -$$Nest$mhandleLightSensorEvent(Lcom/android/server/display/OutdoorDetector;JF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/OutdoorDetector;->handleLightSensorEvent(JF)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAmbientLux(Lcom/android/server/display/OutdoorDetector;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/OutdoorDetector;->updateAmbientLux()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 19
    sget-boolean v0, Lcom/android/server/display/ThermalBrightnessController;->DEBUG:Z

    sput-boolean v0, Lcom/android/server/display/OutdoorDetector;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/display/ThermalBrightnessController;F)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "thermalController"    # Lcom/android/server/display/ThermalBrightnessController;
    .param p3, "threshold"    # F

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/android/server/display/OutdoorDetector$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/OutdoorDetector$1;-><init>(Lcom/android/server/display/OutdoorDetector;)V

    iput-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mSensorListener:Landroid/hardware/SensorEventListener;

    .line 43
    const-class v0, Landroid/hardware/SensorManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mSensorManager:Landroid/hardware/SensorManager;

    .line 44
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mLightSensor:Landroid/hardware/Sensor;

    .line 45
    new-instance v0, Lcom/android/server/display/OutdoorDetector$MyHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/display/OutdoorDetector$MyHandler;-><init>(Lcom/android/server/display/OutdoorDetector;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mHandler:Landroid/os/Handler;

    .line 46
    new-instance v0, Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v1, 0xfa

    const/16 v3, 0xbb8

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/display/AmbientLightRingBuffer;-><init>(JI)V

    iput-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    .line 47
    iput-object p2, p0, Lcom/android/server/display/OutdoorDetector;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    .line 48
    iput p3, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F

    .line 49
    return-void
.end method

.method private applyLightSensorMeasurement(JF)V
    .locals 3
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .line 90
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v1, 0xbb8

    sub-long v1, p1, v1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V

    .line 91
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/display/AmbientLightRingBuffer;->push(JF)V

    .line 93
    iput p3, p0, Lcom/android/server/display/OutdoorDetector;->mLastObservedLux:F

    .line 94
    iput-wide p1, p0, Lcom/android/server/display/OutdoorDetector;->mLastObservedLuxTime:J

    .line 95
    return-void
.end method

.method private handleLightSensorEvent(JF)V
    .locals 2
    .param p1, "time"    # J
    .param p3, "lux"    # F

    .line 80
    sget-boolean v0, Lcom/android/server/display/OutdoorDetector;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleLightSensorEvent: lux = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ThermalBrightnessController.OutdoorDetector"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/display/OutdoorDetector;->applyLightSensorMeasurement(JF)V

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/OutdoorDetector;->updateAmbientLux(J)V

    .line 86
    return-void
.end method

.method private nextEnterOutdoorModeTransition(J)J
    .locals 6
    .param p1, "time"    # J

    .line 140
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    .line 141
    .local v0, "N":I
    move-wide v1, p1

    .line 142
    .local v1, "earliestValidTime":J
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 143
    iget-object v4, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    iget v5, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 144
    goto :goto_1

    .line 146
    :cond_0
    iget-object v4, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v1

    .line 142
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 148
    .end local v3    # "i":I
    :cond_1
    :goto_1
    const-wide/16 v3, 0xbb8

    add-long/2addr v3, v1

    return-wide v3
.end method

.method private nextExitOutdoorModeTransition(J)J
    .locals 6
    .param p1, "time"    # J

    .line 152
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v0}, Lcom/android/server/display/AmbientLightRingBuffer;->size()I

    move-result v0

    .line 153
    .local v0, "N":I
    move-wide v1, p1

    .line 154
    .local v1, "earliestValidTime":J
    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 155
    iget-object v4, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getLux(I)F

    move-result v4

    iget v5, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_0

    .line 156
    goto :goto_1

    .line 158
    :cond_0
    iget-object v4, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    invoke-virtual {v4, v3}, Lcom/android/server/display/AmbientLightRingBuffer;->getTime(I)J

    move-result-wide v1

    .line 154
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 160
    .end local v3    # "i":I
    :cond_1
    :goto_1
    const-wide/16 v3, 0xbb8

    add-long/2addr v3, v1

    return-wide v3
.end method

.method private setOutdoorActive(Z)V
    .locals 2
    .param p1, "active"    # Z

    .line 128
    iget-boolean v0, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z

    if-eq p1, v0, :cond_0

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setOutDoorActive: active: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ThermalBrightnessController.OutdoorDetector"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iput-boolean p1, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z

    .line 131
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mThermalBrightnessController:Lcom/android/server/display/ThermalBrightnessController;

    invoke-virtual {v0}, Lcom/android/server/display/ThermalBrightnessController;->outDoorStateChanged()V

    .line 133
    :cond_0
    return-void
.end method

.method private updateAmbientLux()V
    .locals 5

    .line 99
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 100
    .local v0, "time":J
    iget-object v2, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v3, 0xbb8

    sub-long v3, v0, v3

    invoke-virtual {v2, v3, v4}, Lcom/android/server/display/AmbientLightRingBuffer;->prune(J)V

    .line 101
    invoke-direct {p0, v0, v1}, Lcom/android/server/display/OutdoorDetector;->updateAmbientLux(J)V

    .line 102
    return-void
.end method

.method private updateAmbientLux(J)V
    .locals 10
    .param p1, "time"    # J

    .line 105
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mAmbientLightRingBuffer:Lcom/android/server/display/AmbientLightRingBuffer;

    const-wide/16 v1, 0xbb8

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/android/server/display/AmbientLightRingBuffer;->calculateAmbientLux(JJ)F

    move-result v0

    iput v0, p0, Lcom/android/server/display/OutdoorDetector;->mCurrentAmbientLux:F

    .line 106
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/OutdoorDetector;->nextEnterOutdoorModeTransition(J)J

    move-result-wide v0

    .line 107
    .local v0, "nextEnterOutdoorModeTime":J
    invoke-direct {p0, p1, p2}, Lcom/android/server/display/OutdoorDetector;->nextExitOutdoorModeTransition(J)J

    move-result-wide v2

    .line 109
    .local v2, "nextExitOutdoorModeTime":J
    iget v4, p0, Lcom/android/server/display/OutdoorDetector;->mCurrentAmbientLux:F

    iget v5, p0, Lcom/android/server/display/OutdoorDetector;->mThresholdLuxEnterOutdoor:F

    cmpl-float v6, v4, v5

    const/4 v7, 0x1

    if-ltz v6, :cond_0

    cmp-long v6, v0, p1

    if-gtz v6, :cond_0

    iget-boolean v6, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z

    if-nez v6, :cond_0

    .line 112
    invoke-direct {p0, v7}, Lcom/android/server/display/OutdoorDetector;->setOutdoorActive(Z)V

    goto :goto_0

    .line 113
    :cond_0
    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    cmp-long v4, v2, p1

    if-gtz v4, :cond_1

    .line 115
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/android/server/display/OutdoorDetector;->setOutdoorActive(Z)V

    .line 117
    :cond_1
    :goto_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 118
    .local v4, "nextTransitionTime":J
    cmp-long v6, v4, p1

    if-lez v6, :cond_2

    move-wide v8, v4

    goto :goto_1

    :cond_2
    const-wide/16 v8, 0x3e8

    add-long/2addr v8, p1

    :goto_1
    move-wide v4, v8

    .line 119
    sget-boolean v6, Lcom/android/server/display/OutdoorDetector;->DEBUG:Z

    if-eqz v6, :cond_3

    .line 120
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "updateAmbientLux: Scheduling lux update for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 121
    invoke-static {v4, v5}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", mAmbientLightRingBuffer: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 120
    const-string v8, "ThermalBrightnessController.OutdoorDetector"

    invoke-static {v8, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_3
    iget-object v6, p0, Lcom/android/server/display/OutdoorDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v7, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 125
    return-void
.end method


# virtual methods
.method protected detect(Z)Z
    .locals 5
    .param p1, "enable"    # Z

    .line 52
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z

    if-nez v2, :cond_0

    .line 53
    iput-boolean v0, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z

    .line 54
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/OutdoorDetector;->mSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/android/server/display/OutdoorDetector;->mLightSensor:Landroid/hardware/Sensor;

    iget-object v4, p0, Lcom/android/server/display/OutdoorDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    goto :goto_0

    .line 56
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v2, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z

    if-eqz v2, :cond_1

    .line 57
    iput-boolean v1, p0, Lcom/android/server/display/OutdoorDetector;->mSensorEnabled:Z

    .line 58
    iget-object v2, p0, Lcom/android/server/display/OutdoorDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 59
    iget-object v0, p0, Lcom/android/server/display/OutdoorDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/display/OutdoorDetector;->mSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 60
    invoke-direct {p0, v1}, Lcom/android/server/display/OutdoorDetector;->setOutdoorActive(Z)V

    .line 62
    :cond_1
    :goto_0
    return v1
.end method

.method protected isOutdoorState()Z
    .locals 1

    .line 136
    iget-boolean v0, p0, Lcom/android/server/display/OutdoorDetector;->mIsInOutDoor:Z

    return v0
.end method
