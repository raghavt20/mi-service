.class Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;
.super Ljava/lang/Object;
.source "AutomaticBrightnessControllerImpl.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/display/AutomaticBrightnessControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;


# direct methods
.method constructor <init>(Lcom/android/server/display/AutomaticBrightnessControllerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    .line 382
    iput-object p1, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 400
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 385
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 390
    :sswitch_0
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$monNonUiSensorChanged(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/SensorEvent;)V

    .line 391
    goto :goto_0

    .line 387
    :sswitch_1
    iget-object v0, p0, Lcom/android/server/display/AutomaticBrightnessControllerImpl$2;->this$0:Lcom/android/server/display/AutomaticBrightnessControllerImpl;

    invoke-static {v0, p1}, Lcom/android/server/display/AutomaticBrightnessControllerImpl;->-$$Nest$monProximitySensorChanged(Lcom/android/server/display/AutomaticBrightnessControllerImpl;Landroid/hardware/SensorEvent;)V

    .line 388
    nop

    .line 395
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x1fa2653 -> :sswitch_0
    .end sparse-switch
.end method
