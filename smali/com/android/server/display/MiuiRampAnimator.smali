.class final Lcom/android/server/display/MiuiRampAnimator;
.super Ljava/lang/Object;
.source "MiuiRampAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/display/MiuiRampAnimator$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mAnimatedValue:F

.field private mAnimating:Z

.field private final mAnimationCallback:Ljava/lang/Runnable;

.field private final mChoreographer:Landroid/view/Choreographer;

.field private mCurrentValue:I

.field private mFirstTime:Z

.field private mLastFrameTimeNanos:J

.field private mListener:Lcom/android/server/display/MiuiRampAnimator$Listener;

.field private final mObject:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mProperty:Landroid/util/IntProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/IntProperty<",
            "TT;>;"
        }
    .end annotation
.end field

.field private mRate:I

.field private mTargetValue:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmAnimatedValue(Lcom/android/server/display/MiuiRampAnimator;)F
    .locals 0

    iget p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimatedValue:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmChoreographer(Lcom/android/server/display/MiuiRampAnimator;)Landroid/view/Choreographer;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mChoreographer:Landroid/view/Choreographer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentValue(Lcom/android/server/display/MiuiRampAnimator;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mCurrentValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastFrameTimeNanos(Lcom/android/server/display/MiuiRampAnimator;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mLastFrameTimeNanos:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmListener(Lcom/android/server/display/MiuiRampAnimator;)Lcom/android/server/display/MiuiRampAnimator$Listener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mListener:Lcom/android/server/display/MiuiRampAnimator$Listener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmObject(Lcom/android/server/display/MiuiRampAnimator;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mObject:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProperty(Lcom/android/server/display/MiuiRampAnimator;)Landroid/util/IntProperty;
    .locals 0

    iget-object p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mProperty:Landroid/util/IntProperty;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRate(Lcom/android/server/display/MiuiRampAnimator;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mRate:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTargetValue(Lcom/android/server/display/MiuiRampAnimator;)I
    .locals 0

    iget p0, p0, Lcom/android/server/display/MiuiRampAnimator;->mTargetValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAnimatedValue(Lcom/android/server/display/MiuiRampAnimator;F)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimatedValue:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAnimating(Lcom/android/server/display/MiuiRampAnimator;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimating:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentValue(Lcom/android/server/display/MiuiRampAnimator;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mCurrentValue:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastFrameTimeNanos(Lcom/android/server/display/MiuiRampAnimator;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mLastFrameTimeNanos:J

    return-void
.end method

.method static bridge synthetic -$$Nest$mpostAnimationCallback(Lcom/android/server/display/MiuiRampAnimator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/display/MiuiRampAnimator;->postAnimationCallback()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Landroid/util/IntProperty;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/util/IntProperty<",
            "TT;>;)V"
        }
    .end annotation

    .line 44
    .local p0, "this":Lcom/android/server/display/MiuiRampAnimator;, "Lcom/android/server/display/MiuiRampAnimator<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TT;"
    .local p2, "property":Landroid/util/IntProperty;, "Landroid/util/IntProperty<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mFirstTime:Z

    .line 131
    new-instance v0, Lcom/android/server/display/MiuiRampAnimator$1;

    invoke-direct {v0, p0}, Lcom/android/server/display/MiuiRampAnimator$1;-><init>(Lcom/android/server/display/MiuiRampAnimator;)V

    iput-object v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimationCallback:Ljava/lang/Runnable;

    .line 45
    iput-object p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mObject:Ljava/lang/Object;

    .line 46
    iput-object p2, p0, Lcom/android/server/display/MiuiRampAnimator;->mProperty:Landroid/util/IntProperty;

    .line 47
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mChoreographer:Landroid/view/Choreographer;

    .line 48
    return-void
.end method

.method private cancelAnimationCallback()V
    .locals 4

    .line 128
    .local p0, "this":Lcom/android/server/display/MiuiRampAnimator;, "Lcom/android/server/display/MiuiRampAnimator<TT;>;"
    iget-object v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mChoreographer:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimationCallback:Ljava/lang/Runnable;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/Choreographer;->removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V

    .line 129
    return-void
.end method

.method private postAnimationCallback()V
    .locals 4

    .line 124
    .local p0, "this":Lcom/android/server/display/MiuiRampAnimator;, "Lcom/android/server/display/MiuiRampAnimator<TT;>;"
    iget-object v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mChoreographer:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimationCallback:Ljava/lang/Runnable;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    .line 125
    return-void
.end method


# virtual methods
.method public animateTo(II)Z
    .locals 5
    .param p1, "target"    # I
    .param p2, "rate"    # I

    .line 62
    .local p0, "this":Lcom/android/server/display/MiuiRampAnimator;, "Lcom/android/server/display/MiuiRampAnimator<TT;>;"
    iget-boolean v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mFirstTime:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_6

    if-gtz p2, :cond_0

    goto :goto_0

    .line 88
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimating:Z

    if-eqz v0, :cond_2

    iget v3, p0, Lcom/android/server/display/MiuiRampAnimator;->mRate:I

    if-gt p2, v3, :cond_2

    iget v3, p0, Lcom/android/server/display/MiuiRampAnimator;->mCurrentValue:I

    if-gt p1, v3, :cond_1

    iget v4, p0, Lcom/android/server/display/MiuiRampAnimator;->mTargetValue:I

    if-le v3, v4, :cond_2

    :cond_1
    iget v4, p0, Lcom/android/server/display/MiuiRampAnimator;->mTargetValue:I

    if-gt v4, v3, :cond_3

    if-gt v3, p1, :cond_3

    .line 92
    :cond_2
    iput p2, p0, Lcom/android/server/display/MiuiRampAnimator;->mRate:I

    .line 95
    :cond_3
    iget v3, p0, Lcom/android/server/display/MiuiRampAnimator;->mTargetValue:I

    if-eq v3, p1, :cond_4

    move v2, v1

    .line 96
    .local v2, "changed":Z
    :cond_4
    iput p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mTargetValue:I

    .line 99
    if-nez v0, :cond_5

    iget v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mCurrentValue:I

    if-eq p1, v0, :cond_5

    .line 100
    iput-boolean v1, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimating:Z

    .line 101
    int-to-float v0, v0

    iput v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimatedValue:F

    .line 102
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mLastFrameTimeNanos:J

    .line 103
    invoke-direct {p0}, Lcom/android/server/display/MiuiRampAnimator;->postAnimationCallback()V

    .line 106
    :cond_5
    return v2

    .line 63
    .end local v2    # "changed":Z
    :cond_6
    :goto_0
    if-nez v0, :cond_8

    iget v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mCurrentValue:I

    if-eq p1, v0, :cond_7

    goto :goto_1

    .line 78
    :cond_7
    return v2

    .line 64
    :cond_8
    :goto_1
    iput-boolean v2, p0, Lcom/android/server/display/MiuiRampAnimator;->mFirstTime:Z

    .line 65
    iput v2, p0, Lcom/android/server/display/MiuiRampAnimator;->mRate:I

    .line 66
    iput p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mTargetValue:I

    .line 67
    iput p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mCurrentValue:I

    .line 68
    iget-object v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mProperty:Landroid/util/IntProperty;

    iget-object v3, p0, Lcom/android/server/display/MiuiRampAnimator;->mObject:Ljava/lang/Object;

    invoke-virtual {v0, v3, p1}, Landroid/util/IntProperty;->setValue(Ljava/lang/Object;I)V

    .line 69
    iget-boolean v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimating:Z

    if-eqz v0, :cond_9

    .line 70
    iput-boolean v2, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimating:Z

    .line 71
    invoke-direct {p0}, Lcom/android/server/display/MiuiRampAnimator;->cancelAnimationCallback()V

    .line 73
    :cond_9
    iget-object v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mListener:Lcom/android/server/display/MiuiRampAnimator$Listener;

    if-eqz v0, :cond_a

    .line 74
    invoke-interface {v0}, Lcom/android/server/display/MiuiRampAnimator$Listener;->onAnimationEnd()V

    .line 76
    :cond_a
    return v1
.end method

.method public isAnimating()Z
    .locals 1

    .line 113
    .local p0, "this":Lcom/android/server/display/MiuiRampAnimator;, "Lcom/android/server/display/MiuiRampAnimator<TT;>;"
    iget-boolean v0, p0, Lcom/android/server/display/MiuiRampAnimator;->mAnimating:Z

    return v0
.end method

.method public setListener(Lcom/android/server/display/MiuiRampAnimator$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/server/display/MiuiRampAnimator$Listener;

    .line 120
    .local p0, "this":Lcom/android/server/display/MiuiRampAnimator;, "Lcom/android/server/display/MiuiRampAnimator<TT;>;"
    iput-object p1, p0, Lcom/android/server/display/MiuiRampAnimator;->mListener:Lcom/android/server/display/MiuiRampAnimator$Listener;

    .line 121
    return-void
.end method
