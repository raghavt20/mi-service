.class public Lcom/android/server/testharness/TestHarnessModeServiceImpl;
.super Lcom/android/server/testharness/TestHarnessModeServiceStub;
.source "TestHarnessModeServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.testharness.TestHarnessModeServiceStub$$"
.end annotation


# static fields
.field private static final CMD_DISABLE_MITS:Ljava/lang/String; = "disable_mits"

.field private static final CMD_ENABLE_MITS:Ljava/lang/String; = "enable_mits"

.field private static final CMD_HAS_MITS:Ljava/lang/String; = "has_mits"

.field private static final MITS_ENABLED:Ljava/lang/String; = "MITS_ENABLED"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mPersistentDataBlockManagerInternal:Lcom/android/server/PersistentDataBlockManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    const-class v0, Lcom/android/server/testharness/TestHarnessModeServiceImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceStub;-><init>()V

    return-void
.end method

.method private checkPermissions(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 56
    const-string v0, "android.permission.ENABLE_TEST_HARNESS_MODE"

    const-string v1, "You must hold android.permission.ENABLE_TEST_HARNESS_MODE to enable Test Harness Mode"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method private getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->mPersistentDataBlockManagerInternal:Lcom/android/server/PersistentDataBlockManagerInternal;

    if-nez v0, :cond_0

    .line 37
    sget-object v0, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->TAG:Ljava/lang/String;

    const-string v1, "Getting PersistentDataBlockManagerInternal from LocalServices"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-class v0, Lcom/android/server/PersistentDataBlockManagerInternal;

    .line 39
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/PersistentDataBlockManagerInternal;

    iput-object v0, p0, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->mPersistentDataBlockManagerInternal:Lcom/android/server/PersistentDataBlockManagerInternal;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->mPersistentDataBlockManagerInternal:Lcom/android/server/PersistentDataBlockManagerInternal;

    return-object v0
.end method

.method private isDeviceProvisioned(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    const/4 v0, 0x1

    .line 46
    .local v0, "provisioned":Z
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 48
    .local v1, "cr":Landroid/content/ContentResolver;
    :try_start_0
    const-string v2, "device_provisioned"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    move v0, v3

    .line 51
    goto :goto_1

    .line 49
    :catch_0
    move-exception v2

    .line 50
    .local v2, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 52
    .end local v2    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :goto_1
    return v0
.end method


# virtual methods
.method public isMITSModeEnabled()Z
    .locals 3

    .line 64
    invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal;

    move-result-object v0

    .line 65
    .local v0, "blockManager":Lcom/android/server/PersistentDataBlockManagerInternal;
    if-nez v0, :cond_0

    .line 66
    sget-object v1, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get MITS Mode; no implementation of PersistentDataBlockManagerInternal was bound!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/4 v1, 0x0

    return v1

    .line 70
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-interface {v0}, Lcom/android/server/PersistentDataBlockManagerInternal;->getMITSModeData()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    const-string v2, "MITS_ENABLED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public isMitsCommand(Ljava/lang/String;)Z
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;

    .line 87
    const-string v0, "enable_mits"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "disable_mits"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    const-string v0, "has_mits"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 87
    :goto_1
    return v0
.end method

.method public onMitsCommand(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/content/Context;)I
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "printWriter"    # Ljava/io/PrintWriter;
    .param p3, "context"    # Landroid/content/Context;

    .line 94
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "has_mits"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v0, "disable_mits"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v0, "enable_mits"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 124
    return v3

    .line 117
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    const-string/jumbo v0, "yes"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 119
    return v3

    .line 121
    :cond_1
    const-string v0, "no"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 122
    return v2

    .line 109
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/PersistentDataBlockManagerInternal;->clearMITSModeData()V

    .line 111
    const-string v0, "MITS disabled"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 112
    return v3

    .line 114
    :cond_2
    const-string v0, "MITS already disabled"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 115
    return v2

    .line 96
    :pswitch_2
    const-string v0, "ro.debuggable"

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_3

    .line 97
    const-string v0, "Forbid to enable Mits Mode."

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 98
    return v1

    .line 100
    :cond_3
    invoke-direct {p0, p3}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->checkPermissions(Landroid/content/Context;)V

    .line 101
    invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 102
    invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal;

    move-result-object v0

    const-string v1, "MITS_ENABLED"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/PersistentDataBlockManagerInternal;->setMITSModeData([B)V

    .line 103
    const-string v0, "MITS enabled"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 104
    return v3

    .line 106
    :cond_4
    const-string v0, "MITS already enabled"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 107
    return v2

    :sswitch_data_0
    .sparse-switch
        -0x552d4da9 -> :sswitch_2
        -0x3ac6b3ee -> :sswitch_1
        0x861dba0 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setUpMITSMode(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 75
    invoke-direct {p0, p1}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isDeviceProvisioned(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    return-void

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 81
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "adb_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 83
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    return-void
.end method
