public class com.android.server.testharness.TestHarnessModeServiceImpl extends com.android.server.testharness.TestHarnessModeServiceStub {
	 /* .source "TestHarnessModeServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.testharness.TestHarnessModeServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String CMD_DISABLE_MITS;
private static final java.lang.String CMD_ENABLE_MITS;
private static final java.lang.String CMD_HAS_MITS;
private static final java.lang.String MITS_ENABLED;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.PersistentDataBlockManagerInternal mPersistentDataBlockManagerInternal;
/* # direct methods */
static com.android.server.testharness.TestHarnessModeServiceImpl ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 /* const-class v0, Lcom/android/server/testharness/TestHarnessModeServiceImpl; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
public com.android.server.testharness.TestHarnessModeServiceImpl ( ) {
	 /* .locals 0 */
	 /* .line 23 */
	 /* invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceStub;-><init>()V */
	 return;
} // .end method
private void checkPermissions ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 56 */
	 final String v0 = "android.permission.ENABLE_TEST_HARNESS_MODE"; // const-string v0, "android.permission.ENABLE_TEST_HARNESS_MODE"
	 final String v1 = "You must hold android.permission.ENABLE_TEST_HARNESS_MODE to enable Test Harness Mode"; // const-string v1, "You must hold android.permission.ENABLE_TEST_HARNESS_MODE to enable Test Harness Mode"
	 (( android.content.Context ) p1 ).enforceCallingPermission ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 60 */
	 return;
} // .end method
private com.android.server.PersistentDataBlockManagerInternal getPersistentDataBlock ( ) {
	 /* .locals 2 */
	 /* .line 36 */
	 v0 = this.mPersistentDataBlockManagerInternal;
	 /* if-nez v0, :cond_0 */
	 /* .line 37 */
	 v0 = com.android.server.testharness.TestHarnessModeServiceImpl.TAG;
	 final String v1 = "Getting PersistentDataBlockManagerInternal from LocalServices"; // const-string v1, "Getting PersistentDataBlockManagerInternal from LocalServices"
	 android.util.Slog .d ( v0,v1 );
	 /* .line 38 */
	 /* const-class v0, Lcom/android/server/PersistentDataBlockManagerInternal; */
	 /* .line 39 */
	 com.android.server.LocalServices .getService ( v0 );
	 /* check-cast v0, Lcom/android/server/PersistentDataBlockManagerInternal; */
	 this.mPersistentDataBlockManagerInternal = v0;
	 /* .line 41 */
} // :cond_0
v0 = this.mPersistentDataBlockManagerInternal;
} // .end method
private Boolean isDeviceProvisioned ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 45 */
int v0 = 1; // const/4 v0, 0x1
/* .line 46 */
/* .local v0, "provisioned":Z */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 48 */
/* .local v1, "cr":Landroid/content/ContentResolver; */
try { // :try_start_0
	 final String v2 = "device_provisioned"; // const-string v2, "device_provisioned"
	 v2 = 	 android.provider.Settings$Global .getInt ( v1,v2 );
	 /* :try_end_0 */
	 /* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
	 int v3 = 1; // const/4 v3, 0x1
	 /* if-ne v2, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* move v0, v3 */
/* .line 51 */
/* .line 49 */
/* :catch_0 */
/* move-exception v2 */
/* .line 50 */
/* .local v2, "e":Landroid/provider/Settings$SettingNotFoundException; */
(( android.provider.Settings$SettingNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V
/* .line 52 */
} // .end local v2 # "e":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_1
} // .end method
/* # virtual methods */
public Boolean isMITSModeEnabled ( ) {
/* .locals 3 */
/* .line 64 */
/* invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal; */
/* .line 65 */
/* .local v0, "blockManager":Lcom/android/server/PersistentDataBlockManagerInternal; */
/* if-nez v0, :cond_0 */
/* .line 66 */
v1 = com.android.server.testharness.TestHarnessModeServiceImpl.TAG;
final String v2 = "Failed to get MITS Mode; no implementation of PersistentDataBlockManagerInternal was bound!"; // const-string v2, "Failed to get MITS Mode; no implementation of PersistentDataBlockManagerInternal was bound!"
android.util.Slog .e ( v1,v2 );
/* .line 68 */
int v1 = 0; // const/4 v1, 0x0
/* .line 70 */
} // :cond_0
/* new-instance v1, Ljava/lang/String; */
/* invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V */
final String v2 = "MITS_ENABLED"; // const-string v2, "MITS_ENABLED"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isMitsCommand ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 87 */
final String v0 = "enable_mits"; // const-string v0, "enable_mits"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "disable_mits"; // const-string v0, "disable_mits"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 88 */
final String v0 = "has_mits"; // const-string v0, "has_mits"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 87 */
} // :goto_1
} // .end method
public Integer onMitsCommand ( java.lang.String p0, java.io.PrintWriter p1, android.content.Context p2 ) {
/* .locals 4 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .param p2, "printWriter" # Ljava/io/PrintWriter; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 94 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v1 = 2; // const/4 v1, 0x2
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "has_mits"; // const-string v0, "has_mits"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* :sswitch_1 */
final String v0 = "disable_mits"; // const-string v0, "disable_mits"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
/* :sswitch_2 */
final String v0 = "enable_mits"; // const-string v0, "enable_mits"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v3 */
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 124 */
/* .line 117 */
/* :pswitch_0 */
v0 = (( com.android.server.testharness.TestHarnessModeServiceImpl ) p0 ).isMITSModeEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 118 */
/* const-string/jumbo v0, "yes" */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 119 */
/* .line 121 */
} // :cond_1
final String v0 = "no"; // const-string v0, "no"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 122 */
/* .line 109 */
/* :pswitch_1 */
v0 = (( com.android.server.testharness.TestHarnessModeServiceImpl ) p0 ).isMITSModeEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 110 */
/* invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal; */
/* .line 111 */
final String v0 = "MITS disabled"; // const-string v0, "MITS disabled"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 112 */
/* .line 114 */
} // :cond_2
final String v0 = "MITS already disabled"; // const-string v0, "MITS already disabled"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 115 */
/* .line 96 */
/* :pswitch_2 */
final String v0 = "ro.debuggable"; // const-string v0, "ro.debuggable"
v0 = android.os.SystemProperties .getInt ( v0,v3 );
/* if-eq v0, v2, :cond_3 */
/* .line 97 */
final String v0 = "Forbid to enable Mits Mode."; // const-string v0, "Forbid to enable Mits Mode."
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 98 */
/* .line 100 */
} // :cond_3
/* invoke-direct {p0, p3}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->checkPermissions(Landroid/content/Context;)V */
/* .line 101 */
v0 = (( com.android.server.testharness.TestHarnessModeServiceImpl ) p0 ).isMITSModeEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z
/* if-nez v0, :cond_4 */
/* .line 102 */
/* invoke-direct {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->getPersistentDataBlock()Lcom/android/server/PersistentDataBlockManagerInternal; */
final String v1 = "MITS_ENABLED"; // const-string v1, "MITS_ENABLED"
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
/* .line 103 */
final String v0 = "MITS enabled"; // const-string v0, "MITS enabled"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 104 */
/* .line 106 */
} // :cond_4
final String v0 = "MITS already enabled"; // const-string v0, "MITS already enabled"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 107 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x552d4da9 -> :sswitch_2 */
/* -0x3ac6b3ee -> :sswitch_1 */
/* 0x861dba0 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void setUpMITSMode ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 75 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isDeviceProvisioned(Landroid/content/Context;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 76 */
return;
/* .line 79 */
} // :cond_0
v0 = (( com.android.server.testharness.TestHarnessModeServiceImpl ) p0 ).isMITSModeEnabled ( ); // invoke-virtual {p0}, Lcom/android/server/testharness/TestHarnessModeServiceImpl;->isMITSModeEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 80 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 81 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
final String v1 = "adb_enabled"; // const-string v1, "adb_enabled"
int v2 = 1; // const/4 v2, 0x1
android.provider.Settings$Global .putInt ( v0,v1,v2 );
/* .line 83 */
} // .end local v0 # "cr":Landroid/content/ContentResolver;
} // :cond_1
return;
} // .end method
