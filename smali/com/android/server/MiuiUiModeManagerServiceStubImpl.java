public class com.android.server.MiuiUiModeManagerServiceStubImpl extends com.android.server.MiuiUiModeManagerServiceStub {
	 /* .source "MiuiUiModeManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.MiuiUiModeManagerServiceStub$$" */
} // .end annotation
/* # static fields */
private static Float A;
private static final Integer AUTO_NIGHT_DEFAULT_END_TIME;
private static final Integer AUTO_NIGHT_DEFAULT_START_TIME;
private static final java.lang.String AUTO_NIGHT_END_TIME;
private static final java.lang.String AUTO_NIGHT_START_TIME;
private static Float B;
private static Float C;
private static final java.lang.String DARK_MODE_ENABLE;
private static final java.lang.String DARK_MODE_ENABLE_BY_POWER_SAVE;
private static final java.lang.String DARK_MODE_ENABLE_BY_SETTING;
private static final java.lang.String DARK_MODE_SWITCH_NOW;
private static final Integer GAMMA_SPACE_MAX;
private static final Integer GAMMA_SPACE_MIN;
private static final java.lang.String GET_SUN_TIME_FROM_CLOUD;
private static final Boolean IS_JP_KDDI;
private static final Boolean IS_MEXICO_TELCEL;
private static Float R;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private final android.hardware.display.DisplayManager$DisplayListener mDisplayListener;
private android.hardware.display.DisplayManager mDisplayManager;
private com.android.server.ForceDarkUiModeModeManager mForceDarkUiModeModeManager;
private com.android.server.UiModeManagerService mUiModeManagerService;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.MiuiUiModeManagerServiceStubImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static com.android.server.UiModeManagerService -$$Nest$fgetmUiModeManagerService ( com.android.server.MiuiUiModeManagerServiceStubImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mUiModeManagerService;
} // .end method
static void -$$Nest$mupdateAlpha ( com.android.server.MiuiUiModeManagerServiceStubImpl p0, android.content.Context p1, Float p2, Float p3, Float p4 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->updateAlpha(Landroid/content/Context;FFF)V */
	 return;
} // .end method
static com.android.server.MiuiUiModeManagerServiceStubImpl ( ) {
	 /* .locals 3 */
	 /* .line 35 */
	 /* nop */
	 /* .line 36 */
	 final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
	 android.os.SystemProperties .get ( v0 );
	 /* .line 35 */
	 final String v2 = "mx_telcel"; // const-string v2, "mx_telcel"
	 v1 = 	 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 com.android.server.MiuiUiModeManagerServiceStubImpl.IS_MEXICO_TELCEL = (v1!= 0);
	 /* .line 38 */
	 /* nop */
	 /* .line 39 */
	 android.os.SystemProperties .get ( v0 );
	 /* .line 38 */
	 final String v1 = "jp_kd"; // const-string v1, "jp_kd"
	 v0 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 com.android.server.MiuiUiModeManagerServiceStubImpl.IS_JP_KDDI = (v0!= 0);
	 /* .line 41 */
	 android.content.res.Resources .getSystem ( );
	 /* const v1, 0x10e00f3 */
	 v0 = 	 (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
	 return;
} // .end method
public com.android.server.MiuiUiModeManagerServiceStubImpl ( ) {
	 /* .locals 1 */
	 /* .line 32 */
	 /* invoke-direct {p0}, Lcom/android/server/MiuiUiModeManagerServiceStub;-><init>()V */
	 /* .line 63 */
	 /* new-instance v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;-><init>(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)V */
	 this.mDisplayListener = v0;
	 return;
} // .end method
private Integer calculateMinuteFormat ( Long p0 ) {
	 /* .locals 3 */
	 /* .param p1, "sunTimeMillis" # J */
	 /* .line 272 */
	 final String v0 = "HH"; // const-string v0, "HH"
	 android.text.format.DateFormat .format ( v0,p1,p2 );
	 java.lang.String .valueOf ( v0 );
	 v0 = 	 java.lang.Integer .parseInt ( v0 );
	 /* .line 273 */
	 /* .local v0, "sunHour":I */
	 /* nop */
	 /* .line 274 */
	 final String v1 = "mm"; // const-string v1, "mm"
	 android.text.format.DateFormat .format ( v1,p1,p2 );
	 /* .line 273 */
	 java.lang.String .valueOf ( v1 );
	 v1 = 	 java.lang.Integer .parseInt ( v1 );
	 /* .line 275 */
	 /* .local v1, "sunMinute":I */
	 /* mul-int/lit8 v2, v0, 0x3c */
	 /* add-int/2addr v2, v1 */
	 /* .line 276 */
	 /* .local v2, "sunTime":I */
} // .end method
private Integer convertLinearToGammaFloat ( Float p0, Float p1, Float p2 ) {
	 /* .locals 4 */
	 /* .param p1, "val" # F */
	 /* .param p2, "min" # F */
	 /* .param p3, "max" # F */
	 /* .line 130 */
	 v0 = 	 android.util.MathUtils .norm ( p2,p3,p1 );
	 /* const/high16 v1, 0x41400000 # 12.0f */
	 /* mul-float/2addr v0, v1 */
	 /* .line 132 */
	 /* .local v0, "normalizedVal":F */
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* cmpg-float v1, v0, v1 */
	 /* if-gtz v1, :cond_0 */
	 /* .line 133 */
	 v1 = 	 android.util.MathUtils .sqrt ( v0 );
	 /* mul-float/2addr v1, v2 */
	 /* .local v1, "ret":F */
	 /* .line 135 */
} // .end local v1 # "ret":F
} // :cond_0
/* sub-float v2, v0, v2 */
v2 = android.util.MathUtils .log ( v2 );
/* mul-float/2addr v1, v2 */
/* add-float/2addr v1, v2 */
/* .line 137 */
/* .restart local v1 # "ret":F */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
v2 = android.util.MathUtils .lerp ( v2,v3,v1 );
v2 = java.lang.Math .round ( v2 );
} // .end method
private Long getAlarmInMills ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 10 */
/* .param p1, "startTime" # I */
/* .param p2, "endTime" # I */
/* .param p3, "isSunRise" # Z */
/* .line 223 */
java.util.Calendar .getInstance ( );
/* .line 224 */
/* .local v0, "calendar":Ljava/util/Calendar; */
int v1 = 5; // const/4 v1, 0x5
v2 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 225 */
/* .local v2, "day":I */
/* const/16 v3, 0xb */
v4 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
/* .line 226 */
/* .local v4, "hour":I */
/* const/16 v5, 0xc */
v6 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* .line 227 */
/* .local v6, "minute":I */
/* mul-int/lit8 v7, v4, 0x3c */
/* add-int/2addr v7, v6 */
/* .line 228 */
/* .local v7, "currentTime":I */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 229 */
/* if-ge v7, p1, :cond_0 */
/* .line 230 */
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 232 */
} // :cond_0
/* add-int/lit8 v8, v2, 0x1 */
(( java.util.Calendar ) v0 ).set ( v1, v8 ); // invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V
/* .line 234 */
} // :goto_0
/* div-int/lit8 v1, p2, 0x3c */
(( java.util.Calendar ) v0 ).set ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V
/* .line 235 */
/* rem-int/lit8 v1, p2, 0x3c */
(( java.util.Calendar ) v0 ).set ( v5, v1 ); // invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V
/* .line 237 */
} // :cond_1
/* if-ge v7, p2, :cond_2 */
/* .line 238 */
/* add-int/lit8 v8, v2, -0x1 */
(( java.util.Calendar ) v0 ).set ( v1, v8 ); // invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V
/* .line 240 */
} // :cond_2
(( java.util.Calendar ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V
/* .line 242 */
} // :goto_1
/* div-int/lit8 v1, p1, 0x3c */
(( java.util.Calendar ) v0 ).set ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V
/* .line 243 */
/* rem-int/lit8 v1, p1, 0x3c */
(( java.util.Calendar ) v0 ).set ( v5, v1 ); // invoke-virtual {v0, v5, v1}, Ljava/util/Calendar;->set(II)V
/* .line 245 */
} // :goto_2
/* const/16 v1, 0xd */
int v3 = 0; // const/4 v3, 0x0
(( java.util.Calendar ) v0 ).set ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V
/* .line 246 */
/* const/16 v1, 0xe */
(( java.util.Calendar ) v0 ).set ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V
/* .line 247 */
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v8 */
/* return-wide v8 */
} // .end method
private java.lang.String getTimeInString ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "time" # I */
/* .line 280 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* div-int/lit8 v1, p1, 0x3c */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ":"; // const-string v1, ":"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* rem-int/lit8 v1, p1, 0x3c */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void registUIModeScaleChangeObserver ( com.android.server.UiModeManagerService p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "service" # Lcom/android/server/UiModeManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 151 */
/* new-instance v0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1, p2, p1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$2;-><init>(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/UiModeManagerService;)V */
/* .line 164 */
/* .local v0, "uiModeScaleChangedObserver":Landroid/database/ContentObserver; */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 165 */
/* const-string/jumbo v2, "ui_mode_scale" */
android.provider.Settings$System .getUriFor ( v2 );
/* .line 164 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 166 */
(( android.database.ContentObserver ) v0 ).onChange ( v3 ); // invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 167 */
return;
} // .end method
private void updateAlpha ( android.content.Context p0, Float p1, Float p2, Float p3 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "mBrightness" # F */
/* .param p3, "mMaxBrightness" # F */
/* .param p4, "mMinBrightness" # F */
/* .line 116 */
v0 = /* invoke-direct {p0, p2, p4, p3}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->convertLinearToGammaFloat(FFF)I */
/* div-int/2addr v0, v1 */
/* int-to-float v0, v0 */
/* .line 117 */
/* .local v0, "ratio":F */
/* const-wide/16 v1, 0x0 */
/* .line 118 */
/* .local v1, "alpha":D */
/* float-to-double v3, v0 */
/* const-wide v5, 0x3fd999999999999aL # 0.4 */
/* cmpg-double v3, v3, v5 */
/* const-wide v7, 0x3fb999999999999aL # 0.1 */
/* if-gez v3, :cond_0 */
/* float-to-double v3, v0 */
/* cmpl-double v3, v3, v7 */
/* if-lez v3, :cond_0 */
/* .line 119 */
/* float-to-double v3, v0 */
/* sub-double/2addr v5, v3 */
/* const-wide/high16 v3, 0x4004000000000000L # 2.5 */
/* div-double/2addr v5, v3 */
java.lang.Math .sqrt ( v5,v6 );
/* move-result-wide v1 */
/* .line 121 */
} // :cond_0
/* float-to-double v3, v0 */
/* cmpg-double v3, v3, v7 */
/* if-gtz v3, :cond_1 */
/* .line 122 */
/* float-to-double v3, v0 */
/* const-wide v5, 0x3fc17c1bda5119ceL # 0.1366 */
/* add-double/2addr v3, v5 */
/* const-wide v5, 0x3fe5db22d0e56042L # 0.683 */
/* div-double v1, v3, v5 */
/* .line 124 */
} // :cond_1
/* double-to-float v3, v1 */
/* .line 125 */
/* .local v3, "setalpha":F */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "contrast_alpha"; // const-string v5, "contrast_alpha"
android.provider.Settings$System .putFloat ( v4,v5,v3 );
/* .line 126 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 175 */
v0 = this.mForceDarkUiModeModeManager;
(( com.android.server.ForceDarkUiModeModeManager ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;->dump(Ljava/io/PrintWriter;)V
/* .line 176 */
return;
} // .end method
public com.android.server.twilight.TwilightState getDefaultTwilightState ( ) {
/* .locals 7 */
/* .line 211 */
final String v0 = "MiuiUiModeManagerServiceStubImpl"; // const-string v0, "MiuiUiModeManagerServiceStubImpl"
final String v1 = "cannot get real sunrise and sunset time, return default time or old time"; // const-string v1, "cannot get real sunrise and sunset time, return default time or old time"
android.util.Slog .i ( v0,v1 );
/* .line 212 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "auto_night_end_time"; // const-string v1, "auto_night_end_time"
/* const/16 v2, 0x168 */
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* .line 214 */
/* .local v0, "sunRiseTime":I */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "auto_night_start_time"; // const-string v2, "auto_night_start_time"
/* const/16 v3, 0x438 */
v1 = android.provider.Settings$System .getInt ( v1,v2,v3 );
/* .line 216 */
/* .local v1, "sunSetTime":I */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v1, v0, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J */
/* move-result-wide v2 */
/* .line 217 */
/* .local v2, "sunSetTimeMillis":J */
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {p0, v1, v0, v4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J */
/* move-result-wide v4 */
/* .line 218 */
/* .local v4, "sunRiseTimeMillis":J */
/* new-instance v6, Lcom/android/server/twilight/TwilightState; */
/* invoke-direct {v6, v4, v5, v2, v3}, Lcom/android/server/twilight/TwilightState;-><init>(JJ)V */
/* .line 219 */
/* .local v6, "lastState":Lcom/android/server/twilight/TwilightState; */
} // .end method
public Boolean getForceDarkAppDefaultEnable ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 170 */
com.android.server.ForceDarkAppListProvider .getInstance ( );
v0 = (( com.android.server.ForceDarkAppListProvider ) v0 ).getForceDarkAppDefaultEnable ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppDefaultEnable(Ljava/lang/String;)Z
} // .end method
public Boolean getSwitchValue ( ) {
/* .locals 3 */
/* .line 205 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_switch_now"; // const-string v1, "dark_mode_switch_now"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
public com.android.server.twilight.TwilightState getTwilightState ( Long p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "sunriseTimeMillis" # J */
/* .param p3, "sunsetTimeMillis" # J */
/* .line 264 */
v0 = /* invoke-direct {p0, p3, p4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I */
/* .line 265 */
/* .local v0, "sunsetTime":I */
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I */
/* .line 266 */
/* .local v1, "sunriseTime":I */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J */
/* move-result-wide p3 */
/* .line 267 */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v0, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getAlarmInMills(IIZ)J */
/* move-result-wide p1 */
/* .line 268 */
/* new-instance v2, Lcom/android/server/twilight/TwilightState; */
/* invoke-direct {v2, p1, p2, p3, p4}, Lcom/android/server/twilight/TwilightState;-><init>(JJ)V */
} // .end method
public void init ( com.android.server.UiModeManagerService p0 ) {
/* .locals 8 */
/* .param p1, "uiModeManagerService" # Lcom/android/server/UiModeManagerService; */
/* .line 87 */
final String v0 = "MiuiUiModeManagerServiceStubImpl"; // const-string v0, "MiuiUiModeManagerServiceStubImpl"
this.mUiModeManagerService = p1;
/* .line 88 */
(( com.android.server.UiModeManagerService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/UiModeManagerService;->getContext()Landroid/content/Context;
this.mContext = v1;
/* .line 89 */
/* const-class v2, Landroid/hardware/display/DisplayManager; */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/hardware/display/DisplayManager; */
this.mDisplayManager = v1;
/* .line 90 */
v1 = this.mContext;
/* .line 91 */
final String v2 = "power"; // const-string v2, "power"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/PowerManager; */
/* .line 92 */
/* .local v1, "powerManager":Landroid/os/PowerManager; */
/* new-instance v2, Landroid/content/IntentFilter; */
/* invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V */
/* .line 93 */
/* .local v2, "filter":Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.USER_SWITCHED"; // const-string v3, "android.intent.action.USER_SWITCHED"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 95 */
v3 = this.mContext;
/* invoke-direct {p0, p1, v3}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->registUIModeScaleChangeObserver(Lcom/android/server/UiModeManagerService;Landroid/content/Context;)V */
/* .line 96 */
v3 = this.mDisplayManager;
v4 = this.mDisplayListener;
/* new-instance v5, Landroid/os/Handler; */
/* invoke-direct {v5}, Landroid/os/Handler;-><init>()V */
/* const-wide/16 v6, 0x8 */
(( android.hardware.display.DisplayManager ) v3 ).registerDisplayListener ( v4, v5, v6, v7 ); // invoke-virtual {v3, v4, v5, v6, v7}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;J)V
/* .line 99 */
try { // :try_start_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mNightMode: "; // const-string v4, "mNightMode: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.UiModeManagerService ) p1 ).getService ( ); // invoke-virtual {p1}, Lcom/android/server/UiModeManagerService;->getService()Landroid/app/IUiModeManager;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v3 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 102 */
/* .line 100 */
/* :catch_0 */
/* move-exception v3 */
/* .line 101 */
/* .local v3, "e":Landroid/os/RemoteException; */
final String v4 = "Failure communicating with uimode manager"; // const-string v4, "Failure communicating with uimode manager"
android.util.Slog .w ( v0,v4,v3 );
/* .line 104 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_0
/* new-instance v0, Lcom/android/server/ForceDarkUiModeModeManager; */
/* invoke-direct {v0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;-><init>(Lcom/android/server/UiModeManagerService;)V */
this.mForceDarkUiModeModeManager = v0;
/* .line 105 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1107001d */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 107 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1107001a */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 109 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1107001b */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 111 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x1107001c */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 113 */
return;
} // .end method
public void modifySettingValue ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "value" # I */
/* .line 186 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 196 */
/* :pswitch_0 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_enable_by_power_save"; // const-string v1, "dark_mode_enable_by_power_save"
android.provider.Settings$System .putInt ( v0,v1,p2 );
/* .line 197 */
/* .line 191 */
/* :pswitch_1 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
final String v2 = "dark_mode_switch_now"; // const-string v2, "dark_mode_switch_now"
v0 = android.provider.Settings$System .getInt ( v0,v2,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 192 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putInt ( v0,v2,p2 );
/* .line 188 */
/* :pswitch_2 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_enable_by_setting"; // const-string v1, "dark_mode_enable_by_setting"
android.provider.Settings$System .putInt ( v0,v1,p2 );
/* .line 189 */
/* nop */
/* .line 201 */
} // :cond_0
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "phase" # I */
/* .line 141 */
v0 = this.mForceDarkUiModeModeManager;
(( com.android.server.ForceDarkUiModeModeManager ) v0 ).onBootPhase ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/ForceDarkUiModeModeManager;->onBootPhase(I)V
/* .line 142 */
return;
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 145 */
v0 = this.mForceDarkUiModeModeManager;
v0 = (( com.android.server.ForceDarkUiModeModeManager ) v0 ).onTransact ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/ForceDarkUiModeModeManager;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
} // .end method
public void setDarkModeStatus ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uiMode" # I */
/* .line 180 */
/* and-int/lit8 v0, p1, 0x30 */
/* const/16 v1, 0x20 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* if-ne v0, v1, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v3 */
/* .line 181 */
/* .local v0, "isNightMode":Z */
} // :goto_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
/* move v2, v3 */
} // :goto_1
final String v3 = "dark_mode_enable"; // const-string v3, "dark_mode_enable"
android.provider.Settings$System .putInt ( v1,v3,v2 );
/* .line 182 */
return;
} // .end method
public void updateSunRiseSetTime ( com.android.server.twilight.TwilightState p0 ) {
/* .locals 4 */
/* .param p1, "state" # Lcom/android/server/twilight/TwilightState; */
/* .line 252 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 253 */
(( com.android.server.twilight.TwilightState ) p1 ).sunsetTimeMillis ( ); // invoke-virtual {p1}, Lcom/android/server/twilight/TwilightState;->sunsetTimeMillis()J
/* move-result-wide v0 */
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I */
/* .line 254 */
/* .local v0, "sunsetTime":I */
(( com.android.server.twilight.TwilightState ) p1 ).sunriseTimeMillis ( ); // invoke-virtual {p1}, Lcom/android/server/twilight/TwilightState;->sunriseTimeMillis()J
/* move-result-wide v1 */
v1 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->calculateMinuteFormat(J)I */
/* .line 255 */
/* .local v1, "sunriseTime":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "today state: sunrise\uff1a" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, v1}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getTimeInString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " sunset\uff1a"; // const-string v3, " sunset\uff1a"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 256 */
/* invoke-direct {p0, v0}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->getTimeInString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 255 */
final String v3 = "MiuiUiModeManagerServiceStubImpl"; // const-string v3, "MiuiUiModeManagerServiceStubImpl"
android.util.Slog .i ( v3,v2 );
/* .line 257 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "auto_night_start_time"; // const-string v3, "auto_night_start_time"
android.provider.Settings$System .putInt ( v2,v3,v0 );
/* .line 258 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "auto_night_end_time"; // const-string v3, "auto_night_end_time"
android.provider.Settings$System .putInt ( v2,v3,v1 );
/* .line 260 */
} // .end local v0 # "sunsetTime":I
} // .end local v1 # "sunriseTime":I
} // :cond_0
return;
} // .end method
