.class Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack;
.super Ljava/lang/Object;
.source "BootKeeperStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/BootKeeperStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BootKeeperOneTrack"
.end annotation


# static fields
.field private static final EXTRA_APP_ID:Ljava/lang/String; = "31000401440"

.field private static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "com.android.server"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN:I = 0x1

.field private static final INTENT_ACTION_ONETRACK:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final INTENT_PACKAGE_ONETRACK:Ljava/lang/String; = "com.miui.analytics"

.field private static final RELEASE_SAPCE_ARG:Ljava/lang/String; = "release_space"

.field private static final RELEASE_SPACE_EVENT_NAME:Ljava/lang/String; = "release_space"

.field private static final REPORT_LATENCY:I = 0xa4cb80

.field private static final TAG:Ljava/lang/String; = "BootKeeperOneTrack"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    return-void
.end method

.method static synthetic lambda$reportReleaseSpaceOneTrack$0(Landroid/content/Context;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trigger"    # Z

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportReleaseSpaceOneTrack context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BootKeeperOneTrack"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    if-eqz p0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    goto :goto_2

    .line 274
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const-string v2, "APP_ID"

    const-string v3, "31000401440"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const-string v2, "EVENT_NAME"

    const-string v3, "release_space"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v2, "PACKAGE"

    const-string v4, "com.android.server"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 280
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 282
    :try_start_0
    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v2

    .line 286
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "Unable to start service."

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 283
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 284
    .local v2, "e":Ljava/lang/IllegalStateException;
    const-string v3, "Failed to upload BootKeeper release space  event."

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    nop

    .line 288
    :goto_1
    return-void

    .line 272
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_2
    return-void
.end method

.method public static reportReleaseSpaceOneTrack(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "trigger"    # Z

    .line 269
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/BootKeeperStubImpl$BootKeeperOneTrack$$ExternalSyntheticLambda0;-><init>(Landroid/content/Context;Z)V

    const-wide/32 v2, 0xa4cb80

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 289
    return-void
.end method
