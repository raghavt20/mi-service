public class com.android.server.DarkModeTimeModeManager {
	 /* .source "DarkModeTimeModeManager.java" */
	 /* # static fields */
	 public static final java.lang.String DARK_MODE_ENABLE;
	 public static final Integer SCREEN_DARKMODE;
	 public static final Boolean SUPPORT_DARK_MODE_NOTIFY;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private android.database.ContentObserver mDarkModeObserver;
	 private com.android.server.DarkModeSuggestProvider mDarkModeSuggestProvider;
	 private android.content.BroadcastReceiver mDarkModeTimeModeReceiver;
	 private android.app.UiModeManager mUiModeManager;
	 /* # direct methods */
	 static com.android.server.DarkModeTimeModeManager ( ) {
		 /* .locals 2 */
		 /* .line 61 */
		 /* nop */
		 /* .line 62 */
		 /* const-string/jumbo v0, "support_dark_mode_notify" */
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 miui.util.FeatureParser .getBoolean ( v0,v1 );
		 com.android.server.DarkModeTimeModeManager.SUPPORT_DARK_MODE_NOTIFY = (v0!= 0);
		 /* .line 61 */
		 return;
	 } // .end method
	 public com.android.server.DarkModeTimeModeManager ( ) {
		 /* .locals 0 */
		 /* .line 65 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 66 */
		 return;
	 } // .end method
	 public com.android.server.DarkModeTimeModeManager ( ) {
		 /* .locals 5 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 68 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 69 */
		 this.mContext = p1;
		 /* .line 71 */
		 /* new-instance v0, Landroid/content/IntentFilter; */
		 /* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
		 /* .line 72 */
		 /* .local v0, "intentFilter":Landroid/content/IntentFilter; */
		 /* invoke-direct {p0, v0, p1}, Lcom/android/server/DarkModeTimeModeManager;->addAction(Landroid/content/IntentFilter;Landroid/content/Context;)V */
		 /* .line 73 */
		 /* new-instance v1, Lcom/android/server/DarkModeTimeModeManager$1; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/DarkModeTimeModeManager$1;-><init>(Lcom/android/server/DarkModeTimeModeManager;)V */
		 this.mDarkModeTimeModeReceiver = v1;
		 /* .line 95 */
		 int v2 = 2; // const/4 v2, 0x2
		 (( android.content.Context ) p1 ).registerReceiver ( v1, v0, v2 ); // invoke-virtual {p1, v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
		 /* .line 96 */
		 v1 = this.mContext;
		 /* const-class v2, Landroid/app/UiModeManager; */
		 (( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
		 /* check-cast v1, Landroid/app/UiModeManager; */
		 this.mUiModeManager = v1;
		 /* .line 97 */
		 v1 = 		 com.android.server.DarkModeTimeModeHelper .getDarkModeSuggestCount ( p1 );
		 int v2 = 3; // const/4 v2, 0x3
		 /* if-ge v1, v2, :cond_0 */
		 /* .line 99 */
		 com.android.server.DarkModeSuggestProvider .getInstance ( );
		 this.mDarkModeSuggestProvider = v1;
		 /* .line 100 */
		 (( com.android.server.DarkModeSuggestProvider ) v1 ).registerDataObserver ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/DarkModeSuggestProvider;->registerDataObserver(Landroid/content/Context;)V
		 /* .line 102 */
	 } // :cond_0
	 /* new-instance v1, Lcom/android/server/DarkModeTimeModeManager$2; */
	 /* new-instance v2, Landroid/os/Handler; */
	 /* invoke-direct {v2}, Landroid/os/Handler;-><init>()V */
	 /* invoke-direct {v1, p0, v2, p1}, Lcom/android/server/DarkModeTimeModeManager$2;-><init>(Lcom/android/server/DarkModeTimeModeManager;Landroid/os/Handler;Landroid/content/Context;)V */
	 this.mDarkModeObserver = v1;
	 /* .line 112 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 113 */
	 final String v2 = "dark_mode_enable"; // const-string v2, "dark_mode_enable"
	 android.provider.Settings$System .getUriFor ( v2 );
	 v3 = this.mDarkModeObserver;
	 /* .line 112 */
	 int v4 = 0; // const/4 v4, 0x0
	 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v1, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
	 /* .line 114 */
	 return;
} // .end method
private void addAction ( android.content.IntentFilter p0, android.content.Context p1 ) {
	 /* .locals 2 */
	 /* .param p1, "it" # Landroid/content/IntentFilter; */
	 /* .param p2, "context" # Landroid/content/Context; */
	 /* .line 117 */
	 final String v0 = "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"; // const-string v0, "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"
	 (( android.content.IntentFilter ) p1 ).addAction ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 118 */
	 final String v0 = "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"; // const-string v0, "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"
	 (( android.content.IntentFilter ) p1 ).addAction ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 119 */
	 v0 = 	 com.android.server.DarkModeTimeModeHelper .getDarkModeSuggestCount ( p2 );
	 int v1 = 3; // const/4 v1, 0x3
	 /* if-lt v0, v1, :cond_0 */
	 /* sget-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 121 */
	 } // :cond_0
	 final String v0 = "android.intent.action.USER_PRESENT"; // const-string v0, "android.intent.action.USER_PRESENT"
	 (( android.content.IntentFilter ) p1 ).addAction ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 123 */
} // :cond_1
return;
} // .end method
private void creatNoticifationChannel ( java.lang.String p0, java.lang.CharSequence p1, Integer p2, java.lang.String p3, android.app.NotificationManager p4 ) {
/* .locals 1 */
/* .param p1, "channelId" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/CharSequence; */
/* .param p3, "importance" # I */
/* .param p4, "description" # Ljava/lang/String; */
/* .param p5, "manager" # Landroid/app/NotificationManager; */
/* .line 271 */
/* new-instance v0, Landroid/app/NotificationChannel; */
/* invoke-direct {v0, p1, p2, p3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V */
/* .line 272 */
/* .local v0, "channel":Landroid/app/NotificationChannel; */
(( android.app.NotificationChannel ) v0 ).setDescription ( p4 ); // invoke-virtual {v0, p4}, Landroid/app/NotificationChannel;->setDescription(Ljava/lang/String;)V
/* .line 273 */
(( android.app.NotificationManager ) p5 ).createNotificationChannel ( v0 ); // invoke-virtual {p5, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V
/* .line 274 */
return;
} // .end method
private void initNotification ( android.content.Context p0 ) {
/* .locals 13 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 221 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f01a8 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 222 */
/* .local v0, "title":Ljava/lang/String; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110f01a7 */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 224 */
/* .local v1, "message":Ljava/lang/String; */
/* new-instance v2, Landroid/content/Intent; */
final String v3 = "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"; // const-string v3, "miui.action.intent.DARK_MODE_SUGGEST_MESSAGE"
/* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 225 */
/* .local v2, "intentSettings":Landroid/content/Intent; */
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x4000000 */
android.app.PendingIntent .getBroadcast ( p1,v3,v2,v4 );
/* .line 228 */
/* .local v5, "pendingIntentSettings":Landroid/app/PendingIntent; */
/* new-instance v6, Landroid/content/Intent; */
final String v7 = "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"; // const-string v7, "miui.action.intent.DARK_MODE_SUGGEST_ENABLE"
/* invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 229 */
/* .local v6, "intentEnable":Landroid/content/Intent; */
/* nop */
/* .line 230 */
android.app.PendingIntent .getBroadcast ( p1,v3,v6,v4 );
/* .line 232 */
/* .local v3, "pendingIntentEnable":Landroid/app/PendingIntent; */
/* const-class v4, Landroid/app/NotificationManager; */
/* .line 233 */
(( android.content.Context ) p1 ).getSystemService ( v4 ); // invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v4, Landroid/app/NotificationManager; */
/* .line 235 */
/* .local v4, "notificationManager":Landroid/app/NotificationManager; */
final String v8 = "dark_mode_suggest_id"; // const-string v8, "dark_mode_suggest_id"
final String v9 = "dark_mode"; // const-string v9, "dark_mode"
int v10 = 4; // const/4 v10, 0x4
final String v11 = "des"; // const-string v11, "des"
/* move-object v7, p0 */
/* move-object v12, v4 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/server/DarkModeTimeModeManager;->creatNoticifationChannel(Ljava/lang/String;Ljava/lang/CharSequence;ILjava/lang/String;Landroid/app/NotificationManager;)V */
/* .line 238 */
/* new-instance v7, Landroid/os/Bundle; */
/* invoke-direct {v7}, Landroid/os/Bundle;-><init>()V */
/* .line 239 */
/* .local v7, "arg":Landroid/os/Bundle; */
/* const v8, 0x1108018f */
android.graphics.drawable.Icon .createWithResource ( p1,v8 );
final String v10 = "miui.appIcon"; // const-string v10, "miui.appIcon"
(( android.os.Bundle ) v7 ).putParcelable ( v10, v9 ); // invoke-virtual {v7, v10, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 241 */
final String v9 = "miui.showAction"; // const-string v9, "miui.showAction"
int v10 = 1; // const/4 v10, 0x1
(( android.os.Bundle ) v7 ).putBoolean ( v9, v10 ); // invoke-virtual {v7, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 242 */
/* new-instance v9, Landroid/app/Notification$Builder; */
final String v11 = "dark_mode_suggest_id"; // const-string v11, "dark_mode_suggest_id"
/* invoke-direct {v9, p1, v11}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 243 */
(( android.app.Notification$Builder ) v9 ).addExtras ( v7 ); // invoke-virtual {v9, v7}, Landroid/app/Notification$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;
/* .line 244 */
(( android.app.Notification$Builder ) v9 ).setSmallIcon ( v8 ); // invoke-virtual {v9, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
/* .line 245 */
(( android.app.Notification$Builder ) v8 ).setContentTitle ( v0 ); // invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* .line 246 */
(( android.app.Notification$Builder ) v8 ).setContentText ( v1 ); // invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
/* new-instance v9, Landroid/app/Notification$BigTextStyle; */
/* invoke-direct {v9}, Landroid/app/Notification$BigTextStyle;-><init>()V */
/* .line 247 */
(( android.app.Notification$BigTextStyle ) v9 ).bigText ( v1 ); // invoke-virtual {v9, v1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
(( android.app.Notification$Builder ) v8 ).setStyle ( v9 ); // invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;
/* .line 248 */
(( android.app.Notification$Builder ) v8 ).setContentIntent ( v5 ); // invoke-virtual {v8, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 250 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v11, 0x110f01a6 */
(( android.content.res.Resources ) v9 ).getString ( v11 ); // invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 249 */
/* const v11, 0x1108014e */
(( android.app.Notification$Builder ) v8 ).addAction ( v11, v9, v3 ); // invoke-virtual {v8, v11, v9, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
/* .line 252 */
(( android.app.Notification$Builder ) v8 ).setAutoCancel ( v10 ); // invoke-virtual {v8, v10}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
/* .line 253 */
/* const-wide/16 v11, 0x1388 */
(( android.app.Notification$Builder ) v8 ).setTimeoutAfter ( v11, v12 ); // invoke-virtual {v8, v11, v12}, Landroid/app/Notification$Builder;->setTimeoutAfter(J)Landroid/app/Notification$Builder;
/* .line 254 */
(( android.app.Notification$Builder ) v8 ).build ( ); // invoke-virtual {v8}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
/* .line 256 */
/* .local v8, "notification":Landroid/app/Notification; */
final String v9 = "DarkModeTimeModeManager"; // const-string v9, "DarkModeTimeModeManager"
v11 = android.os.UserHandle.ALL;
(( android.app.NotificationManager ) v4 ).notifyAsUser ( v9, v10, v8, v11 ); // invoke-virtual {v4, v9, v10, v8, v11}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V
/* .line 258 */
return;
} // .end method
private void updateButtonStatus ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 307 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.DarkModeTimeModeHelper .setDarkModeTimeEnable ( p1,v0 );
/* .line 309 */
int v1 = 0; // const/4 v1, 0x0
com.android.server.DarkModeTimeModeHelper .setDarkModeAutoTimeEnable ( p1,v1 );
/* .line 310 */
com.android.server.DarkModeTimeModeHelper .setSunRiseSunSetMode ( p1,v0 );
/* .line 311 */
int v0 = 2; // const/4 v0, 0x2
com.android.server.DarkModeTimeModeHelper .setDarkModeTimeType ( p1,v0 );
/* .line 312 */
return;
} // .end method
private void updateDarkModeTimeModeStatus ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 134 */
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeTimeEnable ( p1 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "dark_mode_switch_now"; // const-string v2, "dark_mode_switch_now"
/* if-nez v0, :cond_0 */
/* .line 135 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getInt ( v0,v2,v1 );
/* .line 136 */
return;
/* .line 139 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 1; // const/4 v3, 0x1
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 140 */
v0 = com.android.server.DarkModeTimeModeHelper .isSuntimeType ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 141 */
v0 = this.mUiModeManager;
(( android.app.UiModeManager ) v0 ).setNightMode ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V
/* .line 143 */
} // :cond_1
v0 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .getDarkModeStartTime ( v0 );
/* .line 144 */
/* .local v0, "time":I */
/* div-int/lit8 v1, v0, 0x3c */
/* rem-int/lit8 v2, v0, 0x3c */
java.time.LocalTime .of ( v1,v2 );
/* .line 145 */
/* .local v1, "localTime":Ljava/time/LocalTime; */
v2 = this.mUiModeManager;
(( android.app.UiModeManager ) v2 ).setCustomNightModeStart ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/UiModeManager;->setCustomNightModeStart(Ljava/time/LocalTime;)V
/* .line 146 */
v2 = this.mContext;
v0 = com.android.server.DarkModeTimeModeHelper .getDarkModeEndTime ( v2 );
/* .line 147 */
/* div-int/lit8 v2, v0, 0x3c */
/* rem-int/lit8 v3, v0, 0x3c */
java.time.LocalTime .of ( v2,v3 );
/* .line 148 */
v2 = this.mUiModeManager;
(( android.app.UiModeManager ) v2 ).setCustomNightModeEnd ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/UiModeManager;->setCustomNightModeEnd(Ljava/time/LocalTime;)V
/* .line 149 */
v2 = this.mUiModeManager;
int v3 = 3; // const/4 v3, 0x3
(( android.app.UiModeManager ) v2 ).setNightMode ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/UiModeManager;->setNightMode(I)V
/* .line 151 */
} // .end local v0 # "time":I
} // .end local v1 # "localTime":Ljava/time/LocalTime;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean canShowDarkModeSuggestNotifocation ( android.content.Context p0 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 160 */
/* sget-boolean v0, Lcom/android/server/DarkModeStatusTracker;->DEBUG:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 161 */
/* .line 164 */
} // :cond_0
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeSuggestEnable ( p1 );
int v2 = 0; // const/4 v2, 0x0
final String v3 = "DarkModeTimeModeManager"; // const-string v3, "DarkModeTimeModeManager"
/* if-nez v0, :cond_1 */
/* .line 165 */
final String v0 = "not get suggest from cloud"; // const-string v0, "not get suggest from cloud"
android.util.Slog .i ( v3,v0 );
/* .line 167 */
/* .line 170 */
} // :cond_1
v0 = com.android.server.DarkModeTimeModeHelper .getDarkModeSuggestCount ( p1 );
int v4 = 3; // const/4 v4, 0x3
/* if-lt v0, v4, :cond_2 */
/* .line 171 */
final String v0 = "count >= 3"; // const-string v0, "count >= 3"
android.util.Slog .i ( v3,v0 );
/* .line 172 */
/* .line 175 */
} // :cond_2
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeOpen ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 176 */
final String v0 = "darkMode is open"; // const-string v0, "darkMode is open"
android.util.Slog .i ( v3,v0 );
/* .line 178 */
/* .line 181 */
} // :cond_3
v0 = com.android.server.DarkModeTimeModeHelper .isInNight ( p1 );
/* if-nez v0, :cond_4 */
/* .line 182 */
final String v0 = "not in night"; // const-string v0, "not in night"
android.util.Slog .i ( v3,v0 );
/* .line 183 */
/* .line 186 */
} // :cond_4
com.android.server.DarkModeTimeModeHelper .getNowTimeInMills ( );
/* move-result-wide v4 */
/* .line 187 */
com.android.server.DarkModeTimeModeHelper .getLastSuggestTime ( p1 );
/* move-result-wide v6 */
/* sub-long/2addr v4, v6 */
/* const-wide/32 v6, 0x240c8400 */
/* cmp-long v0, v4, v6 */
/* if-gez v0, :cond_5 */
/* .line 188 */
final String v0 = "less than 7 days ago"; // const-string v0, "less than 7 days ago"
android.util.Slog .i ( v3,v0 );
/* .line 189 */
/* .line 192 */
} // :cond_5
v0 = com.android.server.DarkModeTimeModeHelper .isOnHome ( p1 );
/* if-nez v0, :cond_6 */
/* .line 193 */
final String v0 = "not on home"; // const-string v0, "not on home"
android.util.Slog .i ( v3,v0 );
/* .line 194 */
/* .line 196 */
} // :cond_6
} // .end method
public void enterSettingsFromNotification ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 322 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "enter_setting_by_notification"; // const-string v1, "enter_setting_by_notification"
int v2 = 1; // const/4 v2, 0x1
android.provider.Settings$System .putInt ( v0,v1,v2 );
/* .line 324 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.settings.DISPLAY_SETTINGS"; // const-string v1, "android.settings.DISPLAY_SETTINGS"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 325 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 326 */
(( android.content.Context ) p1 ).startActivity ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* .line 328 */
int v1 = 3; // const/4 v1, 0x3
com.android.server.DarkModeTimeModeHelper .setDarkModeSuggestCount ( p1,v1 );
/* .line 329 */
/* new-instance v1, Lcom/android/server/DarkModeStauesEvent; */
/* invoke-direct {v1}, Lcom/android/server/DarkModeStauesEvent;-><init>()V */
/* .line 330 */
final String v3 = "darkModeSuggest"; // const-string v3, "darkModeSuggest"
(( com.android.server.DarkModeStauesEvent ) v1 ).setEventName ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 331 */
final String v3 = ""; // const-string v3, ""
(( com.android.server.DarkModeStauesEvent ) v1 ).setTip ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 332 */
(( com.android.server.DarkModeStauesEvent ) v1 ).setSuggestClick ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/DarkModeStauesEvent;->setSuggestClick(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 333 */
/* .local v1, "event":Lcom/android/server/DarkModeStauesEvent; */
com.android.server.DarkModeOneTrackHelper .uploadToOneTrack ( p1,v1 );
/* .line 335 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v3, Lcom/android/server/DarkModeTimeModeManager$3; */
/* invoke-direct {v3, p0, p1}, Lcom/android/server/DarkModeTimeModeManager$3;-><init>(Lcom/android/server/DarkModeTimeModeManager;Landroid/content/Context;)V */
/* const-wide/32 v4, 0xea60 */
(( android.os.Handler ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 350 */
return;
} // .end method
public void onBootPhase ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 126 */
/* invoke-direct {p0, p1}, Lcom/android/server/DarkModeTimeModeManager;->updateDarkModeTimeModeStatus(Landroid/content/Context;)V */
/* .line 127 */
return;
} // .end method
public void showDarkModeSuggestNotification ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 205 */
final String v0 = "DarkModeTimeModeManager"; // const-string v0, "DarkModeTimeModeManager"
/* const-string/jumbo v1, "showDarkModeSuggestNotification" */
android.util.Slog .i ( v0,v1 );
/* .line 206 */
/* invoke-direct {p0, p1}, Lcom/android/server/DarkModeTimeModeManager;->initNotification(Landroid/content/Context;)V */
/* .line 207 */
/* nop */
/* .line 208 */
v0 = com.android.server.DarkModeTimeModeHelper .getDarkModeSuggestCount ( p1 );
int v1 = 1; // const/4 v1, 0x1
/* add-int/2addr v0, v1 */
/* .line 207 */
com.android.server.DarkModeTimeModeHelper .setDarkModeSuggestCount ( p1,v0 );
/* .line 210 */
com.android.server.DarkModeSuggestProvider .getInstance ( );
(( com.android.server.DarkModeSuggestProvider ) v0 ).unRegisterDataObserver ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/DarkModeSuggestProvider;->unRegisterDataObserver(Landroid/content/Context;)V
/* .line 212 */
com.android.server.DarkModeTimeModeHelper .getNowTimeInMills ( );
/* move-result-wide v2 */
com.android.server.DarkModeTimeModeHelper .setLastSuggestTime ( p1,v2,v3 );
/* .line 213 */
/* new-instance v0, Lcom/android/server/DarkModeStauesEvent; */
/* invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V */
/* .line 214 */
final String v2 = "darkModeSuggest"; // const-string v2, "darkModeSuggest"
(( com.android.server.DarkModeStauesEvent ) v0 ).setEventName ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 215 */
final String v2 = ""; // const-string v2, ""
(( com.android.server.DarkModeStauesEvent ) v0 ).setTip ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 216 */
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggest ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggest(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 217 */
/* .local v0, "event":Lcom/android/server/DarkModeStauesEvent; */
com.android.server.DarkModeOneTrackHelper .uploadToOneTrack ( p1,v0 );
/* .line 218 */
return;
} // .end method
public void showDarkModeSuggestToast ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 288 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "open_sun_time_channel"; // const-string v1, "open_sun_time_channel"
/* const-string/jumbo v2, "\u5f39\u7a97" */
android.provider.Settings$System .putString ( v0,v1,v2 );
/* .line 290 */
/* invoke-direct {p0, p1}, Lcom/android/server/DarkModeTimeModeManager;->updateButtonStatus(Landroid/content/Context;)V */
/* .line 291 */
/* new-instance v0, Lcom/android/server/DarkModeStauesEvent; */
/* invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V */
/* .line 292 */
final String v1 = "darkModeSuggest"; // const-string v1, "darkModeSuggest"
(( com.android.server.DarkModeStauesEvent ) v0 ).setEventName ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 293 */
final String v1 = ""; // const-string v1, ""
(( com.android.server.DarkModeStauesEvent ) v0 ).setTip ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
/* .line 294 */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.DarkModeStauesEvent ) v0 ).setSuggestEnable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestEnable(I)Lcom/android/server/DarkModeStauesEvent;
/* .line 295 */
/* .local v0, "event":Lcom/android/server/DarkModeStauesEvent; */
com.android.server.DarkModeOneTrackHelper .uploadToOneTrack ( p1,v0 );
/* .line 297 */
int v1 = 3; // const/4 v1, 0x3
com.android.server.DarkModeTimeModeHelper .setDarkModeSuggestCount ( p1,v1 );
/* .line 298 */
/* nop */
/* .line 299 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110f01a9 */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 298 */
int v2 = 0; // const/4 v2, 0x0
android.widget.Toast .makeText ( p1,v1,v2 );
/* .line 300 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 301 */
return;
} // .end method
