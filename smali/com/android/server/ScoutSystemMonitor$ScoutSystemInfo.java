public class com.android.server.ScoutSystemMonitor$ScoutSystemInfo {
	 /* .source "ScoutSystemMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ScoutSystemMonitor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "ScoutSystemInfo" */
} // .end annotation
/* # instance fields */
private java.lang.String mBinderTransInfo;
private java.lang.String mDescribeInfo;
private java.lang.String mDetails;
private Integer mEvent;
private java.util.ArrayList mHandlerChecks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsHalf;
private Integer mPreScoutLevel;
private Integer mScoutLevel;
private Long mTimeStamp;
private java.lang.String mUuid;
/* # direct methods */
public com.android.server.ScoutSystemMonitor$ScoutSystemInfo ( ) {
/* .locals 1 */
/* .param p1, "mDescribeInfo" # Ljava/lang/String; */
/* .param p2, "mDetails" # Ljava/lang/String; */
/* .param p3, "mScoutLevel" # I */
/* .param p4, "mPreScoutLevel" # I */
/* .param p6, "mIsHalf" # Z */
/* .param p7, "mUuid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "II", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;Z", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 325 */
/* .local p5, "mHandlerChecks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 326 */
this.mDescribeInfo = p1;
/* .line 327 */
this.mDetails = p2;
/* .line 328 */
/* iput p3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mScoutLevel:I */
/* .line 329 */
/* iput p4, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mPreScoutLevel:I */
/* .line 330 */
/* iput-boolean p6, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mIsHalf:Z */
/* .line 331 */
this.mHandlerChecks = p5;
/* .line 332 */
final String v0 = ""; // const-string v0, ""
this.mBinderTransInfo = v0;
/* .line 333 */
this.mUuid = p7;
/* .line 334 */
return;
} // .end method
private java.lang.String getFormatDateTime ( Long p0 ) {
/* .locals 4 */
/* .param p1, "timeMillis" # J */
/* .line 402 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p1, v0 */
/* if-gtz v0, :cond_0 */
/* const-string/jumbo v0, "unknow time" */
/* .line 403 */
} // :cond_0
/* new-instance v0, Ljava/util/Date; */
/* invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V */
/* .line 404 */
/* .local v0, "date":Ljava/util/Date; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy-MM-dd-HH-mm-ss" */
v3 = java.util.Locale.US;
/* invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
/* .line 405 */
/* .local v1, "dateFormat":Ljava/text/SimpleDateFormat; */
(( java.text.SimpleDateFormat ) v1 ).format ( v0 ); // invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
} // .end method
/* # virtual methods */
public java.lang.String getBinderTransInfo ( ) {
/* .locals 1 */
/* .line 361 */
v0 = this.mBinderTransInfo;
} // .end method
public java.lang.String getDescribeInfo ( ) {
/* .locals 1 */
/* .line 341 */
v0 = this.mDescribeInfo;
} // .end method
public java.lang.String getDetails ( ) {
/* .locals 1 */
/* .line 373 */
v0 = this.mDetails;
} // .end method
public Integer getEvent ( ) {
/* .locals 1 */
/* .line 381 */
/* iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mEvent:I */
} // .end method
public java.lang.String getEventString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "eventId" # I */
/* .line 409 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 425 */
final String v0 = "UNKNOW"; // const-string v0, "UNKNOW"
/* .line 417 */
/* :sswitch_0 */
final String v0 = "FW_SCOUT_SLOW"; // const-string v0, "FW_SCOUT_SLOW"
/* .line 415 */
/* :sswitch_1 */
final String v0 = "FW_SCOUT_NORMALLY"; // const-string v0, "FW_SCOUT_NORMALLY"
/* .line 413 */
/* :sswitch_2 */
final String v0 = "FW_SCOUT_BINDER_FULL"; // const-string v0, "FW_SCOUT_BINDER_FULL"
/* .line 411 */
/* :sswitch_3 */
final String v0 = "FW_SCOUT_HANG"; // const-string v0, "FW_SCOUT_HANG"
/* .line 423 */
/* :sswitch_4 */
final String v0 = "WATCHDOG_DUMP_ERROR"; // const-string v0, "WATCHDOG_DUMP_ERROR"
/* .line 419 */
/* :sswitch_5 */
final String v0 = "HALF_WATCHDOG"; // const-string v0, "HALF_WATCHDOG"
/* .line 421 */
/* :sswitch_6 */
final String v0 = "WATCHDOG"; // const-string v0, "WATCHDOG"
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_6 */
/* 0x180 -> :sswitch_5 */
/* 0x181 -> :sswitch_4 */
/* 0x190 -> :sswitch_3 */
/* 0x191 -> :sswitch_2 */
/* 0x192 -> :sswitch_1 */
/* 0x193 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public Boolean getHalfState ( ) {
/* .locals 1 */
/* .line 337 */
/* iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mIsHalf:Z */
} // .end method
public java.util.ArrayList getHandlerCheckers ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 353 */
v0 = this.mHandlerChecks;
} // .end method
public Integer getPreScoutLevel ( ) {
/* .locals 1 */
/* .line 349 */
/* iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mPreScoutLevel:I */
} // .end method
public Integer getScoutLevel ( ) {
/* .locals 1 */
/* .line 345 */
/* iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mScoutLevel:I */
} // .end method
public Long getTimeStamp ( ) {
/* .locals 2 */
/* .line 369 */
/* iget-wide v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mTimeStamp:J */
/* return-wide v0 */
} // .end method
public java.lang.String getUuid ( ) {
/* .locals 1 */
/* .line 384 */
v0 = this.mUuid;
} // .end method
public void setBinderTransInfo ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mBinderTransInfo" # Ljava/lang/String; */
/* .line 357 */
this.mBinderTransInfo = p1;
/* .line 358 */
return;
} // .end method
public void setEvent ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mEvent" # I */
/* .line 377 */
/* iput p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mEvent:I */
/* .line 378 */
return;
} // .end method
public void setTimeStamp ( Long p0 ) {
/* .locals 0 */
/* .param p1, "mTimeStamp" # J */
/* .line 365 */
/* iput-wide p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mTimeStamp:J */
/* .line 366 */
return;
} // .end method
public void setUuid ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "mUuid" # Ljava/lang/String; */
/* .line 387 */
this.mUuid = p1;
/* .line 388 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 391 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 392 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\nException \uff1a "; // const-string v2, "\nException \uff1a "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mEvent:I */
(( com.android.server.ScoutSystemMonitor$ScoutSystemInfo ) p0 ).getEventString ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getEventString(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\nTimeStamp : "; // const-string v2, "\nTimeStamp : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->mTimeStamp:J */
/* .line 393 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/ScoutSystemMonitor$ScoutSystemInfo;->getFormatDateTime(J)Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\nProcessName : system_server\nPid : "; // const-string v2, "\nProcessName : system_server\nPid : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 395 */
v2 = android.os.Process .myPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "\nSummary : "; // const-string v2, "\nSummary : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mDescribeInfo;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mBinderTransInfo;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 392 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 398 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
