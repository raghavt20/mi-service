.class public final Lcom/android/server/lights/MiuiLightsService$LocalService;
.super Lmiui/app/MiuiLightsManagerInternal;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method public static synthetic $r8$lambda$X6B-HUOeIuupprubnXCWR4CdkDo(Lcom/android/server/lights/MiuiLightsService$LocalService;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/lights/MiuiLightsService$LocalService;->lambda$setColorfulLight$0(Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/lights/MiuiLightsService;

    .line 538
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LocalService;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {p0}, Lmiui/app/MiuiLightsManagerInternal;-><init>()V

    return-void
.end method

.method private synthetic lambda$setColorfulLight$0(Ljava/lang/String;I)V
    .locals 4
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "styleType"    # I

    .line 542
    const-string v0, "LightsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setColorfulLight callingPkg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " styleType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LocalService;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLightStyleLoader(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/LightStyleLoader;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/lights/LightStyleLoader;->getLightStyle(I)Ljava/util/List;

    move-result-object v0

    .line 544
    .local v0, "lightStyle":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;"
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LocalService;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 545
    :try_start_0
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LocalService;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, p1, v3, p2, v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setColorfulLightLocked(Ljava/lang/String;IILjava/util/List;)V

    .line 546
    monitor-exit v1

    .line 547
    return-void

    .line 546
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method public getBinderService()Landroid/os/IBinder;
    .locals 1

    .line 552
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LocalService;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmService(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public setColorfulLight(Ljava/lang/String;II)V
    .locals 2
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "styleType"    # I
    .param p3, "userId"    # I

    .line 541
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LocalService;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLightHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/lights/MiuiLightsService$LocalService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/lights/MiuiLightsService$LocalService$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/lights/MiuiLightsService$LocalService;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 548
    return-void
.end method
