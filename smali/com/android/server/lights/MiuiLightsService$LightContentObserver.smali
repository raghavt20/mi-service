.class Lcom/android/server/lights/MiuiLightsService$LightContentObserver;
.super Landroid/database/ContentObserver;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LightContentObserver"
.end annotation


# instance fields
.field public final BATTERY_LIGHT_TURN_ON_URI:Landroid/net/Uri;

.field public final BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

.field public final CALL_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

.field public final LIGHT_TURN_ON_ENDTIME_URI:Landroid/net/Uri;

.field public final LIGHT_TURN_ON_STARTTIME_URI:Landroid/net/Uri;

.field public final LIGHT_TURN_ON_TIME_URI:Landroid/net/Uri;

.field public final LIGHT_TURN_ON_URI:Landroid/net/Uri;

.field public final MMS_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

.field public final MUSIC_LIGHT_TURN_ON_URI:Landroid/net/Uri;

.field public final NOTIFICATION_LIGHT_TURN_ON_URI:Landroid/net/Uri;

.field public final SCREEN_BUTTONS_STATE_URI:Landroid/net/Uri;

.field public final SCREEN_BUTTONS_TURN_ON_URI:Landroid/net/Uri;

.field public final SINGLE_KEY_USE_ACTION_URI:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method public constructor <init>(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    .line 850
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    .line 851
    invoke-static {p1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLightHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 836
    const-string p1, "screen_buttons_state"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SCREEN_BUTTONS_STATE_URI:Landroid/net/Uri;

    .line 837
    const-string/jumbo p1, "single_key_use_enable"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SINGLE_KEY_USE_ACTION_URI:Landroid/net/Uri;

    .line 838
    const-string p1, "screen_buttons_turn_on"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SCREEN_BUTTONS_TURN_ON_URI:Landroid/net/Uri;

    .line 839
    const-string p1, "breathing_light_color"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    .line 840
    const-string p1, "call_breathing_light_color"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->CALL_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    .line 841
    const-string p1, "mms_breathing_light_color"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->MMS_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    .line 842
    const-string p1, "battery_light_turn_on"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->BATTERY_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    .line 843
    const-string p1, "notification_light_turn_on"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->NOTIFICATION_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    .line 844
    const-string p1, "light_turn_on"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_URI:Landroid/net/Uri;

    .line 845
    const-string p1, "light_turn_on_Time"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_TIME_URI:Landroid/net/Uri;

    .line 846
    const-string p1, "light_turn_on_startTime"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_STARTTIME_URI:Landroid/net/Uri;

    .line 847
    const-string p1, "light_turn_on_endTime"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_ENDTIME_URI:Landroid/net/Uri;

    .line 848
    const-string p1, "music_light_turn_on"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->MUSIC_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    .line 852
    return-void
.end method


# virtual methods
.method public observe()V
    .locals 4

    .line 855
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmSupportButtonLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eqz v0, :cond_1

    .line 856
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SCREEN_BUTTONS_STATE_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 858
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmSupportTapFingerprint(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SINGLE_KEY_USE_ACTION_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 862
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SCREEN_BUTTONS_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 866
    :cond_1
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmSupportLedLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 867
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 869
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->CALL_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 871
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->MMS_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 873
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->BATTERY_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 875
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->NOTIFICATION_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 877
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 879
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_TIME_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 881
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->MUSIC_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 883
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_STARTTIME_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 885
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_ENDTIME_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 888
    :cond_2
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 20
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 891
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const/4 v2, 0x0

    .line 892
    .local v2, "light":Lcom/android/server/lights/MiuiLightsService$LightImpl;
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SCREEN_BUTTONS_STATE_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SINGLE_KEY_USE_ACTION_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->SCREEN_BUTTONS_TURN_ON_URI:Landroid/net/Uri;

    .line 893
    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_2

    .line 896
    :cond_0
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x4

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->CALL_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    .line 897
    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->MMS_BREATHING_LIGHT_COLOR_URI:Landroid/net/Uri;

    .line 898
    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto/16 :goto_1

    .line 901
    :cond_1
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->BATTERY_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 902
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget-object v3, v3, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    move-object v2, v3

    check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 903
    invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    goto/16 :goto_3

    .line 904
    :cond_2
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->NOTIFICATION_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 905
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget-object v3, v3, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    aget-object v3, v3, v4

    move-object v2, v3

    check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 906
    invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    goto/16 :goto_3

    .line 907
    :cond_3
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 908
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 909
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mregisterAudioPlaybackCallback(Lcom/android/server/lights/MiuiLightsService;)V

    .line 910
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnMusicLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmAudioManager(Lcom/android/server/lights/MiuiLightsService;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 911
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mturnoffBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 912
    invoke-static {}, Lcom/android/server/lights/VisualizerHolder;->getInstance()Lcom/android/server/lights/VisualizerHolder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mgetDataCaptureListener(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V

    goto :goto_0

    .line 915
    :cond_4
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V

    .line 916
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$munregisterAudioPlaybackCallback(Lcom/android/server/lights/MiuiLightsService;)V

    .line 918
    :cond_5
    :goto_0
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateLightState(Lcom/android/server/lights/MiuiLightsService;)V

    goto/16 :goto_3

    .line 919
    :cond_6
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->MUSIC_LIGHT_TURN_ON_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 920
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmMusicLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 921
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnMusicLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 922
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V

    .line 923
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$munregisterAudioPlaybackCallback(Lcom/android/server/lights/MiuiLightsService;)V

    goto/16 :goto_3

    .line 924
    :cond_7
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-eqz v3, :cond_14

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnMusicLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 925
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mregisterAudioPlaybackCallback(Lcom/android/server/lights/MiuiLightsService;)V

    .line 926
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmAudioManager(Lcom/android/server/lights/MiuiLightsService;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 927
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mturnoffBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 928
    invoke-static {}, Lcom/android/server/lights/VisualizerHolder;->getInstance()Lcom/android/server/lights/VisualizerHolder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mgetDataCaptureListener(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V

    goto/16 :goto_3

    .line 931
    :cond_8
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_TIME_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "light_turn_on_endTime"

    const-string v5, "light_turn_on_startTime"

    const-wide/32 v6, 0x4ef6d80

    const/4 v8, -0x2

    const-wide/32 v9, 0x1808580

    if-eqz v3, :cond_a

    .line 932
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-virtual {v3}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnTimeLight()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 933
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v11

    invoke-static {v11, v5, v9, v10, v8}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v9

    invoke-static {v3, v9, v10}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputlight_start_time(Lcom/android/server/lights/MiuiLightsService;J)V

    .line 935
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v4, v6, v7, v8}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputlight_end_time(Lcom/android/server/lights/MiuiLightsService;J)V

    .line 937
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateWorkState(Lcom/android/server/lights/MiuiLightsService;)V

    .line 938
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateLightState(Lcom/android/server/lights/MiuiLightsService;)V

    goto/16 :goto_3

    .line 939
    :cond_9
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 940
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputmIsWorkTime(Lcom/android/server/lights/MiuiLightsService;Z)V

    .line 941
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateLightState(Lcom/android/server/lights/MiuiLightsService;)V

    goto/16 :goto_3

    .line 943
    :cond_a
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_STARTTIME_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-wide/32 v13, 0xea60

    const-string v15, " minute:"

    const-string v6, "onChange hour:"

    const-string v7, "LightsService"

    const-wide/16 v16, 0x0

    const-wide/32 v18, 0x36ee80

    if-eqz v3, :cond_e

    .line 944
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v5, v9, v10, v8}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v11

    invoke-static {v3, v11, v12}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputlight_start_time(Lcom/android/server/lights/MiuiLightsService;J)V

    .line 946
    sget-boolean v3, Lcom/android/server/lights/LightsService;->DEBUG:Z

    if-eqz v3, :cond_b

    .line 947
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_start_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v11

    div-long v11, v11, v18

    invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_start_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v11

    rem-long v11, v11, v18

    div-long/2addr v11, v13

    invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    :cond_b
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_start_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v3

    cmp-long v3, v3, v16

    if-ltz v3, :cond_c

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_start_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v3

    const-wide/32 v6, 0x5265c00

    cmp-long v3, v3, v6

    if-lez v3, :cond_d

    .line 949
    :cond_c
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmContext(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v5, v9, v10}, Landroid/provider/Settings$Secure;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 951
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3, v9, v10}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputlight_start_time(Lcom/android/server/lights/MiuiLightsService;J)V

    .line 953
    :cond_d
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateWorkState(Lcom/android/server/lights/MiuiLightsService;)V

    .line 954
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateLightState(Lcom/android/server/lights/MiuiLightsService;)V

    goto/16 :goto_3

    .line 955
    :cond_e
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->LIGHT_TURN_ON_ENDTIME_URI:Landroid/net/Uri;

    invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 956
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v5

    const-wide/32 v9, 0x4ef6d80

    invoke-static {v5, v4, v9, v10, v8}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v11

    invoke-static {v3, v11, v12}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputlight_end_time(Lcom/android/server/lights/MiuiLightsService;J)V

    .line 958
    sget-boolean v3, Lcom/android/server/lights/LightsService;->DEBUG:Z

    if-eqz v3, :cond_f

    .line 959
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v5}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_end_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v5

    div-long v5, v5, v18

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v5}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_end_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v5

    rem-long v5, v5, v18

    div-long/2addr v5, v13

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    :cond_f
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_end_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v5

    cmp-long v3, v5, v16

    if-ltz v3, :cond_10

    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlight_end_time(Lcom/android/server/lights/MiuiLightsService;)J

    move-result-wide v5

    const-wide/32 v7, 0x5265c00

    cmp-long v3, v5, v7

    if-lez v3, :cond_11

    .line 961
    :cond_10
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmContext(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-wide/32 v5, 0x4ef6d80

    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$Secure;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 963
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3, v5, v6}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputlight_end_time(Lcom/android/server/lights/MiuiLightsService;J)V

    .line 965
    :cond_11
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateWorkState(Lcom/android/server/lights/MiuiLightsService;)V

    .line 966
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateLightState(Lcom/android/server/lights/MiuiLightsService;)V

    goto :goto_3

    .line 899
    :cond_12
    :goto_1
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget-object v3, v3, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    aget-object v3, v3, v4

    move-object v2, v3

    check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 900
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setFlashing(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 894
    :cond_13
    :goto_2
    iget-object v3, v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget-object v3, v3, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    move-object v2, v3

    check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 895
    invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 968
    :cond_14
    :goto_3
    return-void
.end method
