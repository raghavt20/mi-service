.class public Lcom/android/server/lights/LedDataCaptureListener;
.super Ljava/lang/Object;
.source "LedDataCaptureListener.java"

# interfaces
.implements Lcom/android/server/lights/MiuiLightsService$DataCaptureListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    const-class v0, Lcom/android/server/lights/LedDataCaptureListener;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/lights/LedDataCaptureListener;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method


# virtual methods
.method public onFrequencyCapture(Landroid/content/Context;I[F)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "magnitude_max"    # I
    .param p3, "frequencies"    # [F

    .line 19
    return-void
.end method

.method public onSetLightCallback(Landroid/content/Context;IIIIII)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "lightId"    # I
    .param p3, "color"    # I
    .param p4, "mode"    # I
    .param p5, "onMS"    # I
    .param p6, "offMS"    # I
    .param p7, "brightnessMode"    # I

    .line 16
    return-void
.end method
