public class com.android.server.lights.MiuiLightsService$LightImpl extends com.android.server.lights.LightsService$LightImpl {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/lights/MiuiLightsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "LightImpl" */
} // .end annotation
/* # instance fields */
private java.util.List lightStates;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mBrightnessMode;
private Integer mColor;
private Boolean mDisabled;
private Integer mId;
private Boolean mIsShutDown;
private Integer mLastColor;
public Integer mLastLightStyle;
private Integer mMode;
private Integer mOffMS;
private Integer mOnMS;
private Integer mUid;
private java.lang.String pkg_name;
final com.android.server.lights.MiuiLightsService this$0; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmBrightnessMode ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I */
} // .end method
static Integer -$$Nest$fgetmColor ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
} // .end method
static Boolean -$$Nest$fgetmDisabled ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
} // .end method
static Integer -$$Nest$fgetmId ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
} // .end method
static Integer -$$Nest$fgetmMode ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I */
} // .end method
static Integer -$$Nest$fgetmOffMS ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I */
} // .end method
static Integer -$$Nest$fgetmOnMS ( com.android.server.lights.MiuiLightsService$LightImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I */
} // .end method
static void -$$Nest$mrealSetLightLocked ( com.android.server.lights.MiuiLightsService$LightImpl p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V */
return;
} // .end method
static void -$$Nest$msetColorCommonLocked ( com.android.server.lights.MiuiLightsService$LightImpl p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setColorCommonLocked(IIIII)V */
return;
} // .end method
private com.android.server.lights.MiuiLightsService$LightImpl ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/lights/MiuiLightsService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "mFakeId" # I */
/* .line 225 */
this.this$0 = p1;
/* .line 226 */
v0 = this.mLightsById;
int v1 = 4; // const/4 v1, 0x4
(( android.util.SparseArray ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/lights/LightsService$LightImpl; */
v0 = this.mHwLight;
/* invoke-direct {p0, p1, p2, v0}, Lcom/android/server/lights/LightsService$LightImpl;-><init>(Lcom/android/server/lights/LightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V */
/* .line 214 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
/* .line 216 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z */
/* .line 227 */
/* iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* .line 228 */
int v1 = 2; // const/4 v1, 0x2
/* if-eq p3, v1, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne p3, v1, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
/* .line 230 */
return;
} // .end method
 com.android.server.lights.MiuiLightsService$LightImpl ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;I)V */
return;
} // .end method
private com.android.server.lights.MiuiLightsService$LightImpl ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/lights/MiuiLightsService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "hwLight" # Landroid/hardware/light/HwLight; */
/* .line 218 */
this.this$0 = p1;
/* .line 219 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/LightsService$LightImpl;-><init>(Lcom/android/server/lights/LightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V */
/* .line 214 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
/* .line 216 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z */
/* .line 220 */
/* iget v1, p3, Landroid/hardware/light/HwLight;->id:I */
/* iput v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* .line 221 */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v1, v2, :cond_0 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
/* .line 223 */
return;
} // .end method
 com.android.server.lights.MiuiLightsService$LightImpl ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V */
return;
} // .end method
private void realSetLightLocked ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "color" # I */
/* .param p2, "mode" # I */
/* .param p3, "onMS" # I */
/* .param p4, "offMS" # I */
/* .param p5, "brightnessMode" # I */
/* .line 374 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 375 */
int p1 = 0; // const/4 p1, 0x0
/* .line 378 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* const/16 v1, 0x8 */
/* if-eq v0, v1, :cond_6 */
/* const/16 v1, 0x9 */
/* if-ne v0, v1, :cond_1 */
/* goto/16 :goto_1 */
/* .line 383 */
} // :cond_1
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_2 */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_5 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastColor:I */
/* if-eq v0, p1, :cond_5 */
/* .line 385 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "realSetLightLocked #"; // const-string v1, "realSetLightLocked #"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ": color=#"; // const-string v1, ": color=#"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toHexString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ": onMS="; // const-string v1, ": onMS="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " offMS="; // const-string v1, " offMS="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " mode="; // const-string v1, " mode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LightsService"; // const-string v1, "LightsService"
android.util.Slog .v ( v1,v0 );
/* .line 387 */
v7 = this.this$0;
/* new-instance v8, Lcom/android/server/lights/LightState; */
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* move-object v0, v8 */
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/lights/LightState;-><init>(IIIIII)V */
com.android.server.lights.MiuiLightsService .-$$Nest$maddToLightCollectionLocked ( v7,v8 );
/* .line 388 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLedEvents ( v0 );
/* if-nez v0, :cond_3 */
/* .line 389 */
v0 = this.this$0;
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
com.android.server.lights.MiuiLightsService .-$$Nest$fputmLedEvents ( v0,v1 );
/* .line 391 */
} // :cond_3
/* if-nez p1, :cond_4 */
/* .line 392 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmIsLedTurnOn ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 393 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.lights.MiuiLightsService .-$$Nest$fputmIsLedTurnOn ( v0,v1 );
/* .line 394 */
v0 = this.this$0;
/* iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
com.android.server.lights.MiuiLightsService .-$$Nest$mreportLedEventLocked ( v0,v2,v1,p3,p4 );
/* .line 397 */
} // :cond_4
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmIsLedTurnOn ( v0 );
/* if-nez v0, :cond_5 */
/* .line 398 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.lights.MiuiLightsService .-$$Nest$fputmIsLedTurnOn ( v0,v1 );
/* .line 399 */
v0 = this.this$0;
/* iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
com.android.server.lights.MiuiLightsService .-$$Nest$mreportLedEventLocked ( v0,v2,v1,p3,p4 );
/* .line 403 */
} // :cond_5
} // :goto_0
/* move-object v0, p0 */
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* invoke-super/range {v0 ..v5}, Lcom/android/server/lights/LightsService$LightImpl;->setLightLocked(IIIII)V */
/* .line 404 */
/* iput p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastColor:I */
/* .line 405 */
return;
/* .line 379 */
} // :cond_6
} // :goto_1
/* move-object v0, p0 */
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
/* invoke-super/range {v0 ..v5}, Lcom/android/server/lights/LightsService$LightImpl;->setLightLocked(IIIII)V */
/* .line 380 */
return;
} // .end method
private void setColorCommonLocked ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 0 */
/* .param p1, "color" # I */
/* .param p2, "mode" # I */
/* .param p3, "onMS" # I */
/* .param p4, "offMS" # I */
/* .param p5, "brightnessMode" # I */
/* .line 412 */
/* invoke-super/range {p0 ..p5}, Lcom/android/server/lights/LightsService$LightImpl;->setLightLocked(IIIII)V */
/* .line 413 */
return;
} // .end method
private void updateState ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 0 */
/* .param p1, "color" # I */
/* .param p2, "mode" # I */
/* .param p3, "onMS" # I */
/* .param p4, "offMS" # I */
/* .param p5, "brightnessMode" # I */
/* .line 416 */
/* iput p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
/* .line 417 */
/* iput p2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I */
/* .line 418 */
/* iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I */
/* .line 419 */
/* iput p4, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I */
/* .line 420 */
/* iput p5, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I */
/* .line 421 */
return;
} // .end method
private void updateState ( java.lang.String p0, Integer p1, Integer p2, java.util.List p3 ) {
/* .locals 0 */
/* .param p1, "pkg_name" # Ljava/lang/String; */
/* .param p2, "mUid" # I */
/* .param p3, "styleType" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "II", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 424 */
/* .local p4, "lightStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;" */
this.pkg_name = p1;
/* .line 425 */
/* iput p2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mUid:I */
/* .line 426 */
/* iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
/* .line 427 */
this.lightStates = p4;
/* .line 428 */
return;
} // .end method
/* # virtual methods */
public void pulse ( ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0}, Lcom/android/server/lights/LightsService$LightImpl;->pulse()V */
return;
} // .end method
public void pulse ( Integer p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/lights/LightsService$LightImpl;->pulse(II)V */
return;
} // .end method
public void setBrightness ( Float p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0, p1}, Lcom/android/server/lights/LightsService$LightImpl;->setBrightness(F)V */
return;
} // .end method
public void setBrightness ( Float p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/lights/LightsService$LightImpl;->setBrightness(FI)V */
return;
} // .end method
public void setBrightness ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "brightness" # I */
/* .param p2, "isShutDown" # Z */
/* .line 368 */
/* iput-boolean p2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z */
/* .line 369 */
/* int-to-float v0, p1 */
/* invoke-super {p0, v0}, Lcom/android/server/lights/LightsService$LightImpl;->setBrightness(F)V */
/* .line 370 */
return;
} // .end method
public void setColor ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0, p1}, Lcom/android/server/lights/LightsService$LightImpl;->setColor(I)V */
return;
} // .end method
void setColorfulLightLocked ( java.lang.String p0, Integer p1, Integer p2, java.util.List p3 ) {
/* .locals 3 */
/* .param p1, "pkg_name" # Ljava/lang/String; */
/* .param p2, "mUid" # I */
/* .param p3, "styleType" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "II", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 330 */
/* .local p4, "lightStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;" */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* const/16 v1, 0x8 */
final String v2 = "LightsService"; // const-string v2, "LightsService"
/* if-ne v0, v1, :cond_6 */
/* if-nez p4, :cond_0 */
/* .line 335 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misLightEnable ( v0 );
/* if-nez v0, :cond_1 */
/* .line 336 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(Ljava/lang/String;IILjava/util/List;)V */
/* .line 337 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mdoCancelColorfulLightLocked ( v0 );
/* .line 338 */
return;
/* .line 342 */
} // :cond_1
v0 = this.this$0;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misSceneUncomfort ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 343 */
final String v0 = "Scene is uncomfort , lightstyle phone skip"; // const-string v0, "Scene is uncomfort , lightstyle phone skip"
android.util.Slog .i ( v2,v0 );
/* .line 344 */
return;
/* .line 348 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
int v1 = 1; // const/4 v1, 0x1
v0 = /* if-eq v0, v1, :cond_4 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 357 */
} // :cond_3
v0 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.lights.MiuiLightsService .-$$Nest$mreportLedEventLocked ( v0,p3,v1,v2,v2 );
/* .line 359 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mdoCancelColorfulLightLocked ( v0 );
/* .line 360 */
v0 = this.this$0;
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightsThread; */
v2 = this.this$0;
/* invoke-direct {v1, v2, p4, p3, p2}, Lcom/android/server/lights/MiuiLightsService$LightsThread;-><init>(Lcom/android/server/lights/MiuiLightsService;Ljava/util/List;II)V */
com.android.server.lights.MiuiLightsService .-$$Nest$fputmThread ( v0,v1 );
/* .line 361 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmThread ( v0 );
(( com.android.server.lights.MiuiLightsService$LightsThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->start()V
/* .line 362 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(Ljava/lang/String;IILjava/util/List;)V */
/* .line 363 */
v0 = this.this$0;
/* new-instance v1, Lcom/android/server/lights/LightState; */
/* invoke-direct {v1, p1, p3}, Lcom/android/server/lights/LightState;-><init>(Ljava/lang/String;I)V */
com.android.server.lights.MiuiLightsService .-$$Nest$maddToLightCollectionLocked ( v0,v1 );
/* .line 364 */
return;
/* .line 349 */
} // :cond_4
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
/* if-ne p3, v0, :cond_5 */
/* .line 350 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(Ljava/lang/String;IILjava/util/List;)V */
/* .line 351 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mdoCancelColorfulLightLocked ( v0 );
/* .line 352 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mrecoveryBatteryLight ( v0 );
/* .line 354 */
} // :cond_5
return;
/* .line 331 */
} // :cond_6
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Illegal Argument mLastLightStyle:"; // const-string v1, "Illegal Argument mLastLightStyle:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " lightStates:"; // const-string v1, " lightStates:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v0 );
/* .line 332 */
return;
} // .end method
public void setFlashing ( Integer p0, Integer p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/lights/LightsService$LightImpl;->setFlashing(IIII)V */
return;
} // .end method
void setFlashing ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "colorSettingKey" # Ljava/lang/String; */
/* .param p2, "freqSettingKey" # Ljava/lang/String; */
/* .line 233 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11060002 */
v0 = (( android.content.res.Resources ) v0 ).getColor ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I
/* .line 235 */
/* .local v0, "defaultColor":I */
v1 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v1 );
int v2 = -2; // const/4 v2, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,p1,v0,v2 );
/* .line 238 */
/* .local v1, "color":I */
/* const/16 v2, 0x1f4 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.lights.MiuiLightsService$LightImpl ) p0 ).setFlashing ( v1, v4, v2, v3 ); // invoke-virtual {p0, v1, v4, v2, v3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setFlashing(IIII)V
/* .line 240 */
v2 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmHandler ( v2 );
(( android.os.Handler ) v2 ).removeMessages ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 241 */
v2 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmHandler ( v2 );
v3 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmHandler ( v3 );
android.os.Message .obtain ( v3,v4,p0 );
/* const-wide/16 v4, 0x1f4 */
(( android.os.Handler ) v2 ).sendMessageDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 242 */
return;
} // .end method
void setLightLocked ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 10 */
/* .param p1, "color" # I */
/* .param p2, "mode" # I */
/* .param p3, "onMS" # I */
/* .param p4, "offMS" # I */
/* .param p5, "brightnessMode" # I */
/* .line 265 */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* if-nez v0, :cond_0 */
/* .line 266 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V */
/* .line 267 */
return;
/* .line 270 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misLightEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_c
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* goto/16 :goto_2 */
/* .line 277 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* const/16 v1, 0x9 */
int v2 = 3; // const/4 v2, 0x3
int v3 = 4; // const/4 v3, 0x4
/* if-eq v0, v2, :cond_2 */
/* if-eq v0, v3, :cond_2 */
/* if-ne v0, v1, :cond_6 */
/* .line 281 */
} // :cond_2
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmColorfulLight ( v0 );
/* iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v0, v4, :cond_b */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* if-eq v0, v1, :cond_3 */
v0 = this.this$0;
/* .line 282 */
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misMusicLightPlaying ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* goto/16 :goto_1 */
/* .line 287 */
} // :cond_3
v0 = this.this$0;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misSceneUncomfort ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 288 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V */
/* .line 289 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* move-object v4, p0 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V */
/* .line 290 */
return;
/* .line 294 */
} // :cond_4
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* if-ne v0, v2, :cond_6 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmBatteryManagerInternal ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 295 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmBatteryManagerInternal ( v0 );
v0 = (( android.os.BatteryManagerInternal ) v0 ).getBatteryLevel ( ); // invoke-virtual {v0}, Landroid/os/BatteryManagerInternal;->getBatteryLevel()I
/* const/16 v1, 0x64 */
/* if-ne v0, v1, :cond_5 */
/* .line 296 */
int p1 = 0; // const/4 p1, 0x0
/* .line 298 */
} // :cond_5
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmBatteryManagerInternal ( v0 );
/* const/16 v1, 0xf */
v0 = (( android.os.BatteryManagerInternal ) v0 ).isPowered ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/BatteryManagerInternal;->isPowered(I)Z
/* if-nez v0, :cond_6 */
if ( p1 != null) { // if-eqz p1, :cond_6
/* .line 300 */
int p1 = 0; // const/4 p1, 0x0
/* .line 306 */
} // :cond_6
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* if-ne v0, v3, :cond_7 */
/* .line 307 */
if ( p1 != null) { // if-eqz p1, :cond_9
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
/* if-nez v0, :cond_9 */
/* .line 308 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mturnoffBatteryLight ( v0 );
/* .line 310 */
} // :cond_7
/* if-ne v0, v2, :cond_9 */
/* .line 311 */
v0 = this.this$0;
v0 = this.mLightsByType;
/* aget-object v0, v0, v3 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
/* if-nez v0, :cond_8 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmColorfulLight ( v0 );
/* iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_9 */
/* .line 313 */
} // :cond_8
/* move-object v4, p0 */
/* move v5, p1 */
/* move v6, p2 */
/* move v7, p3 */
/* move v8, p4 */
/* move v9, p5 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V */
/* .line 314 */
return;
/* .line 318 */
} // :cond_9
} // :goto_0
/* move-object v4, p0 */
/* move v5, p1 */
/* move v6, p2 */
/* move v7, p3 */
/* move v8, p4 */
/* move v9, p5 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V */
/* .line 319 */
/* invoke-direct/range {v4 ..v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V */
/* .line 321 */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
/* if-ne v0, v3, :cond_a */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
/* if-nez v0, :cond_a */
/* .line 322 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mrecoveryBatteryLight ( v0 );
/* .line 324 */
} // :cond_a
return;
/* .line 283 */
} // :cond_b
} // :goto_1
return;
/* .line 271 */
} // :cond_c
} // :goto_2
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V */
/* .line 272 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object v1, p0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V */
/* .line 273 */
return;
} // .end method
public void setVrMode ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0, p1}, Lcom/android/server/lights/LightsService$LightImpl;->setVrMode(Z)V */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 432 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "LightImpl{mDisabled="; // const-string v1, "LightImpl{mDisabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", mColor="; // const-string v1, ", mColor="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mMode="; // const-string v1, ", mMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mOnMS="; // const-string v1, ", mOnMS="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mOffMS="; // const-string v1, ", mOffMS="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mBrightnessMode="; // const-string v1, ", mBrightnessMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mId="; // const-string v1, ", mId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mLastColor="; // const-string v1, ", mLastColor="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastColor:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", pkg_name=\'"; // const-string v1, ", pkg_name=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.pkg_name;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", mUid="; // const-string v1, ", mUid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mLastLightStyle="; // const-string v1, ", mLastLightStyle="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mIsShutDown="; // const-string v1, ", mIsShutDown="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void turnOff ( ) { //bridge//synthethic
/* .locals 0 */
/* .line 203 */
/* invoke-super {p0}, Lcom/android/server/lights/LightsService$LightImpl;->turnOff()V */
return;
} // .end method
void updateLight ( ) {
/* .locals 7 */
/* .line 245 */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I */
int v1 = 1; // const/4 v1, 0x1
int v2 = 2; // const/4 v2, 0x2
/* if-ne v2, v0, :cond_2 */
/* .line 246 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misDisableButtonLight ( v0 );
/* if-nez v0, :cond_1 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnButtonLight ( v0 );
/* if-nez v0, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
/* .line 247 */
} // :cond_2
int v2 = 3; // const/4 v2, 0x3
/* if-ne v2, v0, :cond_3 */
/* .line 248 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnBatteryLight ( v0 );
/* xor-int/2addr v0, v1 */
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
/* .line 249 */
} // :cond_3
int v2 = 4; // const/4 v2, 0x4
/* if-ne v2, v0, :cond_4 */
/* .line 250 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnNotificationLight ( v0 );
/* xor-int/2addr v0, v1 */
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
/* .line 251 */
} // :cond_4
/* const/16 v2, 0x9 */
/* if-ne v2, v0, :cond_5 */
/* .line 252 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnMusicLight ( v0 );
/* xor-int/2addr v0, v1 */
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z */
/* .line 253 */
} // :cond_5
/* const/16 v1, 0x8 */
/* if-ne v1, v0, :cond_6 */
/* .line 254 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 255 */
try { // :try_start_0
v1 = this.pkg_name;
/* iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mUid:I */
/* iget v3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
v4 = this.lightStates;
(( com.android.server.lights.MiuiLightsService$LightImpl ) p0 ).setColorfulLightLocked ( v1, v2, v3, v4 ); // invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setColorfulLightLocked(Ljava/lang/String;IILjava/util/List;)V
/* .line 256 */
/* monitor-exit v0 */
/* .line 257 */
return;
/* .line 256 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 259 */
} // :cond_6
} // :goto_1
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 260 */
try { // :try_start_1
/* iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I */
/* iget v3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I */
/* iget v4, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I */
/* iget v5, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I */
/* iget v6, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I */
/* move-object v1, p0 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setLightLocked(IIIII)V */
/* .line 261 */
/* monitor-exit v0 */
/* .line 262 */
return;
/* .line 261 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v1 */
} // .end method
