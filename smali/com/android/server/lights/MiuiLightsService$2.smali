.class Lcom/android/server/lights/MiuiLightsService$2;
.super Lmiui/lights/ILightsManager$Stub;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method public static synthetic $r8$lambda$Ix4SXNyoZBdBCkx4iMcw-2rkGfw(Lcom/android/server/lights/MiuiLightsService$2;Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$2;->lambda$setColorLed$1(Ljava/lang/String;III)V

    return-void
.end method

.method public static synthetic $r8$lambda$VMBVX92WU__OFgQgL2yGGbSVAUU(Lcom/android/server/lights/MiuiLightsService$2;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$2;->lambda$setColorCommon$0(Ljava/lang/String;II)V

    return-void
.end method

.method constructor <init>(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/lights/MiuiLightsService;

    .line 492
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {p0}, Lmiui/lights/ILightsManager$Stub;-><init>()V

    return-void
.end method

.method private synthetic lambda$setColorCommon$0(Ljava/lang/String;II)V
    .locals 8
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "styleType"    # I
    .param p3, "color"    # I

    .line 503
    const-string v0, "LightsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setColorCommon callingPkg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " styleType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " color:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 506
    const/4 v1, 0x7

    if-ne p2, v1, :cond_0

    .line 507
    :try_start_0
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPrivacyLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v3, p3

    invoke-static/range {v2 .. v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 510
    :cond_0
    monitor-exit v0

    .line 511
    return-void

    .line 510
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private synthetic lambda$setColorLed$1(Ljava/lang/String;III)V
    .locals 8
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "styleType"    # I
    .param p3, "color"    # I
    .param p4, "category"    # I

    .line 518
    const-string v0, "LightsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setColorLed callingPkg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " styleType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " color:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 521
    const/16 v1, 0x8

    if-ne p2, v1, :cond_2

    .line 522
    const/4 v1, 0x1

    if-eq p4, v1, :cond_1

    const/4 v1, 0x2

    if-eq p4, v1, :cond_1

    const/4 v1, 0x3

    if-eq p4, v1, :cond_1

    const/4 v1, 0x4

    if-ne p4, v1, :cond_0

    goto :goto_0

    .line 529
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLedLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v2

    const/4 v4, 0x1

    const/16 v5, 0x1f4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v3, p3

    invoke-static/range {v2 .. v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    goto :goto_1

    .line 526
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLedLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v3, p3

    invoke-static/range {v2 .. v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 533
    :cond_2
    :goto_1
    monitor-exit v0

    .line 534
    return-void

    .line 533
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public setColorCommon(ILjava/lang/String;II)V
    .locals 2
    .param p1, "color"    # I
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "styleType"    # I
    .param p4, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 502
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLightHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/lights/MiuiLightsService$2;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 512
    return-void
.end method

.method public setColorLed(ILjava/lang/String;III)V
    .locals 8
    .param p1, "color"    # I
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "styleType"    # I
    .param p4, "userId"    # I
    .param p5, "category"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 517
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLightHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v7, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p2

    move v4, p3

    move v5, p1

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/lights/MiuiLightsService$2;Ljava/lang/String;III)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 535
    return-void
.end method

.method public setColorfulLight(Ljava/lang/String;II)V
    .locals 1
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "styleType"    # I
    .param p3, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 495
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0, p1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mcheckCallerVerify(Lcom/android/server/lights/MiuiLightsService;Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$2;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetlocalService(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LocalService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$LocalService;->setColorfulLight(Ljava/lang/String;II)V

    .line 497
    return-void
.end method
