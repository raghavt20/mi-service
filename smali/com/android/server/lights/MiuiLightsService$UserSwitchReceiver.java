class com.android.server.lights.MiuiLightsService$UserSwitchReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/lights/MiuiLightsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "UserSwitchReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.lights.MiuiLightsService this$0; //synthetic
/* # direct methods */
private com.android.server.lights.MiuiLightsService$UserSwitchReceiver ( ) {
/* .locals 0 */
/* .line 792 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.lights.MiuiLightsService$UserSwitchReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver;-><init>(Lcom/android/server/lights/MiuiLightsService;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 795 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmSupportButtonLight ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 796 */
	 v0 = this.this$0;
	 v0 = this.mLightsByType;
	 int v1 = 2; // const/4 v1, 0x2
	 /* aget-object v0, v0, v1 */
	 /* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
	 (( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
	 /* .line 798 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmSupportLedLight ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 799 */
	 v0 = this.this$0;
	 v0 = this.mLightsByType;
	 int v1 = 3; // const/4 v1, 0x3
	 /* aget-object v0, v0, v1 */
	 /* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
	 (( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
	 /* .line 801 */
} // :cond_1
return;
} // .end method
