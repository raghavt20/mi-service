class com.android.server.lights.MiuiLightsService$3 implements com.android.server.lights.VisualizerHolder$OnDataCaptureListener {
	 /* .source "MiuiLightsService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/lights/MiuiLightsService;->getDataCaptureListener()Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.lights.MiuiLightsService this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$CNkw5v5AKShOjgRWRJ0qs0K2C6w ( com.android.server.lights.MiuiLightsService$3 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService$3;->lambda$onFrequencyCapture$0()V */
return;
} // .end method
 com.android.server.lights.MiuiLightsService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/lights/MiuiLightsService; */
/* .line 712 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$onFrequencyCapture$0 ( ) { //synthethic
/* .locals 3 */
/* .line 721 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmAudioManager ( v0 );
v0 = (( android.media.AudioManager ) v0 ).isMusicActive ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z
/* if-nez v0, :cond_0 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "/proc/"; // const-string v2, "/proc/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmPlayingPid ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 722 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_0 */
/* .line 723 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mreleaseVisualizer ( v0 );
/* .line 725 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void onFrequencyCapture ( Integer p0, Float[] p1 ) {
/* .locals 8 */
/* .param p1, "frequency" # I */
/* .param p2, "frequencies" # [F */
/* .line 715 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 716 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmMusicLight ( v1 );
/* const/high16 v1, -0x1000000 */
/* or-int v3, p1, v1 */
int v4 = 1; // const/4 v4, 0x1
/* const/16 v5, 0x64 */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setLightLocked(IIIII)V */
/* .line 718 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 719 */
/* if-nez p1, :cond_0 */
/* .line 720 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLightHandler ( v0 );
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$3$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/lights/MiuiLightsService$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/lights/MiuiLightsService$3;)V */
/* const-wide/16 v2, 0xbb8 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 727 */
} // :cond_0
return;
/* .line 718 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
