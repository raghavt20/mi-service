.class Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeTickReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method private constructor <init>(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    .line 807
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 810
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmSupportColorGameLed(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnTimeLight()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmIsWorkTime(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 811
    return-void

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mupdateWorkState(Lcom/android/server/lights/MiuiLightsService;)V

    .line 814
    return-void
.end method
