class com.android.server.lights.VisualizerHolder$1 implements android.media.audiofx.Visualizer$OnDataCaptureListener {
	 /* .source "VisualizerHolder.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.lights.VisualizerHolder this$0; //synthetic
final com.android.server.lights.VisualizerHolder$OnDataCaptureListener val$onDataCaptureListener; //synthetic
/* # direct methods */
 com.android.server.lights.VisualizerHolder$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/lights/VisualizerHolder; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 67 */
this.this$0 = p1;
this.val$onDataCaptureListener = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onFftDataCapture ( android.media.audiofx.Visualizer p0, Object[] p1, Integer p2 ) {
/* .locals 7 */
/* .param p1, "visualizer" # Landroid/media/audiofx/Visualizer; */
/* .param p2, "fft" # [B */
/* .param p3, "samplingRate" # I */
/* .line 75 */
v0 = com.android.internal.util.ArrayUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 76 */
} // :cond_0
/* array-length v0, p2 */
/* div-int/lit8 v0, v0, 0x2 */
/* new-array v0, v0, [F */
/* .line 77 */
/* .local v0, "magnitudes":[F */
int v1 = 0; // const/4 v1, 0x0
/* .line 78 */
/* .local v1, "max":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_2 */
/* .line 79 */
/* mul-int/lit8 v3, v2, 0x2 */
/* aget-byte v3, p2, v3 */
/* int-to-double v3, v3 */
/* mul-int/lit8 v5, v2, 0x2 */
/* add-int/lit8 v5, v5, 0x1 */
/* aget-byte v5, p2, v5 */
/* int-to-double v5, v5 */
java.lang.Math .hypot ( v3,v4,v5,v6 );
/* move-result-wide v3 */
/* double-to-float v3, v3 */
/* aput v3, v0, v2 */
/* .line 80 */
/* aget v3, v0, v1 */
/* aget v4, v0, v2 */
/* cmpg-float v3, v3, v4 */
/* if-gez v3, :cond_1 */
/* .line 81 */
/* move v1, v2 */
/* .line 78 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 84 */
} // .end local v2 # "i":I
} // :cond_2
v2 = this.this$0;
/* iget v2, v2, Lcom/android/server/lights/VisualizerHolder;->lastMax:I */
/* if-ne v2, v1, :cond_3 */
/* .line 85 */
return;
/* .line 87 */
} // :cond_3
v2 = this.this$0;
/* iput v1, v2, Lcom/android/server/lights/VisualizerHolder;->lastMax:I */
/* .line 88 */
v2 = this.val$onDataCaptureListener;
/* mul-int v3, v1, p3 */
/* array-length v4, p2 */
/* div-int/2addr v3, v4 */
/* .line 89 */
return;
} // .end method
public void onWaveFormDataCapture ( android.media.audiofx.Visualizer p0, Object[] p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "visualizer" # Landroid/media/audiofx/Visualizer; */
/* .param p2, "waveform" # [B */
/* .param p3, "samplingRate" # I */
/* .line 71 */
return;
} // .end method
