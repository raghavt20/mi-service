class com.android.server.lights.MiuiLightsService$LightsThread extends java.lang.Thread {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/lights/MiuiLightsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LightsThread" */
} // .end annotation
/* # instance fields */
private final Integer LOOP_LIMIT;
private final java.util.List lightStateList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer loop_index;
private Boolean mForceStop;
private final Integer styleType;
final com.android.server.lights.MiuiLightsService this$0; //synthetic
/* # direct methods */
public com.android.server.lights.MiuiLightsService$LightsThread ( ) {
/* .locals 1 */
/* .param p3, "styleType" # I */
/* .param p4, "mUid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;II)V" */
/* } */
} // .end annotation
/* .line 563 */
/* .local p2, "lightStateList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;" */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
/* .line 557 */
/* const/16 v0, 0x23 */
/* iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->LOOP_LIMIT:I */
/* .line 558 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->loop_index:I */
/* .line 564 */
this.lightStateList = p2;
/* .line 565 */
/* iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->styleType:I */
/* .line 566 */
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmTmpWorkSource ( p1 );
(( android.os.WorkSource ) v0 ).set ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/WorkSource;->set(I)V
/* .line 567 */
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmWakeLock ( p1 );
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmTmpWorkSource ( p1 );
(( android.os.PowerManager$WakeLock ) v0 ).setWorkSource ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V
/* .line 568 */
return;
} // .end method
private Long delayLocked ( Long p0 ) {
/* .locals 8 */
/* .param p1, "duration" # J */
/* .line 614 */
/* move-wide v0, p1 */
/* .line 615 */
/* .local v0, "durationRemaining":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v4, p1, v2 */
/* if-lez v4, :cond_2 */
/* .line 616 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* add-long/2addr v4, p1 */
/* .line 619 */
/* .local v4, "bedtime":J */
} // :cond_0
try { // :try_start_0
(( java.lang.Object ) p0 ).wait ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 621 */
/* .line 620 */
/* :catch_0 */
/* move-exception v6 */
/* .line 622 */
} // :goto_0
/* iget-boolean v6, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->mForceStop:Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 623 */
/* .line 625 */
} // :cond_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* sub-long v0, v4, v6 */
/* .line 626 */
/* cmp-long v6, v0, v2 */
/* if-gtz v6, :cond_0 */
/* .line 627 */
} // :goto_1
/* sub-long v2, p1, v0 */
/* return-wide v2 */
/* .line 629 */
} // .end local v4 # "bedtime":J
} // :cond_2
/* return-wide v2 */
} // .end method
/* # virtual methods */
public void cancel ( ) {
/* .locals 2 */
/* .line 633 */
/* monitor-enter p0 */
/* .line 634 */
try { // :try_start_0
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmThread ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->mForceStop:Z */
/* .line 635 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmThread ( v0 );
(( java.lang.Object ) v0 ).notifyAll ( ); // invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
/* .line 636 */
/* monitor-exit p0 */
/* .line 637 */
return;
/* .line 636 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public Boolean playLight ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "styleType" # I */
/* .line 591 */
/* monitor-enter p0 */
/* .line 592 */
try { // :try_start_0
v0 = v0 = this.lightStateList;
/* .line 593 */
/* .local v0, "size":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 594 */
/* .local v1, "index":I */
} // :goto_0
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->mForceStop:Z */
/* if-nez v2, :cond_2 */
/* .line 595 */
/* if-ge v1, v0, :cond_0 */
/* .line 596 */
v2 = this.lightStateList;
/* add-int/lit8 v3, v1, 0x1 */
} // .end local v1 # "index":I
/* .local v3, "index":I */
/* check-cast v1, Lcom/android/server/lights/LightState; */
/* .line 597 */
/* .local v1, "lightState":Lcom/android/server/lights/LightState; */
v2 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmColorfulLight ( v2 );
/* iget v5, v1, Lcom/android/server/lights/LightState;->colorARGB:I */
/* iget v6, v1, Lcom/android/server/lights/LightState;->flashMode:I */
/* iget v7, v1, Lcom/android/server/lights/LightState;->onMS:I */
/* iget v8, v1, Lcom/android/server/lights/LightState;->offMS:I */
/* iget v9, v1, Lcom/android/server/lights/LightState;->brightnessMode:I */
/* invoke-static/range {v4 ..v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 599 */
/* iget v2, v1, Lcom/android/server/lights/LightState;->onMS:I */
/* iget v4, v1, Lcom/android/server/lights/LightState;->offMS:I */
/* add-int/2addr v2, v4 */
/* int-to-long v4, v2 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->delayLocked(J)J */
/* .line 600 */
/* move v1, v3 */
} // .end local v1 # "lightState":Lcom/android/server/lights/LightState;
} // .end local v3 # "index":I
/* .local v1, "index":I */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* if-ne p1, v2, :cond_1 */
/* iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->loop_index:I */
/* const/16 v3, 0x23 */
/* if-ge v2, v3, :cond_1 */
/* .line 601 */
int v1 = 0; // const/4 v1, 0x0
/* .line 602 */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->loop_index:I */
/* .line 604 */
} // :cond_1
(( com.android.server.lights.MiuiLightsService$LightsThread ) p0 ).cancel ( ); // invoke-virtual {p0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->cancel()V
/* .line 605 */
v2 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmColorfulLight ( v2 );
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* invoke-static/range {v3 ..v8}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 606 */
v2 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmColorfulLight ( v2 );
int v3 = -1; // const/4 v3, -0x1
/* iput v3, v2, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
/* .line 609 */
} // .end local v0 # "size":I
} // .end local v1 # "index":I
} // :cond_2
/* monitor-exit p0 */
/* .line 610 */
/* .line 609 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void run ( ) {
/* .locals 4 */
/* .line 572 */
int v0 = -8; // const/4 v0, -0x8
android.os.Process .setThreadPriority ( v0 );
/* .line 573 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmWakeLock ( v0 );
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
/* .line 575 */
try { // :try_start_0
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mturnoffBatteryLight ( v0 );
/* .line 576 */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->styleType:I */
v0 = (( com.android.server.lights.MiuiLightsService$LightsThread ) p0 ).playLight ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->playLight(I)Z
/* .line 577 */
/* .local v0, "finished":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 578 */
v1 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mrecoveryBatteryLight ( v1 );
/* .line 579 */
v1 = this.this$0;
/* iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->styleType:I */
int v3 = 0; // const/4 v3, 0x0
com.android.server.lights.MiuiLightsService .-$$Nest$mreportLedEventLocked ( v1,v2,v3,v3,v3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 582 */
} // .end local v0 # "finished":Z
} // :cond_0
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmWakeLock ( v0 );
(( android.os.PowerManager$WakeLock ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 583 */
/* nop */
/* .line 584 */
return;
/* .line 582 */
/* :catchall_0 */
/* move-exception v0 */
v1 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmWakeLock ( v1 );
(( android.os.PowerManager$WakeLock ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 583 */
/* throw v0 */
} // .end method
