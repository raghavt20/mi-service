public class com.android.server.lights.LightsManagerImpl extends com.android.server.lights.LightsManagerStub {
	 /* .source "LightsManagerImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.lights.LightsManagerStub$$" */
} // .end annotation
/* # static fields */
private static final Boolean SUPPORT_HBM;
private static final java.lang.String TAG;
/* # direct methods */
static com.android.server.lights.LightsManagerImpl ( ) {
	 /* .locals 2 */
	 /* .line 15 */
	 android.content.res.Resources .getSystem ( );
	 /* const v1, 0x11050081 */
	 v0 = 	 (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
	 com.android.server.lights.LightsManagerImpl.SUPPORT_HBM = (v0!= 0);
	 return;
} // .end method
public com.android.server.lights.LightsManagerImpl ( ) {
	 /* .locals 0 */
	 /* .line 12 */
	 /* invoke-direct {p0}, Lcom/android/server/lights/LightsManagerStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public Integer brightnessToColor ( Integer p0, Integer p1, Integer p2 ) {
	 /* .locals 3 */
	 /* .param p1, "id" # I */
	 /* .param p2, "brightness" # I */
	 /* .param p3, "lastColor" # I */
	 /* .line 20 */
	 /* move v0, p2 */
	 /* .line 21 */
	 /* .local v0, "color":I */
	 /* if-nez p1, :cond_2 */
	 /* const/16 v2, 0x8 */
	 /* if-le v1, v2, :cond_2 */
	 /* const/16 v2, 0xe */
	 /* if-gt v1, v2, :cond_2 */
	 /* .line 23 */
	 /* if-gez p2, :cond_0 */
	 /* .line 24 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "invalid backlight "; // const-string v2, "invalid backlight "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = " !!!"; // const-string v2, " !!!"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "LightsManagerImpl"; // const-string v2, "LightsManagerImpl"
	 android.util.Slog .e ( v2,v1 );
	 /* .line 25 */
	 /* .line 27 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/lights/LightsManagerImpl;->SUPPORT_HBM:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 28 */
	 /* and-int/lit16 v0, p2, 0x3fff */
	 /* .line 30 */
} // :cond_1
/* and-int/lit16 v0, p2, 0x1fff */
/* .line 33 */
} // :cond_2
/* and-int/lit16 v0, v0, 0xff */
/* .line 34 */
/* shl-int/lit8 v1, v0, 0x10 */
/* const/high16 v2, -0x1000000 */
/* or-int/2addr v1, v2 */
/* shl-int/lit8 v2, v0, 0x8 */
/* or-int/2addr v1, v2 */
/* or-int/2addr v0, v1 */
/* .line 36 */
} // :goto_0
} // .end method
