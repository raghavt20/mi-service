public class com.android.server.lights.LightState {
	 /* .source "LightState.java" */
	 /* # instance fields */
	 public Integer brightnessMode;
	 public java.lang.String callingPackage;
	 public Integer colorARGB;
	 public Integer flashMode;
	 public Long mAddedTime;
	 public Integer mId;
	 public Integer offMS;
	 public Integer onMS;
	 public Integer styleType;
	 /* # direct methods */
	 public com.android.server.lights.LightState ( ) {
		 /* .locals 0 */
		 /* .param p1, "colorARGB" # I */
		 /* .param p2, "flashMode" # I */
		 /* .param p3, "onMS" # I */
		 /* .param p4, "offMS" # I */
		 /* .param p5, "brightnessMode" # I */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 21 */
		 /* iput p1, p0, Lcom/android/server/lights/LightState;->colorARGB:I */
		 /* .line 22 */
		 /* iput p2, p0, Lcom/android/server/lights/LightState;->flashMode:I */
		 /* .line 23 */
		 /* iput p3, p0, Lcom/android/server/lights/LightState;->onMS:I */
		 /* .line 24 */
		 /* iput p4, p0, Lcom/android/server/lights/LightState;->offMS:I */
		 /* .line 25 */
		 /* iput p5, p0, Lcom/android/server/lights/LightState;->brightnessMode:I */
		 /* .line 26 */
		 return;
	 } // .end method
	 public com.android.server.lights.LightState ( ) {
		 /* .locals 2 */
		 /* .param p1, "mId" # I */
		 /* .param p2, "colorARGB" # I */
		 /* .param p3, "flashMode" # I */
		 /* .param p4, "onMS" # I */
		 /* .param p5, "offMS" # I */
		 /* .param p6, "brightnessMode" # I */
		 /* .line 28 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 java.lang.System .currentTimeMillis ( );
		 /* move-result-wide v0 */
		 /* iput-wide v0, p0, Lcom/android/server/lights/LightState;->mAddedTime:J */
		 /* .line 30 */
		 /* iput p1, p0, Lcom/android/server/lights/LightState;->mId:I */
		 /* .line 31 */
		 /* iput p2, p0, Lcom/android/server/lights/LightState;->colorARGB:I */
		 /* .line 32 */
		 /* iput p3, p0, Lcom/android/server/lights/LightState;->flashMode:I */
		 /* .line 33 */
		 /* iput p4, p0, Lcom/android/server/lights/LightState;->onMS:I */
		 /* .line 34 */
		 /* iput p5, p0, Lcom/android/server/lights/LightState;->offMS:I */
		 /* .line 35 */
		 /* iput p6, p0, Lcom/android/server/lights/LightState;->brightnessMode:I */
		 /* .line 36 */
		 return;
	 } // .end method
	 public com.android.server.lights.LightState ( ) {
		 /* .locals 2 */
		 /* .param p1, "callingPackage" # Ljava/lang/String; */
		 /* .param p2, "styleType" # I */
		 /* .line 38 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 39 */
		 java.lang.System .currentTimeMillis ( );
		 /* move-result-wide v0 */
		 /* iput-wide v0, p0, Lcom/android/server/lights/LightState;->mAddedTime:J */
		 /* .line 40 */
		 /* iput p2, p0, Lcom/android/server/lights/LightState;->styleType:I */
		 /* .line 41 */
		 this.callingPackage = p1;
		 /* .line 42 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String toString ( ) {
		 /* .locals 5 */
		 /* .line 46 */
		 /* new-instance v0, Landroid/icu/text/SimpleDateFormat; */
		 /* const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss" */
		 /* invoke-direct {v0, v1}, Landroid/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
		 /* .line 47 */
		 /* .local v0, "formatter":Landroid/icu/text/SimpleDateFormat; */
		 /* new-instance v1, Ljava/util/Date; */
		 /* iget-wide v2, p0, Lcom/android/server/lights/LightState;->mAddedTime:J */
		 /* invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V */
		 (( android.icu.text.SimpleDateFormat ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Landroid/icu/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
		 /* .line 48 */
		 /* .local v1, "date":Ljava/lang/String; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 49 */
		 /* .local v2, "builder":Ljava/lang/StringBuilder; */
		 final String v3 = ", mAddedTime="; // const-string v3, ", mAddedTime="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 50 */
		 v3 = this.callingPackage;
		 v3 = 		 android.text.TextUtils .isEmpty ( v3 );
		 /* if-nez v3, :cond_0 */
		 /* .line 51 */
		 final String v3 = ", callingPackage="; // const-string v3, ", callingPackage="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v4 = this.callingPackage;
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 52 */
	 } // :cond_0
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->mId:I */
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 final String v3 = ", mId="; // const-string v3, ", mId="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->mId:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 53 */
	 } // :cond_1
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->styleType:I */
	 if ( v3 != null) { // if-eqz v3, :cond_2
		 final String v3 = ", mLastLightStyle="; // const-string v3, ", mLastLightStyle="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->styleType:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 54 */
	 } // :cond_2
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->colorARGB:I */
	 if ( v3 != null) { // if-eqz v3, :cond_3
		 final String v3 = ", colorARGB="; // const-string v3, ", colorARGB="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->colorARGB:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 55 */
	 } // :cond_3
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->onMS:I */
	 if ( v3 != null) { // if-eqz v3, :cond_4
		 final String v3 = ", onMS="; // const-string v3, ", onMS="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->onMS:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 56 */
	 } // :cond_4
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->offMS:I */
	 if ( v3 != null) { // if-eqz v3, :cond_5
		 final String v3 = ", offMS="; // const-string v3, ", offMS="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->offMS:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 57 */
	 } // :cond_5
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->flashMode:I */
	 if ( v3 != null) { // if-eqz v3, :cond_6
		 final String v3 = ", flashMode="; // const-string v3, ", flashMode="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->flashMode:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 58 */
	 } // :cond_6
	 /* iget v3, p0, Lcom/android/server/lights/LightState;->brightnessMode:I */
	 if ( v3 != null) { // if-eqz v3, :cond_7
		 final String v3 = ", brightnessMode="; // const-string v3, ", brightnessMode="
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/android/server/lights/LightState;->brightnessMode:I */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* .line 59 */
	 } // :cond_7
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
