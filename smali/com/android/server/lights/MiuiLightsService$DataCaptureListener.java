public abstract class com.android.server.lights.MiuiLightsService$DataCaptureListener {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/lights/MiuiLightsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "DataCaptureListener" */
} // .end annotation
/* # virtual methods */
public abstract void onFrequencyCapture ( android.content.Context p0, Integer p1, Float[] p2 ) {
} // .end method
public abstract void onSetLightCallback ( android.content.Context p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
} // .end method
