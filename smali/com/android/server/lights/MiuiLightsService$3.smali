.class Lcom/android/server/lights/MiuiLightsService$3;
.super Ljava/lang/Object;
.source "MiuiLightsService.java"

# interfaces
.implements Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/lights/MiuiLightsService;->getDataCaptureListener()Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method public static synthetic $r8$lambda$CNkw5v5AKShOjgRWRJ0qs0K2C6w(Lcom/android/server/lights/MiuiLightsService$3;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService$3;->lambda$onFrequencyCapture$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/lights/MiuiLightsService;

    .line 712
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$onFrequencyCapture$0()V
    .locals 3

    .line 721
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmAudioManager(Lcom/android/server/lights/MiuiLightsService;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/proc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 722
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V

    .line 725
    :cond_0
    return-void
.end method


# virtual methods
.method public onFrequencyCapture(I[F)V
    .locals 8
    .param p1, "frequency"    # I
    .param p2, "frequencies"    # [F

    .line 715
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 716
    :try_start_0
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmMusicLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v2

    const/high16 v1, -0x1000000

    or-int v3, p1, v1

    const/4 v4, 0x1

    const/16 v5, 0x64

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setLightLocked(IIIII)V

    .line 718
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 719
    if-nez p1, :cond_0

    .line 720
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$3;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLightHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/lights/MiuiLightsService$3$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/lights/MiuiLightsService$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/lights/MiuiLightsService$3;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 727
    :cond_0
    return-void

    .line 718
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
