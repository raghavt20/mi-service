.class public Lcom/android/server/lights/LightsManagerImpl;
.super Lcom/android/server/lights/LightsManagerStub;
.source "LightsManagerImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.lights.LightsManagerStub$$"
.end annotation


# static fields
.field private static final SUPPORT_HBM:Z

.field private static final TAG:Ljava/lang/String; = "LightsManagerImpl"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 15
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11050081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/lights/LightsManagerImpl;->SUPPORT_HBM:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/android/server/lights/LightsManagerStub;-><init>()V

    return-void
.end method


# virtual methods
.method public brightnessToColor(III)I
    .locals 3
    .param p1, "id"    # I
    .param p2, "brightness"    # I
    .param p3, "lastColor"    # I

    .line 20
    move v0, p2

    .line 21
    .local v0, "color":I
    if-nez p1, :cond_2

    sget v1, Lmiui/os/DeviceFeature;->BACKLIGHT_BIT:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_2

    sget v1, Lmiui/os/DeviceFeature;->BACKLIGHT_BIT:I

    const/16 v2, 0xe

    if-gt v1, v2, :cond_2

    .line 23
    if-gez p2, :cond_0

    .line 24
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid backlight "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " !!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LightsManagerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    return p3

    .line 27
    :cond_0
    sget-boolean v1, Lcom/android/server/lights/LightsManagerImpl;->SUPPORT_HBM:Z

    if-eqz v1, :cond_1

    .line 28
    and-int/lit16 v0, p2, 0x3fff

    goto :goto_0

    .line 30
    :cond_1
    and-int/lit16 v0, p2, 0x1fff

    goto :goto_0

    .line 33
    :cond_2
    and-int/lit16 v0, v0, 0xff

    .line 34
    shl-int/lit8 v1, v0, 0x10

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    shl-int/lit8 v2, v0, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 36
    :goto_0
    return v0
.end method
