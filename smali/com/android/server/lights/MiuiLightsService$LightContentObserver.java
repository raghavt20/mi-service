class com.android.server.lights.MiuiLightsService$LightContentObserver extends android.database.ContentObserver {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/lights/MiuiLightsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LightContentObserver" */
} // .end annotation
/* # instance fields */
public final android.net.Uri BATTERY_LIGHT_TURN_ON_URI;
public final android.net.Uri BREATHING_LIGHT_COLOR_URI;
public final android.net.Uri CALL_BREATHING_LIGHT_COLOR_URI;
public final android.net.Uri LIGHT_TURN_ON_ENDTIME_URI;
public final android.net.Uri LIGHT_TURN_ON_STARTTIME_URI;
public final android.net.Uri LIGHT_TURN_ON_TIME_URI;
public final android.net.Uri LIGHT_TURN_ON_URI;
public final android.net.Uri MMS_BREATHING_LIGHT_COLOR_URI;
public final android.net.Uri MUSIC_LIGHT_TURN_ON_URI;
public final android.net.Uri NOTIFICATION_LIGHT_TURN_ON_URI;
public final android.net.Uri SCREEN_BUTTONS_STATE_URI;
public final android.net.Uri SCREEN_BUTTONS_TURN_ON_URI;
public final android.net.Uri SINGLE_KEY_USE_ACTION_URI;
final com.android.server.lights.MiuiLightsService this$0; //synthetic
/* # direct methods */
public com.android.server.lights.MiuiLightsService$LightContentObserver ( ) {
/* .locals 0 */
/* .line 850 */
this.this$0 = p1;
/* .line 851 */
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLightHandler ( p1 );
/* invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 836 */
final String p1 = "screen_buttons_state"; // const-string p1, "screen_buttons_state"
android.provider.Settings$Secure .getUriFor ( p1 );
this.SCREEN_BUTTONS_STATE_URI = p1;
/* .line 837 */
/* const-string/jumbo p1, "single_key_use_enable" */
android.provider.Settings$System .getUriFor ( p1 );
this.SINGLE_KEY_USE_ACTION_URI = p1;
/* .line 838 */
final String p1 = "screen_buttons_turn_on"; // const-string p1, "screen_buttons_turn_on"
android.provider.Settings$Secure .getUriFor ( p1 );
this.SCREEN_BUTTONS_TURN_ON_URI = p1;
/* .line 839 */
final String p1 = "breathing_light_color"; // const-string p1, "breathing_light_color"
android.provider.Settings$System .getUriFor ( p1 );
this.BREATHING_LIGHT_COLOR_URI = p1;
/* .line 840 */
final String p1 = "call_breathing_light_color"; // const-string p1, "call_breathing_light_color"
android.provider.Settings$System .getUriFor ( p1 );
this.CALL_BREATHING_LIGHT_COLOR_URI = p1;
/* .line 841 */
final String p1 = "mms_breathing_light_color"; // const-string p1, "mms_breathing_light_color"
android.provider.Settings$System .getUriFor ( p1 );
this.MMS_BREATHING_LIGHT_COLOR_URI = p1;
/* .line 842 */
final String p1 = "battery_light_turn_on"; // const-string p1, "battery_light_turn_on"
android.provider.Settings$Secure .getUriFor ( p1 );
this.BATTERY_LIGHT_TURN_ON_URI = p1;
/* .line 843 */
final String p1 = "notification_light_turn_on"; // const-string p1, "notification_light_turn_on"
android.provider.Settings$Secure .getUriFor ( p1 );
this.NOTIFICATION_LIGHT_TURN_ON_URI = p1;
/* .line 844 */
final String p1 = "light_turn_on"; // const-string p1, "light_turn_on"
android.provider.Settings$Secure .getUriFor ( p1 );
this.LIGHT_TURN_ON_URI = p1;
/* .line 845 */
final String p1 = "light_turn_on_Time"; // const-string p1, "light_turn_on_Time"
android.provider.Settings$Secure .getUriFor ( p1 );
this.LIGHT_TURN_ON_TIME_URI = p1;
/* .line 846 */
final String p1 = "light_turn_on_startTime"; // const-string p1, "light_turn_on_startTime"
android.provider.Settings$Secure .getUriFor ( p1 );
this.LIGHT_TURN_ON_STARTTIME_URI = p1;
/* .line 847 */
final String p1 = "light_turn_on_endTime"; // const-string p1, "light_turn_on_endTime"
android.provider.Settings$Secure .getUriFor ( p1 );
this.LIGHT_TURN_ON_ENDTIME_URI = p1;
/* .line 848 */
final String p1 = "music_light_turn_on"; // const-string p1, "music_light_turn_on"
android.provider.Settings$Secure .getUriFor ( p1 );
this.MUSIC_LIGHT_TURN_ON_URI = p1;
/* .line 852 */
return;
} // .end method
/* # virtual methods */
public void observe ( ) {
/* .locals 4 */
/* .line 855 */
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmSupportButtonLight ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = -1; // const/4 v2, -0x1
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 856 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v3 = this.SCREEN_BUTTONS_STATE_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, p0, v2 ); // invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 858 */
	 v0 = this.this$0;
	 v0 = 	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmSupportTapFingerprint ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 859 */
		 v0 = this.this$0;
		 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
		 v3 = this.SINGLE_KEY_USE_ACTION_URI;
		 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, p0, v2 ); // invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
		 /* .line 862 */
	 } // :cond_0
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v3 = this.SCREEN_BUTTONS_TURN_ON_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, p0, v2 ); // invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 866 */
} // :cond_1
v0 = this.this$0;
v0 = com.android.server.lights.MiuiLightsService .-$$Nest$fgetmSupportLedLight ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 867 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v3 = this.BREATHING_LIGHT_COLOR_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, p0, v2 ); // invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 869 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v3 = this.CALL_BREATHING_LIGHT_COLOR_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, p0, v2 ); // invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 871 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v3 = this.MMS_BREATHING_LIGHT_COLOR_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v3, v1, p0, v2 ); // invoke-virtual {v0, v3, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 873 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.BATTERY_LIGHT_TURN_ON_URI;
	 int v3 = 1; // const/4 v3, 0x1
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 875 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.NOTIFICATION_LIGHT_TURN_ON_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 877 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.LIGHT_TURN_ON_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 879 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.LIGHT_TURN_ON_TIME_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 881 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.MUSIC_LIGHT_TURN_ON_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 883 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.LIGHT_TURN_ON_STARTTIME_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 885 */
	 v0 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v0 );
	 v1 = this.LIGHT_TURN_ON_ENDTIME_URI;
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, p0, v2 ); // invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 888 */
} // :cond_2
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 20 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 891 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
int v2 = 0; // const/4 v2, 0x0
/* .line 892 */
/* .local v2, "light":Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v3 = this.SCREEN_BUTTONS_STATE_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_13 */
v3 = this.SINGLE_KEY_USE_ACTION_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_13 */
v3 = this.SCREEN_BUTTONS_TURN_ON_URI;
/* .line 893 */
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
	 /* goto/16 :goto_2 */
	 /* .line 896 */
} // :cond_0
v3 = this.BREATHING_LIGHT_COLOR_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v4 = 4; // const/4 v4, 0x4
/* if-nez v3, :cond_12 */
v3 = this.CALL_BREATHING_LIGHT_COLOR_URI;
/* .line 897 */
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_12 */
v3 = this.MMS_BREATHING_LIGHT_COLOR_URI;
/* .line 898 */
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* goto/16 :goto_1 */
	 /* .line 901 */
} // :cond_1
v3 = this.BATTERY_LIGHT_TURN_ON_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
	 /* .line 902 */
	 v3 = this.this$0;
	 v3 = this.mLightsByType;
	 int v4 = 3; // const/4 v4, 0x3
	 /* aget-object v3, v3, v4 */
	 /* move-object v2, v3 */
	 /* check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
	 /* .line 903 */
	 (( com.android.server.lights.MiuiLightsService$LightImpl ) v2 ).updateLight ( ); // invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
	 /* goto/16 :goto_3 */
	 /* .line 904 */
} // :cond_2
v3 = this.NOTIFICATION_LIGHT_TURN_ON_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 905 */
	 v3 = this.this$0;
	 v3 = this.mLightsByType;
	 /* aget-object v3, v3, v4 */
	 /* move-object v2, v3 */
	 /* check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
	 /* .line 906 */
	 (( com.android.server.lights.MiuiLightsService$LightImpl ) v2 ).updateLight ( ); // invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
	 /* goto/16 :goto_3 */
	 /* .line 907 */
} // :cond_3
v3 = this.LIGHT_TURN_ON_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
	 /* .line 908 */
	 v3 = this.this$0;
	 v3 = 	 com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnLight ( v3 );
	 if ( v3 != null) { // if-eqz v3, :cond_4
		 /* .line 909 */
		 v3 = this.this$0;
		 com.android.server.lights.MiuiLightsService .-$$Nest$mregisterAudioPlaybackCallback ( v3 );
		 /* .line 910 */
		 v3 = this.this$0;
		 v3 = 		 com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnMusicLight ( v3 );
		 if ( v3 != null) { // if-eqz v3, :cond_5
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmAudioManager ( v3 );
			 v3 = 			 (( android.media.AudioManager ) v3 ).isMusicActive ( ); // invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z
			 if ( v3 != null) { // if-eqz v3, :cond_5
				 /* .line 911 */
				 v3 = this.this$0;
				 com.android.server.lights.MiuiLightsService .-$$Nest$mturnoffBatteryLight ( v3 );
				 /* .line 912 */
				 com.android.server.lights.VisualizerHolder .getInstance ( );
				 v4 = this.this$0;
				 com.android.server.lights.MiuiLightsService .-$$Nest$mgetDataCaptureListener ( v4 );
				 (( com.android.server.lights.VisualizerHolder ) v3 ).setOnDataCaptureListener ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V
				 /* .line 915 */
			 } // :cond_4
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$mreleaseVisualizer ( v3 );
			 /* .line 916 */
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$munregisterAudioPlaybackCallback ( v3 );
			 /* .line 918 */
		 } // :cond_5
	 } // :goto_0
	 v3 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateLightState ( v3 );
	 /* goto/16 :goto_3 */
	 /* .line 919 */
} // :cond_6
v3 = this.MUSIC_LIGHT_TURN_ON_URI;
v3 = (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_8
	 /* .line 920 */
	 v3 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmMusicLight ( v3 );
	 (( com.android.server.lights.MiuiLightsService$LightImpl ) v3 ).updateLight ( ); // invoke-virtual {v3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
	 /* .line 921 */
	 v3 = this.this$0;
	 v3 = 	 com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnMusicLight ( v3 );
	 /* if-nez v3, :cond_7 */
	 /* .line 922 */
	 v3 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$mreleaseVisualizer ( v3 );
	 /* .line 923 */
	 v3 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$munregisterAudioPlaybackCallback ( v3 );
	 /* goto/16 :goto_3 */
	 /* .line 924 */
} // :cond_7
v3 = this.this$0;
v3 = com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnLight ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_14
	 v3 = this.this$0;
	 v3 = 	 com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnMusicLight ( v3 );
	 if ( v3 != null) { // if-eqz v3, :cond_14
		 /* .line 925 */
		 v3 = this.this$0;
		 com.android.server.lights.MiuiLightsService .-$$Nest$mregisterAudioPlaybackCallback ( v3 );
		 /* .line 926 */
		 v3 = this.this$0;
		 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmAudioManager ( v3 );
		 v3 = 		 (( android.media.AudioManager ) v3 ).isMusicActive ( ); // invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z
		 if ( v3 != null) { // if-eqz v3, :cond_14
			 /* .line 927 */
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$mturnoffBatteryLight ( v3 );
			 /* .line 928 */
			 com.android.server.lights.VisualizerHolder .getInstance ( );
			 v4 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$mgetDataCaptureListener ( v4 );
			 (( com.android.server.lights.VisualizerHolder ) v3 ).setOnDataCaptureListener ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V
			 /* goto/16 :goto_3 */
			 /* .line 931 */
		 } // :cond_8
		 v3 = this.LIGHT_TURN_ON_TIME_URI;
		 v3 = 		 (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
		 final String v4 = "light_turn_on_endTime"; // const-string v4, "light_turn_on_endTime"
		 final String v5 = "light_turn_on_startTime"; // const-string v5, "light_turn_on_startTime"
		 /* const-wide/32 v6, 0x4ef6d80 */
		 int v8 = -2; // const/4 v8, -0x2
		 /* const-wide/32 v9, 0x1808580 */
		 if ( v3 != null) { // if-eqz v3, :cond_a
			 /* .line 932 */
			 v3 = this.this$0;
			 v3 = 			 (( com.android.server.lights.MiuiLightsService ) v3 ).isTurnOnTimeLight ( ); // invoke-virtual {v3}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnTimeLight()Z
			 if ( v3 != null) { // if-eqz v3, :cond_9
				 v3 = this.this$0;
				 v3 = 				 com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnLight ( v3 );
				 if ( v3 != null) { // if-eqz v3, :cond_9
					 /* .line 933 */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v3 );
					 android.provider.Settings$Secure .getLongForUser ( v11,v5,v9,v10,v8 );
					 /* move-result-wide v9 */
					 com.android.server.lights.MiuiLightsService .-$$Nest$fputlight_start_time ( v3,v9,v10 );
					 /* .line 935 */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v3 );
					 android.provider.Settings$Secure .getLongForUser ( v5,v4,v6,v7,v8 );
					 /* move-result-wide v4 */
					 com.android.server.lights.MiuiLightsService .-$$Nest$fputlight_end_time ( v3,v4,v5 );
					 /* .line 937 */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateWorkState ( v3 );
					 /* .line 938 */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateLightState ( v3 );
					 /* goto/16 :goto_3 */
					 /* .line 939 */
				 } // :cond_9
				 v3 = this.this$0;
				 v3 = 				 com.android.server.lights.MiuiLightsService .-$$Nest$misTurnOnLight ( v3 );
				 if ( v3 != null) { // if-eqz v3, :cond_14
					 /* .line 940 */
					 v3 = this.this$0;
					 int v4 = 1; // const/4 v4, 0x1
					 com.android.server.lights.MiuiLightsService .-$$Nest$fputmIsWorkTime ( v3,v4 );
					 /* .line 941 */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateLightState ( v3 );
					 /* goto/16 :goto_3 */
					 /* .line 943 */
				 } // :cond_a
				 v3 = this.LIGHT_TURN_ON_STARTTIME_URI;
				 v3 = 				 (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
				 /* const-wide/32 v13, 0xea60 */
				 final String v15 = " minute:"; // const-string v15, " minute:"
				 final String v6 = "onChange hour:"; // const-string v6, "onChange hour:"
				 final String v7 = "LightsService"; // const-string v7, "LightsService"
				 /* const-wide/16 v16, 0x0 */
				 /* const-wide/32 v18, 0x36ee80 */
				 if ( v3 != null) { // if-eqz v3, :cond_e
					 /* .line 944 */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v3 );
					 android.provider.Settings$Secure .getLongForUser ( v4,v5,v9,v10,v8 );
					 /* move-result-wide v11 */
					 com.android.server.lights.MiuiLightsService .-$$Nest$fputlight_start_time ( v3,v11,v12 );
					 /* .line 946 */
					 /* sget-boolean v3, Lcom/android/server/lights/LightsService;->DEBUG:Z */
					 if ( v3 != null) { // if-eqz v3, :cond_b
						 /* .line 947 */
						 /* new-instance v3, Ljava/lang/StringBuilder; */
						 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
						 (( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 v4 = this.this$0;
						 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_start_time ( v4 );
						 /* move-result-wide v11 */
						 /* div-long v11, v11, v18 */
						 (( java.lang.StringBuilder ) v3 ).append ( v11, v12 ); // invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v3 ).append ( v15 ); // invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 v4 = this.this$0;
						 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_start_time ( v4 );
						 /* move-result-wide v11 */
						 /* rem-long v11, v11, v18 */
						 /* div-long/2addr v11, v13 */
						 (( java.lang.StringBuilder ) v3 ).append ( v11, v12 ); // invoke-virtual {v3, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
						 android.util.Slog .i ( v7,v3 );
						 /* .line 948 */
					 } // :cond_b
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_start_time ( v3 );
					 /* move-result-wide v3 */
					 /* cmp-long v3, v3, v16 */
					 /* if-ltz v3, :cond_c */
					 v3 = this.this$0;
					 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_start_time ( v3 );
					 /* move-result-wide v3 */
					 /* const-wide/32 v6, 0x5265c00 */
					 /* cmp-long v3, v3, v6 */
					 /* if-lez v3, :cond_d */
					 /* .line 949 */
				 } // :cond_c
				 v3 = this.this$0;
				 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmContext ( v3 );
				 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
				 android.provider.Settings$Secure .putLong ( v3,v5,v9,v10 );
				 /* .line 951 */
				 v3 = this.this$0;
				 com.android.server.lights.MiuiLightsService .-$$Nest$fputlight_start_time ( v3,v9,v10 );
				 /* .line 953 */
			 } // :cond_d
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateWorkState ( v3 );
			 /* .line 954 */
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateLightState ( v3 );
			 /* goto/16 :goto_3 */
			 /* .line 955 */
		 } // :cond_e
		 v3 = this.LIGHT_TURN_ON_ENDTIME_URI;
		 v3 = 		 (( android.net.Uri ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_14
			 /* .line 956 */
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmResolver ( v3 );
			 /* const-wide/32 v9, 0x4ef6d80 */
			 android.provider.Settings$Secure .getLongForUser ( v5,v4,v9,v10,v8 );
			 /* move-result-wide v11 */
			 com.android.server.lights.MiuiLightsService .-$$Nest$fputlight_end_time ( v3,v11,v12 );
			 /* .line 958 */
			 /* sget-boolean v3, Lcom/android/server/lights/LightsService;->DEBUG:Z */
			 if ( v3 != null) { // if-eqz v3, :cond_f
				 /* .line 959 */
				 /* new-instance v3, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
				 (( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v5 = this.this$0;
				 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_end_time ( v5 );
				 /* move-result-wide v5 */
				 /* div-long v5, v5, v18 */
				 (( java.lang.StringBuilder ) v3 ).append ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v3 ).append ( v15 ); // invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v5 = this.this$0;
				 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_end_time ( v5 );
				 /* move-result-wide v5 */
				 /* rem-long v5, v5, v18 */
				 /* div-long/2addr v5, v13 */
				 (( java.lang.StringBuilder ) v3 ).append ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .i ( v7,v3 );
				 /* .line 960 */
			 } // :cond_f
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_end_time ( v3 );
			 /* move-result-wide v5 */
			 /* cmp-long v3, v5, v16 */
			 /* if-ltz v3, :cond_10 */
			 v3 = this.this$0;
			 com.android.server.lights.MiuiLightsService .-$$Nest$fgetlight_end_time ( v3 );
			 /* move-result-wide v5 */
			 /* const-wide/32 v7, 0x5265c00 */
			 /* cmp-long v3, v5, v7 */
			 /* if-lez v3, :cond_11 */
			 /* .line 961 */
		 } // :cond_10
		 v3 = this.this$0;
		 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmContext ( v3 );
		 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* const-wide/32 v5, 0x4ef6d80 */
		 android.provider.Settings$Secure .putLong ( v3,v4,v5,v6 );
		 /* .line 963 */
		 v3 = this.this$0;
		 com.android.server.lights.MiuiLightsService .-$$Nest$fputlight_end_time ( v3,v5,v6 );
		 /* .line 965 */
	 } // :cond_11
	 v3 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateWorkState ( v3 );
	 /* .line 966 */
	 v3 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$mupdateLightState ( v3 );
	 /* .line 899 */
} // :cond_12
} // :goto_1
v3 = this.this$0;
v3 = this.mLightsByType;
/* aget-object v3, v3, v4 */
/* move-object v2, v3 */
/* check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* .line 900 */
/* invoke-virtual/range {p2 ..p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.lights.MiuiLightsService$LightImpl ) v2 ).setFlashing ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setFlashing(Ljava/lang/String;Ljava/lang/String;)V
/* .line 894 */
} // :cond_13
} // :goto_2
v3 = this.this$0;
v3 = this.mLightsByType;
int v4 = 2; // const/4 v4, 0x2
/* aget-object v3, v3, v4 */
/* move-object v2, v3 */
/* check-cast v2, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* .line 895 */
(( com.android.server.lights.MiuiLightsService$LightImpl ) v2 ).updateLight ( ); // invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 968 */
} // :cond_14
} // :goto_3
return;
} // .end method
