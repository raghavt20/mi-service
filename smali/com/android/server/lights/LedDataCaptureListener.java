public class com.android.server.lights.LedDataCaptureListener implements com.android.server.lights.MiuiLightsService$DataCaptureListener {
	 /* .source "LedDataCaptureListener.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.lights.LedDataCaptureListener ( ) {
		 /* .locals 1 */
		 /* .line 8 */
		 /* const-class v0, Lcom/android/server/lights/LedDataCaptureListener; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.lights.LedDataCaptureListener ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "looper" # Landroid/os/Looper; */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 11 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void onFrequencyCapture ( android.content.Context p0, Integer p1, Float[] p2 ) {
		 /* .locals 0 */
		 /* .param p1, "mContext" # Landroid/content/Context; */
		 /* .param p2, "magnitude_max" # I */
		 /* .param p3, "frequencies" # [F */
		 /* .line 19 */
		 return;
	 } // .end method
	 public void onSetLightCallback ( android.content.Context p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
		 /* .locals 0 */
		 /* .param p1, "mContext" # Landroid/content/Context; */
		 /* .param p2, "lightId" # I */
		 /* .param p3, "color" # I */
		 /* .param p4, "mode" # I */
		 /* .param p5, "onMS" # I */
		 /* .param p6, "offMS" # I */
		 /* .param p7, "brightnessMode" # I */
		 /* .line 16 */
		 return;
	 } // .end method
