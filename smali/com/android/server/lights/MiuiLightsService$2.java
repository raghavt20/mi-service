class com.android.server.lights.MiuiLightsService$2 extends miui.lights.ILightsManager$Stub {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/lights/MiuiLightsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.lights.MiuiLightsService this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$Ix4SXNyoZBdBCkx4iMcw-2rkGfw ( com.android.server.lights.MiuiLightsService$2 p0, java.lang.String p1, Integer p2, Integer p3, Integer p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$2;->lambda$setColorLed$1(Ljava/lang/String;III)V */
return;
} // .end method
public static void $r8$lambda$VMBVX92WU__OFgQgL2yGGbSVAUU ( com.android.server.lights.MiuiLightsService$2 p0, java.lang.String p1, Integer p2, Integer p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$2;->lambda$setColorCommon$0(Ljava/lang/String;II)V */
return;
} // .end method
 com.android.server.lights.MiuiLightsService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/lights/MiuiLightsService; */
/* .line 492 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/lights/ILightsManager$Stub;-><init>()V */
return;
} // .end method
private void lambda$setColorCommon$0 ( java.lang.String p0, Integer p1, Integer p2 ) { //synthethic
/* .locals 8 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .param p2, "styleType" # I */
/* .param p3, "color" # I */
/* .line 503 */
final String v0 = "LightsService"; // const-string v0, "LightsService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setColorCommon callingPkg: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " styleType: "; // const-string v2, " styleType: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " color:"; // const-string v2, " color:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 505 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 506 */
int v1 = 7; // const/4 v1, 0x7
/* if-ne p2, v1, :cond_0 */
/* .line 507 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.lights.MiuiLightsService .-$$Nest$fgetmPrivacyLight ( v1 );
	 int v4 = 0; // const/4 v4, 0x0
	 int v5 = 0; // const/4 v5, 0x0
	 int v6 = 0; // const/4 v6, 0x0
	 int v7 = 0; // const/4 v7, 0x0
	 /* move v3, p3 */
	 /* invoke-static/range {v2 ..v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
	 /* .line 510 */
} // :cond_0
/* monitor-exit v0 */
/* .line 511 */
return;
/* .line 510 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$setColorLed$1 ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) { //synthethic
/* .locals 8 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .param p2, "styleType" # I */
/* .param p3, "color" # I */
/* .param p4, "category" # I */
/* .line 518 */
final String v0 = "LightsService"; // const-string v0, "LightsService"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setColorLed callingPkg: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " styleType: "; // const-string v2, " styleType: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " color:"; // const-string v2, " color:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 520 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 521 */
/* const/16 v1, 0x8 */
/* if-ne p2, v1, :cond_2 */
/* .line 522 */
int v1 = 1; // const/4 v1, 0x1
/* if-eq p4, v1, :cond_1 */
int v1 = 2; // const/4 v1, 0x2
/* if-eq p4, v1, :cond_1 */
int v1 = 3; // const/4 v1, 0x3
/* if-eq p4, v1, :cond_1 */
int v1 = 4; // const/4 v1, 0x4
/* if-ne p4, v1, :cond_0 */
/* .line 529 */
} // :cond_0
try { // :try_start_0
v1 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLedLight ( v1 );
int v4 = 1; // const/4 v4, 0x1
/* const/16 v5, 0x1f4 */
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move v3, p3 */
/* invoke-static/range {v2 ..v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 526 */
} // :cond_1
} // :goto_0
v1 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLedLight ( v1 );
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move v3, p3 */
/* invoke-static/range {v2 ..v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 533 */
} // :cond_2
} // :goto_1
/* monitor-exit v0 */
/* .line 534 */
return;
/* .line 533 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public void setColorCommon ( Integer p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 2 */
/* .param p1, "color" # I */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "styleType" # I */
/* .param p4, "userId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 502 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLightHandler ( v0 );
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2, p3, p1}, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/lights/MiuiLightsService$2;Ljava/lang/String;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 512 */
return;
} // .end method
public void setColorLed ( Integer p0, java.lang.String p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 8 */
/* .param p1, "color" # I */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "styleType" # I */
/* .param p4, "userId" # I */
/* .param p5, "category" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 517 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetmLightHandler ( v0 );
/* new-instance v7, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p2 */
/* move v4, p3 */
/* move v5, p1 */
/* move v6, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/lights/MiuiLightsService$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/lights/MiuiLightsService$2;Ljava/lang/String;III)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 535 */
return;
} // .end method
public void setColorfulLight ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .param p2, "styleType" # I */
/* .param p3, "userId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 495 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$mcheckCallerVerify ( v0,p1 );
/* .line 496 */
v0 = this.this$0;
com.android.server.lights.MiuiLightsService .-$$Nest$fgetlocalService ( v0 );
(( com.android.server.lights.MiuiLightsService$LocalService ) v0 ).setColorfulLight ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$LocalService;->setColorfulLight(Ljava/lang/String;II)V
/* .line 497 */
return;
} // .end method
