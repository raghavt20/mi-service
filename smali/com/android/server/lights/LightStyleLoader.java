public class com.android.server.lights.LightStyleLoader {
	 /* .source "LightStyleLoader.java" */
	 /* # static fields */
	 private static final java.lang.String BRIGHTNESS_MODE;
	 private static final java.lang.String COLOR_ARGB;
	 private static final Boolean DEBUG;
	 private static final Integer DEFAULT_DELAY;
	 private static final Integer DEFAULT_INTERVAL;
	 private static final java.lang.String FLASH_MODE;
	 private static final Integer GAME_EFFECT_ONMS;
	 private static final java.lang.String LIGHTSTATE;
	 private static final java.lang.String LIGHTS_PATH_DIR;
	 private static final Integer LIGHT_COLOR;
	 private static final Integer LIGHT_FREQ;
	 private static final Integer MAXDELAY;
	 private static final java.lang.String OFFMS;
	 private static final java.lang.String ONMS;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private android.util.SparseArray mStyleArray;
	 private Boolean mSupportColorEffect;
	 private final android.content.res.Resources resources;
	 /* # direct methods */
	 static com.android.server.lights.LightStyleLoader ( ) {
		 /* .locals 1 */
		 /* .line 36 */
		 /* sget-boolean v0, Lcom/android/server/lights/LightsService;->DEBUG:Z */
		 com.android.server.lights.LightStyleLoader.DEBUG = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.lights.LightStyleLoader ( ) {
		 /* .locals 4 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 57 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 53 */
		 /* new-instance v0, Landroid/util/SparseArray; */
		 /* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
		 this.mStyleArray = v0;
		 /* .line 58 */
		 (( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 this.resources = v0;
		 /* .line 59 */
		 this.mContext = p1;
		 /* .line 60 */
		 v0 = this.mStyleArray;
		 final String v1 = "lightstyle_default"; // const-string v1, "lightstyle_default"
		 int v2 = 0; // const/4 v2, 0x0
		 (( android.util.SparseArray ) v0 ).append ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 61 */
		 v0 = this.mStyleArray;
		 int v1 = 1; // const/4 v1, 0x1
		 final String v3 = "lightstyle_phone"; // const-string v3, "lightstyle_phone"
		 (( android.util.SparseArray ) v0 ).append ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 62 */
		 v0 = this.mStyleArray;
		 int v1 = 2; // const/4 v1, 0x2
		 final String v3 = "lightstyle_game"; // const-string v3, "lightstyle_game"
		 (( android.util.SparseArray ) v0 ).append ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 63 */
		 v0 = this.mStyleArray;
		 int v1 = 3; // const/4 v1, 0x3
		 final String v3 = "lightstyle_music"; // const-string v3, "lightstyle_music"
		 (( android.util.SparseArray ) v0 ).append ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 64 */
		 v0 = this.mStyleArray;
		 int v1 = 4; // const/4 v1, 0x4
		 final String v3 = "lightstyle_alarm"; // const-string v3, "lightstyle_alarm"
		 (( android.util.SparseArray ) v0 ).append ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 65 */
		 v0 = this.mStyleArray;
		 int v1 = 5; // const/4 v1, 0x5
		 final String v3 = "lightstyle_expand"; // const-string v3, "lightstyle_expand"
		 (( android.util.SparseArray ) v0 ).append ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 66 */
		 v0 = this.mStyleArray;
		 int v1 = 6; // const/4 v1, 0x6
		 final String v3 = "lightstyle_luckymoney"; // const-string v3, "lightstyle_luckymoney"
		 (( android.util.SparseArray ) v0 ).append ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
		 /* .line 68 */
		 /* const-string/jumbo v0, "support_led_colorful_effect" */
		 v0 = 		 miui.util.FeatureParser .getBoolean ( v0,v2 );
		 /* iput-boolean v0, p0, Lcom/android/server/lights/LightStyleLoader;->mSupportColorEffect:Z */
		 /* .line 69 */
		 return;
	 } // .end method
	 private Integer getCustomLight ( Integer p0, Integer p1, Integer p2 ) {
		 /* .locals 7 */
		 /* .param p1, "attrs" # I */
		 /* .param p2, "original" # I */
		 /* .param p3, "style" # I */
		 /* .line 147 */
		 final String v0 = "LightsService"; // const-string v0, "LightsService"
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v2 = "breathing_light"; // const-string v2, "breathing_light"
		 int v3 = -2; // const/4 v3, -0x2
		 android.provider.Settings$Secure .getStringForUser ( v1,v2,v3 );
		 /* .line 149 */
		 /* .local v1, "jsonText":Ljava/lang/String; */
		 v2 = 		 android.text.TextUtils .isEmpty ( v1 );
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 150 */
			 /* .line 153 */
		 } // :cond_0
		 try { // :try_start_0
			 /* new-instance v2, Lorg/json/JSONArray; */
			 /* invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
			 /* .line 154 */
			 /* .local v2, "jsonArray":Lorg/json/JSONArray; */
			 int v3 = 0; // const/4 v3, 0x0
			 /* .local v3, "i":I */
		 } // :goto_0
		 v4 = 		 (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
		 /* if-ge v3, v4, :cond_2 */
		 /* .line 155 */
		 (( org.json.JSONArray ) v2 ).getJSONObject ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
		 /* .line 156 */
		 /* .local v4, "logObject":Lorg/json/JSONObject; */
		 final String v5 = "light"; // const-string v5, "light"
		 v5 = 		 (( org.json.JSONObject ) v4 ).getInt ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 /* if-ne v5, p3, :cond_1 */
		 /* .line 157 */
		 /* packed-switch p1, :pswitch_data_0 */
		 /* .line 163 */
		 /* .line 161 */
		 /* :pswitch_0 */
		 final String v5 = "onMS"; // const-string v5, "onMS"
		 v0 = 		 (( org.json.JSONObject ) v4 ).getInt ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 /* .line 159 */
		 /* :pswitch_1 */
		 final String v5 = "color"; // const-string v5, "color"
		 v0 = 		 (( org.json.JSONObject ) v4 ).getInt ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
		 /* .line 163 */
	 } // :goto_1
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v6, "un know attrs:" */
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v0,v5 );
	 /* :try_end_0 */
	 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 154 */
} // .end local v4 # "logObject":Lorg/json/JSONObject;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 169 */
} // .end local v2 # "jsonArray":Lorg/json/JSONArray;
} // .end local v3 # "i":I
} // :cond_2
/* .line 167 */
/* :catch_0 */
/* move-exception v2 */
/* .line 168 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "Light jsonArray error"; // const-string v3, "Light jsonArray error"
android.util.Slog .i ( v0,v3,v2 );
/* .line 170 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_2
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public java.util.List getLightStyle ( Integer p0 ) {
/* .locals 28 */
/* .param p1, "styleType" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 72 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
final String v3 = "/product/etc/lights/"; // const-string v3, "/product/etc/lights/"
final String v4 = "LightsService"; // const-string v4, "LightsService"
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v5, v0 */
/* .line 73 */
/* .local v5, "ls_list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;" */
int v6 = 0; // const/4 v6, 0x0
/* .line 75 */
/* .local v6, "is":Ljava/io/InputStream; */
/* if-ltz v2, :cond_9 */
try { // :try_start_0
v0 = this.mStyleArray;
(( android.util.SparseArray ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* goto/16 :goto_7 */
/* .line 78 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/lights/LightStyleLoader;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 79 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "parse style: "; // const-string v7, "parse style: "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " id: "; // const-string v7, " id: "
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mStyleArray;
(( android.util.SparseArray ) v7 ).get ( v2 ); // invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v0 );
/* .line 81 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
/* .line 82 */
/* .local v0, "colorARGB":I */
int v13 = 1; // const/4 v13, 0x1
/* .line 83 */
/* .local v13, "flashMode":I */
/* const/16 v14, 0x64 */
/* .line 84 */
/* .local v14, "offMS":I */
/* const/16 v15, 0x64 */
/* .line 85 */
/* .local v15, "onMS":I */
/* const/16 v16, 0x0 */
/* .line 87 */
/* .local v16, "brightnessMode":I */
int v7 = 0; // const/4 v7, 0x0
int v8 = 1; // const/4 v8, 0x1
/* if-ne v2, v8, :cond_3 */
/* .line 88 */
v9 = /* invoke-direct {v1, v8, v15, v2}, Lcom/android/server/lights/LightStyleLoader;->getCustomLight(III)I */
/* move v12, v9 */
/* .line 89 */
/* .local v12, "customOnMS":I */
/* if-eq v12, v15, :cond_2 */
/* .line 90 */
v8 = /* invoke-direct {v1, v7, v0, v2}, Lcom/android/server/lights/LightStyleLoader;->getCustomLight(III)I */
/* .line 91 */
/* .local v8, "customColorARGB":I */
/* new-instance v11, Lcom/android/server/lights/LightState; */
/* const/16 v17, 0x7d0 */
/* move-object v7, v11 */
/* move v9, v13 */
/* move v10, v12 */
/* move/from16 v18, v0 */
/* move-object v0, v11 */
} // .end local v0 # "colorARGB":I
/* .local v18, "colorARGB":I */
/* move/from16 v11, v17 */
/* move/from16 v17, v12 */
} // .end local v12 # "customOnMS":I
/* .local v17, "customOnMS":I */
/* move/from16 v12, v16 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/server/lights/LightState;-><init>(IIIII)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 92 */
/* nop */
/* .line 141 */
libcore.io.IoUtils .closeQuietly ( v6 );
/* .line 92 */
/* .line 89 */
} // .end local v8 # "customColorARGB":I
} // .end local v17 # "customOnMS":I
} // .end local v18 # "colorARGB":I
/* .restart local v0 # "colorARGB":I */
/* .restart local v12 # "customOnMS":I */
} // :cond_2
/* move/from16 v18, v0 */
/* move/from16 v17, v12 */
} // .end local v0 # "colorARGB":I
} // .end local v12 # "customOnMS":I
/* .restart local v17 # "customOnMS":I */
/* .restart local v18 # "colorARGB":I */
/* .line 87 */
} // .end local v17 # "customOnMS":I
} // .end local v18 # "colorARGB":I
/* .restart local v0 # "colorARGB":I */
} // :cond_3
/* move/from16 v18, v0 */
/* .line 97 */
} // .end local v0 # "colorARGB":I
/* .restart local v18 # "colorARGB":I */
} // :goto_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne v2, v0, :cond_4 */
try { // :try_start_1
/* iget-boolean v9, v1, Lcom/android/server/lights/LightStyleLoader;->mSupportColorEffect:Z */
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 98 */
/* new-instance v0, Lcom/android/server/lights/LightState; */
int v8 = 1; // const/4 v8, 0x1
/* const/16 v10, 0x1323 */
/* move-object v7, v0 */
/* move v9, v13 */
/* move v11, v14 */
/* move/from16 v12, v16 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/server/lights/LightState;-><init>(IIIII)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 99 */
/* nop */
/* .line 141 */
libcore.io.IoUtils .closeQuietly ( v6 );
/* .line 99 */
/* .line 102 */
} // :cond_4
try { // :try_start_2
javax.xml.parsers.DocumentBuilderFactory .newInstance ( );
/* .line 103 */
/* .local v9, "factory":Ljavax/xml/parsers/DocumentBuilderFactory; */
(( javax.xml.parsers.DocumentBuilderFactory ) v9 ).newDocumentBuilder ( ); // invoke-virtual {v9}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
/* .line 104 */
/* .local v10, "builder":Ljavax/xml/parsers/DocumentBuilder; */
/* new-instance v11, Ljava/io/BufferedInputStream; */
/* new-instance v12, Ljava/io/FileInputStream; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mStyleArray;
/* .line 105 */
(( android.util.SparseArray ) v7 ).get ( v2 ); // invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ".xml"; // const-string v7, ".xml"
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* const/16 v0, 0x1000 */
/* invoke-direct {v11, v12, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V */
/* move-object v6, v11 */
/* .line 106 */
(( javax.xml.parsers.DocumentBuilder ) v10 ).parse ( v6 ); // invoke-virtual {v10, v6}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
/* .line 107 */
/* .local v0, "doc":Lorg/w3c/dom/Document; */
final String v7 = "lightstate"; // const-string v7, "lightstate"
/* .line 108 */
/* .local v7, "lightList":Lorg/w3c/dom/NodeList; */
int v11 = 0; // const/4 v11, 0x0
/* .local v11, "i":I */
v12 = } // :goto_1
/* if-ge v11, v12, :cond_8 */
/* .line 109 */
/* .line 110 */
/* .local v12, "childNodes":Lorg/w3c/dom/NodeList; */
/* const/16 v19, 0x0 */
/* move/from16 v8, v19 */
/* .local v8, "j":I */
} // :goto_2
/* move-object/from16 v26, v0 */
} // .end local v0 # "doc":Lorg/w3c/dom/Document;
v0 = /* .local v26, "doc":Lorg/w3c/dom/Document; */
/* if-ge v8, v0, :cond_7 */
v0 = /* .line 111 */
/* move-object/from16 v27, v7 */
int v7 = 1; // const/4 v7, 0x1
} // .end local v7 # "lightList":Lorg/w3c/dom/NodeList;
/* .local v27, "lightList":Lorg/w3c/dom/NodeList; */
/* if-ne v0, v7, :cond_6 */
/* .line 112 */
v19 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v19, :sswitch_data_0 */
} // :cond_5
/* :sswitch_0 */
final String v7 = "brightnessMode"; // const-string v7, "brightnessMode"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
int v0 = 4; // const/4 v0, 0x4
/* :sswitch_1 */
final String v7 = "colorARGB"; // const-string v7, "colorARGB"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_2 */
final String v7 = "offMS"; // const-string v7, "offMS"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_3 */
final String v7 = "onMS"; // const-string v7, "onMS"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_4 */
final String v7 = "flashMode"; // const-string v7, "flashMode"
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
int v0 = 1; // const/4 v0, 0x1
} // :goto_3
int v0 = -1; // const/4 v0, -0x1
} // :goto_4
/* packed-switch v0, :pswitch_data_0 */
/* .line 126 */
/* :pswitch_0 */
v0 = java.lang.Integer .parseInt ( v0 );
/* move/from16 v16, v0 */
/* .line 127 */
/* .line 123 */
/* :pswitch_1 */
v0 = java.lang.Integer .parseInt ( v0 );
/* move v15, v0 */
/* .line 124 */
/* .line 120 */
/* :pswitch_2 */
v0 = java.lang.Integer .parseInt ( v0 );
/* move v14, v0 */
/* .line 121 */
/* .line 117 */
/* :pswitch_3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* move v13, v0 */
/* .line 118 */
/* .line 114 */
/* :pswitch_4 */
v0 = android.graphics.Color .parseColor ( v0 );
/* move/from16 v18, v0 */
/* .line 115 */
/* nop */
/* .line 110 */
} // :cond_6
} // :goto_5
/* add-int/lit8 v8, v8, 0x1 */
/* move-object/from16 v0, v26 */
/* move-object/from16 v7, v27 */
/* goto/16 :goto_2 */
} // .end local v27 # "lightList":Lorg/w3c/dom/NodeList;
/* .restart local v7 # "lightList":Lorg/w3c/dom/NodeList; */
} // :cond_7
/* move-object/from16 v27, v7 */
/* .line 133 */
} // .end local v7 # "lightList":Lorg/w3c/dom/NodeList;
} // .end local v8 # "j":I
/* .restart local v27 # "lightList":Lorg/w3c/dom/NodeList; */
/* new-instance v0, Lcom/android/server/lights/LightState; */
/* move-object/from16 v20, v0 */
/* move/from16 v21, v18 */
/* move/from16 v22, v13 */
/* move/from16 v23, v15 */
/* move/from16 v24, v14 */
/* move/from16 v25, v16 */
/* invoke-direct/range {v20 ..v25}, Lcom/android/server/lights/LightState;-><init>(IIIII)V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 108 */
/* nop */
} // .end local v12 # "childNodes":Lorg/w3c/dom/NodeList;
/* add-int/lit8 v11, v11, 0x1 */
/* move-object/from16 v0, v26 */
/* move-object/from16 v7, v27 */
int v8 = 1; // const/4 v8, 0x1
/* goto/16 :goto_1 */
/* .line 135 */
} // .end local v11 # "i":I
} // .end local v26 # "doc":Lorg/w3c/dom/Document;
} // .end local v27 # "lightList":Lorg/w3c/dom/NodeList;
/* .restart local v0 # "doc":Lorg/w3c/dom/Document; */
/* .restart local v7 # "lightList":Lorg/w3c/dom/NodeList; */
} // :cond_8
/* nop */
/* .line 141 */
libcore.io.IoUtils .closeQuietly ( v6 );
/* .line 135 */
/* .line 141 */
} // .end local v0 # "doc":Lorg/w3c/dom/Document;
} // .end local v7 # "lightList":Lorg/w3c/dom/NodeList;
} // .end local v9 # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
} // .end local v10 # "builder":Ljavax/xml/parsers/DocumentBuilder;
} // .end local v13 # "flashMode":I
} // .end local v14 # "offMS":I
} // .end local v15 # "onMS":I
} // .end local v16 # "brightnessMode":I
} // .end local v18 # "colorARGB":I
/* :catchall_0 */
/* move-exception v0 */
/* .line 136 */
/* :catch_0 */
/* move-exception v0 */
/* .line 137 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_3
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "can\'t find xml file : "; // const-string v8, "can\'t find xml file : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mStyleArray;
(( android.util.SparseArray ) v8 ).get ( v2 ); // invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = ".xml--Please check the path to confirm : "; // const-string v8, ".xml--Please check the path to confirm : "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " -- "; // const-string v7, " -- "
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 138 */
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 137 */
android.util.Slog .e ( v4,v3 );
/* .line 139 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 141 */
} // .end local v0 # "e":Ljava/lang/Exception;
libcore.io.IoUtils .closeQuietly ( v6 );
/* .line 142 */
/* nop */
/* .line 143 */
/* .line 141 */
} // :goto_6
libcore.io.IoUtils .closeQuietly ( v6 );
/* .line 142 */
/* throw v0 */
/* .line 76 */
} // :cond_9
} // :goto_7
/* nop */
/* .line 141 */
libcore.io.IoUtils .closeQuietly ( v6 );
/* .line 76 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4464da4d -> :sswitch_4 */
/* 0x341bc5 -> :sswitch_3 */
/* 0x64c1755 -> :sswitch_2 */
/* 0x76098eaf -> :sswitch_1 */
/* 0x76464994 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
