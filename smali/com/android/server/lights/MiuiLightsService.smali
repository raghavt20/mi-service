.class public Lcom/android/server/lights/MiuiLightsService;
.super Lcom/android/server/lights/LightsService;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;,
        Lcom/android/server/lights/MiuiLightsService$LightImpl;,
        Lcom/android/server/lights/MiuiLightsService$LocalService;,
        Lcom/android/server/lights/MiuiLightsService$LightContentObserver;,
        Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver;,
        Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;,
        Lcom/android/server/lights/MiuiLightsService$LightsThread;,
        Lcom/android/server/lights/MiuiLightsService$DataCaptureListener;
    }
.end annotation


# static fields
.field private static final LED_END_WORKTIME_DEF:J = 0x4ef6d80L

.field private static final LED_START_WORKTIME_DEF:J = 0x1808580L

.field public static final LIGHT_ID_COLORFUL:I = 0x8

.field public static final LIGHT_ID_LED:I = 0xb

.field public static final LIGHT_ID_MUSIC:I = 0x9

.field public static final LIGHT_ID_PRIVACY:I = 0xa

.field private static final LIGHT_ON_MS:I = 0x1f4

.field private static final ONE_DAY:J = 0x5265c00L

.field private static final ONE_HOUR:J = 0x36ee80L

.field private static final ONE_MINUTE:J = 0xea60L

.field private static final STOP_FLASH_MSG:I = 0x1

.field private static sInstance:Lcom/android/server/lights/MiuiLightsService;


# instance fields
.field private light_end_time:J

.field private light_start_time:J

.field private final localService:Lcom/android/server/lights/MiuiLightsService$LocalService;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mAudioManagerPlaybackCb:Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;

.field private mBatteryManagerInternal:Landroid/os/BatteryManagerInternal;

.field private final mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

.field private mContext:Landroid/content/Context;

.field private mDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

.field private mHandler:Landroid/os/Handler;

.field private mIsLedTurnOn:Z

.field private mIsWorkTime:Z

.field private mLedEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLedLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

.field private mLightContentObserver:Lcom/android/server/lights/MiuiLightsService$LightContentObserver;

.field private mLightHandler:Landroid/os/Handler;

.field private final mLightStyleLoader:Lcom/android/server/lights/LightStyleLoader;

.field private final mLock:Ljava/lang/Object;

.field private final mMusicLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

.field private mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

.field private mPlayingPid:I

.field private final mPreviousLights:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/lights/LightState;",
            ">;"
        }
    .end annotation
.end field

.field private final mPreviousLightsLimit:I

.field private final mPrivacyLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

.field private mResolver:Landroid/content/ContentResolver;

.field private final mService:Landroid/os/IBinder;

.field private mSupportButtonLight:Z

.field private mSupportColorGameLed:Z

.field private mSupportColorfulLed:Z

.field private mSupportLedLight:Z

.field private mSupportLedSchedule:Z

.field private mSupportTapFingerprint:Z

.field private mThread:Lcom/android/server/lights/MiuiLightsService$LightsThread;

.field private final mTmpWorkSource:Landroid/os/WorkSource;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static bridge synthetic -$$Nest$fgetlight_end_time(Lcom/android/server/lights/MiuiLightsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetlight_start_time(Lcom/android/server/lights/MiuiLightsService;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetlocalService(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LocalService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->localService:Lcom/android/server/lights/MiuiLightsService$LocalService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAudioManager(Lcom/android/server/lights/MiuiLightsService;)Landroid/media/AudioManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManager:Landroid/media/AudioManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBatteryManagerInternal(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/BatteryManagerInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mBatteryManagerInternal:Landroid/os/BatteryManagerInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsLedTurnOn(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsLedTurnOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsWorkTime(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLedEvents(Lcom/android/server/lights/MiuiLightsService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLedLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mLedLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLightHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLightStyleLoader(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/LightStyleLoader;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightStyleLoader:Lcom/android/server/lights/LightStyleLoader;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMusicLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mMusicLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageManagerInt(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/pm/PackageManagerInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPrivacyLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mPrivacyLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/IBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mService:Landroid/os/IBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportButtonLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportColorGameLed(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportLedLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportTapFingerprint(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmThread(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightsThread;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mThread:Lcom/android/server/lights/MiuiLightsService$LightsThread;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTmpWorkSource(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/WorkSource;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mTmpWorkSource:Landroid/os/WorkSource;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWakeLock(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/PowerManager$WakeLock;
    .locals 0

    iget-object p0, p0, Lcom/android/server/lights/MiuiLightsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputlight_end_time(Lcom/android/server/lights/MiuiLightsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputlight_start_time(Lcom/android/server/lights/MiuiLightsService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsLedTurnOn(Lcom/android/server/lights/MiuiLightsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/lights/MiuiLightsService;->mIsLedTurnOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsWorkTime(Lcom/android/server/lights/MiuiLightsService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLedEvents(Lcom/android/server/lights/MiuiLightsService;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPlayingPid(Lcom/android/server/lights/MiuiLightsService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmThread(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$LightsThread;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService;->mThread:Lcom/android/server/lights/MiuiLightsService$LightsThread;

    return-void
.end method

.method static bridge synthetic -$$Nest$maddToLightCollectionLocked(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/LightState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService;->addToLightCollectionLocked(Lcom/android/server/lights/LightState;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckCallerVerify(Lcom/android/server/lights/MiuiLightsService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService;->checkCallerVerify(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoCancelColorfulLightLocked(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->doCancelColorfulLightLocked()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetDataCaptureListener(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->getDataCaptureListener()Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misDisableButtonLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isDisableButtonLight()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misLightEnable(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isLightEnable()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misMusicLightPlaying(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isMusicLightPlaying()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misSceneUncomfort(Lcom/android/server/lights/MiuiLightsService;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService;->isSceneUncomfort(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misTurnOnBatteryLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnBatteryLight()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misTurnOnButtonLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnButtonLight()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misTurnOnLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misTurnOnMusicLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnMusicLight()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misTurnOnNotificationLight(Lcom/android/server/lights/MiuiLightsService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnNotificationLight()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mrecoveryBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->recoveryBatteryLight()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterAudioPlaybackCallback(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->registerAudioPlaybackCallback()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->releaseVisualizer()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportLedEventLocked(Lcom/android/server/lights/MiuiLightsService;IZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService;->reportLedEventLocked(IZII)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mturnoffBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->turnoffBatteryLight()V

    return-void
.end method

.method static bridge synthetic -$$Nest$munregisterAudioPlaybackCallback(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->unregisterAudioPlaybackCallback()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateLightState(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateLightState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateWorkState(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateWorkState()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 103
    invoke-direct {p0, p1}, Lcom/android/server/lights/LightsService;-><init>(Landroid/content/Context;)V

    .line 72
    const/16 v0, 0x64

    iput v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLightsLimit:I

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLock:Ljava/lang/Object;

    .line 74
    new-instance v0, Landroid/os/WorkSource;

    invoke-direct {v0}, Landroid/os/WorkSource;-><init>()V

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mTmpWorkSource:Landroid/os/WorkSource;

    .line 89
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    .line 474
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$1;

    invoke-direct {v1, p0}, Lcom/android/server/lights/MiuiLightsService$1;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mHandler:Landroid/os/Handler;

    .line 492
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$2;

    invoke-direct {v1, p0}, Lcom/android/server/lights/MiuiLightsService$2;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mService:Landroid/os/IBinder;

    .line 650
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I

    .line 651
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;-><init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener-IA;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManagerPlaybackCb:Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;

    .line 104
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    .line 105
    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->populateAvailableLightsforMiui()V

    .line 106
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    .line 107
    new-instance v1, Lcom/android/server/lights/LightStyleLoader;

    invoke-direct {v1, p1}, Lcom/android/server/lights/LightStyleLoader;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLightStyleLoader:Lcom/android/server/lights/LightStyleLoader;

    .line 108
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    const/16 v4, 0x8

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 109
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    const/16 v4, 0x9

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mMusicLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 110
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    const/16 v4, 0xa

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mPrivacyLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 111
    new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    const/16 v4, 0xb

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 112
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 113
    .local v1, "pm":Landroid/os/PowerManager;
    const-string v2, "*lights*"

    invoke-virtual {v1, v0, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 114
    invoke-virtual {v2, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 115
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "MiuiLightsHandlerThread"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 116
    .local v0, "miuiLightsHandlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 117
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightHandler:Landroid/os/Handler;

    .line 118
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLights:Ljava/util/LinkedList;

    .line 119
    new-instance v2, Lcom/android/server/lights/MiuiLightsService$LocalService;

    invoke-direct {v2, p0}, Lcom/android/server/lights/MiuiLightsService$LocalService;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    iput-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->localService:Lcom/android/server/lights/MiuiLightsService$LocalService;

    .line 120
    const-class v3, Lmiui/app/MiuiLightsManagerInternal;

    invoke-static {v3, v2}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 121
    return-void
.end method

.method private addToLightCollectionLocked(Lcom/android/server/lights/LightState;)V
    .locals 2
    .param p1, "lightState"    # Lcom/android/server/lights/LightState;

    .line 1038
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLights:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 1039
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLights:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 1041
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLights:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1042
    return-void
.end method

.method private checkCallerVerify(Ljava/lang/String;)V
    .locals 5
    .param p1, "callingPackage"    # Ljava/lang/String;

    .line 780
    if-eqz p1, :cond_3

    .line 782
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    if-eqz v0, :cond_2

    .line 784
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 785
    .local v0, "uid":I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v1

    .line 786
    .local v1, "appid":I
    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_1

    const/16 v2, 0x3e9

    if-eq v1, v2, :cond_1

    const/16 v2, 0x3f5

    if-eq v1, v2, :cond_1

    if-eqz v0, :cond_1

    const/16 v2, 0x7d0

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 788
    :cond_0
    new-instance v2, Ljava/lang/SecurityException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Disallowed call for uid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 790
    :cond_1
    :goto_0
    return-void

    .line 783
    .end local v0    # "uid":I
    .end local v1    # "appid":I
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current devices doesn\'t support ColorfulLed!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 781
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callingPackage is invalid!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private doCancelColorfulLightLocked()V
    .locals 8

    .line 641
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mThread:Lcom/android/server/lights/MiuiLightsService$LightsThread;

    if-eqz v0, :cond_0

    .line 642
    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->cancel()V

    .line 643
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mThread:Lcom/android/server/lights/MiuiLightsService$LightsThread;

    .line 644
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    .line 645
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 647
    :cond_0
    return-void
.end method

.method private getDataCaptureListener()Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;
    .locals 1

    .line 711
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    if-nez v0, :cond_0

    .line 712
    new-instance v0, Lcom/android/server/lights/MiuiLightsService$3;

    invoke-direct {v0, p0}, Lcom/android/server/lights/MiuiLightsService$3;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    .line 730
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/lights/MiuiLightsService;
    .locals 1

    .line 489
    sget-object v0, Lcom/android/server/lights/MiuiLightsService;->sInstance:Lcom/android/server/lights/MiuiLightsService;

    return-object v0
.end method

.method private isDisableButtonLight()Z
    .locals 5

    .line 1025
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z

    const-string v1, "screen_buttons_state"

    const/4 v2, 0x1

    const/4 v3, -0x2

    const/4 v4, 0x0

    if-eqz v0, :cond_2

    .line 1026
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    .line 1028
    const-string/jumbo v1, "single_key_use_enable"

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v4

    goto :goto_1

    :cond_1
    :goto_0
    nop

    .line 1026
    :goto_1
    return v2

    .line 1032
    :cond_2
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    move v2, v4

    :goto_2
    return v2
.end method

.method private isLightEnable()Z
    .locals 1

    .line 991
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isMusicLightPlaying()Z
    .locals 2

    .line 753
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isSceneUncomfort(I)Z
    .locals 3
    .param p1, "mId"    # I

    .line 981
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    .line 982
    invoke-static {v0}, Landroid/provider/MiuiSettings$SilenceMode;->getZenMode(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 984
    const-string v0, "LightsService"

    const-string v2, "Scene is uncomfort , lights skip!"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    return v1

    .line 987
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isTurnOnBatteryLight()Z
    .locals 4

    .line 1010
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "battery_light_turn_on"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3
.end method

.method private isTurnOnButtonLight()Z
    .locals 4

    .line 1005
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "screen_buttons_turn_on"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3
.end method

.method private isTurnOnLight()Z
    .locals 4

    .line 995
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "light_turn_on"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3
.end method

.method private isTurnOnMusicLight()Z
    .locals 4

    .line 1020
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "music_light_turn_on"

    const/4 v2, -0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3
.end method

.method private isTurnOnNotificationLight()Z
    .locals 4

    .line 1015
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, -0x2

    const-string v2, "notification_light_turn_on"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3
.end method

.method private loadSupportLights()V
    .locals 2

    .line 152
    const-string/jumbo v0, "support_led_light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z

    .line 153
    const-string/jumbo v0, "support_button_light"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z

    .line 154
    const-string/jumbo v0, "support_tap_fingerprint_sensor_to_home"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z

    .line 155
    const-string/jumbo v0, "support_led_colorful"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    .line 156
    const-string/jumbo v0, "support_led_colorful_game"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z

    .line 157
    const-string/jumbo v0, "support_led_schedule"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedSchedule:Z

    .line 158
    return-void
.end method

.method private populateAvailableLightsforMiui()V
    .locals 7

    .line 124
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsById:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 125
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsById:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 126
    .local v1, "type":I
    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 127
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    new-instance v3, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget-object v4, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsById:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/lights/LightsService$LightImpl;

    iget-object v5, v5, Lcom/android/server/lights/LightsService$LightImpl;->mHwLight:Landroid/hardware/light/HwLight;

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;Lcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V

    aput-object v3, v2, v1

    .line 124
    .end local v1    # "type":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 130
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private recoveryBatteryLight()V
    .locals 7

    .line 768
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "skip light bat , cur light id :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget v1, v1, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LightsService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    return-void

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 773
    .local v0, "batteryLight":Lcom/android/server/lights/MiuiLightsService$LightImpl;
    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmColor(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmDisabled(Lcom/android/server/lights/MiuiLightsService$LightImpl;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmId(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/lights/MiuiLightsService;->isSceneUncomfort(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isLightEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 774
    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmColor(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v2

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmMode(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v3

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmOnMS(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v4

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmOffMS(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v5

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmBrightnessMode(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v6

    move-object v1, v0

    invoke-static/range {v1 .. v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 777
    :cond_1
    return-void
.end method

.method private registerAudioPlaybackCallback()V
    .locals 3

    .line 734
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnMusicLight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManagerPlaybackCb:Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->registerAudioPlaybackCallback(Landroid/media/AudioManager$AudioPlaybackCallback;Landroid/os/Handler;)V

    .line 737
    :cond_0
    return-void
.end method

.method private releaseVisualizer()V
    .locals 1

    .line 746
    invoke-static {}, Lcom/android/server/lights/VisualizerHolder;->getInstance()Lcom/android/server/lights/VisualizerHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/lights/VisualizerHolder;->release()V

    .line 747
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mMusicLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->turnOff()V

    .line 748
    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->recoveryBatteryLight()V

    .line 749
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I

    .line 750
    return-void
.end method

.method private reportLedEventLocked(IZII)V
    .locals 5
    .param p1, "mId"    # I
    .param p2, "isTurnOn"    # Z
    .param p3, "onMS"    # I
    .param p4, "offMs"    # I

    .line 451
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 453
    .local v0, "info":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v1, "type"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 454
    const-string v1, "isTurnOn"

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 455
    const-string v1, "onMs"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 456
    const-string v1, "offMs"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 457
    const-string/jumbo v1, "time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 458
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    if-nez v1, :cond_1

    .line 459
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    .line 461
    :cond_1
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v3, 0x1e

    if-lt v1, v3, :cond_2

    .line 463
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    const-string v3, "led"

    iget-object v4, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    invoke-virtual {v1, v3, v4, v2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportEvents(Ljava/lang/String;Ljava/util/List;Z)V

    .line 464
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLedEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :cond_2
    goto :goto_1

    .line 467
    :catch_0
    move-exception v1

    .line 468
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 470
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private turnoffBatteryLight()V
    .locals 7

    .line 760
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 761
    .local v0, "batteryLight":Lcom/android/server/lights/MiuiLightsService$LightImpl;
    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$fgetmColor(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 762
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    invoke-static/range {v1 .. v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 764
    :cond_0
    return-void
.end method

.method private unregisterAudioPlaybackCallback()V
    .locals 2

    .line 740
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManagerPlaybackCb:Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;

    if-eqz v0, :cond_0

    .line 741
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->unregisterAudioPlaybackCallback(Landroid/media/AudioManager$AudioPlaybackCallback;)V

    .line 743
    :cond_0
    return-void
.end method

.method private updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "defKey"    # Ljava/lang/String;
    .param p2, "settingKey"    # Ljava/lang/String;
    .param p3, "defValue"    # Ljava/lang/Boolean;

    .line 195
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 196
    .local v0, "defaultValue":I
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, -0x2

    invoke-static {v1, p2, v0, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 198
    .local v1, "currentValue":I
    if-ne v1, v0, :cond_0

    .line 199
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, p2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 201
    :cond_0
    return-void
.end method

.method private updateLightEnableAndType()V
    .locals 6

    .line 161
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z

    const/4 v1, 0x0

    const/4 v2, -0x2

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "screen_buttons_state"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 164
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 166
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z

    if-eqz v0, :cond_1

    .line 167
    nop

    .line 168
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 167
    const-string v4, "default_notification_led_on"

    const-string v5, "notification_light_turn_on"

    invoke-direct {p0, v4, v5, v3}, Lcom/android/server/lights/MiuiLightsService;->updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 169
    nop

    .line 170
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 169
    const-string v3, "default_battery_led_on"

    const-string v4, "battery_light_turn_on"

    invoke-direct {p0, v3, v4, v0}, Lcom/android/server/lights/MiuiLightsService;->updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 171
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v3, 0x4

    aget-object v0, v0, v3

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 172
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 174
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedSchedule:Z

    if-eqz v0, :cond_3

    .line 175
    nop

    .line 176
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 175
    const-string v1, "default_schedule_led_on"

    const-string v3, "light_turn_on_Time"

    invoke-direct {p0, v1, v3, v0}, Lcom/android/server/lights/MiuiLightsService;->updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 177
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "light_turn_on_endTime"

    const-wide/32 v3, 0x4ef6d80

    invoke-static {v0, v1, v3, v4, v2}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J

    .line 179
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "light_turn_on_startTime"

    const-wide/32 v3, 0x1808580

    invoke-static {v0, v1, v3, v4, v2}, Landroid/provider/Settings$Secure;->getLongForUser(Landroid/content/ContentResolver;Ljava/lang/String;JI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J

    .line 181
    invoke-virtual {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnTimeLight()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182
    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateWorkState()V

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;-><init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver-IA;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.TIME_TICK"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/server/lights/MiuiLightsService;->mLightHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 187
    :cond_3
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    if-eqz v0, :cond_4

    .line 188
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mAudioManager:Landroid/media/AudioManager;

    .line 189
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPackageManagerInt:Landroid/content/pm/PackageManagerInternal;

    .line 190
    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->registerAudioPlaybackCallback()V

    .line 192
    :cond_4
    return-void
.end method

.method private updateLightState()V
    .locals 3

    .line 973
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 974
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 975
    .local v0, "light":Lcom/android/server/lights/MiuiLightsService$LightImpl;
    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 976
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    move-object v0, v1

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    .line 977
    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V

    .line 978
    return-void
.end method

.method private updateWorkState()V
    .locals 9

    .line 818
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedSchedule:Z

    if-nez v0, :cond_0

    return-void

    .line 819
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 820
    .local v0, "cal":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    int-to-long v1, v1

    const-wide/32 v3, 0x36ee80

    mul-long/2addr v1, v3

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    int-to-long v3, v3

    const-wide/32 v5, 0xea60

    mul-long/2addr v3, v5

    add-long/2addr v1, v3

    .line 821
    .local v1, "now_stamp":J
    iget-wide v3, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J

    iget-wide v5, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J

    cmp-long v7, v3, v5

    const/4 v8, 0x0

    if-ltz v7, :cond_2

    .line 822
    cmp-long v3, v1, v3

    if-gtz v3, :cond_1

    cmp-long v3, v1, v5

    if-gez v3, :cond_3

    .line 823
    :cond_1
    iput-boolean v8, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    .line 824
    return-void

    .line 827
    :cond_2
    cmp-long v3, v1, v3

    if-lez v3, :cond_3

    cmp-long v3, v1, v5

    if-gez v3, :cond_3

    .line 828
    iput-boolean v8, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    .line 829
    return-void

    .line 832
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    .line 833
    return-void
.end method


# virtual methods
.method public dumpLight(Ljava/io/PrintWriter;Lcom/android/server/notification/NotificationManagerService$DumpFilter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "filter"    # Lcom/android/server/notification/NotificationManagerService$DumpFilter;

    .line 1052
    const-string v0, "MiuiLightsService Status:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1053
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1054
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ZenMode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MiuiSettings$SilenceMode;->getZenMode(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1055
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mSupportColorFulLight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1056
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mSupportGameColorFulLight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1057
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Led Working Time: state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " end:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1058
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mSupportTapFingerprint:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1059
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mSupportButtonLight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1060
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mSupportLedLight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1061
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mIsLedTurnOn:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mIsLedTurnOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1062
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isLightEnable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isLightEnable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1063
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isTurnOnLight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1064
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isTurnOnButtonLight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnButtonLight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1065
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isTurnOnBatteryLight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnBatteryLight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1066
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isTurnOnNotificationLight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnNotificationLight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1067
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " isTurnOnMusicLight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnMusicLight()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1068
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 1069
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 1070
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1068
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1073
    .end local v1    # "i":I
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mColorfulLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1074
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService;->mMusicLight:Lcom/android/server/lights/MiuiLightsService$LightImpl;

    invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1075
    const-string v1, "  Previous Lights:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1076
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLights:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/lights/LightState;

    .line 1077
    .local v2, "lightstate":Lcom/android/server/lights/LightState;
    const-string v3, "    "

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1078
    invoke-virtual {v2}, Lcom/android/server/lights/LightState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1079
    .end local v2    # "lightstate":Lcom/android/server/lights/LightState;
    goto :goto_1

    .line 1080
    :cond_2
    monitor-exit v0

    .line 1081
    return-void

    .line 1080
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isTurnOnTimeLight()Z
    .locals 4

    .line 1000
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "light_turn_on_Time"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    return v3
.end method

.method public onBootPhase(I)V
    .locals 5
    .param p1, "phase"    # I

    .line 140
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->loadSupportLights()V

    .line 142
    invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateLightEnableAndType()V

    .line 143
    new-instance v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;

    invoke-direct {v0, p0}, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mLightContentObserver:Lcom/android/server/lights/MiuiLightsService$LightContentObserver;

    .line 144
    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->observe()V

    .line 145
    const-class v0, Landroid/os/BatteryManagerInternal;

    invoke-virtual {p0, v0}, Lcom/android/server/lights/MiuiLightsService;->getLocalService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/BatteryManagerInternal;

    iput-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mBatteryManagerInternal:Landroid/os/BatteryManagerInternal;

    .line 146
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver;-><init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver-IA;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.USER_SWITCHED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/server/lights/MiuiLightsService;->mLightHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 149
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .line 134
    invoke-super {p0}, Lcom/android/server/lights/LightsService;->onStart()V

    .line 135
    sput-object p0, Lcom/android/server/lights/MiuiLightsService;->sInstance:Lcom/android/server/lights/MiuiLightsService;

    .line 136
    return-void
.end method
