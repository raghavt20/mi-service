public class com.android.server.lights.MiuiLightsService extends com.android.server.lights.LightsService {
	 /* .source "MiuiLightsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;, */
	 /* Lcom/android/server/lights/MiuiLightsService$LightImpl;, */
	 /* Lcom/android/server/lights/MiuiLightsService$LocalService;, */
	 /* Lcom/android/server/lights/MiuiLightsService$LightContentObserver;, */
	 /* Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver;, */
	 /* Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;, */
	 /* Lcom/android/server/lights/MiuiLightsService$LightsThread;, */
	 /* Lcom/android/server/lights/MiuiLightsService$DataCaptureListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long LED_END_WORKTIME_DEF;
private static final Long LED_START_WORKTIME_DEF;
public static final Integer LIGHT_ID_COLORFUL;
public static final Integer LIGHT_ID_LED;
public static final Integer LIGHT_ID_MUSIC;
public static final Integer LIGHT_ID_PRIVACY;
private static final Integer LIGHT_ON_MS;
private static final Long ONE_DAY;
private static final Long ONE_HOUR;
private static final Long ONE_MINUTE;
private static final Integer STOP_FLASH_MSG;
private static com.android.server.lights.MiuiLightsService sInstance;
/* # instance fields */
private Long light_end_time;
private Long light_start_time;
private final com.android.server.lights.MiuiLightsService$LocalService localService;
private android.media.AudioManager mAudioManager;
private final com.android.server.lights.MiuiLightsService$AudioManagerPlaybackListener mAudioManagerPlaybackCb;
private android.os.BatteryManagerInternal mBatteryManagerInternal;
private final com.android.server.lights.MiuiLightsService$LightImpl mColorfulLight;
private android.content.Context mContext;
private com.android.server.lights.VisualizerHolder$OnDataCaptureListener mDataCaptureListener;
private android.os.Handler mHandler;
private Boolean mIsLedTurnOn;
private Boolean mIsWorkTime;
private java.util.List mLedEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.lights.MiuiLightsService$LightImpl mLedLight;
private com.android.server.lights.MiuiLightsService$LightContentObserver mLightContentObserver;
private android.os.Handler mLightHandler;
private final com.android.server.lights.LightStyleLoader mLightStyleLoader;
private final java.lang.Object mLock;
private final com.android.server.lights.MiuiLightsService$LightImpl mMusicLight;
private android.content.pm.PackageManagerInternal mPackageManagerInt;
private Integer mPlayingPid;
private final java.util.LinkedList mPreviousLights;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/lights/LightState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final Integer mPreviousLightsLimit;
private final com.android.server.lights.MiuiLightsService$LightImpl mPrivacyLight;
private android.content.ContentResolver mResolver;
private final android.os.IBinder mService;
private Boolean mSupportButtonLight;
private Boolean mSupportColorGameLed;
private Boolean mSupportColorfulLed;
private Boolean mSupportLedLight;
private Boolean mSupportLedSchedule;
private Boolean mSupportTapFingerprint;
private com.android.server.lights.MiuiLightsService$LightsThread mThread;
private final android.os.WorkSource mTmpWorkSource;
private final android.os.PowerManager$WakeLock mWakeLock;
/* # direct methods */
static Long -$$Nest$fgetlight_end_time ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetlight_start_time ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J */
/* return-wide v0 */
} // .end method
static com.android.server.lights.MiuiLightsService$LocalService -$$Nest$fgetlocalService ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.localService;
} // .end method
static android.media.AudioManager -$$Nest$fgetmAudioManager ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAudioManager;
} // .end method
static android.os.BatteryManagerInternal -$$Nest$fgetmBatteryManagerInternal ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBatteryManagerInternal;
} // .end method
static com.android.server.lights.MiuiLightsService$LightImpl -$$Nest$fgetmColorfulLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mColorfulLight;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsLedTurnOn ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsLedTurnOn:Z */
} // .end method
static Boolean -$$Nest$fgetmIsWorkTime ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
} // .end method
static java.util.List -$$Nest$fgetmLedEvents ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLedEvents;
} // .end method
static com.android.server.lights.MiuiLightsService$LightImpl -$$Nest$fgetmLedLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLedLight;
} // .end method
static android.os.Handler -$$Nest$fgetmLightHandler ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLightHandler;
} // .end method
static com.android.server.lights.LightStyleLoader -$$Nest$fgetmLightStyleLoader ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLightStyleLoader;
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static com.android.server.lights.MiuiLightsService$LightImpl -$$Nest$fgetmMusicLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMusicLight;
} // .end method
static android.content.pm.PackageManagerInternal -$$Nest$fgetmPackageManagerInt ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageManagerInt;
} // .end method
static Integer -$$Nest$fgetmPlayingPid ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I */
} // .end method
static com.android.server.lights.MiuiLightsService$LightImpl -$$Nest$fgetmPrivacyLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPrivacyLight;
} // .end method
static android.content.ContentResolver -$$Nest$fgetmResolver ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mResolver;
} // .end method
static android.os.IBinder -$$Nest$fgetmService ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mService;
} // .end method
static Boolean -$$Nest$fgetmSupportButtonLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z */
} // .end method
static Boolean -$$Nest$fgetmSupportColorGameLed ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z */
} // .end method
static Boolean -$$Nest$fgetmSupportLedLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z */
} // .end method
static Boolean -$$Nest$fgetmSupportTapFingerprint ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z */
} // .end method
static com.android.server.lights.MiuiLightsService$LightsThread -$$Nest$fgetmThread ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mThread;
} // .end method
static android.os.WorkSource -$$Nest$fgetmTmpWorkSource ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTmpWorkSource;
} // .end method
static android.os.PowerManager$WakeLock -$$Nest$fgetmWakeLock ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWakeLock;
} // .end method
static void -$$Nest$fputlight_end_time ( com.android.server.lights.MiuiLightsService p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J */
return;
} // .end method
static void -$$Nest$fputlight_start_time ( com.android.server.lights.MiuiLightsService p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J */
return;
} // .end method
static void -$$Nest$fputmIsLedTurnOn ( com.android.server.lights.MiuiLightsService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/lights/MiuiLightsService;->mIsLedTurnOn:Z */
return;
} // .end method
static void -$$Nest$fputmIsWorkTime ( com.android.server.lights.MiuiLightsService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
return;
} // .end method
static void -$$Nest$fputmLedEvents ( com.android.server.lights.MiuiLightsService p0, java.util.List p1 ) { //bridge//synthethic
/* .locals 0 */
this.mLedEvents = p1;
return;
} // .end method
static void -$$Nest$fputmPlayingPid ( com.android.server.lights.MiuiLightsService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I */
return;
} // .end method
static void -$$Nest$fputmThread ( com.android.server.lights.MiuiLightsService p0, com.android.server.lights.MiuiLightsService$LightsThread p1 ) { //bridge//synthethic
/* .locals 0 */
this.mThread = p1;
return;
} // .end method
static void -$$Nest$maddToLightCollectionLocked ( com.android.server.lights.MiuiLightsService p0, com.android.server.lights.LightState p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService;->addToLightCollectionLocked(Lcom/android/server/lights/LightState;)V */
return;
} // .end method
static void -$$Nest$mcheckCallerVerify ( com.android.server.lights.MiuiLightsService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService;->checkCallerVerify(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mdoCancelColorfulLightLocked ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->doCancelColorfulLightLocked()V */
return;
} // .end method
static com.android.server.lights.VisualizerHolder$OnDataCaptureListener -$$Nest$mgetDataCaptureListener ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->getDataCaptureListener()Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener; */
} // .end method
static Boolean -$$Nest$misDisableButtonLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isDisableButtonLight()Z */
} // .end method
static Boolean -$$Nest$misLightEnable ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isLightEnable()Z */
} // .end method
static Boolean -$$Nest$misMusicLightPlaying ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isMusicLightPlaying()Z */
} // .end method
static Boolean -$$Nest$misSceneUncomfort ( com.android.server.lights.MiuiLightsService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService;->isSceneUncomfort(I)Z */
} // .end method
static Boolean -$$Nest$misTurnOnBatteryLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnBatteryLight()Z */
} // .end method
static Boolean -$$Nest$misTurnOnButtonLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnButtonLight()Z */
} // .end method
static Boolean -$$Nest$misTurnOnLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z */
} // .end method
static Boolean -$$Nest$misTurnOnMusicLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnMusicLight()Z */
} // .end method
static Boolean -$$Nest$misTurnOnNotificationLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnNotificationLight()Z */
} // .end method
static void -$$Nest$mrecoveryBatteryLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->recoveryBatteryLight()V */
return;
} // .end method
static void -$$Nest$mregisterAudioPlaybackCallback ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->registerAudioPlaybackCallback()V */
return;
} // .end method
static void -$$Nest$mreleaseVisualizer ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->releaseVisualizer()V */
return;
} // .end method
static void -$$Nest$mreportLedEventLocked ( com.android.server.lights.MiuiLightsService p0, Integer p1, Boolean p2, Integer p3, Integer p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService;->reportLedEventLocked(IZII)V */
return;
} // .end method
static void -$$Nest$mturnoffBatteryLight ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->turnoffBatteryLight()V */
return;
} // .end method
static void -$$Nest$munregisterAudioPlaybackCallback ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->unregisterAudioPlaybackCallback()V */
return;
} // .end method
static void -$$Nest$mupdateLightState ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateLightState()V */
return;
} // .end method
static void -$$Nest$mupdateWorkState ( com.android.server.lights.MiuiLightsService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateWorkState()V */
return;
} // .end method
public com.android.server.lights.MiuiLightsService ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 103 */
/* invoke-direct {p0, p1}, Lcom/android/server/lights/LightsService;-><init>(Landroid/content/Context;)V */
/* .line 72 */
/* const/16 v0, 0x64 */
/* iput v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPreviousLightsLimit:I */
/* .line 73 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 74 */
/* new-instance v0, Landroid/os/WorkSource; */
/* invoke-direct {v0}, Landroid/os/WorkSource;-><init>()V */
this.mTmpWorkSource = v0;
/* .line 89 */
/* const-wide/32 v0, 0x5265c00 */
/* iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J */
/* .line 90 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J */
/* .line 91 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
/* .line 474 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/lights/MiuiLightsService$1;-><init>(Lcom/android/server/lights/MiuiLightsService;)V */
this.mHandler = v1;
/* .line 492 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/lights/MiuiLightsService$2;-><init>(Lcom/android/server/lights/MiuiLightsService;)V */
this.mService = v1;
/* .line 650 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I */
/* .line 651 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;-><init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener-IA;)V */
this.mAudioManagerPlaybackCb = v1;
/* .line 104 */
this.mContext = p1;
/* .line 105 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->populateAvailableLightsforMiui()V */
/* .line 106 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v1;
/* .line 107 */
/* new-instance v1, Lcom/android/server/lights/LightStyleLoader; */
/* invoke-direct {v1, p1}, Lcom/android/server/lights/LightStyleLoader;-><init>(Landroid/content/Context;)V */
this.mLightStyleLoader = v1;
/* .line 108 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v3 = this.mContext;
/* const/16 v4, 0x8 */
/* invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V */
this.mColorfulLight = v1;
/* .line 109 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v3 = this.mContext;
/* const/16 v4, 0x9 */
/* invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V */
this.mMusicLight = v1;
/* .line 110 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v3 = this.mContext;
/* const/16 v4, 0xa */
/* invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V */
this.mPrivacyLight = v1;
/* .line 111 */
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v3 = this.mContext;
/* const/16 v4, 0xb */
/* invoke-direct {v1, p0, v3, v4, v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V */
this.mLedLight = v1;
/* .line 112 */
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/PowerManager; */
/* .line 113 */
/* .local v1, "pm":Landroid/os/PowerManager; */
final String v2 = "*lights*"; // const-string v2, "*lights*"
(( android.os.PowerManager ) v1 ).newWakeLock ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mWakeLock = v2;
/* .line 114 */
(( android.os.PowerManager$WakeLock ) v2 ).setReferenceCounted ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V
/* .line 115 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v2 = "MiuiLightsHandlerThread"; // const-string v2, "MiuiLightsHandlerThread"
/* invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 116 */
/* .local v0, "miuiLightsHandlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 117 */
/* new-instance v2, Landroid/os/Handler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mLightHandler = v2;
/* .line 118 */
/* new-instance v2, Ljava/util/LinkedList; */
/* invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V */
this.mPreviousLights = v2;
/* .line 119 */
/* new-instance v2, Lcom/android/server/lights/MiuiLightsService$LocalService; */
/* invoke-direct {v2, p0}, Lcom/android/server/lights/MiuiLightsService$LocalService;-><init>(Lcom/android/server/lights/MiuiLightsService;)V */
this.localService = v2;
/* .line 120 */
/* const-class v3, Lmiui/app/MiuiLightsManagerInternal; */
com.android.server.LocalServices .addService ( v3,v2 );
/* .line 121 */
return;
} // .end method
private void addToLightCollectionLocked ( com.android.server.lights.LightState p0 ) {
/* .locals 2 */
/* .param p1, "lightState" # Lcom/android/server/lights/LightState; */
/* .line 1038 */
v0 = this.mPreviousLights;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* const/16 v1, 0x64 */
/* if-le v0, v1, :cond_0 */
/* .line 1039 */
v0 = this.mPreviousLights;
(( java.util.LinkedList ) v0 ).removeFirst ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 1041 */
} // :cond_0
v0 = this.mPreviousLights;
(( java.util.LinkedList ) v0 ).addLast ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 1042 */
return;
} // .end method
private void checkCallerVerify ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .line 780 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 782 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 784 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 785 */
/* .local v0, "uid":I */
v1 = android.os.UserHandle .getAppId ( v0 );
/* .line 786 */
/* .local v1, "appid":I */
/* const/16 v2, 0x3e8 */
/* if-eq v1, v2, :cond_1 */
/* const/16 v2, 0x3e9 */
/* if-eq v1, v2, :cond_1 */
/* const/16 v2, 0x3f5 */
/* if-eq v1, v2, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const/16 v2, 0x7d0 */
/* if-ne v0, v2, :cond_0 */
/* .line 788 */
} // :cond_0
/* new-instance v2, Ljava/lang/SecurityException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Disallowed call for uid "; // const-string v4, "Disallowed call for uid "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 790 */
} // :cond_1
} // :goto_0
return;
/* .line 783 */
} // .end local v0 # "uid":I
} // .end local v1 # "appid":I
} // :cond_2
/* new-instance v0, Ljava/lang/IllegalStateException; */
final String v1 = "Current devices doesn\'t support ColorfulLed!"; // const-string v1, "Current devices doesn\'t support ColorfulLed!"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 781 */
} // :cond_3
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "callingPackage is invalid!"; // const-string v1, "callingPackage is invalid!"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private void doCancelColorfulLightLocked ( ) {
/* .locals 8 */
/* .line 641 */
v0 = this.mThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 642 */
(( com.android.server.lights.MiuiLightsService$LightsThread ) v0 ).cancel ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->cancel()V
/* .line 643 */
int v0 = 0; // const/4 v0, 0x0
this.mThread = v0;
/* .line 644 */
v0 = this.mColorfulLight;
int v1 = -1; // const/4 v1, -0x1
/* iput v1, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
/* .line 645 */
v2 = this.mColorfulLight;
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* invoke-static/range {v2 ..v7}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 647 */
} // :cond_0
return;
} // .end method
private com.android.server.lights.VisualizerHolder$OnDataCaptureListener getDataCaptureListener ( ) {
/* .locals 1 */
/* .line 711 */
v0 = this.mDataCaptureListener;
/* if-nez v0, :cond_0 */
/* .line 712 */
/* new-instance v0, Lcom/android/server/lights/MiuiLightsService$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/lights/MiuiLightsService$3;-><init>(Lcom/android/server/lights/MiuiLightsService;)V */
this.mDataCaptureListener = v0;
/* .line 730 */
} // :cond_0
v0 = this.mDataCaptureListener;
} // .end method
public static com.android.server.lights.MiuiLightsService getInstance ( ) {
/* .locals 1 */
/* .line 489 */
v0 = com.android.server.lights.MiuiLightsService.sInstance;
} // .end method
private Boolean isDisableButtonLight ( ) {
/* .locals 5 */
/* .line 1025 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z */
final String v1 = "screen_buttons_state"; // const-string v1, "screen_buttons_state"
int v2 = 1; // const/4 v2, 0x1
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1026 */
v0 = this.mResolver;
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v4,v3 );
/* if-nez v0, :cond_1 */
v0 = this.mResolver;
/* .line 1028 */
/* const-string/jumbo v1, "single_key_use_enable" */
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v4,v3 );
/* if-ne v0, v2, :cond_0 */
} // :cond_0
/* move v2, v4 */
} // :cond_1
} // :goto_0
/* nop */
/* .line 1026 */
} // :goto_1
/* .line 1032 */
} // :cond_2
v0 = this.mResolver;
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v4,v3 );
if ( v0 != null) { // if-eqz v0, :cond_3
} // :cond_3
/* move v2, v4 */
} // :goto_2
} // .end method
private Boolean isLightEnable ( ) {
/* .locals 1 */
/* .line 991 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isMusicLightPlaying ( ) {
/* .locals 2 */
/* .line 753 */
/* iget v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isSceneUncomfort ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "mId" # I */
/* .line 981 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* if-eq p1, v0, :cond_0 */
v0 = this.mContext;
/* .line 982 */
v0 = android.provider.MiuiSettings$SilenceMode .getZenMode ( v0 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 984 */
final String v0 = "LightsService"; // const-string v0, "LightsService"
final String v2 = "Scene is uncomfort , lights skip!"; // const-string v2, "Scene is uncomfort , lights skip!"
android.util.Slog .i ( v0,v2 );
/* .line 985 */
/* .line 987 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isTurnOnBatteryLight ( ) {
/* .locals 4 */
/* .line 1010 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "battery_light_turn_on"; // const-string v2, "battery_light_turn_on"
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
/* if-ne v0, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
} // .end method
private Boolean isTurnOnButtonLight ( ) {
/* .locals 4 */
/* .line 1005 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "screen_buttons_turn_on"; // const-string v2, "screen_buttons_turn_on"
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
/* if-ne v0, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
} // .end method
private Boolean isTurnOnLight ( ) {
/* .locals 4 */
/* .line 995 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "light_turn_on"; // const-string v2, "light_turn_on"
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
/* if-ne v0, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
} // .end method
private Boolean isTurnOnMusicLight ( ) {
/* .locals 4 */
/* .line 1020 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z */
/* if-nez v0, :cond_0 */
v0 = this.mResolver;
final String v1 = "music_light_turn_on"; // const-string v1, "music_light_turn_on"
int v2 = -2; // const/4 v2, -0x2
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v3,v2 );
/* if-ne v0, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
} // .end method
private Boolean isTurnOnNotificationLight ( ) {
/* .locals 4 */
/* .line 1015 */
v0 = this.mResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "notification_light_turn_on"; // const-string v2, "notification_light_turn_on"
int v3 = 1; // const/4 v3, 0x1
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
/* if-ne v0, v3, :cond_0 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
} // .end method
private void loadSupportLights ( ) {
/* .locals 2 */
/* .line 152 */
/* const-string/jumbo v0, "support_led_light" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z */
/* .line 153 */
/* const-string/jumbo v0, "support_button_light" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z */
/* .line 154 */
/* const-string/jumbo v0, "support_tap_fingerprint_sensor_to_home" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z */
/* .line 155 */
/* const-string/jumbo v0, "support_led_colorful" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
/* .line 156 */
/* const-string/jumbo v0, "support_led_colorful_game" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z */
/* .line 157 */
/* const-string/jumbo v0, "support_led_schedule" */
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedSchedule:Z */
/* .line 158 */
return;
} // .end method
private void populateAvailableLightsforMiui ( ) {
/* .locals 7 */
/* .line 124 */
v0 = this.mLightsById;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 125 */
v1 = this.mLightsById;
v1 = (( android.util.SparseArray ) v1 ).keyAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 126 */
/* .local v1, "type":I */
/* if-ltz v1, :cond_0 */
v2 = this.mLightsByType;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.mLightsByType;
/* array-length v2, v2 */
/* if-ge v1, v2, :cond_0 */
/* .line 127 */
v2 = this.mLightsByType;
/* new-instance v3, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v4 = this.mContext;
v5 = this.mLightsById;
(( android.util.SparseArray ) v5 ).valueAt ( v0 ); // invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/lights/LightsService$LightImpl; */
v5 = this.mHwLight;
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v3, p0, v4, v5, v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;Lcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V */
/* aput-object v3, v2, v1 */
/* .line 124 */
} // .end local v1 # "type":I
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 130 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private void recoveryBatteryLight ( ) {
/* .locals 7 */
/* .line 768 */
v0 = this.mColorfulLight;
/* iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
/* .line 769 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "skip light bat , cur light id :" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mColorfulLight;
/* iget v1, v1, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LightsService"; // const-string v1, "LightsService"
android.util.Slog .i ( v1,v0 );
/* .line 770 */
return;
/* .line 772 */
} // :cond_0
v0 = this.mLightsByType;
int v1 = 3; // const/4 v1, 0x3
/* aget-object v0, v0, v1 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* .line 773 */
/* .local v0, "batteryLight":Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v1 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmColor ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmDisabled ( v0 );
/* if-nez v1, :cond_1 */
v1 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmId ( v0 );
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/lights/MiuiLightsService;->isSceneUncomfort(I)Z */
/* if-nez v1, :cond_1 */
v1 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isLightEnable()Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 774 */
v2 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmColor ( v0 );
v3 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmMode ( v0 );
v4 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmOnMS ( v0 );
v5 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmOffMS ( v0 );
v6 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmBrightnessMode ( v0 );
/* move-object v1, v0 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 777 */
} // :cond_1
return;
} // .end method
private void registerAudioPlaybackCallback ( ) {
/* .locals 3 */
/* .line 734 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnMusicLight()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 735 */
v0 = this.mAudioManager;
v1 = this.mAudioManagerPlaybackCb;
v2 = this.mLightHandler;
(( android.media.AudioManager ) v0 ).registerAudioPlaybackCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->registerAudioPlaybackCallback(Landroid/media/AudioManager$AudioPlaybackCallback;Landroid/os/Handler;)V
/* .line 737 */
} // :cond_0
return;
} // .end method
private void releaseVisualizer ( ) {
/* .locals 1 */
/* .line 746 */
com.android.server.lights.VisualizerHolder .getInstance ( );
(( com.android.server.lights.VisualizerHolder ) v0 ).release ( ); // invoke-virtual {v0}, Lcom/android/server/lights/VisualizerHolder;->release()V
/* .line 747 */
v0 = this.mMusicLight;
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).turnOff ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->turnOff()V
/* .line 748 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->recoveryBatteryLight()V */
/* .line 749 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/lights/MiuiLightsService;->mPlayingPid:I */
/* .line 750 */
return;
} // .end method
private void reportLedEventLocked ( Integer p0, Boolean p1, Integer p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "mId" # I */
/* .param p2, "isTurnOn" # Z */
/* .param p3, "onMS" # I */
/* .param p4, "offMs" # I */
/* .line 451 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 453 */
/* .local v0, "info":Lorg/json/JSONObject; */
try { // :try_start_0
/* const-string/jumbo v1, "type" */
java.lang.String .valueOf ( p1 );
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 454 */
final String v1 = "isTurnOn"; // const-string v1, "isTurnOn"
int v2 = 0; // const/4 v2, 0x0
if ( p2 != null) { // if-eqz p2, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* move v3, v2 */
} // :goto_0
java.lang.String .valueOf ( v3 );
(( org.json.JSONObject ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 455 */
final String v1 = "onMs"; // const-string v1, "onMs"
java.lang.String .valueOf ( p3 );
(( org.json.JSONObject ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 456 */
final String v1 = "offMs"; // const-string v1, "offMs"
java.lang.String .valueOf ( p4 );
(( org.json.JSONObject ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 457 */
/* const-string/jumbo v1, "time" */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
java.lang.String .valueOf ( v3,v4 );
(( org.json.JSONObject ) v0 ).put ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 458 */
v1 = this.mLedEvents;
/* if-nez v1, :cond_1 */
/* .line 459 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mLedEvents = v1;
/* .line 461 */
} // :cond_1
v1 = this.mLedEvents;
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 462 */
v1 = v1 = this.mLedEvents;
/* const/16 v3, 0x1e */
/* if-lt v1, v3, :cond_2 */
/* .line 463 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
final String v3 = "led"; // const-string v3, "led"
v4 = this.mLedEvents;
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportEvents ( v3, v4, v2 ); // invoke-virtual {v1, v3, v4, v2}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportEvents(Ljava/lang/String;Ljava/util/List;Z)V
/* .line 464 */
v1 = this.mLedEvents;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 469 */
} // :cond_2
/* .line 467 */
/* :catch_0 */
/* move-exception v1 */
/* .line 468 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 470 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void turnoffBatteryLight ( ) {
/* .locals 7 */
/* .line 760 */
v0 = this.mLightsByType;
int v1 = 3; // const/4 v1, 0x3
/* aget-object v0, v0, v1 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* .line 761 */
/* .local v0, "batteryLight":Lcom/android/server/lights/MiuiLightsService$LightImpl; */
v1 = com.android.server.lights.MiuiLightsService$LightImpl .-$$Nest$fgetmColor ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 762 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object v1, v0 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V */
/* .line 764 */
} // :cond_0
return;
} // .end method
private void unregisterAudioPlaybackCallback ( ) {
/* .locals 2 */
/* .line 740 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAudioManagerPlaybackCb;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 741 */
v1 = this.mAudioManager;
(( android.media.AudioManager ) v1 ).unregisterAudioPlaybackCallback ( v0 ); // invoke-virtual {v1, v0}, Landroid/media/AudioManager;->unregisterAudioPlaybackCallback(Landroid/media/AudioManager$AudioPlaybackCallback;)V
/* .line 743 */
} // :cond_0
return;
} // .end method
private void updateLightDefaultState ( java.lang.String p0, java.lang.String p1, java.lang.Boolean p2 ) {
/* .locals 3 */
/* .param p1, "defKey" # Ljava/lang/String; */
/* .param p2, "settingKey" # Ljava/lang/String; */
/* .param p3, "defValue" # Ljava/lang/Boolean; */
/* .line 195 */
v0 = (( java.lang.Boolean ) p3 ).booleanValue ( ); // invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z
v0 = miui.util.FeatureParser .getBoolean ( p1,v0 );
/* .line 196 */
/* .local v0, "defaultValue":I */
v1 = this.mResolver;
int v2 = -2; // const/4 v2, -0x2
v1 = android.provider.Settings$Secure .getIntForUser ( v1,p2,v0,v2 );
/* .line 198 */
/* .local v1, "currentValue":I */
/* if-ne v1, v0, :cond_0 */
/* .line 199 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Secure .putInt ( v2,p2,v0 );
/* .line 201 */
} // :cond_0
return;
} // .end method
private void updateLightEnableAndType ( ) {
/* .locals 6 */
/* .line 161 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z */
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 162 */
v0 = this.mResolver;
final String v3 = "screen_buttons_state"; // const-string v3, "screen_buttons_state"
android.provider.Settings$Secure .putIntForUser ( v0,v3,v1,v2 );
/* .line 164 */
v0 = this.mLightsByType;
int v3 = 2; // const/4 v3, 0x2
/* aget-object v0, v0, v3 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 166 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 167 */
/* nop */
/* .line 168 */
int v0 = 1; // const/4 v0, 0x1
java.lang.Boolean .valueOf ( v0 );
/* .line 167 */
final String v4 = "default_notification_led_on"; // const-string v4, "default_notification_led_on"
final String v5 = "notification_light_turn_on"; // const-string v5, "notification_light_turn_on"
/* invoke-direct {p0, v4, v5, v3}, Lcom/android/server/lights/MiuiLightsService;->updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V */
/* .line 169 */
/* nop */
/* .line 170 */
java.lang.Boolean .valueOf ( v0 );
/* .line 169 */
final String v3 = "default_battery_led_on"; // const-string v3, "default_battery_led_on"
final String v4 = "battery_light_turn_on"; // const-string v4, "battery_light_turn_on"
/* invoke-direct {p0, v3, v4, v0}, Lcom/android/server/lights/MiuiLightsService;->updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V */
/* .line 171 */
v0 = this.mLightsByType;
int v3 = 4; // const/4 v3, 0x4
/* aget-object v0, v0, v3 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 172 */
v0 = this.mLightsByType;
int v3 = 3; // const/4 v3, 0x3
/* aget-object v0, v0, v3 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 174 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedSchedule:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 175 */
/* nop */
/* .line 176 */
java.lang.Boolean .valueOf ( v1 );
/* .line 175 */
final String v1 = "default_schedule_led_on"; // const-string v1, "default_schedule_led_on"
final String v3 = "light_turn_on_Time"; // const-string v3, "light_turn_on_Time"
/* invoke-direct {p0, v1, v3, v0}, Lcom/android/server/lights/MiuiLightsService;->updateLightDefaultState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V */
/* .line 177 */
v0 = this.mResolver;
final String v1 = "light_turn_on_endTime"; // const-string v1, "light_turn_on_endTime"
/* const-wide/32 v3, 0x4ef6d80 */
android.provider.Settings$Secure .getLongForUser ( v0,v1,v3,v4,v2 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J */
/* .line 179 */
v0 = this.mResolver;
final String v1 = "light_turn_on_startTime"; // const-string v1, "light_turn_on_startTime"
/* const-wide/32 v3, 0x1808580 */
android.provider.Settings$Secure .getLongForUser ( v0,v1,v3,v4,v2 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J */
/* .line 181 */
v0 = (( com.android.server.lights.MiuiLightsService ) p0 ).isTurnOnTimeLight ( ); // invoke-virtual {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnTimeLight()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 182 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateWorkState()V */
/* .line 184 */
} // :cond_2
v0 = this.mContext;
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver;-><init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$TimeTickReceiver-IA;)V */
/* new-instance v3, Landroid/content/IntentFilter; */
final String v4 = "android.intent.action.TIME_TICK"; // const-string v4, "android.intent.action.TIME_TICK"
/* invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
v4 = this.mLightHandler;
(( android.content.Context ) v0 ).registerReceiver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 187 */
} // :cond_3
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 188 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 189 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
this.mPackageManagerInt = v0;
/* .line 190 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->registerAudioPlaybackCallback()V */
/* .line 192 */
} // :cond_4
return;
} // .end method
private void updateLightState ( ) {
/* .locals 3 */
/* .line 973 */
v0 = this.mColorfulLight;
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 974 */
v0 = this.mLightsByType;
int v1 = 3; // const/4 v1, 0x3
/* aget-object v0, v0, v1 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* .line 975 */
/* .local v0, "light":Lcom/android/server/lights/MiuiLightsService$LightImpl; */
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 976 */
v1 = this.mLightsByType;
int v2 = 4; // const/4 v2, 0x4
/* aget-object v1, v1, v2 */
/* move-object v0, v1 */
/* check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl; */
/* .line 977 */
(( com.android.server.lights.MiuiLightsService$LightImpl ) v0 ).updateLight ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateLight()V
/* .line 978 */
return;
} // .end method
private void updateWorkState ( ) {
/* .locals 9 */
/* .line 818 */
/* iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedSchedule:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 819 */
} // :cond_0
java.util.Calendar .getInstance ( );
/* .line 820 */
/* .local v0, "cal":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* int-to-long v1, v1 */
/* const-wide/32 v3, 0x36ee80 */
/* mul-long/2addr v1, v3 */
/* const/16 v3, 0xc */
v3 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
/* int-to-long v3, v3 */
/* const-wide/32 v5, 0xea60 */
/* mul-long/2addr v3, v5 */
/* add-long/2addr v1, v3 */
/* .line 821 */
/* .local v1, "now_stamp":J */
/* iget-wide v3, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J */
/* iget-wide v5, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J */
/* cmp-long v7, v3, v5 */
int v8 = 0; // const/4 v8, 0x0
/* if-ltz v7, :cond_2 */
/* .line 822 */
/* cmp-long v3, v1, v3 */
/* if-gtz v3, :cond_1 */
/* cmp-long v3, v1, v5 */
/* if-gez v3, :cond_3 */
/* .line 823 */
} // :cond_1
/* iput-boolean v8, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
/* .line 824 */
return;
/* .line 827 */
} // :cond_2
/* cmp-long v3, v1, v3 */
/* if-lez v3, :cond_3 */
/* cmp-long v3, v1, v5 */
/* if-gez v3, :cond_3 */
/* .line 828 */
/* iput-boolean v8, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
/* .line 829 */
return;
/* .line 832 */
} // :cond_3
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
/* .line 833 */
return;
} // .end method
/* # virtual methods */
public void dumpLight ( java.io.PrintWriter p0, com.android.server.notification.NotificationManagerService$DumpFilter p1 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "filter" # Lcom/android/server/notification/NotificationManagerService$DumpFilter; */
/* .line 1052 */
final String v0 = "MiuiLightsService Status:"; // const-string v0, "MiuiLightsService Status:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1053 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1054 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " ZenMode:"; // const-string v2, " ZenMode:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mContext;
v2 = android.provider.MiuiSettings$SilenceMode .getZenMode ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1055 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mSupportColorFulLight:"; // const-string v2, " mSupportColorFulLight:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorfulLed:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1056 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mSupportGameColorFulLight:"; // const-string v2, " mSupportGameColorFulLight:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportColorGameLed:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1057 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " Led Working Time: state "; // const-string v2, " Led Working Time: state "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mIsWorkTime:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " start:"; // const-string v2, " start:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/lights/MiuiLightsService;->light_start_time:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " end:"; // const-string v2, " end:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/lights/MiuiLightsService;->light_end_time:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1058 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mSupportTapFingerprint:"; // const-string v2, " mSupportTapFingerprint:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportTapFingerprint:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1059 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mSupportButtonLight:"; // const-string v2, " mSupportButtonLight:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportButtonLight:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1060 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mSupportLedLight:"; // const-string v2, " mSupportLedLight:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mSupportLedLight:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1061 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mIsLedTurnOn:"; // const-string v2, " mIsLedTurnOn:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService;->mIsLedTurnOn:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1062 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isLightEnable: "; // const-string v2, " isLightEnable: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isLightEnable()Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1063 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isTurnOnLight: "; // const-string v2, " isTurnOnLight: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnLight()Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1064 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isTurnOnButtonLight: "; // const-string v2, " isTurnOnButtonLight: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnButtonLight()Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1065 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isTurnOnBatteryLight: "; // const-string v2, " isTurnOnBatteryLight: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnBatteryLight()Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1066 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isTurnOnNotificationLight: "; // const-string v2, " isTurnOnNotificationLight: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnNotificationLight()Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1067 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " isTurnOnMusicLight: "; // const-string v2, " isTurnOnMusicLight: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = /* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->isTurnOnMusicLight()Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1068 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.mLightsByType;
/* array-length v2, v2 */
/* if-ge v1, v2, :cond_1 */
/* .line 1069 */
v2 = this.mLightsByType;
/* aget-object v2, v2, v1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1070 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLightsByType;
/* aget-object v3, v3, v1 */
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1068 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1073 */
} // .end local v1 # "i":I
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mColorfulLight;
(( com.android.server.lights.MiuiLightsService$LightImpl ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1074 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mMusicLight;
(( com.android.server.lights.MiuiLightsService$LightImpl ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1075 */
final String v1 = " Previous Lights:"; // const-string v1, " Previous Lights:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1076 */
v1 = this.mPreviousLights;
(( java.util.LinkedList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/lights/LightState; */
/* .line 1077 */
/* .local v2, "lightstate":Lcom/android/server/lights/LightState; */
final String v3 = " "; // const-string v3, " "
(( java.io.PrintWriter ) p1 ).print ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1078 */
(( com.android.server.lights.LightState ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/lights/LightState;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1079 */
} // .end local v2 # "lightstate":Lcom/android/server/lights/LightState;
/* .line 1080 */
} // :cond_2
/* monitor-exit v0 */
/* .line 1081 */
return;
/* .line 1080 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isTurnOnTimeLight ( ) {
/* .locals 4 */
/* .line 1000 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "light_turn_on_Time"; // const-string v2, "light_turn_on_Time"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "phase" # I */
/* .line 140 */
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_0 */
/* .line 141 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->loadSupportLights()V */
/* .line 142 */
/* invoke-direct {p0}, Lcom/android/server/lights/MiuiLightsService;->updateLightEnableAndType()V */
/* .line 143 */
/* new-instance v0, Lcom/android/server/lights/MiuiLightsService$LightContentObserver; */
/* invoke-direct {v0, p0}, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;-><init>(Lcom/android/server/lights/MiuiLightsService;)V */
this.mLightContentObserver = v0;
/* .line 144 */
(( com.android.server.lights.MiuiLightsService$LightContentObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightContentObserver;->observe()V
/* .line 145 */
/* const-class v0, Landroid/os/BatteryManagerInternal; */
(( com.android.server.lights.MiuiLightsService ) p0 ).getLocalService ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/lights/MiuiLightsService;->getLocalService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/BatteryManagerInternal; */
this.mBatteryManagerInternal = v0;
/* .line 146 */
v0 = this.mContext;
/* new-instance v1, Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver;-><init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$UserSwitchReceiver-IA;)V */
/* new-instance v3, Landroid/content/IntentFilter; */
final String v4 = "android.intent.action.USER_SWITCHED"; // const-string v4, "android.intent.action.USER_SWITCHED"
/* invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
v4 = this.mLightHandler;
(( android.content.Context ) v0 ).registerReceiver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 149 */
} // :cond_0
return;
} // .end method
public void onStart ( ) {
/* .locals 0 */
/* .line 134 */
/* invoke-super {p0}, Lcom/android/server/lights/LightsService;->onStart()V */
/* .line 135 */
/* .line 136 */
return;
} // .end method
