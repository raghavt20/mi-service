.class public Lcom/android/server/lights/MiuiLightsService$LightImpl;
.super Lcom/android/server/lights/LightsService$LightImpl;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LightImpl"
.end annotation


# instance fields
.field private lightStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/lights/LightState;",
            ">;"
        }
    .end annotation
.end field

.field private mBrightnessMode:I

.field private mColor:I

.field private mDisabled:Z

.field private mId:I

.field private mIsShutDown:Z

.field private mLastColor:I

.field public mLastLightStyle:I

.field private mMode:I

.field private mOffMS:I

.field private mOnMS:I

.field private mUid:I

.field private pkg_name:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBrightnessMode(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmColor(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisabled(Lcom/android/server/lights/MiuiLightsService$LightImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmId(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMode(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOffMS(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOnMS(Lcom/android/server/lights/MiuiLightsService$LightImpl;)I
    .locals 0

    iget p0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I

    return p0
.end method

.method static bridge synthetic -$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetColorCommonLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setColorCommonLocked(IIIII)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;I)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/lights/MiuiLightsService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mFakeId"    # I

    .line 225
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    .line 226
    iget-object v0, p1, Lcom/android/server/lights/MiuiLightsService;->mLightsById:Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/lights/LightsService$LightImpl;

    iget-object v0, v0, Lcom/android/server/lights/LightsService$LightImpl;->mHwLight:Landroid/hardware/light/HwLight;

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/lights/LightsService$LightImpl;-><init>(Lcom/android/server/lights/LightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V

    .line 214
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z

    .line 227
    iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    .line 228
    const/4 v1, 0x2

    if-eq p3, v1, :cond_0

    const/4 v1, 0x3

    if-ne p3, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    .line 230
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;ILcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;I)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/server/lights/MiuiLightsService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "hwLight"    # Landroid/hardware/light/HwLight;

    .line 218
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    .line 219
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/LightsService$LightImpl;-><init>(Lcom/android/server/lights/LightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V

    .line 214
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z

    .line 220
    iget v1, p3, Landroid/hardware/light/HwLight;->id:I

    iput v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    .line 221
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    .line 223
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;Lcom/android/server/lights/MiuiLightsService$LightImpl-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;-><init>(Lcom/android/server/lights/MiuiLightsService;Landroid/content/Context;Landroid/hardware/light/HwLight;)V

    return-void
.end method

.method private realSetLightLocked(IIIII)V
    .locals 9
    .param p1, "color"    # I
    .param p2, "mode"    # I
    .param p3, "onMS"    # I
    .param p4, "offMS"    # I
    .param p5, "brightnessMode"    # I

    .line 374
    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z

    if-eqz v0, :cond_0

    .line 375
    const/4 p1, 0x0

    .line 378
    :cond_0
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    goto/16 :goto_1

    .line 383
    :cond_1
    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    :cond_2
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastColor:I

    if-eq v0, p1, :cond_5

    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "realSetLightLocked #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": color=#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": onMS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " offMS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LightsService"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v7, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    new-instance v8, Lcom/android/server/lights/LightState;

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    move-object v0, v8

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/server/lights/LightState;-><init>(IIIIII)V

    invoke-static {v7, v8}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$maddToLightCollectionLocked(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/LightState;)V

    .line 388
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLedEvents(Lcom/android/server/lights/MiuiLightsService;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    .line 389
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputmLedEvents(Lcom/android/server/lights/MiuiLightsService;Ljava/util/List;)V

    .line 391
    :cond_3
    if-nez p1, :cond_4

    .line 392
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmIsLedTurnOn(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 393
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputmIsLedTurnOn(Lcom/android/server/lights/MiuiLightsService;Z)V

    .line 394
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    invoke-static {v0, v2, v1, p3, p4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreportLedEventLocked(Lcom/android/server/lights/MiuiLightsService;IZII)V

    goto :goto_0

    .line 397
    :cond_4
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmIsLedTurnOn(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 398
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputmIsLedTurnOn(Lcom/android/server/lights/MiuiLightsService;Z)V

    .line 399
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    invoke-static {v0, v2, v1, p3, p4}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreportLedEventLocked(Lcom/android/server/lights/MiuiLightsService;IZII)V

    .line 403
    :cond_5
    :goto_0
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-super/range {v0 .. v5}, Lcom/android/server/lights/LightsService$LightImpl;->setLightLocked(IIIII)V

    .line 404
    iput p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastColor:I

    .line 405
    return-void

    .line 379
    :cond_6
    :goto_1
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-super/range {v0 .. v5}, Lcom/android/server/lights/LightsService$LightImpl;->setLightLocked(IIIII)V

    .line 380
    return-void
.end method

.method private setColorCommonLocked(IIIII)V
    .locals 0
    .param p1, "color"    # I
    .param p2, "mode"    # I
    .param p3, "onMS"    # I
    .param p4, "offMS"    # I
    .param p5, "brightnessMode"    # I

    .line 412
    invoke-super/range {p0 .. p5}, Lcom/android/server/lights/LightsService$LightImpl;->setLightLocked(IIIII)V

    .line 413
    return-void
.end method

.method private updateState(IIIII)V
    .locals 0
    .param p1, "color"    # I
    .param p2, "mode"    # I
    .param p3, "onMS"    # I
    .param p4, "offMS"    # I
    .param p5, "brightnessMode"    # I

    .line 416
    iput p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    .line 417
    iput p2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I

    .line 418
    iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I

    .line 419
    iput p4, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I

    .line 420
    iput p5, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I

    .line 421
    return-void
.end method

.method private updateState(Ljava/lang/String;IILjava/util/List;)V
    .locals 0
    .param p1, "pkg_name"    # Ljava/lang/String;
    .param p2, "mUid"    # I
    .param p3, "styleType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Lcom/android/server/lights/LightState;",
            ">;)V"
        }
    .end annotation

    .line 424
    .local p4, "lightStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;"
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->pkg_name:Ljava/lang/String;

    .line 425
    iput p2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mUid:I

    .line 426
    iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    .line 427
    iput-object p4, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->lightStates:Ljava/util/List;

    .line 428
    return-void
.end method


# virtual methods
.method public bridge synthetic pulse()V
    .locals 0

    .line 203
    invoke-super {p0}, Lcom/android/server/lights/LightsService$LightImpl;->pulse()V

    return-void
.end method

.method public bridge synthetic pulse(II)V
    .locals 0

    .line 203
    invoke-super {p0, p1, p2}, Lcom/android/server/lights/LightsService$LightImpl;->pulse(II)V

    return-void
.end method

.method public bridge synthetic setBrightness(F)V
    .locals 0

    .line 203
    invoke-super {p0, p1}, Lcom/android/server/lights/LightsService$LightImpl;->setBrightness(F)V

    return-void
.end method

.method public bridge synthetic setBrightness(FI)V
    .locals 0

    .line 203
    invoke-super {p0, p1, p2}, Lcom/android/server/lights/LightsService$LightImpl;->setBrightness(FI)V

    return-void
.end method

.method public setBrightness(IZ)V
    .locals 1
    .param p1, "brightness"    # I
    .param p2, "isShutDown"    # Z

    .line 368
    iput-boolean p2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z

    .line 369
    int-to-float v0, p1

    invoke-super {p0, v0}, Lcom/android/server/lights/LightsService$LightImpl;->setBrightness(F)V

    .line 370
    return-void
.end method

.method public bridge synthetic setColor(I)V
    .locals 0

    .line 203
    invoke-super {p0, p1}, Lcom/android/server/lights/LightsService$LightImpl;->setColor(I)V

    return-void
.end method

.method setColorfulLightLocked(Ljava/lang/String;IILjava/util/List;)V
    .locals 3
    .param p1, "pkg_name"    # Ljava/lang/String;
    .param p2, "mUid"    # I
    .param p3, "styleType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Lcom/android/server/lights/LightState;",
            ">;)V"
        }
    .end annotation

    .line 330
    .local p4, "lightStates":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;"
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    const/16 v1, 0x8

    const-string v2, "LightsService"

    if-ne v0, v1, :cond_6

    if-nez p4, :cond_0

    goto :goto_1

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misLightEnable(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(Ljava/lang/String;IILjava/util/List;)V

    .line 337
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mdoCancelColorfulLightLocked(Lcom/android/server/lights/MiuiLightsService;)V

    .line 338
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misSceneUncomfort(Lcom/android/server/lights/MiuiLightsService;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 343
    const-string v0, "Scene is uncomfort , lightstyle phone skip"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    return-void

    .line 348
    :cond_2
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 357
    :cond_3
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    const/4 v2, 0x0

    invoke-static {v0, p3, v1, v2, v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreportLedEventLocked(Lcom/android/server/lights/MiuiLightsService;IZII)V

    .line 359
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mdoCancelColorfulLightLocked(Lcom/android/server/lights/MiuiLightsService;)V

    .line 360
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    new-instance v1, Lcom/android/server/lights/MiuiLightsService$LightsThread;

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {v1, v2, p4, p3, p2}, Lcom/android/server/lights/MiuiLightsService$LightsThread;-><init>(Lcom/android/server/lights/MiuiLightsService;Ljava/util/List;II)V

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputmThread(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$LightsThread;)V

    .line 361
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmThread(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightsThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->start()V

    .line 362
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(Ljava/lang/String;IILjava/util/List;)V

    .line 363
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    new-instance v1, Lcom/android/server/lights/LightState;

    invoke-direct {v1, p1, p3}, Lcom/android/server/lights/LightState;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$maddToLightCollectionLocked(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/LightState;)V

    .line 364
    return-void

    .line 349
    :cond_4
    :goto_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_5

    .line 350
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(Ljava/lang/String;IILjava/util/List;)V

    .line 351
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mdoCancelColorfulLightLocked(Lcom/android/server/lights/MiuiLightsService;)V

    .line 352
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mrecoveryBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 354
    :cond_5
    return-void

    .line 331
    :cond_6
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal Argument mLastLightStyle:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lightStates:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    return-void
.end method

.method public bridge synthetic setFlashing(IIII)V
    .locals 0

    .line 203
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/server/lights/LightsService$LightImpl;->setFlashing(IIII)V

    return-void
.end method

.method setFlashing(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "colorSettingKey"    # Ljava/lang/String;
    .param p2, "freqSettingKey"    # Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmContext(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11060002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 235
    .local v0, "defaultColor":I
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmResolver(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, p1, v0, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 238
    .local v1, "color":I
    const/16 v2, 0x1f4

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v4, v2, v3}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setFlashing(IIII)V

    .line 240
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 241
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmHandler(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v3, v4, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 242
    return-void
.end method

.method setLightLocked(IIIII)V
    .locals 10
    .param p1, "color"    # I
    .param p2, "mode"    # I
    .param p3, "onMS"    # I
    .param p4, "offMS"    # I
    .param p5, "brightnessMode"    # I

    .line 265
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    if-nez v0, :cond_0

    .line 266
    invoke-direct/range {p0 .. p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V

    .line 267
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misLightEnable(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    if-eqz v0, :cond_1

    goto/16 :goto_2

    .line 277
    :cond_1
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    const/16 v1, 0x9

    const/4 v2, 0x3

    const/4 v3, 0x4

    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-ne v0, v1, :cond_6

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v0

    iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    const/4 v4, 0x1

    if-eq v0, v4, :cond_b

    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    .line 282
    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misMusicLightPlaying(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_1

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misSceneUncomfort(Lcom/android/server/lights/MiuiLightsService;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 288
    invoke-direct/range {p0 .. p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V

    .line 289
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V

    .line 290
    return-void

    .line 294
    :cond_4
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmBatteryManagerInternal(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/BatteryManagerInternal;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 295
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmBatteryManagerInternal(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/BatteryManagerInternal;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/BatteryManagerInternal;->getBatteryLevel()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_5

    .line 296
    const/4 p1, 0x0

    .line 298
    :cond_5
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmBatteryManagerInternal(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/BatteryManagerInternal;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/BatteryManagerInternal;->isPowered(I)Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz p1, :cond_6

    .line 300
    const/4 p1, 0x0

    .line 306
    :cond_6
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    if-ne v0, v3, :cond_7

    .line 307
    if-eqz p1, :cond_9

    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    if-nez v0, :cond_9

    .line 308
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mturnoffBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    goto :goto_0

    .line 310
    :cond_7
    if-ne v0, v2, :cond_9

    .line 311
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget-object v0, v0, Lcom/android/server/lights/MiuiLightsService;->mLightsByType:[Lcom/android/server/lights/LightsService$LightImpl;

    aget-object v0, v0, v3

    check-cast v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;

    iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v0

    iget v0, v0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    .line 313
    :cond_8
    move-object v4, p0

    move v5, p1

    move v6, p2

    move v7, p3

    move v8, p4

    move v9, p5

    invoke-direct/range {v4 .. v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V

    .line 314
    return-void

    .line 318
    :cond_9
    :goto_0
    move-object v4, p0

    move v5, p1

    move v6, p2

    move v7, p3

    move v8, p4

    move v9, p5

    invoke-direct/range {v4 .. v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V

    .line 319
    invoke-direct/range {v4 .. v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V

    .line 321
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    if-ne v0, v3, :cond_a

    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    if-nez v0, :cond_a

    .line 322
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mrecoveryBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 324
    :cond_a
    return-void

    .line 283
    :cond_b
    :goto_1
    return-void

    .line 271
    :cond_c
    :goto_2
    invoke-direct/range {p0 .. p5}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->updateState(IIIII)V

    .line 272
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->realSetLightLocked(IIIII)V

    .line 273
    return-void
.end method

.method public bridge synthetic setVrMode(Z)V
    .locals 0

    .line 203
    invoke-super {p0, p1}, Lcom/android/server/lights/LightsService$LightImpl;->setVrMode(Z)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LightImpl{mDisabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOnMS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOffMS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBrightnessMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pkg_name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->pkg_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mUid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLastLightStyle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsShutDown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mIsShutDown:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic turnOff()V
    .locals 0

    .line 203
    invoke-super {p0}, Lcom/android/server/lights/LightsService$LightImpl;->turnOff()V

    return-void
.end method

.method updateLight()V
    .locals 7

    .line 245
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mId:I

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne v2, v0, :cond_2

    .line 246
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misDisableButtonLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnButtonLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    goto :goto_1

    .line 247
    :cond_2
    const/4 v2, 0x3

    if-ne v2, v0, :cond_3

    .line 248
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnBatteryLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    goto :goto_1

    .line 249
    :cond_3
    const/4 v2, 0x4

    if-ne v2, v0, :cond_4

    .line 250
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnNotificationLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    goto :goto_1

    .line 251
    :cond_4
    const/16 v2, 0x9

    if-ne v2, v0, :cond_5

    .line 252
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnMusicLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mDisabled:Z

    goto :goto_1

    .line 253
    :cond_5
    const/16 v1, 0x8

    if-ne v1, v0, :cond_6

    .line 254
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 255
    :try_start_0
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->pkg_name:Ljava/lang/String;

    iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mUid:I

    iget v3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    iget-object v4, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->lightStates:Ljava/util/List;

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setColorfulLightLocked(Ljava/lang/String;IILjava/util/List;)V

    .line 256
    monitor-exit v0

    .line 257
    return-void

    .line 256
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 259
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 260
    :try_start_1
    iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mColor:I

    iget v3, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mMode:I

    iget v4, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOnMS:I

    iget v5, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mOffMS:I

    iget v6, p0, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mBrightnessMode:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->setLightLocked(IIIII)V

    .line 261
    monitor-exit v0

    .line 262
    return-void

    .line 261
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1
.end method
