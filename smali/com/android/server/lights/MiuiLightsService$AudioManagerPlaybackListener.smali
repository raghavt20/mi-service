.class final Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;
.super Landroid/media/AudioManager$AudioPlaybackCallback;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AudioManagerPlaybackListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method private constructor <init>(Lcom/android/server/lights/MiuiLightsService;)V
    .locals 0

    .line 653
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {p0}, Landroid/media/AudioManager$AudioPlaybackCallback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;-><init>(Lcom/android/server/lights/MiuiLightsService;)V

    return-void
.end method


# virtual methods
.method public onPlaybackConfigChanged(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/AudioPlaybackConfiguration;",
            ">;)V"
        }
    .end annotation

    .line 656
    .local p1, "configs":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;"
    invoke-super {p0, p1}, Landroid/media/AudioManager$AudioPlaybackCallback;->onPlaybackConfigChanged(Ljava/util/List;)V

    .line 657
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misTurnOnMusicLight(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$misLightEnable(Lcom/android/server/lights/MiuiLightsService;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 658
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManagerGlobal;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto/16 :goto_3

    .line 663
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioPlaybackConfiguration;

    .line 664
    .local v2, "config":Landroid/media/AudioPlaybackConfiguration;
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v1, :cond_3

    .line 665
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getClientPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v3

    .line 666
    .local v3, "processNameByPid":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/lights/VisualizerHolder;->getInstance()Lcom/android/server/lights/VisualizerHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/lights/VisualizerHolder;->isAllowed(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 667
    return-void

    .line 669
    :cond_1
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getClientPid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fputmPlayingPid(Lcom/android/server/lights/MiuiLightsService;I)V

    .line 670
    sget-boolean v0, Lcom/android/server/lights/LightsService;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 671
    const-string v0, "LightsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Start Playing process_name:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " mPlayingPid:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v5}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :cond_2
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mturnoffBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 673
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 674
    :try_start_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    new-instance v1, Lcom/android/server/lights/LightState;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPackageManagerInt(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/pm/PackageManagerInternal;

    move-result-object v6

    .line 675
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManagerInternal;->getNameForUid(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6, v4}, Lcom/android/server/lights/LightState;-><init>(Ljava/lang/String;I)V

    .line 674
    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$maddToLightCollectionLocked(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/LightState;)V

    .line 677
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 678
    invoke-static {}, Lcom/android/server/lights/VisualizerHolder;->getInstance()Lcom/android/server/lights/VisualizerHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mgetDataCaptureListener(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V

    .line 679
    goto :goto_1

    .line 677
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 681
    .end local v3    # "processNameByPid":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getClientPid()I

    move-result v3

    iget-object v5, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v5}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v5

    if-ne v3, v5, :cond_6

    .line 682
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I

    move-result v3

    if-eq v3, v4, :cond_4

    .line 683
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I

    move-result v3

    const/4 v5, 0x4

    if-eq v3, v5, :cond_4

    .line 684
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I

    move-result v3

    if-nez v3, :cond_6

    .line 685
    :cond_4
    sget-boolean v0, Lcom/android/server/lights/LightsService;->DEBUG:Z

    if-eqz v0, :cond_5

    const-string v0, "LightsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stop Playing pid:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    :cond_5
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmLock(Lcom/android/server/lights/MiuiLightsService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 687
    :try_start_2
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    new-instance v1, Lcom/android/server/lights/LightState;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPackageManagerInt(Lcom/android/server/lights/MiuiLightsService;)Landroid/content/pm/PackageManagerInternal;

    move-result-object v5

    .line 688
    invoke-virtual {v2}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManagerInternal;->getNameForUid(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5, v4}, Lcom/android/server/lights/LightState;-><init>(Ljava/lang/String;I)V

    .line 687
    invoke-static {v0, v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$maddToLightCollectionLocked(Lcom/android/server/lights/MiuiLightsService;Lcom/android/server/lights/LightState;)V

    .line 690
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 691
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V

    .line 692
    goto :goto_1

    .line 690
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 694
    .end local v2    # "config":Landroid/media/AudioPlaybackConfiguration;
    :cond_6
    goto/16 :goto_0

    .line 698
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    .line 699
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioPlaybackConfiguration;

    .line 700
    .local v1, "config":Landroid/media/AudioPlaybackConfiguration;
    invoke-virtual {v1}, Landroid/media/AudioPlaybackConfiguration;->getClientPid()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v3

    if-ne v2, v3, :cond_8

    .line 701
    return-void

    .line 703
    .end local v1    # "config":Landroid/media/AudioPlaybackConfiguration;
    :cond_8
    goto :goto_2

    .line 704
    :cond_9
    sget-boolean v0, Lcom/android/server/lights/LightsService;->DEBUG:Z

    if-eqz v0, :cond_a

    const-string v0, "LightsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not found player: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmPlayingPid(Lcom/android/server/lights/MiuiLightsService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "in callback!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :cond_a
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V

    .line 707
    :cond_b
    return-void

    .line 660
    :cond_c
    :goto_3
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$AudioManagerPlaybackListener;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreleaseVisualizer(Lcom/android/server/lights/MiuiLightsService;)V

    .line 661
    return-void
.end method
