public class com.android.server.lights.VisualizerHolder {
	 /* .source "VisualizerHolder.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String mMusicWhiteList;
private static final com.android.server.lights.VisualizerHolder ourInstance;
/* # instance fields */
Integer lastMax;
com.android.server.lights.VisualizerHolder$OnDataCaptureListener onDataCaptureListener;
private android.media.audiofx.Visualizer visualizer;
/* # direct methods */
static com.android.server.lights.VisualizerHolder ( ) {
	 /* .locals 48 */
	 /* .line 12 */
	 /* new-instance v0, Lcom/android/server/lights/VisualizerHolder; */
	 /* invoke-direct {v0}, Lcom/android/server/lights/VisualizerHolder;-><init>()V */
	 /* .line 13 */
	 final String v1 = "com.miui.player:remote"; // const-string v1, "com.miui.player:remote"
	 final String v2 = "com.netease.cloudmusic:play"; // const-string v2, "com.netease.cloudmusic:play"
	 final String v3 = "com.tencent.qqmusic:QQPlayerService"; // const-string v3, "com.tencent.qqmusic:QQPlayerService"
	 final String v4 = "fm.xiami.main.player"; // const-string v4, "fm.xiami.main.player"
	 final String v5 = "com.kugou.android.support"; // const-string v5, "com.kugou.android.support"
	 final String v6 = "cn.kuwo.player:service"; // const-string v6, "cn.kuwo.player:service"
	 final String v7 = "com.tencent.blackkey:player"; // const-string v7, "com.tencent.blackkey:player"
	 final String v8 = "cmccwm.mobilemusic"; // const-string v8, "cmccwm.mobilemusic"
	 final String v9 = "com.ting.mp3.android"; // const-string v9, "com.ting.mp3.android"
	 final String v10 = "com.kugou.android.support.ktvapp"; // const-string v10, "com.kugou.android.support.ktvapp"
	 final String v11 = "com.blueocean.musicplayer"; // const-string v11, "com.blueocean.musicplayer"
	 final String v12 = "com.kugou.android.ringtone:player"; // const-string v12, "com.kugou.android.ringtone:player"
	 final String v13 = "com.shoujiduoduo.dj"; // const-string v13, "com.shoujiduoduo.dj"
	 final String v14 = "com.changba"; // const-string v14, "com.changba"
	 final String v15 = "com.shoujiduoduo.ringtone"; // const-string v15, "com.shoujiduoduo.ringtone"
	 final String v16 = "com.hiby.music"; // const-string v16, "com.hiby.music"
	 final String v17 = "com.miui.player:remote"; // const-string v17, "com.miui.player:remote"
	 final String v18 = "com.google.android.music:main"; // const-string v18, "com.google.android.music:main"
	 final String v19 = "com.tencent.ibg.joox"; // const-string v19, "com.tencent.ibg.joox"
	 final String v20 = "com.skysoft.kkbox.android"; // const-string v20, "com.skysoft.kkbox.android"
	 final String v21 = "com.sofeh.android.musicstudio3"; // const-string v21, "com.sofeh.android.musicstudio3"
	 final String v22 = "com.gamestar.perfectpiano"; // const-string v22, "com.gamestar.perfectpiano"
	 final String v23 = "com.opalastudios.pads"; // const-string v23, "com.opalastudios.pads"
	 final String v24 = "com.magix.android.mmjam"; // const-string v24, "com.magix.android.mmjam"
	 final String v25 = "com.musicplayer.playermusic:main"; // const-string v25, "com.musicplayer.playermusic:main"
	 final String v26 = "com.gaana"; // const-string v26, "com.gaana"
	 final String v27 = "com.maxmpz.audioplayer"; // const-string v27, "com.maxmpz.audioplayer"
	 final String v28 = "com.melodis.midomiMusicIdentifier.freemium"; // const-string v28, "com.melodis.midomiMusicIdentifier.freemium"
	 final String v29 = "com.mixvibes.remixlive"; // const-string v29, "com.mixvibes.remixlive"
	 final String v30 = "com.starmakerinteractive.starmaker"; // const-string v30, "com.starmakerinteractive.starmaker"
	 final String v31 = "com.smule.singandroid"; // const-string v31, "com.smule.singandroid"
	 final String v32 = "com.djit.apps.stream"; // const-string v32, "com.djit.apps.stream"
	 /* const-string/jumbo v33, "tunein.service" */
	 final String v34 = "com.shazam.android"; // const-string v34, "com.shazam.android"
	 final String v35 = "com.jangomobile.android"; // const-string v35, "com.jangomobile.android"
	 final String v36 = "com.pandoralite"; // const-string v36, "com.pandoralite"
	 final String v37 = "com.tube.hqmusic"; // const-string v37, "com.tube.hqmusic"
	 final String v38 = "com.amazon.avod.thirdpartyclient"; // const-string v38, "com.amazon.avod.thirdpartyclient"
	 final String v39 = "com.atmusic.app"; // const-string v39, "com.atmusic.app"
	 final String v40 = "com.rubycell.pianisthd"; // const-string v40, "com.rubycell.pianisthd"
	 final String v41 = "com.agminstruments.drumpadmachine"; // const-string v41, "com.agminstruments.drumpadmachine"
	 final String v42 = "com.playermusic.musicplayerapp"; // const-string v42, "com.playermusic.musicplayerapp"
	 final String v43 = "com.famousbluemedia.piano"; // const-string v43, "com.famousbluemedia.piano"
	 final String v44 = "com.apple.android.music"; // const-string v44, "com.apple.android.music"
	 final String v45 = "mb32r.musica.gratis.music.player.free.download"; // const-string v45, "mb32r.musica.gratis.music.player.free.download"
	 final String v46 = "com.famousbluemedia.yokee"; // const-string v46, "com.famousbluemedia.yokee"
	 final String v47 = "com.ss.android.ugc.trill"; // const-string v47, "com.ss.android.ugc.trill"
	 /* filled-new-array/range {v1 ..v47}, [Ljava/lang/String; */
	 return;
} // .end method
private com.android.server.lights.VisualizerHolder ( ) {
	 /* .locals 1 */
	 /* .line 42 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 39 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/lights/VisualizerHolder;->lastMax:I */
	 /* .line 43 */
	 return;
} // .end method
public static com.android.server.lights.VisualizerHolder getInstance ( ) {
	 /* .locals 1 */
	 /* .line 46 */
	 v0 = com.android.server.lights.VisualizerHolder.ourInstance;
} // .end method
private android.media.audiofx.Visualizer getVisualizer ( ) {
	 /* .locals 3 */
	 /* .line 50 */
	 v0 = this.visualizer;
	 /* if-nez v0, :cond_0 */
	 /* .line 52 */
	 try { // :try_start_0
		 /* new-instance v0, Landroid/media/audiofx/Visualizer; */
		 int v1 = 0; // const/4 v1, 0x0
		 /* invoke-direct {v0, v1}, Landroid/media/audiofx/Visualizer;-><init>(I)V */
		 this.visualizer = v0;
		 /* .line 53 */
		 android.media.audiofx.Visualizer .getCaptureSizeRange ( );
		 int v2 = 1; // const/4 v2, 0x1
		 /* aget v1, v1, v2 */
		 (( android.media.audiofx.Visualizer ) v0 ).setCaptureSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/audiofx/Visualizer;->setCaptureSize(I)I
		 /* :try_end_0 */
		 /* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 56 */
		 /* .line 54 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 55 */
		 /* .local v0, "e":Ljava/lang/RuntimeException; */
		 (( java.lang.RuntimeException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V
		 /* .line 58 */
	 } // .end local v0 # "e":Ljava/lang/RuntimeException;
} // :cond_0
} // :goto_0
v0 = this.visualizer;
} // .end method
/* # virtual methods */
public Boolean isAllowed ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "process_name" # Ljava/lang/String; */
/* .line 104 */
v0 = com.android.server.lights.VisualizerHolder.mMusicWhiteList;
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p1 );
} // .end method
public void release ( ) {
/* .locals 2 */
/* .line 95 */
v0 = this.visualizer;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 96 */
int v1 = 0; // const/4 v1, 0x0
(( android.media.audiofx.Visualizer ) v0 ).setEnabled ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/audiofx/Visualizer;->setEnabled(Z)I
/* .line 97 */
v0 = this.visualizer;
(( android.media.audiofx.Visualizer ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/media/audiofx/Visualizer;->release()V
/* .line 99 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.onDataCaptureListener = v0;
/* .line 100 */
this.visualizer = v0;
/* .line 101 */
return;
} // .end method
public void setOnDataCaptureListener ( com.android.server.lights.VisualizerHolder$OnDataCaptureListener p0 ) {
/* .locals 4 */
/* .param p1, "onDataCaptureListener" # Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener; */
/* .line 63 */
this.onDataCaptureListener = p1;
/* .line 64 */
/* invoke-direct {p0}, Lcom/android/server/lights/VisualizerHolder;->getVisualizer()Landroid/media/audiofx/Visualizer; */
/* if-nez v0, :cond_0 */
/* .line 65 */
return;
/* .line 67 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/lights/VisualizerHolder;->getVisualizer()Landroid/media/audiofx/Visualizer; */
/* new-instance v1, Lcom/android/server/lights/VisualizerHolder$1; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/lights/VisualizerHolder$1;-><init>(Lcom/android/server/lights/VisualizerHolder;Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V */
/* .line 90 */
v2 = android.media.audiofx.Visualizer .getMaxCaptureRate ( );
/* div-int/lit8 v2, v2, 0x2 */
/* .line 67 */
int v3 = 1; // const/4 v3, 0x1
(( android.media.audiofx.Visualizer ) v0 ).setDataCaptureListener ( v1, v2, v3, v3 ); // invoke-virtual {v0, v1, v2, v3, v3}, Landroid/media/audiofx/Visualizer;->setDataCaptureListener(Landroid/media/audiofx/Visualizer$OnDataCaptureListener;IZZ)I
/* .line 91 */
/* invoke-direct {p0}, Lcom/android/server/lights/VisualizerHolder;->getVisualizer()Landroid/media/audiofx/Visualizer; */
(( android.media.audiofx.Visualizer ) v0 ).setEnabled ( v3 ); // invoke-virtual {v0, v3}, Landroid/media/audiofx/Visualizer;->setEnabled(Z)I
/* .line 92 */
return;
} // .end method
