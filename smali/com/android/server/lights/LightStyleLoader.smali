.class public Lcom/android/server/lights/LightStyleLoader;
.super Ljava/lang/Object;
.source "LightStyleLoader.java"


# static fields
.field private static final BRIGHTNESS_MODE:Ljava/lang/String; = "brightnessMode"

.field private static final COLOR_ARGB:Ljava/lang/String; = "colorARGB"

.field private static final DEBUG:Z

.field private static final DEFAULT_DELAY:I = 0x7d0

.field private static final DEFAULT_INTERVAL:I = 0x64

.field private static final FLASH_MODE:Ljava/lang/String; = "flashMode"

.field private static final GAME_EFFECT_ONMS:I = 0x1323

.field private static final LIGHTSTATE:Ljava/lang/String; = "lightstate"

.field private static final LIGHTS_PATH_DIR:Ljava/lang/String; = "/product/etc/lights/"

.field private static final LIGHT_COLOR:I = 0x0

.field private static final LIGHT_FREQ:I = 0x1

.field private static final MAXDELAY:I = 0x493e0

.field private static final OFFMS:Ljava/lang/String; = "offMS"

.field private static final ONMS:Ljava/lang/String; = "onMS"

.field private static final TAG:Ljava/lang/String; = "LightsService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mStyleArray:Landroid/util/SparseArray;

.field private mSupportColorEffect:Z

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    sget-boolean v0, Lcom/android/server/lights/LightsService;->DEBUG:Z

    sput-boolean v0, Lcom/android/server/lights/LightStyleLoader;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->resources:Landroid/content/res/Resources;

    .line 59
    iput-object p1, p0, Lcom/android/server/lights/LightStyleLoader;->mContext:Landroid/content/Context;

    .line 60
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const-string v1, "lightstyle_default"

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 61
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v3, "lightstyle_phone"

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v3, "lightstyle_game"

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v3, "lightstyle_music"

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v3, "lightstyle_alarm"

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v3, "lightstyle_expand"

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 66
    iget-object v0, p0, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string v3, "lightstyle_luckymoney"

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 68
    const-string/jumbo v0, "support_led_colorful_effect"

    invoke-static {v0, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/lights/LightStyleLoader;->mSupportColorEffect:Z

    .line 69
    return-void
.end method

.method private getCustomLight(III)I
    .locals 7
    .param p1, "attrs"    # I
    .param p2, "original"    # I
    .param p3, "style"    # I

    .line 147
    const-string v0, "LightsService"

    iget-object v1, p0, Lcom/android/server/lights/LightStyleLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "breathing_light"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "jsonText":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    return p2

    .line 153
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 154
    .local v2, "jsonArray":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 155
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 156
    .local v4, "logObject":Lorg/json/JSONObject;
    const-string v5, "light"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    if-ne v5, p3, :cond_1

    .line 157
    packed-switch p1, :pswitch_data_0

    .line 163
    goto :goto_1

    .line 161
    :pswitch_0
    const-string v5, "onMS"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 159
    :pswitch_1
    const-string v5, "color"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 163
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "un know attrs:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .end local v4    # "logObject":Lorg/json/JSONObject;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 169
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :cond_2
    goto :goto_2

    .line 167
    :catch_0
    move-exception v2

    .line 168
    .local v2, "e":Lorg/json/JSONException;
    const-string v3, "Light jsonArray error"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_2
    return p2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getLightStyle(I)Ljava/util/List;
    .locals 28
    .param p1, "styleType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/android/server/lights/LightState;",
            ">;"
        }
    .end annotation

    .line 72
    move-object/from16 v1, p0

    move/from16 v2, p1

    const-string v3, "/product/etc/lights/"

    const-string v4, "LightsService"

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v5, v0

    .line 73
    .local v5, "ls_list":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;"
    const/4 v6, 0x0

    .line 75
    .local v6, "is":Ljava/io/InputStream;
    if-ltz v2, :cond_9

    :try_start_0
    iget-object v0, v1, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_7

    .line 78
    :cond_0
    sget-boolean v0, Lcom/android/server/lights/LightStyleLoader;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parse style: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " id: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, v1, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_1
    const/4 v0, -0x1

    .line 82
    .local v0, "colorARGB":I
    const/4 v13, 0x1

    .line 83
    .local v13, "flashMode":I
    const/16 v14, 0x64

    .line 84
    .local v14, "offMS":I
    const/16 v15, 0x64

    .line 85
    .local v15, "onMS":I
    const/16 v16, 0x0

    .line 87
    .local v16, "brightnessMode":I
    const/4 v7, 0x0

    const/4 v8, 0x1

    if-ne v2, v8, :cond_3

    .line 88
    invoke-direct {v1, v8, v15, v2}, Lcom/android/server/lights/LightStyleLoader;->getCustomLight(III)I

    move-result v9

    move v12, v9

    .line 89
    .local v12, "customOnMS":I
    if-eq v12, v15, :cond_2

    .line 90
    invoke-direct {v1, v7, v0, v2}, Lcom/android/server/lights/LightStyleLoader;->getCustomLight(III)I

    move-result v8

    .line 91
    .local v8, "customColorARGB":I
    new-instance v11, Lcom/android/server/lights/LightState;

    const/16 v17, 0x7d0

    move-object v7, v11

    move v9, v13

    move v10, v12

    move/from16 v18, v0

    move-object v0, v11

    .end local v0    # "colorARGB":I
    .local v18, "colorARGB":I
    move/from16 v11, v17

    move/from16 v17, v12

    .end local v12    # "customOnMS":I
    .local v17, "customOnMS":I
    move/from16 v12, v16

    invoke-direct/range {v7 .. v12}, Lcom/android/server/lights/LightState;-><init>(IIIII)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    nop

    .line 141
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 92
    return-object v5

    .line 89
    .end local v8    # "customColorARGB":I
    .end local v17    # "customOnMS":I
    .end local v18    # "colorARGB":I
    .restart local v0    # "colorARGB":I
    .restart local v12    # "customOnMS":I
    :cond_2
    move/from16 v18, v0

    move/from16 v17, v12

    .end local v0    # "colorARGB":I
    .end local v12    # "customOnMS":I
    .restart local v17    # "customOnMS":I
    .restart local v18    # "colorARGB":I
    goto :goto_0

    .line 87
    .end local v17    # "customOnMS":I
    .end local v18    # "colorARGB":I
    .restart local v0    # "colorARGB":I
    :cond_3
    move/from16 v18, v0

    .line 97
    .end local v0    # "colorARGB":I
    .restart local v18    # "colorARGB":I
    :goto_0
    const/4 v0, 0x2

    if-ne v2, v0, :cond_4

    :try_start_1
    iget-boolean v9, v1, Lcom/android/server/lights/LightStyleLoader;->mSupportColorEffect:Z

    if-eqz v9, :cond_4

    .line 98
    new-instance v0, Lcom/android/server/lights/LightState;

    const/4 v8, 0x1

    const/16 v10, 0x1323

    move-object v7, v0

    move v9, v13

    move v11, v14

    move/from16 v12, v16

    invoke-direct/range {v7 .. v12}, Lcom/android/server/lights/LightState;-><init>(IIIII)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    nop

    .line 141
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 99
    return-object v5

    .line 102
    :cond_4
    :try_start_2
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v9

    .line 103
    .local v9, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v9}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v10

    .line 104
    .local v10, "builder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v11, Ljava/io/BufferedInputStream;

    new-instance v12, Ljava/io/FileInputStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, v1, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    .line 105
    invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ".xml"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x1000

    invoke-direct {v11, v12, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object v6, v11

    .line 106
    invoke-virtual {v10, v6}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 107
    .local v0, "doc":Lorg/w3c/dom/Document;
    const-string v7, "lightstate"

    invoke-interface {v0, v7}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 108
    .local v7, "lightList":Lorg/w3c/dom/NodeList;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-ge v11, v12, :cond_8

    .line 109
    invoke-interface {v7, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v12}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v12

    .line 110
    .local v12, "childNodes":Lorg/w3c/dom/NodeList;
    const/16 v19, 0x0

    move/from16 v8, v19

    .local v8, "j":I
    :goto_2
    move-object/from16 v26, v0

    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .local v26, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v12}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v0

    if-ge v8, v0, :cond_7

    .line 111
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    move-object/from16 v27, v7

    const/4 v7, 0x1

    .end local v7    # "lightList":Lorg/w3c/dom/NodeList;
    .local v27, "lightList":Lorg/w3c/dom/NodeList;
    if-ne v0, v7, :cond_6

    .line 112
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v19

    sparse-switch v19, :sswitch_data_0

    :cond_5
    goto :goto_3

    :sswitch_0
    const-string v7, "brightnessMode"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    goto :goto_4

    :sswitch_1
    const-string v7, "colorARGB"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    goto :goto_4

    :sswitch_2
    const-string v7, "offMS"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    goto :goto_4

    :sswitch_3
    const-string v7, "onMS"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    goto :goto_4

    :sswitch_4
    const-string v7, "flashMode"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_4

    :goto_3
    const/4 v0, -0x1

    :goto_4
    packed-switch v0, :pswitch_data_0

    goto :goto_5

    .line 126
    :pswitch_0
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move/from16 v16, v0

    .line 127
    goto :goto_5

    .line 123
    :pswitch_1
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v15, v0

    .line 124
    goto :goto_5

    .line 120
    :pswitch_2
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v14, v0

    .line 121
    goto :goto_5

    .line 117
    :pswitch_3
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v13, v0

    .line 118
    goto :goto_5

    .line 114
    :pswitch_4
    invoke-interface {v12, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    move/from16 v18, v0

    .line 115
    nop

    .line 110
    :cond_6
    :goto_5
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, v26

    move-object/from16 v7, v27

    goto/16 :goto_2

    .end local v27    # "lightList":Lorg/w3c/dom/NodeList;
    .restart local v7    # "lightList":Lorg/w3c/dom/NodeList;
    :cond_7
    move-object/from16 v27, v7

    .line 133
    .end local v7    # "lightList":Lorg/w3c/dom/NodeList;
    .end local v8    # "j":I
    .restart local v27    # "lightList":Lorg/w3c/dom/NodeList;
    new-instance v0, Lcom/android/server/lights/LightState;

    move-object/from16 v20, v0

    move/from16 v21, v18

    move/from16 v22, v13

    move/from16 v23, v15

    move/from16 v24, v14

    move/from16 v25, v16

    invoke-direct/range {v20 .. v25}, Lcom/android/server/lights/LightState;-><init>(IIIII)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 108
    nop

    .end local v12    # "childNodes":Lorg/w3c/dom/NodeList;
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, v26

    move-object/from16 v7, v27

    const/4 v8, 0x1

    goto/16 :goto_1

    .line 135
    .end local v11    # "i":I
    .end local v26    # "doc":Lorg/w3c/dom/Document;
    .end local v27    # "lightList":Lorg/w3c/dom/NodeList;
    .restart local v0    # "doc":Lorg/w3c/dom/Document;
    .restart local v7    # "lightList":Lorg/w3c/dom/NodeList;
    :cond_8
    nop

    .line 141
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 135
    return-object v5

    .line 141
    .end local v0    # "doc":Lorg/w3c/dom/Document;
    .end local v7    # "lightList":Lorg/w3c/dom/NodeList;
    .end local v9    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v10    # "builder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v13    # "flashMode":I
    .end local v14    # "offMS":I
    .end local v15    # "onMS":I
    .end local v16    # "brightnessMode":I
    .end local v18    # "colorARGB":I
    :catchall_0
    move-exception v0

    goto :goto_6

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can\'t find xml file : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v1, Lcom/android/server/lights/LightStyleLoader;->mStyleArray:Landroid/util/SparseArray;

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".xml--Please check the path to confirm : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " -- "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 138
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 137
    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 141
    .end local v0    # "e":Ljava/lang/Exception;
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 142
    nop

    .line 143
    return-object v5

    .line 141
    :goto_6
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 142
    throw v0

    .line 76
    :cond_9
    :goto_7
    nop

    .line 141
    invoke-static {v6}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 76
    return-object v5

    :sswitch_data_0
    .sparse-switch
        -0x4464da4d -> :sswitch_4
        0x341bc5 -> :sswitch_3
        0x64c1755 -> :sswitch_2
        0x76098eaf -> :sswitch_1
        0x76464994 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
