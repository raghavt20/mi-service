.class Lcom/android/server/lights/VisualizerHolder$1;
.super Ljava/lang/Object;
.source "VisualizerHolder.java"

# interfaces
.implements Landroid/media/audiofx/Visualizer$OnDataCaptureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/lights/VisualizerHolder;->setOnDataCaptureListener(Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/lights/VisualizerHolder;

.field final synthetic val$onDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;


# direct methods
.method constructor <init>(Lcom/android/server/lights/VisualizerHolder;Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/lights/VisualizerHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 67
    iput-object p1, p0, Lcom/android/server/lights/VisualizerHolder$1;->this$0:Lcom/android/server/lights/VisualizerHolder;

    iput-object p2, p0, Lcom/android/server/lights/VisualizerHolder$1;->val$onDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFftDataCapture(Landroid/media/audiofx/Visualizer;[BI)V
    .locals 7
    .param p1, "visualizer"    # Landroid/media/audiofx/Visualizer;
    .param p2, "fft"    # [B
    .param p3, "samplingRate"    # I

    .line 75
    invoke-static {p2}, Lcom/android/internal/util/ArrayUtils;->isEmpty([B)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 76
    :cond_0
    array-length v0, p2

    div-int/lit8 v0, v0, 0x2

    new-array v0, v0, [F

    .line 77
    .local v0, "magnitudes":[F
    const/4 v1, 0x0

    .line 78
    .local v1, "max":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 79
    mul-int/lit8 v3, v2, 0x2

    aget-byte v3, p2, v3

    int-to-double v3, v3

    mul-int/lit8 v5, v2, 0x2

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p2, v5

    int-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v3

    double-to-float v3, v3

    aput v3, v0, v2

    .line 80
    aget v3, v0, v1

    aget v4, v0, v2

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 81
    move v1, v2

    .line 78
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 84
    .end local v2    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/android/server/lights/VisualizerHolder$1;->this$0:Lcom/android/server/lights/VisualizerHolder;

    iget v2, v2, Lcom/android/server/lights/VisualizerHolder;->lastMax:I

    if-ne v2, v1, :cond_3

    .line 85
    return-void

    .line 87
    :cond_3
    iget-object v2, p0, Lcom/android/server/lights/VisualizerHolder$1;->this$0:Lcom/android/server/lights/VisualizerHolder;

    iput v1, v2, Lcom/android/server/lights/VisualizerHolder;->lastMax:I

    .line 88
    iget-object v2, p0, Lcom/android/server/lights/VisualizerHolder$1;->val$onDataCaptureListener:Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;

    mul-int v3, v1, p3

    array-length v4, p2

    div-int/2addr v3, v4

    invoke-interface {v2, v3, v0}, Lcom/android/server/lights/VisualizerHolder$OnDataCaptureListener;->onFrequencyCapture(I[F)V

    .line 89
    return-void
.end method

.method public onWaveFormDataCapture(Landroid/media/audiofx/Visualizer;[BI)V
    .locals 0
    .param p1, "visualizer"    # Landroid/media/audiofx/Visualizer;
    .param p2, "waveform"    # [B
    .param p3, "samplingRate"    # I

    .line 71
    return-void
.end method
