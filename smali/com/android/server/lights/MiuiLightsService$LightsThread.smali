.class Lcom/android/server/lights/MiuiLightsService$LightsThread;
.super Ljava/lang/Thread;
.source "MiuiLightsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/lights/MiuiLightsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LightsThread"
.end annotation


# instance fields
.field private final LOOP_LIMIT:I

.field private final lightStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/lights/LightState;",
            ">;"
        }
    .end annotation
.end field

.field private loop_index:I

.field private mForceStop:Z

.field private final styleType:I

.field final synthetic this$0:Lcom/android/server/lights/MiuiLightsService;


# direct methods
.method public constructor <init>(Lcom/android/server/lights/MiuiLightsService;Ljava/util/List;II)V
    .locals 1
    .param p3, "styleType"    # I
    .param p4, "mUid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/lights/LightState;",
            ">;II)V"
        }
    .end annotation

    .line 563
    .local p2, "lightStateList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/lights/LightState;>;"
    iput-object p1, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 557
    const/16 v0, 0x23

    iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->LOOP_LIMIT:I

    .line 558
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->loop_index:I

    .line 564
    iput-object p2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->lightStateList:Ljava/util/List;

    .line 565
    iput p3, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->styleType:I

    .line 566
    invoke-static {p1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmTmpWorkSource(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/WorkSource;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/os/WorkSource;->set(I)V

    .line 567
    invoke-static {p1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmWakeLock(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {p1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmTmpWorkSource(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/WorkSource;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    .line 568
    return-void
.end method

.method private delayLocked(J)J
    .locals 8
    .param p1, "duration"    # J

    .line 614
    move-wide v0, p1

    .line 615
    .local v0, "durationRemaining":J
    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    if-lez v4, :cond_2

    .line 616
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    add-long/2addr v4, p1

    .line 619
    .local v4, "bedtime":J
    :cond_0
    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 621
    goto :goto_0

    .line 620
    :catch_0
    move-exception v6

    .line 622
    :goto_0
    iget-boolean v6, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->mForceStop:Z

    if-eqz v6, :cond_1

    .line 623
    goto :goto_1

    .line 625
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 626
    cmp-long v6, v0, v2

    if-gtz v6, :cond_0

    .line 627
    :goto_1
    sub-long v2, p1, v0

    return-wide v2

    .line 629
    .end local v4    # "bedtime":J
    :cond_2
    return-wide v2
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .line 633
    monitor-enter p0

    .line 634
    :try_start_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmThread(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightsThread;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->mForceStop:Z

    .line 635
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmThread(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightsThread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 636
    monitor-exit p0

    .line 637
    return-void

    .line 636
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public playLight(I)Z
    .locals 10
    .param p1, "styleType"    # I

    .line 591
    monitor-enter p0

    .line 592
    :try_start_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->lightStateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 593
    .local v0, "size":I
    const/4 v1, 0x0

    .line 594
    .local v1, "index":I
    :goto_0
    iget-boolean v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->mForceStop:Z

    if-nez v2, :cond_2

    .line 595
    if-ge v1, v0, :cond_0

    .line 596
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->lightStateList:Ljava/util/List;

    add-int/lit8 v3, v1, 0x1

    .end local v1    # "index":I
    .local v3, "index":I
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/lights/LightState;

    .line 597
    .local v1, "lightState":Lcom/android/server/lights/LightState;
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v4

    iget v5, v1, Lcom/android/server/lights/LightState;->colorARGB:I

    iget v6, v1, Lcom/android/server/lights/LightState;->flashMode:I

    iget v7, v1, Lcom/android/server/lights/LightState;->onMS:I

    iget v8, v1, Lcom/android/server/lights/LightState;->offMS:I

    iget v9, v1, Lcom/android/server/lights/LightState;->brightnessMode:I

    invoke-static/range {v4 .. v9}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 599
    iget v2, v1, Lcom/android/server/lights/LightState;->onMS:I

    iget v4, v1, Lcom/android/server/lights/LightState;->offMS:I

    add-int/2addr v2, v4

    int-to-long v4, v2

    invoke-direct {p0, v4, v5}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->delayLocked(J)J

    .line 600
    move v1, v3

    .end local v1    # "lightState":Lcom/android/server/lights/LightState;
    goto :goto_0

    .end local v3    # "index":I
    .local v1, "index":I
    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->loop_index:I

    const/16 v3, 0x23

    if-ge v2, v3, :cond_1

    .line 601
    const/4 v1, 0x0

    .line 602
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->loop_index:I

    goto :goto_0

    .line 604
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->cancel()V

    .line 605
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/android/server/lights/MiuiLightsService$LightImpl;->-$$Nest$mrealSetLightLocked(Lcom/android/server/lights/MiuiLightsService$LightImpl;IIIII)V

    .line 606
    iget-object v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v2}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmColorfulLight(Lcom/android/server/lights/MiuiLightsService;)Lcom/android/server/lights/MiuiLightsService$LightImpl;

    move-result-object v2

    const/4 v3, -0x1

    iput v3, v2, Lcom/android/server/lights/MiuiLightsService$LightImpl;->mLastLightStyle:I

    goto :goto_0

    .line 609
    .end local v0    # "size":I
    .end local v1    # "index":I
    :cond_2
    monitor-exit p0

    .line 610
    return v2

    .line 609
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .line 572
    const/4 v0, -0x8

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 573
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmWakeLock(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 575
    :try_start_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mturnoffBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 576
    iget v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->styleType:I

    invoke-virtual {p0, v0}, Lcom/android/server/lights/MiuiLightsService$LightsThread;->playLight(I)Z

    move-result v0

    .line 577
    .local v0, "finished":Z
    if-eqz v0, :cond_0

    .line 578
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mrecoveryBatteryLight(Lcom/android/server/lights/MiuiLightsService;)V

    .line 579
    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    iget v2, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->styleType:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v3, v3}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$mreportLedEventLocked(Lcom/android/server/lights/MiuiLightsService;IZII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    .end local v0    # "finished":Z
    :cond_0
    iget-object v0, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v0}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmWakeLock(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 583
    nop

    .line 584
    return-void

    .line 582
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/server/lights/MiuiLightsService$LightsThread;->this$0:Lcom/android/server/lights/MiuiLightsService;

    invoke-static {v1}, Lcom/android/server/lights/MiuiLightsService;->-$$Nest$fgetmWakeLock(Lcom/android/server/lights/MiuiLightsService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 583
    throw v0
.end method
