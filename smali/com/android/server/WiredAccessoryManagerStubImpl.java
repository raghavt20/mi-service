public class com.android.server.WiredAccessoryManagerStubImpl implements com.android.server.WiredAccessoryManagerStub {
	 /* .source "WiredAccessoryManagerStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer NOTE_USB_UNSUPPORT_HEADSET_PLUG;
	 private static final Integer SW_JACK_UNSUPPORTED_INSERT_BIT;
	 private static final Integer SW_JACK_UNSUPPORTED_INSERT_BIT_21;
	 private static final Integer SW_JACK_VIDEOOUT_INSERT;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.app.NotificationManager mNm;
	 /* # direct methods */
	 public com.android.server.WiredAccessoryManagerStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private void createNotification ( android.content.Context p0 ) {
		 /* .locals 9 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 80 */
		 /* new-instance v0, Landroid/content/ComponentName; */
		 final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
		 final String v2 = "com.android.settings.Settings$UsbHeadsetUnSupportActivity"; // const-string v2, "com.android.settings.Settings$UsbHeadsetUnSupportActivity"
		 /* invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
		 android.content.Intent .makeRestartActivityTask ( v0 );
		 /* .line 82 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 /* const/high16 v1, 0x4000000 */
		 int v2 = 0; // const/4 v2, 0x0
		 android.app.PendingIntent .getActivity ( p1,v2,v0,v1 );
		 /* .line 84 */
		 /* .local v1, "pit":Landroid/app/PendingIntent; */
		 v3 = com.android.internal.notification.SystemNotificationChannels.USB_HEADSET;
		 /* .line 85 */
		 /* .local v3, "channel":Ljava/lang/String; */
		 /* const v4, 0x110f0228 */
		 /* .line 87 */
		 /* .local v4, "title_unsupported":I */
		 /* const v5, 0x110f0226 */
		 /* .line 89 */
		 /* .local v5, "text_unsupported":I */
		 /* new-instance v6, Landroid/app/Notification$Builder; */
		 /* invoke-direct {v6, p1, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
		 /* .line 90 */
		 /* const v7, 0x10808b9 */
		 (( android.app.Notification$Builder ) v6 ).setSmallIcon ( v7 ); // invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
		 /* .line 91 */
		 /* const-wide/16 v7, 0x0 */
		 (( android.app.Notification$Builder ) v6 ).setWhen ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;
		 /* .line 92 */
		 int v7 = 1; // const/4 v7, 0x1
		 (( android.app.Notification$Builder ) v6 ).setOngoing ( v7 ); // invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;
		 /* .line 93 */
		 (( android.app.Notification$Builder ) v6 ).setDefaults ( v2 ); // invoke-virtual {v6, v2}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;
		 /* .line 94 */
		 /* const v6, 0x106001c */
		 v6 = 		 (( android.content.Context ) p1 ).getColor ( v6 ); // invoke-virtual {p1, v6}, Landroid/content/Context;->getColor(I)I
		 (( android.app.Notification$Builder ) v2 ).setColor ( v6 ); // invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;
		 /* .line 97 */
		 /* const-string/jumbo v6, "sys" */
		 (( android.app.Notification$Builder ) v2 ).setCategory ( v6 ); // invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;
		 /* .line 98 */
		 (( android.app.Notification$Builder ) v2 ).setVisibility ( v7 ); // invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;
		 /* .line 99 */
		 (( android.app.Notification$Builder ) v2 ).setContentIntent ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
		 /* .line 100 */
		 (( android.content.Context ) p1 ).getString ( v4 ); // invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
		 (( android.app.Notification$Builder ) v2 ).setContentTitle ( v6 ); // invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
		 /* .line 101 */
		 (( android.content.Context ) p1 ).getString ( v5 ); // invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;
		 (( android.app.Notification$Builder ) v2 ).setContentText ( v6 ); // invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
		 /* .line 102 */
		 /* .local v2, "builder":Landroid/app/Notification$Builder; */
		 (( android.app.Notification$Builder ) v2 ).build ( ); // invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
		 /* .line 103 */
		 /* .local v6, "notify":Landroid/app/Notification; */
		 v7 = this.mNm;
		 /* const v8, 0x53488888 */
		 (( android.app.NotificationManager ) v7 ).notify ( v8, v6 ); // invoke-virtual {v7, v8, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
		 /* .line 104 */
		 return;
	 } // .end method
	 private Boolean isUnsupportedBit ( Integer p0 ) {
		 /* .locals 2 */
		 /* .param p1, "switchMask" # I */
		 /* .line 44 */
		 final String v0 = "lmi"; // const-string v0, "lmi"
		 v1 = android.os.Build.DEVICE;
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 int v1 = 1; // const/4 v1, 0x1
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 45 */
			 /* const/high16 v0, 0x100000 */
			 /* and-int/2addr v0, p1 */
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 46 */
				 /* .line 49 */
			 } // :cond_0
			 /* const/high16 v0, 0x80000 */
			 /* and-int/2addr v0, p1 */
			 /* if-nez v0, :cond_2 */
			 /* const/16 v0, 0x114 */
			 /* if-ne p1, v0, :cond_1 */
			 /* .line 55 */
		 } // :cond_1
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 52 */
	 } // :cond_2
} // :goto_0
} // .end method
private void showUnsupportDeviceNotification ( android.content.Context p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "switchValues" # I */
/* .line 59 */
if ( p1 != null) { // if-eqz p1, :cond_3
	 /* .line 60 */
	 v0 = this.mNm;
	 /* if-nez v0, :cond_0 */
	 /* .line 61 */
	 final String v0 = "notification"; // const-string v0, "notification"
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/app/NotificationManager; */
	 this.mNm = v0;
	 /* .line 63 */
} // :cond_0
/* const v0, 0x80014 */
final String v1 = "WiredAccessoryManagerStubImpl"; // const-string v1, "WiredAccessoryManagerStubImpl"
/* if-eq p2, v0, :cond_2 */
/* const/16 v0, 0x114 */
/* if-eq p2, v0, :cond_2 */
/* const v0, 0x100014 */
/* if-ne p2, v0, :cond_1 */
/* .line 72 */
} // :cond_1
/* if-nez p2, :cond_3 */
/* .line 73 */
final String v0 = "Unsupported headset removed"; // const-string v0, "Unsupported headset removed"
android.util.Log .d ( v1,v0 );
/* .line 74 */
v0 = this.mNm;
/* const v1, 0x53488888 */
(( android.app.NotificationManager ) v0 ).cancel ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V
/* .line 70 */
} // :cond_2
} // :goto_0
final String v0 = "Unsupported headset inserted"; // const-string v0, "Unsupported headset inserted"
android.util.Log .d ( v1,v0 );
/* .line 71 */
/* invoke-direct {p0, p1}, Lcom/android/server/WiredAccessoryManagerStubImpl;->createNotification(Landroid/content/Context;)V */
/* .line 77 */
} // :cond_3
} // :goto_1
return;
} // .end method
/* # virtual methods */
public Boolean isDeviceUnsupported ( android.content.Context p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "switchValues" # I */
/* .param p3, "switchMask" # I */
/* .line 35 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/WiredAccessoryManagerStubImpl;->isUnsupportedBit(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 36 */
final String v0 = "WiredAccessoryManagerStubImpl"; // const-string v0, "WiredAccessoryManagerStubImpl"
final String v1 = "Device unsupported"; // const-string v1, "Device unsupported"
android.util.Log .d ( v0,v1 );
/* .line 37 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/WiredAccessoryManagerStubImpl;->showUnsupportDeviceNotification(Landroid/content/Context;I)V */
/* .line 38 */
int v0 = 1; // const/4 v0, 0x1
/* .line 40 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
