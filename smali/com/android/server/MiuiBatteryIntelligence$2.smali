.class Lcom/android/server/MiuiBatteryIntelligence$2;
.super Landroid/database/ContentObserver;
.source "MiuiBatteryIntelligence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/MiuiBatteryIntelligence;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryIntelligence;

.field final synthetic val$contentResolver:Landroid/content/ContentResolver;

.field final synthetic val$moduleName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryIntelligence;Landroid/os/Handler;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryIntelligence;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 180
    iput-object p1, p0, Lcom/android/server/MiuiBatteryIntelligence$2;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iput-object p3, p0, Lcom/android/server/MiuiBatteryIntelligence$2;->val$contentResolver:Landroid/content/ContentResolver;

    iput-object p4, p0, Lcom/android/server/MiuiBatteryIntelligence$2;->val$moduleName:Ljava/lang/String;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 183
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 184
    const-string v0, "MiuiBatteryIntelligence"

    const-string v1, "cloud data has update"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/android/server/MiuiBatteryIntelligence$2;->this$0:Lcom/android/server/MiuiBatteryIntelligence;

    iget-object v1, p0, Lcom/android/server/MiuiBatteryIntelligence$2;->val$contentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/server/MiuiBatteryIntelligence$2;->val$moduleName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/server/MiuiBatteryIntelligence;->-$$Nest$mreadLocalCloudControlData(Lcom/android/server/MiuiBatteryIntelligence;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 186
    return-void
.end method
