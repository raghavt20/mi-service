.class public Lcom/android/server/miuibpf/MiuiBpfServiceImpl;
.super Lcom/android/server/miuibpf/MiuiBpfServiceStub;
.source "MiuiBpfServiceImpl.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiBpfService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/android/server/miuibpf/MiuiBpfServiceStub;-><init>()V

    return-void
.end method

.method private static native runMiuiBpfMain()Z
.end method


# virtual methods
.method public start()V
    .locals 2

    .line 20
    const-string v0, "MiuiBpfService"

    const-string v1, "Here we starting MiuiBpfService..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    invoke-static {}, Lcom/android/server/miuibpf/MiuiBpfServiceImpl;->runMiuiBpfMain()Z

    .line 22
    return-void
.end method
