public class com.android.server.miuibpf.MiuiBpfServiceImpl extends com.android.server.miuibpf.MiuiBpfServiceStub {
	 /* .source "MiuiBpfServiceImpl.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.miuibpf.MiuiBpfServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Lcom/android/server/miuibpf/MiuiBpfServiceStub;-><init>()V */
		 return;
	 } // .end method
	 private static native Boolean runMiuiBpfMain ( ) {
	 } // .end method
	 /* # virtual methods */
	 public void start ( ) {
		 /* .locals 2 */
		 /* .line 20 */
		 final String v0 = "MiuiBpfService"; // const-string v0, "MiuiBpfService"
		 final String v1 = "Here we starting MiuiBpfService..."; // const-string v1, "Here we starting MiuiBpfService..."
		 android.util.Log .e ( v0,v1 );
		 /* .line 21 */
		 com.android.server.miuibpf.MiuiBpfServiceImpl .runMiuiBpfMain ( );
		 /* .line 22 */
		 return;
	 } // .end method
