public abstract class com.android.server.FixedFileObserver {
	 /* .source "FixedFileObserver.java" */
	 /* # static fields */
	 private static final java.util.HashMap sObserverLists;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/io/File;", */
	 /* "Ljava/util/Set<", */
	 /* "Lcom/android/server/FixedFileObserver;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private final Integer mMask;
private android.os.FileObserver mObserver;
private final java.io.File mRootPath;
/* # direct methods */
static Integer -$$Nest$fgetmMask ( com.android.server.FixedFileObserver p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/FixedFileObserver;->mMask:I */
} // .end method
static com.android.server.FixedFileObserver ( ) {
/* .locals 1 */
/* .line 13 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
public com.android.server.FixedFileObserver ( ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 19 */
/* const/16 v0, 0xfff */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/FixedFileObserver;-><init>(Ljava/lang/String;I)V */
return;
} // .end method
public com.android.server.FixedFileObserver ( ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "mask" # I */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 21 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
this.mRootPath = v0;
/* .line 22 */
/* iput p2, p0, Lcom/android/server/FixedFileObserver;->mMask:I */
/* .line 23 */
return;
} // .end method
/* # virtual methods */
protected void finalize ( ) {
/* .locals 0 */
/* .line 55 */
(( com.android.server.FixedFileObserver ) p0 ).stopWatching ( ); // invoke-virtual {p0}, Lcom/android/server/FixedFileObserver;->stopWatching()V
return;
} // .end method
public abstract void onEvent ( Integer p0, java.lang.String p1 ) {
} // .end method
public void startWatching ( ) {
/* .locals 4 */
/* .line 28 */
v0 = com.android.server.FixedFileObserver.sObserverLists;
/* monitor-enter v0 */
/* .line 29 */
try { // :try_start_0
	 v1 = this.mRootPath;
	 v1 = 	 (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
	 /* if-nez v1, :cond_0 */
	 v1 = this.mRootPath;
	 /* new-instance v2, Ljava/util/HashSet; */
	 /* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
	 (( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
	 /* .line 31 */
} // :cond_0
v1 = this.mRootPath;
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/Set; */
/* .line 33 */
v2 = /* .local v1, "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;" */
/* if-lez v2, :cond_1 */
/* check-cast v2, Lcom/android/server/FixedFileObserver; */
v2 = this.mObserver;
} // :cond_1
/* new-instance v2, Lcom/android/server/FixedFileObserver$1; */
v3 = this.mRootPath;
(( java.io.File ) v3 ).getPath ( ); // invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;
/* invoke-direct {v2, p0, v3, v1}, Lcom/android/server/FixedFileObserver$1;-><init>(Lcom/android/server/FixedFileObserver;Ljava/lang/String;Ljava/util/Set;)V */
} // :goto_0
this.mObserver = v2;
/* .line 38 */
(( android.os.FileObserver ) v2 ).startWatching ( ); // invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V
/* .line 39 */
/* .line 40 */
/* nop */
} // .end local v1 # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
/* monitor-exit v0 */
/* .line 41 */
return;
/* .line 40 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void stopWatching ( ) {
/* .locals 3 */
/* .line 44 */
v0 = com.android.server.FixedFileObserver.sObserverLists;
/* monitor-enter v0 */
/* .line 45 */
try { // :try_start_0
v1 = this.mRootPath;
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/Set; */
/* .line 46 */
/* .local v1, "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;" */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = this.mObserver;
/* if-nez v2, :cond_0 */
/* .line 48 */
} // :cond_0
v2 = /* .line 49 */
/* if-nez v2, :cond_1 */
v2 = this.mObserver;
(( android.os.FileObserver ) v2 ).stopWatching ( ); // invoke-virtual {v2}, Landroid/os/FileObserver;->stopWatching()V
/* .line 51 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
this.mObserver = v2;
/* .line 52 */
} // .end local v1 # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
/* monitor-exit v0 */
/* .line 53 */
return;
/* .line 46 */
/* .restart local v1 # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;" */
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
return;
/* .line 52 */
} // .end local v1 # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
