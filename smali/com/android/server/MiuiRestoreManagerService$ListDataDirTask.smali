.class Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;
.super Ljava/lang/Object;
.source "MiuiRestoreManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiRestoreManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListDataDirTask"
.end annotation


# instance fields
.field final callback:Lmiui/app/backup/IListDirCallback;

.field final maxCount:I

.field final path:Ljava/lang/String;

.field final start:J

.field final synthetic this$0:Lcom/android/server/MiuiRestoreManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;JILmiui/app/backup/IListDirCallback;)V
    .locals 0
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "start"    # J
    .param p5, "maxCount"    # I
    .param p6, "callback"    # Lmiui/app/backup/IListDirCallback;

    .line 638
    iput-object p1, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 639
    iput-object p2, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->path:Ljava/lang/String;

    .line 640
    iput-wide p3, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->start:J

    .line 641
    iput p5, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->maxCount:I

    .line 642
    iput-object p6, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->callback:Lmiui/app/backup/IListDirCallback;

    .line 643
    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .line 648
    const-string v0, "MiuiRestoreManagerService"

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/String;

    .line 649
    .local v2, "data":[Ljava/lang/String;
    const/4 v3, 0x1

    new-array v3, v3, [J

    .line 651
    .local v3, "offset":[J
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v12, v4

    .line 652
    .local v12, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-static {v4}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->path:Ljava/lang/String;

    iget-wide v6, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->start:J

    iget v8, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->maxCount:I

    int-to-long v8, v8

    move-object v10, v12

    move-object v11, v3

    invoke-virtual/range {v4 .. v11}, Lcom/android/server/pm/Installer;->listDataDir(Ljava/lang/String;JJLjava/util/List;[J)I

    move-result v4

    .line 653
    .local v4, "errorCode":I
    if-nez v4, :cond_0

    .line 654
    invoke-interface {v12, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v5

    .line 659
    .end local v12    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    goto :goto_0

    .line 656
    .end local v4    # "errorCode":I
    :catch_0
    move-exception v4

    .line 657
    .local v4, "e":Lcom/android/server/pm/Installer$InstallerException;
    const/4 v5, -0x1

    .line 658
    .local v5, "errorCode":I
    const-string v6, "list data dir error "

    invoke-static {v0, v6, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v4, v5

    .line 662
    .end local v5    # "errorCode":I
    .local v4, "errorCode":I
    :goto_0
    :try_start_1
    iget-object v7, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->callback:Lmiui/app/backup/IListDirCallback;

    iget-object v9, p0, Lcom/android/server/MiuiRestoreManagerService$ListDataDirTask;->path:Ljava/lang/String;

    aget-wide v11, v3, v1

    move v8, v4

    move-object v10, v2

    invoke-interface/range {v7 .. v12}, Lmiui/app/backup/IListDirCallback;->onListDataDirEnd(ILjava/lang/String;[Ljava/lang/String;J)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 665
    goto :goto_1

    .line 663
    :catch_1
    move-exception v1

    .line 664
    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "error when notify onListDataDirEnd "

    invoke-static {v0, v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 666
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method
