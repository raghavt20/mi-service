public class com.android.server.MiuiBatteryAuthentic {
	 /* .source "MiuiBatteryAuthentic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;, */
	 /* Lcom/android/server/MiuiBatteryAuthentic$IMTService; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BATTERY_AUTHENTIC;
private static final Boolean DEBUG;
private static final java.lang.String DEFAULT_BATTERT_SN;
private static final java.lang.String DEFAULT_IMEI;
private static final Integer DELAY_TIME;
private static volatile com.android.server.MiuiBatteryAuthentic INSTANCE;
private static final java.lang.String PROVISION_COMPLETE_BROADCAST;
private static final java.lang.String PUBLIC_KEY;
private static final java.lang.String TAG;
/* # instance fields */
public java.lang.String mBatterySn;
public java.lang.String mCloudSign;
public final android.content.ContentResolver mContentResolver;
public android.content.Context mContext;
private com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler mHandler;
public com.android.server.MiuiBatteryAuthentic$IMTService mIMTService;
public java.lang.String mImei;
public Boolean mIsVerified;
private miui.util.IMiCharge mMiCharge;
/* # direct methods */
static com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler -$$Nest$fgetmHandler ( com.android.server.MiuiBatteryAuthentic p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static miui.util.IMiCharge -$$Nest$fgetmMiCharge ( com.android.server.MiuiBatteryAuthentic p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mMiCharge;
} // .end method
static com.android.server.MiuiBatteryAuthentic ( ) {
	 /* .locals 1 */
	 /* .line 80 */
	 int v0 = 0; // const/4 v0, 0x0
	 return;
} // .end method
public com.android.server.MiuiBatteryAuthentic ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 93 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 65 */
	 miui.util.IMiCharge .getInstance ( );
	 this.mMiCharge = v0;
	 /* .line 72 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mImei = v0;
	 /* .line 73 */
	 this.mBatterySn = v0;
	 /* .line 74 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z */
	 /* .line 94 */
	 this.mContext = p1;
	 /* .line 95 */
	 /* new-instance v0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler; */
	 com.android.server.MiuiFgThread .get ( );
	 (( com.android.server.MiuiFgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiFgThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;-><init>(Lcom/android/server/MiuiBatteryAuthentic;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 96 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 this.mContentResolver = v0;
	 /* .line 97 */
	 /* new-instance v0, Lcom/android/server/MiuiBatteryAuthentic$IMTService; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryAuthentic$IMTService;-><init>(Lcom/android/server/MiuiBatteryAuthentic;)V */
	 this.mIMTService = v0;
	 /* .line 98 */
	 /* invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic;->initBatteryAuthenticCertificate()V */
	 /* .line 100 */
	 /* new-instance v0, Lcom/android/server/MiuiBatteryAuthentic$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryAuthentic$1;-><init>(Lcom/android/server/MiuiBatteryAuthentic;)V */
	 /* .line 112 */
	 /* .local v0, "receiver":Landroid/content/BroadcastReceiver; */
	 /* new-instance v1, Landroid/content/IntentFilter; */
	 final String v2 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"
	 /* invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
	 /* .line 113 */
	 /* .local v1, "filter":Landroid/content/IntentFilter; */
	 final String v2 = "android.provision.action.PROVISION_COMPLETE"; // const-string v2, "android.provision.action.PROVISION_COMPLETE"
	 (( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 114 */
	 int v2 = 2; // const/4 v2, 0x2
	 (( android.content.Context ) p1 ).registerReceiver ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
	 /* .line 115 */
	 return;
} // .end method
public static com.android.server.MiuiBatteryAuthentic getInstance ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 83 */
	 v0 = com.android.server.MiuiBatteryAuthentic.INSTANCE;
	 /* if-nez v0, :cond_1 */
	 /* .line 84 */
	 /* const-class v0, Lcom/android/server/MiuiBatteryAuthentic; */
	 /* monitor-enter v0 */
	 /* .line 85 */
	 try { // :try_start_0
		 v1 = com.android.server.MiuiBatteryAuthentic.INSTANCE;
		 /* if-nez v1, :cond_0 */
		 /* .line 86 */
		 /* new-instance v1, Lcom/android/server/MiuiBatteryAuthentic; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryAuthentic;-><init>(Landroid/content/Context;)V */
		 /* .line 88 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 90 */
} // :cond_1
} // :goto_0
v0 = com.android.server.MiuiBatteryAuthentic.INSTANCE;
} // .end method
private void initBatteryAuthenticCertificate ( ) {
/* .locals 4 */
/* .line 118 */
v0 = this.mContentResolver;
final String v1 = "battery_authentic_certificate"; // const-string v1, "battery_authentic_certificate"
android.provider.Settings$System .getString ( v0,v1 );
this.mCloudSign = v0;
/* .line 119 */
v0 = this.mHandler;
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) v0 ).getBatterySn ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;
this.mBatterySn = v0;
/* .line 120 */
v0 = this.mCloudSign;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* const-wide/16 v1, 0x0 */
/* if-nez v0, :cond_0 */
/* .line 121 */
v0 = this.mHandler;
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) v0 ).sendMessageDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 123 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z */
/* .line 124 */
v3 = this.mHandler;
(( com.android.server.MiuiBatteryAuthentic$BatteryAuthenticHandler ) v3 ).sendMessageDelayed ( v0, v1, v2 ); // invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V
/* .line 126 */
} // :goto_0
return;
} // .end method
