.class public Lcom/android/server/ScoutHelper$ScoutBinderInfo;
.super Ljava/lang/Object;
.source "ScoutHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ScoutHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScoutBinderInfo"
.end annotation


# instance fields
.field private mBinderTransInfo:Ljava/lang/StringBuilder;

.field private mCallType:I

.field private mFromPid:I

.field private mHasDThread:Z

.field private mMonkeyPid:I

.field private mPid:I

.field private mProcInfo:Ljava/lang/StringBuilder;

.field private mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;)V
    .locals 1
    .param p1, "mPid"    # I
    .param p2, "mFromPid"    # I
    .param p3, "mCallType"    # I
    .param p4, "mTag"    # Ljava/lang/String;

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mHasDThread:Z

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mBinderTransInfo:Ljava/lang/StringBuilder;

    .line 257
    iput p1, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mPid:I

    .line 258
    iput p2, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mFromPid:I

    .line 259
    iput-object p4, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mTag:Ljava/lang/String;

    .line 260
    iput p3, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mCallType:I

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mProcInfo:Ljava/lang/StringBuilder;

    .line 262
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 1
    .param p1, "mPid"    # I
    .param p2, "mCallType"    # I
    .param p3, "mTag"    # Ljava/lang/String;

    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IIILjava/lang/String;)V

    .line 252
    return-void
.end method


# virtual methods
.method public addBinderTransInfo(Ljava/lang/String;)V
    .locals 3
    .param p1, "sInfo"    # Ljava/lang/String;

    .line 281
    iget-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mBinderTransInfo:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    return-void
.end method

.method public getBinderTransInfo()Ljava/lang/String;
    .locals 3

    .line 285
    iget-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mBinderTransInfo:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 286
    .local v0, "info":Ljava/lang/String;
    const-string v1, ""

    if-ne v0, v1, :cond_0

    .line 287
    const-string v1, "Here are no Binder-related exception messages available."

    return-object v1

    .line 289
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Binder Tracsaction Info:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getCallType()I
    .locals 1

    .line 302
    iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mCallType:I

    return v0
.end method

.method public getDThreadState()Z
    .locals 1

    .line 273
    iget-boolean v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mHasDThread:Z

    return v0
.end method

.method public getFromPid()I
    .locals 1

    .line 298
    iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mFromPid:I

    return v0
.end method

.method public getMonkeyPid()I
    .locals 1

    .line 277
    iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mMonkeyPid:I

    return v0
.end method

.method public getPid()I
    .locals 1

    .line 294
    iget v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mPid:I

    return v0
.end method

.method public getProcInfo()Ljava/lang/String;
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mProcInfo:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "info":Ljava/lang/String;
    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public setDThreadState(Z)V
    .locals 0
    .param p1, "mHasDThread"    # Z

    .line 265
    iput-boolean p1, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mHasDThread:Z

    .line 266
    return-void
.end method

.method public setMonkeyPid(I)V
    .locals 0
    .param p1, "monkeyPid"    # I

    .line 269
    iput p1, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mMonkeyPid:I

    .line 270
    return-void
.end method

.method public setProcInfo(Ljava/lang/String;)V
    .locals 3
    .param p1, "procInfo"    # Ljava/lang/String;

    .line 310
    iget-object v0, p0, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->mProcInfo:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    return-void
.end method
