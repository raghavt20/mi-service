class com.android.server.MiuiBatteryServiceImpl$8 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 274 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 277 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 278 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetDEBUG ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "action = "; // const-string v2, "action = "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "MiuiBatteryServiceImpl"; // const-string v2, "MiuiBatteryServiceImpl"
	 android.util.Slog .d ( v2,v1 );
	 /* .line 279 */
} // :cond_0
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
	 /* .line 280 */
	 final String v1 = "plugged"; // const-string v1, "plugged"
	 int v2 = -1; // const/4 v2, -0x1
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 281 */
	 /* .local v1, "plugType":I */
	 final String v3 = "level"; // const-string v3, "level"
	 v2 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v3, v2 ); // invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 282 */
	 /* .local v2, "batteryLevel":I */
	 int v3 = 0; // const/4 v3, 0x0
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 int v4 = 1; // const/4 v4, 0x1
	 } // :cond_1
	 /* move v4, v3 */
	 /* .line 283 */
	 /* .local v4, "plugged":Z */
} // :goto_0
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmIsTestMode ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_3
	 /* const/16 v5, 0x4b */
	 /* if-ge v2, v5, :cond_2 */
	 /* const/16 v5, 0x28 */
	 /* if-gt v2, v5, :cond_3 */
	 /* .line 284 */
} // :cond_2
v5 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v5 );
/* const/16 v6, 0x1a */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v5 ).sendMessage ( v6, v2 ); // invoke-virtual {v5, v6, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V
/* .line 286 */
} // :cond_3
v5 = this.this$0;
v5 = com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmSupportWirelessCharge ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_4
/* if-nez v4, :cond_4 */
v5 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmMiCharge ( v5 );
v5 = (( miui.util.IMiCharge ) v5 ).getWirelessChargingStatus ( ); // invoke-virtual {v5}, Lmiui/util/IMiCharge;->getWirelessChargingStatus()I
/* if-nez v5, :cond_4 */
/* .line 288 */
v5 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v5 );
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v5 ).sendMessage ( v3, v2 ); // invoke-virtual {v5, v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V
/* .line 290 */
} // .end local v1 # "plugType":I
} // .end local v2 # "batteryLevel":I
} // .end local v4 # "plugged":Z
} // :cond_4
} // :cond_5
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const-wide/16 v2, 0x0 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 291 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
/* const/16 v4, 0x12 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).sendMessageDelayed ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 292 */
} // :cond_6
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 293 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fgetmHandler ( v1 );
/* const/16 v4, 0x13 */
(( com.android.server.MiuiBatteryServiceImpl$BatteryHandler ) v1 ).sendMessageDelayed ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V
/* .line 295 */
} // :cond_7
} // :goto_1
return;
} // .end method
