.class Lcom/android/server/MountServiceIdlerImpl;
.super Ljava/lang/Object;
.source "MountServiceIdlerImpl.java"

# interfaces
.implements Lcom/android/server/MountServiceIdlerStub;


# static fields
.field private static final FINISH_INTERVAL_TIME:J = 0x6ddd00L

.field private static final MINIMUM_BATTERY_LEVEL:I = 0xa

.field private static final MINIMUM_INTERVAL_TIME:J = 0x1b7740L

.field private static final TAG:Ljava/lang/String; = "MountServiceIdlerImpl"


# instance fields
.field private mScreenOnReceiver:Landroid/content/BroadcastReceiver;

.field mSm:Landroid/os/storage/IStorageManager;

.field private sNextTrimDuration:J


# direct methods
.method constructor <init>()V
    .locals 2

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-wide/32 v0, 0x6ddd00

    iput-wide v0, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    .line 34
    new-instance v0, Lcom/android/server/MountServiceIdlerImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/MountServiceIdlerImpl$1;-><init>(Lcom/android/server/MountServiceIdlerImpl;)V

    iput-object v0, p0, Lcom/android/server/MountServiceIdlerImpl;->mScreenOnReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public addScreenOnFilter(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 93
    sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z

    if-eqz v0, :cond_0

    .line 94
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 95
    .local v0, "screenOnFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 96
    iget-object v1, p0, Lcom/android/server/MountServiceIdlerImpl;->mScreenOnReceiver:Landroid/content/BroadcastReceiver;

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 98
    .end local v0    # "screenOnFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public internalScheduleIdlePass(Landroid/content/Context;ILandroid/content/ComponentName;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "jobId"    # I
    .param p3, "componentName"    # Landroid/content/ComponentName;

    .line 77
    const-string v0, "jobscheduler"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 79
    .local v0, "tm":Landroid/app/job/JobScheduler;
    iget-wide v1, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    const-wide/32 v3, 0x1b7740

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    .line 80
    iput-wide v3, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    .line 83
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sNextTrimDuration :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MountServiceIdlerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    invoke-direct {v1, p2, p3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 85
    .local v1, "builder":Landroid/app/job/JobInfo$Builder;
    iget-wide v2, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    .line 86
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 88
    const/4 v2, 0x1

    return v2
.end method

.method public runIdleMaint(Landroid/content/Context;ILandroid/content/ComponentName;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "jobId"    # I
    .param p3, "componentName"    # Landroid/content/ComponentName;

    .line 59
    const-string v0, "batterymanager"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/BatteryManager;

    .line 60
    .local v0, "bm":Landroid/os/BatteryManager;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/BatteryManager;->getIntProperty(I)I

    move-result v1

    .line 62
    .local v1, "batteryLevel":I
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 63
    .local v2, "pm":Landroid/os/PowerManager;
    invoke-virtual {v2}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v3

    .line 65
    .local v3, "isInteractive":Z
    if-nez v3, :cond_0

    const/16 v4, 0xa

    if-lt v1, v4, :cond_0

    .line 66
    const-wide/32 v4, 0x6ddd00

    iput-wide v4, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    .line 67
    const/4 v4, 0x0

    return v4

    .line 69
    :cond_0
    iget-wide v4, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    const/4 v6, 0x1

    shr-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/server/MountServiceIdlerImpl;->sNextTrimDuration:J

    .line 70
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/MountServiceIdlerImpl;->internalScheduleIdlePass(Landroid/content/Context;ILandroid/content/ComponentName;)Z

    .line 71
    return v6
.end method
