class com.android.server.MiuiBatteryAuthentic$IMTService {
	 /* .source "MiuiBatteryAuthentic.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryAuthentic; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "IMTService" */
} // .end annotation
/* # instance fields */
private final java.lang.String DEFAULT;
private final Integer GET_ECC_SIGN;
private final Integer GET_FID;
private final java.lang.String INTERFACE_DESCRIPTOR;
private final java.lang.String SERVICE_NAME;
final com.android.server.MiuiBatteryAuthentic this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryAuthentic$IMTService ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryAuthentic; */
/* .line 654 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 655 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.mtdservice@1.0::IMTService" */
this.SERVICE_NAME = v0;
/* .line 656 */
this.INTERFACE_DESCRIPTOR = v0;
/* .line 657 */
final String v0 = "default"; // const-string v0, "default"
this.DEFAULT = v0;
/* .line 658 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->GET_FID:I */
/* .line 659 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;->GET_ECC_SIGN:I */
return;
} // .end method
/* # virtual methods */
public java.lang.String eccSign ( Integer p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "keyType" # I */
/* .param p2, "text" # Ljava/lang/String; */
/* .line 683 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.mtdservice@1.0::IMTService" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 684 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
/* .line 686 */
/* .local v2, "val":Ljava/lang/String; */
try { // :try_start_0
	 final String v3 = "default"; // const-string v3, "default"
	 android.os.HwBinder .getService ( v0,v3 );
	 /* .line 687 */
	 /* .local v3, "hwService":Landroid/os/IHwBinder; */
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 688 */
		 /* new-instance v4, Landroid/os/HwParcel; */
		 /* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
		 /* .line 689 */
		 /* .local v4, "hidl_request":Landroid/os/HwParcel; */
		 (( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
		 /* .line 690 */
		 (( android.os.HwParcel ) v4 ).writeInt32 ( p1 ); // invoke-virtual {v4, p1}, Landroid/os/HwParcel;->writeInt32(I)V
		 /* .line 691 */
		 (( android.os.HwParcel ) v4 ).writeString ( p2 ); // invoke-virtual {v4, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
		 /* .line 692 */
		 int v0 = 2; // const/4 v0, 0x2
		 int v5 = 0; // const/4 v5, 0x0
		 /* .line 693 */
		 (( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
		 /* .line 694 */
		 (( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
		 /* .line 695 */
		 (( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* move-object v2, v0 */
		 /* .line 700 */
	 } // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_0
/* nop */
} // :goto_0
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 701 */
/* .line 700 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 697 */
/* :catch_0 */
/* move-exception v0 */
/* .line 698 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "MiuiBatteryAuthentic"; // const-string v3, "MiuiBatteryAuthentic"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "IMTService eccSign transact failed."; // const-string v5, "IMTService eccSign transact failed."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 700 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 702 */
} // :goto_1
/* .line 700 */
} // :goto_2
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 701 */
/* throw v0 */
} // .end method
public java.lang.String getFid ( ) {
/* .locals 6 */
/* .line 662 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.mtdservice@1.0::IMTService" */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 663 */
/* .local v1, "hidl_reply":Landroid/os/HwParcel; */
int v2 = 0; // const/4 v2, 0x0
/* .line 665 */
/* .local v2, "val":Ljava/lang/String; */
try { // :try_start_0
final String v3 = "default"; // const-string v3, "default"
android.os.HwBinder .getService ( v0,v3 );
/* .line 666 */
/* .local v3, "hwService":Landroid/os/IHwBinder; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 667 */
/* new-instance v4, Landroid/os/HwParcel; */
/* invoke-direct {v4}, Landroid/os/HwParcel;-><init>()V */
/* .line 668 */
/* .local v4, "hidl_request":Landroid/os/HwParcel; */
(( android.os.HwParcel ) v4 ).writeInterfaceToken ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 669 */
int v0 = 1; // const/4 v0, 0x1
int v5 = 0; // const/4 v5, 0x0
/* .line 670 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 671 */
(( android.os.HwParcel ) v4 ).releaseTemporaryStorage ( ); // invoke-virtual {v4}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 672 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v0 */
/* .line 677 */
} // .end local v3 # "hwService":Landroid/os/IHwBinder;
} // .end local v4 # "hidl_request":Landroid/os/HwParcel;
} // :cond_0
/* nop */
} // :goto_0
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 678 */
/* .line 677 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 674 */
/* :catch_0 */
/* move-exception v0 */
/* .line 675 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "MiuiBatteryAuthentic"; // const-string v3, "MiuiBatteryAuthentic"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "IMTService getFid transact failed."; // const-string v5, "IMTService getFid transact failed."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 677 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 679 */
} // :goto_1
/* .line 677 */
} // :goto_2
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 678 */
/* throw v0 */
} // .end method
