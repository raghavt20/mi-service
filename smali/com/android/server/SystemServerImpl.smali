.class public Lcom/android/server/SystemServerImpl;
.super Lcom/android/server/SystemServerStub;
.source "SystemServerImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.MiuiStubImplManifest$$"
.end annotation


# static fields
.field private static final AML_MIUI_WIFI_SERVICE:Ljava/lang/String; = "AmlMiuiWifiService"

.field private static final AML_MIUI_WIFI_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.wifi.AmlMiuiWifiService"

.field private static final AML_SLAVE_WIFI_SERVICE:Ljava/lang/String; = "AmlSlaveWifiService"

.field private static final AML_SLAVE_WIFI_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.wifi.AmlSlaveWifiService"

.field private static final DEBUG:Z = true

.field private static final DEFAULT_HEAP_DUMP_FILE:Ljava/io/File;

.field private static final MAX_HEAP_DUMPS:I = 0x2

.field private static final MIUI_HEAP_DUMP_PATH:Ljava/lang/String; = "/data/miuilog/stability/resleak/fdtrack"

.field private static final MIUI_SLAVE_WIFI_SERVICE:Ljava/lang/String; = "SlaveWifiService"

.field private static final MIUI_WIFI_SERVICE:Ljava/lang/String; = "MiuiWifiService"

.field private static final TAG:Ljava/lang/String; = "SystemServerI"

.field private static final WIFI_APEX_SERVICE_JAR_PATH:Ljava/lang/String; = "/apex/com.android.wifi/javalib/service-wifi.jar"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 82
    const-string v0, "SystemServerI"

    new-instance v1, Ljava/io/File;

    const-string v2, "/data/system/heapdump/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/SystemServerImpl;->DEFAULT_HEAP_DUMP_FILE:Ljava/io/File;

    .line 118
    const-class v1, Lcom/android/server/SystemServerImpl;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    check-cast v1, Ldalvik/system/PathClassLoader;

    .line 119
    .local v1, "pathClassLoader":Ldalvik/system/PathClassLoader;
    const-string v2, "/system_ext/framework/miuix.jar"

    invoke-virtual {v1, v2}, Ldalvik/system/PathClassLoader;->addDexPath(Ljava/lang/String;)V

    .line 121
    const-string v2, "/system_ext/framework/miui-cameraopt.jar"

    invoke-virtual {v1, v2}, Ldalvik/system/PathClassLoader;->addDexPath(Ljava/lang/String;)V

    .line 125
    :try_start_0
    const-string v2, "Load libmiui_service"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string v2, "miui_service"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    goto :goto_0

    .line 127
    :catch_0
    move-exception v2

    .line 128
    .local v2, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v3, "can\'t loadLibrary libmiui_service"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 130
    .end local v1    # "pathClassLoader":Ldalvik/system/PathClassLoader;
    .end local v2    # "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 103
    invoke-direct {p0}, Lcom/android/server/SystemServerStub;-><init>()V

    .line 104
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    .line 106
    const-string v0, "ro.crypto.type"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "vold.decrypt"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "trigger_restart_framework"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    :cond_0
    invoke-static {}, Lcom/android/server/SystemServerImpl;->enforceVersionPolicy()V

    .line 114
    :cond_1
    return-void
.end method

.method private static enforceVersionPolicy()V
    .locals 9

    .line 353
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 354
    .local v0, "product":Ljava/lang/String;
    const-string v1, "ro.product.mod_device"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_in"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 355
    .local v1, "isIndiaBuild":Z
    const-string v2, "chenfeng"

    const-string v3, "chenfeng_"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "DPC_DLC_WHITE_LIST":[Ljava/lang/String;
    const-string v3, "ro.secureboot.lockstate"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "locked"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "SystemServerI"

    if-nez v3, :cond_0

    .line 360
    const-string v3, "enforceVersionPolicy: device unlocked"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    return-void

    .line 364
    :cond_0
    array-length v3, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_5

    aget-object v6, v2, v5

    .line 365
    .local v6, "p":Ljava/lang/String;
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v0, :cond_4

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 366
    :cond_1
    const-string v7, "ro.boot.hwc"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 367
    .local v7, "country":Ljava/lang/String;
    if-eqz v7, :cond_4

    const-string v8, "India"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "IN"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 368
    :cond_2
    if-nez v1, :cond_3

    .line 369
    const-string v8, "DPC/DLC devices IN hardware can\'t run Global build; reboot into recovery!!!"

    invoke-static {v4, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-static {}, Lcom/android/server/SystemServerImpl;->rebootIntoRecovery()V

    goto :goto_1

    .line 372
    :cond_3
    const-string v3, "enforceVersionPolicy:no in device"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    return-void

    .line 364
    .end local v6    # "p":Ljava/lang/String;
    .end local v7    # "country":Ljava/lang/String;
    :cond_4
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 379
    :cond_5
    invoke-static {}, Lcom/android/server/SystemServerImpl;->isGlobalHaredware()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 380
    const-string v3, "enforceVersionPolicy: global device"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    return-void

    .line 384
    :cond_6
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v3, :cond_7

    .line 385
    const-string v3, "CN hardware can\'t run Global build; reboot into recovery!!!"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    invoke-static {}, Lcom/android/server/SystemServerImpl;->rebootIntoRecovery()V

    .line 388
    :cond_7
    return-void
.end method

.method private static isGlobalHaredware()Z
    .locals 2

    .line 345
    const-string v0, "ro.boot.hwc"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "country":Ljava/lang/String;
    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    const-string v1, "CN_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 349
    :cond_0
    const/4 v1, 0x1

    return v1

    .line 347
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return v1
.end method

.method private static rebootIntoRecovery()V
    .locals 2

    .line 338
    const-string v0, "--show_version_mismatch\n"

    invoke-static {v0}, Lcom/android/server/BcbUtil;->setupBcb(Ljava/lang/String;)Z

    .line 339
    const-string/jumbo v0, "sys.powerctl"

    const-string v1, "reboot,recovery"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    return-void
.end method

.method private static startService(Ljava/lang/String;)V
    .locals 4
    .param p0, "className"    # Ljava/lang/String;

    .line 156
    const-class v0, Lcom/android/server/SystemServiceManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/SystemServiceManager;

    .line 158
    .local v0, "manager":Lcom/android/server/SystemServiceManager;
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    goto :goto_0

    .line 159
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to start "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SystemServerI"

    invoke-static {v3, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 162
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method


# virtual methods
.method addCameraCoveredManagerService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 468
    const-string v0, "SystemServerI"

    :try_start_0
    const-string v1, "add CameraCoveredManagerService"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    const-string v1, "com.android.server.cameracovered.MiuiCameraCoveredManagerService$Lifecycle"

    invoke-static {v1}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 472
    goto :goto_0

    .line 470
    :catchall_0
    move-exception v1

    .line 471
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add CameraCoveredManagerService fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    .end local v1    # "e":Ljava/lang/Throwable;
    :goto_0
    return-void
.end method

.method final addExtraServices(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onlyCore"    # Z

    .line 171
    sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_SHOULDERKEY:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_MIGAMEMACRO:Z

    if-eqz v0, :cond_1

    .line 172
    :cond_0
    const-string v0, "com.android.server.input.shoulderkey.ShoulderKeyManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 176
    :cond_1
    invoke-static {}, Lcom/android/server/wm/AppContinuityRouterStub;->get()Lcom/android/server/wm/AppContinuityRouterStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/AppContinuityRouterStub;->initContinuityManagerService()V

    .line 179
    const-string v0, "com.miui.server.SecurityManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 181
    const-string v0, "com.miui.server.MiuiWebViewManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 183
    const-string v0, "com.miui.server.MiuiInitServer$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 185
    const-string v0, "com.miui.server.BackupManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 187
    const-string v0, "com.miui.server.stepcounter.StepCounterManagerService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 189
    const-string v0, "com.android.server.location.LocationPolicyManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 191
    const-string v0, "com.miui.server.PerfShielderService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 193
    const-string v0, "com.miui.server.greeze.GreezeManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 195
    const-string v0, "config_tof_proximity_available"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 196
    const-string v0, "config_tof_gesture_available"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 197
    const-string v0, "config_aon_gesture_available"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 198
    :cond_2
    const-string v0, "com.android.server.tof.ContactlessGestureService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 202
    :cond_3
    invoke-static {}, Lcom/miui/server/car/MiuiCarServiceStub;->get()Lcom/miui/server/car/MiuiCarServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/car/MiuiCarServiceStub;->publishCarService()V

    .line 204
    const-string v0, "com.miui.server.turbosched.TurboSchedManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 206
    const-string v0, "com.android.server.am.ProcessManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 208
    const-string v0, "com.miui.server.rtboost.SchedBoostService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 210
    const-string v0, "com.miui.server.MiuiCldService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 212
    const-string v0, "com.miui.server.MiuiFboService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 214
    const-string v0, "com.miui.server.stability.StabilityLocalService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 216
    const-string v0, "persist.sys.stability.swapEnable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 217
    const-string v0, "com.miui.server.MiuiSwapService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 220
    :cond_4
    const-string v0, "com.android.server.am.MiuiMemoryService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 222
    const-string v0, "com.miui.server.MiuiDfcService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 224
    const-string v0, "com.miui.server.sentinel.MiuiSentinelService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 227
    new-instance v0, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;

    invoke-direct {v0, p1}, Lcom/miui/whetstone/server/WhetstoneActivityManagerService;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "whetstone.activity"

    invoke-static {v2, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 230
    invoke-static {}, Landroid/util/MiuiAppSizeCompatModeImpl;->getInstance()Landroid/util/MiuiAppSizeCompatModeImpl;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/MiuiAppSizeCompatModeImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 231
    const-string v0, "com.android.server.wm.MiuiSizeCompatService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 235
    :cond_5
    sget-boolean v0, Lcom/miui/autoui/MiuiAutoUIManager;->IS_AUTO_UI_ENABLED:Z

    if-eqz v0, :cond_6

    .line 236
    const-string v0, "com.android.server.autoui.MiuiAutoUIManagerService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 240
    :cond_6
    const-string v0, "com.android.server.input.MiuiInputManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 243
    sget-boolean v0, Landroid/hovermode/MiuiHoverModeManager;->IS_HOVERMODE_ENABLED:Z

    if-eqz v0, :cond_7

    .line 244
    const-string v0, "SystemServerI"

    const-string/jumbo v2, "start MiuiHoverModeService"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const-string v0, "com.android.server.wm.MiuiHoverModeService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 249
    :cond_7
    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    if-eqz v0, :cond_8

    .line 250
    const-string v0, "com.android.server.display.DisplayFeatureManagerService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 254
    :cond_8
    const-string v0, "ro.vendor.display.uiservice.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 255
    const-string v0, "com.android.server.ui.UIService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 259
    :cond_9
    invoke-static {}, Lcom/android/server/MiuiFgThread;->initialMiuiFgThread()V

    .line 262
    sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z

    if-eqz v0, :cond_a

    .line 263
    const-string v0, "com.miui.server.enterprise.EnterpriseManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 265
    :cond_a
    sget-boolean v0, Lmiui/enterprise/EnterpriseManagerStub;->ENTERPRISE_ACTIVATED:Z

    if-eqz v0, :cond_b

    .line 266
    invoke-static {}, Lmiui/enterprise/EnterpriseManagerStub;->getInstance()Lmiui/enterprise/IEnterpriseHelper;

    move-result-object v0

    .line 267
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 266
    invoke-interface {v0, p1, v1}, Lmiui/enterprise/IEnterpriseHelper;->startService(Landroid/content/Context;Ljava/lang/ClassLoader;)V

    .line 271
    :cond_b
    const-string v0, "com.android.server.am.SmartPowerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 274
    const-string v0, "com.miui.server.migard.MiGardService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 277
    const-string v0, "com.miui.server.blackmask.MiBlackMaskService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 279
    const-string v0, "com.xiaomi.mirror.service.MirrorService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 282
    invoke-static {}, Landroid/magicpointer/MiuiMagicPointerStub;->isSupportMagicPointer()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 284
    const-string v0, "com.android.server.input.MiuiMagicPointerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 288
    :cond_c
    invoke-static {}, Lcom/android/server/MiuiCommonCloudServiceStub;->getInstance()Lcom/android/server/MiuiCommonCloudServiceStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/MiuiCommonCloudServiceStub;->init(Landroid/content/Context;)V

    .line 290
    const-string v0, "com.miui.server.multisence.MultiSenceService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 292
    const-string v0, "ro.miui.build.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cn"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 293
    const-string v0, "com.miui.server.rescue.BrokenScreenRescueService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 296
    :cond_d
    invoke-static {}, Landroid/appcompat/ApplicationCompatUtilsStub;->get()Landroid/appcompat/ApplicationCompatUtilsStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isAppCompatEnabled()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 298
    invoke-static {}, Lcom/android/server/wm/ApplicationCompatRouterStub;->get()Lcom/android/server/wm/ApplicationCompatRouterStub;

    .line 300
    invoke-static {}, Landroid/appcompat/ApplicationCompatUtilsStub;->get()Landroid/appcompat/ApplicationCompatUtilsStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appcompat/ApplicationCompatUtilsStub;->isContinuityEnabled()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 301
    invoke-static {}, Lcom/android/server/wm/AppContinuityRouterStub;->get()Lcom/android/server/wm/AppContinuityRouterStub;

    .line 305
    :cond_e
    const-string v0, "com.miui.server.MiuiFreeDragService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 307
    const-string v0, "com.android.server.powerconsumpiton.PowerConsumptionService"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 309
    const-string v0, "com.android.server.aiinput.AIInputTextManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 312
    const-string v0, "com.miui.cameraopt.CameraOptManagerService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 315
    new-instance v0, Lcom/xiaomi/interconnection/InterconnectionService;

    invoke-direct {v0, p1}, Lcom/xiaomi/interconnection/InterconnectionService;-><init>(Landroid/content/Context;)V

    const-string/jumbo v1, "xiaomi.InterconnectionService"

    invoke-static {v1, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 319
    const-string v0, "com.xiaomi.vkmode.service.MiuiForceVkService$Lifecycle"

    invoke-static {v0}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/android/server/am/MimdManagerServiceStub;->get()Lcom/android/server/am/MimdManagerServiceStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/am/MimdManagerServiceStub;->systemReady(Landroid/content/Context;)V

    .line 324
    return-void
.end method

.method addMiuiPeriodicCleanerService(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 4
    .param p1, "atm"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 479
    const-string v0, "SystemServerI"

    const-string v1, "persist.sys.periodic.u.enable"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    return-void

    .line 483
    :cond_0
    :try_start_0
    const-string/jumbo v1, "start PeriodicCleanerService"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    const-string v1, "com.android.server.am.PeriodicCleanerService"

    invoke-static {v1}, Lcom/android/server/SystemServerImpl;->startService(Ljava/lang/String;)V

    .line 485
    const-class v1, Lcom/android/server/am/PeriodicCleanerInternalStub;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/PeriodicCleanerInternalStub;

    invoke-virtual {p1, v1}, Lcom/android/server/wm/ActivityTaskManagerService;->setPeriodicCleaner(Lcom/android/server/am/PeriodicCleanerInternalStub;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    goto :goto_0

    .line 486
    :catchall_0
    move-exception v1

    .line 487
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add MiuiPeriodicCleanerService fail "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    .end local v1    # "e":Ljava/lang/Throwable;
    :goto_0
    return-void
.end method

.method addMiuiRestoreManagerService(Landroid/content/Context;Lcom/android/server/pm/Installer;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installer"    # Lcom/android/server/pm/Installer;

    .line 407
    const-string v0, ": service must have a public constructor with a Context argument"

    const-string v1, "Failed to create service "

    const-string v2, "com.android.server.MiuiRestoreManagerService$Lifecycle"

    .line 410
    .local v2, "className":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 413
    .local v3, "classLoader":Ljava/lang/ClassLoader;
    const/4 v4, 0x1

    :try_start_0
    invoke-static {v2, v4, v3}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_5

    .line 423
    .local v5, "serviceClass":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/android/server/SystemService;>;"
    nop

    .line 424
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    .line 425
    .local v6, "name":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Starting "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "SystemServerI"

    invoke-static {v8, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "StartService "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-wide/32 v9, 0x80000

    invoke-static {v9, v10, v7}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 429
    const-class v7, Lcom/android/server/SystemService;

    invoke-virtual {v7, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 435
    const/4 v7, 0x2

    :try_start_1
    new-array v7, v7, [Ljava/lang/Class;

    const-class v9, Landroid/content/Context;

    const/4 v10, 0x0

    aput-object v9, v7, v10

    const-class v9, Lcom/android/server/pm/Installer;

    aput-object v9, v7, v4

    invoke-virtual {v5, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    .line 436
    .local v4, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/android/server/SystemService;>;"
    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/SystemService;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v7

    .line 449
    .end local v4    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Lcom/android/server/SystemService;>;"
    .local v0, "service":Lcom/android/server/SystemService;
    nop

    .line 450
    const-class v1, Lcom/android/server/SystemServiceManager;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/SystemServiceManager;

    .line 452
    .local v1, "manager":Lcom/android/server/SystemServiceManager;
    :try_start_2
    invoke-virtual {v1, v0}, Lcom/android/server/SystemServiceManager;->startService(Lcom/android/server/SystemService;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 455
    goto :goto_0

    .line 453
    :catch_0
    move-exception v4

    .line 454
    .local v4, "e":Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to start "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 456
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 446
    .end local v0    # "service":Lcom/android/server/SystemService;
    .end local v1    # "manager":Lcom/android/server/SystemServiceManager;
    :catch_1
    move-exception v0

    .line 447
    .local v0, "ex":Ljava/lang/reflect/InvocationTargetException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ": service constructor threw an exception"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 443
    .end local v0    # "ex":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v4

    .line 444
    .local v4, "ex":Ljava/lang/NoSuchMethodException;
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 440
    .end local v4    # "ex":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v4

    .line 441
    .local v4, "ex":Ljava/lang/IllegalAccessException;
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 437
    .end local v4    # "ex":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 438
    .local v0, "ex":Ljava/lang/InstantiationException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ": service could not be instantiated"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 430
    .end local v0    # "ex":Ljava/lang/InstantiationException;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to create "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": service must extend "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v4, Lcom/android/server/SystemService;

    .line 431
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    .end local v5    # "serviceClass":Ljava/lang/Class;, "Ljava/lang/Class<Lcom/android/server/SystemService;>;"
    .end local v6    # "name":Ljava/lang/String;
    :catch_5
    move-exception v0

    .line 416
    .local v0, "ex":Ljava/lang/ClassNotFoundException;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " from class loader "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 417
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ": service class not found, usually indicates that the caller should have called PackageManager.hasSystemFeature() to check whether the feature is available on this device before trying to start the services that implement it. Also ensure that the correct path for the classloader is supplied, if applicable."

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method createLightsServices()Ljava/lang/Class;
    .locals 4

    .line 146
    const-class v0, Lcom/android/server/lights/LightsService;

    .line 148
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    const-string v1, "com.android.server.lights.MiuiLightsService"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 151
    goto :goto_0

    .line 149
    :catch_0
    move-exception v1

    .line 150
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "SystemServerI"

    const-string v3, "Failed to find MiuiLightsService"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :goto_0
    return-object v0
.end method

.method createPhoneWindowManager()Lcom/android/server/policy/PhoneWindowManager;
    .locals 2

    .line 134
    new-instance v0, Lcom/android/server/policy/PhoneWindowManager;

    invoke-direct {v0}, Lcom/android/server/policy/PhoneWindowManager;-><init>()V

    .line 136
    .local v0, "phoneWindowManager":Lcom/android/server/policy/PhoneWindowManager;
    :try_start_0
    const-string v1, "com.android.server.policy.MiuiPhoneWindowManager"

    .line 137
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/policy/PhoneWindowManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 140
    goto :goto_0

    .line 138
    :catch_0
    move-exception v1

    .line 139
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 141
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method

.method public getConnectivitylibpath()Ljava/lang/String;
    .locals 1

    .line 460
    const-string v0, ":/system_ext/framework/miui-connectivity-service.jar"

    return-object v0
.end method

.method public getHeapDumpDir()Ljava/io/File;
    .locals 4

    .line 494
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miuilog/stability/resleak/fdtrack"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 495
    .local v0, "fdtrackDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 496
    sget-object v1, Lcom/android/server/SystemServerImpl;->DEFAULT_HEAP_DUMP_FILE:Ljava/io/File;

    return-object v1

    .line 498
    :cond_0
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    const/16 v3, 0x1ed

    invoke-static {v0, v3, v1, v2}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    move-result v1

    if-eqz v1, :cond_1

    .line 499
    sget-object v1, Lcom/android/server/SystemServerImpl;->DEFAULT_HEAP_DUMP_FILE:Ljava/io/File;

    return-object v1

    .line 501
    :cond_1
    return-object v0
.end method

.method public getMiuilibpath()Ljava/lang/String;
    .locals 1

    .line 402
    const-string v0, ":/system_ext/framework/miui-wifi-service.jar"

    return-object v0
.end method

.method public keepDumpSize(Ljava/util/TreeSet;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeSet<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 505
    .local p1, "fileTreeSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    invoke-virtual {p1}, Ljava/util/TreeSet;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    .line 506
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 507
    invoke-virtual {p1}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 509
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 510
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_1

    .line 511
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to clean up fdtrack "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "System"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    goto :goto_1

    .line 515
    :cond_2
    return-void
.end method

.method markBootDexopt(JJ)V
    .locals 3
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J

    .line 397
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    sub-long v1, p3, p1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/BootEventManager;->setBootDexopt(J)V

    .line 398
    return-void
.end method

.method markPmsScan(JJ)V
    .locals 1
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J

    .line 392
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiui/mqsas/sdk/BootEventManager;->setPmsScanStart(J)V

    .line 393
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lmiui/mqsas/sdk/BootEventManager;->setPmsScanEnd(J)V

    .line 394
    return-void
.end method

.method markSystemRun(J)V
    .locals 5
    .param p1, "time"    # J

    .line 328
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 329
    .local v0, "now":J
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v2

    sub-long v3, v0, p1

    invoke-virtual {v2, v3, v4}, Lmiui/mqsas/sdk/BootEventManager;->setZygotePreload(J)V

    .line 330
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lmiui/mqsas/sdk/BootEventManager;->setSystemRun(J)V

    .line 331
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStubHead;->isActivityEmbeddingEnable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStubHead;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStubHead;

    .line 334
    :cond_0
    return-void
.end method

.method public startAmlMiuiWifiService(Lcom/android/server/utils/TimingsTraceAndSlog;Landroid/content/Context;)V
    .locals 3
    .param p1, "t"    # Lcom/android/server/utils/TimingsTraceAndSlog;
    .param p2, "context"    # Landroid/content/Context;

    .line 520
    const-string v0, "MiuiWifiService"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 521
    const-class v0, Lcom/android/server/SystemServiceManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/SystemServiceManager;

    .line 522
    .local v0, "manager":Lcom/android/server/SystemServiceManager;
    const-string v1, "AmlMiuiWifiService"

    invoke-virtual {p1, v1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceBegin(Ljava/lang/String;)V

    .line 523
    const-string v1, "com.android.server.wifi.AmlMiuiWifiService"

    const-string v2, "/apex/com.android.wifi/javalib/service-wifi.jar"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/SystemServiceManager;->startServiceFromJar(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 525
    invoke-virtual {p1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceEnd()V

    .line 527
    .end local v0    # "manager":Lcom/android/server/SystemServiceManager;
    :cond_0
    return-void
.end method

.method public startAmlSlaveWifiService(Lcom/android/server/utils/TimingsTraceAndSlog;Landroid/content/Context;)V
    .locals 3
    .param p1, "t"    # Lcom/android/server/utils/TimingsTraceAndSlog;
    .param p2, "context"    # Landroid/content/Context;

    .line 530
    const-string v0, "SlaveWifiService"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 531
    const-class v0, Lcom/android/server/SystemServiceManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/SystemServiceManager;

    .line 532
    .local v0, "manager":Lcom/android/server/SystemServiceManager;
    const-string v1, "AmlSlaveWifiService"

    invoke-virtual {p1, v1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceBegin(Ljava/lang/String;)V

    .line 533
    const-string v1, "com.android.server.wifi.AmlSlaveWifiService"

    const-string v2, "/apex/com.android.wifi/javalib/service-wifi.jar"

    invoke-virtual {v0, v1, v2}, Lcom/android/server/SystemServiceManager;->startServiceFromJar(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 535
    invoke-virtual {p1}, Lcom/android/server/utils/TimingsTraceAndSlog;->traceEnd()V

    .line 537
    .end local v0    # "manager":Lcom/android/server/SystemServiceManager;
    :cond_0
    return-void
.end method
