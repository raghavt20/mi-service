.class public Lcom/android/server/input/fling/FlingTracker;
.super Ljava/lang/Object;
.source "FlingTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;
    }
.end annotation


# static fields
.field private static final DOWN_NUMBER:Ljava/lang/String; = "down_number"

.field private static final FLING_NUMBER:Ljava/lang/String; = "fling_number"

.field private static final MAX_ARRAY_LEN:I = 0x2

.field private static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field private static final REPORT_INTERVAL:I = 0x14

.field private static final TAG:Ljava/lang/String; = "FlingTracker"


# instance fields
.field private DIR_NAME:Ljava/lang/String;

.field private FILE_NAME:Ljava/lang/String;

.field private final INTERVAL_DAY:J

.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mContext:Landroid/content/Context;

.field private final mEventAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private mEventByDeviceId:[I

.field private mEventCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lmiui/hardware/input/overscroller/FlingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetmEventByDeviceId(Lcom/android/server/input/fling/FlingTracker;)[I
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/fling/FlingTracker;->mEventByDeviceId:[I

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/input/fling/FlingTracker;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/fling/FlingTracker;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateAlarmForTrack(Lcom/android/server/input/fling/FlingTracker;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/fling/FlingTracker;->updateAlarmForTrack()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    .line 38
    const-string v0, "FlingInfo"

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->DIR_NAME:Ljava/lang/String;

    .line 39
    const-string v0, "fling_info"

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->FILE_NAME:Ljava/lang/String;

    .line 40
    const-string v0, "debug.fling.timeout"

    const v1, 0x5265c00

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/server/input/fling/FlingTracker;->INTERVAL_DAY:J

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mLock:Ljava/lang/Object;

    .line 51
    new-instance v0, Lcom/android/server/input/fling/FlingTracker$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/fling/FlingTracker$1;-><init>(Lcom/android/server/input/fling/FlingTracker;)V

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mEventAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 71
    iput-object p1, p0, Lcom/android/server/input/fling/FlingTracker;->mContext:Landroid/content/Context;

    .line 72
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mAlarmManager:Landroid/app/AlarmManager;

    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mEventByDeviceId:[I

    .line 75
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 76
    invoke-direct {p0}, Lcom/android/server/input/fling/FlingTracker;->updateAlarmForTrack()V

    .line 77
    return-void
.end method

.method private updateAlarmForTrack()V
    .locals 8

    .line 61
    sget-boolean v0, Lmiui/hardware/input/overscroller/FlingUtil;->sDisableReport:Z

    if-eqz v0, :cond_0

    .line 62
    return-void

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/fling/FlingTracker;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v2, 0x1

    .line 65
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/server/input/fling/FlingTracker;->INTERVAL_DAY:J

    add-long/2addr v3, v5

    const-string v5, "report_fling_event"

    iget-object v6, p0, Lcom/android/server/input/fling/FlingTracker;->mEventAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 67
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v7

    .line 64
    invoke-virtual/range {v1 .. v7}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 68
    return-void
.end method


# virtual methods
.method public registerPointerEventListener()V
    .locals 2

    .line 138
    sget-boolean v0, Lmiui/hardware/input/overscroller/FlingUtil;->sDisableReport:Z

    if-eqz v0, :cond_0

    .line 139
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;

    invoke-direct {v1, p0}, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;-><init>(Lcom/android/server/input/fling/FlingTracker;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 142
    return-void
.end method

.method public reportEvent()V
    .locals 15

    .line 93
    iget-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 94
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    .line 95
    .local v1, "size":I
    if-nez v1, :cond_0

    .line 96
    monitor-exit v0

    return-void

    .line 98
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/fling/FlingTracker;->mEventByDeviceId:[I

    const/4 v3, 0x0

    aget v4, v2, v3

    const/4 v5, 0x1

    aget v2, v2, v5

    add-int/2addr v2, v4

    .line 99
    .local v2, "totalDownTimes":I
    if-eqz v2, :cond_1

    int-to-double v6, v4

    int-to-double v8, v2

    const-wide/16 v10, 0x0

    add-double/2addr v8, v10

    div-double/2addr v6, v8

    const-wide v8, 0x3fc999999999999aL    # 0.2

    cmpl-double v4, v6, v8

    if-ltz v4, :cond_1

    .line 100
    const-string v4, "FlingTracker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reportEvent: drop events from total: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", from device -1: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/input/fling/FlingTracker;->mEventByDeviceId:[I

    aget v3, v6, v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    monitor-exit v0

    return-void

    .line 104
    :cond_1
    const/4 v4, 0x0

    .line 105
    .local v4, "totalFlingTimes":I
    const/4 v6, 0x0

    .line 106
    .local v6, "nums":I
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 107
    .local v7, "arr":Lorg/json/JSONArray;
    iget-object v8, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 108
    .local v9, "pkgName":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    .line 109
    iget-object v10, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lmiui/hardware/input/overscroller/FlingInfo;

    .line 110
    .local v10, "info":Lmiui/hardware/input/overscroller/FlingInfo;
    iget-object v11, v10, Lmiui/hardware/input/overscroller/FlingInfo;->mEventTimes:[I

    aget v11, v11, v3

    .line 111
    .local v11, "downTimes":I
    iget-object v12, v10, Lmiui/hardware/input/overscroller/FlingInfo;->mEventTimes:[I

    aget v12, v12, v5

    .line 112
    .local v12, "flingTimes":I
    add-int/2addr v4, v12

    .line 113
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    .local v13, "obj":Lorg/json/JSONObject;
    :try_start_1
    const-string v14, "package_name"

    invoke-virtual {v13, v14, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 116
    const-string v14, "down_number"

    invoke-virtual {v13, v14, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 117
    const-string v14, "fling_number"

    invoke-virtual {v13, v14, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 118
    invoke-virtual {v7, v13}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    goto :goto_1

    .line 119
    :catch_0
    move-exception v14

    .line 120
    .local v14, "e":Lorg/json/JSONException;
    :try_start_2
    invoke-virtual {v14}, Lorg/json/JSONException;->printStackTrace()V

    .line 123
    .end local v14    # "e":Lorg/json/JSONException;
    :goto_1
    rem-int/lit8 v14, v6, 0x14

    if-eqz v14, :cond_2

    if-ne v6, v1, :cond_3

    .line 124
    :cond_2
    iget-object v14, p0, Lcom/android/server/input/fling/FlingTracker;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/android/server/input/fling/FlingOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/fling/FlingOneTrackHelper;

    move-result-object v14

    invoke-virtual {v14, v7}, Lcom/android/server/input/fling/FlingOneTrackHelper;->trackAppFlingEvent(Lorg/json/JSONArray;)V

    .line 125
    new-instance v14, Lorg/json/JSONArray;

    invoke-direct {v14}, Lorg/json/JSONArray;-><init>()V

    move-object v7, v14

    .line 127
    :cond_3
    if-ne v6, v1, :cond_4

    .line 128
    iget-object v14, p0, Lcom/android/server/input/fling/FlingTracker;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/android/server/input/fling/FlingOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/fling/FlingOneTrackHelper;

    move-result-object v14

    invoke-virtual {v14, v2, v4}, Lcom/android/server/input/fling/FlingOneTrackHelper;->trackAllAppFlingEvent(II)V

    .line 131
    .end local v9    # "pkgName":Ljava/lang/String;
    .end local v10    # "info":Lmiui/hardware/input/overscroller/FlingInfo;
    .end local v11    # "downTimes":I
    .end local v12    # "flingTimes":I
    .end local v13    # "obj":Lorg/json/JSONObject;
    :cond_4
    goto :goto_0

    .line 132
    :cond_5
    iget-object v5, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 133
    iget-object v5, p0, Lcom/android/server/input/fling/FlingTracker;->mEventByDeviceId:[I

    invoke-static {v5, v3}, Ljava/util/Arrays;->fill([II)V

    .line 134
    .end local v1    # "size":I
    .end local v2    # "totalDownTimes":I
    .end local v4    # "totalFlingTimes":I
    .end local v6    # "nums":I
    .end local v7    # "arr":Lorg/json/JSONArray;
    monitor-exit v0

    .line 135
    return-void

    .line 134
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public trackEvent(Ljava/lang/String;II)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "downTimes"    # I
    .param p3, "flingTimes"    # I

    .line 80
    sget-boolean v0, Lmiui/hardware/input/overscroller/FlingUtil;->sDisableReport:Z

    if-eqz v0, :cond_0

    .line 81
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/fling/FlingTracker;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 84
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    new-instance v2, Lmiui/hardware/input/overscroller/FlingInfo;

    invoke-direct {v2, p1}, Lmiui/hardware/input/overscroller/FlingInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/hardware/input/overscroller/FlingInfo;

    iget-object v1, v1, Lmiui/hardware/input/overscroller/FlingInfo;->mEventTimes:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    add-int/2addr v3, p2

    aput v3, v1, v2

    .line 88
    iget-object v1, p0, Lcom/android/server/input/fling/FlingTracker;->mEventCount:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/hardware/input/overscroller/FlingInfo;

    iget-object v1, v1, Lmiui/hardware/input/overscroller/FlingInfo;->mEventTimes:[I

    const/4 v2, 0x1

    aget v3, v1, v2

    add-int/2addr v3, p3

    aput v3, v1, v2

    .line 89
    monitor-exit v0

    .line 90
    return-void

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
