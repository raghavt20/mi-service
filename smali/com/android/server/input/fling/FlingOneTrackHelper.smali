.class public Lcom/android/server/input/fling/FlingOneTrackHelper;
.super Ljava/lang/Object;
.source "FlingOneTrackHelper.java"


# static fields
.field public static final ALL_APP_DOWN_NUMBER:Ljava/lang/String; = "all_app_down_number"

.field public static final ALL_APP_FLING_NUMBER:Ljava/lang/String; = "all_app_fling_number"

.field private static final ALL_APP_FLING_TRACK_TIP:Ljava/lang/String; = "1257.0.0.0.31520"

.field public static final APP_FLING_INFO:Ljava/lang/String; = "app_fling_info"

.field private static final APP_FLING_TRACK_TIP:Ljava/lang/String; = "1257.0.0.0.31519"

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final FLING_TRACK_TIP:Ljava/lang/String; = "tip"

.field public static final STATISTICS_TRACK_TYPE:Ljava/lang/String; = "statistics"

.field private static final TAG:Ljava/lang/String; = "FlingOneTrackHelper"

.field private static volatile sInstance:Lcom/android/server/input/fling/FlingOneTrackHelper;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/android/server/input/fling/FlingOneTrackHelper;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/fling/FlingOneTrackHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 35
    sget-object v0, Lcom/android/server/input/fling/FlingOneTrackHelper;->sInstance:Lcom/android/server/input/fling/FlingOneTrackHelper;

    if-nez v0, :cond_1

    .line 36
    const-class v0, Lcom/android/server/input/fling/FlingOneTrackHelper;

    monitor-enter v0

    .line 37
    :try_start_0
    sget-object v1, Lcom/android/server/input/fling/FlingOneTrackHelper;->sInstance:Lcom/android/server/input/fling/FlingOneTrackHelper;

    if-nez v1, :cond_0

    .line 38
    new-instance v1, Lcom/android/server/input/fling/FlingOneTrackHelper;

    invoke-direct {v1, p0}, Lcom/android/server/input/fling/FlingOneTrackHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/fling/FlingOneTrackHelper;->sInstance:Lcom/android/server/input/fling/FlingOneTrackHelper;

    .line 40
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 42
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/fling/FlingOneTrackHelper;->sInstance:Lcom/android/server/input/fling/FlingOneTrackHelper;

    return-object v0
.end method

.method private getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "tip"    # Ljava/lang/String;

    .line 46
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 48
    .local v0, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 49
    const-string/jumbo v1, "tip"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    goto :goto_0

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 53
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method


# virtual methods
.method public trackAllAppFlingEvent(II)V
    .locals 3
    .param p1, "downTimes"    # I
    .param p2, "flingTimes"    # I

    .line 67
    const-string/jumbo v0, "statistics"

    const-string v1, "1257.0.0.0.31520"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/fling/FlingOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 69
    .local v0, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "all_app_down_number"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 70
    const-string v1, "all_app_fling_number"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 71
    iget-object v1, p0, Lcom/android/server/input/fling/FlingOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackFlingEvent(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    goto :goto_0

    .line 72
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 75
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-void
.end method

.method public trackAppFlingEvent(Lorg/json/JSONArray;)V
    .locals 3
    .param p1, "array"    # Lorg/json/JSONArray;

    .line 57
    const-string/jumbo v0, "statistics"

    const-string v1, "1257.0.0.0.31519"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/fling/FlingOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 59
    .local v0, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "app_fling_info"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    goto :goto_0

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 63
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/fling/FlingOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackFlingEvent(Ljava/lang/String;)V

    .line 64
    return-void
.end method
