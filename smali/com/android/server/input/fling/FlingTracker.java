public class com.android.server.input.fling.FlingTracker {
	 /* .source "FlingTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/fling/FlingTracker$TrackMotionListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DOWN_NUMBER;
private static final java.lang.String FLING_NUMBER;
private static final Integer MAX_ARRAY_LEN;
private static final java.lang.String PACKAGE_NAME;
private static final Integer REPORT_INTERVAL;
private static final java.lang.String TAG;
/* # instance fields */
private java.lang.String DIR_NAME;
private java.lang.String FILE_NAME;
private final Long INTERVAL_DAY;
private final android.app.AlarmManager mAlarmManager;
private final android.content.Context mContext;
private final android.app.AlarmManager$OnAlarmListener mEventAlarmListener;
private mEventByDeviceId;
private java.util.HashMap mEventCount;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lmiui/hardware/input/overscroller/FlingInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mLock;
/* # direct methods */
static -$$Nest$fgetmEventByDeviceId ( com.android.server.input.fling.FlingTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mEventByDeviceId;
} // .end method
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.input.fling.FlingTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static void -$$Nest$mupdateAlarmForTrack ( com.android.server.input.fling.FlingTracker p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/fling/FlingTracker;->updateAlarmForTrack()V */
return;
} // .end method
public com.android.server.input.fling.FlingTracker ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 70 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 37 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mEventCount = v0;
/* .line 38 */
final String v0 = "FlingInfo"; // const-string v0, "FlingInfo"
this.DIR_NAME = v0;
/* .line 39 */
final String v0 = "fling_info"; // const-string v0, "fling_info"
this.FILE_NAME = v0;
/* .line 40 */
final String v0 = "debug.fling.timeout"; // const-string v0, "debug.fling.timeout"
/* const v1, 0x5265c00 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* int-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/input/fling/FlingTracker;->INTERVAL_DAY:J */
/* .line 42 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 51 */
/* new-instance v0, Lcom/android/server/input/fling/FlingTracker$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/fling/FlingTracker$1;-><init>(Lcom/android/server/input/fling/FlingTracker;)V */
this.mEventAlarmListener = v0;
/* .line 71 */
this.mContext = p1;
/* .line 72 */
final String v0 = "alarm"; // const-string v0, "alarm"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 74 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [I */
this.mEventByDeviceId = v0;
/* .line 75 */
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 76 */
/* invoke-direct {p0}, Lcom/android/server/input/fling/FlingTracker;->updateAlarmForTrack()V */
/* .line 77 */
return;
} // .end method
private void updateAlarmForTrack ( ) {
/* .locals 8 */
/* .line 61 */
/* sget-boolean v0, Lmiui/hardware/input/overscroller/FlingUtil;->sDisableReport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 62 */
return;
/* .line 64 */
} // :cond_0
v1 = this.mAlarmManager;
int v2 = 1; // const/4 v2, 0x1
/* .line 65 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* iget-wide v5, p0, Lcom/android/server/input/fling/FlingTracker;->INTERVAL_DAY:J */
/* add-long/2addr v3, v5 */
final String v5 = "report_fling_event"; // const-string v5, "report_fling_event"
v6 = this.mEventAlarmListener;
/* .line 67 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* .line 64 */
/* invoke-virtual/range {v1 ..v7}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 68 */
return;
} // .end method
/* # virtual methods */
public void registerPointerEventListener ( ) {
/* .locals 2 */
/* .line 138 */
/* sget-boolean v0, Lmiui/hardware/input/overscroller/FlingUtil;->sDisableReport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 139 */
return;
/* .line 141 */
} // :cond_0
v0 = this.mContext;
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( v0 );
/* new-instance v1, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;-><init>(Lcom/android/server/input/fling/FlingTracker;)V */
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).registerPointerEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 142 */
return;
} // .end method
public void reportEvent ( ) {
/* .locals 15 */
/* .line 93 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 94 */
try { // :try_start_0
v1 = this.mEventCount;
v1 = (( java.util.HashMap ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->size()I
/* .line 95 */
/* .local v1, "size":I */
/* if-nez v1, :cond_0 */
/* .line 96 */
/* monitor-exit v0 */
return;
/* .line 98 */
} // :cond_0
v2 = this.mEventByDeviceId;
int v3 = 0; // const/4 v3, 0x0
/* aget v4, v2, v3 */
int v5 = 1; // const/4 v5, 0x1
/* aget v2, v2, v5 */
/* add-int/2addr v2, v4 */
/* .line 99 */
/* .local v2, "totalDownTimes":I */
if ( v2 != null) { // if-eqz v2, :cond_1
/* int-to-double v6, v4 */
/* int-to-double v8, v2 */
/* const-wide/16 v10, 0x0 */
/* add-double/2addr v8, v10 */
/* div-double/2addr v6, v8 */
/* const-wide v8, 0x3fc999999999999aL # 0.2 */
/* cmpl-double v4, v6, v8 */
/* if-ltz v4, :cond_1 */
/* .line 100 */
final String v4 = "FlingTracker"; // const-string v4, "FlingTracker"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "reportEvent: drop events from total: "; // const-string v6, "reportEvent: drop events from total: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", from device -1: "; // const-string v6, ", from device -1: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mEventByDeviceId;
/* aget v3, v6, v3 */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v4,v3 );
/* .line 102 */
/* monitor-exit v0 */
return;
/* .line 104 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .line 105 */
/* .local v4, "totalFlingTimes":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 106 */
/* .local v6, "nums":I */
/* new-instance v7, Lorg/json/JSONArray; */
/* invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V */
/* .line 107 */
/* .local v7, "arr":Lorg/json/JSONArray; */
v8 = this.mEventCount;
(( java.util.HashMap ) v8 ).keySet ( ); // invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v9 = } // :goto_0
if ( v9 != null) { // if-eqz v9, :cond_5
/* check-cast v9, Ljava/lang/String; */
/* .line 108 */
/* .local v9, "pkgName":Ljava/lang/String; */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 109 */
v10 = this.mEventCount;
(( java.util.HashMap ) v10 ).get ( v9 ); // invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v10, Lmiui/hardware/input/overscroller/FlingInfo; */
/* .line 110 */
/* .local v10, "info":Lmiui/hardware/input/overscroller/FlingInfo; */
v11 = this.mEventTimes;
/* aget v11, v11, v3 */
/* .line 111 */
/* .local v11, "downTimes":I */
v12 = this.mEventTimes;
/* aget v12, v12, v5 */
/* .line 112 */
/* .local v12, "flingTimes":I */
/* add-int/2addr v4, v12 */
/* .line 113 */
/* new-instance v13, Lorg/json/JSONObject; */
/* invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 115 */
/* .local v13, "obj":Lorg/json/JSONObject; */
try { // :try_start_1
final String v14 = "package_name"; // const-string v14, "package_name"
(( org.json.JSONObject ) v13 ).put ( v14, v9 ); // invoke-virtual {v13, v14, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 116 */
final String v14 = "down_number"; // const-string v14, "down_number"
(( org.json.JSONObject ) v13 ).put ( v14, v11 ); // invoke-virtual {v13, v14, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 117 */
final String v14 = "fling_number"; // const-string v14, "fling_number"
(( org.json.JSONObject ) v13 ).put ( v14, v12 ); // invoke-virtual {v13, v14, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 118 */
(( org.json.JSONArray ) v7 ).put ( v13 ); // invoke-virtual {v7, v13}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 121 */
/* .line 119 */
/* :catch_0 */
/* move-exception v14 */
/* .line 120 */
/* .local v14, "e":Lorg/json/JSONException; */
try { // :try_start_2
	 (( org.json.JSONException ) v14 ).printStackTrace ( ); // invoke-virtual {v14}, Lorg/json/JSONException;->printStackTrace()V
	 /* .line 123 */
} // .end local v14 # "e":Lorg/json/JSONException;
} // :goto_1
/* rem-int/lit8 v14, v6, 0x14 */
if ( v14 != null) { // if-eqz v14, :cond_2
/* if-ne v6, v1, :cond_3 */
/* .line 124 */
} // :cond_2
v14 = this.mContext;
com.android.server.input.fling.FlingOneTrackHelper .getInstance ( v14 );
(( com.android.server.input.fling.FlingOneTrackHelper ) v14 ).trackAppFlingEvent ( v7 ); // invoke-virtual {v14, v7}, Lcom/android/server/input/fling/FlingOneTrackHelper;->trackAppFlingEvent(Lorg/json/JSONArray;)V
/* .line 125 */
/* new-instance v14, Lorg/json/JSONArray; */
/* invoke-direct {v14}, Lorg/json/JSONArray;-><init>()V */
/* move-object v7, v14 */
/* .line 127 */
} // :cond_3
/* if-ne v6, v1, :cond_4 */
/* .line 128 */
v14 = this.mContext;
com.android.server.input.fling.FlingOneTrackHelper .getInstance ( v14 );
(( com.android.server.input.fling.FlingOneTrackHelper ) v14 ).trackAllAppFlingEvent ( v2, v4 ); // invoke-virtual {v14, v2, v4}, Lcom/android/server/input/fling/FlingOneTrackHelper;->trackAllAppFlingEvent(II)V
/* .line 131 */
} // .end local v9 # "pkgName":Ljava/lang/String;
} // .end local v10 # "info":Lmiui/hardware/input/overscroller/FlingInfo;
} // .end local v11 # "downTimes":I
} // .end local v12 # "flingTimes":I
} // .end local v13 # "obj":Lorg/json/JSONObject;
} // :cond_4
/* .line 132 */
} // :cond_5
v5 = this.mEventCount;
(( java.util.HashMap ) v5 ).clear ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->clear()V
/* .line 133 */
v5 = this.mEventByDeviceId;
java.util.Arrays .fill ( v5,v3 );
/* .line 134 */
} // .end local v1 # "size":I
} // .end local v2 # "totalDownTimes":I
} // .end local v4 # "totalFlingTimes":I
} // .end local v6 # "nums":I
} // .end local v7 # "arr":Lorg/json/JSONArray;
/* monitor-exit v0 */
/* .line 135 */
return;
/* .line 134 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void trackEvent ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "downTimes" # I */
/* .param p3, "flingTimes" # I */
/* .line 80 */
/* sget-boolean v0, Lmiui/hardware/input/overscroller/FlingUtil;->sDisableReport:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 81 */
return;
/* .line 83 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 84 */
try { // :try_start_0
v1 = this.mEventCount;
v1 = (( java.util.HashMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 85 */
v1 = this.mEventCount;
/* new-instance v2, Lmiui/hardware/input/overscroller/FlingInfo; */
/* invoke-direct {v2, p1}, Lmiui/hardware/input/overscroller/FlingInfo;-><init>(Ljava/lang/String;)V */
(( java.util.HashMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 87 */
} // :cond_1
v1 = this.mEventCount;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/hardware/input/overscroller/FlingInfo; */
v1 = this.mEventTimes;
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v1, v2 */
/* add-int/2addr v3, p2 */
/* aput v3, v1, v2 */
/* .line 88 */
v1 = this.mEventCount;
(( java.util.HashMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/hardware/input/overscroller/FlingInfo; */
v1 = this.mEventTimes;
int v2 = 1; // const/4 v2, 0x1
/* aget v3, v1, v2 */
/* add-int/2addr v3, p3 */
/* aput v3, v1, v2 */
/* .line 89 */
/* monitor-exit v0 */
/* .line 90 */
return;
/* .line 89 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
