class com.android.server.input.fling.FlingTracker$TrackMotionListener implements com.miui.server.input.gesture.MiuiGestureListener {
	 /* .source "FlingTracker.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/fling/FlingTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "TrackMotionListener" */
} // .end annotation
/* # instance fields */
final com.android.server.input.fling.FlingTracker this$0; //synthetic
/* # direct methods */
 com.android.server.input.fling.FlingTracker$TrackMotionListener ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/fling/FlingTracker; */
/* .line 144 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 7 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 147 */
v0 = this.this$0;
com.android.server.input.fling.FlingTracker .-$$Nest$fgetmLock ( v0 );
/* monitor-enter v0 */
/* .line 148 */
try { // :try_start_0
	 v1 = 	 (( android.view.MotionEvent ) p1 ).getDeviceId ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I
	 /* .line 149 */
	 /* .local v1, "deviceId":I */
	 v2 = 	 (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
	 /* .line 150 */
	 /* .local v2, "action":I */
	 /* if-nez v2, :cond_1 */
	 /* .line 151 */
	 int v3 = -1; // const/4 v3, -0x1
	 int v4 = 1; // const/4 v4, 0x1
	 /* if-ne v1, v3, :cond_0 */
	 int v3 = 0; // const/4 v3, 0x0
} // :cond_0
/* move v3, v4 */
/* .line 152 */
/* .local v3, "index":I */
} // :goto_0
v5 = this.this$0;
com.android.server.input.fling.FlingTracker .-$$Nest$fgetmEventByDeviceId ( v5 );
/* aget v6, v5, v3 */
/* add-int/2addr v6, v4 */
/* aput v6, v5, v3 */
/* .line 154 */
} // .end local v1 # "deviceId":I
} // .end local v2 # "action":I
} // .end local v3 # "index":I
} // :cond_1
/* monitor-exit v0 */
/* .line 155 */
return;
/* .line 154 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
