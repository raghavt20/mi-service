public class com.android.server.input.fling.FlingOneTrackHelper {
	 /* .source "FlingOneTrackHelper.java" */
	 /* # static fields */
	 public static final java.lang.String ALL_APP_DOWN_NUMBER;
	 public static final java.lang.String ALL_APP_FLING_NUMBER;
	 private static final java.lang.String ALL_APP_FLING_TRACK_TIP;
	 public static final java.lang.String APP_FLING_INFO;
	 private static final java.lang.String APP_FLING_TRACK_TIP;
	 private static final java.lang.String EVENT_NAME;
	 private static final java.lang.String FLING_TRACK_TIP;
	 public static final java.lang.String STATISTICS_TRACK_TYPE;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.input.fling.FlingOneTrackHelper sInstance;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 /* # direct methods */
	 private com.android.server.input.fling.FlingOneTrackHelper ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 30 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 31 */
		 this.mContext = p1;
		 /* .line 32 */
		 return;
	 } // .end method
	 public static com.android.server.input.fling.FlingOneTrackHelper getInstance ( android.content.Context p0 ) {
		 /* .locals 2 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 35 */
		 v0 = com.android.server.input.fling.FlingOneTrackHelper.sInstance;
		 /* if-nez v0, :cond_1 */
		 /* .line 36 */
		 /* const-class v0, Lcom/android/server/input/fling/FlingOneTrackHelper; */
		 /* monitor-enter v0 */
		 /* .line 37 */
		 try { // :try_start_0
			 v1 = com.android.server.input.fling.FlingOneTrackHelper.sInstance;
			 /* if-nez v1, :cond_0 */
			 /* .line 38 */
			 /* new-instance v1, Lcom/android/server/input/fling/FlingOneTrackHelper; */
			 /* invoke-direct {v1, p0}, Lcom/android/server/input/fling/FlingOneTrackHelper;-><init>(Landroid/content/Context;)V */
			 /* .line 40 */
		 } // :cond_0
		 /* monitor-exit v0 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
		 /* .line 42 */
	 } // :cond_1
} // :goto_0
v0 = com.android.server.input.fling.FlingOneTrackHelper.sInstance;
} // .end method
private org.json.JSONObject getTrackJSONObject ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "type" # Ljava/lang/String; */
/* .param p2, "tip" # Ljava/lang/String; */
/* .line 46 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 48 */
/* .local v0, "obj":Lorg/json/JSONObject; */
try { // :try_start_0
	 final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
	 (( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 49 */
	 /* const-string/jumbo v1, "tip" */
	 (( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* :try_end_0 */
	 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 52 */
	 /* .line 50 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 51 */
	 /* .local v1, "e":Lorg/json/JSONException; */
	 (( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
	 /* .line 53 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
} // .end method
/* # virtual methods */
public void trackAllAppFlingEvent ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "downTimes" # I */
/* .param p2, "flingTimes" # I */
/* .line 67 */
/* const-string/jumbo v0, "statistics" */
final String v1 = "1257.0.0.0.31520"; // const-string v1, "1257.0.0.0.31520"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/fling/FlingOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 69 */
/* .local v0, "obj":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "all_app_down_number"; // const-string v1, "all_app_down_number"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 70 */
final String v1 = "all_app_fling_number"; // const-string v1, "all_app_fling_number"
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 71 */
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackFlingEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackFlingEvent(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 74 */
/* .line 72 */
/* :catch_0 */
/* move-exception v1 */
/* .line 73 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 75 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
return;
} // .end method
public void trackAppFlingEvent ( org.json.JSONArray p0 ) {
/* .locals 3 */
/* .param p1, "array" # Lorg/json/JSONArray; */
/* .line 57 */
/* const-string/jumbo v0, "statistics" */
final String v1 = "1257.0.0.0.31519"; // const-string v1, "1257.0.0.0.31519"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/fling/FlingOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 59 */
/* .local v0, "obj":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "app_fling_info"; // const-string v1, "app_fling_info"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 62 */
/* .line 60 */
/* :catch_0 */
/* move-exception v1 */
/* .line 61 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 63 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackFlingEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackFlingEvent(Ljava/lang/String;)V
/* .line 64 */
return;
} // .end method
