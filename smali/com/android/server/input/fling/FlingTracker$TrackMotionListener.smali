.class Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;
.super Ljava/lang/Object;
.source "FlingTracker.java"

# interfaces
.implements Lcom/miui/server/input/gesture/MiuiGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/fling/FlingTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TrackMotionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/fling/FlingTracker;


# direct methods
.method constructor <init>(Lcom/android/server/input/fling/FlingTracker;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/fling/FlingTracker;

    .line 144
    iput-object p1, p0, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;->this$0:Lcom/android/server/input/fling/FlingTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 147
    iget-object v0, p0, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;->this$0:Lcom/android/server/input/fling/FlingTracker;

    invoke-static {v0}, Lcom/android/server/input/fling/FlingTracker;->-$$Nest$fgetmLock(Lcom/android/server/input/fling/FlingTracker;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 148
    :try_start_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v1

    .line 149
    .local v1, "deviceId":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 150
    .local v2, "action":I
    if-nez v2, :cond_1

    .line 151
    const/4 v3, -0x1

    const/4 v4, 0x1

    if-ne v1, v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, v4

    .line 152
    .local v3, "index":I
    :goto_0
    iget-object v5, p0, Lcom/android/server/input/fling/FlingTracker$TrackMotionListener;->this$0:Lcom/android/server/input/fling/FlingTracker;

    invoke-static {v5}, Lcom/android/server/input/fling/FlingTracker;->-$$Nest$fgetmEventByDeviceId(Lcom/android/server/input/fling/FlingTracker;)[I

    move-result-object v5

    aget v6, v5, v3

    add-int/2addr v6, v4

    aput v6, v5, v3

    .line 154
    .end local v1    # "deviceId":I
    .end local v2    # "action":I
    .end local v3    # "index":I
    :cond_1
    monitor-exit v0

    .line 155
    return-void

    .line 154
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
