class com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver extends android.database.ContentObserver {
	 /* .source "TouchWakeUpFeatureManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/TouchWakeUpFeatureManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "SettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.input.TouchWakeUpFeatureManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$jl8_YxQh61pDTohv6DTcv8kcpvY ( com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->refreshAllSettingsInternal()V */
return;
} // .end method
public com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 103 */
this.this$0 = p1;
/* .line 104 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 105 */
return;
} // .end method
private void refreshAllSettingsInternal ( ) {
/* .locals 2 */
/* .line 125 */
final String v0 = "gesture_wakeup"; // const-string v0, "gesture_wakeup"
android.provider.Settings$System .getUriFor ( v0 );
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 127 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 128 */
	 /* const-string/jumbo v0, "subscreen_gesture_wakeup" */
	 android.provider.Settings$System .getUriFor ( v0 );
	 (( com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->onChange(ZLandroid/net/Uri;)V
	 /* .line 131 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 135 */
v0 = this.this$0;
com.android.server.input.TouchWakeUpFeatureManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 136 */
/* .local v0, "cr":Landroid/content/ContentResolver; */
final String v1 = "gesture_wakeup"; // const-string v1, "gesture_wakeup"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 137 */
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v3 = 1; // const/4 v3, 0x1
int v4 = -2; // const/4 v4, -0x2
int v5 = 0; // const/4 v5, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 138 */
	 v2 = this.this$0;
	 v1 = 	 android.provider.Settings$System .getIntForUser ( v0,v1,v5,v4 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
	 } // :cond_0
	 /* move v3, v5 */
} // :goto_0
com.android.server.input.TouchWakeUpFeatureManager .-$$Nest$monGestureWakeupSettingsChanged ( v2,v3 );
/* .line 140 */
} // :cond_1
/* const-string/jumbo v1, "subscreen_gesture_wakeup" */
android.provider.Settings$System .getUriFor ( v1 );
/* .line 141 */
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 142 */
v2 = this.this$0;
v1 = android.provider.Settings$System .getIntForUser ( v0,v1,v5,v4 );
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_2
/* move v3, v5 */
} // :goto_1
com.android.server.input.TouchWakeUpFeatureManager .-$$Nest$monSubScreenGestureWakeupSettingsChanged ( v2,v3 );
/* .line 146 */
} // :cond_3
} // :goto_2
return;
} // .end method
public void refreshAllSettings ( ) {
/* .locals 2 */
/* .line 121 */
v0 = this.this$0;
com.android.server.input.TouchWakeUpFeatureManager .-$$Nest$fgetmHandler ( v0 );
/* new-instance v1, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 122 */
return;
} // .end method
public void register ( ) {
/* .locals 4 */
/* .line 108 */
v0 = this.this$0;
com.android.server.input.TouchWakeUpFeatureManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 109 */
/* .local v0, "contentResolver":Landroid/content/ContentResolver; */
final String v1 = "gesture_wakeup"; // const-string v1, "gesture_wakeup"
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 112 */
/* sget-boolean v1, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 113 */
/* const-string/jumbo v1, "subscreen_gesture_wakeup" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 117 */
} // :cond_0
return;
} // .end method
