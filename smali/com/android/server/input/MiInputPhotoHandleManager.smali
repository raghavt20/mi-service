.class public Lcom/android/server/input/MiInputPhotoHandleManager;
.super Ljava/lang/Object;
.source "MiInputPhotoHandleManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;
    }
.end annotation


# static fields
.field private static final ACTION_HANDLE_STATE_CHANGED:Ljava/lang/String; = "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"

.field private static final EXTRA_HANDLE_CONNECT_STATE:Ljava/lang/String; = "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"

.field private static final EXTRA_HANDLE_PID:Ljava/lang/String; = "pid"

.field private static final EXTRA_HANDLE_VID:Ljava/lang/String; = "vid"

.field private static final HANDLE_STATUS_CONNECTION:I = 0x1

.field private static final HANDLE_STATUS_DISCONNECTION:I = 0x0

.field public static final PHOTO_HANDLE_HAS_BEEN_CONNECTED:Ljava/lang/String; = "photo_handle_has_been_connected"

.field private static final TAG:Ljava/lang/String; = "MiInputPhotoHandleManager"

.field private static sMiInputPhotoHandleManager:Lcom/android/server/input/MiInputPhotoHandleManager;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private mPhotoHandleConnectionStatus:I

.field private mPhotoHandleHasConnected:Z

.field private final mPhotoHandleStatusReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/input/MiInputPhotoHandleManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPhotoHandleConnectionStatus(Lcom/android/server/input/MiInputPhotoHandleManager;)I
    .locals 0

    iget p0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleConnectionStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmPhotoHandleConnectionStatus(Lcom/android/server/input/MiInputPhotoHandleManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleConnectionStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyHandleConnectStatus(Lcom/android/server/input/MiInputPhotoHandleManager;ZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/input/MiInputPhotoHandleManager;->notifyHandleConnectStatus(ZII)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance v0, Lcom/android/server/input/MiInputPhotoHandleManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/MiInputPhotoHandleManager$1;-><init>(Lcom/android/server/input/MiInputPhotoHandleManager;)V

    iput-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 63
    iput-object p1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mContext:Landroid/content/Context;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 65
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/MiInputPhotoHandleManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 54
    const-class v0, Lcom/android/server/input/MiInputPhotoHandleManager;

    monitor-enter v0

    .line 55
    :try_start_0
    sget-object v1, Lcom/android/server/input/MiInputPhotoHandleManager;->sMiInputPhotoHandleManager:Lcom/android/server/input/MiInputPhotoHandleManager;

    if-nez v1, :cond_0

    .line 56
    new-instance v1, Lcom/android/server/input/MiInputPhotoHandleManager;

    invoke-direct {v1, p0}, Lcom/android/server/input/MiInputPhotoHandleManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/MiInputPhotoHandleManager;->sMiInputPhotoHandleManager:Lcom/android/server/input/MiInputPhotoHandleManager;

    .line 58
    :cond_0
    sget-object v1, Lcom/android/server/input/MiInputPhotoHandleManager;->sMiInputPhotoHandleManager:Lcom/android/server/input/MiInputPhotoHandleManager;

    monitor-exit v0

    return-object v1

    .line 59
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private initIntentFilter()V
    .locals 4

    .line 103
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 104
    .local v0, "photoHandleFilter":Landroid/content/IntentFilter;
    const-string v1, "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleStatusReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 107
    return-void
.end method

.method private notifyHandleConnectStatus(ZII)V
    .locals 4
    .param p1, "connection"    # Z
    .param p2, "pid"    # I
    .param p3, "vid"    # I

    .line 118
    invoke-virtual {p0, p2, p3}, Lcom/android/server/input/MiInputPhotoHandleManager;->getPhotoHandleDeviceId(II)I

    move-result v0

    .line 119
    .local v0, "deviceId":I
    iget-object v1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/input/MiuiInputManagerInternal;->notifyPhotoHandleConnectionStatus(ZI)V

    .line 120
    invoke-static {}, Lcom/android/server/policy/PhoneWindowManagerStub;->getInstance()Lcom/android/server/policy/PhoneWindowManagerStub;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/android/server/policy/PhoneWindowManagerStub;->notifyPhotoHandleConnectionStatus(ZI)V

    .line 122
    iget-boolean v1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z

    if-nez v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "photo_handle_has_been_connected"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 124
    iput-boolean v3, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z

    .line 126
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyHandleConnectStatus, connection="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiInputPhotoHandleManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return-void
.end method


# virtual methods
.method public getPhotoHandleDeviceId(II)I
    .locals 8
    .param p1, "pid"    # I
    .param p2, "vid"    # I

    .line 84
    iget-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mContext:Landroid/content/Context;

    const-class v1, Landroid/hardware/input/InputManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    .line 85
    .local v0, "inputManager":Landroid/hardware/input/InputManager;
    const-string v1, "MiInputPhotoHandleManager"

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_2

    aget v5, v2, v4

    .line 87
    .local v5, "deviceId":I
    invoke-virtual {v0, v5}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v6

    .line 88
    .local v6, "inputDevice":Landroid/view/InputDevice;
    invoke-virtual {v6}, Landroid/view/InputDevice;->getProductId()I

    move-result v7

    if-ne v7, p1, :cond_0

    invoke-virtual {v6}, Landroid/view/InputDevice;->getVendorId()I

    move-result v7

    if-ne v7, p2, :cond_0

    .line 89
    return v5

    .line 86
    .end local v5    # "deviceId":I
    .end local v6    # "inputDevice":Landroid/view/InputDevice;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 93
    :cond_1
    const-string v2, "inputManager is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_2
    const-string v2, "not found deviceId"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v1, -0x1

    return v1
.end method

.method public init()V
    .locals 3

    .line 68
    const-string v0, "MiInputPhotoHandleManager"

    const-string v1, "init photo handle manager"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 70
    new-instance v0, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;-><init>(Lcom/android/server/input/MiInputPhotoHandleManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mHandler:Landroid/os/Handler;

    .line 71
    invoke-direct {p0}, Lcom/android/server/input/MiInputPhotoHandleManager;->initIntentFilter()V

    .line 72
    iget-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "photo_handle_has_been_connected"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z

    .line 74
    return-void
.end method

.method public isPhotoHandleHasConnected()Z
    .locals 1

    .line 131
    iget-boolean v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z

    return v0
.end method
