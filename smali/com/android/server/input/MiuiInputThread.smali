.class public final Lcom/android/server/input/MiuiInputThread;
.super Landroid/os/HandlerThread;
.source "MiuiInputThread.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiInputThread"

.field private static volatile sHandler:Landroid/os/Handler;

.field private static volatile sInstance:Lcom/android/server/input/MiuiInputThread;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 16
    const-string v0, "miui_input_thread"

    const/4 v1, -0x4

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 17
    const-string v0, "MiuiInputThread"

    const-string v1, "Shared singleton Thread for input services start"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 18
    return-void
.end method

.method private static ensureThreadLocked()V
    .locals 2

    .line 21
    sget-object v0, Lcom/android/server/input/MiuiInputThread;->sInstance:Lcom/android/server/input/MiuiInputThread;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/android/server/input/MiuiInputThread;

    invoke-direct {v0}, Lcom/android/server/input/MiuiInputThread;-><init>()V

    sput-object v0, Lcom/android/server/input/MiuiInputThread;->sInstance:Lcom/android/server/input/MiuiInputThread;

    .line 23
    sget-object v0, Lcom/android/server/input/MiuiInputThread;->sInstance:Lcom/android/server/input/MiuiInputThread;

    invoke-virtual {v0}, Lcom/android/server/input/MiuiInputThread;->start()V

    .line 24
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/android/server/input/MiuiInputThread;->sInstance:Lcom/android/server/input/MiuiInputThread;

    invoke-virtual {v1}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/android/server/input/MiuiInputThread;->sHandler:Landroid/os/Handler;

    .line 26
    :cond_0
    return-void
.end method

.method public static getHandler()Landroid/os/Handler;
    .locals 2

    .line 36
    const-class v0, Lcom/android/server/input/MiuiInputThread;

    monitor-enter v0

    .line 37
    :try_start_0
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->ensureThreadLocked()V

    .line 38
    sget-object v1, Lcom/android/server/input/MiuiInputThread;->sHandler:Landroid/os/Handler;

    monitor-exit v0

    return-object v1

    .line 39
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getThread()Lcom/android/server/input/MiuiInputThread;
    .locals 2

    .line 29
    const-class v0, Lcom/android/server/input/MiuiInputThread;

    monitor-enter v0

    .line 30
    :try_start_0
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->ensureThreadLocked()V

    .line 31
    sget-object v1, Lcom/android/server/input/MiuiInputThread;->sInstance:Lcom/android/server/input/MiuiInputThread;

    monitor-exit v0

    return-object v1

    .line 32
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
