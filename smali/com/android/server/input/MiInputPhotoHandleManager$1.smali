.class Lcom/android/server/input/MiInputPhotoHandleManager$1;
.super Landroid/content/BroadcastReceiver;
.source "MiInputPhotoHandleManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiInputPhotoHandleManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/MiInputPhotoHandleManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/MiInputPhotoHandleManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/MiInputPhotoHandleManager;

    .line 137
    iput-object p1, p0, Lcom/android/server/input/MiInputPhotoHandleManager$1;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 140
    const-string v0, "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 141
    .local v0, "connectionStatus":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive handle status,connection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiInputPhotoHandleManager"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v2, p0, Lcom/android/server/input/MiInputPhotoHandleManager$1;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    invoke-static {v2}, Lcom/android/server/input/MiInputPhotoHandleManager;->-$$Nest$fgetmPhotoHandleConnectionStatus(Lcom/android/server/input/MiInputPhotoHandleManager;)I

    move-result v2

    if-eq v2, v0, :cond_0

    .line 144
    iget-object v2, p0, Lcom/android/server/input/MiInputPhotoHandleManager$1;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    invoke-static {v2, v0}, Lcom/android/server/input/MiInputPhotoHandleManager;->-$$Nest$fputmPhotoHandleConnectionStatus(Lcom/android/server/input/MiInputPhotoHandleManager;I)V

    .line 145
    const-string v2, "pid"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 146
    .local v2, "pid":I
    const-string/jumbo v3, "vid"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 147
    .local v1, "vid":I
    iget-object v3, p0, Lcom/android/server/input/MiInputPhotoHandleManager$1;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    invoke-static {v3}, Lcom/android/server/input/MiInputPhotoHandleManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/MiInputPhotoHandleManager;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v3, v0, v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    .line 148
    .local v3, "message":Landroid/os/Message;
    iget-object v4, p0, Lcom/android/server/input/MiInputPhotoHandleManager$1;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    invoke-static {v4}, Lcom/android/server/input/MiInputPhotoHandleManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/MiInputPhotoHandleManager;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 151
    .end local v1    # "vid":I
    .end local v2    # "pid":I
    .end local v3    # "message":Landroid/os/Message;
    :cond_0
    return-void
.end method
