.class Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "TouchWakeUpFeatureManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/TouchWakeUpFeatureManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;


# direct methods
.method public static synthetic $r8$lambda$jl8_YxQh61pDTohv6DTcv8kcpvY(Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->refreshAllSettingsInternal()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/input/TouchWakeUpFeatureManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 103
    iput-object p1, p0, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;

    .line 104
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 105
    return-void
.end method

.method private refreshAllSettingsInternal()V
    .locals 2

    .line 125
    const-string v0, "gesture_wakeup"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 127
    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    .line 128
    const-string/jumbo v0, "subscreen_gesture_wakeup"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 135
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;

    invoke-static {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager;->-$$Nest$fgetmContext(Lcom/android/server/input/TouchWakeUpFeatureManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 136
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "gesture_wakeup"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 137
    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, -0x2

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    .line 138
    iget-object v2, p0, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;

    invoke-static {v0, v1, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v3, v5

    :goto_0
    invoke-static {v2, v3}, Lcom/android/server/input/TouchWakeUpFeatureManager;->-$$Nest$monGestureWakeupSettingsChanged(Lcom/android/server/input/TouchWakeUpFeatureManager;Z)V

    goto :goto_2

    .line 140
    :cond_1
    const-string/jumbo v1, "subscreen_gesture_wakeup"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 141
    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 142
    iget-object v2, p0, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;

    invoke-static {v0, v1, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    :goto_1
    invoke-static {v2, v3}, Lcom/android/server/input/TouchWakeUpFeatureManager;->-$$Nest$monSubScreenGestureWakeupSettingsChanged(Lcom/android/server/input/TouchWakeUpFeatureManager;Z)V

    .line 146
    :cond_3
    :goto_2
    return-void
.end method

.method public refreshAllSettings()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;

    invoke-static {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/TouchWakeUpFeatureManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 122
    return-void
.end method

.method public register()V
    .locals 4

    .line 108
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->this$0:Lcom/android/server/input/TouchWakeUpFeatureManager;

    invoke-static {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager;->-$$Nest$fgetmContext(Lcom/android/server/input/TouchWakeUpFeatureManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 109
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string v1, "gesture_wakeup"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 112
    sget-boolean v1, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v1, :cond_0

    .line 113
    const-string/jumbo v1, "subscreen_gesture_wakeup"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 117
    :cond_0
    return-void
.end method
