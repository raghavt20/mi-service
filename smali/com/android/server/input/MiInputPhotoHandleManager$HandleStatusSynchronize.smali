.class Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;
.super Landroid/os/Handler;
.source "MiInputPhotoHandleManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiInputPhotoHandleManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandleStatusSynchronize"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/MiInputPhotoHandleManager;


# direct methods
.method public constructor <init>(Lcom/android/server/input/MiInputPhotoHandleManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 158
    iput-object p1, p0, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    .line 159
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 160
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 164
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    const/4 v3, 0x1

    invoke-static {v0, v3, v1, v2}, Lcom/android/server/input/MiInputPhotoHandleManager;->-$$Nest$mnotifyHandleConnectStatus(Lcom/android/server/input/MiInputPhotoHandleManager;ZII)V

    .line 170
    goto :goto_0

    .line 166
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;->this$0:Lcom/android/server/input/MiInputPhotoHandleManager;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2}, Lcom/android/server/input/MiInputPhotoHandleManager;->-$$Nest$mnotifyHandleConnectStatus(Lcom/android/server/input/MiInputPhotoHandleManager;ZII)V

    .line 167
    nop

    .line 174
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
