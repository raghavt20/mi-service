.class Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
.super Ljava/lang/Object;
.source "IICNodeHelper.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CommandWorker"
.end annotation


# instance fields
.field private final mCommandQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "[B>;"
        }
    .end annotation
.end field

.field private mDoingCommand:[B

.field private mIsRunning:Z

.field private mReceiverPackageIndex:I

.field private mResponse:[B

.field private mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

.field private mTargetAddress:B

.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# direct methods
.method public static synthetic $r8$lambda$NsEEhp0ymux3CDK2bgPZ9ZKBFbM(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->lambda$sendCommand$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->resetWorker()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
    .param p2, "listener"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    .line 1213
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1200
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mTargetAddress:B

    .line 1201
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mCommandQueue:Ljava/util/Queue;

    .line 1204
    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponse:[B

    .line 1214
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    .line 1215
    return-void
.end method

.method private synthetic lambda$sendCommand$0()V
    .locals 2

    .line 1239
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mDoingCommand:[B

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithIIC([B)V

    return-void
.end method

.method private resetWorker()V
    .locals 3

    .line 1305
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I

    .line 1306
    const/16 v1, 0x42

    new-array v2, v1, [B

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mDoingCommand:[B

    .line 1307
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    .line 1308
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1309
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponse:[B

    .line 1310
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v0

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->removeMessages(I)V

    .line 1312
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1316
    const/4 v0, 0x0

    return v0
.end method

.method public emptyCommandResponse()V
    .locals 1

    .line 1301
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;->onCommandResponse()V

    .line 1302
    return-void
.end method

.method public getCommandResponse()[B
    .locals 1

    .line 1284
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponse:[B

    return-object v0
.end method

.method public getCommandSize()I
    .locals 1

    .line 1230
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    return v0
.end method

.method public getReceiverPackageIndex()I
    .locals 1

    .line 1280
    iget v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I

    return v0
.end method

.method public getTargetAddress()B
    .locals 1

    .line 1222
    iget-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mTargetAddress:B

    return v0
.end method

.method public insertCommandToQueue([B)V
    .locals 1
    .param p1, "command"    # [B

    .line 1210
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mCommandQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 1211
    return-void
.end method

.method public isWorkRunning()Z
    .locals 1

    .line 1226
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    return v0
.end method

.method public notResponseException()V
    .locals 2

    .line 1252
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    if-nez v0, :cond_0

    .line 1253
    return-void

    .line 1255
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    const-string v1, "Time Out!"

    invoke-interface {v0, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onWriteSocketErrorInfo(Ljava/lang/String;)V

    .line 1256
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->resetWorker()V

    .line 1257
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;->onCommandResponse()V

    .line 1258
    return-void
.end method

.method public onWorkException([B)V
    .locals 4
    .param p1, "response"    # [B

    .line 1288
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    if-nez v0, :cond_0

    .line 1289
    return-void

    .line 1291
    :cond_0
    const/4 v0, 0x4

    aget-byte v1, p1, v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mDoingCommand:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    if-eq v1, v2, :cond_1

    aget-byte v0, p1, v0

    const/16 v1, -0x6f

    if-ne v0, v1, :cond_2

    const/16 v0, 0x11

    if-ne v2, v0, :cond_2

    .line 1295
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->resetWorker()V

    .line 1296
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;->onCommandResponse()V

    .line 1298
    :cond_2
    return-void
.end method

.method public sendCommand(I)V
    .locals 5
    .param p1, "repeatTimes"    # I

    .line 1234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    .line 1235
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mCommandQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mDoingCommand:[B

    .line 1236
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v0

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->removeMessages(I)V

    .line 1238
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v0

    new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z

    .line 1240
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1241
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "command"

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mDoingCommand:[B

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 1242
    const-string/jumbo v2, "worker"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1243
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 1245
    .local v1, "msg2":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 1246
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1247
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v2

    const-wide/16 v3, 0x5dc

    invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1249
    return-void
.end method

.method public setTargetAddress(B)V
    .locals 0
    .param p1, "address"    # B

    .line 1218
    iput-byte p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mTargetAddress:B

    .line 1219
    return-void
.end method

.method public triggerResponse([B)V
    .locals 3
    .param p1, "response"    # [B

    .line 1261
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    if-nez v0, :cond_0

    .line 1262
    return-void

    .line 1264
    :cond_0
    const/4 v0, 0x4

    aget-byte v0, p1, v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mDoingCommand:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_1

    .line 1265
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponse:[B

    .line 1266
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;->onCommandResponse()V

    .line 1268
    :cond_1
    return-void
.end method

.method public triggerResponseForPackage([BI)V
    .locals 1
    .param p1, "response"    # [B
    .param p2, "index"    # I

    .line 1271
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z

    if-nez v0, :cond_0

    .line 1272
    return-void

    .line 1274
    :cond_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponse:[B

    .line 1275
    iput p2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I

    .line 1276
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mResponseListener:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;->onCommandResponse()V

    .line 1277
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 1321
    iget v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1322
    return-void
.end method
