.class Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;
.super Ljava/lang/Object;
.source "IICNodeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 285
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 288
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "vendor/etc/MCU_Upgrade.bin"

    invoke-direct {v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;)V

    .line 290
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->isValidFile()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    const/16 v1, 0x18

    const-string v2, "Invalid upgrade file, with vendor/etc/MCU_Upgrade.bin"

    invoke-interface {v0, v1, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V

    .line 295
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msupportUpgradeMCU(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V

    .line 300
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getUpgradeCommand()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 305
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPackInfo(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 307
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    .line 308
    return-void
.end method
