public class com.android.server.input.padkeyboard.iic.IICNodeHelper {
	 /* .source "IICNodeHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;, */
	 /* Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;, */
	 /* Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;, */
	 /* Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BUFFER_SIZE;
private static final java.lang.String KEYBOARD_COLOR_BLACK;
private static final java.lang.String KEYBOARD_COLOR_WHITE;
private static final java.lang.String KEYBOARD_INFO_CHANGED;
private static final java.lang.String TAG;
private static final java.lang.String UPGRADE_FOOT_LOCATION;
private static final java.lang.String UPGRADE_HEAD_LOCATION_ODM;
private static final java.lang.String UPGRADE_HEAD_LOCATION_VENDOR;
private static volatile com.android.server.input.padkeyboard.iic.IICNodeHelper sInstance;
/* # instance fields */
private final java.util.List mAllWorkerList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private mAuthCommandResponse;
private final com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager mBlueToothKeyboardManager;
private final com.android.server.input.padkeyboard.iic.CommunicationUtil mCommunicationUtil;
private final android.content.Context mContext;
private java.util.concurrent.CountDownLatch mCountDownLatch;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker mEmptyCommandWorker;
private volatile Boolean mIsBinDataOK;
private Boolean mIsBleKeyboard;
private final java.lang.StringBuilder mKeyBoardUpgradeFilePath;
private com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil mKeyBoardUpgradeUtil;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker mKeyUpgradeCommandWorker;
private java.lang.String mKeyboardBleAdd;
private volatile java.lang.String mKeyboardFlashVersion;
private Object mKeyboardType;
private volatile java.lang.String mKeyboardVersion;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker mKeyboardVersionWorker;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker mMCUUpgradeCommandWorker;
private final java.lang.StringBuilder mMCUUpgradeFilePath;
private volatile java.lang.String mMCUVersion;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker mMCUVersionWorker;
private com.android.server.input.padkeyboard.iic.McuUpgradeUtil mMcuUpgradeUtil;
public com.android.server.input.padkeyboard.iic.NanoSocketCallback mNanoSocketCallback;
private final java.util.Queue mReadyRunningTask;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "Ljava/lang/Runnable;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private volatile Integer mReceiverBinIndex;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker mTouchPadCommandWorker;
private Object mTouchPadType;
private final java.lang.StringBuilder mTouchPadUpgradeFilePath;
private com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil mTouchPadUpgradeUtil;
private volatile java.lang.String mTouchPadVersion;
private final com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler mUpgradeCommandHandler;
private final java.lang.Runnable mUpgradeKeyboardRunnable;
private final java.lang.Runnable mUpgradeMCURunnable;
private Boolean mUpgradeNoCheck;
private final java.lang.Runnable mUpgradeTouchPadRunnable;
/* # direct methods */
public static void $r8$lambda$-baMklQkxU-V4KZpeu37UZXxGDo ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetKeyboardVersionCommand$1()V */
return;
} // .end method
public static void $r8$lambda$2JzDxttHMD4HvOwHZ8gU8FC63ws ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetMCUVersionCommand$3()V */
return;
} // .end method
public static void $r8$lambda$3cAj_zHyyEhbYdv7Qvwn9X1h2Vs ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$upgradeTouchPadIfNeed$6()V */
return;
} // .end method
public static void $r8$lambda$4lTSqpkvxTOqx711lFuw-bKq9EQ ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetKeyboardVersionCommand$0()V */
return;
} // .end method
public static void $r8$lambda$F24BjgTvKRUX19S3HaM1jAtp-PI ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$9()V */
return;
} // .end method
public static void $r8$lambda$GKCgSjb6O-X9ZdtyXMIM_ZasKE0 ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$7()V */
return;
} // .end method
public static void $r8$lambda$MO_rQN50uQ5-fj8DPzT9af0cu0w ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$8()V */
return;
} // .end method
public static void $r8$lambda$RfS-IzdnOwVMvmM7C922hZxflWg ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$11()V */
return;
} // .end method
public static void $r8$lambda$sqsLJ7RANDz-l8GIFqoZ7BHg558 ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$10()V */
return;
} // .end method
public static void $r8$lambda$y4skuJNpvhCDwvKA0v4TvysZ-p4 ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$upgradeKeyboardIfNeed$5()V */
return;
} // .end method
public static void $r8$lambda$zF0YCQmz2h7ZznkntegoUAkV7Go ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$startUpgradeMCUIfNeed$4()V */
return;
} // .end method
public static void $r8$lambda$zo-G4JZLp0S9Qrnc6EM4MLdg7D8 ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetMCUVersionCommand$2()V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.util.concurrent.CountDownLatch -$$Nest$fgetmCountDownLatch ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCountDownLatch;
} // .end method
static Boolean -$$Nest$fgetmIsBleKeyboard ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBleKeyboard:Z */
} // .end method
static java.lang.StringBuilder -$$Nest$fgetmKeyBoardUpgradeFilePath ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyBoardUpgradeFilePath;
} // .end method
static com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil -$$Nest$fgetmKeyBoardUpgradeUtil ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyBoardUpgradeUtil;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker -$$Nest$fgetmKeyUpgradeCommandWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyUpgradeCommandWorker;
} // .end method
static java.lang.String -$$Nest$fgetmKeyboardBleAdd ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardBleAdd;
} // .end method
static java.lang.String -$$Nest$fgetmKeyboardFlashVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardFlashVersion;
} // .end method
static Object -$$Nest$fgetmKeyboardType ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-byte p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B */
} // .end method
static java.lang.String -$$Nest$fgetmKeyboardVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardVersion;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker -$$Nest$fgetmKeyboardVersionWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardVersionWorker;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker -$$Nest$fgetmMCUUpgradeCommandWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMCUUpgradeCommandWorker;
} // .end method
static java.lang.StringBuilder -$$Nest$fgetmMCUUpgradeFilePath ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMCUUpgradeFilePath;
} // .end method
static java.lang.String -$$Nest$fgetmMCUVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMCUVersion;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker -$$Nest$fgetmMCUVersionWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMCUVersionWorker;
} // .end method
static com.android.server.input.padkeyboard.iic.McuUpgradeUtil -$$Nest$fgetmMcuUpgradeUtil ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMcuUpgradeUtil;
} // .end method
static java.util.Queue -$$Nest$fgetmReadyRunningTask ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mReadyRunningTask;
} // .end method
static Integer -$$Nest$fgetmReceiverBinIndex ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReceiverBinIndex:I */
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker -$$Nest$fgetmTouchPadCommandWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTouchPadCommandWorker;
} // .end method
static Object -$$Nest$fgetmTouchPadType ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-byte p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B */
} // .end method
static java.lang.StringBuilder -$$Nest$fgetmTouchPadUpgradeFilePath ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTouchPadUpgradeFilePath;
} // .end method
static com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil -$$Nest$fgetmTouchPadUpgradeUtil ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTouchPadUpgradeUtil;
} // .end method
static java.lang.String -$$Nest$fgetmTouchPadVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTouchPadVersion;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler -$$Nest$fgetmUpgradeCommandHandler ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUpgradeCommandHandler;
} // .end method
static void -$$Nest$fputmAuthCommandResponse ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Object[] p1 ) { //bridge//synthethic
/* .locals 0 */
this.mAuthCommandResponse = p1;
return;
} // .end method
static void -$$Nest$fputmIsBinDataOK ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
return;
} // .end method
static void -$$Nest$fputmIsBleKeyboard ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBleKeyboard:Z */
return;
} // .end method
static void -$$Nest$fputmKeyBoardUpgradeUtil ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil p1 ) { //bridge//synthethic
/* .locals 0 */
this.mKeyBoardUpgradeUtil = p1;
return;
} // .end method
static void -$$Nest$fputmKeyboardBleAdd ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mKeyboardBleAdd = p1;
return;
} // .end method
static void -$$Nest$fputmKeyboardFlashVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mKeyboardFlashVersion = p1;
return;
} // .end method
static void -$$Nest$fputmKeyboardType ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-byte p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B */
return;
} // .end method
static void -$$Nest$fputmKeyboardVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mKeyboardVersion = p1;
return;
} // .end method
static void -$$Nest$fputmMCUVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMCUVersion = p1;
return;
} // .end method
static void -$$Nest$fputmMcuUpgradeUtil ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, com.android.server.input.padkeyboard.iic.McuUpgradeUtil p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMcuUpgradeUtil = p1;
return;
} // .end method
static void -$$Nest$fputmReceiverBinIndex ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReceiverBinIndex:I */
return;
} // .end method
static void -$$Nest$fputmTouchPadType ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-byte p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B */
return;
} // .end method
static void -$$Nest$fputmTouchPadUpgradeUtil ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil p1 ) { //bridge//synthethic
/* .locals 0 */
this.mTouchPadUpgradeUtil = p1;
return;
} // .end method
static void -$$Nest$fputmTouchPadVersion ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mTouchPadVersion = p1;
return;
} // .end method
static void -$$Nest$mcheckDataPkgSum ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Object[] p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->checkDataPkgSum([BLjava/lang/String;)V */
return;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker -$$Nest$mgetTriggerWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->getTriggerWorker()Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
} // .end method
static java.lang.String -$$Nest$mgetUpgradeHead ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->getUpgradeHead()Ljava/lang/String; */
} // .end method
static void -$$Nest$msendBroadCast2Ble ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendBroadCast2Ble()V */
return;
} // .end method
static void -$$Nest$msendBroadCast2Ble ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendBroadCast2Ble(Z)V */
return;
} // .end method
static Boolean -$$Nest$msupportUpgradeKeyboard ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->supportUpgradeKeyboard()Z */
} // .end method
static Boolean -$$Nest$msupportUpgradeMCU ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->supportUpgradeMCU()Z */
} // .end method
static Boolean -$$Nest$msupportUpgradeTouchPad ( com.android.server.input.padkeyboard.iic.IICNodeHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->supportUpgradeTouchPad()Z */
} // .end method
private com.android.server.input.padkeyboard.iic.IICNodeHelper ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 118 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
this.mMCUUpgradeFilePath = v0;
/* .line 56 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
this.mKeyBoardUpgradeFilePath = v0;
/* .line 57 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = "TouchPad_Upgrade.bin"; // const-string v1, "TouchPad_Upgrade.bin"
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
this.mTouchPadUpgradeFilePath = v0;
/* .line 66 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReceiverBinIndex:I */
/* .line 80 */
int v0 = 1; // const/4 v0, 0x1
/* iput-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B */
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
/* iput-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B */
/* .line 91 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAllWorkerList = v0;
/* .line 92 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mReadyRunningTask = v0;
/* .line 104 */
final String v0 = "66:55:44:33:22:11"; // const-string v0, "66:55:44:33:22:11"
this.mKeyboardBleAdd = v0;
/* .line 285 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
this.mUpgradeMCURunnable = v0;
/* .line 339 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
this.mUpgradeKeyboardRunnable = v0;
/* .line 389 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
this.mUpgradeTouchPadRunnable = v0;
/* .line 119 */
this.mContext = p1;
/* .line 120 */
com.android.server.input.padkeyboard.iic.CommunicationUtil .getInstance ( );
this.mCommunicationUtil = v0;
/* .line 121 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).registerSocketCallback ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->registerSocketCallback(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;)V
/* .line 123 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
this.mMcuUpgradeUtil = v0;
/* .line 124 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil; */
/* iget-byte v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B */
/* invoke-direct {v0, p1, v1, v2}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V */
this.mKeyBoardUpgradeUtil = v0;
/* .line 125 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil; */
/* iget-byte v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B */
/* invoke-direct {v0, p1, v1, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V */
this.mTouchPadUpgradeUtil = v0;
/* .line 127 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "keyboard_upgrade"; // const-string v1, "keyboard_upgrade"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 128 */
/* .local v0, "thread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 129 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Landroid/os/Looper;)V */
this.mUpgradeCommandHandler = v1;
/* .line 131 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->initCommandWorker()V */
/* .line 132 */
com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .getInstance ( p1 );
this.mBlueToothKeyboardManager = v1;
/* .line 133 */
return;
} // .end method
private void checkDataPkgSum ( Object[] p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "data" # [B */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 435 */
/* array-length v0, p1 */
int v1 = 5; // const/4 v1, 0x5
/* aget-byte v2, p1, v1 */
/* add-int/lit8 v2, v2, 0x6 */
/* if-ge v0, v2, :cond_0 */
/* .line 437 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Exception Data:"; // const-string v1, "Exception Data:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
android.util.Slog .i ( v1,v0 );
/* .line 438 */
return;
/* .line 440 */
} // :cond_0
v0 = this.mCommunicationUtil;
/* aget-byte v2, p1, v1 */
/* add-int/lit8 v2, v2, 0x6 */
/* aget-byte v1, p1, v1 */
/* add-int/lit8 v1, v1, 0x6 */
/* aget-byte v1, p1, v1 */
int v3 = 0; // const/4 v3, 0x0
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).checkSum ( p1, v3, v2, v1 ); // invoke-virtual {v0, p1, v3, v2, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->checkSum([BIIB)Z
/* if-nez v0, :cond_1 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
/* .line 441 */
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectBleLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
/* if-nez v0, :cond_1 */
/* .line 442 */
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Accumulation and verification failed"; // const-string v2, "Accumulation and verification failed"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 445 */
} // :cond_1
return;
} // .end method
public static com.android.server.input.padkeyboard.iic.IICNodeHelper getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 108 */
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper.sInstance;
/* if-nez v0, :cond_1 */
/* .line 109 */
/* const-class v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* monitor-enter v0 */
/* .line 110 */
try { // :try_start_0
v1 = com.android.server.input.padkeyboard.iic.IICNodeHelper.sInstance;
/* if-nez v1, :cond_0 */
/* .line 111 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;-><init>(Landroid/content/Context;)V */
/* .line 113 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 115 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper.sInstance;
} // .end method
private com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker getTriggerWorker ( ) {
/* .locals 3 */
/* .line 562 */
v0 = this.mAllWorkerList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* .line 563 */
/* .local v1, "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).isWorkRunning ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 564 */
/* .line 566 */
} // .end local v1 # "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
} // :cond_0
/* .line 567 */
} // :cond_1
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "can\'t find the running worker"; // const-string v1, "can\'t find the running worker"
android.util.Slog .e ( v0,v1 );
/* .line 568 */
v0 = this.mEmptyCommandWorker;
} // .end method
private java.lang.String getUpgradeHead ( ) {
/* .locals 1 */
/* .line 334 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 335 */
/* const-string/jumbo v0, "vendor/etc/" */
/* .line 337 */
} // :cond_0
final String v0 = "odm/etc/"; // const-string v0, "odm/etc/"
} // .end method
private void initCommandWorker ( ) {
/* .locals 4 */
/* .line 448 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V */
this.mKeyUpgradeCommandWorker = v0;
/* .line 474 */
/* const/16 v1, 0x38 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).setTargetAddress ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V
/* .line 475 */
v0 = this.mAllWorkerList;
v2 = this.mKeyUpgradeCommandWorker;
/* .line 476 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
/* invoke-direct {v0, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V */
this.mTouchPadCommandWorker = v0;
/* .line 502 */
/* const/16 v2, 0x40 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).setTargetAddress ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V
/* .line 503 */
v0 = this.mAllWorkerList;
v2 = this.mTouchPadCommandWorker;
/* .line 504 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda3; */
/* invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
/* invoke-direct {v0, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V */
this.mMCUUpgradeCommandWorker = v0;
/* .line 529 */
/* const/16 v2, 0x18 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).setTargetAddress ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V
/* .line 530 */
v0 = this.mAllWorkerList;
v3 = this.mMCUUpgradeCommandWorker;
/* .line 532 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* new-instance v3, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda4; */
/* invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
/* invoke-direct {v0, p0, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V */
this.mMCUVersionWorker = v0;
/* .line 541 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).setTargetAddress ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V
/* .line 542 */
v0 = this.mAllWorkerList;
v2 = this.mMCUVersionWorker;
/* .line 544 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda5; */
/* invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
/* invoke-direct {v0, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V */
this.mKeyboardVersionWorker = v0;
/* .line 553 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).setTargetAddress ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V
/* .line 554 */
v0 = this.mAllWorkerList;
v1 = this.mKeyboardVersionWorker;
/* .line 556 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda6; */
/* invoke-direct {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda6;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V */
this.mEmptyCommandWorker = v0;
/* .line 559 */
return;
} // .end method
private void lambda$initCommandWorker$10 ( ) { //synthethic
/* .locals 2 */
/* .line 533 */
v0 = this.mMCUVersionWorker;
v0 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getCommandSize ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 534 */
v0 = this.mMCUVersionWorker;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 536 */
} // :cond_0
v0 = this.mMCUVersionWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 537 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 540 */
} // :goto_0
return;
} // .end method
private void lambda$initCommandWorker$11 ( ) { //synthethic
/* .locals 2 */
/* .line 545 */
v0 = this.mKeyboardVersionWorker;
v0 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getCommandSize ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 546 */
v0 = this.mKeyboardVersionWorker;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 548 */
} // :cond_0
v0 = this.mKeyboardVersionWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 549 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 552 */
} // :goto_0
return;
} // .end method
static void lambda$initCommandWorker$12 ( ) { //synthethic
/* .locals 2 */
/* .line 557 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "No Comannd trigger, The keyboard is Exception response!"; // const-string v1, "No Comannd trigger, The keyboard is Exception response!"
android.util.Slog .e ( v0,v1 );
/* .line 558 */
return;
} // .end method
private void lambda$initCommandWorker$7 ( ) { //synthethic
/* .locals 7 */
/* .line 449 */
v0 = this.mKeyUpgradeCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandResponse()[B
/* .line 450 */
/* .local v0, "responseData":[B */
int v1 = 4; // const/4 v1, 0x4
/* aget-byte v2, v0, v1 */
/* const/16 v3, -0x6f */
/* if-ne v2, v3, :cond_1 */
/* .line 451 */
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
/* const/16 v3, 0x38 */
/* if-nez v2, :cond_0 */
/* .line 452 */
v2 = this.mKeyUpgradeCommandWorker;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).getReceiverPackageIndex ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getReceiverPackageIndex()I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 453 */
/* .local v2, "index":I */
v4 = this.mKeyUpgradeCommandWorker;
v5 = this.mKeyBoardUpgradeUtil;
/* mul-int/lit8 v6, v2, 0x34 */
/* .line 454 */
(( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v5 ).getBinPackInfo ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPackInfo(BI)[B
/* .line 453 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v4 ).insertCommandToQueue ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 456 */
} // .end local v2 # "index":I
/* .line 457 */
} // :cond_0
v2 = this.mKeyUpgradeCommandWorker;
v4 = this.mKeyBoardUpgradeUtil;
/* .line 458 */
(( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v4 ).getUpEndInfo ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getUpEndInfo(B)[B
/* .line 457 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 460 */
v2 = this.mKeyUpgradeCommandWorker;
v4 = this.mKeyBoardUpgradeUtil;
/* .line 461 */
(( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v4 ).getUpFlashInfo ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getUpFlashInfo(B)[B
/* .line 460 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 463 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
/* .line 465 */
} // :goto_0
v2 = this.mKeyUpgradeCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).sendCommand ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 466 */
} // :cond_1
v2 = this.mKeyUpgradeCommandWorker;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).getCommandSize ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 467 */
v2 = this.mKeyUpgradeCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).sendCommand ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 469 */
} // :cond_2
v1 = this.mKeyUpgradeCommandWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v1 );
/* .line 470 */
v1 = this.mUpgradeCommandHandler;
/* const/16 v2, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 473 */
} // :goto_1
return;
} // .end method
private void lambda$initCommandWorker$8 ( ) { //synthethic
/* .locals 7 */
/* .line 477 */
v0 = this.mTouchPadCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandResponse()[B
/* .line 478 */
/* .local v0, "responseData":[B */
int v1 = 4; // const/4 v1, 0x4
/* aget-byte v2, v0, v1 */
/* const/16 v3, -0x6f */
/* if-ne v2, v3, :cond_1 */
/* .line 479 */
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
/* const/16 v3, 0x38 */
/* if-nez v2, :cond_0 */
/* .line 480 */
v2 = this.mTouchPadCommandWorker;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).getReceiverPackageIndex ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getReceiverPackageIndex()I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 481 */
/* .local v2, "index":I */
v4 = this.mTouchPadCommandWorker;
v5 = this.mTouchPadUpgradeUtil;
/* mul-int/lit8 v6, v2, 0x34 */
/* .line 482 */
(( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v5 ).getBinPackInfo ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPackInfo(BI)[B
/* .line 481 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v4 ).insertCommandToQueue ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 484 */
} // .end local v2 # "index":I
/* .line 485 */
} // :cond_0
v2 = this.mTouchPadCommandWorker;
v4 = this.mTouchPadUpgradeUtil;
/* .line 486 */
(( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v4 ).getUpEndInfo ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getUpEndInfo(B)[B
/* .line 485 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 488 */
v2 = this.mTouchPadCommandWorker;
v4 = this.mTouchPadUpgradeUtil;
/* .line 489 */
(( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v4 ).getUpFlashInfo ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getUpFlashInfo(B)[B
/* .line 488 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 491 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
/* .line 493 */
} // :goto_0
v2 = this.mTouchPadCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).sendCommand ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 494 */
} // :cond_1
v2 = this.mTouchPadCommandWorker;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).getCommandSize ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 495 */
v2 = this.mTouchPadCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).sendCommand ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 497 */
} // :cond_2
v1 = this.mTouchPadCommandWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v1 );
/* .line 498 */
v1 = this.mUpgradeCommandHandler;
/* const/16 v2, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 501 */
} // :goto_1
return;
} // .end method
private void lambda$initCommandWorker$9 ( ) { //synthethic
/* .locals 6 */
/* .line 505 */
v0 = this.mMCUUpgradeCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandResponse()[B
/* .line 506 */
/* .local v0, "responseData":[B */
int v1 = 4; // const/4 v1, 0x4
/* aget-byte v2, v0, v1 */
/* const/16 v3, -0x6f */
/* if-ne v2, v3, :cond_1 */
/* .line 507 */
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
/* if-nez v2, :cond_0 */
/* .line 508 */
v2 = this.mMCUUpgradeCommandWorker;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).getReceiverPackageIndex ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getReceiverPackageIndex()I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 509 */
/* .local v2, "index":I */
v3 = this.mMCUUpgradeCommandWorker;
v4 = this.mMcuUpgradeUtil;
/* mul-int/lit8 v5, v2, 0x34 */
/* .line 510 */
(( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v4 ).getBinPackInfo ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPackInfo(I)[B
/* .line 509 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v3 ).insertCommandToQueue ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 511 */
} // .end local v2 # "index":I
/* .line 512 */
} // :cond_0
v2 = this.mMCUUpgradeCommandWorker;
v3 = this.mMcuUpgradeUtil;
/* .line 513 */
(( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v3 ).getUpEndInfo ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getUpEndInfo()[B
/* .line 512 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 514 */
v2 = this.mMCUUpgradeCommandWorker;
v3 = this.mMcuUpgradeUtil;
/* .line 515 */
(( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v3 ).getUpFlashInfo ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getUpFlashInfo()[B
/* .line 514 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 516 */
v2 = this.mMCUUpgradeCommandWorker;
v3 = this.mMcuUpgradeUtil;
/* .line 517 */
(( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v3 ).getResetInfo ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getResetInfo()[B
/* .line 516 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).insertCommandToQueue ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 518 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z */
/* .line 520 */
} // :goto_0
v2 = this.mMCUUpgradeCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).sendCommand ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 521 */
} // :cond_1
v2 = this.mMCUUpgradeCommandWorker;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).getCommandSize ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 522 */
v2 = this.mMCUUpgradeCommandWorker;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).sendCommand ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 524 */
} // :cond_2
v1 = this.mMCUUpgradeCommandWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v1 );
/* .line 525 */
v1 = this.mUpgradeCommandHandler;
/* const/16 v2, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 528 */
} // :goto_1
return;
} // .end method
private void lambda$sendGetKeyboardVersionCommand$0 ( ) { //synthethic
/* .locals 2 */
/* .line 149 */
v0 = this.mKeyboardVersionWorker;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
return;
} // .end method
private void lambda$sendGetKeyboardVersionCommand$1 ( ) { //synthethic
/* .locals 2 */
/* .line 149 */
v0 = this.mReadyRunningTask;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda8; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
return;
} // .end method
private void lambda$sendGetMCUVersionCommand$2 ( ) { //synthethic
/* .locals 2 */
/* .line 166 */
v0 = this.mMCUVersionWorker;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
return;
} // .end method
private void lambda$sendGetMCUVersionCommand$3 ( ) { //synthethic
/* .locals 2 */
/* .line 165 */
v0 = this.mReadyRunningTask;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda9; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
return;
} // .end method
private void lambda$startUpgradeMCUIfNeed$4 ( ) { //synthethic
/* .locals 2 */
/* .line 279 */
v0 = this.mReadyRunningTask;
v1 = this.mUpgradeMCURunnable;
return;
} // .end method
private void lambda$upgradeKeyboardIfNeed$5 ( ) { //synthethic
/* .locals 2 */
/* .line 327 */
v0 = this.mReadyRunningTask;
v1 = this.mUpgradeKeyboardRunnable;
return;
} // .end method
private void lambda$upgradeTouchPadIfNeed$6 ( ) { //synthethic
/* .locals 2 */
/* .line 384 */
v0 = this.mReadyRunningTask;
v1 = this.mUpgradeTouchPadRunnable;
return;
} // .end method
private void sendBroadCast2Ble ( ) {
/* .locals 1 */
/* .line 1187 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendBroadCast2Ble(Z)V */
/* .line 1188 */
return;
} // .end method
private void sendBroadCast2Ble ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isIICConnected" # Z */
/* .line 1191 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Notify Ble iic is "; // const-string v1, "Notify Ble iic is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
android.util.Slog .i ( v1,v0 );
/* .line 1192 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.xiaomi.bluetooth.action.KEYBOARD_ATTACH"; // const-string v1, "com.xiaomi.bluetooth.action.KEYBOARD_ATTACH"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1193 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const/high16 v1, 0x40000000 # 2.0f */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1194 */
final String v1 = "com.xiaomi.bluetooth.keyboard.extra.ADDRESS"; // const-string v1, "com.xiaomi.bluetooth.keyboard.extra.ADDRESS"
v2 = this.mKeyboardBleAdd;
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1195 */
final String v1 = "com.xiaomi.bluetooth.keyboard.extra.ATTACH_STATE"; // const-string v1, "com.xiaomi.bluetooth.keyboard.extra.ATTACH_STATE"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1196 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1197 */
return;
} // .end method
private Boolean supportUpgradeKeyboard ( ) {
/* .locals 3 */
/* .line 369 */
v0 = this.mKeyBoardUpgradeUtil;
v1 = this.mKeyboardVersion;
v0 = (( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v0 ).checkVersion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->checkVersion(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 372 */
} // :cond_0
v0 = this.mNanoSocketCallback;
/* const/16 v1, 0x38 */
final String v2 = "The device version doesn\'t need upgrading"; // const-string v2, "The device version doesn\'t need upgrading"
/* .line 375 */
int v0 = 0; // const/4 v0, 0x0
/* .line 370 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean supportUpgradeMCU ( ) {
/* .locals 3 */
/* .line 312 */
v0 = this.mMcuUpgradeUtil;
v1 = this.mMCUVersion;
v0 = (( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v0 ).checkVersion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->checkVersion(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 315 */
} // :cond_0
v0 = this.mNanoSocketCallback;
/* const/16 v1, 0x18 */
final String v2 = "The device version doesn\'t need upgrading"; // const-string v2, "The device version doesn\'t need upgrading"
/* .line 318 */
int v0 = 0; // const/4 v0, 0x0
/* .line 313 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean supportUpgradeTouchPad ( ) {
/* .locals 3 */
/* .line 420 */
v0 = this.mTouchPadUpgradeUtil;
v1 = this.mTouchPadVersion;
v0 = (( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v0 ).checkVersion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->checkVersion(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 423 */
} // :cond_0
v0 = this.mNanoSocketCallback;
/* const/16 v1, 0x40 */
final String v2 = "The device version doesn\'t need upgrading"; // const-string v2, "The device version doesn\'t need upgrading"
/* .line 426 */
int v0 = 0; // const/4 v0, 0x0
/* .line 421 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
/* # virtual methods */
public checkAuth ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "command" # [B */
/* .line 253 */
/* new-instance v0, Ljava/util/concurrent/CountDownLatch; */
int v1 = 2; // const/4 v1, 0x2
/* invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V */
this.mCountDownLatch = v0;
/* .line 254 */
(( java.util.concurrent.CountDownLatch ) v0 ).countDown ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
/* .line 255 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).communicateWithKeyboardLocked ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V
/* .line 257 */
try { // :try_start_0
v0 = this.mCountDownLatch;
v1 = java.util.concurrent.TimeUnit.MILLISECONDS;
/* const-wide/16 v2, 0x190 */
(( java.util.concurrent.CountDownLatch ) v0 ).await ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 261 */
/* .line 258 */
/* :catch_0 */
/* move-exception v0 */
/* .line 259 */
/* .local v0, "exception":Ljava/lang/InterruptedException; */
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
(( java.lang.InterruptedException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 260 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 262 */
} // .end local v0 # "exception":Ljava/lang/InterruptedException;
} // :goto_0
v0 = this.mAuthCommandResponse;
} // .end method
public void checkHallStatus ( ) {
/* .locals 2 */
/* .line 269 */
v0 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).getLongRawCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B
/* .line 270 */
/* .local v0, "command":[B */
v1 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).setGetHallStatusCommand ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setGetHallStatusCommand([B)V
/* .line 271 */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).communicateWithKeyboardLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V
/* .line 272 */
return;
} // .end method
public void getBleKeyboardStatus ( ) {
/* .locals 2 */
/* .line 1117 */
v0 = this.mBlueToothKeyboardManager;
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$BleDeviceUtil.CMD_KB_STATUS;
(( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ) v0 ).writeDataToBleDevice ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->writeDataToBleDevice([B)V
/* .line 1119 */
return;
} // .end method
public Boolean hasAnyWorkerRunning ( ) {
/* .locals 3 */
/* .line 1178 */
v0 = this.mAllWorkerList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* .line 1179 */
/* .local v1, "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).isWorkRunning ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1180 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1182 */
} // .end local v1 # "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
} // :cond_0
/* .line 1183 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void readKeyboardStatus ( ) {
/* .locals 8 */
/* .line 236 */
v0 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).getLongRawCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B
/* .line 237 */
/* .local v0, "command":[B */
v1 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).setCommandHead ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V
/* .line 238 */
v1 = this.mCommunicationUtil;
/* const/16 v3, 0x4e */
/* const/16 v4, 0x31 */
/* const/16 v5, -0x80 */
/* const/16 v6, 0x38 */
/* const/16 v7, 0x52 */
/* move-object v2, v0 */
/* invoke-virtual/range {v1 ..v7}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setLocalAddress2KeyboardCommand([BBBBBB)V */
/* .line 244 */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).communicateWithKeyboardLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V
/* .line 245 */
return;
} // .end method
public void sendGetKeyboardVersionCommand ( ) {
/* .locals 3 */
/* .line 144 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "offer getKeyboardVersion worker"; // const-string v1, "offer getKeyboardVersion worker"
android.util.Slog .i ( v0,v1 );
/* .line 145 */
v0 = this.mKeyboardVersionWorker;
v1 = this.mCommunicationUtil;
/* .line 146 */
/* const/16 v2, 0x38 */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).getVersionCommand ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getVersionCommand(B)[B
/* .line 145 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 148 */
v0 = this.mUpgradeCommandHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z
/* .line 150 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 152 */
return;
} // .end method
public void sendGetMCUVersionCommand ( ) {
/* .locals 3 */
/* .line 162 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "offer getMCUVersion worker"; // const-string v1, "offer getMCUVersion worker"
android.util.Slog .i ( v0,v1 );
/* .line 163 */
v0 = this.mMCUVersionWorker;
v1 = this.mCommunicationUtil;
/* const/16 v2, 0x18 */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).getVersionCommand ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getVersionCommand(B)[B
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 165 */
v0 = this.mUpgradeCommandHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda7; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z
/* .line 167 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 169 */
return;
} // .end method
public void sendNFCData ( ) {
/* .locals 3 */
/* .line 1108 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder; */
/* invoke-direct {v0}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;-><init>()V */
v1 = com.android.server.input.padkeyboard.OnehopInfo.ACTION_SUFFIX_MIRROR;
/* .line 1109 */
(( com.android.server.input.padkeyboard.OnehopInfo$Builder ) v0 ).setActionSuffix ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->setActionSuffix(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
v1 = this.mContext;
/* .line 1110 */
com.android.server.input.padkeyboard.OnehopInfo .getBtMacAddress ( v1 );
(( com.android.server.input.padkeyboard.OnehopInfo$Builder ) v0 ).setBtMac ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->setBtMac(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
/* .line 1111 */
com.android.server.input.padkeyboard.OnehopInfo .getExtAbility ( );
(( com.android.server.input.padkeyboard.OnehopInfo$Builder ) v0 ).setExtAbility ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->setExtAbility([B)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
/* .line 1112 */
(( com.android.server.input.padkeyboard.OnehopInfo$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->build()Lcom/android/server/input/padkeyboard/OnehopInfo;
/* .line 1113 */
/* .local v0, "info":Lcom/android/server/input/padkeyboard/OnehopInfo; */
v1 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.OnehopInfo ) v0 ).getPayload ( v0 ); // invoke-virtual {v0, v0}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getPayload(Lcom/android/server/input/padkeyboard/OnehopInfo;)[B
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).sendNFC ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sendNFC([B)V
/* .line 1114 */
return;
} // .end method
public void sendRestoreMcuCommand ( ) {
/* .locals 1 */
/* .line 158 */
v0 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).sendRestoreMcuCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sendRestoreMcuCommand()V
/* .line 159 */
return;
} // .end method
public void setKeyboardBacklight ( Object p0 ) {
/* .locals 5 */
/* .param p1, "level" # B */
/* .line 216 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_BACKLIGHT;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .line 218 */
/* .local v0, "feature":B */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectIICLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 219 */
v1 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).getLongRawCommand ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B
/* .line 220 */
/* .local v1, "command":[B */
v2 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v2 ).setCommandHead ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V
/* .line 221 */
v2 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v2 ).setSetKeyboardStatusCommand ( v1, v0, p1 ); // invoke-virtual {v2, v1, v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setSetKeyboardStatusCommand([BBB)V
/* .line 222 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "keyboard_back_light_brightness"; // const-string v3, "keyboard_back_light_brightness"
int v4 = -2; // const/4 v4, -0x2
android.provider.Settings$System .putIntForUser ( v2,v3,p1,v4 );
/* .line 224 */
} // .end local v1 # "command":[B
} // :cond_0
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectBleLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 225 */
com.android.server.input.padkeyboard.iic.CommunicationUtil$BleDeviceUtil .getKeyboardFeatureCommand ( v0,p1 );
/* .line 229 */
/* .restart local v1 # "command":[B */
} // :goto_0
v2 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v2 ).communicateWithKeyboardLocked ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V
/* .line 230 */
return;
/* .line 227 */
} // .end local v1 # "command":[B
} // :cond_1
return;
} // .end method
public void setKeyboardFeature ( Boolean p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .param p2, "featureType" # I */
/* .line 180 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_ENABLE;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_0 */
/* .line 181 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_ENABLE;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .local v0, "feature":B */
/* .line 182 */
} // .end local v0 # "feature":B
} // :cond_0
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_POWER;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_1 */
/* .line 183 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_POWER;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .restart local v0 # "feature":B */
/* .line 184 */
} // .end local v0 # "feature":B
} // :cond_1
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_2 */
/* .line 185 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .restart local v0 # "feature":B */
/* .line 186 */
} // .end local v0 # "feature":B
} // :cond_2
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY_NEW;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_3 */
/* .line 187 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY_NEW;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .restart local v0 # "feature":B */
/* .line 188 */
} // .end local v0 # "feature":B
} // :cond_3
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_4 */
/* .line 189 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .restart local v0 # "feature":B */
/* .line 190 */
} // .end local v0 # "feature":B
} // :cond_4
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE_NEW;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_5 */
/* .line 191 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE_NEW;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .restart local v0 # "feature":B */
/* .line 192 */
} // .end local v0 # "feature":B
} // :cond_5
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_G_SENSOR;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p2, v0, :cond_9 */
/* .line 193 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_G_SENSOR;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* .line 197 */
/* .restart local v0 # "feature":B */
} // :goto_0
v1 = this.mCommunicationUtil;
/* iget-byte v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B */
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).getLedStatusValue ( p2, p1, v2 ); // invoke-virtual {v1, p2, p1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLedStatusValue(IZB)B
/* .line 198 */
/* .local v1, "value":B */
/* if-nez v1, :cond_6 */
/* .line 199 */
/* move v1, p1 */
/* .line 202 */
} // :cond_6
v2 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v2 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v2 ).isConnectIICLocked ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 203 */
v2 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v2 ).getLongRawCommand ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B
/* .line 204 */
/* .local v2, "command":[B */
v3 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v3 ).setCommandHead ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V
/* .line 205 */
v3 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v3 ).setSetKeyboardStatusCommand ( v2, v0, v1 ); // invoke-virtual {v3, v2, v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setSetKeyboardStatusCommand([BBB)V
/* .line 206 */
} // .end local v2 # "command":[B
} // :cond_7
v2 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v2 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v2 ).isConnectBleLocked ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 207 */
com.android.server.input.padkeyboard.iic.CommunicationUtil$BleDeviceUtil .getKeyboardFeatureCommand ( v0,v1 );
/* .line 211 */
/* .restart local v2 # "command":[B */
} // :goto_1
v3 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v3 ).communicateWithKeyboardLocked ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V
/* .line 212 */
return;
/* .line 209 */
} // .end local v2 # "command":[B
} // :cond_8
return;
/* .line 195 */
} // .end local v0 # "feature":B
} // .end local v1 # "value":B
} // :cond_9
return;
} // .end method
public void setNoCheckUpgrade ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 431 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z */
/* .line 432 */
return;
} // .end method
public void setOtaCallBack ( com.android.server.input.padkeyboard.iic.NanoSocketCallback p0 ) {
/* .locals 1 */
/* .param p1, "callBack" # Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback; */
/* .line 136 */
this.mNanoSocketCallback = p1;
/* .line 137 */
v0 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).setOtaCallBack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setOtaCallBack(Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;)V
/* .line 138 */
return;
} // .end method
public void shutdownAllCommandQueue ( ) {
/* .locals 2 */
/* .line 1168 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "clear all keyboard worker"; // const-string v1, "clear all keyboard worker"
android.util.Slog .e ( v0,v1 );
/* .line 1169 */
v0 = this.mKeyboardVersionWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 1170 */
v0 = this.mKeyUpgradeCommandWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 1171 */
v0 = this.mTouchPadCommandWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 1172 */
v0 = this.mMCUVersionWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 1173 */
v0 = this.mMCUUpgradeCommandWorker;
com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker .-$$Nest$mresetWorker ( v0 );
/* .line 1174 */
v0 = this.mReadyRunningTask;
/* .line 1175 */
return;
} // .end method
public void startUpgradeMCUIfNeed ( ) {
/* .locals 2 */
/* .line 278 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "offer upgrade mcu worker"; // const-string v1, "offer upgrade mcu worker"
android.util.Slog .i ( v0,v1 );
/* .line 279 */
v0 = this.mUpgradeCommandHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda11; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda11;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z
/* .line 281 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 283 */
return;
} // .end method
public void upgradeKeyboardIfNeed ( ) {
/* .locals 2 */
/* .line 326 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "offer upgrade keyboard worker"; // const-string v1, "offer upgrade keyboard worker"
android.util.Slog .i ( v0,v1 );
/* .line 327 */
v0 = this.mUpgradeCommandHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda10; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z
/* .line 329 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 331 */
return;
} // .end method
public void upgradeTouchPadIfNeed ( ) {
/* .locals 2 */
/* .line 383 */
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "offer upgrade touchPad worker"; // const-string v1, "offer upgrade touchPad worker"
android.util.Slog .i ( v0,v1 );
/* .line 384 */
v0 = this.mUpgradeCommandHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda12; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda12;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z
/* .line 385 */
v0 = this.mUpgradeCommandHandler;
/* const/16 v1, 0x60 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 387 */
return;
} // .end method
