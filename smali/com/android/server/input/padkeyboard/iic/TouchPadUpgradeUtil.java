public class com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil {
	 /* .source "TouchPadUpgradeUtil.java" */
	 /* # static fields */
	 private static final Integer START_INDEX_FOR_176;
	 private static final Integer START_INDEX_FOR_179;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public Integer mBinCheckSum;
	 public mBinCheckSumByte;
	 public Integer mBinLength;
	 public mBinLengthByte;
	 public mBinStartAddressByte;
	 private java.lang.String mBinStartFlag;
	 public mBinVersion;
	 public java.lang.String mBinVersionStr;
	 private mFileBuf;
	 private Boolean mIsBleDevice;
	 public java.lang.String mPath;
	 public Boolean mValid;
	 /* # direct methods */
	 public com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "path" # Ljava/lang/String; */
		 /* .param p3, "type" # B */
		 /* .line 34 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 25 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* new-array v1, v0, [B */
		 this.mBinLengthByte = v1;
		 /* .line 27 */
		 /* new-array v1, v0, [B */
		 this.mBinCheckSumByte = v1;
		 /* .line 29 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* new-array v2, v1, [B */
		 this.mBinVersion = v2;
		 /* .line 31 */
		 /* new-array v0, v0, [B */
		 this.mBinStartAddressByte = v0;
		 /* .line 35 */
		 final String v0 = "TouchPadKeyboardUpgradeUtil"; // const-string v0, "TouchPadKeyboardUpgradeUtil"
		 /* if-nez p2, :cond_0 */
		 /* .line 36 */
		 final String v1 = "UpgradeOta file Path is null"; // const-string v1, "UpgradeOta file Path is null"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 37 */
		 return;
		 /* .line 39 */
	 } // :cond_0
	 this.mPath = p2;
	 /* .line 41 */
	 /* if-ne p3, v1, :cond_1 */
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z */
/* .line 42 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "TouchPad UpgradeOta file Path:"; // const-string v2, "TouchPad UpgradeOta file Path:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPath;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 43 */
v1 = this.mPath;
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->ReadUpgradeFile(Ljava/lang/String;)[B */
this.mFileBuf = v1;
/* .line 45 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 46 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->parseOtaFile()Z */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mValid:Z */
/* .line 48 */
} // :cond_2
final String v1 = "UpgradeOta file buff is null or length low than 6000"; // const-string v1, "UpgradeOta file buff is null or length low than 6000"
android.util.Slog .i ( v0,v1 );
/* .line 50 */
} // :goto_1
return;
} // .end method
private ReadUpgradeFile ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 55 */
int v0 = 0; // const/4 v0, 0x0
/* .line 56 */
/* .local v0, "fileBuf":[B */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 57 */
/* .local v1, "mFile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 58 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->readFileToBuf(Ljava/lang/String;)[B */
/* .line 60 */
} // :cond_0
final String v2 = "TouchPadKeyboardUpgradeUtil"; // const-string v2, "TouchPadKeyboardUpgradeUtil"
final String v3 = "=== The Upgrade bin file does not exist."; // const-string v3, "=== The Upgrade bin file does not exist."
android.util.Slog .i ( v2,v3 );
/* .line 62 */
} // :goto_0
} // .end method
private Object getSum ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "data" # [B */
/* .param p2, "start" # I */
/* .param p3, "length" # I */
/* .line 277 */
int v0 = 0; // const/4 v0, 0x0
/* .line 278 */
/* .local v0, "sum":B */
/* move v1, p2 */
/* .local v1, "i":I */
} // :goto_0
/* add-int v2, p2, p3 */
/* if-ge v1, v2, :cond_0 */
/* .line 279 */
/* aget-byte v2, p1, v1 */
/* and-int/lit16 v2, v2, 0xff */
/* add-int/2addr v2, v0 */
/* int-to-byte v0, v2 */
/* .line 278 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 281 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
private Integer getSumInt ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "data" # [B */
/* .param p2, "start" # I */
/* .param p3, "length" # I */
/* .line 286 */
int v0 = 0; // const/4 v0, 0x0
/* .line 287 */
/* .local v0, "sum":I */
/* move v1, p2 */
/* .local v1, "i":I */
} // :goto_0
/* add-int v2, p2, p3 */
/* if-ge v1, v2, :cond_0 */
/* .line 288 */
/* aget-byte v2, p1, v1 */
/* and-int/lit16 v2, v2, 0xff */
/* add-int/2addr v0, v2 */
/* .line 287 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 290 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
private Boolean parseOtaFile ( ) {
/* .locals 17 */
/* .line 93 */
/* move-object/from16 v0, p0 */
/* iget-boolean v1, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z */
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* const/16 v1, 0xfc0 */
} // :cond_0
/* move v1, v2 */
/* .line 94 */
/* .local v1, "startIndex":I */
} // :goto_0
v3 = this.mFileBuf;
final String v4 = "TouchPadKeyboardUpgradeUtil"; // const-string v4, "TouchPadKeyboardUpgradeUtil"
if ( v3 != null) { // if-eqz v3, :cond_3
/* array-length v5, v3 */
/* const/16 v6, 0x40 */
/* if-le v5, v6, :cond_3 */
/* .line 95 */
/* move v5, v1 */
/* .line 97 */
/* .local v5, "fileHeadBaseAddr":I */
/* const/16 v7, 0x8 */
/* new-array v8, v7, [B */
/* .line 98 */
/* .local v8, "binStartFlagByte":[B */
/* array-length v9, v8 */
java.lang.System .arraycopy ( v3,v5,v8,v2,v9 );
/* .line 99 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v8 );
this.mBinStartFlag = v3;
/* .line 100 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Bin Start Flag: "; // const-string v9, "Bin Start Flag: "
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mBinStartFlag;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v3 );
/* .line 103 */
/* add-int/2addr v5, v7 */
/* .line 104 */
v3 = this.mFileBuf;
v9 = this.mBinLengthByte;
/* array-length v10, v9 */
java.lang.System .arraycopy ( v3,v5,v9,v2,v10 );
/* .line 105 */
v3 = this.mBinLengthByte;
int v9 = 3; // const/4 v9, 0x3
/* aget-byte v10, v3, v9 */
/* shl-int/lit8 v10, v10, 0x18 */
/* const/high16 v11, -0x1000000 */
/* and-int/2addr v10, v11 */
int v12 = 2; // const/4 v12, 0x2
/* aget-byte v13, v3, v12 */
/* shl-int/lit8 v13, v13, 0x10 */
/* const/high16 v14, 0xff0000 */
/* and-int/2addr v13, v14 */
/* add-int/2addr v10, v13 */
int v13 = 1; // const/4 v13, 0x1
/* aget-byte v15, v3, v13 */
/* shl-int/2addr v15, v7 */
/* const v16, 0xff00 */
/* and-int v15, v15, v16 */
/* add-int/2addr v10, v15 */
/* aget-byte v3, v3, v2 */
/* and-int/lit16 v3, v3, 0xff */
/* add-int/2addr v10, v3 */
/* iput v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I */
/* .line 108 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Bin Lenght: "; // const-string v10, "Bin Lenght: "
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I */
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v3 );
/* .line 111 */
/* add-int/lit8 v5, v5, 0x4 */
/* .line 112 */
v3 = this.mFileBuf;
v10 = this.mBinCheckSumByte;
/* array-length v15, v10 */
java.lang.System .arraycopy ( v3,v5,v10,v2,v15 );
/* .line 113 */
v3 = this.mBinCheckSumByte;
/* aget-byte v10, v3, v9 */
/* shl-int/lit8 v10, v10, 0x18 */
/* and-int/2addr v10, v11 */
/* aget-byte v11, v3, v12 */
/* shl-int/lit8 v11, v11, 0x10 */
/* and-int/2addr v11, v14 */
/* add-int/2addr v10, v11 */
/* aget-byte v11, v3, v13 */
/* shl-int/lit8 v7, v11, 0x8 */
/* and-int v7, v7, v16 */
/* add-int/2addr v10, v7 */
/* aget-byte v3, v3, v2 */
/* and-int/lit16 v3, v3, 0xff */
/* add-int/2addr v10, v3 */
/* iput v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I */
/* .line 116 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin CheckSum: "; // const-string v7, "Bin CheckSum: "
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v3 );
/* .line 119 */
/* add-int/lit8 v5, v5, 0x4 */
/* .line 120 */
/* const/16 v3, 0x32 */
/* new-array v3, v3, [B */
this.mBinVersion = v3;
/* .line 121 */
v7 = this.mFileBuf;
/* array-length v10, v3 */
java.lang.System .arraycopy ( v7,v5,v3,v2,v10 );
/* .line 122 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v7 = this.mBinVersion;
/* aget-byte v7, v7, v2 */
java.lang.Byte .valueOf ( v7 );
/* filled-new-array {v7}, [Ljava/lang/Object; */
final String v10 = "%02x"; // const-string v10, "%02x"
java.lang.String .format ( v10,v7 );
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mBinVersion;
/* aget-byte v7, v7, v13 */
java.lang.Byte .valueOf ( v7 );
/* filled-new-array {v7}, [Ljava/lang/Object; */
java.lang.String .format ( v10,v7 );
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mBinVersionStr = v3;
/* .line 123 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin Version : "; // const-string v7, "Bin Version : "
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mBinVersionStr;
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v3 );
/* .line 126 */
v3 = this.mFileBuf;
/* add-int/lit8 v7, v1, 0x40 */
/* array-length v10, v3 */
/* sub-int/2addr v10, v1 */
/* sub-int/2addr v10, v6 */
v3 = /* invoke-direct {v0, v3, v7, v10}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSumInt([BII)I */
/* .line 127 */
/* .local v3, "sum1":I */
/* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I */
final String v7 = "/"; // const-string v7, "/"
/* if-eq v3, v6, :cond_1 */
/* .line 128 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Bin Check Check Sum Error:"; // const-string v9, "Bin Check Check Sum Error:"
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v6 );
/* .line 129 */
/* .line 133 */
} // :cond_1
v6 = this.mFileBuf;
/* const/16 v10, 0x3f */
v6 = /* invoke-direct {v0, v6, v1, v10}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B */
/* .line 134 */
/* .local v6, "sum2":B */
v10 = this.mFileBuf;
/* add-int/lit8 v11, v1, 0x3f */
/* aget-byte v10, v10, v11 */
/* if-eq v6, v10, :cond_2 */
/* .line 135 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Bin Check Head Sum Error:"; // const-string v10, "Bin Check Head Sum Error:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mFileBuf;
/* add-int/lit8 v10, v1, 0x3f */
/* aget-byte v9, v9, v10 */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v7 );
/* .line 136 */
/* .line 139 */
} // :cond_2
v4 = this.mBinStartAddressByte;
/* aput-byte v2, v4, v2 */
/* .line 140 */
/* const/16 v7, -0x80 */
/* aput-byte v7, v4, v13 */
/* .line 141 */
int v7 = 6; // const/4 v7, 0x6
/* aput-byte v7, v4, v12 */
/* .line 142 */
/* aput-byte v2, v4, v9 */
/* .line 143 */
/* .line 145 */
} // .end local v3 # "sum1":I
} // .end local v5 # "fileHeadBaseAddr":I
} // .end local v6 # "sum2":B
} // .end local v8 # "binStartFlagByte":[B
} // :cond_3
final String v3 = "TouchPad Parse OtaFile err"; // const-string v3, "TouchPad Parse OtaFile err"
android.util.Slog .i ( v4,v3 );
/* .line 147 */
} // .end method
private readFileToBuf ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .line 73 */
int v0 = 0; // const/4 v0, 0x0
/* .line 74 */
/* .local v0, "fileBuf":[B */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 75 */
/* .local v1, "fileStream":Ljava/io/InputStream; */
try { // :try_start_1
v2 = (( java.io.InputStream ) v1 ).available ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->available()I
/* .line 76 */
/* .local v2, "fileLength":I */
/* new-array v3, v2, [B */
/* move-object v0, v3 */
/* .line 77 */
(( java.io.InputStream ) v1 ).read ( v0 ); // invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 78 */
try { // :try_start_2
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 80 */
} // .end local v1 # "fileStream":Ljava/io/InputStream;
/* .line 74 */
} // .end local v2 # "fileLength":I
/* .restart local v1 # "fileStream":Ljava/io/InputStream; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "fileBuf":[B
} // .end local p0 # "this":Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;
} // .end local p1 # "fileName":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 78 */
} // .end local v1 # "fileStream":Ljava/io/InputStream;
/* .restart local v0 # "fileBuf":[B */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil; */
/* .restart local p1 # "fileName":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 79 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "File is not exit!"; // const-string v3, "File is not exit!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "TouchPadKeyboardUpgradeUtil"; // const-string v3, "TouchPadKeyboardUpgradeUtil"
android.util.Slog .i ( v3,v2 );
/* .line 81 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
} // .end method
/* # virtual methods */
public Boolean checkVersion ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 86 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "CheckVersion:"; // const-string v1, "CheckVersion:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBinVersionStr;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "TouchPadKeyboardUpgradeUtil"; // const-string v1, "TouchPadKeyboardUpgradeUtil"
android.util.Slog .i ( v1,v0 );
/* .line 87 */
v0 = this.mBinVersionStr;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( java.lang.String ) v0 ).startsWith ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
v0 = this.mBinVersionStr;
/* .line 88 */
v0 = (( java.lang.String ) v0 ).compareTo ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 87 */
} // :goto_0
} // .end method
public getBinPackInfo ( Object p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "target" # B */
/* .param p2, "offset" # I */
/* .line 178 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 179 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x34 */
/* .line 180 */
/* .local v1, "binlength":I */
/* const/16 v2, -0x56 */
int v3 = 0; // const/4 v3, 0x0
/* aput-byte v2, v0, v3 */
/* .line 181 */
int v2 = 1; // const/4 v2, 0x1
/* const/16 v4, 0x42 */
/* aput-byte v4, v0, v2 */
/* .line 182 */
/* const/16 v5, 0x32 */
int v6 = 2; // const/4 v6, 0x2
/* aput-byte v5, v0, v6 */
/* .line 183 */
int v5 = 3; // const/4 v5, 0x3
/* aput-byte v3, v0, v5 */
/* .line 184 */
/* const/16 v3, 0x4f */
int v7 = 4; // const/4 v7, 0x4
/* aput-byte v3, v0, v7 */
/* .line 185 */
int v3 = 5; // const/4 v3, 0x5
/* const/16 v8, 0x30 */
/* aput-byte v8, v0, v3 */
/* .line 186 */
int v3 = 6; // const/4 v3, 0x6
/* const/16 v8, -0x80 */
/* aput-byte v8, v0, v3 */
/* .line 187 */
int v3 = 7; // const/4 v3, 0x7
/* aput-byte p1, v0, v3 */
/* .line 188 */
/* const/16 v3, 0x8 */
/* const/16 v8, 0x11 */
/* aput-byte v8, v0, v3 */
/* .line 189 */
/* const/16 v3, 0x9 */
/* const/16 v8, 0x38 */
/* aput-byte v8, v0, v3 */
/* .line 190 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( p2 );
/* .line 191 */
/* .local v3, "offsetB":[B */
/* const/16 v8, 0xa */
/* aget-byte v5, v3, v5 */
/* aput-byte v5, v0, v8 */
/* .line 192 */
/* const/16 v5, 0xb */
/* aget-byte v6, v3, v6 */
/* aput-byte v6, v0, v5 */
/* .line 193 */
/* const/16 v5, 0xc */
/* aget-byte v2, v3, v2 */
/* aput-byte v2, v0, v5 */
/* .line 194 */
/* const/16 v2, 0xd */
/* const/16 v5, 0x34 */
/* aput-byte v5, v0, v2 */
/* .line 195 */
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z */
/* const/16 v5, 0xe */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 196 */
v2 = this.mFileBuf;
/* array-length v6, v2 */
/* add-int/lit16 v8, p2, 0x1000 */
/* add-int/2addr v8, v1 */
/* if-ge v6, v8, :cond_0 */
/* .line 197 */
/* array-length v6, v2 */
/* add-int/lit16 v8, p2, 0x1000 */
/* sub-int v1, v6, v8 */
/* .line 199 */
} // :cond_0
/* add-int/lit16 v6, p2, 0x1000 */
java.lang.System .arraycopy ( v2,v6,v0,v5,v1 );
/* .line 201 */
} // :cond_1
v2 = this.mFileBuf;
/* array-length v6, v2 */
/* add-int v8, p2, v1 */
/* if-ge v6, v8, :cond_2 */
/* .line 202 */
/* array-length v6, v2 */
/* sub-int v1, v6, p2 */
/* .line 204 */
} // :cond_2
java.lang.System .arraycopy ( v2,p2,v0,v5,v1 );
/* .line 207 */
} // :goto_0
/* const/16 v2, 0x3e */
v2 = /* invoke-direct {p0, v0, v7, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B */
/* aput-byte v2, v0, v4 */
/* .line 209 */
} // .end method
public Integer getBinPacketTotal ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "length" # I */
/* .line 255 */
int v0 = 0; // const/4 v0, 0x0
/* .line 256 */
/* .local v0, "totle":I */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I */
} // :cond_0
/* iget v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I */
/* add-int/lit8 v1, v1, 0x40 */
/* .line 257 */
/* .local v1, "binLength":I */
} // :goto_0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 265 */
/* :sswitch_0 */
/* div-int/lit8 v0, v1, 0x34 */
/* .line 266 */
/* iget v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I */
/* rem-int/lit8 v2, v2, 0x34 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 267 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 259 */
/* :sswitch_1 */
/* div-int/lit8 v0, v1, 0x14 */
/* .line 260 */
/* iget v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I */
/* rem-int/lit8 v2, v2, 0x14 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 261 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 272 */
} // :cond_1
} // :goto_1
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x20 -> :sswitch_1 */
/* 0x40 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public getUpEndInfo ( Object p0 ) {
/* .locals 7 */
/* .param p1, "target" # B */
/* .line 214 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 215 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 216 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 217 */
/* const/16 v1, 0x32 */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v1, v0, v3 */
/* .line 218 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 220 */
/* const/16 v1, 0x4e */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v1, v0, v4 */
/* .line 221 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v5, 0x30 */
/* aput-byte v5, v0, v1 */
/* .line 222 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v1 */
/* .line 223 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p1, v0, v1 */
/* .line 224 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 225 */
/* const/16 v1, 0x9 */
/* const/16 v5, 0xe */
/* aput-byte v5, v0, v1 */
/* .line 226 */
v1 = this.mBinLengthByte;
/* const/16 v6, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v6,v4 );
/* .line 227 */
v1 = this.mBinStartAddressByte;
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 228 */
v1 = this.mBinCheckSumByte;
/* const/16 v5, 0x12 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 229 */
v1 = this.mBinVersion;
/* const/16 v5, 0x16 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v3 );
/* .line 230 */
/* const/16 v1, 0x14 */
v1 = /* invoke-direct {p0, v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B */
/* const/16 v2, 0x18 */
/* aput-byte v1, v0, v2 */
/* .line 231 */
} // .end method
public getUpFlashInfo ( Object p0 ) {
/* .locals 5 */
/* .param p1, "target" # B */
/* .line 236 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 237 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 238 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 239 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 240 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 242 */
/* const/16 v1, 0x4f */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v1, v0, v3 */
/* .line 243 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x30 */
/* aput-byte v4, v0, v1 */
/* .line 244 */
/* const/16 v1, -0x80 */
int v4 = 6; // const/4 v4, 0x6
/* aput-byte v1, v0, v4 */
/* .line 245 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p1, v0, v1 */
/* .line 246 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 247 */
/* const/16 v1, 0x9 */
/* aput-byte v3, v0, v1 */
/* .line 248 */
v1 = this.mBinStartAddressByte;
/* const/16 v4, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v4,v3 );
/* .line 249 */
/* const/16 v1, 0xe */
v2 = /* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B */
/* aput-byte v2, v0, v1 */
/* .line 250 */
} // .end method
public getUpgradeInfo ( Object p0 ) {
/* .locals 8 */
/* .param p1, "target" # B */
/* .line 152 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 153 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 154 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 155 */
/* const/16 v1, 0x32 */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v1, v0, v3 */
/* .line 156 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 158 */
/* const/16 v4, 0x4f */
int v5 = 4; // const/4 v5, 0x4
/* aput-byte v4, v0, v5 */
/* .line 159 */
int v4 = 5; // const/4 v4, 0x5
/* const/16 v6, 0x30 */
/* aput-byte v6, v0, v4 */
/* .line 160 */
int v4 = 6; // const/4 v4, 0x6
/* const/16 v6, -0x80 */
/* aput-byte v6, v0, v4 */
/* .line 161 */
int v4 = 7; // const/4 v4, 0x7
/* aput-byte p1, v0, v4 */
/* .line 162 */
/* const/16 v4, 0x8 */
/* aput-byte v3, v0, v4 */
/* .line 163 */
/* const/16 v4, 0x9 */
/* const/16 v6, 0x13 */
/* aput-byte v6, v0, v4 */
/* .line 164 */
v4 = this.mBinLengthByte;
/* const/16 v6, 0xa */
java.lang.System .arraycopy ( v4,v2,v0,v6,v5 );
/* .line 165 */
v4 = this.mBinStartAddressByte;
/* const/16 v6, 0xe */
java.lang.System .arraycopy ( v4,v2,v0,v6,v5 );
/* .line 166 */
v4 = this.mBinCheckSumByte;
/* const/16 v6, 0x12 */
java.lang.System .arraycopy ( v4,v2,v0,v6,v5 );
/* .line 167 */
v4 = this.mBinVersion;
/* const/16 v7, 0x16 */
java.lang.System .arraycopy ( v4,v2,v0,v7,v3 );
/* .line 168 */
/* const/16 v2, 0x18 */
/* const/16 v3, 0x2b */
/* aput-byte v3, v0, v2 */
/* .line 169 */
/* const/16 v2, 0x19 */
/* aput-byte v6, v0, v2 */
/* .line 170 */
/* const/16 v3, 0x1a */
/* const/16 v4, 0x20 */
/* aput-byte v4, v0, v3 */
/* .line 171 */
/* const/16 v3, 0x1b */
/* aput-byte v1, v0, v3 */
/* .line 172 */
/* const/16 v1, 0x1d */
v2 = /* invoke-direct {p0, v0, v5, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B */
/* aput-byte v2, v0, v1 */
/* .line 173 */
} // .end method
public Boolean isValidFile ( ) {
/* .locals 1 */
/* .line 67 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mValid:Z */
} // .end method
