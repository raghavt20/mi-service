.class public interface abstract Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;
.super Ljava/lang/Object;
.source "NanoSocketCallback.java"


# static fields
.field public static final CALLBACK_TYPE_KB:I = 0x14

.field public static final CALLBACK_TYPE_KB_DEV:I = 0x3

.field public static final CALLBACK_TYPE_PAD_MCU:I = 0xa

.field public static final CALLBACK_TYPE_TOUCHPAD:I = 0x28

.field public static final OTA_ERROR_REASON_NO_SOCKET:Ljava/lang/String; = "Socket linked exception"

.field public static final OTA_ERROR_REASON_NO_VALID:Ljava/lang/String; = "Invalid upgrade file"

.field public static final OTA_ERROR_REASON_PACKAGE_NUM:Ljava/lang/String; = "Accumulation and verification failed"

.field public static final OTA_ERROR_REASON_VERSION:Ljava/lang/String; = "The device version doesn\'t need upgrading"

.field public static final OTA_ERROR_REASON_WRITE_SOCKET_EXCEPTION:Ljava/lang/String; = "Write data Socket exception"

.field public static final OTA_STATE_TYPE_BEGIN:I = 0x0

.field public static final OTA_STATE_TYPE_FAIL:I = 0x1

.field public static final OTA_STATE_TYPE_SUCCESS:I = 0x2


# virtual methods
.method public abstract onHallStatusChanged(B)V
.end method

.method public abstract onKeyboardEnableStateChanged(Z)V
.end method

.method public abstract onKeyboardGSensorChanged(FFF)V
.end method

.method public abstract onKeyboardSleepStatusChanged(Z)V
.end method

.method public abstract onNFCTouched()V
.end method

.method public abstract onOtaErrorInfo(BLjava/lang/String;)V
.end method

.method public abstract onOtaErrorInfo(BLjava/lang/String;ZI)V
.end method

.method public abstract onOtaProgress(BF)V
.end method

.method public abstract onOtaStateChange(BI)V
.end method

.method public abstract onReadSocketNumError(Ljava/lang/String;)V
.end method

.method public abstract onUpdateKeyboardType(BZ)V
.end method

.method public abstract onUpdateVersion(ILjava/lang/String;)V
.end method

.method public abstract onWriteSocketErrorInfo(Ljava/lang/String;)V
.end method

.method public abstract requestReAuth()V
.end method

.method public abstract requestReleaseAwake()V
.end method

.method public abstract requestStayAwake()V
.end method
