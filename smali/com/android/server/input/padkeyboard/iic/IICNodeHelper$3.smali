.class Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;
.super Ljava/lang/Object;
.source "IICNodeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# direct methods
.method public static synthetic $r8$lambda$CilikKt-LHNIMjginMO4sVuCEpA(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->lambda$run$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 389
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$run$0()V
    .locals 2

    .line 415
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 393
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 394
    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mgetUpgradeHead(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    invoke-static {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;)V

    .line 395
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->isValidFile()Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid upgrade file, with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-interface {v0, v2, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V

    .line 400
    return-void

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msupportUpgradeTouchPad(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V

    .line 405
    return-void

    .line 408
    :cond_1
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "Start upgrade TouchPad"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    move-result-object v1

    .line 411
    const/16 v2, 0x38

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getUpgradeInfo(B)[B

    move-result-object v1

    .line 410
    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 413
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    move-result-object v1

    .line 414
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPackInfo(BI)[B

    move-result-object v1

    .line 413
    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 415
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 416
    return-void
.end method
