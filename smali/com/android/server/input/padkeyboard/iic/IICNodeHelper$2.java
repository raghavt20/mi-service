class com.android.server.input.padkeyboard.iic.IICNodeHelper$2 implements java.lang.Runnable {
	 /* .source "IICNodeHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.iic.IICNodeHelper this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.iic.IICNodeHelper$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* .line 339 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 343 */
v0 = this.this$0;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil; */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v0 );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.this$0;
/* .line 344 */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mgetUpgradeHead ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeFilePath ( v4 );
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v4 = this.this$0;
v4 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardType ( v4 );
/* invoke-direct {v1, v2, v3, v4}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmKeyBoardUpgradeUtil ( v0,v1 );
/* .line 345 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeUtil ( v0 );
v0 = (( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v0 ).isValidFile ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->isValidFile()Z
/* const/16 v1, 0x38 */
/* if-nez v0, :cond_0 */
/* .line 346 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Invalid upgrade file, with "; // const-string v3, "Invalid upgrade file, with "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeFilePath ( v3 );
/* .line 348 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 346 */
/* .line 349 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).emptyCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V
/* .line 350 */
return;
/* .line 353 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msupportUpgradeKeyboard ( v0 );
/* if-nez v0, :cond_1 */
/* .line 354 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).emptyCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V
/* .line 355 */
return;
/* .line 358 */
} // :cond_1
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyUpgradeCommandWorker ( v0 );
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeUtil ( v2 );
/* .line 359 */
(( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v2 ).getUpgradeInfo ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getUpgradeInfo(B)[B
/* .line 358 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 361 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyUpgradeCommandWorker ( v0 );
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeUtil ( v2 );
/* .line 362 */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v2 ).getBinPackInfo ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPackInfo(BI)[B
/* .line 361 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 364 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyUpgradeCommandWorker ( v0 );
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 365 */
return;
} // .end method
