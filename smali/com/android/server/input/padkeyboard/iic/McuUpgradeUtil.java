public class com.android.server.input.padkeyboard.iic.McuUpgradeUtil {
	 /* .source "McuUpgradeUtil.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public Integer mBinCheckSum;
	 public mBinCheckSumByte;
	 public mBinDeviceTypeByte;
	 public mBinFlashAddressByte;
	 public Integer mBinLength;
	 public mBinLengthByte;
	 public Integer mBinPid;
	 public mBinPidByte;
	 private final mBinRoDataByte;
	 public Integer mBinRunStartAddress;
	 public mBinRunStartAddressByte;
	 public mBinStartAddressByte;
	 public java.lang.String mBinVersion;
	 public Integer mBinVid;
	 public mBinVidByte;
	 private android.content.Context mContext;
	 private mFileBuf;
	 public Boolean mIsFileValid;
	 public java.lang.String mOtaFilepath;
	 /* # direct methods */
	 public com.android.server.input.padkeyboard.iic.McuUpgradeUtil ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "otaFilepath" # Ljava/lang/String; */
		 /* .line 39 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 23 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* new-array v1, v0, [B */
		 this.mBinRoDataByte = v1;
		 /* .line 24 */
		 /* new-array v1, v0, [B */
		 this.mBinRunStartAddressByte = v1;
		 /* .line 26 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* new-array v2, v1, [B */
		 this.mBinVidByte = v2;
		 /* .line 28 */
		 /* new-array v1, v1, [B */
		 this.mBinPidByte = v1;
		 /* .line 31 */
		 /* new-array v1, v0, [B */
		 this.mBinLengthByte = v1;
		 /* .line 33 */
		 /* new-array v1, v0, [B */
		 this.mBinStartAddressByte = v1;
		 /* .line 34 */
		 /* new-array v1, v0, [B */
		 this.mBinCheckSumByte = v1;
		 /* .line 36 */
		 /* new-array v0, v0, [B */
		 this.mBinFlashAddressByte = v0;
		 /* .line 37 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* new-array v0, v0, [B */
		 this.mBinDeviceTypeByte = v0;
		 /* .line 40 */
		 final String v0 = "IIC_McuUpgradeUtil"; // const-string v0, "IIC_McuUpgradeUtil"
		 /* if-nez p2, :cond_0 */
		 /* .line 41 */
		 final String v1 = "UpgradeOta file Path is null"; // const-string v1, "UpgradeOta file Path is null"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 42 */
		 return;
		 /* .line 45 */
	 } // :cond_0
	 this.mContext = p1;
	 /* .line 46 */
	 this.mOtaFilepath = p2;
	 /* .line 48 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "UpgradeOta file Path:"; // const-string v2, "UpgradeOta file Path:"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mOtaFilepath;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v0,v1 );
	 /* .line 49 */
	 v1 = this.mOtaFilepath;
	 /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->readUpgradeFile(Ljava/lang/String;)[B */
	 this.mFileBuf = v1;
	 /* .line 51 */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 52 */
		 v0 = 		 /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->parseOtaFile()Z */
		 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mIsFileValid:Z */
		 /* .line 54 */
	 } // :cond_1
	 final String v1 = "UpgradeOta file buff is null or length low than 6000"; // const-string v1, "UpgradeOta file buff is null or length low than 6000"
	 android.util.Slog .i ( v0,v1 );
	 /* .line 56 */
} // :goto_0
return;
} // .end method
private Boolean parseOtaFile ( ) {
/* .locals 22 */
/* .line 99 */
/* move-object/from16 v0, p0 */
v1 = this.mFileBuf;
final String v2 = "IIC_McuUpgradeUtil"; // const-string v2, "IIC_McuUpgradeUtil"
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* array-length v4, v1 */
	 /* const v5, 0x8000 */
	 /* if-le v4, v5, :cond_2 */
	 /* .line 100 */
	 /* const/16 v4, 0x7fc0 */
	 /* .line 102 */
	 /* .local v4, "fileHeadBaseAddress":I */
	 int v6 = 7; // const/4 v6, 0x7
	 /* new-array v7, v6, [B */
	 /* .line 103 */
	 /* .local v7, "binStartFlagByte":[B */
	 /* array-length v8, v7 */
	 java.lang.System .arraycopy ( v1,v4,v7,v3,v8 );
	 /* .line 105 */
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v7 );
	 /* .line 106 */
	 /* .local v1, "binStartFlag":Ljava/lang/String; */
	 /* new-instance v8, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v9 = "Bin Start Flag: "; // const-string v9, "Bin Start Flag: "
	 (( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v8 );
	 /* .line 107 */
	 /* add-int/2addr v4, v6 */
	 /* .line 109 */
	 int v6 = 2; // const/4 v6, 0x2
	 /* new-array v8, v6, [B */
	 /* .line 110 */
	 /* .local v8, "chipIDByte":[B */
	 v9 = this.mFileBuf;
	 /* array-length v10, v8 */
	 java.lang.System .arraycopy ( v9,v4,v8,v3,v10 );
	 /* .line 111 */
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v8,v6 );
	 /* .line 112 */
	 /* .local v9, "binChipId":Ljava/lang/String; */
	 /* new-instance v10, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v11 = "Bin Chip ID: "; // const-string v11, "Bin Chip ID: "
	 (( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v10 );
	 /* .line 113 */
	 /* add-int/2addr v4, v6 */
	 /* .line 115 */
	 v10 = this.mFileBuf;
	 v11 = this.mBinRoDataByte;
	 /* array-length v12, v11 */
	 java.lang.System .arraycopy ( v10,v4,v11,v3,v12 );
	 /* .line 117 */
	 /* new-instance v10, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v11 = "Bin RoData : "; // const-string v11, "Bin RoData : "
	 (( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v11 = this.mBinRoDataByte;
	 int v12 = 4; // const/4 v12, 0x4
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v11,v12 );
	 (( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v10 );
	 /* .line 118 */
	 /* add-int/2addr v4, v12 */
	 /* .line 120 */
	 /* new-array v10, v12, [B */
	 /* .line 121 */
	 /* .local v10, "rameCodeByte":[B */
	 v11 = this.mFileBuf;
	 /* array-length v13, v10 */
	 java.lang.System .arraycopy ( v11,v4,v10,v3,v13 );
	 /* .line 122 */
	 /* new-instance v11, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v13 = "Bin RameCode : "; // const-string v13, "Bin RameCode : "
	 (( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v10,v12 );
	 (( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v11 );
	 /* .line 123 */
	 /* add-int/2addr v4, v12 */
	 /* .line 125 */
	 /* new-array v11, v12, [B */
	 /* .line 126 */
	 /* .local v11, "rameCodeLenByte":[B */
	 v13 = this.mFileBuf;
	 /* array-length v14, v11 */
	 java.lang.System .arraycopy ( v13,v4,v11,v3,v14 );
	 /* .line 128 */
	 /* new-instance v13, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v14 = "Bin RameCodeLength : "; // const-string v14, "Bin RameCodeLength : "
	 (( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v11,v12 );
	 (( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v13 );
	 /* .line 129 */
	 /* add-int/2addr v4, v12 */
	 /* .line 131 */
	 v13 = this.mFileBuf;
	 v14 = this.mBinRunStartAddressByte;
	 /* array-length v15, v14 */
	 java.lang.System .arraycopy ( v13,v4,v14,v3,v15 );
	 /* .line 133 */
	 v13 = this.mBinRunStartAddressByte;
	 int v14 = 3; // const/4 v14, 0x3
	 /* aget-byte v15, v13, v14 */
	 /* const/16 v16, 0x18 */
	 /* shl-int/lit8 v15, v15, 0x18 */
	 /* const/high16 v17, -0x1000000 */
	 /* and-int v15, v15, v17 */
	 /* aget-byte v18, v13, v6 */
	 /* const/16 v5, 0x10 */
	 /* shl-int/lit8 v18, v18, 0x10 */
	 /* const/high16 v19, 0xff0000 */
	 /* and-int v18, v18, v19 */
	 /* add-int v15, v15, v18 */
	 /* const/16 v18, 0x1 */
	 /* aget-byte v20, v13, v18 */
	 /* shl-int/lit8 v20, v20, 0x8 */
	 /* const v21, 0xff00 */
	 /* and-int v20, v20, v21 */
	 /* add-int v15, v15, v20 */
	 /* aget-byte v13, v13, v3 */
	 /* and-int/lit16 v13, v13, 0xff */
	 /* add-int/2addr v15, v13 */
	 /* iput v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddress:I */
	 /* .line 137 */
	 /* new-instance v13, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v15 = "Bin Run Start Address: "; // const-string v15, "Bin Run Start Address: "
	 (( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddress:I */
	 (( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v13 );
	 /* .line 138 */
	 /* add-int/2addr v4, v12 */
	 /* .line 140 */
	 v13 = this.mFileBuf;
	 v15 = this.mBinPidByte;
	 /* array-length v12, v15 */
	 java.lang.System .arraycopy ( v13,v4,v15,v3,v12 );
	 /* .line 141 */
	 v12 = this.mBinPidByte;
	 /* aget-byte v13, v12, v18 */
	 /* shl-int/lit8 v13, v13, 0x8 */
	 /* and-int v13, v13, v21 */
	 /* aget-byte v12, v12, v3 */
	 /* and-int/lit16 v12, v12, 0xff */
	 /* add-int/2addr v13, v12 */
	 /* iput v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPid:I */
	 /* .line 142 */
	 /* new-instance v12, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v13 = "Bin Pid: "; // const-string v13, "Bin Pid: "
	 (( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPid:I */
	 (( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v12 );
	 /* .line 143 */
	 /* add-int/2addr v4, v6 */
	 /* .line 145 */
	 v12 = this.mFileBuf;
	 v13 = this.mBinVidByte;
	 /* array-length v15, v13 */
	 java.lang.System .arraycopy ( v12,v4,v13,v3,v15 );
	 /* .line 146 */
	 v12 = this.mBinVidByte;
	 /* aget-byte v13, v12, v18 */
	 /* shl-int/lit8 v13, v13, 0x8 */
	 /* and-int v13, v13, v21 */
	 /* aget-byte v12, v12, v3 */
	 /* and-int/lit16 v12, v12, 0xff */
	 /* add-int/2addr v13, v12 */
	 /* iput v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVid:I */
	 /* .line 147 */
	 /* new-instance v12, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v13 = "Bin Vid: "; // const-string v13, "Bin Vid: "
	 (( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVid:I */
	 (( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v12 );
	 /* .line 148 */
	 /* add-int/2addr v4, v6 */
	 /* .line 150 */
	 /* add-int/2addr v4, v6 */
	 /* .line 152 */
	 /* new-array v12, v5, [B */
	 /* .line 153 */
	 /* .local v12, "binVersionByte":[B */
	 v13 = this.mFileBuf;
	 /* array-length v15, v12 */
	 java.lang.System .arraycopy ( v13,v4,v12,v3,v15 );
	 /* .line 155 */
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v12 );
	 this.mBinVersion = v13;
	 /* .line 156 */
	 /* new-instance v13, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v15 = "Bin Version : "; // const-string v15, "Bin Version : "
	 (( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v15 = this.mBinVersion;
	 (( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v13 );
	 /* .line 157 */
	 /* add-int/lit8 v4, v4, 0x11 */
	 /* .line 159 */
	 v13 = this.mFileBuf;
	 v15 = this.mBinDeviceTypeByte;
	 /* array-length v5, v15 */
	 /* const/16 v6, 0x7fec */
	 java.lang.System .arraycopy ( v13,v6,v15,v3,v5 );
	 /* .line 162 */
	 v5 = this.mFileBuf;
	 v6 = this.mBinLengthByte;
	 /* array-length v13, v6 */
	 java.lang.System .arraycopy ( v5,v4,v6,v3,v13 );
	 /* .line 164 */
	 v5 = this.mBinLengthByte;
	 /* aget-byte v6, v5, v14 */
	 /* shl-int/lit8 v6, v6, 0x18 */
	 /* and-int v6, v6, v17 */
	 int v13 = 2; // const/4 v13, 0x2
	 /* aget-byte v15, v5, v13 */
	 /* const/16 v13, 0x10 */
	 /* shl-int/2addr v15, v13 */
	 /* and-int v13, v15, v19 */
	 /* add-int/2addr v6, v13 */
	 /* aget-byte v13, v5, v18 */
	 /* shl-int/lit8 v13, v13, 0x8 */
	 /* and-int v13, v13, v21 */
	 /* add-int/2addr v6, v13 */
	 /* aget-byte v5, v5, v3 */
	 /* and-int/lit16 v5, v5, 0xff */
	 /* add-int/2addr v6, v5 */
	 /* iput v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I */
	 /* .line 168 */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Bin Length: "; // const-string v6, "Bin Length: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I */
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v5 );
	 /* .line 169 */
	 int v5 = 4; // const/4 v5, 0x4
	 /* add-int/2addr v4, v5 */
	 /* .line 171 */
	 v5 = this.mFileBuf;
	 v6 = this.mBinCheckSumByte;
	 /* array-length v13, v6 */
	 java.lang.System .arraycopy ( v5,v4,v6,v3,v13 );
	 /* .line 173 */
	 v5 = this.mBinCheckSumByte;
	 /* aget-byte v6, v5, v14 */
	 /* shl-int/lit8 v6, v6, 0x18 */
	 /* and-int v6, v6, v17 */
	 int v13 = 2; // const/4 v13, 0x2
	 /* aget-byte v15, v5, v13 */
	 /* const/16 v13, 0x10 */
	 /* shl-int/lit8 v13, v15, 0x10 */
	 /* and-int v13, v13, v19 */
	 /* add-int/2addr v6, v13 */
	 /* aget-byte v13, v5, v18 */
	 /* shl-int/lit8 v13, v13, 0x8 */
	 /* and-int v13, v13, v21 */
	 /* add-int/2addr v6, v13 */
	 /* aget-byte v5, v5, v3 */
	 /* and-int/lit16 v5, v5, 0xff */
	 /* add-int/2addr v6, v5 */
	 /* iput v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I */
	 /* .line 177 */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Bin CheckSum: "; // const-string v6, "Bin CheckSum: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I */
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v5 );
	 /* .line 180 */
	 v5 = this.mFileBuf;
	 /* array-length v6, v5 */
	 /* const v13, 0x8000 */
	 /* sub-int/2addr v6, v13 */
	 v5 = 	 com.android.server.input.padkeyboard.iic.CommunicationUtil .getSumInt ( v5,v13,v6 );
	 /* .line 182 */
	 /* .local v5, "sum1":I */
	 /* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I */
	 final String v13 = "/"; // const-string v13, "/"
	 /* if-eq v5, v6, :cond_0 */
	 /* .line 183 */
	 /* new-instance v6, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v14 = "Bin Check Check Sum Error:"; // const-string v14, "Bin Check Check Sum Error:"
	 (( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).append ( v13 ); // invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I */
	 (( java.lang.StringBuilder ) v6 ).append ( v13 ); // invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v6 );
	 /* .line 184 */
	 /* .line 188 */
} // :cond_0
v6 = this.mFileBuf;
/* const/16 v15, 0x7fc0 */
/* const/16 v14, 0x3f */
v6 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v6,v15,v14 );
/* .line 189 */
/* .local v6, "sum2":B */
v14 = this.mFileBuf;
/* const/16 v15, 0x7fff */
/* aget-byte v14, v14, v15 */
/* if-eq v6, v14, :cond_1 */
/* .line 190 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Bin Check Head Sum Error:"; // const-string v3, "Bin Check Head Sum Error:"
(( java.lang.StringBuilder ) v14 ).append ( v3 ); // invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v13 ); // invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v13 = this.mFileBuf;
/* aget-byte v13, v13, v15 */
(( java.lang.StringBuilder ) v3 ).append ( v13 ); // invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 191 */
int v2 = 0; // const/4 v2, 0x0
/* .line 194 */
} // :cond_1
/* move v2, v3 */
v3 = this.mBinFlashAddressByte;
/* aput-byte v2, v3, v2 */
/* .line 195 */
/* const/16 v13, -0x80 */
/* aput-byte v13, v3, v18 */
/* .line 196 */
int v13 = 2; // const/4 v13, 0x2
/* aput-byte v2, v3, v13 */
/* .line 197 */
int v14 = 3; // const/4 v14, 0x3
/* aput-byte v2, v3, v14 */
/* .line 199 */
v3 = this.mBinStartAddressByte;
/* aput-byte v2, v3, v2 */
/* .line 200 */
/* aput-byte v2, v3, v18 */
/* .line 201 */
/* aput-byte v2, v3, v13 */
/* .line 202 */
/* aput-byte v2, v3, v14 */
/* .line 204 */
v3 = this.mBinDeviceTypeByte;
/* aput-byte v16, v3, v2 */
/* .line 205 */
/* .line 206 */
} // .end local v1 # "binStartFlag":Ljava/lang/String;
} // .end local v4 # "fileHeadBaseAddress":I
} // .end local v5 # "sum1":I
} // .end local v6 # "sum2":B
} // .end local v7 # "binStartFlagByte":[B
} // .end local v8 # "chipIDByte":[B
} // .end local v9 # "binChipId":Ljava/lang/String;
} // .end local v10 # "rameCodeByte":[B
} // .end local v11 # "rameCodeLenByte":[B
} // .end local v12 # "binVersionByte":[B
} // :cond_2
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 207 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "length = "; // const-string v3, "length = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mFileBuf;
/* array-length v3, v3 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v1 );
/* .line 209 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
} // .end method
private readFileToBuf ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* .line 80 */
/* .local v0, "fileBuf":[B */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 81 */
/* .local v1, "fileStream":Ljava/io/InputStream; */
try { // :try_start_1
v2 = (( java.io.InputStream ) v1 ).available ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->available()I
/* .line 82 */
/* .local v2, "fileLength":I */
/* new-array v3, v2, [B */
/* move-object v0, v3 */
/* .line 83 */
(( java.io.InputStream ) v1 ).read ( v0 ); // invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 84 */
try { // :try_start_2
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 86 */
} // .end local v1 # "fileStream":Ljava/io/InputStream;
/* .line 80 */
} // .end local v2 # "fileLength":I
/* .restart local v1 # "fileStream":Ljava/io/InputStream; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "fileBuf":[B
} // .end local p0 # "this":Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;
} // .end local p1 # "fileName":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 84 */
} // .end local v1 # "fileStream":Ljava/io/InputStream;
/* .restart local v0 # "fileBuf":[B */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil; */
/* .restart local p1 # "fileName":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 85 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "File is not exit!"; // const-string v3, "File is not exit!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "IIC_McuUpgradeUtil"; // const-string v3, "IIC_McuUpgradeUtil"
android.util.Slog .i ( v3,v2 );
/* .line 87 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
} // .end method
private readUpgradeFile ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 61 */
int v0 = 0; // const/4 v0, 0x0
/* .line 62 */
/* .local v0, "fileBuf":[B */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 63 */
/* .local v1, "mFile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 64 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->readFileToBuf(Ljava/lang/String;)[B */
/* .line 66 */
} // :cond_0
final String v2 = "IIC_McuUpgradeUtil"; // const-string v2, "IIC_McuUpgradeUtil"
final String v3 = "The Upgrade bin file does not exist."; // const-string v3, "The Upgrade bin file does not exist."
android.util.Slog .i ( v2,v3 );
/* .line 68 */
} // :goto_0
} // .end method
/* # virtual methods */
public Boolean checkVersion ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 92 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "get version :"; // const-string v1, "get version :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " , bin version:"; // const-string v1, " , bin version:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBinVersion;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "IIC_McuUpgradeUtil"; // const-string v1, "IIC_McuUpgradeUtil"
android.util.Slog .i ( v1,v0 );
/* .line 93 */
v0 = this.mBinVersion;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( java.lang.String ) v0 ).startsWith ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
v0 = this.mBinVersion;
/* .line 94 */
v0 = (( java.lang.String ) v0 ).compareTo ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 93 */
} // :goto_0
} // .end method
public getBinPackInfo ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "offset" # I */
/* .line 245 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 246 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x34 */
/* .line 247 */
/* .local v1, "binlength":I */
/* const/16 v2, -0x56 */
int v3 = 0; // const/4 v3, 0x0
/* aput-byte v2, v0, v3 */
/* .line 248 */
int v2 = 1; // const/4 v2, 0x1
/* const/16 v4, 0x42 */
/* aput-byte v4, v0, v2 */
/* .line 249 */
/* const/16 v5, 0x32 */
int v6 = 2; // const/4 v6, 0x2
/* aput-byte v5, v0, v6 */
/* .line 250 */
int v5 = 3; // const/4 v5, 0x3
/* aput-byte v3, v0, v5 */
/* .line 251 */
/* const/16 v3, 0x4f */
int v7 = 4; // const/4 v7, 0x4
/* aput-byte v3, v0, v7 */
/* .line 252 */
int v3 = 5; // const/4 v3, 0x5
/* const/16 v8, 0x30 */
/* aput-byte v8, v0, v3 */
/* .line 253 */
int v3 = 6; // const/4 v3, 0x6
/* const/16 v8, -0x80 */
/* aput-byte v8, v0, v3 */
/* .line 254 */
int v3 = 7; // const/4 v3, 0x7
/* const/16 v8, 0x18 */
/* aput-byte v8, v0, v3 */
/* .line 255 */
/* const/16 v3, 0x8 */
/* const/16 v8, 0x11 */
/* aput-byte v8, v0, v3 */
/* .line 256 */
/* const/16 v3, 0x9 */
/* const/16 v8, 0x38 */
/* aput-byte v8, v0, v3 */
/* .line 257 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( p1 );
/* .line 258 */
/* .local v3, "offsetB":[B */
/* const/16 v8, 0xa */
/* aget-byte v5, v3, v5 */
/* aput-byte v5, v0, v8 */
/* .line 259 */
/* const/16 v5, 0xb */
/* aget-byte v6, v3, v6 */
/* aput-byte v6, v0, v5 */
/* .line 260 */
/* const/16 v5, 0xc */
/* aget-byte v2, v3, v2 */
/* aput-byte v2, v0, v5 */
/* .line 261 */
v2 = this.mFileBuf;
/* array-length v5, v2 */
/* add-int/lit16 v6, p1, 0x7fc0 */
/* add-int/2addr v6, v1 */
/* if-ge v5, v6, :cond_0 */
/* .line 262 */
/* array-length v5, v2 */
/* add-int/lit16 v5, v5, -0x7fc0 */
/* sub-int v1, v5, p1 */
/* .line 264 */
} // :cond_0
/* const/16 v5, 0xd */
/* const/16 v6, 0x34 */
/* aput-byte v6, v0, v5 */
/* .line 265 */
/* add-int/lit16 v5, p1, 0x7fc0 */
/* const/16 v6, 0xe */
java.lang.System .arraycopy ( v2,v5,v0,v6,v1 );
/* .line 266 */
/* const/16 v2, 0x3e */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v7,v2 );
/* aput-byte v2, v0, v4 */
/* .line 268 */
} // .end method
public Integer getBinPacketTotal ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "length" # I */
/* .line 338 */
int v0 = 0; // const/4 v0, 0x0
/* .line 339 */
/* .local v0, "totle":I */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 347 */
/* :sswitch_0 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I */
/* add-int/lit8 v2, v1, 0x40 */
/* div-int/lit8 v0, v2, 0x34 */
/* .line 348 */
/* add-int/lit8 v1, v1, 0x40 */
/* rem-int/lit8 v1, v1, 0x34 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 349 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 341 */
/* :sswitch_1 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I */
/* add-int/lit8 v2, v1, 0x40 */
/* div-int/lit8 v0, v2, 0x14 */
/* .line 342 */
/* add-int/lit8 v1, v1, 0x40 */
/* rem-int/lit8 v1, v1, 0x14 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 343 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 355 */
} // :cond_0
} // :goto_0
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x20 -> :sswitch_1 */
/* 0x40 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public getResetInfo ( ) {
/* .locals 6 */
/* .line 316 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 317 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 318 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 319 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 320 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 321 */
/* const/16 v2, 0x4f */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v2, v0, v3 */
/* .line 322 */
int v2 = 5; // const/4 v2, 0x5
/* const/16 v4, 0x30 */
/* aput-byte v4, v0, v2 */
/* .line 323 */
int v2 = 6; // const/4 v2, 0x6
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v2 */
/* .line 324 */
int v2 = 7; // const/4 v2, 0x7
/* const/16 v5, 0x18 */
/* aput-byte v5, v0, v2 */
/* .line 325 */
/* const/16 v2, 0x8 */
/* aput-byte v1, v0, v2 */
/* .line 326 */
/* const/16 v1, 0x9 */
/* aput-byte v3, v0, v1 */
/* .line 327 */
/* const/16 v1, 0x57 */
/* const/16 v2, 0xa */
/* aput-byte v1, v0, v2 */
/* .line 328 */
/* const/16 v1, 0xb */
/* const/16 v5, 0x4e */
/* aput-byte v5, v0, v1 */
/* .line 329 */
/* const/16 v1, 0xc */
/* const/16 v5, 0x38 */
/* aput-byte v5, v0, v1 */
/* .line 330 */
/* const/16 v1, 0xd */
/* aput-byte v4, v0, v1 */
/* .line 331 */
/* const/16 v1, 0xe */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v3,v2 );
/* aput-byte v2, v0, v1 */
/* .line 333 */
} // .end method
public getUpEndInfo ( ) {
/* .locals 6 */
/* .line 273 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 274 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 275 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 276 */
/* const/16 v1, 0x32 */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v1, v0, v3 */
/* .line 277 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 278 */
/* const/16 v1, 0x4f */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v1, v0, v4 */
/* .line 279 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v5, 0x30 */
/* aput-byte v5, v0, v1 */
/* .line 280 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v1 */
/* .line 281 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v5, 0x18 */
/* aput-byte v5, v0, v1 */
/* .line 282 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 283 */
/* const/16 v1, 0x9 */
/* const/16 v5, 0x15 */
/* aput-byte v5, v0, v1 */
/* .line 284 */
v1 = this.mBinLengthByte;
/* const/16 v5, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 285 */
v1 = this.mBinStartAddressByte;
/* const/16 v5, 0xe */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 286 */
v1 = this.mBinCheckSumByte;
/* const/16 v5, 0x12 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 287 */
v1 = this.mBinFlashAddressByte;
/* const/16 v5, 0x16 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 288 */
v1 = this.mBinVidByte;
/* const/16 v5, 0x1a */
java.lang.System .arraycopy ( v1,v2,v0,v5,v3 );
/* .line 289 */
v1 = this.mBinPidByte;
/* const/16 v5, 0x1c */
java.lang.System .arraycopy ( v1,v2,v0,v5,v3 );
/* .line 290 */
v1 = this.mBinDeviceTypeByte;
/* aget-byte v1, v1, v2 */
/* const/16 v2, 0x1e */
/* aput-byte v1, v0, v2 */
/* .line 291 */
/* const/16 v1, 0x1b */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v4,v1 );
/* const/16 v2, 0x1f */
/* aput-byte v1, v0, v2 */
/* .line 292 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send UpEndInfo : " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "IIC_McuUpgradeUtil"; // const-string v2, "IIC_McuUpgradeUtil"
android.util.Slog .i ( v2,v1 );
/* .line 293 */
} // .end method
public getUpFlashInfo ( ) {
/* .locals 6 */
/* .line 298 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 299 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 300 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 301 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 302 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 303 */
/* const/16 v1, 0x4f */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v1, v0, v3 */
/* .line 304 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x30 */
/* aput-byte v4, v0, v1 */
/* .line 305 */
/* const/16 v1, -0x80 */
int v4 = 6; // const/4 v4, 0x6
/* aput-byte v1, v0, v4 */
/* .line 306 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v5, 0x18 */
/* aput-byte v5, v0, v1 */
/* .line 307 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 308 */
/* const/16 v1, 0x9 */
/* aput-byte v3, v0, v1 */
/* .line 309 */
v1 = this.mBinLengthByte;
/* const/16 v4, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v4,v3 );
/* .line 310 */
/* const/16 v1, 0xe */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v3,v4 );
/* aput-byte v2, v0, v1 */
/* .line 311 */
} // .end method
public getUpgradeCommand ( ) {
/* .locals 9 */
/* .line 214 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 215 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 216 */
/* const/16 v1, 0x42 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v1, v0, v3 */
/* .line 217 */
/* const/16 v1, 0x32 */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v1, v0, v4 */
/* .line 218 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 219 */
/* const/16 v1, 0x4f */
int v5 = 4; // const/4 v5, 0x4
/* aput-byte v1, v0, v5 */
/* .line 220 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v6, 0x30 */
/* aput-byte v6, v0, v1 */
/* .line 221 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v6, -0x80 */
/* aput-byte v6, v0, v1 */
/* .line 222 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v6, 0x18 */
/* aput-byte v6, v0, v1 */
/* .line 223 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 224 */
/* const/16 v1, 0x9 */
/* const/16 v6, 0x19 */
/* aput-byte v6, v0, v1 */
/* .line 225 */
v1 = this.mBinLengthByte;
/* const/16 v6, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v6,v5 );
/* .line 226 */
v1 = this.mBinStartAddressByte;
/* const/16 v6, 0xe */
java.lang.System .arraycopy ( v1,v2,v0,v6,v5 );
/* .line 227 */
v1 = this.mBinCheckSumByte;
/* const/16 v6, 0x12 */
java.lang.System .arraycopy ( v1,v2,v0,v6,v5 );
/* .line 228 */
v1 = this.mBinFlashAddressByte;
/* const/16 v6, 0x16 */
java.lang.System .arraycopy ( v1,v2,v0,v6,v5 );
/* .line 229 */
v1 = this.mBinVidByte;
/* const/16 v6, 0x1a */
java.lang.System .arraycopy ( v1,v2,v0,v6,v4 );
/* .line 230 */
v1 = this.mBinPidByte;
/* const/16 v6, 0x1c */
java.lang.System .arraycopy ( v1,v2,v0,v6,v4 );
/* .line 231 */
/* const/16 v1, 0x1e */
/* aput-byte v3, v0, v1 */
/* .line 232 */
v1 = this.mBinDeviceTypeByte;
/* aget-byte v1, v1, v2 */
/* const/16 v6, 0x1f */
/* aput-byte v1, v0, v6 */
/* .line 234 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .getYearMonthDayByTimestamp ( v7,v8 );
/* .line 235 */
/* .local v1, "tiems":[I */
/* aget v2, v1, v2 */
/* int-to-byte v2, v2 */
/* const/16 v7, 0x20 */
/* aput-byte v2, v0, v7 */
/* .line 236 */
/* aget v2, v1, v3 */
/* int-to-byte v2, v2 */
/* const/16 v3, 0x21 */
/* aput-byte v2, v0, v3 */
/* .line 237 */
/* aget v2, v1, v4 */
/* int-to-byte v2, v2 */
/* const/16 v3, 0x22 */
/* aput-byte v2, v0, v3 */
/* .line 238 */
/* const/16 v2, 0x23 */
v3 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v5,v6 );
/* aput-byte v3, v0, v2 */
/* .line 239 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "send UpgradeCommand : " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v3, v0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "IIC_McuUpgradeUtil"; // const-string v3, "IIC_McuUpgradeUtil"
android.util.Slog .i ( v3,v2 );
/* .line 240 */
} // .end method
public Boolean isValidFile ( ) {
/* .locals 1 */
/* .line 73 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mIsFileValid:Z */
} // .end method
