.class Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;
.super Landroid/os/Handler;
.source "IICNodeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpgradeCommandHandler"
.end annotation


# static fields
.field public static final DATA_SEND_COMMAND:Ljava/lang/String; = "command"

.field public static final DATA_WORKER_OBJECT:Ljava/lang/String; = "worker"

.field public static final MSG_NOTIFY_READY_DO_COMMAND:I = 0x60

.field public static final MSG_SEND_COMMAND_QUEUE:I = 0x61


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1127
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 1128
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1129
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .line 1133
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x61

    if-ne v0, v1, :cond_1

    .line 1134
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1135
    .local v0, "repeatTimes":I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "worker"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 1136
    .local v2, "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    if-eqz v0, :cond_0

    .line 1137
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "command"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v4

    .line 1138
    .local v4, "command":[B
    sget-object v6, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v6, v4}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithIIC([B)V

    .line 1139
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1140
    .local v6, "data":Landroid/os/Bundle;
    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 1141
    invoke-virtual {v6, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1142
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 1144
    .local v1, "msg2":Landroid/os/Message;
    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 1145
    invoke-virtual {v1, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1146
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    move-result-object v3

    const-wide/16 v7, 0x5dc

    invoke-virtual {v3, v1, v7, v8}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1147
    .end local v1    # "msg2":Landroid/os/Message;
    .end local v4    # "command":[B
    .end local v6    # "data":Landroid/os/Bundle;
    goto :goto_1

    .line 1148
    :cond_0
    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->notResponseException()V

    goto :goto_1

    .line 1150
    .end local v0    # "repeatTimes":I
    .end local v2    # "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x60

    if-ne v0, v1, :cond_4

    .line 1151
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->hasAnyWorkerRunning()Z

    move-result v0

    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    if-nez v0, :cond_3

    .line 1152
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReadyRunningTask(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1153
    .local v0, "worker":Ljava/lang/Runnable;
    if-eqz v0, :cond_2

    .line 1154
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The WorkQueue has:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReadyRunningTask(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/util/Queue;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->requestStayAwake()V

    .line 1156
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1158
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->requestReleaseAwake()V

    .line 1160
    .end local v0    # "worker":Ljava/lang/Runnable;
    :goto_0
    goto :goto_2

    .line 1161
    :cond_3
    const-string v0, "Still has running commandWorker"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1150
    :cond_4
    :goto_1
    nop

    .line 1164
    :goto_2
    return-void
.end method
