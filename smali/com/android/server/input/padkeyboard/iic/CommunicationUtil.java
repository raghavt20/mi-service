public class com.android.server.input.padkeyboard.iic.CommunicationUtil {
	 /* .source "CommunicationUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;, */
	 /* Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;, */
	 /* Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;, */
	 /* Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;, */
	 /* Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BUFFER_SIZE;
public static final Object COMMAND_AUTH_3;
public static final Object COMMAND_AUTH_51;
public static final Object COMMAND_AUTH_52;
public static final Object COMMAND_AUTH_7;
public static final Object COMMAND_AUTH_START;
public static final Object COMMAND_CHECK_MCU_STATUS;
public static final Object COMMAND_CUSTOM_DATA_KB;
public static final Object COMMAND_DATA_PKG;
public static final Object COMMAND_GET_VERSION;
public static final Object COMMAND_G_SENSOR;
public static final Object COMMAND_KB_BACKLIGHT;
public static final Object COMMAND_KB_BATTERY_LEVEL;
public static final Object COMMAND_KB_FEATURE_CAPS_LOCK;
public static final Object COMMAND_KB_FEATURE_POWER;
public static final Object COMMAND_KB_FEATURE_RECOVER;
public static final Object COMMAND_KB_FEATURE_SLEEP;
public static final Object COMMAND_KB_NFC;
public static final Object COMMAND_KB_RE_AUTH;
public static final Object COMMAND_KB_STATUS;
public static final Object COMMAND_KEYBOARD_RESPONSE_STATUS;
public static final Object COMMAND_KEYBOARD_STATUS;
public static final Object COMMAND_KEYBOARD_UPGRADE_STATUS;
public static final Object COMMAND_MCU_BOOT;
public static final Object COMMAND_MCU_RESET;
public static final Object COMMAND_READ_KB_STATUS;
public static final Object COMMAND_RESPONSE_MCU_STATUS;
public static final Object COMMAND_SUCCESS_RUN;
public static final Object COMMAND_UPGRADE;
public static final Object COMMAND_UPGRADE_BUSY;
public static final Object COMMAND_UPGRADE_FINISHED;
public static final Object COMMAND_UPGRADE_FLASH;
public static final Object COMMAN_KEYBOARD_EXTERNAL_FLAG;
public static final Object COMMAN_PAD_BLUETOOTH;
public static final Object COMMAN_PAD_EXTERNAL_FLAG;
public static final Object FEATURE_DISABLE;
public static final Object FEATURE_ENABLE;
public static final Object KEYBOARD_ADDRESS;
public static final Object KEYBOARD_COLOR_BLACK;
public static final Object KEYBOARD_COLOR_WHITE;
public static final Object KEYBOARD_FLASH_ADDRESS;
public static final Object L81A_VERSION_LENGTH;
public static final Object MCU_ADDRESS;
public static final Object OTA_FAIL_NOT_MATCH;
public static final Object PAD_ADDRESS;
public static final Object REPLENISH_PROTOCOL_COMMAND;
public static final Object RESPONSE_BLE_DEVICE_ID;
public static final Object RESPONSE_TYPE;
public static final Object RESPONSE_VENDOR_ONE_LONG_REPORT_ID;
public static final Object RESPONSE_VENDOR_ONE_SHORT_REPORT_ID;
public static final Object RESPONSE_VENDOR_TWO_REPORT_ID;
public static final Object RESPONSE_VENDOR_TWO_SHORT_REPORT_ID;
public static final Integer SEND_COMMAND_BYTE_LONG;
public static final Integer SEND_COMMAND_BYTE_SHORT;
public static final Object SEND_EMPTY_DATA;
public static final Object SEND_REPORT_ID_LONG_DATA;
public static final Object SEND_REPORT_ID_SHORT_DATA;
public static final Object SEND_RESTORE_COMMAND;
public static final Object SEND_SERIAL_NUMBER;
public static final Object SEND_TYPE;
public static final Object SEND_UPGRADE_PACKAGE_COMMAND;
public static final java.lang.String TAG;
public static final Object TOUCHPAD_ADDRESS;
public static final Object UPGRADE_PROTOCOL_COMMAND;
private static volatile com.android.server.input.padkeyboard.iic.CommunicationUtil sCommunicationUtil;
/* # instance fields */
private com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager mKeyboardNanoAppManager;
private final java.lang.Object mLock;
private Integer mNFCDataPkgSize;
private com.android.server.input.padkeyboard.iic.NanoSocketCallback mNanoSocketCallback;
private com.android.server.input.padkeyboard.iic.CommunicationUtil$ReadSocketHandler mReadSocketHandler;
private com.android.server.input.padkeyboard.iic.CommunicationUtil$SocketCallBack mSocketCallBack;
/* # direct methods */
static com.android.server.input.padkeyboard.iic.NanoSocketCallback -$$Nest$fgetmNanoSocketCallback ( com.android.server.input.padkeyboard.iic.CommunicationUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mNanoSocketCallback;
} // .end method
static com.android.server.input.padkeyboard.iic.CommunicationUtil$SocketCallBack -$$Nest$fgetmSocketCallBack ( com.android.server.input.padkeyboard.iic.CommunicationUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSocketCallBack;
} // .end method
private com.android.server.input.padkeyboard.iic.CommunicationUtil ( ) {
	 /* .locals 1 */
	 /* .line 173 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 107 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mLock = v0;
	 /* .line 174 */
	 /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->initSocketClient()V */
	 /* .line 175 */
	 return;
} // .end method
public static com.android.server.input.padkeyboard.iic.CommunicationUtil getInstance ( ) {
	 /* .locals 2 */
	 /* .line 163 */
	 v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil.sCommunicationUtil;
	 /* if-nez v0, :cond_1 */
	 /* .line 164 */
	 /* const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
	 /* monitor-enter v0 */
	 /* .line 165 */
	 try { // :try_start_0
		 v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil.sCommunicationUtil;
		 /* if-nez v1, :cond_0 */
		 /* .line 166 */
		 /* new-instance v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
		 /* invoke-direct {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;-><init>()V */
		 /* .line 168 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 170 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil.sCommunicationUtil;
} // .end method
private java.util.List getNfcData ( Object[] p0 ) {
/* .locals 11 */
/* .param p1, "nfc" # [B */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([B)", */
/* "Ljava/util/List<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 466 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 467 */
/* .local v0, "nfc_data":Ljava/util/List;, "Ljava/util/List<[B>;" */
/* array-length v1, p1 */
/* const/16 v2, 0x8 */
/* add-int/2addr v1, v2 */
/* new-array v1, v1, [B */
/* .line 468 */
/* .local v1, "all_nfc":[B */
/* const/16 v3, 0x4e */
int v4 = 0; // const/4 v4, 0x0
/* aput-byte v3, v1, v4 */
/* .line 469 */
/* const/16 v3, 0x46 */
int v5 = 1; // const/4 v5, 0x1
/* aput-byte v3, v1, v5 */
/* .line 470 */
int v3 = 2; // const/4 v3, 0x2
/* const/16 v6, 0x43 */
/* aput-byte v6, v1, v3 */
/* .line 471 */
/* array-length v3, p1 */
/* add-int/2addr v3, v5 */
/* and-int/lit16 v3, v3, 0xff */
/* int-to-byte v3, v3 */
int v6 = 3; // const/4 v6, 0x3
/* aput-byte v3, v1, v6 */
/* .line 472 */
/* array-length v3, p1 */
/* add-int/2addr v3, v5 */
/* shr-int/2addr v3, v2 */
/* and-int/lit16 v3, v3, 0xffe */
/* int-to-byte v3, v3 */
int v6 = 4; // const/4 v6, 0x4
/* aput-byte v3, v1, v6 */
/* .line 473 */
int v3 = 5; // const/4 v3, 0x5
/* aput-byte v4, v1, v3 */
/* .line 474 */
int v6 = 6; // const/4 v6, 0x6
/* aput-byte v4, v1, v6 */
/* .line 475 */
/* const/16 v7, 0x54 */
int v8 = 7; // const/4 v8, 0x7
/* aput-byte v7, v1, v8 */
/* .line 476 */
/* array-length v7, p1 */
java.lang.System .arraycopy ( p1,v4,v1,v2,v7 );
/* .line 478 */
/* array-length v2, p1 */
/* add-int/2addr v2, v5 */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSumInt ( v1,v8,v2 );
/* .line 479 */
/* .local v2, "sumInt":I */
/* and-int/lit16 v5, v2, 0xff */
/* int-to-byte v5, v5 */
/* aput-byte v5, v1, v3 */
/* .line 480 */
/* shr-int/lit8 v3, v2, 0x8 */
/* and-int/lit16 v3, v3, 0xff */
/* int-to-byte v3, v3 */
/* aput-byte v3, v1, v6 */
/* .line 482 */
/* array-length v3, v1 */
/* const/16 v5, 0x34 */
/* if-le v3, v5, :cond_2 */
/* .line 483 */
/* array-length v3, v1 */
/* add-int/lit8 v3, v3, 0x33 */
/* div-int/2addr v3, v5 */
/* .line 484 */
/* .local v3, "pac_numbers":I */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
/* if-ge v6, v3, :cond_1 */
/* .line 485 */
/* add-int/lit8 v7, v6, 0x1 */
/* if-ne v7, v3, :cond_0 */
/* array-length v7, v1 */
/* mul-int/lit8 v8, v6, 0x34 */
/* sub-int/2addr v7, v8 */
} // :cond_0
/* move v7, v5 */
/* .line 486 */
/* .local v7, "length":I */
} // :goto_1
/* new-array v8, v7, [B */
/* .line 487 */
/* .local v8, "temp":[B */
/* mul-int/lit8 v9, v6, 0x34 */
/* array-length v10, v8 */
java.lang.System .arraycopy ( v1,v9,v8,v4,v10 );
/* .line 488 */
/* .line 484 */
} // .end local v7 # "length":I
} // .end local v8 # "temp":[B
/* add-int/lit8 v6, v6, 0x1 */
/* .line 490 */
} // .end local v3 # "pac_numbers":I
} // .end local v6 # "i":I
} // :cond_1
/* .line 491 */
} // :cond_2
/* .line 494 */
} // :goto_2
} // .end method
public static synchronized Object getSum ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "command" # [B */
/* .param p1, "start" # I */
/* .param p2, "length" # I */
/* const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
/* monitor-enter v0 */
/* .line 423 */
int v1 = 0; // const/4 v1, 0x0
/* .line 424 */
/* .local v1, "sum":B */
/* move v2, p1 */
/* .local v2, "i":I */
} // :goto_0
/* add-int v3, p1, p2 */
/* if-ge v2, v3, :cond_0 */
/* .line 425 */
try { // :try_start_0
/* aget-byte v3, p0, v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* and-int/lit16 v3, v3, 0xff */
/* add-int/2addr v3, v1 */
/* int-to-byte v1, v3 */
/* .line 424 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 422 */
} // .end local v1 # "sum":B
} // .end local v2 # "i":I
} // .end local p0 # "command":[B
} // .end local p1 # "start":I
} // .end local p2 # "length":I
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
/* .line 427 */
/* .restart local v1 # "sum":B */
/* .restart local p0 # "command":[B */
/* .restart local p1 # "start":I */
/* .restart local p2 # "length":I */
} // :cond_0
/* monitor-exit v0 */
} // .end method
public static synchronized Integer getSumInt ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "data" # [B */
/* .param p1, "start" # I */
/* .param p2, "length" # I */
/* const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
/* monitor-enter v0 */
/* .line 431 */
int v1 = 0; // const/4 v1, 0x0
/* .line 432 */
/* .local v1, "sum":I */
/* move v2, p1 */
/* .local v2, "i":I */
} // :goto_0
/* add-int v3, p1, p2 */
/* if-ge v2, v3, :cond_0 */
/* .line 433 */
try { // :try_start_0
/* aget-byte v3, p0, v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* and-int/lit16 v3, v3, 0xff */
/* add-int/2addr v1, v3 */
/* .line 432 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 430 */
} // .end local v1 # "sum":I
} // .end local v2 # "i":I
} // .end local p0 # "data":[B
} // .end local p1 # "start":I
} // .end local p2 # "length":I
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
/* .line 435 */
/* .restart local v1 # "sum":I */
/* .restart local p0 # "data":[B */
/* .restart local p1 # "start":I */
/* .restart local p2 # "length":I */
} // :cond_0
/* monitor-exit v0 */
} // .end method
private void initSocketClient ( ) {
/* .locals 3 */
/* .line 178 */
v0 = this.mReadSocketHandler;
/* if-nez v0, :cond_0 */
/* .line 179 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "IIC_Read_Socket"; // const-string v1, "IIC_Read_Socket"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 180 */
/* .local v0, "readThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 181 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;-><init>(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;Landroid/os/Looper;)V */
this.mReadSocketHandler = v1;
/* .line 183 */
} // .end local v0 # "readThread":Landroid/os/HandlerThread;
} // :cond_0
com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager .getInstance ( );
this.mKeyboardNanoAppManager = v0;
/* .line 184 */
(( com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager ) v0 ).initCallback ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->initCallback()V
/* .line 185 */
v0 = this.mKeyboardNanoAppManager;
(( com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager ) v0 ).setCommunicationUtil ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->setCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)V
/* .line 186 */
return;
} // .end method
/* # virtual methods */
public Boolean checkSum ( Object[] p0, Integer p1, Integer p2, Object p3 ) {
/* .locals 5 */
/* .param p1, "data" # [B */
/* .param p2, "start" # I */
/* .param p3, "length" # I */
/* .param p4, "sum" # B */
/* .line 511 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,p2,p3 );
/* .line 512 */
/* .local v0, "dataSum":B */
int v1 = 1; // const/4 v1, 0x1
/* new-array v2, v1, [B */
/* .line 513 */
/* .local v2, "bytes":[B */
int v3 = 0; // const/4 v3, 0x0
/* aput-byte v0, v2, v3 */
/* .line 514 */
/* aget-byte v4, v2, v3 */
/* if-ne v4, p4, :cond_0 */
} // :cond_0
/* move v1, v3 */
} // :goto_0
} // .end method
public Object getLedStatusValue ( Integer p0, Boolean p1, Object p2 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "enable" # Z */
/* .param p3, "keyType" # B */
/* .line 525 */
/* const/16 v0, 0x21 */
/* if-eq p3, v0, :cond_0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
/* if-nez v0, :cond_2 */
/* .line 526 */
} // :cond_0
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-eq p1, v0, :cond_5 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY_NEW;
/* .line 527 */
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p1, v0, :cond_1 */
/* .line 529 */
} // :cond_1
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-eq p1, v0, :cond_3 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE_NEW;
/* .line 530 */
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* if-ne p1, v0, :cond_2 */
/* .line 534 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 531 */
} // :cond_3
} // :goto_0
if ( p2 != null) { // if-eqz p2, :cond_4
/* const/16 v0, -0x9 */
} // :cond_4
/* const/16 v0, -0xd */
} // :goto_1
/* .line 528 */
} // :cond_5
} // :goto_2
if ( p2 != null) { // if-eqz p2, :cond_6
int v0 = -3; // const/4 v0, -0x3
} // :cond_6
int v0 = -4; // const/4 v0, -0x4
} // :goto_3
} // .end method
public getLongRawCommand ( ) {
/* .locals 1 */
/* .line 285 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
} // .end method
public Integer getNFCDataPkgSize ( ) {
/* .locals 1 */
/* .line 498 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNFCDataPkgSize:I */
} // .end method
public getVersionCommand ( Object p0 ) {
/* .locals 6 */
/* .param p1, "targetAddress" # B */
/* .line 216 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 217 */
/* .local v0, "temp":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 218 */
/* const/16 v1, 0x42 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v1, v0, v3 */
/* .line 219 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v4, 0x32 */
/* aput-byte v4, v0, v1 */
/* .line 220 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 221 */
/* const/16 v1, 0x4e */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v1, v0, v4 */
/* .line 222 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v5, 0x30 */
/* aput-byte v5, v0, v1 */
/* .line 223 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v1 */
/* .line 224 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p1, v0, v1 */
/* .line 225 */
/* const/16 v5, 0x8 */
/* aput-byte v3, v0, v5 */
/* .line 226 */
/* const/16 v5, 0x9 */
/* aput-byte v3, v0, v5 */
/* .line 227 */
/* const/16 v3, 0xa */
/* aput-byte v2, v0, v3 */
/* .line 228 */
/* const/16 v2, 0xb */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v4,v1 );
/* aput-byte v1, v0, v2 */
/* .line 229 */
} // .end method
public void receiverNanoAppData ( Object[] p0 ) {
/* .locals 3 */
/* .param p1, "responseData" # [B */
/* .line 189 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "get keyboard data:"; // const-string v1, "get keyboard data:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "IIC_CommunicationUtil"; // const-string v1, "IIC_CommunicationUtil"
android.util.Slog .i ( v1,v0 );
/* .line 191 */
v0 = this.mReadSocketHandler;
/* if-nez v0, :cond_0 */
/* .line 192 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "IIC_Read_Socket"; // const-string v1, "IIC_Read_Socket"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 193 */
/* .local v0, "readThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 194 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;-><init>(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;Landroid/os/Looper;)V */
this.mReadSocketHandler = v1;
/* .line 196 */
} // .end local v0 # "readThread":Landroid/os/HandlerThread;
} // :cond_0
v0 = this.mReadSocketHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.iic.CommunicationUtil$ReadSocketHandler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 198 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mReadSocketHandler;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil$ReadSocketHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 199 */
return;
} // .end method
public void registerSocketCallback ( com.android.server.input.padkeyboard.iic.CommunicationUtil$SocketCallBack p0 ) {
/* .locals 0 */
/* .param p1, "socketCallBack" # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack; */
/* .line 202 */
this.mSocketCallBack = p1;
/* .line 203 */
return;
} // .end method
public void sendNFC ( Object[] p0 ) {
/* .locals 8 */
/* .param p1, "data_byte" # [B */
/* .line 440 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getNfcData([B)Ljava/util/List; */
/* .line 441 */
v1 = /* .local v0, "nfcData":Ljava/util/List;, "Ljava/util/List<[B>;" */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNFCDataPkgSize:I */
/* .line 442 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* iget v2, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNFCDataPkgSize:I */
/* if-ge v1, v2, :cond_0 */
/* .line 443 */
/* const/16 v2, 0x44 */
/* new-array v2, v2, [B */
/* .line 444 */
/* .local v2, "temp":[B */
/* const/16 v3, -0x56 */
int v4 = 0; // const/4 v4, 0x0
/* aput-byte v3, v2, v4 */
/* .line 445 */
/* const/16 v3, 0x42 */
int v5 = 1; // const/4 v5, 0x1
/* aput-byte v3, v2, v5 */
/* .line 446 */
/* const/16 v3, 0x32 */
int v5 = 2; // const/4 v5, 0x2
/* aput-byte v3, v2, v5 */
/* .line 447 */
int v3 = 3; // const/4 v3, 0x3
/* aput-byte v4, v2, v3 */
/* .line 449 */
/* const/16 v3, 0x4f */
int v6 = 4; // const/4 v6, 0x4
/* aput-byte v3, v2, v6 */
/* .line 450 */
int v3 = 5; // const/4 v3, 0x5
/* const/16 v7, 0x31 */
/* aput-byte v7, v2, v3 */
/* .line 451 */
int v3 = 6; // const/4 v3, 0x6
/* const/16 v7, -0x80 */
/* aput-byte v7, v2, v3 */
/* .line 452 */
int v3 = 7; // const/4 v3, 0x7
/* const/16 v7, 0x38 */
/* aput-byte v7, v2, v3 */
/* .line 454 */
/* const/16 v3, 0x8 */
/* const/16 v7, 0x36 */
/* aput-byte v7, v2, v3 */
/* .line 456 */
/* check-cast v3, [B */
/* array-length v3, v3 */
/* add-int/2addr v3, v5 */
/* int-to-byte v3, v3 */
/* const/16 v5, 0x9 */
/* aput-byte v3, v2, v5 */
v3 = /* .line 457 */
/* int-to-byte v3, v3 */
/* const/16 v5, 0xa */
/* aput-byte v3, v2, v5 */
/* .line 458 */
/* add-int/lit8 v3, v1, 0x1 */
/* int-to-byte v3, v3 */
/* const/16 v5, 0xb */
/* aput-byte v3, v2, v5 */
/* .line 459 */
/* check-cast v5, [B */
/* array-length v5, v5 */
/* const/16 v7, 0xc */
java.lang.System .arraycopy ( v3,v4,v2,v7,v5 );
/* .line 460 */
/* check-cast v3, [B */
/* array-length v3, v3 */
/* add-int/2addr v3, v7 */
/* check-cast v4, [B */
/* array-length v4, v4 */
/* add-int/2addr v4, v7 */
v4 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v2,v6,v4 );
/* aput-byte v4, v2, v3 */
/* .line 461 */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) p0 ).writeSocketCmd ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->writeSocketCmd([B)Z
/* .line 442 */
} // .end local v2 # "temp":[B
/* add-int/lit8 v1, v1, 0x1 */
/* .line 463 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
public void sendRestoreMcuCommand ( ) {
/* .locals 6 */
/* .line 236 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 237 */
/* .local v0, "temp":[B */
/* const/16 v1, 0x32 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 238 */
int v1 = 1; // const/4 v1, 0x1
/* aput-byte v2, v0, v1 */
/* .line 239 */
/* const/16 v1, 0x4f */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v1, v0, v3 */
/* .line 240 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v4, 0x30 */
/* aput-byte v4, v0, v1 */
/* .line 241 */
int v1 = 4; // const/4 v1, 0x4
/* const/16 v4, -0x80 */
/* aput-byte v4, v0, v1 */
/* .line 242 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x18 */
/* aput-byte v4, v0, v1 */
/* .line 243 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v4, 0x1d */
/* aput-byte v4, v0, v1 */
/* .line 245 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v4, 0xd */
/* aput-byte v4, v0, v1 */
/* .line 247 */
/* const/16 v1, 0x8 */
/* const/16 v5, -0x74 */
/* aput-byte v5, v0, v1 */
/* .line 248 */
/* const/16 v1, 0x9 */
/* const/16 v5, 0x3f */
/* aput-byte v5, v0, v1 */
/* .line 249 */
/* const/16 v1, 0xa */
/* const/16 v5, 0x65 */
/* aput-byte v5, v0, v1 */
/* .line 250 */
/* const/16 v1, 0xb */
/* const/16 v5, -0x7f */
/* aput-byte v5, v0, v1 */
/* .line 251 */
/* const/16 v1, 0xc */
/* const/16 v5, -0x6e */
/* aput-byte v5, v0, v1 */
/* .line 252 */
/* const/16 v1, -0x32 */
/* aput-byte v1, v0, v4 */
/* .line 253 */
/* const/16 v1, 0xe */
/* aput-byte v2, v0, v1 */
/* .line 254 */
/* const/16 v1, 0xf */
/* const/16 v2, -0x6c */
/* aput-byte v2, v0, v1 */
/* .line 255 */
/* const/16 v1, 0x10 */
/* const/16 v2, -0x58 */
/* aput-byte v2, v0, v1 */
/* .line 256 */
/* const/16 v1, 0x11 */
/* const/16 v2, -0x7e */
/* aput-byte v2, v0, v1 */
/* .line 257 */
/* const/16 v1, 0x12 */
/* const/16 v2, 0x2d */
/* aput-byte v2, v0, v1 */
/* .line 258 */
/* const/16 v1, 0x5b */
/* const/16 v2, 0x13 */
/* aput-byte v1, v0, v2 */
/* .line 260 */
/* const/16 v1, 0x14 */
/* const/16 v4, 0x7e */
/* aput-byte v4, v0, v1 */
/* .line 261 */
/* const/16 v1, 0x15 */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v3,v2 );
/* aput-byte v2, v0, v1 */
/* .line 262 */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) p0 ).writeSocketCmd ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->writeSocketCmd([B)Z
/* .line 263 */
return;
} // .end method
public void setCheckKeyboardResponseCommand ( Object[] p0, Object p1, Object p2, Object p3, Object p4, Object p5 ) {
/* .locals 4 */
/* .param p1, "bytes" # [B */
/* .param p2, "reportId" # B */
/* .param p3, "version" # B */
/* .param p4, "sourceAdd" # B */
/* .param p5, "targetAdd" # B */
/* .param p6, "lastCommandId" # B */
/* .line 374 */
int v0 = 4; // const/4 v0, 0x4
/* aput-byte p2, p1, v0 */
/* .line 375 */
int v1 = 5; // const/4 v1, 0x5
/* aput-byte p3, p1, v1 */
/* .line 376 */
int v1 = 6; // const/4 v1, 0x6
/* aput-byte p4, p1, v1 */
/* .line 377 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p5, p1, v1 */
/* .line 378 */
/* const/16 v2, 0x8 */
/* const/16 v3, 0x30 */
/* aput-byte v3, p1, v2 */
/* .line 379 */
/* const/16 v2, 0x9 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v3, p1, v2 */
/* .line 380 */
/* const/16 v2, 0xa */
/* aput-byte p6, p1, v2 */
/* .line 381 */
/* const/16 v2, 0xb */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,v0,v1 );
/* aput-byte v0, p1, v2 */
/* .line 382 */
return;
} // .end method
public void setCheckMCUStatusCommand ( Object[] p0, Object p1 ) {
/* .locals 4 */
/* .param p1, "data" # [B */
/* .param p2, "command" # B */
/* .line 391 */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) p0 ).setCommandHead ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V
/* .line 392 */
/* const/16 v0, 0x4e */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v0, p1, v1 */
/* .line 393 */
int v0 = 5; // const/4 v0, 0x5
/* const/16 v2, 0x31 */
/* aput-byte v2, p1, v0 */
/* .line 394 */
int v0 = 6; // const/4 v0, 0x6
/* const/16 v2, -0x80 */
/* aput-byte v2, p1, v0 */
/* .line 395 */
/* const/16 v0, 0x38 */
int v2 = 7; // const/4 v2, 0x7
/* aput-byte v0, p1, v2 */
/* .line 396 */
/* const/16 v0, 0x8 */
/* aput-byte p2, p1, v0 */
/* .line 397 */
/* const/16 v0, 0x9 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v3, p1, v0 */
/* .line 398 */
/* const/16 v0, 0xa */
/* aput-byte v3, p1, v0 */
/* .line 399 */
/* const/16 v0, 0xb */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,v1,v2 );
/* aput-byte v1, p1, v0 */
/* .line 400 */
return;
} // .end method
public void setCommandHead ( Object[] p0 ) {
/* .locals 3 */
/* .param p1, "bytes" # [B */
/* .line 294 */
/* const/16 v0, -0x56 */
int v1 = 0; // const/4 v1, 0x0
/* aput-byte v0, p1, v1 */
/* .line 295 */
int v0 = 1; // const/4 v0, 0x1
/* const/16 v2, 0x42 */
/* aput-byte v2, p1, v0 */
/* .line 296 */
int v0 = 2; // const/4 v0, 0x2
/* const/16 v2, 0x32 */
/* aput-byte v2, p1, v0 */
/* .line 297 */
int v0 = 3; // const/4 v0, 0x3
/* aput-byte v1, p1, v0 */
/* .line 298 */
return;
} // .end method
public void setGetHallStatusCommand ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "data" # [B */
/* .line 403 */
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) p0 ).setCommandHead ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V
/* .line 404 */
/* const/16 v0, 0x4f */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v0, p1, v1 */
/* .line 405 */
int v0 = 5; // const/4 v0, 0x5
/* const/16 v2, 0x20 */
/* aput-byte v2, p1, v0 */
/* .line 406 */
int v0 = 6; // const/4 v0, 0x6
/* const/16 v2, -0x80 */
/* aput-byte v2, p1, v0 */
/* .line 407 */
int v0 = 7; // const/4 v0, 0x7
/* aput-byte v2, p1, v0 */
/* .line 408 */
/* const/16 v2, 0x8 */
/* const/16 v3, -0x1f */
/* aput-byte v3, p1, v2 */
/* .line 409 */
/* const/16 v2, 0x9 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v3, p1, v2 */
/* .line 410 */
/* const/16 v2, 0xa */
int v3 = 0; // const/4 v3, 0x0
/* aput-byte v3, p1, v2 */
/* .line 411 */
/* const/16 v2, 0xb */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,v1,v0 );
/* aput-byte v0, p1, v2 */
/* .line 412 */
return;
} // .end method
public void setLocalAddress2KeyboardCommand ( Object[] p0, Object p1, Object p2, Object p3, Object p4, Object p5 ) {
/* .locals 11 */
/* .param p1, "bytes" # [B */
/* .param p2, "reportId" # B */
/* .param p3, "version" # B */
/* .param p4, "sourceAdd" # B */
/* .param p5, "targetAdd" # B */
/* .param p6, "feature" # B */
/* .line 341 */
/* move-object v0, p1 */
android.bluetooth.BluetoothAdapter .getDefaultAdapter ( );
(( android.bluetooth.BluetoothAdapter ) v1 ).getAddress ( ); // invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;
/* .line 342 */
/* .local v1, "localAddress":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "get pad Address:"; // const-string v3, "get pad Address:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "IIC_CommunicationUtil"; // const-string v3, "IIC_CommunicationUtil"
android.util.Slog .i ( v3,v2 );
/* .line 343 */
int v2 = 4; // const/4 v2, 0x4
/* aput-byte p2, v0, v2 */
/* .line 344 */
int v3 = 5; // const/4 v3, 0x5
/* aput-byte p3, v0, v3 */
/* .line 345 */
int v3 = 6; // const/4 v3, 0x6
/* aput-byte p4, v0, v3 */
/* .line 346 */
int v4 = 7; // const/4 v4, 0x7
/* aput-byte p5, v0, v4 */
/* .line 347 */
/* const/16 v4, 0x8 */
/* aput-byte p6, v0, v4 */
/* .line 348 */
/* const/16 v4, 0x9 */
/* aput-byte v3, v0, v4 */
/* .line 349 */
/* const/16 v3, 0xa */
/* .line 350 */
/* .local v3, "i":I */
v4 = android.text.TextUtils .isEmpty ( v1 );
int v5 = 0; // const/4 v5, 0x0
/* const/16 v6, 0x10 */
/* if-nez v4, :cond_0 */
/* .line 351 */
final String v4 = ":"; // const-string v4, ":"
(( java.lang.String ) v1 ).split ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* array-length v7, v4 */
} // :goto_0
/* if-ge v5, v7, :cond_1 */
/* aget-object v8, v4, v5 */
/* .line 352 */
/* .local v8, "retval":Ljava/lang/String; */
/* add-int/lit8 v9, v3, 0x1 */
} // .end local v3 # "i":I
/* .local v9, "i":I */
java.lang.Integer .valueOf ( v8,v6 );
v10 = (( java.lang.Integer ) v10 ).byteValue ( ); // invoke-virtual {v10}, Ljava/lang/Integer;->byteValue()B
/* aput-byte v10, v0, v3 */
/* .line 351 */
} // .end local v8 # "retval":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* move v3, v9 */
/* .line 355 */
} // .end local v9 # "i":I
/* .restart local v3 # "i":I */
} // :cond_0
/* const/16 v3, 0xa */
} // :goto_1
/* if-ge v3, v6, :cond_1 */
/* .line 356 */
/* add-int/lit8 v4, v3, 0x1 */
} // .end local v3 # "i":I
/* .local v4, "i":I */
/* aput-byte v5, v0, v3 */
/* .line 355 */
/* add-int/lit8 v3, v4, 0x1 */
} // .end local v4 # "i":I
/* .restart local v3 # "i":I */
/* .line 359 */
} // :cond_1
/* const/16 v4, 0xd */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,v2,v4 );
/* aput-byte v2, v0, v6 */
/* .line 360 */
return;
} // .end method
public void setOtaCallBack ( com.android.server.input.padkeyboard.iic.NanoSocketCallback p0 ) {
/* .locals 0 */
/* .param p1, "callBack" # Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback; */
/* .line 206 */
this.mNanoSocketCallback = p1;
/* .line 207 */
return;
} // .end method
public void setReadKeyboardCommand ( Object[] p0, Object p1, Object p2, Object p3, Object p4, Object p5 ) {
/* .locals 4 */
/* .param p1, "bytes" # [B */
/* .param p2, "reportId" # B */
/* .param p3, "version" # B */
/* .param p4, "sourceAdd" # B */
/* .param p5, "targetAdd" # B */
/* .param p6, "feature" # B */
/* .line 330 */
int v0 = 4; // const/4 v0, 0x4
/* aput-byte p2, p1, v0 */
/* .line 331 */
int v1 = 5; // const/4 v1, 0x5
/* aput-byte p3, p1, v1 */
/* .line 332 */
int v1 = 6; // const/4 v1, 0x6
/* aput-byte p4, p1, v1 */
/* .line 333 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p5, p1, v1 */
/* .line 334 */
/* const/16 v2, 0x8 */
/* aput-byte p6, p1, v2 */
/* .line 335 */
/* const/16 v2, 0x9 */
int v3 = 0; // const/4 v3, 0x0
/* aput-byte v3, p1, v2 */
/* .line 336 */
/* const/16 v2, 0xa */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,v0,v1 );
/* aput-byte v0, p1, v2 */
/* .line 337 */
return;
} // .end method
public void setSetKeyboardStatusCommand ( Object[] p0, Object p1, Object p2 ) {
/* .locals 4 */
/* .param p1, "bytes" # [B */
/* .param p2, "commandId" # B */
/* .param p3, "data" # B */
/* .line 308 */
/* const/16 v0, 0x4e */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v0, p1, v1 */
/* .line 309 */
int v0 = 5; // const/4 v0, 0x5
/* const/16 v2, 0x31 */
/* aput-byte v2, p1, v0 */
/* .line 310 */
int v0 = 6; // const/4 v0, 0x6
/* const/16 v2, -0x80 */
/* aput-byte v2, p1, v0 */
/* .line 311 */
/* const/16 v0, 0x38 */
int v2 = 7; // const/4 v2, 0x7
/* aput-byte v0, p1, v2 */
/* .line 312 */
/* const/16 v0, 0x8 */
/* aput-byte p2, p1, v0 */
/* .line 313 */
/* const/16 v0, 0x9 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v3, p1, v0 */
/* .line 314 */
/* const/16 v0, 0xa */
/* aput-byte p3, p1, v0 */
/* .line 315 */
/* const/16 v0, 0xb */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( p1,v1,v2 );
/* aput-byte v1, p1, v0 */
/* .line 316 */
return;
} // .end method
public Boolean writeSocketCmd ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "buf" # [B */
/* .line 272 */
/* array-length v0, p1 */
/* new-array v0, v0, [Ljava/lang/Byte; */
/* .line 273 */
/* .local v0, "temp":[Ljava/lang/Byte; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 274 */
/* .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p1 */
/* if-ge v2, v3, :cond_0 */
/* .line 275 */
/* aget-byte v3, p1, v2 */
java.lang.Byte .valueOf ( v3 );
/* aput-object v3, v0, v2 */
/* .line 274 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 277 */
} // .end local v2 # "i":I
} // :cond_0
java.util.Collections .addAll ( v1,v0 );
/* .line 278 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "write to IIC:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v3, p1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "IIC_CommunicationUtil"; // const-string v3, "IIC_CommunicationUtil"
android.util.Slog .i ( v3,v2 );
/* .line 279 */
v2 = this.mKeyboardNanoAppManager;
(( com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager ) v2 ).sendCommandToNano ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->sendCommandToNano(Ljava/util/ArrayList;)Z
/* .line 280 */
int v2 = 0; // const/4 v2, 0x0
} // .end method
