class com.android.server.input.padkeyboard.iic.IICNodeHelper$3 implements java.lang.Runnable {
	 /* .source "IICNodeHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.iic.IICNodeHelper this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$CilikKt-LHNIMjginMO4sVuCEpA ( com.android.server.input.padkeyboard.iic.IICNodeHelper$3 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;->lambda$run$0()V */
return;
} // .end method
 com.android.server.input.padkeyboard.iic.IICNodeHelper$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* .line 389 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$run$0 ( ) { //synthethic
/* .locals 2 */
/* .line 415 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadCommandWorker ( v0 );
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 393 */
v0 = this.this$0;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil; */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v0 );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.this$0;
/* .line 394 */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mgetUpgradeHead ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeFilePath ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v4 = this.this$0;
v4 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadType ( v4 );
/* invoke-direct {v1, v2, v3, v4}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmTouchPadUpgradeUtil ( v0,v1 );
/* .line 395 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeUtil ( v0 );
v0 = (( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v0 ).isValidFile ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->isValidFile()Z
/* if-nez v0, :cond_0 */
/* .line 396 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid upgrade file, with "; // const-string v2, "Invalid upgrade file, with "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeFilePath ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const/16 v2, 0x40 */
/* .line 399 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).emptyCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V
/* .line 400 */
return;
/* .line 403 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msupportUpgradeTouchPad ( v0 );
/* if-nez v0, :cond_1 */
/* .line 404 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).emptyCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V
/* .line 405 */
return;
/* .line 408 */
} // :cond_1
final String v0 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v0, "MiuiPadKeyboard_IICNodeHelper"
final String v1 = "Start upgrade TouchPad"; // const-string v1, "Start upgrade TouchPad"
android.util.Slog .i ( v0,v1 );
/* .line 410 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadCommandWorker ( v0 );
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeUtil ( v1 );
/* .line 411 */
/* const/16 v2, 0x38 */
(( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v1 ).getUpgradeInfo ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getUpgradeInfo(B)[B
/* .line 410 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 413 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadCommandWorker ( v0 );
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeUtil ( v1 );
/* .line 414 */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v1 ).getBinPackInfo ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPackInfo(BI)[B
/* .line 413 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 415 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v0 );
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;)V */
/* const-wide/16 v2, 0x1388 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 416 */
return;
} // .end method
