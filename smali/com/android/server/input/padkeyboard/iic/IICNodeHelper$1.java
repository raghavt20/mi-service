class com.android.server.input.padkeyboard.iic.IICNodeHelper$1 implements java.lang.Runnable {
	 /* .source "IICNodeHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.iic.IICNodeHelper this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.iic.IICNodeHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* .line 285 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 288 */
v0 = this.this$0;
/* new-instance v1, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil; */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v0 );
/* const-string/jumbo v3, "vendor/etc/MCU_Upgrade.bin" */
/* invoke-direct {v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmMcuUpgradeUtil ( v0,v1 );
/* .line 290 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMcuUpgradeUtil ( v0 );
v0 = (( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v0 ).isValidFile ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->isValidFile()Z
/* if-nez v0, :cond_0 */
/* .line 291 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* const/16 v1, 0x18 */
final String v2 = "Invalid upgrade file, with vendor/etc/MCU_Upgrade.bin"; // const-string v2, "Invalid upgrade file, with vendor/etc/MCU_Upgrade.bin"
/* .line 294 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).emptyCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V
/* .line 295 */
return;
/* .line 298 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msupportUpgradeMCU ( v0 );
/* if-nez v0, :cond_1 */
/* .line 299 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).emptyCommandResponse ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V
/* .line 300 */
return;
/* .line 303 */
} // :cond_1
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMcuUpgradeUtil ( v1 );
(( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v1 ).getUpgradeCommand ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getUpgradeCommand()[B
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 305 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMcuUpgradeUtil ( v1 );
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v1 ).getBinPackInfo ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPackInfo(I)[B
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).insertCommandToQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V
/* .line 307 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).sendCommand ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V
/* .line 308 */
return;
} // .end method
