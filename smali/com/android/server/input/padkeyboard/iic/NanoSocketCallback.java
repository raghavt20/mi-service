public abstract class com.android.server.input.padkeyboard.iic.NanoSocketCallback {
	 /* .source "NanoSocketCallback.java" */
	 /* # static fields */
	 public static final Integer CALLBACK_TYPE_KB;
	 public static final Integer CALLBACK_TYPE_KB_DEV;
	 public static final Integer CALLBACK_TYPE_PAD_MCU;
	 public static final Integer CALLBACK_TYPE_TOUCHPAD;
	 public static final java.lang.String OTA_ERROR_REASON_NO_SOCKET;
	 public static final java.lang.String OTA_ERROR_REASON_NO_VALID;
	 public static final java.lang.String OTA_ERROR_REASON_PACKAGE_NUM;
	 public static final java.lang.String OTA_ERROR_REASON_VERSION;
	 public static final java.lang.String OTA_ERROR_REASON_WRITE_SOCKET_EXCEPTION;
	 public static final Integer OTA_STATE_TYPE_BEGIN;
	 public static final Integer OTA_STATE_TYPE_FAIL;
	 public static final Integer OTA_STATE_TYPE_SUCCESS;
	 /* # virtual methods */
	 public abstract void onHallStatusChanged ( Object p0 ) {
	 } // .end method
	 public abstract void onKeyboardEnableStateChanged ( Boolean p0 ) {
	 } // .end method
	 public abstract void onKeyboardGSensorChanged ( Float p0, Float p1, Float p2 ) {
	 } // .end method
	 public abstract void onKeyboardSleepStatusChanged ( Boolean p0 ) {
	 } // .end method
	 public abstract void onNFCTouched ( ) {
	 } // .end method
	 public abstract void onOtaErrorInfo ( Object p0, java.lang.String p1 ) {
	 } // .end method
	 public abstract void onOtaErrorInfo ( Object p0, java.lang.String p1, Boolean p2, Integer p3 ) {
	 } // .end method
	 public abstract void onOtaProgress ( Object p0, Float p1 ) {
	 } // .end method
	 public abstract void onOtaStateChange ( Object p0, Integer p1 ) {
	 } // .end method
	 public abstract void onReadSocketNumError ( java.lang.String p0 ) {
	 } // .end method
	 public abstract void onUpdateKeyboardType ( Object p0, Boolean p1 ) {
	 } // .end method
	 public abstract void onUpdateVersion ( Integer p0, java.lang.String p1 ) {
	 } // .end method
	 public abstract void onWriteSocketErrorInfo ( java.lang.String p0 ) {
	 } // .end method
	 public abstract void requestReAuth ( ) {
	 } // .end method
	 public abstract void requestReleaseAwake ( ) {
	 } // .end method
	 public abstract void requestStayAwake ( ) {
	 } // .end method
