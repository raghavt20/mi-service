.class public Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;
.super Ljava/lang/Object;
.source "KeyboardNanoAppManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "KeyboardNanoApp"

.field private static volatile mIKeyboardNanoapp:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;


# instance fields
.field private mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

.field private mKeyboardNanoapp:Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;

.field private mNanoappCallback:Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 3

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    :try_start_0
    invoke-static {}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;->getService()Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mKeyboardNanoapp:Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    goto :goto_0

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "KeyboardNanoApp"

    const-string v2, "exception when get service"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;
    .locals 2

    .line 29
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mIKeyboardNanoapp:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    if-nez v0, :cond_1

    .line 30
    const-class v0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    monitor-enter v0

    .line 31
    :try_start_0
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mIKeyboardNanoapp:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    if-nez v1, :cond_0

    .line 32
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    invoke-direct {v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;-><init>()V

    sput-object v1, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mIKeyboardNanoapp:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    .line 34
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 36
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mIKeyboardNanoapp:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    return-object v0
.end method


# virtual methods
.method public initCallback()V
    .locals 3

    .line 50
    :try_start_0
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;-><init>(Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mNanoappCallback:Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback;

    .line 65
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mKeyboardNanoapp:Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;

    invoke-interface {v1, v0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;->setCallback(Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set callback error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KeyboardNanoApp"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public sendCommandToNano(Ljava/util/ArrayList;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)Z"
        }
    .end annotation

    .line 41
    .local p1, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mKeyboardNanoapp:Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;

    invoke-interface {v0, p1}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp;->sendCmd(Ljava/util/ArrayList;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set callback error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KeyboardNanoApp"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public setCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)V
    .locals 0
    .param p1, "communicationUtil"    # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 72
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 73
    return-void
.end method
