.class Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;
.super Ljava/lang/Object;
.source "IICNodeHelper.java"

# interfaces
.implements Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SocketCallbackListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 574
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getSuitableBinName(BB)V
    .locals 4
    .param p1, "keyboardValue"    # B
    .param p2, "touchpadValue"    # B

    .line 805
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;B)V

    .line 806
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "0x%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0, p2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmTouchPadType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;B)V

    .line 810
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 811
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "TouchPad_Upgrade"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 812
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".bin"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set TouchPad Upgrade File:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    return-void
.end method

.method private onMcuVersionResponse([B)V
    .locals 4
    .param p1, "data"    # [B

    .line 1088
    const/16 v0, 0x10

    new-array v1, v0, [B

    .line 1089
    .local v1, "deviceVersion":[B
    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-static {p1, v2, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1090
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmMCUVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V

    .line 1092
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1093
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "MCU_Upgrade"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".bin"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1094
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set MCU Upgrade File:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MiuiPadKeyboard_IICNodeHelper"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUVersionWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1097
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUVersionWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    if-eqz v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    invoke-interface {v0, v3, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onUpdateVersion(ILjava/lang/String;)V

    .line 1104
    :cond_1
    return-void
.end method

.method private parseGsensorDataInfo([B)V
    .locals 9
    .param p1, "data"    # [B

    .line 915
    const v0, 0x411ccccd    # 9.8f

    .line 916
    .local v0, "G":F
    const/4 v1, 0x7

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x4

    and-int/lit16 v1, v1, 0xff0

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    shr-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, 0xf

    or-int/2addr v1, v2

    .line 917
    .local v1, "x":I
    const/16 v2, 0x9

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x4

    and-int/lit16 v2, v2, 0xff0

    const/16 v3, 0x8

    aget-byte v3, p1, v3

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, 0xf

    or-int/2addr v2, v3

    .line 918
    .local v2, "y":I
    const/16 v3, 0xb

    aget-byte v3, p1, v3

    shl-int/lit8 v3, v3, 0x4

    and-int/lit16 v3, v3, 0xff0

    const/16 v4, 0xa

    aget-byte v4, p1, v4

    shr-int/lit8 v4, v4, 0x4

    and-int/lit8 v4, v4, 0xf

    or-int/2addr v3, v4

    .line 919
    .local v3, "z":I
    const/16 v4, 0x800

    .line 920
    .local v4, "mask":I
    and-int v5, v1, v4

    if-ne v5, v4, :cond_0

    .line 921
    rsub-int v5, v1, 0x1000

    neg-int v1, v5

    .line 923
    :cond_0
    and-int v5, v2, v4

    if-ne v5, v4, :cond_1

    .line 924
    rsub-int v5, v2, 0x1000

    neg-int v2, v5

    .line 926
    :cond_1
    and-int v5, v3, v4

    if-ne v5, v4, :cond_2

    .line 927
    rsub-int v5, v3, 0x1000

    neg-int v3, v5

    .line 929
    :cond_2
    int-to-float v5, v1

    mul-float/2addr v5, v0

    const/high16 v6, 0x43800000    # 256.0f

    div-float/2addr v5, v6

    .line 930
    .local v5, "x_normal":F
    neg-int v7, v2

    int-to-float v7, v7

    mul-float/2addr v7, v0

    div-float/2addr v7, v6

    .line 931
    .local v7, "y_normal":F
    neg-int v8, v3

    int-to-float v8, v8

    mul-float/2addr v8, v0

    div-float/2addr v8, v6

    .line 933
    .local v8, "z_normal":F
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v6, v6, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v6, v5, v7, v8}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onKeyboardGSensorChanged(FFF)V

    .line 934
    return-void
.end method

.method private parseKeyboardStatus([B)V
    .locals 10
    .param p1, "data"    # [B

    .line 938
    const/4 v0, 0x0

    aget-byte v1, p1, v0

    const/16 v2, 0x26

    const/16 v3, 0x23

    const/16 v4, 0x12

    const/16 v5, 0x9

    const-string v6, "MiuiPadKeyboard_IICNodeHelper"

    if-eq v1, v2, :cond_0

    aget-byte v1, p1, v0

    const/16 v2, 0x24

    if-eq v1, v2, :cond_0

    aget-byte v1, p1, v0

    if-ne v1, v3, :cond_6

    .line 941
    :cond_0
    aget-byte v1, p1, v4

    const/4 v2, 0x1

    if-nez v1, :cond_4

    .line 943
    aget-byte v1, p1, v5

    and-int/lit8 v1, v1, 0x63

    if-ne v1, v3, :cond_1

    .line 944
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectIICLocked(Z)V

    goto :goto_0

    .line 946
    :cond_1
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectIICLocked(Z)V

    .line 947
    aget-byte v1, p1, v5

    and-int/lit8 v1, v1, 0x63

    const/16 v3, 0x43

    if-ne v1, v3, :cond_2

    .line 948
    const-string v1, "Keyboard is connect,but Pogo pin is exception"

    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v2

    .line 950
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x110f0249

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 949
    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 952
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 953
    :cond_2
    aget-byte v0, p1, v5

    and-int/lit8 v0, v0, 0x3

    if-ne v0, v2, :cond_3

    .line 954
    const-string v0, "Keyboard is connect,but TRX is exception or leather case coming"

    invoke-static {v6, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 958
    :cond_3
    const-string v0, "Keyboard is disConnection"

    invoke-static {v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 961
    :cond_4
    aget-byte v1, p1, v4

    if-ne v1, v2, :cond_5

    .line 962
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectIICLocked(Z)V

    .line 963
    const-string v0, "The keyboard power supply current exceeds the limit\uff01"

    invoke-static {v6, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    :cond_5
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmIsBleKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 966
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msendBroadCast2Ble(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    .line 969
    :cond_6
    const/16 v0, 0xb

    aget-byte v0, p1, v0

    shl-int/lit8 v0, v0, 0x8

    const v1, 0xff00

    and-int/2addr v0, v1

    const/16 v1, 0xa

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    .line 970
    .local v0, "battry":I
    const/16 v1, 0x11

    aget-byte v2, p1, v1

    and-int/lit8 v2, v2, 0x7

    .line 971
    .local v2, "serial":I
    aget-byte v3, p1, v1

    shr-int/lit8 v3, v3, 0x4

    const/16 v7, 0xf

    and-int/2addr v3, v7

    .line 973
    .local v3, "times":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Receiver mcu KEY_S:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-byte v5, p1, v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v5

    const-string v9, "0x%02x"

    invoke-static {v9, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " KEY_R:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "mv E_UART:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v8, 0xd

    aget-byte v8, p1, v8

    .line 975
    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    const/16 v9, 0xc

    aget-byte v9, p1, v9

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    filled-new-array {v8, v9}, [Ljava/lang/Object;

    move-result-object v8

    .line 974
    const-string v9, "0x%02x%02x"

    invoke-static {v9, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " E_PPM:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-byte v1, p1, v1

    .line 976
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const/16 v8, 0x10

    aget-byte v8, p1, v8

    .line 977
    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    aget-byte v7, p1, v7

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    const/16 v9, 0xe

    aget-byte v9, p1, v9

    invoke-static {v9}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    filled-new-array {v1, v8, v7, v9}, [Ljava/lang/Object;

    move-result-object v1

    .line 976
    const-string v7, "0x%02x%02x%02x%02x"

    invoke-static {v7, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " Serial:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " PowerUp:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " OCP_F:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v4, p1, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 973
    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    return-void
.end method

.method private parseKeyboardUpdateFinishInfo([B)V
    .locals 6
    .param p1, "data"    # [B

    .line 887
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mgetTriggerWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    .line 888
    .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    const/4 v1, 0x6

    aget-byte v2, p1, v1

    if-nez v2, :cond_0

    .line 889
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    goto :goto_0

    .line 890
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    if-eqz v2, :cond_1

    .line 891
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upgrade finished Info Response error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v1, p1, v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    const v5, 0x110f024d

    invoke-interface {v2, v3, v1, v4, v5}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;ZI)V

    .line 894
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 896
    :cond_1
    :goto_0
    return-void
.end method

.method private parseKeyboardUpdateInfo([B)V
    .locals 6
    .param p1, "data"    # [B

    .line 819
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mgetTriggerWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    .line 820
    .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    const/4 v1, 0x6

    aget-byte v2, p1, v1

    const-string v3, "MiuiPadKeyboard_IICNodeHelper"

    if-nez v2, :cond_2

    .line 821
    const-string v1, "Receiver Keyboard/TouchPad Upgrade Mode"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 823
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v3

    invoke-interface {v1, v3, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaStateChange(BI)V

    goto :goto_0

    .line 825
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 826
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v3

    invoke-interface {v1, v3, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaStateChange(BI)V

    .line 829
    :cond_1
    :goto_0
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    goto :goto_1

    .line 830
    :cond_2
    aget-byte v2, p1, v1

    const/4 v4, 0x7

    if-ne v2, v4, :cond_3

    .line 831
    const-string v1, "The Keyboard Upgrade not match devices"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    goto :goto_1

    .line 834
    :cond_3
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 835
    const-string/jumbo v2, "stop upgrade keyboard and touch pad"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upgrade Info Response error"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v1, p1, v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 838
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->shutdownAllCommandQueue()V

    .line 840
    :goto_1
    return-void
.end method

.method private parseKeyboardUpdatePackageInfo([B)V
    .locals 8
    .param p1, "data"    # [B

    .line 843
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mgetTriggerWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    .line 844
    .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    const/4 v1, 0x6

    aget-byte v2, p1, v1

    const/4 v3, 0x1

    if-nez v2, :cond_5

    .line 846
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v2, v4

    const/16 v4, 0x9

    aget-byte v4, p1, v4

    const/16 v5, 0x8

    shl-int/2addr v4, v5

    const v6, 0xff00

    and-int/2addr v4, v6

    add-int/2addr v2, v4

    aget-byte v4, p1, v5

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x34

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;I)V

    .line 849
    const/4 v1, 0x0

    .line 850
    .local v1, "progress":F
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v2

    const/16 v4, 0x38

    const/high16 v5, 0x42c80000    # 100.0f

    const v6, 0x461c4000    # 10000.0f

    const/16 v7, 0x40

    if-ne v2, v4, :cond_1

    .line 851
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPacketTotal(I)I

    move-result v4

    sub-int/2addr v4, v3

    if-ne v2, v4, :cond_0

    .line 853
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmIsBinDataOK(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V

    .line 855
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    move-result-object v3

    .line 857
    invoke-virtual {v3, v7}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPacketTotal(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v6

    div-float/2addr v2, v5

    .line 855
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v1, v2

    goto :goto_0

    .line 860
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v2

    if-ne v2, v7, :cond_3

    .line 861
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPacketTotal(I)I

    move-result v4

    sub-int/2addr v4, v3

    if-ne v2, v4, :cond_2

    .line 863
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmIsBinDataOK(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V

    .line 865
    :cond_2
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    move-result-object v3

    .line 867
    invoke-virtual {v3, v7}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPacketTotal(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v6

    div-float/2addr v2, v5

    .line 865
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v1, v2

    goto :goto_0

    .line 870
    :cond_3
    const-string v2, "MiuiPadKeyboard_IICNodeHelper"

    const-string v3, "not match command worker, stop send pkg info"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 874
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponseForPackage([BI)V

    .line 875
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    if-eqz v2, :cond_4

    .line 876
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v3

    invoke-interface {v2, v3, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaProgress(BF)V

    .line 878
    .end local v1    # "progress":F
    :cond_4
    goto :goto_1

    .line 879
    :cond_5
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Keyboard Bin Package Info Response error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-byte v1, p1, v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v5, 0x110f024d

    invoke-interface {v2, v4, v1, v3, v5}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;ZI)V

    .line 882
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 884
    :goto_1
    return-void
.end method

.method private parseKeyboardUpgradeFlash([B)V
    .locals 6
    .param p1, "data"    # [B

    .line 899
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mgetTriggerWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    .line 900
    .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    const/4 v1, 0x6

    aget-byte v2, p1, v1

    if-nez v2, :cond_0

    .line 901
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    .line 902
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaStateChange(BI)V

    goto :goto_0

    .line 904
    :cond_0
    aget-byte v2, p1, v1

    const/4 v3, 0x7

    if-ne v2, v3, :cond_1

    .line 905
    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    const-string v2, "Keyboard is busy upgrading"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 907
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Keyboard Flash Info Response error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v1, p1, v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    const v5, 0x110f024d

    invoke-interface {v2, v3, v1, v4, v5}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;ZI)V

    .line 910
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 912
    :goto_0
    return-void
.end method

.method private parseKeyboardVersionInfo([B)V
    .locals 7
    .param p1, "data"    # [B

    .line 736
    array-length v0, p1

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 737
    return-void

    .line 739
    :cond_0
    const/4 v0, 0x0

    .line 740
    .local v0, "keyboardType":I
    const/4 v1, 0x2

    aget-byte v2, p1, v1

    const/16 v3, 0x38

    const/16 v4, 0x8

    const/16 v5, 0x9

    const-string v6, "%02x"

    if-ne v2, v3, :cond_8

    .line 742
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 744
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Keyboard_Upgrade"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 746
    const/4 v1, 0x5

    aget-byte v3, p1, v1

    if-eq v3, v1, :cond_5

    .line 747
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v5, p1, v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v5

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, p1, v4

    .line 748
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    .line 747
    invoke-static {v6, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmTouchPadVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V

    .line 749
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v3, v3, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmTouchPadVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x28

    invoke-interface {v1, v4, v3}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onUpdateVersion(ILjava/lang/String;)V

    .line 752
    const/16 v1, 0xd

    aget-byte v3, p1, v1

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->hasTouchpad(B)Z

    move-result v3

    .line 753
    .local v3, "hasTouchPad":Z
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v4, v4, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    aget-byte v5, p1, v1

    invoke-interface {v4, v5, v3}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onUpdateKeyboardType(BZ)V

    .line 754
    if-eqz v3, :cond_1

    .line 755
    const/16 v4, 0x200

    goto :goto_0

    :cond_1
    const/16 v4, 0x100

    :goto_0
    or-int/2addr v0, v4

    .line 756
    aget-byte v1, p1, v1

    const/16 v4, 0xe

    aget-byte v4, p1, v4

    invoke-direct {p0, v1, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->getSuitableBinName(BB)V

    .line 757
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const/16 v4, 0x11

    aget-byte v4, p1, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    move v2, v5

    :cond_2
    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmIsBleKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V

    .line 759
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v1

    .line 760
    .local v1, "commonConfig":Lcom/android/server/input/config/InputCommonConfig;
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B

    move-result v2

    const/16 v4, 0x20

    if-eq v2, v4, :cond_3

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B

    move-result v2

    const/16 v4, 0x21

    if-eq v2, v4, :cond_3

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B

    move-result v2

    const/16 v4, 0x10

    if-ne v2, v4, :cond_4

    .line 763
    :cond_3
    const v2, 0x3d8f5c29    # 0.07f

    invoke-virtual {v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setTopGestureHotZoneHeightRate(F)V

    .line 764
    const v2, 0x3d75c28f    # 0.06f

    invoke-virtual {v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setSlidGestureHotZoneWidthRate(F)V

    .line 766
    :cond_4
    invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 768
    .end local v1    # "commonConfig":Lcom/android/server/input/config/InputCommonConfig;
    .end local v3    # "hasTouchPad":Z
    :cond_5
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".bin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 769
    const/16 v1, 0xa

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 770
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardType(I)V

    .line 771
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B

    move-result v1

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isKeyboardSupportMuteLight(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 772
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->registerMuteLightReceiver()V

    .line 775
    :cond_6
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "notify_keyboard_info_changed"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 776
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardVersionWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 778
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardVersionWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    .line 780
    :cond_7
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x6

    aget-byte v3, p1, v3

    .line 781
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    .line 780
    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmKeyboardVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V

    .line 782
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x14

    invoke-interface {v1, v3, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onUpdateVersion(ILjava/lang/String;)V

    goto :goto_1

    .line 785
    :cond_8
    aget-byte v1, p1, v1

    const/16 v2, 0x39

    if-ne v1, v2, :cond_9

    .line 786
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v3, p1, v5

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v3, p1, v4

    .line 787
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    invoke-static {v6, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmKeyboardFlashVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V

    .line 788
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardFlashVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v1, v3, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onUpdateVersion(ILjava/lang/String;)V

    .line 792
    :cond_9
    :goto_1
    return-void
.end method


# virtual methods
.method public responseFromKeyboard([B)V
    .locals 7
    .param p1, "data"    # [B

    .line 577
    array-length v0, p1

    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception Data:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, p1

    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    return-void

    .line 581
    :cond_0
    const/4 v0, 0x4

    aget-byte v0, p1, v0

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v5, 0x7

    const/4 v6, 0x1

    sparse-switch v0, :sswitch_data_0

    .line 729
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Other Command:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, p1

    .line 730
    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 729
    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 690
    :sswitch_0
    aget-byte v0, p1, v2

    if-ne v0, v6, :cond_f

    .line 691
    const-string v0, "Receiver Nfc Device"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onNFCTouched()V

    goto/16 :goto_0

    .line 634
    :sswitch_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver GSensor status"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 635
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseGsensorDataInfo([B)V

    .line 636
    goto/16 :goto_0

    .line 703
    :sswitch_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmIsBleKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 704
    goto/16 :goto_0

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x13

    aget-byte v2, p1, v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x14

    aget-byte v4, p1, v4

    .line 707
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x15

    aget-byte v4, p1, v4

    .line 708
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x16

    aget-byte v4, p1, v4

    .line 709
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x17

    aget-byte v4, p1, v4

    .line 710
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x18

    aget-byte v2, p1, v2

    .line 711
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmKeyboardBleAdd(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V

    .line 712
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msendBroadCast2Ble(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    .line 713
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardBleAdd(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x2

    const-string v3, "miui_keyboard_address"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 716
    goto/16 :goto_0

    .line 642
    :sswitch_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmAuthCommandResponse(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[B)V

    .line 643
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmCountDownLatch(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 644
    goto/16 :goto_0

    .line 664
    :sswitch_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v5, "Receiver Keyboard sleep status"

    invoke-static {v0, p1, v5}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 665
    aget-byte v0, p1, v4

    if-eq v0, v6, :cond_2

    .line 666
    const-string v0, "parse KeyboardSleepStatus fail!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    goto/16 :goto_0

    .line 670
    :cond_2
    aget-byte v0, p1, v2

    if-nez v0, :cond_3

    .line 671
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v0, v6}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onKeyboardSleepStatusChanged(Z)V

    goto/16 :goto_0

    .line 672
    :cond_3
    aget-byte v0, p1, v2

    if-ne v0, v6, :cond_f

    .line 673
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v0, v3}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onKeyboardSleepStatusChanged(Z)V

    goto/16 :goto_0

    .line 677
    :sswitch_5
    aget-byte v0, p1, v4

    if-eq v0, v6, :cond_4

    .line 678
    const-string v0, "parse keyboard power fail!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    goto/16 :goto_0

    .line 682
    :cond_4
    aget-byte v0, p1, v2

    if-ne v0, v6, :cond_5

    .line 683
    const-string v0, "Set keyboard power on"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 686
    :cond_5
    const-string v0, "Set keyboard power low"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    goto/16 :goto_0

    .line 718
    :sswitch_6
    aget-byte v0, p1, v2

    if-nez v0, :cond_6

    .line 719
    const-string v0, "keyboard has restart! "

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    goto/16 :goto_0

    .line 722
    :cond_6
    aget-byte v0, p1, v2

    const/16 v3, 0x64

    if-eq v0, v3, :cond_7

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyboard reauth data error! "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    goto/16 :goto_0

    .line 726
    :cond_7
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->requestReAuth()V

    .line 727
    goto/16 :goto_0

    .line 696
    :sswitch_7
    const/4 v0, 0x0

    .line 697
    .local v0, "isEnable":Z
    aget-byte v1, p1, v4

    if-ne v1, v6, :cond_8

    .line 698
    const/4 v0, 0x1

    .line 700
    :cond_8
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v1, v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v1, v0}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onKeyboardEnableStateChanged(Z)V

    .line 701
    goto/16 :goto_0

    .line 646
    .end local v0    # "isEnable":Z
    :sswitch_8
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v5, "Receiver Keyboard recover firmware status"

    invoke-static {v0, p1, v5}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 647
    aget-byte v0, p1, v4

    if-eq v0, v6, :cond_9

    .line 648
    const-string v0, "parse keyboard recover firmware status fail!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    goto/16 :goto_0

    .line 651
    :cond_9
    aget-byte v0, p1, v2

    const/16 v1, 0x36

    if-ne v0, v1, :cond_f

    .line 652
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v1

    .line 653
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110f0241

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 652
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 655
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 656
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmIsBleKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 658
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msendBroadCast2Ble(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V

    .line 659
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msendBroadCast2Ble(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    goto/16 :goto_0

    .line 604
    :sswitch_9
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver Keyboard Flash"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 605
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpgradeFlash([B)V

    .line 606
    goto/16 :goto_0

    .line 600
    :sswitch_a
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver Keyboard Upgrade End"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 601
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpdateFinishInfo([B)V

    .line 602
    goto/16 :goto_0

    .line 590
    :sswitch_b
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver Keyboard Upgrade"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 591
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpdateInfo([B)V

    .line 592
    goto/16 :goto_0

    .line 584
    :sswitch_c
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver keyBoard/Flash Version"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 586
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardVersionInfo([B)V

    .line 587
    goto/16 :goto_0

    .line 617
    :sswitch_d
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v2, "Receiver 803 report read Keyboard status"

    invoke-static {v0, p1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 618
    aget-byte v0, p1, v5

    if-nez v0, :cond_a

    .line 619
    aget-byte v0, p1, v4

    sget-object v2, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_BACKLIGHT:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v2

    if-ne v0, v2, :cond_f

    .line 620
    const-string/jumbo v0, "set backLight success"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 623
    :cond_a
    aget-byte v0, p1, v5

    if-ne v0, v6, :cond_b

    .line 624
    const-string v0, "Command word not supported/malformed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 625
    :cond_b
    aget-byte v0, p1, v5

    const/4 v2, 0x2

    if-ne v0, v2, :cond_c

    .line 626
    const-string v0, "Keyboard is disconnection, Command write fail"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 627
    :cond_c
    aget-byte v0, p1, v5

    const/4 v2, 0x3

    if-ne v0, v2, :cond_d

    .line 628
    const-string v0, "MCU is busy,The Command lose possibility"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 630
    :cond_d
    const-string v0, "Other Error"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    goto :goto_0

    .line 608
    :sswitch_e
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v2, "Receiver Keyboard Search Status"

    invoke-static {v0, p1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 609
    aget-byte v0, p1, v5

    if-nez v0, :cond_e

    .line 610
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardStatus([B)V

    goto :goto_0

    .line 612
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "read Mcu Status error : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v2, p1, v5

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "0x%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    goto :goto_0

    .line 595
    :sswitch_f
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver Keyboard Bin Package"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 596
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpdatePackageInfo([B)V

    .line 597
    nop

    .line 733
    :cond_f
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6f -> :sswitch_f
        -0x5e -> :sswitch_e
        -0x10 -> :sswitch_d
        0x1 -> :sswitch_c
        0x2 -> :sswitch_b
        0x4 -> :sswitch_a
        0x6 -> :sswitch_9
        0x20 -> :sswitch_8
        0x22 -> :sswitch_7
        0x24 -> :sswitch_6
        0x25 -> :sswitch_5
        0x28 -> :sswitch_4
        0x31 -> :sswitch_3
        0x32 -> :sswitch_3
        0x33 -> :sswitch_3
        0x34 -> :sswitch_3
        0x35 -> :sswitch_3
        0x52 -> :sswitch_2
        0x64 -> :sswitch_1
        0x68 -> :sswitch_0
    .end sparse-switch
.end method

.method public responseFromMCU([B)V
    .locals 6
    .param p1, "data"    # [B

    .line 984
    array-length v0, p1

    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 985
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception Data:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, p1

    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 986
    return-void

    .line 988
    :cond_0
    const/4 v0, 0x4

    aget-byte v0, p1, v0

    const/4 v3, 0x2

    const/16 v4, 0x18

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 1069
    :sswitch_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver MCU Flash"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 1070
    aget-byte v0, p1, v2

    if-nez v0, :cond_1

    .line 1071
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    goto/16 :goto_0

    .line 1073
    :cond_1
    aget-byte v0, p1, v2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_8

    .line 1074
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 1075
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MCU Flash error:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    goto/16 :goto_0

    .line 1057
    :sswitch_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver MCU UpEnd"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 1059
    aget-byte v0, p1, v2

    if-nez v0, :cond_2

    .line 1060
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    goto/16 :goto_0

    .line 1062
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UpEnd error:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 1065
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 1067
    goto/16 :goto_0

    .line 1041
    :sswitch_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver MCU Reset"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 1042
    aget-byte v0, p1, v2

    if-nez v0, :cond_3

    .line 1043
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmMCUVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V

    .line 1044
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    .line 1045
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v0, v4, v3}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaStateChange(BI)V

    goto/16 :goto_0

    .line 1049
    :cond_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reset error:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 1052
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 1054
    goto/16 :goto_0

    .line 998
    :sswitch_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v3, "Receiver MCU Upgrade"

    invoke-static {v0, p1, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 999
    aget-byte v0, p1, v2

    if-nez v0, :cond_4

    .line 1000
    const-string v0, "receiver MCU Upgrade start"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    const/4 v1, 0x0

    invoke-interface {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaStateChange(BI)V

    .line 1004
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V

    goto/16 :goto_0

    .line 1006
    :cond_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MCU Upgrade info error"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 1009
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 1011
    goto/16 :goto_0

    .line 991
    :sswitch_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver get MCU version"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 992
    aget-byte v0, p1, v2

    if-ne v0, v3, :cond_8

    .line 993
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->onMcuVersionResponse([B)V

    goto/16 :goto_0

    .line 1013
    :sswitch_5
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    const-string v1, "Receiver MCU Bin Package"

    invoke-static {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V

    .line 1014
    aget-byte v0, p1, v2

    if-nez v0, :cond_7

    .line 1015
    const/16 v0, 0xa

    aget-byte v0, p1, v0

    shl-int/lit8 v0, v0, 0x10

    const/high16 v1, 0xff0000

    and-int/2addr v0, v1

    const/16 v1, 0x9

    aget-byte v1, p1, v1

    const/16 v2, 0x8

    shl-int/2addr v1, v2

    const v3, 0xff00

    and-int/2addr v1, v3

    add-int/2addr v0, v1

    aget-byte v1, p1, v2

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x34

    .line 1018
    .local v0, "nextIndex":I
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;I)V

    .line 1019
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPacketTotal(I)I

    move-result v2

    const/4 v5, 0x1

    sub-int/2addr v2, v5

    if-ne v1, v2, :cond_5

    .line 1020
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1, v5}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmIsBinDataOK(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V

    .line 1022
    :cond_5
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponseForPackage([BI)V

    .line 1024
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    move-result-object v2

    .line 1025
    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPacketTotal(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const v2, 0x461c4000    # 10000.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    .line 1024
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    .line 1027
    .local v1, "progress":F
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    if-eqz v2, :cond_6

    .line 1028
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v2, v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    invoke-interface {v2, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaProgress(BF)V

    .line 1032
    .end local v0    # "nextIndex":I
    .end local v1    # "progress":F
    :cond_6
    goto :goto_0

    .line 1033
    :cond_7
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MCU Package info error:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 1036
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V

    .line 1038
    nop

    .line 1084
    :cond_8
    :goto_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6f -> :sswitch_5
        0x1 -> :sswitch_4
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch
.end method
