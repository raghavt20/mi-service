.class public Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;
.super Ljava/lang/Object;
.source "TouchPadUpgradeUtil.java"


# static fields
.field private static final START_INDEX_FOR_176:I = 0x0

.field private static final START_INDEX_FOR_179:I = 0xfc0

.field private static final TAG:Ljava/lang/String; = "TouchPadKeyboardUpgradeUtil"


# instance fields
.field public mBinCheckSum:I

.field public mBinCheckSumByte:[B

.field public mBinLength:I

.field public mBinLengthByte:[B

.field public mBinStartAddressByte:[B

.field private mBinStartFlag:Ljava/lang/String;

.field public mBinVersion:[B

.field public mBinVersionStr:Ljava/lang/String;

.field private mFileBuf:[B

.field private mIsBleDevice:Z

.field public mPath:Ljava/lang/String;

.field public mValid:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;B)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "type"    # B

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x4

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLengthByte:[B

    .line 27
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSumByte:[B

    .line 29
    const/4 v1, 0x2

    new-array v2, v1, [B

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersion:[B

    .line 31
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartAddressByte:[B

    .line 35
    const-string v0, "TouchPadKeyboardUpgradeUtil"

    if-nez p2, :cond_0

    .line 36
    const-string v1, "UpgradeOta file Path is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void

    .line 39
    :cond_0
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mPath:Ljava/lang/String;

    .line 41
    if-ne p3, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPad UpgradeOta file Path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->ReadUpgradeFile(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    .line 45
    if-eqz v1, :cond_2

    .line 46
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->parseOtaFile()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mValid:Z

    goto :goto_1

    .line 48
    :cond_2
    const-string v1, "UpgradeOta file buff is null or length low than 6000"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :goto_1
    return-void
.end method

.method private ReadUpgradeFile(Ljava/lang/String;)[B
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "fileBuf":[B
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 57
    .local v1, "mFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->readFileToBuf(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_0
    const-string v2, "TouchPadKeyboardUpgradeUtil"

    const-string v3, "=== The Upgrade bin file does not exist."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_0
    return-object v0
.end method

.method private getSum([BII)B
    .locals 3
    .param p1, "data"    # [B
    .param p2, "start"    # I
    .param p3, "length"    # I

    .line 277
    const/4 v0, 0x0

    .line 278
    .local v0, "sum":B
    move v1, p2

    .local v1, "i":I
    :goto_0
    add-int v2, p2, p3

    if-ge v1, v2, :cond_0

    .line 279
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v2, v0

    int-to-byte v0, v2

    .line 278
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 281
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method private getSumInt([BII)I
    .locals 3
    .param p1, "data"    # [B
    .param p2, "start"    # I
    .param p3, "length"    # I

    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "sum":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    add-int v2, p2, p3

    if-ge v1, v2, :cond_0

    .line 288
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v0, v2

    .line 287
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 290
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method private parseOtaFile()Z
    .locals 17

    .line 93
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/16 v1, 0xfc0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 94
    .local v1, "startIndex":I
    :goto_0
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    const-string v4, "TouchPadKeyboardUpgradeUtil"

    if-eqz v3, :cond_3

    array-length v5, v3

    const/16 v6, 0x40

    if-le v5, v6, :cond_3

    .line 95
    move v5, v1

    .line 97
    .local v5, "fileHeadBaseAddr":I
    const/16 v7, 0x8

    new-array v8, v7, [B

    .line 98
    .local v8, "binStartFlagByte":[B
    array-length v9, v8

    invoke-static {v3, v5, v8, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    invoke-static {v8}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartFlag:Ljava/lang/String;

    .line 100
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bin Start Flag: "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v9, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartFlag:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    add-int/2addr v5, v7

    .line 104
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    iget-object v9, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLengthByte:[B

    array-length v10, v9

    invoke-static {v3, v5, v9, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLengthByte:[B

    const/4 v9, 0x3

    aget-byte v10, v3, v9

    shl-int/lit8 v10, v10, 0x18

    const/high16 v11, -0x1000000

    and-int/2addr v10, v11

    const/4 v12, 0x2

    aget-byte v13, v3, v12

    shl-int/lit8 v13, v13, 0x10

    const/high16 v14, 0xff0000

    and-int/2addr v13, v14

    add-int/2addr v10, v13

    const/4 v13, 0x1

    aget-byte v15, v3, v13

    shl-int/2addr v15, v7

    const v16, 0xff00

    and-int v15, v15, v16

    add-int/2addr v10, v15

    aget-byte v3, v3, v2

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v10, v3

    iput v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I

    .line 108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bin Lenght: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    add-int/lit8 v5, v5, 0x4

    .line 112
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    iget-object v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSumByte:[B

    array-length v15, v10

    invoke-static {v3, v5, v10, v2, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSumByte:[B

    aget-byte v10, v3, v9

    shl-int/lit8 v10, v10, 0x18

    and-int/2addr v10, v11

    aget-byte v11, v3, v12

    shl-int/lit8 v11, v11, 0x10

    and-int/2addr v11, v14

    add-int/2addr v10, v11

    aget-byte v11, v3, v13

    shl-int/lit8 v7, v11, 0x8

    and-int v7, v7, v16

    add-int/2addr v10, v7

    aget-byte v3, v3, v2

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v10, v3

    iput v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I

    .line 116
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin CheckSum: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    add-int/lit8 v5, v5, 0x4

    .line 120
    const/16 v3, 0x32

    new-array v3, v3, [B

    iput-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersion:[B

    .line 121
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    array-length v10, v3

    invoke-static {v7, v5, v3, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersion:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v7

    const-string v10, "%02x"

    invoke-static {v10, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersion:[B

    aget-byte v7, v7, v13

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v7

    invoke-static {v10, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    .line 123
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin Version : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    add-int/lit8 v7, v1, 0x40

    array-length v10, v3

    sub-int/2addr v10, v1

    sub-int/2addr v10, v6

    invoke-direct {v0, v3, v7, v10}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSumInt([BII)I

    move-result v3

    .line 127
    .local v3, "sum1":I
    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I

    const-string v7, "/"

    if-eq v3, v6, :cond_1

    .line 128
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bin Check Check Sum Error:"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    return v2

    .line 133
    :cond_1
    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    const/16 v10, 0x3f

    invoke-direct {v0, v6, v1, v10}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B

    move-result v6

    .line 134
    .local v6, "sum2":B
    iget-object v10, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    add-int/lit8 v11, v1, 0x3f

    aget-byte v10, v10, v11

    if-eq v6, v10, :cond_2

    .line 135
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bin Check Head Sum Error:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    add-int/lit8 v10, v1, 0x3f

    aget-byte v9, v9, v10

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    return v2

    .line 139
    :cond_2
    iget-object v4, v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartAddressByte:[B

    aput-byte v2, v4, v2

    .line 140
    const/16 v7, -0x80

    aput-byte v7, v4, v13

    .line 141
    const/4 v7, 0x6

    aput-byte v7, v4, v12

    .line 142
    aput-byte v2, v4, v9

    .line 143
    return v13

    .line 145
    .end local v3    # "sum1":I
    .end local v5    # "fileHeadBaseAddr":I
    .end local v6    # "sum2":B
    .end local v8    # "binStartFlagByte":[B
    :cond_3
    const-string v3, "TouchPad Parse OtaFile err"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    return v2
.end method

.method private readFileToBuf(Ljava/lang/String;)[B
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "fileBuf":[B
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .local v1, "fileStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v2

    .line 76
    .local v2, "fileLength":I
    new-array v3, v2, [B

    move-object v0, v3

    .line 77
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 80
    .end local v1    # "fileStream":Ljava/io/InputStream;
    goto :goto_1

    .line 74
    .end local v2    # "fileLength":I
    .restart local v1    # "fileStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "fileBuf":[B
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;
    .end local p1    # "fileName":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 78
    .end local v1    # "fileStream":Ljava/io/InputStream;
    .restart local v0    # "fileBuf":[B
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;
    .restart local p1    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File is not exit!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "TouchPadKeyboardUpgradeUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-object v0
.end method


# virtual methods
.method public checkVersion(Ljava/lang/String;)Z
    .locals 2
    .param p1, "version"    # Ljava/lang/String;

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TouchPadKeyboardUpgradeUtil"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    .line 88
    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 87
    :goto_0
    return v0
.end method

.method public getBinPackInfo(BI)[B
    .locals 9
    .param p1, "target"    # B
    .param p2, "offset"    # I

    .line 178
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 179
    .local v0, "bytes":[B
    const/16 v1, 0x34

    .line 180
    .local v1, "binlength":I
    const/16 v2, -0x56

    const/4 v3, 0x0

    aput-byte v2, v0, v3

    .line 181
    const/4 v2, 0x1

    const/16 v4, 0x42

    aput-byte v4, v0, v2

    .line 182
    const/16 v5, 0x32

    const/4 v6, 0x2

    aput-byte v5, v0, v6

    .line 183
    const/4 v5, 0x3

    aput-byte v3, v0, v5

    .line 184
    const/16 v3, 0x4f

    const/4 v7, 0x4

    aput-byte v3, v0, v7

    .line 185
    const/4 v3, 0x5

    const/16 v8, 0x30

    aput-byte v8, v0, v3

    .line 186
    const/4 v3, 0x6

    const/16 v8, -0x80

    aput-byte v8, v0, v3

    .line 187
    const/4 v3, 0x7

    aput-byte p1, v0, v3

    .line 188
    const/16 v3, 0x8

    const/16 v8, 0x11

    aput-byte v8, v0, v3

    .line 189
    const/16 v3, 0x9

    const/16 v8, 0x38

    aput-byte v8, v0, v3

    .line 190
    invoke-static {p2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v3

    .line 191
    .local v3, "offsetB":[B
    const/16 v8, 0xa

    aget-byte v5, v3, v5

    aput-byte v5, v0, v8

    .line 192
    const/16 v5, 0xb

    aget-byte v6, v3, v6

    aput-byte v6, v0, v5

    .line 193
    const/16 v5, 0xc

    aget-byte v2, v3, v2

    aput-byte v2, v0, v5

    .line 194
    const/16 v2, 0xd

    const/16 v5, 0x34

    aput-byte v5, v0, v2

    .line 195
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z

    const/16 v5, 0xe

    if-eqz v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    array-length v6, v2

    add-int/lit16 v8, p2, 0x1000

    add-int/2addr v8, v1

    if-ge v6, v8, :cond_0

    .line 197
    array-length v6, v2

    add-int/lit16 v8, p2, 0x1000

    sub-int v1, v6, v8

    .line 199
    :cond_0
    add-int/lit16 v6, p2, 0x1000

    invoke-static {v2, v6, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 201
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mFileBuf:[B

    array-length v6, v2

    add-int v8, p2, v1

    if-ge v6, v8, :cond_2

    .line 202
    array-length v6, v2

    sub-int v1, v6, p2

    .line 204
    :cond_2
    invoke-static {v2, p2, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    :goto_0
    const/16 v2, 0x3e

    invoke-direct {p0, v0, v7, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v4

    .line 209
    return-object v0
.end method

.method public getBinPacketTotal(I)I
    .locals 3
    .param p1, "length"    # I

    .line 255
    const/4 v0, 0x0

    .line 256
    .local v0, "totle":I
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mIsBleDevice:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I

    add-int/lit8 v1, v1, 0x40

    .line 257
    .local v1, "binLength":I
    :goto_0
    sparse-switch p1, :sswitch_data_0

    goto :goto_1

    .line 265
    :sswitch_0
    div-int/lit8 v0, v1, 0x34

    .line 266
    iget v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I

    rem-int/lit8 v2, v2, 0x34

    if-eqz v2, :cond_1

    .line 267
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 259
    :sswitch_1
    div-int/lit8 v0, v1, 0x14

    .line 260
    iget v2, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLength:I

    rem-int/lit8 v2, v2, 0x14

    if-eqz v2, :cond_1

    .line 261
    add-int/lit8 v0, v0, 0x1

    .line 272
    :cond_1
    :goto_1
    return v0

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public getUpEndInfo(B)[B
    .locals 7
    .param p1, "target"    # B

    .line 214
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 215
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 216
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 217
    const/16 v1, 0x32

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 218
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 220
    const/16 v1, 0x4e

    const/4 v4, 0x4

    aput-byte v1, v0, v4

    .line 221
    const/4 v1, 0x5

    const/16 v5, 0x30

    aput-byte v5, v0, v1

    .line 222
    const/4 v1, 0x6

    const/16 v5, -0x80

    aput-byte v5, v0, v1

    .line 223
    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 224
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 225
    const/16 v1, 0x9

    const/16 v5, 0xe

    aput-byte v5, v0, v1

    .line 226
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLengthByte:[B

    const/16 v6, 0xa

    invoke-static {v1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartAddressByte:[B

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSumByte:[B

    const/16 v5, 0x12

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 229
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersion:[B

    const/16 v5, 0x16

    invoke-static {v1, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 230
    const/16 v1, 0x14

    invoke-direct {p0, v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B

    move-result v1

    const/16 v2, 0x18

    aput-byte v1, v0, v2

    .line 231
    return-object v0
.end method

.method public getUpFlashInfo(B)[B
    .locals 5
    .param p1, "target"    # B

    .line 236
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 237
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 238
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 239
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 240
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 242
    const/16 v1, 0x4f

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 243
    const/4 v1, 0x5

    const/16 v4, 0x30

    aput-byte v4, v0, v1

    .line 244
    const/16 v1, -0x80

    const/4 v4, 0x6

    aput-byte v1, v0, v4

    .line 245
    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 246
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 247
    const/16 v1, 0x9

    aput-byte v3, v0, v1

    .line 248
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartAddressByte:[B

    const/16 v4, 0xa

    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    const/16 v1, 0xe

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 250
    return-object v0
.end method

.method public getUpgradeInfo(B)[B
    .locals 8
    .param p1, "target"    # B

    .line 152
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 153
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 154
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 155
    const/16 v1, 0x32

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 156
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 158
    const/16 v4, 0x4f

    const/4 v5, 0x4

    aput-byte v4, v0, v5

    .line 159
    const/4 v4, 0x5

    const/16 v6, 0x30

    aput-byte v6, v0, v4

    .line 160
    const/4 v4, 0x6

    const/16 v6, -0x80

    aput-byte v6, v0, v4

    .line 161
    const/4 v4, 0x7

    aput-byte p1, v0, v4

    .line 162
    const/16 v4, 0x8

    aput-byte v3, v0, v4

    .line 163
    const/16 v4, 0x9

    const/16 v6, 0x13

    aput-byte v6, v0, v4

    .line 164
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinLengthByte:[B

    const/16 v6, 0xa

    invoke-static {v4, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinStartAddressByte:[B

    const/16 v6, 0xe

    invoke-static {v4, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinCheckSumByte:[B

    const/16 v6, 0x12

    invoke-static {v4, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 167
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mBinVersion:[B

    const/16 v7, 0x16

    invoke-static {v4, v2, v0, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 168
    const/16 v2, 0x18

    const/16 v3, 0x2b

    aput-byte v3, v0, v2

    .line 169
    const/16 v2, 0x19

    aput-byte v6, v0, v2

    .line 170
    const/16 v3, 0x1a

    const/16 v4, 0x20

    aput-byte v4, v0, v3

    .line 171
    const/16 v3, 0x1b

    aput-byte v1, v0, v3

    .line 172
    const/16 v1, 0x1d

    invoke-direct {p0, v0, v5, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 173
    return-object v0
.end method

.method public isValidFile()Z
    .locals 1

    .line 67
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->mValid:Z

    return v0
.end method
