.class public Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;
.super Ljava/lang/Object;
.source "McuUpgradeUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "IIC_McuUpgradeUtil"


# instance fields
.field public mBinCheckSum:I

.field public mBinCheckSumByte:[B

.field public mBinDeviceTypeByte:[B

.field public mBinFlashAddressByte:[B

.field public mBinLength:I

.field public mBinLengthByte:[B

.field public mBinPid:I

.field public mBinPidByte:[B

.field private final mBinRoDataByte:[B

.field public mBinRunStartAddress:I

.field public mBinRunStartAddressByte:[B

.field public mBinStartAddressByte:[B

.field public mBinVersion:Ljava/lang/String;

.field public mBinVid:I

.field public mBinVidByte:[B

.field private mContext:Landroid/content/Context;

.field private mFileBuf:[B

.field public mIsFileValid:Z

.field public mOtaFilepath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "otaFilepath"    # Ljava/lang/String;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x4

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRoDataByte:[B

    .line 24
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddressByte:[B

    .line 26
    const/4 v1, 0x2

    new-array v2, v1, [B

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVidByte:[B

    .line 28
    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPidByte:[B

    .line 31
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLengthByte:[B

    .line 33
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinStartAddressByte:[B

    .line 34
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSumByte:[B

    .line 36
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinFlashAddressByte:[B

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinDeviceTypeByte:[B

    .line 40
    const-string v0, "IIC_McuUpgradeUtil"

    if-nez p2, :cond_0

    .line 41
    const-string v1, "UpgradeOta file Path is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void

    .line 45
    :cond_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mContext:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mOtaFilepath:Ljava/lang/String;

    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpgradeOta file Path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mOtaFilepath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mOtaFilepath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->readUpgradeFile(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    .line 51
    if-eqz v1, :cond_1

    .line 52
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->parseOtaFile()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mIsFileValid:Z

    goto :goto_0

    .line 54
    :cond_1
    const-string v1, "UpgradeOta file buff is null or length low than 6000"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :goto_0
    return-void
.end method

.method private parseOtaFile()Z
    .locals 22

    .line 99
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    const-string v2, "IIC_McuUpgradeUtil"

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    array-length v4, v1

    const v5, 0x8000

    if-le v4, v5, :cond_2

    .line 100
    const/16 v4, 0x7fc0

    .line 102
    .local v4, "fileHeadBaseAddress":I
    const/4 v6, 0x7

    new-array v7, v6, [B

    .line 103
    .local v7, "binStartFlagByte":[B
    array-length v8, v7

    invoke-static {v1, v4, v7, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "binStartFlag":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bin Start Flag: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    add-int/2addr v4, v6

    .line 109
    const/4 v6, 0x2

    new-array v8, v6, [B

    .line 110
    .local v8, "chipIDByte":[B
    iget-object v9, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v10, v8

    invoke-static {v9, v4, v8, v3, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 111
    invoke-static {v8, v6}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v9

    .line 112
    .local v9, "binChipId":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bin Chip ID: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    add-int/2addr v4, v6

    .line 115
    iget-object v10, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v11, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRoDataByte:[B

    array-length v12, v11

    invoke-static {v10, v4, v11, v3, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bin RoData : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRoDataByte:[B

    const/4 v12, 0x4

    invoke-static {v11, v12}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    add-int/2addr v4, v12

    .line 120
    new-array v10, v12, [B

    .line 121
    .local v10, "rameCodeByte":[B
    iget-object v11, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v13, v10

    invoke-static {v11, v4, v10, v3, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Bin RameCode : "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v10, v12}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    add-int/2addr v4, v12

    .line 125
    new-array v11, v12, [B

    .line 126
    .local v11, "rameCodeLenByte":[B
    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v14, v11

    invoke-static {v13, v4, v11, v3, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 128
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Bin RameCodeLength : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v11, v12}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    add-int/2addr v4, v12

    .line 131
    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v14, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddressByte:[B

    array-length v15, v14

    invoke-static {v13, v4, v14, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 133
    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddressByte:[B

    const/4 v14, 0x3

    aget-byte v15, v13, v14

    const/16 v16, 0x18

    shl-int/lit8 v15, v15, 0x18

    const/high16 v17, -0x1000000

    and-int v15, v15, v17

    aget-byte v18, v13, v6

    const/16 v5, 0x10

    shl-int/lit8 v18, v18, 0x10

    const/high16 v19, 0xff0000

    and-int v18, v18, v19

    add-int v15, v15, v18

    const/16 v18, 0x1

    aget-byte v20, v13, v18

    shl-int/lit8 v20, v20, 0x8

    const v21, 0xff00

    and-int v20, v20, v21

    add-int v15, v15, v20

    aget-byte v13, v13, v3

    and-int/lit16 v13, v13, 0xff

    add-int/2addr v15, v13

    iput v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddress:I

    .line 137
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Bin Run Start Address: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinRunStartAddress:I

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    add-int/2addr v4, v12

    .line 140
    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPidByte:[B

    array-length v12, v15

    invoke-static {v13, v4, v15, v3, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 141
    iget-object v12, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPidByte:[B

    aget-byte v13, v12, v18

    shl-int/lit8 v13, v13, 0x8

    and-int v13, v13, v21

    aget-byte v12, v12, v3

    and-int/lit16 v12, v12, 0xff

    add-int/2addr v13, v12

    iput v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPid:I

    .line 142
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Bin Pid: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPid:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    add-int/2addr v4, v6

    .line 145
    iget-object v12, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVidByte:[B

    array-length v15, v13

    invoke-static {v12, v4, v13, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    iget-object v12, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVidByte:[B

    aget-byte v13, v12, v18

    shl-int/lit8 v13, v13, 0x8

    and-int v13, v13, v21

    aget-byte v12, v12, v3

    and-int/lit16 v12, v12, 0xff

    add-int/2addr v13, v12

    iput v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVid:I

    .line 147
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Bin Vid: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVid:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    add-int/2addr v4, v6

    .line 150
    add-int/2addr v4, v6

    .line 152
    new-array v12, v5, [B

    .line 153
    .local v12, "binVersionByte":[B
    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v15, v12

    invoke-static {v13, v4, v12, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    invoke-static {v12}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVersion:Ljava/lang/String;

    .line 156
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Bin Version : "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVersion:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    add-int/lit8 v4, v4, 0x11

    .line 159
    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v15, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinDeviceTypeByte:[B

    array-length v5, v15

    const/16 v6, 0x7fec

    invoke-static {v13, v6, v15, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLengthByte:[B

    array-length v13, v6

    invoke-static {v5, v4, v6, v3, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 164
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLengthByte:[B

    aget-byte v6, v5, v14

    shl-int/lit8 v6, v6, 0x18

    and-int v6, v6, v17

    const/4 v13, 0x2

    aget-byte v15, v5, v13

    const/16 v13, 0x10

    shl-int/2addr v15, v13

    and-int v13, v15, v19

    add-int/2addr v6, v13

    aget-byte v13, v5, v18

    shl-int/lit8 v13, v13, 0x8

    and-int v13, v13, v21

    add-int/2addr v6, v13

    aget-byte v5, v5, v3

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v6, v5

    iput v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I

    .line 168
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin Length: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v5, 0x4

    add-int/2addr v4, v5

    .line 171
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSumByte:[B

    array-length v13, v6

    invoke-static {v5, v4, v6, v3, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 173
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSumByte:[B

    aget-byte v6, v5, v14

    shl-int/lit8 v6, v6, 0x18

    and-int v6, v6, v17

    const/4 v13, 0x2

    aget-byte v15, v5, v13

    const/16 v13, 0x10

    shl-int/lit8 v13, v15, 0x10

    and-int v13, v13, v19

    add-int/2addr v6, v13

    aget-byte v13, v5, v18

    shl-int/lit8 v13, v13, 0x8

    and-int v13, v13, v21

    add-int/2addr v6, v13

    aget-byte v5, v5, v3

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v6, v5

    iput v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I

    .line 177
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin CheckSum: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v6, v5

    const v13, 0x8000

    sub-int/2addr v6, v13

    invoke-static {v5, v13, v6}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSumInt([BII)I

    move-result v5

    .line 182
    .local v5, "sum1":I
    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I

    const-string v13, "/"

    if-eq v5, v6, :cond_0

    .line 183
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Bin Check Check Sum Error:"

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return v3

    .line 188
    :cond_0
    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    const/16 v15, 0x7fc0

    const/16 v14, 0x3f

    invoke-static {v6, v15, v14}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v6

    .line 189
    .local v6, "sum2":B
    iget-object v14, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    const/16 v15, 0x7fff

    aget-byte v14, v14, v15

    if-eq v6, v14, :cond_1

    .line 190
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bin Check Head Sum Error:"

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v13, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    aget-byte v13, v13, v15

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const/4 v2, 0x0

    return v2

    .line 194
    :cond_1
    move v2, v3

    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinFlashAddressByte:[B

    aput-byte v2, v3, v2

    .line 195
    const/16 v13, -0x80

    aput-byte v13, v3, v18

    .line 196
    const/4 v13, 0x2

    aput-byte v2, v3, v13

    .line 197
    const/4 v14, 0x3

    aput-byte v2, v3, v14

    .line 199
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinStartAddressByte:[B

    aput-byte v2, v3, v2

    .line 200
    aput-byte v2, v3, v18

    .line 201
    aput-byte v2, v3, v13

    .line 202
    aput-byte v2, v3, v14

    .line 204
    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinDeviceTypeByte:[B

    aput-byte v16, v3, v2

    .line 205
    return v18

    .line 206
    .end local v1    # "binStartFlag":Ljava/lang/String;
    .end local v4    # "fileHeadBaseAddress":I
    .end local v5    # "sum1":I
    .end local v6    # "sum2":B
    .end local v7    # "binStartFlagByte":[B
    .end local v8    # "chipIDByte":[B
    .end local v9    # "binChipId":Ljava/lang/String;
    .end local v10    # "rameCodeByte":[B
    .end local v11    # "rameCodeLenByte":[B
    .end local v12    # "binVersionByte":[B
    :cond_2
    if-eqz v1, :cond_3

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "length = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method private readFileToBuf(Ljava/lang/String;)[B
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "fileBuf":[B
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .local v1, "fileStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v2

    .line 82
    .local v2, "fileLength":I
    new-array v3, v2, [B

    move-object v0, v3

    .line 83
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 86
    .end local v1    # "fileStream":Ljava/io/InputStream;
    goto :goto_1

    .line 80
    .end local v2    # "fileLength":I
    .restart local v1    # "fileStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "fileBuf":[B
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;
    .end local p1    # "fileName":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 84
    .end local v1    # "fileStream":Ljava/io/InputStream;
    .restart local v0    # "fileBuf":[B
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;
    .restart local p1    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 85
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File is not exit!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IIC_McuUpgradeUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-object v0
.end method

.method private readUpgradeFile(Ljava/lang/String;)[B
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    .line 62
    .local v0, "fileBuf":[B
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .local v1, "mFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->readFileToBuf(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_0
    const-string v2, "IIC_McuUpgradeUtil"

    const-string v3, "The Upgrade bin file does not exist."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :goto_0
    return-object v0
.end method


# virtual methods
.method public checkVersion(Ljava/lang/String;)Z
    .locals 2
    .param p1, "version"    # Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "get version :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , bin version:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IIC_McuUpgradeUtil"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVersion:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 93
    :goto_0
    return v0
.end method

.method public getBinPackInfo(I)[B
    .locals 9
    .param p1, "offset"    # I

    .line 245
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 246
    .local v0, "bytes":[B
    const/16 v1, 0x34

    .line 247
    .local v1, "binlength":I
    const/16 v2, -0x56

    const/4 v3, 0x0

    aput-byte v2, v0, v3

    .line 248
    const/4 v2, 0x1

    const/16 v4, 0x42

    aput-byte v4, v0, v2

    .line 249
    const/16 v5, 0x32

    const/4 v6, 0x2

    aput-byte v5, v0, v6

    .line 250
    const/4 v5, 0x3

    aput-byte v3, v0, v5

    .line 251
    const/16 v3, 0x4f

    const/4 v7, 0x4

    aput-byte v3, v0, v7

    .line 252
    const/4 v3, 0x5

    const/16 v8, 0x30

    aput-byte v8, v0, v3

    .line 253
    const/4 v3, 0x6

    const/16 v8, -0x80

    aput-byte v8, v0, v3

    .line 254
    const/4 v3, 0x7

    const/16 v8, 0x18

    aput-byte v8, v0, v3

    .line 255
    const/16 v3, 0x8

    const/16 v8, 0x11

    aput-byte v8, v0, v3

    .line 256
    const/16 v3, 0x9

    const/16 v8, 0x38

    aput-byte v8, v0, v3

    .line 257
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v3

    .line 258
    .local v3, "offsetB":[B
    const/16 v8, 0xa

    aget-byte v5, v3, v5

    aput-byte v5, v0, v8

    .line 259
    const/16 v5, 0xb

    aget-byte v6, v3, v6

    aput-byte v6, v0, v5

    .line 260
    const/16 v5, 0xc

    aget-byte v2, v3, v2

    aput-byte v2, v0, v5

    .line 261
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mFileBuf:[B

    array-length v5, v2

    add-int/lit16 v6, p1, 0x7fc0

    add-int/2addr v6, v1

    if-ge v5, v6, :cond_0

    .line 262
    array-length v5, v2

    add-int/lit16 v5, v5, -0x7fc0

    sub-int v1, v5, p1

    .line 264
    :cond_0
    const/16 v5, 0xd

    const/16 v6, 0x34

    aput-byte v6, v0, v5

    .line 265
    add-int/lit16 v5, p1, 0x7fc0

    const/16 v6, 0xe

    invoke-static {v2, v5, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 266
    const/16 v2, 0x3e

    invoke-static {v0, v7, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v4

    .line 268
    return-object v0
.end method

.method public getBinPacketTotal(I)I
    .locals 3
    .param p1, "length"    # I

    .line 338
    const/4 v0, 0x0

    .line 339
    .local v0, "totle":I
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 347
    :sswitch_0
    iget v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I

    add-int/lit8 v2, v1, 0x40

    div-int/lit8 v0, v2, 0x34

    .line 348
    add-int/lit8 v1, v1, 0x40

    rem-int/lit8 v1, v1, 0x34

    if-eqz v1, :cond_0

    .line 349
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    :sswitch_1
    iget v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLength:I

    add-int/lit8 v2, v1, 0x40

    div-int/lit8 v0, v2, 0x14

    .line 342
    add-int/lit8 v1, v1, 0x40

    rem-int/lit8 v1, v1, 0x14

    if-eqz v1, :cond_0

    .line 343
    add-int/lit8 v0, v0, 0x1

    .line 355
    :cond_0
    :goto_0
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public getResetInfo()[B
    .locals 6

    .line 316
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 317
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 318
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 319
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 320
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 321
    const/16 v2, 0x4f

    const/4 v3, 0x4

    aput-byte v2, v0, v3

    .line 322
    const/4 v2, 0x5

    const/16 v4, 0x30

    aput-byte v4, v0, v2

    .line 323
    const/4 v2, 0x6

    const/16 v5, -0x80

    aput-byte v5, v0, v2

    .line 324
    const/4 v2, 0x7

    const/16 v5, 0x18

    aput-byte v5, v0, v2

    .line 325
    const/16 v2, 0x8

    aput-byte v1, v0, v2

    .line 326
    const/16 v1, 0x9

    aput-byte v3, v0, v1

    .line 327
    const/16 v1, 0x57

    const/16 v2, 0xa

    aput-byte v1, v0, v2

    .line 328
    const/16 v1, 0xb

    const/16 v5, 0x4e

    aput-byte v5, v0, v1

    .line 329
    const/16 v1, 0xc

    const/16 v5, 0x38

    aput-byte v5, v0, v1

    .line 330
    const/16 v1, 0xd

    aput-byte v4, v0, v1

    .line 331
    const/16 v1, 0xe

    invoke-static {v0, v3, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 333
    return-object v0
.end method

.method public getUpEndInfo()[B
    .locals 6

    .line 273
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 274
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 275
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 276
    const/16 v1, 0x32

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 277
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 278
    const/16 v1, 0x4f

    const/4 v4, 0x4

    aput-byte v1, v0, v4

    .line 279
    const/4 v1, 0x5

    const/16 v5, 0x30

    aput-byte v5, v0, v1

    .line 280
    const/4 v1, 0x6

    const/16 v5, -0x80

    aput-byte v5, v0, v1

    .line 281
    const/4 v1, 0x7

    const/16 v5, 0x18

    aput-byte v5, v0, v1

    .line 282
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 283
    const/16 v1, 0x9

    const/16 v5, 0x15

    aput-byte v5, v0, v1

    .line 284
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLengthByte:[B

    const/16 v5, 0xa

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 285
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinStartAddressByte:[B

    const/16 v5, 0xe

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 286
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSumByte:[B

    const/16 v5, 0x12

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinFlashAddressByte:[B

    const/16 v5, 0x16

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 288
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVidByte:[B

    const/16 v5, 0x1a

    invoke-static {v1, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 289
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPidByte:[B

    const/16 v5, 0x1c

    invoke-static {v1, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 290
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinDeviceTypeByte:[B

    aget-byte v1, v1, v2

    const/16 v2, 0x1e

    aput-byte v1, v0, v2

    .line 291
    const/16 v1, 0x1b

    invoke-static {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    const/16 v2, 0x1f

    aput-byte v1, v0, v2

    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send UpEndInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "IIC_McuUpgradeUtil"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    return-object v0
.end method

.method public getUpFlashInfo()[B
    .locals 6

    .line 298
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 299
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 300
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 301
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 302
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 303
    const/16 v1, 0x4f

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 304
    const/4 v1, 0x5

    const/16 v4, 0x30

    aput-byte v4, v0, v1

    .line 305
    const/16 v1, -0x80

    const/4 v4, 0x6

    aput-byte v1, v0, v4

    .line 306
    const/4 v1, 0x7

    const/16 v5, 0x18

    aput-byte v5, v0, v1

    .line 307
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 308
    const/16 v1, 0x9

    aput-byte v3, v0, v1

    .line 309
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLengthByte:[B

    const/16 v4, 0xa

    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 310
    const/16 v1, 0xe

    invoke-static {v0, v3, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 311
    return-object v0
.end method

.method public getUpgradeCommand()[B
    .locals 9

    .line 214
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 215
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 216
    const/16 v1, 0x42

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 217
    const/16 v1, 0x32

    const/4 v4, 0x2

    aput-byte v1, v0, v4

    .line 218
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 219
    const/16 v1, 0x4f

    const/4 v5, 0x4

    aput-byte v1, v0, v5

    .line 220
    const/4 v1, 0x5

    const/16 v6, 0x30

    aput-byte v6, v0, v1

    .line 221
    const/4 v1, 0x6

    const/16 v6, -0x80

    aput-byte v6, v0, v1

    .line 222
    const/4 v1, 0x7

    const/16 v6, 0x18

    aput-byte v6, v0, v1

    .line 223
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 224
    const/16 v1, 0x9

    const/16 v6, 0x19

    aput-byte v6, v0, v1

    .line 225
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinLengthByte:[B

    const/16 v6, 0xa

    invoke-static {v1, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 226
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinStartAddressByte:[B

    const/16 v6, 0xe

    invoke-static {v1, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinCheckSumByte:[B

    const/16 v6, 0x12

    invoke-static {v1, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinFlashAddressByte:[B

    const/16 v6, 0x16

    invoke-static {v1, v2, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 229
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinVidByte:[B

    const/16 v6, 0x1a

    invoke-static {v1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 230
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinPidByte:[B

    const/16 v6, 0x1c

    invoke-static {v1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 231
    const/16 v1, 0x1e

    aput-byte v3, v0, v1

    .line 232
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mBinDeviceTypeByte:[B

    aget-byte v1, v1, v2

    const/16 v6, 0x1f

    aput-byte v1, v0, v6

    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getYearMonthDayByTimestamp(J)[I

    move-result-object v1

    .line 235
    .local v1, "tiems":[I
    aget v2, v1, v2

    int-to-byte v2, v2

    const/16 v7, 0x20

    aput-byte v2, v0, v7

    .line 236
    aget v2, v1, v3

    int-to-byte v2, v2

    const/16 v3, 0x21

    aput-byte v2, v0, v3

    .line 237
    aget v2, v1, v4

    int-to-byte v2, v2

    const/16 v3, 0x22

    aput-byte v2, v0, v3

    .line 238
    const/16 v2, 0x23

    invoke-static {v0, v5, v6}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v3

    aput-byte v3, v0, v2

    .line 239
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "send UpgradeCommand : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-static {v0, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IIC_McuUpgradeUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    return-object v0
.end method

.method public isValidFile()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->mIsFileValid:Z

    return v0
.end method
