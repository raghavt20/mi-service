.class public Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.super Ljava/lang/Object;
.source "IICNodeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;,
        Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;,
        Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;,
        Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;
    }
.end annotation


# static fields
.field public static final BUFFER_SIZE:I = 0x64

.field private static final KEYBOARD_COLOR_BLACK:Ljava/lang/String; = "black"

.field private static final KEYBOARD_COLOR_WHITE:Ljava/lang/String; = "white"

.field private static final KEYBOARD_INFO_CHANGED:Ljava/lang/String; = "notify_keyboard_info_changed"

.field private static final TAG:Ljava/lang/String; = "MiuiPadKeyboard_IICNodeHelper"

.field private static final UPGRADE_FOOT_LOCATION:Ljava/lang/String; = ".bin"

.field private static final UPGRADE_HEAD_LOCATION_ODM:Ljava/lang/String; = "odm/etc/"

.field private static final UPGRADE_HEAD_LOCATION_VENDOR:Ljava/lang/String; = "vendor/etc/"

.field private static volatile sInstance:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# instance fields
.field private final mAllWorkerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;",
            ">;"
        }
    .end annotation
.end field

.field private mAuthCommandResponse:[B

.field private final mBlueToothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

.field private final mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

.field private final mContext:Landroid/content/Context;

.field private mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

.field private mEmptyCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

.field private volatile mIsBinDataOK:Z

.field private mIsBleKeyboard:Z

.field private final mKeyBoardUpgradeFilePath:Ljava/lang/StringBuilder;

.field private mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

.field private mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

.field private mKeyboardBleAdd:Ljava/lang/String;

.field private volatile mKeyboardFlashVersion:Ljava/lang/String;

.field private mKeyboardType:B

.field private volatile mKeyboardVersion:Ljava/lang/String;

.field private mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

.field private mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

.field private final mMCUUpgradeFilePath:Ljava/lang/StringBuilder;

.field private volatile mMCUVersion:Ljava/lang/String;

.field private mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

.field private mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

.field public mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

.field private final mReadyRunningTask:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mReceiverBinIndex:I

.field private mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

.field private mTouchPadType:B

.field private final mTouchPadUpgradeFilePath:Ljava/lang/StringBuilder;

.field private mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

.field private volatile mTouchPadVersion:Ljava/lang/String;

.field private final mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

.field private final mUpgradeKeyboardRunnable:Ljava/lang/Runnable;

.field private final mUpgradeMCURunnable:Ljava/lang/Runnable;

.field private mUpgradeNoCheck:Z

.field private final mUpgradeTouchPadRunnable:Ljava/lang/Runnable;


# direct methods
.method public static synthetic $r8$lambda$-baMklQkxU-V4KZpeu37UZXxGDo(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetKeyboardVersionCommand$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$2JzDxttHMD4HvOwHZ8gU8FC63ws(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetMCUVersionCommand$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$3cAj_zHyyEhbYdv7Qvwn9X1h2Vs(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$upgradeTouchPadIfNeed$6()V

    return-void
.end method

.method public static synthetic $r8$lambda$4lTSqpkvxTOqx711lFuw-bKq9EQ(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetKeyboardVersionCommand$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$F24BjgTvKRUX19S3HaM1jAtp-PI(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$9()V

    return-void
.end method

.method public static synthetic $r8$lambda$GKCgSjb6O-X9ZdtyXMIM_ZasKE0(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$7()V

    return-void
.end method

.method public static synthetic $r8$lambda$MO_rQN50uQ5-fj8DPzT9af0cu0w(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$8()V

    return-void
.end method

.method public static synthetic $r8$lambda$RfS-IzdnOwVMvmM7C922hZxflWg(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$11()V

    return-void
.end method

.method public static synthetic $r8$lambda$sqsLJ7RANDz-l8GIFqoZ7BHg558(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$initCommandWorker$10()V

    return-void
.end method

.method public static synthetic $r8$lambda$y4skuJNpvhCDwvKA0v4TvysZ-p4(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$upgradeKeyboardIfNeed$5()V

    return-void
.end method

.method public static synthetic $r8$lambda$zF0YCQmz2h7ZznkntegoUAkV7Go(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$startUpgradeMCUIfNeed$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$zo-G4JZLp0S9Qrnc6EM4MLdg7D8(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->lambda$sendGetMCUVersionCommand$2()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCountDownLatch(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/util/concurrent/CountDownLatch;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsBleKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBleKeyboard:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeFilePath:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardBleAdd(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardBleAdd:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardFlashVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardFlashVersion:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B
    .locals 0

    iget-byte p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersion:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardVersionWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMCUUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMCUUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeFilePath:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMCUVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersion:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMCUVersionWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmReadyRunningTask(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/util/Queue;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)I
    .locals 0

    iget p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReceiverBinIndex:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTouchPadCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTouchPadType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B
    .locals 0

    iget-byte p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTouchPadUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeFilePath:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTouchPadVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadVersion:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUpgradeCommandHandler(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAuthCommandResponse(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[B)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAuthCommandResponse:[B

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsBinDataOK(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsBleKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBleKeyboard:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmKeyboardBleAdd(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardBleAdd:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmKeyboardFlashVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardFlashVersion:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;B)V
    .locals 0

    iput-byte p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmKeyboardVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersion:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMCUVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersion:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMcuUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmReceiverBinIndex(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReceiverBinIndex:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTouchPadType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;B)V
    .locals 0

    iput-byte p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTouchPadUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTouchPadVersion(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadVersion:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckDataPkgSum(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;[BLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->checkDataPkgSum([BLjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetTriggerWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->getTriggerWorker()Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetUpgradeHead(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->getUpgradeHead()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msendBroadCast2Ble(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendBroadCast2Ble()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendBroadCast2Ble(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendBroadCast2Ble(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msupportUpgradeKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->supportUpgradeKeyboard()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msupportUpgradeMCU(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->supportUpgradeMCU()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msupportUpgradeTouchPad(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->supportUpgradeTouchPad()Z

    move-result p0

    return p0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeFilePath:Ljava/lang/StringBuilder;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeFilePath:Ljava/lang/StringBuilder;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TouchPad_Upgrade.bin"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeFilePath:Ljava/lang/StringBuilder;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReceiverBinIndex:I

    .line 80
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B

    .line 81
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    .line 104
    const-string v0, "66:55:44:33:22:11"

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardBleAdd:Ljava/lang/String;

    .line 285
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$1;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeMCURunnable:Ljava/lang/Runnable;

    .line 339
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeKeyboardRunnable:Ljava/lang/Runnable;

    .line 389
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$3;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeTouchPadRunnable:Ljava/lang/Runnable;

    .line 119
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mContext:Landroid/content/Context;

    .line 120
    invoke-static {}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getInstance()Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 121
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->registerSocketCallback(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;)V

    .line 123
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    .line 124
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    iget-byte v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B

    invoke-direct {v0, p1, v1, v2}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    .line 125
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    iget-byte v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadType:B

    invoke-direct {v0, p1, v1, v2}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    .line 127
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "keyboard_upgrade"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 128
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 129
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    .line 131
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->initCommandWorker()V

    .line 132
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mBlueToothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    .line 133
    return-void
.end method

.method private checkDataPkgSum([BLjava/lang/String;)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "reason"    # Ljava/lang/String;

    .line 435
    array-length v0, p1

    const/4 v1, 0x5

    aget-byte v2, p1, v1

    add-int/lit8 v2, v2, 0x6

    if-ge v0, v2, :cond_0

    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Exception Data:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    return-void

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    aget-byte v2, p1, v1

    add-int/lit8 v2, v2, 0x6

    aget-byte v1, p1, v1

    add-int/lit8 v1, v1, 0x6

    aget-byte v1, p1, v1

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3, v2, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    .line 441
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 442
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Accumulation and verification failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onReadSocketNumError(Ljava/lang/String;)V

    .line 445
    :cond_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 108
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sInstance:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    if-nez v0, :cond_1

    .line 109
    const-class v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    monitor-enter v0

    .line 110
    :try_start_0
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sInstance:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    if-nez v1, :cond_0

    .line 111
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sInstance:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 113
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 115
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sInstance:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    return-object v0
.end method

.method private getTriggerWorker()Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    .locals 3

    .line 562
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 563
    .local v1, "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 564
    return-object v1

    .line 566
    .end local v1    # "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    :cond_0
    goto :goto_0

    .line 567
    :cond_1
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "can\'t find the running worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mEmptyCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    return-object v0
.end method

.method private getUpgradeHead()Ljava/lang/String;
    .locals 1

    .line 334
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    const-string/jumbo v0, "vendor/etc/"

    return-object v0

    .line 337
    :cond_0
    const-string v0, "odm/etc/"

    return-object v0
.end method

.method private initCommandWorker()V
    .locals 4

    .line 448
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 474
    const/16 v1, 0x38

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V

    .line 475
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 476
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-direct {v0, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 502
    const/16 v2, 0x40

    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V

    .line 503
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 504
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-direct {v0, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 529
    const/16 v2, 0x18

    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V

    .line 530
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 532
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    new-instance v3, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda4;

    invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-direct {v0, p0, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 541
    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V

    .line 542
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda5;

    invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-direct {v0, p0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 553
    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->setTargetAddress(B)V

    .line 554
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 556
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda6;

    invoke-direct {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda6;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mEmptyCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 559
    return-void
.end method

.method private synthetic lambda$initCommandWorker$10()V
    .locals 2

    .line 533
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_0

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 537
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 540
    :goto_0
    return-void
.end method

.method private synthetic lambda$initCommandWorker$11()V
    .locals 2

    .line 545
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_0

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 549
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 552
    :goto_0
    return-void
.end method

.method static synthetic lambda$initCommandWorker$12()V
    .locals 2

    .line 557
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "No Comannd trigger, The keyboard is Exception response!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    return-void
.end method

.method private synthetic lambda$initCommandWorker$7()V
    .locals 7

    .line 449
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandResponse()[B

    move-result-object v0

    .line 450
    .local v0, "responseData":[B
    const/4 v1, 0x4

    aget-byte v2, v0, v1

    const/16 v3, -0x6f

    if-ne v2, v3, :cond_1

    .line 451
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    const/16 v3, 0x38

    if-nez v2, :cond_0

    .line 452
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getReceiverPackageIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 453
    .local v2, "index":I
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    mul-int/lit8 v6, v2, 0x34

    .line 454
    invoke-virtual {v5, v3, v6}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPackInfo(BI)[B

    move-result-object v3

    .line 453
    invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 456
    .end local v2    # "index":I
    goto :goto_0

    .line 457
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    .line 458
    invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getUpEndInfo(B)[B

    move-result-object v4

    .line 457
    invoke-virtual {v2, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 460
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    .line 461
    invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getUpFlashInfo(B)[B

    move-result-object v3

    .line 460
    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 463
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    .line 465
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_1

    .line 466
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I

    move-result v2

    if-eqz v2, :cond_2

    .line 467
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_1

    .line 469
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 470
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v2, 0x60

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 473
    :goto_1
    return-void
.end method

.method private synthetic lambda$initCommandWorker$8()V
    .locals 7

    .line 477
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandResponse()[B

    move-result-object v0

    .line 478
    .local v0, "responseData":[B
    const/4 v1, 0x4

    aget-byte v2, v0, v1

    const/16 v3, -0x6f

    if-ne v2, v3, :cond_1

    .line 479
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    const/16 v3, 0x38

    if-nez v2, :cond_0

    .line 480
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getReceiverPackageIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 481
    .local v2, "index":I
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    mul-int/lit8 v6, v2, 0x34

    .line 482
    invoke-virtual {v5, v3, v6}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPackInfo(BI)[B

    move-result-object v3

    .line 481
    invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 484
    .end local v2    # "index":I
    goto :goto_0

    .line 485
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    .line 486
    invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getUpEndInfo(B)[B

    move-result-object v4

    .line 485
    invoke-virtual {v2, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 488
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    .line 489
    invoke-virtual {v4, v3}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getUpFlashInfo(B)[B

    move-result-object v3

    .line 488
    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 491
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    .line 493
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_1

    .line 494
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I

    move-result v2

    if-eqz v2, :cond_2

    .line 495
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_1

    .line 497
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 498
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v2, 0x60

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 501
    :goto_1
    return-void
.end method

.method private synthetic lambda$initCommandWorker$9()V
    .locals 6

    .line 505
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandResponse()[B

    move-result-object v0

    .line 506
    .local v0, "responseData":[B
    const/4 v1, 0x4

    aget-byte v2, v0, v1

    const/16 v3, -0x6f

    if-ne v2, v3, :cond_1

    .line 507
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    if-nez v2, :cond_0

    .line 508
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getReceiverPackageIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 509
    .local v2, "index":I
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    mul-int/lit8 v5, v2, 0x34

    .line 510
    invoke-virtual {v4, v5}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPackInfo(I)[B

    move-result-object v4

    .line 509
    invoke-virtual {v3, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 511
    .end local v2    # "index":I
    goto :goto_0

    .line 512
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    .line 513
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getUpEndInfo()[B

    move-result-object v3

    .line 512
    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 514
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    .line 515
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getUpFlashInfo()[B

    move-result-object v3

    .line 514
    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 516
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    .line 517
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getResetInfo()[B

    move-result-object v3

    .line 516
    invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 518
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mIsBinDataOK:Z

    .line 520
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_1

    .line 521
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getCommandSize()I

    move-result v2

    if-eqz v2, :cond_2

    .line 522
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    goto :goto_1

    .line 524
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 525
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v2, 0x60

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 528
    :goto_1
    return-void
.end method

.method private synthetic lambda$sendGetKeyboardVersionCommand$0()V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    return-void
.end method

.method private synthetic lambda$sendGetKeyboardVersionCommand$1()V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic lambda$sendGetMCUVersionCommand$2()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    return-void
.end method

.method private synthetic lambda$sendGetMCUVersionCommand$3()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic lambda$startUpgradeMCUIfNeed$4()V
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeMCURunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic lambda$upgradeKeyboardIfNeed$5()V
    .locals 2

    .line 327
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeKeyboardRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic lambda$upgradeTouchPadIfNeed$6()V
    .locals 2

    .line 384
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeTouchPadRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method private sendBroadCast2Ble()V
    .locals 1

    .line 1187
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendBroadCast2Ble(Z)V

    .line 1188
    return-void
.end method

.method private sendBroadCast2Ble(Z)V
    .locals 3
    .param p1, "isIICConnected"    # Z

    .line 1191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Notify Ble iic is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1192
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.bluetooth.action.KEYBOARD_ATTACH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1193
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1194
    const-string v1, "com.xiaomi.bluetooth.keyboard.extra.ADDRESS"

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardBleAdd:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1195
    const-string v1, "com.xiaomi.bluetooth.keyboard.extra.ATTACH_STATE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1196
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1197
    return-void
.end method

.method private supportUpgradeKeyboard()Z
    .locals 3

    .line 369
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyBoardUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->checkVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    const/16 v1, 0x38

    const-string v2, "The device version doesn\'t need upgrading"

    invoke-interface {v0, v1, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 375
    const/4 v0, 0x0

    return v0

    .line 370
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private supportUpgradeMCU()Z
    .locals 3

    .line 312
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMcuUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->checkVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    const/16 v1, 0x18

    const-string v2, "The device version doesn\'t need upgrading"

    invoke-interface {v0, v1, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 318
    const/4 v0, 0x0

    return v0

    .line 313
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private supportUpgradeTouchPad()Z
    .locals 3

    .line 420
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadUpgradeUtil:Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->checkVersion(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    const/16 v1, 0x40

    const-string v2, "The device version doesn\'t need upgrading"

    invoke-interface {v0, v1, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 426
    const/4 v0, 0x0

    return v0

    .line 421
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public checkAuth([B)[B
    .locals 4
    .param p1, "command"    # [B

    .line 253
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 254
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 255
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "exception":Ljava/lang/InterruptedException;
    const-string v1, "MiuiPadKeyboard_IICNodeHelper"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 262
    .end local v0    # "exception":Ljava/lang/InterruptedException;
    :goto_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAuthCommandResponse:[B

    return-object v0
.end method

.method public checkHallStatus()V
    .locals 2

    .line 269
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B

    move-result-object v0

    .line 270
    .local v0, "command":[B
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setGetHallStatusCommand([B)V

    .line 271
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V

    .line 272
    return-void
.end method

.method public getBleKeyboardStatus()V
    .locals 2

    .line 1117
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mBlueToothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;->CMD_KB_STATUS:[B

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->writeDataToBleDevice([B)V

    .line 1119
    return-void
.end method

.method public hasAnyWorkerRunning()Z
    .locals 3

    .line 1178
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mAllWorkerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    .line 1179
    .local v1, "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1180
    const/4 v0, 0x1

    return v0

    .line 1182
    .end local v1    # "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
    :cond_0
    goto :goto_0

    .line 1183
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public readKeyboardStatus()V
    .locals 8

    .line 236
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B

    move-result-object v0

    .line 237
    .local v0, "command":[B
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V

    .line 238
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    const/16 v3, 0x4e

    const/16 v4, 0x31

    const/16 v5, -0x80

    const/16 v6, 0x38

    const/16 v7, 0x52

    move-object v2, v0

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setLocalAddress2KeyboardCommand([BBBBBB)V

    .line 244
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V

    .line 245
    return-void
.end method

.method public sendGetKeyboardVersionCommand()V
    .locals 3

    .line 144
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "offer getKeyboardVersion worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 146
    const/16 v2, 0x38

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getVersionCommand(B)[B

    move-result-object v1

    .line 145
    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 148
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z

    .line 150
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 152
    return-void
.end method

.method public sendGetMCUVersionCommand()V
    .locals 3

    .line 162
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "offer getMCUVersion worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getVersionCommand(B)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 165
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z

    .line 167
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 169
    return-void
.end method

.method public sendNFCData()V
    .locals 3

    .line 1108
    new-instance v0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;

    invoke-direct {v0}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;-><init>()V

    sget-object v1, Lcom/android/server/input/padkeyboard/OnehopInfo;->ACTION_SUFFIX_MIRROR:Ljava/lang/String;

    .line 1109
    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->setActionSuffix(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mContext:Landroid/content/Context;

    .line 1110
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getBtMacAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->setBtMac(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;

    move-result-object v0

    .line 1111
    invoke-static {}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getExtAbility()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->setExtAbility([B)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;

    move-result-object v0

    .line 1112
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->build()Lcom/android/server/input/padkeyboard/OnehopInfo;

    move-result-object v0

    .line 1113
    .local v0, "info":Lcom/android/server/input/padkeyboard/OnehopInfo;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v0, v0}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getPayload(Lcom/android/server/input/padkeyboard/OnehopInfo;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sendNFC([B)V

    .line 1114
    return-void
.end method

.method public sendRestoreMcuCommand()V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sendRestoreMcuCommand()V

    .line 159
    return-void
.end method

.method public setKeyboardBacklight(B)V
    .locals 5
    .param p1, "level"    # B

    .line 216
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_BACKLIGHT:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .line 218
    .local v0, "feature":B
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B

    move-result-object v1

    .line 220
    .local v1, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V

    .line 221
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v2, v1, v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setSetKeyboardStatusCommand([BBB)V

    .line 222
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "keyboard_back_light_brightness"

    const/4 v4, -0x2

    invoke-static {v2, v3, p1, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_0

    .line 224
    .end local v1    # "command":[B
    :cond_0
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    invoke-static {v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;->getKeyboardFeatureCommand(BB)[B

    move-result-object v1

    .line 229
    .restart local v1    # "command":[B
    :goto_0
    sget-object v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V

    .line 230
    return-void

    .line 227
    .end local v1    # "command":[B
    :cond_1
    return-void
.end method

.method public setKeyboardFeature(ZI)V
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "featureType"    # I

    .line 180
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_ENABLE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 181
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_ENABLE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .local v0, "feature":B
    goto :goto_0

    .line 182
    .end local v0    # "feature":B
    :cond_0
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_POWER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 183
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_POWER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .restart local v0    # "feature":B
    goto :goto_0

    .line 184
    .end local v0    # "feature":B
    :cond_1
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 185
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .restart local v0    # "feature":B
    goto :goto_0

    .line 186
    .end local v0    # "feature":B
    :cond_2
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 187
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .restart local v0    # "feature":B
    goto :goto_0

    .line 188
    .end local v0    # "feature":B
    :cond_3
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_4

    .line 189
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .restart local v0    # "feature":B
    goto :goto_0

    .line 190
    .end local v0    # "feature":B
    :cond_4
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_5

    .line 191
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .restart local v0    # "feature":B
    goto :goto_0

    .line 192
    .end local v0    # "feature":B
    :cond_5
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_G_SENSOR:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p2, v0, :cond_9

    .line 193
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_G_SENSOR:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B

    move-result v0

    .line 197
    .restart local v0    # "feature":B
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    iget-byte v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardType:B

    invoke-virtual {v1, p2, p1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLedStatusValue(IZB)B

    move-result v1

    .line 198
    .local v1, "value":B
    if-nez v1, :cond_6

    .line 199
    move v1, p1

    .line 202
    :cond_6
    sget-object v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 203
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B

    move-result-object v2

    .line 204
    .local v2, "command":[B
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v3, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V

    .line 205
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v3, v2, v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setSetKeyboardStatusCommand([BBB)V

    goto :goto_1

    .line 206
    .end local v2    # "command":[B
    :cond_7
    sget-object v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 207
    invoke-static {v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;->getKeyboardFeatureCommand(BB)[B

    move-result-object v2

    .line 211
    .restart local v2    # "command":[B
    :goto_1
    sget-object v3, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v3, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithKeyboardLocked([B)V

    .line 212
    return-void

    .line 209
    .end local v2    # "command":[B
    :cond_8
    return-void

    .line 195
    .end local v0    # "feature":B
    .end local v1    # "value":B
    :cond_9
    return-void
.end method

.method public setNoCheckUpgrade(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 431
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeNoCheck:Z

    .line 432
    return-void
.end method

.method public setOtaCallBack(Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;)V
    .locals 1
    .param p1, "callBack"    # Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    .line 136
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    .line 137
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setOtaCallBack(Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;)V

    .line 138
    return-void
.end method

.method public shutdownAllCommandQueue()V
    .locals 2

    .line 1168
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "clear all keyboard worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1169
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyboardVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 1170
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mKeyUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 1171
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mTouchPadCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 1172
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUVersionWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 1173
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mMCUUpgradeCommandWorker:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->-$$Nest$mresetWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V

    .line 1174
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mReadyRunningTask:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1175
    return-void
.end method

.method public startUpgradeMCUIfNeed()V
    .locals 2

    .line 278
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "offer upgrade mcu worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda11;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda11;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z

    .line 281
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 283
    return-void
.end method

.method public upgradeKeyboardIfNeed()V
    .locals 2

    .line 326
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "offer upgrade keyboard worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda10;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda10;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z

    .line 329
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 331
    return-void
.end method

.method public upgradeTouchPadIfNeed()V
    .locals 2

    .line 383
    const-string v0, "MiuiPadKeyboard_IICNodeHelper"

    const-string v1, "offer upgrade touchPad worker"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda12;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$$ExternalSyntheticLambda12;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z

    .line 385
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mUpgradeCommandHandler:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessage(Landroid/os/Message;)Z

    .line 387
    return-void
.end method
