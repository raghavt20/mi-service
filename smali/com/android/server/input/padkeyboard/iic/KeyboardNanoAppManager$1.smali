.class Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;
.super Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback$Stub;
.source "KeyboardNanoAppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->initCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    .line 50
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;->this$0:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    invoke-direct {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public dataReceive(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 52
    .local p1, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;->this$0:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->-$$Nest$fgetmCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [B

    .line 54
    .local v0, "responseData":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 55
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    aput-byte v2, v0, v1

    .line 54
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;->this$0:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->-$$Nest$fgetmCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->receiverNanoAppData([B)V

    .line 59
    .end local v0    # "responseData":[B
    :cond_1
    return-void
.end method

.method public errorReceive(I)V
    .locals 2
    .param p1, "errorCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "errorCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KeyboardNanoApp"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    return-void
.end method
