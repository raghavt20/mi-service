class com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker implements android.os.Parcelable {
	 /* .source "IICNodeHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "CommandWorker" */
} // .end annotation
/* # instance fields */
private final java.util.Queue mCommandQueue;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Queue<", */
/* "[B>;" */
/* } */
} // .end annotation
} // .end field
private mDoingCommand;
private Boolean mIsRunning;
private Integer mReceiverPackageIndex;
private mResponse;
private com.android.server.input.padkeyboard.iic.IICNodeHelper$ResponseListener mResponseListener;
private Object mTargetAddress;
final com.android.server.input.padkeyboard.iic.IICNodeHelper this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$NsEEhp0ymux3CDK2bgPZ9ZKBFbM ( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->lambda$sendCommand$0()V */
return;
} // .end method
static void -$$Nest$mresetWorker ( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->resetWorker()V */
return;
} // .end method
public com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* .param p2, "listener" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$ResponseListener; */
/* .line 1213 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1200 */
int v0 = 0; // const/4 v0, 0x0
/* iput-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mTargetAddress:B */
/* .line 1201 */
/* new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue; */
/* invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V */
this.mCommandQueue = v0;
/* .line 1204 */
/* const/16 v0, 0x40 */
/* new-array v0, v0, [B */
this.mResponse = v0;
/* .line 1214 */
this.mResponseListener = p2;
/* .line 1215 */
return;
} // .end method
private void lambda$sendCommand$0 ( ) { //synthethic
/* .locals 2 */
/* .line 1239 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = this.mDoingCommand;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).communicateWithIIC ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithIIC([B)V
return;
} // .end method
private void resetWorker ( ) {
/* .locals 3 */
/* .line 1305 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I */
/* .line 1306 */
/* const/16 v1, 0x42 */
/* new-array v2, v1, [B */
this.mDoingCommand = v2;
/* .line 1307 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
/* .line 1308 */
v0 = this.mCommandQueue;
/* .line 1309 */
/* new-array v0, v1, [B */
this.mResponse = v0;
/* .line 1310 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v0 );
/* const/16 v1, 0x61 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->removeMessages(I)V
/* .line 1312 */
return;
} // .end method
/* # virtual methods */
public Integer describeContents ( ) {
/* .locals 1 */
/* .line 1316 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void emptyCommandResponse ( ) {
/* .locals 1 */
/* .line 1301 */
v0 = this.mResponseListener;
/* .line 1302 */
return;
} // .end method
public getCommandResponse ( ) {
/* .locals 1 */
/* .line 1284 */
v0 = this.mResponse;
} // .end method
public Integer getCommandSize ( ) {
/* .locals 1 */
/* .line 1230 */
v0 = v0 = this.mCommandQueue;
} // .end method
public Integer getReceiverPackageIndex ( ) {
/* .locals 1 */
/* .line 1280 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I */
} // .end method
public Object getTargetAddress ( ) {
/* .locals 1 */
/* .line 1222 */
/* iget-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mTargetAddress:B */
} // .end method
public void insertCommandToQueue ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "command" # [B */
/* .line 1210 */
v0 = this.mCommandQueue;
/* .line 1211 */
return;
} // .end method
public Boolean isWorkRunning ( ) {
/* .locals 1 */
/* .line 1226 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
} // .end method
public void notResponseException ( ) {
/* .locals 2 */
/* .line 1252 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 1253 */
return;
/* .line 1255 */
} // :cond_0
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
final String v1 = "Time Out!"; // const-string v1, "Time Out!"
/* .line 1256 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->resetWorker()V */
/* .line 1257 */
v0 = this.mResponseListener;
/* .line 1258 */
return;
} // .end method
public void onWorkException ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "response" # [B */
/* .line 1288 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 1289 */
return;
/* .line 1291 */
} // :cond_0
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v1, p1, v0 */
v2 = this.mDoingCommand;
/* const/16 v3, 0x8 */
/* aget-byte v2, v2, v3 */
/* if-eq v1, v2, :cond_1 */
/* aget-byte v0, p1, v0 */
/* const/16 v1, -0x6f */
/* if-ne v0, v1, :cond_2 */
/* const/16 v0, 0x11 */
/* if-ne v2, v0, :cond_2 */
/* .line 1295 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->resetWorker()V */
/* .line 1296 */
v0 = this.mResponseListener;
/* .line 1298 */
} // :cond_2
return;
} // .end method
public void sendCommand ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "repeatTimes" # I */
/* .line 1234 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
/* .line 1235 */
v0 = this.mCommandQueue;
/* check-cast v0, [B */
this.mDoingCommand = v0;
/* .line 1236 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v0 );
/* const/16 v1, 0x61 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->removeMessages(I)V
/* .line 1238 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v0 );
/* new-instance v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;)V */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1240 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1241 */
/* .local v0, "data":Landroid/os/Bundle; */
final String v2 = "command"; // const-string v2, "command"
v3 = this.mDoingCommand;
(( android.os.Bundle ) v0 ).putByteArray ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
/* .line 1242 */
/* const-string/jumbo v2, "worker" */
(( android.os.Bundle ) v0 ).putParcelable ( v2, p0 ); // invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 1243 */
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v2 );
android.os.Message .obtain ( v2,v1 );
/* .line 1245 */
/* .local v1, "msg2":Landroid/os/Message; */
/* iput p1, v1, Landroid/os/Message;->arg1:I */
/* .line 1246 */
(( android.os.Message ) v1 ).setData ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 1247 */
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v2 );
/* const-wide/16 v3, 0x5dc */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v2 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1249 */
return;
} // .end method
public void setTargetAddress ( Object p0 ) {
/* .locals 0 */
/* .param p1, "address" # B */
/* .line 1218 */
/* iput-byte p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mTargetAddress:B */
/* .line 1219 */
return;
} // .end method
public void triggerResponse ( Object[] p0 ) {
/* .locals 3 */
/* .param p1, "response" # [B */
/* .line 1261 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 1262 */
return;
/* .line 1264 */
} // :cond_0
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v0, p1, v0 */
v1 = this.mDoingCommand;
/* const/16 v2, 0x8 */
/* aget-byte v1, v1, v2 */
/* if-ne v0, v1, :cond_1 */
/* .line 1265 */
this.mResponse = p1;
/* .line 1266 */
v0 = this.mResponseListener;
/* .line 1268 */
} // :cond_1
return;
} // .end method
public void triggerResponseForPackage ( Object[] p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "response" # [B */
/* .param p2, "index" # I */
/* .line 1271 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mIsRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 1272 */
return;
/* .line 1274 */
} // :cond_0
this.mResponse = p1;
/* .line 1275 */
/* iput p2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I */
/* .line 1276 */
v0 = this.mResponseListener;
/* .line 1277 */
return;
} // .end method
public void writeToParcel ( android.os.Parcel p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "dest" # Landroid/os/Parcel; */
/* .param p2, "flags" # I */
/* .line 1321 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->mReceiverPackageIndex:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 1322 */
return;
} // .end method
