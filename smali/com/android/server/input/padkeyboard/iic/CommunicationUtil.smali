.class public Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
.super Ljava/lang/Object;
.source "CommunicationUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;,
        Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;,
        Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;,
        Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;,
        Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;
    }
.end annotation


# static fields
.field public static final BUFFER_SIZE:I = 0x400

.field public static final COMMAND_AUTH_3:B = 0x32t

.field public static final COMMAND_AUTH_51:B = 0x33t

.field public static final COMMAND_AUTH_52:B = 0x34t

.field public static final COMMAND_AUTH_7:B = 0x35t

.field public static final COMMAND_AUTH_START:B = 0x31t

.field public static final COMMAND_CHECK_MCU_STATUS:B = -0x5ft

.field public static final COMMAND_CUSTOM_DATA_KB:B = 0x69t

.field public static final COMMAND_DATA_PKG:B = -0x6ft

.field public static final COMMAND_GET_VERSION:B = 0x1t

.field public static final COMMAND_G_SENSOR:B = 0x64t

.field public static final COMMAND_KB_BACKLIGHT:B = 0x23t

.field public static final COMMAND_KB_BATTERY_LEVEL:B = 0x29t

.field public static final COMMAND_KB_FEATURE_CAPS_LOCK:B = 0x26t

.field public static final COMMAND_KB_FEATURE_POWER:B = 0x25t

.field public static final COMMAND_KB_FEATURE_RECOVER:B = 0x20t

.field public static final COMMAND_KB_FEATURE_SLEEP:B = 0x28t

.field public static final COMMAND_KB_NFC:B = 0x36t

.field public static final COMMAND_KB_RE_AUTH:B = 0x24t

.field public static final COMMAND_KB_STATUS:B = 0x22t

.field public static final COMMAND_KEYBOARD_RESPONSE_STATUS:B = 0x30t

.field public static final COMMAND_KEYBOARD_STATUS:B = -0x10t

.field public static final COMMAND_KEYBOARD_UPGRADE_STATUS:B = 0x75t

.field public static final COMMAND_MCU_BOOT:B = 0x12t

.field public static final COMMAND_MCU_RESET:B = 0x3t

.field public static final COMMAND_READ_KB_STATUS:B = 0x52t

.field public static final COMMAND_RESPONSE_MCU_STATUS:B = -0x5et

.field public static final COMMAND_SUCCESS_RUN:B = 0x0t

.field public static final COMMAND_UPGRADE:B = 0x2t

.field public static final COMMAND_UPGRADE_BUSY:B = 0x7t

.field public static final COMMAND_UPGRADE_FINISHED:B = 0x4t

.field public static final COMMAND_UPGRADE_FLASH:B = 0x6t

.field public static final COMMAN_KEYBOARD_EXTERNAL_FLAG:B = 0x69t

.field public static final COMMAN_PAD_BLUETOOTH:B = 0x52t

.field public static final COMMAN_PAD_EXTERNAL_FLAG:B = 0x68t

.field public static final FEATURE_DISABLE:B = 0x0t

.field public static final FEATURE_ENABLE:B = 0x1t

.field public static final KEYBOARD_ADDRESS:B = 0x38t

.field public static final KEYBOARD_COLOR_BLACK:B = 0x41t

.field public static final KEYBOARD_COLOR_WHITE:B = 0x42t

.field public static final KEYBOARD_FLASH_ADDRESS:B = 0x39t

.field public static final L81A_VERSION_LENGTH:B = 0x5t

.field public static final MCU_ADDRESS:B = 0x18t

.field public static final OTA_FAIL_NOT_MATCH:B = 0x7t

.field public static final PAD_ADDRESS:B = -0x80t

.field public static final REPLENISH_PROTOCOL_COMMAND:B = 0x31t

.field public static final RESPONSE_BLE_DEVICE_ID:B = 0x31t

.field public static final RESPONSE_TYPE:B = 0x57t

.field public static final RESPONSE_VENDOR_ONE_LONG_REPORT_ID:B = 0x24t

.field public static final RESPONSE_VENDOR_ONE_SHORT_REPORT_ID:B = 0x23t

.field public static final RESPONSE_VENDOR_TWO_REPORT_ID:B = 0x26t

.field public static final RESPONSE_VENDOR_TWO_SHORT_REPORT_ID:B = 0x22t

.field public static final SEND_COMMAND_BYTE_LONG:I = 0x44

.field public static final SEND_COMMAND_BYTE_SHORT:I = 0x22

.field public static final SEND_EMPTY_DATA:B = 0x0t

.field public static final SEND_REPORT_ID_LONG_DATA:B = 0x4ft

.field public static final SEND_REPORT_ID_SHORT_DATA:B = 0x4et

.field public static final SEND_RESTORE_COMMAND:B = 0x1dt

.field public static final SEND_SERIAL_NUMBER:B = 0x0t

.field public static final SEND_TYPE:B = 0x32t

.field public static final SEND_UPGRADE_PACKAGE_COMMAND:B = 0x11t

.field public static final TAG:Ljava/lang/String; = "IIC_CommunicationUtil"

.field public static final TOUCHPAD_ADDRESS:B = 0x40t

.field public static final UPGRADE_PROTOCOL_COMMAND:B = 0x30t

.field private static volatile sCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;


# instance fields
.field private mKeyboardNanoAppManager:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

.field private final mLock:Ljava/lang/Object;

.field private mNFCDataPkgSize:I

.field private mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

.field private mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

.field private mSocketCallBack:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;


# direct methods
.method static bridge synthetic -$$Nest$fgetmNanoSocketCallback(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSocketCallBack(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mSocketCallBack:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 1

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mLock:Ljava/lang/Object;

    .line 174
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->initSocketClient()V

    .line 175
    return-void
.end method

.method public static getInstance()Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
    .locals 2

    .line 163
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    if-nez v0, :cond_1

    .line 164
    const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    monitor-enter v0

    .line 165
    :try_start_0
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    if-nez v1, :cond_0

    .line 166
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-direct {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;-><init>()V

    sput-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 168
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 170
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->sCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    return-object v0
.end method

.method private getNfcData([B)Ljava/util/List;
    .locals 11
    .param p1, "nfc"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation

    .line 466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v0, "nfc_data":Ljava/util/List;, "Ljava/util/List<[B>;"
    array-length v1, p1

    const/16 v2, 0x8

    add-int/2addr v1, v2

    new-array v1, v1, [B

    .line 468
    .local v1, "all_nfc":[B
    const/16 v3, 0x4e

    const/4 v4, 0x0

    aput-byte v3, v1, v4

    .line 469
    const/16 v3, 0x46

    const/4 v5, 0x1

    aput-byte v3, v1, v5

    .line 470
    const/4 v3, 0x2

    const/16 v6, 0x43

    aput-byte v6, v1, v3

    .line 471
    array-length v3, p1

    add-int/2addr v3, v5

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v6, 0x3

    aput-byte v3, v1, v6

    .line 472
    array-length v3, p1

    add-int/2addr v3, v5

    shr-int/2addr v3, v2

    and-int/lit16 v3, v3, 0xffe

    int-to-byte v3, v3

    const/4 v6, 0x4

    aput-byte v3, v1, v6

    .line 473
    const/4 v3, 0x5

    aput-byte v4, v1, v3

    .line 474
    const/4 v6, 0x6

    aput-byte v4, v1, v6

    .line 475
    const/16 v7, 0x54

    const/4 v8, 0x7

    aput-byte v7, v1, v8

    .line 476
    array-length v7, p1

    invoke-static {p1, v4, v1, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 478
    array-length v2, p1

    add-int/2addr v2, v5

    invoke-static {v1, v8, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSumInt([BII)I

    move-result v2

    .line 479
    .local v2, "sumInt":I
    and-int/lit16 v5, v2, 0xff

    int-to-byte v5, v5

    aput-byte v5, v1, v3

    .line 480
    shr-int/lit8 v3, v2, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v1, v6

    .line 482
    array-length v3, v1

    const/16 v5, 0x34

    if-le v3, v5, :cond_2

    .line 483
    array-length v3, v1

    add-int/lit8 v3, v3, 0x33

    div-int/2addr v3, v5

    .line 484
    .local v3, "pac_numbers":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v3, :cond_1

    .line 485
    add-int/lit8 v7, v6, 0x1

    if-ne v7, v3, :cond_0

    array-length v7, v1

    mul-int/lit8 v8, v6, 0x34

    sub-int/2addr v7, v8

    goto :goto_1

    :cond_0
    move v7, v5

    .line 486
    .local v7, "length":I
    :goto_1
    new-array v8, v7, [B

    .line 487
    .local v8, "temp":[B
    mul-int/lit8 v9, v6, 0x34

    array-length v10, v8

    invoke-static {v1, v9, v8, v4, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 488
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 484
    .end local v7    # "length":I
    .end local v8    # "temp":[B
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 490
    .end local v3    # "pac_numbers":I
    .end local v6    # "i":I
    :cond_1
    goto :goto_2

    .line 491
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494
    :goto_2
    return-object v0
.end method

.method public static declared-synchronized getSum([BII)B
    .locals 4
    .param p0, "command"    # [B
    .param p1, "start"    # I
    .param p2, "length"    # I

    const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    monitor-enter v0

    .line 423
    const/4 v1, 0x0

    .line 424
    .local v1, "sum":B
    move v2, p1

    .local v2, "i":I
    :goto_0
    add-int v3, p1, p2

    if-ge v2, v3, :cond_0

    .line 425
    :try_start_0
    aget-byte v3, p0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v3, v1

    int-to-byte v1, v3

    .line 424
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 422
    .end local v1    # "sum":B
    .end local v2    # "i":I
    .end local p0    # "command":[B
    .end local p1    # "start":I
    .end local p2    # "length":I
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    .line 427
    .restart local v1    # "sum":B
    .restart local p0    # "command":[B
    .restart local p1    # "start":I
    .restart local p2    # "length":I
    :cond_0
    monitor-exit v0

    return v1
.end method

.method public static declared-synchronized getSumInt([BII)I
    .locals 4
    .param p0, "data"    # [B
    .param p1, "start"    # I
    .param p2, "length"    # I

    const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    monitor-enter v0

    .line 431
    const/4 v1, 0x0

    .line 432
    .local v1, "sum":I
    move v2, p1

    .local v2, "i":I
    :goto_0
    add-int v3, p1, p2

    if-ge v2, v3, :cond_0

    .line 433
    :try_start_0
    aget-byte v3, p0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v1, v3

    .line 432
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 430
    .end local v1    # "sum":I
    .end local v2    # "i":I
    .end local p0    # "data":[B
    .end local p1    # "start":I
    .end local p2    # "length":I
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    .line 435
    .restart local v1    # "sum":I
    .restart local p0    # "data":[B
    .restart local p1    # "start":I
    .restart local p2    # "length":I
    :cond_0
    monitor-exit v0

    return v1
.end method

.method private initSocketClient()V
    .locals 3

    .line 178
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "IIC_Read_Socket"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 180
    .local v0, "readThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 181
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;-><init>(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    .line 183
    .end local v0    # "readThread":Landroid/os/HandlerThread;
    :cond_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->getInstance()Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mKeyboardNanoAppManager:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    .line 184
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->initCallback()V

    .line 185
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mKeyboardNanoAppManager:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    invoke-virtual {v0, p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->setCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)V

    .line 186
    return-void
.end method


# virtual methods
.method public checkSum([BIIB)Z
    .locals 5
    .param p1, "data"    # [B
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "sum"    # B

    .line 511
    invoke-static {p1, p2, p3}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v0

    .line 512
    .local v0, "dataSum":B
    const/4 v1, 0x1

    new-array v2, v1, [B

    .line 513
    .local v2, "bytes":[B
    const/4 v3, 0x0

    aput-byte v0, v2, v3

    .line 514
    aget-byte v4, v2, v3

    if-ne v4, p4, :cond_0

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    return v1
.end method

.method public getLedStatusValue(IZB)B
    .locals 1
    .param p1, "type"    # I
    .param p2, "enable"    # Z
    .param p3, "keyType"    # B

    .line 525
    const/16 v0, 0x21

    if-eq p3, v0, :cond_0

    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v0

    if-nez v0, :cond_2

    .line 526
    :cond_0
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-eq p1, v0, :cond_5

    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    .line 527
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p1, v0, :cond_1

    goto :goto_2

    .line 529
    :cond_1
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-eq p1, v0, :cond_3

    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    .line 530
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    if-ne p1, v0, :cond_2

    goto :goto_0

    .line 534
    :cond_2
    const/4 v0, 0x0

    return v0

    .line 531
    :cond_3
    :goto_0
    if-eqz p2, :cond_4

    const/16 v0, -0x9

    goto :goto_1

    :cond_4
    const/16 v0, -0xd

    :goto_1
    return v0

    .line 528
    :cond_5
    :goto_2
    if-eqz p2, :cond_6

    const/4 v0, -0x3

    goto :goto_3

    :cond_6
    const/4 v0, -0x4

    :goto_3
    return v0
.end method

.method public getLongRawCommand()[B
    .locals 1

    .line 285
    const/16 v0, 0x44

    new-array v0, v0, [B

    return-object v0
.end method

.method public getNFCDataPkgSize()I
    .locals 1

    .line 498
    iget v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNFCDataPkgSize:I

    return v0
.end method

.method public getVersionCommand(B)[B
    .locals 6
    .param p1, "targetAddress"    # B

    .line 216
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 217
    .local v0, "temp":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 218
    const/16 v1, 0x42

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 219
    const/4 v1, 0x2

    const/16 v4, 0x32

    aput-byte v4, v0, v1

    .line 220
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 221
    const/16 v1, 0x4e

    const/4 v4, 0x4

    aput-byte v1, v0, v4

    .line 222
    const/4 v1, 0x5

    const/16 v5, 0x30

    aput-byte v5, v0, v1

    .line 223
    const/4 v1, 0x6

    const/16 v5, -0x80

    aput-byte v5, v0, v1

    .line 224
    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 225
    const/16 v5, 0x8

    aput-byte v3, v0, v5

    .line 226
    const/16 v5, 0x9

    aput-byte v3, v0, v5

    .line 227
    const/16 v3, 0xa

    aput-byte v2, v0, v3

    .line 228
    const/16 v2, 0xb

    invoke-static {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    aput-byte v1, v0, v2

    .line 229
    return-object v0
.end method

.method public receiverNanoAppData([B)V
    .locals 3
    .param p1, "responseData"    # [B

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "get keyboard data:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "IIC_CommunicationUtil"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "IIC_Read_Socket"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 193
    .local v0, "readThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 194
    new-instance v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;-><init>(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    .line 196
    .end local v0    # "readThread":Landroid/os/HandlerThread;
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 198
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mReadSocketHandler:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;->sendMessage(Landroid/os/Message;)Z

    .line 199
    return-void
.end method

.method public registerSocketCallback(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;)V
    .locals 0
    .param p1, "socketCallBack"    # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;

    .line 202
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mSocketCallBack:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$SocketCallBack;

    .line 203
    return-void
.end method

.method public sendNFC([B)V
    .locals 8
    .param p1, "data_byte"    # [B

    .line 440
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getNfcData([B)Ljava/util/List;

    move-result-object v0

    .line 441
    .local v0, "nfcData":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNFCDataPkgSize:I

    .line 442
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNFCDataPkgSize:I

    if-ge v1, v2, :cond_0

    .line 443
    const/16 v2, 0x44

    new-array v2, v2, [B

    .line 444
    .local v2, "temp":[B
    const/16 v3, -0x56

    const/4 v4, 0x0

    aput-byte v3, v2, v4

    .line 445
    const/16 v3, 0x42

    const/4 v5, 0x1

    aput-byte v3, v2, v5

    .line 446
    const/16 v3, 0x32

    const/4 v5, 0x2

    aput-byte v3, v2, v5

    .line 447
    const/4 v3, 0x3

    aput-byte v4, v2, v3

    .line 449
    const/16 v3, 0x4f

    const/4 v6, 0x4

    aput-byte v3, v2, v6

    .line 450
    const/4 v3, 0x5

    const/16 v7, 0x31

    aput-byte v7, v2, v3

    .line 451
    const/4 v3, 0x6

    const/16 v7, -0x80

    aput-byte v7, v2, v3

    .line 452
    const/4 v3, 0x7

    const/16 v7, 0x38

    aput-byte v7, v2, v3

    .line 454
    const/16 v3, 0x8

    const/16 v7, 0x36

    aput-byte v7, v2, v3

    .line 456
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    array-length v3, v3

    add-int/2addr v3, v5

    int-to-byte v3, v3

    const/16 v5, 0x9

    aput-byte v3, v2, v5

    .line 457
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    int-to-byte v3, v3

    const/16 v5, 0xa

    aput-byte v3, v2, v5

    .line 458
    add-int/lit8 v3, v1, 0x1

    int-to-byte v3, v3

    const/16 v5, 0xb

    aput-byte v3, v2, v5

    .line 459
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    const/16 v7, 0xc

    invoke-static {v3, v4, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 460
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    array-length v3, v3

    add-int/2addr v3, v7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v4, v4

    add-int/2addr v4, v7

    invoke-static {v2, v6, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v4

    aput-byte v4, v2, v3

    .line 461
    invoke-virtual {p0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->writeSocketCmd([B)Z

    .line 442
    .end local v2    # "temp":[B
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 463
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public sendRestoreMcuCommand()V
    .locals 6

    .line 236
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 237
    .local v0, "temp":[B
    const/16 v1, 0x32

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 238
    const/4 v1, 0x1

    aput-byte v2, v0, v1

    .line 239
    const/16 v1, 0x4f

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 240
    const/4 v1, 0x3

    const/16 v4, 0x30

    aput-byte v4, v0, v1

    .line 241
    const/4 v1, 0x4

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 242
    const/4 v1, 0x5

    const/16 v4, 0x18

    aput-byte v4, v0, v1

    .line 243
    const/4 v1, 0x6

    const/16 v4, 0x1d

    aput-byte v4, v0, v1

    .line 245
    const/4 v1, 0x7

    const/16 v4, 0xd

    aput-byte v4, v0, v1

    .line 247
    const/16 v1, 0x8

    const/16 v5, -0x74

    aput-byte v5, v0, v1

    .line 248
    const/16 v1, 0x9

    const/16 v5, 0x3f

    aput-byte v5, v0, v1

    .line 249
    const/16 v1, 0xa

    const/16 v5, 0x65

    aput-byte v5, v0, v1

    .line 250
    const/16 v1, 0xb

    const/16 v5, -0x7f

    aput-byte v5, v0, v1

    .line 251
    const/16 v1, 0xc

    const/16 v5, -0x6e

    aput-byte v5, v0, v1

    .line 252
    const/16 v1, -0x32

    aput-byte v1, v0, v4

    .line 253
    const/16 v1, 0xe

    aput-byte v2, v0, v1

    .line 254
    const/16 v1, 0xf

    const/16 v2, -0x6c

    aput-byte v2, v0, v1

    .line 255
    const/16 v1, 0x10

    const/16 v2, -0x58

    aput-byte v2, v0, v1

    .line 256
    const/16 v1, 0x11

    const/16 v2, -0x7e

    aput-byte v2, v0, v1

    .line 257
    const/16 v1, 0x12

    const/16 v2, 0x2d

    aput-byte v2, v0, v1

    .line 258
    const/16 v1, 0x5b

    const/16 v2, 0x13

    aput-byte v1, v0, v2

    .line 260
    const/16 v1, 0x14

    const/16 v4, 0x7e

    aput-byte v4, v0, v1

    .line 261
    const/16 v1, 0x15

    invoke-static {v0, v3, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 262
    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->writeSocketCmd([B)Z

    .line 263
    return-void
.end method

.method public setCheckKeyboardResponseCommand([BBBBBB)V
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "reportId"    # B
    .param p3, "version"    # B
    .param p4, "sourceAdd"    # B
    .param p5, "targetAdd"    # B
    .param p6, "lastCommandId"    # B

    .line 374
    const/4 v0, 0x4

    aput-byte p2, p1, v0

    .line 375
    const/4 v1, 0x5

    aput-byte p3, p1, v1

    .line 376
    const/4 v1, 0x6

    aput-byte p4, p1, v1

    .line 377
    const/4 v1, 0x7

    aput-byte p5, p1, v1

    .line 378
    const/16 v2, 0x8

    const/16 v3, 0x30

    aput-byte v3, p1, v2

    .line 379
    const/16 v2, 0x9

    const/4 v3, 0x1

    aput-byte v3, p1, v2

    .line 380
    const/16 v2, 0xa

    aput-byte p6, p1, v2

    .line 381
    const/16 v2, 0xb

    invoke-static {p1, v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v0

    aput-byte v0, p1, v2

    .line 382
    return-void
.end method

.method public setCheckMCUStatusCommand([BB)V
    .locals 4
    .param p1, "data"    # [B
    .param p2, "command"    # B

    .line 391
    invoke-virtual {p0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V

    .line 392
    const/16 v0, 0x4e

    const/4 v1, 0x4

    aput-byte v0, p1, v1

    .line 393
    const/4 v0, 0x5

    const/16 v2, 0x31

    aput-byte v2, p1, v0

    .line 394
    const/4 v0, 0x6

    const/16 v2, -0x80

    aput-byte v2, p1, v0

    .line 395
    const/16 v0, 0x38

    const/4 v2, 0x7

    aput-byte v0, p1, v2

    .line 396
    const/16 v0, 0x8

    aput-byte p2, p1, v0

    .line 397
    const/16 v0, 0x9

    const/4 v3, 0x1

    aput-byte v3, p1, v0

    .line 398
    const/16 v0, 0xa

    aput-byte v3, p1, v0

    .line 399
    const/16 v0, 0xb

    invoke-static {p1, v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    aput-byte v1, p1, v0

    .line 400
    return-void
.end method

.method public setCommandHead([B)V
    .locals 3
    .param p1, "bytes"    # [B

    .line 294
    const/16 v0, -0x56

    const/4 v1, 0x0

    aput-byte v0, p1, v1

    .line 295
    const/4 v0, 0x1

    const/16 v2, 0x42

    aput-byte v2, p1, v0

    .line 296
    const/4 v0, 0x2

    const/16 v2, 0x32

    aput-byte v2, p1, v0

    .line 297
    const/4 v0, 0x3

    aput-byte v1, p1, v0

    .line 298
    return-void
.end method

.method public setGetHallStatusCommand([B)V
    .locals 4
    .param p1, "data"    # [B

    .line 403
    invoke-virtual {p0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCommandHead([B)V

    .line 404
    const/16 v0, 0x4f

    const/4 v1, 0x4

    aput-byte v0, p1, v1

    .line 405
    const/4 v0, 0x5

    const/16 v2, 0x20

    aput-byte v2, p1, v0

    .line 406
    const/4 v0, 0x6

    const/16 v2, -0x80

    aput-byte v2, p1, v0

    .line 407
    const/4 v0, 0x7

    aput-byte v2, p1, v0

    .line 408
    const/16 v2, 0x8

    const/16 v3, -0x1f

    aput-byte v3, p1, v2

    .line 409
    const/16 v2, 0x9

    const/4 v3, 0x1

    aput-byte v3, p1, v2

    .line 410
    const/16 v2, 0xa

    const/4 v3, 0x0

    aput-byte v3, p1, v2

    .line 411
    const/16 v2, 0xb

    invoke-static {p1, v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v0

    aput-byte v0, p1, v2

    .line 412
    return-void
.end method

.method public setLocalAddress2KeyboardCommand([BBBBBB)V
    .locals 11
    .param p1, "bytes"    # [B
    .param p2, "reportId"    # B
    .param p3, "version"    # B
    .param p4, "sourceAdd"    # B
    .param p5, "targetAdd"    # B
    .param p6, "feature"    # B

    .line 341
    move-object v0, p1

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 342
    .local v1, "localAddress":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get pad Address:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IIC_CommunicationUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const/4 v2, 0x4

    aput-byte p2, v0, v2

    .line 344
    const/4 v3, 0x5

    aput-byte p3, v0, v3

    .line 345
    const/4 v3, 0x6

    aput-byte p4, v0, v3

    .line 346
    const/4 v4, 0x7

    aput-byte p5, v0, v4

    .line 347
    const/16 v4, 0x8

    aput-byte p6, v0, v4

    .line 348
    const/16 v4, 0x9

    aput-byte v3, v0, v4

    .line 349
    const/16 v3, 0xa

    .line 350
    .local v3, "i":I
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    const/16 v6, 0x10

    if-nez v4, :cond_0

    .line 351
    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v7, v4

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v8, v4, v5

    .line 352
    .local v8, "retval":Ljava/lang/String;
    add-int/lit8 v9, v3, 0x1

    .end local v3    # "i":I
    .local v9, "i":I
    invoke-static {v8, v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->byteValue()B

    move-result v10

    aput-byte v10, v0, v3

    .line 351
    .end local v8    # "retval":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    move v3, v9

    goto :goto_0

    .line 355
    .end local v9    # "i":I
    .restart local v3    # "i":I
    :cond_0
    const/16 v3, 0xa

    :goto_1
    if-ge v3, v6, :cond_1

    .line 356
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput-byte v5, v0, v3

    .line 355
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 359
    :cond_1
    const/16 v4, 0xd

    invoke-static {p1, v2, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v6

    .line 360
    return-void
.end method

.method public setOtaCallBack(Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;)V
    .locals 0
    .param p1, "callBack"    # Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    .line 206
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    .line 207
    return-void
.end method

.method public setReadKeyboardCommand([BBBBBB)V
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "reportId"    # B
    .param p3, "version"    # B
    .param p4, "sourceAdd"    # B
    .param p5, "targetAdd"    # B
    .param p6, "feature"    # B

    .line 330
    const/4 v0, 0x4

    aput-byte p2, p1, v0

    .line 331
    const/4 v1, 0x5

    aput-byte p3, p1, v1

    .line 332
    const/4 v1, 0x6

    aput-byte p4, p1, v1

    .line 333
    const/4 v1, 0x7

    aput-byte p5, p1, v1

    .line 334
    const/16 v2, 0x8

    aput-byte p6, p1, v2

    .line 335
    const/16 v2, 0x9

    const/4 v3, 0x0

    aput-byte v3, p1, v2

    .line 336
    const/16 v2, 0xa

    invoke-static {p1, v0, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v0

    aput-byte v0, p1, v2

    .line 337
    return-void
.end method

.method public setSetKeyboardStatusCommand([BBB)V
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "commandId"    # B
    .param p3, "data"    # B

    .line 308
    const/16 v0, 0x4e

    const/4 v1, 0x4

    aput-byte v0, p1, v1

    .line 309
    const/4 v0, 0x5

    const/16 v2, 0x31

    aput-byte v2, p1, v0

    .line 310
    const/4 v0, 0x6

    const/16 v2, -0x80

    aput-byte v2, p1, v0

    .line 311
    const/16 v0, 0x38

    const/4 v2, 0x7

    aput-byte v0, p1, v2

    .line 312
    const/16 v0, 0x8

    aput-byte p2, p1, v0

    .line 313
    const/16 v0, 0x9

    const/4 v3, 0x1

    aput-byte v3, p1, v0

    .line 314
    const/16 v0, 0xa

    aput-byte p3, p1, v0

    .line 315
    const/16 v0, 0xb

    invoke-static {p1, v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    aput-byte v1, p1, v0

    .line 316
    return-void
.end method

.method public writeSocketCmd([B)Z
    .locals 4
    .param p1, "buf"    # [B

    .line 272
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/Byte;

    .line 273
    .local v0, "temp":[Ljava/lang/Byte;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 274
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 275
    aget-byte v3, p1, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v0, v2

    .line 274
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 277
    .end local v2    # "i":I
    :cond_0
    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 278
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "write to IIC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-static {p1, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IIC_CommunicationUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->mKeyboardNanoAppManager:Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;

    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->sendCommandToNano(Ljava/util/ArrayList;)Z

    .line 280
    const/4 v2, 0x0

    return v2
.end method
