.class public Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;
.super Ljava/lang/Object;
.source "KeyboardUpgradeUtil.java"


# static fields
.field private static final START_INFO_INDEX_FOR_176:I = 0x20fc0

.field private static final START_INFO_INDEX_FOR_179:I = 0x5fc0

.field private static final START_INFO_INDEX_FOR_8012:I = 0x2fc0

.field private static final TAG:Ljava/lang/String; = "KeyboardUpgradeUtil"


# instance fields
.field public mBinCheckSum:I

.field public mBinCheckSumByte:[B

.field public mBinLength:I

.field public mBinLengthByte:[B

.field public mBinStartAddressByte:[B

.field public mBinVersion:[B

.field public mBinVersionStr:Ljava/lang/String;

.field private mFileBuf:[B

.field private mKeyType:B

.field private mNFCType:B

.field private mStartInfoIndex:I

.field private mTouchType:B

.field public mUpgradeFilePath:Ljava/lang/String;

.field public mValid:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;B)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "keyType"    # B

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x4

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    .line 29
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    .line 31
    const/4 v1, 0x2

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    .line 33
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    .line 39
    const-string v0, "KeyboardUpgradeUtil"

    if-nez p2, :cond_0

    .line 40
    const-string v1, "UpgradeOta file Path is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-void

    .line 43
    :cond_0
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mUpgradeFilePath:Ljava/lang/String;

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpgradeOta file Path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mUpgradeFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mUpgradeFilePath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->ReadUpgradeFile(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    .line 48
    if-eqz v1, :cond_1

    .line 49
    invoke-direct {p0, p3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFile(B)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mValid:Z

    goto :goto_0

    .line 51
    :cond_1
    const-string v1, "UpgradeOta file buff is null or length low than 6000"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :goto_0
    return-void
.end method

.method private ReadUpgradeFile(Ljava/lang/String;)[B
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "fileBuf":[B
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->readFileToBuf(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0

    .line 72
    :cond_0
    const-string v2, "KeyboardUpgradeUtil"

    const-string v3, "=== The Upgrade bin file does not exist."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :goto_0
    return-object v0
.end method

.method private parseOtaFile(B)Z
    .locals 1
    .param p1, "keyType"    # B

    .line 56
    const/16 v0, 0x21

    if-ne p1, v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFileFor179()Z

    move-result v0

    return v0

    .line 59
    :cond_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFileFor8012()Z

    move-result v0

    return v0

    .line 62
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFileFor176()Z

    move-result v0

    return v0
.end method

.method private parseOtaFileFor176()Z
    .locals 18

    .line 105
    move-object/from16 v0, p0

    const v1, 0x20fc0

    iput v1, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    .line 106
    iget-object v2, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    array-length v4, v2

    const v5, 0x21000

    if-le v4, v5, :cond_2

    .line 107
    const v4, 0x20fc0

    .line 109
    .local v4, "fileHeadBaseAddr":I
    const/16 v6, 0x8

    new-array v7, v6, [B

    .line 110
    .local v7, "binStartFlagByte":[B
    array-length v8, v7

    invoke-static {v2, v4, v7, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "binStartFlag":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bin Start Flag: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "KeyboardUpgradeUtil"

    invoke-static {v9, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    add-int/2addr v4, v6

    .line 116
    iget-object v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget-object v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    array-length v11, v10

    invoke-static {v8, v4, v10, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    iget-object v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    const/4 v10, 0x3

    aget-byte v11, v8, v10

    shl-int/lit8 v11, v11, 0x18

    const/high16 v12, -0x1000000

    and-int/2addr v11, v12

    const/4 v13, 0x2

    aget-byte v14, v8, v13

    shl-int/lit8 v14, v14, 0x10

    const/high16 v15, 0xff0000

    and-int/2addr v14, v15

    add-int/2addr v11, v14

    const/4 v14, 0x1

    aget-byte v16, v8, v14

    shl-int/lit8 v16, v16, 0x8

    const v17, 0xff00

    and-int v16, v16, v17

    add-int v11, v11, v16

    aget-byte v8, v8, v3

    and-int/lit16 v8, v8, 0xff

    add-int/2addr v11, v8

    iput v11, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    .line 121
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bin Length: "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v11, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    add-int/lit8 v4, v4, 0x4

    .line 124
    iget-object v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget-object v11, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    array-length v5, v11

    invoke-static {v8, v4, v11, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    aget-byte v8, v5, v10

    shl-int/lit8 v8, v8, 0x18

    and-int/2addr v8, v12

    aget-byte v11, v5, v13

    shl-int/lit8 v11, v11, 0x10

    and-int/2addr v11, v15

    add-int/2addr v8, v11

    aget-byte v11, v5, v14

    shl-int/lit8 v6, v11, 0x8

    and-int v6, v6, v17

    add-int/2addr v8, v6

    aget-byte v5, v5, v3

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v8, v5

    iput v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    .line 130
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin CheckSum: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    add-int/lit8 v4, v4, 0x4

    .line 133
    new-array v5, v13, [B

    iput-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    .line 134
    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    array-length v8, v5

    invoke-static {v6, v4, v5, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    aget-byte v6, v6, v14

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    filled-new-array {v6}, [Ljava/lang/Object;

    move-result-object v6

    const-string v8, "%02x"

    invoke-static {v8, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    aget-byte v6, v6, v3

    .line 136
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    filled-new-array {v6}, [Ljava/lang/Object;

    move-result-object v6

    .line 135
    invoke-static {v8, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    .line 137
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin Version : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    array-length v6, v5

    sub-int/2addr v6, v1

    add-int/lit8 v6, v6, -0x40

    const v8, 0x21000

    invoke-static {v5, v8, v6}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSumInt([BII)I

    move-result v5

    .line 142
    .local v5, "sum1":I
    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    const-string v8, "/"

    if-eq v5, v6, :cond_0

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin Check Check Sum Error:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return v3

    .line 148
    :cond_0
    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    const/16 v11, 0x3f

    invoke-static {v6, v1, v11}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    .line 149
    .local v1, "sum2":B
    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    const v11, 0x20fff

    aget-byte v12, v6, v11

    if-eq v1, v12, :cond_1

    .line 150
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bin Check Head Sum Error:"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    aget-byte v8, v8, v11

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    return v3

    .line 156
    :cond_1
    const v8, 0x210c8

    aget-byte v8, v6, v8

    iput-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    .line 157
    const v8, 0x210c9

    aget-byte v8, v6, v8

    shr-int/lit8 v8, v8, 0x6

    int-to-byte v8, v8

    iput-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    .line 158
    const v8, 0x210ca

    aget-byte v6, v6, v8

    and-int/lit8 v6, v6, 0xf

    int-to-byte v6, v6

    iput-byte v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    .line 159
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "For 176 mKeyType:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", mNFCType:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", mTouchType:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    aput-byte v3, v6, v3

    .line 163
    const/16 v8, -0x80

    aput-byte v8, v6, v14

    .line 164
    const/4 v8, 0x5

    aput-byte v8, v6, v13

    .line 165
    aput-byte v3, v6, v10

    .line 166
    return v14

    .line 168
    .end local v1    # "sum2":B
    .end local v2    # "binStartFlag":Ljava/lang/String;
    .end local v4    # "fileHeadBaseAddr":I
    .end local v5    # "sum1":I
    .end local v7    # "binStartFlagByte":[B
    :cond_2
    return v3
.end method

.method private parseOtaFileFor179()Z
    .locals 17

    .line 173
    move-object/from16 v0, p0

    const/16 v1, 0x5fc0

    iput v1, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    .line 174
    iget-object v1, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    const-string v2, "KeyboardUpgradeUtil"

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    array-length v4, v1

    const/16 v5, 0xfc0

    if-le v4, v5, :cond_2

    .line 175
    const/16 v4, 0x5fc0

    .line 177
    .local v4, "mFileHeadBaseAddr":I
    const/16 v5, 0x8

    new-array v6, v5, [B

    .line 178
    .local v6, "binStartFlagByte":[B
    array-length v7, v6

    invoke-static {v1, v4, v6, v3, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v1

    .line 181
    .local v1, "binStartFlag":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bin Start Flag: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    add-int/2addr v4, v5

    .line 185
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget-object v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    array-length v9, v8

    invoke-static {v7, v4, v8, v3, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    const/4 v8, 0x3

    aget-byte v9, v7, v8

    shl-int/lit8 v9, v9, 0x18

    const/high16 v10, -0x1000000

    and-int/2addr v9, v10

    const/4 v11, 0x2

    aget-byte v12, v7, v11

    const/16 v13, 0x10

    shl-int/2addr v12, v13

    const/high16 v14, 0xff0000

    and-int/2addr v12, v14

    add-int/2addr v9, v12

    const/4 v12, 0x1

    aget-byte v15, v7, v12

    shl-int/2addr v15, v5

    const v16, 0xff00

    and-int v15, v15, v16

    add-int/2addr v9, v15

    aget-byte v7, v7, v3

    and-int/lit16 v7, v7, 0xff

    add-int/2addr v9, v7

    iput v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    .line 189
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bin Lenght: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    add-int/lit8 v4, v4, 0x4

    .line 193
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget-object v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    array-length v15, v9

    invoke-static {v7, v4, v9, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 195
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    aget-byte v9, v7, v8

    shl-int/lit8 v9, v9, 0x18

    and-int/2addr v9, v10

    aget-byte v10, v7, v11

    shl-int/2addr v10, v13

    and-int/2addr v10, v14

    add-int/2addr v9, v10

    aget-byte v10, v7, v12

    shl-int/lit8 v5, v10, 0x8

    and-int v5, v5, v16

    add-int/2addr v9, v5

    aget-byte v5, v7, v3

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v9, v5

    iput v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    .line 198
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin CheckSum: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    add-int/lit8 v4, v4, 0x4

    .line 202
    new-array v5, v11, [B

    iput-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    .line 203
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    array-length v9, v5

    invoke-static {v7, v4, v5, v3, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 204
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    aget-byte v7, v7, v12

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v7

    const-string v9, "%02x"

    invoke-static {v9, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    aget-byte v7, v7, v3

    .line 205
    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v7

    .line 204
    invoke-static {v9, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    .line 206
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin Version : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v5, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/lit8 v9, v7, 0x40

    array-length v10, v5

    add-int/lit8 v7, v7, 0x40

    sub-int/2addr v10, v7

    invoke-static {v5, v9, v10}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSumInt([BII)I

    move-result v5

    .line 211
    .local v5, "sum1":I
    iget v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    const-string v9, "/"

    if-eq v5, v7, :cond_0

    .line 212
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bin Check Check Sum Error:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    return v3

    .line 217
    :cond_0
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    const/16 v14, 0x3f

    invoke-static {v7, v10, v14}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v7

    .line 218
    .local v7, "sum2":B
    iget-object v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v15, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/2addr v15, v14

    aget-byte v15, v10, v15

    if-eq v7, v15, :cond_1

    .line 219
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bin Check Head Sum Error:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/2addr v10, v14

    aget-byte v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    return v3

    .line 225
    :cond_1
    const/16 v9, 0x6048

    aget-byte v9, v10, v9

    iput-byte v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    .line 226
    const/16 v9, 0x6049

    aget-byte v9, v10, v9

    shr-int/lit8 v9, v9, 0x5

    int-to-byte v9, v9

    iput-byte v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    .line 227
    const/16 v9, 0x604a

    aget-byte v9, v10, v9

    and-int/lit8 v9, v9, 0xf

    int-to-byte v9, v9

    iput-byte v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    .line 228
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "For 179 mKeyType:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-byte v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mNFCType:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-byte v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", mTouchType:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-byte v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v2, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v2, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    aput-byte v3, v2, v3

    .line 232
    aput-byte v13, v2, v12

    .line 233
    const/4 v9, 0x6

    aput-byte v9, v2, v11

    .line 234
    aput-byte v3, v2, v8

    .line 235
    return v12

    .line 237
    .end local v1    # "binStartFlag":Ljava/lang/String;
    .end local v4    # "mFileHeadBaseAddr":I
    .end local v5    # "sum1":I
    .end local v6    # "binStartFlagByte":[B
    .end local v7    # "sum2":B
    :cond_2
    const-string v1, "179 Parse OtaFile err"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return v3
.end method

.method private parseOtaFileFor8012()Z
    .locals 15

    .line 244
    const/16 v0, 0x2fc0

    iput v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    .line 245
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    const-string v1, "KeyboardUpgradeUtil"

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    array-length v3, v0

    const/16 v4, 0xfc0

    if-le v3, v4, :cond_2

    .line 246
    const/16 v3, 0x2fc0

    .line 248
    .local v3, "mFileHeadBaseAddr":I
    const/16 v4, 0x8

    new-array v5, v4, [B

    .line 249
    .local v5, "binStartFlagByte":[B
    array-length v6, v5

    invoke-static {v0, v3, v5, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "binStartFlag":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin Start Flag: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    add-int/2addr v3, v4

    .line 256
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    array-length v8, v7

    invoke-static {v6, v3, v7, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 257
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    const/4 v7, 0x3

    aget-byte v8, v6, v7

    shl-int/lit8 v8, v8, 0x18

    const/high16 v9, -0x1000000

    and-int/2addr v8, v9

    const/4 v10, 0x2

    aget-byte v11, v6, v10

    shl-int/lit8 v11, v11, 0x10

    const/high16 v12, 0xff0000

    and-int/2addr v11, v12

    add-int/2addr v8, v11

    const/4 v11, 0x1

    aget-byte v13, v6, v11

    shl-int/2addr v13, v4

    const v14, 0xff00

    and-int/2addr v13, v14

    add-int/2addr v8, v13

    aget-byte v6, v6, v2

    and-int/lit16 v6, v6, 0xff

    add-int/2addr v8, v6

    iput v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    .line 260
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bin Lenght: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    add-int/lit8 v3, v3, 0x4

    .line 264
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    array-length v13, v8

    invoke-static {v6, v3, v8, v2, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 266
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    aget-byte v8, v6, v7

    shl-int/lit8 v8, v8, 0x18

    and-int/2addr v8, v9

    aget-byte v9, v6, v10

    shl-int/lit8 v9, v9, 0x10

    and-int/2addr v9, v12

    add-int/2addr v8, v9

    aget-byte v9, v6, v11

    shl-int/lit8 v4, v9, 0x8

    and-int/2addr v4, v14

    add-int/2addr v8, v4

    aget-byte v4, v6, v2

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v8, v4

    iput v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    .line 269
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin CheckSum: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    add-int/lit8 v3, v3, 0x4

    .line 273
    new-array v4, v10, [B

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    .line 274
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    array-length v8, v4

    invoke-static {v6, v3, v4, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 275
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    aget-byte v6, v6, v11

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    filled-new-array {v6}, [Ljava/lang/Object;

    move-result-object v6

    const-string v8, "%02x"

    invoke-static {v8, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    aget-byte v6, v6, v2

    .line 276
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    filled-new-array {v6}, [Ljava/lang/Object;

    move-result-object v6

    .line 275
    invoke-static {v8, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    .line 277
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bin Version : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/lit8 v8, v6, 0x40

    array-length v9, v4

    add-int/lit8 v6, v6, 0x40

    sub-int/2addr v9, v6

    invoke-static {v4, v8, v9}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSumInt([BII)I

    move-result v4

    .line 282
    .local v4, "sum1":I
    iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    const-string v8, "/"

    if-eq v4, v6, :cond_0

    .line 283
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin Check Check Sum Error:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    return v2

    .line 288
    :cond_0
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    const/16 v12, 0x3f

    invoke-static {v6, v9, v12}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v6

    .line 289
    .local v6, "sum2":B
    iget-object v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v13, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/2addr v13, v12

    aget-byte v13, v9, v13

    if-eq v6, v13, :cond_1

    .line 290
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bin Check Head Sum Error:"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    iget v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/2addr v9, v12

    aget-byte v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    return v2

    .line 296
    :cond_1
    const/16 v8, 0x5fd8

    aget-byte v8, v9, v8

    iput-byte v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    .line 297
    const/16 v8, 0x5fd9

    aget-byte v8, v9, v8

    shr-int/lit8 v8, v8, 0x5

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    .line 298
    const/16 v8, 0x5fda

    aget-byte v8, v9, v8

    and-int/lit8 v8, v8, 0xf

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    .line 299
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "For 8012 mKeyType:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-byte v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mNFCType:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-byte v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mTouchType:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-byte v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    aput-byte v2, v1, v2

    .line 303
    const/16 v8, 0x20

    aput-byte v8, v1, v11

    .line 304
    aput-byte v2, v1, v10

    .line 305
    aput-byte v2, v1, v7

    .line 306
    return v11

    .line 308
    .end local v0    # "binStartFlag":Ljava/lang/String;
    .end local v3    # "mFileHeadBaseAddr":I
    .end local v4    # "sum1":I
    .end local v5    # "binStartFlagByte":[B
    .end local v6    # "sum2":B
    :cond_2
    const-string v0, "8012 Parse OtaFile err"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    return v2
.end method

.method private readFileToBuf(Ljava/lang/String;)[B
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "fileBuf":[B
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    .local v1, "fileStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v2

    .line 88
    .local v2, "fileLength":I
    new-array v3, v2, [B

    move-object v0, v3

    .line 89
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    .end local v1    # "fileStream":Ljava/io/InputStream;
    goto :goto_1

    .line 86
    .end local v2    # "fileLength":I
    .restart local v1    # "fileStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "fileBuf":[B
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;
    .end local p1    # "fileName":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 90
    .end local v1    # "fileStream":Ljava/io/InputStream;
    .restart local v0    # "fileBuf":[B
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;
    .restart local p1    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File is not exit!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "KeyboardUpgradeUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-object v0
.end method


# virtual methods
.method public checkVersion(Ljava/lang/String;)Z
    .locals 2
    .param p1, "version"    # Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "get verison :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , bin version:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KeyboardUpgradeUtil"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersionStr:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 99
    :goto_0
    return v0
.end method

.method public getBinPackInfo(BI)[B
    .locals 9
    .param p1, "target"    # B
    .param p2, "offset"    # I

    .line 342
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 343
    .local v0, "bytes":[B
    const/16 v1, 0x34

    .line 344
    .local v1, "binlength":I
    const/16 v2, -0x56

    const/4 v3, 0x0

    aput-byte v2, v0, v3

    .line 345
    const/4 v2, 0x1

    const/16 v4, 0x42

    aput-byte v4, v0, v2

    .line 346
    const/16 v5, 0x32

    const/4 v6, 0x2

    aput-byte v5, v0, v6

    .line 347
    const/4 v5, 0x3

    aput-byte v3, v0, v5

    .line 348
    const/16 v3, 0x4f

    const/4 v7, 0x4

    aput-byte v3, v0, v7

    .line 349
    const/4 v3, 0x5

    const/16 v8, 0x30

    aput-byte v8, v0, v3

    .line 350
    const/4 v3, 0x6

    const/16 v8, -0x80

    aput-byte v8, v0, v3

    .line 351
    const/4 v3, 0x7

    aput-byte p1, v0, v3

    .line 352
    const/16 v3, 0x8

    const/16 v8, 0x11

    aput-byte v8, v0, v3

    .line 353
    const/16 v3, 0x9

    const/16 v8, 0x38

    aput-byte v8, v0, v3

    .line 354
    invoke-static {p2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v3

    .line 355
    .local v3, "offsetB":[B
    const/16 v8, 0xa

    aget-byte v5, v3, v5

    aput-byte v5, v0, v8

    .line 356
    const/16 v5, 0xb

    aget-byte v6, v3, v6

    aput-byte v6, v0, v5

    .line 357
    const/16 v5, 0xc

    aget-byte v2, v3, v2

    aput-byte v2, v0, v5

    .line 358
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mFileBuf:[B

    array-length v5, v2

    iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I

    add-int/lit8 v8, v6, 0x40

    add-int/2addr v8, p2

    add-int/2addr v8, v1

    if-ge v5, v8, :cond_0

    .line 359
    array-length v5, v2

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x40

    sub-int v1, v5, p2

    .line 361
    :cond_0
    const/16 v5, 0xd

    const/16 v8, 0x34

    aput-byte v8, v0, v5

    .line 362
    add-int/lit8 v6, v6, 0x40

    add-int/2addr v6, p2

    const/16 v5, 0xe

    invoke-static {v2, v6, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 363
    const/16 v2, 0x3e

    invoke-static {v0, v7, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v4

    .line 365
    return-object v0
.end method

.method public getBinPacketTotal(I)I
    .locals 2
    .param p1, "length"    # I

    .line 409
    const/4 v0, 0x0

    .line 410
    .local v0, "totle":I
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 418
    :sswitch_0
    iget v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    div-int/lit8 v0, v1, 0x34

    .line 419
    rem-int/lit8 v1, v1, 0x34

    if-eqz v1, :cond_0

    .line 420
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 412
    :sswitch_1
    iget v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I

    div-int/lit8 v0, v1, 0x14

    .line 413
    rem-int/lit8 v1, v1, 0x14

    if-eqz v1, :cond_0

    .line 414
    add-int/lit8 v0, v0, 0x1

    .line 426
    :cond_0
    :goto_0
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public getUpEndInfo(B)[B
    .locals 7
    .param p1, "target"    # B

    .line 370
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 371
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 372
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 373
    const/16 v1, 0x32

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 374
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 375
    const/16 v1, 0x4f

    const/4 v4, 0x4

    aput-byte v1, v0, v4

    .line 376
    const/4 v1, 0x5

    const/16 v5, 0x30

    aput-byte v5, v0, v1

    .line 377
    const/4 v1, 0x6

    const/16 v5, -0x80

    aput-byte v5, v0, v1

    .line 378
    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 379
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 380
    const/16 v1, 0x9

    const/16 v5, 0xe

    aput-byte v5, v0, v1

    .line 381
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    const/16 v6, 0xa

    invoke-static {v1, v2, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 382
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 383
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    const/16 v5, 0x12

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 384
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    const/16 v5, 0x16

    invoke-static {v1, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 385
    const/16 v1, 0x15

    invoke-static {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    const/16 v2, 0x18

    aput-byte v1, v0, v2

    .line 386
    return-object v0
.end method

.method public getUpFlashInfo(B)[B
    .locals 5
    .param p1, "target"    # B

    .line 391
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 392
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 393
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 394
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 395
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 396
    const/16 v1, 0x4f

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 397
    const/4 v1, 0x5

    const/16 v4, 0x30

    aput-byte v4, v0, v1

    .line 398
    const/16 v1, -0x80

    const/4 v4, 0x6

    aput-byte v1, v0, v4

    .line 399
    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 400
    const/16 v1, 0x8

    aput-byte v4, v0, v1

    .line 401
    const/16 v1, 0x9

    aput-byte v3, v0, v1

    .line 402
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    const/16 v4, 0xa

    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 403
    const/16 v1, 0xe

    invoke-static {v0, v3, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 404
    return-object v0
.end method

.method public getUpgradeInfo(B)[B
    .locals 7
    .param p1, "target"    # B

    .line 316
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 317
    .local v0, "bytes":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 318
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 319
    const/16 v1, 0x32

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 320
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 321
    const/16 v1, 0x4e

    const/4 v4, 0x4

    aput-byte v1, v0, v4

    .line 322
    const/4 v1, 0x5

    const/16 v5, 0x30

    aput-byte v5, v0, v1

    .line 323
    const/4 v1, 0x6

    const/16 v5, -0x80

    aput-byte v5, v0, v1

    .line 324
    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 325
    const/16 v1, 0x8

    aput-byte v3, v0, v1

    .line 326
    const/16 v1, 0x9

    const/16 v5, 0x13

    aput-byte v5, v0, v1

    .line 327
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLengthByte:[B

    const/16 v5, 0xa

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 328
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinStartAddressByte:[B

    const/16 v5, 0xe

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 329
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSumByte:[B

    const/16 v5, 0x12

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 330
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinVersion:[B

    const/16 v6, 0x16

    invoke-static {v1, v2, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 331
    const/16 v1, 0x18

    const/16 v2, 0x2b

    aput-byte v2, v0, v1

    .line 332
    const/16 v1, 0x19

    aput-byte v5, v0, v1

    .line 333
    const/16 v2, 0x1a

    iget-byte v3, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B

    aput-byte v3, v0, v2

    .line 334
    const/16 v2, 0x1b

    iget-byte v3, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B

    aput-byte v3, v0, v2

    .line 335
    const/16 v2, 0x1c

    iget-byte v3, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B

    aput-byte v3, v0, v2

    .line 336
    const/16 v2, 0x1d

    invoke-static {v0, v4, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v1

    aput-byte v1, v0, v2

    .line 337
    return-object v0
.end method

.method public isValidFile()Z
    .locals 1

    .line 79
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mValid:Z

    return v0
.end method
