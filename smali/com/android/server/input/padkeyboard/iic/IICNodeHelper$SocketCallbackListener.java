class com.android.server.input.padkeyboard.iic.IICNodeHelper$SocketCallbackListener implements com.android.server.input.padkeyboard.iic.CommunicationUtil$SocketCallBack {
	 /* .source "IICNodeHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "SocketCallbackListener" */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.iic.IICNodeHelper this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.iic.IICNodeHelper$SocketCallbackListener ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* .line 574 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void getSuitableBinName ( Object p0, Object p1 ) {
/* .locals 4 */
/* .param p1, "keyboardValue" # B */
/* .param p2, "touchpadValue" # B */
/* .line 805 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmKeyboardType ( v0,p1 );
/* .line 806 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeFilePath ( v0 );
final String v1 = "_"; // const-string v1, "_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Byte .valueOf ( p1 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "0x%02x"; // const-string v3, "0x%02x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 809 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmTouchPadType ( v0,p2 );
/* .line 810 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeFilePath ( v0 );
int v2 = 0; // const/4 v2, 0x0
(( java.lang.StringBuilder ) v0 ).setLength ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 811 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeFilePath ( v0 );
final String v2 = "TouchPad_Upgrade"; // const-string v2, "TouchPad_Upgrade"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 812 */
java.lang.Byte .valueOf ( p2 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ".bin"; // const-string v1, ".bin"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 814 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set TouchPad Upgrade File:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeFilePath ( v1 );
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
android.util.Slog .i ( v1,v0 );
/* .line 815 */
return;
} // .end method
private void onMcuVersionResponse ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "data" # [B */
/* .line 1088 */
/* const/16 v0, 0x10 */
/* new-array v1, v0, [B */
/* .line 1089 */
/* .local v1, "deviceVersion":[B */
int v2 = 7; // const/4 v2, 0x7
int v3 = 0; // const/4 v3, 0x0
java.lang.System .arraycopy ( p1,v2,v1,v3,v0 );
/* .line 1090 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v1 );
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmMCUVersion ( v0,v2 );
/* .line 1092 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeFilePath ( v0 );
(( java.lang.StringBuilder ) v0 ).setLength ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 1093 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeFilePath ( v0 );
final String v2 = "MCU_Upgrade"; // const-string v2, "MCU_Upgrade"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".bin"; // const-string v2, ".bin"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1094 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set MCU Upgrade File:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeFilePath ( v2 );
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v2, "MiuiPadKeyboard_IICNodeHelper"
android.util.Slog .i ( v2,v0 );
/* .line 1096 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUVersionWorker ( v0 );
v0 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).isWorkRunning ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 1097 */
	 v0 = this.this$0;
	 com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUVersionWorker ( v0 );
	 (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
	 /* .line 1100 */
} // :cond_0
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1101 */
	 v0 = this.this$0;
	 v0 = this.mNanoSocketCallback;
	 v2 = this.this$0;
	 v2 = this.mNanoSocketCallback;
	 v2 = this.this$0;
	 com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUVersion ( v2 );
	 /* const/16 v3, 0xa */
	 /* .line 1104 */
} // :cond_1
return;
} // .end method
private void parseGsensorDataInfo ( Object[] p0 ) {
/* .locals 9 */
/* .param p1, "data" # [B */
/* .line 915 */
/* const v0, 0x411ccccd # 9.8f */
/* .line 916 */
/* .local v0, "G":F */
int v1 = 7; // const/4 v1, 0x7
/* aget-byte v1, p1, v1 */
/* shl-int/lit8 v1, v1, 0x4 */
/* and-int/lit16 v1, v1, 0xff0 */
int v2 = 6; // const/4 v2, 0x6
/* aget-byte v2, p1, v2 */
/* shr-int/lit8 v2, v2, 0x4 */
/* and-int/lit8 v2, v2, 0xf */
/* or-int/2addr v1, v2 */
/* .line 917 */
/* .local v1, "x":I */
/* const/16 v2, 0x9 */
/* aget-byte v2, p1, v2 */
/* shl-int/lit8 v2, v2, 0x4 */
/* and-int/lit16 v2, v2, 0xff0 */
/* const/16 v3, 0x8 */
/* aget-byte v3, p1, v3 */
/* shr-int/lit8 v3, v3, 0x4 */
/* and-int/lit8 v3, v3, 0xf */
/* or-int/2addr v2, v3 */
/* .line 918 */
/* .local v2, "y":I */
/* const/16 v3, 0xb */
/* aget-byte v3, p1, v3 */
/* shl-int/lit8 v3, v3, 0x4 */
/* and-int/lit16 v3, v3, 0xff0 */
/* const/16 v4, 0xa */
/* aget-byte v4, p1, v4 */
/* shr-int/lit8 v4, v4, 0x4 */
/* and-int/lit8 v4, v4, 0xf */
/* or-int/2addr v3, v4 */
/* .line 919 */
/* .local v3, "z":I */
/* const/16 v4, 0x800 */
/* .line 920 */
/* .local v4, "mask":I */
/* and-int v5, v1, v4 */
/* if-ne v5, v4, :cond_0 */
/* .line 921 */
/* rsub-int v5, v1, 0x1000 */
/* neg-int v1, v5 */
/* .line 923 */
} // :cond_0
/* and-int v5, v2, v4 */
/* if-ne v5, v4, :cond_1 */
/* .line 924 */
/* rsub-int v5, v2, 0x1000 */
/* neg-int v2, v5 */
/* .line 926 */
} // :cond_1
/* and-int v5, v3, v4 */
/* if-ne v5, v4, :cond_2 */
/* .line 927 */
/* rsub-int v5, v3, 0x1000 */
/* neg-int v3, v5 */
/* .line 929 */
} // :cond_2
/* int-to-float v5, v1 */
/* mul-float/2addr v5, v0 */
/* const/high16 v6, 0x43800000 # 256.0f */
/* div-float/2addr v5, v6 */
/* .line 930 */
/* .local v5, "x_normal":F */
/* neg-int v7, v2 */
/* int-to-float v7, v7 */
/* mul-float/2addr v7, v0 */
/* div-float/2addr v7, v6 */
/* .line 931 */
/* .local v7, "y_normal":F */
/* neg-int v8, v3 */
/* int-to-float v8, v8 */
/* mul-float/2addr v8, v0 */
/* div-float/2addr v8, v6 */
/* .line 933 */
/* .local v8, "z_normal":F */
v6 = this.this$0;
v6 = this.mNanoSocketCallback;
/* .line 934 */
return;
} // .end method
private void parseKeyboardStatus ( Object[] p0 ) {
/* .locals 10 */
/* .param p1, "data" # [B */
/* .line 938 */
int v0 = 0; // const/4 v0, 0x0
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x26 */
/* const/16 v3, 0x23 */
/* const/16 v4, 0x12 */
/* const/16 v5, 0x9 */
final String v6 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v6, "MiuiPadKeyboard_IICNodeHelper"
/* if-eq v1, v2, :cond_0 */
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x24 */
/* if-eq v1, v2, :cond_0 */
/* aget-byte v1, p1, v0 */
/* if-ne v1, v3, :cond_6 */
/* .line 941 */
} // :cond_0
/* aget-byte v1, p1, v4 */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_4 */
/* .line 943 */
/* aget-byte v1, p1, v5 */
/* and-int/lit8 v1, v1, 0x63 */
/* if-ne v1, v3, :cond_1 */
/* .line 944 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).setConnectIICLocked ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectIICLocked(Z)V
/* .line 946 */
} // :cond_1
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).setConnectIICLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectIICLocked(Z)V
/* .line 947 */
/* aget-byte v1, p1, v5 */
/* and-int/lit8 v1, v1, 0x63 */
/* const/16 v3, 0x43 */
/* if-ne v1, v3, :cond_2 */
/* .line 948 */
final String v1 = "Keyboard is connect,but Pogo pin is exception"; // const-string v1, "Keyboard is connect,but Pogo pin is exception"
android.util.Slog .i ( v6,v1 );
/* .line 949 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v1 );
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v2 );
/* .line 950 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v3, 0x110f0249 */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 949 */
android.widget.Toast .makeText ( v1,v2,v0 );
/* .line 952 */
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 953 */
} // :cond_2
/* aget-byte v0, p1, v5 */
/* and-int/lit8 v0, v0, 0x3 */
/* if-ne v0, v2, :cond_3 */
/* .line 954 */
final String v0 = "Keyboard is connect,but TRX is exception or leather case coming"; // const-string v0, "Keyboard is connect,but TRX is exception or leather case coming"
android.util.Slog .i ( v6,v0 );
/* .line 958 */
} // :cond_3
final String v0 = "Keyboard is disConnection"; // const-string v0, "Keyboard is disConnection"
android.util.Slog .e ( v6,v0 );
/* .line 961 */
} // :cond_4
/* aget-byte v1, p1, v4 */
/* if-ne v1, v2, :cond_5 */
/* .line 962 */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).setConnectIICLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectIICLocked(Z)V
/* .line 963 */
final String v0 = "The keyboard power supply current exceeds the limit\uff01"; // const-string v0, "The keyboard power supply current exceeds the limit\uff01"
android.util.Slog .i ( v6,v0 );
/* .line 965 */
} // :cond_5
} // :goto_0
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v0, :cond_6 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmIsBleKeyboard ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 966 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msendBroadCast2Ble ( v0 );
/* .line 969 */
} // :cond_6
/* const/16 v0, 0xb */
/* aget-byte v0, p1, v0 */
/* shl-int/lit8 v0, v0, 0x8 */
/* const v1, 0xff00 */
/* and-int/2addr v0, v1 */
/* const/16 v1, 0xa */
/* aget-byte v1, p1, v1 */
/* and-int/lit16 v1, v1, 0xff */
/* add-int/2addr v0, v1 */
/* .line 970 */
/* .local v0, "battry":I */
/* const/16 v1, 0x11 */
/* aget-byte v2, p1, v1 */
/* and-int/lit8 v2, v2, 0x7 */
/* .line 971 */
/* .local v2, "serial":I */
/* aget-byte v3, p1, v1 */
/* shr-int/lit8 v3, v3, 0x4 */
/* const/16 v7, 0xf */
/* and-int/2addr v3, v7 */
/* .line 973 */
/* .local v3, "times":I */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Receiver mcu KEY_S:"; // const-string v9, "Receiver mcu KEY_S:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v5, p1, v5 */
java.lang.Byte .valueOf ( v5 );
/* filled-new-array {v5}, [Ljava/lang/Object; */
final String v9 = "0x%02x"; // const-string v9, "0x%02x"
java.lang.String .format ( v9,v5 );
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " KEY_R:"; // const-string v8, " KEY_R:"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = "mv E_UART:"; // const-string v8, "mv E_UART:"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v8, 0xd */
/* aget-byte v8, p1, v8 */
/* .line 975 */
java.lang.Byte .valueOf ( v8 );
/* const/16 v9, 0xc */
/* aget-byte v9, p1, v9 */
java.lang.Byte .valueOf ( v9 );
/* filled-new-array {v8, v9}, [Ljava/lang/Object; */
/* .line 974 */
final String v9 = "0x%02x%02x"; // const-string v9, "0x%02x%02x"
java.lang.String .format ( v9,v8 );
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " E_PPM:"; // const-string v8, " E_PPM:"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v1, p1, v1 */
/* .line 976 */
java.lang.Byte .valueOf ( v1 );
/* const/16 v8, 0x10 */
/* aget-byte v8, p1, v8 */
/* .line 977 */
java.lang.Byte .valueOf ( v8 );
/* aget-byte v7, p1, v7 */
java.lang.Byte .valueOf ( v7 );
/* const/16 v9, 0xe */
/* aget-byte v9, p1, v9 */
java.lang.Byte .valueOf ( v9 );
/* filled-new-array {v1, v8, v7, v9}, [Ljava/lang/Object; */
/* .line 976 */
final String v7 = "0x%02x%02x%02x%02x"; // const-string v7, "0x%02x%02x%02x%02x"
java.lang.String .format ( v7,v1 );
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " Serial:"; // const-string v5, " Serial:"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " PowerUp:"; // const-string v5, " PowerUp:"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " OCP_F:"; // const-string v5, " OCP_F:"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v4, p1, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 973 */
android.util.Slog .i ( v6,v1 );
/* .line 979 */
return;
} // .end method
private void parseKeyboardUpdateFinishInfo ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "data" # [B */
/* .line 887 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mgetTriggerWorker ( v0 );
/* .line 888 */
/* .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v2, p1, v1 */
/* if-nez v2, :cond_0 */
/* .line 889 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* .line 890 */
} // :cond_0
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 891 */
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v3 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Upgrade finished Info Response error "; // const-string v5, "Upgrade finished Info Response error "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v1, p1, v1 */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
/* const v5, 0x110f024d */
/* .line 894 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 896 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void parseKeyboardUpdateInfo ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "data" # [B */
/* .line 819 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mgetTriggerWorker ( v0 );
/* .line 820 */
/* .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v2, p1, v1 */
final String v3 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v3, "MiuiPadKeyboard_IICNodeHelper"
/* if-nez v2, :cond_2 */
/* .line 821 */
final String v1 = "Receiver Keyboard/TouchPad Upgrade Mode"; // const-string v1, "Receiver Keyboard/TouchPad Upgrade Mode"
android.util.Slog .i ( v3,v1 );
/* .line 822 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyUpgradeCommandWorker ( v1 );
v1 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).isWorkRunning ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 823 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
v3 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* .line 825 */
} // :cond_0
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadCommandWorker ( v1 );
v1 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).isWorkRunning ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 826 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
v3 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* .line 829 */
} // :cond_1
} // :goto_0
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* .line 830 */
} // :cond_2
/* aget-byte v2, p1, v1 */
int v4 = 7; // const/4 v4, 0x7
/* if-ne v2, v4, :cond_3 */
/* .line 831 */
final String v1 = "The Keyboard Upgrade not match devices"; // const-string v1, "The Keyboard Upgrade not match devices"
android.util.Slog .e ( v3,v1 );
/* .line 832 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 834 */
} // :cond_3
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 835 */
/* const-string/jumbo v2, "stop upgrade keyboard and touch pad" */
android.util.Slog .i ( v3,v2 );
/* .line 836 */
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v3 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Upgrade Info Response error"; // const-string v5, "Upgrade Info Response error"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v1, p1, v1 */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 838 */
v1 = this.this$0;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v1 ).shutdownAllCommandQueue ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->shutdownAllCommandQueue()V
/* .line 840 */
} // :goto_1
return;
} // .end method
private void parseKeyboardUpdatePackageInfo ( Object[] p0 ) {
/* .locals 8 */
/* .param p1, "data" # [B */
/* .line 843 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mgetTriggerWorker ( v0 );
/* .line 844 */
/* .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v2, p1, v1 */
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_5 */
/* .line 846 */
v1 = this.this$0;
/* const/16 v2, 0xa */
/* aget-byte v2, p1, v2 */
/* shl-int/lit8 v2, v2, 0x10 */
/* const/high16 v4, 0xff0000 */
/* and-int/2addr v2, v4 */
/* const/16 v4, 0x9 */
/* aget-byte v4, p1, v4 */
/* const/16 v5, 0x8 */
/* shl-int/2addr v4, v5 */
/* const v6, 0xff00 */
/* and-int/2addr v4, v6 */
/* add-int/2addr v2, v4 */
/* aget-byte v4, p1, v5 */
/* and-int/lit16 v4, v4, 0xff */
/* add-int/2addr v2, v4 */
/* div-int/lit8 v2, v2, 0x34 */
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmReceiverBinIndex ( v1,v2 );
/* .line 849 */
int v1 = 0; // const/4 v1, 0x0
/* .line 850 */
/* .local v1, "progress":F */
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* const/16 v4, 0x38 */
/* const/high16 v5, 0x42c80000 # 100.0f */
/* const v6, 0x461c4000 # 10000.0f */
/* const/16 v7, 0x40 */
/* if-ne v2, v4, :cond_1 */
/* .line 851 */
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v2 );
v4 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeUtil ( v4 );
v4 = (( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v4 ).getBinPacketTotal ( v7 ); // invoke-virtual {v4, v7}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPacketTotal(I)I
/* sub-int/2addr v4, v3 */
/* if-ne v2, v4, :cond_0 */
/* .line 853 */
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmIsBinDataOK ( v2,v3 );
/* .line 855 */
} // :cond_0
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v2 );
/* int-to-float v2, v2 */
v3 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeUtil ( v3 );
/* .line 857 */
v3 = (( com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ) v3 ).getBinPacketTotal ( v7 ); // invoke-virtual {v3, v7}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPacketTotal(I)I
/* int-to-float v3, v3 */
/* div-float/2addr v2, v3 */
/* mul-float/2addr v2, v6 */
/* div-float/2addr v2, v5 */
/* .line 855 */
v2 = java.lang.Math .round ( v2 );
/* int-to-float v1, v2 */
/* .line 860 */
} // :cond_1
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* if-ne v2, v7, :cond_3 */
/* .line 861 */
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v2 );
v4 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeUtil ( v4 );
v4 = (( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v4 ).getBinPacketTotal ( v7 ); // invoke-virtual {v4, v7}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPacketTotal(I)I
/* sub-int/2addr v4, v3 */
/* if-ne v2, v4, :cond_2 */
/* .line 863 */
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmIsBinDataOK ( v2,v3 );
/* .line 865 */
} // :cond_2
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v2 );
/* int-to-float v2, v2 */
v3 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadUpgradeUtil ( v3 );
/* .line 867 */
v3 = (( com.android.server.input.padkeyboard.iic.TouchPadUpgradeUtil ) v3 ).getBinPacketTotal ( v7 ); // invoke-virtual {v3, v7}, Lcom/android/server/input/padkeyboard/iic/TouchPadUpgradeUtil;->getBinPacketTotal(I)I
/* int-to-float v3, v3 */
/* div-float/2addr v2, v3 */
/* mul-float/2addr v2, v6 */
/* div-float/2addr v2, v5 */
/* .line 865 */
v2 = java.lang.Math .round ( v2 );
/* int-to-float v1, v2 */
/* .line 870 */
} // :cond_3
final String v2 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v2, "MiuiPadKeyboard_IICNodeHelper"
final String v3 = "not match command worker, stop send pkg info"; // const-string v3, "not match command worker, stop send pkg info"
android.util.Slog .e ( v2,v3 );
/* .line 871 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 874 */
} // :goto_0
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v2 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponseForPackage ( p1, v2 ); // invoke-virtual {v0, p1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponseForPackage([BI)V
/* .line 875 */
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 876 */
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v3 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* .line 878 */
} // .end local v1 # "progress":F
} // :cond_4
/* .line 879 */
} // :cond_5
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v4 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Keyboard Bin Package Info Response error "; // const-string v6, "Keyboard Bin Package Info Response error "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v1, p1, v1 */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v5, 0x110f024d */
/* .line 882 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 884 */
} // :goto_1
return;
} // .end method
private void parseKeyboardUpgradeFlash ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "data" # [B */
/* .line 899 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mgetTriggerWorker ( v0 );
/* .line 900 */
/* .local v0, "runningWorker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v2, p1, v1 */
/* if-nez v2, :cond_0 */
/* .line 901 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* .line 902 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
v2 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
int v3 = 2; // const/4 v3, 0x2
/* .line 904 */
} // :cond_0
/* aget-byte v2, p1, v1 */
int v3 = 7; // const/4 v3, 0x7
/* if-ne v2, v3, :cond_1 */
/* .line 905 */
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
final String v2 = "Keyboard is busy upgrading"; // const-string v2, "Keyboard is busy upgrading"
android.util.Slog .i ( v1,v2 );
/* .line 907 */
} // :cond_1
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v3 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).getTargetAddress ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->getTargetAddress()B
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Keyboard Flash Info Response error "; // const-string v5, "Keyboard Flash Info Response error "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v1, p1, v1 */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v4 = 1; // const/4 v4, 0x1
/* const v5, 0x110f024d */
/* .line 910 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 912 */
} // :goto_0
return;
} // .end method
private void parseKeyboardVersionInfo ( Object[] p0 ) {
/* .locals 7 */
/* .param p1, "data" # [B */
/* .line 736 */
/* array-length v0, p1 */
/* const/16 v1, 0x13 */
/* if-ge v0, v1, :cond_0 */
/* .line 737 */
return;
/* .line 739 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 740 */
/* .local v0, "keyboardType":I */
int v1 = 2; // const/4 v1, 0x2
/* aget-byte v2, p1, v1 */
/* const/16 v3, 0x38 */
/* const/16 v4, 0x8 */
/* const/16 v5, 0x9 */
final String v6 = "%02x"; // const-string v6, "%02x"
/* if-ne v2, v3, :cond_8 */
/* .line 742 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeFilePath ( v1 );
int v2 = 0; // const/4 v2, 0x0
(( java.lang.StringBuilder ) v1 ).setLength ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 744 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeFilePath ( v1 );
final String v3 = "Keyboard_Upgrade"; // const-string v3, "Keyboard_Upgrade"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 746 */
int v1 = 5; // const/4 v1, 0x5
/* aget-byte v3, p1, v1 */
/* if-eq v3, v1, :cond_5 */
/* .line 747 */
v1 = this.this$0;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-byte v5, p1, v5 */
java.lang.Byte .valueOf ( v5 );
/* filled-new-array {v5}, [Ljava/lang/Object; */
java.lang.String .format ( v6,v5 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v4, p1, v4 */
/* .line 748 */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
/* .line 747 */
java.lang.String .format ( v6,v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmTouchPadVersion ( v1,v3 );
/* .line 749 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
v3 = this.this$0;
v3 = this.mNanoSocketCallback;
v3 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmTouchPadVersion ( v3 );
/* const/16 v4, 0x28 */
/* .line 752 */
/* const/16 v1, 0xd */
/* aget-byte v3, p1, v1 */
v3 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .hasTouchpad ( v3 );
/* .line 753 */
/* .local v3, "hasTouchPad":Z */
v4 = this.this$0;
v4 = this.mNanoSocketCallback;
/* aget-byte v5, p1, v1 */
/* .line 754 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 755 */
/* const/16 v4, 0x200 */
} // :cond_1
/* const/16 v4, 0x100 */
} // :goto_0
/* or-int/2addr v0, v4 */
/* .line 756 */
/* aget-byte v1, p1, v1 */
/* const/16 v4, 0xe */
/* aget-byte v4, p1, v4 */
/* invoke-direct {p0, v1, v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->getSuitableBinName(BB)V */
/* .line 757 */
v1 = this.this$0;
/* const/16 v4, 0x11 */
/* aget-byte v4, p1, v4 */
int v5 = 1; // const/4 v5, 0x1
/* if-ne v4, v5, :cond_2 */
/* move v2, v5 */
} // :cond_2
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmIsBleKeyboard ( v1,v2 );
/* .line 759 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 760 */
/* .local v1, "commonConfig":Lcom/android/server/input/config/InputCommonConfig; */
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardType ( v2 );
/* const/16 v4, 0x20 */
/* if-eq v2, v4, :cond_3 */
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardType ( v2 );
/* const/16 v4, 0x21 */
/* if-eq v2, v4, :cond_3 */
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardType ( v2 );
/* const/16 v4, 0x10 */
/* if-ne v2, v4, :cond_4 */
/* .line 763 */
} // :cond_3
/* const v2, 0x3d8f5c29 # 0.07f */
(( com.android.server.input.config.InputCommonConfig ) v1 ).setTopGestureHotZoneHeightRate ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setTopGestureHotZoneHeightRate(F)V
/* .line 764 */
/* const v2, 0x3d75c28f # 0.06f */
(( com.android.server.input.config.InputCommonConfig ) v1 ).setSlidGestureHotZoneWidthRate ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setSlidGestureHotZoneWidthRate(F)V
/* .line 766 */
} // :cond_4
(( com.android.server.input.config.InputCommonConfig ) v1 ).flushToNative ( ); // invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 768 */
} // .end local v1 # "commonConfig":Lcom/android/server/input/config/InputCommonConfig;
} // .end local v3 # "hasTouchPad":Z
} // :cond_5
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyBoardUpgradeFilePath ( v1 );
final String v2 = ".bin"; // const-string v2, ".bin"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 769 */
/* const/16 v1, 0xa */
/* aget-byte v1, p1, v1 */
/* and-int/lit16 v1, v1, 0xff */
/* or-int/2addr v0, v1 */
/* .line 770 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v1 );
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .getInstance ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) v1 ).setKeyboardType ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardType(I)V
/* .line 771 */
v1 = this.this$0;
v1 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardType ( v1 );
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isKeyboardSupportMuteLight ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 772 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v1 );
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .getInstance ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) v1 ).registerMuteLightReceiver ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->registerMuteLightReceiver()V
/* .line 775 */
} // :cond_6
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "notify_keyboard_info_changed"; // const-string v2, "notify_keyboard_info_changed"
android.provider.Settings$System .putInt ( v1,v2,v0 );
/* .line 776 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardVersionWorker ( v1 );
v1 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).isWorkRunning ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->isWorkRunning()Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 778 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardVersionWorker ( v1 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).triggerResponse ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* .line 780 */
} // :cond_7
v1 = this.this$0;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
int v3 = 7; // const/4 v3, 0x7
/* aget-byte v3, p1, v3 */
java.lang.Byte .valueOf ( v3 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
java.lang.String .format ( v6,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v3 = 6; // const/4 v3, 0x6
/* aget-byte v3, p1, v3 */
/* .line 781 */
java.lang.Byte .valueOf ( v3 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
/* .line 780 */
java.lang.String .format ( v6,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmKeyboardVersion ( v1,v2 );
/* .line 782 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardVersion ( v2 );
/* const/16 v3, 0x14 */
/* .line 785 */
} // :cond_8
/* aget-byte v1, p1, v1 */
/* const/16 v2, 0x39 */
/* if-ne v1, v2, :cond_9 */
/* .line 786 */
v1 = this.this$0;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-byte v3, p1, v5 */
java.lang.Byte .valueOf ( v3 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
java.lang.String .format ( v6,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v3, p1, v4 */
/* .line 787 */
java.lang.Byte .valueOf ( v3 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
java.lang.String .format ( v6,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmKeyboardFlashVersion ( v1,v2 );
/* .line 788 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardFlashVersion ( v2 );
int v3 = 3; // const/4 v3, 0x3
/* .line 792 */
} // :cond_9
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void responseFromKeyboard ( Object[] p0 ) {
/* .locals 7 */
/* .param p1, "data" # [B */
/* .line 577 */
/* array-length v0, p1 */
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
int v2 = 6; // const/4 v2, 0x6
/* if-ge v0, v2, :cond_0 */
/* .line 578 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception Data:"; // const-string v2, "Exception Data:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, p1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 579 */
return;
/* .line 581 */
} // :cond_0
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v0, p1, v0 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 5; // const/4 v4, 0x5
int v5 = 7; // const/4 v5, 0x7
int v6 = 1; // const/4 v6, 0x1
/* sparse-switch v0, :sswitch_data_0 */
/* .line 729 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Other Command:"; // const-string v2, "Other Command:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, p1 */
/* .line 730 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 729 */
android.util.Slog .e ( v1,v0 );
/* goto/16 :goto_0 */
/* .line 690 */
/* :sswitch_0 */
/* aget-byte v0, p1, v2 */
/* if-ne v0, v6, :cond_f */
/* .line 691 */
final String v0 = "Receiver Nfc Device"; // const-string v0, "Receiver Nfc Device"
android.util.Slog .i ( v1,v0 );
/* .line 692 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* goto/16 :goto_0 */
/* .line 634 */
/* :sswitch_1 */
v0 = this.this$0;
final String v1 = "Receiver GSensor status"; // const-string v1, "Receiver GSensor status"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 635 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseGsensorDataInfo([B)V */
/* .line 636 */
/* goto/16 :goto_0 */
/* .line 703 */
/* :sswitch_2 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmIsBleKeyboard ( v0 );
/* if-nez v0, :cond_1 */
/* .line 704 */
/* goto/16 :goto_0 */
/* .line 706 */
} // :cond_1
v0 = this.this$0;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const/16 v2, 0x13 */
/* aget-byte v2, p1, v2 */
java.lang.Byte .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ":"; // const-string v2, ":"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x14 */
/* aget-byte v4, p1, v4 */
/* .line 707 */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x15 */
/* aget-byte v4, p1, v4 */
/* .line 708 */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x16 */
/* aget-byte v4, p1, v4 */
/* .line 709 */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x17 */
/* aget-byte v4, p1, v4 */
/* .line 710 */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x18 */
/* aget-byte v2, p1, v2 */
/* .line 711 */
java.lang.Byte .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmKeyboardBleAdd ( v0,v1 );
/* .line 712 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msendBroadCast2Ble ( v0 );
/* .line 713 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmKeyboardBleAdd ( v1 );
int v2 = -2; // const/4 v2, -0x2
final String v3 = "miui_keyboard_address"; // const-string v3, "miui_keyboard_address"
android.provider.Settings$System .putStringForUser ( v0,v3,v1,v2 );
/* .line 716 */
/* goto/16 :goto_0 */
/* .line 642 */
/* :sswitch_3 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmAuthCommandResponse ( v0,p1 );
/* .line 643 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmCountDownLatch ( v0 );
(( java.util.concurrent.CountDownLatch ) v0 ).countDown ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
/* .line 644 */
/* goto/16 :goto_0 */
/* .line 664 */
/* :sswitch_4 */
v0 = this.this$0;
final String v5 = "Receiver Keyboard sleep status"; // const-string v5, "Receiver Keyboard sleep status"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v5 );
/* .line 665 */
/* aget-byte v0, p1, v4 */
/* if-eq v0, v6, :cond_2 */
/* .line 666 */
final String v0 = "parse KeyboardSleepStatus fail!"; // const-string v0, "parse KeyboardSleepStatus fail!"
android.util.Slog .e ( v1,v0 );
/* .line 667 */
/* goto/16 :goto_0 */
/* .line 670 */
} // :cond_2
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_3 */
/* .line 671 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* goto/16 :goto_0 */
/* .line 672 */
} // :cond_3
/* aget-byte v0, p1, v2 */
/* if-ne v0, v6, :cond_f */
/* .line 673 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* goto/16 :goto_0 */
/* .line 677 */
/* :sswitch_5 */
/* aget-byte v0, p1, v4 */
/* if-eq v0, v6, :cond_4 */
/* .line 678 */
final String v0 = "parse keyboard power fail!"; // const-string v0, "parse keyboard power fail!"
android.util.Slog .e ( v1,v0 );
/* .line 679 */
/* goto/16 :goto_0 */
/* .line 682 */
} // :cond_4
/* aget-byte v0, p1, v2 */
/* if-ne v0, v6, :cond_5 */
/* .line 683 */
final String v0 = "Set keyboard power on"; // const-string v0, "Set keyboard power on"
android.util.Slog .i ( v1,v0 );
/* goto/16 :goto_0 */
/* .line 686 */
} // :cond_5
final String v0 = "Set keyboard power low"; // const-string v0, "Set keyboard power low"
android.util.Slog .i ( v1,v0 );
/* .line 688 */
/* goto/16 :goto_0 */
/* .line 718 */
/* :sswitch_6 */
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_6 */
/* .line 719 */
final String v0 = "keyboard has restart! "; // const-string v0, "keyboard has restart! "
android.util.Slog .i ( v1,v0 );
/* .line 720 */
/* goto/16 :goto_0 */
/* .line 722 */
} // :cond_6
/* aget-byte v0, p1, v2 */
/* const/16 v3, 0x64 */
/* if-eq v0, v3, :cond_7 */
/* .line 723 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "keyboard reauth data error! "; // const-string v3, "keyboard reauth data error! "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 724 */
/* goto/16 :goto_0 */
/* .line 726 */
} // :cond_7
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* .line 727 */
/* goto/16 :goto_0 */
/* .line 696 */
/* :sswitch_7 */
int v0 = 0; // const/4 v0, 0x0
/* .line 697 */
/* .local v0, "isEnable":Z */
/* aget-byte v1, p1, v4 */
/* if-ne v1, v6, :cond_8 */
/* .line 698 */
int v0 = 1; // const/4 v0, 0x1
/* .line 700 */
} // :cond_8
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
/* .line 701 */
/* goto/16 :goto_0 */
/* .line 646 */
} // .end local v0 # "isEnable":Z
/* :sswitch_8 */
v0 = this.this$0;
final String v5 = "Receiver Keyboard recover firmware status"; // const-string v5, "Receiver Keyboard recover firmware status"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v5 );
/* .line 647 */
/* aget-byte v0, p1, v4 */
/* if-eq v0, v6, :cond_9 */
/* .line 648 */
final String v0 = "parse keyboard recover firmware status fail!"; // const-string v0, "parse keyboard recover firmware status fail!"
android.util.Slog .e ( v1,v0 );
/* .line 649 */
/* goto/16 :goto_0 */
/* .line 651 */
} // :cond_9
/* aget-byte v0, p1, v2 */
/* const/16 v1, 0x36 */
/* if-ne v0, v1, :cond_f */
/* .line 652 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v0 );
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmContext ( v1 );
/* .line 653 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110f0241 */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 652 */
android.widget.Toast .makeText ( v0,v1,v3 );
/* .line 655 */
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 656 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmIsBleKeyboard ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 658 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msendBroadCast2Ble ( v0,v3 );
/* .line 659 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$msendBroadCast2Ble ( v0 );
/* goto/16 :goto_0 */
/* .line 604 */
/* :sswitch_9 */
v0 = this.this$0;
final String v1 = "Receiver Keyboard Flash"; // const-string v1, "Receiver Keyboard Flash"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 605 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpgradeFlash([B)V */
/* .line 606 */
/* goto/16 :goto_0 */
/* .line 600 */
/* :sswitch_a */
v0 = this.this$0;
final String v1 = "Receiver Keyboard Upgrade End"; // const-string v1, "Receiver Keyboard Upgrade End"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 601 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpdateFinishInfo([B)V */
/* .line 602 */
/* goto/16 :goto_0 */
/* .line 590 */
/* :sswitch_b */
v0 = this.this$0;
final String v1 = "Receiver Keyboard Upgrade"; // const-string v1, "Receiver Keyboard Upgrade"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 591 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpdateInfo([B)V */
/* .line 592 */
/* goto/16 :goto_0 */
/* .line 584 */
/* :sswitch_c */
v0 = this.this$0;
final String v1 = "Receiver keyBoard/Flash Version"; // const-string v1, "Receiver keyBoard/Flash Version"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 586 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardVersionInfo([B)V */
/* .line 587 */
/* goto/16 :goto_0 */
/* .line 617 */
/* :sswitch_d */
v0 = this.this$0;
final String v2 = "Receiver 803 report read Keyboard status"; // const-string v2, "Receiver 803 report read Keyboard status"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v2 );
/* .line 618 */
/* aget-byte v0, p1, v5 */
/* if-nez v0, :cond_a */
/* .line 619 */
/* aget-byte v0, p1, v4 */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_BACKLIGHT;
v2 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v2 ).getCommand ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getCommand()B
/* if-ne v0, v2, :cond_f */
/* .line 620 */
/* const-string/jumbo v0, "set backLight success" */
android.util.Slog .i ( v1,v0 );
/* .line 623 */
} // :cond_a
/* aget-byte v0, p1, v5 */
/* if-ne v0, v6, :cond_b */
/* .line 624 */
final String v0 = "Command word not supported/malformed"; // const-string v0, "Command word not supported/malformed"
android.util.Slog .e ( v1,v0 );
/* .line 625 */
} // :cond_b
/* aget-byte v0, p1, v5 */
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_c */
/* .line 626 */
final String v0 = "Keyboard is disconnection, Command write fail"; // const-string v0, "Keyboard is disconnection, Command write fail"
android.util.Slog .e ( v1,v0 );
/* .line 627 */
} // :cond_c
/* aget-byte v0, p1, v5 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v0, v2, :cond_d */
/* .line 628 */
final String v0 = "MCU is busy,The Command lose possibility"; // const-string v0, "MCU is busy,The Command lose possibility"
android.util.Slog .e ( v1,v0 );
/* .line 630 */
} // :cond_d
final String v0 = "Other Error"; // const-string v0, "Other Error"
android.util.Slog .e ( v1,v0 );
/* .line 632 */
/* .line 608 */
/* :sswitch_e */
v0 = this.this$0;
final String v2 = "Receiver Keyboard Search Status"; // const-string v2, "Receiver Keyboard Search Status"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v2 );
/* .line 609 */
/* aget-byte v0, p1, v5 */
/* if-nez v0, :cond_e */
/* .line 610 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardStatus([B)V */
/* .line 612 */
} // :cond_e
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "read Mcu Status error : "; // const-string v2, "read Mcu Status error : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v5 */
java.lang.Byte .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "0x%02x"; // const-string v3, "0x%02x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 614 */
/* .line 595 */
/* :sswitch_f */
v0 = this.this$0;
final String v1 = "Receiver Keyboard Bin Package"; // const-string v1, "Receiver Keyboard Bin Package"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 596 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->parseKeyboardUpdatePackageInfo([B)V */
/* .line 597 */
/* nop */
/* .line 733 */
} // :cond_f
} // :goto_0
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6f -> :sswitch_f */
/* -0x5e -> :sswitch_e */
/* -0x10 -> :sswitch_d */
/* 0x1 -> :sswitch_c */
/* 0x2 -> :sswitch_b */
/* 0x4 -> :sswitch_a */
/* 0x6 -> :sswitch_9 */
/* 0x20 -> :sswitch_8 */
/* 0x22 -> :sswitch_7 */
/* 0x24 -> :sswitch_6 */
/* 0x25 -> :sswitch_5 */
/* 0x28 -> :sswitch_4 */
/* 0x31 -> :sswitch_3 */
/* 0x32 -> :sswitch_3 */
/* 0x33 -> :sswitch_3 */
/* 0x34 -> :sswitch_3 */
/* 0x35 -> :sswitch_3 */
/* 0x52 -> :sswitch_2 */
/* 0x64 -> :sswitch_1 */
/* 0x68 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void responseFromMCU ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "data" # [B */
/* .line 984 */
/* array-length v0, p1 */
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
int v2 = 6; // const/4 v2, 0x6
/* if-ge v0, v2, :cond_0 */
/* .line 985 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception Data:"; // const-string v2, "Exception Data:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, p1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 986 */
return;
/* .line 988 */
} // :cond_0
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v0, p1, v0 */
int v3 = 2; // const/4 v3, 0x2
/* const/16 v4, 0x18 */
/* sparse-switch v0, :sswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 1069 */
/* :sswitch_0 */
v0 = this.this$0;
final String v1 = "Receiver MCU Flash"; // const-string v1, "Receiver MCU Flash"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 1070 */
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_1 */
/* .line 1071 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* goto/16 :goto_0 */
/* .line 1073 */
} // :cond_1
/* aget-byte v0, p1, v2 */
int v1 = 5; // const/4 v1, 0x5
/* if-eq v0, v1, :cond_8 */
/* .line 1074 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 1075 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MCU Flash error:"; // const-string v3, "MCU Flash error:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* goto/16 :goto_0 */
/* .line 1057 */
/* :sswitch_1 */
v0 = this.this$0;
final String v1 = "Receiver MCU UpEnd"; // const-string v1, "Receiver MCU UpEnd"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 1059 */
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_2 */
/* .line 1060 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* goto/16 :goto_0 */
/* .line 1062 */
} // :cond_2
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "UpEnd error:"; // const-string v3, "UpEnd error:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1065 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 1067 */
/* goto/16 :goto_0 */
/* .line 1041 */
/* :sswitch_2 */
v0 = this.this$0;
final String v1 = "Receiver MCU Reset"; // const-string v1, "Receiver MCU Reset"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 1042 */
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_3 */
/* .line 1043 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmMCUVersion ( v0,v1 );
/* .line 1044 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* .line 1045 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* goto/16 :goto_0 */
/* .line 1049 */
} // :cond_3
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Reset error:"; // const-string v3, "Reset error:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1052 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 1054 */
/* goto/16 :goto_0 */
/* .line 998 */
/* :sswitch_3 */
v0 = this.this$0;
final String v3 = "Receiver MCU Upgrade"; // const-string v3, "Receiver MCU Upgrade"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v3 );
/* .line 999 */
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_4 */
/* .line 1000 */
final String v0 = "receiver MCU Upgrade start"; // const-string v0, "receiver MCU Upgrade start"
android.util.Slog .i ( v1,v0 );
/* .line 1001 */
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
int v1 = 0; // const/4 v1, 0x0
/* .line 1004 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).triggerResponse ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponse([B)V
/* goto/16 :goto_0 */
/* .line 1006 */
} // :cond_4
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MCU Upgrade info error"; // const-string v3, "MCU Upgrade info error"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1009 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 1011 */
/* goto/16 :goto_0 */
/* .line 991 */
/* :sswitch_4 */
v0 = this.this$0;
final String v1 = "Receiver get MCU version"; // const-string v1, "Receiver get MCU version"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 992 */
/* aget-byte v0, p1, v2 */
/* if-ne v0, v3, :cond_8 */
/* .line 993 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$SocketCallbackListener;->onMcuVersionResponse([B)V */
/* goto/16 :goto_0 */
/* .line 1013 */
/* :sswitch_5 */
v0 = this.this$0;
final String v1 = "Receiver MCU Bin Package"; // const-string v1, "Receiver MCU Bin Package"
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$mcheckDataPkgSum ( v0,p1,v1 );
/* .line 1014 */
/* aget-byte v0, p1, v2 */
/* if-nez v0, :cond_7 */
/* .line 1015 */
/* const/16 v0, 0xa */
/* aget-byte v0, p1, v0 */
/* shl-int/lit8 v0, v0, 0x10 */
/* const/high16 v1, 0xff0000 */
/* and-int/2addr v0, v1 */
/* const/16 v1, 0x9 */
/* aget-byte v1, p1, v1 */
/* const/16 v2, 0x8 */
/* shl-int/2addr v1, v2 */
/* const v3, 0xff00 */
/* and-int/2addr v1, v3 */
/* add-int/2addr v0, v1 */
/* aget-byte v1, p1, v2 */
/* and-int/lit16 v1, v1, 0xff */
/* add-int/2addr v0, v1 */
/* div-int/lit8 v0, v0, 0x34 */
/* .line 1018 */
/* .local v0, "nextIndex":I */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmReceiverBinIndex ( v1,v0 );
/* .line 1019 */
v1 = this.this$0;
v1 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v1 );
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMcuUpgradeUtil ( v2 );
/* const/16 v3, 0x40 */
v2 = (( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v2 ).getBinPacketTotal ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPacketTotal(I)I
int v5 = 1; // const/4 v5, 0x1
/* sub-int/2addr v2, v5 */
/* if-ne v1, v2, :cond_5 */
/* .line 1020 */
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fputmIsBinDataOK ( v1,v5 );
/* .line 1022 */
} // :cond_5
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v1 );
v2 = this.this$0;
v2 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v2 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v1 ).triggerResponseForPackage ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->triggerResponseForPackage([BI)V
/* .line 1024 */
v1 = this.this$0;
v1 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReceiverBinIndex ( v1 );
/* int-to-float v1, v1 */
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMcuUpgradeUtil ( v2 );
/* .line 1025 */
v2 = (( com.android.server.input.padkeyboard.iic.McuUpgradeUtil ) v2 ).getBinPacketTotal ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/padkeyboard/iic/McuUpgradeUtil;->getBinPacketTotal(I)I
/* int-to-float v2, v2 */
/* div-float/2addr v1, v2 */
/* const v2, 0x461c4000 # 10000.0f */
/* mul-float/2addr v1, v2 */
/* const/high16 v2, 0x42c80000 # 100.0f */
/* div-float/2addr v1, v2 */
/* .line 1024 */
v1 = java.lang.Math .round ( v1 );
/* int-to-float v1, v1 */
/* .line 1027 */
/* .local v1, "progress":F */
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 1028 */
v2 = this.this$0;
v2 = this.mNanoSocketCallback;
/* .line 1032 */
} // .end local v0 # "nextIndex":I
} // .end local v1 # "progress":F
} // :cond_6
/* .line 1033 */
} // :cond_7
v0 = this.this$0;
v0 = this.mNanoSocketCallback;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MCU Package info error:"; // const-string v3, "MCU Package info error:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1036 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmMCUUpgradeCommandWorker ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v0 ).onWorkException ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->onWorkException([B)V
/* .line 1038 */
/* nop */
/* .line 1084 */
} // :cond_8
} // :goto_0
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x6f -> :sswitch_5 */
/* 0x1 -> :sswitch_4 */
/* 0x2 -> :sswitch_3 */
/* 0x3 -> :sswitch_2 */
/* 0x4 -> :sswitch_1 */
/* 0x6 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
