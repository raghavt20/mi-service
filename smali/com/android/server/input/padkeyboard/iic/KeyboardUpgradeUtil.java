public class com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil {
	 /* .source "KeyboardUpgradeUtil.java" */
	 /* # static fields */
	 private static final Integer START_INFO_INDEX_FOR_176;
	 private static final Integer START_INFO_INDEX_FOR_179;
	 private static final Integer START_INFO_INDEX_FOR_8012;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public Integer mBinCheckSum;
	 public mBinCheckSumByte;
	 public Integer mBinLength;
	 public mBinLengthByte;
	 public mBinStartAddressByte;
	 public mBinVersion;
	 public java.lang.String mBinVersionStr;
	 private mFileBuf;
	 private Object mKeyType;
	 private Object mNFCType;
	 private Integer mStartInfoIndex;
	 private Object mTouchType;
	 public java.lang.String mUpgradeFilePath;
	 public Boolean mValid;
	 /* # direct methods */
	 public com.android.server.input.padkeyboard.iic.KeyboardUpgradeUtil ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "path" # Ljava/lang/String; */
		 /* .param p3, "keyType" # B */
		 /* .line 38 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* new-array v1, v0, [B */
		 this.mBinLengthByte = v1;
		 /* .line 29 */
		 /* new-array v1, v0, [B */
		 this.mBinCheckSumByte = v1;
		 /* .line 31 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* new-array v1, v1, [B */
		 this.mBinVersion = v1;
		 /* .line 33 */
		 /* new-array v0, v0, [B */
		 this.mBinStartAddressByte = v0;
		 /* .line 39 */
		 final String v0 = "KeyboardUpgradeUtil"; // const-string v0, "KeyboardUpgradeUtil"
		 /* if-nez p2, :cond_0 */
		 /* .line 40 */
		 final String v1 = "UpgradeOta file Path is null"; // const-string v1, "UpgradeOta file Path is null"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 41 */
		 return;
		 /* .line 43 */
	 } // :cond_0
	 this.mUpgradeFilePath = p2;
	 /* .line 45 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "UpgradeOta file Path:"; // const-string v2, "UpgradeOta file Path:"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mUpgradeFilePath;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v0,v1 );
	 /* .line 46 */
	 v1 = this.mUpgradeFilePath;
	 /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->ReadUpgradeFile(Ljava/lang/String;)[B */
	 this.mFileBuf = v1;
	 /* .line 48 */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 49 */
		 v0 = 		 /* invoke-direct {p0, p3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFile(B)Z */
		 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mValid:Z */
		 /* .line 51 */
	 } // :cond_1
	 final String v1 = "UpgradeOta file buff is null or length low than 6000"; // const-string v1, "UpgradeOta file buff is null or length low than 6000"
	 android.util.Slog .i ( v0,v1 );
	 /* .line 53 */
} // :goto_0
return;
} // .end method
private ReadUpgradeFile ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 67 */
int v0 = 0; // const/4 v0, 0x0
/* .line 68 */
/* .local v0, "fileBuf":[B */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 69 */
/* .local v1, "file":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 70 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->readFileToBuf(Ljava/lang/String;)[B */
	 /* .line 72 */
} // :cond_0
final String v2 = "KeyboardUpgradeUtil"; // const-string v2, "KeyboardUpgradeUtil"
final String v3 = "=== The Upgrade bin file does not exist."; // const-string v3, "=== The Upgrade bin file does not exist."
android.util.Slog .i ( v2,v3 );
/* .line 74 */
} // :goto_0
} // .end method
private Boolean parseOtaFile ( Object p0 ) {
/* .locals 1 */
/* .param p1, "keyType" # B */
/* .line 56 */
/* const/16 v0, 0x21 */
/* if-ne p1, v0, :cond_0 */
/* .line 57 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFileFor179()Z */
/* .line 59 */
} // :cond_0
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
/* if-nez v0, :cond_1 */
/* .line 60 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFileFor8012()Z */
/* .line 62 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->parseOtaFileFor176()Z */
} // .end method
private Boolean parseOtaFileFor176 ( ) {
/* .locals 18 */
/* .line 105 */
/* move-object/from16 v0, p0 */
/* const v1, 0x20fc0 */
/* iput v1, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* .line 106 */
v2 = this.mFileBuf;
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
/* array-length v4, v2 */
/* const v5, 0x21000 */
/* if-le v4, v5, :cond_2 */
/* .line 107 */
/* const v4, 0x20fc0 */
/* .line 109 */
/* .local v4, "fileHeadBaseAddr":I */
/* const/16 v6, 0x8 */
/* new-array v7, v6, [B */
/* .line 110 */
/* .local v7, "binStartFlagByte":[B */
/* array-length v8, v7 */
java.lang.System .arraycopy ( v2,v4,v7,v3,v8 );
/* .line 112 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v7 );
/* .line 113 */
/* .local v2, "binStartFlag":Ljava/lang/String; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Bin Start Flag: "; // const-string v9, "Bin Start Flag: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v9 = "KeyboardUpgradeUtil"; // const-string v9, "KeyboardUpgradeUtil"
android.util.Slog .i ( v9,v8 );
/* .line 114 */
/* add-int/2addr v4, v6 */
/* .line 116 */
v8 = this.mFileBuf;
v10 = this.mBinLengthByte;
/* array-length v11, v10 */
java.lang.System .arraycopy ( v8,v4,v10,v3,v11 );
/* .line 117 */
v8 = this.mBinLengthByte;
int v10 = 3; // const/4 v10, 0x3
/* aget-byte v11, v8, v10 */
/* shl-int/lit8 v11, v11, 0x18 */
/* const/high16 v12, -0x1000000 */
/* and-int/2addr v11, v12 */
int v13 = 2; // const/4 v13, 0x2
/* aget-byte v14, v8, v13 */
/* shl-int/lit8 v14, v14, 0x10 */
/* const/high16 v15, 0xff0000 */
/* and-int/2addr v14, v15 */
/* add-int/2addr v11, v14 */
int v14 = 1; // const/4 v14, 0x1
/* aget-byte v16, v8, v14 */
/* shl-int/lit8 v16, v16, 0x8 */
/* const v17, 0xff00 */
/* and-int v16, v16, v17 */
/* add-int v11, v11, v16 */
/* aget-byte v8, v8, v3 */
/* and-int/lit16 v8, v8, 0xff */
/* add-int/2addr v11, v8 */
/* iput v11, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
/* .line 121 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Bin Length: "; // const-string v11, "Bin Length: "
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v8 );
/* .line 122 */
/* add-int/lit8 v4, v4, 0x4 */
/* .line 124 */
v8 = this.mFileBuf;
v11 = this.mBinCheckSumByte;
/* array-length v5, v11 */
java.lang.System .arraycopy ( v8,v4,v11,v3,v5 );
/* .line 126 */
v5 = this.mBinCheckSumByte;
/* aget-byte v8, v5, v10 */
/* shl-int/lit8 v8, v8, 0x18 */
/* and-int/2addr v8, v12 */
/* aget-byte v11, v5, v13 */
/* shl-int/lit8 v11, v11, 0x10 */
/* and-int/2addr v11, v15 */
/* add-int/2addr v8, v11 */
/* aget-byte v11, v5, v14 */
/* shl-int/lit8 v6, v11, 0x8 */
/* and-int v6, v6, v17 */
/* add-int/2addr v8, v6 */
/* aget-byte v5, v5, v3 */
/* and-int/lit16 v5, v5, 0xff */
/* add-int/2addr v8, v5 */
/* iput v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
/* .line 130 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bin CheckSum: "; // const-string v6, "Bin CheckSum: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v5 );
/* .line 131 */
/* add-int/lit8 v4, v4, 0x4 */
/* .line 133 */
/* new-array v5, v13, [B */
this.mBinVersion = v5;
/* .line 134 */
v6 = this.mFileBuf;
/* array-length v8, v5 */
java.lang.System .arraycopy ( v6,v4,v5,v3,v8 );
/* .line 135 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.mBinVersion;
/* aget-byte v6, v6, v14 */
java.lang.Byte .valueOf ( v6 );
/* filled-new-array {v6}, [Ljava/lang/Object; */
final String v8 = "%02x"; // const-string v8, "%02x"
java.lang.String .format ( v8,v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mBinVersion;
/* aget-byte v6, v6, v3 */
/* .line 136 */
java.lang.Byte .valueOf ( v6 );
/* filled-new-array {v6}, [Ljava/lang/Object; */
/* .line 135 */
java.lang.String .format ( v8,v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mBinVersionStr = v5;
/* .line 137 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bin Version : "; // const-string v6, "Bin Version : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mBinVersionStr;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v5 );
/* .line 140 */
v5 = this.mFileBuf;
/* array-length v6, v5 */
/* sub-int/2addr v6, v1 */
/* add-int/lit8 v6, v6, -0x40 */
/* const v8, 0x21000 */
v5 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSumInt ( v5,v8,v6 );
/* .line 142 */
/* .local v5, "sum1":I */
/* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
final String v8 = "/"; // const-string v8, "/"
/* if-eq v5, v6, :cond_0 */
/* .line 143 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bin Check Check Sum Error:"; // const-string v6, "Bin Check Check Sum Error:"
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v1 );
/* .line 144 */
/* .line 148 */
} // :cond_0
v6 = this.mFileBuf;
/* const/16 v11, 0x3f */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v6,v1,v11 );
/* .line 149 */
/* .local v1, "sum2":B */
v6 = this.mFileBuf;
/* const v11, 0x20fff */
/* aget-byte v12, v6, v11 */
/* if-eq v1, v12, :cond_1 */
/* .line 150 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Bin Check Head Sum Error:"; // const-string v10, "Bin Check Head Sum Error:"
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mFileBuf;
/* aget-byte v8, v8, v11 */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v6 );
/* .line 153 */
/* .line 156 */
} // :cond_1
/* const v8, 0x210c8 */
/* aget-byte v8, v6, v8 */
/* iput-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
/* .line 157 */
/* const v8, 0x210c9 */
/* aget-byte v8, v6, v8 */
/* shr-int/lit8 v8, v8, 0x6 */
/* int-to-byte v8, v8 */
/* iput-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
/* .line 158 */
/* const v8, 0x210ca */
/* aget-byte v6, v6, v8 */
/* and-int/lit8 v6, v6, 0xf */
/* int-to-byte v6, v6 */
/* iput-byte v6, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
/* .line 159 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "For 176 mKeyType:"; // const-string v8, "For 176 mKeyType:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ", mNFCType:"; // const-string v8, ", mNFCType:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ", mTouchType:"; // const-string v8, ", mTouchType:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v6 );
/* .line 162 */
v6 = this.mBinStartAddressByte;
/* aput-byte v3, v6, v3 */
/* .line 163 */
/* const/16 v8, -0x80 */
/* aput-byte v8, v6, v14 */
/* .line 164 */
int v8 = 5; // const/4 v8, 0x5
/* aput-byte v8, v6, v13 */
/* .line 165 */
/* aput-byte v3, v6, v10 */
/* .line 166 */
/* .line 168 */
} // .end local v1 # "sum2":B
} // .end local v2 # "binStartFlag":Ljava/lang/String;
} // .end local v4 # "fileHeadBaseAddr":I
} // .end local v5 # "sum1":I
} // .end local v7 # "binStartFlagByte":[B
} // :cond_2
} // .end method
private Boolean parseOtaFileFor179 ( ) {
/* .locals 17 */
/* .line 173 */
/* move-object/from16 v0, p0 */
/* const/16 v1, 0x5fc0 */
/* iput v1, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* .line 174 */
v1 = this.mFileBuf;
final String v2 = "KeyboardUpgradeUtil"; // const-string v2, "KeyboardUpgradeUtil"
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* array-length v4, v1 */
/* const/16 v5, 0xfc0 */
/* if-le v4, v5, :cond_2 */
/* .line 175 */
/* const/16 v4, 0x5fc0 */
/* .line 177 */
/* .local v4, "mFileHeadBaseAddr":I */
/* const/16 v5, 0x8 */
/* new-array v6, v5, [B */
/* .line 178 */
/* .local v6, "binStartFlagByte":[B */
/* array-length v7, v6 */
java.lang.System .arraycopy ( v1,v4,v6,v3,v7 );
/* .line 180 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v6 );
/* .line 181 */
/* .local v1, "binStartFlag":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Bin Start Flag: "; // const-string v8, "Bin Start Flag: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v7 );
/* .line 182 */
/* add-int/2addr v4, v5 */
/* .line 185 */
v7 = this.mFileBuf;
v8 = this.mBinLengthByte;
/* array-length v9, v8 */
java.lang.System .arraycopy ( v7,v4,v8,v3,v9 );
/* .line 186 */
v7 = this.mBinLengthByte;
int v8 = 3; // const/4 v8, 0x3
/* aget-byte v9, v7, v8 */
/* shl-int/lit8 v9, v9, 0x18 */
/* const/high16 v10, -0x1000000 */
/* and-int/2addr v9, v10 */
int v11 = 2; // const/4 v11, 0x2
/* aget-byte v12, v7, v11 */
/* const/16 v13, 0x10 */
/* shl-int/2addr v12, v13 */
/* const/high16 v14, 0xff0000 */
/* and-int/2addr v12, v14 */
/* add-int/2addr v9, v12 */
int v12 = 1; // const/4 v12, 0x1
/* aget-byte v15, v7, v12 */
/* shl-int/2addr v15, v5 */
/* const v16, 0xff00 */
/* and-int v15, v15, v16 */
/* add-int/2addr v9, v15 */
/* aget-byte v7, v7, v3 */
/* and-int/lit16 v7, v7, 0xff */
/* add-int/2addr v9, v7 */
/* iput v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
/* .line 189 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Bin Lenght: "; // const-string v9, "Bin Lenght: "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v7 );
/* .line 190 */
/* add-int/lit8 v4, v4, 0x4 */
/* .line 193 */
v7 = this.mFileBuf;
v9 = this.mBinCheckSumByte;
/* array-length v15, v9 */
java.lang.System .arraycopy ( v7,v4,v9,v3,v15 );
/* .line 195 */
v7 = this.mBinCheckSumByte;
/* aget-byte v9, v7, v8 */
/* shl-int/lit8 v9, v9, 0x18 */
/* and-int/2addr v9, v10 */
/* aget-byte v10, v7, v11 */
/* shl-int/2addr v10, v13 */
/* and-int/2addr v10, v14 */
/* add-int/2addr v9, v10 */
/* aget-byte v10, v7, v12 */
/* shl-int/lit8 v5, v10, 0x8 */
/* and-int v5, v5, v16 */
/* add-int/2addr v9, v5 */
/* aget-byte v5, v7, v3 */
/* and-int/lit16 v5, v5, 0xff */
/* add-int/2addr v9, v5 */
/* iput v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
/* .line 198 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin CheckSum: "; // const-string v7, "Bin CheckSum: "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v5 );
/* .line 199 */
/* add-int/lit8 v4, v4, 0x4 */
/* .line 202 */
/* new-array v5, v11, [B */
this.mBinVersion = v5;
/* .line 203 */
v7 = this.mFileBuf;
/* array-length v9, v5 */
java.lang.System .arraycopy ( v7,v4,v5,v3,v9 );
/* .line 204 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v7 = this.mBinVersion;
/* aget-byte v7, v7, v12 */
java.lang.Byte .valueOf ( v7 );
/* filled-new-array {v7}, [Ljava/lang/Object; */
final String v9 = "%02x"; // const-string v9, "%02x"
java.lang.String .format ( v9,v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mBinVersion;
/* aget-byte v7, v7, v3 */
/* .line 205 */
java.lang.Byte .valueOf ( v7 );
/* filled-new-array {v7}, [Ljava/lang/Object; */
/* .line 204 */
java.lang.String .format ( v9,v7 );
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mBinVersionStr = v5;
/* .line 206 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin Version : "; // const-string v7, "Bin Version : "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mBinVersionStr;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v5 );
/* .line 209 */
v5 = this.mFileBuf;
/* iget v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/lit8 v9, v7, 0x40 */
/* array-length v10, v5 */
/* add-int/lit8 v7, v7, 0x40 */
/* sub-int/2addr v10, v7 */
v5 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSumInt ( v5,v9,v10 );
/* .line 211 */
/* .local v5, "sum1":I */
/* iget v7, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
final String v9 = "/"; // const-string v9, "/"
/* if-eq v5, v7, :cond_0 */
/* .line 212 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Bin Check Check Sum Error:"; // const-string v8, "Bin Check Check Sum Error:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v7 );
/* .line 213 */
/* .line 217 */
} // :cond_0
v7 = this.mFileBuf;
/* iget v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* const/16 v14, 0x3f */
v7 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v7,v10,v14 );
/* .line 218 */
/* .local v7, "sum2":B */
v10 = this.mFileBuf;
/* iget v15, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/2addr v15, v14 */
/* aget-byte v15, v10, v15 */
/* if-eq v7, v15, :cond_1 */
/* .line 219 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "Bin Check Head Sum Error:"; // const-string v10, "Bin Check Head Sum Error:"
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.mFileBuf;
/* iget v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/2addr v10, v14 */
/* aget-byte v9, v9, v10 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v8 );
/* .line 222 */
/* .line 225 */
} // :cond_1
/* const/16 v9, 0x6048 */
/* aget-byte v9, v10, v9 */
/* iput-byte v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
/* .line 226 */
/* const/16 v9, 0x6049 */
/* aget-byte v9, v10, v9 */
/* shr-int/lit8 v9, v9, 0x5 */
/* int-to-byte v9, v9 */
/* iput-byte v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
/* .line 227 */
/* const/16 v9, 0x604a */
/* aget-byte v9, v10, v9 */
/* and-int/lit8 v9, v9, 0xf */
/* int-to-byte v9, v9 */
/* iput-byte v9, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
/* .line 228 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "For 179 mKeyType:"; // const-string v10, "For 179 mKeyType:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", mNFCType:"; // const-string v10, ", mNFCType:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", mTouchType:"; // const-string v10, ", mTouchType:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v10, v0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v9 );
/* .line 231 */
v2 = this.mBinStartAddressByte;
/* aput-byte v3, v2, v3 */
/* .line 232 */
/* aput-byte v13, v2, v12 */
/* .line 233 */
int v9 = 6; // const/4 v9, 0x6
/* aput-byte v9, v2, v11 */
/* .line 234 */
/* aput-byte v3, v2, v8 */
/* .line 235 */
/* .line 237 */
} // .end local v1 # "binStartFlag":Ljava/lang/String;
} // .end local v4 # "mFileHeadBaseAddr":I
} // .end local v5 # "sum1":I
} // .end local v6 # "binStartFlagByte":[B
} // .end local v7 # "sum2":B
} // :cond_2
final String v1 = "179 Parse OtaFile err"; // const-string v1, "179 Parse OtaFile err"
android.util.Slog .i ( v2,v1 );
/* .line 239 */
} // .end method
private Boolean parseOtaFileFor8012 ( ) {
/* .locals 15 */
/* .line 244 */
/* const/16 v0, 0x2fc0 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* .line 245 */
v0 = this.mFileBuf;
final String v1 = "KeyboardUpgradeUtil"; // const-string v1, "KeyboardUpgradeUtil"
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* array-length v3, v0 */
/* const/16 v4, 0xfc0 */
/* if-le v3, v4, :cond_2 */
/* .line 246 */
/* const/16 v3, 0x2fc0 */
/* .line 248 */
/* .local v3, "mFileHeadBaseAddr":I */
/* const/16 v4, 0x8 */
/* new-array v5, v4, [B */
/* .line 249 */
/* .local v5, "binStartFlagByte":[B */
/* array-length v6, v5 */
java.lang.System .arraycopy ( v0,v3,v5,v2,v6 );
/* .line 251 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v5 );
/* .line 252 */
/* .local v0, "binStartFlag":Ljava/lang/String; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin Start Flag: "; // const-string v7, "Bin Start Flag: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v6 );
/* .line 253 */
/* add-int/2addr v3, v4 */
/* .line 256 */
v6 = this.mFileBuf;
v7 = this.mBinLengthByte;
/* array-length v8, v7 */
java.lang.System .arraycopy ( v6,v3,v7,v2,v8 );
/* .line 257 */
v6 = this.mBinLengthByte;
int v7 = 3; // const/4 v7, 0x3
/* aget-byte v8, v6, v7 */
/* shl-int/lit8 v8, v8, 0x18 */
/* const/high16 v9, -0x1000000 */
/* and-int/2addr v8, v9 */
int v10 = 2; // const/4 v10, 0x2
/* aget-byte v11, v6, v10 */
/* shl-int/lit8 v11, v11, 0x10 */
/* const/high16 v12, 0xff0000 */
/* and-int/2addr v11, v12 */
/* add-int/2addr v8, v11 */
int v11 = 1; // const/4 v11, 0x1
/* aget-byte v13, v6, v11 */
/* shl-int/2addr v13, v4 */
/* const v14, 0xff00 */
/* and-int/2addr v13, v14 */
/* add-int/2addr v8, v13 */
/* aget-byte v6, v6, v2 */
/* and-int/lit16 v6, v6, 0xff */
/* add-int/2addr v8, v6 */
/* iput v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
/* .line 260 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Bin Lenght: "; // const-string v8, "Bin Lenght: "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v6 );
/* .line 261 */
/* add-int/lit8 v3, v3, 0x4 */
/* .line 264 */
v6 = this.mFileBuf;
v8 = this.mBinCheckSumByte;
/* array-length v13, v8 */
java.lang.System .arraycopy ( v6,v3,v8,v2,v13 );
/* .line 266 */
v6 = this.mBinCheckSumByte;
/* aget-byte v8, v6, v7 */
/* shl-int/lit8 v8, v8, 0x18 */
/* and-int/2addr v8, v9 */
/* aget-byte v9, v6, v10 */
/* shl-int/lit8 v9, v9, 0x10 */
/* and-int/2addr v9, v12 */
/* add-int/2addr v8, v9 */
/* aget-byte v9, v6, v11 */
/* shl-int/lit8 v4, v9, 0x8 */
/* and-int/2addr v4, v14 */
/* add-int/2addr v8, v4 */
/* aget-byte v4, v6, v2 */
/* and-int/lit16 v4, v4, 0xff */
/* add-int/2addr v8, v4 */
/* iput v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
/* .line 269 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bin CheckSum: "; // const-string v6, "Bin CheckSum: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 270 */
/* add-int/lit8 v3, v3, 0x4 */
/* .line 273 */
/* new-array v4, v10, [B */
this.mBinVersion = v4;
/* .line 274 */
v6 = this.mFileBuf;
/* array-length v8, v4 */
java.lang.System .arraycopy ( v6,v3,v4,v2,v8 );
/* .line 275 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.mBinVersion;
/* aget-byte v6, v6, v11 */
java.lang.Byte .valueOf ( v6 );
/* filled-new-array {v6}, [Ljava/lang/Object; */
final String v8 = "%02x"; // const-string v8, "%02x"
java.lang.String .format ( v8,v6 );
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mBinVersion;
/* aget-byte v6, v6, v2 */
/* .line 276 */
java.lang.Byte .valueOf ( v6 );
/* filled-new-array {v6}, [Ljava/lang/Object; */
/* .line 275 */
java.lang.String .format ( v8,v6 );
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.mBinVersionStr = v4;
/* .line 277 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Bin Version : "; // const-string v6, "Bin Version : "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mBinVersionStr;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 280 */
v4 = this.mFileBuf;
/* iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/lit8 v8, v6, 0x40 */
/* array-length v9, v4 */
/* add-int/lit8 v6, v6, 0x40 */
/* sub-int/2addr v9, v6 */
v4 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSumInt ( v4,v8,v9 );
/* .line 282 */
/* .local v4, "sum1":I */
/* iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
final String v8 = "/"; // const-string v8, "/"
/* if-eq v4, v6, :cond_0 */
/* .line 283 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin Check Check Sum Error:"; // const-string v7, "Bin Check Check Sum Error:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinCheckSum:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v6 );
/* .line 284 */
/* .line 288 */
} // :cond_0
v6 = this.mFileBuf;
/* iget v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* const/16 v12, 0x3f */
v6 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v6,v9,v12 );
/* .line 289 */
/* .local v6, "sum2":B */
v9 = this.mFileBuf;
/* iget v13, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/2addr v13, v12 */
/* aget-byte v13, v9, v13 */
/* if-eq v6, v13, :cond_1 */
/* .line 290 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Bin Check Head Sum Error:"; // const-string v9, "Bin Check Head Sum Error:"
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mFileBuf;
/* iget v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/2addr v9, v12 */
/* aget-byte v8, v8, v9 */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v7 );
/* .line 293 */
/* .line 296 */
} // :cond_1
/* const/16 v8, 0x5fd8 */
/* aget-byte v8, v9, v8 */
/* iput-byte v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
/* .line 297 */
/* const/16 v8, 0x5fd9 */
/* aget-byte v8, v9, v8 */
/* shr-int/lit8 v8, v8, 0x5 */
/* int-to-byte v8, v8 */
/* iput-byte v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
/* .line 298 */
/* const/16 v8, 0x5fda */
/* aget-byte v8, v9, v8 */
/* and-int/lit8 v8, v8, 0xf */
/* int-to-byte v8, v8 */
/* iput-byte v8, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
/* .line 299 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "For 8012 mKeyType:"; // const-string v9, "For 8012 mKeyType:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mNFCType:"; // const-string v9, ", mNFCType:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", mTouchType:"; // const-string v9, ", mTouchType:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v9, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v8 );
/* .line 302 */
v1 = this.mBinStartAddressByte;
/* aput-byte v2, v1, v2 */
/* .line 303 */
/* const/16 v8, 0x20 */
/* aput-byte v8, v1, v11 */
/* .line 304 */
/* aput-byte v2, v1, v10 */
/* .line 305 */
/* aput-byte v2, v1, v7 */
/* .line 306 */
/* .line 308 */
} // .end local v0 # "binStartFlag":Ljava/lang/String;
} // .end local v3 # "mFileHeadBaseAddr":I
} // .end local v4 # "sum1":I
} // .end local v5 # "binStartFlagByte":[B
} // .end local v6 # "sum2":B
} // :cond_2
final String v0 = "8012 Parse OtaFile err"; // const-string v0, "8012 Parse OtaFile err"
android.util.Slog .i ( v1,v0 );
/* .line 310 */
} // .end method
private readFileToBuf ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .line 85 */
int v0 = 0; // const/4 v0, 0x0
/* .line 86 */
/* .local v0, "fileBuf":[B */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 87 */
/* .local v1, "fileStream":Ljava/io/InputStream; */
try { // :try_start_1
v2 = (( java.io.InputStream ) v1 ).available ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->available()I
/* .line 88 */
/* .local v2, "fileLength":I */
/* new-array v3, v2, [B */
/* move-object v0, v3 */
/* .line 89 */
(( java.io.InputStream ) v1 ).read ( v0 ); // invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 90 */
try { // :try_start_2
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 92 */
} // .end local v1 # "fileStream":Ljava/io/InputStream;
/* .line 86 */
} // .end local v2 # "fileLength":I
/* .restart local v1 # "fileStream":Ljava/io/InputStream; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "fileBuf":[B
} // .end local p0 # "this":Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;
} // .end local p1 # "fileName":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 90 */
} // .end local v1 # "fileStream":Ljava/io/InputStream;
/* .restart local v0 # "fileBuf":[B */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil; */
/* .restart local p1 # "fileName":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 91 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "File is not exit!"; // const-string v3, "File is not exit!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "KeyboardUpgradeUtil"; // const-string v3, "KeyboardUpgradeUtil"
android.util.Slog .i ( v3,v2 );
/* .line 93 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
} // .end method
/* # virtual methods */
public Boolean checkVersion ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 98 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "get verison :"; // const-string v1, "get verison :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " , bin version:"; // const-string v1, " , bin version:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mBinVersionStr;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "KeyboardUpgradeUtil"; // const-string v1, "KeyboardUpgradeUtil"
android.util.Slog .i ( v1,v0 );
/* .line 99 */
v0 = this.mBinVersionStr;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( java.lang.String ) v0 ).startsWith ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
v0 = this.mBinVersionStr;
/* .line 100 */
v0 = (( java.lang.String ) v0 ).compareTo ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 99 */
} // :goto_0
} // .end method
public getBinPackInfo ( Object p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "target" # B */
/* .param p2, "offset" # I */
/* .line 342 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 343 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x34 */
/* .line 344 */
/* .local v1, "binlength":I */
/* const/16 v2, -0x56 */
int v3 = 0; // const/4 v3, 0x0
/* aput-byte v2, v0, v3 */
/* .line 345 */
int v2 = 1; // const/4 v2, 0x1
/* const/16 v4, 0x42 */
/* aput-byte v4, v0, v2 */
/* .line 346 */
/* const/16 v5, 0x32 */
int v6 = 2; // const/4 v6, 0x2
/* aput-byte v5, v0, v6 */
/* .line 347 */
int v5 = 3; // const/4 v5, 0x3
/* aput-byte v3, v0, v5 */
/* .line 348 */
/* const/16 v3, 0x4f */
int v7 = 4; // const/4 v7, 0x4
/* aput-byte v3, v0, v7 */
/* .line 349 */
int v3 = 5; // const/4 v3, 0x5
/* const/16 v8, 0x30 */
/* aput-byte v8, v0, v3 */
/* .line 350 */
int v3 = 6; // const/4 v3, 0x6
/* const/16 v8, -0x80 */
/* aput-byte v8, v0, v3 */
/* .line 351 */
int v3 = 7; // const/4 v3, 0x7
/* aput-byte p1, v0, v3 */
/* .line 352 */
/* const/16 v3, 0x8 */
/* const/16 v8, 0x11 */
/* aput-byte v8, v0, v3 */
/* .line 353 */
/* const/16 v3, 0x9 */
/* const/16 v8, 0x38 */
/* aput-byte v8, v0, v3 */
/* .line 354 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( p2 );
/* .line 355 */
/* .local v3, "offsetB":[B */
/* const/16 v8, 0xa */
/* aget-byte v5, v3, v5 */
/* aput-byte v5, v0, v8 */
/* .line 356 */
/* const/16 v5, 0xb */
/* aget-byte v6, v3, v6 */
/* aput-byte v6, v0, v5 */
/* .line 357 */
/* const/16 v5, 0xc */
/* aget-byte v2, v3, v2 */
/* aput-byte v2, v0, v5 */
/* .line 358 */
v2 = this.mFileBuf;
/* array-length v5, v2 */
/* iget v6, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mStartInfoIndex:I */
/* add-int/lit8 v8, v6, 0x40 */
/* add-int/2addr v8, p2 */
/* add-int/2addr v8, v1 */
/* if-ge v5, v8, :cond_0 */
/* .line 359 */
/* array-length v5, v2 */
/* sub-int/2addr v5, v6 */
/* add-int/lit8 v5, v5, -0x40 */
/* sub-int v1, v5, p2 */
/* .line 361 */
} // :cond_0
/* const/16 v5, 0xd */
/* const/16 v8, 0x34 */
/* aput-byte v8, v0, v5 */
/* .line 362 */
/* add-int/lit8 v6, v6, 0x40 */
/* add-int/2addr v6, p2 */
/* const/16 v5, 0xe */
java.lang.System .arraycopy ( v2,v6,v0,v5,v1 );
/* .line 363 */
/* const/16 v2, 0x3e */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v7,v2 );
/* aput-byte v2, v0, v4 */
/* .line 365 */
} // .end method
public Integer getBinPacketTotal ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "length" # I */
/* .line 409 */
int v0 = 0; // const/4 v0, 0x0
/* .line 410 */
/* .local v0, "totle":I */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 418 */
/* :sswitch_0 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
/* div-int/lit8 v0, v1, 0x34 */
/* .line 419 */
/* rem-int/lit8 v1, v1, 0x34 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 420 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 412 */
/* :sswitch_1 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mBinLength:I */
/* div-int/lit8 v0, v1, 0x14 */
/* .line 413 */
/* rem-int/lit8 v1, v1, 0x14 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 414 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 426 */
} // :cond_0
} // :goto_0
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x20 -> :sswitch_1 */
/* 0x40 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public getUpEndInfo ( Object p0 ) {
/* .locals 7 */
/* .param p1, "target" # B */
/* .line 370 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 371 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 372 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 373 */
/* const/16 v1, 0x32 */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v1, v0, v3 */
/* .line 374 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 375 */
/* const/16 v1, 0x4f */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v1, v0, v4 */
/* .line 376 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v5, 0x30 */
/* aput-byte v5, v0, v1 */
/* .line 377 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v1 */
/* .line 378 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p1, v0, v1 */
/* .line 379 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 380 */
/* const/16 v1, 0x9 */
/* const/16 v5, 0xe */
/* aput-byte v5, v0, v1 */
/* .line 381 */
v1 = this.mBinLengthByte;
/* const/16 v6, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v6,v4 );
/* .line 382 */
v1 = this.mBinStartAddressByte;
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 383 */
v1 = this.mBinCheckSumByte;
/* const/16 v5, 0x12 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 384 */
v1 = this.mBinVersion;
/* const/16 v5, 0x16 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v3 );
/* .line 385 */
/* const/16 v1, 0x15 */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v4,v1 );
/* const/16 v2, 0x18 */
/* aput-byte v1, v0, v2 */
/* .line 386 */
} // .end method
public getUpFlashInfo ( Object p0 ) {
/* .locals 5 */
/* .param p1, "target" # B */
/* .line 391 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 392 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 393 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 394 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 395 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 396 */
/* const/16 v1, 0x4f */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v1, v0, v3 */
/* .line 397 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x30 */
/* aput-byte v4, v0, v1 */
/* .line 398 */
/* const/16 v1, -0x80 */
int v4 = 6; // const/4 v4, 0x6
/* aput-byte v1, v0, v4 */
/* .line 399 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p1, v0, v1 */
/* .line 400 */
/* const/16 v1, 0x8 */
/* aput-byte v4, v0, v1 */
/* .line 401 */
/* const/16 v1, 0x9 */
/* aput-byte v3, v0, v1 */
/* .line 402 */
v1 = this.mBinStartAddressByte;
/* const/16 v4, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v4,v3 );
/* .line 403 */
/* const/16 v1, 0xe */
v2 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v3,v4 );
/* aput-byte v2, v0, v1 */
/* .line 404 */
} // .end method
public getUpgradeInfo ( Object p0 ) {
/* .locals 7 */
/* .param p1, "target" # B */
/* .line 316 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 317 */
/* .local v0, "bytes":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 318 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 319 */
/* const/16 v1, 0x32 */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v1, v0, v3 */
/* .line 320 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 321 */
/* const/16 v1, 0x4e */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v1, v0, v4 */
/* .line 322 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v5, 0x30 */
/* aput-byte v5, v0, v1 */
/* .line 323 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v1 */
/* .line 324 */
int v1 = 7; // const/4 v1, 0x7
/* aput-byte p1, v0, v1 */
/* .line 325 */
/* const/16 v1, 0x8 */
/* aput-byte v3, v0, v1 */
/* .line 326 */
/* const/16 v1, 0x9 */
/* const/16 v5, 0x13 */
/* aput-byte v5, v0, v1 */
/* .line 327 */
v1 = this.mBinLengthByte;
/* const/16 v5, 0xa */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 328 */
v1 = this.mBinStartAddressByte;
/* const/16 v5, 0xe */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 329 */
v1 = this.mBinCheckSumByte;
/* const/16 v5, 0x12 */
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 330 */
v1 = this.mBinVersion;
/* const/16 v6, 0x16 */
java.lang.System .arraycopy ( v1,v2,v0,v6,v3 );
/* .line 331 */
/* const/16 v1, 0x18 */
/* const/16 v2, 0x2b */
/* aput-byte v2, v0, v1 */
/* .line 332 */
/* const/16 v1, 0x19 */
/* aput-byte v5, v0, v1 */
/* .line 333 */
/* const/16 v2, 0x1a */
/* iget-byte v3, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mKeyType:B */
/* aput-byte v3, v0, v2 */
/* .line 334 */
/* const/16 v2, 0x1b */
/* iget-byte v3, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mTouchType:B */
/* aput-byte v3, v0, v2 */
/* .line 335 */
/* const/16 v2, 0x1c */
/* iget-byte v3, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mNFCType:B */
/* aput-byte v3, v0, v2 */
/* .line 336 */
/* const/16 v2, 0x1d */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil .getSum ( v0,v4,v1 );
/* aput-byte v1, v0, v2 */
/* .line 337 */
} // .end method
public Boolean isValidFile ( ) {
/* .locals 1 */
/* .line 79 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->mValid:Z */
} // .end method
