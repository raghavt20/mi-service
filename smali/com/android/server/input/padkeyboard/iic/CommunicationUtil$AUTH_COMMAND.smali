.class public final enum Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;
.super Ljava/lang/Enum;
.source "CommunicationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AUTH_COMMAND"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

.field public static final enum AUTH_START:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

.field public static final enum AUTH_STEP3:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

.field public static final enum AUTH_STEP5_1:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

.field public static final enum AUTH_STEP5_2:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

.field public static final enum AUTH_STEP7:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;


# instance fields
.field private final mCommand:B

.field private final mSize:B


# direct methods
.method private static synthetic $values()[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;
    .locals 5

    .line 138
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_START:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP3:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    sget-object v2, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP5_1:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    sget-object v3, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP5_2:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    sget-object v4, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP7:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    filled-new-array {v0, v1, v2, v3, v4}, [Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 139
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    const/16 v1, 0x31

    const/4 v2, 0x6

    const-string v3, "AUTH_START"

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;-><init>(Ljava/lang/String;IBB)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_START:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    .line 140
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    const/16 v1, 0x32

    const/16 v2, 0x24

    const-string v3, "AUTH_STEP3"

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;-><init>(Ljava/lang/String;IBB)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP3:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    .line 141
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    const/16 v1, 0x33

    const/16 v2, 0x10

    const-string v3, "AUTH_STEP5_1"

    const/4 v4, 0x2

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;-><init>(Ljava/lang/String;IBB)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP5_1:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    .line 142
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    const/4 v1, 0x3

    const/16 v2, 0x34

    const-string v3, "AUTH_STEP5_2"

    invoke-direct {v0, v3, v1, v2, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;-><init>(Ljava/lang/String;IBB)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP5_2:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    .line 143
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    const/4 v1, 0x4

    const/16 v2, 0x35

    const-string v3, "AUTH_STEP7"

    invoke-direct {v0, v3, v1, v2, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;-><init>(Ljava/lang/String;IBB)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP7:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    .line 138
    invoke-static {}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->$values()[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->$VALUES:[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IBB)V
    .locals 0
    .param p3, "command"    # B
    .param p4, "size"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(BB)V"
        }
    .end annotation

    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    iput-byte p3, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->mCommand:B

    .line 150
    iput-byte p4, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->mSize:B

    .line 151
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 138
    const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    return-object v0
.end method

.method public static values()[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;
    .locals 1

    .line 138
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->$VALUES:[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    invoke-virtual {v0}, [Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    return-object v0
.end method


# virtual methods
.method public getCommand()B
    .locals 1

    .line 154
    iget-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->mCommand:B

    return v0
.end method

.method public getSize()B
    .locals 1

    .line 158
    iget-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->mSize:B

    return v0
.end method
