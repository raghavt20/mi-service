.class public final enum Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;
.super Ljava/lang/Enum;
.source "CommunicationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KB_FEATURE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_BACKLIGHT:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_ENABLE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_FIRMWARE_RECOVER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_G_SENSOR:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_POWER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_SLEEP_STATUS:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

.field public static final enum KB_WRITE_CMD_ACK:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;


# instance fields
.field private final mCommand:B

.field private final mIndex:I


# direct methods
.method private static synthetic $values()[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;
    .locals 11

    .line 112
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_ENABLE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_POWER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v2, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v3, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_WRITE_CMD_ACK:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v4, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_FIRMWARE_RECOVER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v5, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_SLEEP_STATUS:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v6, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_G_SENSOR:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v7, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_BACKLIGHT:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v8, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v9, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    sget-object v10, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    filled-new-array/range {v0 .. v10}, [Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 113
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_ENABLE"

    const/4 v2, 0x0

    const/16 v3, 0x22

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_ENABLE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_POWER"

    const/16 v2, 0x25

    const/4 v3, 0x2

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_POWER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_CAPS_KEY"

    const/16 v2, 0x26

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    .line 114
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_WRITE_CMD_ACK"

    const/16 v3, 0x30

    const/4 v5, 0x4

    invoke-direct {v0, v1, v4, v3, v5}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_WRITE_CMD_ACK:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_FIRMWARE_RECOVER"

    const/16 v3, 0x20

    const/4 v4, 0x5

    invoke-direct {v0, v1, v5, v3, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_FIRMWARE_RECOVER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    .line 115
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_SLEEP_STATUS"

    const/16 v3, 0x28

    const/4 v5, 0x6

    invoke-direct {v0, v1, v4, v3, v5}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_SLEEP_STATUS:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_G_SENSOR"

    const/16 v3, 0x27

    const/4 v4, 0x7

    invoke-direct {v0, v1, v5, v3, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_G_SENSOR:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_BACKLIGHT"

    const/16 v3, 0x23

    const/16 v5, 0x8

    invoke-direct {v0, v1, v4, v3, v5}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_BACKLIGHT:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    .line 116
    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_MIC_MUTE"

    const/16 v3, 0x9

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_CAPS_KEY_NEW"

    const/16 v2, 0x2e

    const/16 v4, 0xa

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    new-instance v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    const-string v1, "KB_MIC_MUTE_NEW"

    const/16 v3, 0xb

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;-><init>(Ljava/lang/String;IBI)V

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    .line 112
    invoke-static {}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->$values()[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->$VALUES:[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IBI)V
    .locals 0
    .param p3, "commandId"    # B
    .param p4, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(BI)V"
        }
    .end annotation

    .line 121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 122
    iput-byte p3, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->mCommand:B

    .line 123
    iput p4, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->mIndex:I

    .line 124
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 112
    const-class v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;
    .locals 1

    .line 112
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->$VALUES:[Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, [Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    return-object v0
.end method


# virtual methods
.method public getCommand()B
    .locals 1

    .line 127
    iget-byte v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->mCommand:B

    return v0
.end method

.method public getIndex()I
    .locals 1

    .line 131
    iget v0, p0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->mIndex:I

    return v0
.end method
