class com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler extends android.os.Handler {
	 /* .source "IICNodeHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "UpgradeCommandHandler" */
} // .end annotation
/* # static fields */
public static final java.lang.String DATA_SEND_COMMAND;
public static final java.lang.String DATA_WORKER_OBJECT;
public static final Integer MSG_NOTIFY_READY_DO_COMMAND;
public static final Integer MSG_SEND_COMMAND_QUEUE;
/* # instance fields */
final com.android.server.input.padkeyboard.iic.IICNodeHelper this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1127 */
this.this$0 = p1;
/* .line 1128 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1129 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 9 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1133 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const/16 v1, 0x61 */
/* if-ne v0, v1, :cond_1 */
/* .line 1134 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 1135 */
/* .local v0, "repeatTimes":I */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* const-string/jumbo v3, "worker" */
(( android.os.Bundle ) v2 ).getParcelable ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v2, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
/* .line 1136 */
/* .local v2, "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 1137 */
	 (( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
	 final String v5 = "command"; // const-string v5, "command"
	 (( android.os.Bundle ) v4 ).getByteArray ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B
	 /* .line 1138 */
	 /* .local v4, "command":[B */
	 v6 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
	 (( com.android.server.input.padkeyboard.KeyboardInteraction ) v6 ).communicateWithIIC ( v4 ); // invoke-virtual {v6, v4}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->communicateWithIIC([B)V
	 /* .line 1139 */
	 /* new-instance v6, Landroid/os/Bundle; */
	 /* invoke-direct {v6}, Landroid/os/Bundle;-><init>()V */
	 /* .line 1140 */
	 /* .local v6, "data":Landroid/os/Bundle; */
	 (( android.os.Bundle ) v6 ).putByteArray ( v5, v4 ); // invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
	 /* .line 1141 */
	 (( android.os.Bundle ) v6 ).putParcelable ( v3, v2 ); // invoke-virtual {v6, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
	 /* .line 1142 */
	 v3 = this.this$0;
	 com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v3 );
	 android.os.Message .obtain ( v3,v1 );
	 /* .line 1144 */
	 /* .local v1, "msg2":Landroid/os/Message; */
	 /* add-int/lit8 v0, v0, -0x1 */
	 /* iput v0, v1, Landroid/os/Message;->arg1:I */
	 /* .line 1145 */
	 (( android.os.Message ) v1 ).setData ( v6 ); // invoke-virtual {v1, v6}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
	 /* .line 1146 */
	 v3 = this.this$0;
	 com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmUpgradeCommandHandler ( v3 );
	 /* const-wide/16 v7, 0x5dc */
	 (( com.android.server.input.padkeyboard.iic.IICNodeHelper$UpgradeCommandHandler ) v3 ).sendMessageDelayed ( v1, v7, v8 ); // invoke-virtual {v3, v1, v7, v8}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$UpgradeCommandHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
	 /* .line 1147 */
} // .end local v1 # "msg2":Landroid/os/Message;
} // .end local v4 # "command":[B
} // .end local v6 # "data":Landroid/os/Bundle;
/* .line 1148 */
} // :cond_0
(( com.android.server.input.padkeyboard.iic.IICNodeHelper$CommandWorker ) v2 ).notResponseException ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->notResponseException()V
/* .line 1150 */
} // .end local v0 # "repeatTimes":I
} // .end local v2 # "worker":Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;
} // :cond_1
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const/16 v1, 0x60 */
/* if-ne v0, v1, :cond_4 */
/* .line 1151 */
v0 = this.this$0;
v0 = (( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).hasAnyWorkerRunning ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->hasAnyWorkerRunning()Z
final String v1 = "MiuiPadKeyboard_IICNodeHelper"; // const-string v1, "MiuiPadKeyboard_IICNodeHelper"
/* if-nez v0, :cond_3 */
/* .line 1152 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReadyRunningTask ( v0 );
/* check-cast v0, Ljava/lang/Runnable; */
/* .line 1153 */
/* .local v0, "worker":Ljava/lang/Runnable; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1154 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "The WorkQueue has:"; // const-string v3, "The WorkQueue has:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.android.server.input.padkeyboard.iic.IICNodeHelper .-$$Nest$fgetmReadyRunningTask ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 1155 */
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
/* .line 1156 */
/* .line 1158 */
} // :cond_2
v1 = this.this$0;
v1 = this.mNanoSocketCallback;
/* .line 1160 */
} // .end local v0 # "worker":Ljava/lang/Runnable;
} // :goto_0
/* .line 1161 */
} // :cond_3
final String v0 = "Still has running commandWorker"; // const-string v0, "Still has running commandWorker"
android.util.Slog .i ( v1,v0 );
/* .line 1150 */
} // :cond_4
} // :goto_1
/* nop */
/* .line 1164 */
} // :goto_2
return;
} // .end method
