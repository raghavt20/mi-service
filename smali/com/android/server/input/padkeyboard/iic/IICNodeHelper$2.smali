.class Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;
.super Ljava/lang/Object;
.source "IICNodeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 339
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 343
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    new-instance v1, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 344
    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$mgetUpgradeHead(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyboardType(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)B

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    invoke-static {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fputmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;)V

    .line 345
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->isValidFile()Z

    move-result v0

    const/16 v1, 0x38

    if-nez v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    iget-object v0, v0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->mNanoSocketCallback:Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid upgrade file, with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeFilePath(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 348
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 346
    invoke-interface {v0, v1, v2}, Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;->onOtaErrorInfo(BLjava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V

    .line 350
    return-void

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$msupportUpgradeKeyboard(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->emptyCommandResponse()V

    .line 355
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    move-result-object v2

    .line 359
    invoke-virtual {v2, v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getUpgradeInfo(B)[B

    move-result-object v2

    .line 358
    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 361
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyBoardUpgradeUtil(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;

    move-result-object v2

    .line 362
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/android/server/input/padkeyboard/iic/KeyboardUpgradeUtil;->getBinPackInfo(BI)[B

    move-result-object v1

    .line 361
    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->insertCommandToQueue([B)V

    .line 364
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$2;->this$0:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->-$$Nest$fgetmKeyUpgradeCommandWorker(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper$CommandWorker;->sendCommand(I)V

    .line 365
    return-void
.end method
