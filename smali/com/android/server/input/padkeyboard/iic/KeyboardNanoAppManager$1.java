class com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager$1 extends vendor.xiaomi.hardware.keyboardnanoapp.V1_0.INanoappCallback$Stub {
	 /* .source "KeyboardNanoAppManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;->initCallback()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager; */
/* .line 50 */
this.this$0 = p1;
/* invoke-direct {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void dataReceive ( java.util.ArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 52 */
/* .local p1, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager .-$$Nest$fgetmCommunicationUtil ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 53 */
v0 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* new-array v0, v0, [B */
/* .line 54 */
/* .local v0, "responseData":[B */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 55 */
(( java.util.ArrayList ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Byte; */
v2 = (( java.lang.Byte ) v2 ).byteValue ( ); // invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v2, v0, v1 */
/* .line 54 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 57 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.this$0;
com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager .-$$Nest$fgetmCommunicationUtil ( v1 );
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).receiverNanoAppData ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->receiverNanoAppData([B)V
/* .line 59 */
} // .end local v0 # "responseData":[B
} // :cond_1
return;
} // .end method
public void errorReceive ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "errorCode" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 62 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "errorCode: "; // const-string v1, "errorCode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "KeyboardNanoApp"; // const-string v1, "KeyboardNanoApp"
android.util.Slog .i ( v1,v0 );
/* .line 63 */
return;
} // .end method
