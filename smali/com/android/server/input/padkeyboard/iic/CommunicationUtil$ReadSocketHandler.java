class com.android.server.input.padkeyboard.iic.CommunicationUtil$ReadSocketHandler extends android.os.Handler {
	 /* .source "CommunicationUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ReadSocketHandler" */
} // .end annotation
/* # static fields */
public static final Integer MSG_READ;
/* # instance fields */
final com.android.server.input.padkeyboard.iic.CommunicationUtil this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.iic.CommunicationUtil$ReadSocketHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 543 */
this.this$0 = p1;
/* .line 544 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 545 */
return;
} // .end method
private void dealReadSocketPackage ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "data" # [B */
/* .line 580 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmNanoSocketCallback ( v0 );
final String v1 = "IIC_CommunicationUtil"; // const-string v1, "IIC_CommunicationUtil"
if ( v0 != null) { // if-eqz v0, :cond_8
	 v0 = this.this$0;
	 com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmSocketCallBack ( v0 );
	 /* if-nez v0, :cond_0 */
	 /* goto/16 :goto_1 */
	 /* .line 585 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* aget-byte v2, p1, v0 */
/* const/16 v3, -0x80 */
/* if-ne v2, v3, :cond_1 */
/* .line 586 */
v2 = this.this$0;
com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmNanoSocketCallback ( v2 );
final String v4 = "Write data Socket exception"; // const-string v4, "Write data Socket exception"
/* .line 588 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception socket command is:"; // const-string v4, "Exception socket command is:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, p1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 592 */
} // :cond_1
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x24 */
/* if-eq v1, v2, :cond_2 */
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x23 */
/* if-eq v1, v2, :cond_2 */
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x26 */
/* if-eq v1, v2, :cond_2 */
/* aget-byte v0, p1, v0 */
/* const/16 v1, 0x22 */
/* if-ne v0, v1, :cond_7 */
} // :cond_2
int v0 = 3; // const/4 v0, 0x3
/* aget-byte v0, p1, v0 */
/* if-ne v0, v3, :cond_7 */
/* .line 596 */
int v0 = 1; // const/4 v0, 0x1
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x30 */
/* const/16 v4, 0x38 */
int v5 = 2; // const/4 v5, 0x2
/* if-ne v1, v2, :cond_5 */
/* .line 598 */
/* aget-byte v0, p1, v5 */
/* const/16 v1, 0x18 */
/* if-ne v0, v1, :cond_3 */
/* .line 600 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmSocketCallBack ( v0 );
/* .line 601 */
} // :cond_3
/* aget-byte v0, p1, v5 */
/* if-eq v0, v4, :cond_4 */
/* aget-byte v0, p1, v5 */
/* const/16 v1, 0x39 */
/* if-ne v0, v1, :cond_7 */
/* .line 602 */
} // :cond_4
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmSocketCallBack ( v0 );
/* .line 604 */
} // :cond_5
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x31 */
/* if-ne v1, v2, :cond_6 */
/* aget-byte v1, p1, v5 */
/* if-ne v1, v4, :cond_6 */
/* .line 606 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmSocketCallBack ( v0 );
/* .line 607 */
} // :cond_6
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x20 */
/* if-ne v1, v2, :cond_7 */
/* aget-byte v1, p1, v5 */
/* if-ne v1, v3, :cond_7 */
/* .line 608 */
int v1 = 4; // const/4 v1, 0x4
/* aget-byte v1, p1, v1 */
/* const/16 v2, -0x1f */
/* if-ne v1, v2, :cond_7 */
int v1 = 5; // const/4 v1, 0x5
/* aget-byte v1, p1, v1 */
/* if-ne v1, v0, :cond_7 */
/* .line 609 */
v0 = this.this$0;
com.android.server.input.padkeyboard.iic.CommunicationUtil .-$$Nest$fgetmNanoSocketCallback ( v0 );
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v1, p1, v1 */
/* .line 613 */
} // :cond_7
} // :goto_0
return;
/* .line 581 */
} // :cond_8
} // :goto_1
final String v0 = "Miui Keyboard Manager is not ready,Abandon this socket package."; // const-string v0, "Miui Keyboard Manager is not ready,Abandon this socket package."
android.util.Slog .e ( v1,v0 );
/* .line 582 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 549 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_5 */
/* .line 550 */
v0 = this.obj;
/* check-cast v0, [B */
/* .line 551 */
/* .local v0, "data":[B */
int v2 = 0; // const/4 v2, 0x0
/* .line 552 */
/* .local v2, "position":I */
/* if-nez v0, :cond_0 */
/* .line 553 */
return;
/* .line 555 */
} // :cond_0
} // :goto_0
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_5 */
/* .line 556 */
/* aget-byte v3, v0, v2 */
/* const/16 v4, -0x56 */
final String v5 = "IIC_CommunicationUtil"; // const-string v5, "IIC_CommunicationUtil"
int v6 = 0; // const/4 v6, 0x0
/* if-ne v3, v4, :cond_2 */
/* .line 557 */
/* add-int/lit8 v3, v2, 0x1 */
/* aget-byte v3, v0, v3 */
/* new-array v3, v3, [B */
/* .line 558 */
/* .local v3, "temp":[B */
/* add-int/lit8 v4, v2, 0x2 */
/* array-length v7, v3 */
/* add-int/2addr v4, v7 */
/* array-length v7, v0 */
/* if-gt v4, v7, :cond_1 */
/* .line 559 */
/* add-int/lit8 v4, v2, 0x2 */
/* array-length v5, v3 */
java.lang.System .arraycopy ( v0,v4,v3,v6,v5 );
/* .line 560 */
/* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;->dealReadSocketPackage([B)V */
/* .line 561 */
/* array-length v4, v3 */
/* add-int/lit8 v4, v4, 0x2 */
/* add-int/2addr v2, v4 */
/* .line 566 */
} // .end local v3 # "temp":[B
/* .line 563 */
/* .restart local v3 # "temp":[B */
} // :cond_1
final String v1 = "Drop not complete Data!"; // const-string v1, "Drop not complete Data!"
android.util.Slog .e ( v5,v1 );
/* .line 564 */
/* .line 566 */
} // .end local v3 # "temp":[B
} // :cond_2
/* aget-byte v3, v0, v6 */
/* const/16 v4, 0x23 */
/* if-ne v3, v4, :cond_4 */
/* aget-byte v3, v0, v1 */
/* const/16 v4, 0x31 */
/* if-eq v3, v4, :cond_3 */
/* aget-byte v1, v0, v1 */
/* const/16 v3, 0x30 */
/* if-ne v1, v3, :cond_4 */
/* .line 569 */
} // :cond_3
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$ReadSocketHandler;->dealReadSocketPackage([B)V */
/* .line 570 */
/* .line 572 */
} // :cond_4
final String v1 = "Receiver Data is too old!"; // const-string v1, "Receiver Data is too old!"
android.util.Slog .e ( v5,v1 );
/* .line 573 */
/* nop */
/* .line 577 */
} // .end local v0 # "data":[B
} // .end local v2 # "position":I
} // :cond_5
} // :goto_1
return;
} // .end method
