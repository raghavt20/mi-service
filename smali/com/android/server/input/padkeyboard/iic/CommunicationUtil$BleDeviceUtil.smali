.class public Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;
.super Ljava/lang/Object;
.source "CommunicationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BleDeviceUtil"
.end annotation


# static fields
.field public static final CMD_KB_STATUS:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 627
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;->CMD_KB_STATUS:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x30t
        0x1t
        0x1t
        0x0t
        0x32t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getKeyboardFeatureCommand(BB)[B
    .locals 4
    .param p0, "command"    # B
    .param p1, "data"    # B

    .line 631
    const/4 v0, 0x5

    new-array v0, v0, [B

    .line 632
    .local v0, "result":[B
    const/16 v1, 0x31

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 633
    const/4 v1, 0x1

    aput-byte p0, v0, v1

    .line 634
    const/4 v3, 0x2

    aput-byte v1, v0, v3

    .line 635
    const/4 v1, 0x3

    aput-byte p1, v0, v1

    .line 636
    const/4 v1, 0x4

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 637
    return-object v0
.end method
