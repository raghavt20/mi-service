public class com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager {
	 /* .source "KeyboardNanoAppManager.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager mIKeyboardNanoapp;
	 /* # instance fields */
	 private com.android.server.input.padkeyboard.iic.CommunicationUtil mCommunicationUtil;
	 private vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp mKeyboardNanoapp;
	 private vendor.xiaomi.hardware.keyboardnanoapp.V1_0.INanoappCallback mNanoappCallback;
	 /* # direct methods */
	 static com.android.server.input.padkeyboard.iic.CommunicationUtil -$$Nest$fgetmCommunicationUtil ( com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mCommunicationUtil;
	 } // .end method
	 private com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager ( ) {
		 /* .locals 3 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 22 */
		 try { // :try_start_0
			 vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp .getService ( );
			 this.mKeyboardNanoapp = v0;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 25 */
			 /* .line 23 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 24 */
			 /* .local v0, "e":Ljava/lang/Exception; */
			 final String v1 = "KeyboardNanoApp"; // const-string v1, "KeyboardNanoApp"
			 final String v2 = "exception when get service"; // const-string v2, "exception when get service"
			 android.util.Slog .i ( v1,v2 );
			 /* .line 26 */
		 } // .end local v0 # "e":Ljava/lang/Exception;
	 } // :goto_0
	 return;
} // .end method
public static com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager getInstance ( ) {
	 /* .locals 2 */
	 /* .line 29 */
	 v0 = com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager.mIKeyboardNanoapp;
	 /* if-nez v0, :cond_1 */
	 /* .line 30 */
	 /* const-class v0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager; */
	 /* monitor-enter v0 */
	 /* .line 31 */
	 try { // :try_start_0
		 v1 = com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager.mIKeyboardNanoapp;
		 /* if-nez v1, :cond_0 */
		 /* .line 32 */
		 /* new-instance v1, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager; */
		 /* invoke-direct {v1}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;-><init>()V */
		 /* .line 34 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 36 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.padkeyboard.iic.KeyboardNanoAppManager.mIKeyboardNanoapp;
} // .end method
/* # virtual methods */
public void initCallback ( ) {
/* .locals 3 */
/* .line 50 */
try { // :try_start_0
/* new-instance v0, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager$1;-><init>(Lcom/android/server/input/padkeyboard/iic/KeyboardNanoAppManager;)V */
this.mNanoappCallback = v0;
/* .line 65 */
v1 = this.mKeyboardNanoapp;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 68 */
/* .line 66 */
/* :catch_0 */
/* move-exception v0 */
/* .line 67 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set callback error:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "KeyboardNanoApp"; // const-string v2, "KeyboardNanoApp"
android.util.Slog .e ( v2,v1 );
/* .line 69 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean sendCommandToNano ( java.util.ArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 41 */
/* .local p1, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
try { // :try_start_0
v0 = this.mKeyboardNanoapp;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 44 */
/* .line 42 */
/* :catch_0 */
/* move-exception v0 */
/* .line 43 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set callback error:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "KeyboardNanoApp"; // const-string v2, "KeyboardNanoApp"
android.util.Slog .e ( v2,v1 );
/* .line 45 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setCommunicationUtil ( com.android.server.input.padkeyboard.iic.CommunicationUtil p0 ) {
/* .locals 0 */
/* .param p1, "communicationUtil" # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
/* .line 72 */
this.mCommunicationUtil = p1;
/* .line 73 */
return;
} // .end method
