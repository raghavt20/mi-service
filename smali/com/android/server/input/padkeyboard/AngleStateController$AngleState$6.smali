.class final enum Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;
.super Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
.source "AngleStateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4010
    name = null
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;IFF)V
    .locals 6
    .param p3, "lower"    # F
    .param p4, "upper"    # F

    .line 137
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$6-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;-><init>(Ljava/lang/String;IFF)V

    return-void
.end method


# virtual methods
.method public getCurrentKeyboardStatus()Z
    .locals 1

    .line 154
    const/4 v0, 0x1

    return v0
.end method

.method public isCurrentState(F)Z
    .locals 1
    .param p1, "angle"    # F

    .line 140
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->lower:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->upper:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
    .locals 2
    .param p1, "angle"    # F

    .line 145
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->lower:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    const/high16 v0, 0x43b40000    # 360.0f

    iget v1, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->lower:F

    sub-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 146
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->upper:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->onChange(Z)V

    .line 147
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;->NO_WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    move-result-object v0

    return-object v0

    .line 149
    :cond_1
    return-object p0
.end method
