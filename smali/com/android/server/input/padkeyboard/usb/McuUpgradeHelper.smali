.class public Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;
.super Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;
.source "McuUpgradeHelper.java"


# static fields
.field protected static final BIN_HEAD_LENGTH:I = 0x3f

.field protected static final HEAD_BASE_ADDRESS:I = 0x7fc0

.field protected static final KB_MCU_BIN_PATH:Ljava/lang/String; = "/vendor/etc/XM_KB_MCU.bin"

.field protected static final TARGET:I = 0x18

.field public static final VERSION_HEAD:Ljava/lang/String; = "XM2021"


# instance fields
.field protected mBinChipId:Ljava/lang/String;

.field protected mBinPid:I

.field protected final mBinRoDataByte:[B

.field protected mBinStartFlag:Ljava/lang/String;

.field protected mBinVid:I

.field protected final mCheckSumByte:[B

.field protected final mDeviceTypeByte:[B

.field protected final mLengthByte:[B

.field protected final mPidByte:[B

.field protected final mRameCodeByte:[B

.field protected final mRameCodeLenByte:[B

.field protected mRunStartAddress:I

.field protected final mRunStartByte:[B

.field protected final mVersionByte:[B

.field protected final mVidByte:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 44
    const-string v0, "/vendor/etc/XM_KB_MCU.bin"

    invoke-direct {p0, p1, v0}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x4

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinRoDataByte:[B

    .line 30
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRameCodeByte:[B

    .line 31
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRameCodeLenByte:[B

    .line 32
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRunStartByte:[B

    .line 34
    const/4 v1, 0x2

    new-array v2, v1, [B

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mPidByte:[B

    .line 36
    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVidByte:[B

    .line 38
    const/16 v1, 0x10

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVersionByte:[B

    .line 39
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    .line 40
    const/4 v1, 0x1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mDeviceTypeByte:[B

    .line 41
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSumByte:[B

    .line 45
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpgradeFileHead()V

    .line 46
    return-void
.end method

.method private parseUpEndCommand()Z
    .locals 5

    .line 373
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "MiuiPadKeyboardManager"

    if-ne v0, v2, :cond_0

    .line 374
    const-string v0, "receive up end command state need wait"

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return v3

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v0, v0, v1

    if-eqz v0, :cond_1

    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive up end command state error:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    return v3

    .line 381
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUpFlashCommand()Z

    move-result v0

    return v0
.end method

.method private parseUpFlashCommand()Z
    .locals 5

    .line 408
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive up flash command state error:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v4, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    return v3

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v4, 0x7

    aget-byte v1, v1, v4

    invoke-static {v0, v3, v4, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive up flash command checkSum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    return v3

    .line 416
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendResetCommand()Z

    move-result v0

    return v0
.end method

.method private parseUpgradeCommand()Z
    .locals 5

    .line 305
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive upgrade command state error:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v4, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    return v3

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v4, 0x7

    aget-byte v0, v0, v4

    if-eqz v0, :cond_1

    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive upgrade command mode error:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v4, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    return v3

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/16 v4, 0x8

    aget-byte v1, v1, v4

    invoke-static {v0, v3, v4, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_2

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive upgrade command checkSum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    return v3

    .line 317
    :cond_2
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendBinData()Z

    move-result v0

    return v0
.end method

.method private sendBinData()Z
    .locals 5

    .line 321
    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getBinPacketNum(I)I

    move-result v0

    .line 322
    .local v0, "packetNum":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send bin data, packet num: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const/4 v1, 0x0

    .local v1, "num":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 324
    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendOnePacket(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 325
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "send bin data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    const/4 v2, 0x0

    return v2

    .line 323
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 329
    .end local v1    # "num":I
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUpEndCommand()Z

    move-result v1

    return v1
.end method

.method private sendOnePacket(I)Z
    .locals 5
    .param p1, "num"    # I

    .line 333
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 334
    mul-int/lit8 v0, p1, 0x34

    const/16 v2, 0x40

    invoke-virtual {p0, v0, v2}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getBinPackInfo(II)[B

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    invoke-static {v0, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 335
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 336
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 337
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 339
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    const/16 v3, -0x6f

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    if-nez v2, :cond_0

    .line 340
    const/4 v1, 0x1

    return v1

    .line 344
    :cond_1
    const/16 v2, 0xc8

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 335
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    .end local v0    # "i":I
    :cond_2
    return v1
.end method

.method private sendResetCommand()Z
    .locals 9

    .line 420
    const-string/jumbo v0, "send reset command"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 422
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getResetCommand()[B

    move-result-object v0

    .line 423
    .local v0, "command":[B
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    array-length v4, v0

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 424
    const/4 v3, 0x0

    .line 425
    .local v3, "resetSent":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v5, 0x3

    if-ge v4, v5, :cond_5

    .line 426
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    .line 427
    const/4 v3, 0x1

    .line 428
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-static {v5, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 429
    :goto_1
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v5, v7, v8}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 430
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v7, 0x6

    aget-byte v5, v5, v7

    if-eqz v5, :cond_0

    .line 431
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "receive reset command state error:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v7, v8, v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    goto :goto_1

    .line 434
    :cond_0
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    const/4 v8, 0x7

    aget-byte v7, v7, v8

    invoke-static {v5, v2, v8, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v5

    if-nez v5, :cond_1

    .line 435
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "receive reset command checkSum error:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    aget-byte v7, v7, v8

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    goto :goto_1

    .line 438
    :cond_1
    const-string v2, "receive reset command"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    return v6

    .line 442
    :cond_2
    const-string/jumbo v5, "send reset command failed"

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_3
    const/16 v5, 0x3e8

    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 445
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v5, :cond_4

    .line 446
    return v6

    .line 425
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 450
    .end local v4    # "i":I
    :cond_5
    return v3
.end method

.method private sendUpEndCommand()Z
    .locals 6

    .line 350
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 351
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getUpEndCommand()[B

    move-result-object v0

    .line 352
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 353
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x3

    if-ge v2, v3, :cond_4

    .line 354
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 355
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 356
    :cond_0
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 357
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpEndCommand()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 358
    const/4 v1, 0x1

    return v1

    .line 362
    :cond_1
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send up end command failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_2
    const/16 v3, 0x1f4

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 365
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v3, :cond_3

    .line 366
    return v1

    .line 353
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 369
    .end local v2    # "i":I
    :cond_4
    return v1
.end method

.method private sendUpFlashCommand()Z
    .locals 6

    .line 385
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 386
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getUpFlashCommand()[B

    move-result-object v0

    .line 387
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 388
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x6

    if-ge v2, v3, :cond_4

    .line 389
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 390
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 391
    :cond_0
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 392
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpFlashCommand()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 393
    const/4 v1, 0x1

    return v1

    .line 397
    :cond_1
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send up flash command failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_2
    const/16 v3, 0x3e8

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 400
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v3, :cond_3

    .line 401
    return v1

    .line 388
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 404
    .end local v2    # "i":I
    :cond_4
    return v1
.end method

.method private sendUpgradeCommand()Z
    .locals 7

    .line 281
    const-string/jumbo v0, "send upgrade command"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 283
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getUpgradeCommand()[B

    move-result-object v0

    .line 284
    .local v0, "command":[B
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    array-length v4, v0

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 285
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v4, 0x3

    if-ge v3, v4, :cond_4

    .line 286
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 287
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-static {v4, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 288
    :cond_0
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 289
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpgradeCommand()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 290
    const/4 v1, 0x1

    return v1

    .line 294
    :cond_1
    const-string/jumbo v4, "send upgrade command failed"

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_2
    const/16 v4, 0x1f4

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 297
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v4, :cond_3

    .line 298
    return v2

    .line 285
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 301
    .end local v3    # "i":I
    :cond_4
    return v2
.end method


# virtual methods
.method public getBinPackInfo(II)[B
    .locals 21
    .param p1, "offset"    # I
    .param p2, "packetLength"    # I

    .line 144
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    const/16 v3, 0x20

    new-array v4, v3, [B

    .line 145
    .local v4, "bytes":[B
    const/4 v5, 0x0

    .line 146
    .local v5, "binlength":I
    const/16 v7, 0x8

    const/4 v8, 0x7

    const/4 v9, 0x6

    const/4 v10, 0x5

    const/16 v11, 0x11

    const/4 v12, 0x4

    const/16 v13, 0x18

    const/16 v14, -0x80

    const/16 v15, 0x30

    const/16 v16, 0x4f

    const/16 v17, 0x9

    const/16 v18, 0x2

    const/16 v19, 0x1

    const/4 v6, 0x0

    const/16 v20, 0x3

    if-ne v2, v3, :cond_1

    .line 147
    new-array v4, v3, [B

    .line 148
    const/16 v3, 0x14

    .line 149
    .end local v5    # "binlength":I
    .local v3, "binlength":I
    aput-byte v16, v4, v6

    .line 150
    aput-byte v15, v4, v19

    .line 151
    aput-byte v14, v4, v18

    .line 152
    aput-byte v13, v4, v20

    .line 153
    aput-byte v11, v4, v12

    .line 154
    const/16 v5, 0x1d

    aput-byte v5, v4, v10

    .line 155
    invoke-static/range {p1 .. p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v5

    .line 156
    .local v5, "offsetB":[B
    aget-byte v10, v5, v20

    aput-byte v10, v4, v9

    .line 157
    aget-byte v9, v5, v18

    aput-byte v9, v4, v8

    .line 158
    aget-byte v8, v5, v19

    aput-byte v8, v4, v7

    .line 160
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    add-int/lit16 v8, v1, 0x7fc0

    add-int/2addr v8, v3

    if-ge v7, v8, :cond_0

    .line 161
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    add-int/lit16 v7, v7, -0x7fc0

    sub-int/2addr v7, v1

    .line 162
    .end local v3    # "binlength":I
    .local v7, "binlength":I
    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v3

    aget-byte v3, v3, v20

    aput-byte v3, v4, v17

    move v3, v7

    goto :goto_0

    .line 164
    .end local v7    # "binlength":I
    .restart local v3    # "binlength":I
    :cond_0
    const/16 v3, 0x14

    .line 165
    const/16 v7, 0x14

    aput-byte v7, v4, v17

    .line 167
    :goto_0
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    add-int/lit16 v8, v1, 0x7fc0

    const/16 v9, 0xa

    invoke-static {v7, v8, v4, v9, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 169
    const/16 v7, 0x1f

    invoke-static {v4, v6, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v6

    aput-byte v6, v4, v7

    .line 170
    .end local v5    # "offsetB":[B
    move v5, v3

    goto :goto_2

    .end local v3    # "binlength":I
    .local v5, "binlength":I
    :cond_1
    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 171
    new-array v4, v3, [B

    .line 172
    const/16 v3, 0x34

    .line 173
    .end local v5    # "binlength":I
    .restart local v3    # "binlength":I
    aput-byte v16, v4, v6

    .line 174
    aput-byte v15, v4, v19

    .line 175
    aput-byte v14, v4, v18

    .line 176
    aput-byte v13, v4, v20

    .line 177
    aput-byte v11, v4, v12

    .line 178
    const/16 v5, 0x38

    aput-byte v5, v4, v10

    .line 179
    invoke-static/range {p1 .. p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v5

    .line 180
    .local v5, "offsetB":[B
    aget-byte v10, v5, v20

    aput-byte v10, v4, v9

    .line 181
    aget-byte v9, v5, v18

    aput-byte v9, v4, v8

    .line 182
    aget-byte v8, v5, v19

    aput-byte v8, v4, v7

    .line 183
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    add-int/lit16 v8, v1, 0x7fc0

    add-int/2addr v8, v3

    const/16 v9, 0x34

    if-ge v7, v8, :cond_2

    .line 184
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    add-int/lit16 v7, v7, -0x7fc0

    sub-int/2addr v7, v1

    .line 185
    .end local v3    # "binlength":I
    .restart local v7    # "binlength":I
    aput-byte v9, v4, v17

    move v3, v7

    goto :goto_1

    .line 187
    .end local v7    # "binlength":I
    .restart local v3    # "binlength":I
    :cond_2
    const/16 v3, 0x34

    .line 188
    aput-byte v9, v4, v17

    .line 190
    :goto_1
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    add-int/lit16 v8, v1, 0x7fc0

    const/16 v9, 0xa

    invoke-static {v7, v8, v4, v9, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 191
    const/16 v7, 0x3e

    invoke-static {v4, v6, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v6

    aput-byte v6, v4, v7

    move v5, v3

    .line 194
    .end local v3    # "binlength":I
    .local v5, "binlength":I
    :cond_3
    :goto_2
    return-object v4
.end method

.method public getBinPacketNum(I)I
    .locals 5
    .param p1, "length"    # I

    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "totle":I
    const/16 v1, 0x20

    const/16 v2, 0x40

    if-ne p1, v1, :cond_0

    .line 251
    iget v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinLength:I

    add-int/2addr v1, v2

    int-to-double v1, v1

    const-wide/high16 v3, 0x4034000000000000L    # 20.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    goto :goto_0

    .line 252
    :cond_0
    if-ne p1, v2, :cond_1

    .line 253
    iget v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinLength:I

    add-int/2addr v1, v2

    int-to-double v1, v1

    const-wide/high16 v3, 0x404a000000000000L    # 52.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 255
    :cond_1
    :goto_0
    return v0
.end method

.method public getResetCommand()[B
    .locals 6

    .line 218
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 219
    .local v0, "bytes":[B
    const/4 v1, 0x0

    const/16 v2, 0x4e

    aput-byte v2, v0, v1

    .line 220
    const/4 v3, 0x1

    const/16 v4, 0x30

    aput-byte v4, v0, v3

    .line 221
    const/4 v3, 0x2

    const/16 v5, -0x80

    aput-byte v5, v0, v3

    .line 222
    const/16 v3, 0x18

    const/4 v5, 0x3

    aput-byte v3, v0, v5

    .line 223
    const/4 v3, 0x4

    aput-byte v5, v0, v3

    .line 224
    const/4 v5, 0x5

    aput-byte v3, v0, v5

    .line 225
    const/4 v3, 0x6

    const/16 v5, 0x57

    aput-byte v5, v0, v3

    .line 226
    const/4 v3, 0x7

    aput-byte v2, v0, v3

    .line 227
    const/16 v2, 0x8

    const/16 v3, 0x38

    aput-byte v3, v0, v2

    .line 228
    const/16 v2, 0x9

    aput-byte v4, v0, v2

    .line 229
    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v1

    aput-byte v1, v0, v2

    .line 230
    return-object v0
.end method

.method public getUpEndCommand()[B
    .locals 8

    .line 199
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 200
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 201
    const/4 v1, 0x1

    const/16 v3, 0x30

    aput-byte v3, v0, v1

    .line 202
    const/4 v1, 0x2

    const/16 v3, -0x80

    aput-byte v3, v0, v1

    .line 203
    const/4 v4, 0x3

    const/16 v5, 0x18

    aput-byte v5, v0, v4

    .line 204
    const/4 v4, 0x4

    aput-byte v4, v0, v4

    .line 205
    const/4 v6, 0x5

    const/16 v7, 0x15

    aput-byte v7, v0, v6

    .line 206
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    const/4 v7, 0x6

    invoke-static {v6, v2, v0, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSumByte:[B

    const/16 v7, 0xe

    invoke-static {v6, v2, v0, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 208
    const/16 v4, 0x13

    aput-byte v3, v0, v4

    .line 209
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVidByte:[B

    const/16 v4, 0x16

    invoke-static {v3, v2, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mPidByte:[B

    invoke-static {v3, v2, v0, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 211
    const/16 v1, 0x1a

    aput-byte v5, v0, v1

    .line 212
    const/16 v1, 0x1b

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 213
    return-object v0
.end method

.method public getUpFlashCommand()[B
    .locals 5

    .line 235
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 236
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 237
    const/4 v1, 0x1

    const/16 v3, 0x30

    aput-byte v3, v0, v1

    .line 238
    const/4 v1, 0x2

    const/16 v3, -0x80

    aput-byte v3, v0, v1

    .line 239
    const/4 v1, 0x3

    const/16 v3, 0x18

    aput-byte v3, v0, v1

    .line 240
    const/4 v1, 0x4

    const/4 v3, 0x6

    aput-byte v3, v0, v1

    .line 241
    const/4 v4, 0x5

    aput-byte v1, v0, v4

    .line 242
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    invoke-static {v4, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 243
    const/16 v1, 0xa

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 244
    return-object v0
.end method

.method public getUpgradeCommand()[B
    .locals 9

    .line 117
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 118
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 119
    const/16 v1, 0x30

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 120
    const/16 v1, -0x80

    const/4 v4, 0x2

    aput-byte v1, v0, v4

    .line 121
    const/4 v1, 0x3

    const/16 v5, 0x18

    aput-byte v5, v0, v1

    .line 122
    const/4 v1, 0x4

    aput-byte v4, v0, v1

    .line 123
    const/4 v6, 0x5

    const/16 v7, 0x19

    aput-byte v7, v0, v6

    .line 124
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    const/4 v7, 0x6

    invoke-static {v6, v2, v0, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 125
    new-array v6, v1, [B

    const/16 v7, 0xa

    invoke-static {v6, v2, v0, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSumByte:[B

    const/16 v7, 0xe

    invoke-static {v6, v2, v0, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    new-array v6, v1, [B

    fill-array-data v6, :array_0

    .line 128
    .local v6, "binFlashAddressByte":[B
    const/16 v7, 0x12

    invoke-static {v6, v2, v0, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVidByte:[B

    const/16 v7, 0x16

    invoke-static {v1, v2, v0, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mPidByte:[B

    invoke-static {v1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    const/16 v1, 0x1a

    aput-byte v3, v0, v1

    .line 132
    const/16 v1, 0x1b

    aput-byte v5, v0, v1

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getYearMonthDayByTimestamp(J)[I

    move-result-object v1

    .line 135
    .local v1, "times":[I
    aget v5, v1, v2

    int-to-byte v5, v5

    const/16 v7, 0x1c

    aput-byte v5, v0, v7

    .line 136
    aget v3, v1, v3

    int-to-byte v3, v3

    const/16 v5, 0x1d

    aput-byte v3, v0, v5

    .line 137
    aget v3, v1, v4

    int-to-byte v3, v3

    const/16 v4, 0x1e

    aput-byte v3, v0, v4

    .line 138
    const/16 v3, 0x1f

    invoke-static {v0, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v3

    .line 139
    return-object v0

    :array_0
    .array-data 1
        0x0t
        -0x80t
        0x0t
        0x0t
    .end array-data
.end method

.method protected parseUpgradeFileHead()V
    .locals 14

    .line 50
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v0, v0

    const v1, 0x8000

    if-le v0, v1, :cond_0

    .line 51
    const/16 v0, 0x7fc0

    .line 53
    .local v0, "headAddress":I
    const/4 v1, 0x7

    new-array v1, v1, [B

    .line 54
    .local v1, "startFlagByte":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v3, v1

    const/4 v4, 0x0

    invoke-static {v2, v0, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinStartFlag:Ljava/lang/String;

    .line 56
    array-length v2, v1

    add-int/2addr v0, v2

    .line 58
    const/4 v2, 0x2

    new-array v3, v2, [B

    .line 59
    .local v3, "chipIDByte":[B
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v6, v3

    invoke-static {v5, v0, v3, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 60
    array-length v5, v3

    invoke-static {v3, v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinChipId:Ljava/lang/String;

    .line 61
    array-length v5, v3

    add-int/2addr v0, v5

    .line 63
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinRoDataByte:[B

    array-length v7, v6

    invoke-static {v5, v0, v6, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinRoDataByte:[B

    array-length v5, v5

    add-int/2addr v0, v5

    .line 66
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRameCodeByte:[B

    array-length v7, v6

    invoke-static {v5, v0, v6, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRameCodeByte:[B

    array-length v5, v5

    add-int/2addr v0, v5

    .line 69
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRameCodeLenByte:[B

    array-length v7, v6

    invoke-static {v5, v0, v6, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRameCodeLenByte:[B

    array-length v5, v5

    add-int/2addr v0, v5

    .line 72
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRunStartByte:[B

    array-length v7, v6

    invoke-static {v5, v0, v6, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRunStartByte:[B

    const/4 v6, 0x3

    aget-byte v7, v5, v6

    shl-int/lit8 v7, v7, 0x18

    const/high16 v8, -0x1000000

    and-int/2addr v7, v8

    aget-byte v9, v5, v2

    shl-int/lit8 v9, v9, 0x10

    const/high16 v10, 0xff0000

    and-int/2addr v9, v10

    add-int/2addr v7, v9

    const/4 v9, 0x1

    aget-byte v11, v5, v9

    shl-int/lit8 v11, v11, 0x8

    const v12, 0xff00

    and-int/2addr v11, v12

    add-int/2addr v7, v11

    aget-byte v11, v5, v4

    and-int/lit16 v11, v11, 0xff

    add-int/2addr v7, v11

    iput v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRunStartAddress:I

    .line 76
    array-length v5, v5

    add-int/2addr v0, v5

    .line 78
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mPidByte:[B

    array-length v11, v7

    invoke-static {v5, v0, v7, v4, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mPidByte:[B

    aget-byte v7, v5, v9

    shl-int/lit8 v7, v7, 0x8

    and-int/2addr v7, v12

    aget-byte v11, v5, v4

    and-int/lit16 v11, v11, 0xff

    add-int/2addr v7, v11

    iput v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinPid:I

    .line 80
    array-length v5, v5

    add-int/2addr v0, v5

    .line 82
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVidByte:[B

    array-length v11, v7

    invoke-static {v5, v0, v7, v4, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVidByte:[B

    aget-byte v7, v5, v9

    shl-int/lit8 v7, v7, 0x8

    and-int/2addr v7, v12

    aget-byte v11, v5, v4

    and-int/lit16 v11, v11, 0xff

    add-int/2addr v7, v11

    iput v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinVid:I

    .line 84
    array-length v5, v5

    add-int/2addr v0, v5

    .line 86
    add-int/2addr v0, v2

    .line 88
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVersionByte:[B

    array-length v11, v7

    invoke-static {v5, v0, v7, v4, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVersionByte:[B

    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVersion:Ljava/lang/String;

    .line 90
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bin Version : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVersion:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "MiuiPadKeyboardManager"

    invoke-static {v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mVersionByte:[B

    array-length v5, v5

    add-int/2addr v0, v5

    .line 93
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v11, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mDeviceTypeByte:[B

    array-length v13, v11

    invoke-static {v5, v0, v11, v4, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mDeviceTypeByte:[B

    array-length v5, v5

    add-int/2addr v0, v5

    .line 96
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v11, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    array-length v13, v11

    invoke-static {v5, v0, v11, v4, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    aget-byte v11, v5, v6

    shl-int/lit8 v11, v11, 0x18

    and-int/2addr v11, v8

    aget-byte v13, v5, v2

    shl-int/lit8 v13, v13, 0x10

    and-int/2addr v13, v10

    add-int/2addr v11, v13

    aget-byte v13, v5, v9

    shl-int/lit8 v13, v13, 0x8

    and-int/2addr v13, v12

    add-int/2addr v11, v13

    aget-byte v5, v5, v4

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v11, v5

    iput v11, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinLength:I

    .line 100
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mLengthByte:[B

    array-length v5, v5

    add-int/2addr v0, v5

    .line 102
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    iget-object v11, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSumByte:[B

    array-length v13, v11

    invoke-static {v5, v0, v11, v4, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSumByte:[B

    aget-byte v6, v5, v6

    shl-int/lit8 v6, v6, 0x18

    and-int/2addr v6, v8

    aget-byte v2, v5, v2

    shl-int/lit8 v2, v2, 0x10

    and-int/2addr v2, v10

    add-int/2addr v6, v2

    aget-byte v2, v5, v9

    shl-int/lit8 v2, v2, 0x8

    and-int/2addr v2, v12

    add-int/2addr v6, v2

    aget-byte v2, v5, v4

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v6, v2

    iput v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSum:I

    .line 107
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    const/16 v4, 0x7fc0

    const/16 v5, 0x3f

    invoke-static {v2, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    .line 108
    .local v2, "sum":B
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    const/16 v5, 0x7fff

    aget-byte v4, v4, v5

    .line 109
    .local v4, "binSum":B
    if-eq v2, v4, :cond_0

    .line 110
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bin check head sum error:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    .end local v0    # "headAddress":I
    .end local v1    # "startFlagByte":[B
    .end local v2    # "sum":B
    .end local v3    # "chipIDByte":[B
    .end local v4    # "binSum":B
    :cond_0
    return-void
.end method

.method public startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z
    .locals 4
    .param p1, "usbDevice"    # Landroid/hardware/usb/UsbDevice;
    .param p2, "usbConnection"    # Landroid/hardware/usb/UsbDeviceConnection;
    .param p3, "outUsbEndpoint"    # Landroid/hardware/usb/UsbEndpoint;
    .param p4, "inUsbEndpoint"    # Landroid/hardware/usb/UsbEndpoint;

    .line 261
    const-string/jumbo v0, "start mcu upgrade"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    if-nez p4, :cond_0

    goto :goto_1

    .line 267
    :cond_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 268
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 269
    iput-object p3, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 270
    iput-object p4, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 272
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mFileBuf:[B

    array-length v2, v2

    const v3, 0x8000

    if-ge v2, v3, :cond_1

    goto :goto_0

    .line 277
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUpgradeCommand()Z

    move-result v0

    return v0

    .line 273
    :cond_2
    :goto_0
    const-string/jumbo v2, "start upgrade failed : invalid bin file"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    return v0

    .line 264
    :cond_3
    :goto_1
    const-string/jumbo v2, "start upgrade failed : device not ready"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    return v0
.end method
