.class public Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;
.super Ljava/lang/Object;
.source "UsbKeyboardUtil.java"


# static fields
.field public static final COMMAND_8031_BOOT:I = 0x12

.field public static final COMMAND_8031_RESET:I = 0x3

.field public static final COMMAND_8031_STATUS:I = 0xa1

.field public static final COMMAND_8031_VERSION:I = 0x1

.field public static final COMMAND_BACK_LIGHT_ENABLE:I = 0x23

.field public static final COMMAND_CAPS_LOCK:I = 0x26

.field public static final COMMAND_FIRMWARE_RECOVER:I = 0x20

.field public static final COMMAND_KEYBOARD_ENABLE:I = 0x22

.field public static final COMMAND_MIAUTH_INIT:I = 0x31

.field public static final COMMAND_MIAUTH_STEP3_TYPE1:I = 0x32

.field public static final COMMAND_POWER_STATE:I = 0x25

.field public static final COMMAND_READ_KEYBOARD:I = 0x52

.field public static final COMMAND_TOUCH_PAD_ENABLE:I = 0x21

.field public static final COMMAND_TOUCH_PAD_SENSITIVITY:I = 0x24

.field public static final COMMAND_WRITE_CMD_ACK:I = 0x30

.field public static final DEVICE_PRODUCT_ID:I = 0x3ffc

.field public static final DEVICE_VENDOR_ID:I = 0x3206

.field public static final KB_TYPE_HIGH:I = 0x3

.field public static final KB_TYPE_HIGH_OLD:I = 0x1

.field public static final KB_TYPE_LOW:I = 0x2

.field public static final OBJECT_FLASH:I = 0x39

.field public static final OBJECT_KEYBOARD:I = 0x38

.field public static final OBJECT_MCU:I = 0x18

.field public static final PACKET_32:I = 0x4e

.field public static final PACKET_64:I = 0x4f

.field public static final SOURCE_HOST:I = 0x80

.field public static final VALUE_BACK_LIGHT_DISABLE:I = 0x0

.field public static final VALUE_BACK_LIGHT_ENABLE:I = 0x1

.field public static final VALUE_CAPS_LOCK:I = 0x1

.field public static final VALUE_FIRMWARE_RECOVER:I = 0x36

.field public static final VALUE_KEYBOARD_DISABLE:I = 0x0

.field public static final VALUE_KEYBOARD_ENABLE:I = 0x1

.field public static final VALUE_POWER_STATE:I = 0x0

.field public static final VALUE_TOUCH_PAD_DISABLE:I = 0x0

.field public static final VALUE_TOUCH_PAD_ENABLE:I = 0x1

.field public static final VERSION_KEYBOARD:I = 0x31

.field public static final VERSION_MCU:I = 0x30


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method public static commandGetBootInfo()[B
    .locals 5

    .line 94
    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 95
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 96
    const/16 v1, 0x30

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 97
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 98
    const/4 v1, 0x3

    const/16 v4, 0x18

    aput-byte v4, v0, v1

    .line 99
    const/4 v1, 0x4

    const/16 v4, 0x12

    aput-byte v4, v0, v1

    .line 100
    const/4 v1, 0x5

    aput-byte v3, v0, v1

    .line 101
    const/4 v1, 0x6

    aput-byte v2, v0, v1

    .line 102
    const/4 v1, 0x7

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 103
    return-object v0
.end method

.method public static commandGetConnectState()[B
    .locals 5

    .line 67
    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 68
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 69
    const/16 v1, 0x31

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 70
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 71
    const/4 v1, 0x3

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 72
    const/4 v1, 0x4

    const/16 v4, -0x5f

    aput-byte v4, v0, v1

    .line 73
    const/4 v1, 0x5

    aput-byte v3, v0, v1

    .line 74
    const/4 v1, 0x6

    aput-byte v3, v0, v1

    .line 75
    const/4 v1, 0x7

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 76
    return-object v0
.end method

.method public static commandGetKeyboardStatus()[B
    .locals 4

    .line 124
    const/4 v0, 0x7

    new-array v0, v0, [B

    .line 125
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 126
    const/4 v1, 0x1

    const/16 v3, 0x31

    aput-byte v3, v0, v1

    .line 127
    const/4 v1, 0x2

    const/16 v3, -0x80

    aput-byte v3, v0, v1

    .line 128
    const/4 v1, 0x3

    const/16 v3, 0x38

    aput-byte v3, v0, v1

    .line 129
    const/4 v1, 0x4

    const/16 v3, 0x52

    aput-byte v3, v0, v1

    .line 130
    const/4 v1, 0x5

    aput-byte v2, v0, v1

    .line 131
    const/4 v1, 0x6

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 132
    return-object v0
.end method

.method public static commandGetKeyboardStatus(I)[B
    .locals 5
    .param p0, "target"    # I

    .line 136
    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 137
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 138
    const/16 v1, 0x31

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 139
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 140
    const/4 v1, 0x3

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 141
    const/4 v1, 0x4

    const/16 v4, 0x30

    aput-byte v4, v0, v1

    .line 142
    const/4 v1, 0x5

    aput-byte v3, v0, v1

    .line 143
    const/4 v1, 0x6

    int-to-byte v3, p0

    aput-byte v3, v0, v1

    .line 144
    const/4 v1, 0x7

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 145
    return-object v0
.end method

.method public static commandGetKeyboardVersion()[B
    .locals 5

    .line 162
    const/16 v0, 0x40

    new-array v0, v0, [B

    .line 163
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 164
    const/16 v1, 0x30

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 165
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 166
    const/4 v1, 0x3

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 167
    const/4 v1, 0x4

    aput-byte v3, v0, v1

    .line 168
    const/4 v1, 0x5

    aput-byte v3, v0, v1

    .line 169
    const/4 v1, 0x6

    aput-byte v3, v0, v1

    .line 170
    const/4 v1, 0x7

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 171
    return-object v0
.end method

.method public static commandGetResetInfo()[B
    .locals 6

    .line 108
    const/16 v0, 0xb

    new-array v0, v0, [B

    .line 109
    .local v0, "bytes":[B
    const/4 v1, 0x0

    const/16 v2, 0x4e

    aput-byte v2, v0, v1

    .line 110
    const/4 v3, 0x1

    const/16 v4, 0x30

    aput-byte v4, v0, v3

    .line 111
    const/4 v3, 0x2

    const/16 v5, -0x80

    aput-byte v5, v0, v3

    .line 112
    const/16 v3, 0x18

    const/4 v5, 0x3

    aput-byte v3, v0, v5

    .line 113
    const/4 v3, 0x4

    aput-byte v5, v0, v3

    .line 114
    const/4 v5, 0x5

    aput-byte v3, v0, v5

    .line 115
    const/4 v3, 0x6

    const/16 v5, 0x57

    aput-byte v5, v0, v3

    .line 116
    const/4 v3, 0x7

    aput-byte v2, v0, v3

    .line 117
    const/16 v2, 0x8

    const/16 v3, 0x38

    aput-byte v3, v0, v2

    .line 118
    const/16 v2, 0x9

    aput-byte v4, v0, v2

    .line 119
    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v1

    aput-byte v1, v0, v2

    .line 120
    return-object v0
.end method

.method public static commandGetVersionInfo()[B
    .locals 5

    .line 81
    const/4 v0, 0x7

    new-array v0, v0, [B

    .line 82
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 83
    const/16 v1, 0x30

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 84
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 85
    const/4 v1, 0x3

    const/16 v4, 0x18

    aput-byte v4, v0, v1

    .line 86
    const/4 v1, 0x4

    aput-byte v3, v0, v1

    .line 87
    const/4 v1, 0x5

    aput-byte v2, v0, v1

    .line 88
    const/4 v1, 0x6

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 89
    return-object v0
.end method

.method public static commandMiAuthStep3Type1([B[B)[B
    .locals 5
    .param p0, "keyMeta"    # [B
    .param p1, "challenge"    # [B

    .line 193
    const/16 v0, 0x1b

    new-array v0, v0, [B

    .line 194
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 195
    const/4 v1, 0x1

    const/16 v3, 0x31

    aput-byte v3, v0, v1

    .line 196
    const/4 v1, 0x2

    const/16 v3, -0x80

    aput-byte v3, v0, v1

    .line 197
    const/4 v1, 0x3

    const/16 v3, 0x38

    aput-byte v3, v0, v1

    .line 198
    const/16 v1, 0x32

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 199
    const/4 v1, 0x5

    const/16 v4, 0x14

    aput-byte v4, v0, v1

    .line 200
    if-eqz p0, :cond_0

    array-length v1, p0

    if-ne v1, v3, :cond_0

    .line 201
    const/4 v1, 0x6

    invoke-static {p0, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    :cond_0
    if-eqz p1, :cond_1

    array-length v1, p1

    const/16 v3, 0x10

    if-ne v1, v3, :cond_1

    .line 204
    const/16 v1, 0xa

    invoke-static {p1, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 206
    :cond_1
    const/16 v1, 0x1a

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 207
    return-object v0
.end method

.method public static commandMiDevAuthInit()[B
    .locals 5

    .line 175
    const/16 v0, 0xd

    new-array v0, v0, [B

    .line 176
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 177
    const/4 v1, 0x1

    const/16 v3, 0x31

    aput-byte v3, v0, v1

    .line 178
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 179
    const/4 v1, 0x3

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 180
    const/4 v1, 0x4

    aput-byte v3, v0, v1

    .line 181
    const/4 v1, 0x5

    const/4 v3, 0x6

    aput-byte v3, v0, v1

    .line 182
    const/16 v1, 0x4d

    aput-byte v1, v0, v3

    .line 183
    const/4 v1, 0x7

    const/16 v3, 0x49

    aput-byte v3, v0, v1

    .line 184
    const/16 v1, 0x8

    const/16 v3, 0x41

    aput-byte v3, v0, v1

    .line 185
    const/16 v1, 0x9

    const/16 v3, 0x55

    aput-byte v3, v0, v1

    .line 186
    const/16 v1, 0xa

    const/16 v3, 0x54

    aput-byte v3, v0, v1

    .line 187
    const/16 v1, 0xb

    const/16 v3, 0x48

    aput-byte v3, v0, v1

    .line 188
    const/16 v1, 0xc

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 189
    return-object v0
.end method

.method public static commandWriteKeyboardStatus(II)[B
    .locals 5
    .param p0, "target"    # I
    .param p1, "value"    # I

    .line 149
    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 150
    .local v0, "bytes":[B
    const/16 v1, 0x4e

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 151
    const/16 v1, 0x31

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 152
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 153
    const/4 v1, 0x3

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 154
    const/4 v1, 0x4

    int-to-byte v4, p0

    aput-byte v4, v0, v1

    .line 155
    const/4 v1, 0x5

    aput-byte v3, v0, v1

    .line 156
    const/4 v1, 0x6

    int-to-byte v3, p1

    aput-byte v3, v0, v1

    .line 157
    const/4 v1, 0x7

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 158
    return-object v0
.end method
