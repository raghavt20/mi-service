.class public abstract Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;
.super Ljava/lang/Object;
.source "UpgradeHelper.java"


# static fields
.field protected static final BIN_PATH:Ljava/lang/String; = "/vendor/etc/"

.field protected static final MAX_RETRY_TIMES:I = 0x3

.field protected static final MIN_FILE_LENGTH:I = 0x40


# instance fields
.field protected mBinLength:I

.field protected mCheckSum:I

.field protected mContext:Landroid/content/Context;

.field protected mFileBuf:[B

.field protected mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field protected mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field protected final mRecBuf:[B

.field protected final mSendBuf:[B

.field protected mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

.field protected mUsbDevice:Landroid/hardware/usb/UsbDevice;

.field protected mUsbInterface:Landroid/hardware/usb/UsbInterface;

.field protected mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "binPath"    # Ljava/lang/String;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "0000"

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mVersion:Ljava/lang/String;

    .line 26
    const/16 v0, 0x40

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mSendBuf:[B

    .line 27
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mRecBuf:[B

    .line 36
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mContext:Landroid/content/Context;

    .line 37
    invoke-virtual {p0, p2}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->readUpgradeFile(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method public abstract getBinPackInfo(II)[B
.end method

.method public abstract getBinPacketNum(I)I
.end method

.method public abstract getResetCommand()[B
.end method

.method public abstract getUpEndCommand()[B
.end method

.method public abstract getUpgradeCommand()[B
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public isLowerVersionThan(Ljava/lang/String;)Z
    .locals 1
    .param p1, "version"    # Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mFileBuf:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mVersion:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 66
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 65
    :goto_1
    return v0
.end method

.method protected abstract parseUpgradeFileHead()V
.end method

.method protected readFileToBuf(Ljava/io/File;)[B
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "fileBuf":[B
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .local v1, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->available()I

    move-result v2

    .line 53
    .local v2, "fileLen":I
    new-array v3, v2, [B

    move-object v0, v3

    .line 54
    invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    .end local v2    # "fileLen":I
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 51
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "fileBuf":[B
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;
    .end local p1    # "file":Ljava/io/File;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 57
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v0    # "fileBuf":[B
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;
    .restart local p1    # "file":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 58
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 55
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 56
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 59
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :goto_1
    nop

    .line 60
    :goto_2
    return-object v0
.end method

.method protected readUpgradeFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "binPath"    # Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 42
    .local v0, "binFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->readFileToBuf(Ljava/io/File;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->mFileBuf:[B

    goto :goto_0

    .line 45
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "upgrade file not exist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :goto_0
    return-void
.end method

.method protected sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
    .locals 3
    .param p1, "connection"    # Landroid/hardware/usb/UsbDeviceConnection;
    .param p2, "endpoint"    # Landroid/hardware/usb/UsbEndpoint;
    .param p3, "data"    # [B

    .line 75
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    goto :goto_0

    .line 78
    :cond_0
    array-length v1, p3

    const/16 v2, 0x1f4

    invoke-virtual {p1, p2, p3, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 76
    :cond_2
    :goto_0
    return v0
.end method

.method public abstract startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z
.end method
