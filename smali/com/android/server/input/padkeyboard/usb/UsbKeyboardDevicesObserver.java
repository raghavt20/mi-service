public class com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver extends android.os.FileObserver {
	 /* .source "UsbKeyboardDevicesObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String FILE_PATH;
private static final java.util.List sFileList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.io.File sKeyboard;
/* # instance fields */
private com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver$KeyboardActionListener mKeyboardActionListener;
/* # direct methods */
static com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ( ) {
/* .locals 3 */
/* .line 17 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 18 */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/bus/platform/drivers/xiaomi-keyboard/soc:xiaomi_keyboard/xiaomi_keyboard_conn_status"; // const-string v2, "/sys/bus/platform/drivers/xiaomi-keyboard/soc:xiaomi_keyboard/xiaomi_keyboard_conn_status"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 21 */
/* .line 22 */
return;
} // .end method
public com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ( ) {
/* .locals 2 */
/* .param p1, "keyboardActionListener" # Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener; */
/* .line 25 */
v0 = com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver.sFileList;
/* const/16 v1, 0x302 */
/* invoke-direct {p0, v0, v1}, Landroid/os/FileObserver;-><init>(Ljava/util/List;I)V */
/* .line 26 */
this.mKeyboardActionListener = p1;
/* .line 27 */
(( com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ) p0 ).enableKeyboardDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->enableKeyboardDevice()V
/* .line 28 */
return;
} // .end method
/* # virtual methods */
public void enableKeyboardDevice ( ) {
/* .locals 1 */
/* .line 39 */
final String v0 = "enable_keyboard"; // const-string v0, "enable_keyboard"
(( com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ) p0 ).writeKeyboardDevice ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->writeKeyboardDevice(Ljava/lang/String;)V
/* .line 40 */
return;
} // .end method
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 32 */
/* and-int/lit16 v0, p1, 0xfff */
/* .line 33 */
/* .local v0, "action":I */
v1 = this.mKeyboardActionListener;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 34 */
/* .line 36 */
} // :cond_0
return;
} // .end method
public void resetKeyboardDevice ( ) {
/* .locals 1 */
/* .line 43 */
final String v0 = "reset"; // const-string v0, "reset"
(( com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ) p0 ).writeKeyboardDevice ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->writeKeyboardDevice(Ljava/lang/String;)V
/* .line 44 */
return;
} // .end method
public void resetKeyboardHost ( ) {
/* .locals 1 */
/* .line 47 */
final String v0 = "host_reset"; // const-string v0, "host_reset"
(( com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ) p0 ).writeKeyboardDevice ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->writeKeyboardDevice(Ljava/lang/String;)V
/* .line 48 */
return;
} // .end method
public void startWatching ( ) {
/* .locals 2 */
/* .line 66 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
/* const-string/jumbo v1, "startWatching" */
android.util.Slog .i ( v0,v1 );
/* .line 67 */
/* invoke-super {p0}, Landroid/os/FileObserver;->startWatching()V */
/* .line 68 */
return;
} // .end method
public void stopWatching ( ) {
/* .locals 2 */
/* .line 72 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
/* const-string/jumbo v1, "stopWatching" */
android.util.Slog .i ( v0,v1 );
/* .line 73 */
/* invoke-super {p0}, Landroid/os/FileObserver;->stopWatching()V */
/* .line 74 */
return;
} // .end method
public void writeKeyboardDevice ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "command" # Ljava/lang/String; */
/* .line 51 */
v0 = com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver.sKeyboard;
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 52 */
try { // :try_start_0
	 /* new-instance v1, Ljava/io/FileOutputStream; */
	 /* invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move-object v0, v1 */
	 /* .line 53 */
	 /* .local v0, "out":Ljava/io/FileOutputStream; */
	 try { // :try_start_1
		 (( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
		 (( java.io.FileOutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
		 /* .line 54 */
		 (( java.io.FileOutputStream ) v0 ).flush ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 55 */
		 try { // :try_start_2
			 (( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
			 /* :try_end_2 */
			 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
			 /* .line 52 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 try { // :try_start_3
				 (( java.io.FileOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
				 /* :try_end_3 */
				 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
				 /* :catchall_1 */
				 /* move-exception v3 */
				 try { // :try_start_4
					 (( java.lang.Throwable ) v1 ).addSuppressed ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
				 } // .end local p0 # "this":Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;
			 } // .end local p1 # "command":Ljava/lang/String;
		 } // :goto_0
		 /* throw v1 */
		 /* :try_end_4 */
		 /* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
		 /* .line 55 */
	 } // .end local v0 # "out":Ljava/io/FileOutputStream;
	 /* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver; */
	 /* .restart local p1 # "command":Ljava/lang/String; */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 56 */
	 /* .local v0, "e":Ljava/io/IOException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v3, "write xiaomi_keyboard_conn_status error : " */
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.io.IOException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v1 );
	 /* .line 57 */
	 (( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
	 /* .line 58 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
/* .line 60 */
} // :cond_0
/* const-string/jumbo v0, "xiaomi_keyboard_conn_status not exists" */
android.util.Slog .i ( v2,v0 );
/* .line 62 */
} // :goto_2
return;
} // .end method
