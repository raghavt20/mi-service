.class public Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;
.super Landroid/os/FileObserver;
.source "UsbKeyboardDevicesObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;
    }
.end annotation


# static fields
.field public static final FILE_PATH:Ljava/lang/String; = "/sys/bus/platform/drivers/xiaomi-keyboard/soc:xiaomi_keyboard/xiaomi_keyboard_conn_status"

.field private static final sFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final sKeyboard:Ljava/io/File;


# instance fields
.field private mKeyboardActionListener:Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->sFileList:Ljava/util/List;

    .line 18
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/bus/platform/drivers/xiaomi-keyboard/soc:xiaomi_keyboard/xiaomi_keyboard_conn_status"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->sKeyboard:Ljava/io/File;

    .line 21
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;)V
    .locals 2
    .param p1, "keyboardActionListener"    # Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;

    .line 25
    sget-object v0, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->sFileList:Ljava/util/List;

    const/16 v1, 0x302

    invoke-direct {p0, v0, v1}, Landroid/os/FileObserver;-><init>(Ljava/util/List;I)V

    .line 26
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->mKeyboardActionListener:Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;

    .line 27
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->enableKeyboardDevice()V

    .line 28
    return-void
.end method


# virtual methods
.method public enableKeyboardDevice()V
    .locals 1

    .line 39
    const-string v0, "enable_keyboard"

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->writeKeyboardDevice(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public onEvent(ILjava/lang/String;)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .line 32
    and-int/lit16 v0, p1, 0xfff

    .line 33
    .local v0, "action":I
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->mKeyboardActionListener:Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;

    if-eqz v1, :cond_0

    .line 34
    invoke-interface {v1}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;->onKeyboardAction()V

    .line 36
    :cond_0
    return-void
.end method

.method public resetKeyboardDevice()V
    .locals 1

    .line 43
    const-string v0, "reset"

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->writeKeyboardDevice(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public resetKeyboardHost()V
    .locals 1

    .line 47
    const-string v0, "host_reset"

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->writeKeyboardDevice(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public startWatching()V
    .locals 2

    .line 66
    const-string v0, "MiuiPadKeyboardManager"

    const-string/jumbo v1, "startWatching"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-super {p0}, Landroid/os/FileObserver;->startWatching()V

    .line 68
    return-void
.end method

.method public stopWatching()V
    .locals 2

    .line 72
    const-string v0, "MiuiPadKeyboardManager"

    const-string/jumbo v1, "stopWatching"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-super {p0}, Landroid/os/FileObserver;->stopWatching()V

    .line 74
    return-void
.end method

.method public writeKeyboardDevice(Ljava/lang/String;)V
    .locals 4
    .param p1, "command"    # Ljava/lang/String;

    .line 51
    sget-object v0, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->sKeyboard:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "MiuiPadKeyboardManager"

    if-eqz v1, :cond_0

    .line 52
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 53
    .local v0, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 54
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 52
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;
    .end local p1    # "command":Ljava/lang/String;
    :goto_0
    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 55
    .end local v0    # "out":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;
    .restart local p1    # "command":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "write xiaomi_keyboard_conn_status error : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 58
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    goto :goto_2

    .line 60
    :cond_0
    const-string/jumbo v0, "xiaomi_keyboard_conn_status not exists"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_2
    return-void
.end method
