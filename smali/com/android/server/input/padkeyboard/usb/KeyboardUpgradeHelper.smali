.class public Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
.super Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;
.source "KeyboardUpgradeHelper.java"


# static fields
.field public static FIRST_LOW_KB_TYPE:Ljava/lang/String;

.field public static KB_BIN_PATH:Ljava/lang/String;

.field public static KB_H_BIN_PATH:Ljava/lang/String;

.field public static KB_L_BIN_PATH_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static WL_BIN_PATH:Ljava/lang/String;


# instance fields
.field protected mCheckSumByte:[B

.field protected mLengthByte:[B

.field protected mStartAddressByte:[B

.field protected mVersionByte:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_L_BIN_PATH_MAP:Ljava/util/Map;

    .line 25
    const-string v1, "a"

    sput-object v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->FIRST_LOW_KB_TYPE:Ljava/lang/String;

    .line 26
    const-string v1, "/vendor/etc/XM_KB.bin"

    sput-object v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_BIN_PATH:Ljava/lang/String;

    .line 27
    const-string v1, "/vendor/etc/XM_KB_H.bin"

    sput-object v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_H_BIN_PATH:Ljava/lang/String;

    .line 28
    const-string v1, "/vendor/etc/XM_KB_WL.bin"

    sput-object v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->WL_BIN_PATH:Ljava/lang/String;

    .line 36
    const-string v1, "20"

    const-string v2, "/vendor/etc/XM_KB_L.bin"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_L_BIN_PATH_MAP:Ljava/util/Map;

    const-string v1, "4a"

    const-string v2, "/vendor/etc/XM_KB_L_A.bin"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_L_BIN_PATH_MAP:Ljava/util/Map;

    const-string v1, "4b"

    const-string v2, "/vendor/etc/XM_KB_L_B.bin"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_L_BIN_PATH_MAP:Ljava/util/Map;

    const-string v1, "4c"

    const-string v2, "/vendor/etc/XM_KB_L_C.bin"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "binPath"    # Ljava/lang/String;

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x4

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mLengthByte:[B

    .line 31
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mStartAddressByte:[B

    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersionByte:[B

    .line 44
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->parseUpgradeFileHead()V

    .line 45
    return-void
.end method

.method private parseUpEndCommand()Z
    .locals 5

    .line 325
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "MiuiPadKeyboardManager"

    if-ne v0, v2, :cond_0

    .line 326
    const-string v0, "receive up end command state need wait"

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    return v3

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v0, v0, v1

    if-eqz v0, :cond_1

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive up end command state error:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v2, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    return v3

    .line 333
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendResetCommand()Z

    move-result v0

    return v0
.end method

.method private parseUpgradeCommand()Z
    .locals 5

    .line 256
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive upgrade command state error:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v4, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    return v3

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v4, 0x7

    aget-byte v0, v0, v4

    if-eqz v0, :cond_1

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive upgrade command mode error:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v4, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    return v3

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/16 v4, 0x8

    aget-byte v1, v1, v4

    invoke-static {v0, v3, v4, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_2

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive upgrade command checkSum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    return v3

    .line 268
    :cond_2
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendBinData()Z

    move-result v0

    return v0
.end method

.method private sendBinData()Z
    .locals 5

    .line 272
    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getBinPacketNum(I)I

    move-result v0

    .line 273
    .local v0, "packetNum":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send bin data, packet num: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v1, 0x0

    .local v1, "num":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 275
    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendOnePacket(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "send bin data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const/4 v2, 0x0

    return v2

    .line 274
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 280
    .end local v1    # "num":I
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUpEndCommand()Z

    move-result v1

    return v1
.end method

.method private sendOnePacket(I)Z
    .locals 5
    .param p1, "num"    # I

    .line 284
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 285
    mul-int/lit8 v0, p1, 0x34

    const/16 v2, 0x40

    invoke-virtual {p0, v0, v2}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getBinPackInfo(II)[B

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    invoke-static {v0, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 286
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_3

    .line 287
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 288
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 289
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 290
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    const/16 v4, -0x6f

    if-eq v2, v4, :cond_1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v2, v2, v3

    const/16 v3, 0x11

    if-ne v2, v3, :cond_0

    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    if-nez v2, :cond_0

    .line 292
    const/4 v1, 0x1

    return v1

    .line 296
    :cond_2
    const/16 v2, 0xc8

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "i":I
    :cond_3
    return v1
.end method

.method private sendResetCommand()Z
    .locals 9

    .line 338
    const-string/jumbo v0, "send reset command"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 340
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getResetCommand()[B

    move-result-object v0

    .line 341
    .local v0, "command":[B
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    array-length v4, v0

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 342
    const/4 v3, 0x0

    .line 343
    .local v3, "resetSent":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v5, 0x3

    if-ge v4, v5, :cond_5

    .line 344
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    .line 345
    const/4 v3, 0x1

    .line 346
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-static {v5, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 347
    :goto_1
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v5, v7, v8}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 348
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v7, 0x6

    aget-byte v5, v5, v7

    if-eqz v5, :cond_0

    .line 349
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "receive reset command state error:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v7, v8, v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    goto :goto_1

    .line 352
    :cond_0
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    const/4 v8, 0x7

    aget-byte v7, v7, v8

    invoke-static {v5, v2, v8, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v5

    if-nez v5, :cond_1

    .line 353
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "receive reset command checkSum error:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    aget-byte v7, v7, v8

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    goto :goto_1

    .line 356
    :cond_1
    const-string v2, "receive reset command"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    return v6

    .line 360
    :cond_2
    const-string/jumbo v5, "send reset command failed"

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_3
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v5, :cond_4

    .line 363
    return v6

    .line 365
    :cond_4
    const/16 v5, 0x3e8

    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 343
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 368
    .end local v4    # "i":I
    :cond_5
    return v3
.end method

.method private sendUpEndCommand()Z
    .locals 6

    .line 302
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 303
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getUpEndCommand()[B

    move-result-object v0

    .line 304
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 305
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x3

    if-ge v2, v3, :cond_4

    .line 306
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 308
    :cond_0
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 309
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->parseUpEndCommand()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 310
    const/4 v1, 0x1

    return v1

    .line 314
    :cond_1
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send up end command failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_2
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v3, :cond_3

    .line 317
    return v1

    .line 319
    :cond_3
    const/16 v3, 0x1f4

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 305
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 321
    .end local v2    # "i":I
    :cond_4
    return v1
.end method

.method private sendUpgradeCommand()Z
    .locals 7

    .line 232
    const-string/jumbo v0, "send upgrade command"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 234
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getUpgradeCommand()[B

    move-result-object v0

    .line 235
    .local v0, "command":[B
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    array-length v4, v0

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 236
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v4, 0x3

    if-ge v3, v4, :cond_4

    .line 237
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mSendBuf:[B

    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 238
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-static {v4, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 239
    :cond_0
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mRecBuf:[B

    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 240
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->parseUpgradeCommand()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 241
    const/4 v1, 0x1

    return v1

    .line 245
    :cond_1
    const-string/jumbo v4, "send upgrade command failed"

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_2
    const/16 v4, 0x1f4

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 248
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v4, :cond_3

    .line 249
    return v2

    .line 236
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 252
    .end local v3    # "i":I
    :cond_4
    return v2
.end method


# virtual methods
.method public getBinPackInfo(II)[B
    .locals 20
    .param p1, "offset"    # I
    .param p2, "packetLength"    # I

    .line 112
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    const/16 v3, 0x20

    new-array v4, v3, [B

    .line 113
    .local v4, "bytes":[B
    const/4 v5, 0x0

    .line 114
    .local v5, "binlength":I
    const/4 v7, 0x7

    const/4 v8, 0x6

    const/16 v9, 0x11

    const/16 v10, -0x80

    const/16 v11, 0x30

    const/16 v12, 0x4f

    const/4 v13, 0x5

    const/4 v14, 0x4

    const/16 v15, 0x38

    const/16 v16, 0x9

    const/16 v17, 0x2

    const/16 v18, 0x1

    const/16 v19, 0x3

    const/4 v6, 0x0

    if-ne v2, v3, :cond_1

    .line 115
    new-array v4, v3, [B

    .line 116
    const/16 v3, 0x14

    .line 117
    .end local v5    # "binlength":I
    .local v3, "binlength":I
    aput-byte v12, v4, v6

    .line 118
    aput-byte v11, v4, v18

    .line 119
    aput-byte v10, v4, v17

    .line 120
    aput-byte v15, v4, v19

    .line 121
    aput-byte v9, v4, v14

    .line 122
    const/16 v5, 0x1d

    aput-byte v5, v4, v13

    .line 123
    add-int/lit16 v5, v1, 0x1000

    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v5

    .line 124
    .local v5, "offsetB":[B
    aget-byte v9, v5, v19

    aput-byte v9, v4, v8

    .line 125
    aget-byte v8, v5, v17

    aput-byte v8, v4, v7

    .line 126
    aget-byte v7, v5, v18

    const/16 v8, 0x8

    aput-byte v7, v4, v8

    .line 128
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    add-int v8, v1, v3

    if-ge v7, v8, :cond_0

    .line 129
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    sub-int/2addr v7, v1

    .line 130
    .end local v3    # "binlength":I
    .local v7, "binlength":I
    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v3

    aget-byte v3, v3, v19

    aput-byte v3, v4, v16

    move v3, v7

    goto :goto_0

    .line 132
    .end local v7    # "binlength":I
    .restart local v3    # "binlength":I
    :cond_0
    const/16 v7, 0x14

    aput-byte v7, v4, v16

    .line 134
    :goto_0
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    const/16 v8, 0xa

    invoke-static {v7, v1, v4, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 136
    const/16 v7, 0x1f

    invoke-static {v4, v6, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v6

    aput-byte v6, v4, v7

    .line 137
    .end local v5    # "offsetB":[B
    move v5, v3

    goto :goto_1

    .end local v3    # "binlength":I
    .local v5, "binlength":I
    :cond_1
    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 138
    new-array v4, v3, [B

    .line 139
    const/16 v3, 0x34

    .line 140
    .end local v5    # "binlength":I
    .restart local v3    # "binlength":I
    aput-byte v12, v4, v6

    .line 141
    aput-byte v11, v4, v18

    .line 142
    aput-byte v10, v4, v17

    .line 143
    aput-byte v15, v4, v19

    .line 144
    aput-byte v9, v4, v14

    .line 145
    aput-byte v15, v4, v13

    .line 146
    add-int/lit16 v5, v1, 0x1000

    invoke-static {v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v5

    .line 147
    .local v5, "offsetB":[B
    aget-byte v9, v5, v19

    aput-byte v9, v4, v8

    .line 148
    aget-byte v8, v5, v17

    aput-byte v8, v4, v7

    .line 149
    aget-byte v7, v5, v18

    const/16 v8, 0x8

    aput-byte v7, v4, v8

    .line 150
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    add-int v8, v1, v3

    if-ge v7, v8, :cond_2

    .line 151
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    sub-int v3, v7, v1

    .line 152
    add-int/lit8 v7, v3, 0x4

    int-to-byte v7, v7

    aput-byte v7, v4, v13

    .line 153
    int-to-byte v7, v3

    aput-byte v7, v4, v16

    .line 154
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    const/16 v8, 0xa

    invoke-static {v7, v1, v4, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    add-int/lit8 v7, v3, 0xa

    add-int/lit8 v8, v3, 0xa

    invoke-static {v4, v6, v8}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v6

    aput-byte v6, v4, v7

    move v5, v3

    goto :goto_1

    .line 157
    :cond_2
    const/16 v3, 0x34

    .line 158
    const/16 v7, 0x34

    aput-byte v7, v4, v16

    .line 159
    iget-object v7, v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    const/16 v8, 0xa

    invoke-static {v7, v1, v4, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    const/16 v7, 0x3e

    invoke-static {v4, v6, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v6

    aput-byte v6, v4, v7

    move v5, v3

    .line 164
    .end local v3    # "binlength":I
    .local v5, "binlength":I
    :cond_3
    :goto_1
    return-object v4
.end method

.method public getBinPacketNum(I)I
    .locals 5
    .param p1, "length"    # I

    .line 200
    const/4 v0, 0x0

    .line 201
    .local v0, "num":I
    const/16 v1, 0x20

    if-ne p1, v1, :cond_0

    .line 202
    iget v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mBinLength:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x4034000000000000L    # 20.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    goto :goto_0

    .line 203
    :cond_0
    const/16 v1, 0x40

    if-ne p1, v1, :cond_1

    .line 204
    iget v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mBinLength:I

    int-to-double v1, v1

    const-wide/high16 v3, 0x404a000000000000L    # 52.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 206
    :cond_1
    :goto_0
    return v0
.end method

.method public getResetCommand()[B
    .locals 5

    .line 186
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 187
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 188
    const/4 v1, 0x1

    const/16 v3, 0x30

    aput-byte v3, v0, v1

    .line 189
    const/4 v1, 0x2

    const/16 v3, -0x80

    aput-byte v3, v0, v1

    .line 190
    const/4 v1, 0x3

    const/16 v3, 0x38

    aput-byte v3, v0, v1

    .line 191
    const/4 v1, 0x4

    const/4 v3, 0x6

    aput-byte v3, v0, v1

    .line 192
    const/4 v4, 0x5

    aput-byte v1, v0, v4

    .line 193
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mStartAddressByte:[B

    invoke-static {v4, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 194
    const/16 v1, 0xa

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 195
    return-object v0
.end method

.method public getUpEndCommand()[B
    .locals 7

    .line 169
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 170
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 171
    const/16 v1, 0x30

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 172
    const/16 v1, -0x80

    const/4 v4, 0x2

    aput-byte v1, v0, v4

    .line 173
    const/4 v1, 0x3

    const/16 v5, 0x38

    aput-byte v5, v0, v1

    .line 174
    const/4 v1, 0x4

    aput-byte v1, v0, v1

    .line 175
    const/4 v5, 0x5

    const/16 v6, 0xb

    aput-byte v6, v0, v5

    .line 176
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mLengthByte:[B

    const/4 v6, 0x6

    invoke-static {v5, v2, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 177
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mStartAddressByte:[B

    const/16 v6, 0xa

    invoke-static {v5, v2, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    const/16 v5, 0xe

    invoke-static {v1, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersionByte:[B

    const/16 v3, 0xf

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    const/16 v1, 0x11

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 181
    return-object v0
.end method

.method public getUpgradeCommand()[B
    .locals 7

    .line 95
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 96
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 97
    const/16 v1, 0x30

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    .line 98
    const/16 v1, -0x80

    const/4 v4, 0x2

    aput-byte v1, v0, v4

    .line 99
    const/4 v1, 0x3

    const/16 v5, 0x38

    aput-byte v5, v0, v1

    .line 100
    const/4 v1, 0x4

    aput-byte v4, v0, v1

    .line 101
    const/4 v5, 0x5

    const/16 v6, 0xb

    aput-byte v6, v0, v5

    .line 102
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mLengthByte:[B

    const/4 v6, 0x6

    invoke-static {v5, v2, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mStartAddressByte:[B

    const/16 v6, 0xa

    invoke-static {v5, v2, v0, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    const/16 v5, 0xe

    invoke-static {v1, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersionByte:[B

    const/16 v3, 0xf

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    const/16 v1, 0x11

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 107
    return-object v0
.end method

.method protected parseUpgradeFileHead()V
    .locals 10

    .line 49
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v0, v0

    const/16 v1, 0x40

    if-le v0, v1, :cond_2

    .line 50
    const/4 v0, 0x0

    .line 52
    .local v0, "headAddress":I
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mLengthByte:[B

    array-length v3, v2

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mLengthByte:[B

    const/4 v2, 0x1

    aget-byte v3, v1, v2

    shl-int/lit8 v3, v3, 0x8

    const v5, 0xff00

    and-int/2addr v3, v5

    aget-byte v1, v1, v4

    and-int/lit16 v1, v1, 0xff

    add-int/2addr v3, v1

    iput v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mBinLength:I

    .line 54
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mLengthByte:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 57
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mStartAddressByte:[B

    array-length v5, v3

    invoke-static {v1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mStartAddressByte:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 61
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    array-length v5, v3

    invoke-static {v1, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    aget-byte v3, v1, v4

    and-int/lit16 v3, v3, 0x80

    const-string v5, ""

    const/16 v6, 0x10

    if-eqz v3, :cond_0

    .line 63
    new-instance v1, Ljava/math/BigInteger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FFFFFF"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    array-length v8, v7

    .line 64
    invoke-static {v7, v8, v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2HexString([BILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 65
    invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I

    move-result v1

    iput v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSum:I

    goto :goto_0

    .line 67
    :cond_0
    array-length v3, v1

    .line 68
    invoke-static {v1, v3, v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2HexString([BILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-static {v1, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSum:I

    .line 70
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 73
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersionByte:[B

    array-length v7, v3

    invoke-static {v1, v0, v3, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersionByte:[B

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersion:Ljava/lang/String;

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bin Version : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mVersion:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MiuiPadKeyboardManager"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v7, v7

    sub-int/2addr v7, v6

    invoke-static {v1, v6, v7}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v1

    .line 79
    .local v1, "sum2":I
    not-int v7, v1

    add-int/2addr v7, v2

    .line 80
    .local v7, "sum":I
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "hexSum":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSumByte:[B

    array-length v9, v8

    .line 82
    invoke-static {v8, v9, v5}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2HexString([BILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 81
    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 83
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    .line 84
    const-string v4, "check sum error"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 86
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v3, v3

    sub-int/2addr v3, v6

    new-array v3, v3, [B

    .line 87
    .local v3, "realData":[B
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v8, v3

    invoke-static {v5, v6, v3, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iput-object v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    .line 91
    .end local v0    # "headAddress":I
    .end local v1    # "sum2":I
    .end local v2    # "hexSum":Ljava/lang/String;
    .end local v3    # "realData":[B
    .end local v7    # "sum":I
    :cond_2
    :goto_1
    return-void
.end method

.method public startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z
    .locals 4
    .param p1, "usbDevice"    # Landroid/hardware/usb/UsbDevice;
    .param p2, "usbConnection"    # Landroid/hardware/usb/UsbDeviceConnection;
    .param p3, "outUsbEndpoint"    # Landroid/hardware/usb/UsbEndpoint;
    .param p4, "inUsbEndpoint"    # Landroid/hardware/usb/UsbEndpoint;

    .line 212
    const-string/jumbo v0, "start keyboard upgrade"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    if-nez p4, :cond_0

    goto :goto_1

    .line 218
    :cond_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 219
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 220
    iput-object p3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 221
    iput-object p4, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 223
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mFileBuf:[B

    array-length v2, v2

    const/16 v3, 0x40

    if-ge v2, v3, :cond_1

    goto :goto_0

    .line 228
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUpgradeCommand()Z

    move-result v0

    return v0

    .line 224
    :cond_2
    :goto_0
    const-string/jumbo v2, "start upgrade failed : invalid bin file"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    return v0

    .line 215
    :cond_3
    :goto_1
    const-string/jumbo v2, "start upgrade failed : device not ready"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    return v0
.end method
