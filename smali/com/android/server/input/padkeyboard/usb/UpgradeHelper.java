public abstract class com.android.server.input.padkeyboard.usb.UpgradeHelper {
	 /* .source "UpgradeHelper.java" */
	 /* # static fields */
	 protected static final java.lang.String BIN_PATH;
	 protected static final Integer MAX_RETRY_TIMES;
	 protected static final Integer MIN_FILE_LENGTH;
	 /* # instance fields */
	 protected Integer mBinLength;
	 protected Integer mCheckSum;
	 protected android.content.Context mContext;
	 protected mFileBuf;
	 protected android.hardware.usb.UsbEndpoint mInUsbEndpoint;
	 protected android.hardware.usb.UsbEndpoint mOutUsbEndpoint;
	 protected final mRecBuf;
	 protected final mSendBuf;
	 protected android.hardware.usb.UsbDeviceConnection mUsbConnection;
	 protected android.hardware.usb.UsbDevice mUsbDevice;
	 protected android.hardware.usb.UsbInterface mUsbInterface;
	 protected java.lang.String mVersion;
	 /* # direct methods */
	 public com.android.server.input.padkeyboard.usb.UpgradeHelper ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "binPath" # Ljava/lang/String; */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 23 */
		 final String v0 = "0000"; // const-string v0, "0000"
		 this.mVersion = v0;
		 /* .line 26 */
		 /* const/16 v0, 0x40 */
		 /* new-array v1, v0, [B */
		 this.mSendBuf = v1;
		 /* .line 27 */
		 /* new-array v0, v0, [B */
		 this.mRecBuf = v0;
		 /* .line 36 */
		 this.mContext = p1;
		 /* .line 37 */
		 (( com.android.server.input.padkeyboard.usb.UpgradeHelper ) p0 ).readUpgradeFile ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->readUpgradeFile(Ljava/lang/String;)V
		 /* .line 38 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract getBinPackInfo ( Integer p0, Integer p1 ) {
	 } // .end method
	 public abstract Integer getBinPacketNum ( Integer p0 ) {
	 } // .end method
	 public abstract getResetCommand ( ) {
	 } // .end method
	 public abstract getUpEndCommand ( ) {
	 } // .end method
	 public abstract getUpgradeCommand ( ) {
	 } // .end method
	 public java.lang.String getVersion ( ) {
		 /* .locals 1 */
		 /* .line 70 */
		 v0 = this.mVersion;
	 } // .end method
	 public Boolean isLowerVersionThan ( java.lang.String p0 ) {
		 /* .locals 1 */
		 /* .param p1, "version" # Ljava/lang/String; */
		 /* .line 65 */
		 v0 = this.mFileBuf;
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 v0 = this.mVersion;
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 if ( p1 != null) { // if-eqz p1, :cond_0
					 /* .line 66 */
					 v0 = 					 (( java.lang.String ) p1 ).compareTo ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
					 /* if-ltz v0, :cond_0 */
				 } // :cond_0
				 int v0 = 0; // const/4 v0, 0x0
			 } // :cond_1
		 } // :goto_0
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 65 */
	 } // :goto_1
} // .end method
protected abstract void parseUpgradeFileHead ( ) {
} // .end method
protected readFileToBuf ( java.io.File p0 ) {
	 /* .locals 4 */
	 /* .param p1, "file" # Ljava/io/File; */
	 /* .line 50 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 51 */
	 /* .local v0, "fileBuf":[B */
	 try { // :try_start_0
		 /* new-instance v1, Ljava/io/FileInputStream; */
		 /* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
		 /* :try_end_0 */
		 /* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 52 */
		 /* .local v1, "fileInputStream":Ljava/io/FileInputStream; */
		 try { // :try_start_1
			 v2 = 			 (( java.io.FileInputStream ) v1 ).available ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->available()I
			 /* .line 53 */
			 /* .local v2, "fileLen":I */
			 /* new-array v3, v2, [B */
			 /* move-object v0, v3 */
			 /* .line 54 */
			 (( java.io.FileInputStream ) v1 ).read ( v0 ); // invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 55 */
		 } // .end local v2 # "fileLen":I
		 try { // :try_start_2
			 (( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
			 /* :try_end_2 */
			 /* .catch Ljava/io/FileNotFoundException; {:try_start_2 ..:try_end_2} :catch_1 */
			 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
			 /* .line 51 */
			 /* :catchall_0 */
			 /* move-exception v2 */
			 try { // :try_start_3
				 (( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
				 /* :try_end_3 */
				 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
				 /* :catchall_1 */
				 /* move-exception v3 */
				 try { // :try_start_4
					 (( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
				 } // .end local v0 # "fileBuf":[B
			 } // .end local p0 # "this":Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;
		 } // .end local p1 # "file":Ljava/io/File;
	 } // :goto_0
	 /* throw v2 */
	 /* :try_end_4 */
	 /* .catch Ljava/io/FileNotFoundException; {:try_start_4 ..:try_end_4} :catch_1 */
	 /* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
	 /* .line 57 */
} // .end local v1 # "fileInputStream":Ljava/io/FileInputStream;
/* .restart local v0 # "fileBuf":[B */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/usb/UpgradeHelper; */
/* .restart local p1 # "file":Ljava/io/File; */
/* :catch_0 */
/* move-exception v1 */
/* .line 58 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 55 */
} // .end local v1 # "e":Ljava/io/IOException;
/* :catch_1 */
/* move-exception v1 */
/* .line 56 */
/* .local v1, "e":Ljava/io/FileNotFoundException; */
(( java.io.FileNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
/* .line 59 */
} // .end local v1 # "e":Ljava/io/FileNotFoundException;
} // :goto_1
/* nop */
/* .line 60 */
} // :goto_2
} // .end method
protected void readUpgradeFile ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "binPath" # Ljava/lang/String; */
/* .line 41 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 42 */
/* .local v0, "binFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 43 */
(( com.android.server.input.padkeyboard.usb.UpgradeHelper ) p0 ).readFileToBuf ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;->readFileToBuf(Ljava/io/File;)[B
this.mFileBuf = v1;
/* .line 45 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "upgrade file not exist: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v0 ).getPath ( ); // invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 47 */
} // :goto_0
return;
} // .end method
protected Boolean sendUsbData ( android.hardware.usb.UsbDeviceConnection p0, android.hardware.usb.UsbEndpoint p1, Object[] p2 ) {
/* .locals 3 */
/* .param p1, "connection" # Landroid/hardware/usb/UsbDeviceConnection; */
/* .param p2, "endpoint" # Landroid/hardware/usb/UsbEndpoint; */
/* .param p3, "data" # [B */
/* .line 75 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* if-nez p3, :cond_0 */
/* .line 78 */
} // :cond_0
/* array-length v1, p3 */
/* const/16 v2, 0x1f4 */
v1 = (( android.hardware.usb.UsbDeviceConnection ) p1 ).bulkTransfer ( p2, p3, v1, v2 ); // invoke-virtual {p1, p2, p3, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 76 */
} // :cond_2
} // :goto_0
} // .end method
public abstract Boolean startUpgrade ( android.hardware.usb.UsbDevice p0, android.hardware.usb.UsbDeviceConnection p1, android.hardware.usb.UsbEndpoint p2, android.hardware.usb.UsbEndpoint p3 ) {
} // .end method
