public class com.android.server.input.padkeyboard.usb.McuUpgradeHelper extends com.android.server.input.padkeyboard.usb.UpgradeHelper {
	 /* .source "McuUpgradeHelper.java" */
	 /* # static fields */
	 protected static final Integer BIN_HEAD_LENGTH;
	 protected static final Integer HEAD_BASE_ADDRESS;
	 protected static final java.lang.String KB_MCU_BIN_PATH;
	 protected static final Integer TARGET;
	 public static final java.lang.String VERSION_HEAD;
	 /* # instance fields */
	 protected java.lang.String mBinChipId;
	 protected Integer mBinPid;
	 protected final mBinRoDataByte;
	 protected java.lang.String mBinStartFlag;
	 protected Integer mBinVid;
	 protected final mCheckSumByte;
	 protected final mDeviceTypeByte;
	 protected final mLengthByte;
	 protected final mPidByte;
	 protected final mRameCodeByte;
	 protected final mRameCodeLenByte;
	 protected Integer mRunStartAddress;
	 protected final mRunStartByte;
	 protected final mVersionByte;
	 protected final mVidByte;
	 /* # direct methods */
	 public com.android.server.input.padkeyboard.usb.McuUpgradeHelper ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 44 */
		 final String v0 = "/vendor/etc/XM_KB_MCU.bin"; // const-string v0, "/vendor/etc/XM_KB_MCU.bin"
		 /* invoke-direct {p0, p1, v0}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
		 /* .line 29 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* new-array v1, v0, [B */
		 this.mBinRoDataByte = v1;
		 /* .line 30 */
		 /* new-array v1, v0, [B */
		 this.mRameCodeByte = v1;
		 /* .line 31 */
		 /* new-array v1, v0, [B */
		 this.mRameCodeLenByte = v1;
		 /* .line 32 */
		 /* new-array v1, v0, [B */
		 this.mRunStartByte = v1;
		 /* .line 34 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* new-array v2, v1, [B */
		 this.mPidByte = v2;
		 /* .line 36 */
		 /* new-array v1, v1, [B */
		 this.mVidByte = v1;
		 /* .line 38 */
		 /* const/16 v1, 0x10 */
		 /* new-array v1, v1, [B */
		 this.mVersionByte = v1;
		 /* .line 39 */
		 /* new-array v1, v0, [B */
		 this.mLengthByte = v1;
		 /* .line 40 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* new-array v1, v1, [B */
		 this.mDeviceTypeByte = v1;
		 /* .line 41 */
		 /* new-array v0, v0, [B */
		 this.mCheckSumByte = v0;
		 /* .line 45 */
		 (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).parseUpgradeFileHead ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpgradeFileHead()V
		 /* .line 46 */
		 return;
	 } // .end method
	 private Boolean parseUpEndCommand ( ) {
		 /* .locals 5 */
		 /* .line 373 */
		 v0 = this.mRecBuf;
		 int v1 = 6; // const/4 v1, 0x6
		 /* aget-byte v0, v0, v1 */
		 int v2 = 1; // const/4 v2, 0x1
		 int v3 = 0; // const/4 v3, 0x0
		 final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
		 /* if-ne v0, v2, :cond_0 */
		 /* .line 374 */
		 final String v0 = "receive up end command state need wait"; // const-string v0, "receive up end command state need wait"
		 android.util.Slog .i ( v4,v0 );
		 /* .line 375 */
		 /* .line 377 */
	 } // :cond_0
	 v0 = this.mRecBuf;
	 /* aget-byte v0, v0, v1 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 378 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "receive up end command state error:"; // const-string v2, "receive up end command state error:"
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.mRecBuf;
		 /* aget-byte v1, v2, v1 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v4,v0 );
		 /* .line 379 */
		 /* .line 381 */
	 } // :cond_1
	 v0 = 	 /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUpFlashCommand()Z */
} // .end method
private Boolean parseUpFlashCommand ( ) {
	 /* .locals 5 */
	 /* .line 408 */
	 v0 = this.mRecBuf;
	 int v1 = 6; // const/4 v1, 0x6
	 /* aget-byte v0, v0, v1 */
	 final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
	 int v3 = 0; // const/4 v3, 0x0
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 409 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "receive up flash command state error:"; // const-string v4, "receive up flash command state error:"
		 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v4 = this.mRecBuf;
		 /* aget-byte v1, v4, v1 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v2,v0 );
		 /* .line 410 */
		 /* .line 412 */
	 } // :cond_0
	 v0 = this.mRecBuf;
	 v1 = this.mRecBuf;
	 int v4 = 7; // const/4 v4, 0x7
	 /* aget-byte v1, v1, v4 */
	 v0 = 	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( v0,v3,v4,v1 );
	 /* if-nez v0, :cond_1 */
	 /* .line 413 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "receive up flash command checkSum error:"; // const-string v1, "receive up flash command checkSum error:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.mRecBuf;
	 /* aget-byte v1, v1, v4 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* .line 414 */
	 /* .line 416 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendResetCommand()Z */
} // .end method
private Boolean parseUpgradeCommand ( ) {
/* .locals 5 */
/* .line 305 */
v0 = this.mRecBuf;
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v0, v0, v1 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 306 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "receive upgrade command state error:"; // const-string v4, "receive upgrade command state error:"
	 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.mRecBuf;
	 /* aget-byte v1, v4, v1 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* .line 307 */
	 /* .line 309 */
} // :cond_0
v0 = this.mRecBuf;
int v4 = 7; // const/4 v4, 0x7
/* aget-byte v0, v0, v4 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 310 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "receive upgrade command mode error:"; // const-string v4, "receive upgrade command mode error:"
	 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.mRecBuf;
	 /* aget-byte v1, v4, v1 */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* .line 311 */
	 /* .line 313 */
} // :cond_1
v0 = this.mRecBuf;
v1 = this.mRecBuf;
/* const/16 v4, 0x8 */
/* aget-byte v1, v1, v4 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( v0,v3,v4,v1 );
/* if-nez v0, :cond_2 */
/* .line 314 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive upgrade command checkSum error:"; // const-string v1, "receive upgrade command checkSum error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRecBuf;
/* aget-byte v1, v1, v4 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 315 */
/* .line 317 */
} // :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendBinData()Z */
} // .end method
private Boolean sendBinData ( ) {
/* .locals 5 */
/* .line 321 */
/* const/16 v0, 0x40 */
v0 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).getBinPacketNum ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getBinPacketNum(I)I
/* .line 322 */
/* .local v0, "packetNum":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send bin data, packet num: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 323 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "num":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 324 */
v3 = /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendOnePacket(I)Z */
/* if-nez v3, :cond_0 */
/* .line 325 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "send bin data " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " failed"; // const-string v4, " failed"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 326 */
int v2 = 0; // const/4 v2, 0x0
/* .line 323 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 329 */
} // .end local v1 # "num":I
} // :cond_1
v1 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUpEndCommand()Z */
} // .end method
private Boolean sendOnePacket ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "num" # I */
/* .line 333 */
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 334 */
/* mul-int/lit8 v0, p1, 0x34 */
/* const/16 v2, 0x40 */
(( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).getBinPackInfo ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getBinPackInfo(II)[B
v3 = this.mSendBuf;
java.lang.System .arraycopy ( v0,v1,v3,v1,v2 );
/* .line 335 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v2 = 3; // const/4 v2, 0x3
/* if-ge v0, v2, :cond_2 */
/* .line 336 */
v2 = this.mUsbConnection;
v3 = this.mOutUsbEndpoint;
v4 = this.mSendBuf;
v2 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 337 */
v2 = this.mRecBuf;
java.util.Arrays .fill ( v2,v1 );
/* .line 338 */
} // :cond_0
v2 = this.mUsbConnection;
v3 = this.mInUsbEndpoint;
v4 = this.mRecBuf;
v2 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 339 */
v2 = this.mRecBuf;
int v3 = 4; // const/4 v3, 0x4
/* aget-byte v2, v2, v3 */
/* const/16 v3, -0x6f */
/* if-ne v2, v3, :cond_0 */
v2 = this.mRecBuf;
int v3 = 6; // const/4 v3, 0x6
/* aget-byte v2, v2, v3 */
/* if-nez v2, :cond_0 */
/* .line 340 */
int v1 = 1; // const/4 v1, 0x1
/* .line 344 */
} // :cond_1
/* const/16 v2, 0xc8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v2 );
/* .line 335 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 346 */
} // .end local v0 # "i":I
} // :cond_2
} // .end method
private Boolean sendResetCommand ( ) {
/* .locals 9 */
/* .line 420 */
/* const-string/jumbo v0, "send reset command" */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 421 */
v0 = this.mSendBuf;
int v2 = 0; // const/4 v2, 0x0
java.util.Arrays .fill ( v0,v2 );
/* .line 422 */
(( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).getResetCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getResetCommand()[B
/* .line 423 */
/* .local v0, "command":[B */
v3 = this.mSendBuf;
/* array-length v4, v0 */
java.lang.System .arraycopy ( v0,v2,v3,v2,v4 );
/* .line 424 */
int v3 = 0; // const/4 v3, 0x0
/* .line 425 */
/* .local v3, "resetSent":Z */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
int v5 = 3; // const/4 v5, 0x3
/* if-ge v4, v5, :cond_5 */
/* .line 426 */
v5 = this.mUsbConnection;
v6 = this.mOutUsbEndpoint;
v7 = this.mSendBuf;
v5 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v5, v6, v7 ); // invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
int v6 = 1; // const/4 v6, 0x1
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 427 */
int v3 = 1; // const/4 v3, 0x1
/* .line 428 */
v5 = this.mRecBuf;
java.util.Arrays .fill ( v5,v2 );
/* .line 429 */
} // :goto_1
v5 = this.mUsbConnection;
v7 = this.mInUsbEndpoint;
v8 = this.mRecBuf;
v5 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v5, v7, v8 ); // invoke-virtual {p0, v5, v7, v8}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 430 */
v5 = this.mRecBuf;
int v7 = 6; // const/4 v7, 0x6
/* aget-byte v5, v5, v7 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 431 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "receive reset command state error:"; // const-string v8, "receive reset command state error:"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mRecBuf;
/* aget-byte v7, v8, v7 */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
/* .line 432 */
/* .line 434 */
} // :cond_0
v5 = this.mRecBuf;
v7 = this.mRecBuf;
int v8 = 7; // const/4 v8, 0x7
/* aget-byte v7, v7, v8 */
v5 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( v5,v2,v8,v7 );
/* if-nez v5, :cond_1 */
/* .line 435 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "receive reset command checkSum error:"; // const-string v7, "receive reset command checkSum error:"
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mRecBuf;
/* aget-byte v7, v7, v8 */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
/* .line 436 */
/* .line 438 */
} // :cond_1
final String v2 = "receive reset command"; // const-string v2, "receive reset command"
android.util.Slog .i ( v1,v2 );
/* .line 439 */
/* .line 442 */
} // :cond_2
/* const-string/jumbo v5, "send reset command failed" */
android.util.Slog .i ( v1,v5 );
/* .line 444 */
} // :cond_3
/* const/16 v5, 0x3e8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v5 );
/* .line 445 */
v5 = this.mUsbDevice;
/* if-nez v5, :cond_4 */
/* .line 446 */
/* .line 425 */
} // :cond_4
/* add-int/lit8 v4, v4, 0x1 */
/* goto/16 :goto_0 */
/* .line 450 */
} // .end local v4 # "i":I
} // :cond_5
} // .end method
private Boolean sendUpEndCommand ( ) {
/* .locals 6 */
/* .line 350 */
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 351 */
(( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).getUpEndCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getUpEndCommand()[B
/* .line 352 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 353 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 3; // const/4 v3, 0x3
/* if-ge v2, v3, :cond_4 */
/* .line 354 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 355 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 356 */
} // :cond_0
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 357 */
v3 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpEndCommand()Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 358 */
int v1 = 1; // const/4 v1, 0x1
/* .line 362 */
} // :cond_1
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send up end command failed" */
android.util.Slog .i ( v3,v4 );
/* .line 364 */
} // :cond_2
/* const/16 v3, 0x1f4 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v3 );
/* .line 365 */
v3 = this.mUsbDevice;
/* if-nez v3, :cond_3 */
/* .line 366 */
/* .line 353 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 369 */
} // .end local v2 # "i":I
} // :cond_4
} // .end method
private Boolean sendUpFlashCommand ( ) {
/* .locals 6 */
/* .line 385 */
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 386 */
(( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).getUpFlashCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getUpFlashCommand()[B
/* .line 387 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 388 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 6; // const/4 v3, 0x6
/* if-ge v2, v3, :cond_4 */
/* .line 389 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 390 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 391 */
} // :cond_0
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 392 */
v3 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpFlashCommand()Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 393 */
int v1 = 1; // const/4 v1, 0x1
/* .line 397 */
} // :cond_1
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send up flash command failed" */
android.util.Slog .i ( v3,v4 );
/* .line 399 */
} // :cond_2
/* const/16 v3, 0x3e8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v3 );
/* .line 400 */
v3 = this.mUsbDevice;
/* if-nez v3, :cond_3 */
/* .line 401 */
/* .line 388 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 404 */
} // .end local v2 # "i":I
} // :cond_4
} // .end method
private Boolean sendUpgradeCommand ( ) {
/* .locals 7 */
/* .line 281 */
/* const-string/jumbo v0, "send upgrade command" */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 282 */
v0 = this.mSendBuf;
int v2 = 0; // const/4 v2, 0x0
java.util.Arrays .fill ( v0,v2 );
/* .line 283 */
(( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).getUpgradeCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getUpgradeCommand()[B
/* .line 284 */
/* .local v0, "command":[B */
v3 = this.mSendBuf;
/* array-length v4, v0 */
java.lang.System .arraycopy ( v0,v2,v3,v2,v4 );
/* .line 285 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
int v4 = 3; // const/4 v4, 0x3
/* if-ge v3, v4, :cond_4 */
/* .line 286 */
v4 = this.mUsbConnection;
v5 = this.mOutUsbEndpoint;
v6 = this.mSendBuf;
v4 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v4, v5, v6 ); // invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 287 */
v4 = this.mRecBuf;
java.util.Arrays .fill ( v4,v2 );
/* .line 288 */
} // :cond_0
v4 = this.mUsbConnection;
v5 = this.mInUsbEndpoint;
v6 = this.mRecBuf;
v4 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) p0 ).sendUsbData ( v4, v5, v6 ); // invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 289 */
v4 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->parseUpgradeCommand()Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 290 */
int v1 = 1; // const/4 v1, 0x1
/* .line 294 */
} // :cond_1
/* const-string/jumbo v4, "send upgrade command failed" */
android.util.Slog .i ( v1,v4 );
/* .line 296 */
} // :cond_2
/* const/16 v4, 0x1f4 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v4 );
/* .line 297 */
v4 = this.mUsbDevice;
/* if-nez v4, :cond_3 */
/* .line 298 */
/* .line 285 */
} // :cond_3
/* add-int/lit8 v3, v3, 0x1 */
/* .line 301 */
} // .end local v3 # "i":I
} // :cond_4
} // .end method
/* # virtual methods */
public getBinPackInfo ( Integer p0, Integer p1 ) {
/* .locals 21 */
/* .param p1, "offset" # I */
/* .param p2, "packetLength" # I */
/* .line 144 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
/* move/from16 v2, p2 */
/* const/16 v3, 0x20 */
/* new-array v4, v3, [B */
/* .line 145 */
/* .local v4, "bytes":[B */
int v5 = 0; // const/4 v5, 0x0
/* .line 146 */
/* .local v5, "binlength":I */
/* const/16 v7, 0x8 */
int v8 = 7; // const/4 v8, 0x7
int v9 = 6; // const/4 v9, 0x6
int v10 = 5; // const/4 v10, 0x5
/* const/16 v11, 0x11 */
int v12 = 4; // const/4 v12, 0x4
/* const/16 v13, 0x18 */
/* const/16 v14, -0x80 */
/* const/16 v15, 0x30 */
/* const/16 v16, 0x4f */
/* const/16 v17, 0x9 */
/* const/16 v18, 0x2 */
/* const/16 v19, 0x1 */
int v6 = 0; // const/4 v6, 0x0
/* const/16 v20, 0x3 */
/* if-ne v2, v3, :cond_1 */
/* .line 147 */
/* new-array v4, v3, [B */
/* .line 148 */
/* const/16 v3, 0x14 */
/* .line 149 */
} // .end local v5 # "binlength":I
/* .local v3, "binlength":I */
/* aput-byte v16, v4, v6 */
/* .line 150 */
/* aput-byte v15, v4, v19 */
/* .line 151 */
/* aput-byte v14, v4, v18 */
/* .line 152 */
/* aput-byte v13, v4, v20 */
/* .line 153 */
/* aput-byte v11, v4, v12 */
/* .line 154 */
/* const/16 v5, 0x1d */
/* aput-byte v5, v4, v10 */
/* .line 155 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B */
/* .line 156 */
/* .local v5, "offsetB":[B */
/* aget-byte v10, v5, v20 */
/* aput-byte v10, v4, v9 */
/* .line 157 */
/* aget-byte v9, v5, v18 */
/* aput-byte v9, v4, v8 */
/* .line 158 */
/* aget-byte v8, v5, v19 */
/* aput-byte v8, v4, v7 */
/* .line 160 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* add-int/lit16 v8, v1, 0x7fc0 */
/* add-int/2addr v8, v3 */
/* if-ge v7, v8, :cond_0 */
/* .line 161 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* add-int/lit16 v7, v7, -0x7fc0 */
/* sub-int/2addr v7, v1 */
/* .line 162 */
} // .end local v3 # "binlength":I
/* .local v7, "binlength":I */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( v7 );
/* aget-byte v3, v3, v20 */
/* aput-byte v3, v4, v17 */
/* move v3, v7 */
/* .line 164 */
} // .end local v7 # "binlength":I
/* .restart local v3 # "binlength":I */
} // :cond_0
/* const/16 v3, 0x14 */
/* .line 165 */
/* const/16 v7, 0x14 */
/* aput-byte v7, v4, v17 */
/* .line 167 */
} // :goto_0
v7 = this.mFileBuf;
/* add-int/lit16 v8, v1, 0x7fc0 */
/* const/16 v9, 0xa */
java.lang.System .arraycopy ( v7,v8,v4,v9,v3 );
/* .line 169 */
/* const/16 v7, 0x1f */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v4,v6,v7 );
/* aput-byte v6, v4, v7 */
/* .line 170 */
} // .end local v5 # "offsetB":[B
/* move v5, v3 */
} // .end local v3 # "binlength":I
/* .local v5, "binlength":I */
} // :cond_1
/* const/16 v3, 0x40 */
/* if-ne v2, v3, :cond_3 */
/* .line 171 */
/* new-array v4, v3, [B */
/* .line 172 */
/* const/16 v3, 0x34 */
/* .line 173 */
} // .end local v5 # "binlength":I
/* .restart local v3 # "binlength":I */
/* aput-byte v16, v4, v6 */
/* .line 174 */
/* aput-byte v15, v4, v19 */
/* .line 175 */
/* aput-byte v14, v4, v18 */
/* .line 176 */
/* aput-byte v13, v4, v20 */
/* .line 177 */
/* aput-byte v11, v4, v12 */
/* .line 178 */
/* const/16 v5, 0x38 */
/* aput-byte v5, v4, v10 */
/* .line 179 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B */
/* .line 180 */
/* .local v5, "offsetB":[B */
/* aget-byte v10, v5, v20 */
/* aput-byte v10, v4, v9 */
/* .line 181 */
/* aget-byte v9, v5, v18 */
/* aput-byte v9, v4, v8 */
/* .line 182 */
/* aget-byte v8, v5, v19 */
/* aput-byte v8, v4, v7 */
/* .line 183 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* add-int/lit16 v8, v1, 0x7fc0 */
/* add-int/2addr v8, v3 */
/* const/16 v9, 0x34 */
/* if-ge v7, v8, :cond_2 */
/* .line 184 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* add-int/lit16 v7, v7, -0x7fc0 */
/* sub-int/2addr v7, v1 */
/* .line 185 */
} // .end local v3 # "binlength":I
/* .restart local v7 # "binlength":I */
/* aput-byte v9, v4, v17 */
/* move v3, v7 */
/* .line 187 */
} // .end local v7 # "binlength":I
/* .restart local v3 # "binlength":I */
} // :cond_2
/* const/16 v3, 0x34 */
/* .line 188 */
/* aput-byte v9, v4, v17 */
/* .line 190 */
} // :goto_1
v7 = this.mFileBuf;
/* add-int/lit16 v8, v1, 0x7fc0 */
/* const/16 v9, 0xa */
java.lang.System .arraycopy ( v7,v8,v4,v9,v3 );
/* .line 191 */
/* const/16 v7, 0x3e */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v4,v6,v7 );
/* aput-byte v6, v4, v7 */
/* move v5, v3 */
/* .line 194 */
} // .end local v3 # "binlength":I
/* .local v5, "binlength":I */
} // :cond_3
} // :goto_2
} // .end method
public Integer getBinPacketNum ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "length" # I */
/* .line 249 */
int v0 = 0; // const/4 v0, 0x0
/* .line 250 */
/* .local v0, "totle":I */
/* const/16 v1, 0x20 */
/* const/16 v2, 0x40 */
/* if-ne p1, v1, :cond_0 */
/* .line 251 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinLength:I */
/* add-int/2addr v1, v2 */
/* int-to-double v1, v1 */
/* const-wide/high16 v3, 0x4034000000000000L # 20.0 */
/* div-double/2addr v1, v3 */
java.lang.Math .ceil ( v1,v2 );
/* move-result-wide v1 */
/* double-to-int v0, v1 */
/* .line 252 */
} // :cond_0
/* if-ne p1, v2, :cond_1 */
/* .line 253 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinLength:I */
/* add-int/2addr v1, v2 */
/* int-to-double v1, v1 */
/* const-wide/high16 v3, 0x404a000000000000L # 52.0 */
/* div-double/2addr v1, v3 */
java.lang.Math .ceil ( v1,v2 );
/* move-result-wide v1 */
/* double-to-int v0, v1 */
/* .line 255 */
} // :cond_1
} // :goto_0
} // .end method
public getResetCommand ( ) {
/* .locals 6 */
/* .line 218 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 219 */
/* .local v0, "bytes":[B */
int v1 = 0; // const/4 v1, 0x0
/* const/16 v2, 0x4e */
/* aput-byte v2, v0, v1 */
/* .line 220 */
int v3 = 1; // const/4 v3, 0x1
/* const/16 v4, 0x30 */
/* aput-byte v4, v0, v3 */
/* .line 221 */
int v3 = 2; // const/4 v3, 0x2
/* const/16 v5, -0x80 */
/* aput-byte v5, v0, v3 */
/* .line 222 */
/* const/16 v3, 0x18 */
int v5 = 3; // const/4 v5, 0x3
/* aput-byte v3, v0, v5 */
/* .line 223 */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v5, v0, v3 */
/* .line 224 */
int v5 = 5; // const/4 v5, 0x5
/* aput-byte v3, v0, v5 */
/* .line 225 */
int v3 = 6; // const/4 v3, 0x6
/* const/16 v5, 0x57 */
/* aput-byte v5, v0, v3 */
/* .line 226 */
int v3 = 7; // const/4 v3, 0x7
/* aput-byte v2, v0, v3 */
/* .line 227 */
/* const/16 v2, 0x8 */
/* const/16 v3, 0x38 */
/* aput-byte v3, v0, v2 */
/* .line 228 */
/* const/16 v2, 0x9 */
/* aput-byte v4, v0, v2 */
/* .line 229 */
/* const/16 v2, 0xa */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v1,v2 );
/* aput-byte v1, v0, v2 */
/* .line 230 */
} // .end method
public getUpEndCommand ( ) {
/* .locals 8 */
/* .line 199 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 200 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 201 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x30 */
/* aput-byte v3, v0, v1 */
/* .line 202 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, -0x80 */
/* aput-byte v3, v0, v1 */
/* .line 203 */
int v4 = 3; // const/4 v4, 0x3
/* const/16 v5, 0x18 */
/* aput-byte v5, v0, v4 */
/* .line 204 */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v4, v0, v4 */
/* .line 205 */
int v6 = 5; // const/4 v6, 0x5
/* const/16 v7, 0x15 */
/* aput-byte v7, v0, v6 */
/* .line 206 */
v6 = this.mLengthByte;
int v7 = 6; // const/4 v7, 0x6
java.lang.System .arraycopy ( v6,v2,v0,v7,v4 );
/* .line 207 */
v6 = this.mCheckSumByte;
/* const/16 v7, 0xe */
java.lang.System .arraycopy ( v6,v2,v0,v7,v4 );
/* .line 208 */
/* const/16 v4, 0x13 */
/* aput-byte v3, v0, v4 */
/* .line 209 */
v3 = this.mVidByte;
/* const/16 v4, 0x16 */
java.lang.System .arraycopy ( v3,v2,v0,v4,v1 );
/* .line 210 */
v3 = this.mPidByte;
java.lang.System .arraycopy ( v3,v2,v0,v5,v1 );
/* .line 211 */
/* const/16 v1, 0x1a */
/* aput-byte v5, v0, v1 */
/* .line 212 */
/* const/16 v1, 0x1b */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 213 */
} // .end method
public getUpFlashCommand ( ) {
/* .locals 5 */
/* .line 235 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 236 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 237 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x30 */
/* aput-byte v3, v0, v1 */
/* .line 238 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, -0x80 */
/* aput-byte v3, v0, v1 */
/* .line 239 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v3, 0x18 */
/* aput-byte v3, v0, v1 */
/* .line 240 */
int v1 = 4; // const/4 v1, 0x4
int v3 = 6; // const/4 v3, 0x6
/* aput-byte v3, v0, v1 */
/* .line 241 */
int v4 = 5; // const/4 v4, 0x5
/* aput-byte v1, v0, v4 */
/* .line 242 */
v4 = this.mLengthByte;
java.lang.System .arraycopy ( v4,v2,v0,v3,v1 );
/* .line 243 */
/* const/16 v1, 0xa */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 244 */
} // .end method
public getUpgradeCommand ( ) {
/* .locals 9 */
/* .line 117 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 118 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 119 */
/* const/16 v1, 0x30 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v1, v0, v3 */
/* .line 120 */
/* const/16 v1, -0x80 */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v1, v0, v4 */
/* .line 121 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v5, 0x18 */
/* aput-byte v5, v0, v1 */
/* .line 122 */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v4, v0, v1 */
/* .line 123 */
int v6 = 5; // const/4 v6, 0x5
/* const/16 v7, 0x19 */
/* aput-byte v7, v0, v6 */
/* .line 124 */
v6 = this.mLengthByte;
int v7 = 6; // const/4 v7, 0x6
java.lang.System .arraycopy ( v6,v2,v0,v7,v1 );
/* .line 125 */
/* new-array v6, v1, [B */
/* const/16 v7, 0xa */
java.lang.System .arraycopy ( v6,v2,v0,v7,v1 );
/* .line 126 */
v6 = this.mCheckSumByte;
/* const/16 v7, 0xe */
java.lang.System .arraycopy ( v6,v2,v0,v7,v1 );
/* .line 127 */
/* new-array v6, v1, [B */
/* fill-array-data v6, :array_0 */
/* .line 128 */
/* .local v6, "binFlashAddressByte":[B */
/* const/16 v7, 0x12 */
java.lang.System .arraycopy ( v6,v2,v0,v7,v1 );
/* .line 129 */
v1 = this.mVidByte;
/* const/16 v7, 0x16 */
java.lang.System .arraycopy ( v1,v2,v0,v7,v4 );
/* .line 130 */
v1 = this.mPidByte;
java.lang.System .arraycopy ( v1,v2,v0,v5,v4 );
/* .line 131 */
/* const/16 v1, 0x1a */
/* aput-byte v3, v0, v1 */
/* .line 132 */
/* const/16 v1, 0x1b */
/* aput-byte v5, v0, v1 */
/* .line 134 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .getYearMonthDayByTimestamp ( v7,v8 );
/* .line 135 */
/* .local v1, "times":[I */
/* aget v5, v1, v2 */
/* int-to-byte v5, v5 */
/* const/16 v7, 0x1c */
/* aput-byte v5, v0, v7 */
/* .line 136 */
/* aget v3, v1, v3 */
/* int-to-byte v3, v3 */
/* const/16 v5, 0x1d */
/* aput-byte v3, v0, v5 */
/* .line 137 */
/* aget v3, v1, v4 */
/* int-to-byte v3, v3 */
/* const/16 v4, 0x1e */
/* aput-byte v3, v0, v4 */
/* .line 138 */
/* const/16 v3, 0x1f */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v3 );
/* aput-byte v2, v0, v3 */
/* .line 139 */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* -0x80t */
/* 0x0t */
/* 0x0t */
} // .end array-data
} // .end method
protected void parseUpgradeFileHead ( ) {
/* .locals 14 */
/* .line 50 */
v0 = this.mFileBuf;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mFileBuf;
/* array-length v0, v0 */
/* const v1, 0x8000 */
/* if-le v0, v1, :cond_0 */
/* .line 51 */
/* const/16 v0, 0x7fc0 */
/* .line 53 */
/* .local v0, "headAddress":I */
int v1 = 7; // const/4 v1, 0x7
/* new-array v1, v1, [B */
/* .line 54 */
/* .local v1, "startFlagByte":[B */
v2 = this.mFileBuf;
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
java.lang.System .arraycopy ( v2,v0,v1,v4,v3 );
/* .line 55 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v1 );
this.mBinStartFlag = v2;
/* .line 56 */
/* array-length v2, v1 */
/* add-int/2addr v0, v2 */
/* .line 58 */
int v2 = 2; // const/4 v2, 0x2
/* new-array v3, v2, [B */
/* .line 59 */
/* .local v3, "chipIDByte":[B */
v5 = this.mFileBuf;
/* array-length v6, v3 */
java.lang.System .arraycopy ( v5,v0,v3,v4,v6 );
/* .line 60 */
/* array-length v5, v3 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v3,v5 );
this.mBinChipId = v5;
/* .line 61 */
/* array-length v5, v3 */
/* add-int/2addr v0, v5 */
/* .line 63 */
v5 = this.mFileBuf;
v6 = this.mBinRoDataByte;
/* array-length v7, v6 */
java.lang.System .arraycopy ( v5,v0,v6,v4,v7 );
/* .line 64 */
v5 = this.mBinRoDataByte;
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 66 */
v5 = this.mFileBuf;
v6 = this.mRameCodeByte;
/* array-length v7, v6 */
java.lang.System .arraycopy ( v5,v0,v6,v4,v7 );
/* .line 67 */
v5 = this.mRameCodeByte;
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 69 */
v5 = this.mFileBuf;
v6 = this.mRameCodeLenByte;
/* array-length v7, v6 */
java.lang.System .arraycopy ( v5,v0,v6,v4,v7 );
/* .line 70 */
v5 = this.mRameCodeLenByte;
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 72 */
v5 = this.mFileBuf;
v6 = this.mRunStartByte;
/* array-length v7, v6 */
java.lang.System .arraycopy ( v5,v0,v6,v4,v7 );
/* .line 73 */
v5 = this.mRunStartByte;
int v6 = 3; // const/4 v6, 0x3
/* aget-byte v7, v5, v6 */
/* shl-int/lit8 v7, v7, 0x18 */
/* const/high16 v8, -0x1000000 */
/* and-int/2addr v7, v8 */
/* aget-byte v9, v5, v2 */
/* shl-int/lit8 v9, v9, 0x10 */
/* const/high16 v10, 0xff0000 */
/* and-int/2addr v9, v10 */
/* add-int/2addr v7, v9 */
int v9 = 1; // const/4 v9, 0x1
/* aget-byte v11, v5, v9 */
/* shl-int/lit8 v11, v11, 0x8 */
/* const v12, 0xff00 */
/* and-int/2addr v11, v12 */
/* add-int/2addr v7, v11 */
/* aget-byte v11, v5, v4 */
/* and-int/lit16 v11, v11, 0xff */
/* add-int/2addr v7, v11 */
/* iput v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mRunStartAddress:I */
/* .line 76 */
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 78 */
v5 = this.mFileBuf;
v7 = this.mPidByte;
/* array-length v11, v7 */
java.lang.System .arraycopy ( v5,v0,v7,v4,v11 );
/* .line 79 */
v5 = this.mPidByte;
/* aget-byte v7, v5, v9 */
/* shl-int/lit8 v7, v7, 0x8 */
/* and-int/2addr v7, v12 */
/* aget-byte v11, v5, v4 */
/* and-int/lit16 v11, v11, 0xff */
/* add-int/2addr v7, v11 */
/* iput v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinPid:I */
/* .line 80 */
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 82 */
v5 = this.mFileBuf;
v7 = this.mVidByte;
/* array-length v11, v7 */
java.lang.System .arraycopy ( v5,v0,v7,v4,v11 );
/* .line 83 */
v5 = this.mVidByte;
/* aget-byte v7, v5, v9 */
/* shl-int/lit8 v7, v7, 0x8 */
/* and-int/2addr v7, v12 */
/* aget-byte v11, v5, v4 */
/* and-int/lit16 v11, v11, 0xff */
/* add-int/2addr v7, v11 */
/* iput v7, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinVid:I */
/* .line 84 */
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 86 */
/* add-int/2addr v0, v2 */
/* .line 88 */
v5 = this.mFileBuf;
v7 = this.mVersionByte;
/* array-length v11, v7 */
java.lang.System .arraycopy ( v5,v0,v7,v4,v11 );
/* .line 89 */
v5 = this.mVersionByte;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v5 );
this.mVersion = v5;
/* .line 90 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Bin Version : "; // const-string v7, "Bin Version : "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mVersion;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "MiuiPadKeyboardManager"; // const-string v7, "MiuiPadKeyboardManager"
android.util.Slog .i ( v7,v5 );
/* .line 91 */
v5 = this.mVersionByte;
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 93 */
v5 = this.mFileBuf;
v11 = this.mDeviceTypeByte;
/* array-length v13, v11 */
java.lang.System .arraycopy ( v5,v0,v11,v4,v13 );
/* .line 94 */
v5 = this.mDeviceTypeByte;
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 96 */
v5 = this.mFileBuf;
v11 = this.mLengthByte;
/* array-length v13, v11 */
java.lang.System .arraycopy ( v5,v0,v11,v4,v13 );
/* .line 97 */
v5 = this.mLengthByte;
/* aget-byte v11, v5, v6 */
/* shl-int/lit8 v11, v11, 0x18 */
/* and-int/2addr v11, v8 */
/* aget-byte v13, v5, v2 */
/* shl-int/lit8 v13, v13, 0x10 */
/* and-int/2addr v13, v10 */
/* add-int/2addr v11, v13 */
/* aget-byte v13, v5, v9 */
/* shl-int/lit8 v13, v13, 0x8 */
/* and-int/2addr v13, v12 */
/* add-int/2addr v11, v13 */
/* aget-byte v5, v5, v4 */
/* and-int/lit16 v5, v5, 0xff */
/* add-int/2addr v11, v5 */
/* iput v11, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mBinLength:I */
/* .line 100 */
v5 = this.mLengthByte;
/* array-length v5, v5 */
/* add-int/2addr v0, v5 */
/* .line 102 */
v5 = this.mFileBuf;
v11 = this.mCheckSumByte;
/* array-length v13, v11 */
java.lang.System .arraycopy ( v5,v0,v11,v4,v13 );
/* .line 103 */
v5 = this.mCheckSumByte;
/* aget-byte v6, v5, v6 */
/* shl-int/lit8 v6, v6, 0x18 */
/* and-int/2addr v6, v8 */
/* aget-byte v2, v5, v2 */
/* shl-int/lit8 v2, v2, 0x10 */
/* and-int/2addr v2, v10 */
/* add-int/2addr v6, v2 */
/* aget-byte v2, v5, v9 */
/* shl-int/lit8 v2, v2, 0x8 */
/* and-int/2addr v2, v12 */
/* add-int/2addr v6, v2 */
/* aget-byte v2, v5, v4 */
/* and-int/lit16 v2, v2, 0xff */
/* add-int/2addr v6, v2 */
/* iput v6, p0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->mCheckSum:I */
/* .line 107 */
v2 = this.mFileBuf;
/* const/16 v4, 0x7fc0 */
/* const/16 v5, 0x3f */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v2,v4,v5 );
/* .line 108 */
/* .local v2, "sum":B */
v4 = this.mFileBuf;
/* const/16 v5, 0x7fff */
/* aget-byte v4, v4, v5 */
/* .line 109 */
/* .local v4, "binSum":B */
/* if-eq v2, v4, :cond_0 */
/* .line 110 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "bin check head sum error:"; // const-string v6, "bin check head sum error:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "/"; // const-string v6, "/"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v5 );
/* .line 113 */
} // .end local v0 # "headAddress":I
} // .end local v1 # "startFlagByte":[B
} // .end local v2 # "sum":B
} // .end local v3 # "chipIDByte":[B
} // .end local v4 # "binSum":B
} // :cond_0
return;
} // .end method
public Boolean startUpgrade ( android.hardware.usb.UsbDevice p0, android.hardware.usb.UsbDeviceConnection p1, android.hardware.usb.UsbEndpoint p2, android.hardware.usb.UsbEndpoint p3 ) {
/* .locals 4 */
/* .param p1, "usbDevice" # Landroid/hardware/usb/UsbDevice; */
/* .param p2, "usbConnection" # Landroid/hardware/usb/UsbDeviceConnection; */
/* .param p3, "outUsbEndpoint" # Landroid/hardware/usb/UsbEndpoint; */
/* .param p4, "inUsbEndpoint" # Landroid/hardware/usb/UsbEndpoint; */
/* .line 261 */
/* const-string/jumbo v0, "start mcu upgrade" */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 262 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
if ( p3 != null) { // if-eqz p3, :cond_3
/* if-nez p4, :cond_0 */
/* .line 267 */
} // :cond_0
this.mUsbDevice = p1;
/* .line 268 */
this.mUsbConnection = p2;
/* .line 269 */
this.mOutUsbEndpoint = p3;
/* .line 270 */
this.mInUsbEndpoint = p4;
/* .line 272 */
v2 = this.mFileBuf;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mFileBuf;
/* array-length v2, v2 */
/* const v3, 0x8000 */
/* if-ge v2, v3, :cond_1 */
/* .line 277 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->sendUpgradeCommand()Z */
/* .line 273 */
} // :cond_2
} // :goto_0
/* const-string/jumbo v2, "start upgrade failed : invalid bin file" */
android.util.Slog .i ( v1,v2 );
/* .line 274 */
/* .line 264 */
} // :cond_3
} // :goto_1
/* const-string/jumbo v2, "start upgrade failed : device not ready" */
android.util.Slog .i ( v1,v2 );
/* .line 265 */
} // .end method
