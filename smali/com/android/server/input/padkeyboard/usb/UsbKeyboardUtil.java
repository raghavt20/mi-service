public class com.android.server.input.padkeyboard.usb.UsbKeyboardUtil {
	 /* .source "UsbKeyboardUtil.java" */
	 /* # static fields */
	 public static final Integer COMMAND_8031_BOOT;
	 public static final Integer COMMAND_8031_RESET;
	 public static final Integer COMMAND_8031_STATUS;
	 public static final Integer COMMAND_8031_VERSION;
	 public static final Integer COMMAND_BACK_LIGHT_ENABLE;
	 public static final Integer COMMAND_CAPS_LOCK;
	 public static final Integer COMMAND_FIRMWARE_RECOVER;
	 public static final Integer COMMAND_KEYBOARD_ENABLE;
	 public static final Integer COMMAND_MIAUTH_INIT;
	 public static final Integer COMMAND_MIAUTH_STEP3_TYPE1;
	 public static final Integer COMMAND_POWER_STATE;
	 public static final Integer COMMAND_READ_KEYBOARD;
	 public static final Integer COMMAND_TOUCH_PAD_ENABLE;
	 public static final Integer COMMAND_TOUCH_PAD_SENSITIVITY;
	 public static final Integer COMMAND_WRITE_CMD_ACK;
	 public static final Integer DEVICE_PRODUCT_ID;
	 public static final Integer DEVICE_VENDOR_ID;
	 public static final Integer KB_TYPE_HIGH;
	 public static final Integer KB_TYPE_HIGH_OLD;
	 public static final Integer KB_TYPE_LOW;
	 public static final Integer OBJECT_FLASH;
	 public static final Integer OBJECT_KEYBOARD;
	 public static final Integer OBJECT_MCU;
	 public static final Integer PACKET_32;
	 public static final Integer PACKET_64;
	 public static final Integer SOURCE_HOST;
	 public static final Integer VALUE_BACK_LIGHT_DISABLE;
	 public static final Integer VALUE_BACK_LIGHT_ENABLE;
	 public static final Integer VALUE_CAPS_LOCK;
	 public static final Integer VALUE_FIRMWARE_RECOVER;
	 public static final Integer VALUE_KEYBOARD_DISABLE;
	 public static final Integer VALUE_KEYBOARD_ENABLE;
	 public static final Integer VALUE_POWER_STATE;
	 public static final Integer VALUE_TOUCH_PAD_DISABLE;
	 public static final Integer VALUE_TOUCH_PAD_ENABLE;
	 public static final Integer VERSION_KEYBOARD;
	 public static final Integer VERSION_MCU;
	 /* # direct methods */
	 private com.android.server.input.padkeyboard.usb.UsbKeyboardUtil ( ) {
		 /* .locals 0 */
		 /* .line 62 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 63 */
		 return;
	 } // .end method
	 public static commandGetBootInfo ( ) {
		 /* .locals 5 */
		 /* .line 94 */
		 /* const/16 v0, 0x8 */
		 /* new-array v0, v0, [B */
		 /* .line 95 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 96 */
		 /* const/16 v1, 0x30 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* aput-byte v1, v0, v3 */
		 /* .line 97 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 98 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x18 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 99 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* const/16 v4, 0x12 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 100 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v3, v0, v1 */
		 /* .line 101 */
		 int v1 = 6; // const/4 v1, 0x6
		 /* aput-byte v2, v0, v1 */
		 /* .line 102 */
		 int v1 = 7; // const/4 v1, 0x7
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 103 */
	 } // .end method
	 public static commandGetConnectState ( ) {
		 /* .locals 5 */
		 /* .line 67 */
		 /* const/16 v0, 0x8 */
		 /* new-array v0, v0, [B */
		 /* .line 68 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 69 */
		 /* const/16 v1, 0x31 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* aput-byte v1, v0, v3 */
		 /* .line 70 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 71 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x38 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 72 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* const/16 v4, -0x5f */
		 /* aput-byte v4, v0, v1 */
		 /* .line 73 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v3, v0, v1 */
		 /* .line 74 */
		 int v1 = 6; // const/4 v1, 0x6
		 /* aput-byte v3, v0, v1 */
		 /* .line 75 */
		 int v1 = 7; // const/4 v1, 0x7
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 76 */
	 } // .end method
	 public static commandGetKeyboardStatus ( ) {
		 /* .locals 4 */
		 /* .line 124 */
		 int v0 = 7; // const/4 v0, 0x7
		 /* new-array v0, v0, [B */
		 /* .line 125 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 126 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* const/16 v3, 0x31 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 127 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v3, -0x80 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 128 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v3, 0x38 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 129 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* const/16 v3, 0x52 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 130 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v2, v0, v1 */
		 /* .line 131 */
		 int v1 = 6; // const/4 v1, 0x6
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 132 */
	 } // .end method
	 public static commandGetKeyboardStatus ( Integer p0 ) {
		 /* .locals 5 */
		 /* .param p0, "target" # I */
		 /* .line 136 */
		 /* const/16 v0, 0x8 */
		 /* new-array v0, v0, [B */
		 /* .line 137 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 138 */
		 /* const/16 v1, 0x31 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* aput-byte v1, v0, v3 */
		 /* .line 139 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 140 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x38 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 141 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* const/16 v4, 0x30 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 142 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v3, v0, v1 */
		 /* .line 143 */
		 int v1 = 6; // const/4 v1, 0x6
		 /* int-to-byte v3, p0 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 144 */
		 int v1 = 7; // const/4 v1, 0x7
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 145 */
	 } // .end method
	 public static commandGetKeyboardVersion ( ) {
		 /* .locals 5 */
		 /* .line 162 */
		 /* const/16 v0, 0x40 */
		 /* new-array v0, v0, [B */
		 /* .line 163 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 164 */
		 /* const/16 v1, 0x30 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* aput-byte v1, v0, v3 */
		 /* .line 165 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 166 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x38 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 167 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* aput-byte v3, v0, v1 */
		 /* .line 168 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v3, v0, v1 */
		 /* .line 169 */
		 int v1 = 6; // const/4 v1, 0x6
		 /* aput-byte v3, v0, v1 */
		 /* .line 170 */
		 int v1 = 7; // const/4 v1, 0x7
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 171 */
	 } // .end method
	 public static commandGetResetInfo ( ) {
		 /* .locals 6 */
		 /* .line 108 */
		 /* const/16 v0, 0xb */
		 /* new-array v0, v0, [B */
		 /* .line 109 */
		 /* .local v0, "bytes":[B */
		 int v1 = 0; // const/4 v1, 0x0
		 /* const/16 v2, 0x4e */
		 /* aput-byte v2, v0, v1 */
		 /* .line 110 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* const/16 v4, 0x30 */
		 /* aput-byte v4, v0, v3 */
		 /* .line 111 */
		 int v3 = 2; // const/4 v3, 0x2
		 /* const/16 v5, -0x80 */
		 /* aput-byte v5, v0, v3 */
		 /* .line 112 */
		 /* const/16 v3, 0x18 */
		 int v5 = 3; // const/4 v5, 0x3
		 /* aput-byte v3, v0, v5 */
		 /* .line 113 */
		 int v3 = 4; // const/4 v3, 0x4
		 /* aput-byte v5, v0, v3 */
		 /* .line 114 */
		 int v5 = 5; // const/4 v5, 0x5
		 /* aput-byte v3, v0, v5 */
		 /* .line 115 */
		 int v3 = 6; // const/4 v3, 0x6
		 /* const/16 v5, 0x57 */
		 /* aput-byte v5, v0, v3 */
		 /* .line 116 */
		 int v3 = 7; // const/4 v3, 0x7
		 /* aput-byte v2, v0, v3 */
		 /* .line 117 */
		 /* const/16 v2, 0x8 */
		 /* const/16 v3, 0x38 */
		 /* aput-byte v3, v0, v2 */
		 /* .line 118 */
		 /* const/16 v2, 0x9 */
		 /* aput-byte v4, v0, v2 */
		 /* .line 119 */
		 /* const/16 v2, 0xa */
		 v1 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v1,v2 );
		 /* aput-byte v1, v0, v2 */
		 /* .line 120 */
	 } // .end method
	 public static commandGetVersionInfo ( ) {
		 /* .locals 5 */
		 /* .line 81 */
		 int v0 = 7; // const/4 v0, 0x7
		 /* new-array v0, v0, [B */
		 /* .line 82 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 83 */
		 /* const/16 v1, 0x30 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* aput-byte v1, v0, v3 */
		 /* .line 84 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 85 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x18 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 86 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* aput-byte v3, v0, v1 */
		 /* .line 87 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v2, v0, v1 */
		 /* .line 88 */
		 int v1 = 6; // const/4 v1, 0x6
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 89 */
	 } // .end method
	 public static commandMiAuthStep3Type1 ( Object[] p0, Object[] p1 ) {
		 /* .locals 5 */
		 /* .param p0, "keyMeta" # [B */
		 /* .param p1, "challenge" # [B */
		 /* .line 193 */
		 /* const/16 v0, 0x1b */
		 /* new-array v0, v0, [B */
		 /* .line 194 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4f */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 195 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* const/16 v3, 0x31 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 196 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v3, -0x80 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 197 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v3, 0x38 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 198 */
		 /* const/16 v1, 0x32 */
		 int v3 = 4; // const/4 v3, 0x4
		 /* aput-byte v1, v0, v3 */
		 /* .line 199 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* const/16 v4, 0x14 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 200 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 /* array-length v1, p0 */
			 /* if-ne v1, v3, :cond_0 */
			 /* .line 201 */
			 int v1 = 6; // const/4 v1, 0x6
			 java.lang.System .arraycopy ( p0,v2,v0,v1,v3 );
			 /* .line 203 */
		 } // :cond_0
		 if ( p1 != null) { // if-eqz p1, :cond_1
			 /* array-length v1, p1 */
			 /* const/16 v3, 0x10 */
			 /* if-ne v1, v3, :cond_1 */
			 /* .line 204 */
			 /* const/16 v1, 0xa */
			 java.lang.System .arraycopy ( p1,v2,v0,v1,v3 );
			 /* .line 206 */
		 } // :cond_1
		 /* const/16 v1, 0x1a */
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 207 */
	 } // .end method
	 public static commandMiDevAuthInit ( ) {
		 /* .locals 5 */
		 /* .line 175 */
		 /* const/16 v0, 0xd */
		 /* new-array v0, v0, [B */
		 /* .line 176 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4f */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 177 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* const/16 v3, 0x31 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 178 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 179 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x38 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 180 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* aput-byte v3, v0, v1 */
		 /* .line 181 */
		 int v1 = 5; // const/4 v1, 0x5
		 int v3 = 6; // const/4 v3, 0x6
		 /* aput-byte v3, v0, v1 */
		 /* .line 182 */
		 /* const/16 v1, 0x4d */
		 /* aput-byte v1, v0, v3 */
		 /* .line 183 */
		 int v1 = 7; // const/4 v1, 0x7
		 /* const/16 v3, 0x49 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 184 */
		 /* const/16 v1, 0x8 */
		 /* const/16 v3, 0x41 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 185 */
		 /* const/16 v1, 0x9 */
		 /* const/16 v3, 0x55 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 186 */
		 /* const/16 v1, 0xa */
		 /* const/16 v3, 0x54 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 187 */
		 /* const/16 v1, 0xb */
		 /* const/16 v3, 0x48 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 188 */
		 /* const/16 v1, 0xc */
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 189 */
	 } // .end method
	 public static commandWriteKeyboardStatus ( Integer p0, Integer p1 ) {
		 /* .locals 5 */
		 /* .param p0, "target" # I */
		 /* .param p1, "value" # I */
		 /* .line 149 */
		 /* const/16 v0, 0x8 */
		 /* new-array v0, v0, [B */
		 /* .line 150 */
		 /* .local v0, "bytes":[B */
		 /* const/16 v1, 0x4e */
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-byte v1, v0, v2 */
		 /* .line 151 */
		 /* const/16 v1, 0x31 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* aput-byte v1, v0, v3 */
		 /* .line 152 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* const/16 v4, -0x80 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 153 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* const/16 v4, 0x38 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 154 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* int-to-byte v4, p0 */
		 /* aput-byte v4, v0, v1 */
		 /* .line 155 */
		 int v1 = 5; // const/4 v1, 0x5
		 /* aput-byte v3, v0, v1 */
		 /* .line 156 */
		 int v1 = 6; // const/4 v1, 0x6
		 /* int-to-byte v3, p1 */
		 /* aput-byte v3, v0, v1 */
		 /* .line 157 */
		 int v1 = 7; // const/4 v1, 0x7
		 v2 = 		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
		 /* aput-byte v2, v0, v1 */
		 /* .line 158 */
	 } // .end method
