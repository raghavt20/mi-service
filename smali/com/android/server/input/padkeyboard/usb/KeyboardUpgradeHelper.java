public class com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper extends com.android.server.input.padkeyboard.usb.UpgradeHelper {
	 /* .source "KeyboardUpgradeHelper.java" */
	 /* # static fields */
	 public static java.lang.String FIRST_LOW_KB_TYPE;
	 public static java.lang.String KB_BIN_PATH;
	 public static java.lang.String KB_H_BIN_PATH;
	 public static java.util.Map KB_L_BIN_PATH_MAP;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public static java.lang.String WL_BIN_PATH;
/* # instance fields */
protected mCheckSumByte;
protected mLengthByte;
protected mStartAddressByte;
protected mVersionByte;
/* # direct methods */
static com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ( ) {
/* .locals 3 */
/* .line 24 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 25 */
final String v1 = "a"; // const-string v1, "a"
/* .line 26 */
final String v1 = "/vendor/etc/XM_KB.bin"; // const-string v1, "/vendor/etc/XM_KB.bin"
/* .line 27 */
final String v1 = "/vendor/etc/XM_KB_H.bin"; // const-string v1, "/vendor/etc/XM_KB_H.bin"
/* .line 28 */
final String v1 = "/vendor/etc/XM_KB_WL.bin"; // const-string v1, "/vendor/etc/XM_KB_WL.bin"
/* .line 36 */
final String v1 = "20"; // const-string v1, "20"
final String v2 = "/vendor/etc/XM_KB_L.bin"; // const-string v2, "/vendor/etc/XM_KB_L.bin"
/* .line 37 */
v0 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.KB_L_BIN_PATH_MAP;
final String v1 = "4a"; // const-string v1, "4a"
final String v2 = "/vendor/etc/XM_KB_L_A.bin"; // const-string v2, "/vendor/etc/XM_KB_L_A.bin"
/* .line 38 */
v0 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.KB_L_BIN_PATH_MAP;
final String v1 = "4b"; // const-string v1, "4b"
final String v2 = "/vendor/etc/XM_KB_L_B.bin"; // const-string v2, "/vendor/etc/XM_KB_L_B.bin"
/* .line 39 */
v0 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.KB_L_BIN_PATH_MAP;
final String v1 = "4c"; // const-string v1, "4c"
final String v2 = "/vendor/etc/XM_KB_L_C.bin"; // const-string v2, "/vendor/etc/XM_KB_L_C.bin"
/* .line 40 */
return;
} // .end method
public com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "binPath" # Ljava/lang/String; */
/* .line 43 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/usb/UpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 30 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v1, v0, [B */
this.mLengthByte = v1;
/* .line 31 */
/* new-array v0, v0, [B */
this.mStartAddressByte = v0;
/* .line 32 */
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [B */
this.mCheckSumByte = v0;
/* .line 33 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [B */
this.mVersionByte = v0;
/* .line 44 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).parseUpgradeFileHead ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->parseUpgradeFileHead()V
/* .line 45 */
return;
} // .end method
private Boolean parseUpEndCommand ( ) {
/* .locals 5 */
/* .line 325 */
v0 = this.mRecBuf;
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v0, v0, v1 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
/* if-ne v0, v2, :cond_0 */
/* .line 326 */
final String v0 = "receive up end command state need wait"; // const-string v0, "receive up end command state need wait"
android.util.Slog .i ( v4,v0 );
/* .line 327 */
/* .line 329 */
} // :cond_0
v0 = this.mRecBuf;
/* aget-byte v0, v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 330 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive up end command state error:"; // const-string v2, "receive up end command state error:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mRecBuf;
/* aget-byte v1, v2, v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v0 );
/* .line 331 */
/* .line 333 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendResetCommand()Z */
} // .end method
private Boolean parseUpgradeCommand ( ) {
/* .locals 5 */
/* .line 256 */
v0 = this.mRecBuf;
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v0, v0, v1 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 257 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive upgrade command state error:"; // const-string v4, "receive upgrade command state error:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mRecBuf;
/* aget-byte v1, v4, v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 258 */
/* .line 260 */
} // :cond_0
v0 = this.mRecBuf;
int v4 = 7; // const/4 v4, 0x7
/* aget-byte v0, v0, v4 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 261 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive upgrade command mode error:"; // const-string v4, "receive upgrade command mode error:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mRecBuf;
/* aget-byte v1, v4, v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 262 */
/* .line 264 */
} // :cond_1
v0 = this.mRecBuf;
v1 = this.mRecBuf;
/* const/16 v4, 0x8 */
/* aget-byte v1, v1, v4 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( v0,v3,v4,v1 );
/* if-nez v0, :cond_2 */
/* .line 265 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive upgrade command checkSum error:"; // const-string v1, "receive upgrade command checkSum error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRecBuf;
/* aget-byte v1, v1, v4 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 266 */
/* .line 268 */
} // :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendBinData()Z */
} // .end method
private Boolean sendBinData ( ) {
/* .locals 5 */
/* .line 272 */
/* const/16 v0, 0x40 */
v0 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).getBinPacketNum ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getBinPacketNum(I)I
/* .line 273 */
/* .local v0, "packetNum":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send bin data, packet num: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 274 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "num":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 275 */
v3 = /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendOnePacket(I)Z */
/* if-nez v3, :cond_0 */
/* .line 276 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "send bin data " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " failed"; // const-string v4, " failed"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 277 */
int v2 = 0; // const/4 v2, 0x0
/* .line 274 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 280 */
} // .end local v1 # "num":I
} // :cond_1
v1 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUpEndCommand()Z */
} // .end method
private Boolean sendOnePacket ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "num" # I */
/* .line 284 */
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 285 */
/* mul-int/lit8 v0, p1, 0x34 */
/* const/16 v2, 0x40 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).getBinPackInfo ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getBinPackInfo(II)[B
v3 = this.mSendBuf;
java.lang.System .arraycopy ( v0,v1,v3,v1,v2 );
/* .line 286 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v2 = 3; // const/4 v2, 0x3
/* if-ge v0, v2, :cond_3 */
/* .line 287 */
v2 = this.mUsbConnection;
v3 = this.mOutUsbEndpoint;
v4 = this.mSendBuf;
v2 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 288 */
v2 = this.mRecBuf;
java.util.Arrays .fill ( v2,v1 );
/* .line 289 */
} // :cond_0
v2 = this.mUsbConnection;
v3 = this.mInUsbEndpoint;
v4 = this.mRecBuf;
v2 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 290 */
v2 = this.mRecBuf;
int v3 = 4; // const/4 v3, 0x4
/* aget-byte v2, v2, v3 */
/* const/16 v4, -0x6f */
/* if-eq v2, v4, :cond_1 */
v2 = this.mRecBuf;
/* aget-byte v2, v2, v3 */
/* const/16 v3, 0x11 */
/* if-ne v2, v3, :cond_0 */
} // :cond_1
v2 = this.mRecBuf;
int v3 = 6; // const/4 v3, 0x6
/* aget-byte v2, v2, v3 */
/* if-nez v2, :cond_0 */
/* .line 292 */
int v1 = 1; // const/4 v1, 0x1
/* .line 296 */
} // :cond_2
/* const/16 v2, 0xc8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v2 );
/* .line 286 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 298 */
} // .end local v0 # "i":I
} // :cond_3
} // .end method
private Boolean sendResetCommand ( ) {
/* .locals 9 */
/* .line 338 */
/* const-string/jumbo v0, "send reset command" */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 339 */
v0 = this.mSendBuf;
int v2 = 0; // const/4 v2, 0x0
java.util.Arrays .fill ( v0,v2 );
/* .line 340 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).getResetCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getResetCommand()[B
/* .line 341 */
/* .local v0, "command":[B */
v3 = this.mSendBuf;
/* array-length v4, v0 */
java.lang.System .arraycopy ( v0,v2,v3,v2,v4 );
/* .line 342 */
int v3 = 0; // const/4 v3, 0x0
/* .line 343 */
/* .local v3, "resetSent":Z */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
int v5 = 3; // const/4 v5, 0x3
/* if-ge v4, v5, :cond_5 */
/* .line 344 */
v5 = this.mUsbConnection;
v6 = this.mOutUsbEndpoint;
v7 = this.mSendBuf;
v5 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v5, v6, v7 ); // invoke-virtual {p0, v5, v6, v7}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
int v6 = 1; // const/4 v6, 0x1
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 345 */
int v3 = 1; // const/4 v3, 0x1
/* .line 346 */
v5 = this.mRecBuf;
java.util.Arrays .fill ( v5,v2 );
/* .line 347 */
} // :goto_1
v5 = this.mUsbConnection;
v7 = this.mInUsbEndpoint;
v8 = this.mRecBuf;
v5 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v5, v7, v8 ); // invoke-virtual {p0, v5, v7, v8}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 348 */
v5 = this.mRecBuf;
int v7 = 6; // const/4 v7, 0x6
/* aget-byte v5, v5, v7 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 349 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "receive reset command state error:"; // const-string v8, "receive reset command state error:"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mRecBuf;
/* aget-byte v7, v8, v7 */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
/* .line 350 */
/* .line 352 */
} // :cond_0
v5 = this.mRecBuf;
v7 = this.mRecBuf;
int v8 = 7; // const/4 v8, 0x7
/* aget-byte v7, v7, v8 */
v5 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( v5,v2,v8,v7 );
/* if-nez v5, :cond_1 */
/* .line 353 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "receive reset command checkSum error:"; // const-string v7, "receive reset command checkSum error:"
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mRecBuf;
/* aget-byte v7, v7, v8 */
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
/* .line 354 */
/* .line 356 */
} // :cond_1
final String v2 = "receive reset command"; // const-string v2, "receive reset command"
android.util.Slog .i ( v1,v2 );
/* .line 357 */
/* .line 360 */
} // :cond_2
/* const-string/jumbo v5, "send reset command failed" */
android.util.Slog .i ( v1,v5 );
/* .line 362 */
} // :cond_3
v5 = this.mUsbDevice;
/* if-nez v5, :cond_4 */
/* .line 363 */
/* .line 365 */
} // :cond_4
/* const/16 v5, 0x3e8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v5 );
/* .line 343 */
/* add-int/lit8 v4, v4, 0x1 */
/* goto/16 :goto_0 */
/* .line 368 */
} // .end local v4 # "i":I
} // :cond_5
} // .end method
private Boolean sendUpEndCommand ( ) {
/* .locals 6 */
/* .line 302 */
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 303 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).getUpEndCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getUpEndCommand()[B
/* .line 304 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 305 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 3; // const/4 v3, 0x3
/* if-ge v2, v3, :cond_4 */
/* .line 306 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 307 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 308 */
} // :cond_0
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 309 */
v3 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->parseUpEndCommand()Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 310 */
int v1 = 1; // const/4 v1, 0x1
/* .line 314 */
} // :cond_1
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send up end command failed" */
android.util.Slog .i ( v3,v4 );
/* .line 316 */
} // :cond_2
v3 = this.mUsbDevice;
/* if-nez v3, :cond_3 */
/* .line 317 */
/* .line 319 */
} // :cond_3
/* const/16 v3, 0x1f4 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v3 );
/* .line 305 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 321 */
} // .end local v2 # "i":I
} // :cond_4
} // .end method
private Boolean sendUpgradeCommand ( ) {
/* .locals 7 */
/* .line 232 */
/* const-string/jumbo v0, "send upgrade command" */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 233 */
v0 = this.mSendBuf;
int v2 = 0; // const/4 v2, 0x0
java.util.Arrays .fill ( v0,v2 );
/* .line 234 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).getUpgradeCommand ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getUpgradeCommand()[B
/* .line 235 */
/* .local v0, "command":[B */
v3 = this.mSendBuf;
/* array-length v4, v0 */
java.lang.System .arraycopy ( v0,v2,v3,v2,v4 );
/* .line 236 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
int v4 = 3; // const/4 v4, 0x3
/* if-ge v3, v4, :cond_4 */
/* .line 237 */
v4 = this.mUsbConnection;
v5 = this.mOutUsbEndpoint;
v6 = this.mSendBuf;
v4 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v4, v5, v6 ); // invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 238 */
v4 = this.mRecBuf;
java.util.Arrays .fill ( v4,v2 );
/* .line 239 */
} // :cond_0
v4 = this.mUsbConnection;
v5 = this.mInUsbEndpoint;
v6 = this.mRecBuf;
v4 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) p0 ).sendUsbData ( v4, v5, v6 ); // invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 240 */
v4 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->parseUpgradeCommand()Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 241 */
int v1 = 1; // const/4 v1, 0x1
/* .line 245 */
} // :cond_1
/* const-string/jumbo v4, "send upgrade command failed" */
android.util.Slog .i ( v1,v4 );
/* .line 247 */
} // :cond_2
/* const/16 v4, 0x1f4 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v4 );
/* .line 248 */
v4 = this.mUsbDevice;
/* if-nez v4, :cond_3 */
/* .line 249 */
/* .line 236 */
} // :cond_3
/* add-int/lit8 v3, v3, 0x1 */
/* .line 252 */
} // .end local v3 # "i":I
} // :cond_4
} // .end method
/* # virtual methods */
public getBinPackInfo ( Integer p0, Integer p1 ) {
/* .locals 20 */
/* .param p1, "offset" # I */
/* .param p2, "packetLength" # I */
/* .line 112 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
/* move/from16 v2, p2 */
/* const/16 v3, 0x20 */
/* new-array v4, v3, [B */
/* .line 113 */
/* .local v4, "bytes":[B */
int v5 = 0; // const/4 v5, 0x0
/* .line 114 */
/* .local v5, "binlength":I */
int v7 = 7; // const/4 v7, 0x7
int v8 = 6; // const/4 v8, 0x6
/* const/16 v9, 0x11 */
/* const/16 v10, -0x80 */
/* const/16 v11, 0x30 */
/* const/16 v12, 0x4f */
int v13 = 5; // const/4 v13, 0x5
int v14 = 4; // const/4 v14, 0x4
/* const/16 v15, 0x38 */
/* const/16 v16, 0x9 */
/* const/16 v17, 0x2 */
/* const/16 v18, 0x1 */
/* const/16 v19, 0x3 */
int v6 = 0; // const/4 v6, 0x0
/* if-ne v2, v3, :cond_1 */
/* .line 115 */
/* new-array v4, v3, [B */
/* .line 116 */
/* const/16 v3, 0x14 */
/* .line 117 */
} // .end local v5 # "binlength":I
/* .local v3, "binlength":I */
/* aput-byte v12, v4, v6 */
/* .line 118 */
/* aput-byte v11, v4, v18 */
/* .line 119 */
/* aput-byte v10, v4, v17 */
/* .line 120 */
/* aput-byte v15, v4, v19 */
/* .line 121 */
/* aput-byte v9, v4, v14 */
/* .line 122 */
/* const/16 v5, 0x1d */
/* aput-byte v5, v4, v13 */
/* .line 123 */
/* add-int/lit16 v5, v1, 0x1000 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( v5 );
/* .line 124 */
/* .local v5, "offsetB":[B */
/* aget-byte v9, v5, v19 */
/* aput-byte v9, v4, v8 */
/* .line 125 */
/* aget-byte v8, v5, v17 */
/* aput-byte v8, v4, v7 */
/* .line 126 */
/* aget-byte v7, v5, v18 */
/* const/16 v8, 0x8 */
/* aput-byte v7, v4, v8 */
/* .line 128 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* add-int v8, v1, v3 */
/* if-ge v7, v8, :cond_0 */
/* .line 129 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* sub-int/2addr v7, v1 */
/* .line 130 */
} // .end local v3 # "binlength":I
/* .local v7, "binlength":I */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( v7 );
/* aget-byte v3, v3, v19 */
/* aput-byte v3, v4, v16 */
/* move v3, v7 */
/* .line 132 */
} // .end local v7 # "binlength":I
/* .restart local v3 # "binlength":I */
} // :cond_0
/* const/16 v7, 0x14 */
/* aput-byte v7, v4, v16 */
/* .line 134 */
} // :goto_0
v7 = this.mFileBuf;
/* const/16 v8, 0xa */
java.lang.System .arraycopy ( v7,v1,v4,v8,v3 );
/* .line 136 */
/* const/16 v7, 0x1f */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v4,v6,v7 );
/* aput-byte v6, v4, v7 */
/* .line 137 */
} // .end local v5 # "offsetB":[B
/* move v5, v3 */
} // .end local v3 # "binlength":I
/* .local v5, "binlength":I */
} // :cond_1
/* const/16 v3, 0x40 */
/* if-ne v2, v3, :cond_3 */
/* .line 138 */
/* new-array v4, v3, [B */
/* .line 139 */
/* const/16 v3, 0x34 */
/* .line 140 */
} // .end local v5 # "binlength":I
/* .restart local v3 # "binlength":I */
/* aput-byte v12, v4, v6 */
/* .line 141 */
/* aput-byte v11, v4, v18 */
/* .line 142 */
/* aput-byte v10, v4, v17 */
/* .line 143 */
/* aput-byte v15, v4, v19 */
/* .line 144 */
/* aput-byte v9, v4, v14 */
/* .line 145 */
/* aput-byte v15, v4, v13 */
/* .line 146 */
/* add-int/lit16 v5, v1, 0x1000 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( v5 );
/* .line 147 */
/* .local v5, "offsetB":[B */
/* aget-byte v9, v5, v19 */
/* aput-byte v9, v4, v8 */
/* .line 148 */
/* aget-byte v8, v5, v17 */
/* aput-byte v8, v4, v7 */
/* .line 149 */
/* aget-byte v7, v5, v18 */
/* const/16 v8, 0x8 */
/* aput-byte v7, v4, v8 */
/* .line 150 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* add-int v8, v1, v3 */
/* if-ge v7, v8, :cond_2 */
/* .line 151 */
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* sub-int v3, v7, v1 */
/* .line 152 */
/* add-int/lit8 v7, v3, 0x4 */
/* int-to-byte v7, v7 */
/* aput-byte v7, v4, v13 */
/* .line 153 */
/* int-to-byte v7, v3 */
/* aput-byte v7, v4, v16 */
/* .line 154 */
v7 = this.mFileBuf;
/* const/16 v8, 0xa */
java.lang.System .arraycopy ( v7,v1,v4,v8,v3 );
/* .line 155 */
/* add-int/lit8 v7, v3, 0xa */
/* add-int/lit8 v8, v3, 0xa */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v4,v6,v8 );
/* aput-byte v6, v4, v7 */
/* move v5, v3 */
/* .line 157 */
} // :cond_2
/* const/16 v3, 0x34 */
/* .line 158 */
/* const/16 v7, 0x34 */
/* aput-byte v7, v4, v16 */
/* .line 159 */
v7 = this.mFileBuf;
/* const/16 v8, 0xa */
java.lang.System .arraycopy ( v7,v1,v4,v8,v3 );
/* .line 160 */
/* const/16 v7, 0x3e */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v4,v6,v7 );
/* aput-byte v6, v4, v7 */
/* move v5, v3 */
/* .line 164 */
} // .end local v3 # "binlength":I
/* .local v5, "binlength":I */
} // :cond_3
} // :goto_1
} // .end method
public Integer getBinPacketNum ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "length" # I */
/* .line 200 */
int v0 = 0; // const/4 v0, 0x0
/* .line 201 */
/* .local v0, "num":I */
/* const/16 v1, 0x20 */
/* if-ne p1, v1, :cond_0 */
/* .line 202 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mBinLength:I */
/* int-to-double v1, v1 */
/* const-wide/high16 v3, 0x4034000000000000L # 20.0 */
/* div-double/2addr v1, v3 */
java.lang.Math .ceil ( v1,v2 );
/* move-result-wide v1 */
/* double-to-int v0, v1 */
/* .line 203 */
} // :cond_0
/* const/16 v1, 0x40 */
/* if-ne p1, v1, :cond_1 */
/* .line 204 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mBinLength:I */
/* int-to-double v1, v1 */
/* const-wide/high16 v3, 0x404a000000000000L # 52.0 */
/* div-double/2addr v1, v3 */
java.lang.Math .ceil ( v1,v2 );
/* move-result-wide v1 */
/* double-to-int v0, v1 */
/* .line 206 */
} // :cond_1
} // :goto_0
} // .end method
public getResetCommand ( ) {
/* .locals 5 */
/* .line 186 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 187 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 188 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x30 */
/* aput-byte v3, v0, v1 */
/* .line 189 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, -0x80 */
/* aput-byte v3, v0, v1 */
/* .line 190 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v3, 0x38 */
/* aput-byte v3, v0, v1 */
/* .line 191 */
int v1 = 4; // const/4 v1, 0x4
int v3 = 6; // const/4 v3, 0x6
/* aput-byte v3, v0, v1 */
/* .line 192 */
int v4 = 5; // const/4 v4, 0x5
/* aput-byte v1, v0, v4 */
/* .line 193 */
v4 = this.mStartAddressByte;
java.lang.System .arraycopy ( v4,v2,v0,v3,v1 );
/* .line 194 */
/* const/16 v1, 0xa */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 195 */
} // .end method
public getUpEndCommand ( ) {
/* .locals 7 */
/* .line 169 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 170 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 171 */
/* const/16 v1, 0x30 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v1, v0, v3 */
/* .line 172 */
/* const/16 v1, -0x80 */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v1, v0, v4 */
/* .line 173 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v5, 0x38 */
/* aput-byte v5, v0, v1 */
/* .line 174 */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v1, v0, v1 */
/* .line 175 */
int v5 = 5; // const/4 v5, 0x5
/* const/16 v6, 0xb */
/* aput-byte v6, v0, v5 */
/* .line 176 */
v5 = this.mLengthByte;
int v6 = 6; // const/4 v6, 0x6
java.lang.System .arraycopy ( v5,v2,v0,v6,v1 );
/* .line 177 */
v5 = this.mStartAddressByte;
/* const/16 v6, 0xa */
java.lang.System .arraycopy ( v5,v2,v0,v6,v1 );
/* .line 178 */
v1 = this.mCheckSumByte;
/* const/16 v5, 0xe */
java.lang.System .arraycopy ( v1,v2,v0,v5,v3 );
/* .line 179 */
v1 = this.mVersionByte;
/* const/16 v3, 0xf */
java.lang.System .arraycopy ( v1,v2,v0,v3,v4 );
/* .line 180 */
/* const/16 v1, 0x11 */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 181 */
} // .end method
public getUpgradeCommand ( ) {
/* .locals 7 */
/* .line 95 */
/* const/16 v0, 0x20 */
/* new-array v0, v0, [B */
/* .line 96 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 97 */
/* const/16 v1, 0x30 */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v1, v0, v3 */
/* .line 98 */
/* const/16 v1, -0x80 */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v1, v0, v4 */
/* .line 99 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v5, 0x38 */
/* aput-byte v5, v0, v1 */
/* .line 100 */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v4, v0, v1 */
/* .line 101 */
int v5 = 5; // const/4 v5, 0x5
/* const/16 v6, 0xb */
/* aput-byte v6, v0, v5 */
/* .line 102 */
v5 = this.mLengthByte;
int v6 = 6; // const/4 v6, 0x6
java.lang.System .arraycopy ( v5,v2,v0,v6,v1 );
/* .line 103 */
v5 = this.mStartAddressByte;
/* const/16 v6, 0xa */
java.lang.System .arraycopy ( v5,v2,v0,v6,v1 );
/* .line 104 */
v1 = this.mCheckSumByte;
/* const/16 v5, 0xe */
java.lang.System .arraycopy ( v1,v2,v0,v5,v3 );
/* .line 105 */
v1 = this.mVersionByte;
/* const/16 v3, 0xf */
java.lang.System .arraycopy ( v1,v2,v0,v3,v4 );
/* .line 106 */
/* const/16 v1, 0x11 */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 107 */
} // .end method
protected void parseUpgradeFileHead ( ) {
/* .locals 10 */
/* .line 49 */
v0 = this.mFileBuf;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mFileBuf;
/* array-length v0, v0 */
/* const/16 v1, 0x40 */
/* if-le v0, v1, :cond_2 */
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
/* .local v0, "headAddress":I */
v1 = this.mFileBuf;
v2 = this.mLengthByte;
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
java.lang.System .arraycopy ( v1,v0,v2,v4,v3 );
/* .line 53 */
v1 = this.mLengthByte;
int v2 = 1; // const/4 v2, 0x1
/* aget-byte v3, v1, v2 */
/* shl-int/lit8 v3, v3, 0x8 */
/* const v5, 0xff00 */
/* and-int/2addr v3, v5 */
/* aget-byte v1, v1, v4 */
/* and-int/lit16 v1, v1, 0xff */
/* add-int/2addr v3, v1 */
/* iput v3, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mBinLength:I */
/* .line 54 */
v1 = this.mLengthByte;
/* array-length v1, v1 */
/* add-int/2addr v0, v1 */
/* .line 57 */
v1 = this.mFileBuf;
v3 = this.mStartAddressByte;
/* array-length v5, v3 */
java.lang.System .arraycopy ( v1,v0,v3,v4,v5 );
/* .line 58 */
v1 = this.mStartAddressByte;
/* array-length v1, v1 */
/* add-int/2addr v0, v1 */
/* .line 61 */
v1 = this.mFileBuf;
v3 = this.mCheckSumByte;
/* array-length v5, v3 */
java.lang.System .arraycopy ( v1,v0,v3,v4,v5 );
/* .line 62 */
v1 = this.mCheckSumByte;
/* aget-byte v3, v1, v4 */
/* and-int/lit16 v3, v3, 0x80 */
final String v5 = ""; // const-string v5, ""
/* const/16 v6, 0x10 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 63 */
/* new-instance v1, Ljava/math/BigInteger; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "FFFFFF"; // const-string v7, "FFFFFF"
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mCheckSumByte;
/* array-length v8, v7 */
/* .line 64 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2HexString ( v7,v8,v5 );
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v3, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V */
/* .line 65 */
v1 = (( java.math.BigInteger ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/math/BigInteger;->intValue()I
/* iput v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSum:I */
/* .line 67 */
} // :cond_0
/* array-length v3, v1 */
/* .line 68 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2HexString ( v1,v3,v5 );
/* .line 67 */
v1 = java.lang.Integer .parseInt ( v1,v6 );
/* iput v1, p0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->mCheckSum:I */
/* .line 70 */
} // :goto_0
v1 = this.mCheckSumByte;
/* array-length v1, v1 */
/* add-int/2addr v0, v1 */
/* .line 73 */
v1 = this.mFileBuf;
v3 = this.mVersionByte;
/* array-length v7, v3 */
java.lang.System .arraycopy ( v1,v0,v3,v4,v7 );
/* .line 74 */
v1 = this.mVersionByte;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v1 );
this.mVersion = v1;
/* .line 75 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Bin Version : "; // const-string v3, "Bin Version : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mVersion;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
android.util.Slog .i ( v3,v1 );
/* .line 78 */
v1 = this.mFileBuf;
v7 = this.mFileBuf;
/* array-length v7, v7 */
/* sub-int/2addr v7, v6 */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v1,v6,v7 );
/* .line 79 */
/* .local v1, "sum2":I */
/* not-int v7, v1 */
/* add-int/2addr v7, v2 */
/* .line 80 */
/* .local v7, "sum":I */
java.lang.Integer .toHexString ( v7 );
/* .line 81 */
/* .local v2, "hexSum":Ljava/lang/String; */
v8 = this.mCheckSumByte;
/* array-length v9, v8 */
/* .line 82 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2HexString ( v8,v9,v5 );
/* .line 81 */
v5 = (( java.lang.String ) v2 ).contains ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v5, :cond_1 */
/* .line 83 */
int v4 = 0; // const/4 v4, 0x0
this.mFileBuf = v4;
/* .line 84 */
final String v4 = "check sum error"; // const-string v4, "check sum error"
android.util.Slog .i ( v3,v4 );
/* .line 86 */
} // :cond_1
v3 = this.mFileBuf;
/* array-length v3, v3 */
/* sub-int/2addr v3, v6 */
/* new-array v3, v3, [B */
/* .line 87 */
/* .local v3, "realData":[B */
v5 = this.mFileBuf;
/* array-length v8, v3 */
java.lang.System .arraycopy ( v5,v6,v3,v4,v8 );
/* .line 88 */
this.mFileBuf = v3;
/* .line 91 */
} // .end local v0 # "headAddress":I
} // .end local v1 # "sum2":I
} // .end local v2 # "hexSum":Ljava/lang/String;
} // .end local v3 # "realData":[B
} // .end local v7 # "sum":I
} // :cond_2
} // :goto_1
return;
} // .end method
public Boolean startUpgrade ( android.hardware.usb.UsbDevice p0, android.hardware.usb.UsbDeviceConnection p1, android.hardware.usb.UsbEndpoint p2, android.hardware.usb.UsbEndpoint p3 ) {
/* .locals 4 */
/* .param p1, "usbDevice" # Landroid/hardware/usb/UsbDevice; */
/* .param p2, "usbConnection" # Landroid/hardware/usb/UsbDeviceConnection; */
/* .param p3, "outUsbEndpoint" # Landroid/hardware/usb/UsbEndpoint; */
/* .param p4, "inUsbEndpoint" # Landroid/hardware/usb/UsbEndpoint; */
/* .line 212 */
/* const-string/jumbo v0, "start keyboard upgrade" */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 213 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
if ( p3 != null) { // if-eqz p3, :cond_3
/* if-nez p4, :cond_0 */
/* .line 218 */
} // :cond_0
this.mUsbDevice = p1;
/* .line 219 */
this.mUsbConnection = p2;
/* .line 220 */
this.mOutUsbEndpoint = p3;
/* .line 221 */
this.mInUsbEndpoint = p4;
/* .line 223 */
v2 = this.mFileBuf;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mFileBuf;
/* array-length v2, v2 */
/* const/16 v3, 0x40 */
/* if-ge v2, v3, :cond_1 */
/* .line 228 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->sendUpgradeCommand()Z */
/* .line 224 */
} // :cond_2
} // :goto_0
/* const-string/jumbo v2, "start upgrade failed : invalid bin file" */
android.util.Slog .i ( v1,v2 );
/* .line 225 */
/* .line 215 */
} // :cond_3
} // :goto_1
/* const-string/jumbo v2, "start upgrade failed : device not ready" */
android.util.Slog .i ( v1,v2 );
/* .line 216 */
} // .end method
