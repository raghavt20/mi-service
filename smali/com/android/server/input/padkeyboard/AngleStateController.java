public class com.android.server.input.padkeyboard.AngleStateController {
	 /* .source "AngleStateController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private com.android.server.input.padkeyboard.AngleStateController$AngleState mCurrentState;
private volatile Boolean mIdentityPass;
private volatile Integer mKbLevel;
private volatile Boolean mLidOpen;
private android.os.PowerManager mPowerManager;
private volatile Boolean mShouldIgnoreKeyboard;
private volatile Boolean mTabletOpen;
/* # direct methods */
public static void $r8$lambda$0jOTacZicYOJ2MAKjiN8mJXY8tM ( com.android.server.input.padkeyboard.AngleStateController p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$1(Z)V */
	 return;
} // .end method
public static void $r8$lambda$0qe9D-eH746VFsR7HJFuMT2FNXo ( com.android.server.input.padkeyboard.AngleStateController p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$3(Z)V */
	 return;
} // .end method
public static void $r8$lambda$VAdZDqhIwWES3NSKBy2Z85T_YFU ( com.android.server.input.padkeyboard.AngleStateController p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$2(Z)V */
	 return;
} // .end method
public static void $r8$lambda$Vzi1FtdAQ9Vdvo457NF5CbBSZ3o ( com.android.server.input.padkeyboard.AngleStateController p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$0(Z)V */
	 return;
} // .end method
public static void $r8$lambda$ipLUQXERU-2Tk3ZLH7W1vhmopVE ( com.android.server.input.padkeyboard.AngleStateController p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$4(Z)V */
	 return;
} // .end method
public static void $r8$lambda$pziw1pCtTyWG5SDFFBNDVd_LHx0 ( com.android.server.input.padkeyboard.AngleStateController p0, Boolean p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$5(Z)V */
	 return;
} // .end method
public com.android.server.input.padkeyboard.AngleStateController ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 198 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 192 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 193 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
	 /* .line 194 */
	 int v1 = 2; // const/4 v1, 0x2
	 /* iput v1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I */
	 /* .line 195 */
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
	 /* .line 196 */
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z */
	 /* .line 199 */
	 this.mContext = p1;
	 /* .line 200 */
	 final String v0 = "power"; // const-string v0, "power"
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/os/PowerManager; */
	 this.mPowerManager = v0;
	 /* .line 201 */
	 /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/AngleStateController;->initState()V */
	 /* .line 202 */
	 return;
} // .end method
private void initCurrentState ( ) {
	 /* .locals 1 */
	 /* .line 229 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.CLOSE_STATE;
	 this.mCurrentState = v0;
	 /* .line 230 */
	 return;
} // .end method
private void initState ( ) {
	 /* .locals 2 */
	 /* .line 236 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.CLOSE_STATE;
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V */
	 (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).setOnChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
	 /* .line 244 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.NO_WORK_STATE_1;
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda1; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V */
	 (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).setOnChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
	 /* .line 252 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.WORK_STATE_1;
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda2; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V */
	 (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).setOnChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
	 /* .line 260 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.WORK_STATE_2;
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda3; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V */
	 (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).setOnChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
	 /* .line 268 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.NO_WORK_STATE_2;
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda4; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V */
	 (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).setOnChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
	 /* .line 276 */
	 v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.BACK_STATE;
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda5; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V */
	 (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).setOnChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
	 /* .line 280 */
	 return;
} // .end method
private void lambda$initState$0 ( Boolean p0 ) { //synthethic
	 /* .locals 2 */
	 /* .param p1, "toUpper" # Z */
	 /* .line 237 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AngleState CLOSE_STATE onchange toUpper: "; // const-string v1, "AngleState CLOSE_STATE onchange toUpper: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 238 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 239 */
	 return;
} // .end method
private void lambda$initState$1 ( Boolean p0 ) { //synthethic
	 /* .locals 2 */
	 /* .param p1, "toUpper" # Z */
	 /* .line 245 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AngleState NO_WORK_STATE_1 onchange toUpper: "; // const-string v1, "AngleState NO_WORK_STATE_1 onchange toUpper: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 246 */
	 /* xor-int/lit8 v0, p1, 0x1 */
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 247 */
	 return;
} // .end method
private void lambda$initState$2 ( Boolean p0 ) { //synthethic
	 /* .locals 2 */
	 /* .param p1, "toUpper" # Z */
	 /* .line 253 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AngleState WORK_STATE_1 onchange toUpper: "; // const-string v1, "AngleState WORK_STATE_1 onchange toUpper: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 254 */
	 /* xor-int/lit8 v0, p1, 0x1 */
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 255 */
	 return;
} // .end method
private void lambda$initState$3 ( Boolean p0 ) { //synthethic
	 /* .locals 2 */
	 /* .param p1, "toUpper" # Z */
	 /* .line 261 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AngleState WORK_STATE_2 onchange toUpper: "; // const-string v1, "AngleState WORK_STATE_2 onchange toUpper: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 262 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 263 */
	 return;
} // .end method
private void lambda$initState$4 ( Boolean p0 ) { //synthethic
	 /* .locals 2 */
	 /* .param p1, "toUpper" # Z */
	 /* .line 269 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AngleState NO_WORK_STATE_2 onchange toUpper: "; // const-string v1, "AngleState NO_WORK_STATE_2 onchange toUpper: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 270 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 271 */
	 return;
} // .end method
private void lambda$initState$5 ( Boolean p0 ) { //synthethic
	 /* .locals 2 */
	 /* .param p1, "toUpper" # Z */
	 /* .line 277 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "AngleState BACK_STATE onchange toUpper: "; // const-string v1, "AngleState BACK_STATE onchange toUpper: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 278 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 279 */
	 return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
	 /* .locals 1 */
	 /* .param p1, "prefix" # Ljava/lang/String; */
	 /* .param p2, "pw" # Ljava/io/PrintWriter; */
	 /* .line 358 */
	 (( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 359 */
	 final String v0 = "mCurrentState="; // const-string v0, "mCurrentState="
	 (( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 360 */
	 v0 = this.mCurrentState;
	 (( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
	 /* .line 362 */
	 (( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 363 */
	 final String v0 = "mIdentityPass="; // const-string v0, "mIdentityPass="
	 (( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 364 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z */
	 (( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
	 /* .line 366 */
	 (( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 367 */
	 final String v0 = "mLidOpen="; // const-string v0, "mLidOpen="
	 (( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 368 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
	 (( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
	 /* .line 370 */
	 (( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 371 */
	 final String v0 = "mTabletOpen="; // const-string v0, "mTabletOpen="
	 (( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 372 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
	 (( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
	 /* .line 374 */
	 (( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 375 */
	 final String v0 = "mShouldIgnoreKeyboard="; // const-string v0, "mShouldIgnoreKeyboard="
	 (( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 376 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 (( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
	 /* .line 377 */
	 return;
} // .end method
public Boolean getIdentityStatus ( ) {
	 /* .locals 1 */
	 /* .line 318 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z */
} // .end method
public Boolean getLidStatus ( ) {
	 /* .locals 1 */
	 /* .line 310 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
} // .end method
public Boolean getTabletStatus ( ) {
	 /* .locals 1 */
	 /* .line 314 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
} // .end method
public Boolean isWorkState ( ) {
	 /* .locals 1 */
	 /* .line 302 */
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public void notifyLidSwitchChanged ( Boolean p0 ) {
	 /* .locals 2 */
	 /* .param p1, "lidOpen" # Z */
	 /* .line 338 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
	 /* .line 339 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "notifyLidSwitchChanged: "; // const-string v1, "notifyLidSwitchChanged: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 340 */
	 return;
} // .end method
public void notifyTabletSwitchChanged ( Boolean p0 ) {
	 /* .locals 2 */
	 /* .param p1, "tabletOpen" # Z */
	 /* .line 343 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
	 /* .line 344 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "notifyTabletSwitchChanged: "; // const-string v1, "notifyTabletSwitchChanged: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 345 */
	 return;
} // .end method
public void resetAngleStatus ( ) {
	 /* .locals 1 */
	 /* .line 353 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 /* .line 354 */
	 return;
} // .end method
public void setIdentityState ( Boolean p0 ) {
	 /* .locals 2 */
	 /* .param p1, "identityPass" # Z */
	 /* .line 348 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "Keyboard authentication status: "; // const-string v1, "Keyboard authentication status: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "AngleStateController"; // const-string v1, "AngleStateController"
	 android.util.Slog .i ( v1,v0 );
	 /* .line 349 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z */
	 /* .line 350 */
	 return;
} // .end method
public void setKbLevel ( Integer p0, Boolean p1 ) {
	 /* .locals 5 */
	 /* .param p1, "kbLevel" # I */
	 /* .param p2, "shouldWakeUp" # Z */
	 /* .line 322 */
	 /* iput p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I */
	 /* .line 323 */
	 if ( p2 != null) { // if-eqz p2, :cond_0
		 /* iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I */
		 int v1 = 2; // const/4 v1, 0x2
		 /* if-ne v0, v1, :cond_0 */
		 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 324 */
				 v0 = this.mPowerManager;
				 android.os.SystemClock .uptimeMillis ( );
				 /* move-result-wide v1 */
				 int v3 = 0; // const/4 v3, 0x0
				 /* const-string/jumbo v4, "usb_keyboard_attach" */
				 (( android.os.PowerManager ) v0 ).wakeUp ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
				 /* .line 327 */
			 } // :cond_0
			 return;
		 } // .end method
		 public void setShouldIgnoreKeyboardFromKeyboard ( Boolean p0 ) {
			 /* .locals 0 */
			 /* .param p1, "isEnable" # Z */
			 /* .line 306 */
			 /* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
			 /* .line 307 */
			 return;
		 } // .end method
		 public Boolean shouldIgnoreKeyboard ( ) {
			 /* .locals 4 */
			 /* .line 284 */
			 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z */
			 int v1 = 1; // const/4 v1, 0x1
			 /* if-nez v0, :cond_0 */
			 /* .line 285 */
			 /* .line 287 */
		 } // :cond_0
		 /* iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I */
		 int v2 = 2; // const/4 v2, 0x2
		 int v3 = 0; // const/4 v3, 0x0
		 /* if-ne v0, v2, :cond_1 */
		 /* .line 288 */
		 /* .line 290 */
	 } // :cond_1
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_3
		 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_2
		 } // :cond_2
		 /* move v1, v3 */
	 } // :cond_3
} // :goto_0
} // .end method
public Boolean shouldIgnoreKeyboardForIIC ( ) {
/* .locals 2 */
/* .line 295 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 296 */
/* .line 298 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_1
	 } // :cond_1
	 int v1 = 0; // const/4 v1, 0x0
} // :cond_2
} // :goto_0
} // .end method
public void updateAngleState ( Float p0 ) {
/* .locals 1 */
/* .param p1, "angle" # F */
/* .line 205 */
v0 = java.lang.Float .isNaN ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 206 */
return;
/* .line 209 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
/* if-nez v0, :cond_1 */
/* .line 210 */
int p1 = 0; // const/4 p1, 0x0
/* .line 213 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
/* if-nez v0, :cond_2 */
/* .line 214 */
/* const/high16 p1, 0x43b40000 # 360.0f */
/* .line 217 */
} // :cond_2
v0 = this.mCurrentState;
/* if-nez v0, :cond_3 */
/* .line 218 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/AngleStateController;->initCurrentState()V */
/* .line 221 */
} // :cond_3
v0 = this.mCurrentState;
v0 = (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).isCurrentState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->isCurrentState(F)Z
/* if-nez v0, :cond_4 */
/* .line 222 */
v0 = this.mCurrentState;
(( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).toNextState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
this.mCurrentState = v0;
/* .line 224 */
} // :cond_4
v0 = this.mCurrentState;
v0 = (( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).getCurrentKeyboardStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->getCurrentKeyboardStatus()Z
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z */
/* .line 226 */
} // :goto_0
return;
} // .end method
public void wakeUpIfNeed ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "shouldWakeUp" # Z */
/* .line 330 */
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .supportPadKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 332 */
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 0; // const/4 v3, 0x0
final String v4 = "miui_keyboard_attach"; // const-string v4, "miui_keyboard_attach"
(( android.os.PowerManager ) v0 ).wakeUp ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
/* .line 335 */
} // :cond_0
return;
} // .end method
