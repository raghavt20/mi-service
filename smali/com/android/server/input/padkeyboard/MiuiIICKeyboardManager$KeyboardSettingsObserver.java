class com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiIICKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "KeyboardSettingsObserver" */
} // .end annotation
/* # instance fields */
public Boolean mShouldUnregisterLanguageReceiver;
final com.android.server.input.padkeyboard.MiuiIICKeyboardManager this$0; //synthetic
/* # direct methods */
public com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1420 */
this.this$0 = p1;
/* .line 1421 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 1419 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->mShouldUnregisterLanguageReceiver:Z */
/* .line 1422 */
return;
} // .end method
/* # virtual methods */
public void observerObject ( ) {
/* .locals 5 */
/* .line 1425 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1426 */
final String v1 = "keyboard_back_light_automatic_adjustment"; // const-string v1, "keyboard_back_light_automatic_adjustment"
android.provider.Settings$System .getUriFor ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardObserver ( v3 );
/* .line 1425 */
int v4 = 0; // const/4 v4, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1427 */
android.provider.Settings$System .getUriFor ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 1429 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1430 */
final String v1 = "keyboard_back_light_brightness"; // const-string v1, "keyboard_back_light_brightness"
android.provider.Settings$System .getUriFor ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardObserver ( v3 );
/* .line 1429 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1431 */
android.provider.Settings$System .getUriFor ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 1433 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1434 */
final String v1 = "enable_tap_touch_pad"; // const-string v1, "enable_tap_touch_pad"
android.provider.Settings$System .getUriFor ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardObserver ( v3 );
/* .line 1433 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1435 */
android.provider.Settings$System .getUriFor ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 1438 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1439 */
final String v1 = "enable_upgrade_no_check"; // const-string v1, "enable_upgrade_no_check"
android.provider.Settings$System .getUriFor ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardObserver ( v3 );
/* .line 1438 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1440 */
android.provider.Settings$System .getUriFor ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 1442 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1443 */
final String v1 = "miui_keyboard_address"; // const-string v1, "miui_keyboard_address"
android.provider.Settings$System .getUriFor ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardObserver ( v3 );
/* .line 1442 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1444 */
android.provider.Settings$System .getUriFor ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 1446 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1447 */
/* const-string/jumbo v1, "user_setup_complete" */
android.provider.Settings$Secure .getUriFor ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardObserver ( v3 );
/* .line 1446 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v4, v3 ); // invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 1448 */
android.provider.Settings$Secure .getUriFor ( v1 );
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) p0 ).onChange ( v4, v0 ); // invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 1449 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 7 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1453 */
final String v0 = "keyboard_back_light_automatic_adjustment"; // const-string v0, "keyboard_back_light_automatic_adjustment"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 1; // const/4 v3, 0x1
int v4 = -2; // const/4 v4, -0x2
int v5 = 0; // const/4 v5, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 1454 */
	 v1 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v6 ).getContentResolver ( ); // invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 v0 = 	 android.provider.Settings$System .getIntForUser ( v6,v0,v5,v4 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
	 } // :cond_0
	 /* move v3, v5 */
} // :goto_0
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fputmIsAutoBackLight ( v1,v3 );
/* .line 1456 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "update auto back light:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIsAutoBackLight ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 1457 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIsAutoBackLight ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1459 */
	 v0 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fputmUserSetBackLight ( v0,v5 );
	 /* .line 1461 */
} // :cond_1
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$mupdateLightSensor ( v0 );
/* goto/16 :goto_5 */
/* .line 1462 */
} // :cond_2
final String v0 = "keyboard_back_light_brightness"; // const-string v0, "keyboard_back_light_brightness"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1463 */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$System .getIntForUser ( v2,v0,v5,v4 );
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fputmBackLightBrightness ( v1,v0 );
/* goto/16 :goto_5 */
/* .line 1465 */
} // :cond_3
final String v0 = "enable_tap_touch_pad"; // const-string v0, "enable_tap_touch_pad"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1466 */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1467 */
v6 = miui.hardware.input.InputFeature .getTouchpadTapDefaultValue ( );
/* .line 1466 */
v0 = android.provider.Settings$System .getIntForUser ( v1,v0,v6,v4 );
/* if-ne v0, v3, :cond_4 */
} // :cond_4
/* move v3, v5 */
} // :goto_1
/* move v0, v3 */
/* .line 1469 */
/* .local v0, "enable":Z */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 1470 */
/* .local v1, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
(( com.android.server.input.config.InputCommonConfig ) v1 ).setTapTouchPad ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/config/InputCommonConfig;->setTapTouchPad(Z)V
/* .line 1471 */
(( com.android.server.input.config.InputCommonConfig ) v1 ).flushToNative ( ); // invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 1472 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update tap touchpad:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 1473 */
} // .end local v0 # "enable":Z
} // .end local v1 # "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
/* goto/16 :goto_5 */
} // :cond_5
final String v0 = "enable_upgrade_no_check"; // const-string v0, "enable_upgrade_no_check"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1474 */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$System .getIntForUser ( v1,v0,v5,v4 );
if ( v0 != null) { // if-eqz v0, :cond_6
} // :cond_6
/* move v3, v5 */
} // :goto_2
/* move v0, v3 */
/* .line 1476 */
/* .restart local v0 # "enable":Z */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v1 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v1 ).setNoCheckUpgrade ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setNoCheckUpgrade(Z)V
/* .line 1477 */
} // .end local v0 # "enable":Z
} // :cond_7
final String v0 = "miui_keyboard_address"; // const-string v0, "miui_keyboard_address"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1478 */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v1,v0,v4 );
/* .line 1480 */
/* .local v0, "address":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmBluetoothKeyboardManager ( v1 );
(( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ) v1 ).setCurrentBleKeyboardAddress ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->setCurrentBleKeyboardAddress(Ljava/lang/String;)V
} // .end local v0 # "address":Ljava/lang/String;
/* .line 1482 */
} // :cond_8
/* const-string/jumbo v0, "user_setup_complete" */
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 1483 */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
/* .line 1484 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1483 */
v0 = android.provider.Settings$Secure .getIntForUser ( v1,v0,v5,v4 );
/* if-ne v0, v3, :cond_9 */
} // :cond_9
/* move v3, v5 */
} // :goto_3
/* move v0, v3 */
/* .line 1486 */
/* .local v0, "isUserSetupComplete":Z */
if ( v0 != null) { // if-eqz v0, :cond_b
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->mShouldUnregisterLanguageReceiver:Z */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 1487 */
/* iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->mShouldUnregisterLanguageReceiver:Z */
/* .line 1489 */
try { // :try_start_0
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v1 );
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmLanguageChangeReceiver ( v3 );
(( android.content.Context ) v1 ).unregisterReceiver ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 1490 */
final String v1 = "User setup completed, unregister language listener"; // const-string v1, "User setup completed, unregister language listener"
android.util.Slog .i ( v2,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1493 */
/* .line 1491 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1492 */
/* .local v1, "e":Ljava/lang/IllegalArgumentException; */
final String v3 = "Unregister Exception!"; // const-string v3, "Unregister Exception!"
android.util.Slog .i ( v2,v3 );
/* .line 1482 */
} // .end local v0 # "isUserSetupComplete":Z
} // .end local v1 # "e":Ljava/lang/IllegalArgumentException;
} // :cond_a
} // :goto_4
/* nop */
/* .line 1496 */
} // :cond_b
} // :goto_5
return;
} // .end method
