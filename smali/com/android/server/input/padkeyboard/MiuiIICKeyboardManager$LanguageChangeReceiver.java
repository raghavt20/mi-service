class com.android.server.input.padkeyboard.MiuiIICKeyboardManager$LanguageChangeReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiuiIICKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LanguageChangeReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.MiuiIICKeyboardManager this$0; //synthetic
/* # direct methods */
private com.android.server.input.padkeyboard.MiuiIICKeyboardManager$LanguageChangeReceiver ( ) {
/* .locals 0 */
/* .line 773 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
 com.android.server.input.padkeyboard.MiuiIICKeyboardManager$LanguageChangeReceiver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 11 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 776 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v0 );
/* if-nez v0, :cond_0 */
/* .line 778 */
return;
/* .line 780 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v1 = "android.intent.action.LOCALE_CHANGED"; // const-string v1, "android.intent.action.LOCALE_CHANGED"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 782 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
v0 = this.locale;
/* .line 783 */
/* .local v0, "currentLocale":Ljava/util/Locale; */
(( java.util.Locale ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;
/* .line 784 */
/* .local v1, "currentLanguage":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "currentLanguage is :"; // const-string v3, "currentLanguage is :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
android.util.Slog .i ( v3,v2 );
/* .line 785 */
v2 = android.text.TextUtils .isEmpty ( v1 );
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 786 */
	 return;
	 /* .line 788 */
} // :cond_1
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v2 );
(( com.android.server.input.InputManagerService ) v2 ).getKeyboardLayouts ( ); // invoke-virtual {v2}, Lcom/android/server/input/InputManagerService;->getKeyboardLayouts()[Landroid/hardware/input/KeyboardLayout;
/* .line 789 */
/* .local v2, "keyboardLayouts":[Landroid/hardware/input/KeyboardLayout; */
/* array-length v4, v2 */
int v5 = 0; // const/4 v5, 0x0
/* move v6, v5 */
} // :goto_0
/* if-ge v6, v4, :cond_2 */
/* aget-object v7, v2, v6 */
/* .line 791 */
/* .local v7, "keyboardLayout":Landroid/hardware/input/KeyboardLayout; */
v8 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v8 );
v9 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v9 );
/* .line 792 */
(( android.view.InputDevice ) v9 ).getIdentifier ( ); // invoke-virtual {v9}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 793 */
(( android.hardware.input.KeyboardLayout ) v7 ).getDescriptor ( ); // invoke-virtual {v7}, Landroid/hardware/input/KeyboardLayout;->getDescriptor()Ljava/lang/String;
/* .line 791 */
(( com.android.server.input.InputManagerService ) v8 ).removeKeyboardLayoutForInputDevice ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Lcom/android/server/input/InputManagerService;->removeKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 789 */
} // .end local v7 # "keyboardLayout":Landroid/hardware/input/KeyboardLayout;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 797 */
} // :cond_2
v4 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v4, :sswitch_data_0 */
} // :cond_3
/* :sswitch_0 */
/* const-string/jumbo v4, "th_TH" */
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
int v5 = 1; // const/4 v5, 0x1
/* :sswitch_1 */
final String v4 = "ru_RU"; // const-string v4, "ru_RU"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
int v5 = 3; // const/4 v5, 0x3
/* :sswitch_2 */
final String v4 = "it_IT"; // const-string v4, "it_IT"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
int v5 = 7; // const/4 v5, 0x7
/* :sswitch_3 */
final String v4 = "fr_FR"; // const-string v4, "fr_FR"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
int v5 = 2; // const/4 v5, 0x2
/* :sswitch_4 */
final String v4 = "es_US"; // const-string v4, "es_US"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
	 int v5 = 5; // const/4 v5, 0x5
	 /* :sswitch_5 */
	 final String v4 = "en_GB"; // const-string v4, "en_GB"
	 v4 = 	 (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* :sswitch_6 */
		 final String v4 = "de_DE"; // const-string v4, "de_DE"
		 v4 = 		 (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v4 != null) { // if-eqz v4, :cond_3
			 int v5 = 4; // const/4 v5, 0x4
			 /* :sswitch_7 */
			 final String v4 = "ar_EG"; // const-string v4, "ar_EG"
			 v4 = 			 (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v4 != null) { // if-eqz v4, :cond_3
				 int v5 = 6; // const/4 v5, 0x6
			 } // :goto_1
			 int v5 = -1; // const/4 v5, -0x1
		 } // :goto_2
		 final String v4 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"; // const-string v4, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"
		 /* packed-switch v5, :pswitch_data_0 */
		 /* .line 847 */
		 return;
		 /* .line 841 */
		 /* :pswitch_0 */
		 final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_italian"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_italian"
		 /* .line 842 */
		 /* .local v5, "keyboardLayoutDescriptor":Ljava/lang/String; */
		 v6 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v6 );
		 v7 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v7 );
		 /* .line 843 */
		 (( android.view.InputDevice ) v7 ).getIdentifier ( ); // invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
		 /* .line 842 */
		 (( com.android.server.input.InputManagerService ) v6 ).setCurrentKeyboardLayoutForInputDevice ( v7, v4 ); // invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
		 /* .line 845 */
		 /* goto/16 :goto_3 */
		 /* .line 835 */
	 } // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
	 /* :pswitch_1 */
	 final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_arabic"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_arabic"
	 /* .line 836 */
	 /* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
	 v6 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v6 );
	 v7 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v7 );
	 /* .line 837 */
	 (( android.view.InputDevice ) v7 ).getIdentifier ( ); // invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
	 /* .line 836 */
	 (( com.android.server.input.InputManagerService ) v6 ).setCurrentKeyboardLayoutForInputDevice ( v7, v4 ); // invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
	 /* .line 839 */
	 /* goto/16 :goto_3 */
	 /* .line 829 */
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
/* :pswitch_2 */
final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"
/* .line 830 */
/* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
v4 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v4 );
v6 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v6 );
/* .line 831 */
(( android.view.InputDevice ) v6 ).getIdentifier ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 830 */
final String v7 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_spanish"; // const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_spanish"
(( com.android.server.input.InputManagerService ) v4 ).setCurrentKeyboardLayoutForInputDevice ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 833 */
/* .line 823 */
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
/* :pswitch_3 */
final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"
/* .line 824 */
/* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
v4 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v4 );
v6 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v6 );
/* .line 825 */
(( android.view.InputDevice ) v6 ).getIdentifier ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 824 */
final String v7 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_german"; // const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_german"
(( com.android.server.input.InputManagerService ) v4 ).setCurrentKeyboardLayoutForInputDevice ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 827 */
/* .line 817 */
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
/* :pswitch_4 */
final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_russian"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_russian"
/* .line 818 */
/* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
v6 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v6 );
v7 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v7 );
/* .line 819 */
(( android.view.InputDevice ) v7 ).getIdentifier ( ); // invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 818 */
(( com.android.server.input.InputManagerService ) v6 ).setCurrentKeyboardLayoutForInputDevice ( v7, v4 ); // invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 821 */
/* .line 811 */
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
/* :pswitch_5 */
final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"
/* .line 812 */
/* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
v4 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v4 );
v6 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v6 );
/* .line 813 */
(( android.view.InputDevice ) v6 ).getIdentifier ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 812 */
final String v7 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_french"; // const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_french"
(( com.android.server.input.InputManagerService ) v4 ).setCurrentKeyboardLayoutForInputDevice ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 815 */
/* .line 805 */
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
/* :pswitch_6 */
final String v5 = "com.miui.miinput/com.miui.miinput.keyboardlayout.InputDeviceReceiver/keyboard_layout_thai"; // const-string v5, "com.miui.miinput/com.miui.miinput.keyboardlayout.InputDeviceReceiver/keyboard_layout_thai"
/* .line 806 */
/* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
v6 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v6 );
v7 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v7 );
/* .line 807 */
(( android.view.InputDevice ) v7 ).getIdentifier ( ); // invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 806 */
(( com.android.server.input.InputManagerService ) v6 ).setCurrentKeyboardLayoutForInputDevice ( v7, v4 ); // invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 809 */
/* .line 799 */
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
/* :pswitch_7 */
final String v5 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"; // const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"
/* .line 800 */
/* .restart local v5 # "keyboardLayoutDescriptor":Ljava/lang/String; */
v4 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v4 );
v6 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v6 );
/* .line 801 */
(( android.view.InputDevice ) v6 ).getIdentifier ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 800 */
final String v7 = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"; // const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"
(( com.android.server.input.InputManagerService ) v4 ).setCurrentKeyboardLayoutForInputDevice ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 803 */
/* nop */
/* .line 849 */
} // :goto_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "add keyboardLayout: "; // const-string v6, "add keyboardLayout: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 850 */
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmInputManager ( v3 );
v4 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmKeyboardDevice ( v4 );
/* .line 851 */
(( android.view.InputDevice ) v4 ).getIdentifier ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;
/* .line 850 */
(( com.android.server.input.InputManagerService ) v3 ).addKeyboardLayoutForInputDevice ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/input/InputManagerService;->addKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V
/* .line 853 */
} // .end local v0 # "currentLocale":Ljava/util/Locale;
} // .end local v1 # "currentLanguage":Ljava/lang/String;
} // .end local v2 # "keyboardLayouts":[Landroid/hardware/input/KeyboardLayout;
} // .end local v5 # "keyboardLayoutDescriptor":Ljava/lang/String;
} // :cond_4
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x58c2770 -> :sswitch_7 */
/* 0x5b084ff -> :sswitch_6 */
/* 0x5c2b431 -> :sswitch_5 */
/* 0x5c4fbcf -> :sswitch_4 */
/* 0x5d29d1f -> :sswitch_3 */
/* 0x5fdccbf -> :sswitch_2 */
/* 0x67d15bf -> :sswitch_1 */
/* 0x6935c1f -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
