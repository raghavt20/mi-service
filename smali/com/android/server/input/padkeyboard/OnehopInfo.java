public class com.android.server.input.padkeyboard.OnehopInfo {
	 /* .source "OnehopInfo.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/OnehopInfo$Builder; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION;
private static java.lang.String ACTION_BASE;
public static java.lang.String ACTION_SUFFIX_MIRROR;
private static final java.lang.String DEFAULT_BLUETOOTH_MAC_ADDRESS;
private static final Integer DEVICE_TYPE;
private static final PREFIX_PROTOCOL;
private static final PROTOCOL_ID;
private static final java.lang.String SECURE_SETTINGS_BLUETOOTH_ADDRESS;
private static final SUFFIX_PROTOCOL;
private static final java.lang.String TAG;
private static final Integer TYPE_ABILITY_BASE;
private static final Integer TYPE_ACTION_BASE;
public static final Integer TYPE_CUSTOM_ACTION;
private static final Integer TYPE_DEVICEINFO_BASE;
public static final Integer TYPE_DEVICEINFO_BT_MAC;
public static final Integer TYPE_EXT_ABILITY;
/* # instance fields */
private java.lang.String action_suffix;
private java.lang.String bt_mac;
private Integer device_type;
private ext_ability;
/* # direct methods */
static com.android.server.input.padkeyboard.OnehopInfo ( ) {
	 /* .locals 1 */
	 /* .line 22 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* new-array v0, v0, [B */
	 /* fill-array-data v0, :array_0 */
	 /* .line 24 */
	 final String v0 = "com.miui.onehop.action"; // const-string v0, "com.miui.onehop.action"
	 /* .line 31 */
	 final String v0 = "MIRROR"; // const-string v0, "MIRROR"
	 /* .line 34 */
	 /* const/16 v0, 0x15 */
	 /* new-array v0, v0, [B */
	 /* fill-array-data v0, :array_1 */
	 /* .line 41 */
	 int v0 = 4; // const/4 v0, 0x4
	 /* new-array v0, v0, [B */
	 /* fill-array-data v0, :array_2 */
	 return;
	 /* :array_0 */
	 /* .array-data 1 */
	 /* 0x27t */
	 /* 0x17t */
} // .end array-data
/* nop */
/* :array_1 */
/* .array-data 1 */
/* 0x8t */
/* 0x1t */
/* 0x10t */
/* 0xdt */
/* 0x22t */
/* 0x1t */
/* 0x3t */
/* 0x2at */
/* 0x9t */
/* 0x4dt */
/* 0x49t */
/* 0x2dt */
/* 0x4et */
/* 0x46t */
/* 0x43t */
/* 0x54t */
/* 0x41t */
/* 0x47t */
/* 0x38t */
/* 0xft */
/* 0x4at */
} // .end array-data
/* nop */
/* :array_2 */
/* .array-data 1 */
/* 0x6at */
/* 0x2t */
/* -0x6t */
/* 0x7ft */
} // .end array-data
} // .end method
private com.android.server.input.padkeyboard.OnehopInfo ( ) {
/* .locals 1 */
/* .param p1, "builder" # Lcom/android/server/input/padkeyboard/OnehopInfo$Builder; */
/* .line 54 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
com.android.server.input.padkeyboard.OnehopInfo$Builder .-$$Nest$fgetaction_suffix ( p1 );
this.action_suffix = v0;
/* .line 56 */
v0 = com.android.server.input.padkeyboard.OnehopInfo$Builder .-$$Nest$fgetdevice_type ( p1 );
/* iput v0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->device_type:I */
/* .line 57 */
com.android.server.input.padkeyboard.OnehopInfo$Builder .-$$Nest$fgetbt_mac ( p1 );
this.bt_mac = v0;
/* .line 58 */
com.android.server.input.padkeyboard.OnehopInfo$Builder .-$$Nest$fgetext_ability ( p1 );
this.ext_ability = v0;
/* .line 59 */
return;
} // .end method
 com.android.server.input.padkeyboard.OnehopInfo ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/OnehopInfo;-><init>(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)V */
return;
} // .end method
private static final generateNFCProtocol ( Object[] p0 ) {
/* .locals 5 */
/* .param p0, "appDataProtocol" # [B */
/* .line 300 */
/* if-nez p0, :cond_0 */
/* .line 301 */
final String v0 = "OnehopInfo"; // const-string v0, "OnehopInfo"
final String v1 = "generateNFCProtocol appDataProtocol is null"; // const-string v1, "generateNFCProtocol appDataProtocol is null"
android.util.Slog .e ( v0,v1 );
/* .line 302 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [B */
/* .line 304 */
} // :cond_0
v0 = com.android.server.input.padkeyboard.OnehopInfo.PREFIX_PROTOCOL;
/* array-length v1, v0 */
/* add-int/lit8 v1, v1, 0x2 */
/* add-int/lit8 v1, v1, 0x1 */
/* array-length v2, p0 */
/* add-int/2addr v1, v2 */
v2 = com.android.server.input.padkeyboard.OnehopInfo.SUFFIX_PROTOCOL;
/* array-length v3, v2 */
/* add-int/2addr v1, v3 */
/* .line 306 */
/* .local v1, "totoleLength":I */
java.nio.ByteBuffer .allocate ( v1 );
/* .line 307 */
/* .local v3, "totalData":Ljava/nio/ByteBuffer; */
/* const/16 v4, 0xa */
(( java.nio.ByteBuffer ) v3 ).put ( v4 ); // invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 308 */
/* add-int/lit8 v4, v1, -0x2 */
/* int-to-byte v4, v4 */
(( java.nio.ByteBuffer ) v3 ).put ( v4 ); // invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 309 */
(( java.nio.ByteBuffer ) v3 ).put ( v0 ); // invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 310 */
/* array-length v0, p0 */
/* int-to-byte v0, v0 */
(( java.nio.ByteBuffer ) v3 ).put ( v0 ); // invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 311 */
(( java.nio.ByteBuffer ) v3 ).put ( p0 ); // invoke-virtual {v3, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 312 */
(( java.nio.ByteBuffer ) v3 ).put ( v2 ); // invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 313 */
(( java.nio.ByteBuffer ) v3 ).array ( ); // invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B
/* .line 314 */
/* .local v0, "protocolData":[B */
} // .end method
public static java.lang.String getBtMacAddress ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 253 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "OnehopInfo"; // const-string v1, "OnehopInfo"
/* if-nez p0, :cond_0 */
/* .line 254 */
final String v2 = "mContext is null"; // const-string v2, "mContext is null"
android.util.Slog .e ( v1,v2 );
/* .line 255 */
/* .line 257 */
} // :cond_0
android.bluetooth.BluetoothAdapter .getDefaultAdapter ( );
/* .line 258 */
/* .local v2, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter; */
/* if-nez v2, :cond_1 */
/* .line 259 */
final String v3 = "bluetoothAdapter is null"; // const-string v3, "bluetoothAdapter is null"
android.util.Slog .e ( v1,v3 );
/* .line 260 */
/* .line 262 */
} // :cond_1
(( android.bluetooth.BluetoothAdapter ) v2 ).getAddress ( ); // invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;
/* .line 263 */
/* .local v0, "address":Ljava/lang/String; */
final String v1 = "02:00:00:00:00:00"; // const-string v1, "02:00:00:00:00:00"
if ( v0 != null) { // if-eqz v0, :cond_2
v3 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_2 */
/* .line 264 */
v3 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_2 */
/* .line 265 */
/* .line 267 */
} // :cond_2
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "bluetooth_address"; // const-string v4, "bluetooth_address"
android.provider.Settings$Secure .getString ( v3,v4 );
/* .line 269 */
/* .local v3, "address2":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_4
v4 = (( java.lang.String ) v3 ).isEmpty ( ); // invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z
/* if-nez v4, :cond_4 */
/* .line 270 */
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 273 */
} // :cond_3
/* .line 271 */
} // :cond_4
} // :goto_0
com.android.server.input.padkeyboard.OnehopInfo .getBtMacAddressByReflection ( );
} // .end method
private static java.lang.String getBtMacAddressByReflection ( ) {
/* .locals 8 */
/* .line 278 */
final String v0 = "OnehopInfo"; // const-string v0, "OnehopInfo"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
android.bluetooth.BluetoothAdapter .getDefaultAdapter ( );
/* .line 279 */
/* .local v2, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter; */
/* if-nez v2, :cond_0 */
/* .line 280 */
/* .line 282 */
} // :cond_0
final String v3 = "mService"; // const-string v3, "mService"
/* const-class v4, Ljava/lang/Object; */
miui.util.ReflectionUtils .getObjectField ( v2,v3,v4 );
/* .line 283 */
/* .local v3, "bluetoothManagerService":Ljava/lang/Object; */
/* if-nez v3, :cond_1 */
/* .line 284 */
final String v4 = "getBtMacAdressByReflection: bluetooth manager service is null"; // const-string v4, "getBtMacAdressByReflection: bluetooth manager service is null"
android.util.Slog .w ( v0,v4 );
/* .line 285 */
/* .line 287 */
} // :cond_1
(( java.lang.Object ) v3 ).getClass ( ); // invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v5 = "getAddress"; // const-string v5, "getAddress"
int v6 = 0; // const/4 v6, 0x0
/* new-array v7, v6, [Ljava/lang/Class; */
(( java.lang.Class ) v4 ).getMethod ( v5, v7 ); // invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v5, v6, [Ljava/lang/Object; */
/* .line 288 */
(( java.lang.reflect.Method ) v4 ).invoke ( v3, v5 ); // invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 289 */
/* .local v4, "address":Ljava/lang/Object; */
if ( v4 != null) { // if-eqz v4, :cond_3
/* instance-of v5, v4, Ljava/lang/String; */
/* if-nez v5, :cond_2 */
/* .line 292 */
} // :cond_2
/* move-object v5, v4 */
/* check-cast v5, Ljava/lang/String; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 290 */
} // :cond_3
} // :goto_0
/* .line 293 */
} // .end local v2 # "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
} // .end local v3 # "bluetoothManagerService":Ljava/lang/Object;
} // .end local v4 # "address":Ljava/lang/Object;
/* :catch_0 */
/* move-exception v2 */
/* .line 294 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "occur exception:"; // const-string v4, "occur exception:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).getLocalizedMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 295 */
} // .end method
public static getExtAbility ( ) {
/* .locals 5 */
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
/* .line 245 */
/* .local v0, "b":B */
int v1 = 0; // const/4 v1, 0x0
/* .line 247 */
/* .local v1, "i":I */
/* or-int/lit8 v2, v0, 0x1 */
/* add-int/2addr v1, v2 */
/* .line 248 */
/* and-int/lit16 v2, v1, 0xff */
/* int-to-byte v2, v2 */
/* .line 249 */
/* .local v2, "result":B */
int v3 = 1; // const/4 v3, 0x1
/* new-array v3, v3, [B */
int v4 = 0; // const/4 v4, 0x0
/* aput-byte v2, v3, v4 */
} // .end method
private Integer getTLVLength ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "value" # Ljava/lang/String; */
/* .line 146 */
int v0 = 0; // const/4 v0, 0x0
/* .line 147 */
/* .local v0, "length":I */
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_0 */
/* .line 148 */
v1 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) p1 ).getBytes ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
/* array-length v0, v1 */
/* .line 150 */
} // :cond_0
} // .end method
private Integer getTLVLength ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "byteValue" # [B */
/* .line 154 */
int v0 = 0; // const/4 v0, 0x0
/* .line 155 */
/* .local v0, "length":I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 156 */
/* array-length v0, p1 */
/* .line 158 */
} // :cond_0
} // .end method
public static toProtocolByteArray ( Object[] p0, Integer p1, java.util.Map p2, java.lang.String p3, Object[] p4 ) {
/* .locals 10 */
/* .param p0, "protocolId" # [B */
/* .param p1, "deviceType" # I */
/* .param p3, "action" # Ljava/lang/String; */
/* .param p4, "payload" # [B */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([BI", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "[B>;", */
/* "Ljava/lang/String;", */
/* "[B)[B" */
/* } */
} // .end annotation
/* .line 181 */
/* .local p2, "tlvCountNum":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;[B>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 183 */
/* .local v0, "totalLenth":I */
/* add-int/lit8 v0, v0, 0x2 */
/* .line 185 */
/* add-int/lit8 v0, v0, 0x4 */
/* .line 187 */
int v1 = 1; // const/4 v1, 0x1
/* add-int/2addr v0, v1 */
/* .line 188 */
int v2 = 0; // const/4 v2, 0x0
final String v3 = "OnehopInfo"; // const-string v3, "OnehopInfo"
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 189 */
try { // :try_start_0
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_0
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 190 */
/* .local v5, "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;" */
/* check-cast v6, [B */
/* .line 192 */
/* .local v6, "entryValue":[B */
/* add-int/lit8 v7, v0, 0x2 */
/* array-length v8, v6 */
/* add-int v0, v7, v8 */
/* .line 193 */
} // .end local v5 # "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
} // .end local v6 # "entryValue":[B
/* .line 195 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .line 196 */
/* .local v4, "actionConntent":[B */
v5 = android.text.TextUtils .isEmpty ( p3 );
/* if-nez v5, :cond_5 */
/* .line 197 */
v5 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) p3 ).getBytes ( v5 ); // invoke-virtual {p3, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
/* move-object v4, v5 */
/* .line 199 */
/* add-int/lit8 v5, v0, 0x1 */
/* array-length v6, v4 */
/* add-int/2addr v5, v6 */
/* .line 204 */
} // .end local v0 # "totalLenth":I
/* .local v5, "totalLenth":I */
/* array-length v0, p4 */
/* add-int/2addr v5, v0 */
/* .line 206 */
java.nio.ByteBuffer .allocate ( v5 );
/* .line 207 */
/* .local v0, "totalData":Ljava/nio/ByteBuffer; */
(( java.nio.ByteBuffer ) v0 ).put ( p0 ); // invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 208 */
(( java.nio.ByteBuffer ) v0 ).putInt ( p1 ); // invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 210 */
v6 = if ( p2 != null) { // if-eqz p2, :cond_2
/* if-le v6, v1, :cond_2 */
v1 = /* .line 211 */
/* int-to-byte v1, v1 */
(( java.nio.ByteBuffer ) v0 ).put ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 212 */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Ljava/util/Map$Entry; */
/* .line 213 */
/* .local v6, "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;" */
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 214 */
/* .local v7, "entryKey":I */
/* check-cast v8, [B */
/* .line 215 */
/* .local v8, "entryValue":[B */
(( java.nio.ByteBuffer ) v0 ).putInt ( v7 ); // invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 216 */
/* array-length v9, v8 */
/* int-to-byte v9, v9 */
(( java.nio.ByteBuffer ) v0 ).put ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 217 */
(( java.nio.ByteBuffer ) v0 ).put ( v8 ); // invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 218 */
/* nop */
} // .end local v6 # "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
} // .end local v7 # "entryKey":I
} // .end local v8 # "entryValue":[B
} // :cond_1
/* .line 220 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
(( java.nio.ByteBuffer ) v0 ).put ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 222 */
} // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 223 */
/* array-length v1, v4 */
/* int-to-byte v1, v1 */
(( java.nio.ByteBuffer ) v0 ).put ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 224 */
(( java.nio.ByteBuffer ) v0 ).put ( v4 ); // invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 229 */
if ( p4 != null) { // if-eqz p4, :cond_3
/* .line 230 */
(( java.nio.ByteBuffer ) v0 ).put ( p4 ); // invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 235 */
(( java.nio.ByteBuffer ) v0 ).array ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B
/* .line 236 */
/* .local v1, "totalDataArray":[B */
/* .line 232 */
} // .end local v1 # "totalDataArray":[B
} // :cond_3
/* const-string/jumbo v1, "toProtocolByteArray mPayload is null" */
android.util.Slog .e ( v3,v1 );
/* .line 233 */
/* .line 226 */
} // :cond_4
/* const-string/jumbo v1, "toProtocolByteArray actionConntent is null" */
android.util.Slog .e ( v3,v1 );
/* .line 227 */
/* .line 201 */
} // .end local v5 # "totalLenth":I
/* .local v0, "totalLenth":I */
} // :cond_5
/* const-string/jumbo v1, "toProtocolByteArray mAction is null" */
android.util.Slog .e ( v3,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 202 */
/* .line 237 */
} // .end local v0 # "totalLenth":I
} // .end local v4 # "actionConntent":[B
/* :catch_0 */
/* move-exception v0 */
/* .line 238 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "toProtocolByteArray error : " */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v1 );
/* .line 240 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
/* # virtual methods */
public getPayload ( com.android.server.input.padkeyboard.OnehopInfo p0 ) {
/* .locals 5 */
/* .param p1, "info" # Lcom/android/server/input/padkeyboard/OnehopInfo; */
/* .line 162 */
(( com.android.server.input.padkeyboard.OnehopInfo ) p1 ).toPayloadByteArray ( ); // invoke-virtual {p1}, Lcom/android/server/input/padkeyboard/OnehopInfo;->toPayloadByteArray()[B
/* .line 163 */
/* .local v0, "payload":[B */
v1 = com.android.server.input.padkeyboard.OnehopInfo.PROTOCOL_ID;
int v2 = 0; // const/4 v2, 0x0
final String v3 = "TAG_DISCOVERED"; // const-string v3, "TAG_DISCOVERED"
/* const/16 v4, 0x8 */
com.android.server.input.padkeyboard.OnehopInfo .toProtocolByteArray ( v1,v4,v2,v3,v0 );
/* .line 165 */
/* .local v1, "protocolData":[B */
com.android.server.input.padkeyboard.OnehopInfo .generateNFCProtocol ( v1 );
/* .line 166 */
/* .local v2, "nfcProtocolValue":[B */
} // .end method
public toPayloadByteArray ( ) {
/* .locals 7 */
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
/* .line 97 */
/* .local v0, "totalLenth":I */
try { // :try_start_0
v1 = this.action_suffix;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getTLVLength(Ljava/lang/String;)I */
/* .line 98 */
/* .local v1, "act_l":I */
/* if-lez v1, :cond_0 */
/* .line 99 */
/* add-int/lit8 v2, v0, 0x2 */
/* add-int v0, v2, v1 */
/* .line 103 */
} // :cond_0
v2 = this.bt_mac;
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getTLVLength(Ljava/lang/String;)I */
/* .line 104 */
/* .local v2, "bt_mac_l":I */
/* if-lez v2, :cond_1 */
/* .line 105 */
/* add-int/lit8 v3, v0, 0x2 */
/* add-int v0, v3, v2 */
/* .line 109 */
} // :cond_1
v3 = this.ext_ability;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getTLVLength([B)I */
/* .line 110 */
/* .local v3, "ext_ability_l":I */
/* if-lez v3, :cond_2 */
/* .line 111 */
/* add-int/lit8 v4, v0, 0x2 */
/* add-int v0, v4, v3 */
/* .line 114 */
} // :cond_2
java.nio.ByteBuffer .allocate ( v0 );
/* .line 117 */
/* .local v4, "totalData":Ljava/nio/ByteBuffer; */
/* if-lez v1, :cond_3 */
/* .line 118 */
/* const/16 v5, 0x65 */
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 119 */
/* int-to-byte v5, v1 */
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 120 */
v5 = this.action_suffix;
v6 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v5 ).getBytes ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 124 */
} // :cond_3
/* if-lez v2, :cond_4 */
/* .line 125 */
int v5 = 1; // const/4 v5, 0x1
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 126 */
/* int-to-byte v5, v2 */
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 127 */
v5 = this.bt_mac;
v6 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v5 ).getBytes ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 131 */
} // :cond_4
/* if-lez v3, :cond_5 */
/* .line 132 */
/* const/16 v5, 0x79 */
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 133 */
/* int-to-byte v5, v3 */
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 134 */
v5 = this.ext_ability;
(( java.nio.ByteBuffer ) v4 ).put ( v5 ); // invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 137 */
} // :cond_5
(( java.nio.ByteBuffer ) v4 ).array ( ); // invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 138 */
/* .local v5, "totalDataArray":[B */
/* .line 139 */
} // .end local v0 # "totalLenth":I
} // .end local v1 # "act_l":I
} // .end local v2 # "bt_mac_l":I
} // .end local v3 # "ext_ability_l":I
} // .end local v4 # "totalData":Ljava/nio/ByteBuffer;
} // .end local v5 # "totalDataArray":[B
/* :catch_0 */
/* move-exception v0 */
/* .line 140 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = ""; // const-string v2, ""
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "OnehopInfo"; // const-string v2, "OnehopInfo"
android.util.Log .e ( v2,v1 );
/* .line 142 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 319 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "OnehopInfo{, action_suffix=\'"; // const-string v1, "OnehopInfo{, action_suffix=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.action_suffix;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", device_type="; // const-string v2, ", device_type="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->device_type:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", bt_mac=\'"; // const-string v2, ", bt_mac=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.bt_mac;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", ext_ability=\'"; // const-string v2, ", ext_ability=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.ext_ability;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
