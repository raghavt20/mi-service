.class Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiIICKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    .line 129
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 132
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.action.MICROPHONE_MUTE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/PadManager;->getMuteStatus()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setMuteLight(Z)V

    .line 135
    :cond_0
    return-void
.end method
