.class Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;
.super Landroid/os/Handler;
.source "MiuiUsbKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field private static final MSG_CHECK_KEYBOARD_IDENTITY:I = 0xb

.field private static final MSG_GET_DEVICE_TIME_OUT:I = 0xc

.field private static final MSG_GET_KEYBOARD_REPORT_DATA:I = 0x7

.field private static final MSG_GET_MCU_RESET_MODE:I = 0x3

.field private static final MSG_READ_CONNECT_STATE:I = 0x1

.field private static final MSG_READ_KEYBOARD_STATUS:I = 0x5

.field private static final MSG_READ_KEYBOARD_VERSION:I = 0x4

.field private static final MSG_READ_MCU_VERSION:I = 0x2

.field private static final MSG_START_KEYBOARD_UPGRADE:I = 0x9

.field private static final MSG_START_MCU_UPGRADE:I = 0x8

.field private static final MSG_START_WIRELESS_UPGRADE:I = 0xa

.field private static final MSG_TEST_FUNCTION:I = 0x0

.field private static final MSG_WRITE_KEYBOARD_STATUS:I = 0x6


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;


# direct methods
.method public constructor <init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 504
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    .line 505
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 506
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 511
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 572
    :pswitch_0
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "reset keyboard usb host cause get usb device time out"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmKeyboardDevicesObserver(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->resetKeyboardHost()V

    goto/16 :goto_0

    .line 563
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 564
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 565
    goto :goto_0

    .line 567
    :cond_0
    const-string v1, "first_check"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 568
    .local v1, "isFirst":Z
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoCheckKeyboardIdentity(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Z)V

    .line 569
    goto :goto_0

    .line 559
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "isFirst":Z
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoStartWirelessUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 560
    goto :goto_0

    .line 555
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoStartKeyboardUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 556
    goto :goto_0

    .line 551
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoStartMcuUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 552
    goto :goto_0

    .line 547
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoGetReportData(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 548
    goto :goto_0

    .line 537
    :pswitch_6
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 538
    .restart local v0    # "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 539
    goto :goto_0

    .line 541
    :cond_1
    const-string/jumbo v1, "target"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 542
    .local v1, "target":I
    const-string/jumbo v3, "value"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 543
    .local v2, "value":I
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v3, v1, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoWritePadKeyBoardStatus(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;II)V

    .line 544
    goto :goto_0

    .line 533
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "target":I
    .end local v2    # "value":I
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoReadKeyboardStatus(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 534
    goto :goto_0

    .line 529
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoReadKeyboardVersion(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 530
    goto :goto_0

    .line 525
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoGetMcuReset(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 526
    goto :goto_0

    .line 521
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoReadMcuVersion(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 522
    goto :goto_0

    .line 517
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mdoReadConnectState(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 518
    goto :goto_0

    .line 513
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mtestFunction(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 514
    nop

    .line 576
    :goto_0
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessagesOrCallbacks()Z

    move-result v0

    if-nez v0, :cond_2

    .line 577
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mcloseDevice(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 579
    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
