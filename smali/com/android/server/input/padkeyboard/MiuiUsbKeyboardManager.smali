.class public Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
.super Ljava/lang/Object;
.source "MiuiUsbKeyboardManager.java"

# interfaces
.implements Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;
.implements Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;,
        Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;
    }
.end annotation


# static fields
.field private static final KEYBOARD_AUTO_UPGRADE:Ljava/lang/String; = "keyboard_auto_upgrade"

.field private static final KEYBOARD_BACK_LIGHT:Ljava/lang/String; = "keyboard_back_light"

.field private static final KEY_COMMAND_TARGET:Ljava/lang/String; = "target"

.field private static final KEY_COMMAND_VALUE:Ljava/lang/String; = "value"

.field private static final KEY_FIRST_CHECK:Ljava/lang/String; = "first_check"

.field private static final MAX_CHECK_IDENTITY_TIMES:I = 0x5

.field private static final MAX_GET_USB_DEVICE_TIME_OUT:I = 0x4e20

.field private static final MAX_RETRY_TIMES:I = 0x2

.field private static final MAX_UPGRADE_FAILED_TIMES:I = 0x5

.field private static volatile sInstance:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;


# instance fields
.field private final mAccSensor:Landroid/hardware/Sensor;

.field private final mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

.field private mBinKbHighVersion:Ljava/lang/String;

.field private mBinKbLowVersion:Ljava/lang/String;

.field private mBinKbVersion:Ljava/lang/String;

.field private mBinMcuVersion:Ljava/lang/String;

.field private mBinWirelessVersion:Ljava/lang/String;

.field private mCheckIdentityTimes:I

.field private final mContentObserver:Landroid/database/ContentObserver;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mEnableAutoUpgrade:Z

.field private final mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private mInputDeviceId:I

.field private final mInputManager:Lcom/android/server/input/InputManagerService;

.field private mIsFirstAction:Z

.field private mIsKeyboardReady:Z

.field private mIsSetupComplete:Z

.field private mKbTypeLevel:I

.field private mKeyboardDevicesObserver:Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;

.field private mKeyboardUpgradeFailedTimes:I

.field private mKeyboardVersion:Ljava/lang/String;

.field private final mLocalGData:[F

.field private mMcuUpgradeFailedTimes:I

.field private mMcuVersion:Ljava/lang/String;

.field private mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private mPenState:I

.field private final mRecBuf:[B

.field private mRecentConnTime:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReportInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private mReportInterface:Landroid/hardware/usb/UsbInterface;

.field private mScreenState:Z

.field private final mSendBuf:[B

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private mShouldEnableBackLight:Z

.field private mShouldWakeUp:Z

.field private mUsbActionReceiver:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;

.field private mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

.field private mUsbDevice:Landroid/hardware/usb/UsbDevice;

.field private final mUsbDeviceLock:Ljava/lang/Object;

.field private mUsbInterface:Landroid/hardware/usb/UsbInterface;

.field private final mUsbManager:Landroid/hardware/usb/UsbManager;

.field private mWirelessVersion:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAngleStateController(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/AngleStateController;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmEnableAutoUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsKeyboardReady(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardDevicesObserver(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardDevicesObserver:Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShouldEnableBackLight(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUsbDeviceLock(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDeviceLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmEnableAutoUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmShouldEnableBackLight(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUsbDevice(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/hardware/usb/UsbDevice;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    return-void
.end method

.method static bridge synthetic -$$Nest$mcloseDevice(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->closeDevice()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoCheckKeyboardIdentity(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doCheckKeyboardIdentity(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoGetMcuReset(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetMcuReset()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoGetReportData(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetReportData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoReadConnectState(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadConnectState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoReadKeyboardStatus(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadKeyboardStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoReadKeyboardVersion(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadKeyboardVersion()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoReadMcuVersion(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadMcuVersion()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoStartKeyboardUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doStartKeyboardUpgrade()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoStartMcuUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doStartMcuUpgrade()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoStartWirelessUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doStartWirelessUpgrade()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoWritePadKeyBoardStatus(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doWritePadKeyBoardStatus(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monUsbDeviceAttach(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onUsbDeviceAttach()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monUsbDeviceDetach(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onUsbDeviceDetach()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtestFunction(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->testFunction()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    .line 91
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDeviceLock:Ljava/lang/Object;

    .line 104
    const/16 v1, 0x40

    new-array v2, v1, [B

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    .line 105
    new-array v1, v1, [B

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    .line 109
    const/4 v1, 0x3

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mLocalGData:[F

    .line 113
    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I

    .line 114
    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    .line 115
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mPenState:I

    .line 116
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    .line 117
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z

    .line 118
    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I

    .line 122
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z

    .line 234
    new-instance v2, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContentObserver:Landroid/database/ContentObserver;

    .line 737
    new-instance v3, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$2;

    invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$2;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecentConnTime:Ljava/util/Map;

    .line 127
    const-string v3, "MiuiPadKeyboardManager"

    const-string v4, "MiuiUsbKeyboardManager"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    .line 129
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->registerBroadcastReceiver(Landroid/content/Context;)V

    .line 130
    const-string v3, "input"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    check-cast v3, Lcom/android/server/input/InputManagerService;

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    .line 131
    const-string/jumbo v3, "usb"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbManager;

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbManager:Landroid/hardware/usb/UsbManager;

    .line 132
    const-string/jumbo v3, "sensor"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 133
    invoke-virtual {v3, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAccSensor:Landroid/hardware/Sensor;

    .line 135
    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "pad_keyboard_transfer_thread"

    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandlerThread:Landroid/os/HandlerThread;

    .line 136
    invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V

    .line 137
    new-instance v4, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v4, p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    .line 138
    new-instance v3, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;

    invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;-><init>(Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;)V

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardDevicesObserver:Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;

    .line 139
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->startWatching()V

    .line 141
    new-instance v3, Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-direct {v3, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 143
    nop

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 143
    const-string v4, "keyboard_back_light"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v0

    :goto_0
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z

    .line 145
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 146
    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 145
    invoke-virtual {v3, v4, v0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 147
    nop

    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 147
    const-string v4, "keyboard_auto_upgrade"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v0

    :goto_1
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 150
    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 149
    invoke-virtual {v1, v3, v0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 152
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 153
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardTypeValue()I

    move-result v1

    .line 152
    const-string v2, "keyboard_type_level"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 154
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 155
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    const/16 v1, 0x3206

    const/16 v2, 0x3ffc

    invoke-virtual {v0, v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setMiuiKeyboardInfo(II)V

    .line 156
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 157
    return-void
.end method

.method private checkKeyboardIdentity(IZ)V
    .locals 5
    .param p1, "delay"    # I
    .param p2, "isFirst"    # Z

    .line 460
    if-eqz p2, :cond_0

    .line 461
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeMessages(I)V

    .line 464
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 465
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 466
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "first_check"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 467
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 468
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    int-to-long v3, p1

    invoke-virtual {v2, v0, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 469
    return-void
.end method

.method private cleanUsbCash(I)V
    .locals 4
    .param p1, "times"    # I

    .line 1548
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1549
    return-void

    .line 1552
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 1553
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    .line 1552
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1555
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private closeDevice()V
    .locals 3

    .line 1558
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDeviceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1559
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v1, :cond_0

    .line 1560
    monitor-exit v0

    return-void

    .line 1562
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v2, :cond_1

    .line 1563
    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    goto :goto_0

    .line 1564
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v2, :cond_2

    .line 1565
    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 1567
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    .line 1568
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 1569
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 1570
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInterface:Landroid/hardware/usb/UsbInterface;

    .line 1571
    monitor-exit v0

    .line 1572
    return-void

    .line 1571
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private doCheckKeyboardIdentity(Z)V
    .locals 1
    .param p1, "isFirst"    # Z

    .line 1613
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1614
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchBeforeU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I

    move-result v0

    goto :goto_0

    .line 1615
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchAfterU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I

    move-result v0

    :goto_0
    nop

    .line 1616
    .local v0, "identity":I
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->processIdentity(I)V

    .line 1617
    return-void
.end method

.method private doGetMcuReset()V
    .locals 7

    .line 821
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 822
    return-void

    .line 825
    :cond_0
    const-string v0, "get mcu reset"

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 828
    invoke-static {}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandGetResetInfo()[B

    move-result-object v0

    .line 829
    .local v0, "command":[B
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v4, v0

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 831
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v4, 0x2

    if-ge v3, v4, :cond_5

    .line 832
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 833
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v4, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 834
    :cond_1
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 835
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseUsbReset([B)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 836
    return-void

    .line 839
    :cond_2
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-nez v4, :cond_4

    .line 840
    return-void

    .line 843
    :cond_3
    const-string/jumbo v4, "send reset failed"

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    const/16 v4, 0xc8

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 831
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 847
    .end local v3    # "i":I
    :cond_5
    return-void
.end method

.method private doGetReportData()V
    .locals 8

    .line 1208
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForReport()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1209
    return-void

    .line 1212
    :cond_0
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "get report data"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1213
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 1214
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1216
    .local v0, "startTime":J
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v2

    move v3, v2

    .local v3, "hasReport":Z
    if-nez v2, :cond_3

    .line 1217
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/16 v6, 0x14

    cmp-long v2, v4, v6

    if-gez v2, :cond_2

    goto :goto_1

    .line 1222
    :cond_2
    return-void

    .line 1218
    :cond_3
    :goto_1
    if-eqz v3, :cond_1

    .line 1219
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReportData([B)V

    goto :goto_0
.end method

.method private doReadConnectState()V
    .locals 6

    .line 647
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 648
    return-void

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 652
    invoke-static {}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandGetConnectState()[B

    move-result-object v0

    .line 653
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 655
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    .line 656
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 657
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 658
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 659
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseConnectState([B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 660
    return-void

    .line 664
    :cond_2
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send connect failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 667
    .end local v2    # "i":I
    :cond_4
    return-void
.end method

.method private doReadKeyboardStatus()V
    .locals 6

    .line 924
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 929
    invoke-static {}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandGetKeyboardStatus()[B

    move-result-object v0

    .line 930
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 932
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    .line 933
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 934
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 935
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 936
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReadKeyboardStatus([B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 937
    return-void

    .line 941
    :cond_2
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send read keyboard failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    const/16 v3, 0xc8

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 932
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 945
    .end local v2    # "i":I
    :cond_4
    return-void

    .line 925
    .end local v0    # "command":[B
    :cond_5
    :goto_1
    return-void
.end method

.method private doReadKeyboardVersion()V
    .locals 6

    .line 858
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 859
    return-void

    .line 862
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 863
    invoke-static {}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandGetKeyboardVersion()[B

    move-result-object v0

    .line 864
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 866
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    .line 867
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 868
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 869
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 870
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseKeyboardVersion([B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 871
    return-void

    .line 875
    :cond_2
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send version failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 878
    .end local v2    # "i":I
    :cond_4
    return-void
.end method

.method private doReadMcuVersion()V
    .locals 6

    .line 780
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-nez v0, :cond_0

    .line 781
    return-void

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 785
    invoke-static {}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandGetVersionInfo()[B

    move-result-object v0

    .line 786
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 788
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_4

    .line 789
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 790
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 791
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 792
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseUsbDeviceVersion([B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 793
    return-void

    .line 797
    :cond_2
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send version failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    const/16 v3, 0xc8

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 788
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 801
    .end local v2    # "i":I
    :cond_4
    return-void
.end method

.method private doStartKeyboardUpgrade()V
    .locals 8

    .line 1354
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    const-string v1, "MiuiPadKeyboardManager"

    if-eqz v0, :cond_a

    const-string v2, "0000"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 1358
    :cond_0
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbVersion:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 1359
    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_3

    :cond_1
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbLowVersion:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 1361
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbLowVersion:Ljava/lang/String;

    .line 1362
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1361
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbLowVersion:Ljava/lang/String;

    .line 1363
    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_3

    :cond_2
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbHighVersion:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 1365
    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_4

    .line 1366
    :cond_3
    const-string v0, "no need to start keyboard upgrade"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    return-void

    .line 1369
    :cond_4
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    const/4 v3, 0x5

    if-le v0, v3, :cond_5

    .line 1370
    const-string/jumbo v0, "upgrade keyboard failed too many times"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    return-void

    .line 1374
    :cond_5
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardUpgradeHelper()Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;

    move-result-object v0

    .line 1375
    .local v0, "keyboardUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
    if-nez v0, :cond_6

    .line 1376
    const-string v2, "can not get KeyboardUpgradeHelper"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1377
    return-void

    .line 1379
    :cond_6
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    .line 1380
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    const/4 v4, 0x0

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 1381
    invoke-virtual {v5, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1382
    const-string v2, "give up keyboard upgrade : wrong type"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    return-void

    .line 1385
    :cond_7
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    if-eqz v3, :cond_8

    invoke-virtual {v0, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->isLowerVersionThan(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1386
    const-string v2, "give up keyboard upgrade : upper version"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    return-void

    .line 1389
    :cond_8
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1390
    const v6, 0x110f024e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1389
    invoke-static {v3, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 1391
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1392
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 1393
    invoke-virtual {v0, v3, v5, v6, v7}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1394
    const-string v2, "keyboard upgrade success"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1395
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1396
    const v3, 0x110f024f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1395
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1397
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1398
    iput v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    .line 1399
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetMcuReset()V

    goto :goto_0

    .line 1401
    :cond_9
    const-string v3, "keyboard upgrade failed"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1402
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1403
    const v5, 0x110f024d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1402
    invoke-static {v1, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1404
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1405
    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    .line 1406
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbHighVersion:Ljava/lang/String;

    .line 1407
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbLowVersion:Ljava/lang/String;

    .line 1409
    :goto_0
    return-void

    .line 1355
    .end local v0    # "keyboardUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
    :cond_a
    :goto_1
    const-string v0, "invalid keyboard version"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1356
    return-void
.end method

.method private doStartMcuUpgrade()V
    .locals 6

    .line 1317
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    const-string v1, "MiuiPadKeyboardManager"

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinMcuVersion:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1318
    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 1319
    const-string v0, "no need to start mcu upgrade"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1320
    return-void

    .line 1322
    :cond_0
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I

    const/4 v2, 0x5

    if-le v0, v2, :cond_1

    .line 1323
    const-string/jumbo v0, "upgrade mcu failed too many times"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    return-void

    .line 1326
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v2, "0000000000000000"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 1331
    :cond_2
    new-instance v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;-><init>(Landroid/content/Context;)V

    .line 1332
    .local v0, "mcuUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getVersion()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinMcuVersion:Ljava/lang/String;

    .line 1333
    const-string v3, "XM2021"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1334
    const-string v2, "give up upgrade : invalid version head"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    return-void

    .line 1337
    :cond_3
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    .line 1338
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->isLowerVersionThan(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1339
    const-string v2, "give up upgrade : upper version"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    return-void

    .line 1342
    :cond_4
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 1343
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1344
    const-string/jumbo v2, "upgrade mcu success"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I

    goto :goto_0

    .line 1347
    :cond_5
    const-string/jumbo v2, "upgrade mcu failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I

    .line 1349
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinMcuVersion:Ljava/lang/String;

    .line 1351
    :goto_0
    return-void

    .line 1327
    .end local v0    # "mcuUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;
    :cond_6
    :goto_1
    const-string/jumbo v0, "unknown mcu version"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    return-void
.end method

.method private doStartWirelessUpgrade()V
    .locals 7

    .line 1443
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mWirelessVersion:Ljava/lang/String;

    const-string v1, "MiuiPadKeyboardManager"

    if-eqz v0, :cond_5

    const-string v2, "0000"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 1447
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinWirelessVersion:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mWirelessVersion:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 1448
    const-string v0, "no need to start wireless upgrade"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1449
    return-void

    .line 1451
    :cond_1
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    const/4 v2, 0x5

    if-le v0, v2, :cond_2

    .line 1452
    const-string/jumbo v0, "upgrade keyboard failed too many times"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1453
    return-void

    .line 1456
    :cond_2
    new-instance v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->WL_BIN_PATH:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1458
    .local v0, "wirelessUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinWirelessVersion:Ljava/lang/String;

    .line 1459
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    .line 1460
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mWirelessVersion:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->isLowerVersionThan(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1461
    const-string v2, "give up wireless upgrade : upper version"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    return-void

    .line 1464
    :cond_3
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1465
    const v4, 0x110f024e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1464
    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 1466
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1467
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 1468
    invoke-virtual {v0, v2, v3, v5, v6}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1469
    const-string/jumbo v2, "wireless upgrade success"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1470
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1471
    const v3, 0x110f024f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1470
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1472
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1473
    iput v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    .line 1474
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetMcuReset()V

    goto :goto_0

    .line 1476
    :cond_4
    const-string/jumbo v2, "wireless upgrade failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1478
    const v3, 0x110f024d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1477
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1479
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1480
    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I

    .line 1481
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinWirelessVersion:Ljava/lang/String;

    .line 1483
    :goto_0
    return-void

    .line 1444
    .end local v0    # "wirelessUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
    :cond_5
    :goto_1
    const-string/jumbo v0, "unknown wireless version"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1445
    return-void
.end method

.method private doWritePadKeyBoardStatus(II)V
    .locals 6
    .param p1, "target"    # I
    .param p2, "value"    # I

    .line 1005
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 1009
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 1010
    invoke-static {p1, p2}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandWriteKeyboardStatus(II)[B

    move-result-object v0

    .line 1011
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1013
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    .line 1014
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1015
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 1016
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1017
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseWriteKeyBoardStatus([BII)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1018
    return-void

    .line 1022
    :cond_2
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send write mcu failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    const/16 v3, 0xc8

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 1013
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1026
    .end local v2    # "i":I
    :cond_4
    return-void

    .line 1006
    .end local v0    # "command":[B
    :cond_5
    :goto_1
    return-void
.end method

.method private getDeviceReadyForReport()Z
    .locals 6

    .line 1486
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDeviceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1487
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v5, :cond_0

    .line 1489
    invoke-virtual {v3, v4, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    .line 1490
    monitor-exit v0

    return v2

    .line 1493
    :cond_0
    const/4 v3, 0x0

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getUsbDevice()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1494
    monitor-exit v0

    return v3

    .line 1497
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getReportEndpoint(Landroid/hardware/usb/UsbDevice;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1498
    const-string v1, "MiuiPadKeyboardManager"

    const-string v2, "get usb report endpoint fail"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    monitor-exit v0

    return v3

    .line 1501
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v1, :cond_3

    .line 1502
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbManager:Landroid/hardware/usb/UsbManager;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1, v4}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 1504
    :cond_3
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v4, :cond_5

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v5, :cond_4

    goto :goto_0

    .line 1509
    :cond_4
    invoke-virtual {v1, v4, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    .line 1510
    monitor-exit v0

    .line 1511
    return v2

    .line 1505
    :cond_5
    :goto_0
    const-string v1, "MiuiPadKeyboardManager"

    const-string v2, "get usb report connection fail"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1506
    monitor-exit v0

    return v3

    .line 1510
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getDeviceReadyForTransfer()Z
    .locals 6

    .line 1123
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDeviceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1124
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v5, :cond_0

    .line 1126
    invoke-virtual {v3, v4, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    .line 1127
    monitor-exit v0

    return v2

    .line 1130
    :cond_0
    const/4 v3, 0x0

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getUsbDevice()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1131
    monitor-exit v0

    return v3

    .line 1134
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getTransferEndpoint(Landroid/hardware/usb/UsbDevice;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1135
    const-string v1, "MiuiPadKeyboardManager"

    const-string v2, "get transfer endpoint failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    monitor-exit v0

    return v3

    .line 1138
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v1, :cond_3

    .line 1139
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbManager:Landroid/hardware/usb/UsbManager;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1, v4}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 1141
    :cond_3
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-nez v4, :cond_4

    goto :goto_0

    .line 1146
    :cond_4
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v1, v3, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    .line 1147
    monitor-exit v0

    .line 1148
    return v2

    .line 1142
    :cond_5
    :goto_0
    const-string v1, "MiuiPadKeyboardManager"

    const-string v2, "get usb transfer connection failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    monitor-exit v0

    return v3

    .line 1147
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 160
    sget-object v0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    if-nez v0, :cond_1

    .line 161
    const-class v0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    monitor-enter v0

    .line 162
    :try_start_0
    sget-object v1, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    if-nez v1, :cond_0

    .line 163
    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    .line 165
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 167
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    return-object v0
.end method

.method private getKeyboardTypeValue()I
    .locals 2

    .line 919
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 920
    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 919
    :goto_0
    return v0
.end method

.method private getKeyboardUpgradeHelper()Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
    .locals 6

    .line 1412
    const/4 v0, 0x0

    .line 1413
    .local v0, "helper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    .line 1434
    :pswitch_0
    new-instance v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_H_BIN_PATH:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, v1

    .line 1435
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbHighVersion:Ljava/lang/String;

    .line 1436
    goto/16 :goto_0

    .line 1419
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 1420
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1421
    .local v1, "lowKbType":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->FIRST_LOW_KB_TYPE:Ljava/lang/String;

    .line 1422
    invoke-virtual {v2, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    .line 1423
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    invoke-virtual {v5, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1425
    :cond_0
    sget-object v2, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_L_BIN_PATH_MAP:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1426
    .local v2, "lowBinPath":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 1427
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unhandled low keyboard type:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", stop upgrade"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiPadKeyboardManager"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1428
    goto :goto_0

    .line 1430
    :cond_1
    new-instance v3, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, v3

    .line 1431
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbLowVersion:Ljava/lang/String;

    .line 1432
    goto :goto_0

    .line 1415
    .end local v1    # "lowKbType":Ljava/lang/String;
    .end local v2    # "lowBinPath":Ljava/lang/String;
    :pswitch_2
    new-instance v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->KB_BIN_PATH:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, v1

    .line 1416
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mBinKbVersion:Ljava/lang/String;

    .line 1417
    nop

    .line 1439
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getReportEndpoint(Landroid/hardware/usb/UsbDevice;)Z
    .locals 6
    .param p1, "device"    # Landroid/hardware/usb/UsbDevice;

    .line 1515
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 1516
    return v0

    .line 1519
    :cond_0
    invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getConfiguration(I)Landroid/hardware/usb/UsbConfiguration;

    move-result-object v1

    .line 1520
    .local v1, "configuration":Landroid/hardware/usb/UsbConfiguration;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/hardware/usb/UsbConfiguration;->getInterfaceCount()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1521
    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbConfiguration;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v3

    .line 1523
    .local v3, "anInterface":Landroid/hardware/usb/UsbInterface;
    invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1524
    invoke-virtual {v3, v0}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 1526
    if-eqz v4, :cond_1

    .line 1527
    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mReportInterface:Landroid/hardware/usb/UsbInterface;

    .line 1528
    return v5

    .line 1520
    .end local v3    # "anInterface":Landroid/hardware/usb/UsbInterface;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1532
    .end local v2    # "i":I
    :cond_2
    return v0
.end method

.method private getTransferEndpoint(Landroid/hardware/usb/UsbDevice;)Z
    .locals 8
    .param p1, "device"    # Landroid/hardware/usb/UsbDevice;

    .line 1177
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 1178
    return v0

    .line 1181
    :cond_0
    invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getConfiguration(I)Landroid/hardware/usb/UsbConfiguration;

    move-result-object v1

    .line 1182
    .local v1, "configuration":Landroid/hardware/usb/UsbConfiguration;
    const/4 v2, 0x0

    .local v2, "interfaceNum":I
    :goto_0
    invoke-virtual {v1}, Landroid/hardware/usb/UsbConfiguration;->getInterfaceCount()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 1183
    invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbConfiguration;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v3

    .line 1184
    .local v3, "anInterface":Landroid/hardware/usb/UsbInterface;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    .line 1185
    goto :goto_3

    .line 1187
    :cond_1
    const/4 v4, 0x0

    .local v4, "endPointNum":I
    :goto_1
    invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v5

    if-ge v4, v5, :cond_5

    .line 1188
    invoke-virtual {v3, v4}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v5

    .line 1189
    .local v5, "endpoint":Landroid/hardware/usb/UsbEndpoint;
    if-nez v5, :cond_2

    .line 1190
    goto :goto_2

    .line 1192
    :cond_2
    invoke-virtual {v5}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v6

    .line 1193
    .local v6, "direction":I
    const/16 v7, 0x80

    if-ne v6, v7, :cond_3

    .line 1194
    iput-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_2

    .line 1195
    :cond_3
    if-nez v6, :cond_4

    .line 1196
    iput-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 1187
    .end local v5    # "endpoint":Landroid/hardware/usb/UsbEndpoint;
    .end local v6    # "direction":I
    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1199
    .end local v4    # "endPointNum":I
    :cond_5
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    if-eqz v4, :cond_6

    .line 1200
    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbInterface:Landroid/hardware/usb/UsbInterface;

    .line 1201
    const/4 v0, 0x1

    return v0

    .line 1182
    .end local v3    # "anInterface":Landroid/hardware/usb/UsbInterface;
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1204
    .end local v2    # "interfaceNum":I
    :cond_7
    return v0
.end method

.method private getUsbDevice()Z
    .locals 7

    .line 1152
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDeviceLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1153
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v1

    .line 1154
    .local v1, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/usb/UsbDevice;

    .line 1155
    .local v3, "device":Landroid/hardware/usb/UsbDevice;
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v4

    const/16 v5, 0x3206

    if-ne v4, v5, :cond_0

    .line 1156
    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v4

    const/16 v5, 0x3ffc

    if-ne v4, v5, :cond_0

    .line 1157
    const-string v4, "MiuiPadKeyboardManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getUsbDevice: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 1160
    .end local v3    # "device":Landroid/hardware/usb/UsbDevice;
    :cond_0
    goto :goto_0

    .line 1161
    .end local v1    # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1163
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    const/16 v1, 0xc

    if-nez v0, :cond_2

    .line 1164
    const-string v0, "MiuiPadKeyboardManager"

    const-string v2, "get usb device failed"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1166
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 1170
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeMessages(I)V

    .line 1173
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    return v0

    .line 1161
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private isUserSetUp()Z
    .locals 4

    .line 1696
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z

    if-nez v0, :cond_1

    .line 1697
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "user_setup_complete"

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z

    .line 1700
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z

    return v0
.end method

.method private notifyKeyboardStateChanged(Z)V
    .locals 4
    .param p1, "newState"    # Z

    .line 714
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    .line 715
    .local v0, "oldState":Z
    if-ne v0, p1, :cond_0

    .line 716
    return-void

    .line 718
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    .line 719
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keyboardStatus changed :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "notifyIgnoredInputDevicesChanged"

    invoke-static {v1, v3, v2}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 723
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onKeyboardAttach()V

    .line 726
    :cond_1
    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 727
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onKeyboardDetach()V

    .line 730
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v1, :cond_3

    .line 731
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAccSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 733
    :cond_3
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v1, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 735
    :goto_0
    return-void
.end method

.method private onKeyboardAttach()V
    .locals 4

    .line 747
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "onKeyboardAttach"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecentConnTime:Ljava/util/Map;

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 749
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readKeyboardVersion()V

    .line 750
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    .line 751
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 752
    :cond_0
    move v0, v2

    :goto_0
    nop

    .line 753
    .local v0, "value":I
    const/16 v3, 0x23

    invoke-virtual {p0, v3, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    .line 755
    .end local v0    # "value":I
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->isUserSetUp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 756
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->startKeyboardUpgrade()V

    .line 757
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->startWirelessUpgrade()V

    .line 758
    invoke-direct {p0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->checkKeyboardIdentity(IZ)V

    .line 760
    :cond_2
    return-void
.end method

.method private onKeyboardDetach()V
    .locals 6

    .line 763
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "onKeyboardDetach"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecentConnTime:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Ljava/util/Map$Entry;

    .line 765
    .local v0, "entries":[Ljava/util/Map$Entry;, "[Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecentConnTime:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 766
    array-length v1, v0

    const/4 v2, 0x1

    if-lez v1, :cond_0

    .line 767
    array-length v1, v0

    sub-int/2addr v1, v2

    aget-object v1, v0, v1

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 769
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, -0x2

    const-string v5, "keyboard_type_level"

    invoke-static {v1, v5, v3, v4}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 771
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 773
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 774
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 775
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    .line 777
    :cond_1
    return-void
.end method

.method private onUsbDeviceAttach()V
    .locals 2

    .line 376
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "mcu usb device attach"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeMessages(I)V

    .line 378
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 379
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardReportData()V

    .line 380
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readMcuVersion()V

    .line 381
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->isUserSetUp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->startMcuUpgrade()V

    .line 384
    :cond_0
    return-void
.end method

.method private onUsbDeviceDetach()V
    .locals 2

    .line 387
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "mcu usb device detach"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v0, :cond_0

    .line 389
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->notifyKeyboardStateChanged(Z)V

    .line 391
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z

    .line 392
    return-void
.end method

.method private parseConnectState([B)Z
    .locals 10
    .param p1, "recBuf"    # [B

    .line 670
    const/4 v0, 0x4

    aget-byte v1, p1, v0

    const/16 v2, -0x5e

    const-string v3, "%02x"

    const-string v4, "MiuiPadKeyboardManager"

    const/4 v5, 0x0

    if-eq v1, v2, :cond_0

    .line 671
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive connect state error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v0, p1, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    return v5

    .line 674
    :cond_0
    const/4 v1, 0x2

    new-array v2, v1, [B

    .line 675
    .local v2, "voltage":[B
    const/16 v6, 0xa

    array-length v7, v2

    invoke-static {p1, v6, v2, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 676
    new-array v1, v1, [B

    .line 677
    .local v1, "clockOff":[B
    const/16 v6, 0xc

    array-length v7, v1

    invoke-static {p1, v6, v1, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 678
    new-array v0, v0, [B

    .line 679
    .local v0, "trueOff":[B
    const/16 v6, 0xe

    array-length v7, v0

    invoke-static {p1, v6, v0, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 680
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "receive connect state:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x9

    aget-byte v8, p1, v7

    invoke-static {v8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    filled-new-array {v8}, [Ljava/lang/Object;

    move-result-object v8

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "  Voltage:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 681
    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "  E_UART:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 682
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "  E_PPM:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 683
    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 680
    invoke-static {v4, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    const/16 v6, 0x12

    aget-byte v6, p1, v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_1

    .line 685
    const-string v3, "keyboard is over charged"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    return v5

    .line 688
    :cond_1
    aget-byte v6, p1, v7

    and-int/lit8 v6, v6, 0x3

    if-ne v6, v8, :cond_2

    .line 689
    const-string v3, "TRX check failed"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    return v5

    .line 692
    :cond_2
    aget-byte v6, p1, v7

    and-int/lit8 v6, v6, 0x63

    const/16 v9, 0x43

    if-ne v6, v9, :cond_3

    .line 693
    const-string v3, "pin connect failed"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 695
    const v6, 0x110f0249

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 694
    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 696
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 697
    return v5

    .line 700
    :cond_3
    aget-byte v6, p1, v7

    and-int/lit8 v6, v6, 0x3

    if-nez v6, :cond_4

    .line 701
    invoke-direct {p0, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->notifyKeyboardStateChanged(Z)V

    .line 702
    return v8

    .line 704
    :cond_4
    aget-byte v6, p1, v7

    and-int/lit8 v6, v6, 0x63

    const/16 v9, 0x23

    if-ne v6, v9, :cond_5

    .line 705
    invoke-direct {p0, v8}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->notifyKeyboardStateChanged(Z)V

    .line 706
    return v8

    .line 709
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unhandled connect state:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-byte v7, p1, v7

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    filled-new-array {v7}, [Ljava/lang/Object;

    move-result-object v7

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    return v5
.end method

.method private parseGsensorData([B)V
    .locals 11
    .param p1, "recBuf"    # [B

    .line 1254
    const/16 v0, 0xe

    aget-byte v1, p1, v0

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    const-string v1, "MiuiPadKeyboardManager"

    if-nez v0, :cond_0

    .line 1255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive gsensor data checksum error:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, p1

    .line 1256
    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1255
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    return-void

    .line 1260
    :cond_0
    const/4 v0, 0x2

    new-array v3, v0, [B

    .line 1264
    .local v3, "temp":[B
    const/16 v4, 0x8

    array-length v5, v3

    invoke-static {p1, v4, v3, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1265
    const/4 v4, 0x1

    aget-byte v5, v3, v4

    and-int/lit16 v5, v5, 0x80

    const-string v6, "FFFF"

    const/16 v7, 0x10

    if-eqz v5, :cond_1

    .line 1266
    new-instance v5, Ljava/math/BigInteger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8, v7}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v5}, Ljava/math/BigInteger;->intValue()I

    move-result v5

    int-to-float v5, v5

    .local v5, "x":F
    goto :goto_0

    .line 1268
    .end local v5    # "x":F
    :cond_1
    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-float v5, v5

    .line 1271
    .restart local v5    # "x":F
    :goto_0
    const/16 v8, 0xa

    array-length v9, v3

    invoke-static {p1, v8, v3, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1272
    aget-byte v8, v3, v4

    and-int/lit16 v8, v8, 0x80

    if-eqz v8, :cond_2

    .line 1273
    new-instance v8, Ljava/math/BigInteger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v7}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v8}, Ljava/math/BigInteger;->intValue()I

    move-result v8

    int-to-float v8, v8

    .local v8, "y":F
    goto :goto_1

    .line 1275
    .end local v8    # "y":F
    :cond_2
    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v8

    int-to-float v8, v8

    .line 1278
    .restart local v8    # "y":F
    :goto_1
    const/16 v9, 0xc

    array-length v10, v3

    invoke-static {p1, v9, v3, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1279
    aget-byte v9, v3, v4

    and-int/lit16 v9, v9, 0x80

    if-eqz v9, :cond_3

    .line 1280
    new-instance v9, Ljava/math/BigInteger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, v6, v7}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v9}, Ljava/math/BigInteger;->intValue()I

    move-result v6

    int-to-float v6, v6

    .local v6, "z":F
    goto :goto_2

    .line 1282
    .end local v6    # "z":F
    :cond_3
    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    int-to-float v6, v6

    .line 1285
    .restart local v6    # "z":F
    :goto_2
    const v7, 0x411ccccd    # 9.8f

    mul-float v9, v5, v7

    const/high16 v10, 0x45000000    # 2048.0f

    div-float/2addr v9, v10

    .line 1286
    .end local v5    # "x":F
    .local v9, "x":F
    mul-float v5, v8, v7

    div-float/2addr v5, v10

    .line 1287
    .end local v8    # "y":F
    .local v5, "y":F
    mul-float/2addr v7, v6

    div-float/2addr v7, v10

    .line 1288
    .end local v6    # "z":F
    .local v7, "z":F
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mLocalGData:[F

    aget v2, v6, v2

    .line 1289
    .local v2, "xLocal":F
    aget v4, v6, v4

    .line 1290
    .local v4, "yLocal":F
    aget v0, v6, v0

    .line 1291
    .local v0, "zLocal":F
    invoke-static {v9, v2, v7, v0}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->calculatePKAngle(FFFF)F

    move-result v6

    .line 1292
    .local v6, "resultAngle":F
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "result angle = "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1293
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v6}, Lcom/android/server/input/padkeyboard/AngleStateController;->updateAngleState(F)V

    .line 1294
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 1295
    return-void
.end method

.method private parseHallState([B)V
    .locals 3
    .param p1, "recBuf"    # [B

    .line 1298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Receive Hall State:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x8

    aget-byte v1, p1, v1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "%02x"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1299
    return-void
.end method

.method private parseKeyboardType()I
    .locals 4

    .line 904
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    const/4 v1, 0x2

    if-nez v0, :cond_0

    .line 905
    return v1

    .line 907
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 908
    .local v0, "type":Ljava/lang/String;
    const-string v2, "1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 909
    return v3

    .line 910
    :cond_1
    const-string v2, "2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "4"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 912
    :cond_2
    const-string v2, "3"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 913
    const/4 v1, 0x3

    return v1

    .line 915
    :cond_3
    return v1

    .line 911
    :cond_4
    :goto_0
    return v1
.end method

.method private parseKeyboardVersion([B)Z
    .locals 9
    .param p1, "recBuf"    # [B

    .line 881
    const/4 v0, 0x4

    aget-byte v1, p1, v0

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v4, :cond_0

    .line 882
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive keyboard version error:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v0, p1, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v4, "%02x"

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    return v3

    .line 886
    :cond_0
    const/4 v0, 0x2

    new-array v1, v0, [B

    .line 887
    .local v1, "kbVersion":[B
    const/4 v5, 0x6

    array-length v6, v1

    invoke-static {p1, v5, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 888
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 889
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "receive keyboard version:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseKeyboardType()I

    move-result v5

    iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    .line 891
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    iget-boolean v7, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldWakeUp:Z

    if-eqz v7, :cond_1

    iget-boolean v7, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mScreenState:Z

    if-nez v7, :cond_1

    move v7, v4

    goto :goto_0

    :cond_1
    move v7, v3

    :goto_0
    invoke-virtual {v6, v5, v7}, Lcom/android/server/input/padkeyboard/AngleStateController;->setKbLevel(IZ)V

    .line 892
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 893
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 894
    iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I

    if-ne v6, v0, :cond_2

    move v6, v3

    goto :goto_1

    :cond_2
    move v6, v4

    .line 893
    :goto_1
    const-string v7, "keyboard_type_level"

    const/4 v8, -0x2

    invoke-static {v5, v7, v6, v8}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 896
    new-array v0, v0, [B

    .line 897
    .local v0, "wlVersion":[B
    const/16 v5, 0x8

    array-length v6, v0

    invoke-static {p1, v5, v0, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 898
    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2RevertHexString([B)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mWirelessVersion:Ljava/lang/String;

    .line 899
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "receive wireless version:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mWirelessVersion:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    return v4
.end method

.method private parsePenState([B)V
    .locals 5
    .param p1, "recBuf"    # [B

    .line 1302
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Receive Pen State:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x8

    aget-byte v2, p1, v1

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " PenBatteryState:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x9

    aget-byte v4, p1, v2

    .line 1303
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1302
    const-string v3, "MiuiPadKeyboardManager"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1304
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mPenState:I

    aget-byte v3, p1, v1

    if-ne v0, v3, :cond_0

    .line 1305
    return-void

    .line 1307
    :cond_0
    aget-byte v0, p1, v1

    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mPenState:I

    .line 1309
    aget-byte v0, p1, v1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 1310
    const/4 v0, 0x4

    aget-byte v1, p1, v2

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendPenBatteryState(II)V

    goto :goto_0

    .line 1311
    :cond_1
    aget-byte v0, p1, v1

    if-nez v0, :cond_2

    .line 1312
    aget-byte v0, p1, v2

    invoke-direct {p0, v3, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendPenBatteryState(II)V

    .line 1314
    :cond_2
    :goto_0
    return-void
.end method

.method private parseReadKeyboardStatus([B)Z
    .locals 5
    .param p1, "recBuf"    # [B

    .line 948
    const/4 v0, 0x7

    aget-byte v1, p1, v0

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 949
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive mcu state error:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v0, p1, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v4, "%02x"

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    return v3

    .line 952
    :cond_0
    const/16 v0, 0x8

    aget-byte v1, p1, v0

    invoke-static {p1, v3, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 953
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive mcu checksum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    .line 954
    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 953
    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    return v3

    .line 958
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->receiveKeyboardStatus()Z

    move-result v0

    return v0
.end method

.method private parseReceiveKeyboardStatus([B)Z
    .locals 5
    .param p1, "recBuf"    # [B

    .line 981
    const/4 v0, 0x4

    aget-byte v0, p1, v0

    const/16 v1, 0x52

    const/4 v2, 0x0

    const-string v3, "MiuiPadKeyboardManager"

    if-eq v0, v1, :cond_0

    .line 982
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive keyboard state error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    .line 983
    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 982
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    return v2

    .line 986
    :cond_0
    const/16 v0, 0x13

    aget-byte v1, p1, v0

    invoke-static {p1, v2, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 987
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive keyboard checksum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    .line 988
    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 987
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    return v2

    .line 991
    :cond_1
    const/4 v0, 0x6

    new-array v1, v0, [B

    .line 992
    .local v1, "gsensorData":[B
    const/16 v4, 0xa

    invoke-static {p1, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 993
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive TouchPadEnable:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v0, p1, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive KeyBoardEnable:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive BackLightEnable:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x8

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive TouchPadSensitivity:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x9

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive GsensorData:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v2, v1

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive PenState:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x10

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive PenBatteryState:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x11

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive PowerState:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x12

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    const/4 v0, 0x1

    return v0
.end method

.method private parseReceiveWriteCmdAck([BII)Z
    .locals 4
    .param p1, "recBuf"    # [B
    .param p2, "target"    # I
    .param p3, "value"    # I

    .line 1097
    const/4 v0, 0x4

    aget-byte v0, p1, v0

    const/16 v1, 0x30

    const/4 v2, 0x0

    const-string v3, "MiuiPadKeyboardManager"

    if-eq v0, v1, :cond_0

    .line 1098
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive keyboard write result status error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    .line 1099
    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1098
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    return v2

    .line 1102
    :cond_0
    const/16 v0, 0x8

    aget-byte v1, p1, v0

    invoke-static {p1, v2, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive keyboard write result checksum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    .line 1104
    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1103
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    return v2

    .line 1107
    :cond_1
    const/4 v0, 0x6

    aget-byte v0, p1, v0

    if-ne p2, v0, :cond_2

    const/4 v0, 0x7

    aget-byte v0, p1, v0

    if-ne p3, v0, :cond_2

    .line 1108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "write cmd success, command:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1110
    :cond_2
    const-string/jumbo v0, "write cmd failed"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private parseReportData([B)V
    .locals 2
    .param p1, "recBuf"    # [B

    .line 1225
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    const/16 v1, 0x26

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    aget-byte v0, p1, v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_0

    .line 1226
    const/4 v0, 0x4

    aget-byte v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1232
    :sswitch_0
    const/4 v0, 0x6

    aget-byte v0, p1, v0

    packed-switch v0, :pswitch_data_0

    .line 1243
    goto :goto_0

    .line 1240
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parsePenState([B)V

    .line 1241
    goto :goto_0

    .line 1237
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseHallState([B)V

    .line 1238
    goto :goto_0

    .line 1234
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseGsensorData([B)V

    .line 1235
    goto :goto_0

    .line 1228
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseConnectState([B)Z

    .line 1229
    nop

    .line 1251
    :cond_0
    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5e -> :sswitch_1
        0x69 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch -0x60
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private parseUsbDeviceVersion([B)Z
    .locals 6
    .param p1, "recBuf"    # [B

    .line 804
    const/4 v0, 0x6

    aget-byte v1, p1, v0

    const/4 v2, 0x2

    const-string v3, "%02x"

    const-string v4, "MiuiPadKeyboardManager"

    const/4 v5, 0x0

    if-eq v1, v2, :cond_0

    .line 805
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive version state error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v0, p1, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    return v5

    .line 808
    :cond_0
    const/16 v0, 0x1e

    aget-byte v1, p1, v0

    invoke-static {p1, v5, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v1

    if-nez v1, :cond_1

    .line 809
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive version checksum error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v0, p1, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    return v5

    .line 813
    :cond_1
    const/16 v0, 0x10

    new-array v1, v0, [B

    .line 814
    .local v1, "deviceVersion":[B
    const/4 v2, 0x7

    invoke-static {p1, v2, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 815
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2String([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    .line 816
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive mcu version:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const/4 v0, 0x1

    return v0
.end method

.method private parseUsbReset([B)Z
    .locals 4
    .param p1, "recBuf"    # [B

    .line 850
    const/4 v0, 0x0

    aget-byte v1, p1, v0

    const/16 v2, 0x24

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    aget-byte v1, p1, v3

    const/16 v2, 0x31

    if-ne v1, v2, :cond_1

    const/4 v1, 0x6

    aget-byte v1, p1, v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 854
    :cond_0
    return v0

    .line 851
    :cond_1
    :goto_0
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "receive reset success"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    return v3
.end method

.method private parseWriteCmdAck([BII)Z
    .locals 4
    .param p1, "recBuf"    # [B
    .param p2, "target"    # I
    .param p3, "value"    # I

    .line 1066
    const/4 v0, 0x7

    aget-byte v0, p1, v0

    const-string v1, "MiuiPadKeyboardManager"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 1067
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive write cmd ack error:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v3, p1

    .line 1068
    invoke-static {p1, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1067
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    return v2

    .line 1072
    :cond_0
    const/16 v0, 0x8

    aget-byte v3, p1, v0

    invoke-static {p1, v2, v0, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1073
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive write cmd ack checksum error:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v3, p1

    .line 1074
    invoke-static {p1, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1073
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    return v2

    .line 1078
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->receiveWriteCmdAck(II)Z

    move-result v0

    return v0
.end method

.method private parseWriteKeyBoardStatus([BII)Z
    .locals 5
    .param p1, "recBuf"    # [B
    .param p2, "target"    # I
    .param p3, "value"    # I

    .line 1029
    const/4 v0, 0x7

    aget-byte v1, p1, v0

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 1030
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive mcu state error:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v0, p1, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v4, "%02x"

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    return v3

    .line 1033
    :cond_0
    const/16 v0, 0x8

    aget-byte v1, p1, v0

    invoke-static {p1, v3, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1034
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive mcu checksum error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p1

    .line 1035
    invoke-static {p1, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1034
    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    return v3

    .line 1038
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readWriteCmdAck(II)Z

    move-result v0

    return v0
.end method

.method private processIdentity(I)V
    .locals 5
    .param p1, "identity"    # I

    .line 1620
    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, "MiuiPadKeyboardManager"

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1648
    :pswitch_0
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->showRejectConfirmDialog(I)V

    .line 1649
    const-string v1, "keyboard identity transfer error"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1651
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 1652
    goto :goto_0

    .line 1643
    :pswitch_1
    const-string v1, "keyboard identity internal error"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1644
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1645
    goto :goto_0

    .line 1634
    :pswitch_2
    iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I

    const/4 v4, 0x5

    if-ge v3, v4, :cond_0

    .line 1635
    const/16 v3, 0x1388

    invoke-direct {p0, v3, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->checkKeyboardIdentity(IZ)V

    .line 1636
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I

    .line 1638
    :cond_0
    const-string v0, "keyboard identity need check again"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1640
    goto :goto_0

    .line 1627
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->showRejectConfirmDialog(I)V

    .line 1628
    const-string v1, "keyboard identity auth reject"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1630
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 1631
    goto :goto_0

    .line 1622
    :pswitch_4
    const-string v0, "keyboard identity auth ok"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1623
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1624
    nop

    .line 1656
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private readWriteCmdAck(II)Z
    .locals 6
    .param p1, "target"    # I
    .param p2, "value"    # I

    .line 1042
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 1046
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 1047
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardUtil;->commandGetKeyboardStatus(I)[B

    move-result-object v0

    .line 1048
    .local v0, "command":[B
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v3, v0

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1050
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    .line 1051
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1052
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v3, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 1053
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1054
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v3, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseWriteCmdAck([BII)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1055
    const/4 v1, 0x1

    return v1

    .line 1059
    :cond_2
    const-string v3, "MiuiPadKeyboardManager"

    const-string/jumbo v4, "send write cmd ack failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1062
    .end local v2    # "i":I
    :cond_4
    return v1

    .line 1043
    .end local v0    # "command":[B
    :cond_5
    :goto_1
    return v1
.end method

.method private receiveKeyboardStatus()Z
    .locals 5

    .line 962
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 966
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_3

    .line 967
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 968
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 969
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReceiveKeyboardStatus([B)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 970
    const/4 v1, 0x1

    return v1

    .line 973
    :cond_1
    const-string v2, "MiuiPadKeyboardManager"

    const-string v3, "receive keyboard status failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    :cond_2
    const/16 v2, 0x64

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 966
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 977
    .end local v0    # "i":I
    :cond_3
    return v1

    .line 963
    :cond_4
    :goto_1
    return v1
.end method

.method private receiveWriteCmdAck(II)Z
    .locals 4
    .param p1, "target"    # I
    .param p2, "value"    # I

    .line 1082
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    if-ge v0, v1, :cond_2

    .line 1083
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([BB)V

    .line 1084
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1085
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v1, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReceiveWriteCmdAck([BII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1086
    const/4 v1, 0x1

    return v1

    .line 1089
    :cond_0
    const-string v1, "MiuiPadKeyboardManager"

    const-string v2, "receive keyboard write result failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    const/16 v1, 0x64

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->operationWait(I)V

    .line 1082
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1093
    .end local v0    # "i":I
    :cond_2
    return v2
.end method

.method private registerBroadcastReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 178
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "registerBroadcastReceiver"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    new-instance v0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbActionReceiver:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;

    .line 180
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 181
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 182
    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 183
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbActionReceiver:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 184
    return-void
.end method

.method private sendPenBatteryState(II)V
    .locals 3
    .param p1, "penState"    # I
    .param p2, "penBatteryState"    # I

    .line 1601
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1602
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1603
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.stylus.MiuiStylusReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1605
    const-string v1, "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1606
    const-string v1, "miui.intent.extra.REVERSE_PEN_SOC"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1607
    const-string/jumbo v1, "source"

    const-string v2, "keyboard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1608
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1609
    const-string v1, "MiuiPadKeyboardManager"

    const-string v2, "pen battery state send"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1610
    return-void
.end method

.method private sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z
    .locals 3
    .param p1, "connection"    # Landroid/hardware/usb/UsbDeviceConnection;
    .param p2, "endpoint"    # Landroid/hardware/usb/UsbEndpoint;
    .param p3, "data"    # [B

    .line 1116
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    goto :goto_0

    .line 1119
    :cond_0
    array-length v1, p3

    const/16 v2, 0x1f4

    invoke-virtual {p1, p2, p3, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 1117
    :cond_2
    :goto_0
    return v0
.end method

.method public static shouldClearActivityInfoFlags()I
    .locals 1

    .line 486
    const/16 v0, 0x30

    return v0
.end method

.method private showRejectConfirmDialog(I)V
    .locals 5
    .param p1, "type"    # I

    .line 1659
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 1661
    .local v0, "systemuiContext":Landroid/content/Context;
    sparse-switch p1, :sswitch_data_0

    .line 1671
    return-void

    .line 1667
    :sswitch_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1668
    const v2, 0x110f024c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1669
    .local v1, "message":Ljava/lang/String;
    goto :goto_0

    .line 1663
    .end local v1    # "message":Ljava/lang/String;
    :sswitch_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1664
    const v2, 0x110f024b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1665
    .restart local v1    # "message":Ljava/lang/String;
    nop

    .line 1674
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v2, :cond_0

    .line 1675
    invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1677
    :cond_0
    new-instance v2, Lmiuix/appcompat/app/AlertDialog$Builder;

    const v3, 0x66110006

    invoke-direct {v2, v0, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1679
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 1680
    invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 1681
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1682
    const v4, 0x110f024a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1681
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 1683
    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    .line 1684
    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 1685
    .local v2, "attrs":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x7d3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1686
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v4, 0x20000

    or-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1687
    const/16 v3, 0x11

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1688
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v3, v3, 0x110

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 1690
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v3}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1692
    .end local v2    # "attrs":Landroid/view/WindowManager$LayoutParams;
    :goto_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->show()V

    .line 1693
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method

.method public static supportPadKeyboard()Z
    .locals 2

    .line 171
    const-string/jumbo v0, "support_usb_keyboard"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private testFunction()V
    .locals 3

    .line 273
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "---testFunction start-----"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readConnectState()V

    .line 276
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readMcuVersion()V

    .line 277
    const/16 v2, 0x23

    invoke-virtual {p0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    .line 279
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readKeyboardStatus()V

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private unRegisterBroadcastReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 190
    const-string v0, "MiuiPadKeyboardManager"

    const-string/jumbo v1, "unRegisterBroadcastReceiver"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbActionReceiver:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public commandMiAuthStep3Type1([B[B)[B
    .locals 1
    .param p1, "keyMeta"    # [B
    .param p2, "challenge"    # [B

    .line 638
    invoke-static {p1, p2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->commandMiAuthStep3Type1ForUSB([B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public commandMiAuthStep5Type1([B)[B
    .locals 1
    .param p1, "token"    # [B

    .line 643
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public commandMiDevAuthInit()[B
    .locals 1

    .line 633
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->commandMiDevAuthInitForUSB()[B

    move-result-object v0

    return-object v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 1704
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1705
    const-string v0, "MiuiPadKeyboardManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1707
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1708
    const-string v0, "mUsbDevice="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1709
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    if-eqz v0, :cond_0

    .line 1710
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[DeviceName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",VendorId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 1711
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ProductId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbDevice:Landroid/hardware/usb/UsbDevice;

    .line 1712
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1710
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 1714
    :cond_0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1717
    :goto_0
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1718
    const-string v0, "mMcuVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1719
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1721
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1722
    const-string v0, "mKeyboardVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1723
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1725
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1726
    const-string v0, "mWirelessVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1727
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mWirelessVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1729
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/padkeyboard/AngleStateController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 1731
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1732
    const-string v0, "mRecentConnTime="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1733
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecentConnTime:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1734
    return-void
.end method

.method public enableOrDisableInputDevice()V
    .locals 6

    .line 1576
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v0, v1}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    .line 1577
    .local v0, "inputDevice":Landroid/view/InputDevice;
    if-eqz v0, :cond_0

    .line 1578
    invoke-virtual {v0}, Landroid/view/InputDevice;->isEnabled()Z

    move-result v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1579
    return-void

    .line 1582
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x23

    const-string v4, "MiuiPadKeyboardManager"

    if-eqz v1, :cond_2

    .line 1583
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    iget v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v1, v5}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V

    .line 1584
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v1, :cond_1

    .line 1585
    invoke-virtual {p0, v3, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    .line 1588
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disable keyboard device id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1590
    :cond_2
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    iget v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v1, v5}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V

    .line 1591
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z

    if-eqz v1, :cond_3

    .line 1592
    const/4 v1, 0x1

    invoke-virtual {p0, v3, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    .line 1595
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable keyboard device id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    const-string v3, "notifyIgnoredInputDevicesChanged"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1598
    return-void
.end method

.method public getKeyboardReportData()V
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 348
    :cond_0
    return-void
.end method

.method public getKeyboardStatus()Lmiui/hardware/input/MiuiKeyboardStatus;
    .locals 10

    .line 450
    new-instance v9, Lmiui/hardware/input/MiuiKeyboardStatus;

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 451
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getIdentityStatus()Z

    move-result v2

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 452
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->isWorkState()Z

    move-result v0

    xor-int/lit8 v3, v0, 0x1

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 453
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getLidStatus()Z

    move-result v4

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 454
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getTabletStatus()Z

    move-result v5

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 455
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuVersion:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lmiui/hardware/input/MiuiKeyboardStatus;-><init>(ZZZZZZLjava/lang/String;Ljava/lang/String;)V

    .line 450
    return-object v9
.end method

.method public getMcuResetMode()V
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 302
    return-void
.end method

.method public isKeyboardReady()Z
    .locals 1

    .line 445
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyLidSwitchChanged(Z)V
    .locals 1
    .param p1, "lidOpen"    # Z

    .line 427
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyLidSwitchChanged(Z)V

    .line 428
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 429
    return-void
.end method

.method public notifyScreenState(Z)V
    .locals 2
    .param p1, "screenState"    # Z

    .line 472
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mScreenState:Z

    .line 473
    if-nez p1, :cond_1

    .line 474
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldWakeUp:Z

    .line 475
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z

    if-eqz v1, :cond_0

    .line 477
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    .line 483
    :cond_1
    return-void
.end method

.method public notifyTabletSwitchChanged(Z)V
    .locals 1
    .param p1, "tabletOpen"    # Z

    .line 437
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyTabletSwitchChanged(Z)V

    .line 438
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V

    .line 439
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 1545
    return-void
.end method

.method public onKeyboardAction()V
    .locals 2

    .line 264
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "onKeyboardAction"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldWakeUp:Z

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardReportData()V

    .line 270
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 1537
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1538
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mLocalGData:[F

    array-length v2, v1

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1540
    :cond_0
    return-void
.end method

.method public readConnectState()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 288
    return-void
.end method

.method public readKeyboardStatus()V
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 317
    return-void
.end method

.method public readKeyboardVersion()V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 310
    return-void
.end method

.method public readMcuVersion()V
    .locals 2

    .line 294
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 295
    return-void
.end method

.method public removeKeyboardDevicesIfNeeded([Landroid/view/InputDevice;)[Landroid/view/InputDevice;
    .locals 8
    .param p1, "allInputDevices"    # [Landroid/view/InputDevice;

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 357
    .local v0, "newInputDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/InputDevice;>;"
    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, p1, v3

    .line 358
    .local v4, "inputDevice":Landroid/view/InputDevice;
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v5

    const/16 v6, 0x3ffc

    if-ne v5, v6, :cond_2

    .line 359
    invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I

    move-result v5

    const/16 v6, 0x3206

    if-ne v5, v6, :cond_2

    .line 360
    iget v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v4}, Landroid/view/InputDevice;->getId()I

    move-result v6

    const-string v7, "MiuiPadKeyboardManager"

    if-eq v5, v6, :cond_0

    .line 361
    invoke-virtual {v4}, Landroid/view/InputDevice;->getId()I

    move-result v5

    iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    .line 362
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "update keyboard device id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_0
    iget-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v5, :cond_1

    .line 365
    return-object p1

    .line 367
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "filter keyboard device :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    goto :goto_1

    .line 370
    :cond_2
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    .end local v4    # "inputDevice":Landroid/view/InputDevice;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 372
    :cond_3
    new-array v1, v2, [Landroid/view/InputDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/view/InputDevice;

    return-object v1
.end method

.method public sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B
    .locals 7
    .param p1, "command"    # [B
    .param p2, "callback"    # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 592
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandlerThread:Landroid/os/HandlerThread;

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-eq v0, v1, :cond_0

    .line 593
    const-string/jumbo v0, "sendCommandForRespond should be called in mHandlerThread"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    new-array v0, v3, [B

    return-object v0

    .line 597
    :cond_0
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 598
    const-string v0, "getDeviceReadyForTransfer fail"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    new-array v0, v3, [B

    return-object v0

    .line 602
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 603
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v1, p1

    invoke-static {p1, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 605
    const/4 v0, 0x0

    .line 606
    .local v0, "failCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x2

    if-ge v1, v4, :cond_6

    .line 607
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mOutUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 608
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-static {v4, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 609
    :cond_2
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mUsbConnection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInUsbEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 610
    if-eqz p2, :cond_3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-interface {p2, v4}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;->isCorrectPackage([B)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 611
    :cond_3
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    return-object v2

    .line 615
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Try Send command failed:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mSendBuf:[B

    array-length v6, v5

    .line 616
    invoke-static {v5, v6}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 615
    invoke-static {v2, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 606
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 621
    .end local v1    # "i":I
    :cond_6
    if-lez v0, :cond_7

    .line 622
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->cleanUsbCash(I)V

    .line 625
    :cond_7
    if-ne v0, v4, :cond_8

    .line 626
    new-array v1, v3, [B

    return-object v1

    .line 628
    :cond_8
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mRecBuf:[B

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    return-object v1
.end method

.method public sendKeyboardCapsLock()V
    .locals 2

    .line 338
    const/16 v0, 0x26

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    .line 339
    return-void
.end method

.method public startKeyboardUpgrade()V
    .locals 2

    .line 407
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 410
    :cond_0
    return-void
.end method

.method public startMcuUpgrade()V
    .locals 2

    .line 398
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 401
    :cond_0
    return-void
.end method

.method public startWirelessUpgrade()V
    .locals 2

    .line 416
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 419
    :cond_0
    return-void
.end method

.method public writePadKeyBoardStatus(II)V
    .locals 3
    .param p1, "target"    # I
    .param p2, "value"    # I

    .line 326
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 327
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 328
    .local v1, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "target"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 329
    const-string/jumbo v2, "value"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 330
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 331
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mHandler:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    invoke-virtual {v2, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z

    .line 332
    return-void
.end method
