.class public interface abstract Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;
.super Ljava/lang/Object;
.source "MiuiPadKeyboardManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$KeyboardAuthCallback;,
        Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "MiuiPadKeyboardManager"


# direct methods
.method public static getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 157
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-static {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    move-result-object v0

    return-object v0

    .line 159
    :cond_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    invoke-static {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    move-result-object v0

    return-object v0

    .line 162
    :cond_1
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "notSupport any keyboard!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public static isXiaomiKeyboard(II)Z
    .locals 2
    .param p0, "vendorId"    # I
    .param p1, "productId"    # I

    .line 93
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 94
    invoke-static {p1, p0}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiIICExternalDevice(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    invoke-static {p1, p0}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiBLEKeyboard(II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 94
    :cond_1
    return v1

    .line 96
    :cond_2
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    invoke-static {p0, p1}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiUSBExternalDevice(II)Z

    move-result v0

    return v0

    .line 99
    :cond_3
    return v1
.end method

.method public static shouldClearActivityInfoFlags()I
    .locals 1

    .line 103
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->shouldClearActivityInfoFlags()I

    move-result v0

    return v0

    .line 105
    :cond_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->shouldClearActivityInfoFlags()I

    move-result v0

    return v0

    .line 108
    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract commandMiAuthStep3Type1([B[B)[B
.end method

.method public abstract commandMiAuthStep5Type1([B)[B
.end method

.method public abstract commandMiDevAuthInit()[B
.end method

.method public abstract dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
.end method

.method public abstract enableOrDisableInputDevice()V
.end method

.method public getKeyboardBackLightBrightness()I
    .locals 1

    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getKeyboardReportData()V
.end method

.method public abstract getKeyboardStatus()Lmiui/hardware/input/MiuiKeyboardStatus;
.end method

.method public getKeyboardType()I
    .locals 1

    .line 153
    const/4 v0, -0x1

    return v0
.end method

.method public abstract isKeyboardReady()Z
.end method

.method public abstract notifyLidSwitchChanged(Z)V
.end method

.method public abstract notifyScreenState(Z)V
.end method

.method public abstract notifyTabletSwitchChanged(Z)V
.end method

.method public readHallStatus()V
    .locals 0

    .line 138
    return-void
.end method

.method public abstract readKeyboardStatus()V
.end method

.method public abstract removeKeyboardDevicesIfNeeded([Landroid/view/InputDevice;)[Landroid/view/InputDevice;
.end method

.method public sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B
    .locals 1
    .param p1, "command"    # [B
    .param p2, "callback"    # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 112
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public setCapsLockLight(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 121
    return-void
.end method

.method public setKeyboardBackLightBrightness(I)V
    .locals 0
    .param p1, "brightness"    # I

    .line 127
    return-void
.end method

.method public setMuteLight(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 124
    return-void
.end method

.method public wakeKeyboard176()V
    .locals 0

    .line 145
    return-void
.end method
