class com.android.server.input.padkeyboard.KeyboardAuthHelper$2 implements android.content.ServiceConnection {
	 /* .source "KeyboardAuthHelper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/KeyboardAuthHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.KeyboardAuthHelper this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.KeyboardAuthHelper$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/KeyboardAuthHelper; */
/* .line 159 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 4 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 163 */
final String v0 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v0, "MiuiKeyboardManager_MiDevAuthService"
try { // :try_start_0
	 v1 = this.this$0;
	 com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$fgetmDeathRecipient ( v1 );
	 int v2 = 0; // const/4 v2, 0x0
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 166 */
	 /* .line 164 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 165 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "linkToDeath fail: "; // const-string v3, "linkToDeath fail: "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v2 );
	 /* .line 167 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
v1 = this.this$0;
com.xiaomi.devauth.IMiDevAuthInterface$Stub .asInterface ( p2 );
com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$fputmService ( v1,v2 );
/* .line 168 */
v1 = this.this$0;
com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$fgetmService ( v1 );
/* if-nez v1, :cond_0 */
/* .line 169 */
final String v1 = "Try connect midevauth service fail"; // const-string v1, "Try connect midevauth service fail"
android.util.Slog .e ( v0,v1 );
/* .line 171 */
} // :cond_0
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 175 */
v0 = this.this$0;
com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$fgetmContext ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 176 */
final String v0 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v0, "MiuiKeyboardManager_MiDevAuthService"
final String v1 = "re-bind to MiDevAuth service"; // const-string v1, "re-bind to MiDevAuth service"
android.util.Slog .i ( v0,v1 );
/* .line 177 */
v0 = this.this$0;
com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$fgetmContext ( v0 );
v1 = this.this$0;
com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$fgetmConn ( v1 );
(( android.content.Context ) v0 ).unbindService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 178 */
v0 = this.this$0;
com.android.server.input.padkeyboard.KeyboardAuthHelper .-$$Nest$minitService ( v0 );
/* .line 180 */
} // :cond_0
return;
} // .end method
