.class Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;
.super Landroid/database/ContentObserver;
.source "MiuiUsbKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 234
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 237
    const-string v0, "keyboard_back_light"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_4

    .line 238
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Landroid/content/Context;

    move-result-object v5

    .line 239
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 238
    invoke-static {v5, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    invoke-static {v1, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fputmShouldEnableBackLight(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Z)V

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mBackLightObserver onChange:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmShouldEnableBackLight(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmIsKeyboardReady(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 242
    return-void

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmAngleStateController(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/AngleStateController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    return-void

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmShouldEnableBackLight(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z

    move-result v0

    const/16 v1, 0x23

    if-eqz v0, :cond_3

    .line 248
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-virtual {v0, v1, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    goto :goto_1

    .line 251
    :cond_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V

    goto :goto_1

    .line 254
    :cond_4
    const-string v0, "keyboard_auto_upgrade"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 255
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Landroid/content/Context;

    move-result-object v5

    .line 256
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 255
    invoke-static {v5, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_5

    move v3, v4

    :cond_5
    invoke-static {v1, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fputmEnableAutoUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Z)V

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mEnableAutoUpgrade onChange:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmEnableAutoUpgrade(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_6
    :goto_1
    return-void
.end method
