public abstract class com.android.server.input.padkeyboard.MiuiPadKeyboardManager implements android.hardware.SensorEventListener {
	 /* .source "MiuiPadKeyboardManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$KeyboardAuthCallback;, */
	 /* Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String TAG;
/* # direct methods */
public static com.android.server.input.padkeyboard.MiuiPadKeyboardManager getKeyboardManager ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 157 */
	 v0 = 	 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .supportPadKeyboard ( );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 158 */
		 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .getInstance ( p0 );
		 /* .line 159 */
	 } // :cond_0
	 v0 = 	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .supportPadKeyboard ( );
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 160 */
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .getInstance ( p0 );
		 /* .line 162 */
	 } // :cond_1
	 final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
	 final String v1 = "notSupport any keyboard!"; // const-string v1, "notSupport any keyboard!"
	 android.util.Slog .e ( v0,v1 );
	 /* .line 163 */
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isXiaomiKeyboard ( Integer p0, Integer p1 ) {
	 /* .locals 2 */
	 /* .param p0, "vendorId" # I */
	 /* .param p1, "productId" # I */
	 /* .line 93 */
	 v0 = 	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .supportPadKeyboard ( );
	 int v1 = 0; // const/4 v1, 0x0
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 94 */
		 v0 = 		 miui.hardware.input.MiuiKeyboardHelper .isXiaomiIICExternalDevice ( p1,p0 );
		 /* if-nez v0, :cond_0 */
		 /* .line 95 */
		 v0 = 		 miui.hardware.input.MiuiKeyboardHelper .isXiaomiBLEKeyboard ( p1,p0 );
		 if ( v0 != null) { // if-eqz v0, :cond_1
		 } // :cond_0
		 int v1 = 1; // const/4 v1, 0x1
		 /* .line 94 */
	 } // :cond_1
	 /* .line 96 */
} // :cond_2
v0 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .supportPadKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 97 */
	 v0 = 	 miui.hardware.input.MiuiKeyboardHelper .isXiaomiUSBExternalDevice ( p0,p1 );
	 /* .line 99 */
} // :cond_3
} // .end method
public static Integer shouldClearActivityInfoFlags ( ) {
/* .locals 1 */
/* .line 103 */
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .supportPadKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 104 */
	 v0 = 	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .shouldClearActivityInfoFlags ( );
	 /* .line 105 */
} // :cond_0
v0 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .supportPadKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 106 */
	 v0 = 	 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .shouldClearActivityInfoFlags ( );
	 /* .line 108 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public abstract commandMiAuthStep3Type1 ( Object[] p0, Object[] p1 ) {
} // .end method
public abstract commandMiAuthStep5Type1 ( Object[] p0 ) {
} // .end method
public abstract commandMiDevAuthInit ( ) {
} // .end method
public abstract void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
} // .end method
public abstract void enableOrDisableInputDevice ( ) {
} // .end method
public Integer getKeyboardBackLightBrightness ( ) {
/* .locals 1 */
/* .line 131 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public abstract void getKeyboardReportData ( ) {
} // .end method
public abstract miui.hardware.input.MiuiKeyboardStatus getKeyboardStatus ( ) {
} // .end method
public Integer getKeyboardType ( ) {
/* .locals 1 */
/* .line 153 */
int v0 = -1; // const/4 v0, -0x1
} // .end method
public abstract Boolean isKeyboardReady ( ) {
} // .end method
public abstract void notifyLidSwitchChanged ( Boolean p0 ) {
} // .end method
public abstract void notifyScreenState ( Boolean p0 ) {
} // .end method
public abstract void notifyTabletSwitchChanged ( Boolean p0 ) {
} // .end method
public void readHallStatus ( ) {
/* .locals 0 */
/* .line 138 */
return;
} // .end method
public abstract void readKeyboardStatus ( ) {
} // .end method
public abstract android.view.InputDevice removeKeyboardDevicesIfNeeded ( android.view.InputDevice[] p0 ) {
} // .end method
public sendCommandForRespond ( Object[] p0, com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback p1 ) {
/* .locals 1 */
/* .param p1, "command" # [B */
/* .param p2, "callback" # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback; */
/* .line 112 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [B */
} // .end method
public void setCapsLockLight ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 121 */
return;
} // .end method
public void setKeyboardBackLightBrightness ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "brightness" # I */
/* .line 127 */
return;
} // .end method
public void setMuteLight ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 124 */
return;
} // .end method
public void wakeKeyboard176 ( ) {
/* .locals 0 */
/* .line 145 */
return;
} // .end method
