.class public Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;
.super Ljava/lang/Object;
.source "KeyboardAuthHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$KeyboardAuthInstance;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "com.xiaomi.devauth.MiDevAuthService"

.field public static final INTERNAL_ERROR_LIMIT:I = 0x3

.field public static final KEYBOARD_AUTH_OK:I = 0x0

.field public static final KEYBOARD_IDENTITY_RETRY_TIME:I = 0x1388

.field public static final KEYBOARD_INTERNAL_ERROR:I = 0x3

.field public static final KEYBOARD_NEED_CHECK_AGAIN:I = 0x2

.field public static final KEYBOARD_REJECT:I = 0x1

.field public static final KEYBOARD_TRANSFER_ERROR:I = 0x4

.field private static final MIDEVAUTH_TAG:Ljava/lang/String; = "MiuiKeyboardManager_MiDevAuthService"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.devauth"

.field public static final TRANSFER_ERROR_LIMIT:I = 0x2

.field private static sInternalErrorCount:I

.field private static sTransferErrorCount:I


# instance fields
.field private mChallengeCallbackLaunchAfterU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

.field private mChallengeCallbackLaunchBeforeU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

.field private mConn:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private mInitCallbackLaunchAfterU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

.field private mInitCallbackLaunchBeforeU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

.field private mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;


# direct methods
.method static bridge synthetic -$$Nest$fgetmConn(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/content/ServiceConnection;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mConn:Landroid/content/ServiceConnection;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/os/IBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Lcom/xiaomi/devauth/IMiDevAuthInterface;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;Lcom/xiaomi/devauth/IMiDevAuthInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    return-void
.end method

.method static bridge synthetic -$$Nest$minitService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 42
    const/4 v0, 0x0

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    .line 43
    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mContext:Landroid/content/Context;

    .line 37
    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    .line 87
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda0;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mInitCallbackLaunchBeforeU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 100
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda1;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mChallengeCallbackLaunchBeforeU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 114
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda2;

    invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda2;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mInitCallbackLaunchAfterU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 127
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda3;

    invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda3;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mChallengeCallbackLaunchAfterU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 148
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;-><init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    .line 159
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;-><init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mConn:Landroid/content/ServiceConnection;

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;-><init>()V

    return-void
.end method

.method private static BreakInternalErrorCounter()V
    .locals 1

    .line 62
    const/4 v0, 0x3

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    .line 63
    return-void
.end method

.method private static BreakTransferErrorCounter()V
    .locals 1

    .line 66
    const/4 v0, 0x2

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    .line 67
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 81
    const-string v0, "MiuiKeyboardManager_MiDevAuthService"

    const-string v1, "Init bind to MiDevAuth service"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$KeyboardAuthInstance;->-$$Nest$sfgetINSTANCE()Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->setContext(Landroid/content/Context;)V

    .line 83
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$KeyboardAuthInstance;->-$$Nest$sfgetINSTANCE()Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V

    .line 84
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$KeyboardAuthInstance;->-$$Nest$sfgetINSTANCE()Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    return-object v0
.end method

.method private static increaseInternalErrorCounter()V
    .locals 1

    .line 50
    sget v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    .line 51
    return-void
.end method

.method private static increaseTransferErrorCounter()V
    .locals 1

    .line 46
    sget v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    .line 47
    return-void
.end method

.method private initService()V
    .locals 5

    .line 184
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.xiaomi.devauth"

    const-string v3, "com.xiaomi.devauth.MiDevAuthService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 186
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    const-string v1, "MiuiKeyboardManager_MiDevAuthService"

    const-string v2, "cannot bind service: com.xiaomi.devauth.MiDevAuthService"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$0([B)Z
    .locals 5
    .param p0, "recBuf"    # [B

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[mInitCallbackLaunchBeforeU]Init, get rsp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p0

    invoke-static {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyboardManager_MiDevAuthService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v0, 0x5

    aget-byte v2, p0, v0

    const/16 v3, 0x1a

    const/4 v4, 0x0

    if-eq v2, v3, :cond_0

    .line 90
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mInitCallbackLaunchBeforeU]Init, Wrong length:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v0, p0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v3, "%02x"

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return v4

    .line 93
    :cond_0
    const/16 v0, 0x20

    aget-byte v2, p0, v0

    invoke-static {p0, v4, v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    const-string v0, "[mInitCallbackLaunchBeforeU]MiDevAuth Init, Receive wrong checksum"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return v4

    .line 97
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic lambda$new$1([B)Z
    .locals 5
    .param p0, "recBuf"    # [B

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, rsp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p0

    invoke-static {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyboardManager_MiDevAuthService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v0, 0x5

    aget-byte v2, p0, v0

    const/16 v3, 0x10

    const/4 v4, 0x0

    if-eq v2, v3, :cond_0

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, Wrong length:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v0, p0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v3, "%02x"

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    return v4

    .line 107
    :cond_0
    const/16 v0, 0x16

    aget-byte v2, p0, v0

    invoke-static {p0, v4, v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    const-string v0, "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, Receive wrong checksum"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return v4

    .line 111
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic lambda$new$2([B)Z
    .locals 5
    .param p0, "recBuf"    # [B

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[mInitCallbackLaunchAfterU]Init, get rsp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p0

    invoke-static {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyboardManager_MiDevAuthService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v0, 0x5

    aget-byte v2, p0, v0

    const/16 v3, 0x1a

    const/4 v4, 0x0

    if-eq v2, v3, :cond_0

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mInitCallbackLaunchAfterU]Init, Wrong length:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v0, p0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v3, "%02x"

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return v4

    .line 120
    :cond_0
    const/16 v0, 0x20

    aget-byte v2, p0, v0

    invoke-static {p0, v4, v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_1

    .line 121
    const-string v0, "[mInitCallbackLaunchAfterU]MiDevAuth Init, Receive wrong checksum"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    return v4

    .line 124
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic lambda$new$3([B)Z
    .locals 6
    .param p0, "recBuf"    # [B

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[mChallengeCallbackLaunchAfterU]Get token from keyboard, rsp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p0

    invoke-static {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiKeyboardManager_MiDevAuthService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/4 v0, 0x5

    aget-byte v2, p0, v0

    const/16 v3, 0x20

    const/16 v4, 0x10

    const/4 v5, 0x0

    if-eq v2, v3, :cond_0

    aget-byte v2, p0, v0

    if-eq v2, v4, :cond_0

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mChallengeCallbackLaunchAfterU]Get token from keyboard, Wrong length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v0, p0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v3, "%02x"

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    return v5

    .line 136
    :cond_0
    aget-byte v0, p0, v0

    const/4 v2, 0x1

    if-ne v0, v4, :cond_1

    .line 137
    const-string v0, "[mChallengeCallbackLaunchAfterU]Get token from keyboard should be 32 bytes but hack for 120 version"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return v2

    .line 141
    :cond_1
    const/16 v0, 0x26

    aget-byte v3, p0, v0

    invoke-static {p0, v5, v0, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->checkSum([BIIB)Z

    move-result v0

    if-nez v0, :cond_2

    .line 142
    const-string v0, "[mChallengeCallbackLaunchAfterU]Get token from keyboard, Receive wrong checksum"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return v5

    .line 145
    :cond_2
    return v2
.end method

.method private static resetInternalErrorCounter()V
    .locals 1

    .line 58
    const/4 v0, 0x0

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    .line 59
    return-void
.end method

.method private static resetTransferErrorCounter()V
    .locals 1

    .line 54
    const/4 v0, 0x0

    sput v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    .line 55
    return-void
.end method

.method private setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 77
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mContext:Landroid/content/Context;

    .line 78
    return-void
.end method


# virtual methods
.method public doCheckKeyboardIdentityLaunchAfterU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I
    .locals 21
    .param p1, "miuiPadKeyboardManager"    # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;
    .param p2, "isFirst"    # Z

    .line 318
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU ]Begin check keyboardIdentity "

    const-string v3, "MiuiKeyboardManager_MiDevAuthService"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    if-eqz p2, :cond_0

    .line 320
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 321
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetInternalErrorCounter()V

    .line 324
    :cond_0
    sget v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    const/4 v4, 0x4

    const/4 v5, 0x2

    if-le v0, v5, :cond_1

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[doCheckKeyboardIdentityLaunchAfterU] Meet transfer error counter:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v5, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    return v4

    .line 328
    :cond_1
    sget v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    const/4 v6, 0x3

    if-le v0, v6, :cond_2

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[doCheckKeyboardIdentityLaunchAfterU] Meet internal error counter:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    return v6

    .line 333
    :cond_2
    const/16 v0, 0x10

    new-array v13, v0, [B

    .line 334
    .local v13, "uid":[B
    new-array v14, v4, [B

    .line 335
    .local v14, "keyMetaData1":[B
    new-array v15, v4, [B

    .line 337
    .local v15, "keyMetaData2":[B
    const/4 v7, 0x0

    .line 338
    .local v7, "chooseKeyMeta":[B
    const/4 v8, 0x0

    .line 340
    .local v8, "challenge":[B
    const/16 v16, 0x0

    .line 344
    .local v16, "result":I
    invoke-interface/range {p1 .. p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiDevAuthInit()[B

    move-result-object v12

    .line 345
    .local v12, "initCommand":[B
    iget-object v9, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mInitCallbackLaunchAfterU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    invoke-interface {v2, v12, v9}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B

    move-result-object v11

    .line 346
    .local v11, "r1":[B
    array-length v9, v11

    if-nez v9, :cond_3

    .line 347
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseTransferErrorCounter()V

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[doCheckKeyboardIdentityLaunchAfterU]Get wrong init response, Error counter:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    return v5

    .line 352
    :cond_3
    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-static {v11, v9, v13, v10, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 353
    const/16 v9, 0x18

    invoke-static {v11, v9, v14, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 354
    const/16 v9, 0x1c

    invoke-static {v11, v9, v15, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 355
    iget-object v9, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    if-nez v9, :cond_4

    .line 356
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V

    .line 358
    :cond_4
    iget-object v9, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    if-nez v9, :cond_5

    .line 359
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] MiDevAuth Service is unavaiable!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    return v5

    .line 364
    :cond_5
    :try_start_0
    invoke-interface {v9, v13, v14, v15}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->chooseKey([B[B[B)[B

    move-result-object v9
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_5

    .line 369
    .end local v7    # "chooseKeyMeta":[B
    .local v9, "chooseKeyMeta":[B
    nop

    .line 370
    array-length v7, v9

    const/4 v6, 0x1

    if-eq v7, v4, :cond_7

    .line 371
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Choose KeyMeta from midevauth service fail!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    array-length v0, v9

    if-nez v0, :cond_6

    .line 373
    return v6

    .line 375
    :cond_6
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 376
    return v5

    .line 383
    :cond_7
    :try_start_1
    iget-object v4, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    invoke-interface {v4, v0}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->getChallenge(I)[B

    move-result-object v4
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4

    .line 388
    .end local v8    # "challenge":[B
    .local v4, "challenge":[B
    nop

    .line 389
    array-length v7, v4

    if-eq v7, v0, :cond_8

    .line 390
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 391
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Get challenge from midevauth service fail!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    return v5

    .line 395
    :cond_8
    invoke-interface {v2, v9, v4}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiAuthStep3Type1([B[B)[B

    move-result-object v8

    .line 396
    .local v8, "challengeCommand":[B
    iget-object v7, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mChallengeCallbackLaunchAfterU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    invoke-interface {v2, v8, v7}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B

    move-result-object v7

    .line 397
    .local v7, "r2":[B
    array-length v6, v7

    if-nez v6, :cond_9

    .line 398
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseTransferErrorCounter()V

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[doCheckKeyboardIdentityLaunchAfterU] Get wrong token from keyboard! Error counter:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v6, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    return v5

    .line 404
    :cond_9
    const/4 v6, 0x5

    aget-byte v6, v7, v6

    if-ne v6, v0, :cond_a

    .line 405
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] hack for 120 version, return auth ok"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    return v10

    .line 409
    :cond_a
    new-array v6, v0, [B

    .line 410
    .local v6, "keyboardToken":[B
    const/4 v5, 0x6

    invoke-static {v7, v5, v6, v10, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 414
    :try_start_2
    iget-object v5, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    const/16 v17, 0x1

    move-object/from16 v18, v14

    move-object v14, v7

    .end local v7    # "r2":[B
    .local v14, "r2":[B
    .local v18, "keyMetaData1":[B
    move-object v7, v5

    move-object v5, v8

    .end local v8    # "challengeCommand":[B
    .local v5, "challengeCommand":[B
    move/from16 v8, v17

    move-object/from16 v17, v9

    .end local v9    # "chooseKeyMeta":[B
    .local v17, "chooseKeyMeta":[B
    move-object v9, v13

    move-object/from16 v10, v17

    move-object/from16 v19, v11

    .end local v11    # "r1":[B
    .local v19, "r1":[B
    move-object v11, v4

    move-object/from16 v20, v12

    .end local v12    # "initCommand":[B
    .local v20, "initCommand":[B
    move-object v12, v6

    :try_start_3
    invoke-interface/range {v7 .. v12}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->tokenVerify(I[B[B[B[B)I

    move-result v7
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    .line 419
    .end local v16    # "result":I
    .local v7, "result":I
    nop

    .line 420
    const/4 v8, 0x1

    if-ne v7, v8, :cond_b

    .line 421
    const-string v8, "[doCheckKeyboardIdentityLaunchAfterU] verify token success with online key"

    invoke-static {v3, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_b
    const/4 v8, 0x2

    if-ne v7, v8, :cond_c

    .line 424
    const-string v8, "[doCheckKeyboardIdentityLaunchAfterU] verify token success with offline key"

    invoke-static {v3, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    :cond_c
    const/4 v8, 0x3

    if-ne v7, v8, :cond_d

    .line 427
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 428
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 429
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Meet internal error when try verify token!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    const/4 v3, 0x2

    return v3

    .line 432
    :cond_d
    const/4 v8, -0x1

    if-ne v7, v8, :cond_e

    .line 433
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->BreakInternalErrorCounter()V

    .line 434
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->BreakTransferErrorCounter()V

    .line 435
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] fail to verify keyboard token!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const/4 v8, 0x1

    return v8

    .line 441
    :cond_e
    const/4 v8, 0x1

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static {v14, v9, v4, v10, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 442
    const/4 v9, 0x0

    .line 444
    .local v9, "padToken":[B
    :try_start_4
    iget-object v11, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    move-object/from16 v12, v17

    .end local v17    # "chooseKeyMeta":[B
    .local v12, "chooseKeyMeta":[B
    :try_start_5
    invoke-interface {v11, v8, v13, v12, v4}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->tokenGet(I[B[B[B)[B

    move-result-object v8
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 449
    .end local v9    # "padToken":[B
    .local v8, "padToken":[B
    nop

    .line 451
    array-length v9, v8

    if-eq v9, v0, :cond_f

    .line 452
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 453
    const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Get token from midevauth service fail!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    const/4 v3, 0x2

    return v3

    .line 456
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[doCheckKeyboardIdentityLaunchAfterU] tokenGet from midevauthservice  =  "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v9, v8

    invoke-static {v8, v9}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    invoke-interface {v2, v8}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiAuthStep5Type1([B)[B

    move-result-object v0

    .line 459
    .local v0, "verifyDeviceCommand":[B
    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B

    .line 461
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 462
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetInternalErrorCounter()V

    .line 463
    return v10

    .line 445
    .end local v0    # "verifyDeviceCommand":[B
    .end local v8    # "padToken":[B
    .restart local v9    # "padToken":[B
    :catch_0
    move-exception v0

    goto :goto_0

    .end local v12    # "chooseKeyMeta":[B
    .restart local v17    # "chooseKeyMeta":[B
    :catch_1
    move-exception v0

    move-object/from16 v12, v17

    .line 446
    .end local v17    # "chooseKeyMeta":[B
    .local v0, "e":Landroid/os/RemoteException;
    .restart local v12    # "chooseKeyMeta":[B
    :goto_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 447
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[doCheckKeyboardIdentityLaunchAfterU] call tokenGet fail: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    const/4 v3, 0x2

    return v3

    .line 415
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v7    # "result":I
    .end local v9    # "padToken":[B
    .end local v12    # "chooseKeyMeta":[B
    .restart local v16    # "result":I
    .restart local v17    # "chooseKeyMeta":[B
    :catch_2
    move-exception v0

    move-object/from16 v12, v17

    .end local v17    # "chooseKeyMeta":[B
    .restart local v12    # "chooseKeyMeta":[B
    goto :goto_1

    .end local v5    # "challengeCommand":[B
    .end local v18    # "keyMetaData1":[B
    .end local v19    # "r1":[B
    .end local v20    # "initCommand":[B
    .local v7, "r2":[B
    .local v8, "challengeCommand":[B
    .local v9, "chooseKeyMeta":[B
    .restart local v11    # "r1":[B
    .local v12, "initCommand":[B
    .local v14, "keyMetaData1":[B
    :catch_3
    move-exception v0

    move-object v5, v8

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    move-object/from16 v18, v14

    move-object v14, v7

    move-object v12, v9

    .line 416
    .end local v7    # "r2":[B
    .end local v8    # "challengeCommand":[B
    .end local v9    # "chooseKeyMeta":[B
    .end local v11    # "r1":[B
    .restart local v0    # "e":Landroid/os/RemoteException;
    .restart local v5    # "challengeCommand":[B
    .local v12, "chooseKeyMeta":[B
    .local v14, "r2":[B
    .restart local v18    # "keyMetaData1":[B
    .restart local v19    # "r1":[B
    .restart local v20    # "initCommand":[B
    :goto_1
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 417
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[doCheckKeyboardIdentityLaunchAfterU] call token_verify fail: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    const/4 v3, 0x2

    return v3

    .line 384
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v4    # "challenge":[B
    .end local v5    # "challengeCommand":[B
    .end local v6    # "keyboardToken":[B
    .end local v18    # "keyMetaData1":[B
    .end local v19    # "r1":[B
    .end local v20    # "initCommand":[B
    .local v8, "challenge":[B
    .restart local v9    # "chooseKeyMeta":[B
    .restart local v11    # "r1":[B
    .local v12, "initCommand":[B
    .local v14, "keyMetaData1":[B
    :catch_4
    move-exception v0

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    move-object/from16 v18, v14

    move-object v12, v9

    .line 385
    .end local v9    # "chooseKeyMeta":[B
    .end local v11    # "r1":[B
    .end local v14    # "keyMetaData1":[B
    .restart local v0    # "e":Landroid/os/RemoteException;
    .local v12, "chooseKeyMeta":[B
    .restart local v18    # "keyMetaData1":[B
    .restart local v19    # "r1":[B
    .restart local v20    # "initCommand":[B
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 386
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[doCheckKeyboardIdentityLaunchAfterU] call getChallenge fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const/4 v3, 0x2

    return v3

    .line 365
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v18    # "keyMetaData1":[B
    .end local v19    # "r1":[B
    .end local v20    # "initCommand":[B
    .local v7, "chooseKeyMeta":[B
    .restart local v11    # "r1":[B
    .local v12, "initCommand":[B
    .restart local v14    # "keyMetaData1":[B
    :catch_5
    move-exception v0

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    move-object/from16 v18, v14

    move-object v4, v0

    .end local v11    # "r1":[B
    .end local v12    # "initCommand":[B
    .end local v14    # "keyMetaData1":[B
    .restart local v18    # "keyMetaData1":[B
    .restart local v19    # "r1":[B
    .restart local v20    # "initCommand":[B
    move-object v0, v4

    .line 366
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 367
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[doCheckKeyboardIdentityLaunchAfterU] call chooseKeyMeta fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/4 v3, 0x2

    return v3
.end method

.method public doCheckKeyboardIdentityLaunchBeforeU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I
    .locals 21
    .param p1, "miuiPadKeyboardManager"    # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;
    .param p2, "isFirst"    # Z

    .line 192
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU ]Begin check keyboardIdentity "

    const-string v3, "MiuiKeyboardManager_MiDevAuthService"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    if-eqz p2, :cond_0

    .line 194
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 195
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetInternalErrorCounter()V

    .line 197
    :cond_0
    sget v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    const/4 v4, 0x4

    const/4 v5, 0x2

    if-le v0, v5, :cond_1

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[doCheckKeyboardIdentityLaunchBeforeU] Meet transfer error counter:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v5, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    return v4

    .line 201
    :cond_1
    sget v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    const/4 v6, 0x3

    if-le v0, v6, :cond_2

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[doCheckKeyboardIdentityLaunchBeforeU] Meet internal error counter:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sInternalErrorCount:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    return v6

    .line 206
    :cond_2
    const/16 v0, 0x10

    new-array v13, v0, [B

    .line 207
    .local v13, "uid":[B
    new-array v14, v4, [B

    .line 208
    .local v14, "keyMetaData1":[B
    new-array v15, v4, [B

    .line 210
    .local v15, "keyMetaData2":[B
    const/4 v7, 0x0

    .line 211
    .local v7, "chooseKeyMeta":[B
    const/4 v8, 0x0

    .line 213
    .local v8, "challenge":[B
    const/16 v16, 0x0

    .line 217
    .local v16, "result":I
    invoke-interface/range {p1 .. p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiDevAuthInit()[B

    move-result-object v12

    .line 218
    .local v12, "initCommand":[B
    iget-object v9, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mInitCallbackLaunchBeforeU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    invoke-interface {v2, v12, v9}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B

    move-result-object v11

    .line 219
    .local v11, "r1":[B
    array-length v9, v11

    if-nez v9, :cond_3

    .line 220
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseTransferErrorCounter()V

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Init fail! Error counter:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    return v5

    .line 225
    :cond_3
    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-static {v11, v9, v13, v10, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 226
    const/16 v9, 0x18

    invoke-static {v11, v9, v14, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    const/16 v9, 0x1c

    invoke-static {v11, v9, v15, v10, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    iget-object v9, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    if-nez v9, :cond_4

    .line 229
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V

    .line 231
    :cond_4
    iget-object v9, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    if-nez v9, :cond_5

    .line 232
    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Service is unavaiable!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return v5

    .line 237
    :cond_5
    :try_start_0
    invoke-interface {v9, v13, v14, v15}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->chooseKey([B[B[B)[B

    move-result-object v9
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_3

    .line 242
    .end local v7    # "chooseKeyMeta":[B
    .local v9, "chooseKeyMeta":[B
    nop

    .line 243
    array-length v7, v9

    const/4 v6, 0x1

    if-eq v7, v4, :cond_7

    .line 244
    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Choose KeyMeta from midevauth service fail!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    array-length v0, v9

    if-nez v0, :cond_6

    .line 246
    return v6

    .line 248
    :cond_6
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 249
    return v5

    .line 256
    :cond_7
    :try_start_1
    iget-object v4, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;

    invoke-interface {v4, v0}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->getChallenge(I)[B

    move-result-object v4
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 261
    .end local v8    # "challenge":[B
    .local v4, "challenge":[B
    nop

    .line 262
    array-length v7, v4

    if-eq v7, v0, :cond_8

    .line 263
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 264
    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Get challenge from midevauth service fail!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    return v5

    .line 268
    :cond_8
    invoke-interface {v2, v9, v4}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiAuthStep3Type1([B[B)[B

    move-result-object v8

    .line 269
    .local v8, "challengeCommand":[B
    iget-object v7, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mChallengeCallbackLaunchBeforeU:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    invoke-interface {v2, v8, v7}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B

    move-result-object v7

    .line 271
    .local v7, "r2":[B
    array-length v6, v7

    if-nez v6, :cond_9

    .line 272
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseTransferErrorCounter()V

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Get token from keyboard fail! Error counter:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v6, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->sTransferErrorCount:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    return v5

    .line 276
    :cond_9
    new-array v6, v0, [B

    .line 277
    .local v6, "token":[B
    const/4 v5, 0x6

    invoke-static {v7, v5, v6, v10, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 281
    :try_start_2
    iget-object v0, v1, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->mService:Lcom/xiaomi/devauth/IMiDevAuthInterface;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v5, 0x1

    move-object/from16 v17, v7

    .end local v7    # "r2":[B
    .local v17, "r2":[B
    move-object v7, v0

    move-object/from16 v18, v8

    .end local v8    # "challengeCommand":[B
    .local v18, "challengeCommand":[B
    move v8, v5

    move-object v5, v9

    .end local v9    # "chooseKeyMeta":[B
    .local v5, "chooseKeyMeta":[B
    move-object v9, v13

    move v0, v10

    move-object v10, v5

    move-object/from16 v19, v11

    .end local v11    # "r1":[B
    .local v19, "r1":[B
    move-object v11, v4

    move-object/from16 v20, v12

    .end local v12    # "initCommand":[B
    .local v20, "initCommand":[B
    move-object v12, v6

    :try_start_3
    invoke-interface/range {v7 .. v12}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->tokenVerify(I[B[B[B[B)I

    move-result v7
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 286
    .end local v16    # "result":I
    .local v7, "result":I
    nop

    .line 287
    const/4 v8, 0x1

    if-ne v7, v8, :cond_a

    .line 288
    const-string v8, "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard PASS with online key"

    invoke-static {v3, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 290
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetInternalErrorCounter()V

    .line 291
    return v0

    .line 293
    :cond_a
    const/4 v8, 0x2

    if-ne v7, v8, :cond_b

    .line 294
    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard PASS with offline key, need check again later"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 296
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetInternalErrorCounter()V

    .line 297
    return v8

    .line 299
    :cond_b
    const/4 v0, 0x3

    if-ne v7, v0, :cond_c

    .line 300
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->resetTransferErrorCounter()V

    .line 301
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 302
    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Meet internal error when try check keyboard!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    return v8

    .line 305
    :cond_c
    const/4 v0, -0x1

    if-ne v7, v0, :cond_d

    .line 306
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->BreakInternalErrorCounter()V

    .line 307
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->BreakTransferErrorCounter()V

    .line 308
    const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] fail to verify keyboard token!"

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const/4 v0, 0x1

    return v0

    .line 312
    :cond_d
    const/4 v0, 0x1

    const-string v8, "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard Fail!"

    invoke-static {v3, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    return v0

    .line 282
    .end local v7    # "result":I
    .restart local v16    # "result":I
    :catch_0
    move-exception v0

    goto :goto_0

    .end local v5    # "chooseKeyMeta":[B
    .end local v17    # "r2":[B
    .end local v18    # "challengeCommand":[B
    .end local v19    # "r1":[B
    .end local v20    # "initCommand":[B
    .local v7, "r2":[B
    .restart local v8    # "challengeCommand":[B
    .restart local v9    # "chooseKeyMeta":[B
    .restart local v11    # "r1":[B
    .restart local v12    # "initCommand":[B
    :catch_1
    move-exception v0

    move-object/from16 v17, v7

    move-object/from16 v18, v8

    move-object v5, v9

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    .line 283
    .end local v7    # "r2":[B
    .end local v8    # "challengeCommand":[B
    .end local v9    # "chooseKeyMeta":[B
    .end local v11    # "r1":[B
    .end local v12    # "initCommand":[B
    .local v0, "e":Landroid/os/RemoteException;
    .restart local v5    # "chooseKeyMeta":[B
    .restart local v17    # "r2":[B
    .restart local v18    # "challengeCommand":[B
    .restart local v19    # "r1":[B
    .restart local v20    # "initCommand":[B
    :goto_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 284
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[doCheckKeyboardIdentityLaunchBeforeU] call token_verify fail: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const/4 v3, 0x2

    return v3

    .line 257
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v4    # "challenge":[B
    .end local v5    # "chooseKeyMeta":[B
    .end local v6    # "token":[B
    .end local v17    # "r2":[B
    .end local v18    # "challengeCommand":[B
    .end local v19    # "r1":[B
    .end local v20    # "initCommand":[B
    .local v8, "challenge":[B
    .restart local v9    # "chooseKeyMeta":[B
    .restart local v11    # "r1":[B
    .restart local v12    # "initCommand":[B
    :catch_2
    move-exception v0

    move-object v5, v9

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    .line 258
    .end local v9    # "chooseKeyMeta":[B
    .end local v11    # "r1":[B
    .end local v12    # "initCommand":[B
    .restart local v0    # "e":Landroid/os/RemoteException;
    .restart local v5    # "chooseKeyMeta":[B
    .restart local v19    # "r1":[B
    .restart local v20    # "initCommand":[B
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 259
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[doCheckKeyboardIdentityLaunchBeforeU] call getChallenge fail: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/4 v3, 0x2

    return v3

    .line 238
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v5    # "chooseKeyMeta":[B
    .end local v19    # "r1":[B
    .end local v20    # "initCommand":[B
    .local v7, "chooseKeyMeta":[B
    .restart local v11    # "r1":[B
    .restart local v12    # "initCommand":[B
    :catch_3
    move-exception v0

    move-object/from16 v19, v11

    move-object/from16 v20, v12

    move-object v4, v0

    .end local v11    # "r1":[B
    .end local v12    # "initCommand":[B
    .restart local v19    # "r1":[B
    .restart local v20    # "initCommand":[B
    move-object v0, v4

    .line 239
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->increaseInternalErrorCounter()V

    .line 240
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[doCheckKeyboardIdentityLaunchBeforeU] call chooseKeyMeta fail: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v3, 0x2

    return v3
.end method
