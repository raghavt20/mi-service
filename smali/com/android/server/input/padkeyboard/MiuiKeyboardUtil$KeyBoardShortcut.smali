.class public Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut;
.super Ljava/lang/Object;
.source "MiuiKeyboardUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KeyBoardShortcut"
.end annotation


# static fields
.field public static final ADJUST_SPLIT_SCREEN:Ljava/lang/String; = "\u8c03\u6574\u5206\u5c4f\u6bd4\u4f8b"

.field public static final BACK_ESC:Ljava/lang/String; = "\u8fd4\u56de"

.field public static final BACK_HOME:Ljava/lang/String; = "\u56de\u5230\u684c\u9762"

.field public static final BRIGHTNESS_DOWN:Ljava/lang/String; = "\u4eae\u5ea6\u51cf"

.field public static final BRIGHTNESS_UP:Ljava/lang/String; = "\u4eae\u5ea6\u52a0"

.field public static final CLOSE_WINDOW:Ljava/lang/String; = "\u5173\u95ed\u5f53\u524d\u7a97\u53e3"

.field public static final DELETE_WORD:Ljava/lang/String; = "\u5220\u9664"

.field public static final FULL_SCREEN:Ljava/lang/String; = "\u8fdb\u5165\u5168\u5c4f\u6a21\u5f0f"

.field public static final LAUNCH_CONTROL_CENTER:Ljava/lang/String; = "\u6253\u5f00\u63a7\u5236\u4e2d\u5fc3"

.field public static final LAUNCH_NOTIFICATION_CENTER:Ljava/lang/String; = "\u6253\u5f00\u901a\u77e5\u4e2d\u5fc3"

.field public static final LAUNCH_RECENTS_TASKS:Ljava/lang/String; = "\u663e\u793a\u6700\u8fd1\u4efb\u52a1"

.field public static final LOCK_SCREEN:Ljava/lang/String; = "\u9501\u5c4f"

.field public static final MEDIA_NEXT:Ljava/lang/String; = "\u4e0b\u4e00\u66f2"

.field public static final MEDIA_PLAY_PAUSE:Ljava/lang/String; = "\u6682\u505c\u64ad\u653e"

.field public static final MEDIA_PREVIOUS:Ljava/lang/String; = "\u4e0a\u4e00\u66f2"

.field public static final MUTE_MODE:Ljava/lang/String; = "\u9ea6\u514b\u98ce\u5f00\u5173"

.field public static final OPEN_APP:Ljava/lang/String; = "\u6253\u5f00\u81ea\u5b9a\u4e49\u5e94\u7528"

.field public static final SCREENSHOT:Ljava/lang/String; = "\u622a\u5168\u5c4f"

.field public static final SCREEN_SHOT_PARTIAL:Ljava/lang/String; = "\u533a\u57df\u622a\u5c4f"

.field public static final SHOW_DOCK:Ljava/lang/String; = "\u663e\u793adock"

.field public static final SHOW_SHORTCUT_LIST:Ljava/lang/String; = "\u663e\u793a\u5feb\u6377\u952e\u5217\u8868"

.field public static final SMALL_WINDOW:Ljava/lang/String; = "\u8fdb\u5165\u5c0f\u7a97\u6a21\u5f0f"

.field public static final TOGGLE_RECENT_APP:Ljava/lang/String; = "\u5feb\u5207\u6700\u8fd1\u5e94\u7528"

.field public static final TYPE_APP:I = 0x0

.field public static final TYPE_CLOSEAPP:I = 0x8

.field public static final TYPE_CONTROLPANEL:I = 0x1

.field public static final TYPE_FULLSCREEN:I = 0xa

.field public static final TYPE_HOME:I = 0x4

.field public static final TYPE_LEFTSPLITSCREEN:I = 0xb

.field public static final TYPE_LOCKSCREEN:I = 0x5

.field public static final TYPE_NOTIFICATIONPANLE:I = 0x2

.field public static final TYPE_RECENT:I = 0x3

.field public static final TYPE_RIGHTSPLITSCREEN:I = 0xc

.field public static final TYPE_SCREENSHOT:I = 0x6

.field public static final TYPE_SCREENSHOTPARTIAL:I = 0x7

.field public static final TYPE_SMALLWINDOW:I = 0x9

.field public static final VOICE_ASSISTANT:Ljava/lang/String; = "\u5c0f\u7231\u540c\u5b66"

.field public static final VOICE_TO_WORD:Ljava/lang/String; = "\u5feb\u6377\u8bed\u97f3"

.field public static final VOLUME_DOWN:Ljava/lang/String; = "\u97f3\u91cf\u4e0b"

.field public static final VOLUME_MUTE:Ljava/lang/String; = "\u9759\u97f3"

.field public static final VOLUME_UP:Ljava/lang/String; = "\u97f3\u91cf\u4e0a"

.field public static final ZEN_MODE:Ljava/lang/String; = "\u52ff\u6270"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getShortCutNameByType(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 567
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 596
    const/4 v0, 0x0

    return-object v0

    .line 592
    :pswitch_0
    const-string/jumbo v0, "\u8c03\u6574\u5206\u5c4f\u6bd4\u4f8b"

    return-object v0

    .line 589
    :pswitch_1
    const-string/jumbo v0, "\u8fdb\u5165\u5168\u5c4f\u6a21\u5f0f"

    return-object v0

    .line 587
    :pswitch_2
    const-string/jumbo v0, "\u8fdb\u5165\u5c0f\u7a97\u6a21\u5f0f"

    return-object v0

    .line 585
    :pswitch_3
    const-string/jumbo v0, "\u5173\u95ed\u5f53\u524d\u7a97\u53e3"

    return-object v0

    .line 583
    :pswitch_4
    const-string/jumbo v0, "\u533a\u57df\u622a\u5c4f"

    return-object v0

    .line 581
    :pswitch_5
    const-string/jumbo v0, "\u622a\u5168\u5c4f"

    return-object v0

    .line 579
    :pswitch_6
    const-string/jumbo v0, "\u9501\u5c4f"

    return-object v0

    .line 577
    :pswitch_7
    const-string/jumbo v0, "\u56de\u5230\u684c\u9762"

    return-object v0

    .line 575
    :pswitch_8
    const-string/jumbo v0, "\u663e\u793a\u6700\u8fd1\u4efb\u52a1"

    return-object v0

    .line 573
    :pswitch_9
    const-string/jumbo v0, "\u6253\u5f00\u901a\u77e5\u4e2d\u5fc3"

    return-object v0

    .line 571
    :pswitch_a
    const-string/jumbo v0, "\u6253\u5f00\u63a7\u5236\u4e2d\u5fc3"

    return-object v0

    .line 569
    :pswitch_b
    const-string/jumbo v0, "\u6253\u5f00\u81ea\u5b9a\u4e49\u5e94\u7528"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getShortcutNameByKeyCode(I)Ljava/lang/String;
    .locals 1
    .param p0, "keyCode"    # I

    .line 601
    sparse-switch p0, :sswitch_data_0

    goto :goto_0

    .line 603
    :sswitch_0
    const-string/jumbo v0, "\u9501\u5c4f"

    return-object v0

    .line 605
    :sswitch_1
    const-string/jumbo v0, "\u52ff\u6270"

    return-object v0

    .line 607
    :sswitch_2
    const-string/jumbo v0, "\u5c0f\u7231\u540c\u5b66"

    return-object v0

    .line 609
    :sswitch_3
    const-string/jumbo v0, "\u9ea6\u514b\u98ce\u5f00\u5173"

    return-object v0

    .line 625
    :sswitch_4
    const-string/jumbo v0, "\u4eae\u5ea6\u52a0"

    return-object v0

    .line 623
    :sswitch_5
    const-string/jumbo v0, "\u4eae\u5ea6\u51cf"

    return-object v0

    .line 619
    :sswitch_6
    const-string/jumbo v0, "\u9759\u97f3"

    return-object v0

    .line 632
    :sswitch_7
    invoke-static {}, Lmiui/hardware/input/MiuiKeyboardHelper;->support6FKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    const-string/jumbo v0, "\u5220\u9664"

    return-object v0

    .line 615
    :sswitch_8
    const-string/jumbo v0, "\u4e0a\u4e00\u66f2"

    return-object v0

    .line 611
    :sswitch_9
    const-string/jumbo v0, "\u4e0b\u4e00\u66f2"

    return-object v0

    .line 613
    :sswitch_a
    const-string/jumbo v0, "\u6682\u505c\u64ad\u653e"

    return-object v0

    .line 617
    :sswitch_b
    const-string/jumbo v0, "\u97f3\u91cf\u4e0b"

    return-object v0

    .line 621
    :sswitch_c
    const-string/jumbo v0, "\u97f3\u91cf\u4e0a"

    return-object v0

    .line 627
    :sswitch_d
    invoke-static {}, Lmiui/hardware/input/MiuiKeyboardHelper;->support6FKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    const-string/jumbo v0, "\u8fd4\u56de"

    return-object v0

    .line 639
    :cond_0
    :goto_0
    const-string v0, ""

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_d
        0x18 -> :sswitch_c
        0x19 -> :sswitch_b
        0x55 -> :sswitch_a
        0x57 -> :sswitch_9
        0x58 -> :sswitch_8
        0x70 -> :sswitch_7
        0xa4 -> :sswitch_6
        0xdc -> :sswitch_5
        0xdd -> :sswitch_4
        0x270c -> :sswitch_3
        0x270d -> :sswitch_2
        0x270e -> :sswitch_1
        0x270f -> :sswitch_0
    .end sparse-switch
.end method

.method public static getShortcutNameByKeyCodeWithAction(IZ)Ljava/lang/String;
    .locals 2
    .param p0, "keyCode"    # I
    .param p1, "isLongPress"    # Z

    .line 643
    invoke-static {}, Lmiui/hardware/input/MiuiKeyboardHelper;->support6FKeyboard()Z

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_0

    .line 644
    return-object v1

    .line 646
    :cond_0
    packed-switch p0, :pswitch_data_0

    .line 656
    return-object v1

    .line 648
    :pswitch_0
    if-eqz p1, :cond_1

    .line 649
    const-string/jumbo v0, "\u533a\u57df\u622a\u5c4f"

    return-object v0

    .line 651
    :cond_1
    const-string/jumbo v0, "\u622a\u5168\u5c4f"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x270a
        :pswitch_0
    .end packed-switch
.end method
