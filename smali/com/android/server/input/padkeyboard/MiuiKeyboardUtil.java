public class com.android.server.input.padkeyboard.MiuiKeyboardUtil {
	 /* .source "MiuiKeyboardUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ARABIC;
private static final java.util.Map BRIGHTNESS_SETTINGS_RELATION;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String FRENCH;
public static final java.lang.String GERMAN;
public static final java.lang.String ITALIA;
public static final java.lang.String KEYBOARD_LAYOUT_ARABIC;
public static final java.lang.String KEYBOARD_LAYOUT_FRENCH;
public static final java.lang.String KEYBOARD_LAYOUT_GERMAN;
public static final java.lang.String KEYBOARD_LAYOUT_ITALIA;
public static final java.lang.String KEYBOARD_LAYOUT_RUSSIAN;
public static final java.lang.String KEYBOARD_LAYOUT_SPAINISH;
public static final java.lang.String KEYBOARD_LAYOUT_THAI;
public static final java.lang.String KEYBOARD_LAYOUT_UK;
public static final java.lang.String KEYBOARD_LAYOUT_US;
public static final Integer KEYBOARD_LEVEL_HIGH;
public static final Integer KEYBOARD_LEVEL_LOW;
public static final java.lang.String KEYBOARD_TYPE_LEVEL;
public static final Object KEYBOARD_TYPE_LQ_BLE;
public static final Object KEYBOARD_TYPE_LQ_HIGH;
public static final Object KEYBOARD_TYPE_LQ_LOW;
public static final Object KEYBOARD_TYPE_UCJ;
public static final Object KEYBOARD_TYPE_UCJ_HIGH;
public static final java.lang.String RUSSIA;
public static final java.lang.String SPAINISH;
private static final java.lang.String TAG;
public static final java.lang.String THAI;
public static final java.lang.String UK;
public static final java.lang.String US;
public static final Integer VALUE_KB_TYPE_HIGH;
public static final Integer VALUE_KB_TYPE_LOW;
private static final angle_cos;
private static final angle_sin;
private static java.lang.String mMcuVersion;
/* # direct methods */
static com.android.server.input.padkeyboard.MiuiKeyboardUtil ( ) {
/* .locals 4 */
/* .line 76 */
int v0 = 0; // const/4 v0, 0x0
/* .line 78 */
/* const/16 v0, 0x5b */
/* new-array v1, v0, [F */
/* fill-array-data v1, :array_0 */
/* .line 101 */
/* new-array v0, v0, [F */
/* fill-array-data v0, :array_1 */
/* .line 124 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 126 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3f19999a # 0.6f */
java.lang.Float .valueOf ( v2 );
/* .line 127 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3f4ccccd # 0.8f */
java.lang.Float .valueOf ( v2 );
/* .line 128 */
int v1 = 2; // const/4 v1, 0x2
java.lang.Integer .valueOf ( v1 );
/* const/high16 v3, 0x3f800000 # 1.0f */
java.lang.Float .valueOf ( v3 );
/* .line 129 */
int v1 = 3; // const/4 v1, 0x3
java.lang.Integer .valueOf ( v1 );
/* .line 130 */
int v1 = 4; // const/4 v1, 0x4
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3f333333 # 0.7f */
java.lang.Float .valueOf ( v2 );
/* .line 131 */
int v1 = 5; // const/4 v1, 0x5
java.lang.Integer .valueOf ( v1 );
/* const/high16 v2, 0x3f000000 # 0.5f */
java.lang.Float .valueOf ( v2 );
/* .line 132 */
int v1 = 6; // const/4 v1, 0x6
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3ecccccd # 0.4f */
java.lang.Float .valueOf ( v2 );
/* .line 133 */
int v1 = 7; // const/4 v1, 0x7
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3e99999a # 0.3f */
java.lang.Float .valueOf ( v2 );
/* .line 134 */
/* const/16 v1, 0x8 */
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3e4ccccd # 0.2f */
java.lang.Float .valueOf ( v2 );
/* .line 135 */
/* const/16 v1, 0x9 */
java.lang.Integer .valueOf ( v1 );
/* const v2, 0x3dcccccd # 0.1f */
java.lang.Float .valueOf ( v2 );
/* .line 136 */
/* const/16 v1, 0xa */
java.lang.Integer .valueOf ( v1 );
int v2 = 0; // const/4 v2, 0x0
java.lang.Float .valueOf ( v2 );
/* .line 137 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x3f800000 # 1.0f */
/* 0x3f7ff60a */
/* 0x3f7fd817 */
/* 0x3f7fa637 */
/* 0x3f7f605b */
/* 0x3f7f06a3 */
/* 0x3f7e98fe */
/* 0x3f7e177f */
/* 0x3f7d8234 */
/* 0x3f7cd91f */
/* 0x3f7c1c61 */
/* 0x3f7b4be8 */
/* 0x3f7a67e8 */
/* 0x3f797050 # 0.97437f */
/* 0x3f786552 */
/* 0x3f7746ed */
/* 0x3f761544 */
/* 0x3f74d068 */
/* 0x3f737879 */
/* 0x3f720d88 */
/* 0x3f708fb8 */
/* 0x3f6eff19 */
/* 0x3f6d5bee */
/* 0x3f6ba637 */
/* 0x3f69de16 */
/* 0x3f6803cd */
/* 0x3f66175d */
/* 0x3f641909 */
/* 0x3f6208e1 */
/* 0x3f5fe719 */
/* 0x3f5db3d0 */
/* 0x3f5b6f4c */
/* 0x3f5919ac */
/* 0x3f56b325 */
/* 0x3f543bd6 */
/* 0x3f51b3f2 */
/* 0x3f4f1bbd */
/* 0x3f4c7358 */
/* 0x3f49bb17 */
/* 0x3f46f30a */
/* 0x3f441b76 */
/* 0x3f4134ad */
/* 0x3f3e3ec0 */
/* 0x3f3b3a04 */
/* 0x3f3826ab */
/* 0x3f3504f7 */
/* 0x3f31d51b */
/* 0x3f2e976c */
/* 0x3f2b4c2b */
/* 0x3f27f37c */
/* 0x3f248dc1 */
/* 0x3f211b1e */
/* 0x3f1d9bf6 */
/* 0x3f1a108c */
/* 0x3f167914 */
/* 0x3f12d5e0 */
/* 0x3f0f2746 */
/* 0x3f0b6d76 */
/* 0x3f07a8c6 */
/* 0x3f03d988 */
/* 0x3f000000 # 0.5f */
/* 0x3ef83904 # 0.48481f */
/* 0x3ef05ea2 */
/* 0x3ee87182 */
/* 0x3ee0722a */
/* 0x3ed86163 */
/* 0x3ed03fd5 # 0.406737f */
/* 0x3ec80de5 */
/* 0x3ebfcc7d */
/* 0x3eb77c03 */
/* 0x3eaf1d3f # 0.34202f */
/* 0x3ea6b0d9 */
/* 0x3e9e377a */
/* 0x3e95b1c8 */
/* 0x3e8d204b # 0.275637f */
/* 0x3e8483ed */
/* 0x3e77ba67 */
/* 0x3e66598e # 0.224951f */
/* 0x3e54e6e2 */
/* 0x3e43636f */
/* 0x3e31d0c8 # 0.173648f */
/* 0x3e20303c # 0.156434f */
/* 0x3e0e835e */
/* 0x3df99674 */
/* 0x3dd612c7 */
/* 0x3db27ed8 */
/* 0x3d8edc3c */
/* 0x3d565e46 # 0.052336f */
/* 0x3d0ef241 # 0.034899f */
/* 0x3c8ef77f # 0.017452f */
/* -0x80000000 */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x0 */
/* 0x3c8ef77f # 0.017452f */
/* 0x3d0ef241 # 0.034899f */
/* 0x3d565e46 # 0.052336f */
/* 0x3d8edc3c */
/* 0x3db27ed8 */
/* 0x3dd612c7 */
/* 0x3df99674 */
/* 0x3e0e835e */
/* 0x3e20303c # 0.156434f */
/* 0x3e31d0c8 # 0.173648f */
/* 0x3e43636f */
/* 0x3e54e6e2 */
/* 0x3e66598e # 0.224951f */
/* 0x3e77ba67 */
/* 0x3e8483ed */
/* 0x3e8d204b # 0.275637f */
/* 0x3e95b1c8 */
/* 0x3e9e377a */
/* 0x3ea6b0d9 */
/* 0x3eaf1d3f # 0.34202f */
/* 0x3eb77c03 */
/* 0x3ebfcc7d */
/* 0x3ec80de5 */
/* 0x3ed03fd5 # 0.406737f */
/* 0x3ed86163 */
/* 0x3ee0722a */
/* 0x3ee87182 */
/* 0x3ef05ea2 */
/* 0x3ef83904 # 0.48481f */
/* 0x3f000000 # 0.5f */
/* 0x3f03d988 */
/* 0x3f07a8c6 */
/* 0x3f0b6d76 */
/* 0x3f0f2746 */
/* 0x3f12d5e0 */
/* 0x3f167914 */
/* 0x3f1a108c */
/* 0x3f1d9c06 */
/* 0x3f211b1e */
/* 0x3f248dc1 */
/* 0x3f27f37c */
/* 0x3f2b4c2b */
/* 0x3f2e976c */
/* 0x3f31d51b */
/* 0x3f3504f7 */
/* 0x3f3826ab */
/* 0x3f3b3a04 */
/* 0x3f3e3ec0 */
/* 0x3f4134ad */
/* 0x3f441b76 */
/* 0x3f46f30a */
/* 0x3f49bb17 */
/* 0x3f4c7369 */
/* 0x3f4f1bbd */
/* 0x3f51b3f2 */
/* 0x3f543bd6 */
/* 0x3f56b325 */
/* 0x3f5919ac */
/* 0x3f5b6f4c */
/* 0x3f5db3d0 */
/* 0x3f5fe719 */
/* 0x3f6208e1 */
/* 0x3f641909 */
/* 0x3f66175d */
/* 0x3f6803cd */
/* 0x3f69de16 */
/* 0x3f6ba637 */
/* 0x3f6d5bee */
/* 0x3f6eff19 */
/* 0x3f708fb8 */
/* 0x3f720d88 */
/* 0x3f737879 */
/* 0x3f74d068 */
/* 0x3f761544 */
/* 0x3f7746ed */
/* 0x3f786552 */
/* 0x3f797050 # 0.97437f */
/* 0x3f7a67e8 */
/* 0x3f7b4be8 */
/* 0x3f7c1c61 */
/* 0x3f7cd91f */
/* 0x3f7d8234 */
/* 0x3f7e177f */
/* 0x3f7e98fe */
/* 0x3f7f06a3 */
/* 0x3f7f605b */
/* 0x3f7fa637 */
/* 0x3f7fd817 */
/* 0x3f7ff60a */
/* 0x3f800000 # 1.0f */
} // .end array-data
} // .end method
private com.android.server.input.padkeyboard.MiuiKeyboardUtil ( ) {
/* .locals 0 */
/* .line 139 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 140 */
return;
} // .end method
public static java.lang.String Bytes2Hex ( Object[] p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "data" # [B */
/* .param p1, "len" # I */
/* .line 274 */
final String v0 = ","; // const-string v0, ","
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2HexString ( p0,p1,v0 );
} // .end method
public static java.lang.String Bytes2HexString ( Object[] p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p0, "data" # [B */
/* .param p1, "len" # I */
/* .param p2, "s" # Ljava/lang/String; */
/* .line 278 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 279 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
/* array-length v1, p0 */
/* if-le p1, v1, :cond_0 */
/* .line 280 */
/* array-length p1, p0 */
/* .line 282 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p1, :cond_1 */
/* .line 283 */
/* aget-byte v2, p0, v1 */
java.lang.Byte .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 282 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 285 */
} // .end local v1 # "i":I
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String Bytes2RevertHexString ( Object[] p0 ) {
/* .locals 6 */
/* .param p0, "data" # [B */
/* .line 289 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 290 */
/* .local v0, "hexBuilder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_0 */
/* .line 291 */
/* aget-byte v2, p0, v1 */
java.lang.Byte .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 290 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 293 */
} // .end local v1 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 294 */
/* .local v1, "hexStr":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 295 */
/* .local v2, "result":Ljava/lang/StringBuilder; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
v4 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* if-ge v3, v4, :cond_1 */
/* .line 296 */
v4 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* add-int/lit8 v4, v4, -0x2 */
/* sub-int/2addr v4, v3 */
v5 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* sub-int/2addr v5, v3 */
(( java.lang.String ) v1 ).substring ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 295 */
/* add-int/lit8 v3, v3, 0x2 */
/* .line 298 */
} // .end local v3 # "i":I
} // :cond_1
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String Bytes2String ( Object[] p0 ) {
/* .locals 2 */
/* .param p0, "data" # [B */
/* .line 307 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/String; */
final String v1 = "UTF-8"; // const-string v1, "UTF-8"
/* invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 308 */
/* :catch_0 */
/* move-exception v0 */
/* .line 309 */
/* .local v0, "e":Ljava/io/UnsupportedEncodingException; */
(( java.io.UnsupportedEncodingException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
/* .line 311 */
} // .end local v0 # "e":Ljava/io/UnsupportedEncodingException;
final String v0 = ""; // const-string v0, ""
} // .end method
public static java.lang.String Hex2String ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p0, "hex" # Ljava/lang/String; */
/* .line 319 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 320 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* add-int/lit8 v2, v2, -0x1 */
/* if-ge v1, v2, :cond_0 */
/* .line 321 */
/* add-int/lit8 v2, v1, 0x2 */
(( java.lang.String ) p0 ).substring ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 322 */
/* .local v2, "output":Ljava/lang/String; */
/* const/16 v3, 0x10 */
v3 = java.lang.Integer .parseInt ( v2,v3 );
/* .line 323 */
/* .local v3, "decimal":I */
/* int-to-char v4, v3 */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 320 */
} // .end local v2 # "output":Ljava/lang/String;
} // .end local v3 # "decimal":I
/* add-int/lit8 v1, v1, 0x2 */
/* .line 325 */
} // .end local v1 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String String2HexString ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "str" # Ljava/lang/String; */
/* .line 333 */
(( java.lang.String ) p0 ).toCharArray ( ); // invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C
/* .line 334 */
/* .local v0, "chars":[C */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 335 */
/* .local v1, "stringBuilder":Ljava/lang/StringBuilder; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_0 */
/* .line 336 */
/* aget-char v3, v0, v2 */
java.lang.Integer .toHexString ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 335 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 338 */
} // .end local v2 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static Integer byte2int ( Object p0 ) {
/* .locals 1 */
/* .param p0, "data" # B */
/* .line 375 */
/* and-int/lit16 v0, p0, 0xff */
} // .end method
public static Float calculatePKAngle ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 15 */
/* .param p0, "x1" # F */
/* .param p1, "x2" # F */
/* .param p2, "z1" # F */
/* .param p3, "z2" # F */
/* .line 171 */
/* mul-float v0, p0, p1 */
/* .line 173 */
/* .local v0, "quadrant":F */
/* mul-float v1, p0, p0 */
/* mul-float v2, p2, p2 */
/* add-float/2addr v1, v2 */
/* float-to-double v1, v1 */
java.lang.Math .sqrt ( v1,v2 );
/* move-result-wide v1 */
/* double-to-float v1, v1 */
/* div-float v1, p2, v1 */
/* .line 174 */
/* .local v1, "cos1":F */
/* mul-float v2, p1, p1 */
/* mul-float v3, p3, p3 */
/* add-float/2addr v2, v3 */
/* float-to-double v2, v2 */
java.lang.Math .sqrt ( v2,v3 );
/* move-result-wide v2 */
/* double-to-float v2, v2 */
/* div-float v2, p3, v2 */
/* .line 177 */
/* .local v2, "cos2":F */
/* float-to-double v3, v1 */
/* const-wide/high16 v5, 0x3ff0000000000000L # 1.0 */
/* sub-double/2addr v3, v5 */
java.lang.Math .abs ( v3,v4 );
/* move-result-wide v3 */
/* const-wide v7, 0x3f847ae147ae147bL # 0.01 */
/* cmpg-double v3, v3, v7 */
/* const-wide v9, 0x400921fb54442d18L # Math.PI */
/* const-wide v11, 0x4066800000000000L # 180.0 */
/* if-gez v3, :cond_0 */
/* .line 178 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "angle1":F */
/* .line 179 */
} // .end local v3 # "angle1":F
} // :cond_0
/* float-to-double v3, v1 */
/* add-double/2addr v3, v5 */
java.lang.Math .abs ( v3,v4 );
/* move-result-wide v3 */
/* cmpg-double v3, v3, v7 */
/* if-gez v3, :cond_1 */
/* .line 180 */
/* const/high16 v3, 0x43340000 # 180.0f */
/* .restart local v3 # "angle1":F */
/* .line 182 */
} // .end local v3 # "angle1":F
} // :cond_1
/* float-to-double v3, v1 */
java.lang.Math .acos ( v3,v4 );
/* move-result-wide v3 */
/* mul-double/2addr v3, v11 */
/* div-double/2addr v3, v9 */
/* double-to-float v3, v3 */
/* .line 184 */
/* .restart local v3 # "angle1":F */
} // :goto_0
/* float-to-double v13, v2 */
/* sub-double/2addr v13, v5 */
java.lang.Math .abs ( v13,v14 );
/* move-result-wide v13 */
/* cmpg-double v4, v13, v7 */
/* if-gez v4, :cond_2 */
/* .line 185 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "angle2":F */
/* .line 186 */
} // .end local v4 # "angle2":F
} // :cond_2
/* float-to-double v13, v2 */
/* add-double/2addr v13, v5 */
java.lang.Math .abs ( v13,v14 );
/* move-result-wide v4 */
/* cmpg-double v4, v4, v7 */
/* if-gez v4, :cond_3 */
/* .line 187 */
/* const/high16 v4, 0x43340000 # 180.0f */
/* .restart local v4 # "angle2":F */
/* .line 189 */
} // .end local v4 # "angle2":F
} // :cond_3
/* float-to-double v4, v2 */
java.lang.Math .acos ( v4,v5 );
/* move-result-wide v4 */
/* mul-double/2addr v4, v11 */
/* div-double/2addr v4, v9 */
/* double-to-float v4, v4 */
/* .line 191 */
/* .restart local v4 # "angle2":F */
} // :goto_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "angle1 = "; // const-string v6, "angle1 = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v6 = " angle2 = "; // const-string v6, " angle2 = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "CommonUtil"; // const-string v6, "CommonUtil"
android.util.Slog .i ( v6,v5 );
/* .line 194 */
/* sub-float v5, v3, v4 */
/* .line 195 */
/* .local v5, "angleOffset":F */
/* add-float v7, v3, v4 */
/* .line 196 */
/* .local v7, "angleSum":F */
/* const/high16 v8, 0x43340000 # 180.0f */
/* sub-float v9, v8, v4 */
/* sub-float v9, v3, v9 */
/* .line 198 */
/* .local v9, "angleOffset2":F */
int v10 = 0; // const/4 v10, 0x0
/* cmpl-float v11, v0, v10 */
/* if-lez v11, :cond_7 */
/* .line 199 */
/* cmpg-float v11, p0, v10 */
/* if-gez v11, :cond_4 */
/* cmpl-float v11, v5, v10 */
/* if-gtz v11, :cond_5 */
} // :cond_4
/* cmpl-float v11, p0, v10 */
/* if-lez v11, :cond_6 */
/* cmpg-float v10, v5, v10 */
/* if-gez v10, :cond_6 */
/* .line 200 */
} // :cond_5
v10 = java.lang.Math .abs ( v5 );
/* sub-float/2addr v8, v10 */
/* .local v8, "resultAngle":F */
/* .line 202 */
} // .end local v8 # "resultAngle":F
} // :cond_6
v10 = java.lang.Math .abs ( v5 );
/* add-float/2addr v8, v10 */
/* .restart local v8 # "resultAngle":F */
/* .line 205 */
} // .end local v8 # "resultAngle":F
} // :cond_7
/* cmpg-float v11, p0, v10 */
/* if-gez v11, :cond_9 */
/* .line 206 */
/* cmpl-float v10, v9, v10 */
/* if-lez v10, :cond_8 */
/* .line 207 */
/* const/high16 v10, 0x43b40000 # 360.0f */
/* sub-float v8, v7, v8 */
/* sub-float v8, v10, v8 */
/* .restart local v8 # "resultAngle":F */
/* .line 209 */
} // .end local v8 # "resultAngle":F
} // :cond_8
/* sub-float/2addr v8, v7 */
/* .restart local v8 # "resultAngle":F */
/* .line 212 */
} // .end local v8 # "resultAngle":F
} // :cond_9
/* cmpl-float v10, v9, v10 */
/* if-lez v10, :cond_a */
/* .line 213 */
/* const/high16 v8, -0x3ccc0000 # -180.0f */
/* add-float/2addr v8, v7 */
/* .restart local v8 # "resultAngle":F */
/* .line 215 */
} // .end local v8 # "resultAngle":F
} // :cond_a
/* add-float/2addr v8, v7 */
/* .line 219 */
/* .restart local v8 # "resultAngle":F */
} // :goto_2
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "result Angle = "; // const-string v11, "result Angle = "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v10 );
/* .line 220 */
} // .end method
public static Integer calculatePKAngleV2 ( Float p0, Float p1, Float p2, Float p3, Float p4, Float p5 ) {
/* .locals 10 */
/* .param p0, "kX" # F */
/* .param p1, "kY" # F */
/* .param p2, "kZ" # F */
/* .param p3, "pX" # F */
/* .param p4, "pY" # F */
/* .param p5, "pZ" # F */
/* .line 225 */
/* mul-float v0, p0, p0 */
/* mul-float v1, p1, p1 */
/* add-float/2addr v0, v1 */
/* mul-float v1, p2, p2 */
/* add-float/2addr v0, v1 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .invSqrt ( v0 );
/* .line 226 */
/* .local v0, "recipNorm":F */
/* mul-float/2addr p0, v0 */
/* .line 227 */
/* mul-float/2addr p1, v0 */
/* .line 228 */
/* mul-float/2addr p2, v0 */
/* .line 230 */
/* mul-float v1, p3, p3 */
/* mul-float v2, p4, p4 */
/* add-float/2addr v1, v2 */
/* mul-float v2, p5, p5 */
/* add-float/2addr v1, v2 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .invSqrt ( v1 );
/* .line 231 */
/* mul-float/2addr p3, v0 */
/* .line 232 */
/* mul-float/2addr p4, v0 */
/* .line 233 */
/* mul-float/2addr p5, v0 */
/* .line 235 */
int v1 = 0; // const/4 v1, 0x0
/* .line 236 */
/* .local v1, "deviation1":F */
int v2 = 0; // const/4 v2, 0x0
/* .line 237 */
/* .local v2, "deviation2":F */
/* const/high16 v3, 0x42c80000 # 100.0f */
/* .line 238 */
/* .local v3, "min_deviation":F */
int v4 = 0; // const/4 v4, 0x0
/* .line 240 */
/* .local v4, "min_deviation_angle":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* const/16 v6, 0x168 */
/* if-gt v5, v6, :cond_4 */
/* .line 243 */
/* const/16 v6, 0x5a */
/* if-gt v5, v6, :cond_0 */
/* .line 244 */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_cos;
/* aget v6, v6, v5 */
/* mul-float v7, v6, p0 */
v8 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_sin;
/* aget v8, v8, v5 */
/* mul-float v9, v8, p2 */
/* sub-float/2addr v7, v9 */
/* add-float/2addr v7, p3 */
/* .line 245 */
} // .end local v1 # "deviation1":F
/* .local v7, "deviation1":F */
/* mul-float/2addr v6, p2 */
/* mul-float/2addr v8, p0 */
/* add-float/2addr v6, v8 */
/* add-float/2addr v6, p5 */
/* move v2, v6 */
/* move v1, v7 */
} // .end local v2 # "deviation2":F
/* .local v6, "deviation2":F */
/* goto/16 :goto_1 */
/* .line 246 */
} // .end local v6 # "deviation2":F
} // .end local v7 # "deviation1":F
/* .restart local v1 # "deviation1":F */
/* .restart local v2 # "deviation2":F */
} // :cond_0
/* const/16 v7, 0xb4 */
/* if-le v5, v6, :cond_1 */
/* if-gt v5, v7, :cond_1 */
/* .line 247 */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_cos;
/* rsub-int v7, v5, 0xb4 */
/* aget v7, v6, v7 */
/* neg-float v7, v7 */
/* mul-float/2addr v7, p0 */
v8 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_sin;
/* rsub-int v9, v5, 0xb4 */
/* aget v9, v8, v9 */
/* mul-float/2addr v9, p2 */
/* sub-float/2addr v7, v9 */
/* add-float/2addr v7, p3 */
/* .line 248 */
} // .end local v1 # "deviation1":F
/* .restart local v7 # "deviation1":F */
/* rsub-int v1, v5, 0xb4 */
/* aget v1, v6, v1 */
/* neg-float v1, v1 */
/* mul-float/2addr v1, p2 */
/* rsub-int v6, v5, 0xb4 */
/* aget v6, v8, v6 */
/* mul-float/2addr v6, p0 */
/* add-float/2addr v1, v6 */
/* add-float/2addr v1, p5 */
/* move v2, v1 */
/* move v1, v7 */
} // .end local v2 # "deviation2":F
/* .local v1, "deviation2":F */
/* .line 249 */
} // .end local v7 # "deviation1":F
/* .local v1, "deviation1":F */
/* .restart local v2 # "deviation2":F */
} // :cond_1
/* if-le v5, v7, :cond_2 */
/* const/16 v6, 0x10e */
/* if-gt v5, v6, :cond_2 */
/* .line 250 */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_cos;
/* add-int/lit16 v7, v5, -0xb4 */
/* aget v7, v6, v7 */
/* neg-float v7, v7 */
/* mul-float/2addr v7, p0 */
v8 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_sin;
/* add-int/lit16 v9, v5, -0xb4 */
/* aget v9, v8, v9 */
/* neg-float v9, v9 */
/* mul-float/2addr v9, p2 */
/* sub-float/2addr v7, v9 */
/* add-float/2addr v7, p3 */
/* .line 251 */
} // .end local v1 # "deviation1":F
/* .restart local v7 # "deviation1":F */
/* add-int/lit16 v1, v5, -0xb4 */
/* aget v1, v6, v1 */
/* neg-float v1, v1 */
/* mul-float/2addr v1, p2 */
/* add-int/lit16 v6, v5, -0xb4 */
/* aget v6, v8, v6 */
/* neg-float v6, v6 */
/* mul-float/2addr v6, p0 */
/* add-float/2addr v1, v6 */
/* add-float/2addr v1, p5 */
/* move v2, v1 */
/* move v1, v7 */
} // .end local v2 # "deviation2":F
/* .local v1, "deviation2":F */
/* .line 253 */
} // .end local v7 # "deviation1":F
/* .local v1, "deviation1":F */
/* .restart local v2 # "deviation2":F */
} // :cond_2
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_cos;
/* rsub-int v7, v5, 0x168 */
/* aget v7, v6, v7 */
/* mul-float/2addr v7, p0 */
v8 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.angle_sin;
/* rsub-int v9, v5, 0x168 */
/* aget v9, v8, v9 */
/* neg-float v9, v9 */
/* mul-float/2addr v9, p2 */
/* sub-float/2addr v7, v9 */
/* add-float/2addr v7, p3 */
/* .line 254 */
} // .end local v1 # "deviation1":F
/* .restart local v7 # "deviation1":F */
/* rsub-int v1, v5, 0x168 */
/* aget v1, v6, v1 */
/* mul-float/2addr v1, p2 */
/* rsub-int v6, v5, 0x168 */
/* aget v6, v8, v6 */
/* neg-float v6, v6 */
/* mul-float/2addr v6, p0 */
/* add-float/2addr v1, v6 */
/* add-float/2addr v1, p5 */
/* move v2, v1 */
/* move v1, v7 */
/* .line 256 */
} // .end local v7 # "deviation1":F
/* .restart local v1 # "deviation1":F */
} // :goto_1
v6 = java.lang.Math .abs ( v1 );
v7 = java.lang.Math .abs ( v2 );
/* add-float/2addr v6, v7 */
/* cmpg-float v6, v6, v3 */
/* if-gez v6, :cond_3 */
/* .line 257 */
v6 = java.lang.Math .abs ( v1 );
v7 = java.lang.Math .abs ( v2 );
/* add-float/2addr v6, v7 */
/* .line 258 */
} // .end local v3 # "min_deviation":F
/* .local v6, "min_deviation":F */
/* move v3, v5 */
/* move v4, v3 */
/* move v3, v6 */
/* .line 240 */
} // .end local v6 # "min_deviation":F
/* .restart local v3 # "min_deviation":F */
} // :cond_3
/* add-int/lit8 v5, v5, 0x1 */
/* goto/16 :goto_0 */
/* .line 263 */
} // .end local v5 # "i":I
} // :cond_4
v5 = java.lang.Math .abs ( p4 );
v6 = java.lang.Math .abs ( p1 );
/* cmpl-float v5, v5, v6 */
/* if-lez v5, :cond_5 */
v5 = java.lang.Math .abs ( p4 );
} // :cond_5
v5 = java.lang.Math .abs ( p1 );
/* .line 265 */
/* .local v5, "accel_angle_error":F */
} // :goto_2
/* const v6, 0x3f7ae148 # 0.98f */
/* cmpl-float v6, v5, v6 */
/* if-lez v6, :cond_6 */
int v6 = -1; // const/4 v6, -0x1
} // :cond_6
/* move v6, v4 */
} // :goto_3
} // .end method
public static synchronized Boolean checkSum ( Object[] p0, Integer p1, Integer p2, Object p3 ) {
/* .locals 3 */
/* .param p0, "data" # [B */
/* .param p1, "start" # I */
/* .param p2, "length" # I */
/* .param p3, "sum" # B */
/* const-class v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil; */
/* monitor-enter v0 */
/* .line 151 */
try { // :try_start_0
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( p0,p1,p2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 152 */
/* .local v1, "dsum":B */
/* if-ne v1, p3, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 150 */
} // .end local v1 # "dsum":B
} // .end local p0 # "data":[B
} // .end local p1 # "start":I
} // .end local p2 # "length":I
} // .end local p3 # "sum":B
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
public static commandMiAuthStep3Type1ForIIC ( Object[] p0, Object[] p1 ) {
/* .locals 6 */
/* .param p0, "keyMeta" # [B */
/* .param p1, "challenge" # [B */
/* .line 402 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 403 */
/* .local v0, "temp":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 404 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 405 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 406 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 407 */
/* const/16 v1, 0x4f */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v1, v0, v3 */
/* .line 408 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x31 */
/* aput-byte v4, v0, v1 */
/* .line 409 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v4, -0x80 */
/* aput-byte v4, v0, v1 */
/* .line 410 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v4, 0x38 */
/* aput-byte v4, v0, v1 */
/* .line 411 */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$AUTH_COMMAND.AUTH_STEP3;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$AUTH_COMMAND ) v1 ).getCommand ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->getCommand()B
/* const/16 v4, 0x8 */
/* aput-byte v1, v0, v4 */
/* .line 412 */
/* const/16 v1, 0x9 */
/* const/16 v4, 0x14 */
/* aput-byte v4, v0, v1 */
/* .line 414 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* array-length v1, p0 */
/* if-ne v1, v3, :cond_0 */
/* .line 415 */
/* const/16 v1, 0xa */
java.lang.System .arraycopy ( p0,v2,v0,v1,v3 );
/* .line 417 */
} // :cond_0
/* const/16 v1, 0xe */
if ( p1 != null) { // if-eqz p1, :cond_1
/* array-length v4, p1 */
/* const/16 v5, 0x10 */
/* if-ne v4, v5, :cond_1 */
/* .line 418 */
java.lang.System .arraycopy ( p1,v2,v0,v1,v5 );
/* .line 419 */
/* const/16 v1, 0x1b */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v3,v1 );
/* const/16 v2, 0x1e */
/* aput-byte v1, v0, v2 */
/* .line 421 */
} // :cond_1
/* const/16 v2, 0xb */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v3,v2 );
/* aput-byte v2, v0, v1 */
/* .line 423 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send 3-1 auth = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CommonUtil"; // const-string v2, "CommonUtil"
android.util.Slog .i ( v2,v1 );
/* .line 424 */
} // .end method
public static commandMiAuthStep3Type1ForUSB ( Object[] p0, Object[] p1 ) {
/* .locals 5 */
/* .param p0, "keyMeta" # [B */
/* .param p1, "challenge" # [B */
/* .line 468 */
/* const/16 v0, 0x1b */
/* new-array v0, v0, [B */
/* .line 469 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 470 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x31 */
/* aput-byte v3, v0, v1 */
/* .line 471 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, -0x80 */
/* aput-byte v3, v0, v1 */
/* .line 472 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v3, 0x38 */
/* aput-byte v3, v0, v1 */
/* .line 473 */
/* const/16 v1, 0x32 */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v1, v0, v3 */
/* .line 474 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x14 */
/* aput-byte v4, v0, v1 */
/* .line 475 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* array-length v1, p0 */
/* if-ne v1, v3, :cond_0 */
/* .line 476 */
int v1 = 6; // const/4 v1, 0x6
java.lang.System .arraycopy ( p0,v2,v0,v1,v3 );
/* .line 478 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* array-length v1, p1 */
/* const/16 v3, 0x10 */
/* if-ne v1, v3, :cond_1 */
/* .line 479 */
/* const/16 v1, 0xa */
java.lang.System .arraycopy ( p1,v2,v0,v1,v3 );
/* .line 481 */
} // :cond_1
/* const/16 v1, 0x1a */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 482 */
} // .end method
public static commandMiAuthStep5Type1ForIIC ( Object[] p0 ) {
/* .locals 6 */
/* .param p0, "token" # [B */
/* .line 428 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 429 */
/* .local v0, "temp":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 430 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 431 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 432 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 433 */
/* const/16 v1, 0x4f */
int v3 = 4; // const/4 v3, 0x4
/* aput-byte v1, v0, v3 */
/* .line 434 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v4, 0x31 */
/* aput-byte v4, v0, v1 */
/* .line 435 */
int v1 = 6; // const/4 v1, 0x6
/* const/16 v4, -0x80 */
/* aput-byte v4, v0, v1 */
/* .line 436 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v4, 0x38 */
/* aput-byte v4, v0, v1 */
/* .line 437 */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$AUTH_COMMAND.AUTH_STEP5_1;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$AUTH_COMMAND ) v1 ).getCommand ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->getCommand()B
/* const/16 v4, 0x8 */
/* aput-byte v1, v0, v4 */
/* .line 438 */
/* const/16 v1, 0x9 */
/* const/16 v4, 0x10 */
/* aput-byte v4, v0, v1 */
/* .line 439 */
/* const/16 v1, 0xa */
if ( p0 != null) { // if-eqz p0, :cond_0
/* array-length v5, p0 */
/* if-ne v5, v4, :cond_0 */
/* .line 440 */
java.lang.System .arraycopy ( p0,v2,v0,v1,v4 );
/* .line 441 */
/* const/16 v1, 0x1a */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v3,v1 );
/* aput-byte v2, v0, v1 */
/* .line 443 */
} // :cond_0
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v3,v1 );
/* aput-byte v2, v0, v1 */
/* .line 445 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send 5-1 auth = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CommonUtil"; // const-string v2, "CommonUtil"
android.util.Slog .i ( v2,v1 );
/* .line 446 */
} // .end method
public static commandMiDevAuthInitForIIC ( ) {
/* .locals 5 */
/* .line 379 */
/* const/16 v0, 0x44 */
/* new-array v0, v0, [B */
/* .line 380 */
/* .local v0, "temp":[B */
/* const/16 v1, -0x56 */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 381 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x42 */
/* aput-byte v3, v0, v1 */
/* .line 382 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v3, 0x32 */
/* aput-byte v3, v0, v1 */
/* .line 383 */
int v1 = 3; // const/4 v1, 0x3
/* aput-byte v2, v0, v1 */
/* .line 384 */
/* const/16 v1, 0x4f */
int v2 = 4; // const/4 v2, 0x4
/* aput-byte v1, v0, v2 */
/* .line 385 */
int v1 = 5; // const/4 v1, 0x5
/* const/16 v3, 0x31 */
/* aput-byte v3, v0, v1 */
/* .line 386 */
/* const/16 v1, -0x80 */
int v3 = 6; // const/4 v3, 0x6
/* aput-byte v1, v0, v3 */
/* .line 387 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v4, 0x38 */
/* aput-byte v4, v0, v1 */
/* .line 388 */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$AUTH_COMMAND.AUTH_START;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$AUTH_COMMAND ) v1 ).getCommand ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->getCommand()B
/* const/16 v4, 0x8 */
/* aput-byte v1, v0, v4 */
/* .line 389 */
/* const/16 v1, 0x9 */
/* aput-byte v3, v0, v1 */
/* .line 390 */
/* const/16 v1, 0xa */
/* const/16 v3, 0x4d */
/* aput-byte v3, v0, v1 */
/* .line 391 */
/* const/16 v1, 0xb */
/* const/16 v3, 0x49 */
/* aput-byte v3, v0, v1 */
/* .line 392 */
/* const/16 v1, 0xc */
/* const/16 v3, 0x41 */
/* aput-byte v3, v0, v1 */
/* .line 393 */
/* const/16 v1, 0x55 */
/* const/16 v3, 0xd */
/* aput-byte v1, v0, v3 */
/* .line 394 */
/* const/16 v1, 0xe */
/* const/16 v4, 0x54 */
/* aput-byte v4, v0, v1 */
/* .line 395 */
/* const/16 v1, 0xf */
/* const/16 v4, 0x48 */
/* aput-byte v4, v0, v1 */
/* .line 396 */
/* const/16 v1, 0x10 */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v3 );
/* aput-byte v2, v0, v1 */
/* .line 397 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "send init auth = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "CommonUtil"; // const-string v2, "CommonUtil"
android.util.Slog .i ( v2,v1 );
/* .line 398 */
} // .end method
public static commandMiDevAuthInitForUSB ( ) {
/* .locals 5 */
/* .line 450 */
/* const/16 v0, 0xd */
/* new-array v0, v0, [B */
/* .line 451 */
/* .local v0, "bytes":[B */
/* const/16 v1, 0x4f */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 452 */
int v1 = 1; // const/4 v1, 0x1
/* const/16 v3, 0x31 */
/* aput-byte v3, v0, v1 */
/* .line 453 */
int v1 = 2; // const/4 v1, 0x2
/* const/16 v4, -0x80 */
/* aput-byte v4, v0, v1 */
/* .line 454 */
int v1 = 3; // const/4 v1, 0x3
/* const/16 v4, 0x38 */
/* aput-byte v4, v0, v1 */
/* .line 455 */
int v1 = 4; // const/4 v1, 0x4
/* aput-byte v3, v0, v1 */
/* .line 456 */
int v1 = 5; // const/4 v1, 0x5
int v3 = 6; // const/4 v3, 0x6
/* aput-byte v3, v0, v1 */
/* .line 457 */
/* const/16 v1, 0x4d */
/* aput-byte v1, v0, v3 */
/* .line 458 */
int v1 = 7; // const/4 v1, 0x7
/* const/16 v3, 0x49 */
/* aput-byte v3, v0, v1 */
/* .line 459 */
/* const/16 v1, 0x8 */
/* const/16 v3, 0x41 */
/* aput-byte v3, v0, v1 */
/* .line 460 */
/* const/16 v1, 0x9 */
/* const/16 v3, 0x55 */
/* aput-byte v3, v0, v1 */
/* .line 461 */
/* const/16 v1, 0xa */
/* const/16 v3, 0x54 */
/* aput-byte v3, v0, v1 */
/* .line 462 */
/* const/16 v1, 0xb */
/* const/16 v3, 0x48 */
/* aput-byte v3, v0, v1 */
/* .line 463 */
/* const/16 v1, 0xc */
v2 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getSum ( v0,v2,v1 );
/* aput-byte v2, v0, v1 */
/* .line 464 */
} // .end method
public static Integer getBackLightBrightnessWithSensor ( Float p0 ) {
/* .locals 3 */
/* .param p0, "brightness" # F */
/* .line 486 */
int v0 = 0; // const/4 v0, 0x0
p0 = java.lang.Math .max ( p0,v0 );
/* .line 487 */
/* const/high16 v0, 0x42480000 # 50.0f */
p0 = java.lang.Math .min ( p0,v0 );
/* .line 488 */
/* const/high16 v0, 0x40a00000 # 5.0f */
/* div-float v0, p0, v0 */
v0 = java.lang.Math .round ( v0 );
/* .line 489 */
/* .local v0, "level":I */
int v1 = 0; // const/4 v1, 0x0
v0 = java.lang.Math .max ( v0,v1 );
/* .line 490 */
/* const/16 v1, 0xa */
v0 = java.lang.Math .min ( v0,v1 );
/* .line 491 */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.BRIGHTNESS_SETTINGS_RELATION;
java.lang.Integer .valueOf ( v0 );
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* .line 493 */
/* .local v1, "result":F */
/* const/high16 v2, 0x3f000000 # 0.5f */
/* cmpl-float v2, v1, v2 */
/* if-lez v2, :cond_0 */
v2 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v2 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v2 ).isConnectBleLocked ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
/* .line 494 */
v2 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v2 ).isConnectIICLocked ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v2, :cond_0 */
/* .line 495 */
/* const/high16 v1, 0x3f000000 # 0.5f */
/* .line 497 */
} // :cond_0
/* const/high16 v2, 0x42c80000 # 100.0f */
/* mul-float/2addr v2, v1 */
v2 = java.lang.Math .round ( v2 );
} // .end method
public static synchronized Object getSum ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p0, "data" # [B */
/* .param p1, "start" # I */
/* .param p2, "length" # I */
/* const-class v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil; */
/* monitor-enter v0 */
/* .line 143 */
int v1 = 0; // const/4 v1, 0x0
/* .line 144 */
/* .local v1, "sum":B */
/* move v2, p1 */
/* .local v2, "i":I */
} // :goto_0
/* add-int v3, p1, p2 */
/* if-ge v2, v3, :cond_0 */
/* .line 145 */
try { // :try_start_0
/* aget-byte v3, p0, v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* add-int/2addr v3, v1 */
/* int-to-byte v1, v3 */
/* .line 144 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 142 */
} // .end local v1 # "sum":B
} // .end local v2 # "i":I
} // .end local p0 # "data":[B
} // .end local p1 # "start":I
} // .end local p2 # "length":I
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
/* .line 147 */
/* .restart local v1 # "sum":B */
/* .restart local p0 # "data":[B */
/* .restart local p1 # "start":I */
/* .restart local p2 # "length":I */
} // :cond_0
/* monitor-exit v0 */
} // .end method
public static getYearMonthDayByTimestamp ( Long p0 ) {
/* .locals 5 */
/* .param p0, "timestamp" # J */
/* .line 356 */
java.util.Calendar .getInstance ( );
/* .line 357 */
/* .local v0, "calendar":Ljava/util/Calendar; */
(( java.util.Calendar ) v0 ).setTimeInMillis ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V
/* .line 358 */
int v1 = 3; // const/4 v1, 0x3
/* new-array v1, v1, [I */
/* .line 359 */
/* .local v1, "ret":[I */
int v2 = 1; // const/4 v2, 0x1
v3 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
int v4 = 0; // const/4 v4, 0x0
/* aput v3, v1, v4 */
/* .line 360 */
int v3 = 2; // const/4 v3, 0x2
v4 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v4, v2 */
/* aput v4, v1, v2 */
/* .line 361 */
int v2 = 5; // const/4 v2, 0x5
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* aput v2, v1, v3 */
/* .line 362 */
} // .end method
public static Boolean hasTouchpad ( Object p0 ) {
/* .locals 1 */
/* .param p0, "type" # B */
/* .line 514 */
/* const/16 v0, 0x20 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x21 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x10 */
/* if-ne p0, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public static int2Bytes ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "data" # I */
/* .line 342 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v1, v0, [B */
/* .line 343 */
/* .local v1, "b":[B */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v0, :cond_0 */
/* .line 344 */
/* mul-int/lit8 v3, v2, 0x8 */
/* rsub-int/lit8 v3, v3, 0x18 */
/* shr-int v3, p0, v3 */
/* int-to-byte v3, v3 */
/* aput-byte v3, v1, v2 */
/* .line 343 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 346 */
} // .end local v2 # "i":I
} // :cond_0
} // .end method
public static Boolean interceptShortCutKeyIfCustomDefined ( android.content.Context p0, android.view.KeyEvent p1, com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE p2 ) {
/* .locals 7 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "stage" # Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE; */
/* .line 661 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 662 */
/* .local v0, "keyCode":I */
v1 = com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE.BEFORE_DISPATCHING;
int v2 = 0; // const/4 v2, 0x0
/* if-ne p2, v1, :cond_7 */
/* .line 663 */
/* int-to-long v3, v0 */
/* .line 664 */
/* .local v3, "shortCutKey":J */
v1 = (( android.view.KeyEvent ) p1 ).getMetaState ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I
/* and-int/lit8 v1, v1, 0x32 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 665 */
v1 = (( android.view.KeyEvent ) p1 ).getMetaState ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I
/* and-int/lit8 v1, v1, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 666 */
/* const-wide v5, 0x2000000000L */
/* or-long/2addr v3, v5 */
/* .line 667 */
} // :cond_0
v1 = (( android.view.KeyEvent ) p1 ).getMetaState ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I
/* and-int/lit8 v1, v1, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 668 */
/* const-wide v5, 0x1000000000L */
/* or-long/2addr v3, v5 */
/* .line 670 */
} // :cond_1
} // :goto_0
/* const-wide v5, 0x200000000L */
/* or-long/2addr v3, v5 */
/* .line 672 */
} // :cond_2
v1 = (( android.view.KeyEvent ) p1 ).isMetaPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isMetaPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 673 */
/* const-wide/high16 v5, 0x1000000000000L */
/* or-long/2addr v3, v5 */
/* .line 675 */
} // :cond_3
v1 = (( android.view.KeyEvent ) p1 ).isCtrlPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isCtrlPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 676 */
/* const-wide v5, 0x100000000000L */
/* or-long/2addr v3, v5 */
/* .line 678 */
} // :cond_4
v1 = (( android.view.KeyEvent ) p1 ).isShiftPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 679 */
/* const-wide v5, 0x100000000L */
/* or-long/2addr v3, v5 */
/* .line 681 */
} // :cond_5
/* nop */
/* .line 682 */
com.miui.server.input.util.MiuiCustomizeShortCutUtils .getInstance ( p0 );
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v1 ).getMiuiKeyboardShortcutInfo ( ); // invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getMiuiKeyboardShortcutInfo()Landroid/util/LongSparseArray;
/* .line 683 */
/* .local v1, "shortCutInfos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 684 */
(( android.util.LongSparseArray ) v1 ).get ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
if ( v5 != null) { // if-eqz v5, :cond_6
(( android.util.LongSparseArray ) v1 ).get ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v5, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v5 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v5 ).isEnable ( ); // invoke-virtual {v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z
/* if-nez v5, :cond_6 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_6
/* .line 687 */
} // .end local v1 # "shortCutInfos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
} // .end local v3 # "shortCutKey":J
} // :cond_7
} // .end method
public static Float invSqrt ( Float p0 ) {
/* .locals 4 */
/* .param p0, "num" # F */
/* .line 156 */
/* const/high16 v0, 0x3f000000 # 0.5f */
/* mul-float/2addr v0, p0 */
/* .line 157 */
/* .local v0, "xHalf":F */
v1 = java.lang.Float .floatToIntBits ( p0 );
/* .line 158 */
/* .local v1, "temp":I */
/* shr-int/lit8 v2, v1, 0x1 */
/* const v3, 0x5f3759df */
/* sub-int/2addr v3, v2 */
/* .line 159 */
} // .end local v1 # "temp":I
/* .local v3, "temp":I */
p0 = java.lang.Float .intBitsToFloat ( v3 );
/* .line 160 */
/* mul-float v1, v0, p0 */
/* mul-float/2addr v1, p0 */
/* const/high16 v2, 0x3fc00000 # 1.5f */
/* sub-float/2addr v2, v1 */
/* mul-float/2addr p0, v2 */
/* .line 161 */
} // .end method
public static Boolean isKeyboardSupportMuteLight ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "type" # I */
/* .line 508 */
/* const/16 v0, 0x21 */
int v1 = 1; // const/4 v1, 0x1
/* if-eq p0, v0, :cond_1 */
/* if-eq p0, v1, :cond_1 */
/* const/16 v0, 0x10 */
/* if-ne p0, v0, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
} // .end method
public static Boolean isXM2022MCU ( ) {
/* .locals 2 */
/* .line 692 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil.mMcuVersion;
if ( v0 != null) { // if-eqz v0, :cond_0
final String v1 = "XM2022"; // const-string v1, "XM2022"
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static void notifySettingsKeyboardStatusChanged ( android.content.Context p0, android.os.Bundle p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "data" # Landroid/os/Bundle; */
/* .line 501 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.xiaomi.input.action.keyboard.KEYBOARD_STATUS_CHANGED"; // const-string v1, "com.xiaomi.input.action.keyboard.KEYBOARD_STATUS_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 502 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 503 */
(( android.content.Intent ) v0 ).putExtras ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 504 */
v1 = android.os.UserHandle.OWNER;
(( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 505 */
return;
} // .end method
public static void operationWait ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "ms" # I */
/* .line 367 */
try { // :try_start_0
java.lang.Thread .currentThread ( );
/* int-to-long v0, p0 */
java.lang.Thread .sleep ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 371 */
/* .line 368 */
/* :catch_0 */
/* move-exception v0 */
/* .line 369 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
(( java.lang.InterruptedException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 370 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v1 ).interrupt ( ); // invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
/* .line 372 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_0
return;
} // .end method
public static void setMcuVersion ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p0, "version" # Ljava/lang/String; */
/* .line 696 */
/* .line 697 */
return;
} // .end method
public static Boolean supportCalculateAngle ( ) {
/* .locals 4 */
/* .line 701 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
/* .line 702 */
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* .line 704 */
/* .local v0, "iicConnected":Z */
v1 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
/* .line 705 */
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectBleLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
} // :cond_0
/* move v1, v3 */
/* .line 706 */
/* .local v1, "bleConnected":Z */
} // :goto_0
/* if-nez v0, :cond_2 */
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
/* move v2, v3 */
} // :cond_2
} // :goto_1
} // .end method
