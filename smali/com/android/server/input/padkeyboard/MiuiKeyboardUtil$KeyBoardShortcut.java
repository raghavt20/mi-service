public class com.android.server.input.padkeyboard.MiuiKeyboardUtil$KeyBoardShortcut {
	 /* .source "MiuiKeyboardUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "KeyBoardShortcut" */
} // .end annotation
/* # static fields */
public static final java.lang.String ADJUST_SPLIT_SCREEN;
public static final java.lang.String BACK_ESC;
public static final java.lang.String BACK_HOME;
public static final java.lang.String BRIGHTNESS_DOWN;
public static final java.lang.String BRIGHTNESS_UP;
public static final java.lang.String CLOSE_WINDOW;
public static final java.lang.String DELETE_WORD;
public static final java.lang.String FULL_SCREEN;
public static final java.lang.String LAUNCH_CONTROL_CENTER;
public static final java.lang.String LAUNCH_NOTIFICATION_CENTER;
public static final java.lang.String LAUNCH_RECENTS_TASKS;
public static final java.lang.String LOCK_SCREEN;
public static final java.lang.String MEDIA_NEXT;
public static final java.lang.String MEDIA_PLAY_PAUSE;
public static final java.lang.String MEDIA_PREVIOUS;
public static final java.lang.String MUTE_MODE;
public static final java.lang.String OPEN_APP;
public static final java.lang.String SCREENSHOT;
public static final java.lang.String SCREEN_SHOT_PARTIAL;
public static final java.lang.String SHOW_DOCK;
public static final java.lang.String SHOW_SHORTCUT_LIST;
public static final java.lang.String SMALL_WINDOW;
public static final java.lang.String TOGGLE_RECENT_APP;
public static final Integer TYPE_APP;
public static final Integer TYPE_CLOSEAPP;
public static final Integer TYPE_CONTROLPANEL;
public static final Integer TYPE_FULLSCREEN;
public static final Integer TYPE_HOME;
public static final Integer TYPE_LEFTSPLITSCREEN;
public static final Integer TYPE_LOCKSCREEN;
public static final Integer TYPE_NOTIFICATIONPANLE;
public static final Integer TYPE_RECENT;
public static final Integer TYPE_RIGHTSPLITSCREEN;
public static final Integer TYPE_SCREENSHOT;
public static final Integer TYPE_SCREENSHOTPARTIAL;
public static final Integer TYPE_SMALLWINDOW;
public static final java.lang.String VOICE_ASSISTANT;
public static final java.lang.String VOICE_TO_WORD;
public static final java.lang.String VOLUME_DOWN;
public static final java.lang.String VOLUME_MUTE;
public static final java.lang.String VOLUME_UP;
public static final java.lang.String ZEN_MODE;
/* # direct methods */
public com.android.server.input.padkeyboard.MiuiKeyboardUtil$KeyBoardShortcut ( ) {
/* .locals 0 */
/* .line 519 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.lang.String getShortCutNameByType ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 1 */
/* .param p0, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 567 */
v0 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p0 ).getType ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 596 */
int v0 = 0; // const/4 v0, 0x0
/* .line 592 */
/* :pswitch_0 */
/* const-string/jumbo v0, "\u8c03\u6574\u5206\u5c4f\u6bd4\u4f8b" */
/* .line 589 */
/* :pswitch_1 */
/* const-string/jumbo v0, "\u8fdb\u5165\u5168\u5c4f\u6a21\u5f0f" */
/* .line 587 */
/* :pswitch_2 */
/* const-string/jumbo v0, "\u8fdb\u5165\u5c0f\u7a97\u6a21\u5f0f" */
/* .line 585 */
/* :pswitch_3 */
/* const-string/jumbo v0, "\u5173\u95ed\u5f53\u524d\u7a97\u53e3" */
/* .line 583 */
/* :pswitch_4 */
/* const-string/jumbo v0, "\u533a\u57df\u622a\u5c4f" */
/* .line 581 */
/* :pswitch_5 */
/* const-string/jumbo v0, "\u622a\u5168\u5c4f" */
/* .line 579 */
/* :pswitch_6 */
/* const-string/jumbo v0, "\u9501\u5c4f" */
/* .line 577 */
/* :pswitch_7 */
/* const-string/jumbo v0, "\u56de\u5230\u684c\u9762" */
/* .line 575 */
/* :pswitch_8 */
/* const-string/jumbo v0, "\u663e\u793a\u6700\u8fd1\u4efb\u52a1" */
/* .line 573 */
/* :pswitch_9 */
/* const-string/jumbo v0, "\u6253\u5f00\u901a\u77e5\u4e2d\u5fc3" */
/* .line 571 */
/* :pswitch_a */
/* const-string/jumbo v0, "\u6253\u5f00\u63a7\u5236\u4e2d\u5fc3" */
/* .line 569 */
/* :pswitch_b */
/* const-string/jumbo v0, "\u6253\u5f00\u81ea\u5b9a\u4e49\u5e94\u7528" */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static java.lang.String getShortcutNameByKeyCode ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "keyCode" # I */
/* .line 601 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 603 */
/* :sswitch_0 */
/* const-string/jumbo v0, "\u9501\u5c4f" */
/* .line 605 */
/* :sswitch_1 */
/* const-string/jumbo v0, "\u52ff\u6270" */
/* .line 607 */
/* :sswitch_2 */
/* const-string/jumbo v0, "\u5c0f\u7231\u540c\u5b66" */
/* .line 609 */
/* :sswitch_3 */
/* const-string/jumbo v0, "\u9ea6\u514b\u98ce\u5f00\u5173" */
/* .line 625 */
/* :sswitch_4 */
/* const-string/jumbo v0, "\u4eae\u5ea6\u52a0" */
/* .line 623 */
/* :sswitch_5 */
/* const-string/jumbo v0, "\u4eae\u5ea6\u51cf" */
/* .line 619 */
/* :sswitch_6 */
/* const-string/jumbo v0, "\u9759\u97f3" */
/* .line 632 */
/* :sswitch_7 */
v0 = miui.hardware.input.MiuiKeyboardHelper .support6FKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 633 */
/* const-string/jumbo v0, "\u5220\u9664" */
/* .line 615 */
/* :sswitch_8 */
/* const-string/jumbo v0, "\u4e0a\u4e00\u66f2" */
/* .line 611 */
/* :sswitch_9 */
/* const-string/jumbo v0, "\u4e0b\u4e00\u66f2" */
/* .line 613 */
/* :sswitch_a */
/* const-string/jumbo v0, "\u6682\u505c\u64ad\u653e" */
/* .line 617 */
/* :sswitch_b */
/* const-string/jumbo v0, "\u97f3\u91cf\u4e0b" */
/* .line 621 */
/* :sswitch_c */
/* const-string/jumbo v0, "\u97f3\u91cf\u4e0a" */
/* .line 627 */
/* :sswitch_d */
v0 = miui.hardware.input.MiuiKeyboardHelper .support6FKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 628 */
	 /* const-string/jumbo v0, "\u8fd4\u56de" */
	 /* .line 639 */
} // :cond_0
} // :goto_0
final String v0 = ""; // const-string v0, ""
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x4 -> :sswitch_d */
/* 0x18 -> :sswitch_c */
/* 0x19 -> :sswitch_b */
/* 0x55 -> :sswitch_a */
/* 0x57 -> :sswitch_9 */
/* 0x58 -> :sswitch_8 */
/* 0x70 -> :sswitch_7 */
/* 0xa4 -> :sswitch_6 */
/* 0xdc -> :sswitch_5 */
/* 0xdd -> :sswitch_4 */
/* 0x270c -> :sswitch_3 */
/* 0x270d -> :sswitch_2 */
/* 0x270e -> :sswitch_1 */
/* 0x270f -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public static java.lang.String getShortcutNameByKeyCodeWithAction ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "keyCode" # I */
/* .param p1, "isLongPress" # Z */
/* .line 643 */
v0 = miui.hardware.input.MiuiKeyboardHelper .support6FKeyboard ( );
final String v1 = ""; // const-string v1, ""
/* if-nez v0, :cond_0 */
/* .line 644 */
/* .line 646 */
} // :cond_0
/* packed-switch p0, :pswitch_data_0 */
/* .line 656 */
/* .line 648 */
/* :pswitch_0 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 649 */
/* const-string/jumbo v0, "\u533a\u57df\u622a\u5c4f" */
/* .line 651 */
} // :cond_1
/* const-string/jumbo v0, "\u622a\u5168\u5c4f" */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x270a */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
