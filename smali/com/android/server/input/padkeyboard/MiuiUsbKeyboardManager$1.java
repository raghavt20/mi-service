class com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$1 extends android.database.ContentObserver {
	 /* .source "MiuiUsbKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.MiuiUsbKeyboardManager this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 234 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 237 */
final String v0 = "keyboard_back_light"; // const-string v0, "keyboard_back_light"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v1 != null) { // if-eqz v1, :cond_4
	 /* .line 238 */
	 v1 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmContext ( v1 );
	 /* .line 239 */
	 (( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 238 */
	 v0 = 	 android.provider.Settings$Secure .getInt ( v5,v0,v4 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* move v0, v4 */
	 } // :cond_0
	 /* move v0, v3 */
} // :goto_0
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fputmShouldEnableBackLight ( v1,v0 );
/* .line 240 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mBackLightObserver onChange:"; // const-string v1, "mBackLightObserver onChange:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmShouldEnableBackLight ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 241 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmIsKeyboardReady ( v0 );
/* if-nez v0, :cond_1 */
/* .line 242 */
return;
/* .line 244 */
} // :cond_1
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmAngleStateController ( v0 );
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboard ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 245 */
return;
/* .line 247 */
} // :cond_2
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmShouldEnableBackLight ( v0 );
/* const/16 v1, 0x23 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 248 */
v0 = this.this$0;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) v0 ).writePadKeyBoardStatus ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 251 */
} // :cond_3
v0 = this.this$0;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) v0 ).writePadKeyBoardStatus ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 254 */
} // :cond_4
final String v0 = "keyboard_auto_upgrade"; // const-string v0, "keyboard_auto_upgrade"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 255 */
v1 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmContext ( v1 );
/* .line 256 */
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 255 */
v0 = android.provider.Settings$Secure .getInt ( v5,v0,v4 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* move v3, v4 */
} // :cond_5
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fputmEnableAutoUpgrade ( v1,v3 );
/* .line 257 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mEnableAutoUpgrade onChange:"; // const-string v1, "mEnableAutoUpgrade onChange:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmEnableAutoUpgrade ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 259 */
} // :cond_6
} // :goto_1
return;
} // .end method
