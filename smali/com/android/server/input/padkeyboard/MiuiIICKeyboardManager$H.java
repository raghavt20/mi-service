class com.android.server.input.padkeyboard.MiuiIICKeyboardManager$H extends android.os.Handler {
	 /* .source "MiuiIICKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
public static final java.lang.String DATA_FEATURE_TYPE;
public static final java.lang.String DATA_FEATURE_VALUE;
public static final java.lang.String DATA_KEY_FIRST_CHECK;
public static final Integer MSG_CHECK_MCU_STATUS;
public static final Integer MSG_CHECK_STAY_WAKE_KEYBOARD;
public static final Integer MSG_GET_KEYBOARD_VERSION;
public static final Integer MSG_GET_MCU_VERSION;
public static final Integer MSG_READ_HALL_DATA;
public static final Integer MSG_READ_KB_STATUS;
public static final Integer MSG_RESTORE_MCU;
public static final Integer MSG_SEND_NFC_DATA;
public static final Integer MSG_SET_BACK_LIGHT;
public static final Integer MSG_SET_CAPS_LOCK_LIGHT;
public static final Integer MSG_SET_KB_STATUS;
public static final Integer MSG_SET_MUTE_LIGHT;
public static final Integer MSG_START_CHECK_AUTH;
public static final Integer MSG_UPGRADE_KEYBOARD;
public static final Integer MSG_UPGRADE_MCU;
public static final Integer MSG_UPGRADE_TOUCHPAD;
/* # instance fields */
final com.android.server.input.padkeyboard.MiuiIICKeyboardManager this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.MiuiIICKeyboardManager$H ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1331 */
this.this$0 = p1;
/* .line 1332 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1333 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1337 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const-string/jumbo v1, "value" */
int v2 = 1; // const/4 v2, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 1408 */
/* :pswitch_0 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmShouldUpgradeTouchPad ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 1409 */
	 v0 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
	 (( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).upgradeTouchPadIfNeed ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->upgradeTouchPadIfNeed()V
	 /* goto/16 :goto_1 */
	 /* .line 1405 */
	 /* :pswitch_1 */
	 v0 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
	 (( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).sendNFCData ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendNFCData()V
	 /* .line 1406 */
	 /* goto/16 :goto_1 */
	 /* .line 1401 */
	 /* :pswitch_2 */
	 (( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
	 v0 = 	 (( android.os.Bundle ) v0 ).getByte ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B
	 /* .line 1402 */
	 /* .local v0, "value":B */
	 v1 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v1 );
	 (( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v1 ).setKeyboardBacklight ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setKeyboardBacklight(B)V
	 /* .line 1403 */
	 /* goto/16 :goto_1 */
	 /* .line 1352 */
} // .end local v0 # "value":B
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).sendGetMCUVersionCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendGetMCUVersionCommand()V
/* .line 1353 */
/* goto/16 :goto_1 */
/* .line 1344 */
/* :pswitch_4 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmShouldUpgradeMCU ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 v0 = 	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 1345 */
		 v0 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
		 (( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).startUpgradeMCUIfNeed ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->startUpgradeMCUIfNeed()V
		 /* goto/16 :goto_1 */
		 /* .line 1398 */
		 /* :pswitch_5 */
		 v0 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
		 (( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).checkHallStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->checkHallStatus()V
		 /* .line 1399 */
		 /* goto/16 :goto_1 */
		 /* .line 1390 */
		 /* :pswitch_6 */
		 v0 = this.this$0;
		 v0 = 		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmShouldStayWakeKeyboard ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* .line 1391 */
			 v0 = this.this$0;
			 (( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) v0 ).wakeKeyboard176 ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->wakeKeyboard176()V
			 /* .line 1392 */
			 v0 = this.this$0;
			 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmHandler ( v0 );
			 int v1 = 7; // const/4 v1, 0x7
			 (( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
			 /* .line 1394 */
			 /* .local v0, "stayWakeMsg":Landroid/os/Message; */
			 v1 = this.this$0;
			 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmHandler ( v1 );
			 /* const-wide/16 v2, 0x2710 */
			 (( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
			 /* .line 1395 */
		 } // .end local v0 # "stayWakeMsg":Landroid/os/Message;
		 /* goto/16 :goto_1 */
		 /* .line 1384 */
		 /* :pswitch_7 */
		 v0 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmCommunicationUtil ( v0 );
		 (( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).getLongRawCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B
		 /* .line 1385 */
		 /* .local v0, "command":[B */
		 v1 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmCommunicationUtil ( v1 );
		 /* const/16 v2, -0x5f */
		 (( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).setCheckMCUStatusCommand ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCheckMCUStatusCommand([BB)V
		 /* .line 1387 */
		 v1 = this.this$0;
		 (( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) v1 ).writeCommandToIIC ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->writeCommandToIIC([B)V
		 /* .line 1388 */
		 /* goto/16 :goto_1 */
		 /* .line 1372 */
	 } // .end local v0 # "command":[B
	 /* :pswitch_8 */
	 v0 = this.this$0;
	 v0 = 	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIsKeyboardReady ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 1373 */
		 final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
		 /* const-string/jumbo v1, "start authentication" */
		 android.util.Slog .i ( v0,v1 );
		 /* .line 1374 */
		 (( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
		 /* .line 1375 */
		 /* .local v0, "bundle":Landroid/os/Bundle; */
		 /* if-nez v0, :cond_0 */
		 /* .line 1376 */
		 /* goto/16 :goto_1 */
		 /* .line 1378 */
	 } // :cond_0
	 final String v1 = "is_first_check"; // const-string v1, "is_first_check"
	 v1 = 	 (( android.os.Bundle ) v0 ).getBoolean ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
	 /* .line 1379 */
	 /* .local v1, "isFirst":Z */
	 v2 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$mdoCheckKeyboardIdentity ( v2,v1 );
	 /* .line 1380 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // .end local v1 # "isFirst":Z
/* .line 1365 */
/* :pswitch_9 */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIsKeyboardReady ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1366 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
int v3 = 0; // const/4 v3, 0x0
v0 = (( android.os.Bundle ) v0 ).getInt ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* if-ne v0, v2, :cond_1 */
} // :cond_1
/* move v2, v3 */
} // :goto_0
/* move v0, v2 */
/* .line 1367 */
/* .local v0, "enable":Z */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v2 = "feature"; // const-string v2, "feature"
int v3 = -1; // const/4 v3, -0x1
v1 = (( android.os.Bundle ) v1 ).getInt ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 1368 */
/* .local v1, "feature":I */
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v2 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v2 ).setKeyboardFeature ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setKeyboardFeature(ZI)V
/* .line 1369 */
} // .end local v0 # "enable":Z
} // .end local v1 # "feature":I
/* .line 1358 */
/* :pswitch_a */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIsKeyboardReady ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1359 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).readKeyboardStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->readKeyboardStatus()V
/* .line 1355 */
/* :pswitch_b */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).sendRestoreMcuCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendRestoreMcuCommand()V
/* .line 1356 */
/* .line 1349 */
/* :pswitch_c */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).sendGetKeyboardVersionCommand ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendGetKeyboardVersionCommand()V
/* .line 1350 */
/* .line 1339 */
/* :pswitch_d */
v0 = this.this$0;
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmShouldUpgradeKeyboard ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1340 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiIICKeyboardManager .-$$Nest$fgetmIICNodeHelper ( v0 );
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).upgradeKeyboardIfNeed ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->upgradeKeyboardIfNeed()V
/* .line 1415 */
} // :cond_2
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_9 */
/* :pswitch_9 */
} // .end packed-switch
} // .end method
