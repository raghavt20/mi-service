public class com.android.server.input.padkeyboard.MiuiUsbKeyboardManager implements com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver$KeyboardActionListener implements com.android.server.input.padkeyboard.MiuiPadKeyboardManager {
	 /* .source "MiuiUsbKeyboardManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;, */
	 /* Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String KEYBOARD_AUTO_UPGRADE;
private static final java.lang.String KEYBOARD_BACK_LIGHT;
private static final java.lang.String KEY_COMMAND_TARGET;
private static final java.lang.String KEY_COMMAND_VALUE;
private static final java.lang.String KEY_FIRST_CHECK;
private static final Integer MAX_CHECK_IDENTITY_TIMES;
private static final Integer MAX_GET_USB_DEVICE_TIME_OUT;
private static final Integer MAX_RETRY_TIMES;
private static final Integer MAX_UPGRADE_FAILED_TIMES;
private static volatile com.android.server.input.padkeyboard.MiuiUsbKeyboardManager sInstance;
/* # instance fields */
private final android.hardware.Sensor mAccSensor;
private final com.android.server.input.padkeyboard.AngleStateController mAngleStateController;
private java.lang.String mBinKbHighVersion;
private java.lang.String mBinKbLowVersion;
private java.lang.String mBinKbVersion;
private java.lang.String mBinMcuVersion;
private java.lang.String mBinWirelessVersion;
private Integer mCheckIdentityTimes;
private final android.database.ContentObserver mContentObserver;
private final android.content.Context mContext;
private miuix.appcompat.app.AlertDialog mDialog;
private Boolean mEnableAutoUpgrade;
private final com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H mHandler;
private final android.os.HandlerThread mHandlerThread;
private android.hardware.usb.UsbEndpoint mInUsbEndpoint;
private Integer mInputDeviceId;
private final com.android.server.input.InputManagerService mInputManager;
private Boolean mIsFirstAction;
private Boolean mIsKeyboardReady;
private Boolean mIsSetupComplete;
private Integer mKbTypeLevel;
private com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver mKeyboardDevicesObserver;
private Integer mKeyboardUpgradeFailedTimes;
private java.lang.String mKeyboardVersion;
private final mLocalGData;
private Integer mMcuUpgradeFailedTimes;
private java.lang.String mMcuVersion;
private android.hardware.usb.UsbEndpoint mOutUsbEndpoint;
private Integer mPenState;
private final mRecBuf;
private java.util.Map mRecentConnTime;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.hardware.usb.UsbEndpoint mReportInUsbEndpoint;
private android.hardware.usb.UsbInterface mReportInterface;
private Boolean mScreenState;
private final mSendBuf;
private final android.hardware.SensorManager mSensorManager;
private Boolean mShouldEnableBackLight;
private Boolean mShouldWakeUp;
private com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$UsbActionReceiver mUsbActionReceiver;
private android.hardware.usb.UsbDeviceConnection mUsbConnection;
private android.hardware.usb.UsbDevice mUsbDevice;
private final java.lang.Object mUsbDeviceLock;
private android.hardware.usb.UsbInterface mUsbInterface;
private final android.hardware.usb.UsbManager mUsbManager;
private java.lang.String mWirelessVersion;
/* # direct methods */
static com.android.server.input.padkeyboard.AngleStateController -$$Nest$fgetmAngleStateController ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAngleStateController;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmEnableAutoUpgrade ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z */
} // .end method
static com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H -$$Nest$fgetmHandler ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsKeyboardReady ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
} // .end method
static com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver -$$Nest$fgetmKeyboardDevicesObserver ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardDevicesObserver;
} // .end method
static Boolean -$$Nest$fgetmShouldEnableBackLight ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z */
} // .end method
static java.lang.Object -$$Nest$fgetmUsbDeviceLock ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUsbDeviceLock;
} // .end method
static void -$$Nest$fputmEnableAutoUpgrade ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z */
return;
} // .end method
static void -$$Nest$fputmShouldEnableBackLight ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z */
return;
} // .end method
static void -$$Nest$fputmUsbDevice ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0, android.hardware.usb.UsbDevice p1 ) { //bridge//synthethic
/* .locals 0 */
this.mUsbDevice = p1;
return;
} // .end method
static void -$$Nest$mcloseDevice ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->closeDevice()V */
return;
} // .end method
static void -$$Nest$mdoCheckKeyboardIdentity ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doCheckKeyboardIdentity(Z)V */
return;
} // .end method
static void -$$Nest$mdoGetMcuReset ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetMcuReset()V */
return;
} // .end method
static void -$$Nest$mdoGetReportData ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetReportData()V */
return;
} // .end method
static void -$$Nest$mdoReadConnectState ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadConnectState()V */
return;
} // .end method
static void -$$Nest$mdoReadKeyboardStatus ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadKeyboardStatus()V */
return;
} // .end method
static void -$$Nest$mdoReadKeyboardVersion ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadKeyboardVersion()V */
return;
} // .end method
static void -$$Nest$mdoReadMcuVersion ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doReadMcuVersion()V */
return;
} // .end method
static void -$$Nest$mdoStartKeyboardUpgrade ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doStartKeyboardUpgrade()V */
return;
} // .end method
static void -$$Nest$mdoStartMcuUpgrade ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doStartMcuUpgrade()V */
return;
} // .end method
static void -$$Nest$mdoStartWirelessUpgrade ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doStartWirelessUpgrade()V */
return;
} // .end method
static void -$$Nest$mdoWritePadKeyBoardStatus ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doWritePadKeyBoardStatus(II)V */
return;
} // .end method
static void -$$Nest$monUsbDeviceAttach ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onUsbDeviceAttach()V */
return;
} // .end method
static void -$$Nest$monUsbDeviceDetach ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onUsbDeviceDetach()V */
return;
} // .end method
static void -$$Nest$mtestFunction ( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->testFunction()V */
return;
} // .end method
public com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 126 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* .line 91 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mUsbDeviceLock = v1;
/* .line 104 */
/* const/16 v1, 0x40 */
/* new-array v2, v1, [B */
this.mSendBuf = v2;
/* .line 105 */
/* new-array v1, v1, [B */
this.mRecBuf = v1;
/* .line 109 */
int v1 = 3; // const/4 v1, 0x3
/* new-array v1, v1, [F */
this.mLocalGData = v1;
/* .line 113 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I */
/* .line 114 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* .line 115 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mPenState:I */
/* .line 116 */
int v1 = 2; // const/4 v1, 0x2
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
/* .line 117 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z */
/* .line 118 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I */
/* .line 122 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z */
/* .line 234 */
/* new-instance v2, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1; */
/* new-instance v3, Landroid/os/Handler; */
/* invoke-direct {v3}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$1;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/os/Handler;)V */
this.mContentObserver = v2;
/* .line 737 */
/* new-instance v3, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$2; */
/* invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$2;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V */
this.mRecentConnTime = v3;
/* .line 127 */
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
final String v4 = "MiuiUsbKeyboardManager"; // const-string v4, "MiuiUsbKeyboardManager"
android.util.Slog .i ( v3,v4 );
/* .line 128 */
this.mContext = p1;
/* .line 129 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->registerBroadcastReceiver(Landroid/content/Context;)V */
/* .line 130 */
final String v3 = "input"; // const-string v3, "input"
android.os.ServiceManager .getService ( v3 );
/* check-cast v3, Lcom/android/server/input/InputManagerService; */
this.mInputManager = v3;
/* .line 131 */
/* const-string/jumbo v3, "usb" */
(( android.content.Context ) p1 ).getSystemService ( v3 ); // invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/hardware/usb/UsbManager; */
this.mUsbManager = v3;
/* .line 132 */
/* const-string/jumbo v3, "sensor" */
(( android.content.Context ) p1 ).getSystemService ( v3 ); // invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/hardware/SensorManager; */
this.mSensorManager = v3;
/* .line 133 */
(( android.hardware.SensorManager ) v3 ).getDefaultSensor ( v1 ); // invoke-virtual {v3, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mAccSensor = v3;
/* .line 135 */
/* new-instance v3, Landroid/os/HandlerThread; */
final String v4 = "pad_keyboard_transfer_thread"; // const-string v4, "pad_keyboard_transfer_thread"
/* invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v3;
/* .line 136 */
(( android.os.HandlerThread ) v3 ).start ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V
/* .line 137 */
/* new-instance v4, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H; */
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v4, p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/os/Looper;)V */
this.mHandler = v4;
/* .line 138 */
/* new-instance v3, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver; */
/* invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;-><init>(Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver$KeyboardActionListener;)V */
this.mKeyboardDevicesObserver = v3;
/* .line 139 */
(( com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ) v3 ).startWatching ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->startWatching()V
/* .line 141 */
/* new-instance v3, Lcom/android/server/input/padkeyboard/AngleStateController; */
/* invoke-direct {v3, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;-><init>(Landroid/content/Context;)V */
this.mAngleStateController = v3;
/* .line 143 */
/* nop */
/* .line 144 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 143 */
final String v4 = "keyboard_back_light"; // const-string v4, "keyboard_back_light"
v3 = android.provider.Settings$Secure .getInt ( v3,v4,v1 );
if ( v3 != null) { // if-eqz v3, :cond_0
/* move v3, v1 */
} // :cond_0
/* move v3, v0 */
} // :goto_0
/* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z */
/* .line 145 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 146 */
android.provider.Settings$Secure .getUriFor ( v4 );
/* .line 145 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v4, v0, v2 ); // invoke-virtual {v3, v4, v0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 147 */
/* nop */
/* .line 148 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 147 */
final String v4 = "keyboard_auto_upgrade"; // const-string v4, "keyboard_auto_upgrade"
v3 = android.provider.Settings$Secure .getInt ( v3,v4,v1 );
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v1, v0 */
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z */
/* .line 149 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 150 */
android.provider.Settings$Secure .getUriFor ( v4 );
/* .line 149 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v3, v0, v2 ); // invoke-virtual {v1, v3, v0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 152 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 153 */
v1 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardTypeValue()I */
/* .line 152 */
final String v2 = "keyboard_type_level"; // const-string v2, "keyboard_type_level"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$Secure .putIntForUser ( v0,v2,v1,v3 );
/* .line 154 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 155 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
/* const/16 v1, 0x3206 */
/* const/16 v2, 0x3ffc */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setMiuiKeyboardInfo ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setMiuiKeyboardInfo(II)V
/* .line 156 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 157 */
return;
} // .end method
private void checkKeyboardIdentity ( Integer p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "delay" # I */
/* .param p2, "isFirst" # Z */
/* .line 460 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 461 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I */
/* .line 463 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0xb */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeMessages(I)V
/* .line 464 */
v0 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
/* .line 465 */
/* .local v0, "msg":Landroid/os/Message; */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 466 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "first_check"; // const-string v2, "first_check"
(( android.os.Bundle ) v1 ).putBoolean ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 467 */
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 468 */
v2 = this.mHandler;
/* int-to-long v3, p1 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v2 ).sendMessageDelayed ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 469 */
return;
} // .end method
private void cleanUsbCash ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "times" # I */
/* .line 1548 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* if-nez v0, :cond_0 */
/* .line 1549 */
return;
/* .line 1552 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* if-ge v0, p1, :cond_1 */
/* .line 1553 */
v1 = this.mUsbConnection;
v2 = this.mInUsbEndpoint;
v3 = this.mRecBuf;
/* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
/* .line 1552 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1555 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private void closeDevice ( ) {
/* .locals 3 */
/* .line 1558 */
v0 = this.mUsbDeviceLock;
/* monitor-enter v0 */
/* .line 1559 */
try { // :try_start_0
v1 = this.mUsbConnection;
/* if-nez v1, :cond_0 */
/* .line 1560 */
/* monitor-exit v0 */
return;
/* .line 1562 */
} // :cond_0
v2 = this.mUsbInterface;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1563 */
(( android.hardware.usb.UsbDeviceConnection ) v1 ).releaseInterface ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z
/* .line 1564 */
} // :cond_1
v2 = this.mReportInterface;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1565 */
(( android.hardware.usb.UsbDeviceConnection ) v1 ).releaseInterface ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z
/* .line 1567 */
} // :cond_2
} // :goto_0
v1 = this.mUsbConnection;
(( android.hardware.usb.UsbDeviceConnection ) v1 ).close ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDeviceConnection;->close()V
/* .line 1568 */
int v1 = 0; // const/4 v1, 0x0
this.mUsbConnection = v1;
/* .line 1569 */
this.mUsbInterface = v1;
/* .line 1570 */
this.mReportInterface = v1;
/* .line 1571 */
/* monitor-exit v0 */
/* .line 1572 */
return;
/* .line 1571 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void doCheckKeyboardIdentity ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isFirst" # Z */
/* .line 1613 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1614 */
v0 = this.mContext;
com.android.server.input.padkeyboard.KeyboardAuthHelper .getInstance ( v0 );
v0 = (( com.android.server.input.padkeyboard.KeyboardAuthHelper ) v0 ).doCheckKeyboardIdentityLaunchBeforeU ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchBeforeU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I
/* .line 1615 */
} // :cond_0
v0 = this.mContext;
com.android.server.input.padkeyboard.KeyboardAuthHelper .getInstance ( v0 );
v0 = (( com.android.server.input.padkeyboard.KeyboardAuthHelper ) v0 ).doCheckKeyboardIdentityLaunchAfterU ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchAfterU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I
} // :goto_0
/* nop */
/* .line 1616 */
/* .local v0, "identity":I */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->processIdentity(I)V */
/* .line 1617 */
return;
} // .end method
private void doGetMcuReset ( ) {
/* .locals 7 */
/* .line 821 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* if-nez v0, :cond_0 */
/* .line 822 */
return;
/* .line 825 */
} // :cond_0
final String v0 = "get mcu reset"; // const-string v0, "get mcu reset"
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 827 */
v0 = this.mSendBuf;
int v2 = 0; // const/4 v2, 0x0
java.util.Arrays .fill ( v0,v2 );
/* .line 828 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandGetResetInfo ( );
/* .line 829 */
/* .local v0, "command":[B */
v3 = this.mSendBuf;
/* array-length v4, v0 */
java.lang.System .arraycopy ( v0,v2,v3,v2,v4 );
/* .line 831 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
int v4 = 2; // const/4 v4, 0x2
/* if-ge v3, v4, :cond_5 */
/* .line 832 */
v4 = this.mUsbConnection;
v5 = this.mOutUsbEndpoint;
v6 = this.mSendBuf;
v4 = /* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 833 */
v4 = this.mRecBuf;
java.util.Arrays .fill ( v4,v2 );
/* .line 834 */
} // :cond_1
v4 = this.mUsbConnection;
v5 = this.mInUsbEndpoint;
v6 = this.mRecBuf;
v4 = /* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 835 */
v4 = this.mRecBuf;
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseUsbReset([B)Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 836 */
return;
/* .line 839 */
} // :cond_2
v4 = this.mUsbDevice;
/* if-nez v4, :cond_4 */
/* .line 840 */
return;
/* .line 843 */
} // :cond_3
/* const-string/jumbo v4, "send reset failed" */
android.util.Slog .i ( v1,v4 );
/* .line 844 */
/* const/16 v4, 0xc8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v4 );
/* .line 831 */
} // :cond_4
/* add-int/lit8 v3, v3, 0x1 */
/* .line 847 */
} // .end local v3 # "i":I
} // :cond_5
return;
} // .end method
private void doGetReportData ( ) {
/* .locals 8 */
/* .line 1208 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForReport()Z */
/* if-nez v0, :cond_0 */
/* .line 1209 */
return;
/* .line 1212 */
} // :cond_0
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "get report data"; // const-string v1, "get report data"
android.util.Slog .i ( v0,v1 );
/* .line 1213 */
v0 = this.mRecBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 1214 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 1216 */
/* .local v0, "startTime":J */
} // :cond_1
} // :goto_0
v2 = this.mUsbConnection;
v3 = this.mReportInUsbEndpoint;
v4 = this.mRecBuf;
v2 = /* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
/* move v3, v2 */
/* .local v3, "hasReport":Z */
/* if-nez v2, :cond_3 */
/* .line 1217 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* sub-long/2addr v4, v0 */
/* const-wide/16 v6, 0x14 */
/* cmp-long v2, v4, v6 */
/* if-gez v2, :cond_2 */
/* .line 1222 */
} // :cond_2
return;
/* .line 1218 */
} // :cond_3
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1219 */
v2 = this.mRecBuf;
/* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReportData([B)V */
} // .end method
private void doReadConnectState ( ) {
/* .locals 6 */
/* .line 647 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* if-nez v0, :cond_0 */
/* .line 648 */
return;
/* .line 651 */
} // :cond_0
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 652 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandGetConnectState ( );
/* .line 653 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 655 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ge v2, v3, :cond_4 */
/* .line 656 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 657 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 658 */
} // :cond_1
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 659 */
v3 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseConnectState([B)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 660 */
return;
/* .line 664 */
} // :cond_2
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send connect failed" */
android.util.Slog .i ( v3,v4 );
/* .line 655 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 667 */
} // .end local v2 # "i":I
} // :cond_4
return;
} // .end method
private void doReadKeyboardStatus ( ) {
/* .locals 6 */
/* .line 924 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* if-nez v0, :cond_0 */
/* .line 928 */
} // :cond_0
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 929 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandGetKeyboardStatus ( );
/* .line 930 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 932 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ge v2, v3, :cond_4 */
/* .line 933 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 934 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 935 */
} // :cond_1
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 936 */
v3 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReadKeyboardStatus([B)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 937 */
return;
/* .line 941 */
} // :cond_2
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send read keyboard failed" */
android.util.Slog .i ( v3,v4 );
/* .line 942 */
/* const/16 v3, 0xc8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v3 );
/* .line 932 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 945 */
} // .end local v2 # "i":I
} // :cond_4
return;
/* .line 925 */
} // .end local v0 # "command":[B
} // :cond_5
} // :goto_1
return;
} // .end method
private void doReadKeyboardVersion ( ) {
/* .locals 6 */
/* .line 858 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* if-nez v0, :cond_0 */
/* .line 859 */
return;
/* .line 862 */
} // :cond_0
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 863 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandGetKeyboardVersion ( );
/* .line 864 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 866 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ge v2, v3, :cond_4 */
/* .line 867 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 868 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 869 */
} // :cond_1
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 870 */
v3 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseKeyboardVersion([B)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 871 */
return;
/* .line 875 */
} // :cond_2
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send version failed" */
android.util.Slog .i ( v3,v4 );
/* .line 866 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 878 */
} // .end local v2 # "i":I
} // :cond_4
return;
} // .end method
private void doReadMcuVersion ( ) {
/* .locals 6 */
/* .line 780 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* if-nez v0, :cond_0 */
/* .line 781 */
return;
/* .line 784 */
} // :cond_0
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 785 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandGetVersionInfo ( );
/* .line 786 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 788 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 4; // const/4 v3, 0x4
/* if-ge v2, v3, :cond_4 */
/* .line 789 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 790 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 791 */
} // :cond_1
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 792 */
v3 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseUsbDeviceVersion([B)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 793 */
return;
/* .line 797 */
} // :cond_2
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send version failed" */
android.util.Slog .i ( v3,v4 );
/* .line 798 */
/* const/16 v3, 0xc8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v3 );
/* .line 788 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 801 */
} // .end local v2 # "i":I
} // :cond_4
return;
} // .end method
private void doStartKeyboardUpgrade ( ) {
/* .locals 8 */
/* .line 1354 */
v0 = this.mKeyboardVersion;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
if ( v0 != null) { // if-eqz v0, :cond_a
final String v2 = "0000"; // const-string v2, "0000"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 1358 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
v0 = this.mBinKbVersion;
if ( v0 != null) { // if-eqz v0, :cond_1
v3 = this.mKeyboardVersion;
/* .line 1359 */
v0 = (( java.lang.String ) v3 ).compareTo ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-gez v0, :cond_3 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_2 */
v0 = this.mBinKbLowVersion;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mKeyboardVersion;
/* .line 1361 */
v3 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* sub-int/2addr v3, v2 */
(( java.lang.String ) v0 ).substring ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v3 = this.mBinKbLowVersion;
/* .line 1362 */
v4 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
/* sub-int/2addr v4, v2 */
(( java.lang.String ) v3 ).substring ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 1361 */
v0 = android.text.TextUtils .equals ( v0,v3 );
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mKeyboardVersion;
v3 = this.mBinKbLowVersion;
/* .line 1363 */
v0 = (( java.lang.String ) v0 ).compareTo ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-gez v0, :cond_3 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v0, v3, :cond_4 */
v0 = this.mBinKbHighVersion;
if ( v0 != null) { // if-eqz v0, :cond_4
v3 = this.mKeyboardVersion;
/* .line 1365 */
v0 = (( java.lang.String ) v3 ).compareTo ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-ltz v0, :cond_4 */
/* .line 1366 */
} // :cond_3
final String v0 = "no need to start keyboard upgrade"; // const-string v0, "no need to start keyboard upgrade"
android.util.Slog .i ( v1,v0 );
/* .line 1367 */
return;
/* .line 1369 */
} // :cond_4
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
int v3 = 5; // const/4 v3, 0x5
/* if-le v0, v3, :cond_5 */
/* .line 1370 */
/* const-string/jumbo v0, "upgrade keyboard failed too many times" */
android.util.Slog .i ( v1,v0 );
/* .line 1371 */
return;
/* .line 1374 */
} // :cond_5
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardUpgradeHelper()Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
/* .line 1375 */
/* .local v0, "keyboardUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
/* if-nez v0, :cond_6 */
/* .line 1376 */
final String v2 = "can not get KeyboardUpgradeHelper"; // const-string v2, "can not get KeyboardUpgradeHelper"
android.util.Slog .i ( v1,v2 );
/* .line 1377 */
return;
/* .line 1379 */
} // :cond_6
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* .line 1380 */
v3 = this.mKeyboardVersion;
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_7
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;
(( java.lang.String ) v3 ).substring ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
v5 = this.mKeyboardVersion;
/* .line 1381 */
(( java.lang.String ) v5 ).substring ( v4, v2 ); // invoke-virtual {v5, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_7 */
/* .line 1382 */
final String v2 = "give up keyboard upgrade : wrong type"; // const-string v2, "give up keyboard upgrade : wrong type"
android.util.Slog .i ( v1,v2 );
/* .line 1383 */
return;
/* .line 1385 */
} // :cond_7
v3 = this.mKeyboardVersion;
if ( v3 != null) { // if-eqz v3, :cond_8
v3 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).isLowerVersionThan ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->isLowerVersionThan(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 1386 */
final String v2 = "give up keyboard upgrade : upper version"; // const-string v2, "give up keyboard upgrade : upper version"
android.util.Slog .i ( v1,v2 );
/* .line 1387 */
return;
/* .line 1389 */
} // :cond_8
v3 = this.mContext;
(( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1390 */
/* const v6, 0x110f024e */
(( android.content.res.Resources ) v5 ).getString ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1389 */
android.widget.Toast .makeText ( v3,v5,v4 );
/* .line 1391 */
(( android.widget.Toast ) v3 ).show ( ); // invoke-virtual {v3}, Landroid/widget/Toast;->show()V
/* .line 1392 */
v3 = this.mUsbDevice;
v5 = this.mUsbConnection;
v6 = this.mOutUsbEndpoint;
v7 = this.mInUsbEndpoint;
/* .line 1393 */
v3 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).startUpgrade ( v3, v5, v6, v7 ); // invoke-virtual {v0, v3, v5, v6, v7}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z
if ( v3 != null) { // if-eqz v3, :cond_9
/* .line 1394 */
final String v2 = "keyboard upgrade success"; // const-string v2, "keyboard upgrade success"
android.util.Slog .i ( v1,v2 );
/* .line 1395 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1396 */
/* const v3, 0x110f024f */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1395 */
android.widget.Toast .makeText ( v1,v2,v4 );
/* .line 1397 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 1398 */
/* iput v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* .line 1399 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetMcuReset()V */
/* .line 1401 */
} // :cond_9
final String v3 = "keyboard upgrade failed"; // const-string v3, "keyboard upgrade failed"
android.util.Slog .i ( v1,v3 );
/* .line 1402 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1403 */
/* const v5, 0x110f024d */
(( android.content.res.Resources ) v3 ).getString ( v5 ); // invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1402 */
android.widget.Toast .makeText ( v1,v3,v4 );
/* .line 1404 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 1405 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* add-int/2addr v1, v2 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* .line 1406 */
int v1 = 0; // const/4 v1, 0x0
this.mBinKbHighVersion = v1;
/* .line 1407 */
this.mBinKbLowVersion = v1;
/* .line 1409 */
} // :goto_0
return;
/* .line 1355 */
} // .end local v0 # "keyboardUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
} // :cond_a
} // :goto_1
final String v0 = "invalid keyboard version"; // const-string v0, "invalid keyboard version"
android.util.Slog .i ( v1,v0 );
/* .line 1356 */
return;
} // .end method
private void doStartMcuUpgrade ( ) {
/* .locals 6 */
/* .line 1317 */
v0 = this.mMcuVersion;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = this.mBinMcuVersion;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1318 */
v0 = (( java.lang.String ) v0 ).compareTo ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-ltz v0, :cond_0 */
/* .line 1319 */
final String v0 = "no need to start mcu upgrade"; // const-string v0, "no need to start mcu upgrade"
android.util.Slog .i ( v1,v0 );
/* .line 1320 */
return;
/* .line 1322 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I */
int v2 = 5; // const/4 v2, 0x5
/* if-le v0, v2, :cond_1 */
/* .line 1323 */
/* const-string/jumbo v0, "upgrade mcu failed too many times" */
android.util.Slog .i ( v1,v0 );
/* .line 1324 */
return;
/* .line 1326 */
} // :cond_1
v0 = this.mMcuVersion;
if ( v0 != null) { // if-eqz v0, :cond_6
final String v2 = "0000000000000000"; // const-string v2, "0000000000000000"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1331 */
} // :cond_2
/* new-instance v0, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper; */
v2 = this.mContext;
/* invoke-direct {v0, v2}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;-><init>(Landroid/content/Context;)V */
/* .line 1332 */
/* .local v0, "mcuUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper; */
(( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->getVersion()Ljava/lang/String;
this.mBinMcuVersion = v2;
/* .line 1333 */
final String v3 = "XM2021"; // const-string v3, "XM2021"
v2 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v2, :cond_3 */
/* .line 1334 */
final String v2 = "give up upgrade : invalid version head"; // const-string v2, "give up upgrade : invalid version head"
android.util.Slog .i ( v1,v2 );
/* .line 1335 */
return;
/* .line 1337 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* .line 1338 */
v2 = this.mMcuVersion;
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) v0 ).isLowerVersionThan ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->isLowerVersionThan(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1339 */
final String v2 = "give up upgrade : upper version"; // const-string v2, "give up upgrade : upper version"
android.util.Slog .i ( v1,v2 );
/* .line 1340 */
return;
/* .line 1342 */
} // :cond_4
v2 = this.mUsbDevice;
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mInUsbEndpoint;
/* .line 1343 */
v2 = (( com.android.server.input.padkeyboard.usb.McuUpgradeHelper ) v0 ).startUpgrade ( v2, v3, v4, v5 ); // invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;->startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1344 */
/* const-string/jumbo v2, "upgrade mcu success" */
android.util.Slog .i ( v1,v2 );
/* .line 1345 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I */
/* .line 1347 */
} // :cond_5
/* const-string/jumbo v2, "upgrade mcu failed" */
android.util.Slog .i ( v1,v2 );
/* .line 1348 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mMcuUpgradeFailedTimes:I */
/* .line 1349 */
int v1 = 0; // const/4 v1, 0x0
this.mBinMcuVersion = v1;
/* .line 1351 */
} // :goto_0
return;
/* .line 1327 */
} // .end local v0 # "mcuUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/McuUpgradeHelper;
} // :cond_6
} // :goto_1
/* const-string/jumbo v0, "unknown mcu version" */
android.util.Slog .i ( v1,v0 );
/* .line 1328 */
return;
} // .end method
private void doStartWirelessUpgrade ( ) {
/* .locals 7 */
/* .line 1443 */
v0 = this.mWirelessVersion;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
if ( v0 != null) { // if-eqz v0, :cond_5
final String v2 = "0000"; // const-string v2, "0000"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 1447 */
} // :cond_0
v0 = this.mBinWirelessVersion;
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = this.mWirelessVersion;
v0 = (( java.lang.String ) v2 ).compareTo ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-ltz v0, :cond_1 */
/* .line 1448 */
final String v0 = "no need to start wireless upgrade"; // const-string v0, "no need to start wireless upgrade"
android.util.Slog .i ( v1,v0 );
/* .line 1449 */
return;
/* .line 1451 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
int v2 = 5; // const/4 v2, 0x5
/* if-le v0, v2, :cond_2 */
/* .line 1452 */
/* const-string/jumbo v0, "upgrade keyboard failed too many times" */
android.util.Slog .i ( v1,v0 );
/* .line 1453 */
return;
/* .line 1456 */
} // :cond_2
/* new-instance v0, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
v2 = this.mContext;
v3 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.WL_BIN_PATH;
/* invoke-direct {v0, v2, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* .line 1458 */
/* .local v0, "wirelessUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;
this.mBinWirelessVersion = v2;
/* .line 1459 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* .line 1460 */
v2 = this.mWirelessVersion;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).isLowerVersionThan ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->isLowerVersionThan(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1461 */
final String v2 = "give up wireless upgrade : upper version"; // const-string v2, "give up wireless upgrade : upper version"
android.util.Slog .i ( v1,v2 );
/* .line 1462 */
return;
/* .line 1464 */
} // :cond_3
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1465 */
/* const v4, 0x110f024e */
(( android.content.res.Resources ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1464 */
int v4 = 0; // const/4 v4, 0x0
android.widget.Toast .makeText ( v2,v3,v4 );
/* .line 1466 */
(( android.widget.Toast ) v2 ).show ( ); // invoke-virtual {v2}, Landroid/widget/Toast;->show()V
/* .line 1467 */
v2 = this.mUsbDevice;
v3 = this.mUsbConnection;
v5 = this.mOutUsbEndpoint;
v6 = this.mInUsbEndpoint;
/* .line 1468 */
v2 = (( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).startUpgrade ( v2, v3, v5, v6 ); // invoke-virtual {v0, v2, v3, v5, v6}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->startUpgrade(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1469 */
/* const-string/jumbo v2, "wireless upgrade success" */
android.util.Slog .i ( v1,v2 );
/* .line 1470 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1471 */
/* const v3, 0x110f024f */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1470 */
android.widget.Toast .makeText ( v1,v2,v4 );
/* .line 1472 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 1473 */
/* iput v4, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* .line 1474 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->doGetMcuReset()V */
/* .line 1476 */
} // :cond_4
/* const-string/jumbo v2, "wireless upgrade failed" */
android.util.Slog .i ( v1,v2 );
/* .line 1477 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1478 */
/* const v3, 0x110f024d */
(( android.content.res.Resources ) v2 ).getString ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1477 */
android.widget.Toast .makeText ( v1,v2,v4 );
/* .line 1479 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 1480 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKeyboardUpgradeFailedTimes:I */
/* .line 1481 */
int v1 = 0; // const/4 v1, 0x0
this.mBinWirelessVersion = v1;
/* .line 1483 */
} // :goto_0
return;
/* .line 1444 */
} // .end local v0 # "wirelessUpgradeHelper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;
} // :cond_5
} // :goto_1
/* const-string/jumbo v0, "unknown wireless version" */
android.util.Slog .i ( v1,v0 );
/* .line 1445 */
return;
} // .end method
private void doWritePadKeyBoardStatus ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "target" # I */
/* .param p2, "value" # I */
/* .line 1005 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* if-nez v0, :cond_0 */
/* .line 1009 */
} // :cond_0
v0 = this.mSendBuf;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 1010 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandWriteKeyboardStatus ( p1,p2 );
/* .line 1011 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 1013 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ge v2, v3, :cond_4 */
/* .line 1014 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1015 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 1016 */
} // :cond_1
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1017 */
v3 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseWriteKeyBoardStatus([BII)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1018 */
return;
/* .line 1022 */
} // :cond_2
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send write mcu failed" */
android.util.Slog .i ( v3,v4 );
/* .line 1023 */
/* const/16 v3, 0xc8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v3 );
/* .line 1013 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1026 */
} // .end local v2 # "i":I
} // :cond_4
return;
/* .line 1006 */
} // .end local v0 # "command":[B
} // :cond_5
} // :goto_1
return;
} // .end method
private Boolean getDeviceReadyForReport ( ) {
/* .locals 6 */
/* .line 1486 */
v0 = this.mUsbDeviceLock;
/* monitor-enter v0 */
/* .line 1487 */
try { // :try_start_0
v1 = this.mUsbDevice;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
v3 = this.mUsbConnection;
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = this.mReportInterface;
if ( v4 != null) { // if-eqz v4, :cond_0
v5 = this.mReportInUsbEndpoint;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1489 */
(( android.hardware.usb.UsbDeviceConnection ) v3 ).claimInterface ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z
/* .line 1490 */
/* monitor-exit v0 */
/* .line 1493 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* if-nez v1, :cond_1 */
v1 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getUsbDevice()Z */
/* if-nez v1, :cond_1 */
/* .line 1494 */
/* monitor-exit v0 */
/* .line 1497 */
} // :cond_1
v1 = this.mUsbDevice;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getReportEndpoint(Landroid/hardware/usb/UsbDevice;)Z */
/* if-nez v1, :cond_2 */
/* .line 1498 */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
final String v2 = "get usb report endpoint fail"; // const-string v2, "get usb report endpoint fail"
android.util.Slog .i ( v1,v2 );
/* .line 1499 */
/* monitor-exit v0 */
/* .line 1501 */
} // :cond_2
v1 = this.mUsbConnection;
/* if-nez v1, :cond_3 */
/* .line 1502 */
v1 = this.mUsbManager;
v4 = this.mUsbDevice;
(( android.hardware.usb.UsbManager ) v1 ).openDevice ( v4 ); // invoke-virtual {v1, v4}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
this.mUsbConnection = v1;
/* .line 1504 */
} // :cond_3
v1 = this.mUsbConnection;
if ( v1 != null) { // if-eqz v1, :cond_5
v4 = this.mReportInterface;
if ( v4 != null) { // if-eqz v4, :cond_5
v5 = this.mReportInUsbEndpoint;
/* if-nez v5, :cond_4 */
/* .line 1509 */
} // :cond_4
(( android.hardware.usb.UsbDeviceConnection ) v1 ).claimInterface ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z
/* .line 1510 */
/* monitor-exit v0 */
/* .line 1511 */
/* .line 1505 */
} // :cond_5
} // :goto_0
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
final String v2 = "get usb report connection fail"; // const-string v2, "get usb report connection fail"
android.util.Slog .i ( v1,v2 );
/* .line 1506 */
/* monitor-exit v0 */
/* .line 1510 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean getDeviceReadyForTransfer ( ) {
/* .locals 6 */
/* .line 1123 */
v0 = this.mUsbDeviceLock;
/* monitor-enter v0 */
/* .line 1124 */
try { // :try_start_0
v1 = this.mUsbDevice;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
v3 = this.mUsbConnection;
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = this.mUsbInterface;
if ( v4 != null) { // if-eqz v4, :cond_0
v5 = this.mOutUsbEndpoint;
if ( v5 != null) { // if-eqz v5, :cond_0
v5 = this.mInUsbEndpoint;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1126 */
(( android.hardware.usb.UsbDeviceConnection ) v3 ).claimInterface ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z
/* .line 1127 */
/* monitor-exit v0 */
/* .line 1130 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* if-nez v1, :cond_1 */
v1 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getUsbDevice()Z */
/* if-nez v1, :cond_1 */
/* .line 1131 */
/* monitor-exit v0 */
/* .line 1134 */
} // :cond_1
v1 = this.mUsbDevice;
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getTransferEndpoint(Landroid/hardware/usb/UsbDevice;)Z */
/* if-nez v1, :cond_2 */
/* .line 1135 */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
final String v2 = "get transfer endpoint failed"; // const-string v2, "get transfer endpoint failed"
android.util.Slog .i ( v1,v2 );
/* .line 1136 */
/* monitor-exit v0 */
/* .line 1138 */
} // :cond_2
v1 = this.mUsbConnection;
/* if-nez v1, :cond_3 */
/* .line 1139 */
v1 = this.mUsbManager;
v4 = this.mUsbDevice;
(( android.hardware.usb.UsbManager ) v1 ).openDevice ( v4 ); // invoke-virtual {v1, v4}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
this.mUsbConnection = v1;
/* .line 1141 */
} // :cond_3
v1 = this.mUsbConnection;
if ( v1 != null) { // if-eqz v1, :cond_5
v4 = this.mOutUsbEndpoint;
if ( v4 != null) { // if-eqz v4, :cond_5
v4 = this.mInUsbEndpoint;
/* if-nez v4, :cond_4 */
/* .line 1146 */
} // :cond_4
v3 = this.mUsbInterface;
(( android.hardware.usb.UsbDeviceConnection ) v1 ).claimInterface ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z
/* .line 1147 */
/* monitor-exit v0 */
/* .line 1148 */
/* .line 1142 */
} // :cond_5
} // :goto_0
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
final String v2 = "get usb transfer connection failed"; // const-string v2, "get usb transfer connection failed"
android.util.Slog .i ( v1,v2 );
/* .line 1143 */
/* monitor-exit v0 */
/* .line 1147 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static com.android.server.input.padkeyboard.MiuiUsbKeyboardManager getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 160 */
v0 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 161 */
/* const-class v0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
/* monitor-enter v0 */
/* .line 162 */
try { // :try_start_0
v1 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 163 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;-><init>(Landroid/content/Context;)V */
/* .line 165 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 167 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.padkeyboard.MiuiUsbKeyboardManager.sInstance;
} // .end method
private Integer getKeyboardTypeValue ( ) {
/* .locals 2 */
/* .line 919 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 920 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 919 */
} // :goto_0
} // .end method
private com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper getKeyboardUpgradeHelper ( ) {
/* .locals 6 */
/* .line 1412 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1413 */
/* .local v0, "helper":Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
/* packed-switch v1, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 1434 */
/* :pswitch_0 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
v2 = this.mContext;
v3 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.KB_H_BIN_PATH;
/* invoke-direct {v1, v2, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1435 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;
this.mBinKbHighVersion = v1;
/* .line 1436 */
/* goto/16 :goto_0 */
/* .line 1419 */
/* :pswitch_1 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mKeyboardVersion;
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
(( java.lang.String ) v2 ).substring ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mKeyboardVersion;
/* .line 1420 */
v5 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* sub-int/2addr v5, v4 */
(( java.lang.String ) v2 ).substring ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1421 */
/* .local v1, "lowKbType":Ljava/lang/String; */
int v2 = 2; // const/4 v2, 0x2
(( java.lang.String ) v1 ).substring ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
v5 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.FIRST_LOW_KB_TYPE;
/* .line 1422 */
v2 = (( java.lang.String ) v2 ).compareTo ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
/* if-gez v2, :cond_0 */
/* .line 1423 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.mKeyboardVersion;
(( java.lang.String ) v5 ).substring ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "0"; // const-string v3, "0"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1425 */
} // :cond_0
v2 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.KB_L_BIN_PATH_MAP;
/* check-cast v2, Ljava/lang/String; */
/* .line 1426 */
/* .local v2, "lowBinPath":Ljava/lang/String; */
/* if-nez v2, :cond_1 */
/* .line 1427 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "unhandled low keyboard type:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", stop upgrade"; // const-string v4, ", stop upgrade"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
android.util.Slog .i ( v4,v3 );
/* .line 1428 */
/* .line 1430 */
} // :cond_1
/* new-instance v3, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
v4 = this.mContext;
/* invoke-direct {v3, v4, v2}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* move-object v0, v3 */
/* .line 1431 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;
this.mBinKbLowVersion = v3;
/* .line 1432 */
/* .line 1415 */
} // .end local v1 # "lowKbType":Ljava/lang/String;
} // .end local v2 # "lowBinPath":Ljava/lang/String;
/* :pswitch_2 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper; */
v2 = this.mContext;
v3 = com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper.KB_BIN_PATH;
/* invoke-direct {v1, v2, v3}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1416 */
(( com.android.server.input.padkeyboard.usb.KeyboardUpgradeHelper ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/KeyboardUpgradeHelper;->getVersion()Ljava/lang/String;
this.mBinKbVersion = v1;
/* .line 1417 */
/* nop */
/* .line 1439 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean getReportEndpoint ( android.hardware.usb.UsbDevice p0 ) {
/* .locals 6 */
/* .param p1, "device" # Landroid/hardware/usb/UsbDevice; */
/* .line 1515 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1516 */
/* .line 1519 */
} // :cond_0
(( android.hardware.usb.UsbDevice ) p1 ).getConfiguration ( v0 ); // invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getConfiguration(I)Landroid/hardware/usb/UsbConfiguration;
/* .line 1520 */
/* .local v1, "configuration":Landroid/hardware/usb/UsbConfiguration; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( android.hardware.usb.UsbConfiguration ) v1 ).getInterfaceCount ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbConfiguration;->getInterfaceCount()I
/* if-ge v2, v3, :cond_2 */
/* .line 1521 */
(( android.hardware.usb.UsbConfiguration ) v1 ).getInterface ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbConfiguration;->getInterface(I)Landroid/hardware/usb/UsbInterface;
/* .line 1523 */
/* .local v3, "anInterface":Landroid/hardware/usb/UsbInterface; */
v4 = (( android.hardware.usb.UsbInterface ) v3 ).getId ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getId()I
int v5 = 3; // const/4 v5, 0x3
/* if-ne v4, v5, :cond_1 */
v4 = (( android.hardware.usb.UsbInterface ) v3 ).getEndpointCount ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I
int v5 = 1; // const/4 v5, 0x1
/* if-ne v4, v5, :cond_1 */
/* .line 1524 */
(( android.hardware.usb.UsbInterface ) v3 ).getEndpoint ( v0 ); // invoke-virtual {v3, v0}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;
this.mReportInUsbEndpoint = v4;
/* .line 1526 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1527 */
this.mReportInterface = v3;
/* .line 1528 */
/* .line 1520 */
} // .end local v3 # "anInterface":Landroid/hardware/usb/UsbInterface;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1532 */
} // .end local v2 # "i":I
} // :cond_2
} // .end method
private Boolean getTransferEndpoint ( android.hardware.usb.UsbDevice p0 ) {
/* .locals 8 */
/* .param p1, "device" # Landroid/hardware/usb/UsbDevice; */
/* .line 1177 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 1178 */
/* .line 1181 */
} // :cond_0
(( android.hardware.usb.UsbDevice ) p1 ).getConfiguration ( v0 ); // invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getConfiguration(I)Landroid/hardware/usb/UsbConfiguration;
/* .line 1182 */
/* .local v1, "configuration":Landroid/hardware/usb/UsbConfiguration; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "interfaceNum":I */
} // :goto_0
v3 = (( android.hardware.usb.UsbConfiguration ) v1 ).getInterfaceCount ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbConfiguration;->getInterfaceCount()I
/* if-ge v2, v3, :cond_7 */
/* .line 1183 */
(( android.hardware.usb.UsbConfiguration ) v1 ).getInterface ( v2 ); // invoke-virtual {v1, v2}, Landroid/hardware/usb/UsbConfiguration;->getInterface(I)Landroid/hardware/usb/UsbInterface;
/* .line 1184 */
/* .local v3, "anInterface":Landroid/hardware/usb/UsbInterface; */
if ( v3 != null) { // if-eqz v3, :cond_6
v4 = (( android.hardware.usb.UsbInterface ) v3 ).getEndpointCount ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I
int v5 = 2; // const/4 v5, 0x2
/* if-ge v4, v5, :cond_1 */
/* .line 1185 */
/* .line 1187 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "endPointNum":I */
} // :goto_1
v5 = (( android.hardware.usb.UsbInterface ) v3 ).getEndpointCount ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I
/* if-ge v4, v5, :cond_5 */
/* .line 1188 */
(( android.hardware.usb.UsbInterface ) v3 ).getEndpoint ( v4 ); // invoke-virtual {v3, v4}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;
/* .line 1189 */
/* .local v5, "endpoint":Landroid/hardware/usb/UsbEndpoint; */
/* if-nez v5, :cond_2 */
/* .line 1190 */
/* .line 1192 */
} // :cond_2
v6 = (( android.hardware.usb.UsbEndpoint ) v5 ).getDirection ( ); // invoke-virtual {v5}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I
/* .line 1193 */
/* .local v6, "direction":I */
/* const/16 v7, 0x80 */
/* if-ne v6, v7, :cond_3 */
/* .line 1194 */
this.mInUsbEndpoint = v5;
/* .line 1195 */
} // :cond_3
/* if-nez v6, :cond_4 */
/* .line 1196 */
this.mOutUsbEndpoint = v5;
/* .line 1187 */
} // .end local v5 # "endpoint":Landroid/hardware/usb/UsbEndpoint;
} // .end local v6 # "direction":I
} // :cond_4
} // :goto_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1199 */
} // .end local v4 # "endPointNum":I
} // :cond_5
v4 = this.mInUsbEndpoint;
if ( v4 != null) { // if-eqz v4, :cond_6
v4 = this.mOutUsbEndpoint;
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 1200 */
this.mUsbInterface = v3;
/* .line 1201 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1182 */
} // .end local v3 # "anInterface":Landroid/hardware/usb/UsbInterface;
} // :cond_6
} // :goto_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1204 */
} // .end local v2 # "interfaceNum":I
} // :cond_7
} // .end method
private Boolean getUsbDevice ( ) {
/* .locals 7 */
/* .line 1152 */
v0 = this.mUsbDeviceLock;
/* monitor-enter v0 */
/* .line 1153 */
try { // :try_start_0
v1 = this.mUsbManager;
(( android.hardware.usb.UsbManager ) v1 ).getDeviceList ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;
/* .line 1154 */
/* .local v1, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;" */
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Landroid/hardware/usb/UsbDevice; */
/* .line 1155 */
/* .local v3, "device":Landroid/hardware/usb/UsbDevice; */
v4 = this.mUsbManager;
v4 = (( android.hardware.usb.UsbManager ) v4 ).hasPermission ( v3 ); // invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
v4 = (( android.hardware.usb.UsbDevice ) v3 ).getVendorId ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
/* const/16 v5, 0x3206 */
/* if-ne v4, v5, :cond_0 */
/* .line 1156 */
v4 = (( android.hardware.usb.UsbDevice ) v3 ).getProductId ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getProductId()I
/* const/16 v5, 0x3ffc */
/* if-ne v4, v5, :cond_0 */
/* .line 1157 */
final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getUsbDevice: "; // const-string v6, "getUsbDevice: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.hardware.usb.UsbDevice ) v3 ).getDeviceName ( ); // invoke-virtual {v3}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v5 );
/* .line 1158 */
this.mUsbDevice = v3;
/* .line 1160 */
} // .end local v3 # "device":Landroid/hardware/usb/UsbDevice;
} // :cond_0
/* .line 1161 */
} // .end local v1 # "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/hardware/usb/UsbDevice;>;"
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1163 */
v0 = this.mUsbDevice;
/* const/16 v1, 0xc */
/* if-nez v0, :cond_2 */
/* .line 1164 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v2 = "get usb device failed"; // const-string v2, "get usb device failed"
android.util.Slog .i ( v0,v2 );
/* .line 1165 */
v0 = this.mHandler;
v0 = (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z
/* if-nez v0, :cond_3 */
/* .line 1166 */
v0 = this.mHandler;
/* const-wide/16 v2, 0x4e20 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 1170 */
} // :cond_2
v0 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeMessages(I)V
/* .line 1173 */
} // :cond_3
} // :goto_1
v0 = this.mUsbDevice;
if ( v0 != null) { // if-eqz v0, :cond_4
int v0 = 1; // const/4 v0, 0x1
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // :goto_2
/* .line 1161 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isUserSetUp ( ) {
/* .locals 4 */
/* .line 1696 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z */
/* if-nez v0, :cond_1 */
/* .line 1697 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "user_setup_complete" */
int v2 = -2; // const/4 v2, -0x2
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v3,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z */
/* .line 1700 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsSetupComplete:Z */
} // .end method
private void notifyKeyboardStateChanged ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "newState" # Z */
/* .line 714 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* .line 715 */
/* .local v0, "oldState":Z */
/* if-ne v0, p1, :cond_0 */
/* .line 716 */
return;
/* .line 718 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* .line 719 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "keyboardStatus changed :"; // const-string v2, "keyboardStatus changed :"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 720 */
v1 = this.mInputManager;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
final String v3 = "notifyIgnoredInputDevicesChanged"; // const-string v3, "notifyIgnoredInputDevicesChanged"
com.android.server.input.ReflectionUtils .callPrivateMethod ( v1,v3,v2 );
/* .line 722 */
/* if-nez v0, :cond_1 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 723 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onKeyboardAttach()V */
/* .line 726 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez p1, :cond_2 */
/* .line 727 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->onKeyboardDetach()V */
/* .line 730 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 731 */
v1 = this.mSensorManager;
v2 = this.mAccSensor;
int v3 = 3; // const/4 v3, 0x3
(( android.hardware.SensorManager ) v1 ).registerListener ( p0, v2, v3 ); // invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 733 */
} // :cond_3
v1 = this.mSensorManager;
(( android.hardware.SensorManager ) v1 ).unregisterListener ( p0 ); // invoke-virtual {v1, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 735 */
} // :goto_0
return;
} // .end method
private void onKeyboardAttach ( ) {
/* .locals 4 */
/* .line 747 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "onKeyboardAttach"; // const-string v1, "onKeyboardAttach"
android.util.Slog .i ( v0,v1 );
/* .line 748 */
v0 = this.mRecentConnTime;
java.text.DateFormat .getDateTimeInstance ( );
/* new-instance v2, Ljava/util/Date; */
/* invoke-direct {v2}, Ljava/util/Date;-><init>()V */
(( java.text.DateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
/* .line 749 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).readKeyboardVersion ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readKeyboardVersion()V
/* .line 750 */
v0 = this.mAngleStateController;
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboard ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_1 */
/* .line 751 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
/* .line 752 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
/* nop */
/* .line 753 */
/* .local v0, "value":I */
/* const/16 v3, 0x23 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).writePadKeyBoardStatus ( v3, v0 ); // invoke-virtual {p0, v3, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 755 */
} // .end local v0 # "value":I
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->isUserSetUp()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 756 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).startKeyboardUpgrade ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->startKeyboardUpgrade()V
/* .line 757 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).startWirelessUpgrade ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->startWirelessUpgrade()V
/* .line 758 */
/* invoke-direct {p0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->checkKeyboardIdentity(IZ)V */
/* .line 760 */
} // :cond_2
return;
} // .end method
private void onKeyboardDetach ( ) {
/* .locals 6 */
/* .line 763 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "onKeyboardDetach"; // const-string v1, "onKeyboardDetach"
android.util.Slog .i ( v0,v1 );
/* .line 764 */
v0 = v0 = this.mRecentConnTime;
/* new-array v0, v0, [Ljava/util/Map$Entry; */
/* .line 765 */
/* .local v0, "entries":[Ljava/util/Map$Entry;, "[Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
v1 = this.mRecentConnTime;
/* .line 766 */
/* array-length v1, v0 */
int v2 = 1; // const/4 v2, 0x1
/* if-lez v1, :cond_0 */
/* .line 767 */
/* array-length v1, v0 */
/* sub-int/2addr v1, v2 */
/* aget-object v1, v0, v1 */
java.text.DateFormat .getDateTimeInstance ( );
/* new-instance v4, Ljava/util/Date; */
/* invoke-direct {v4}, Ljava/util/Date;-><init>()V */
(( java.text.DateFormat ) v3 ).format ( v4 ); // invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 769 */
} // :cond_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
final String v5 = "keyboard_type_level"; // const-string v5, "keyboard_type_level"
android.provider.Settings$Secure .putIntForUser ( v1,v5,v3,v4 );
/* .line 771 */
v1 = this.mHandler;
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v1 ).removeCallbacksAndMessages ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 773 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 774 */
v1 = this.mDialog;
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = (( miuix.appcompat.app.AlertDialog ) v1 ).isShowing ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 775 */
v1 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v1 ).dismiss ( ); // invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V
/* .line 777 */
} // :cond_1
return;
} // .end method
private void onUsbDeviceAttach ( ) {
/* .locals 2 */
/* .line 376 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "mcu usb device attach"; // const-string v1, "mcu usb device attach"
android.util.Slog .i ( v0,v1 );
/* .line 377 */
v0 = this.mHandler;
/* const/16 v1, 0xc */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeMessages(I)V
/* .line 378 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 379 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).getKeyboardReportData ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardReportData()V
/* .line 380 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).readMcuVersion ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readMcuVersion()V
/* .line 381 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mEnableAutoUpgrade:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->isUserSetUp()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 382 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).startMcuUpgrade ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->startMcuUpgrade()V
/* .line 384 */
} // :cond_0
return;
} // .end method
private void onUsbDeviceDetach ( ) {
/* .locals 2 */
/* .line 387 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "mcu usb device detach"; // const-string v1, "mcu usb device detach"
android.util.Slog .i ( v0,v1 );
/* .line 388 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 389 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->notifyKeyboardStateChanged(Z)V */
/* .line 391 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z */
/* .line 392 */
return;
} // .end method
private Boolean parseConnectState ( Object[] p0 ) {
/* .locals 10 */
/* .param p1, "recBuf" # [B */
/* .line 670 */
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v1, p1, v0 */
/* const/16 v2, -0x5e */
final String v3 = "%02x"; // const-string v3, "%02x"
final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
int v5 = 0; // const/4 v5, 0x0
/* if-eq v1, v2, :cond_0 */
/* .line 671 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive connect state error:"; // const-string v2, "receive connect state error:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v0 );
/* .line 672 */
/* .line 674 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* new-array v2, v1, [B */
/* .line 675 */
/* .local v2, "voltage":[B */
/* const/16 v6, 0xa */
/* array-length v7, v2 */
java.lang.System .arraycopy ( p1,v6,v2,v5,v7 );
/* .line 676 */
/* new-array v1, v1, [B */
/* .line 677 */
/* .local v1, "clockOff":[B */
/* const/16 v6, 0xc */
/* array-length v7, v1 */
java.lang.System .arraycopy ( p1,v6,v1,v5,v7 );
/* .line 678 */
/* new-array v0, v0, [B */
/* .line 679 */
/* .local v0, "trueOff":[B */
/* const/16 v6, 0xe */
/* array-length v7, v0 */
java.lang.System .arraycopy ( p1,v6,v0,v5,v7 );
/* .line 680 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "receive connect state:"; // const-string v7, "receive connect state:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v7, 0x9 */
/* aget-byte v8, p1, v7 */
java.lang.Byte .valueOf ( v8 );
/* filled-new-array {v8}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v8 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " Voltage:"; // const-string v8, " Voltage:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 681 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v2 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " E_UART:"; // const-string v8, " E_UART:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 682 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v1 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " E_PPM:"; // const-string v8, " E_PPM:"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 683 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v0 );
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 680 */
android.util.Slog .i ( v4,v6 );
/* .line 684 */
/* const/16 v6, 0x12 */
/* aget-byte v6, p1, v6 */
int v8 = 1; // const/4 v8, 0x1
/* if-ne v6, v8, :cond_1 */
/* .line 685 */
final String v3 = "keyboard is over charged"; // const-string v3, "keyboard is over charged"
android.util.Slog .i ( v4,v3 );
/* .line 686 */
/* .line 688 */
} // :cond_1
/* aget-byte v6, p1, v7 */
/* and-int/lit8 v6, v6, 0x3 */
/* if-ne v6, v8, :cond_2 */
/* .line 689 */
final String v3 = "TRX check failed"; // const-string v3, "TRX check failed"
android.util.Slog .i ( v4,v3 );
/* .line 690 */
/* .line 692 */
} // :cond_2
/* aget-byte v6, p1, v7 */
/* and-int/lit8 v6, v6, 0x63 */
/* const/16 v9, 0x43 */
/* if-ne v6, v9, :cond_3 */
/* .line 693 */
final String v3 = "pin connect failed"; // const-string v3, "pin connect failed"
android.util.Slog .i ( v4,v3 );
/* .line 694 */
v3 = this.mContext;
(( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 695 */
/* const v6, 0x110f0249 */
(( android.content.res.Resources ) v4 ).getString ( v6 ); // invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 694 */
android.widget.Toast .makeText ( v3,v4,v5 );
/* .line 696 */
(( android.widget.Toast ) v3 ).show ( ); // invoke-virtual {v3}, Landroid/widget/Toast;->show()V
/* .line 697 */
/* .line 700 */
} // :cond_3
/* aget-byte v6, p1, v7 */
/* and-int/lit8 v6, v6, 0x3 */
/* if-nez v6, :cond_4 */
/* .line 701 */
/* invoke-direct {p0, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->notifyKeyboardStateChanged(Z)V */
/* .line 702 */
/* .line 704 */
} // :cond_4
/* aget-byte v6, p1, v7 */
/* and-int/lit8 v6, v6, 0x63 */
/* const/16 v9, 0x23 */
/* if-ne v6, v9, :cond_5 */
/* .line 705 */
/* invoke-direct {p0, v8}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->notifyKeyboardStateChanged(Z)V */
/* .line 706 */
/* .line 709 */
} // :cond_5
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "unhandled connect state:" */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v7, p1, v7 */
java.lang.Byte .valueOf ( v7 );
/* filled-new-array {v7}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v7 );
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v3 );
/* .line 710 */
} // .end method
private void parseGsensorData ( Object[] p0 ) {
/* .locals 11 */
/* .param p1, "recBuf" # [B */
/* .line 1254 */
/* const/16 v0, 0xe */
/* aget-byte v1, p1, v0 */
int v2 = 0; // const/4 v2, 0x0
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v2,v0,v1 );
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
/* if-nez v0, :cond_0 */
/* .line 1255 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive gsensor data checksum error:"; // const-string v2, "receive gsensor data checksum error:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, p1 */
/* .line 1256 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1255 */
android.util.Slog .i ( v1,v0 );
/* .line 1257 */
return;
/* .line 1260 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* new-array v3, v0, [B */
/* .line 1264 */
/* .local v3, "temp":[B */
/* const/16 v4, 0x8 */
/* array-length v5, v3 */
java.lang.System .arraycopy ( p1,v4,v3,v2,v5 );
/* .line 1265 */
int v4 = 1; // const/4 v4, 0x1
/* aget-byte v5, v3, v4 */
/* and-int/lit16 v5, v5, 0x80 */
final String v6 = "FFFF"; // const-string v6, "FFFF"
/* const/16 v7, 0x10 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1266 */
/* new-instance v5, Ljava/math/BigInteger; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v3 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v5, v8, v7}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V */
v5 = (( java.math.BigInteger ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/math/BigInteger;->intValue()I
/* int-to-float v5, v5 */
/* .local v5, "x":F */
/* .line 1268 */
} // .end local v5 # "x":F
} // :cond_1
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v3 );
v5 = java.lang.Integer .parseInt ( v5,v7 );
/* int-to-float v5, v5 */
/* .line 1271 */
/* .restart local v5 # "x":F */
} // :goto_0
/* const/16 v8, 0xa */
/* array-length v9, v3 */
java.lang.System .arraycopy ( p1,v8,v3,v2,v9 );
/* .line 1272 */
/* aget-byte v8, v3, v4 */
/* and-int/lit16 v8, v8, 0x80 */
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 1273 */
/* new-instance v8, Ljava/math/BigInteger; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v3 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v8, v9, v7}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V */
v8 = (( java.math.BigInteger ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/math/BigInteger;->intValue()I
/* int-to-float v8, v8 */
/* .local v8, "y":F */
/* .line 1275 */
} // .end local v8 # "y":F
} // :cond_2
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v3 );
v8 = java.lang.Integer .parseInt ( v8,v7 );
/* int-to-float v8, v8 */
/* .line 1278 */
/* .restart local v8 # "y":F */
} // :goto_1
/* const/16 v9, 0xc */
/* array-length v10, v3 */
java.lang.System .arraycopy ( p1,v9,v3,v2,v10 );
/* .line 1279 */
/* aget-byte v9, v3, v4 */
/* and-int/lit16 v9, v9, 0x80 */
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 1280 */
/* new-instance v9, Ljava/math/BigInteger; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v6 ); // invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v3 );
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v9, v6, v7}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V */
v6 = (( java.math.BigInteger ) v9 ).intValue ( ); // invoke-virtual {v9}, Ljava/math/BigInteger;->intValue()I
/* int-to-float v6, v6 */
/* .local v6, "z":F */
/* .line 1282 */
} // .end local v6 # "z":F
} // :cond_3
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v3 );
v6 = java.lang.Integer .parseInt ( v6,v7 );
/* int-to-float v6, v6 */
/* .line 1285 */
/* .restart local v6 # "z":F */
} // :goto_2
/* const v7, 0x411ccccd # 9.8f */
/* mul-float v9, v5, v7 */
/* const/high16 v10, 0x45000000 # 2048.0f */
/* div-float/2addr v9, v10 */
/* .line 1286 */
} // .end local v5 # "x":F
/* .local v9, "x":F */
/* mul-float v5, v8, v7 */
/* div-float/2addr v5, v10 */
/* .line 1287 */
} // .end local v8 # "y":F
/* .local v5, "y":F */
/* mul-float/2addr v7, v6 */
/* div-float/2addr v7, v10 */
/* .line 1288 */
} // .end local v6 # "z":F
/* .local v7, "z":F */
v6 = this.mLocalGData;
/* aget v2, v6, v2 */
/* .line 1289 */
/* .local v2, "xLocal":F */
/* aget v4, v6, v4 */
/* .line 1290 */
/* .local v4, "yLocal":F */
/* aget v0, v6, v0 */
/* .line 1291 */
/* .local v0, "zLocal":F */
v6 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .calculatePKAngle ( v9,v2,v7,v0 );
/* .line 1292 */
/* .local v6, "resultAngle":F */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "result angle = "; // const-string v10, "result angle = "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v8 );
/* .line 1293 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).updateAngleState ( v6 ); // invoke-virtual {v1, v6}, Lcom/android/server/input/padkeyboard/AngleStateController;->updateAngleState(F)V
/* .line 1294 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1295 */
return;
} // .end method
private void parseHallState ( Object[] p0 ) {
/* .locals 3 */
/* .param p1, "recBuf" # [B */
/* .line 1298 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Receive Hall State:"; // const-string v1, "Receive Hall State:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x8 */
/* aget-byte v1, p1, v1 */
java.lang.Byte .valueOf ( v1 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
final String v2 = "%02x"; // const-string v2, "%02x"
java.lang.String .format ( v2,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 1299 */
return;
} // .end method
private Integer parseKeyboardType ( ) {
/* .locals 4 */
/* .line 904 */
v0 = this.mKeyboardVersion;
int v1 = 2; // const/4 v1, 0x2
/* if-nez v0, :cond_0 */
/* .line 905 */
/* .line 907 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
(( java.lang.String ) v0 ).substring ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 908 */
/* .local v0, "type":Ljava/lang/String; */
final String v2 = "1"; // const-string v2, "1"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 909 */
/* .line 910 */
} // :cond_1
final String v2 = "2"; // const-string v2, "2"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_4 */
final String v2 = "4"; // const-string v2, "4"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 912 */
} // :cond_2
final String v2 = "3"; // const-string v2, "3"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 913 */
int v1 = 3; // const/4 v1, 0x3
/* .line 915 */
} // :cond_3
/* .line 911 */
} // :cond_4
} // :goto_0
} // .end method
private Boolean parseKeyboardVersion ( Object[] p0 ) {
/* .locals 9 */
/* .param p1, "recBuf" # [B */
/* .line 881 */
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v1, p1, v0 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
/* if-eq v1, v4, :cond_0 */
/* .line 882 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive keyboard version error:"; // const-string v4, "receive keyboard version error:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v4 = "%02x"; // const-string v4, "%02x"
java.lang.String .format ( v4,v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 883 */
/* .line 886 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* new-array v1, v0, [B */
/* .line 887 */
/* .local v1, "kbVersion":[B */
int v5 = 6; // const/4 v5, 0x6
/* array-length v6, v1 */
java.lang.System .arraycopy ( p1,v5,v1,v3,v6 );
/* .line 888 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v1 );
this.mKeyboardVersion = v5;
/* .line 889 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "receive keyboard version:"; // const-string v6, "receive keyboard version:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mKeyboardVersion;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v5 );
/* .line 890 */
v5 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseKeyboardType()I */
/* iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
/* .line 891 */
v6 = this.mAngleStateController;
/* iget-boolean v7, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldWakeUp:Z */
if ( v7 != null) { // if-eqz v7, :cond_1
/* iget-boolean v7, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mScreenState:Z */
/* if-nez v7, :cond_1 */
/* move v7, v4 */
} // :cond_1
/* move v7, v3 */
} // :goto_0
(( com.android.server.input.padkeyboard.AngleStateController ) v6 ).setKbLevel ( v5, v7 ); // invoke-virtual {v6, v5, v7}, Lcom/android/server/input/padkeyboard/AngleStateController;->setKbLevel(IZ)V
/* .line 892 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 893 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 894 */
/* iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mKbTypeLevel:I */
/* if-ne v6, v0, :cond_2 */
/* move v6, v3 */
} // :cond_2
/* move v6, v4 */
/* .line 893 */
} // :goto_1
final String v7 = "keyboard_type_level"; // const-string v7, "keyboard_type_level"
int v8 = -2; // const/4 v8, -0x2
android.provider.Settings$Secure .putIntForUser ( v5,v7,v6,v8 );
/* .line 896 */
/* new-array v0, v0, [B */
/* .line 897 */
/* .local v0, "wlVersion":[B */
/* const/16 v5, 0x8 */
/* array-length v6, v0 */
java.lang.System .arraycopy ( p1,v5,v0,v3,v6 );
/* .line 898 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2RevertHexString ( v0 );
this.mWirelessVersion = v3;
/* .line 899 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "receive wireless version:"; // const-string v5, "receive wireless version:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mWirelessVersion;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 900 */
} // .end method
private void parsePenState ( Object[] p0 ) {
/* .locals 5 */
/* .param p1, "recBuf" # [B */
/* .line 1302 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Receive Pen State:"; // const-string v1, "Receive Pen State:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x8 */
/* aget-byte v2, p1, v1 */
java.lang.Byte .valueOf ( v2 );
/* filled-new-array {v2}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " PenBatteryState:"; // const-string v2, " PenBatteryState:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x9 */
/* aget-byte v4, p1, v2 */
/* .line 1303 */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v4}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v4 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1302 */
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
android.util.Slog .i ( v3,v0 );
/* .line 1304 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mPenState:I */
/* aget-byte v3, p1, v1 */
/* if-ne v0, v3, :cond_0 */
/* .line 1305 */
return;
/* .line 1307 */
} // :cond_0
/* aget-byte v0, p1, v1 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mPenState:I */
/* .line 1309 */
/* aget-byte v0, p1, v1 */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_1 */
/* .line 1310 */
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v1, p1, v2 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendPenBatteryState(II)V */
/* .line 1311 */
} // :cond_1
/* aget-byte v0, p1, v1 */
/* if-nez v0, :cond_2 */
/* .line 1312 */
/* aget-byte v0, p1, v2 */
/* invoke-direct {p0, v3, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendPenBatteryState(II)V */
/* .line 1314 */
} // :cond_2
} // :goto_0
return;
} // .end method
private Boolean parseReadKeyboardStatus ( Object[] p0 ) {
/* .locals 5 */
/* .param p1, "recBuf" # [B */
/* .line 948 */
int v0 = 7; // const/4 v0, 0x7
/* aget-byte v1, p1, v0 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 949 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive mcu state error:"; // const-string v4, "receive mcu state error:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v4 = "%02x"; // const-string v4, "%02x"
java.lang.String .format ( v4,v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 950 */
/* .line 952 */
} // :cond_0
/* const/16 v0, 0x8 */
/* aget-byte v1, p1, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v3,v0,v1 );
/* if-nez v0, :cond_1 */
/* .line 953 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive mcu checksum error:"; // const-string v1, "receive mcu checksum error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
/* .line 954 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 953 */
android.util.Slog .i ( v2,v0 );
/* .line 955 */
/* .line 958 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->receiveKeyboardStatus()Z */
} // .end method
private Boolean parseReceiveKeyboardStatus ( Object[] p0 ) {
/* .locals 5 */
/* .param p1, "recBuf" # [B */
/* .line 981 */
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v0, p1, v0 */
/* const/16 v1, 0x52 */
int v2 = 0; // const/4 v2, 0x0
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* if-eq v0, v1, :cond_0 */
/* .line 982 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive keyboard state error:"; // const-string v1, "receive keyboard state error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
/* .line 983 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 982 */
android.util.Slog .i ( v3,v0 );
/* .line 984 */
/* .line 986 */
} // :cond_0
/* const/16 v0, 0x13 */
/* aget-byte v1, p1, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v2,v0,v1 );
/* if-nez v0, :cond_1 */
/* .line 987 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive keyboard checksum error:"; // const-string v1, "receive keyboard checksum error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
/* .line 988 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 987 */
android.util.Slog .i ( v3,v0 );
/* .line 989 */
/* .line 991 */
} // :cond_1
int v0 = 6; // const/4 v0, 0x6
/* new-array v1, v0, [B */
/* .line 992 */
/* .local v1, "gsensorData":[B */
/* const/16 v4, 0xa */
java.lang.System .arraycopy ( p1,v4,v1,v2,v0 );
/* .line 993 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Receive TouchPadEnable:"; // const-string v4, "Receive TouchPadEnable:"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 994 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive KeyBoardEnable:"; // const-string v2, "Receive KeyBoardEnable:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v2 = 7; // const/4 v2, 0x7
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 995 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive BackLightEnable:"; // const-string v2, "Receive BackLightEnable:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x8 */
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 996 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive TouchPadSensitivity:"; // const-string v2, "Receive TouchPadSensitivity:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x9 */
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 997 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive GsensorData:"; // const-string v2, "Receive GsensorData:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v1 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 998 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive PenState:"; // const-string v2, "Receive PenState:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x10 */
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 999 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive PenBatteryState:"; // const-string v2, "Receive PenBatteryState:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x11 */
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 1000 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive PowerState:"; // const-string v2, "Receive PowerState:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x12 */
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 1001 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean parseReceiveWriteCmdAck ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "recBuf" # [B */
/* .param p2, "target" # I */
/* .param p3, "value" # I */
/* .line 1097 */
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v0, p1, v0 */
/* const/16 v1, 0x30 */
int v2 = 0; // const/4 v2, 0x0
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* if-eq v0, v1, :cond_0 */
/* .line 1098 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive keyboard write result status error:"; // const-string v1, "receive keyboard write result status error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
/* .line 1099 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1098 */
android.util.Slog .i ( v3,v0 );
/* .line 1100 */
/* .line 1102 */
} // :cond_0
/* const/16 v0, 0x8 */
/* aget-byte v1, p1, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v2,v0,v1 );
/* if-nez v0, :cond_1 */
/* .line 1103 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive keyboard write result checksum error:"; // const-string v1, "receive keyboard write result checksum error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
/* .line 1104 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1103 */
android.util.Slog .i ( v3,v0 );
/* .line 1105 */
/* .line 1107 */
} // :cond_1
int v0 = 6; // const/4 v0, 0x6
/* aget-byte v0, p1, v0 */
/* if-ne p2, v0, :cond_2 */
int v0 = 7; // const/4 v0, 0x7
/* aget-byte v0, p1, v0 */
/* if-ne p3, v0, :cond_2 */
/* .line 1108 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "write cmd success, command:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " value:"; // const-string v1, " value:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 1110 */
} // :cond_2
/* const-string/jumbo v0, "write cmd failed" */
android.util.Slog .i ( v3,v0 );
/* .line 1112 */
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void parseReportData ( Object[] p0 ) {
/* .locals 2 */
/* .param p1, "recBuf" # [B */
/* .line 1225 */
int v0 = 0; // const/4 v0, 0x0
/* aget-byte v0, p1, v0 */
/* const/16 v1, 0x26 */
/* if-ne v0, v1, :cond_0 */
int v0 = 2; // const/4 v0, 0x2
/* aget-byte v0, p1, v0 */
/* const/16 v1, 0x38 */
/* if-ne v0, v1, :cond_0 */
/* .line 1226 */
int v0 = 4; // const/4 v0, 0x4
/* aget-byte v0, p1, v0 */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 1232 */
/* :sswitch_0 */
int v0 = 6; // const/4 v0, 0x6
/* aget-byte v0, p1, v0 */
/* packed-switch v0, :pswitch_data_0 */
/* .line 1243 */
/* .line 1240 */
/* :pswitch_0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parsePenState([B)V */
/* .line 1241 */
/* .line 1237 */
/* :pswitch_1 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseHallState([B)V */
/* .line 1238 */
/* .line 1234 */
/* :pswitch_2 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseGsensorData([B)V */
/* .line 1235 */
/* .line 1228 */
/* :sswitch_1 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseConnectState([B)Z */
/* .line 1229 */
/* nop */
/* .line 1251 */
} // :cond_0
} // :goto_0
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5e -> :sswitch_1 */
/* 0x69 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch -0x60 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean parseUsbDeviceVersion ( Object[] p0 ) {
/* .locals 6 */
/* .param p1, "recBuf" # [B */
/* .line 804 */
int v0 = 6; // const/4 v0, 0x6
/* aget-byte v1, p1, v0 */
int v2 = 2; // const/4 v2, 0x2
final String v3 = "%02x"; // const-string v3, "%02x"
final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
int v5 = 0; // const/4 v5, 0x0
/* if-eq v1, v2, :cond_0 */
/* .line 805 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive version state error:"; // const-string v2, "receive version state error:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v0 );
/* .line 806 */
/* .line 808 */
} // :cond_0
/* const/16 v0, 0x1e */
/* aget-byte v1, p1, v0 */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v5,v0,v1 );
/* if-nez v1, :cond_1 */
/* .line 809 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive version checksum error:"; // const-string v2, "receive version checksum error:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v0 );
/* .line 810 */
/* .line 813 */
} // :cond_1
/* const/16 v0, 0x10 */
/* new-array v1, v0, [B */
/* .line 814 */
/* .local v1, "deviceVersion":[B */
int v2 = 7; // const/4 v2, 0x7
java.lang.System .arraycopy ( p1,v2,v1,v5,v0 );
/* .line 815 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2String ( v1 );
this.mMcuVersion = v0;
/* .line 816 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive mcu version:"; // const-string v2, "receive mcu version:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mMcuVersion;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v0 );
/* .line 817 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean parseUsbReset ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "recBuf" # [B */
/* .line 850 */
int v0 = 0; // const/4 v0, 0x0
/* aget-byte v1, p1, v0 */
/* const/16 v2, 0x24 */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v2, :cond_1 */
/* aget-byte v1, p1, v3 */
/* const/16 v2, 0x31 */
/* if-ne v1, v2, :cond_1 */
int v1 = 6; // const/4 v1, 0x6
/* aget-byte v1, p1, v1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 854 */
} // :cond_0
/* .line 851 */
} // :cond_1
} // :goto_0
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "receive reset success"; // const-string v1, "receive reset success"
android.util.Slog .i ( v0,v1 );
/* .line 852 */
} // .end method
private Boolean parseWriteCmdAck ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "recBuf" # [B */
/* .param p2, "target" # I */
/* .param p3, "value" # I */
/* .line 1066 */
int v0 = 7; // const/4 v0, 0x7
/* aget-byte v0, p1, v0 */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1067 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "receive write cmd ack error:"; // const-string v3, "receive write cmd ack error:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v3, p1 */
/* .line 1068 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1067 */
android.util.Slog .i ( v1,v0 );
/* .line 1069 */
/* .line 1072 */
} // :cond_0
/* const/16 v0, 0x8 */
/* aget-byte v3, p1, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v2,v0,v3 );
/* if-nez v0, :cond_1 */
/* .line 1073 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "receive write cmd ack checksum error:"; // const-string v3, "receive write cmd ack checksum error:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v3, p1 */
/* .line 1074 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1073 */
android.util.Slog .i ( v1,v0 );
/* .line 1075 */
/* .line 1078 */
} // :cond_1
v0 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->receiveWriteCmdAck(II)Z */
} // .end method
private Boolean parseWriteKeyBoardStatus ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "recBuf" # [B */
/* .param p2, "target" # I */
/* .param p3, "value" # I */
/* .line 1029 */
int v0 = 7; // const/4 v0, 0x7
/* aget-byte v1, p1, v0 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1030 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "receive mcu state error:"; // const-string v4, "receive mcu state error:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p1, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v4 = "%02x"; // const-string v4, "%02x"
java.lang.String .format ( v4,v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v0 );
/* .line 1031 */
/* .line 1033 */
} // :cond_0
/* const/16 v0, 0x8 */
/* aget-byte v1, p1, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p1,v3,v0,v1 );
/* if-nez v0, :cond_1 */
/* .line 1034 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive mcu checksum error:"; // const-string v1, "receive mcu checksum error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p1 */
/* .line 1035 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1034 */
android.util.Slog .i ( v2,v0 );
/* .line 1036 */
/* .line 1038 */
} // :cond_1
v0 = /* invoke-direct {p0, p2, p3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readWriteCmdAck(II)Z */
} // .end method
private void processIdentity ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "identity" # I */
/* .line 1620 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
/* packed-switch p1, :pswitch_data_0 */
/* .line 1648 */
/* :pswitch_0 */
int v1 = 4; // const/4 v1, 0x4
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->showRejectConfirmDialog(I)V */
/* .line 1649 */
final String v1 = "keyboard identity transfer error"; // const-string v1, "keyboard identity transfer error"
android.util.Slog .i ( v2,v1 );
/* .line 1650 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1651 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1652 */
/* .line 1643 */
/* :pswitch_1 */
final String v1 = "keyboard identity internal error"; // const-string v1, "keyboard identity internal error"
android.util.Slog .i ( v2,v1 );
/* .line 1644 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1645 */
/* .line 1634 */
/* :pswitch_2 */
/* iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I */
int v4 = 5; // const/4 v4, 0x5
/* if-ge v3, v4, :cond_0 */
/* .line 1635 */
/* const/16 v3, 0x1388 */
/* invoke-direct {p0, v3, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->checkKeyboardIdentity(IZ)V */
/* .line 1636 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I */
/* add-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mCheckIdentityTimes:I */
/* .line 1638 */
} // :cond_0
final String v0 = "keyboard identity need check again"; // const-string v0, "keyboard identity need check again"
android.util.Slog .i ( v2,v0 );
/* .line 1639 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).setIdentityState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1640 */
/* .line 1627 */
/* :pswitch_3 */
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->showRejectConfirmDialog(I)V */
/* .line 1628 */
final String v1 = "keyboard identity auth reject"; // const-string v1, "keyboard identity auth reject"
android.util.Slog .i ( v2,v1 );
/* .line 1629 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1630 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1631 */
/* .line 1622 */
/* :pswitch_4 */
final String v0 = "keyboard identity auth ok"; // const-string v0, "keyboard identity auth ok"
android.util.Slog .i ( v2,v0 );
/* .line 1623 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).setIdentityState ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1624 */
/* nop */
/* .line 1656 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean readWriteCmdAck ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "target" # I */
/* .param p2, "value" # I */
/* .line 1042 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* if-nez v0, :cond_0 */
/* .line 1046 */
} // :cond_0
v0 = this.mSendBuf;
java.util.Arrays .fill ( v0,v1 );
/* .line 1047 */
com.android.server.input.padkeyboard.usb.UsbKeyboardUtil .commandGetKeyboardStatus ( p1 );
/* .line 1048 */
/* .local v0, "command":[B */
v2 = this.mSendBuf;
/* array-length v3, v0 */
java.lang.System .arraycopy ( v0,v1,v2,v1,v3 );
/* .line 1050 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
int v3 = 2; // const/4 v3, 0x2
/* if-ge v2, v3, :cond_4 */
/* .line 1051 */
v3 = this.mUsbConnection;
v4 = this.mOutUsbEndpoint;
v5 = this.mSendBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1052 */
v3 = this.mRecBuf;
java.util.Arrays .fill ( v3,v1 );
/* .line 1053 */
} // :cond_1
v3 = this.mUsbConnection;
v4 = this.mInUsbEndpoint;
v5 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 1054 */
v3 = this.mRecBuf;
v3 = /* invoke-direct {p0, v3, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseWriteCmdAck([BII)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1055 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1059 */
} // :cond_2
final String v3 = "MiuiPadKeyboardManager"; // const-string v3, "MiuiPadKeyboardManager"
/* const-string/jumbo v4, "send write cmd ack failed" */
android.util.Slog .i ( v3,v4 );
/* .line 1050 */
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1062 */
} // .end local v2 # "i":I
} // :cond_4
/* .line 1043 */
} // .end local v0 # "command":[B
} // :cond_5
} // :goto_1
} // .end method
private Boolean receiveKeyboardStatus ( ) {
/* .locals 5 */
/* .line 962 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
/* if-nez v0, :cond_0 */
/* .line 966 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v2 = 2; // const/4 v2, 0x2
/* if-ge v0, v2, :cond_3 */
/* .line 967 */
v2 = this.mRecBuf;
java.util.Arrays .fill ( v2,v1 );
/* .line 968 */
v2 = this.mUsbConnection;
v3 = this.mInUsbEndpoint;
v4 = this.mRecBuf;
v2 = /* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 969 */
v2 = this.mRecBuf;
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReceiveKeyboardStatus([B)Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 970 */
int v1 = 1; // const/4 v1, 0x1
/* .line 973 */
} // :cond_1
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
final String v3 = "receive keyboard status failed"; // const-string v3, "receive keyboard status failed"
android.util.Slog .i ( v2,v3 );
/* .line 975 */
} // :cond_2
/* const/16 v2, 0x64 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v2 );
/* .line 966 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 977 */
} // .end local v0 # "i":I
} // :cond_3
/* .line 963 */
} // :cond_4
} // :goto_1
} // .end method
private Boolean receiveWriteCmdAck ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "target" # I */
/* .param p2, "value" # I */
/* .line 1082 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 2; // const/4 v1, 0x2
int v2 = 0; // const/4 v2, 0x0
/* if-ge v0, v1, :cond_2 */
/* .line 1083 */
v1 = this.mRecBuf;
java.util.Arrays .fill ( v1,v2 );
/* .line 1084 */
v1 = this.mUsbConnection;
v2 = this.mInUsbEndpoint;
v3 = this.mRecBuf;
v1 = /* invoke-direct {p0, v1, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1085 */
v1 = this.mRecBuf;
v1 = /* invoke-direct {p0, v1, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->parseReceiveWriteCmdAck([BII)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1086 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1089 */
} // :cond_0
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
final String v2 = "receive keyboard write result failed"; // const-string v2, "receive keyboard write result failed"
android.util.Slog .i ( v1,v2 );
/* .line 1090 */
/* const/16 v1, 0x64 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .operationWait ( v1 );
/* .line 1082 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1093 */
} // .end local v0 # "i":I
} // :cond_2
} // .end method
private void registerBroadcastReceiver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 178 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "registerBroadcastReceiver"; // const-string v1, "registerBroadcastReceiver"
android.util.Slog .i ( v0,v1 );
/* .line 179 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;-><init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V */
this.mUsbActionReceiver = v0;
/* .line 180 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 181 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "android.hardware.usb.action.USB_DEVICE_ATTACHED"; // const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 182 */
final String v1 = "android.hardware.usb.action.USB_DEVICE_DETACHED"; // const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 183 */
v1 = this.mUsbActionReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 184 */
return;
} // .end method
private void sendPenBatteryState ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "penState" # I */
/* .param p2, "penBatteryState" # I */
/* .line 1601 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1602 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"; // const-string v1, "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1603 */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
final String v2 = "com.android.settings.stylus.MiuiStylusReceiver"; // const-string v2, "com.android.settings.stylus.MiuiStylusReceiver"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1605 */
final String v1 = "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"; // const-string v1, "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1606 */
final String v1 = "miui.intent.extra.REVERSE_PEN_SOC"; // const-string v1, "miui.intent.extra.REVERSE_PEN_SOC"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1607 */
/* const-string/jumbo v1, "source" */
final String v2 = "keyboard"; // const-string v2, "keyboard"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1608 */
v1 = this.mContext;
v2 = android.os.UserHandle.ALL;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1609 */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
final String v2 = "pen battery state send"; // const-string v2, "pen battery state send"
android.util.Slog .i ( v1,v2 );
/* .line 1610 */
return;
} // .end method
private Boolean sendUsbData ( android.hardware.usb.UsbDeviceConnection p0, android.hardware.usb.UsbEndpoint p1, Object[] p2 ) {
/* .locals 3 */
/* .param p1, "connection" # Landroid/hardware/usb/UsbDeviceConnection; */
/* .param p2, "endpoint" # Landroid/hardware/usb/UsbEndpoint; */
/* .param p3, "data" # [B */
/* .line 1116 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* if-nez p3, :cond_0 */
/* .line 1119 */
} // :cond_0
/* array-length v1, p3 */
/* const/16 v2, 0x1f4 */
v1 = (( android.hardware.usb.UsbDeviceConnection ) p1 ).bulkTransfer ( p2, p3, v1, v2 ); // invoke-virtual {p1, p2, p3, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 1117 */
} // :cond_2
} // :goto_0
} // .end method
public static Integer shouldClearActivityInfoFlags ( ) {
/* .locals 1 */
/* .line 486 */
/* const/16 v0, 0x30 */
} // .end method
private void showRejectConfirmDialog ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 1659 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getSystemUiContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;
/* .line 1661 */
/* .local v0, "systemuiContext":Landroid/content/Context; */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1671 */
return;
/* .line 1667 */
/* :sswitch_0 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1668 */
/* const v2, 0x110f024c */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1669 */
/* .local v1, "message":Ljava/lang/String; */
/* .line 1663 */
} // .end local v1 # "message":Ljava/lang/String;
/* :sswitch_1 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1664 */
/* const v2, 0x110f024b */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1665 */
/* .restart local v1 # "message":Ljava/lang/String; */
/* nop */
/* .line 1674 */
} // :goto_0
v2 = this.mDialog;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1675 */
(( miuix.appcompat.app.AlertDialog ) v2 ).setMessage ( v1 ); // invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V
/* .line 1677 */
} // :cond_0
/* new-instance v2, Lmiuix/appcompat/app/AlertDialog$Builder; */
/* const v3, 0x66110006 */
/* invoke-direct {v2, v0, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V */
/* .line 1679 */
int v3 = 1; // const/4 v3, 0x1
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setCancelable ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1680 */
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setMessage ( v1 ); // invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1681 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1682 */
/* const v4, 0x110f024a */
(( android.content.res.Resources ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1681 */
int v4 = 0; // const/4 v4, 0x0
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setPositiveButton ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1683 */
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).create ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;
this.mDialog = v2;
/* .line 1684 */
(( miuix.appcompat.app.AlertDialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v2 ).getAttributes ( ); // invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
/* .line 1685 */
/* .local v2, "attrs":Landroid/view/WindowManager$LayoutParams; */
/* const/16 v3, 0x7d3 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 1686 */
/* iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v4, 0x20000 */
/* or-int/2addr v3, v4 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 1687 */
/* const/16 v3, 0x11 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 1688 */
/* iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit16 v3, v3, 0x110 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 1690 */
v3 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v3 ).getWindow ( ); // invoke-virtual {v3}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v3 ).setAttributes ( v2 ); // invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 1692 */
} // .end local v2 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // :goto_1
v2 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v2 ).show ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->show()V
/* .line 1693 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x4 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public static Boolean supportPadKeyboard ( ) {
/* .locals 2 */
/* .line 171 */
/* const-string/jumbo v0, "support_usb_keyboard" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
} // .end method
private void testFunction ( ) {
/* .locals 3 */
/* .line 273 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "---testFunction start-----"; // const-string v1, "---testFunction start-----"
android.util.Slog .i ( v0,v1 );
/* .line 274 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* if-ge v0, v1, :cond_0 */
/* .line 275 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).readConnectState ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readConnectState()V
/* .line 276 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).readMcuVersion ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readMcuVersion()V
/* .line 277 */
/* const/16 v2, 0x23 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).writePadKeyBoardStatus ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 279 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).readKeyboardStatus ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->readKeyboardStatus()V
/* .line 274 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 281 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void unRegisterBroadcastReceiver ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 190 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
/* const-string/jumbo v1, "unRegisterBroadcastReceiver" */
android.util.Slog .i ( v0,v1 );
/* .line 191 */
v0 = this.mUsbActionReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 192 */
(( android.content.Context ) p1 ).unregisterReceiver ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 194 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public commandMiAuthStep3Type1 ( Object[] p0, Object[] p1 ) {
/* .locals 1 */
/* .param p1, "keyMeta" # [B */
/* .param p2, "challenge" # [B */
/* .line 638 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .commandMiAuthStep3Type1ForUSB ( p1,p2 );
} // .end method
public commandMiAuthStep5Type1 ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "token" # [B */
/* .line 643 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [B */
} // .end method
public commandMiDevAuthInit ( ) {
/* .locals 1 */
/* .line 633 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .commandMiDevAuthInitForUSB ( );
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 1704 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1705 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1707 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1708 */
final String v0 = "mUsbDevice="; // const-string v0, "mUsbDevice="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1709 */
v0 = this.mUsbDevice;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1710 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[DeviceName="; // const-string v1, "[DeviceName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mUsbDevice;
(( android.hardware.usb.UsbDevice ) v1 ).getDeviceName ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",VendorId="; // const-string v1, ",VendorId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mUsbDevice;
/* .line 1711 */
v1 = (( android.hardware.usb.UsbDevice ) v1 ).getVendorId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",ProductId="; // const-string v1, ",ProductId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mUsbDevice;
/* .line 1712 */
v1 = (( android.hardware.usb.UsbDevice ) v1 ).getProductId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1710 */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1714 */
} // :cond_0
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 1717 */
} // :goto_0
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1718 */
final String v0 = "mMcuVersion="; // const-string v0, "mMcuVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1719 */
v0 = this.mMcuVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1721 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1722 */
final String v0 = "mKeyboardVersion="; // const-string v0, "mKeyboardVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1723 */
v0 = this.mKeyboardVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1725 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1726 */
final String v0 = "mWirelessVersion="; // const-string v0, "mWirelessVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1727 */
v0 = this.mWirelessVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1729 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/padkeyboard/AngleStateController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 1731 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1732 */
final String v0 = "mRecentConnTime="; // const-string v0, "mRecentConnTime="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1733 */
v0 = this.mRecentConnTime;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1734 */
return;
} // .end method
public void enableOrDisableInputDevice ( ) {
/* .locals 6 */
/* .line 1576 */
v0 = this.mInputManager;
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
(( com.android.server.input.InputManagerService ) v0 ).getInputDevice ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 1577 */
/* .local v0, "inputDevice":Landroid/view/InputDevice; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1578 */
v1 = (( android.view.InputDevice ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->isEnabled()Z
v2 = this.mAngleStateController;
v2 = (( com.android.server.input.padkeyboard.AngleStateController ) v2 ).shouldIgnoreKeyboard ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z
/* if-eq v1, v2, :cond_0 */
/* .line 1579 */
return;
/* .line 1582 */
} // :cond_0
v1 = this.mAngleStateController;
v1 = (( com.android.server.input.padkeyboard.AngleStateController ) v1 ).shouldIgnoreKeyboard ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x23 */
final String v4 = "MiuiPadKeyboardManager"; // const-string v4, "MiuiPadKeyboardManager"
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1583 */
v1 = this.mInputManager;
/* iget v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
(( com.android.server.input.InputManagerService ) v1 ).disableInputDevice ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V
/* .line 1584 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1585 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).writePadKeyBoardStatus ( v3, v2 ); // invoke-virtual {p0, v3, v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 1588 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "disable keyboard device id: "; // const-string v3, "disable keyboard device id: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v1 );
/* .line 1590 */
} // :cond_2
v1 = this.mInputManager;
/* iget v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
(( com.android.server.input.InputManagerService ) v1 ).enableInputDevice ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V
/* .line 1591 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldEnableBackLight:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1592 */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).writePadKeyBoardStatus ( v3, v1 ); // invoke-virtual {p0, v3, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 1595 */
} // :cond_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "enable keyboard device id: "; // const-string v3, "enable keyboard device id: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v1 );
/* .line 1597 */
} // :goto_0
v1 = this.mInputManager;
final String v3 = "notifyIgnoredInputDevicesChanged"; // const-string v3, "notifyIgnoredInputDevicesChanged"
/* new-array v2, v2, [Ljava/lang/Object; */
com.android.server.input.ReflectionUtils .callPrivateMethod ( v1,v3,v2 );
/* .line 1598 */
return;
} // .end method
public void getKeyboardReportData ( ) {
/* .locals 2 */
/* .line 345 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
v0 = (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z
/* if-nez v0, :cond_0 */
/* .line 346 */
v0 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 348 */
} // :cond_0
return;
} // .end method
public miui.hardware.input.MiuiKeyboardStatus getKeyboardStatus ( ) {
/* .locals 10 */
/* .line 450 */
/* new-instance v9, Lmiui/hardware/input/MiuiKeyboardStatus; */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
v0 = this.mAngleStateController;
/* .line 451 */
v2 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).getIdentityStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getIdentityStatus()Z
v0 = this.mAngleStateController;
/* .line 452 */
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).isWorkState ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->isWorkState()Z
/* xor-int/lit8 v3, v0, 0x1 */
v0 = this.mAngleStateController;
/* .line 453 */
v4 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).getLidStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getLidStatus()Z
v0 = this.mAngleStateController;
/* .line 454 */
v5 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).getTabletStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getTabletStatus()Z
v0 = this.mAngleStateController;
/* .line 455 */
v6 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
v7 = this.mMcuVersion;
v8 = this.mKeyboardVersion;
/* move-object v0, v9 */
/* invoke-direct/range {v0 ..v8}, Lmiui/hardware/input/MiuiKeyboardStatus;-><init>(ZZZZZZLjava/lang/String;Ljava/lang/String;)V */
/* .line 450 */
} // .end method
public void getMcuResetMode ( ) {
/* .locals 2 */
/* .line 301 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 302 */
return;
} // .end method
public Boolean isKeyboardReady ( ) {
/* .locals 1 */
/* .line 445 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mAngleStateController;
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboard ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboard()Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyLidSwitchChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "lidOpen" # Z */
/* .line 427 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).notifyLidSwitchChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyLidSwitchChanged(Z)V
/* .line 428 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 429 */
return;
} // .end method
public void notifyScreenState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "screenState" # Z */
/* .line 472 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mScreenState:Z */
/* .line 473 */
/* if-nez p1, :cond_1 */
/* .line 474 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldWakeUp:Z */
/* .line 475 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 477 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z */
/* .line 479 */
} // :cond_0
v0 = this.mDialog;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( miuix.appcompat.app.AlertDialog ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 480 */
v0 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).dismiss ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V
/* .line 483 */
} // :cond_1
return;
} // .end method
public void notifyTabletSwitchChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "tabletOpen" # Z */
/* .line 437 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).notifyTabletSwitchChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyTabletSwitchChanged(Z)V
/* .line 438 */
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->enableOrDisableInputDevice()V
/* .line 439 */
return;
} // .end method
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 1545 */
return;
} // .end method
public void onKeyboardAction ( ) {
/* .locals 2 */
/* .line 264 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "onKeyboardAction"; // const-string v1, "onKeyboardAction"
android.util.Slog .i ( v0,v1 );
/* .line 265 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 266 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mShouldWakeUp:Z */
/* .line 267 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsFirstAction:Z */
/* .line 269 */
} // :cond_0
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).getKeyboardReportData ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getKeyboardReportData()V
/* .line 270 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 1537 */
v0 = this.sensor;
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 1538 */
v0 = this.values;
v1 = this.mLocalGData;
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
java.lang.System .arraycopy ( v0,v3,v1,v3,v2 );
/* .line 1540 */
} // :cond_0
return;
} // .end method
public void readConnectState ( ) {
/* .locals 2 */
/* .line 287 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 288 */
return;
} // .end method
public void readKeyboardStatus ( ) {
/* .locals 2 */
/* .line 316 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 317 */
return;
} // .end method
public void readKeyboardVersion ( ) {
/* .locals 2 */
/* .line 309 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessageAtFrontOfQueue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
/* .line 310 */
return;
} // .end method
public void readMcuVersion ( ) {
/* .locals 2 */
/* .line 294 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 295 */
return;
} // .end method
public android.view.InputDevice removeKeyboardDevicesIfNeeded ( android.view.InputDevice[] p0 ) {
/* .locals 8 */
/* .param p1, "allInputDevices" # [Landroid/view/InputDevice; */
/* .line 356 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 357 */
/* .local v0, "newInputDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/InputDevice;>;" */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_3 */
/* aget-object v4, p1, v3 */
/* .line 358 */
/* .local v4, "inputDevice":Landroid/view/InputDevice; */
v5 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v6, 0x3ffc */
/* if-ne v5, v6, :cond_2 */
/* .line 359 */
v5 = (( android.view.InputDevice ) v4 ).getVendorId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I
/* const/16 v6, 0x3206 */
/* if-ne v5, v6, :cond_2 */
/* .line 360 */
/* iget v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
v6 = (( android.view.InputDevice ) v4 ).getId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getId()I
final String v7 = "MiuiPadKeyboardManager"; // const-string v7, "MiuiPadKeyboardManager"
/* if-eq v5, v6, :cond_0 */
/* .line 361 */
v5 = (( android.view.InputDevice ) v4 ).getId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getId()I
/* iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
/* .line 362 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "update keyboard device id: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mInputDeviceId:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v5 );
/* .line 364 */
} // :cond_0
/* iget-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->mIsKeyboardReady:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 365 */
/* .line 367 */
} // :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "filter keyboard device :"; // const-string v6, "filter keyboard device :"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.view.InputDevice ) v4 ).getName ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v5 );
/* .line 368 */
/* .line 370 */
} // :cond_2
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 357 */
} // .end local v4 # "inputDevice":Landroid/view/InputDevice;
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 372 */
} // :cond_3
/* new-array v1, v2, [Landroid/view/InputDevice; */
(( java.util.ArrayList ) v0 ).toArray ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v1, [Landroid/view/InputDevice; */
} // .end method
public sendCommandForRespond ( Object[] p0, com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback p1 ) {
/* .locals 7 */
/* .param p1, "command" # [B */
/* .param p2, "callback" # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback; */
/* .line 592 */
java.lang.Thread .currentThread ( );
v1 = this.mHandlerThread;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
/* if-eq v0, v1, :cond_0 */
/* .line 593 */
/* const-string/jumbo v0, "sendCommandForRespond should be called in mHandlerThread" */
android.util.Slog .i ( v2,v0 );
/* .line 594 */
/* new-array v0, v3, [B */
/* .line 597 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->getDeviceReadyForTransfer()Z */
/* if-nez v0, :cond_1 */
/* .line 598 */
final String v0 = "getDeviceReadyForTransfer fail"; // const-string v0, "getDeviceReadyForTransfer fail"
android.util.Slog .i ( v2,v0 );
/* .line 599 */
/* new-array v0, v3, [B */
/* .line 602 */
} // :cond_1
v0 = this.mSendBuf;
java.util.Arrays .fill ( v0,v3 );
/* .line 603 */
v0 = this.mSendBuf;
/* array-length v1, p1 */
java.lang.System .arraycopy ( p1,v3,v0,v3,v1 );
/* .line 605 */
int v0 = 0; // const/4 v0, 0x0
/* .line 606 */
/* .local v0, "failCount":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
int v4 = 2; // const/4 v4, 0x2
/* if-ge v1, v4, :cond_6 */
/* .line 607 */
v4 = this.mUsbConnection;
v5 = this.mOutUsbEndpoint;
v6 = this.mSendBuf;
v4 = /* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 608 */
v4 = this.mRecBuf;
java.util.Arrays .fill ( v4,v3 );
/* .line 609 */
} // :cond_2
v4 = this.mUsbConnection;
v5 = this.mInUsbEndpoint;
v6 = this.mRecBuf;
v4 = /* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->sendUsbData(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;[B)Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 610 */
if ( p2 != null) { // if-eqz p2, :cond_3
v4 = v4 = this.mRecBuf;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 611 */
} // :cond_3
v2 = this.mRecBuf;
/* .line 615 */
} // :cond_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Try Send command failed:"; // const-string v5, "Try Send command failed:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mSendBuf;
/* array-length v6, v5 */
/* .line 616 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v5,v6 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 615 */
android.util.Slog .v ( v2,v4 );
/* .line 618 */
} // :cond_5
/* add-int/lit8 v0, v0, 0x1 */
/* .line 606 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 621 */
} // .end local v1 # "i":I
} // :cond_6
/* if-lez v0, :cond_7 */
/* .line 622 */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->cleanUsbCash(I)V */
/* .line 625 */
} // :cond_7
/* if-ne v0, v4, :cond_8 */
/* .line 626 */
/* new-array v1, v3, [B */
/* .line 628 */
} // :cond_8
v1 = this.mRecBuf;
(( ) v1 ).clone ( ); // invoke-virtual {v1}, [B->clone()Ljava/lang/Object;
/* check-cast v1, [B */
} // .end method
public void sendKeyboardCapsLock ( ) {
/* .locals 2 */
/* .line 338 */
/* const/16 v0, 0x26 */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager ) p0 ).writePadKeyBoardStatus ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 339 */
return;
} // .end method
public void startKeyboardUpgrade ( ) {
/* .locals 2 */
/* .line 407 */
v0 = this.mHandler;
/* const/16 v1, 0x9 */
v0 = (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z
/* if-nez v0, :cond_0 */
/* .line 408 */
v0 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 410 */
} // :cond_0
return;
} // .end method
public void startMcuUpgrade ( ) {
/* .locals 2 */
/* .line 398 */
v0 = this.mHandler;
/* const/16 v1, 0x8 */
v0 = (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z
/* if-nez v0, :cond_0 */
/* .line 399 */
v0 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 401 */
} // :cond_0
return;
} // .end method
public void startWirelessUpgrade ( ) {
/* .locals 2 */
/* .line 416 */
v0 = this.mHandler;
/* const/16 v1, 0xa */
v0 = (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessages(I)Z
/* if-nez v0, :cond_0 */
/* .line 417 */
v0 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 419 */
} // :cond_0
return;
} // .end method
public void writePadKeyBoardStatus ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "target" # I */
/* .param p2, "value" # I */
/* .line 326 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->obtainMessage(I)Landroid/os/Message;
/* .line 327 */
/* .local v0, "msg":Landroid/os/Message; */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 328 */
/* .local v1, "bundle":Landroid/os/Bundle; */
/* const-string/jumbo v2, "target" */
(( android.os.Bundle ) v1 ).putInt ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 329 */
/* const-string/jumbo v2, "value" */
(( android.os.Bundle ) v1 ).putInt ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 330 */
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 331 */
v2 = this.mHandler;
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->sendMessage(Landroid/os/Message;)Z
/* .line 332 */
return;
} // .end method
