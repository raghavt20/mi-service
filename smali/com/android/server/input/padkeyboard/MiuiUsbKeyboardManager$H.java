class com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H extends android.os.Handler {
	 /* .source "MiuiUsbKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final Integer MSG_CHECK_KEYBOARD_IDENTITY;
private static final Integer MSG_GET_DEVICE_TIME_OUT;
private static final Integer MSG_GET_KEYBOARD_REPORT_DATA;
private static final Integer MSG_GET_MCU_RESET_MODE;
private static final Integer MSG_READ_CONNECT_STATE;
private static final Integer MSG_READ_KEYBOARD_STATUS;
private static final Integer MSG_READ_KEYBOARD_VERSION;
private static final Integer MSG_READ_MCU_VERSION;
private static final Integer MSG_START_KEYBOARD_UPGRADE;
private static final Integer MSG_START_MCU_UPGRADE;
private static final Integer MSG_START_WIRELESS_UPGRADE;
private static final Integer MSG_TEST_FUNCTION;
private static final Integer MSG_WRITE_KEYBOARD_STATUS;
/* # instance fields */
final com.android.server.input.padkeyboard.MiuiUsbKeyboardManager this$0; //synthetic
/* # direct methods */
public com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 504 */
this.this$0 = p1;
/* .line 505 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 506 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 511 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 572 */
/* :pswitch_0 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "reset keyboard usb host cause get usb device time out"; // const-string v1, "reset keyboard usb host cause get usb device time out"
android.util.Slog .i ( v0,v1 );
/* .line 573 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmKeyboardDevicesObserver ( v0 );
(( com.android.server.input.padkeyboard.usb.UsbKeyboardDevicesObserver ) v0 ).resetKeyboardHost ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/usb/UsbKeyboardDevicesObserver;->resetKeyboardHost()V
/* goto/16 :goto_0 */
/* .line 563 */
/* :pswitch_1 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 564 */
/* .local v0, "bundle":Landroid/os/Bundle; */
/* if-nez v0, :cond_0 */
/* .line 565 */
/* .line 567 */
} // :cond_0
final String v1 = "first_check"; // const-string v1, "first_check"
int v2 = 1; // const/4 v2, 0x1
v1 = (( android.os.Bundle ) v0 ).getBoolean ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
/* .line 568 */
/* .local v1, "isFirst":Z */
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoCheckKeyboardIdentity ( v2,v1 );
/* .line 569 */
/* .line 559 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // .end local v1 # "isFirst":Z
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoStartWirelessUpgrade ( v0 );
/* .line 560 */
/* .line 555 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoStartKeyboardUpgrade ( v0 );
/* .line 556 */
/* .line 551 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoStartMcuUpgrade ( v0 );
/* .line 552 */
/* .line 547 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoGetReportData ( v0 );
/* .line 548 */
/* .line 537 */
/* :pswitch_6 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 538 */
/* .restart local v0 # "bundle":Landroid/os/Bundle; */
/* if-nez v0, :cond_1 */
/* .line 539 */
/* .line 541 */
} // :cond_1
/* const-string/jumbo v1, "target" */
int v2 = 0; // const/4 v2, 0x0
v1 = (( android.os.Bundle ) v0 ).getInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 542 */
/* .local v1, "target":I */
/* const-string/jumbo v3, "value" */
v2 = (( android.os.Bundle ) v0 ).getInt ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 543 */
/* .local v2, "value":I */
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoWritePadKeyBoardStatus ( v3,v1,v2 );
/* .line 544 */
/* .line 533 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // .end local v1 # "target":I
} // .end local v2 # "value":I
/* :pswitch_7 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoReadKeyboardStatus ( v0 );
/* .line 534 */
/* .line 529 */
/* :pswitch_8 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoReadKeyboardVersion ( v0 );
/* .line 530 */
/* .line 525 */
/* :pswitch_9 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoGetMcuReset ( v0 );
/* .line 526 */
/* .line 521 */
/* :pswitch_a */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoReadMcuVersion ( v0 );
/* .line 522 */
/* .line 517 */
/* :pswitch_b */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mdoReadConnectState ( v0 );
/* .line 518 */
/* .line 513 */
/* :pswitch_c */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mtestFunction ( v0 );
/* .line 514 */
/* nop */
/* .line 576 */
} // :goto_0
v0 = (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) p0 ).hasMessagesOrCallbacks ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->hasMessagesOrCallbacks()Z
/* if-nez v0, :cond_2 */
/* .line 577 */
v0 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mcloseDevice ( v0 );
/* .line 579 */
} // :cond_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
