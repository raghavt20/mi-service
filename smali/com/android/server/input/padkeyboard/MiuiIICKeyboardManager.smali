.class public Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
.super Ljava/lang/Object;
.source "MiuiIICKeyboardManager.java"

# interfaces
.implements Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;
.implements Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;
.implements Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;,
        Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;,
        Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;
    }
.end annotation


# static fields
.field public static final AUTO_BACK_BRIGHTNESS:Ljava/lang/String; = "keyboard_back_light_automatic_adjustment"

.field public static final CURRENT_BACK_BRIGHTNESS:Ljava/lang/String; = "keyboard_back_light_brightness"

.field private static final DEFAULT_DEVICE_ID:I = -0x63

.field public static final ENABLE_TAP_TOUCH_PAD:Ljava/lang/String; = "enable_tap_touch_pad"

.field private static final ENABLE_UPGRADE_NO_CHECK:Ljava/lang/String; = "enable_upgrade_no_check"

.field public static final LAST_CONNECT_BLE_ADDRESS:Ljava/lang/String; = "miui_keyboard_address"

.field private static final MAX_CHECK_IDENTITY_TIMES:I = 0x5

.field private static volatile sInstance:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;


# instance fields
.field private final G:F

.field private final mAccSensor:Landroid/hardware/Sensor;

.field private final mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

.field private volatile mAuthStarted:Z

.field private mBackLightBrightness:I

.field private final mBleDeviceList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mBleDeviceListLock:Ljava/lang/Object;

.field private final mBluetoothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

.field private volatile mCheckIdentityPass:Z

.field private mCheckIdentityTimes:I

.field private mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

.field private mConsumerDeviceId:I

.field private final mContext:Landroid/content/Context;

.field private mDialog:Lmiuix/appcompat/app/AlertDialog;

.field private final mHandler:Landroid/os/Handler;

.field private mHasTouchPad:Z

.field private final mIICNodeHelper:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

.field private final mInputManager:Lcom/android/server/input/InputManagerService;

.field private mIsAutoBackLight:Z

.field private mIsAutoBackLightEffected:Z

.field private mIsKeyboardReady:Z

.field private mIsKeyboardSleep:Z

.field private mKeyboardAccReady:Z

.field private mKeyboardDevice:Landroid/view/InputDevice;

.field private mKeyboardDeviceId:I

.field private final mKeyboardGData:[F

.field private final mKeyboardObserver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

.field private mKeyboardType:I

.field private mKeyboardVersion:Ljava/lang/String;

.field private mLanguageChangeReceiver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;

.field private final mLightSensor:Landroid/hardware/Sensor;

.field private final mLocalGData:[F

.field private mMCUVersion:Ljava/lang/String;

.field private final mMicMuteReceiver:Landroid/content/BroadcastReceiver;

.field private mPadAccReady:Z

.field private final mPowerManager:Landroid/os/PowerManager;

.field private mScreenState:Z

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private mShouldStayWakeKeyboard:Z

.field private mShouldUpgradeKeyboard:Z

.field private mShouldUpgradeMCU:Z

.field private mShouldUpgradeTouchPad:Z

.field private mStopAwakeRunnable:Ljava/lang/Runnable;

.field private mTouchDeviceId:I

.field private mTouchPadVersion:Ljava/lang/String;

.field private mUserSetBackLight:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mlock:Ljava/lang/Object;


# direct methods
.method public static synthetic $r8$lambda$8IbYAuQyeUENOAD4Cq-4NAWP_Rc(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->acquireWakeLock()V

    return-void
.end method

.method public static synthetic $r8$lambda$9sT42a2-9Fw85TP7xCQpX3WYPvE(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$removeKeyboardDevicesIfNeeded$0(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Fv0MPZV8Tp5UQ8mjMZj8IFvGAUM(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$notifyScreenState$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$Kh2kVhwXy8cdWcBe9iysNWV3MSo(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardAttach()V

    return-void
.end method

.method public static synthetic $r8$lambda$OCmF4I4dWdcMv88u7DuW-ueH4J0(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$notifyScreenState$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$UxCry0Qn23UtO3xIfFxxR13DCTs(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stayWakeForKeyboard176()V

    return-void
.end method

.method public static synthetic $r8$lambda$WP4xfbdfM26WziH9B-vTAZ6Ugbk(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->changeDeviceEnableState()V

    return-void
.end method

.method public static synthetic $r8$lambda$hqI_sPMl8TiZiVcct9qPVwc2f3k(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$postShowRejectConfirmDialog$3(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBluetoothKeyboardManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBluetoothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCommunicationUtil(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIICNodeHelper:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsAutoBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsKeyboardReady(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDevice:Landroid/view/InputDevice;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardObserver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLanguageChangeReceiver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLanguageChangeReceiver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShouldStayWakeKeyboard(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmShouldUpgradeKeyboard(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmShouldUpgradeMCU(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmShouldUpgradeTouchPad(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBackLightBrightness(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsAutoBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUserSetBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoCheckKeyboardIdentity(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->doCheckKeyboardIdentity(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseWakeLock(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->releaseWakeLock()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstopWakeForKeyboard176(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopWakeForKeyboard176()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateLightSensor(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateLightSensor()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x3

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    .line 84
    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardGData:[F

    .line 85
    const v0, 0x411ccccd    # 9.8f

    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->G:F

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityPass:Z

    .line 95
    const/16 v1, 0x141

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardType:I

    .line 97
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mlock:Ljava/lang/Object;

    .line 98
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I

    .line 100
    const/16 v1, -0x63

    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    .line 101
    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    .line 102
    iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    .line 103
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceList:Ljava/util/Set;

    .line 104
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceListLock:Ljava/lang/Object;

    .line 105
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    .line 107
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    .line 108
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z

    .line 110
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLightEffected:Z

    .line 116
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z

    .line 117
    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    .line 118
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z

    .line 120
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z

    .line 124
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z

    .line 125
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    .line 126
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    .line 129
    new-instance v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mMicMuteReceiver:Landroid/content/BroadcastReceiver;

    .line 529
    new-instance v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$2;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$2;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mStopAwakeRunnable:Ljava/lang/Runnable;

    .line 139
    const-string v0, "MiuiPadKeyboardManager"

    const-string v2, "MiuiIICKeyboardManager"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "IIC_Pad_Manager"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 141
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 142
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    .line 143
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBluetoothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    .line 144
    new-instance v2, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    .line 145
    new-instance v2, Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-direct {v2, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 146
    const-string v2, "input"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    check-cast v2, Lcom/android/server/input/InputManagerService;

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    .line 147
    const-string/jumbo v2, "sensor"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    .line 148
    invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAccSensor:Landroid/hardware/Sensor;

    .line 149
    const/4 v1, 0x5

    invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLightSensor:Landroid/hardware/Sensor;

    .line 150
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPowerManager:Landroid/os/PowerManager;

    .line 151
    const/4 v2, 0x6

    const-string v3, "iic_upgrade"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 153
    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver-IA;)V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLanguageChangeReceiver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;

    .line 154
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 155
    .local v1, "languageFilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLanguageChangeReceiver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 157
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v2

    .line 158
    .local v2, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    const/16 v3, 0x15d9

    const/16 v4, 0xa3

    invoke-virtual {v2, v3, v4}, Lcom/android/server/input/config/InputCommonConfig;->setMiuiKeyboardInfo(II)V

    .line 160
    invoke-virtual {v2}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 161
    new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardObserver:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    .line 162
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIICNodeHelper:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    .line 163
    invoke-virtual {v4, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setOtaCallBack(Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;)V

    .line 164
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->observerObject()V

    .line 165
    sget-object v3, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v3, p0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setMiuiIICKeyboardManagerLocked(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    .line 166
    sget-object v3, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v3, p0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->addListener(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V

    .line 167
    invoke-static {}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getInstance()Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 169
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->readKeyboardStatus()V

    .line 170
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->readHallStatus()V

    .line 173
    const/16 v3, 0xa

    invoke-virtual {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 174
    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 175
    return-void
.end method

.method private acquireWakeLock()V
    .locals 1

    .line 1222
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1225
    :cond_0
    return-void
.end method

.method private calculateAngle()V
    .locals 10

    .line 1169
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z

    const-string v1, "MiuiPadKeyboardManager"

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    if-eqz v0, :cond_1

    .line 1170
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->supportCalculateAngle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1171
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardGData:[F

    const/4 v2, 0x0

    aget v3, v0, v2

    const/4 v4, 0x1

    aget v5, v0, v4

    const/4 v6, 0x2

    aget v0, v0, v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    aget v2, v7, v2

    aget v8, v7, v4

    aget v9, v7, v6

    move v4, v5

    move v5, v0

    move v6, v2

    move v7, v8

    move v8, v9

    invoke-static/range {v3 .. v8}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->calculatePKAngleV2(FFFFFF)I

    move-result v0

    .line 1178
    .local v0, "resultAngle":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 1179
    const-string v2, "accel angle error > 0.98, dont update angle"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1181
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "result Angle = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1182
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->updateAngleState(F)V

    .line 1183
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 1185
    .end local v0    # "resultAngle":I
    :goto_0
    goto :goto_1

    .line 1186
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The Pad or Keyboard gsensor not ready, mKeyboardAccReady: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mPadAccReady: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", IIC maybe disconnect"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1189
    :goto_1
    return-void
.end method

.method private changeDeviceEnableState()V
    .locals 6

    .line 381
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isDeviceEnableStatusChanged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    return-void

    .line 384
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableOrEnableDevice:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v0

    const/16 v2, -0x63

    if-eqz v0, :cond_6

    .line 388
    const-string v0, "Disable Xiaomi Keyboard!"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardStatus()V

    .line 390
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    if-eq v0, v2, :cond_1

    .line 391
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V

    .line 393
    :cond_1
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    if-eq v0, v2, :cond_2

    .line 394
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V

    .line 396
    :cond_2
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    if-eq v0, v2, :cond_3

    .line 397
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V

    .line 399
    :cond_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceList:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 400
    .local v3, "bleDevice":I
    if-eq v3, v2, :cond_4

    .line 401
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disable Ble Keyboard:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v4, v3}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V

    .line 404
    .end local v3    # "bleDevice":I
    :cond_4
    goto :goto_0

    :cond_5
    goto :goto_2

    .line 406
    :cond_6
    const-string v0, "Enable Xiaomi Keyboard!"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardStatus()V

    .line 408
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    if-eq v0, v2, :cond_7

    .line 409
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V

    .line 411
    :cond_7
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    if-eq v0, v2, :cond_8

    .line 412
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V

    .line 414
    :cond_8
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    if-eq v0, v2, :cond_9

    .line 415
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V

    .line 417
    :cond_9
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceList:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 418
    .restart local v3    # "bleDevice":I
    if-eq v3, v2, :cond_a

    .line 419
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enable Ble Keyboard:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v4, v3}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V

    .line 422
    .end local v3    # "bleDevice":I
    :cond_a
    goto :goto_1

    .line 424
    :cond_b
    :goto_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "notifyIgnoredInputDevicesChanged"

    invoke-static {v0, v2, v1}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    return-void
.end method

.method private doCheckKeyboardIdentity(Z)V
    .locals 2
    .param p1, "isFirst"    # Z

    .line 1030
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchBeforeU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I

    move-result v0

    goto :goto_0

    .line 1032
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchAfterU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I

    move-result v0

    :goto_0
    nop

    .line 1033
    .local v0, "result":I
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->processIdentity(I)V

    .line 1034
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isFailedIdentity(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1036
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V

    goto :goto_1

    .line 1037
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isSuccessfulIdentity(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1039
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    if-eqz v1, :cond_2

    .line 1040
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 1042
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    if-eqz v1, :cond_3

    .line 1043
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 1045
    :cond_3
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    if-nez v1, :cond_4

    .line 1046
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopStayAwake()V

    .line 1049
    :cond_4
    :goto_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 178
    sget-object v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    if-nez v0, :cond_1

    .line 179
    const-class v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    monitor-enter v0

    .line 180
    :try_start_0
    sget-object v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    if-nez v1, :cond_0

    .line 181
    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    .line 183
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 185
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sInstance:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    return-object v0
.end method

.method private isDeviceEnableStatusChanged()Z
    .locals 9

    .line 429
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    invoke-virtual {v0, v1}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    .line 430
    .local v0, "iicKeyboardDevice":Landroid/view/InputDevice;
    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/InputDevice;->isEnabled()Z

    move-result v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 431
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 432
    return v1

    .line 435
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    invoke-virtual {v2, v3}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v2

    .line 436
    .local v2, "consumerKeyboardDevice":Landroid/view/InputDevice;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/InputDevice;->isEnabled()Z

    move-result v3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 437
    invoke-virtual {v4}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v4

    if-ne v3, v4, :cond_1

    .line 438
    return v1

    .line 441
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    iget v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    invoke-virtual {v3, v4}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v3

    .line 442
    .local v3, "touchPadDevice":Landroid/view/InputDevice;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/InputDevice;->isEnabled()Z

    move-result v4

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 443
    invoke-virtual {v5}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v5

    if-ne v4, v5, :cond_2

    .line 444
    return v1

    .line 447
    :cond_2
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceList:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 448
    .local v5, "belDeviceId":I
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v6, v5}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v6

    .line 449
    .local v6, "bleDevice":Landroid/view/InputDevice;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/view/InputDevice;->isEnabled()Z

    move-result v7

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 450
    invoke-virtual {v8}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v8

    if-ne v7, v8, :cond_3

    .line 451
    return v1

    .line 453
    .end local v5    # "belDeviceId":I
    .end local v6    # "bleDevice":Landroid/view/InputDevice;
    :cond_3
    goto :goto_0

    .line 454
    :cond_4
    const/4 v1, 0x0

    return v1
.end method

.method private isFailedIdentity(I)Z
    .locals 2
    .param p1, "result"    # I

    .line 1058
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private isSuccessfulIdentity(I)Z
    .locals 2
    .param p1, "result"    # I

    .line 1052
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private synthetic lambda$notifyScreenState$1()V
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V

    return-void
.end method

.method private synthetic lambda$notifyScreenState$2()V
    .locals 1

    .line 354
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    return-void
.end method

.method private synthetic lambda$postShowRejectConfirmDialog$3(I)V
    .locals 0
    .param p1, "type"    # I

    .line 1118
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->showRejectConfirmDialog(I)V

    .line 1119
    return-void
.end method

.method private synthetic lambda$removeKeyboardDevicesIfNeeded$0(I)V
    .locals 2
    .param p1, "deviceId"    # I

    .line 280
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceList:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private motionJudge(FFF)Z
    .locals 3
    .param p1, "accX"    # F
    .param p2, "accY"    # F
    .param p3, "accZ"    # F

    .line 1201
    mul-float v0, p1, p1

    mul-float v1, p2, p2

    add-float/2addr v0, v1

    mul-float v1, p3, p3

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1204
    .local v0, "mCombAcc":F
    const v1, 0x411ccccd    # 9.8f

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3e96872b    # 0.294f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private onKeyboardAttach()V
    .locals 3

    .line 988
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->wakeUpIfNeed(Z)V

    .line 989
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardStatus()V

    .line 990
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 991
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 992
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 993
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 994
    invoke-virtual {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 995
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V

    .line 997
    :cond_0
    return-void
.end method

.method private onKeyboardDetach()V
    .locals 1

    .line 1003
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    .line 1006
    :cond_0
    return-void
.end method

.method private onKeyboardStateChanged(IZ)V
    .locals 7
    .param p1, "connectionType"    # I
    .param p2, "connectionStatus"    # Z

    .line 869
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->haveConnection()Z

    move-result v0

    .line 870
    .local v0, "result":Z
    if-nez p1, :cond_0

    .line 872
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V

    .line 874
    if-nez p2, :cond_0

    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    const/16 v2, 0x32

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateBackLightBrightness(I)V

    .line 879
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    const/4 v2, 0x3

    const-string v3, "notifyIgnoredInputDevicesChanged"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v1, v0, :cond_2

    .line 881
    if-nez p1, :cond_1

    .line 882
    if-eqz p2, :cond_1

    .line 884
    iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    .line 885
    iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    .line 886
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 887
    invoke-virtual {p0, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 888
    invoke-direct {p0, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V

    .line 889
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAccSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v5, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 892
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 893
    return-void

    .line 895
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Keyboard link state changed:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ", old status:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v6, "MiuiPadKeyboardManager"

    invoke-static {v6, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    .line 897
    if-eqz v0, :cond_4

    .line 898
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 900
    iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    .line 901
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardAttach()V

    .line 903
    :cond_3
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateLightSensor()V

    .line 904
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAccSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v5, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 906
    :cond_4
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v1, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 907
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->resetCalculateStatus()V

    .line 908
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardDetach()V

    .line 910
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 911
    return-void
.end method

.method private parseAddress2String(B)Ljava/lang/String;
    .locals 2
    .param p1, "address"    # B

    .line 857
    const-string v0, ""

    .line 858
    .local v0, "target":Ljava/lang/String;
    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    .line 859
    const-string v0, "MCU"

    goto :goto_0

    .line 860
    :cond_0
    const/16 v1, 0x40

    if-ne p1, v1, :cond_1

    .line 861
    const-string v0, "TouchPad"

    goto :goto_0

    .line 862
    :cond_1
    const/16 v1, 0x38

    if-ne p1, v1, :cond_2

    .line 863
    const-string v0, "Keyboard"

    .line 865
    :cond_2
    :goto_0
    return-object v0
.end method

.method private postShowRejectConfirmDialog(I)V
    .locals 2
    .param p1, "type"    # I

    .line 1117
    invoke-static {}, Lcom/android/server/UiThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1120
    return-void
.end method

.method private processIdentity(I)V
    .locals 5
    .param p1, "identity"    # I

    .line 1068
    const/4 v0, 0x5

    const-string v1, "MiuiPadKeyboardManager"

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1101
    :pswitch_0
    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->postShowRejectConfirmDialog(I)V

    .line 1102
    const-string v2, "keyboard identity transfer error"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1103
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1104
    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V

    .line 1105
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 1106
    goto :goto_0

    .line 1095
    :pswitch_1
    const-string v2, "keyboard identity internal error"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1097
    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V

    .line 1098
    goto :goto_0

    .line 1085
    :pswitch_2
    iget v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I

    if-ge v4, v0, :cond_0

    .line 1086
    const-string v2, "keyboard identity need check again"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    const/16 v1, 0x1388

    invoke-direct {p0, v1, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V

    .line 1088
    goto :goto_0

    .line 1090
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1091
    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V

    .line 1092
    goto :goto_0

    .line 1077
    :pswitch_3
    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->postShowRejectConfirmDialog(I)V

    .line 1078
    const-string v2, "keyboard identity auth reject"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1080
    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V

    .line 1081
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 1082
    goto :goto_0

    .line 1070
    :pswitch_4
    const-string v4, "keyboard identity auth ok"

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V

    .line 1072
    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V

    .line 1073
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 1074
    nop

    .line 1109
    :goto_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    if-ne p1, v1, :cond_2

    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I

    if-ne v1, v0, :cond_2

    .line 1112
    :cond_1
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z

    .line 1114
    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private releaseWakeLock()V
    .locals 1

    .line 1216
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1217
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1219
    :cond_0
    return-void
.end method

.method private resetCalculateStatus()V
    .locals 3

    .line 1208
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z

    .line 1209
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    .line 1210
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 1211
    const/4 v0, 0x1

    aput v2, v1, v0

    .line 1212
    const/4 v0, 0x2

    aput v2, v1, v0

    .line 1213
    return-void
.end method

.method private setAuthState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .line 1021
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityPass:Z

    .line 1022
    return-void
.end method

.method private setBacklight(I)V
    .locals 3
    .param p1, "brightness"    # I

    .line 1247
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1248
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "value"

    int-to-byte v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 1249
    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V

    .line 1250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set keyboard backLight brightness "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1251
    return-void
.end method

.method private setKeyboardStatus()V
    .locals 2

    .line 1255
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    if-nez v0, :cond_0

    .line 1256
    const-string v0, "MiuiPadKeyboardManager"

    const-string v1, "Abandon Communicate with keyboard because screen"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    return-void

    .line 1260
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1261
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->getCapsLockStatus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setCapsLockLight(Z)V

    .line 1262
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V

    .line 1263
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->getMuteStatus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setMuteLight(Z)V

    goto :goto_0

    .line 1265
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setCapsLockLight(Z)V

    .line 1266
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V

    .line 1267
    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setMuteLight(Z)V

    .line 1269
    :goto_0
    return-void
.end method

.method public static shouldClearActivityInfoFlags()I
    .locals 1

    .line 914
    const/16 v0, 0x70

    return v0
.end method

.method private showRejectConfirmDialog(I)V
    .locals 5
    .param p1, "type"    # I

    .line 1128
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 1130
    .local v0, "systemuiContext":Landroid/content/Context;
    sparse-switch p1, :sswitch_data_0

    .line 1140
    return-void

    .line 1136
    :sswitch_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1137
    const v2, 0x110f024c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1138
    .local v1, "message":Ljava/lang/String;
    goto :goto_0

    .line 1132
    .end local v1    # "message":Ljava/lang/String;
    :sswitch_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1133
    const v2, 0x110f024b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1134
    .restart local v1    # "message":Ljava/lang/String;
    nop

    .line 1143
    :goto_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v2, :cond_0

    .line 1144
    invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1146
    :cond_0
    new-instance v2, Lmiuix/appcompat/app/AlertDialog$Builder;

    const v3, 0x66110006

    invoke-direct {v2, v0, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1148
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 1149
    invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 1150
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1151
    const v4, 0x110f024a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1150
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v2

    .line 1153
    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    .line 1154
    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 1155
    .local v2, "attrs":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x7d3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1156
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v4, 0x20000

    or-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1157
    const/16 v3, 0x11

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1158
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v3, v3, 0x110

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 1160
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v3}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1162
    .end local v2    # "attrs":Landroid/view/WindowManager$LayoutParams;
    :goto_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->show()V

    .line 1163
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method

.method private startCheckIdentity(IZ)V
    .locals 2
    .param p1, "delay"    # I
    .param p2, "isFirst"    # Z

    .line 969
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    if-eqz v0, :cond_2

    .line 970
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stayAwake()V

    .line 971
    if-eqz p2, :cond_1

    .line 972
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z

    if-eqz v0, :cond_0

    .line 973
    return-void

    .line 975
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z

    .line 976
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I

    .line 978
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 979
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "is_first_check"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 980
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;I)V

    .line 982
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    return-void
.end method

.method private stayWakeForKeyboard176()V
    .locals 4

    .line 1232
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z

    if-nez v0, :cond_0

    .line 1233
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1234
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1235
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z

    .line 1237
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private stopAllCommandWorker()V
    .locals 3

    .line 1009
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1010
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopStayAwake()V

    .line 1011
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIICNodeHelper:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1012
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z

    .line 1013
    return-void
.end method

.method private stopWakeForKeyboard176()V
    .locals 2

    .line 1240
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z

    if-eqz v0, :cond_0

    .line 1241
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z

    .line 1242
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1244
    :cond_0
    return-void
.end method

.method public static supportPadKeyboard()Z
    .locals 2

    .line 189
    const-string/jumbo v0, "support_iic_keyboard"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private updateBackLightBrightness(I)V
    .locals 1
    .param p1, "brightness"    # I

    .line 696
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    if-eq v0, p1, :cond_0

    .line 697
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBluetoothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->wakeUpKeyboardIfNeed()V

    .line 698
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V

    .line 700
    :cond_0
    return-void
.end method

.method private updateLightSensor()V
    .locals 3

    .line 923
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLightSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 924
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 927
    :cond_0
    return-void
.end method


# virtual methods
.method public commandMiAuthStep3Type1([B[B)[B
    .locals 1
    .param p1, "keyMeta"    # [B
    .param p2, "challenge"    # [B

    .line 645
    invoke-static {p1, p2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->commandMiAuthStep3Type1ForIIC([B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public commandMiAuthStep5Type1([B)[B
    .locals 1
    .param p1, "token"    # [B

    .line 650
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->commandMiAuthStep5Type1ForIIC([B)[B

    move-result-object v0

    return-object v0
.end method

.method public commandMiDevAuthInit()[B
    .locals 1

    .line 640
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->commandMiDevAuthInitForIIC()[B

    move-result-object v0

    return-object v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 736
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 737
    const-string v0, "MIuiI2CKeyboardManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 739
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 740
    const-string v0, "mMcuVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 741
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mMCUVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 742
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 743
    const-string v0, "mKeyboardVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 744
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 745
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 746
    const-string v0, "mTouchPadVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 747
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchPadVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 748
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 749
    const-string v0, "AuthState="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 750
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityPass:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 751
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 752
    const-string v0, "mIsKeyboardReady="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 753
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 754
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 755
    const-string v0, "isKeyboardDisable="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 756
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 757
    const-string v0, "mAuthStarted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 758
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 759
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHasTouchPad:Z

    if-eqz v0, :cond_0

    .line 760
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 761
    const-string v0, "hasTouchPad="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 762
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHasTouchPad:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 763
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 764
    const-string v0, "backLightBrightness="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 765
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/padkeyboard/AngleStateController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 770
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 771
    return-void
.end method

.method public enableOrDisableInputDevice()V
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 377
    return-void
.end method

.method public getKeyboardBackLightBrightness()I
    .locals 1

    .line 704
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    return v0
.end method

.method public getKeyboardReportData()V
    .locals 1

    .line 371
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 372
    return-void
.end method

.method public getKeyboardStatus()Lmiui/hardware/input/MiuiKeyboardStatus;
    .locals 10

    .line 655
    new-instance v9, Lmiui/hardware/input/MiuiKeyboardStatus;

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 656
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getIdentityStatus()Z

    move-result v2

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 657
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->isWorkState()Z

    move-result v3

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 658
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getLidStatus()Z

    move-result v4

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 659
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getTabletStatus()Z

    move-result v5

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    .line 660
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z

    move-result v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mMCUVersion:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lmiui/hardware/input/MiuiKeyboardStatus;-><init>(ZZZZZZLjava/lang/String;Ljava/lang/String;)V

    .line 655
    return-object v9
.end method

.method public getKeyboardType()I
    .locals 2

    .line 1301
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mlock:Ljava/lang/Object;

    monitor-enter v0

    .line 1302
    :try_start_0
    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardType:I

    monitor-exit v0

    return v1

    .line 1303
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method isIICKeyboardActive()Z
    .locals 1

    .line 946
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isKeyboardReady()Z
    .locals 1

    .line 459
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public notifyLidSwitchChanged(Z)V
    .locals 2
    .param p1, "lidOpen"    # Z

    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lid Switch Changed to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyLidSwitchChanged(Z)V

    .line 306
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 307
    return-void
.end method

.method public notifyScreenState(Z)V
    .locals 5
    .param p1, "screenState"    # Z

    .line 327
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    if-ne v0, p1, :cond_0

    .line 328
    return-void

    .line 330
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Notify Screen State changed to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 333
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 336
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 337
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBluetoothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->notifyUpgradeIfNeed()V

    goto :goto_0

    .line 340
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V

    .line 344
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V

    .line 345
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    if-eqz v1, :cond_5

    .line 346
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v0, :cond_3

    .line 347
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateLightSensor()V

    .line 349
    :cond_3
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->supportCalculateAngle()Z

    move-result v0

    const-wide/16 v1, 0xc8

    if-eqz v0, :cond_4

    .line 350
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAccSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x3

    invoke-virtual {v0, p0, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 352
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda2;

    invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 354
    :cond_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda3;

    invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 359
    :cond_5
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z

    .line 360
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 361
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    .line 363
    :cond_6
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 364
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 365
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->resetCalculateStatus()V

    .line 367
    :goto_1
    return-void
.end method

.method public notifyTabletSwitchChanged(Z)V
    .locals 2
    .param p1, "tabletOpen"    # Z

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TabletSwitch changed to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyTabletSwitchChanged(Z)V

    .line 313
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 314
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/input/PadManager;->setIsTableOpen(Z)V

    .line 315
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 241
    return-void
.end method

.method public onHallStatusChanged(B)V
    .locals 4
    .param p1, "status"    # B

    .line 319
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->byte2int(B)I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 320
    .local v0, "lidStatus":Z
    :goto_0
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->byte2int(B)I

    move-result v3

    and-int/2addr v3, v2

    if-eqz v3, :cond_1

    move v1, v2

    .line 321
    .local v1, "tabletStatus":Z
    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->notifyLidSwitchChanged(Z)V

    .line 322
    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->notifyTabletSwitchChanged(Z)V

    .line 323
    return-void
.end method

.method public onKeyboardEnableStateChanged(Z)V
    .locals 2
    .param p1, "isEnable"    # Z

    .line 714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receiver keyboard enable status change to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAngleStateController:Lcom/android/server/input/padkeyboard/AngleStateController;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->setShouldIgnoreKeyboardFromKeyboard(Z)V

    .line 716
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V

    .line 717
    return-void
.end method

.method public onKeyboardGSensorChanged(FFF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .line 589
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardGData:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 590
    const/4 v1, 0x1

    aput p2, v0, v1

    .line 591
    const/4 v2, 0x2

    aput p3, v0, v2

    .line 592
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z

    .line 593
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->calculateAngle()V

    .line 594
    return-void
.end method

.method public onKeyboardSleepStatusChanged(Z)V
    .locals 2
    .param p1, "isSleep"    # Z

    .line 598
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z

    if-eq v0, p1, :cond_0

    .line 599
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z

    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Keyboard will sleep:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBluetoothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->setIICKeyboardSleepStatus(Z)V

    .line 602
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z

    if-nez v0, :cond_0

    .line 603
    iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V

    .line 606
    :cond_0
    return-void
.end method

.method public onKeyboardStatusChanged(IZ)V
    .locals 0
    .param p1, "connectionType"    # I
    .param p2, "connectionStatus"    # Z

    .line 709
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardStateChanged(IZ)V

    .line 710
    return-void
.end method

.method public onNFCTouched()V
    .locals 5

    .line 610
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z

    if-nez v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    const-string v4, "NFC Device Touched"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 614
    :cond_0
    return-void
.end method

.method public onOtaErrorInfo(BLjava/lang/String;)V
    .locals 2
    .param p1, "targetAddress"    # B
    .param p2, "reason"    # Ljava/lang/String;

    .line 551
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onOtaErrorInfo(BLjava/lang/String;ZI)V

    .line 552
    return-void
.end method

.method public onOtaErrorInfo(BLjava/lang/String;ZI)V
    .locals 3
    .param p1, "targetAddress"    # B
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "isNeedToast"    # Z
    .param p4, "resId"    # I

    .line 556
    const/4 v0, 0x0

    if-eqz p3, :cond_0

    const/4 v1, -0x1

    if-eq p4, v1, :cond_0

    .line 557
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    .line 558
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 557
    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 559
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 561
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->parseAddress2String(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OTA Fail because: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    if-eqz p2, :cond_4

    const-string v1, "The device version doesn\'t need upgrading"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Invalid upgrade file"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 563
    :cond_1
    const/16 v1, 0x38

    if-ne p1, v1, :cond_2

    .line 564
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    goto :goto_0

    .line 565
    :cond_2
    const/16 v1, 0x40

    if-ne p1, v1, :cond_3

    .line 566
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    goto :goto_0

    .line 567
    :cond_3
    const/16 v1, 0x18

    if-ne p1, v1, :cond_4

    .line 568
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z

    .line 571
    :cond_4
    :goto_0
    return-void
.end method

.method public onOtaProgress(BF)V
    .locals 2
    .param p1, "targetAddress"    # B
    .param p2, "pro"    # F

    .line 580
    const/high16 v0, 0x42480000    # 50.0f

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x42c80000    # 100.0f

    cmpl-float v0, p2, v0

    if-nez v0, :cond_1

    .line 581
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current upgrade progress\uff1a type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->parseAddress2String(B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", process: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_1
    return-void
.end method

.method public onOtaStateChange(BI)V
    .locals 7
    .param p1, "targetAddress"    # B
    .param p2, "status"    # I

    .line 494
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->parseAddress2String(B)Ljava/lang/String;

    move-result-object v0

    .line 495
    .local v0, "target":Ljava/lang/String;
    const/16 v1, 0x38

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x0

    if-nez p2, :cond_0

    .line 496
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OTA Start!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    if-ne p1, v1, :cond_0

    .line 498
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    .line 499
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x110f024e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 498
    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 501
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 505
    :cond_0
    const/4 v4, 0x2

    if-ne p2, v4, :cond_4

    .line 506
    if-ne p1, v1, :cond_1

    .line 507
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    .line 508
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x110f024f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 507
    invoke-static {v1, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 510
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 511
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z

    goto :goto_0

    .line 512
    :cond_1
    const/16 v1, 0x40

    if-ne p1, v1, :cond_2

    .line 513
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z

    goto :goto_0

    .line 514
    :cond_2
    const/16 v1, 0x18

    if-ne p1, v1, :cond_3

    .line 515
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z

    .line 517
    :cond_3
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " OTA Successfully!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :cond_4
    return-void
.end method

.method public onReadSocketNumError(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "read socket find num error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 194
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isKeyboardReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    return-void

    .line 198
    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_4

    .line 200
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v1

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->motionJudge(FFF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    .line 202
    return-void

    .line 205
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    aget v3, v3, v2

    sub-float/2addr v0, v3

    .line 206
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v3, 0x3e96872b    # 0.294f

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_2

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v1

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    aget v3, v3, v1

    sub-float/2addr v0, v3

    .line 207
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v3, 0x3e48b439

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_2

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    aget v3, v3, v5

    sub-float/2addr v0, v3

    .line 208
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v3, 0x3f16872b    # 0.588f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_7

    .line 210
    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mLocalGData:[F

    array-length v4, v3

    invoke-static {v0, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 211
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z

    .line 212
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isIICKeyboardActive()Z

    move-result v0

    if-nez v0, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->wakeKeyboard176()V

    .line 216
    :cond_3
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->calculateAngle()V

    goto :goto_0

    .line 218
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z

    if-nez v0, :cond_7

    .line 220
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getBackLightBrightnessWithSensor(F)I

    move-result v0

    .line 222
    .local v0, "brightness":I
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    if-eq v0, v1, :cond_7

    .line 223
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 224
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isIICKeyboardActive()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 225
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V

    goto :goto_0

    .line 228
    :cond_5
    iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I

    goto :goto_0

    .line 230
    :cond_6
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 232
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V

    .line 236
    .end local v0    # "brightness":I
    :cond_7
    :goto_0
    return-void
.end method

.method public onUpdateKeyboardType(BZ)V
    .locals 5
    .param p1, "type"    # B
    .param p2, "hasTouchPad"    # Z

    .line 484
    iput-boolean p2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHasTouchPad:Z

    .line 485
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 487
    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 488
    :cond_0
    move v2, v1

    :goto_0
    nop

    .line 485
    const-string v3, "keyboard_type_level"

    const/4 v4, -0x2

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 489
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mInputManager:Lcom/android/server/input/InputManagerService;

    const-string v2, "notifyIgnoredInputDevicesChanged"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    return-void
.end method

.method public onUpdateVersion(ILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "version"    # Ljava/lang/String;

    .line 464
    const/4 v0, 0x0

    .line 465
    .local v0, "device":Ljava/lang/String;
    const/16 v1, 0x14

    if-ne p1, v1, :cond_0

    .line 466
    const-string v0, "Keyboard"

    .line 467
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardVersion:Ljava/lang/String;

    .line 468
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->getKeyboardReportData()V

    goto :goto_0

    .line 469
    :cond_0
    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 470
    const-string v0, "Flash Keyboard"

    goto :goto_0

    .line 471
    :cond_1
    const/16 v1, 0xa

    if-ne p1, v1, :cond_2

    .line 472
    const-string v0, "Pad MCU"

    .line 473
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mMCUVersion:Ljava/lang/String;

    .line 474
    invoke-static {p2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->setMcuVersion(Ljava/lang/String;)V

    goto :goto_0

    .line 475
    :cond_2
    const/16 v1, 0x28

    if-ne p1, v1, :cond_3

    .line 476
    const-string v0, "TouchPad"

    .line 477
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchPadVersion:Ljava/lang/String;

    .line 479
    :cond_3
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVersion for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiPadKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return-void
.end method

.method public onWriteSocketErrorInfo(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .line 523
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Write Socket Fail because:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    const-string v0, "Write data Socket exception"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V

    .line 527
    :cond_0
    return-void
.end method

.method public readHallStatus()V
    .locals 1

    .line 626
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 627
    return-void
.end method

.method public readKeyboardStatus()V
    .locals 1

    .line 621
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V

    .line 622
    return-void
.end method

.method public registerMuteLightReceiver()V
    .locals 3

    .line 1279
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.action.MICROPHONE_MUTE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1280
    .local v0, "muteFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mMicMuteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1281
    return-void
.end method

.method public removeKeyboardDevicesIfNeeded([Landroid/view/InputDevice;)[Landroid/view/InputDevice;
    .locals 8
    .param p1, "allInputDevices"    # [Landroid/view/InputDevice;

    .line 245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v0, "tempDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/InputDevice;>;"
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBleDeviceList:Ljava/util/Set;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda6;

    invoke-direct {v3, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda6;-><init>(Ljava/util/Set;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 247
    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_9

    aget-object v4, p1, v3

    .line 248
    .local v4, "device":Landroid/view/InputDevice;
    invoke-virtual {v4}, Landroid/view/InputDevice;->getId()I

    move-result v5

    .line 249
    .local v5, "deviceId":I
    invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I

    move-result v6

    const/16 v7, 0x15d9

    if-ne v6, v7, :cond_6

    .line 250
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v6

    const/16 v7, 0xa3

    if-ne v6, v7, :cond_1

    .line 251
    iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    if-eq v6, v5, :cond_0

    .line 252
    iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I

    .line 254
    :cond_0
    iput-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDevice:Landroid/view/InputDevice;

    .line 255
    sget-object v6, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v6}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v6

    if-nez v6, :cond_8

    .line 256
    goto/16 :goto_1

    .line 258
    :cond_1
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v6

    const/16 v7, 0xa4

    if-ne v6, v7, :cond_3

    .line 260
    iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    if-eq v6, v5, :cond_2

    .line 261
    iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I

    .line 263
    :cond_2
    sget-object v6, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v6}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v6

    if-nez v6, :cond_8

    .line 264
    goto :goto_1

    .line 266
    :cond_3
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v6

    const/16 v7, 0xa1

    if-ne v6, v7, :cond_5

    .line 268
    iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    if-eq v6, v5, :cond_4

    .line 269
    iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I

    .line 271
    :cond_4
    sget-object v6, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v6}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v6

    if-nez v6, :cond_8

    .line 272
    goto :goto_1

    .line 274
    :cond_5
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v6

    const/16 v7, 0xa2

    if-ne v6, v7, :cond_8

    .line 276
    goto :goto_1

    .line 278
    :cond_6
    invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I

    move-result v6

    const v7, 0xbf01

    if-ne v6, v7, :cond_8

    .line 279
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v6

    const/16 v7, 0x40

    if-ne v6, v7, :cond_8

    .line 280
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda7;

    invoke-direct {v7, p0, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 281
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 282
    invoke-virtual {v4}, Landroid/view/InputDevice;->getSources()I

    move-result v6

    const/16 v7, 0x301

    and-int/2addr v6, v7

    if-eq v6, v7, :cond_8

    .line 285
    goto :goto_1

    .line 290
    :cond_7
    invoke-virtual {v4}, Landroid/view/InputDevice;->getSources()I

    move-result v6

    const/16 v7, 0x2303

    and-int/2addr v6, v7

    if-eq v6, v7, :cond_8

    .line 293
    goto :goto_1

    .line 297
    :cond_8
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    .end local v4    # "device":Landroid/view/InputDevice;
    .end local v5    # "deviceId":I
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 299
    :cond_9
    new-array v1, v2, [Landroid/view/InputDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/view/InputDevice;

    return-object v1
.end method

.method public requestReAuth()V
    .locals 2

    .line 731
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V

    .line 732
    return-void
.end method

.method public requestReleaseAwake()V
    .locals 0

    .line 726
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopStayAwake()V

    .line 727
    return-void
.end method

.method public requestStayAwake()V
    .locals 0

    .line 721
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stayAwake()V

    .line 722
    return-void
.end method

.method sendCommand(I)V
    .locals 1
    .param p1, "command"    # I

    .line 930
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V

    .line 931
    return-void
.end method

.method sendCommand(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "command"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .line 934
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;I)V

    .line 935
    return-void
.end method

.method sendCommand(ILandroid/os/Bundle;I)V
    .locals 4
    .param p1, "command"    # I
    .param p2, "data"    # Landroid/os/Bundle;
    .param p3, "delay"    # I

    .line 938
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 939
    .local v0, "message":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 940
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 941
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 942
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    int-to-long v2, p3

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 943
    return-void
.end method

.method public sendCommandForRespond([BLcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;)[B
    .locals 2
    .param p1, "command"    # [B
    .param p2, "callback"    # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;

    .line 631
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIICNodeHelper:Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->checkAuth([B)[B

    move-result-object v0

    .line 632
    .local v0, "result":[B
    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    invoke-interface {p2, v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback;->isCorrectPackage([B)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 633
    :cond_0
    return-object v0

    .line 635
    :cond_1
    const/4 v1, 0x0

    new-array v1, v1, [B

    return-object v1
.end method

.method public setCapsLockLight(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 666
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setCapsLight :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 668
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 669
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 670
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v1

    goto :goto_0

    .line 671
    :cond_0
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_CAPS_KEY_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v1

    :goto_0
    nop

    .line 672
    .local v1, "index":I
    const-string v2, "feature"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 673
    const/16 v2, 0xe

    invoke-virtual {p0, v2, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V

    .line 674
    return-void
.end method

.method public setKeyboardBackLightBrightness(I)V
    .locals 1
    .param p1, "brightness"    # I

    .line 691
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z

    .line 692
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateBackLightBrightness(I)V

    .line 693
    return-void
.end method

.method public setKeyboardType(I)V
    .locals 2
    .param p1, "type"    # I

    .line 1272
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mlock:Ljava/lang/Object;

    monitor-enter v0

    .line 1273
    :try_start_0
    iput p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardType:I

    .line 1274
    monitor-exit v0

    .line 1275
    return-void

    .line 1274
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setMuteLight(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 678
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMuteLight :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiPadKeyboardManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 680
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 681
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 682
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v1

    goto :goto_0

    .line 683
    :cond_0
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_MIC_MUTE_NEW:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v1

    :goto_0
    nop

    .line 684
    .local v1, "index":I
    const-string v2, "feature"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 685
    const/16 v2, 0xf

    invoke-virtual {p0, v2, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V

    .line 686
    return-void
.end method

.method public stayAwake()V
    .locals 2

    .line 543
    const-string v0, "MiuiPadKeyboardManager"

    const-string/jumbo v1, "stay awake"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mStopAwakeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 545
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda8;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 546
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 547
    return-void
.end method

.method public stopStayAwake()V
    .locals 4

    .line 539
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mStopAwakeRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 540
    return-void
.end method

.method public wakeKeyboard176()V
    .locals 2

    .line 1228
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->KB_POWER:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->writePadKeyBoardStatus(II)V

    .line 1229
    return-void
.end method

.method public writeCommandToIIC([B)V
    .locals 1
    .param p1, "command"    # [B

    .line 919
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->writeSocketCmd([B)Z

    .line 920
    return-void
.end method

.method public writePadKeyBoardStatus(II)V
    .locals 2
    .param p1, "target"    # I
    .param p2, "value"    # I

    .line 956
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 957
    .local v0, "data":Landroid/os/Bundle;
    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 958
    const-string v1, "feature"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 959
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V

    .line 960
    return-void
.end method
