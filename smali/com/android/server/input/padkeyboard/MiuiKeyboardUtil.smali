.class public Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;
.super Ljava/lang/Object;
.source "MiuiKeyboardUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil$KeyBoardShortcut;
    }
.end annotation


# static fields
.field public static final ARABIC:Ljava/lang/String; = "ar_EG"

.field private static final BRIGHTNESS_SETTINGS_RELATION:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final FRENCH:Ljava/lang/String; = "fr_FR"

.field public static final GERMAN:Ljava/lang/String; = "de_DE"

.field public static final ITALIA:Ljava/lang/String; = "it_IT"

.field public static final KEYBOARD_LAYOUT_ARABIC:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_arabic"

.field public static final KEYBOARD_LAYOUT_FRENCH:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_french"

.field public static final KEYBOARD_LAYOUT_GERMAN:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_german"

.field public static final KEYBOARD_LAYOUT_ITALIA:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_italian"

.field public static final KEYBOARD_LAYOUT_RUSSIAN:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_russian"

.field public static final KEYBOARD_LAYOUT_SPAINISH:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_spanish"

.field public static final KEYBOARD_LAYOUT_THAI:Ljava/lang/String; = "com.miui.miinput/com.miui.miinput.keyboardlayout.InputDeviceReceiver/keyboard_layout_thai"

.field public static final KEYBOARD_LAYOUT_UK:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"

.field public static final KEYBOARD_LAYOUT_US:Ljava/lang/String; = "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"

.field public static final KEYBOARD_LEVEL_HIGH:I = 0x200

.field public static final KEYBOARD_LEVEL_LOW:I = 0x100

.field public static final KEYBOARD_TYPE_LEVEL:Ljava/lang/String; = "keyboard_type_level"

.field public static final KEYBOARD_TYPE_LQ_BLE:B = 0x21t

.field public static final KEYBOARD_TYPE_LQ_HIGH:B = 0x20t

.field public static final KEYBOARD_TYPE_LQ_LOW:B = 0x2t

.field public static final KEYBOARD_TYPE_UCJ:B = 0x1t

.field public static final KEYBOARD_TYPE_UCJ_HIGH:B = 0x10t

.field public static final RUSSIA:Ljava/lang/String; = "ru_RU"

.field public static final SPAINISH:Ljava/lang/String; = "es_US"

.field private static final TAG:Ljava/lang/String; = "CommonUtil"

.field public static final THAI:Ljava/lang/String; = "th_TH"

.field public static final UK:Ljava/lang/String; = "en_GB"

.field public static final US:Ljava/lang/String; = "en_US"

.field public static final VALUE_KB_TYPE_HIGH:I = 0x1

.field public static final VALUE_KB_TYPE_LOW:I

.field private static final angle_cos:[F

.field private static final angle_sin:[F

.field private static mMcuVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->mMcuVersion:Ljava/lang/String;

    .line 78
    const/16 v0, 0x5b

    new-array v1, v0, [F

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_cos:[F

    .line 101
    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_sin:[F

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->BRIGHTNESS_SETTINGS_RELATION:Ljava/util/Map;

    .line 126
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3f19999a    # 0.6f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3f4ccccd    # 0.8f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3f333333    # 0.7f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3ecccccd    # 0.4f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3e99999a    # 0.3f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3e4ccccd    # 0.2f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x3dcccccd    # 0.1f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f7ff60a
        0x3f7fd817
        0x3f7fa637
        0x3f7f605b
        0x3f7f06a3
        0x3f7e98fe
        0x3f7e177f
        0x3f7d8234
        0x3f7cd91f
        0x3f7c1c61
        0x3f7b4be8
        0x3f7a67e8
        0x3f797050    # 0.97437f
        0x3f786552
        0x3f7746ed
        0x3f761544
        0x3f74d068
        0x3f737879
        0x3f720d88
        0x3f708fb8
        0x3f6eff19
        0x3f6d5bee
        0x3f6ba637
        0x3f69de16
        0x3f6803cd
        0x3f66175d
        0x3f641909
        0x3f6208e1
        0x3f5fe719
        0x3f5db3d0
        0x3f5b6f4c
        0x3f5919ac
        0x3f56b325
        0x3f543bd6
        0x3f51b3f2
        0x3f4f1bbd
        0x3f4c7358
        0x3f49bb17
        0x3f46f30a
        0x3f441b76
        0x3f4134ad
        0x3f3e3ec0
        0x3f3b3a04
        0x3f3826ab
        0x3f3504f7
        0x3f31d51b
        0x3f2e976c
        0x3f2b4c2b
        0x3f27f37c
        0x3f248dc1
        0x3f211b1e
        0x3f1d9bf6
        0x3f1a108c
        0x3f167914
        0x3f12d5e0
        0x3f0f2746
        0x3f0b6d76
        0x3f07a8c6
        0x3f03d988
        0x3f000000    # 0.5f
        0x3ef83904    # 0.48481f
        0x3ef05ea2
        0x3ee87182
        0x3ee0722a
        0x3ed86163
        0x3ed03fd5    # 0.406737f
        0x3ec80de5
        0x3ebfcc7d
        0x3eb77c03
        0x3eaf1d3f    # 0.34202f
        0x3ea6b0d9
        0x3e9e377a
        0x3e95b1c8
        0x3e8d204b    # 0.275637f
        0x3e8483ed
        0x3e77ba67
        0x3e66598e    # 0.224951f
        0x3e54e6e2
        0x3e43636f
        0x3e31d0c8    # 0.173648f
        0x3e20303c    # 0.156434f
        0x3e0e835e
        0x3df99674
        0x3dd612c7
        0x3db27ed8
        0x3d8edc3c
        0x3d565e46    # 0.052336f
        0x3d0ef241    # 0.034899f
        0x3c8ef77f    # 0.017452f
        -0x80000000
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3c8ef77f    # 0.017452f
        0x3d0ef241    # 0.034899f
        0x3d565e46    # 0.052336f
        0x3d8edc3c
        0x3db27ed8
        0x3dd612c7
        0x3df99674
        0x3e0e835e
        0x3e20303c    # 0.156434f
        0x3e31d0c8    # 0.173648f
        0x3e43636f
        0x3e54e6e2
        0x3e66598e    # 0.224951f
        0x3e77ba67
        0x3e8483ed
        0x3e8d204b    # 0.275637f
        0x3e95b1c8
        0x3e9e377a
        0x3ea6b0d9
        0x3eaf1d3f    # 0.34202f
        0x3eb77c03
        0x3ebfcc7d
        0x3ec80de5
        0x3ed03fd5    # 0.406737f
        0x3ed86163
        0x3ee0722a
        0x3ee87182
        0x3ef05ea2
        0x3ef83904    # 0.48481f
        0x3f000000    # 0.5f
        0x3f03d988
        0x3f07a8c6
        0x3f0b6d76
        0x3f0f2746
        0x3f12d5e0
        0x3f167914
        0x3f1a108c
        0x3f1d9c06
        0x3f211b1e
        0x3f248dc1
        0x3f27f37c
        0x3f2b4c2b
        0x3f2e976c
        0x3f31d51b
        0x3f3504f7
        0x3f3826ab
        0x3f3b3a04
        0x3f3e3ec0
        0x3f4134ad
        0x3f441b76
        0x3f46f30a
        0x3f49bb17
        0x3f4c7369
        0x3f4f1bbd
        0x3f51b3f2
        0x3f543bd6
        0x3f56b325
        0x3f5919ac
        0x3f5b6f4c
        0x3f5db3d0
        0x3f5fe719
        0x3f6208e1
        0x3f641909
        0x3f66175d
        0x3f6803cd
        0x3f69de16
        0x3f6ba637
        0x3f6d5bee
        0x3f6eff19
        0x3f708fb8
        0x3f720d88
        0x3f737879
        0x3f74d068
        0x3f761544
        0x3f7746ed
        0x3f786552
        0x3f797050    # 0.97437f
        0x3f7a67e8
        0x3f7b4be8
        0x3f7c1c61
        0x3f7cd91f
        0x3f7d8234
        0x3f7e177f
        0x3f7e98fe
        0x3f7f06a3
        0x3f7f605b
        0x3f7fa637
        0x3f7fd817
        0x3f7ff60a
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    return-void
.end method

.method public static Bytes2Hex([BI)Ljava/lang/String;
    .locals 1
    .param p0, "data"    # [B
    .param p1, "len"    # I

    .line 274
    const-string v0, ","

    invoke-static {p0, p1, v0}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2HexString([BILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static Bytes2HexString([BILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "data"    # [B
    .param p1, "len"    # I
    .param p2, "s"    # Ljava/lang/String;

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 279
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    array-length v1, p0

    if-le p1, v1, :cond_0

    .line 280
    array-length p1, p0

    .line 282
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 283
    aget-byte v2, p0, v1

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 285
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static Bytes2RevertHexString([B)Ljava/lang/String;
    .locals 6
    .param p0, "data"    # [B

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 290
    .local v0, "hexBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 291
    aget-byte v2, p0, v1

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 293
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "hexStr":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 295
    .local v2, "result":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 296
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    sub-int/2addr v4, v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v5, v3

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 298
    .end local v3    # "i":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static Bytes2String([B)Ljava/lang/String;
    .locals 2
    .param p0, "data"    # [B

    .line 307
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 311
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    const-string v0, ""

    return-object v0
.end method

.method public static Hex2String(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "hex"    # Ljava/lang/String;

    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 320
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 321
    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 322
    .local v2, "output":Ljava/lang/String;
    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 323
    .local v3, "decimal":I
    int-to-char v4, v3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 320
    .end local v2    # "output":Ljava/lang/String;
    .end local v3    # "decimal":I
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 325
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static String2HexString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .line 333
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 334
    .local v0, "chars":[C
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 335
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 336
    aget-char v3, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 338
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static byte2int(B)I
    .locals 1
    .param p0, "data"    # B

    .line 375
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static calculatePKAngle(FFFF)F
    .locals 15
    .param p0, "x1"    # F
    .param p1, "x2"    # F
    .param p2, "z1"    # F
    .param p3, "z2"    # F

    .line 171
    mul-float v0, p0, p1

    .line 173
    .local v0, "quadrant":F
    mul-float v1, p0, p0

    mul-float v2, p2, p2

    add-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v1, v1

    div-float v1, p2, v1

    .line 174
    .local v1, "cos1":F
    mul-float v2, p1, p1

    mul-float v3, p3, p3

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v2, p3, v2

    .line 177
    .local v2, "cos2":F
    float-to-double v3, v1

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    const-wide v7, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v3, v3, v7

    const-wide v9, 0x400921fb54442d18L    # Math.PI

    const-wide v11, 0x4066800000000000L    # 180.0

    if-gez v3, :cond_0

    .line 178
    const/4 v3, 0x0

    .local v3, "angle1":F
    goto :goto_0

    .line 179
    .end local v3    # "angle1":F
    :cond_0
    float-to-double v3, v1

    add-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    cmpg-double v3, v3, v7

    if-gez v3, :cond_1

    .line 180
    const/high16 v3, 0x43340000    # 180.0f

    .restart local v3    # "angle1":F
    goto :goto_0

    .line 182
    .end local v3    # "angle1":F
    :cond_1
    float-to-double v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->acos(D)D

    move-result-wide v3

    mul-double/2addr v3, v11

    div-double/2addr v3, v9

    double-to-float v3, v3

    .line 184
    .restart local v3    # "angle1":F
    :goto_0
    float-to-double v13, v2

    sub-double/2addr v13, v5

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v13

    cmpg-double v4, v13, v7

    if-gez v4, :cond_2

    .line 185
    const/4 v4, 0x0

    .local v4, "angle2":F
    goto :goto_1

    .line 186
    .end local v4    # "angle2":F
    :cond_2
    float-to-double v13, v2

    add-double/2addr v13, v5

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpg-double v4, v4, v7

    if-gez v4, :cond_3

    .line 187
    const/high16 v4, 0x43340000    # 180.0f

    .restart local v4    # "angle2":F
    goto :goto_1

    .line 189
    .end local v4    # "angle2":F
    :cond_3
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    mul-double/2addr v4, v11

    div-double/2addr v4, v9

    double-to-float v4, v4

    .line 191
    .restart local v4    # "angle2":F
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "angle1 = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " angle2 = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "CommonUtil"

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    sub-float v5, v3, v4

    .line 195
    .local v5, "angleOffset":F
    add-float v7, v3, v4

    .line 196
    .local v7, "angleSum":F
    const/high16 v8, 0x43340000    # 180.0f

    sub-float v9, v8, v4

    sub-float v9, v3, v9

    .line 198
    .local v9, "angleOffset2":F
    const/4 v10, 0x0

    cmpl-float v11, v0, v10

    if-lez v11, :cond_7

    .line 199
    cmpg-float v11, p0, v10

    if-gez v11, :cond_4

    cmpl-float v11, v5, v10

    if-gtz v11, :cond_5

    :cond_4
    cmpl-float v11, p0, v10

    if-lez v11, :cond_6

    cmpg-float v10, v5, v10

    if-gez v10, :cond_6

    .line 200
    :cond_5
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v10

    sub-float/2addr v8, v10

    .local v8, "resultAngle":F
    goto :goto_2

    .line 202
    .end local v8    # "resultAngle":F
    :cond_6
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v10

    add-float/2addr v8, v10

    .restart local v8    # "resultAngle":F
    goto :goto_2

    .line 205
    .end local v8    # "resultAngle":F
    :cond_7
    cmpg-float v11, p0, v10

    if-gez v11, :cond_9

    .line 206
    cmpl-float v10, v9, v10

    if-lez v10, :cond_8

    .line 207
    const/high16 v10, 0x43b40000    # 360.0f

    sub-float v8, v7, v8

    sub-float v8, v10, v8

    .restart local v8    # "resultAngle":F
    goto :goto_2

    .line 209
    .end local v8    # "resultAngle":F
    :cond_8
    sub-float/2addr v8, v7

    .restart local v8    # "resultAngle":F
    goto :goto_2

    .line 212
    .end local v8    # "resultAngle":F
    :cond_9
    cmpl-float v10, v9, v10

    if-lez v10, :cond_a

    .line 213
    const/high16 v8, -0x3ccc0000    # -180.0f

    add-float/2addr v8, v7

    .restart local v8    # "resultAngle":F
    goto :goto_2

    .line 215
    .end local v8    # "resultAngle":F
    :cond_a
    add-float/2addr v8, v7

    .line 219
    .restart local v8    # "resultAngle":F
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "result Angle = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    return v8
.end method

.method public static calculatePKAngleV2(FFFFFF)I
    .locals 10
    .param p0, "kX"    # F
    .param p1, "kY"    # F
    .param p2, "kZ"    # F
    .param p3, "pX"    # F
    .param p4, "pY"    # F
    .param p5, "pZ"    # F

    .line 225
    mul-float v0, p0, p0

    mul-float v1, p1, p1

    add-float/2addr v0, v1

    mul-float v1, p2, p2

    add-float/2addr v0, v1

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->invSqrt(F)F

    move-result v0

    .line 226
    .local v0, "recipNorm":F
    mul-float/2addr p0, v0

    .line 227
    mul-float/2addr p1, v0

    .line 228
    mul-float/2addr p2, v0

    .line 230
    mul-float v1, p3, p3

    mul-float v2, p4, p4

    add-float/2addr v1, v2

    mul-float v2, p5, p5

    add-float/2addr v1, v2

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->invSqrt(F)F

    move-result v0

    .line 231
    mul-float/2addr p3, v0

    .line 232
    mul-float/2addr p4, v0

    .line 233
    mul-float/2addr p5, v0

    .line 235
    const/4 v1, 0x0

    .line 236
    .local v1, "deviation1":F
    const/4 v2, 0x0

    .line 237
    .local v2, "deviation2":F
    const/high16 v3, 0x42c80000    # 100.0f

    .line 238
    .local v3, "min_deviation":F
    const/4 v4, 0x0

    .line 240
    .local v4, "min_deviation_angle":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    const/16 v6, 0x168

    if-gt v5, v6, :cond_4

    .line 243
    const/16 v6, 0x5a

    if-gt v5, v6, :cond_0

    .line 244
    sget-object v6, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_cos:[F

    aget v6, v6, v5

    mul-float v7, v6, p0

    sget-object v8, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_sin:[F

    aget v8, v8, v5

    mul-float v9, v8, p2

    sub-float/2addr v7, v9

    add-float/2addr v7, p3

    .line 245
    .end local v1    # "deviation1":F
    .local v7, "deviation1":F
    mul-float/2addr v6, p2

    mul-float/2addr v8, p0

    add-float/2addr v6, v8

    add-float/2addr v6, p5

    move v2, v6

    move v1, v7

    .end local v2    # "deviation2":F
    .local v6, "deviation2":F
    goto/16 :goto_1

    .line 246
    .end local v6    # "deviation2":F
    .end local v7    # "deviation1":F
    .restart local v1    # "deviation1":F
    .restart local v2    # "deviation2":F
    :cond_0
    const/16 v7, 0xb4

    if-le v5, v6, :cond_1

    if-gt v5, v7, :cond_1

    .line 247
    sget-object v6, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_cos:[F

    rsub-int v7, v5, 0xb4

    aget v7, v6, v7

    neg-float v7, v7

    mul-float/2addr v7, p0

    sget-object v8, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_sin:[F

    rsub-int v9, v5, 0xb4

    aget v9, v8, v9

    mul-float/2addr v9, p2

    sub-float/2addr v7, v9

    add-float/2addr v7, p3

    .line 248
    .end local v1    # "deviation1":F
    .restart local v7    # "deviation1":F
    rsub-int v1, v5, 0xb4

    aget v1, v6, v1

    neg-float v1, v1

    mul-float/2addr v1, p2

    rsub-int v6, v5, 0xb4

    aget v6, v8, v6

    mul-float/2addr v6, p0

    add-float/2addr v1, v6

    add-float/2addr v1, p5

    move v2, v1

    move v1, v7

    .end local v2    # "deviation2":F
    .local v1, "deviation2":F
    goto :goto_1

    .line 249
    .end local v7    # "deviation1":F
    .local v1, "deviation1":F
    .restart local v2    # "deviation2":F
    :cond_1
    if-le v5, v7, :cond_2

    const/16 v6, 0x10e

    if-gt v5, v6, :cond_2

    .line 250
    sget-object v6, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_cos:[F

    add-int/lit16 v7, v5, -0xb4

    aget v7, v6, v7

    neg-float v7, v7

    mul-float/2addr v7, p0

    sget-object v8, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_sin:[F

    add-int/lit16 v9, v5, -0xb4

    aget v9, v8, v9

    neg-float v9, v9

    mul-float/2addr v9, p2

    sub-float/2addr v7, v9

    add-float/2addr v7, p3

    .line 251
    .end local v1    # "deviation1":F
    .restart local v7    # "deviation1":F
    add-int/lit16 v1, v5, -0xb4

    aget v1, v6, v1

    neg-float v1, v1

    mul-float/2addr v1, p2

    add-int/lit16 v6, v5, -0xb4

    aget v6, v8, v6

    neg-float v6, v6

    mul-float/2addr v6, p0

    add-float/2addr v1, v6

    add-float/2addr v1, p5

    move v2, v1

    move v1, v7

    .end local v2    # "deviation2":F
    .local v1, "deviation2":F
    goto :goto_1

    .line 253
    .end local v7    # "deviation1":F
    .local v1, "deviation1":F
    .restart local v2    # "deviation2":F
    :cond_2
    sget-object v6, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_cos:[F

    rsub-int v7, v5, 0x168

    aget v7, v6, v7

    mul-float/2addr v7, p0

    sget-object v8, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->angle_sin:[F

    rsub-int v9, v5, 0x168

    aget v9, v8, v9

    neg-float v9, v9

    mul-float/2addr v9, p2

    sub-float/2addr v7, v9

    add-float/2addr v7, p3

    .line 254
    .end local v1    # "deviation1":F
    .restart local v7    # "deviation1":F
    rsub-int v1, v5, 0x168

    aget v1, v6, v1

    mul-float/2addr v1, p2

    rsub-int v6, v5, 0x168

    aget v6, v8, v6

    neg-float v6, v6

    mul-float/2addr v6, p0

    add-float/2addr v1, v6

    add-float/2addr v1, p5

    move v2, v1

    move v1, v7

    .line 256
    .end local v7    # "deviation1":F
    .restart local v1    # "deviation1":F
    :goto_1
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    add-float/2addr v6, v7

    cmpg-float v6, v6, v3

    if-gez v6, :cond_3

    .line 257
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    add-float/2addr v6, v7

    .line 258
    .end local v3    # "min_deviation":F
    .local v6, "min_deviation":F
    move v3, v5

    move v4, v3

    move v3, v6

    .line 240
    .end local v6    # "min_deviation":F
    .restart local v3    # "min_deviation":F
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 263
    .end local v5    # "i":I
    :cond_4
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    goto :goto_2

    :cond_5
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 265
    .local v5, "accel_angle_error":F
    :goto_2
    const v6, 0x3f7ae148    # 0.98f

    cmpl-float v6, v5, v6

    if-lez v6, :cond_6

    const/4 v6, -0x1

    goto :goto_3

    :cond_6
    move v6, v4

    :goto_3
    return v6
.end method

.method public static declared-synchronized checkSum([BIIB)Z
    .locals 3
    .param p0, "data"    # [B
    .param p1, "start"    # I
    .param p2, "length"    # I
    .param p3, "sum"    # B

    const-class v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;

    monitor-enter v0

    .line 151
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    .local v1, "dsum":B
    if-ne v1, p3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    monitor-exit v0

    return v2

    .line 150
    .end local v1    # "dsum":B
    .end local p0    # "data":[B
    .end local p1    # "start":I
    .end local p2    # "length":I
    .end local p3    # "sum":B
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static commandMiAuthStep3Type1ForIIC([B[B)[B
    .locals 6
    .param p0, "keyMeta"    # [B
    .param p1, "challenge"    # [B

    .line 402
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 403
    .local v0, "temp":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 404
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 405
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 406
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 407
    const/16 v1, 0x4f

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 408
    const/4 v1, 0x5

    const/16 v4, 0x31

    aput-byte v4, v0, v1

    .line 409
    const/4 v1, 0x6

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 410
    const/4 v1, 0x7

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 411
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP3:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->getCommand()B

    move-result v1

    const/16 v4, 0x8

    aput-byte v1, v0, v4

    .line 412
    const/16 v1, 0x9

    const/16 v4, 0x14

    aput-byte v4, v0, v1

    .line 414
    if-eqz p0, :cond_0

    array-length v1, p0

    if-ne v1, v3, :cond_0

    .line 415
    const/16 v1, 0xa

    invoke-static {p0, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 417
    :cond_0
    const/16 v1, 0xe

    if-eqz p1, :cond_1

    array-length v4, p1

    const/16 v5, 0x10

    if-ne v4, v5, :cond_1

    .line 418
    invoke-static {p1, v2, v0, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 419
    const/16 v1, 0x1b

    invoke-static {v0, v3, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v1

    const/16 v2, 0x1e

    aput-byte v1, v0, v2

    goto :goto_0

    .line 421
    :cond_1
    const/16 v2, 0xb

    invoke-static {v0, v3, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 423
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send 3-1 auth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CommonUtil"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    return-object v0
.end method

.method public static commandMiAuthStep3Type1ForUSB([B[B)[B
    .locals 5
    .param p0, "keyMeta"    # [B
    .param p1, "challenge"    # [B

    .line 468
    const/16 v0, 0x1b

    new-array v0, v0, [B

    .line 469
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 470
    const/4 v1, 0x1

    const/16 v3, 0x31

    aput-byte v3, v0, v1

    .line 471
    const/4 v1, 0x2

    const/16 v3, -0x80

    aput-byte v3, v0, v1

    .line 472
    const/4 v1, 0x3

    const/16 v3, 0x38

    aput-byte v3, v0, v1

    .line 473
    const/16 v1, 0x32

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 474
    const/4 v1, 0x5

    const/16 v4, 0x14

    aput-byte v4, v0, v1

    .line 475
    if-eqz p0, :cond_0

    array-length v1, p0

    if-ne v1, v3, :cond_0

    .line 476
    const/4 v1, 0x6

    invoke-static {p0, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 478
    :cond_0
    if-eqz p1, :cond_1

    array-length v1, p1

    const/16 v3, 0x10

    if-ne v1, v3, :cond_1

    .line 479
    const/16 v1, 0xa

    invoke-static {p1, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 481
    :cond_1
    const/16 v1, 0x1a

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 482
    return-object v0
.end method

.method public static commandMiAuthStep5Type1ForIIC([B)[B
    .locals 6
    .param p0, "token"    # [B

    .line 428
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 429
    .local v0, "temp":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 430
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 431
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 432
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 433
    const/16 v1, 0x4f

    const/4 v3, 0x4

    aput-byte v1, v0, v3

    .line 434
    const/4 v1, 0x5

    const/16 v4, 0x31

    aput-byte v4, v0, v1

    .line 435
    const/4 v1, 0x6

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 436
    const/4 v1, 0x7

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 437
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_STEP5_1:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->getCommand()B

    move-result v1

    const/16 v4, 0x8

    aput-byte v1, v0, v4

    .line 438
    const/16 v1, 0x9

    const/16 v4, 0x10

    aput-byte v4, v0, v1

    .line 439
    const/16 v1, 0xa

    if-eqz p0, :cond_0

    array-length v5, p0

    if-ne v5, v4, :cond_0

    .line 440
    invoke-static {p0, v2, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 441
    const/16 v1, 0x1a

    invoke-static {v0, v3, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    goto :goto_0

    .line 443
    :cond_0
    invoke-static {v0, v3, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 445
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send 5-1 auth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CommonUtil"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    return-object v0
.end method

.method public static commandMiDevAuthInitForIIC()[B
    .locals 5

    .line 379
    const/16 v0, 0x44

    new-array v0, v0, [B

    .line 380
    .local v0, "temp":[B
    const/16 v1, -0x56

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 381
    const/4 v1, 0x1

    const/16 v3, 0x42

    aput-byte v3, v0, v1

    .line 382
    const/4 v1, 0x2

    const/16 v3, 0x32

    aput-byte v3, v0, v1

    .line 383
    const/4 v1, 0x3

    aput-byte v2, v0, v1

    .line 384
    const/16 v1, 0x4f

    const/4 v2, 0x4

    aput-byte v1, v0, v2

    .line 385
    const/4 v1, 0x5

    const/16 v3, 0x31

    aput-byte v3, v0, v1

    .line 386
    const/16 v1, -0x80

    const/4 v3, 0x6

    aput-byte v1, v0, v3

    .line 387
    const/4 v1, 0x7

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 388
    sget-object v1, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->AUTH_START:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$AUTH_COMMAND;->getCommand()B

    move-result v1

    const/16 v4, 0x8

    aput-byte v1, v0, v4

    .line 389
    const/16 v1, 0x9

    aput-byte v3, v0, v1

    .line 390
    const/16 v1, 0xa

    const/16 v3, 0x4d

    aput-byte v3, v0, v1

    .line 391
    const/16 v1, 0xb

    const/16 v3, 0x49

    aput-byte v3, v0, v1

    .line 392
    const/16 v1, 0xc

    const/16 v3, 0x41

    aput-byte v3, v0, v1

    .line 393
    const/16 v1, 0x55

    const/16 v3, 0xd

    aput-byte v1, v0, v3

    .line 394
    const/16 v1, 0xe

    const/16 v4, 0x54

    aput-byte v4, v0, v1

    .line 395
    const/16 v1, 0xf

    const/16 v4, 0x48

    aput-byte v4, v0, v1

    .line 396
    const/16 v1, 0x10

    invoke-static {v0, v2, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 397
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send init auth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CommonUtil"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    return-object v0
.end method

.method public static commandMiDevAuthInitForUSB()[B
    .locals 5

    .line 450
    const/16 v0, 0xd

    new-array v0, v0, [B

    .line 451
    .local v0, "bytes":[B
    const/16 v1, 0x4f

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 452
    const/4 v1, 0x1

    const/16 v3, 0x31

    aput-byte v3, v0, v1

    .line 453
    const/4 v1, 0x2

    const/16 v4, -0x80

    aput-byte v4, v0, v1

    .line 454
    const/4 v1, 0x3

    const/16 v4, 0x38

    aput-byte v4, v0, v1

    .line 455
    const/4 v1, 0x4

    aput-byte v3, v0, v1

    .line 456
    const/4 v1, 0x5

    const/4 v3, 0x6

    aput-byte v3, v0, v1

    .line 457
    const/16 v1, 0x4d

    aput-byte v1, v0, v3

    .line 458
    const/4 v1, 0x7

    const/16 v3, 0x49

    aput-byte v3, v0, v1

    .line 459
    const/16 v1, 0x8

    const/16 v3, 0x41

    aput-byte v3, v0, v1

    .line 460
    const/16 v1, 0x9

    const/16 v3, 0x55

    aput-byte v3, v0, v1

    .line 461
    const/16 v1, 0xa

    const/16 v3, 0x54

    aput-byte v3, v0, v1

    .line 462
    const/16 v1, 0xb

    const/16 v3, 0x48

    aput-byte v3, v0, v1

    .line 463
    const/16 v1, 0xc

    invoke-static {v0, v2, v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->getSum([BII)B

    move-result v2

    aput-byte v2, v0, v1

    .line 464
    return-object v0
.end method

.method public static getBackLightBrightnessWithSensor(F)I
    .locals 3
    .param p0, "brightness"    # F

    .line 486
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/lang/Math;->max(FF)F

    move-result p0

    .line 487
    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {p0, v0}, Ljava/lang/Math;->min(FF)F

    move-result p0

    .line 488
    const/high16 v0, 0x40a00000    # 5.0f

    div-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 489
    .local v0, "level":I
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 490
    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 491
    sget-object v1, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->BRIGHTNESS_SETTINGS_RELATION:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 493
    .local v1, "result":F
    const/high16 v2, 0x3f000000    # 0.5f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    sget-object v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    .line 494
    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 495
    const/high16 v1, 0x3f000000    # 0.5f

    .line 497
    :cond_0
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    return v2
.end method

.method public static declared-synchronized getSum([BII)B
    .locals 4
    .param p0, "data"    # [B
    .param p1, "start"    # I
    .param p2, "length"    # I

    const-class v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;

    monitor-enter v0

    .line 143
    const/4 v1, 0x0

    .line 144
    .local v1, "sum":B
    move v2, p1

    .local v2, "i":I
    :goto_0
    add-int v3, p1, p2

    if-ge v2, v3, :cond_0

    .line 145
    :try_start_0
    aget-byte v3, p0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v3, v1

    int-to-byte v1, v3

    .line 144
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 142
    .end local v1    # "sum":B
    .end local v2    # "i":I
    .end local p0    # "data":[B
    .end local p1    # "start":I
    .end local p2    # "length":I
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    .line 147
    .restart local v1    # "sum":B
    .restart local p0    # "data":[B
    .restart local p1    # "start":I
    .restart local p2    # "length":I
    :cond_0
    monitor-exit v0

    return v1
.end method

.method public static getYearMonthDayByTimestamp(J)[I
    .locals 5
    .param p0, "timestamp"    # J

    .line 356
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 357
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 358
    const/4 v1, 0x3

    new-array v1, v1, [I

    .line 359
    .local v1, "ret":[I
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x0

    aput v3, v1, v4

    .line 360
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/2addr v4, v2

    aput v4, v1, v2

    .line 361
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    aput v2, v1, v3

    .line 362
    return-object v1
.end method

.method public static hasTouchpad(B)Z
    .locals 1
    .param p0, "type"    # B

    .line 514
    const/16 v0, 0x20

    if-eq p0, v0, :cond_1

    const/16 v0, 0x21

    if-eq p0, v0, :cond_1

    const/16 v0, 0x10

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public static int2Bytes(I)[B
    .locals 4
    .param p0, "data"    # I

    .line 342
    const/4 v0, 0x4

    new-array v1, v0, [B

    .line 343
    .local v1, "b":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 344
    mul-int/lit8 v3, v2, 0x8

    rsub-int/lit8 v3, v3, 0x18

    shr-int v3, p0, v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 343
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 346
    .end local v2    # "i":I
    :cond_0
    return-object v1
.end method

.method public static interceptShortCutKeyIfCustomDefined(Landroid/content/Context;Landroid/view/KeyEvent;Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "stage"    # Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;

    .line 661
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 662
    .local v0, "keyCode":I
    sget-object v1, Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;->BEFORE_DISPATCHING:Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;

    const/4 v2, 0x0

    if-ne p2, v1, :cond_7

    .line 663
    int-to-long v3, v0

    .line 664
    .local v3, "shortCutKey":J
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x32

    if-eqz v1, :cond_2

    .line 665
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_0

    .line 666
    const-wide v5, 0x2000000000L

    or-long/2addr v3, v5

    goto :goto_0

    .line 667
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_1

    .line 668
    const-wide v5, 0x1000000000L

    or-long/2addr v3, v5

    .line 670
    :cond_1
    :goto_0
    const-wide v5, 0x200000000L

    or-long/2addr v3, v5

    .line 672
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isMetaPressed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 673
    const-wide/high16 v5, 0x1000000000000L

    or-long/2addr v3, v5

    .line 675
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 676
    const-wide v5, 0x100000000000L

    or-long/2addr v3, v5

    .line 678
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 679
    const-wide v5, 0x100000000L

    or-long/2addr v3, v5

    .line 681
    :cond_5
    nop

    .line 682
    invoke-static {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getMiuiKeyboardShortcutInfo()Landroid/util/LongSparseArray;

    move-result-object v1

    .line 683
    .local v1, "shortCutInfos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    if-eqz v1, :cond_7

    .line 684
    invoke-virtual {v1, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v1, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v2, 0x1

    :cond_6
    return v2

    .line 687
    .end local v1    # "shortCutInfos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    .end local v3    # "shortCutKey":J
    :cond_7
    return v2
.end method

.method public static invSqrt(F)F
    .locals 4
    .param p0, "num"    # F

    .line 156
    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, p0

    .line 157
    .local v0, "xHalf":F
    invoke-static {p0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 158
    .local v1, "temp":I
    shr-int/lit8 v2, v1, 0x1

    const v3, 0x5f3759df

    sub-int/2addr v3, v2

    .line 159
    .end local v1    # "temp":I
    .local v3, "temp":I
    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result p0

    .line 160
    mul-float v1, v0, p0

    mul-float/2addr v1, p0

    const/high16 v2, 0x3fc00000    # 1.5f

    sub-float/2addr v2, v1

    mul-float/2addr p0, v2

    .line 161
    return p0
.end method

.method public static isKeyboardSupportMuteLight(I)Z
    .locals 2
    .param p0, "type"    # I

    .line 508
    const/16 v0, 0x21

    const/4 v1, 0x1

    if-eq p0, v0, :cond_1

    if-eq p0, v1, :cond_1

    const/16 v0, 0x10

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public static isXM2022MCU()Z
    .locals 2

    .line 692
    sget-object v0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->mMcuVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v1, "XM2022"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static notifySettingsKeyboardStatusChanged(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "data"    # Landroid/os/Bundle;

    .line 501
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.input.action.keyboard.KEYBOARD_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 502
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.securitycore"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 504
    sget-object v1, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 505
    return-void
.end method

.method public static operationWait(I)V
    .locals 2
    .param p0, "ms"    # I

    .line 367
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    int-to-long v0, p0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 370
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 372
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method

.method public static setMcuVersion(Ljava/lang/String;)V
    .locals 0
    .param p0, "version"    # Ljava/lang/String;

    .line 696
    sput-object p0, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->mMcuVersion:Ljava/lang/String;

    .line 697
    return-void
.end method

.method public static supportCalculateAngle()Z
    .locals 4

    .line 701
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    .line 702
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    .line 704
    .local v0, "iicConnected":Z
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    .line 705
    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    .line 706
    .local v1, "bleConnected":Z
    :goto_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :cond_2
    :goto_1
    return v2
.end method
