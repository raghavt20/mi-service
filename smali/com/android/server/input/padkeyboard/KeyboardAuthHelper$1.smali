.class Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;
.super Ljava/lang/Object;
.source "KeyboardAuthHelper.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    .line 148
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 151
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Lcom/xiaomi/devauth/IMiDevAuthInterface;

    move-result-object v0

    if-nez v0, :cond_0

    .line 152
    return-void

    .line 154
    :cond_0
    const-string v0, "MiuiKeyboardManager_MiDevAuthService"

    const-string v1, "binderDied, unlink service"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Lcom/xiaomi/devauth/IMiDevAuthInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmDeathRecipient(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/os/IBinder$DeathRecipient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 156
    return-void
.end method
