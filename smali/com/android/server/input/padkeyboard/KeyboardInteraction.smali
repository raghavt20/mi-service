.class public final enum Lcom/android/server/input/padkeyboard/KeyboardInteraction;
.super Ljava/lang/Enum;
.source "KeyboardInteraction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/input/padkeyboard/KeyboardInteraction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/input/padkeyboard/KeyboardInteraction;

.field public static final CONNECTION_TYPE_BLE:I = 0x1

.field public static final CONNECTION_TYPE_IIC:I = 0x0

.field public static final enum INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

.field private static final TAG:Ljava/lang/String; = "KeyboardInteraction"


# instance fields
.field private mBlueToothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

.field private final mHandler:Landroid/os/Handler;

.field private mIsConnectBle:Z

.field private mIsConnectIIC:Z

.field private mListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mMiuiIICKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;


# direct methods
.method public static synthetic $r8$lambda$5s3u0GR84lFKkvt49IgiZvhZGPo(Lcom/android/server/input/padkeyboard/KeyboardInteraction;Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->lambda$setConnectBleLocked$0(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V

    return-void
.end method

.method public static synthetic $r8$lambda$m5xpdPToEv3VVjhiHqL0Ew5I-uk(Lcom/android/server/input/padkeyboard/KeyboardInteraction;Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->lambda$setConnectIICLocked$1(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V

    return-void
.end method

.method private static synthetic $values()[Lcom/android/server/input/padkeyboard/KeyboardInteraction;
    .locals 1

    .line 13
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    filled-new-array {v0}, [Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 14
    new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    const-string v1, "INTERACTION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    .line 13
    invoke-static {}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->$values()[Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->$VALUES:[Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    .line 23
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mListenerList:Ljava/util/List;

    .line 28
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mHandler:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method private synthetic lambda$setConnectBleLocked$0(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;

    .line 57
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    invoke-interface {p1, v0, v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;->onKeyboardStatusChanged(IZ)V

    return-void
.end method

.method private synthetic lambda$setConnectIICLocked$1(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;

    .line 80
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    invoke-interface {p1, v0, v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;->onKeyboardStatusChanged(IZ)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/KeyboardInteraction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 13
    const-class v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    return-object v0
.end method

.method public static values()[Lcom/android/server/input/padkeyboard/KeyboardInteraction;
    .locals 1

    .line 13
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->$VALUES:[Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, [Lcom/android/server/input/padkeyboard/KeyboardInteraction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    return-object v0
.end method


# virtual methods
.method public addListener(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;

    .line 32
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 33
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    :cond_0
    monitor-exit v0

    .line 37
    return-void

    .line 36
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public communicateWithIIC([B)V
    .locals 1
    .param p1, "command"    # [B

    .line 123
    if-nez p1, :cond_0

    .line 124
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mMiuiIICKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->writeCommandToIIC([B)V

    .line 127
    return-void
.end method

.method public communicateWithKeyboardLocked([B)V
    .locals 4
    .param p1, "command"    # [B

    .line 107
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 108
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    .line 109
    .local v1, "iicConnected":Z
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    .line 110
    .local v2, "bleConnected":Z
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mMiuiIICKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->writeCommandToIIC([B)V

    goto :goto_0

    .line 114
    :cond_0
    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mBlueToothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    if-eqz v0, :cond_1

    .line 115
    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->writeDataToBleDevice([B)V

    goto :goto_0

    .line 117
    :cond_1
    const-string v0, "KeyboardInteraction"

    const-string v3, "Can\'t communicate with keyboard because no connection"

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :goto_0
    return-void

    .line 110
    .end local v1    # "iicConnected":Z
    .end local v2    # "bleConnected":Z
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public haveConnection()Z
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 131
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    monitor-exit v0

    return v1

    .line 132
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isConnectBleLocked()Z
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 64
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    monitor-exit v0

    return v1

    .line 65
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isConnectIICLocked()Z
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 87
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    monitor-exit v0

    return v1

    .line 88
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeListener(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;

    .line 40
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 42
    monitor-exit v0

    .line 43
    return-void

    .line 42
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setBlueToothKeyboardManagerLocked(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)V
    .locals 2
    .param p1, "blueToothKeyboardManager"    # Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    .line 93
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 94
    :try_start_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mBlueToothKeyboardManager:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    .line 95
    monitor-exit v0

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setConnectBleLocked(Z)V
    .locals 5
    .param p1, "connectBle"    # Z

    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "needNotify":Z
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 48
    :try_start_0
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    if-eq v2, p1, :cond_0

    .line 49
    const/4 v0, 0x1

    .line 50
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    .line 52
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    if-eqz v0, :cond_1

    .line 54
    const-string v1, "KeyboardInteraction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notify Keyboard Ble Connection is Changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectBle:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mListenerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;

    .line 56
    .local v2, "listener":Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/server/input/padkeyboard/KeyboardInteraction$$ExternalSyntheticLambda0;

    invoke-direct {v4, p0, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/KeyboardInteraction;Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 58
    .end local v2    # "listener":Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;
    goto :goto_0

    .line 60
    :cond_1
    return-void

    .line 52
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public setConnectIICLocked(Z)V
    .locals 5
    .param p1, "connectIIC"    # Z

    .line 69
    const/4 v0, 0x0

    .line 70
    .local v0, "needNotify":Z
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 71
    :try_start_0
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    if-eq v2, p1, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 73
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    .line 75
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    if-eqz v0, :cond_1

    .line 77
    const-string v1, "KeyboardInteraction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notify Keyboard IIC Connection is Changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mIsConnectIIC:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mListenerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;

    .line 79
    .local v2, "listener":Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/android/server/input/padkeyboard/KeyboardInteraction$$ExternalSyntheticLambda1;

    invoke-direct {v4, p0, v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/KeyboardInteraction;Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 81
    .end local v2    # "listener":Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;
    goto :goto_0

    .line 83
    :cond_1
    return-void

    .line 75
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public setMiuiIICKeyboardManagerLocked(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 2
    .param p1, "iicKeyboardManager"    # Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    .line 99
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 100
    :try_start_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->mMiuiIICKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    .line 101
    monitor-exit v0

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
