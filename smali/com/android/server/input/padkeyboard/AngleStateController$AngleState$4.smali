.class final enum Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;
.super Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
.source "AngleStateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4010
    name = null
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;IFF)V
    .locals 6
    .param p3, "lower"    # F
    .param p4, "upper"    # F

    .line 89
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$4-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;-><init>(Ljava/lang/String;IFF)V

    return-void
.end method


# virtual methods
.method public getCurrentKeyboardStatus()Z
    .locals 1

    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public isCurrentState(F)Z
    .locals 1
    .param p1, "angle"    # F

    .line 92
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->lower:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->upper:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
    .locals 3
    .param p1, "angle"    # F

    .line 97
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->upper:F

    cmpl-float v0, p1, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ltz v0, :cond_1

    .line 98
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->upper:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->onChange(Z)V

    .line 99
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->NO_WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    move-result-object v0

    return-object v0

    .line 100
    :cond_1
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->lower:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    .line 101
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->upper:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->onChange(Z)V

    .line 102
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;->WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    move-result-object v0

    return-object v0

    .line 104
    :cond_3
    return-object p0
.end method
