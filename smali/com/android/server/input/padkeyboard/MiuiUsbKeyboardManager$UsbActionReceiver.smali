.class Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiUsbKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UsbActionReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    .line 196
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 200
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "action":Ljava/lang/String;
    const-string v1, "device"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    .line 202
    .local v1, "device":Landroid/hardware/usb/UsbDevice;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v2, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_1
    const-string v2, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    const/16 v3, 0x3ffc

    const/16 v4, 0x3206

    const/4 v5, 0x0

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_2

    .line 216
    :pswitch_0
    const-string v2, "MiuiPadKeyboardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "USB device detached: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 218
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 219
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 220
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$mcloseDevice(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    .line 221
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmUsbDeviceLock(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 222
    :try_start_0
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v3, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fputmUsbDevice(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/hardware/usb/UsbDevice;)V

    .line 223
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$monUsbDeviceDetach(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    goto :goto_2

    .line 223
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 204
    :pswitch_1
    const-string v2, "MiuiPadKeyboardManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "USB device attached: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 206
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 207
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 208
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fgetmUsbDeviceLock(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 209
    :try_start_2
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v3, v1}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$fputmUsbDevice(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;Landroid/hardware/usb/UsbDevice;)V

    .line 210
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 211
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$UsbActionReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;->-$$Nest$monUsbDeviceAttach(Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager;)V

    goto :goto_2

    .line 210
    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3

    .line 231
    :cond_1
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e02a835 -> :sswitch_1
        -0x5fdc9a67 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
