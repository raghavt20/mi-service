.class public Lcom/android/server/input/padkeyboard/OnehopInfo;
.super Ljava/lang/Object;
.source "OnehopInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
    }
.end annotation


# static fields
.field private static final ACTION:Ljava/lang/String; = "TAG_DISCOVERED"

.field private static ACTION_BASE:Ljava/lang/String; = null

.field public static ACTION_SUFFIX_MIRROR:Ljava/lang/String; = null

.field private static final DEFAULT_BLUETOOTH_MAC_ADDRESS:Ljava/lang/String; = "02:00:00:00:00:00"

.field private static final DEVICE_TYPE:I = 0x8

.field private static final PREFIX_PROTOCOL:[B

.field private static final PROTOCOL_ID:[B

.field private static final SECURE_SETTINGS_BLUETOOTH_ADDRESS:Ljava/lang/String; = "bluetooth_address"

.field private static final SUFFIX_PROTOCOL:[B

.field private static final TAG:Ljava/lang/String; = "OnehopInfo"

.field private static final TYPE_ABILITY_BASE:I = 0x78

.field private static final TYPE_ACTION_BASE:I = 0x64

.field public static final TYPE_CUSTOM_ACTION:I = 0x65

.field private static final TYPE_DEVICEINFO_BASE:I = 0x0

.field public static final TYPE_DEVICEINFO_BT_MAC:I = 0x1

.field public static final TYPE_EXT_ABILITY:I = 0x79


# instance fields
.field private action_suffix:Ljava/lang/String;

.field private bt_mac:Ljava/lang/String;

.field private device_type:I

.field private ext_ability:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/input/padkeyboard/OnehopInfo;->PROTOCOL_ID:[B

    .line 24
    const-string v0, "com.miui.onehop.action"

    sput-object v0, Lcom/android/server/input/padkeyboard/OnehopInfo;->ACTION_BASE:Ljava/lang/String;

    .line 31
    const-string v0, "MIRROR"

    sput-object v0, Lcom/android/server/input/padkeyboard/OnehopInfo;->ACTION_SUFFIX_MIRROR:Ljava/lang/String;

    .line 34
    const/16 v0, 0x15

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/input/padkeyboard/OnehopInfo;->PREFIX_PROTOCOL:[B

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/server/input/padkeyboard/OnehopInfo;->SUFFIX_PROTOCOL:[B

    return-void

    :array_0
    .array-data 1
        0x27t
        0x17t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x8t
        0x1t
        0x10t
        0xdt
        0x22t
        0x1t
        0x3t
        0x2at
        0x9t
        0x4dt
        0x49t
        0x2dt
        0x4et
        0x46t
        0x43t
        0x54t
        0x41t
        0x47t
        0x38t
        0xft
        0x4at
    .end array-data

    nop

    :array_2
    .array-data 1
        0x6at
        0x2t
        -0x6t
        0x7ft
    .end array-data
.end method

.method private constructor <init>(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->-$$Nest$fgetaction_suffix(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->action_suffix:Ljava/lang/String;

    .line 56
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->-$$Nest$fgetdevice_type(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->device_type:I

    .line 57
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->-$$Nest$fgetbt_mac(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->bt_mac:Ljava/lang/String;

    .line 58
    invoke-static {p1}, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->-$$Nest$fgetext_ability(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->ext_ability:[B

    .line 59
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;Lcom/android/server/input/padkeyboard/OnehopInfo-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/OnehopInfo;-><init>(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)V

    return-void
.end method

.method private static final generateNFCProtocol([B)[B
    .locals 5
    .param p0, "appDataProtocol"    # [B

    .line 300
    if-nez p0, :cond_0

    .line 301
    const-string v0, "OnehopInfo"

    const-string v1, "generateNFCProtocol appDataProtocol is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0

    .line 304
    :cond_0
    sget-object v0, Lcom/android/server/input/padkeyboard/OnehopInfo;->PREFIX_PROTOCOL:[B

    array-length v1, v0

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    array-length v2, p0

    add-int/2addr v1, v2

    sget-object v2, Lcom/android/server/input/padkeyboard/OnehopInfo;->SUFFIX_PROTOCOL:[B

    array-length v3, v2

    add-int/2addr v1, v3

    .line 306
    .local v1, "totoleLength":I
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 307
    .local v3, "totalData":Ljava/nio/ByteBuffer;
    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 308
    add-int/lit8 v4, v1, -0x2

    int-to-byte v4, v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 309
    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 310
    array-length v0, p0

    int-to-byte v0, v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 311
    invoke-virtual {v3, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 312
    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 313
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 314
    .local v0, "protocolData":[B
    return-object v0
.end method

.method public static getBtMacAddress(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 253
    const/4 v0, 0x0

    const-string v1, "OnehopInfo"

    if-nez p0, :cond_0

    .line 254
    const-string v2, "mContext is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    return-object v0

    .line 257
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    .line 258
    .local v2, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v2, :cond_1

    .line 259
    const-string v3, "bluetoothAdapter is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    return-object v0

    .line 262
    :cond_1
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "address":Ljava/lang/String;
    const-string v1, "02:00:00:00:00:00"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 264
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 265
    return-object v0

    .line 267
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "bluetooth_address"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 269
    .local v3, "address2":Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 270
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 273
    :cond_3
    return-object v3

    .line 271
    :cond_4
    :goto_0
    invoke-static {}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getBtMacAddressByReflection()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getBtMacAddressByReflection()Ljava/lang/String;
    .locals 8

    .line 278
    const-string v0, "OnehopInfo"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    .line 279
    .local v2, "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v2, :cond_0

    .line 280
    return-object v1

    .line 282
    :cond_0
    const-string v3, "mService"

    const-class v4, Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lmiui/util/ReflectionUtils;->getObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .line 283
    .local v3, "bluetoothManagerService":Ljava/lang/Object;
    if-nez v3, :cond_1

    .line 284
    const-string v4, "getBtMacAdressByReflection: bluetooth manager service is null"

    invoke-static {v0, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    return-object v1

    .line 287
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getAddress"

    const/4 v6, 0x0

    new-array v7, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    .line 288
    invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 289
    .local v4, "address":Ljava/lang/Object;
    if-eqz v4, :cond_3

    instance-of v5, v4, Ljava/lang/String;

    if-nez v5, :cond_2

    goto :goto_0

    .line 292
    :cond_2
    move-object v5, v4

    check-cast v5, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v5

    .line 290
    :cond_3
    :goto_0
    return-object v1

    .line 293
    .end local v2    # "bluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    .end local v3    # "bluetoothManagerService":Ljava/lang/Object;
    .end local v4    # "address":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 294
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "occur exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    return-object v1
.end method

.method public static getExtAbility()[B
    .locals 5

    .line 244
    const/4 v0, 0x0

    .line 245
    .local v0, "b":B
    const/4 v1, 0x0

    .line 247
    .local v1, "i":I
    or-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v2

    .line 248
    and-int/lit16 v2, v1, 0xff

    int-to-byte v2, v2

    .line 249
    .local v2, "result":B
    const/4 v3, 0x1

    new-array v3, v3, [B

    const/4 v4, 0x0

    aput-byte v2, v3, v4

    return-object v3
.end method

.method private getTLVLength(Ljava/lang/String;)I
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .line 146
    const/4 v0, 0x0

    .line 147
    .local v0, "length":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    array-length v0, v1

    .line 150
    :cond_0
    return v0
.end method

.method private getTLVLength([B)I
    .locals 1
    .param p1, "byteValue"    # [B

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "length":I
    if-eqz p1, :cond_0

    .line 156
    array-length v0, p1

    .line 158
    :cond_0
    return v0
.end method

.method public static toProtocolByteArray([BILjava/util/Map;Ljava/lang/String;[B)[B
    .locals 10
    .param p0, "protocolId"    # [B
    .param p1, "deviceType"    # I
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BI",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "[B>;",
            "Ljava/lang/String;",
            "[B)[B"
        }
    .end annotation

    .line 181
    .local p2, "tlvCountNum":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;[B>;"
    const/4 v0, 0x0

    .line 183
    .local v0, "totalLenth":I
    add-int/lit8 v0, v0, 0x2

    .line 185
    add-int/lit8 v0, v0, 0x4

    .line 187
    const/4 v1, 0x1

    add-int/2addr v0, v1

    .line 188
    const/4 v2, 0x0

    const-string v3, "OnehopInfo"

    if-eqz p2, :cond_0

    .line 189
    :try_start_0
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 190
    .local v5, "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    .line 192
    .local v6, "entryValue":[B
    add-int/lit8 v7, v0, 0x2

    array-length v8, v6

    add-int v0, v7, v8

    .line 193
    .end local v5    # "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
    .end local v6    # "entryValue":[B
    goto :goto_0

    .line 195
    :cond_0
    const/4 v4, 0x0

    .line 196
    .local v4, "actionConntent":[B
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 197
    sget-object v5, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p3, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    move-object v4, v5

    .line 199
    add-int/lit8 v5, v0, 0x1

    array-length v6, v4

    add-int/2addr v5, v6

    .line 204
    .end local v0    # "totalLenth":I
    .local v5, "totalLenth":I
    array-length v0, p4

    add-int/2addr v5, v0

    .line 206
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 207
    .local v0, "totalData":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 208
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 210
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v6

    if-le v6, v1, :cond_2

    .line 211
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 212
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 213
    .local v6, "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 214
    .local v7, "entryKey":I
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    .line 215
    .local v8, "entryValue":[B
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 216
    array-length v9, v8

    int-to-byte v9, v9

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 217
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 218
    nop

    .end local v6    # "entryData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
    .end local v7    # "entryKey":I
    .end local v8    # "entryValue":[B
    goto :goto_1

    :cond_1
    goto :goto_2

    .line 220
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 222
    :goto_2
    if-eqz v4, :cond_4

    .line 223
    array-length v1, v4

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 224
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 229
    if-eqz p4, :cond_3

    .line 230
    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 235
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 236
    .local v1, "totalDataArray":[B
    return-object v1

    .line 232
    .end local v1    # "totalDataArray":[B
    :cond_3
    const-string/jumbo v1, "toProtocolByteArray mPayload is null"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return-object v2

    .line 226
    :cond_4
    const-string/jumbo v1, "toProtocolByteArray actionConntent is null"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    return-object v2

    .line 201
    .end local v5    # "totalLenth":I
    .local v0, "totalLenth":I
    :cond_5
    const-string/jumbo v1, "toProtocolByteArray mAction is null"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    return-object v2

    .line 237
    .end local v0    # "totalLenth":I
    .end local v4    # "actionConntent":[B
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "toProtocolByteArray error : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    .end local v0    # "e":Ljava/lang/Exception;
    return-object v2
.end method


# virtual methods
.method public getPayload(Lcom/android/server/input/padkeyboard/OnehopInfo;)[B
    .locals 5
    .param p1, "info"    # Lcom/android/server/input/padkeyboard/OnehopInfo;

    .line 162
    invoke-virtual {p1}, Lcom/android/server/input/padkeyboard/OnehopInfo;->toPayloadByteArray()[B

    move-result-object v0

    .line 163
    .local v0, "payload":[B
    sget-object v1, Lcom/android/server/input/padkeyboard/OnehopInfo;->PROTOCOL_ID:[B

    const/4 v2, 0x0

    const-string v3, "TAG_DISCOVERED"

    const/16 v4, 0x8

    invoke-static {v1, v4, v2, v3, v0}, Lcom/android/server/input/padkeyboard/OnehopInfo;->toProtocolByteArray([BILjava/util/Map;Ljava/lang/String;[B)[B

    move-result-object v1

    .line 165
    .local v1, "protocolData":[B
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/OnehopInfo;->generateNFCProtocol([B)[B

    move-result-object v2

    .line 166
    .local v2, "nfcProtocolValue":[B
    return-object v2
.end method

.method public toPayloadByteArray()[B
    .locals 7

    .line 95
    const/4 v0, 0x0

    .line 97
    .local v0, "totalLenth":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->action_suffix:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getTLVLength(Ljava/lang/String;)I

    move-result v1

    .line 98
    .local v1, "act_l":I
    if-lez v1, :cond_0

    .line 99
    add-int/lit8 v2, v0, 0x2

    add-int v0, v2, v1

    .line 103
    :cond_0
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->bt_mac:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getTLVLength(Ljava/lang/String;)I

    move-result v2

    .line 104
    .local v2, "bt_mac_l":I
    if-lez v2, :cond_1

    .line 105
    add-int/lit8 v3, v0, 0x2

    add-int v0, v3, v2

    .line 109
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->ext_ability:[B

    invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/OnehopInfo;->getTLVLength([B)I

    move-result v3

    .line 110
    .local v3, "ext_ability_l":I
    if-lez v3, :cond_2

    .line 111
    add-int/lit8 v4, v0, 0x2

    add-int v0, v4, v3

    .line 114
    :cond_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 117
    .local v4, "totalData":Ljava/nio/ByteBuffer;
    if-lez v1, :cond_3

    .line 118
    const/16 v5, 0x65

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 119
    int-to-byte v5, v1

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 120
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->action_suffix:Ljava/lang/String;

    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 124
    :cond_3
    if-lez v2, :cond_4

    .line 125
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 126
    int-to-byte v5, v2

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 127
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->bt_mac:Ljava/lang/String;

    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 131
    :cond_4
    if-lez v3, :cond_5

    .line 132
    const/16 v5, 0x79

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 133
    int-to-byte v5, v3

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 134
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->ext_ability:[B

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 137
    :cond_5
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .local v5, "totalDataArray":[B
    return-object v5

    .line 139
    .end local v0    # "totalLenth":I
    .end local v1    # "act_l":I
    .end local v2    # "bt_mac_l":I
    .end local v3    # "ext_ability_l":I
    .end local v4    # "totalData":Ljava/nio/ByteBuffer;
    .end local v5    # "totalDataArray":[B
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OnehopInfo"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnehopInfo{, action_suffix=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->action_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", device_type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->device_type:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", bt_mac=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->bt_mac:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ext_ability=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/OnehopInfo;->ext_ability:[B

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
