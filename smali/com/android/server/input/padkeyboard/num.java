public abstract class num extends java.lang.Enum {
	 /* .source "AngleStateController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/AngleStateController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x4409 */
/* name = "AngleState" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener; */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/PermittedSubclasses; */
/* value = { */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$1;, */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$2;, */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$3;, */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;, */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$5;, */
/* Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6; */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Enum<", */
/* "Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
private static final com.android.server.input.padkeyboard.AngleStateController$AngleState $VALUES; //synthetic
public static final com.android.server.input.padkeyboard.AngleStateController$AngleState BACK_STATE;
public static final com.android.server.input.padkeyboard.AngleStateController$AngleState CLOSE_STATE;
public static final com.android.server.input.padkeyboard.AngleStateController$AngleState NO_WORK_STATE_1;
public static final com.android.server.input.padkeyboard.AngleStateController$AngleState NO_WORK_STATE_2;
public static final com.android.server.input.padkeyboard.AngleStateController$AngleState WORK_STATE_1;
public static final com.android.server.input.padkeyboard.AngleStateController$AngleState WORK_STATE_2;
/* # instance fields */
protected Float lower;
protected com.android.server.input.padkeyboard.AngleStateController$AngleState$OnChangeListener onChangeListener;
protected Float upper;
/* # direct methods */
private static com.android.server.input.padkeyboard.AngleStateController$AngleState $values ( ) { //synthethic
/* .locals 6 */
/* .line 19 */
v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.CLOSE_STATE;
v1 = com.android.server.input.padkeyboard.AngleStateController$AngleState.NO_WORK_STATE_1;
v2 = com.android.server.input.padkeyboard.AngleStateController$AngleState.WORK_STATE_1;
v3 = com.android.server.input.padkeyboard.AngleStateController$AngleState.WORK_STATE_2;
v4 = com.android.server.input.padkeyboard.AngleStateController$AngleState.NO_WORK_STATE_2;
v5 = com.android.server.input.padkeyboard.AngleStateController$AngleState.BACK_STATE;
/* filled-new-array/range {v0 ..v5}, [Lcom/android/server/input/padkeyboard/AngleStateController$AngleState; */
} // .end method
static num ( ) {
/* .locals 13 */
/* .line 20 */
/* new-instance v6, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$1; */
final String v1 = "CLOSE_STATE"; // const-string v1, "CLOSE_STATE"
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/high16 v4, 0x40a00000 # 5.0f */
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, v6 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$1;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$1-IA;)V */
/* .line 41 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$2; */
final String v8 = "NO_WORK_STATE_1"; // const-string v8, "NO_WORK_STATE_1"
int v9 = 1; // const/4 v9, 0x1
/* const/high16 v10, 0x40a00000 # 5.0f */
/* const/high16 v11, 0x42340000 # 45.0f */
int v12 = 0; // const/4 v12, 0x0
/* move-object v7, v0 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$2;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$2-IA;)V */
/* .line 65 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$3; */
final String v2 = "WORK_STATE_1"; // const-string v2, "WORK_STATE_1"
int v3 = 2; // const/4 v3, 0x2
/* const/high16 v4, 0x42340000 # 45.0f */
/* const/high16 v5, 0x42b40000 # 90.0f */
int v6 = 0; // const/4 v6, 0x0
/* move-object v1, v0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$3;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$3-IA;)V */
/* .line 89 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4; */
final String v8 = "WORK_STATE_2"; // const-string v8, "WORK_STATE_2"
int v9 = 3; // const/4 v9, 0x3
/* const/high16 v10, 0x42b40000 # 90.0f */
/* const/high16 v11, 0x43390000 # 185.0f */
/* move-object v7, v0 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$4-IA;)V */
/* .line 113 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$5; */
final String v2 = "NO_WORK_STATE_2"; // const-string v2, "NO_WORK_STATE_2"
int v3 = 4; // const/4 v3, 0x4
/* const/high16 v4, 0x43390000 # 185.0f */
/* const v5, 0x43b18000 # 355.0f */
/* move-object v1, v0 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$5;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$5-IA;)V */
/* .line 137 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6; */
final String v8 = "BACK_STATE"; // const-string v8, "BACK_STATE"
int v9 = 5; // const/4 v9, 0x5
/* const v10, 0x43b18000 # 355.0f */
/* const/high16 v11, 0x43b40000 # 360.0f */
/* move-object v7, v0 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$6-IA;)V */
/* .line 19 */
com.android.server.input.padkeyboard.AngleStateController$AngleState .$values ( );
return;
} // .end method
private num ( ) {
/* .locals 0 */
/* .param p3, "lower" # F */
/* .param p4, "upper" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(FF)V" */
/* } */
} // .end annotation
/* .line 162 */
/* invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V */
/* .line 163 */
/* iput p4, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->upper:F */
/* .line 164 */
/* iput p3, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->lower:F */
/* .line 165 */
return;
} // .end method
 num ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;-><init>(Ljava/lang/String;IFF)V */
return;
} // .end method
public static com.android.server.input.padkeyboard.AngleStateController$AngleState valueOf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 19 */
/* const-class v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState; */
java.lang.Enum .valueOf ( v0,p0 );
/* check-cast v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState; */
} // .end method
public static com.android.server.input.padkeyboard.AngleStateController$AngleState values ( ) {
/* .locals 1 */
/* .line 19 */
v0 = com.android.server.input.padkeyboard.AngleStateController$AngleState.$VALUES;
(( com.android.server.input.padkeyboard.AngleStateController$AngleState ) v0 ).clone ( ); // invoke-virtual {v0}, [Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->clone()Ljava/lang/Object;
/* check-cast v0, [Lcom/android/server/input/padkeyboard/AngleStateController$AngleState; */
} // .end method
/* # virtual methods */
public abstract Boolean getCurrentKeyboardStatus ( ) {
} // .end method
public abstract Boolean isCurrentState ( Float p0 ) {
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "toUpper" # Z */
/* .line 175 */
v0 = this.onChangeListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 176 */
/* .line 178 */
} // :cond_0
return;
} // .end method
public void setOnChangeListener ( com.android.server.input.padkeyboard.AngleStateController$AngleState$OnChangeListener p0 ) {
/* .locals 0 */
/* .param p1, "onChangeListener" # Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener; */
/* .line 181 */
this.onChangeListener = p1;
/* .line 182 */
return;
} // .end method
public abstract com.android.server.input.padkeyboard.AngleStateController$AngleState toNextState ( Float p0 ) {
} // .end method
