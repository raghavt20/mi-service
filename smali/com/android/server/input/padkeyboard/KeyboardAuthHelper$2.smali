.class Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;
.super Ljava/lang/Object;
.source "KeyboardAuthHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    .line 159
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 163
    const-string v0, "MiuiKeyboardManager_MiDevAuthService"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmDeathRecipient(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/os/IBinder$DeathRecipient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p2, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    goto :goto_0

    .line 164
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "linkToDeath fail: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {p2}, Lcom/xiaomi/devauth/IMiDevAuthInterface$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/devauth/IMiDevAuthInterface;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fputmService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;Lcom/xiaomi/devauth/IMiDevAuthInterface;)V

    .line 168
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Lcom/xiaomi/devauth/IMiDevAuthInterface;

    move-result-object v1

    if-nez v1, :cond_0

    .line 169
    const-string v1, "Try connect midevauth service fail"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 175
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "MiuiKeyboardManager_MiDevAuthService"

    const-string v1, "re-bind to MiDevAuth service"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$fgetmConn(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 178
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;->this$0:Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->-$$Nest$minitService(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V

    .line 180
    :cond_0
    return-void
.end method
