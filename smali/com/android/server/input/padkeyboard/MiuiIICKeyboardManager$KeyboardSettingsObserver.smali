.class Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiIICKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyboardSettingsObserver"
.end annotation


# instance fields
.field public mShouldUnregisterLanguageReceiver:Z

.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;


# direct methods
.method public constructor <init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1420
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    .line 1421
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->mShouldUnregisterLanguageReceiver:Z

    .line 1422
    return-void
.end method


# virtual methods
.method public observerObject()V
    .locals 5

    .line 1425
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1426
    const-string v1, "keyboard_back_light_automatic_adjustment"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    move-result-object v3

    .line 1425
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1427
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1429
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1430
    const-string v1, "keyboard_back_light_brightness"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    move-result-object v3

    .line 1429
    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1431
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1433
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1434
    const-string v1, "enable_tap_touch_pad"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    move-result-object v3

    .line 1433
    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1435
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1438
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1439
    const-string v1, "enable_upgrade_no_check"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    move-result-object v3

    .line 1438
    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1440
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1442
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1443
    const-string v1, "miui_keyboard_address"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    move-result-object v3

    .line 1442
    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1444
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1446
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1447
    const-string/jumbo v1, "user_setup_complete"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardObserver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;

    move-result-object v3

    .line 1446
    invoke-virtual {v0, v2, v4, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1448
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1449
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 7
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1453
    const-string v0, "keyboard_back_light_automatic_adjustment"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "MiuiPadKeyboardManager"

    const/4 v3, 0x1

    const/4 v4, -0x2

    const/4 v5, 0x0

    if-eqz v1, :cond_2

    .line 1454
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v0, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v3, v5

    :goto_0
    invoke-static {v1, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fputmIsAutoBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Z)V

    .line 1456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update auto back light:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIsAutoBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1457
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIsAutoBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1459
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fputmUserSetBackLight(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Z)V

    .line 1461
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$mupdateLightSensor(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    goto/16 :goto_5

    .line 1462
    :cond_2
    const-string v0, "keyboard_back_light_brightness"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1463
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fputmBackLightBrightness(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V

    goto/16 :goto_5

    .line 1465
    :cond_3
    const-string v0, "enable_tap_touch_pad"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1466
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1467
    invoke-static {}, Lmiui/hardware/input/InputFeature;->getTouchpadTapDefaultValue()I

    move-result v6

    .line 1466
    invoke-static {v1, v0, v6, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_4

    goto :goto_1

    :cond_4
    move v3, v5

    :goto_1
    move v0, v3

    .line 1469
    .local v0, "enable":Z
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v1

    .line 1470
    .local v1, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    invoke-virtual {v1, v0}, Lcom/android/server/input/config/InputCommonConfig;->setTapTouchPad(Z)V

    .line 1471
    invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 1472
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update tap touchpad:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1473
    .end local v0    # "enable":Z
    .end local v1    # "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    goto/16 :goto_5

    :cond_5
    const-string v0, "enable_upgrade_no_check"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1474
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v5, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_2

    :cond_6
    move v3, v5

    :goto_2
    move v0, v3

    .line 1476
    .restart local v0    # "enable":Z
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setNoCheckUpgrade(Z)V

    .line 1477
    .end local v0    # "enable":Z
    goto :goto_5

    :cond_7
    const-string v0, "miui_keyboard_address"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1478
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1480
    .local v0, "address":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmBluetoothKeyboardManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->setCurrentBleKeyboardAddress(Ljava/lang/String;)V

    .end local v0    # "address":Ljava/lang/String;
    goto :goto_4

    .line 1482
    :cond_8
    const-string/jumbo v0, "user_setup_complete"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1483
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v1

    .line 1484
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1483
    invoke-static {v1, v0, v5, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v3, :cond_9

    goto :goto_3

    :cond_9
    move v3, v5

    :goto_3
    move v0, v3

    .line 1486
    .local v0, "isUserSetupComplete":Z
    if-eqz v0, :cond_b

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->mShouldUnregisterLanguageReceiver:Z

    if-eqz v1, :cond_b

    .line 1487
    iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->mShouldUnregisterLanguageReceiver:Z

    .line 1489
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmLanguageChangeReceiver(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1490
    const-string v1, "User setup completed, unregister language listener"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1493
    goto :goto_5

    .line 1491
    :catch_0
    move-exception v1

    .line 1492
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "Unregister Exception!"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 1482
    .end local v0    # "isUserSetupComplete":Z
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_a
    :goto_4
    nop

    .line 1496
    :cond_b
    :goto_5
    return-void
.end method
