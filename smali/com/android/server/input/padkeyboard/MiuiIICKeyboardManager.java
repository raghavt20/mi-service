public class com.android.server.input.padkeyboard.MiuiIICKeyboardManager implements com.android.server.input.padkeyboard.iic.NanoSocketCallback implements com.android.server.input.padkeyboard.MiuiPadKeyboardManager implements com.android.server.input.padkeyboard.KeyboardInteraction$KeyboardStatusChangedListener {
	 /* .source "MiuiIICKeyboardManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;, */
	 /* Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;, */
	 /* Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String AUTO_BACK_BRIGHTNESS;
public static final java.lang.String CURRENT_BACK_BRIGHTNESS;
private static final Integer DEFAULT_DEVICE_ID;
public static final java.lang.String ENABLE_TAP_TOUCH_PAD;
private static final java.lang.String ENABLE_UPGRADE_NO_CHECK;
public static final java.lang.String LAST_CONNECT_BLE_ADDRESS;
private static final Integer MAX_CHECK_IDENTITY_TIMES;
private static volatile com.android.server.input.padkeyboard.MiuiIICKeyboardManager sInstance;
/* # instance fields */
private final Float G;
private final android.hardware.Sensor mAccSensor;
private final com.android.server.input.padkeyboard.AngleStateController mAngleStateController;
private volatile Boolean mAuthStarted;
private Integer mBackLightBrightness;
private final java.util.Set mBleDeviceList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mBleDeviceListLock;
private final com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager mBluetoothKeyboardManager;
private volatile Boolean mCheckIdentityPass;
private Integer mCheckIdentityTimes;
private com.android.server.input.padkeyboard.iic.CommunicationUtil mCommunicationUtil;
private Integer mConsumerDeviceId;
private final android.content.Context mContext;
private miuix.appcompat.app.AlertDialog mDialog;
private final android.os.Handler mHandler;
private Boolean mHasTouchPad;
private final com.android.server.input.padkeyboard.iic.IICNodeHelper mIICNodeHelper;
private final com.android.server.input.InputManagerService mInputManager;
private Boolean mIsAutoBackLight;
private Boolean mIsAutoBackLightEffected;
private Boolean mIsKeyboardReady;
private Boolean mIsKeyboardSleep;
private Boolean mKeyboardAccReady;
private android.view.InputDevice mKeyboardDevice;
private Integer mKeyboardDeviceId;
private final mKeyboardGData;
private final com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver mKeyboardObserver;
private Integer mKeyboardType;
private java.lang.String mKeyboardVersion;
private com.android.server.input.padkeyboard.MiuiIICKeyboardManager$LanguageChangeReceiver mLanguageChangeReceiver;
private final android.hardware.Sensor mLightSensor;
private final mLocalGData;
private java.lang.String mMCUVersion;
private final android.content.BroadcastReceiver mMicMuteReceiver;
private Boolean mPadAccReady;
private final android.os.PowerManager mPowerManager;
private Boolean mScreenState;
private final android.hardware.SensorManager mSensorManager;
private Boolean mShouldStayWakeKeyboard;
private Boolean mShouldUpgradeKeyboard;
private Boolean mShouldUpgradeMCU;
private Boolean mShouldUpgradeTouchPad;
private java.lang.Runnable mStopAwakeRunnable;
private Integer mTouchDeviceId;
private java.lang.String mTouchPadVersion;
private Boolean mUserSetBackLight;
private final android.os.PowerManager$WakeLock mWakeLock;
private java.lang.Object mlock;
/* # direct methods */
public static void $r8$lambda$8IbYAuQyeUENOAD4Cq-4NAWP_Rc ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->acquireWakeLock()V */
return;
} // .end method
public static void $r8$lambda$9sT42a2-9Fw85TP7xCQpX3WYPvE ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$removeKeyboardDevicesIfNeeded$0(I)V */
return;
} // .end method
public static void $r8$lambda$Fv0MPZV8Tp5UQ8mjMZj8IFvGAUM ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$notifyScreenState$1()V */
return;
} // .end method
public static void $r8$lambda$Kh2kVhwXy8cdWcBe9iysNWV3MSo ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardAttach()V */
return;
} // .end method
public static void $r8$lambda$OCmF4I4dWdcMv88u7DuW-ueH4J0 ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$notifyScreenState$2()V */
return;
} // .end method
public static void $r8$lambda$UxCry0Qn23UtO3xIfFxxR13DCTs ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stayWakeForKeyboard176()V */
return;
} // .end method
public static void $r8$lambda$WP4xfbdfM26WziH9B-vTAZ6Ugbk ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->changeDeviceEnableState()V */
return;
} // .end method
public static void $r8$lambda$hqI_sPMl8TiZiVcct9qPVwc2f3k ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->lambda$postShowRejectConfirmDialog$3(I)V */
return;
} // .end method
static com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager -$$Nest$fgetmBluetoothKeyboardManager ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBluetoothKeyboardManager;
} // .end method
static com.android.server.input.padkeyboard.iic.CommunicationUtil -$$Nest$fgetmCommunicationUtil ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCommunicationUtil;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static com.android.server.input.padkeyboard.iic.IICNodeHelper -$$Nest$fgetmIICNodeHelper ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mIICNodeHelper;
} // .end method
static com.android.server.input.InputManagerService -$$Nest$fgetmInputManager ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInputManager;
} // .end method
static Boolean -$$Nest$fgetmIsAutoBackLight ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z */
} // .end method
static Boolean -$$Nest$fgetmIsKeyboardReady ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
} // .end method
static android.view.InputDevice -$$Nest$fgetmKeyboardDevice ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardDevice;
} // .end method
static com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver -$$Nest$fgetmKeyboardObserver ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKeyboardObserver;
} // .end method
static com.android.server.input.padkeyboard.MiuiIICKeyboardManager$LanguageChangeReceiver -$$Nest$fgetmLanguageChangeReceiver ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLanguageChangeReceiver;
} // .end method
static Boolean -$$Nest$fgetmShouldStayWakeKeyboard ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z */
} // .end method
static Boolean -$$Nest$fgetmShouldUpgradeKeyboard ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
} // .end method
static Boolean -$$Nest$fgetmShouldUpgradeMCU ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z */
} // .end method
static Boolean -$$Nest$fgetmShouldUpgradeTouchPad ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
} // .end method
static void -$$Nest$fputmBackLightBrightness ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
return;
} // .end method
static void -$$Nest$fputmIsAutoBackLight ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z */
return;
} // .end method
static void -$$Nest$fputmUserSetBackLight ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z */
return;
} // .end method
static void -$$Nest$mdoCheckKeyboardIdentity ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->doCheckKeyboardIdentity(Z)V */
return;
} // .end method
static void -$$Nest$mreleaseWakeLock ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->releaseWakeLock()V */
return;
} // .end method
static void -$$Nest$mstopWakeForKeyboard176 ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopWakeForKeyboard176()V */
return;
} // .end method
static void -$$Nest$mupdateLightSensor ( com.android.server.input.padkeyboard.MiuiIICKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateLightSensor()V */
return;
} // .end method
private com.android.server.input.padkeyboard.MiuiIICKeyboardManager ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 138 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 83 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v1, v0, [F */
this.mLocalGData = v1;
/* .line 84 */
/* new-array v0, v0, [F */
this.mKeyboardGData = v0;
/* .line 85 */
/* const v0, 0x411ccccd # 9.8f */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->G:F */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityPass:Z */
/* .line 95 */
/* const/16 v1, 0x141 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardType:I */
/* .line 97 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mlock = v1;
/* .line 98 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I */
/* .line 100 */
/* const/16 v1, -0x63 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
/* .line 101 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
/* .line 102 */
/* iput v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
/* .line 103 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
java.util.Collections .synchronizedSet ( v1 );
this.mBleDeviceList = v1;
/* .line 104 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mBleDeviceListLock = v1;
/* .line 105 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
/* .line 107 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
/* .line 108 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z */
/* .line 110 */
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLightEffected:Z */
/* .line 116 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z */
/* .line 117 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* .line 118 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z */
/* .line 120 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z */
/* .line 124 */
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z */
/* .line 125 */
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
/* .line 126 */
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
/* .line 129 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$1;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
this.mMicMuteReceiver = v0;
/* .line 529 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$2;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
this.mStopAwakeRunnable = v0;
/* .line 139 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v2 = "MiuiIICKeyboardManager"; // const-string v2, "MiuiIICKeyboardManager"
android.util.Slog .i ( v0,v2 );
/* .line 140 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v2 = "IIC_Pad_Manager"; // const-string v2, "IIC_Pad_Manager"
/* invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 141 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 142 */
this.mContext = p1;
/* .line 143 */
com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .getInstance ( p1 );
this.mBluetoothKeyboardManager = v2;
/* .line 144 */
/* new-instance v2, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 145 */
/* new-instance v2, Lcom/android/server/input/padkeyboard/AngleStateController; */
/* invoke-direct {v2, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;-><init>(Landroid/content/Context;)V */
this.mAngleStateController = v2;
/* .line 146 */
final String v2 = "input"; // const-string v2, "input"
android.os.ServiceManager .getService ( v2 );
/* check-cast v2, Lcom/android/server/input/InputManagerService; */
this.mInputManager = v2;
/* .line 147 */
/* const-string/jumbo v2, "sensor" */
(( android.content.Context ) p1 ).getSystemService ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/hardware/SensorManager; */
this.mSensorManager = v2;
/* .line 148 */
(( android.hardware.SensorManager ) v2 ).getDefaultSensor ( v1 ); // invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mAccSensor = v1;
/* .line 149 */
int v1 = 5; // const/4 v1, 0x5
(( android.hardware.SensorManager ) v2 ).getDefaultSensor ( v1 ); // invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mLightSensor = v1;
/* .line 150 */
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/PowerManager; */
this.mPowerManager = v1;
/* .line 151 */
int v2 = 6; // const/4 v2, 0x6
final String v3 = "iic_upgrade"; // const-string v3, "iic_upgrade"
(( android.os.PowerManager ) v1 ).newWakeLock ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mWakeLock = v1;
/* .line 153 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver-IA;)V */
this.mLanguageChangeReceiver = v1;
/* .line 154 */
/* new-instance v1, Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.LOCALE_CHANGED"; // const-string v2, "android.intent.action.LOCALE_CHANGED"
/* invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 155 */
/* .local v1, "languageFilter":Landroid/content/IntentFilter; */
v2 = this.mLanguageChangeReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v2, v1 ); // invoke-virtual {p1, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 157 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 158 */
/* .local v2, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
/* const/16 v3, 0x15d9 */
/* const/16 v4, 0xa3 */
(( com.android.server.input.config.InputCommonConfig ) v2 ).setMiuiKeyboardInfo ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/input/config/InputCommonConfig;->setMiuiKeyboardInfo(II)V
/* .line 160 */
(( com.android.server.input.config.InputCommonConfig ) v2 ).flushToNative ( ); // invoke-virtual {v2}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 161 */
/* new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver; */
com.android.server.input.MiuiInputThread .getHandler ( );
/* invoke-direct {v3, p0, v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Landroid/os/Handler;)V */
this.mKeyboardObserver = v3;
/* .line 162 */
com.android.server.input.padkeyboard.iic.IICNodeHelper .getInstance ( p1 );
this.mIICNodeHelper = v4;
/* .line 163 */
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v4 ).setOtaCallBack ( p0 ); // invoke-virtual {v4, p0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setOtaCallBack(Lcom/android/server/input/padkeyboard/iic/NanoSocketCallback;)V
/* .line 164 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager$KeyboardSettingsObserver ) v3 ).observerObject ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$KeyboardSettingsObserver;->observerObject()V
/* .line 165 */
v3 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v3 ).setMiuiIICKeyboardManagerLocked ( p0 ); // invoke-virtual {v3, p0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setMiuiIICKeyboardManagerLocked(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
/* .line 166 */
v3 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v3 ).addListener ( p0 ); // invoke-virtual {v3, p0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->addListener(Lcom/android/server/input/padkeyboard/KeyboardInteraction$KeyboardStatusChangedListener;)V
/* .line 167 */
com.android.server.input.padkeyboard.iic.CommunicationUtil .getInstance ( );
this.mCommunicationUtil = v3;
/* .line 169 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).readKeyboardStatus ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->readKeyboardStatus()V
/* .line 170 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).readHallStatus ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->readHallStatus()V
/* .line 173 */
/* const/16 v3, 0xa */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 174 */
/* const/16 v3, 0x9 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 175 */
return;
} // .end method
private void acquireWakeLock ( ) {
/* .locals 1 */
/* .line 1222 */
v0 = this.mWakeLock;
v0 = (( android.os.PowerManager$WakeLock ) v0 ).isHeld ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z
/* if-nez v0, :cond_0 */
/* .line 1223 */
v0 = this.mWakeLock;
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
/* .line 1225 */
} // :cond_0
return;
} // .end method
private void calculateAngle ( ) {
/* .locals 10 */
/* .line 1169 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z */
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1170 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .supportCalculateAngle ( );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1171 */
	 v0 = this.mKeyboardGData;
	 int v2 = 0; // const/4 v2, 0x0
	 /* aget v3, v0, v2 */
	 int v4 = 1; // const/4 v4, 0x1
	 /* aget v5, v0, v4 */
	 int v6 = 2; // const/4 v6, 0x2
	 /* aget v0, v0, v6 */
	 v7 = this.mLocalGData;
	 /* aget v2, v7, v2 */
	 /* aget v8, v7, v4 */
	 /* aget v9, v7, v6 */
	 /* move v4, v5 */
	 /* move v5, v0 */
	 /* move v6, v2 */
	 /* move v7, v8 */
	 /* move v8, v9 */
	 v0 = 	 /* invoke-static/range {v3 ..v8}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->calculatePKAngleV2(FFFFFF)I */
	 /* .line 1178 */
	 /* .local v0, "resultAngle":I */
	 int v2 = -1; // const/4 v2, -0x1
	 /* if-ne v0, v2, :cond_0 */
	 /* .line 1179 */
	 final String v2 = "accel angle error > 0.98, dont update angle"; // const-string v2, "accel angle error > 0.98, dont update angle"
	 android.util.Slog .i ( v1,v2 );
	 /* .line 1181 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "result Angle = "; // const-string v3, "result Angle = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 1182 */
v1 = this.mAngleStateController;
/* int-to-float v2, v0 */
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).updateAngleState ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->updateAngleState(F)V
/* .line 1183 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1185 */
} // .end local v0 # "resultAngle":I
} // :goto_0
/* .line 1186 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "The Pad or Keyboard gsensor not ready, mKeyboardAccReady: "; // const-string v2, "The Pad or Keyboard gsensor not ready, mKeyboardAccReady: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", mPadAccReady: "; // const-string v2, ", mPadAccReady: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", IIC maybe disconnect"; // const-string v2, ", IIC maybe disconnect"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 1189 */
} // :goto_1
return;
} // .end method
private void changeDeviceEnableState ( ) {
/* .locals 6 */
/* .line 381 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isDeviceEnableStatusChanged()Z */
/* if-nez v0, :cond_0 */
/* .line 382 */
return;
/* .line 384 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableOrEnableDevice:"; // const-string v1, "enableOrEnableDevice:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 387 */
v0 = this.mAngleStateController;
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
/* const/16 v2, -0x63 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 388 */
final String v0 = "Disable Xiaomi Keyboard!"; // const-string v0, "Disable Xiaomi Keyboard!"
android.util.Slog .i ( v1,v0 );
/* .line 389 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardStatus()V */
/* .line 390 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
/* if-eq v0, v2, :cond_1 */
/* .line 391 */
v3 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v3 ).disableInputDevice ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V
/* .line 393 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
/* if-eq v0, v2, :cond_2 */
/* .line 394 */
v3 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v3 ).disableInputDevice ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V
/* .line 396 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
/* if-eq v0, v2, :cond_3 */
/* .line 397 */
v3 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v3 ).disableInputDevice ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V
/* .line 399 */
} // :cond_3
v0 = this.mBleDeviceList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 400 */
/* .local v3, "bleDevice":I */
/* if-eq v3, v2, :cond_4 */
/* .line 401 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Disable Ble Keyboard:"; // const-string v5, "Disable Ble Keyboard:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 402 */
v4 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v4 ).disableInputDevice ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/InputManagerService;->disableInputDevice(I)V
/* .line 404 */
} // .end local v3 # "bleDevice":I
} // :cond_4
} // :cond_5
/* .line 406 */
} // :cond_6
final String v0 = "Enable Xiaomi Keyboard!"; // const-string v0, "Enable Xiaomi Keyboard!"
android.util.Slog .i ( v1,v0 );
/* .line 407 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardStatus()V */
/* .line 408 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
/* if-eq v0, v2, :cond_7 */
/* .line 409 */
v3 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v3 ).enableInputDevice ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V
/* .line 411 */
} // :cond_7
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
/* if-eq v0, v2, :cond_8 */
/* .line 412 */
v3 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v3 ).enableInputDevice ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V
/* .line 414 */
} // :cond_8
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
/* if-eq v0, v2, :cond_9 */
/* .line 415 */
v3 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v3 ).enableInputDevice ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V
/* .line 417 */
} // :cond_9
v0 = this.mBleDeviceList;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_b
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 418 */
/* .restart local v3 # "bleDevice":I */
/* if-eq v3, v2, :cond_a */
/* .line 419 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Enable Ble Keyboard:"; // const-string v5, "Enable Ble Keyboard:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 420 */
v4 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v4 ).enableInputDevice ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/InputManagerService;->enableInputDevice(I)V
/* .line 422 */
} // .end local v3 # "bleDevice":I
} // :cond_a
/* .line 424 */
} // :cond_b
} // :goto_2
v0 = this.mInputManager;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
final String v2 = "notifyIgnoredInputDevicesChanged"; // const-string v2, "notifyIgnoredInputDevicesChanged"
com.android.server.input.ReflectionUtils .callPrivateMethod ( v0,v2,v1 );
/* .line 425 */
return;
} // .end method
private void doCheckKeyboardIdentity ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isFirst" # Z */
/* .line 1030 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1031 */
v0 = this.mContext;
com.android.server.input.padkeyboard.KeyboardAuthHelper .getInstance ( v0 );
v0 = (( com.android.server.input.padkeyboard.KeyboardAuthHelper ) v0 ).doCheckKeyboardIdentityLaunchBeforeU ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchBeforeU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I
/* .line 1032 */
} // :cond_0
v0 = this.mContext;
com.android.server.input.padkeyboard.KeyboardAuthHelper .getInstance ( v0 );
v0 = (( com.android.server.input.padkeyboard.KeyboardAuthHelper ) v0 ).doCheckKeyboardIdentityLaunchAfterU ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->doCheckKeyboardIdentityLaunchAfterU(Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;Z)I
} // :goto_0
/* nop */
/* .line 1033 */
/* .local v0, "result":I */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->processIdentity(I)V */
/* .line 1034 */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isFailedIdentity(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1036 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V */
/* .line 1037 */
} // :cond_1
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isSuccessfulIdentity(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1039 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1040 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 1042 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1043 */
/* const/16 v1, 0xd */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 1045 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
/* if-nez v1, :cond_4 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
/* if-nez v1, :cond_4 */
/* .line 1046 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).stopStayAwake ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopStayAwake()V
/* .line 1049 */
} // :cond_4
} // :goto_1
return;
} // .end method
public static com.android.server.input.padkeyboard.MiuiIICKeyboardManager getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 178 */
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 179 */
/* const-class v0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
/* monitor-enter v0 */
/* .line 180 */
try { // :try_start_0
v1 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 181 */
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;-><init>(Landroid/content/Context;)V */
/* .line 183 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 185 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager.sInstance;
} // .end method
private Boolean isDeviceEnableStatusChanged ( ) {
/* .locals 9 */
/* .line 429 */
v0 = this.mInputManager;
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
(( com.android.server.input.InputManagerService ) v0 ).getInputDevice ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 430 */
/* .local v0, "iicKeyboardDevice":Landroid/view/InputDevice; */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = (( android.view.InputDevice ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->isEnabled()Z
v3 = this.mAngleStateController;
/* .line 431 */
v3 = (( com.android.server.input.padkeyboard.AngleStateController ) v3 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
/* if-ne v2, v3, :cond_0 */
/* .line 432 */
/* .line 435 */
} // :cond_0
v2 = this.mInputManager;
/* iget v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
(( com.android.server.input.InputManagerService ) v2 ).getInputDevice ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 436 */
/* .local v2, "consumerKeyboardDevice":Landroid/view/InputDevice; */
if ( v2 != null) { // if-eqz v2, :cond_1
v3 = (( android.view.InputDevice ) v2 ).isEnabled ( ); // invoke-virtual {v2}, Landroid/view/InputDevice;->isEnabled()Z
v4 = this.mAngleStateController;
/* .line 437 */
v4 = (( com.android.server.input.padkeyboard.AngleStateController ) v4 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v4}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
/* if-ne v3, v4, :cond_1 */
/* .line 438 */
/* .line 441 */
} // :cond_1
v3 = this.mInputManager;
/* iget v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
(( com.android.server.input.InputManagerService ) v3 ).getInputDevice ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 442 */
/* .local v3, "touchPadDevice":Landroid/view/InputDevice; */
if ( v3 != null) { // if-eqz v3, :cond_2
v4 = (( android.view.InputDevice ) v3 ).isEnabled ( ); // invoke-virtual {v3}, Landroid/view/InputDevice;->isEnabled()Z
v5 = this.mAngleStateController;
/* .line 443 */
v5 = (( com.android.server.input.padkeyboard.AngleStateController ) v5 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v5}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
/* if-ne v4, v5, :cond_2 */
/* .line 444 */
/* .line 447 */
} // :cond_2
v4 = this.mBleDeviceList;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 448 */
/* .local v5, "belDeviceId":I */
v6 = this.mInputManager;
(( com.android.server.input.InputManagerService ) v6 ).getInputDevice ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 449 */
/* .local v6, "bleDevice":Landroid/view/InputDevice; */
if ( v6 != null) { // if-eqz v6, :cond_3
v7 = (( android.view.InputDevice ) v6 ).isEnabled ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->isEnabled()Z
v8 = this.mAngleStateController;
/* .line 450 */
v8 = (( com.android.server.input.padkeyboard.AngleStateController ) v8 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v8}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
/* if-ne v7, v8, :cond_3 */
/* .line 451 */
/* .line 453 */
} // .end local v5 # "belDeviceId":I
} // .end local v6 # "bleDevice":Landroid/view/InputDevice;
} // :cond_3
/* .line 454 */
} // :cond_4
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean isFailedIdentity ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "result" # I */
/* .line 1058 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_1 */
int v1 = 4; // const/4 v1, 0x4
/* if-ne p1, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
} // .end method
private Boolean isSuccessfulIdentity ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "result" # I */
/* .line 1052 */
if ( p1 != null) { // if-eqz p1, :cond_1
int v0 = 3; // const/4 v0, 0x3
/* if-eq p1, v0, :cond_1 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_0 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I */
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private void lambda$notifyScreenState$1 ( ) { //synthethic
/* .locals 2 */
/* .line 334 */
v0 = this.mContext;
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v0 );
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).setKeyboardShortcutEnable ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V
return;
} // .end method
private void lambda$notifyScreenState$2 ( ) { //synthethic
/* .locals 1 */
/* .line 354 */
/* const/16 v0, 0x9 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
return;
} // .end method
private void lambda$postShowRejectConfirmDialog$3 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "type" # I */
/* .line 1118 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->showRejectConfirmDialog(I)V */
/* .line 1119 */
return;
} // .end method
private void lambda$removeKeyboardDevicesIfNeeded$0 ( Integer p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "deviceId" # I */
/* .line 280 */
v0 = this.mBleDeviceList;
java.lang.Integer .valueOf ( p1 );
return;
} // .end method
private Boolean motionJudge ( Float p0, Float p1, Float p2 ) {
/* .locals 3 */
/* .param p1, "accX" # F */
/* .param p2, "accY" # F */
/* .param p3, "accZ" # F */
/* .line 1201 */
/* mul-float v0, p1, p1 */
/* mul-float v1, p2, p2 */
/* add-float/2addr v0, v1 */
/* mul-float v1, p3, p3 */
/* add-float/2addr v0, v1 */
/* float-to-double v0, v0 */
java.lang.Math .sqrt ( v0,v1 );
/* move-result-wide v0 */
/* double-to-float v0, v0 */
/* .line 1204 */
/* .local v0, "mCombAcc":F */
/* const v1, 0x411ccccd # 9.8f */
/* sub-float v1, v0, v1 */
v1 = java.lang.Math .abs ( v1 );
/* const v2, 0x3e96872b # 0.294f */
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private void onKeyboardAttach ( ) {
/* .locals 3 */
/* .line 988 */
v0 = this.mAngleStateController;
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
int v2 = 1; // const/4 v2, 0x1
/* xor-int/2addr v1, v2 */
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).wakeUpIfNeed ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->wakeUpIfNeed(Z)V
/* .line 989 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setKeyboardStatus()V */
/* .line 990 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 991 */
/* const/16 v0, 0xa */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 992 */
/* const/16 v0, 0x9 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 993 */
/* const/16 v0, 0xc */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 994 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 995 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V */
/* .line 997 */
} // :cond_0
return;
} // .end method
private void onKeyboardDetach ( ) {
/* .locals 1 */
/* .line 1003 */
v0 = this.mDialog;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( miuix.appcompat.app.AlertDialog ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1004 */
v0 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).dismiss ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V
/* .line 1006 */
} // :cond_0
return;
} // .end method
private void onKeyboardStateChanged ( Integer p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "connectionType" # I */
/* .param p2, "connectionStatus" # Z */
/* .line 869 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).haveConnection ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->haveConnection()Z
/* .line 870 */
/* .local v0, "result":Z */
/* if-nez p1, :cond_0 */
/* .line 872 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V */
/* .line 874 */
/* if-nez p2, :cond_0 */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectBleLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 875 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* const/16 v2, 0x32 */
v1 = java.lang.Math .min ( v1,v2 );
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateBackLightBrightness(I)V */
/* .line 879 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
int v2 = 3; // const/4 v2, 0x3
final String v3 = "notifyIgnoredInputDevicesChanged"; // const-string v3, "notifyIgnoredInputDevicesChanged"
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
/* if-ne v1, v0, :cond_2 */
/* .line 881 */
/* if-nez p1, :cond_1 */
/* .line 882 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 884 */
/* iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
/* .line 885 */
/* iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
/* .line 886 */
/* const/16 v1, 0xc */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 887 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 888 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V */
/* .line 889 */
v1 = this.mSensorManager;
v5 = this.mAccSensor;
(( android.hardware.SensorManager ) v1 ).registerListener ( p0, v5, v2 ); // invoke-virtual {v1, p0, v5, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 892 */
} // :cond_1
v1 = this.mInputManager;
/* new-array v2, v4, [Ljava/lang/Object; */
com.android.server.input.ReflectionUtils .callPrivateMethod ( v1,v3,v2 );
/* .line 893 */
return;
/* .line 895 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Keyboard link state changed:"; // const-string v6, "Keyboard link state changed:"
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v6 = ", old status:"; // const-string v6, ", old status:"
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "MiuiPadKeyboardManager"; // const-string v6, "MiuiPadKeyboardManager"
android.util.Slog .i ( v6,v1 );
/* .line 896 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
/* .line 897 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 898 */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectIICLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 900 */
/* iput-boolean v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
/* .line 901 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardAttach()V */
/* .line 903 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateLightSensor()V */
/* .line 904 */
v1 = this.mSensorManager;
v5 = this.mAccSensor;
(( android.hardware.SensorManager ) v1 ).registerListener ( p0, v5, v2 ); // invoke-virtual {v1, p0, v5, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 906 */
} // :cond_4
v1 = this.mSensorManager;
(( android.hardware.SensorManager ) v1 ).unregisterListener ( p0 ); // invoke-virtual {v1, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 907 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->resetCalculateStatus()V */
/* .line 908 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardDetach()V */
/* .line 910 */
} // :goto_0
v1 = this.mInputManager;
/* new-array v2, v4, [Ljava/lang/Object; */
com.android.server.input.ReflectionUtils .callPrivateMethod ( v1,v3,v2 );
/* .line 911 */
return;
} // .end method
private java.lang.String parseAddress2String ( Object p0 ) {
/* .locals 2 */
/* .param p1, "address" # B */
/* .line 857 */
final String v0 = ""; // const-string v0, ""
/* .line 858 */
/* .local v0, "target":Ljava/lang/String; */
/* const/16 v1, 0x18 */
/* if-ne p1, v1, :cond_0 */
/* .line 859 */
final String v0 = "MCU"; // const-string v0, "MCU"
/* .line 860 */
} // :cond_0
/* const/16 v1, 0x40 */
/* if-ne p1, v1, :cond_1 */
/* .line 861 */
final String v0 = "TouchPad"; // const-string v0, "TouchPad"
/* .line 862 */
} // :cond_1
/* const/16 v1, 0x38 */
/* if-ne p1, v1, :cond_2 */
/* .line 863 */
final String v0 = "Keyboard"; // const-string v0, "Keyboard"
/* .line 865 */
} // :cond_2
} // :goto_0
} // .end method
private void postShowRejectConfirmDialog ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 1117 */
com.android.server.UiThread .getHandler ( );
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1120 */
return;
} // .end method
private void processIdentity ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "identity" # I */
/* .line 1068 */
int v0 = 5; // const/4 v0, 0x5
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 1101 */
/* :pswitch_0 */
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->postShowRejectConfirmDialog(I)V */
/* .line 1102 */
final String v2 = "keyboard identity transfer error"; // const-string v2, "keyboard identity transfer error"
android.util.Slog .i ( v1,v2 );
/* .line 1103 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1104 */
/* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V */
/* .line 1105 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1106 */
/* .line 1095 */
/* :pswitch_1 */
final String v2 = "keyboard identity internal error"; // const-string v2, "keyboard identity internal error"
android.util.Slog .i ( v1,v2 );
/* .line 1096 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1097 */
/* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V */
/* .line 1098 */
/* .line 1085 */
/* :pswitch_2 */
/* iget v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I */
/* add-int/2addr v4, v2 */
/* iput v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I */
/* if-ge v4, v0, :cond_0 */
/* .line 1086 */
final String v2 = "keyboard identity need check again"; // const-string v2, "keyboard identity need check again"
android.util.Slog .i ( v1,v2 );
/* .line 1087 */
/* const/16 v1, 0x1388 */
/* invoke-direct {p0, v1, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V */
/* .line 1088 */
/* .line 1090 */
} // :cond_0
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1091 */
/* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V */
/* .line 1092 */
/* .line 1077 */
/* :pswitch_3 */
/* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->postShowRejectConfirmDialog(I)V */
/* .line 1078 */
final String v2 = "keyboard identity auth reject"; // const-string v2, "keyboard identity auth reject"
android.util.Slog .i ( v1,v2 );
/* .line 1079 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1080 */
/* invoke-direct {p0, v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V */
/* .line 1081 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1082 */
/* .line 1070 */
/* :pswitch_4 */
final String v4 = "keyboard identity auth ok"; // const-string v4, "keyboard identity auth ok"
android.util.Slog .i ( v1,v4 );
/* .line 1071 */
v1 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v1 ).setIdentityState ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/AngleStateController;->setIdentityState(Z)V
/* .line 1072 */
/* invoke-direct {p0, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setAuthState(Z)V */
/* .line 1073 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 1074 */
/* nop */
/* .line 1109 */
} // :goto_0
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_1 */
/* if-ne p1, v1, :cond_2 */
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I */
/* if-ne v1, v0, :cond_2 */
/* .line 1112 */
} // :cond_1
/* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z */
/* .line 1114 */
} // :cond_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void releaseWakeLock ( ) {
/* .locals 1 */
/* .line 1216 */
v0 = this.mWakeLock;
v0 = (( android.os.PowerManager$WakeLock ) v0 ).isHeld ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1217 */
v0 = this.mWakeLock;
(( android.os.PowerManager$WakeLock ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 1219 */
} // :cond_0
return;
} // .end method
private void resetCalculateStatus ( ) {
/* .locals 3 */
/* .line 1208 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z */
/* .line 1209 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
/* .line 1210 */
v1 = this.mLocalGData;
int v2 = 0; // const/4 v2, 0x0
/* aput v2, v1, v0 */
/* .line 1211 */
int v0 = 1; // const/4 v0, 0x1
/* aput v2, v1, v0 */
/* .line 1212 */
int v0 = 2; // const/4 v0, 0x2
/* aput v2, v1, v0 */
/* .line 1213 */
return;
} // .end method
private void setAuthState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "state" # Z */
/* .line 1021 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityPass:Z */
/* .line 1022 */
return;
} // .end method
private void setBacklight ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "brightness" # I */
/* .line 1247 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1248 */
/* .local v0, "data":Landroid/os/Bundle; */
/* const-string/jumbo v1, "value" */
/* int-to-byte v2, p1 */
(( android.os.Bundle ) v0 ).putByte ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V
/* .line 1249 */
/* const/16 v1, 0xb */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V
/* .line 1250 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set keyboard backLight brightness " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 1251 */
return;
} // .end method
private void setKeyboardStatus ( ) {
/* .locals 2 */
/* .line 1255 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
/* if-nez v0, :cond_0 */
/* .line 1256 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
final String v1 = "Abandon Communicate with keyboard because screen"; // const-string v1, "Abandon Communicate with keyboard because screen"
android.util.Slog .i ( v0,v1 );
/* .line 1257 */
return;
/* .line 1260 */
} // :cond_0
v0 = this.mAngleStateController;
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
/* if-nez v0, :cond_1 */
/* .line 1261 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).getCapsLockStatus ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->getCapsLockStatus()Z
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).setCapsLockLight ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setCapsLockLight(Z)V
/* .line 1262 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V */
/* .line 1263 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).getMuteStatus ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->getMuteStatus()Z
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).setMuteLight ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setMuteLight(Z)V
/* .line 1265 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).setCapsLockLight ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setCapsLockLight(Z)V
/* .line 1266 */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V */
/* .line 1267 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).setMuteLight ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setMuteLight(Z)V
/* .line 1269 */
} // :goto_0
return;
} // .end method
public static Integer shouldClearActivityInfoFlags ( ) {
/* .locals 1 */
/* .line 914 */
/* const/16 v0, 0x70 */
} // .end method
private void showRejectConfirmDialog ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 1128 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getSystemUiContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;
/* .line 1130 */
/* .local v0, "systemuiContext":Landroid/content/Context; */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1140 */
return;
/* .line 1136 */
/* :sswitch_0 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1137 */
/* const v2, 0x110f024c */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1138 */
/* .local v1, "message":Ljava/lang/String; */
/* .line 1132 */
} // .end local v1 # "message":Ljava/lang/String;
/* :sswitch_1 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1133 */
/* const v2, 0x110f024b */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1134 */
/* .restart local v1 # "message":Ljava/lang/String; */
/* nop */
/* .line 1143 */
} // :goto_0
v2 = this.mDialog;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1144 */
(( miuix.appcompat.app.AlertDialog ) v2 ).setMessage ( v1 ); // invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V
/* .line 1146 */
} // :cond_0
/* new-instance v2, Lmiuix/appcompat/app/AlertDialog$Builder; */
/* const v3, 0x66110006 */
/* invoke-direct {v2, v0, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V */
/* .line 1148 */
int v3 = 1; // const/4 v3, 0x1
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setCancelable ( v3 ); // invoke-virtual {v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1149 */
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setMessage ( v1 ); // invoke-virtual {v2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1150 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1151 */
/* const v4, 0x110f024a */
(( android.content.res.Resources ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1150 */
int v4 = 0; // const/4 v4, 0x0
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).setPositiveButton ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;
/* .line 1153 */
(( miuix.appcompat.app.AlertDialog$Builder ) v2 ).create ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;
this.mDialog = v2;
/* .line 1154 */
(( miuix.appcompat.app.AlertDialog ) v2 ).getWindow ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v2 ).getAttributes ( ); // invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
/* .line 1155 */
/* .local v2, "attrs":Landroid/view/WindowManager$LayoutParams; */
/* const/16 v3, 0x7d3 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 1156 */
/* iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v4, 0x20000 */
/* or-int/2addr v3, v4 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 1157 */
/* const/16 v3, 0x11 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 1158 */
/* iget v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit16 v3, v3, 0x110 */
/* iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 1160 */
v3 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v3 ).getWindow ( ); // invoke-virtual {v3}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;
(( android.view.Window ) v3 ).setAttributes ( v2 ); // invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
/* .line 1162 */
} // .end local v2 # "attrs":Landroid/view/WindowManager$LayoutParams;
} // :goto_1
v2 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v2 ).show ( ); // invoke-virtual {v2}, Lmiuix/appcompat/app/AlertDialog;->show()V
/* .line 1163 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x4 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void startCheckIdentity ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "delay" # I */
/* .param p2, "isFirst" # Z */
/* .line 969 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 970 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).stayAwake ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stayAwake()V
/* .line 971 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 972 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 973 */
return;
/* .line 975 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z */
/* .line 976 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityTimes:I */
/* .line 978 */
} // :cond_1
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 979 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "is_first_check"; // const-string v1, "is_first_check"
(( android.os.Bundle ) v0 ).putBoolean ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 980 */
int v1 = 5; // const/4 v1, 0x5
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v1, v0, p1 ); // invoke-virtual {p0, v1, v0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;I)V
/* .line 982 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // :cond_2
return;
} // .end method
private void stayWakeForKeyboard176 ( ) {
/* .locals 4 */
/* .line 1232 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z */
/* if-nez v0, :cond_0 */
/* .line 1233 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 1234 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x2710 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1235 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z */
/* .line 1237 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
return;
} // .end method
private void stopAllCommandWorker ( ) {
/* .locals 3 */
/* .line 1009 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 1010 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).stopStayAwake ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopStayAwake()V
/* .line 1011 */
v0 = this.mHandler;
v1 = this.mIICNodeHelper;
java.util.Objects .requireNonNull ( v1 );
/* new-instance v2, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1012 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z */
/* .line 1013 */
return;
} // .end method
private void stopWakeForKeyboard176 ( ) {
/* .locals 2 */
/* .line 1240 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1241 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldStayWakeKeyboard:Z */
/* .line 1242 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1244 */
} // :cond_0
return;
} // .end method
public static Boolean supportPadKeyboard ( ) {
/* .locals 2 */
/* .line 189 */
/* const-string/jumbo v0, "support_iic_keyboard" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
} // .end method
private void updateBackLightBrightness ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # I */
/* .line 696 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* if-eq v0, p1, :cond_0 */
/* .line 697 */
v0 = this.mBluetoothKeyboardManager;
(( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ) v0 ).wakeUpKeyboardIfNeed ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->wakeUpKeyboardIfNeed()V
/* .line 698 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V */
/* .line 700 */
} // :cond_0
return;
} // .end method
private void updateLightSensor ( ) {
/* .locals 3 */
/* .line 923 */
v0 = this.mSensorManager;
v1 = this.mLightSensor;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
/* .line 924 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsAutoBackLight:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 925 */
v0 = this.mSensorManager;
v1 = this.mLightSensor;
int v2 = 3; // const/4 v2, 0x3
(( android.hardware.SensorManager ) v0 ).registerListener ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 927 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public commandMiAuthStep3Type1 ( Object[] p0, Object[] p1 ) {
/* .locals 1 */
/* .param p1, "keyMeta" # [B */
/* .param p2, "challenge" # [B */
/* .line 645 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .commandMiAuthStep3Type1ForIIC ( p1,p2 );
} // .end method
public commandMiAuthStep5Type1 ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "token" # [B */
/* .line 650 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .commandMiAuthStep5Type1ForIIC ( p1 );
} // .end method
public commandMiDevAuthInit ( ) {
/* .locals 1 */
/* .line 640 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .commandMiDevAuthInitForIIC ( );
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 1 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 736 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 737 */
final String v0 = "MIuiI2CKeyboardManager"; // const-string v0, "MIuiI2CKeyboardManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 739 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 740 */
final String v0 = "mMcuVersion="; // const-string v0, "mMcuVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 741 */
v0 = this.mMCUVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 742 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 743 */
final String v0 = "mKeyboardVersion="; // const-string v0, "mKeyboardVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 744 */
v0 = this.mKeyboardVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 745 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 746 */
final String v0 = "mTouchPadVersion="; // const-string v0, "mTouchPadVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 747 */
v0 = this.mTouchPadVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 748 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 749 */
final String v0 = "AuthState="; // const-string v0, "AuthState="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 750 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mCheckIdentityPass:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 751 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 752 */
final String v0 = "mIsKeyboardReady="; // const-string v0, "mIsKeyboardReady="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 753 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 754 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 755 */
final String v0 = "isKeyboardDisable="; // const-string v0, "isKeyboardDisable="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 756 */
v0 = this.mAngleStateController;
v0 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 757 */
final String v0 = "mAuthStarted="; // const-string v0, "mAuthStarted="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 758 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mAuthStarted:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 759 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHasTouchPad:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 760 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 761 */
final String v0 = "hasTouchPad="; // const-string v0, "hasTouchPad="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 762 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHasTouchPad:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 763 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 764 */
final String v0 = "backLightBrightness="; // const-string v0, "backLightBrightness="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 765 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 768 */
} // :cond_0
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/padkeyboard/AngleStateController;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 770 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 771 */
return;
} // .end method
public void enableOrDisableInputDevice ( ) {
/* .locals 2 */
/* .line 376 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 377 */
return;
} // .end method
public Integer getKeyboardBackLightBrightness ( ) {
/* .locals 1 */
/* .line 704 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
} // .end method
public void getKeyboardReportData ( ) {
/* .locals 1 */
/* .line 371 */
int v0 = 3; // const/4 v0, 0x3
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 372 */
return;
} // .end method
public miui.hardware.input.MiuiKeyboardStatus getKeyboardStatus ( ) {
/* .locals 10 */
/* .line 655 */
/* new-instance v9, Lmiui/hardware/input/MiuiKeyboardStatus; */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
v0 = this.mAngleStateController;
/* .line 656 */
v2 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).getIdentityStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getIdentityStatus()Z
v0 = this.mAngleStateController;
/* .line 657 */
v3 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).isWorkState ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->isWorkState()Z
v0 = this.mAngleStateController;
/* .line 658 */
v4 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).getLidStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getLidStatus()Z
v0 = this.mAngleStateController;
/* .line 659 */
v5 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).getTabletStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->getTabletStatus()Z
v0 = this.mAngleStateController;
/* .line 660 */
v6 = (( com.android.server.input.padkeyboard.AngleStateController ) v0 ).shouldIgnoreKeyboardForIIC ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController;->shouldIgnoreKeyboardForIIC()Z
v7 = this.mMCUVersion;
v8 = this.mKeyboardVersion;
/* move-object v0, v9 */
/* invoke-direct/range {v0 ..v8}, Lmiui/hardware/input/MiuiKeyboardStatus;-><init>(ZZZZZZLjava/lang/String;Ljava/lang/String;)V */
/* .line 655 */
} // .end method
public Integer getKeyboardType ( ) {
/* .locals 2 */
/* .line 1301 */
v0 = this.mlock;
/* monitor-enter v0 */
/* .line 1302 */
try { // :try_start_0
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardType:I */
/* monitor-exit v0 */
/* .line 1303 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean isIICKeyboardActive ( ) {
/* .locals 1 */
/* .line 946 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isKeyboardReady ( ) {
/* .locals 1 */
/* .line 459 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyLidSwitchChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "lidOpen" # Z */
/* .line 304 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Lid Switch Changed to "; // const-string v1, "Lid Switch Changed to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 305 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).notifyLidSwitchChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyLidSwitchChanged(Z)V
/* .line 306 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 307 */
return;
} // .end method
public void notifyScreenState ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "screenState" # Z */
/* .line 327 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
/* if-ne v0, p1, :cond_0 */
/* .line 328 */
return;
/* .line 330 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
/* .line 331 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Notify Screen State changed to "; // const-string v1, "Notify Screen State changed to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 332 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 333 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v2, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
/* const-wide/16 v3, 0x12c */
(( android.os.Handler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 336 */
v1 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 337 */
v1 = this.mBluetoothKeyboardManager;
(( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ) v1 ).notifyUpgradeIfNeed ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->notifyUpgradeIfNeed()V
/* .line 340 */
} // :cond_1
v1 = this.mContext;
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v1 );
(( com.android.server.policy.MiuiKeyInterceptExtend ) v1 ).setKeyboardShortcutEnable ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V
/* .line 344 */
} // :cond_2
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V */
/* .line 345 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 346 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 347 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateLightSensor()V */
/* .line 349 */
} // :cond_3
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .supportCalculateAngle ( );
/* const-wide/16 v1, 0xc8 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 350 */
v0 = this.mSensorManager;
v3 = this.mAccSensor;
int v4 = 3; // const/4 v4, 0x3
(( android.hardware.SensorManager ) v0 ).registerListener ( p0, v3, v4 ); // invoke-virtual {v0, p0, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
/* .line 352 */
v0 = this.mHandler;
/* new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
(( android.os.Handler ) v0 ).postDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 354 */
} // :cond_4
v0 = this.mHandler;
/* new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda3; */
/* invoke-direct {v3, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
(( android.os.Handler ) v0 ).postDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 359 */
} // :cond_5
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z */
/* .line 360 */
v0 = this.mDialog;
if ( v0 != null) { // if-eqz v0, :cond_6
v0 = (( miuix.appcompat.app.AlertDialog ) v0 ).isShowing ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->isShowing()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 361 */
v0 = this.mDialog;
(( miuix.appcompat.app.AlertDialog ) v0 ).dismiss ( ); // invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V
/* .line 363 */
} // :cond_6
v0 = this.mSensorManager;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( p0 ); // invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 364 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 365 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->resetCalculateStatus()V */
/* .line 367 */
} // :goto_1
return;
} // .end method
public void notifyTabletSwitchChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "tabletOpen" # Z */
/* .line 311 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TabletSwitch changed to "; // const-string v1, "TabletSwitch changed to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 312 */
v0 = this.mAngleStateController;
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).notifyTabletSwitchChanged ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->notifyTabletSwitchChanged(Z)V
/* .line 313 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 314 */
com.miui.server.input.PadManager .getInstance ( );
(( com.miui.server.input.PadManager ) v0 ).setIsTableOpen ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/PadManager;->setIsTableOpen(Z)V
/* .line 315 */
return;
} // .end method
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 241 */
return;
} // .end method
public void onHallStatusChanged ( Object p0 ) {
/* .locals 4 */
/* .param p1, "status" # B */
/* .line 319 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .byte2int ( p1 );
/* and-int/lit8 v0, v0, 0x10 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
/* .line 320 */
/* .local v0, "lidStatus":Z */
} // :goto_0
v3 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .byte2int ( p1 );
/* and-int/2addr v3, v2 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* move v1, v2 */
/* .line 321 */
/* .local v1, "tabletStatus":Z */
} // :cond_1
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).notifyLidSwitchChanged ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->notifyLidSwitchChanged(Z)V
/* .line 322 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).notifyTabletSwitchChanged ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->notifyTabletSwitchChanged(Z)V
/* .line 323 */
return;
} // .end method
public void onKeyboardEnableStateChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isEnable" # Z */
/* .line 714 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receiver keyboard enable status change to "; // const-string v1, "receiver keyboard enable status change to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 715 */
v0 = this.mAngleStateController;
/* xor-int/lit8 v1, p1, 0x1 */
(( com.android.server.input.padkeyboard.AngleStateController ) v0 ).setShouldIgnoreKeyboardFromKeyboard ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController;->setShouldIgnoreKeyboardFromKeyboard(Z)V
/* .line 716 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).enableOrDisableInputDevice ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->enableOrDisableInputDevice()V
/* .line 717 */
return;
} // .end method
public void onKeyboardGSensorChanged ( Float p0, Float p1, Float p2 ) {
/* .locals 3 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .param p3, "z" # F */
/* .line 589 */
v0 = this.mKeyboardGData;
int v1 = 0; // const/4 v1, 0x0
/* aput p1, v0, v1 */
/* .line 590 */
int v1 = 1; // const/4 v1, 0x1
/* aput p2, v0, v1 */
/* .line 591 */
int v2 = 2; // const/4 v2, 0x2
/* aput p3, v0, v2 */
/* .line 592 */
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardAccReady:Z */
/* .line 593 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->calculateAngle()V */
/* .line 594 */
return;
} // .end method
public void onKeyboardSleepStatusChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isSleep" # Z */
/* .line 598 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 599 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z */
/* .line 600 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Keyboard will sleep:"; // const-string v1, "Keyboard will sleep:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 601 */
v0 = this.mBluetoothKeyboardManager;
(( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ) v0 ).setIICKeyboardSleepStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->setIICKeyboardSleepStatus(Z)V
/* .line 602 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardSleep:Z */
/* if-nez v0, :cond_0 */
/* .line 603 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V */
/* .line 606 */
} // :cond_0
return;
} // .end method
public void onKeyboardStatusChanged ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "connectionType" # I */
/* .param p2, "connectionStatus" # Z */
/* .line 709 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onKeyboardStateChanged(IZ)V */
/* .line 710 */
return;
} // .end method
public void onNFCTouched ( ) {
/* .locals 5 */
/* .line 610 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mScreenState:Z */
/* if-nez v0, :cond_0 */
/* .line 611 */
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 0; // const/4 v3, 0x0
final String v4 = "NFC Device Touched"; // const-string v4, "NFC Device Touched"
(( android.os.PowerManager ) v0 ).wakeUp ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
/* .line 614 */
} // :cond_0
return;
} // .end method
public void onOtaErrorInfo ( Object p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "targetAddress" # B */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 551 */
int v0 = 0; // const/4 v0, 0x0
int v1 = -1; // const/4 v1, -0x1
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).onOtaErrorInfo ( p1, p2, v0, v1 ); // invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->onOtaErrorInfo(BLjava/lang/String;ZI)V
/* .line 552 */
return;
} // .end method
public void onOtaErrorInfo ( Object p0, java.lang.String p1, Boolean p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "targetAddress" # B */
/* .param p2, "reason" # Ljava/lang/String; */
/* .param p3, "isNeedToast" # Z */
/* .param p4, "resId" # I */
/* .line 556 */
int v0 = 0; // const/4 v0, 0x0
if ( p3 != null) { // if-eqz p3, :cond_0
int v1 = -1; // const/4 v1, -0x1
/* if-eq p4, v1, :cond_0 */
/* .line 557 */
v1 = this.mContext;
/* .line 558 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getString ( p4 ); // invoke-virtual {v2, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 557 */
android.widget.Toast .makeText ( v1,v2,v0 );
/* .line 559 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 561 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->parseAddress2String(B)Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " OTA Fail because: "; // const-string v2, " OTA Fail because: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .e ( v2,v1 );
/* .line 562 */
if ( p2 != null) { // if-eqz p2, :cond_4
final String v1 = "The device version doesn\'t need upgrading"; // const-string v1, "The device version doesn\'t need upgrading"
v1 = (( java.lang.String ) p2 ).contains ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_1 */
final String v1 = "Invalid upgrade file"; // const-string v1, "Invalid upgrade file"
v1 = (( java.lang.String ) p2 ).contains ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 563 */
} // :cond_1
/* const/16 v1, 0x38 */
/* if-ne p1, v1, :cond_2 */
/* .line 564 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
/* .line 565 */
} // :cond_2
/* const/16 v1, 0x40 */
/* if-ne p1, v1, :cond_3 */
/* .line 566 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
/* .line 567 */
} // :cond_3
/* const/16 v1, 0x18 */
/* if-ne p1, v1, :cond_4 */
/* .line 568 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z */
/* .line 571 */
} // :cond_4
} // :goto_0
return;
} // .end method
public void onOtaProgress ( Object p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "targetAddress" # B */
/* .param p2, "pro" # F */
/* .line 580 */
/* const/high16 v0, 0x42480000 # 50.0f */
/* cmpl-float v0, p2, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/high16 v0, 0x42c80000 # 100.0f */
/* cmpl-float v0, p2, v0 */
/* if-nez v0, :cond_1 */
/* .line 581 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Current upgrade progress\uff1a type = "; // const-string v1, "Current upgrade progress\uff1a type = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->parseAddress2String(B)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", process: "; // const-string v1, ", process: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 585 */
} // :cond_1
return;
} // .end method
public void onOtaStateChange ( Object p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "targetAddress" # B */
/* .param p2, "status" # I */
/* .line 494 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->parseAddress2String(B)Ljava/lang/String; */
/* .line 495 */
/* .local v0, "target":Ljava/lang/String; */
/* const/16 v1, 0x38 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
int v3 = 0; // const/4 v3, 0x0
/* if-nez p2, :cond_0 */
/* .line 496 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " OTA Start!"; // const-string v5, " OTA Start!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* .line 497 */
/* if-ne p1, v1, :cond_0 */
/* .line 498 */
v4 = this.mContext;
/* .line 499 */
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v6, 0x110f024e */
(( android.content.res.Resources ) v5 ).getString ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 498 */
android.widget.Toast .makeText ( v4,v5,v3 );
/* .line 501 */
(( android.widget.Toast ) v4 ).show ( ); // invoke-virtual {v4}, Landroid/widget/Toast;->show()V
/* .line 505 */
} // :cond_0
int v4 = 2; // const/4 v4, 0x2
/* if-ne p2, v4, :cond_4 */
/* .line 506 */
/* if-ne p1, v1, :cond_1 */
/* .line 507 */
v1 = this.mContext;
/* .line 508 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x110f024f */
(( android.content.res.Resources ) v4 ).getString ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 507 */
android.widget.Toast .makeText ( v1,v4,v3 );
/* .line 510 */
(( android.widget.Toast ) v1 ).show ( ); // invoke-virtual {v1}, Landroid/widget/Toast;->show()V
/* .line 511 */
/* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeKeyboard:Z */
/* .line 512 */
} // :cond_1
/* const/16 v1, 0x40 */
/* if-ne p1, v1, :cond_2 */
/* .line 513 */
/* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeTouchPad:Z */
/* .line 514 */
} // :cond_2
/* const/16 v1, 0x18 */
/* if-ne p1, v1, :cond_3 */
/* .line 515 */
/* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mShouldUpgradeMCU:Z */
/* .line 517 */
} // :cond_3
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " OTA Successfully!"; // const-string v3, " OTA Successfully!"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v1 );
/* .line 519 */
} // :cond_4
return;
} // .end method
public void onReadSocketNumError ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 575 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "read socket find num error:"; // const-string v1, "read socket find num error:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .e ( v1,v0 );
/* .line 576 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 194 */
v0 = (( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).isKeyboardReady ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isKeyboardReady()Z
/* if-nez v0, :cond_0 */
/* .line 195 */
return;
/* .line 198 */
} // :cond_0
v0 = this.sensor;
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_4 */
/* .line 200 */
v0 = this.values;
/* aget v0, v0, v2 */
v3 = this.values;
/* aget v3, v3, v1 */
v4 = this.values;
int v5 = 2; // const/4 v5, 0x2
/* aget v4, v4, v5 */
v0 = /* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->motionJudge(FFF)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 201 */
/* iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
/* .line 202 */
return;
/* .line 205 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.values;
/* aget v0, v0, v2 */
v3 = this.mLocalGData;
/* aget v3, v3, v2 */
/* sub-float/2addr v0, v3 */
/* .line 206 */
v0 = java.lang.Math .abs ( v0 );
/* const v3, 0x3e96872b # 0.294f */
/* cmpl-float v0, v0, v3 */
/* if-gtz v0, :cond_2 */
v0 = this.values;
/* aget v0, v0, v1 */
v3 = this.mLocalGData;
/* aget v3, v3, v1 */
/* sub-float/2addr v0, v3 */
/* .line 207 */
v0 = java.lang.Math .abs ( v0 );
/* const v3, 0x3e48b439 */
/* cmpl-float v0, v0, v3 */
/* if-gtz v0, :cond_2 */
v0 = this.values;
/* aget v0, v0, v5 */
v3 = this.mLocalGData;
/* aget v3, v3, v5 */
/* sub-float/2addr v0, v3 */
/* .line 208 */
v0 = java.lang.Math .abs ( v0 );
/* const v3, 0x3f16872b # 0.588f */
/* cmpl-float v0, v0, v3 */
/* if-lez v0, :cond_7 */
/* .line 210 */
} // :cond_2
v0 = this.values;
v3 = this.mLocalGData;
/* array-length v4, v3 */
java.lang.System .arraycopy ( v0,v2,v3,v2,v4 );
/* .line 211 */
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mPadAccReady:Z */
/* .line 212 */
v0 = (( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).isIICKeyboardActive ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isIICKeyboardActive()Z
/* if-nez v0, :cond_3 */
/* .line 214 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).wakeKeyboard176 ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->wakeKeyboard176()V
/* .line 216 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->calculateAngle()V */
/* .line 218 */
} // :cond_4
v0 = this.sensor;
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_7 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z */
/* if-nez v0, :cond_7 */
/* .line 220 */
v0 = this.values;
/* aget v0, v0, v2 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .getBackLightBrightnessWithSensor ( v0 );
/* .line 222 */
/* .local v0, "brightness":I */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mIsKeyboardReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_7
/* iget v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* if-eq v0, v1, :cond_7 */
/* .line 223 */
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectIICLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 224 */
v1 = (( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).isIICKeyboardActive ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->isIICKeyboardActive()Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 225 */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V */
/* .line 228 */
} // :cond_5
/* iput v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mBackLightBrightness:I */
/* .line 230 */
} // :cond_6
v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v1 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).isConnectBleLocked ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 232 */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->setBacklight(I)V */
/* .line 236 */
} // .end local v0 # "brightness":I
} // :cond_7
} // :goto_0
return;
} // .end method
public void onUpdateKeyboardType ( Object p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "type" # B */
/* .param p2, "hasTouchPad" # Z */
/* .line 484 */
/* iput-boolean p2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mHasTouchPad:Z */
/* .line 485 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 487 */
int v1 = 0; // const/4 v1, 0x0
if ( p2 != null) { // if-eqz p2, :cond_0
int v2 = 1; // const/4 v2, 0x1
/* .line 488 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
/* nop */
/* .line 485 */
final String v3 = "keyboard_type_level"; // const-string v3, "keyboard_type_level"
int v4 = -2; // const/4 v4, -0x2
android.provider.Settings$Secure .putIntForUser ( v0,v3,v2,v4 );
/* .line 489 */
v0 = this.mInputManager;
final String v2 = "notifyIgnoredInputDevicesChanged"; // const-string v2, "notifyIgnoredInputDevicesChanged"
/* new-array v1, v1, [Ljava/lang/Object; */
com.android.server.input.ReflectionUtils .callPrivateMethod ( v0,v2,v1 );
/* .line 490 */
return;
} // .end method
public void onUpdateVersion ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "version" # Ljava/lang/String; */
/* .line 464 */
int v0 = 0; // const/4 v0, 0x0
/* .line 465 */
/* .local v0, "device":Ljava/lang/String; */
/* const/16 v1, 0x14 */
/* if-ne p1, v1, :cond_0 */
/* .line 466 */
final String v0 = "Keyboard"; // const-string v0, "Keyboard"
/* .line 467 */
this.mKeyboardVersion = p2;
/* .line 468 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).getKeyboardReportData ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->getKeyboardReportData()V
/* .line 469 */
} // :cond_0
int v1 = 3; // const/4 v1, 0x3
/* if-ne p1, v1, :cond_1 */
/* .line 470 */
final String v0 = "Flash Keyboard"; // const-string v0, "Flash Keyboard"
/* .line 471 */
} // :cond_1
/* const/16 v1, 0xa */
/* if-ne p1, v1, :cond_2 */
/* .line 472 */
final String v0 = "Pad MCU"; // const-string v0, "Pad MCU"
/* .line 473 */
this.mMCUVersion = p2;
/* .line 474 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .setMcuVersion ( p2 );
/* .line 475 */
} // :cond_2
/* const/16 v1, 0x28 */
/* if-ne p1, v1, :cond_3 */
/* .line 476 */
final String v0 = "TouchPad"; // const-string v0, "TouchPad"
/* .line 477 */
this.mTouchPadVersion = p2;
/* .line 479 */
} // :cond_3
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getVersion for "; // const-string v2, "getVersion for "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " is :"; // const-string v2, " is :"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 480 */
return;
} // .end method
public void onWriteSocketErrorInfo ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 523 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Write Socket Fail because:"; // const-string v1, "Write Socket Fail because:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .e ( v1,v0 );
/* .line 524 */
final String v0 = "Write data Socket exception"; // const-string v0, "Write data Socket exception"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 525 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopAllCommandWorker()V */
/* .line 527 */
} // :cond_0
return;
} // .end method
public void readHallStatus ( ) {
/* .locals 1 */
/* .line 626 */
/* const/16 v0, 0x8 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 627 */
return;
} // .end method
public void readKeyboardStatus ( ) {
/* .locals 1 */
/* .line 621 */
int v0 = 6; // const/4 v0, 0x6
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(I)V
/* .line 622 */
return;
} // .end method
public void registerMuteLightReceiver ( ) {
/* .locals 3 */
/* .line 1279 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.media.action.MICROPHONE_MUTE_CHANGED"; // const-string v1, "android.media.action.MICROPHONE_MUTE_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 1280 */
/* .local v0, "muteFilter":Landroid/content/IntentFilter; */
v1 = this.mContext;
v2 = this.mMicMuteReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1281 */
return;
} // .end method
public android.view.InputDevice removeKeyboardDevicesIfNeeded ( android.view.InputDevice[] p0 ) {
/* .locals 8 */
/* .param p1, "allInputDevices" # [Landroid/view/InputDevice; */
/* .line 245 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 246 */
/* .local v0, "tempDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/InputDevice;>;" */
v1 = this.mHandler;
v2 = this.mBleDeviceList;
java.util.Objects .requireNonNull ( v2 );
/* new-instance v3, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda6; */
/* invoke-direct {v3, v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda6;-><init>(Ljava/util/Set;)V */
(( android.os.Handler ) v1 ).post ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 247 */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_9 */
/* aget-object v4, p1, v3 */
/* .line 248 */
/* .local v4, "device":Landroid/view/InputDevice; */
v5 = (( android.view.InputDevice ) v4 ).getId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getId()I
/* .line 249 */
/* .local v5, "deviceId":I */
v6 = (( android.view.InputDevice ) v4 ).getVendorId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I
/* const/16 v7, 0x15d9 */
/* if-ne v6, v7, :cond_6 */
/* .line 250 */
v6 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v7, 0xa3 */
/* if-ne v6, v7, :cond_1 */
/* .line 251 */
/* iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
/* if-eq v6, v5, :cond_0 */
/* .line 252 */
/* iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardDeviceId:I */
/* .line 254 */
} // :cond_0
this.mKeyboardDevice = v4;
/* .line 255 */
v6 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v6 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v6 ).isConnectIICLocked ( ); // invoke-virtual {v6}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v6, :cond_8 */
/* .line 256 */
/* goto/16 :goto_1 */
/* .line 258 */
} // :cond_1
v6 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v7, 0xa4 */
/* if-ne v6, v7, :cond_3 */
/* .line 260 */
/* iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
/* if-eq v6, v5, :cond_2 */
/* .line 261 */
/* iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mConsumerDeviceId:I */
/* .line 263 */
} // :cond_2
v6 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v6 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v6 ).isConnectIICLocked ( ); // invoke-virtual {v6}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v6, :cond_8 */
/* .line 264 */
/* .line 266 */
} // :cond_3
v6 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v7, 0xa1 */
/* if-ne v6, v7, :cond_5 */
/* .line 268 */
/* iget v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
/* if-eq v6, v5, :cond_4 */
/* .line 269 */
/* iput v5, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mTouchDeviceId:I */
/* .line 271 */
} // :cond_4
v6 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v6 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v6 ).isConnectIICLocked ( ); // invoke-virtual {v6}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v6, :cond_8 */
/* .line 272 */
/* .line 274 */
} // :cond_5
v6 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v7, 0xa2 */
/* if-ne v6, v7, :cond_8 */
/* .line 276 */
/* .line 278 */
} // :cond_6
v6 = (( android.view.InputDevice ) v4 ).getVendorId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I
/* const v7, 0xbf01 */
/* if-ne v6, v7, :cond_8 */
/* .line 279 */
v6 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v7, 0x40 */
/* if-ne v6, v7, :cond_8 */
/* .line 280 */
v6 = this.mHandler;
/* new-instance v7, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda7; */
/* invoke-direct {v7, p0, v5}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda7;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;I)V */
(( android.os.Handler ) v6 ).post ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 281 */
v6 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 282 */
v6 = (( android.view.InputDevice ) v4 ).getSources ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getSources()I
/* const/16 v7, 0x301 */
/* and-int/2addr v6, v7 */
/* if-eq v6, v7, :cond_8 */
/* .line 285 */
/* .line 290 */
} // :cond_7
v6 = (( android.view.InputDevice ) v4 ).getSources ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getSources()I
/* const/16 v7, 0x2303 */
/* and-int/2addr v6, v7 */
/* if-eq v6, v7, :cond_8 */
/* .line 293 */
/* .line 297 */
} // :cond_8
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 247 */
} // .end local v4 # "device":Landroid/view/InputDevice;
} // .end local v5 # "deviceId":I
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_0 */
/* .line 299 */
} // :cond_9
/* new-array v1, v2, [Landroid/view/InputDevice; */
(( java.util.ArrayList ) v0 ).toArray ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v1, [Landroid/view/InputDevice; */
} // .end method
public void requestReAuth ( ) {
/* .locals 2 */
/* .line 731 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->startCheckIdentity(IZ)V */
/* .line 732 */
return;
} // .end method
public void requestReleaseAwake ( ) {
/* .locals 0 */
/* .line 726 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).stopStayAwake ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stopStayAwake()V
/* .line 727 */
return;
} // .end method
public void requestStayAwake ( ) {
/* .locals 0 */
/* .line 721 */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).stayAwake ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->stayAwake()V
/* .line 722 */
return;
} // .end method
void sendCommand ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "command" # I */
/* .line 930 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V
/* .line 931 */
return;
} // .end method
void sendCommand ( Integer p0, android.os.Bundle p1 ) {
/* .locals 1 */
/* .param p1, "command" # I */
/* .param p2, "data" # Landroid/os/Bundle; */
/* .line 934 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;I)V
/* .line 935 */
return;
} // .end method
void sendCommand ( Integer p0, android.os.Bundle p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "command" # I */
/* .param p2, "data" # Landroid/os/Bundle; */
/* .param p3, "delay" # I */
/* .line 938 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 939 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p1, v0, Landroid/os/Message;->what:I */
/* .line 940 */
(( android.os.Message ) v0 ).setData ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 941 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).removeMessages ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 942 */
v1 = this.mHandler;
/* int-to-long v2, p3 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 943 */
return;
} // .end method
public sendCommandForRespond ( Object[] p0, com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback p1 ) {
/* .locals 2 */
/* .param p1, "command" # [B */
/* .param p2, "callback" # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager$CommandCallback; */
/* .line 631 */
v0 = this.mIICNodeHelper;
(( com.android.server.input.padkeyboard.iic.IICNodeHelper ) v0 ).checkAuth ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->checkAuth([B)[B
/* .line 632 */
/* .local v0, "result":[B */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = if ( p2 != null) { // if-eqz p2, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 633 */
} // :cond_0
/* .line 635 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [B */
} // .end method
public void setCapsLockLight ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 666 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setCapsLight :" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 667 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 668 */
/* .local v0, "data":Landroid/os/Bundle; */
/* const-string/jumbo v1, "value" */
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 669 */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 670 */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v1 ).getIndex ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* .line 671 */
} // :cond_0
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_CAPS_KEY_NEW;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v1 ).getIndex ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
} // :goto_0
/* nop */
/* .line 672 */
/* .local v1, "index":I */
final String v2 = "feature"; // const-string v2, "feature"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 673 */
/* const/16 v2, 0xe */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V
/* .line 674 */
return;
} // .end method
public void setKeyboardBackLightBrightness ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "brightness" # I */
/* .line 691 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mUserSetBackLight:Z */
/* .line 692 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->updateBackLightBrightness(I)V */
/* .line 693 */
return;
} // .end method
public void setKeyboardType ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 1272 */
v0 = this.mlock;
/* monitor-enter v0 */
/* .line 1273 */
try { // :try_start_0
/* iput p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->mKeyboardType:I */
/* .line 1274 */
/* monitor-exit v0 */
/* .line 1275 */
return;
/* .line 1274 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setMuteLight ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 678 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMuteLight :" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiPadKeyboardManager"; // const-string v1, "MiuiPadKeyboardManager"
android.util.Slog .i ( v1,v0 );
/* .line 679 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 680 */
/* .local v0, "data":Landroid/os/Bundle; */
/* const-string/jumbo v1, "value" */
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 681 */
v1 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .isXM2022MCU ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 682 */
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v1 ).getIndex ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
/* .line 683 */
} // :cond_0
v1 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_MIC_MUTE_NEW;
v1 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v1 ).getIndex ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
} // :goto_0
/* nop */
/* .line 684 */
/* .local v1, "index":I */
final String v2 = "feature"; // const-string v2, "feature"
(( android.os.Bundle ) v0 ).putInt ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 685 */
/* const/16 v2, 0xf */
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V
/* .line 686 */
return;
} // .end method
public void stayAwake ( ) {
/* .locals 2 */
/* .line 543 */
final String v0 = "MiuiPadKeyboardManager"; // const-string v0, "MiuiPadKeyboardManager"
/* const-string/jumbo v1, "stay awake" */
android.util.Slog .i ( v0,v1 );
/* .line 544 */
v0 = this.mHandler;
v1 = this.mStopAwakeRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 545 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda8; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda8;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 546 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda9; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$$ExternalSyntheticLambda9;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 547 */
return;
} // .end method
public void stopStayAwake ( ) {
/* .locals 4 */
/* .line 539 */
v0 = this.mHandler;
v1 = this.mStopAwakeRunnable;
/* const-wide/16 v2, 0x12c */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 540 */
return;
} // .end method
public void wakeKeyboard176 ( ) {
/* .locals 2 */
/* .line 1228 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE.KB_POWER;
v0 = (( com.android.server.input.padkeyboard.iic.CommunicationUtil$KB_FEATURE ) v0 ).getIndex ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$KB_FEATURE;->getIndex()I
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).writePadKeyBoardStatus ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->writePadKeyBoardStatus(II)V
/* .line 1229 */
return;
} // .end method
public void writeCommandToIIC ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "command" # [B */
/* .line 919 */
v0 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v0 ).writeSocketCmd ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->writeSocketCmd([B)Z
/* .line 920 */
return;
} // .end method
public void writePadKeyBoardStatus ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "target" # I */
/* .param p2, "value" # I */
/* .line 956 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 957 */
/* .local v0, "data":Landroid/os/Bundle; */
/* const-string/jumbo v1, "value" */
(( android.os.Bundle ) v0 ).putInt ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 958 */
final String v1 = "feature"; // const-string v1, "feature"
(( android.os.Bundle ) v0 ).putInt ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 959 */
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.input.padkeyboard.MiuiIICKeyboardManager ) p0 ).sendCommand ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->sendCommand(ILandroid/os/Bundle;)V
/* .line 960 */
return;
} // .end method
