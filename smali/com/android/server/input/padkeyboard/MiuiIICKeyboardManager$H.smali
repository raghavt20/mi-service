.class Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;
.super Landroid/os/Handler;
.source "MiuiIICKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field public static final DATA_FEATURE_TYPE:Ljava/lang/String; = "feature"

.field public static final DATA_FEATURE_VALUE:Ljava/lang/String; = "value"

.field public static final DATA_KEY_FIRST_CHECK:Ljava/lang/String; = "is_first_check"

.field public static final MSG_CHECK_MCU_STATUS:I = 0x6

.field public static final MSG_CHECK_STAY_WAKE_KEYBOARD:I = 0x7

.field public static final MSG_GET_KEYBOARD_VERSION:I = 0x1

.field public static final MSG_GET_MCU_VERSION:I = 0xa

.field public static final MSG_READ_HALL_DATA:I = 0x8

.field public static final MSG_READ_KB_STATUS:I = 0x3

.field public static final MSG_RESTORE_MCU:I = 0x2

.field public static final MSG_SEND_NFC_DATA:I = 0xc

.field public static final MSG_SET_BACK_LIGHT:I = 0xb

.field public static final MSG_SET_CAPS_LOCK_LIGHT:I = 0xe

.field public static final MSG_SET_KB_STATUS:I = 0x4

.field public static final MSG_SET_MUTE_LIGHT:I = 0xf

.field public static final MSG_START_CHECK_AUTH:I = 0x5

.field public static final MSG_UPGRADE_KEYBOARD:I = 0x0

.field public static final MSG_UPGRADE_MCU:I = 0x9

.field public static final MSG_UPGRADE_TOUCHPAD:I = 0xd


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1331
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    .line 1332
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1333
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 1337
    iget v0, p1, Landroid/os/Message;->what:I

    const-string/jumbo v1, "value"

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 1408
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmShouldUpgradeTouchPad(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1409
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->upgradeTouchPadIfNeed()V

    goto/16 :goto_1

    .line 1405
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendNFCData()V

    .line 1406
    goto/16 :goto_1

    .line 1401
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 1402
    .local v0, "value":B
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setKeyboardBacklight(B)V

    .line 1403
    goto/16 :goto_1

    .line 1352
    .end local v0    # "value":B
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendGetMCUVersionCommand()V

    .line 1353
    goto/16 :goto_1

    .line 1344
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmShouldUpgradeMCU(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->isXM2022MCU()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1345
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->startUpgradeMCUIfNeed()V

    goto/16 :goto_1

    .line 1398
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->checkHallStatus()V

    .line 1399
    goto/16 :goto_1

    .line 1390
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmShouldStayWakeKeyboard(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1391
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->wakeKeyboard176()V

    .line 1392
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1394
    .local v0, "stayWakeMsg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmHandler(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1395
    .end local v0    # "stayWakeMsg":Landroid/os/Message;
    goto/16 :goto_1

    .line 1384
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmCommunicationUtil(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getLongRawCommand()[B

    move-result-object v0

    .line 1385
    .local v0, "command":[B
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmCommunicationUtil(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v1

    const/16 v2, -0x5f

    invoke-virtual {v1, v0, v2}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->setCheckMCUStatusCommand([BB)V

    .line 1387
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->writeCommandToIIC([B)V

    .line 1388
    goto/16 :goto_1

    .line 1372
    .end local v0    # "command":[B
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIsKeyboardReady(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1373
    const-string v0, "MiuiPadKeyboardManager"

    const-string/jumbo v1, "start authentication"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1375
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 1376
    goto/16 :goto_1

    .line 1378
    :cond_0
    const-string v1, "is_first_check"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1379
    .local v1, "isFirst":Z
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v2, v1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$mdoCheckKeyboardIdentity(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Z)V

    .line 1380
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "isFirst":Z
    goto :goto_1

    .line 1365
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIsKeyboardReady(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1366
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    move v0, v2

    .line 1367
    .local v0, "enable":Z
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "feature"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1368
    .local v1, "feature":I
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->setKeyboardFeature(ZI)V

    .line 1369
    .end local v0    # "enable":Z
    .end local v1    # "feature":I
    goto :goto_1

    .line 1358
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIsKeyboardReady(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1359
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->readKeyboardStatus()V

    goto :goto_1

    .line 1355
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendRestoreMcuCommand()V

    .line 1356
    goto :goto_1

    .line 1349
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->sendGetKeyboardVersionCommand()V

    .line 1350
    goto :goto_1

    .line 1339
    :pswitch_d
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmShouldUpgradeKeyboard(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1340
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$H;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmIICNodeHelper(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/iic/IICNodeHelper;->upgradeKeyboardIfNeed()V

    .line 1415
    :cond_2
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method
