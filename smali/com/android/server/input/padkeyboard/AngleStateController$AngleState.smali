.class public abstract enum Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
.super Ljava/lang/Enum;
.source "AngleStateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/AngleStateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "AngleState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/PermittedSubclasses;
    value = {
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$1;,
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$2;,
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$3;,
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;,
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$5;,
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field public static final enum BACK_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field public static final enum CLOSE_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field public static final enum NO_WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field public static final enum NO_WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field public static final enum WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field public static final enum WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;


# instance fields
.field protected lower:F

.field protected onChangeListener:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;

.field protected upper:F


# direct methods
.method private static synthetic $values()[Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
    .locals 6

    .line 19
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->CLOSE_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    sget-object v1, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->NO_WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    sget-object v2, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    sget-object v3, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    sget-object v4, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->NO_WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    sget-object v5, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->BACK_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    filled-new-array/range {v0 .. v5}, [Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 13

    .line 20
    new-instance v6, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$1;

    const-string v1, "CLOSE_STATE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x40a00000    # 5.0f

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$1;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$1-IA;)V

    sput-object v6, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->CLOSE_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 41
    new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$2;

    const-string v8, "NO_WORK_STATE_1"

    const/4 v9, 0x1

    const/high16 v10, 0x40a00000    # 5.0f

    const/high16 v11, 0x42340000    # 45.0f

    const/4 v12, 0x0

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$2;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$2-IA;)V

    sput-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->NO_WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 65
    new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$3;

    const-string v2, "WORK_STATE_1"

    const/4 v3, 0x2

    const/high16 v4, 0x42340000    # 45.0f

    const/high16 v5, 0x42b40000    # 90.0f

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$3;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$3-IA;)V

    sput-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 89
    new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;

    const-string v8, "WORK_STATE_2"

    const/4 v9, 0x3

    const/high16 v10, 0x42b40000    # 90.0f

    const/high16 v11, 0x43390000    # 185.0f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$4;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$4-IA;)V

    sput-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 113
    new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$5;

    const-string v2, "NO_WORK_STATE_2"

    const/4 v3, 0x4

    const/high16 v4, 0x43390000    # 185.0f

    const v5, 0x43b18000    # 355.0f

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$5;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$5-IA;)V

    sput-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->NO_WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 137
    new-instance v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;

    const-string v8, "BACK_STATE"

    const/4 v9, 0x5

    const v10, 0x43b18000    # 355.0f

    const/high16 v11, 0x43b40000    # 360.0f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$6;-><init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState$6-IA;)V

    sput-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->BACK_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 19
    invoke-static {}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->$values()[Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->$VALUES:[Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFF)V
    .locals 0
    .param p3, "lower"    # F
    .param p4, "upper"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)V"
        }
    .end annotation

    .line 162
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 163
    iput p4, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->upper:F

    .line 164
    iput p3, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->lower:F

    .line 165
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IFFLcom/android/server/input/padkeyboard/AngleStateController$AngleState-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;-><init>(Ljava/lang/String;IFF)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 19
    const-class v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    return-object v0
.end method

.method public static values()[Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
    .locals 1

    .line 19
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->$VALUES:[Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0}, [Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    return-object v0
.end method


# virtual methods
.method public abstract getCurrentKeyboardStatus()Z
.end method

.method public abstract isCurrentState(F)Z
.end method

.method public onChange(Z)V
    .locals 1
    .param p1, "toUpper"    # Z

    .line 175
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->onChangeListener:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;

    if-eqz v0, :cond_0

    .line 176
    invoke-interface {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;->onChange(Z)V

    .line 178
    :cond_0
    return-void
.end method

.method public setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V
    .locals 0
    .param p1, "onChangeListener"    # Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;

    .line 181
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->onChangeListener:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;

    .line 182
    return-void
.end method

.method public abstract toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
.end method
