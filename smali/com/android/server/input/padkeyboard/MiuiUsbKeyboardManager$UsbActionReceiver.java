class com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$UsbActionReceiver extends android.content.BroadcastReceiver {
	 /* .source "MiuiUsbKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "UsbActionReceiver" */
} // .end annotation
/* # instance fields */
final com.android.server.input.padkeyboard.MiuiUsbKeyboardManager this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$UsbActionReceiver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager; */
/* .line 196 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 200 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 201 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "device"; // const-string v1, "device"
(( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v1, Landroid/hardware/usb/UsbDevice; */
/* .line 202 */
/* .local v1, "device":Landroid/hardware/usb/UsbDevice; */
v2 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v2 = "android.hardware.usb.action.USB_DEVICE_DETACHED"; // const-string v2, "android.hardware.usb.action.USB_DEVICE_DETACHED"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
/* :sswitch_1 */
final String v2 = "android.hardware.usb.action.USB_DEVICE_ATTACHED"; // const-string v2, "android.hardware.usb.action.USB_DEVICE_ATTACHED"
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 int v2 = 0; // const/4 v2, 0x0
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* const/16 v3, 0x3ffc */
/* const/16 v4, 0x3206 */
int v5 = 0; // const/4 v5, 0x0
/* packed-switch v2, :pswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 216 */
/* :pswitch_0 */
final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "USB device detached: "; // const-string v7, "USB device detached: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.hardware.usb.UsbDevice ) v1 ).getDeviceName ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v6 );
/* .line 217 */
v2 = (( android.hardware.usb.UsbDevice ) v1 ).getVendorId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
/* if-ne v2, v4, :cond_1 */
/* .line 218 */
v2 = (( android.hardware.usb.UsbDevice ) v1 ).getProductId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I
/* if-ne v2, v3, :cond_1 */
/* .line 219 */
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmHandler ( v2 );
(( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v2 ).removeCallbacksAndMessages ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 220 */
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$mcloseDevice ( v2 );
/* .line 221 */
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmUsbDeviceLock ( v2 );
/* monitor-enter v2 */
/* .line 222 */
try { // :try_start_0
v3 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fputmUsbDevice ( v3,v5 );
/* .line 223 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 224 */
v2 = this.this$0;
com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$monUsbDeviceDetach ( v2 );
/* .line 223 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
	 /* monitor-exit v2 */
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* throw v3 */
	 /* .line 204 */
	 /* :pswitch_1 */
	 final String v2 = "MiuiPadKeyboardManager"; // const-string v2, "MiuiPadKeyboardManager"
	 /* new-instance v6, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v7 = "USB device attached: "; // const-string v7, "USB device attached: "
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( android.hardware.usb.UsbDevice ) v1 ).getDeviceName ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v6 );
	 /* .line 205 */
	 v2 = 	 (( android.hardware.usb.UsbDevice ) v1 ).getVendorId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I
	 /* if-ne v2, v4, :cond_1 */
	 /* .line 206 */
	 v2 = 	 (( android.hardware.usb.UsbDevice ) v1 ).getProductId ( ); // invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I
	 /* if-ne v2, v3, :cond_1 */
	 /* .line 207 */
	 v2 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmHandler ( v2 );
	 (( com.android.server.input.padkeyboard.MiuiUsbKeyboardManager$H ) v2 ).removeCallbacksAndMessages ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/server/input/padkeyboard/MiuiUsbKeyboardManager$H;->removeCallbacksAndMessages(Ljava/lang/Object;)V
	 /* .line 208 */
	 v2 = this.this$0;
	 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fgetmUsbDeviceLock ( v2 );
	 /* monitor-enter v2 */
	 /* .line 209 */
	 try { // :try_start_2
		 v3 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$fputmUsbDevice ( v3,v1 );
		 /* .line 210 */
		 /* monitor-exit v2 */
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
		 /* .line 211 */
		 v2 = this.this$0;
		 com.android.server.input.padkeyboard.MiuiUsbKeyboardManager .-$$Nest$monUsbDeviceAttach ( v2 );
		 /* .line 210 */
		 /* :catchall_1 */
		 /* move-exception v3 */
		 try { // :try_start_3
			 /* monitor-exit v2 */
			 /* :try_end_3 */
			 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
			 /* throw v3 */
			 /* .line 231 */
		 } // :cond_1
	 } // :goto_2
	 return;
	 /* nop */
	 /* :sswitch_data_0 */
	 /* .sparse-switch */
	 /* -0x7e02a835 -> :sswitch_1 */
	 /* -0x5fdc9a67 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
