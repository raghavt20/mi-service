.class public Lcom/android/server/input/padkeyboard/AngleStateController;
.super Ljava/lang/Object;
.source "AngleStateController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AngleStateController"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

.field private volatile mIdentityPass:Z

.field private volatile mKbLevel:I

.field private volatile mLidOpen:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private volatile mShouldIgnoreKeyboard:Z

.field private volatile mTabletOpen:Z


# direct methods
.method public static synthetic $r8$lambda$0jOTacZicYOJ2MAKjiN8mJXY8tM(Lcom/android/server/input/padkeyboard/AngleStateController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$1(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$0qe9D-eH746VFsR7HJFuMT2FNXo(Lcom/android/server/input/padkeyboard/AngleStateController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$3(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$VAdZDqhIwWES3NSKBy2Z85T_YFU(Lcom/android/server/input/padkeyboard/AngleStateController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$2(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$Vzi1FtdAQ9Vdvo457NF5CbBSZ3o(Lcom/android/server/input/padkeyboard/AngleStateController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$0(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$ipLUQXERU-2Tk3ZLH7W1vhmopVE(Lcom/android/server/input/padkeyboard/AngleStateController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$4(Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$pziw1pCtTyWG5SDFFBNDVd_LHx0(Lcom/android/server/input/padkeyboard/AngleStateController;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController;->lambda$initState$5(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    .line 194
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I

    .line 195
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    .line 196
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z

    .line 199
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mContext:Landroid/content/Context;

    .line 200
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mPowerManager:Landroid/os/PowerManager;

    .line 201
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/AngleStateController;->initState()V

    .line 202
    return-void
.end method

.method private initCurrentState()V
    .locals 1

    .line 229
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->CLOSE_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    .line 230
    return-void
.end method

.method private initState()V
    .locals 2

    .line 236
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->CLOSE_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V

    .line 244
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->NO_WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V

    .line 252
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->WORK_STATE_1:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V

    .line 260
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V

    .line 268
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->NO_WORK_STATE_2:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V

    .line 276
    sget-object v0, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->BACK_STATE:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    new-instance v1, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/AngleStateController$$ExternalSyntheticLambda5;-><init>(Lcom/android/server/input/padkeyboard/AngleStateController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->setOnChangeListener(Lcom/android/server/input/padkeyboard/AngleStateController$AngleState$OnChangeListener;)V

    .line 280
    return-void
.end method

.method private synthetic lambda$initState$0(Z)V
    .locals 2
    .param p1, "toUpper"    # Z

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleState CLOSE_STATE onchange toUpper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 239
    return-void
.end method

.method private synthetic lambda$initState$1(Z)V
    .locals 2
    .param p1, "toUpper"    # Z

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleState NO_WORK_STATE_1 onchange toUpper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    xor-int/lit8 v0, p1, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 247
    return-void
.end method

.method private synthetic lambda$initState$2(Z)V
    .locals 2
    .param p1, "toUpper"    # Z

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleState WORK_STATE_1 onchange toUpper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    xor-int/lit8 v0, p1, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 255
    return-void
.end method

.method private synthetic lambda$initState$3(Z)V
    .locals 2
    .param p1, "toUpper"    # Z

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleState WORK_STATE_2 onchange toUpper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 263
    return-void
.end method

.method private synthetic lambda$initState$4(Z)V
    .locals 2
    .param p1, "toUpper"    # Z

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleState NO_WORK_STATE_2 onchange toUpper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 271
    return-void
.end method

.method private synthetic lambda$initState$5(Z)V
    .locals 2
    .param p1, "toUpper"    # Z

    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleState BACK_STATE onchange toUpper: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 279
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 358
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 359
    const-string v0, "mCurrentState="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 362
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 363
    const-string v0, "mIdentityPass="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 364
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 366
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 367
    const-string v0, "mLidOpen="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 368
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 370
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 371
    const-string v0, "mTabletOpen="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 372
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 374
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 375
    const-string v0, "mShouldIgnoreKeyboard="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 376
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 377
    return-void
.end method

.method public getIdentityStatus()Z
    .locals 1

    .line 318
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z

    return v0
.end method

.method public getLidStatus()Z
    .locals 1

    .line 310
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    return v0
.end method

.method public getTabletStatus()Z
    .locals 1

    .line 314
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    return v0
.end method

.method public isWorkState()Z
    .locals 1

    .line 302
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public notifyLidSwitchChanged(Z)V
    .locals 2
    .param p1, "lidOpen"    # Z

    .line 338
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyLidSwitchChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    return-void
.end method

.method public notifyTabletSwitchChanged(Z)V
    .locals 2
    .param p1, "tabletOpen"    # Z

    .line 343
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyTabletSwitchChanged: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    return-void
.end method

.method public resetAngleStatus()V
    .locals 1

    .line 353
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 354
    return-void
.end method

.method public setIdentityState(Z)V
    .locals 2
    .param p1, "identityPass"    # Z

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Keyboard authentication status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AngleStateController"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z

    .line 350
    return-void
.end method

.method public setKbLevel(IZ)V
    .locals 5
    .param p1, "kbLevel"    # I
    .param p2, "shouldWakeUp"    # Z

    .line 322
    iput p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I

    .line 323
    if-eqz p2, :cond_0

    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    const-string/jumbo v4, "usb_keyboard_attach"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 327
    :cond_0
    return-void
.end method

.method public setShouldIgnoreKeyboardFromKeyboard(Z)V
    .locals 0
    .param p1, "isEnable"    # Z

    .line 306
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 307
    return-void
.end method

.method public shouldIgnoreKeyboard()Z
    .locals 4

    .line 284
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 285
    return v1

    .line 287
    :cond_0
    iget v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mKbLevel:I

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-ne v0, v2, :cond_1

    .line 288
    return v3

    .line 290
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    move v1, v3

    :cond_3
    :goto_0
    return v1
.end method

.method public shouldIgnoreKeyboardForIIC()Z
    .locals 2

    .line 295
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mIdentityPass:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 296
    return v1

    .line 298
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public updateAngleState(F)V
    .locals 1
    .param p1, "angle"    # F

    .line 205
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    return-void

    .line 209
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    if-nez v0, :cond_1

    .line 210
    const/4 p1, 0x0

    .line 213
    :cond_1
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    if-nez v0, :cond_2

    .line 214
    const/high16 p1, 0x43b40000    # 360.0f

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    if-nez v0, :cond_3

    .line 218
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/AngleStateController;->initCurrentState()V

    .line 221
    :cond_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->isCurrentState(F)Z

    move-result v0

    if-nez v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0, p1}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->toNextState(F)Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    goto :goto_0

    .line 224
    :cond_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mCurrentState:Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/AngleStateController$AngleState;->getCurrentKeyboardStatus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mShouldIgnoreKeyboard:Z

    .line 226
    :goto_0
    return-void
.end method

.method public wakeUpIfNeed(Z)V
    .locals 5
    .param p1, "shouldWakeUp"    # Z

    .line 330
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mLidOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mTabletOpen:Z

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/AngleStateController;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x0

    const-string v4, "miui_keyboard_attach"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 335
    :cond_0
    return-void
.end method
