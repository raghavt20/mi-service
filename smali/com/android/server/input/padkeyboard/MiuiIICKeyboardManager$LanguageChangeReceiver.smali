.class Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiIICKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LanguageChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;


# direct methods
.method private constructor <init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V
    .locals 0

    .line 773
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;-><init>(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 776
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v0

    if-nez v0, :cond_0

    .line 778
    return-void

    .line 780
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 782
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 783
    .local v0, "currentLocale":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 784
    .local v1, "currentLanguage":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "currentLanguage is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiPadKeyboardManager"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 786
    return-void

    .line 788
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/input/InputManagerService;->getKeyboardLayouts()[Landroid/hardware/input/KeyboardLayout;

    move-result-object v2

    .line 789
    .local v2, "keyboardLayouts":[Landroid/hardware/input/KeyboardLayout;
    array-length v4, v2

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v4, :cond_2

    aget-object v7, v2, v6

    .line 791
    .local v7, "keyboardLayout":Landroid/hardware/input/KeyboardLayout;
    iget-object v8, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v8}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v9}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v9

    .line 792
    invoke-virtual {v9}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v9

    .line 793
    invoke-virtual {v7}, Landroid/hardware/input/KeyboardLayout;->getDescriptor()Ljava/lang/String;

    move-result-object v10

    .line 791
    invoke-virtual {v8, v9, v10}, Lcom/android/server/input/InputManagerService;->removeKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 789
    .end local v7    # "keyboardLayout":Landroid/hardware/input/KeyboardLayout;
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 797
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    goto :goto_1

    :sswitch_0
    const-string/jumbo v4, "th_TH"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_1
    const-string v4, "ru_RU"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x3

    goto :goto_2

    :sswitch_2
    const-string v4, "it_IT"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x7

    goto :goto_2

    :sswitch_3
    const-string v4, "fr_FR"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_4
    const-string v4, "es_US"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x5

    goto :goto_2

    :sswitch_5
    const-string v4, "en_GB"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_2

    :sswitch_6
    const-string v4, "de_DE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x4

    goto :goto_2

    :sswitch_7
    const-string v4, "ar_EG"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v5, 0x6

    goto :goto_2

    :goto_1
    const/4 v5, -0x1

    :goto_2
    const-string v4, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"

    packed-switch v5, :pswitch_data_0

    .line 847
    return-void

    .line 841
    :pswitch_0
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_italian"

    .line 842
    .local v5, "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v7

    .line 843
    invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v7

    .line 842
    invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 845
    goto/16 :goto_3

    .line 835
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_1
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_arabic"

    .line 836
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v7

    .line 837
    invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v7

    .line 836
    invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 839
    goto/16 :goto_3

    .line 829
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_2
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"

    .line 830
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v6

    .line 831
    invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v6

    .line 830
    const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_spanish"

    invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 833
    goto :goto_3

    .line 823
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_3
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"

    .line 824
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v6

    .line 825
    invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v6

    .line 824
    const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_german"

    invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 827
    goto :goto_3

    .line 817
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_4
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_russian"

    .line 818
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v7

    .line 819
    invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v7

    .line 818
    invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 821
    goto :goto_3

    .line 811
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_5
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"

    .line 812
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v6

    .line 813
    invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v6

    .line 812
    const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_french"

    invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 815
    goto :goto_3

    .line 805
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_6
    const-string v5, "com.miui.miinput/com.miui.miinput.keyboardlayout.InputDeviceReceiver/keyboard_layout_thai"

    .line 806
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v7}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v7

    .line 807
    invoke-virtual {v7}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v7

    .line 806
    invoke-virtual {v6, v7, v4}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 809
    goto :goto_3

    .line 799
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :pswitch_7
    const-string v5, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_us"

    .line 800
    .restart local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v4

    iget-object v6, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v6}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v6

    .line 801
    invoke-virtual {v6}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v6

    .line 800
    const-string v7, "com.android.inputdevices/com.android.inputdevices.InputDeviceReceiver/keyboard_layout_english_uk"

    invoke-virtual {v4, v6, v7}, Lcom/android/server/input/InputManagerService;->setCurrentKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 803
    nop

    .line 849
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add keyboardLayout: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v3}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmInputManager(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Lcom/android/server/input/InputManagerService;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager$LanguageChangeReceiver;->this$0:Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;

    invoke-static {v4}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->-$$Nest$fgetmKeyboardDevice(Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;)Landroid/view/InputDevice;

    move-result-object v4

    .line 851
    invoke-virtual {v4}, Landroid/view/InputDevice;->getIdentifier()Landroid/hardware/input/InputDeviceIdentifier;

    move-result-object v4

    .line 850
    invoke-virtual {v3, v4, v5}, Lcom/android/server/input/InputManagerService;->addKeyboardLayoutForInputDevice(Landroid/hardware/input/InputDeviceIdentifier;Ljava/lang/String;)V

    .line 853
    .end local v0    # "currentLocale":Ljava/util/Locale;
    .end local v1    # "currentLanguage":Ljava/lang/String;
    .end local v2    # "keyboardLayouts":[Landroid/hardware/input/KeyboardLayout;
    .end local v5    # "keyboardLayoutDescriptor":Ljava/lang/String;
    :cond_4
    return-void

    :sswitch_data_0
    .sparse-switch
        0x58c2770 -> :sswitch_7
        0x5b084ff -> :sswitch_6
        0x5c2b431 -> :sswitch_5
        0x5c4fbcf -> :sswitch_4
        0x5d29d1f -> :sswitch_3
        0x5fdccbf -> :sswitch_2
        0x67d15bf -> :sswitch_1
        0x6935c1f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
