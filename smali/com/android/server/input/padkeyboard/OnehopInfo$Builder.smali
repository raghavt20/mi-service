.class public final Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
.super Ljava/lang/Object;
.source "OnehopInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/OnehopInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private action_suffix:Ljava/lang/String;

.field private bt_mac:Ljava/lang/String;

.field private device_type:I

.field private ext_ability:[B


# direct methods
.method static bridge synthetic -$$Nest$fgetaction_suffix(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->action_suffix:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetbt_mac(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->bt_mac:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetdevice_type(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)I
    .locals 0

    iget p0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->device_type:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetext_ability(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;)[B
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->ext_ability:[B

    return-object p0
.end method

.method public constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/android/server/input/padkeyboard/OnehopInfo;
    .locals 2

    .line 89
    new-instance v0, Lcom/android/server/input/padkeyboard/OnehopInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/padkeyboard/OnehopInfo;-><init>(Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;Lcom/android/server/input/padkeyboard/OnehopInfo-IA;)V

    return-object v0
.end method

.method public setActionSuffix(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
    .locals 0
    .param p1, "actionSuffix"    # Ljava/lang/String;

    .line 74
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->action_suffix:Ljava/lang/String;

    .line 75
    return-object p0
.end method

.method public setBtMac(Ljava/lang/String;)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
    .locals 0
    .param p1, "btMac"    # Ljava/lang/String;

    .line 79
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->bt_mac:Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public setExtAbility([B)Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;
    .locals 0
    .param p1, "extAbility"    # [B

    .line 84
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/OnehopInfo$Builder;->ext_ability:[B

    .line 85
    return-object p0
.end method
