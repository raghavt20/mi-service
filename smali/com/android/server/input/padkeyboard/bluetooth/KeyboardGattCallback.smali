.class public Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "KeyboardGattCallback.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "KeyboardGattCallback"


# instance fields
.field public final BLE_DEVICE_SLEEP_INTERVAL:I

.field private mAlreadyNotifyBattery:Z

.field private final mBleHandler:Landroid/os/Handler;

.field private mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

.field private mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

.field private volatile mIsActiveMode:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    .line 41
    const/16 v0, 0x28

    iput v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->BLE_DEVICE_SLEEP_INTERVAL:I

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z

    .line 49
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mBleHandler:Landroid/os/Handler;

    .line 50
    return-void
.end method

.method private WriteBleCharacter([BI)V
    .locals 3
    .param p1, "buf"    # [B
    .param p2, "type"    # I

    .line 223
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    const-string v1, "KeyboardGattCallback"

    if-nez v0, :cond_0

    .line 224
    const-string v0, "WriteBleCharacter mGattDeviceItem is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    return-void

    .line 227
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WriteBleCharacter type "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", write data "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 228
    const/16 v2, 0x12

    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 230
    return-void
.end method

.method private declared-synchronized clearDevice()V
    .locals 2

    monitor-enter p0

    .line 261
    :try_start_0
    const-string v0, "KeyboardGattCallback"

    const-string v1, "clearDevice"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z

    .line 263
    sget-object v1, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectBleLocked(Z)V

    .line 264
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-eqz v0, :cond_0

    .line 265
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->clear()V

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;
    :cond_0
    monitor-exit p0

    return-void

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getBluetoothGatt()Landroid/bluetooth/BluetoothGatt;
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    return-object v0

    .line 253
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    return-object v0
.end method

.method public notifyUpgradeIfNeed()V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendStartOta()V

    .line 273
    :cond_0
    return-void
.end method

.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;[B)V
    .locals 9
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "value"    # [B

    .line 125
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;[B)V

    .line 126
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    const-string v1, "KeyboardGattCallback"

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "The iic devices is connected, ignore ble info!"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return-void

    .line 131
    :cond_0
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_BATTERY_LEVEL:Ljava/util/UUID;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get keyboard battery :"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v4, p3, v3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    aget-byte v0, p3, v3

    .line 135
    .local v0, "nowElectricity":I
    const/16 v1, 0xa

    if-gt v0, v1, :cond_1

    iget-boolean v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z

    if-nez v4, :cond_1

    .line 136
    iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z

    .line 137
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 138
    .local v2, "castData":Landroid/os/Bundle;
    const-string v4, "electricity"

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v4

    .line 139
    invoke-static {v4, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->notifySettingsKeyboardStatusChanged(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 142
    .end local v2    # "castData":Landroid/os/Bundle;
    :cond_1
    if-le v0, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z

    if-eqz v1, :cond_2

    .line 143
    iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z

    .line 146
    .end local v0    # "nowElectricity":I
    :cond_2
    return-void

    .line 149
    :cond_3
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_CONTROL_CHARACTER:Ljava/util/UUID;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deviceName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " received:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 152
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v2

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v3

    array-length v3, v3

    .line 151
    invoke-static {v2, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-eqz v0, :cond_4

    .line 154
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->parseInfo([B)V

    .line 156
    :cond_4
    return-void

    .line 159
    :cond_5
    array-length v0, p3

    if-le v0, v2, :cond_8

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get raw ble data:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v4, p3

    invoke-static {p3, v4}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/16 v0, 0x40

    new-array v0, v0, [B

    .line 163
    .local v0, "temp":[B
    const/16 v4, 0x23

    aput-byte v4, v0, v3

    .line 164
    aget-byte v4, p3, v3

    aput-byte v4, v0, v2

    .line 165
    const/16 v4, 0x38

    const/4 v5, 0x2

    aput-byte v4, v0, v5

    .line 166
    const/16 v4, -0x80

    const/4 v6, 0x3

    aput-byte v4, v0, v6

    .line 167
    aget-byte v4, p3, v2

    const/4 v7, 0x4

    aput-byte v4, v0, v7

    .line 168
    array-length v4, p3

    if-lt v4, v6, :cond_6

    .line 169
    array-length v4, p3

    sub-int/2addr v4, v5

    const/4 v8, 0x5

    invoke-static {p3, v5, v0, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    :cond_6
    aget-byte v3, p3, v3

    const/16 v4, 0x30

    if-ne v3, v4, :cond_7

    aget-byte v3, p3, v2

    if-ne v3, v2, :cond_7

    .line 174
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v3, p3, v7

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "%02x"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-byte v3, p3, v6

    .line 175
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    .line 174
    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 176
    .local v2, "version":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ble device version:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-eqz v1, :cond_7

    .line 178
    invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setVersionAndStartOta(Ljava/lang/String;)V

    .line 181
    .end local v2    # "version":Ljava/lang/String;
    :cond_7
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->receiverNanoAppData([B)V

    .line 183
    .end local v0    # "temp":[B
    :cond_8
    return-void
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 0
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .line 189
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    .line 190
    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .line 108
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " onCharacteristicWrite: write data:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 110
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v1

    .line 109
    const/16 v2, 0x12

    invoke-static {v1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", and status is : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KeyboardGattCallback"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_CONTROL_CHARACTER:Ljava/util/UUID;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_PACKET_CHARACTER:Ljava/util/UUID;

    .line 113
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->notifyDeviceFree()V

    .line 119
    :cond_1
    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I
    .param p3, "newState"    # I

    .line 66
    const-string v0, "KeyboardGattCallback"

    if-nez p1, :cond_0

    .line 67
    const-string v1, "ConnectionStateChanged, but gatt is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    return-void

    .line 70
    :cond_0
    packed-switch p3, :pswitch_data_0

    .line 81
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New state not processed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->clearDevice()V

    goto :goto_0

    .line 72
    :pswitch_1
    const-string v1, "ConnectionStateChanged connected!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z

    .line 74
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    .line 75
    goto :goto_0

    .line 77
    :pswitch_2
    const-string v1, "ConnectionStateChanged disconnected!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->clearDevice()V

    .line 79
    nop

    .line 85
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onConnectionUpdated(Landroid/bluetooth/BluetoothGatt;IIII)V
    .locals 1
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "interval"    # I
    .param p3, "latency"    # I
    .param p4, "timeout"    # I
    .param p5, "status"    # I

    .line 55
    if-nez p5, :cond_1

    .line 56
    const/16 v0, 0x28

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z

    .line 58
    :cond_1
    return-void
.end method

.method public onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .line 205
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V

    .line 206
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B

    move-result-object v0

    .line 207
    .local v0, "value":[B
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDescriptorRead: deviceName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "read data:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v0

    .line 208
    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 207
    const-string v2, "KeyboardGattCallback"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    return-void
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .line 214
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V

    .line 215
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B

    move-result-object v0

    .line 216
    .local v0, "value":[B
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDescriptorWrite: deviceName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " write data:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v0

    .line 217
    invoke-static {v0, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    const-string v2, "KeyboardGattCallback"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return-void
.end method

.method public onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "mtu"    # I
    .param p3, "status"    # I

    .line 194
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onMtuChanged: deviceName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mtu:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KeyboardGattCallback"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-eqz v0, :cond_0

    .line 197
    add-int/lit8 v1, p2, -0x3

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setPackageSize(I)V

    .line 198
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendGetKeyboardStatus()V

    .line 200
    :cond_0
    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I

    .line 89
    const-string v0, "KeyboardGattCallback"

    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServicesDiscovered: deviceName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 91
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-direct {v0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;-><init>(Landroid/bluetooth/BluetoothGatt;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    .line 94
    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setNotificationCharacters()V

    .line 95
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setConnected(Z)V

    .line 97
    :cond_0
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendSetMtu()V

    goto :goto_0

    .line 101
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServicesDiscovered received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or null pointer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_2
    :goto_0
    return-void
.end method

.method public sendCommandToKeyboard([B)V
    .locals 4
    .param p1, "command"    # [B

    .line 234
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 238
    :cond_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 239
    .local v0, "context":Landroid/content/Context;
    if-eqz p1, :cond_1

    array-length v1, p1

    const/4 v2, 0x4

    if-le v1, v2, :cond_1

    .line 240
    const/4 v1, 0x1

    aget-byte v1, p1, v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x3

    aget-byte v2, p1, v2

    const-string v3, "keyboard_back_light_brightness"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 245
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->WriteBleCharacter([BI)V

    .line 246
    return-void

    .line 235
    .end local v0    # "context":Landroid/content/Context;
    :cond_2
    :goto_0
    const-string v0, "KeyboardGattCallback"

    const-string v1, "Abandon command because ble isn\'t connected or sleep"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    return-void
.end method

.method public setActiveMode(Z)V
    .locals 0
    .param p1, "isActiveMode"    # Z

    .line 61
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z

    .line 62
    return-void
.end method

.method public setCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)V
    .locals 0
    .param p1, "communicationUtil"    # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 257
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mCommunicationUtil:Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    .line 258
    return-void
.end method
