.class public Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
.super Ljava/lang/Object;
.source "UpgradeZipFile.java"


# static fields
.field public static OTA_FLIE_PATH:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "UpgradeZipFile"


# instance fields
.field private mFileBufBin:[B

.field private mFileBufDat:[B

.field private mFileBufJson:[B

.field private mPath:Ljava/lang/String;

.field private mValid:Z

.field private mVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    const-string v0, "odm/etc/Redmi_Keyboard_OTA.zip"

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->OTA_FLIE_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufJson:[B

    .line 23
    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    .line 24
    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    .line 36
    const-string v0, "UpgradeZipFile"

    if-nez p1, :cond_0

    .line 37
    const-string v1, "UpgradeOta file Path is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    return-void

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mPath:Ljava/lang/String;

    .line 41
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpgradeOta file Path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->readFileToBuf(Ljava/lang/String;)V

    .line 43
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    if-eqz v1, :cond_1

    .line 44
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mValid:Z

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    const/16 v3, 0x41

    aget-byte v2, v2, v3

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "%02x"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    const/16 v4, 0x40

    aget-byte v2, v2, v4

    .line 46
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    .line 45
    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mVersion:Ljava/lang/String;

    .line 47
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bin version is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 49
    :cond_1
    const-string v1, "UpgradeOta file buff is null or length low than 6000"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :goto_0
    return-void
.end method

.method private readFileToBuf(Ljava/lang/String;)V
    .locals 12
    .param p1, "fileName"    # Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const-string v2, "UpgradeZipFile"

    if-nez v1, :cond_0

    .line 73
    const-string v1, "The Upgrade file does not exist."

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    return-void

    .line 76
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    .local v1, "fIn":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v3

    const/high16 v4, 0xa00000

    if-le v3, v4, :cond_1

    .line 78
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File too large: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes (max 10 MB)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 115
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 79
    return-void

    .line 81
    :cond_1
    :try_start_3
    new-instance v3, Ljava/util/zip/ZipInputStream;

    invoke-direct {v3, v1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 83
    .local v3, "zipIn":Ljava/util/zip/ZipInputStream;
    :try_start_4
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v4

    .line 84
    .local v4, "zipEntry":Ljava/util/zip/ZipEntry;
    const/16 v5, 0x400

    new-array v5, v5, [B

    .line 85
    .local v5, "buffer":[B
    :goto_0
    if-eqz v4, :cond_6

    .line 86
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_5

    .line 87
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    .line 88
    .local v6, "zipFileName":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v7

    long-to-int v7, v7

    .line 89
    .local v7, "leng":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ZipFileName:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v8, ".json"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 91
    new-array v8, v7, [B

    iput-object v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufJson:[B

    .line 92
    invoke-virtual {v3, v8}, Ljava/util/zip/ZipInputStream;->read([B)I

    .line 95
    :cond_2
    const-string v8, ".dat"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 96
    new-array v8, v7, [B

    iput-object v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    .line 97
    invoke-virtual {v3, v8}, Ljava/util/zip/ZipInputStream;->read([B)I

    .line 100
    :cond_3
    const-string v8, ".bin"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 101
    new-array v8, v7, [B

    iput-object v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    .line 102
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 104
    .local v8, "baos":Ljava/io/ByteArrayOutputStream;
    :goto_1
    invoke-virtual {v3, v5}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v9

    move v10, v9

    .local v10, "count":I
    const/4 v11, -0x1

    if-eq v9, v11, :cond_4

    .line 105
    const/4 v9, 0x0

    invoke-virtual {v8, v5, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 107
    :cond_4
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    iput-object v9, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    .line 112
    .end local v6    # "zipFileName":Ljava/lang/String;
    .end local v7    # "leng":I
    .end local v8    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v10    # "count":I
    :cond_5
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v4, v6

    goto :goto_0

    .line 114
    .end local v4    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v5    # "buffer":[B
    :cond_6
    :try_start_5
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 115
    .end local v3    # "zipIn":Ljava/util/zip/ZipInputStream;
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 117
    .end local v1    # "fIn":Ljava/io/InputStream;
    goto :goto_4

    .line 81
    .restart local v1    # "fIn":Ljava/io/InputStream;
    .restart local v3    # "zipIn":Ljava/util/zip/ZipInputStream;
    :catchall_0
    move-exception v4

    :try_start_7
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v5

    :try_start_8
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "fIn":Ljava/io/InputStream;
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    .end local p1    # "fileName":Ljava/lang/String;
    :goto_2
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 76
    .end local v3    # "zipIn":Ljava/util/zip/ZipInputStream;
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "fIn":Ljava/io/InputStream;
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    .restart local p1    # "fileName":Ljava/lang/String;
    :catchall_2
    move-exception v3

    :try_start_9
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_3

    :catchall_3
    move-exception v4

    :try_start_a
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    .end local p1    # "fileName":Ljava/lang/String;
    :goto_3
    throw v3
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    .line 115
    .end local v1    # "fIn":Ljava/io/InputStream;
    .restart local v0    # "file":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    .restart local p1    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readFileToBuf exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_4
    return-void
.end method


# virtual methods
.method public getFileBufBin()[B
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    return-object v0
.end method

.method public getFileBufBinCrc()I
    .locals 5

    .line 152
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 153
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 154
    .local v0, "crc32":Ljava/util/zip/CRC32;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update([B)V

    .line 155
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v1

    const-wide v3, 0xffffffffL

    and-long/2addr v1, v3

    long-to-int v1, v1

    return v1

    .line 157
    .end local v0    # "crc32":Ljava/util/zip/CRC32;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getFileBufDat()[B
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    return-object v0
.end method

.method public getFileBufDatCrc()I
    .locals 5

    .line 143
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 144
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 145
    .local v0, "crc32":Ljava/util/zip/CRC32;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update([B)V

    .line 146
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v1

    const-wide v3, 0xffffffffL

    and-long/2addr v1, v3

    long-to-int v1, v1

    return v1

    .line 148
    .end local v0    # "crc32":Ljava/util/zip/CRC32;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getPercent(II)D
    .locals 5
    .param p1, "packetIndex"    # I
    .param p2, "packetLength"    # I

    .line 131
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    array-length v1, v0

    const/16 v2, 0x40

    if-le v1, v2, :cond_0

    .line 132
    mul-int v1, p1, p2

    mul-int/lit8 v1, v1, 0x64

    int-to-float v1, v1

    array-length v0, v0

    int-to-float v0, v0

    div-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 134
    .local v0, "progress":F
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 135
    .local v1, "df":Ljava/text/DecimalFormat;
    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "percent":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    return-wide v3

    .line 138
    .end local v0    # "progress":F
    .end local v1    # "df":Ljava/text/DecimalFormat;
    .end local v2    # "percent":Ljava/lang/String;
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public isValidFile()Z
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufDat:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mFileBufBin:[B

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x1

    return v0

    .line 58
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mVersion:Ljava/lang/String;

    .line 67
    return-void
.end method
