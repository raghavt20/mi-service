public class com.android.server.input.padkeyboard.bluetooth.upgrade.OtaHandler extends android.os.Handler {
	 /* .source "OtaHandler.java" */
	 /* # static fields */
	 public static final Integer MSG_CHECK_SUM;
	 public static final Integer MSG_CLEAR_DEVICE;
	 public static final Integer MSG_CTRL_CREATE_BIN;
	 public static final Integer MSG_CTRL_CREATE_DAT;
	 public static final Integer MSG_CTRL_EXECUTE;
	 public static final Integer MSG_CTRL_SELECT_BIN;
	 public static final Integer MSG_CTRL_SELECT_DAT;
	 public static final Integer MSG_CTRL_SET_PRN;
	 public static final Integer MSG_DATA_PACKET;
	 public static final Integer MSG_GET_KB_STATUS;
	 public static final Integer MSG_OTA_RESULT;
	 public static final Integer MSG_SEND_BIN;
	 public static final Integer MSG_SEND_DAT;
	 public static final Integer MSG_SET_MTU;
	 public static final Integer MSG_START_OTA;
	 public static final Integer OTA_FAIL;
	 public static final Integer OTA_START;
	 public static final Integer OTA_SUCCESS;
	 private static final java.lang.String TAG;
	 public static final Integer TYPE_BIN;
	 public static final Integer TYPE_DAT;
	 /* # instance fields */
	 com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem mGattDeviceItem;
	 /* # direct methods */
	 com.android.server.input.padkeyboard.bluetooth.upgrade.OtaHandler ( ) {
		 /* .locals 0 */
		 /* .param p1, "looper" # Landroid/os/Looper; */
		 /* .param p2, "item" # Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem; */
		 /* .line 42 */
		 /* invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
		 /* .line 43 */
		 this.mGattDeviceItem = p2;
		 /* .line 44 */
		 return;
	 } // .end method
	 private void showOtaResult ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p1, "result" # I */
		 /* .line 119 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 120 */
		 /* .local v0, "resId":I */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v2, "showOtaResult: result " */
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "OtaHandler"; // const-string v2, "OtaHandler"
		 android.util.Slog .i ( v2,v1 );
		 /* .line 121 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* if-ne p1, v1, :cond_0 */
		 /* .line 122 */
		 /* const v0, 0x110f024e */
		 /* .line 124 */
	 } // :cond_0
	 /* if-nez p1, :cond_1 */
	 /* .line 125 */
	 /* const v0, 0x110f024f */
	 /* .line 127 */
} // :cond_1
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_2 */
/* .line 128 */
/* const v0, 0x110f024d */
/* .line 130 */
} // :cond_2
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v1 ).getSystemContext ( ); // invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
/* .line 131 */
/* .local v1, "context":Landroid/content/Context; */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getString ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
android.widget.Toast .makeText ( v1,v2,v3 );
(( android.widget.Toast ) v2 ).show ( ); // invoke-virtual {v2}, Landroid/widget/Toast;->show()V
/* .line 132 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 48 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 49 */
v0 = this.mGattDeviceItem;
final String v1 = "OtaHandler"; // const-string v1, "OtaHandler"
/* if-nez v0, :cond_0 */
/* .line 50 */
final String v0 = "handleMessage: mGattDeviceItem null"; // const-string v0, "handleMessage: mGattDeviceItem null"
android.util.Log .d ( v1,v0 );
/* .line 51 */
return;
/* .line 53 */
} // :cond_0
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 114 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unknown msg " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* goto/16 :goto_0 */
/* .line 110 */
/* :pswitch_0 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 111 */
/* .local v0, "result":I */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->showOtaResult(I)V */
/* .line 112 */
/* goto/16 :goto_0 */
/* .line 106 */
} // .end local v0 # "result":I
/* :pswitch_1 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 107 */
/* .local v0, "index":I */
v1 = this.mGattDeviceItem;
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v1 ).sendData ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendData(II)V
/* .line 108 */
/* goto/16 :goto_0 */
/* .line 101 */
} // .end local v0 # "index":I
/* :pswitch_2 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 102 */
/* .restart local v0 # "index":I */
v1 = this.mGattDeviceItem;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v1 ).sendData ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendData(II)V
/* .line 103 */
/* goto/16 :goto_0 */
/* .line 88 */
} // .end local v0 # "index":I
/* :pswitch_3 */
/* const-string/jumbo v0, "start ctrl execute" */
android.util.Slog .i ( v1,v0 );
/* .line 89 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).ctrlExcute ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->ctrlExcute()V
/* .line 90 */
/* goto/16 :goto_0 */
/* .line 96 */
/* :pswitch_4 */
/* const-string/jumbo v0, "start create bin" */
android.util.Slog .i ( v1,v0 );
/* .line 97 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).createBin ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->createBin()V
/* .line 98 */
/* .line 92 */
/* :pswitch_5 */
/* const-string/jumbo v0, "start select bin" */
android.util.Slog .i ( v1,v0 );
/* .line 93 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).selectBin ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->selectBin()V
/* .line 94 */
/* .line 84 */
/* :pswitch_6 */
/* const-string/jumbo v0, "start check sum" */
android.util.Slog .i ( v1,v0 );
/* .line 85 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).checkSum ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->checkSum()V
/* .line 86 */
/* .line 80 */
/* :pswitch_7 */
/* const-string/jumbo v0, "start data packet" */
android.util.Slog .i ( v1,v0 );
/* .line 81 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).sendPacket ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendPacket()V
/* .line 82 */
/* .line 75 */
/* :pswitch_8 */
/* const-string/jumbo v0, "start create dat" */
android.util.Slog .i ( v1,v0 );
/* .line 76 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).createDat ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->createDat()V
/* .line 77 */
/* .line 71 */
/* :pswitch_9 */
/* const-string/jumbo v0, "start set prn" */
android.util.Slog .i ( v1,v0 );
/* .line 72 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).setPrn ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setPrn()V
/* .line 73 */
/* .line 67 */
/* :pswitch_a */
/* const-string/jumbo v0, "start select dat" */
android.util.Slog .i ( v1,v0 );
/* .line 68 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).selectDat ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->selectDat()V
/* .line 69 */
/* .line 63 */
/* :pswitch_b */
/* const-string/jumbo v0, "start BLE OTA ..." */
android.util.Slog .i ( v1,v0 );
/* .line 64 */
v0 = this.mGattDeviceItem;
v1 = com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile.OTA_FLIE_PATH;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).startUpgrade ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->startUpgrade(Ljava/lang/String;)V
/* .line 65 */
/* .line 59 */
/* :pswitch_c */
/* const-string/jumbo v0, "start get keyboard status" */
android.util.Slog .i ( v1,v0 );
/* .line 60 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).getKeyboardStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getKeyboardStatus()V
/* .line 61 */
/* .line 55 */
/* :pswitch_d */
/* const-string/jumbo v0, "start requestMtu" */
android.util.Slog .i ( v1,v0 );
/* .line 56 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).setMtu ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setMtu()V
/* .line 57 */
/* nop */
/* .line 116 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
