public class com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile {
	 /* .source "UpgradeZipFile.java" */
	 /* # static fields */
	 public static java.lang.String OTA_FLIE_PATH;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private mFileBufBin;
	 private mFileBufDat;
	 private mFileBufJson;
	 private java.lang.String mPath;
	 private Boolean mValid;
	 private java.lang.String mVersion;
	 /* # direct methods */
	 static com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ( ) {
		 /* .locals 1 */
		 /* .line 25 */
		 final String v0 = "odm/etc/Redmi_Keyboard_OTA.zip"; // const-string v0, "odm/etc/Redmi_Keyboard_OTA.zip"
		 return;
	 } // .end method
	 public com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ( ) {
		 /* .locals 5 */
		 /* .param p1, "path" # Ljava/lang/String; */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 22 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mFileBufJson = v0;
		 /* .line 23 */
		 this.mFileBufDat = v0;
		 /* .line 24 */
		 this.mFileBufBin = v0;
		 /* .line 36 */
		 final String v0 = "UpgradeZipFile"; // const-string v0, "UpgradeZipFile"
		 /* if-nez p1, :cond_0 */
		 /* .line 37 */
		 final String v1 = "UpgradeOta file Path is null"; // const-string v1, "UpgradeOta file Path is null"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 38 */
		 return;
		 /* .line 40 */
	 } // :cond_0
	 this.mPath = p1;
	 /* .line 41 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "UpgradeOta file Path:"; // const-string v2, "UpgradeOta file Path:"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.mPath;
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v0,v1 );
	 /* .line 42 */
	 v1 = this.mPath;
	 /* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->readFileToBuf(Ljava/lang/String;)V */
	 /* .line 43 */
	 v1 = this.mFileBufDat;
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 v1 = this.mFileBufBin;
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 44 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->mValid:Z */
			 /* .line 45 */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 v2 = this.mFileBufBin;
			 /* const/16 v3, 0x41 */
			 /* aget-byte v2, v2, v3 */
			 java.lang.Byte .valueOf ( v2 );
			 /* filled-new-array {v2}, [Ljava/lang/Object; */
			 final String v3 = "%02x"; // const-string v3, "%02x"
			 java.lang.String .format ( v3,v2 );
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v2 = this.mFileBufBin;
			 /* const/16 v4, 0x40 */
			 /* aget-byte v2, v2, v4 */
			 /* .line 46 */
			 java.lang.Byte .valueOf ( v2 );
			 /* filled-new-array {v2}, [Ljava/lang/Object; */
			 /* .line 45 */
			 java.lang.String .format ( v3,v2 );
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 this.mVersion = v1;
			 /* .line 47 */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v2 = "Bin version is "; // const-string v2, "Bin version is "
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v2 = this.mVersion;
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .i ( v0,v1 );
			 /* .line 49 */
		 } // :cond_1
		 final String v1 = "UpgradeOta file buff is null or length low than 6000"; // const-string v1, "UpgradeOta file buff is null or length low than 6000"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 51 */
	 } // :goto_0
	 return;
} // .end method
private void readFileToBuf ( java.lang.String p0 ) {
	 /* .locals 12 */
	 /* .param p1, "fileName" # Ljava/lang/String; */
	 /* .line 71 */
	 /* new-instance v0, Ljava/io/File; */
	 /* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
	 /* .line 72 */
	 /* .local v0, "file":Ljava/io/File; */
	 v1 = 	 (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
	 final String v2 = "UpgradeZipFile"; // const-string v2, "UpgradeZipFile"
	 /* if-nez v1, :cond_0 */
	 /* .line 73 */
	 final String v1 = "The Upgrade file does not exist."; // const-string v1, "The Upgrade file does not exist."
	 android.util.Slog .i ( v2,v1 );
	 /* .line 74 */
	 return;
	 /* .line 76 */
} // :cond_0
try { // :try_start_0
	 /* new-instance v1, Ljava/io/FileInputStream; */
	 /* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 77 */
	 /* .local v1, "fIn":Ljava/io/InputStream; */
	 try { // :try_start_1
		 v3 = 		 (( java.io.InputStream ) v1 ).available ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->available()I
		 /* const/high16 v4, 0xa00000 */
		 /* if-le v3, v4, :cond_1 */
		 /* .line 78 */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = "File too large: "; // const-string v4, "File too large: "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v4 = 		 (( java.io.InputStream ) v1 ).available ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->available()I
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v4 = " bytes (max 10 MB)"; // const-string v4, " bytes (max 10 MB)"
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v2,v3 );
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
		 /* .line 115 */
		 try { // :try_start_2
			 (( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
			 /* :try_end_2 */
			 /* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
			 /* .line 79 */
			 return;
			 /* .line 81 */
		 } // :cond_1
		 try { // :try_start_3
			 /* new-instance v3, Ljava/util/zip/ZipInputStream; */
			 /* invoke-direct {v3, v1}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V */
			 /* :try_end_3 */
			 /* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
			 /* .line 83 */
			 /* .local v3, "zipIn":Ljava/util/zip/ZipInputStream; */
			 try { // :try_start_4
				 (( java.util.zip.ZipInputStream ) v3 ).getNextEntry ( ); // invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
				 /* .line 84 */
				 /* .local v4, "zipEntry":Ljava/util/zip/ZipEntry; */
				 /* const/16 v5, 0x400 */
				 /* new-array v5, v5, [B */
				 /* .line 85 */
				 /* .local v5, "buffer":[B */
			 } // :goto_0
			 if ( v4 != null) { // if-eqz v4, :cond_6
				 /* .line 86 */
				 v6 = 				 (( java.util.zip.ZipEntry ) v4 ).isDirectory ( ); // invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->isDirectory()Z
				 /* if-nez v6, :cond_5 */
				 /* .line 87 */
				 (( java.util.zip.ZipEntry ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;
				 /* .line 88 */
				 /* .local v6, "zipFileName":Ljava/lang/String; */
				 (( java.util.zip.ZipEntry ) v4 ).getSize ( ); // invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getSize()J
				 /* move-result-wide v7 */
				 /* long-to-int v7, v7 */
				 /* .line 89 */
				 /* .local v7, "leng":I */
				 /* new-instance v8, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v9 = "ZipFileName:"; // const-string v9, "ZipFileName:"
				 (( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 final String v9 = "/"; // const-string v9, "/"
				 (( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .i ( v2,v8 );
				 /* .line 90 */
				 final String v8 = ".json"; // const-string v8, ".json"
				 v8 = 				 (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
				 if ( v8 != null) { // if-eqz v8, :cond_2
					 /* .line 91 */
					 /* new-array v8, v7, [B */
					 this.mFileBufJson = v8;
					 /* .line 92 */
					 (( java.util.zip.ZipInputStream ) v3 ).read ( v8 ); // invoke-virtual {v3, v8}, Ljava/util/zip/ZipInputStream;->read([B)I
					 /* .line 95 */
				 } // :cond_2
				 final String v8 = ".dat"; // const-string v8, ".dat"
				 v8 = 				 (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
				 if ( v8 != null) { // if-eqz v8, :cond_3
					 /* .line 96 */
					 /* new-array v8, v7, [B */
					 this.mFileBufDat = v8;
					 /* .line 97 */
					 (( java.util.zip.ZipInputStream ) v3 ).read ( v8 ); // invoke-virtual {v3, v8}, Ljava/util/zip/ZipInputStream;->read([B)I
					 /* .line 100 */
				 } // :cond_3
				 final String v8 = ".bin"; // const-string v8, ".bin"
				 v8 = 				 (( java.lang.String ) v6 ).contains ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
				 if ( v8 != null) { // if-eqz v8, :cond_5
					 /* .line 101 */
					 /* new-array v8, v7, [B */
					 this.mFileBufBin = v8;
					 /* .line 102 */
					 /* new-instance v8, Ljava/io/ByteArrayOutputStream; */
					 /* invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V */
					 /* .line 104 */
					 /* .local v8, "baos":Ljava/io/ByteArrayOutputStream; */
				 } // :goto_1
				 v9 = 				 (( java.util.zip.ZipInputStream ) v3 ).read ( v5 ); // invoke-virtual {v3, v5}, Ljava/util/zip/ZipInputStream;->read([B)I
				 /* move v10, v9 */
				 /* .local v10, "count":I */
				 int v11 = -1; // const/4 v11, -0x1
				 /* if-eq v9, v11, :cond_4 */
				 /* .line 105 */
				 int v9 = 0; // const/4 v9, 0x0
				 (( java.io.ByteArrayOutputStream ) v8 ).write ( v5, v9, v10 ); // invoke-virtual {v8, v5, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V
				 /* .line 107 */
			 } // :cond_4
			 (( java.io.ByteArrayOutputStream ) v8 ).toByteArray ( ); // invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
			 this.mFileBufBin = v9;
			 /* .line 112 */
		 } // .end local v6 # "zipFileName":Ljava/lang/String;
	 } // .end local v7 # "leng":I
} // .end local v8 # "baos":Ljava/io/ByteArrayOutputStream;
} // .end local v10 # "count":I
} // :cond_5
(( java.util.zip.ZipInputStream ) v3 ).getNextEntry ( ); // invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* move-object v4, v6 */
/* .line 114 */
} // .end local v4 # "zipEntry":Ljava/util/zip/ZipEntry;
} // .end local v5 # "buffer":[B
} // :cond_6
try { // :try_start_5
(( java.util.zip.ZipInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 115 */
} // .end local v3 # "zipIn":Ljava/util/zip/ZipInputStream;
try { // :try_start_6
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .line 117 */
} // .end local v1 # "fIn":Ljava/io/InputStream;
/* .line 81 */
/* .restart local v1 # "fIn":Ljava/io/InputStream; */
/* .restart local v3 # "zipIn":Ljava/util/zip/ZipInputStream; */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_7
(( java.util.zip.ZipInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_8
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local v1 # "fIn":Ljava/io/InputStream;
} // .end local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
} // .end local p1 # "fileName":Ljava/lang/String;
} // :goto_2
/* throw v4 */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* .line 76 */
} // .end local v3 # "zipIn":Ljava/util/zip/ZipInputStream;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local v1 # "fIn":Ljava/io/InputStream; */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile; */
/* .restart local p1 # "fileName":Ljava/lang/String; */
/* :catchall_2 */
/* move-exception v3 */
try { // :try_start_9
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_3 */
/* :catchall_3 */
/* move-exception v4 */
try { // :try_start_a
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
} // .end local p1 # "fileName":Ljava/lang/String;
} // :goto_3
/* throw v3 */
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_0 */
/* .line 115 */
} // .end local v1 # "fIn":Ljava/io/InputStream;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile; */
/* .restart local p1 # "fileName":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 116 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "readFileToBuf exception: "; // const-string v4, "readFileToBuf exception: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 118 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_4
return;
} // .end method
/* # virtual methods */
public getFileBufBin ( ) {
/* .locals 1 */
/* .line 125 */
v0 = this.mFileBufBin;
} // .end method
public Integer getFileBufBinCrc ( ) {
/* .locals 5 */
/* .line 152 */
v0 = this.mFileBufBin;
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, v0 */
/* if-lez v0, :cond_0 */
/* .line 153 */
/* new-instance v0, Ljava/util/zip/CRC32; */
/* invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V */
/* .line 154 */
/* .local v0, "crc32":Ljava/util/zip/CRC32; */
v1 = this.mFileBufBin;
(( java.util.zip.CRC32 ) v0 ).update ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update([B)V
/* .line 155 */
(( java.util.zip.CRC32 ) v0 ).getValue ( ); // invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J
/* move-result-wide v1 */
/* const-wide v3, 0xffffffffL */
/* and-long/2addr v1, v3 */
/* long-to-int v1, v1 */
/* .line 157 */
} // .end local v0 # "crc32":Ljava/util/zip/CRC32;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public getFileBufDat ( ) {
/* .locals 1 */
/* .line 121 */
v0 = this.mFileBufDat;
} // .end method
public Integer getFileBufDatCrc ( ) {
/* .locals 5 */
/* .line 143 */
v0 = this.mFileBufDat;
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, v0 */
/* if-lez v0, :cond_0 */
/* .line 144 */
/* new-instance v0, Ljava/util/zip/CRC32; */
/* invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V */
/* .line 145 */
/* .local v0, "crc32":Ljava/util/zip/CRC32; */
v1 = this.mFileBufDat;
(( java.util.zip.CRC32 ) v0 ).update ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update([B)V
/* .line 146 */
(( java.util.zip.CRC32 ) v0 ).getValue ( ); // invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J
/* move-result-wide v1 */
/* const-wide v3, 0xffffffffL */
/* and-long/2addr v1, v3 */
/* long-to-int v1, v1 */
/* .line 148 */
} // .end local v0 # "crc32":Ljava/util/zip/CRC32;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Double getPercent ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "packetIndex" # I */
/* .param p2, "packetLength" # I */
/* .line 131 */
v0 = this.mFileBufBin;
/* array-length v1, v0 */
/* const/16 v2, 0x40 */
/* if-le v1, v2, :cond_0 */
/* .line 132 */
/* mul-int v1, p1, p2 */
/* mul-int/lit8 v1, v1, 0x64 */
/* int-to-float v1, v1 */
/* array-length v0, v0 */
/* int-to-float v0, v0 */
/* div-float/2addr v1, v0 */
v0 = java.lang.Math .round ( v1 );
/* int-to-float v0, v0 */
/* .line 134 */
/* .local v0, "progress":F */
/* new-instance v1, Ljava/text/DecimalFormat; */
final String v2 = "#.00"; // const-string v2, "#.00"
/* invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V */
/* .line 135 */
/* .local v1, "df":Ljava/text/DecimalFormat; */
/* float-to-double v2, v0 */
(( java.text.DecimalFormat ) v1 ).format ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
/* .line 136 */
/* .local v2, "percent":Ljava/lang/String; */
java.lang.Double .parseDouble ( v2 );
/* move-result-wide v3 */
/* return-wide v3 */
/* .line 138 */
} // .end local v0 # "progress":F
} // .end local v1 # "df":Ljava/text/DecimalFormat;
} // .end local v2 # "percent":Ljava/lang/String;
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public java.lang.String getVersion ( ) {
/* .locals 1 */
/* .line 62 */
v0 = this.mVersion;
} // .end method
public Boolean isValidFile ( ) {
/* .locals 1 */
/* .line 55 */
v0 = this.mFileBufDat;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mFileBufBin;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 56 */
int v0 = 1; // const/4 v0, 0x1
/* .line 58 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setVersion ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 66 */
this.mVersion = p1;
/* .line 67 */
return;
} // .end method
