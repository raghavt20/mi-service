public class com.android.server.input.padkeyboard.bluetooth.upgrade.Constant {
	 /* .source "Constant.java" */
	 /* # static fields */
	 public static final java.lang.String ACTION_CONNECTION_STATE_CHANGED;
	 public static final java.util.UUID CLIENT_CHARACTERISTIC_CONFIG;
	 public static CMD_CTRL_CREATE_BIN;
	 public static CMD_CTRL_CREATE_DAT;
	 public static CMD_CTRL_SELECT_BIN;
	 public static CMD_CTRL_SELECT_DAT;
	 public static CMD_CTRL_SET_PRN;
	 public static CMD_DFU_CTRL_CAL_CHECKSUM;
	 public static CMD_DFU_CTRL_EXECUTE;
	 public static java.lang.String DFU_CONTROL_CHARACTER;
	 public static final Object DFU_CTRL_CAL_CHECKSUM;
	 public static final Object DFU_CTRL_CREATE;
	 public static final Object DFU_CTRL_EXECTUE;
	 public static final Object DFU_CTRL_RESERVE;
	 public static final Object DFU_CTRL_RESPONSE;
	 public static final Object DFU_CTRL_SELECT;
	 public static final Object DFU_CTRL_SET_PRN;
	 public static final Object DFU_FILE_BIN;
	 public static final Object DFU_FILE_DAT;
	 public static java.lang.String DFU_PACKET_CHARACTER;
	 public static java.lang.String DFU_SERVICE;
	 public static final Object DFU_SUCCESS;
	 public static java.lang.String DFU_VERSION_CHARACTER;
	 public static Integer GATT_DFU_PROFILE;
	 public static Integer GATT_HID_PROFILE;
	 public static java.lang.String HID_BATTERY_LEVEL;
	 public static java.lang.String HID_BATTERY_SERVICE;
	 public static java.lang.String HID_SERVICE;
	 public static final java.util.UUID UUID_BATTERY_LEVEL;
	 public static final java.util.UUID UUID_BATTERY_SERVICE;
	 public static final java.util.UUID UUID_DFU_CONTROL_CHARACTER;
	 public static final java.util.UUID UUID_DFU_PACKET_CHARACTER;
	 public static final java.util.UUID UUID_DFU_SERVICE;
	 public static final java.util.UUID UUID_DFU_VERSION_CHARACTER;
	 public static final java.util.UUID UUID_HID_SERVICE;
	 /* # direct methods */
	 static com.android.server.input.padkeyboard.bluetooth.upgrade.Constant ( ) {
		 /* .locals 5 */
		 /* .line 8 */
		 final String v0 = "0000fe59-0000-1000-8000-00805f9b34fb"; // const-string v0, "0000fe59-0000-1000-8000-00805f9b34fb"
		 /* .line 9 */
		 final String v1 = "8ec90001-f315-4f60-9fb8-838830daea50"; // const-string v1, "8ec90001-f315-4f60-9fb8-838830daea50"
		 /* .line 10 */
		 final String v1 = "8ec90002-f315-4f60-9fb8-838830daea50"; // const-string v1, "8ec90002-f315-4f60-9fb8-838830daea50"
		 /* .line 11 */
		 final String v1 = "00000003-0000-1000-8000-00805f9b34fb"; // const-string v1, "00000003-0000-1000-8000-00805f9b34fb"
		 /* .line 12 */
		 final String v1 = "00001812-0000-1000-8000-00805f9b34fb"; // const-string v1, "00001812-0000-1000-8000-00805f9b34fb"
		 /* .line 13 */
		 final String v1 = "0000180f-0000-1000-8000-00805f9b34fb"; // const-string v1, "0000180f-0000-1000-8000-00805f9b34fb"
		 /* .line 14 */
		 final String v1 = "00002a19-0000-1000-8000-00805f9b34fb"; // const-string v1, "00002a19-0000-1000-8000-00805f9b34fb"
		 /* .line 15 */
		 java.util.UUID .fromString ( v0 );
		 /* .line 16 */
		 v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.DFU_CONTROL_CHARACTER;
		 java.util.UUID .fromString ( v0 );
		 /* .line 17 */
		 v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.DFU_PACKET_CHARACTER;
		 java.util.UUID .fromString ( v0 );
		 /* .line 18 */
		 v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.DFU_VERSION_CHARACTER;
		 java.util.UUID .fromString ( v0 );
		 /* .line 19 */
		 v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.HID_SERVICE;
		 java.util.UUID .fromString ( v0 );
		 /* .line 20 */
		 v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.HID_BATTERY_SERVICE;
		 java.util.UUID .fromString ( v0 );
		 /* .line 21 */
		 v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.HID_BATTERY_LEVEL;
		 java.util.UUID .fromString ( v0 );
		 /* .line 22 */
		 /* new-instance v0, Ljava/util/UUID; */
		 /* const-wide v1, 0x290200001000L */
		 /* const-wide v3, -0x7fffff7fa064cb05L # -2.724079460785E-312 */
		 /* invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V */
		 /* .line 23 */
		 /* const/16 v0, 0x9 */
		 /* .line 24 */
		 /* const/16 v0, 0xa */
		 /* .line 40 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_0 */
		 /* .line 41 */
		 /* new-array v0, v0, [B */
		 /* fill-array-data v0, :array_1 */
		 /* .line 42 */
		 int v0 = 3; // const/4 v0, 0x3
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_2 */
		 /* .line 43 */
		 int v1 = 6; // const/4 v1, 0x6
		 /* new-array v2, v1, [B */
		 /* fill-array-data v2, :array_3 */
		 /* .line 45 */
		 /* new-array v1, v1, [B */
		 /* fill-array-data v1, :array_4 */
		 /* .line 47 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* new-array v2, v1, [B */
		 int v3 = 0; // const/4 v3, 0x0
		 /* aput-byte v0, v2, v3 */
		 /* .line 48 */
		 /* new-array v0, v1, [B */
		 int v1 = 4; // const/4 v1, 0x4
		 /* aput-byte v1, v0, v3 */
		 return;
		 /* nop */
		 /* :array_0 */
		 /* .array-data 1 */
		 /* 0x6t */
		 /* 0x1t */
	 } // .end array-data
	 /* nop */
	 /* :array_1 */
	 /* .array-data 1 */
	 /* 0x6t */
	 /* 0x2t */
} // .end array-data
/* nop */
/* :array_2 */
/* .array-data 1 */
/* 0x2t */
/* 0x0t */
/* 0x0t */
} // .end array-data
/* :array_3 */
/* .array-data 1 */
/* 0x1t */
/* 0x1t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
} // .end array-data
/* nop */
/* :array_4 */
/* .array-data 1 */
/* 0x1t */
/* 0x2t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
} // .end array-data
} // .end method
public com.android.server.input.padkeyboard.bluetooth.upgrade.Constant ( ) {
/* .locals 0 */
/* .line 5 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
