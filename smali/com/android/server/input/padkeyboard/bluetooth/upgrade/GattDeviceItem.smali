.class public Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;
.super Ljava/lang/Object;
.source "GattDeviceItem.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final TAG:Ljava/lang/String; = "GattDeviceItem"

.field public static final TYPE_CONTROL:I = 0x1

.field public static final TYPE_HID:I = 0x0

.field public static final TYPE_PACKET:I = 0x2


# instance fields
.field private mAddress:Ljava/lang/String;

.field public mBleKeyboardBatteryRead:Landroid/bluetooth/BluetoothGattCharacteristic;

.field public mBleKeyboardHidRead:Landroid/bluetooth/BluetoothGattCharacteristic;

.field public mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private volatile mConnected:Z

.field private mCurVersion:Ljava/lang/String;

.field private mDatDone:Z

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field final mDeviceBusyLock:Ljava/lang/Object;

.field public mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

.field public mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private mGatt:Landroid/bluetooth/BluetoothGatt;

.field private mHandler:Landroid/os/Handler;

.field mHandlerThread:Landroid/os/HandlerThread;

.field private mName:Ljava/lang/String;

.field private mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

.field private mOtaProgerss:D

.field private mOtaRunning:Z

.field private mOtaStartTime:J

.field private mPackageSize:I

.field private volatile mRequestCompleted:Z

.field public mSupportProfile:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGatt;)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    .line 101
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    .line 102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D

    .line 103
    const/16 v0, 0x14

    iput v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I

    .line 119
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDeviceBusyLock:Ljava/lang/Object;

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    .line 131
    invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getSupportProfile(Landroid/bluetooth/BluetoothGatt;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    .line 132
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    .line 133
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 134
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mAddress:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mName:Ljava/lang/String;

    .line 136
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "KEYBOARD_BLE_OTA"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandlerThread:Landroid/os/HandlerThread;

    .line 138
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 139
    new-instance v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;

    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;-><init>(Landroid/os/Looper;Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    .line 141
    :cond_0
    return-void
.end method

.method private checkVersion()Z
    .locals 2

    .line 617
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getVersion()Ljava/lang/String;

    move-result-object v0

    .line 618
    .local v0, "binVersion":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mCurVersion:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private getSupportProfile(Landroid/bluetooth/BluetoothGatt;)Ljava/util/List;
    .locals 8
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothGatt;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v0, "profiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_2

    .line 148
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object v1

    .line 149
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 150
    .local v2, "serviceUUIDList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothGattService;

    .line 151
    .local v4, "bluetoothGattService":Landroid/bluetooth/BluetoothGattService;
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    .end local v4    # "bluetoothGattService":Landroid/bluetooth/BluetoothGattService;
    goto :goto_0

    .line 155
    :cond_0
    sget-object v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_HID_SERVICE:Ljava/util/UUID;

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "GattDeviceItem"

    if-eqz v3, :cond_1

    .line 156
    const-string v3, "Support HID Profile"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    sget v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->GATT_HID_PROFILE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_HID_SERVICE:Ljava/util/UUID;

    invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v3

    .line 159
    .local v3, "service":Landroid/bluetooth/BluetoothGattService;
    if-eqz v3, :cond_1

    .line 160
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    .line 161
    .local v5, "size":I
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v6

    add-int/lit8 v7, v5, -0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 162
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v6

    add-int/lit8 v7, v5, -0x2

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BluetoothGattCharacteristic;

    iput-object v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 163
    sget-object v6, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectBleLocked(Z)V

    .line 164
    sget-object v6, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_BATTERY_SERVICE:Ljava/util/UUID;

    invoke-virtual {p1, v6}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v6

    .line 166
    .local v6, "batteryService":Landroid/bluetooth/BluetoothGattService;
    if-eqz v6, :cond_1

    .line 167
    sget-object v7, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_BATTERY_LEVEL:Ljava/util/UUID;

    invoke-virtual {v6, v7}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v7

    iput-object v7, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardBatteryRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 173
    .end local v3    # "service":Landroid/bluetooth/BluetoothGattService;
    .end local v5    # "size":I
    .end local v6    # "batteryService":Landroid/bluetooth/BluetoothGattService;
    :cond_1
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 175
    sget-object v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_SERVICE:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 176
    const-string v3, "Support DFU Profile"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    sget v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->GATT_DFU_PROFILE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    sget-object v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_SERVICE:Ljava/util/UUID;

    invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v3

    .line 179
    .restart local v3    # "service":Landroid/bluetooth/BluetoothGattService;
    if-eqz v3, :cond_2

    .line 180
    sget-object v4, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_CONTROL_CHARACTER:Ljava/util/UUID;

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 181
    sget-object v4, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_PACKET_CHARACTER:Ljava/util/UUID;

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 186
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    .end local v2    # "serviceUUIDList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "service":Landroid/bluetooth/BluetoothGattService;
    :cond_2
    return-object v0
.end method

.method private sendBinInfo(I)V
    .locals 10
    .param p1, "index"    # I

    .line 473
    const-string v0, "GattDeviceItem"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufBin()[B

    move-result-object v1

    .line 474
    .local v1, "fileBufBin":[B
    array-length v2, v1

    .line 475
    .local v2, "data_len":I
    iget v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I

    .line 476
    .local v3, "dfu_len":I
    add-int v4, v2, v3

    add-int/lit8 v4, v4, -0x1

    div-int/2addr v4, v3

    .line 477
    .local v4, "all":I
    if-nez p1, :cond_0

    .line 478
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "data_len:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", dfu_len:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", all:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    :cond_0
    add-int/lit8 v5, v4, -0x1

    if-ne p1, v5, :cond_1

    mul-int v5, p1, v3

    sub-int v5, v2, v5

    goto :goto_0

    :cond_1
    move v5, v3

    :goto_0
    new-array v5, v5, [B

    .line 481
    .local v5, "temp":[B
    mul-int v6, p1, v3

    array-length v7, v5

    const/4 v8, 0x0

    invoke-static {v1, v6, v5, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 482
    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 483
    iget-boolean v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->isOtaRun()Z

    move-result v6

    if-nez v6, :cond_2

    goto :goto_2

    .line 487
    :cond_2
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-virtual {v6, p1, v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getPercent(II)D

    move-result-wide v6

    .line 488
    .local v6, "percent":D
    iget-wide v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D

    cmpl-double v8, v6, v8

    if-lez v8, :cond_3

    .line 489
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "-- DFU progerss:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    iput-wide v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D

    .line 492
    :cond_3
    iget-object v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v8

    .line 493
    .local v8, "message":Landroid/os/Message;
    add-int/lit8 v9, p1, 0x1

    if-ge v9, v4, :cond_4

    .line 495
    const/16 v9, 0xd

    iput v9, v8, Landroid/os/Message;->what:I

    .line 496
    add-int/lit8 v9, p1, 0x1

    iput v9, v8, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 499
    :cond_4
    const/16 v9, 0x8

    iput v9, v8, Landroid/os/Message;->what:I

    .line 501
    :goto_1
    iget-object v9, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v9, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 505
    nop

    .end local v1    # "fileBufBin":[B
    .end local v2    # "data_len":I
    .end local v3    # "dfu_len":I
    .end local v4    # "all":I
    .end local v5    # "temp":[B
    .end local v6    # "percent":D
    .end local v8    # "message":Landroid/os/Message;
    goto :goto_3

    .line 484
    .restart local v1    # "fileBufBin":[B
    .restart local v2    # "data_len":I
    .restart local v3    # "dfu_len":I
    .restart local v4    # "all":I
    .restart local v5    # "temp":[B
    :cond_5
    :goto_2
    const-string/jumbo v6, "sendBinInfo: device has disconnected or ota failed"

    invoke-static {v0, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    return-void

    .line 503
    .end local v1    # "fileBufBin":[B
    .end local v2    # "data_len":I
    .end local v3    # "dfu_len":I
    .end local v4    # "all":I
    .end local v5    # "temp":[B
    :catch_0
    move-exception v1

    .line 504
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sendBinInfo exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method private sendCtrlCreateBin()V
    .locals 2

    .line 571
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 572
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 573
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 574
    return-void
.end method

.method private sendCtrlCreateDat()V
    .locals 2

    .line 583
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 584
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 585
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 586
    return-void
.end method

.method private sendCtrlExecute()V
    .locals 2

    .line 589
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 590
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0xb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 591
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 592
    return-void
.end method

.method private sendCtrlSelectBin()V
    .locals 2

    .line 559
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 560
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0x9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 561
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 562
    return-void
.end method

.method private sendCtrlSelectDat()V
    .locals 2

    .line 553
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 554
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 555
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 556
    return-void
.end method

.method private sendCtrlSetPrn()V
    .locals 2

    .line 565
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 566
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 567
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 568
    return-void
.end method

.method private sendDatInfo(I)V
    .locals 9
    .param p1, "index"    # I

    .line 444
    const-string v0, "GattDeviceItem"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufDat()[B

    move-result-object v1

    .line 445
    .local v1, "fileBufDat":[B
    array-length v2, v1

    .line 446
    .local v2, "data_len":I
    iget v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I

    .line 447
    .local v3, "dfu_len":I
    add-int v4, v2, v3

    add-int/lit8 v4, v4, -0x1

    div-int/2addr v4, v3

    .line 448
    .local v4, "all":I
    add-int/lit8 v5, v4, -0x1

    if-ne p1, v5, :cond_0

    mul-int v5, p1, v3

    sub-int v5, v2, v5

    goto :goto_0

    :cond_0
    move v5, v3

    :goto_0
    new-array v5, v5, [B

    .line 449
    .local v5, "temp":[B
    mul-int v6, p1, v3

    array-length v7, v5

    const/4 v8, 0x0

    invoke-static {v1, v6, v5, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 450
    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 451
    iget-boolean v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->isOtaRun()Z

    move-result v6

    if-nez v6, :cond_1

    goto :goto_2

    .line 455
    :cond_1
    iget-object v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 456
    .local v6, "message":Landroid/os/Message;
    add-int/lit8 v7, p1, 0x1

    if-ge v7, v4, :cond_2

    .line 458
    const/16 v7, 0xc

    iput v7, v6, Landroid/os/Message;->what:I

    .line 459
    add-int/lit8 v7, p1, 0x1

    iput v7, v6, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 462
    :cond_2
    const/16 v7, 0x8

    iput v7, v6, Landroid/os/Message;->what:I

    .line 464
    :goto_1
    iget-object v7, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 468
    nop

    .end local v1    # "fileBufDat":[B
    .end local v2    # "data_len":I
    .end local v3    # "dfu_len":I
    .end local v4    # "all":I
    .end local v5    # "temp":[B
    .end local v6    # "message":Landroid/os/Message;
    goto :goto_3

    .line 452
    .restart local v1    # "fileBufDat":[B
    .restart local v2    # "data_len":I
    .restart local v3    # "dfu_len":I
    .restart local v4    # "all":I
    .restart local v5    # "temp":[B
    :cond_3
    :goto_2
    const-string/jumbo v6, "sendDatInfo: device has disconnected or ota failed"

    invoke-static {v0, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 453
    return-void

    .line 466
    .end local v1    # "fileBufDat":[B
    .end local v2    # "data_len":I
    .end local v3    # "dfu_len":I
    .end local v4    # "all":I
    .end local v5    # "temp":[B
    :catch_0
    move-exception v1

    .line 467
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sendDatInfo exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    return-void
.end method

.method private sendDataPacket()V
    .locals 2

    .line 577
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 578
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x7

    iput v1, v0, Landroid/os/Message;->what:I

    .line 579
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 580
    return-void
.end method

.method private sendOtaResult(I)V
    .locals 2
    .param p1, "result"    # I

    .line 623
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 624
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0xe

    iput v1, v0, Landroid/os/Message;->what:I

    .line 625
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 626
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 627
    return-void
.end method

.method private writeCommand([BI)V
    .locals 4
    .param p1, "command"    # [B
    .param p2, "type"    # I

    .line 388
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    const-string v1, "GattDeviceItem"

    if-nez v0, :cond_0

    .line 389
    const-string/jumbo v0, "writeCommand failed gatt null"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    return-void

    .line 393
    :cond_0
    const/4 v0, 0x1

    if-nez p2, :cond_3

    .line 394
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v2, :cond_2

    .line 395
    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 396
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 398
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    .line 399
    .local v0, "ret":Z
    if-nez v0, :cond_1

    .line 400
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "write to BLE Hid fail! error code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 402
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "write to BLE Hid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    .line 403
    invoke-static {p1, v3}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 402
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    .end local v0    # "ret":Z
    :cond_2
    :goto_0
    return-void

    .line 410
    :cond_3
    const/4 v2, 0x2

    if-ne p2, v0, :cond_6

    .line 411
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_5

    .line 412
    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 414
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 415
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    .line 417
    .restart local v0    # "ret":Z
    if-nez v0, :cond_4

    .line 418
    const-string v2, "DfuControl Characteristic is false"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    .end local v0    # "ret":Z
    :cond_4
    goto :goto_1

    .line 421
    :cond_5
    const-string v0, "DfuControl Characteristic is null:"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :goto_1
    return-void

    .line 426
    :cond_6
    if-ne p2, v2, :cond_9

    .line 427
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v2, :cond_8

    .line 428
    invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 430
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 431
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    .line 432
    .restart local v0    # "ret":Z
    if-nez v0, :cond_7

    .line 433
    const-string v2, "DfuPacket Characteristic is false"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    .end local v0    # "ret":Z
    :cond_7
    goto :goto_2

    .line 436
    :cond_8
    const-string v0, "DfuPacket Characteristic is null:"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_9
    :goto_2
    return-void
.end method


# virtual methods
.method public checkSum()V
    .locals 2

    .line 734
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_DFU_CTRL_CAL_CHECKSUM:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 735
    return-void
.end method

.method public clear()V
    .locals 2

    .line 263
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 265
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    .line 267
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaRunning:Z

    .line 268
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    .line 269
    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z

    .line 270
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_1

    .line 271
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mAddress:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 274
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mAddress:Ljava/lang/String;

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mName:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 277
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mName:Ljava/lang/String;

    .line 279
    :cond_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 280
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    .line 282
    :cond_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    if-eqz v0, :cond_5

    .line 283
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    .line 285
    :cond_5
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mCurVersion:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 286
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mCurVersion:Ljava/lang/String;

    .line 288
    :cond_6
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_7

    .line 289
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 291
    :cond_7
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_8

    .line 292
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 294
    :cond_8
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_9

    .line 295
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidWrite:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 297
    :cond_9
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_a

    .line 298
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 300
    :cond_a
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardBatteryRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_b

    .line 301
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardBatteryRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 303
    :cond_b
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_c

    .line 304
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 305
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    .line 307
    :cond_c
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_d

    .line 308
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 309
    iput-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandlerThread:Landroid/os/HandlerThread;

    .line 311
    :cond_d
    return-void
.end method

.method public createBin()V
    .locals 5

    .line 746
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_CREATE_BIN:[B

    .line 747
    .local v0, "data":[B
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    .line 748
    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufBin()[B

    move-result-object v1

    array-length v1, v1

    .line 747
    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v1

    .line 749
    .local v1, "dat_len":[B
    const/4 v2, 0x3

    aget-byte v3, v1, v2

    const/4 v4, 0x2

    aput-byte v3, v0, v4

    .line 750
    aget-byte v3, v1, v4

    aput-byte v3, v0, v2

    .line 751
    const/4 v2, 0x1

    aget-byte v3, v1, v2

    const/4 v4, 0x4

    aput-byte v3, v0, v4

    .line 752
    const/4 v3, 0x0

    aget-byte v3, v1, v3

    const/4 v4, 0x5

    aput-byte v3, v0, v4

    .line 753
    invoke-virtual {p0, v0, v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 754
    return-void
.end method

.method public createDat()V
    .locals 6

    .line 722
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_CREATE_DAT:[B

    .line 723
    .local v0, "data":[B
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getOtaFile()Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    move-result-object v1

    .line 724
    .local v1, "otaBinFile":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    nop

    .line 725
    invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufDat()[B

    move-result-object v2

    array-length v2, v2

    .line 724
    invoke-static {v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->int2Bytes(I)[B

    move-result-object v2

    .line 726
    .local v2, "dat_len":[B
    const/4 v3, 0x3

    aget-byte v4, v2, v3

    const/4 v5, 0x2

    aput-byte v4, v0, v5

    .line 727
    aget-byte v4, v2, v5

    aput-byte v4, v0, v3

    .line 728
    const/4 v3, 0x1

    aget-byte v4, v2, v3

    const/4 v5, 0x4

    aput-byte v4, v0, v5

    .line 729
    const/4 v4, 0x0

    aget-byte v4, v2, v4

    const/4 v5, 0x5

    aput-byte v4, v0, v5

    .line 730
    invoke-virtual {p0, v0, v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 731
    return-void
.end method

.method public ctrlExcute()V
    .locals 2

    .line 738
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_DFU_CTRL_EXECUTE:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 739
    return-void
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getGatt()Landroid/bluetooth/BluetoothGatt;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    return-object v0
.end method

.method public getKeyboardStatus()V
    .locals 2

    .line 712
    sget-object v0, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil$BleDeviceUtil;->CMD_KB_STATUS:[B

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 714
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getOtaFile()Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    return-object v0
.end method

.method public getOtaProgerss()D
    .locals 2

    .line 237
    iget-wide v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D

    return-wide v0
.end method

.method public getPackageSize()I
    .locals 1

    .line 246
    iget v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mCurVersion:Ljava/lang/String;

    return-object v0
.end method

.method public isDfuDatDone()Z
    .locals 1

    .line 220
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    return v0
.end method

.method public isOtaRun()Z
    .locals 1

    .line 229
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaRunning:Z

    return v0
.end method

.method public notifyDeviceFree()V
    .locals 2

    .line 774
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDeviceBusyLock:Ljava/lang/Object;

    monitor-enter v0

    .line 775
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    .line 776
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDeviceBusyLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 777
    monitor-exit v0

    .line 778
    return-void

    .line 777
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public parseInfo([B)V
    .locals 7
    .param p1, "info"    # [B

    .line 645
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    const-string v1, "GattDeviceItem"

    if-nez v0, :cond_0

    .line 646
    const-string v0, "parseInfo: mGatt is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    return-void

    .line 650
    :cond_0
    const/4 v0, 0x0

    aget-byte v2, p1, v0

    const/16 v3, 0x60

    if-ne v2, v3, :cond_6

    .line 651
    const/4 v2, 0x2

    aget-byte v3, p1, v2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 652
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-- Recive: DFU_CTRL_RESPONSE Failed: cmd: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v3, p1, v4

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "result:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v2, p1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaFailed()V

    .line 655
    return-void

    .line 657
    :cond_1
    aget-byte v2, p1, v4

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_1

    .line 659
    :pswitch_1
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    if-eqz v0, :cond_2

    .line 660
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlCreateBin()V

    goto/16 :goto_1

    .line 662
    :cond_2
    const-string v0, "-- Recive: DAT DFU_CTRL_SELECT SUCCESS"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlSetPrn()V

    .line 665
    goto/16 :goto_1

    .line 694
    :pswitch_2
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    if-nez v2, :cond_3

    .line 695
    iput-boolean v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    .line 696
    const-string v0, "-- Recive: DAT DFU_CTRL_EXECTUE SUCCESS"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlSelectBin()V

    goto/16 :goto_1

    .line 699
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-- Recive: BIN DFU_CTRL_EXECTUE SUCCESS ota time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 700
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaStartTime:J

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 699
    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaResult(I)V

    goto/16 :goto_1

    .line 674
    :pswitch_3
    const/4 v0, 0x0

    .line 675
    .local v0, "file_crc":I
    iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    if-eqz v2, :cond_4

    .line 676
    const-string v2, "-- Recive: BIN DFU_CTRL_CAL_CHECKSUM SUCCESS"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufBinCrc()I

    move-result v0

    goto :goto_0

    .line 679
    :cond_4
    const-string v2, "-- Recive: DAT DFU_CTRL_CAL_CHECKSUM SUCCESS"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufDatCrc()I

    move-result v0

    .line 682
    :goto_0
    const/4 v2, 0x7

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0x8

    aget-byte v4, p1, v3

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v3, v4, 0x8

    add-int/2addr v2, v3

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    add-int/2addr v2, v3

    const/16 v3, 0xa

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x18

    add-int/2addr v2, v3

    .line 684
    .local v2, "crc":I
    if-ne v0, v2, :cond_5

    .line 685
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlExecute()V

    goto :goto_1

    .line 687
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-- Recive: DFU_CTRL_CAL_CHECKSUM crc Failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaFailed()V

    .line 692
    goto :goto_1

    .line 670
    .end local v0    # "file_crc":I
    .end local v2    # "crc":I
    :pswitch_4
    const-string v0, "-- Recive: DAT DFU_CTRL_SET_PRN SUCCESS"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlCreateDat()V

    .line 672
    goto :goto_1

    .line 667
    :pswitch_5
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendDataPacket()V

    .line 668
    nop

    .line 706
    :cond_6
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public selectBin()V
    .locals 2

    .line 742
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_SELECT_BIN:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 743
    return-void
.end method

.method public selectDat()V
    .locals 2

    .line 716
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_SELECT_DAT:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 717
    return-void
.end method

.method public sendData(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "index"    # I

    .line 765
    if-nez p1, :cond_0

    .line 766
    invoke-direct {p0, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendDatInfo(I)V

    .line 768
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 769
    invoke-direct {p0, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendBinInfo(I)V

    .line 771
    :cond_1
    return-void
.end method

.method public sendGetKeyboardStatus()V
    .locals 2

    .line 601
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 602
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 603
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 604
    return-void
.end method

.method public sendOtaFailed()V
    .locals 2

    .line 632
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 633
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 636
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    .line 637
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V

    .line 638
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaResult(I)V

    .line 639
    return-void
.end method

.method public sendPacket()V
    .locals 2

    .line 757
    iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 758
    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendBinInfo(I)V

    goto :goto_0

    .line 760
    :cond_0
    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendDatInfo(I)V

    .line 762
    :goto_0
    return-void
.end method

.method public sendSetMtu()V
    .locals 2

    .line 595
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 596
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 597
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 598
    return-void
.end method

.method public sendStartOta()V
    .locals 4

    .line 607
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 611
    .local v0, "message":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 613
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 614
    return-void
.end method

.method public setConnected(Z)V
    .locals 0
    .param p1, "connected"    # Z

    .line 190
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z

    .line 191
    return-void
.end method

.method public setDfuDatDone(Z)V
    .locals 0
    .param p1, "done"    # Z

    .line 224
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z

    .line 225
    return-void
.end method

.method public setMtu()V
    .locals 2

    .line 708
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    const/16 v1, 0x16b

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGatt;->requestMtu(I)Z

    .line 709
    return-void
.end method

.method public setNotificationCharacters()V
    .locals 4

    .line 314
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    if-eqz v0, :cond_5

    .line 315
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "GattDeviceItem"

    if-nez v0, :cond_0

    .line 316
    const-string v0, "Device SupportProfile is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    sget v2, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->GATT_DFU_PROFILE:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    .line 322
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_2

    .line 323
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 324
    const-string/jumbo v0, "set Notification DfuControl Characteristic failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuControl:Landroid/bluetooth/BluetoothGattCharacteristic;

    sget-object v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CLIENT_CHARACTERISTIC_CONFIG:Ljava/util/UUID;

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    .line 328
    .local v0, "descriptor":Landroid/bluetooth/BluetoothGattDescriptor;
    sget-object v3, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    .line 329
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    .line 331
    .end local v0    # "descriptor":Landroid/bluetooth/BluetoothGattDescriptor;
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDfuPacket:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_3

    .line 332
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_3

    .line 333
    const-string/jumbo v0, "set Notification DfuPacket Characteristic failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :cond_3
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mSupportProfile:Ljava/util/List;

    sget v3, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->GATT_HID_PROFILE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 339
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardHidRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_4

    .line 340
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 341
    const-string/jumbo v0, "set Notification Hid Characteristic failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_4
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mBleKeyboardBatteryRead:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_5

    .line 345
    iget-object v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 347
    const-string/jumbo v0, "set Notification Battery Characteristic failed"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_5
    return-void
.end method

.method public setOtaFile(Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;)V
    .locals 0
    .param p1, "otaFile"    # Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    .line 259
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaFile:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    .line 260
    return-void
.end method

.method public setOtaProgerss(D)V
    .locals 0
    .param p1, "otaProgerss"    # D

    .line 241
    iput-wide p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D

    .line 242
    return-void
.end method

.method public setOtaRun(Z)V
    .locals 0
    .param p1, "run"    # Z

    .line 233
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaRunning:Z

    .line 234
    return-void
.end method

.method public setPackageSize(I)V
    .locals 0
    .param p1, "size"    # I

    .line 250
    iput p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I

    .line 251
    return-void
.end method

.method public setPrn()V
    .locals 2

    .line 719
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_SET_PRN:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V

    .line 720
    return-void
.end method

.method public setVersionAndStartOta(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .line 198
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mCurVersion:Ljava/lang/String;

    .line 199
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendStartOta()V

    .line 200
    return-void
.end method

.method public startUpgrade(Ljava/lang/String;)V
    .locals 6
    .param p1, "binPath"    # Ljava/lang/String;

    .line 511
    const-string v0, "GattDeviceItem"

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->isOtaRun()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    const-string v1, "StartUpgrade ota is running"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    return-void

    .line 516
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V

    .line 519
    const/4 v2, 0x0

    .line 520
    .local v2, "zipValid":Z
    const/4 v3, 0x0

    .line 521
    .local v3, "upgradeFile":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    if-eqz p1, :cond_1

    .line 522
    new-instance v4, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;

    invoke-direct {v4, p1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    .line 523
    invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->isValidFile()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 524
    const/4 v2, 0x1

    .line 525
    invoke-virtual {p0, v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaFile(Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;)V

    .line 528
    :cond_1
    const/4 v4, 0x0

    if-nez v2, :cond_2

    .line 529
    const-string v1, "The OTA file unvalid!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    invoke-virtual {p0, v4}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V

    .line 531
    return-void

    .line 535
    :cond_2
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->checkVersion()Z

    move-result v5

    if-nez v5, :cond_3

    .line 536
    const-string/jumbo v1, "version not need to upgrade"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    invoke-virtual {p0, v4}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V

    .line 538
    return-void

    .line 542
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaStartTime:J

    .line 544
    invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaResult(I)V

    .line 546
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlSelectDat()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 549
    .end local v2    # "zipValid":Z
    .end local v3    # "upgradeFile":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
    goto :goto_0

    .line 547
    :catch_0
    move-exception v1

    .line 548
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startUpgrade exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public syncWriteCommand([BI)V
    .locals 5
    .param p1, "command"    # [B
    .param p2, "type"    # I

    .line 355
    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 385
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->writeCommand([BI)V

    .line 386
    return-void

    .line 358
    :cond_1
    :goto_0
    :try_start_0
    const-string v0, "GattDeviceItem"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "syncWriteCommand "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x12

    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " type = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDeviceBusyLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :try_start_1
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    if-nez v1, :cond_2

    .line 363
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDeviceBusyLock:Ljava/lang/Object;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V

    .line 365
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    if-nez v1, :cond_3

    .line 366
    const-string v1, "GattDeviceItem"

    const-string/jumbo v2, "syncWriteCommand ota failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaFailed()V

    .line 368
    monitor-exit v0

    return-void

    .line 370
    :cond_3
    iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z

    if-nez v1, :cond_4

    .line 371
    const-string v1, "GattDeviceItem"

    const-string/jumbo v2, "syncWriteCommand failed because device has disconnected"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    monitor-exit v0

    return-void

    .line 375
    :cond_4
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    .line 376
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->writeCommand([BI)V

    .line 377
    const-string v1, "GattDeviceItem"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "syncWriteCommand "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->Bytes2Hex([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", out mRequestCompleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    monitor-exit v0

    .line 382
    goto :goto_1

    .line 379
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;
    .end local p1    # "command":[B
    .end local p2    # "type":I
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 380
    .restart local p0    # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;
    .restart local p1    # "command":[B
    .restart local p2    # "type":I
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "GattDeviceItem"

    const-string v2, "Sleeping interrupted"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-void
.end method
