.class public Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;
.super Landroid/os/Handler;
.source "OtaHandler.java"


# static fields
.field public static final MSG_CHECK_SUM:I = 0x8

.field public static final MSG_CLEAR_DEVICE:I = 0xf

.field public static final MSG_CTRL_CREATE_BIN:I = 0xa

.field public static final MSG_CTRL_CREATE_DAT:I = 0x6

.field public static final MSG_CTRL_EXECUTE:I = 0xb

.field public static final MSG_CTRL_SELECT_BIN:I = 0x9

.field public static final MSG_CTRL_SELECT_DAT:I = 0x4

.field public static final MSG_CTRL_SET_PRN:I = 0x5

.field public static final MSG_DATA_PACKET:I = 0x7

.field public static final MSG_GET_KB_STATUS:I = 0x2

.field public static final MSG_OTA_RESULT:I = 0xe

.field public static final MSG_SEND_BIN:I = 0xd

.field public static final MSG_SEND_DAT:I = 0xc

.field public static final MSG_SET_MTU:I = 0x1

.field public static final MSG_START_OTA:I = 0x3

.field public static final OTA_FAIL:I = 0x2

.field public static final OTA_START:I = 0x1

.field public static final OTA_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "OtaHandler"

.field public static final TYPE_BIN:I = 0x1

.field public static final TYPE_DAT:I


# instance fields
.field mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;


# direct methods
.method constructor <init>(Landroid/os/Looper;Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;)V
    .locals 0
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "item"    # Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    .line 42
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 43
    iput-object p2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    .line 44
    return-void
.end method

.method private showOtaResult(I)V
    .locals 4
    .param p1, "result"    # I

    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "resId":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "showOtaResult: result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "OtaHandler"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 122
    const v0, 0x110f024e

    .line 124
    :cond_0
    if-nez p1, :cond_1

    .line 125
    const v0, 0x110f024f

    .line 127
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 128
    const v0, 0x110f024d

    .line 130
    :cond_2
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v1

    .line 131
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 132
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 48
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 49
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    const-string v1, "OtaHandler"

    if-nez v0, :cond_0

    .line 50
    const-string v0, "handleMessage: mGattDeviceItem null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    return-void

    .line 53
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown msg "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 110
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 111
    .local v0, "result":I
    invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->showOtaResult(I)V

    .line 112
    goto/16 :goto_0

    .line 106
    .end local v0    # "result":I
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 107
    .local v0, "index":I
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendData(II)V

    .line 108
    goto/16 :goto_0

    .line 101
    .end local v0    # "index":I
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 102
    .restart local v0    # "index":I
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendData(II)V

    .line 103
    goto/16 :goto_0

    .line 88
    .end local v0    # "index":I
    :pswitch_3
    const-string/jumbo v0, "start ctrl execute"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->ctrlExcute()V

    .line 90
    goto/16 :goto_0

    .line 96
    :pswitch_4
    const-string/jumbo v0, "start create bin"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->createBin()V

    .line 98
    goto :goto_0

    .line 92
    :pswitch_5
    const-string/jumbo v0, "start select bin"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->selectBin()V

    .line 94
    goto :goto_0

    .line 84
    :pswitch_6
    const-string/jumbo v0, "start check sum"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->checkSum()V

    .line 86
    goto :goto_0

    .line 80
    :pswitch_7
    const-string/jumbo v0, "start data packet"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendPacket()V

    .line 82
    goto :goto_0

    .line 75
    :pswitch_8
    const-string/jumbo v0, "start create dat"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->createDat()V

    .line 77
    goto :goto_0

    .line 71
    :pswitch_9
    const-string/jumbo v0, "start set prn"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setPrn()V

    .line 73
    goto :goto_0

    .line 67
    :pswitch_a
    const-string/jumbo v0, "start select dat"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->selectDat()V

    .line 69
    goto :goto_0

    .line 63
    :pswitch_b
    const-string/jumbo v0, "start BLE OTA ..."

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    sget-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->OTA_FLIE_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->startUpgrade(Ljava/lang/String;)V

    .line 65
    goto :goto_0

    .line 59
    :pswitch_c
    const-string/jumbo v0, "start get keyboard status"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getKeyboardStatus()V

    .line 61
    goto :goto_0

    .line 55
    :pswitch_d
    const-string/jumbo v0, "start requestMtu"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;->mGattDeviceItem:Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;

    invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setMtu()V

    .line 57
    nop

    .line 116
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
