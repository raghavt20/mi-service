.class public Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;
.super Ljava/lang/Object;
.source "Constant.java"


# static fields
.field public static final ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

.field public static final CLIENT_CHARACTERISTIC_CONFIG:Ljava/util/UUID;

.field public static CMD_CTRL_CREATE_BIN:[B = null

.field public static CMD_CTRL_CREATE_DAT:[B = null

.field public static CMD_CTRL_SELECT_BIN:[B = null

.field public static CMD_CTRL_SELECT_DAT:[B = null

.field public static CMD_CTRL_SET_PRN:[B = null

.field public static CMD_DFU_CTRL_CAL_CHECKSUM:[B = null

.field public static CMD_DFU_CTRL_EXECUTE:[B = null

.field public static DFU_CONTROL_CHARACTER:Ljava/lang/String; = null

.field public static final DFU_CTRL_CAL_CHECKSUM:B = 0x3t

.field public static final DFU_CTRL_CREATE:B = 0x1t

.field public static final DFU_CTRL_EXECTUE:B = 0x4t

.field public static final DFU_CTRL_RESERVE:B = 0x5t

.field public static final DFU_CTRL_RESPONSE:B = 0x60t

.field public static final DFU_CTRL_SELECT:B = 0x6t

.field public static final DFU_CTRL_SET_PRN:B = 0x2t

.field public static final DFU_FILE_BIN:B = 0x2t

.field public static final DFU_FILE_DAT:B = 0x1t

.field public static DFU_PACKET_CHARACTER:Ljava/lang/String; = null

.field public static DFU_SERVICE:Ljava/lang/String; = null

.field public static final DFU_SUCCESS:B = 0x1t

.field public static DFU_VERSION_CHARACTER:Ljava/lang/String;

.field public static GATT_DFU_PROFILE:I

.field public static GATT_HID_PROFILE:I

.field public static HID_BATTERY_LEVEL:Ljava/lang/String;

.field public static HID_BATTERY_SERVICE:Ljava/lang/String;

.field public static HID_SERVICE:Ljava/lang/String;

.field public static final UUID_BATTERY_LEVEL:Ljava/util/UUID;

.field public static final UUID_BATTERY_SERVICE:Ljava/util/UUID;

.field public static final UUID_DFU_CONTROL_CHARACTER:Ljava/util/UUID;

.field public static final UUID_DFU_PACKET_CHARACTER:Ljava/util/UUID;

.field public static final UUID_DFU_SERVICE:Ljava/util/UUID;

.field public static final UUID_DFU_VERSION_CHARACTER:Ljava/util/UUID;

.field public static final UUID_HID_SERVICE:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 8
    const-string v0, "0000fe59-0000-1000-8000-00805f9b34fb"

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_SERVICE:Ljava/lang/String;

    .line 9
    const-string v1, "8ec90001-f315-4f60-9fb8-838830daea50"

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_CONTROL_CHARACTER:Ljava/lang/String;

    .line 10
    const-string v1, "8ec90002-f315-4f60-9fb8-838830daea50"

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_PACKET_CHARACTER:Ljava/lang/String;

    .line 11
    const-string v1, "00000003-0000-1000-8000-00805f9b34fb"

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_VERSION_CHARACTER:Ljava/lang/String;

    .line 12
    const-string v1, "00001812-0000-1000-8000-00805f9b34fb"

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->HID_SERVICE:Ljava/lang/String;

    .line 13
    const-string v1, "0000180f-0000-1000-8000-00805f9b34fb"

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->HID_BATTERY_SERVICE:Ljava/lang/String;

    .line 14
    const-string v1, "00002a19-0000-1000-8000-00805f9b34fb"

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->HID_BATTERY_LEVEL:Ljava/lang/String;

    .line 15
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_SERVICE:Ljava/util/UUID;

    .line 16
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_CONTROL_CHARACTER:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_CONTROL_CHARACTER:Ljava/util/UUID;

    .line 17
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_PACKET_CHARACTER:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_PACKET_CHARACTER:Ljava/util/UUID;

    .line 18
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->DFU_VERSION_CHARACTER:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_DFU_VERSION_CHARACTER:Ljava/util/UUID;

    .line 19
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->HID_SERVICE:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_HID_SERVICE:Ljava/util/UUID;

    .line 20
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->HID_BATTERY_SERVICE:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_BATTERY_SERVICE:Ljava/util/UUID;

    .line 21
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->HID_BATTERY_LEVEL:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->UUID_BATTERY_LEVEL:Ljava/util/UUID;

    .line 22
    new-instance v0, Ljava/util/UUID;

    const-wide v1, 0x290200001000L

    const-wide v3, -0x7fffff7fa064cb05L    # -2.724079460785E-312

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CLIENT_CHARACTERISTIC_CONFIG:Ljava/util/UUID;

    .line 23
    const/16 v0, 0x9

    sput v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->GATT_DFU_PROFILE:I

    .line 24
    const/16 v0, 0xa

    sput v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->GATT_HID_PROFILE:I

    .line 40
    const/4 v0, 0x2

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_SELECT_DAT:[B

    .line 41
    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_SELECT_BIN:[B

    .line 42
    const/4 v0, 0x3

    new-array v1, v0, [B

    fill-array-data v1, :array_2

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_SET_PRN:[B

    .line 43
    const/4 v1, 0x6

    new-array v2, v1, [B

    fill-array-data v2, :array_3

    sput-object v2, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_CREATE_DAT:[B

    .line 45
    new-array v1, v1, [B

    fill-array-data v1, :array_4

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_CTRL_CREATE_BIN:[B

    .line 47
    const/4 v1, 0x1

    new-array v2, v1, [B

    const/4 v3, 0x0

    aput-byte v0, v2, v3

    sput-object v2, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_DFU_CTRL_CAL_CHECKSUM:[B

    .line 48
    new-array v0, v1, [B

    const/4 v1, 0x4

    aput-byte v1, v0, v3

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/Constant;->CMD_DFU_CTRL_EXECUTE:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x6t
        0x1t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x6t
        0x2t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x2t
        0x0t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x1t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
