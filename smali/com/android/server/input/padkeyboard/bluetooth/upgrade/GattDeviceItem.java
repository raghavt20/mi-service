public class com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem implements java.io.Serializable {
	 /* .source "GattDeviceItem.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 public static final Integer TYPE_CONTROL;
	 public static final Integer TYPE_HID;
	 public static final Integer TYPE_PACKET;
	 /* # instance fields */
	 private java.lang.String mAddress;
	 public android.bluetooth.BluetoothGattCharacteristic mBleKeyboardBatteryRead;
	 public android.bluetooth.BluetoothGattCharacteristic mBleKeyboardHidRead;
	 public android.bluetooth.BluetoothGattCharacteristic mBleKeyboardHidWrite;
	 private volatile Boolean mConnected;
	 private java.lang.String mCurVersion;
	 private Boolean mDatDone;
	 private android.bluetooth.BluetoothDevice mDevice;
	 final java.lang.Object mDeviceBusyLock;
	 public android.bluetooth.BluetoothGattCharacteristic mDfuControl;
	 public android.bluetooth.BluetoothGattCharacteristic mDfuPacket;
	 private android.bluetooth.BluetoothGatt mGatt;
	 private android.os.Handler mHandler;
	 android.os.HandlerThread mHandlerThread;
	 private java.lang.String mName;
	 private com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile mOtaFile;
	 private Double mOtaProgerss;
	 private Boolean mOtaRunning;
	 private Long mOtaStartTime;
	 private Integer mPackageSize;
	 private volatile Boolean mRequestCompleted;
	 public java.util.List mSupportProfile;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ( ) {
/* .locals 2 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .line 130 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z */
/* .line 90 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mSupportProfile = v1;
/* .line 101 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
/* .line 102 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D */
/* .line 103 */
/* const/16 v0, 0x14 */
/* iput v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I */
/* .line 119 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mDeviceBusyLock = v0;
/* .line 124 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
/* .line 131 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getSupportProfile(Landroid/bluetooth/BluetoothGatt;)Ljava/util/List; */
this.mSupportProfile = v0;
/* .line 132 */
this.mGatt = p1;
/* .line 133 */
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
this.mDevice = v0;
/* .line 134 */
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v0 ).getAddress ( ); // invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
this.mAddress = v0;
/* .line 135 */
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v0 ).getName ( ); // invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
this.mName = v0;
/* .line 136 */
v0 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 137 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "KEYBOARD_BLE_OTA"; // const-string v1, "KEYBOARD_BLE_OTA"
	 /* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 this.mHandlerThread = v0;
	 /* .line 138 */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 139 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler; */
	 v1 = this.mHandlerThread;
	 (( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1, p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/OtaHandler;-><init>(Landroid/os/Looper;Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;)V */
	 this.mHandler = v0;
	 /* .line 141 */
} // :cond_0
return;
} // .end method
private Boolean checkVersion ( ) {
/* .locals 2 */
/* .line 617 */
v0 = this.mOtaFile;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getVersion()Ljava/lang/String;
/* .line 618 */
/* .local v0, "binVersion":Ljava/lang/String; */
v1 = this.mCurVersion;
if ( v1 != null) { // if-eqz v1, :cond_0
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 v1 = 		 (( java.lang.String ) v0 ).compareTo ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
		 /* if-lez v1, :cond_0 */
		 int v1 = 1; // const/4 v1, 0x1
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private java.util.List getSupportProfile ( android.bluetooth.BluetoothGatt p0 ) {
/* .locals 8 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/bluetooth/BluetoothGatt;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 145 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 146 */
/* .local v0, "profiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 148 */
(( android.bluetooth.BluetoothGatt ) p1 ).getServices ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;
/* .line 149 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 150 */
/* .local v2, "serviceUUIDList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Landroid/bluetooth/BluetoothGattService; */
/* .line 151 */
/* .local v4, "bluetoothGattService":Landroid/bluetooth/BluetoothGattService; */
(( android.bluetooth.BluetoothGattService ) v4 ).getUuid ( ); // invoke-virtual {v4}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;
(( java.util.UUID ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;
(( java.lang.String ) v5 ).toUpperCase ( ); // invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 152 */
} // .end local v4 # "bluetoothGattService":Landroid/bluetooth/BluetoothGattService;
/* .line 155 */
} // :cond_0
v3 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_HID_SERVICE;
(( java.util.UUID ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).toUpperCase ( ); // invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
final String v4 = "GattDeviceItem"; // const-string v4, "GattDeviceItem"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 156 */
final String v3 = "Support HID Profile"; // const-string v3, "Support HID Profile"
android.util.Slog .i ( v4,v3 );
/* .line 157 */
java.lang.Integer .valueOf ( v3 );
/* .line 158 */
v3 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_HID_SERVICE;
(( android.bluetooth.BluetoothGatt ) p1 ).getService ( v3 ); // invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;
/* .line 159 */
/* .local v3, "service":Landroid/bluetooth/BluetoothGattService; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 160 */
v5 = (( android.bluetooth.BluetoothGattService ) v3 ).getCharacteristics ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;
/* .line 161 */
/* .local v5, "size":I */
(( android.bluetooth.BluetoothGattService ) v3 ).getCharacteristics ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;
/* add-int/lit8 v7, v5, -0x1 */
/* check-cast v6, Landroid/bluetooth/BluetoothGattCharacteristic; */
this.mBleKeyboardHidRead = v6;
/* .line 162 */
(( android.bluetooth.BluetoothGattService ) v3 ).getCharacteristics ( ); // invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;
/* add-int/lit8 v7, v5, -0x2 */
/* check-cast v6, Landroid/bluetooth/BluetoothGattCharacteristic; */
this.mBleKeyboardHidWrite = v6;
/* .line 163 */
v6 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
int v7 = 1; // const/4 v7, 0x1
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v6 ).setConnectBleLocked ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectBleLocked(Z)V
/* .line 164 */
v6 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_BATTERY_SERVICE;
(( android.bluetooth.BluetoothGatt ) p1 ).getService ( v6 ); // invoke-virtual {p1, v6}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;
/* .line 166 */
/* .local v6, "batteryService":Landroid/bluetooth/BluetoothGattService; */
if ( v6 != null) { // if-eqz v6, :cond_1
	 /* .line 167 */
	 v7 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_BATTERY_LEVEL;
	 (( android.bluetooth.BluetoothGattService ) v6 ).getCharacteristic ( v7 ); // invoke-virtual {v6, v7}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
	 this.mBleKeyboardBatteryRead = v7;
	 /* .line 173 */
} // .end local v3 # "service":Landroid/bluetooth/BluetoothGattService;
} // .end local v5 # "size":I
} // .end local v6 # "batteryService":Landroid/bluetooth/BluetoothGattService;
} // :cond_1
v3 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 175 */
v3 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.DFU_SERVICE;
(( java.lang.String ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).toUpperCase ( ); // invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 176 */
final String v3 = "Support DFU Profile"; // const-string v3, "Support DFU Profile"
android.util.Slog .i ( v4,v3 );
/* .line 177 */
java.lang.Integer .valueOf ( v3 );
/* .line 178 */
v3 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_DFU_SERVICE;
(( android.bluetooth.BluetoothGatt ) p1 ).getService ( v3 ); // invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;
/* .line 179 */
/* .restart local v3 # "service":Landroid/bluetooth/BluetoothGattService; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 180 */
v4 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_DFU_CONTROL_CHARACTER;
(( android.bluetooth.BluetoothGattService ) v3 ).getCharacteristic ( v4 ); // invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
this.mDfuControl = v4;
/* .line 181 */
v4 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_DFU_PACKET_CHARACTER;
(( android.bluetooth.BluetoothGattService ) v3 ).getCharacteristic ( v4 ); // invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
this.mDfuPacket = v4;
/* .line 186 */
} // .end local v1 # "list":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
} // .end local v2 # "serviceUUIDList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v3 # "service":Landroid/bluetooth/BluetoothGattService;
} // :cond_2
} // .end method
private void sendBinInfo ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "index" # I */
/* .line 473 */
final String v0 = "GattDeviceItem"; // const-string v0, "GattDeviceItem"
try { // :try_start_0
v1 = this.mOtaFile;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v1 ).getFileBufBin ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufBin()[B
/* .line 474 */
/* .local v1, "fileBufBin":[B */
/* array-length v2, v1 */
/* .line 475 */
/* .local v2, "data_len":I */
/* iget v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I */
/* .line 476 */
/* .local v3, "dfu_len":I */
/* add-int v4, v2, v3 */
/* add-int/lit8 v4, v4, -0x1 */
/* div-int/2addr v4, v3 */
/* .line 477 */
/* .local v4, "all":I */
/* if-nez p1, :cond_0 */
/* .line 478 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "data_len:"; // const-string v6, "data_len:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", dfu_len:"; // const-string v6, ", dfu_len:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", all:"; // const-string v6, ", all:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v5 );
/* .line 480 */
} // :cond_0
/* add-int/lit8 v5, v4, -0x1 */
/* if-ne p1, v5, :cond_1 */
/* mul-int v5, p1, v3 */
/* sub-int v5, v2, v5 */
} // :cond_1
/* move v5, v3 */
} // :goto_0
/* new-array v5, v5, [B */
/* .line 481 */
/* .local v5, "temp":[B */
/* mul-int v6, p1, v3 */
/* array-length v7, v5 */
int v8 = 0; // const/4 v8, 0x0
java.lang.System .arraycopy ( v1,v6,v5,v8,v7 );
/* .line 482 */
int v6 = 2; // const/4 v6, 0x2
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 483 */
/* iget-boolean v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z */
if ( v6 != null) { // if-eqz v6, :cond_5
v6 = (( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).isOtaRun ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->isOtaRun()Z
/* if-nez v6, :cond_2 */
/* .line 487 */
} // :cond_2
v6 = this.mOtaFile;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v6 ).getPercent ( p1, v3 ); // invoke-virtual {v6, p1, v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getPercent(II)D
/* move-result-wide v6 */
/* .line 488 */
/* .local v6, "percent":D */
/* iget-wide v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D */
/* cmpl-double v8, v6, v8 */
/* if-lez v8, :cond_3 */
/* .line 489 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "-- DFU progerss:"; // const-string v9, "-- DFU progerss:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v8 );
/* .line 490 */
/* iput-wide v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D */
/* .line 492 */
} // :cond_3
v8 = this.mHandler;
(( android.os.Handler ) v8 ).obtainMessage ( ); // invoke-virtual {v8}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 493 */
/* .local v8, "message":Landroid/os/Message; */
/* add-int/lit8 v9, p1, 0x1 */
/* if-ge v9, v4, :cond_4 */
/* .line 495 */
/* const/16 v9, 0xd */
/* iput v9, v8, Landroid/os/Message;->what:I */
/* .line 496 */
/* add-int/lit8 v9, p1, 0x1 */
/* iput v9, v8, Landroid/os/Message;->arg1:I */
/* .line 499 */
} // :cond_4
/* const/16 v9, 0x8 */
/* iput v9, v8, Landroid/os/Message;->what:I */
/* .line 501 */
} // :goto_1
v9 = this.mHandler;
(( android.os.Handler ) v9 ).sendMessage ( v8 ); // invoke-virtual {v9, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 505 */
/* nop */
} // .end local v1 # "fileBufBin":[B
} // .end local v2 # "data_len":I
} // .end local v3 # "dfu_len":I
} // .end local v4 # "all":I
} // .end local v5 # "temp":[B
} // .end local v6 # "percent":D
} // .end local v8 # "message":Landroid/os/Message;
/* .line 484 */
/* .restart local v1 # "fileBufBin":[B */
/* .restart local v2 # "data_len":I */
/* .restart local v3 # "dfu_len":I */
/* .restart local v4 # "all":I */
/* .restart local v5 # "temp":[B */
} // :cond_5
} // :goto_2
/* const-string/jumbo v6, "sendBinInfo: device has disconnected or ota failed" */
android.util.Slog .i ( v0,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 485 */
return;
/* .line 503 */
} // .end local v1 # "fileBufBin":[B
} // .end local v2 # "data_len":I
} // .end local v3 # "dfu_len":I
} // .end local v4 # "all":I
} // .end local v5 # "temp":[B
/* :catch_0 */
/* move-exception v1 */
/* .line 504 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "sendBinInfo exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 506 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
private void sendCtrlCreateBin ( ) {
/* .locals 2 */
/* .line 571 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 572 */
/* .local v0, "message":Landroid/os/Message; */
/* const/16 v1, 0xa */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 573 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 574 */
return;
} // .end method
private void sendCtrlCreateDat ( ) {
/* .locals 2 */
/* .line 583 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 584 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 6; // const/4 v1, 0x6
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 585 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 586 */
return;
} // .end method
private void sendCtrlExecute ( ) {
/* .locals 2 */
/* .line 589 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 590 */
/* .local v0, "message":Landroid/os/Message; */
/* const/16 v1, 0xb */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 591 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 592 */
return;
} // .end method
private void sendCtrlSelectBin ( ) {
/* .locals 2 */
/* .line 559 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 560 */
/* .local v0, "message":Landroid/os/Message; */
/* const/16 v1, 0x9 */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 561 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 562 */
return;
} // .end method
private void sendCtrlSelectDat ( ) {
/* .locals 2 */
/* .line 553 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 554 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 4; // const/4 v1, 0x4
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 555 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 556 */
return;
} // .end method
private void sendCtrlSetPrn ( ) {
/* .locals 2 */
/* .line 565 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 566 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 5; // const/4 v1, 0x5
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 567 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 568 */
return;
} // .end method
private void sendDatInfo ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "index" # I */
/* .line 444 */
final String v0 = "GattDeviceItem"; // const-string v0, "GattDeviceItem"
try { // :try_start_0
v1 = this.mOtaFile;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v1 ).getFileBufDat ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufDat()[B
/* .line 445 */
/* .local v1, "fileBufDat":[B */
/* array-length v2, v1 */
/* .line 446 */
/* .local v2, "data_len":I */
/* iget v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I */
/* .line 447 */
/* .local v3, "dfu_len":I */
/* add-int v4, v2, v3 */
/* add-int/lit8 v4, v4, -0x1 */
/* div-int/2addr v4, v3 */
/* .line 448 */
/* .local v4, "all":I */
/* add-int/lit8 v5, v4, -0x1 */
/* if-ne p1, v5, :cond_0 */
/* mul-int v5, p1, v3 */
/* sub-int v5, v2, v5 */
} // :cond_0
/* move v5, v3 */
} // :goto_0
/* new-array v5, v5, [B */
/* .line 449 */
/* .local v5, "temp":[B */
/* mul-int v6, p1, v3 */
/* array-length v7, v5 */
int v8 = 0; // const/4 v8, 0x0
java.lang.System .arraycopy ( v1,v6,v5,v8,v7 );
/* .line 450 */
int v6 = 2; // const/4 v6, 0x2
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 451 */
/* iget-boolean v6, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
v6 = (( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).isOtaRun ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->isOtaRun()Z
/* if-nez v6, :cond_1 */
/* .line 455 */
} // :cond_1
v6 = this.mHandler;
(( android.os.Handler ) v6 ).obtainMessage ( ); // invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 456 */
/* .local v6, "message":Landroid/os/Message; */
/* add-int/lit8 v7, p1, 0x1 */
/* if-ge v7, v4, :cond_2 */
/* .line 458 */
/* const/16 v7, 0xc */
/* iput v7, v6, Landroid/os/Message;->what:I */
/* .line 459 */
/* add-int/lit8 v7, p1, 0x1 */
/* iput v7, v6, Landroid/os/Message;->arg1:I */
/* .line 462 */
} // :cond_2
/* const/16 v7, 0x8 */
/* iput v7, v6, Landroid/os/Message;->what:I */
/* .line 464 */
} // :goto_1
v7 = this.mHandler;
(( android.os.Handler ) v7 ).sendMessage ( v6 ); // invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 468 */
/* nop */
} // .end local v1 # "fileBufDat":[B
} // .end local v2 # "data_len":I
} // .end local v3 # "dfu_len":I
} // .end local v4 # "all":I
} // .end local v5 # "temp":[B
} // .end local v6 # "message":Landroid/os/Message;
/* .line 452 */
/* .restart local v1 # "fileBufDat":[B */
/* .restart local v2 # "data_len":I */
/* .restart local v3 # "dfu_len":I */
/* .restart local v4 # "all":I */
/* .restart local v5 # "temp":[B */
} // :cond_3
} // :goto_2
/* const-string/jumbo v6, "sendDatInfo: device has disconnected or ota failed" */
android.util.Slog .i ( v0,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 453 */
return;
/* .line 466 */
} // .end local v1 # "fileBufDat":[B
} // .end local v2 # "data_len":I
} // .end local v3 # "dfu_len":I
} // .end local v4 # "all":I
} // .end local v5 # "temp":[B
/* :catch_0 */
/* move-exception v1 */
/* .line 467 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "sendDatInfo exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 469 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_3
return;
} // .end method
private void sendDataPacket ( ) {
/* .locals 2 */
/* .line 577 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 578 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 7; // const/4 v1, 0x7
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 579 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 580 */
return;
} // .end method
private void sendOtaResult ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "result" # I */
/* .line 623 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 624 */
/* .local v0, "message":Landroid/os/Message; */
/* const/16 v1, 0xe */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 625 */
/* iput p1, v0, Landroid/os/Message;->arg1:I */
/* .line 626 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 627 */
return;
} // .end method
private void writeCommand ( Object[] p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "command" # [B */
/* .param p2, "type" # I */
/* .line 388 */
v0 = this.mGatt;
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
/* if-nez v0, :cond_0 */
/* .line 389 */
/* const-string/jumbo v0, "writeCommand failed gatt null" */
android.util.Slog .i ( v1,v0 );
/* .line 390 */
return;
/* .line 393 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-nez p2, :cond_3 */
/* .line 394 */
v2 = this.mBleKeyboardHidWrite;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 395 */
(( android.bluetooth.BluetoothGattCharacteristic ) v2 ).setValue ( p1 ); // invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z
/* .line 396 */
v2 = this.mBleKeyboardHidWrite;
(( android.bluetooth.BluetoothGattCharacteristic ) v2 ).setWriteType ( v0 ); // invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V
/* .line 398 */
v0 = this.mGatt;
v2 = this.mBleKeyboardHidWrite;
v0 = (( android.bluetooth.BluetoothGatt ) v0 ).writeCharacteristic ( v2 ); // invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z
/* .line 399 */
/* .local v0, "ret":Z */
/* if-nez v0, :cond_1 */
/* .line 400 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "write to BLE Hid fail! error code:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 402 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "write to BLE Hid:" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v3, p1 */
/* .line 403 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 402 */
android.util.Slog .i ( v1,v2 );
/* .line 407 */
} // .end local v0 # "ret":Z
} // :cond_2
} // :goto_0
return;
/* .line 410 */
} // :cond_3
int v2 = 2; // const/4 v2, 0x2
/* if-ne p2, v0, :cond_6 */
/* .line 411 */
v0 = this.mDfuControl;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 412 */
(( android.bluetooth.BluetoothGattCharacteristic ) v0 ).setWriteType ( v2 ); // invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V
/* .line 414 */
v0 = this.mDfuControl;
(( android.bluetooth.BluetoothGattCharacteristic ) v0 ).setValue ( p1 ); // invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z
/* .line 415 */
v0 = this.mGatt;
v2 = this.mDfuControl;
v0 = (( android.bluetooth.BluetoothGatt ) v0 ).writeCharacteristic ( v2 ); // invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z
/* .line 417 */
/* .restart local v0 # "ret":Z */
/* if-nez v0, :cond_4 */
/* .line 418 */
final String v2 = "DfuControl Characteristic is false"; // const-string v2, "DfuControl Characteristic is false"
android.util.Slog .e ( v1,v2 );
/* .line 420 */
} // .end local v0 # "ret":Z
} // :cond_4
/* .line 421 */
} // :cond_5
final String v0 = "DfuControl Characteristic is null:"; // const-string v0, "DfuControl Characteristic is null:"
android.util.Slog .i ( v1,v0 );
/* .line 423 */
} // :goto_1
return;
/* .line 426 */
} // :cond_6
/* if-ne p2, v2, :cond_9 */
/* .line 427 */
v2 = this.mDfuPacket;
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 428 */
(( android.bluetooth.BluetoothGattCharacteristic ) v2 ).setWriteType ( v0 ); // invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V
/* .line 430 */
v0 = this.mDfuPacket;
(( android.bluetooth.BluetoothGattCharacteristic ) v0 ).setValue ( p1 ); // invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z
/* .line 431 */
v0 = this.mGatt;
v2 = this.mDfuPacket;
v0 = (( android.bluetooth.BluetoothGatt ) v0 ).writeCharacteristic ( v2 ); // invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z
/* .line 432 */
/* .restart local v0 # "ret":Z */
/* if-nez v0, :cond_7 */
/* .line 433 */
final String v2 = "DfuPacket Characteristic is false"; // const-string v2, "DfuPacket Characteristic is false"
android.util.Slog .i ( v1,v2 );
/* .line 435 */
} // .end local v0 # "ret":Z
} // :cond_7
/* .line 436 */
} // :cond_8
final String v0 = "DfuPacket Characteristic is null:"; // const-string v0, "DfuPacket Characteristic is null:"
android.util.Slog .i ( v1,v0 );
/* .line 439 */
} // :cond_9
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void checkSum ( ) {
/* .locals 2 */
/* .line 734 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_DFU_CTRL_CAL_CHECKSUM;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 735 */
return;
} // .end method
public void clear ( ) {
/* .locals 2 */
/* .line 263 */
v0 = this.mGatt;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 264 */
(( android.bluetooth.BluetoothGatt ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V
/* .line 265 */
this.mGatt = v1;
/* .line 267 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaRunning:Z */
/* .line 268 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
/* .line 269 */
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z */
/* .line 270 */
v0 = this.mDevice;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 271 */
this.mDevice = v1;
/* .line 273 */
} // :cond_1
v0 = this.mAddress;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 274 */
this.mAddress = v1;
/* .line 276 */
} // :cond_2
v0 = this.mName;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 277 */
this.mName = v1;
/* .line 279 */
} // :cond_3
v0 = this.mSupportProfile;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 280 */
this.mSupportProfile = v1;
/* .line 282 */
} // :cond_4
v0 = this.mOtaFile;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 283 */
this.mOtaFile = v1;
/* .line 285 */
} // :cond_5
v0 = this.mCurVersion;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 286 */
this.mCurVersion = v1;
/* .line 288 */
} // :cond_6
v0 = this.mDfuControl;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 289 */
this.mDfuControl = v1;
/* .line 291 */
} // :cond_7
v0 = this.mDfuPacket;
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 292 */
this.mDfuPacket = v1;
/* .line 294 */
} // :cond_8
v0 = this.mBleKeyboardHidWrite;
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 295 */
this.mBleKeyboardHidWrite = v1;
/* .line 297 */
} // :cond_9
v0 = this.mBleKeyboardHidRead;
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 298 */
this.mBleKeyboardHidRead = v1;
/* .line 300 */
} // :cond_a
v0 = this.mBleKeyboardBatteryRead;
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 301 */
this.mBleKeyboardBatteryRead = v1;
/* .line 303 */
} // :cond_b
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 304 */
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 305 */
this.mHandler = v1;
/* .line 307 */
} // :cond_c
v0 = this.mHandlerThread;
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 308 */
(( android.os.HandlerThread ) v0 ).quitSafely ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z
/* .line 309 */
this.mHandlerThread = v1;
/* .line 311 */
} // :cond_d
return;
} // .end method
public void createBin ( ) {
/* .locals 5 */
/* .line 746 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_CTRL_CREATE_BIN;
/* .line 747 */
/* .local v0, "data":[B */
v1 = this.mOtaFile;
/* .line 748 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v1 ).getFileBufBin ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufBin()[B
/* array-length v1, v1 */
/* .line 747 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( v1 );
/* .line 749 */
/* .local v1, "dat_len":[B */
int v2 = 3; // const/4 v2, 0x3
/* aget-byte v3, v1, v2 */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v3, v0, v4 */
/* .line 750 */
/* aget-byte v3, v1, v4 */
/* aput-byte v3, v0, v2 */
/* .line 751 */
int v2 = 1; // const/4 v2, 0x1
/* aget-byte v3, v1, v2 */
int v4 = 4; // const/4 v4, 0x4
/* aput-byte v3, v0, v4 */
/* .line 752 */
int v3 = 0; // const/4 v3, 0x0
/* aget-byte v3, v1, v3 */
int v4 = 5; // const/4 v4, 0x5
/* aput-byte v3, v0, v4 */
/* .line 753 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 754 */
return;
} // .end method
public void createDat ( ) {
/* .locals 6 */
/* .line 722 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_CTRL_CREATE_DAT;
/* .line 723 */
/* .local v0, "data":[B */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).getOtaFile ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getOtaFile()Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
/* .line 724 */
/* .local v1, "otaBinFile":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile; */
/* nop */
/* .line 725 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v1 ).getFileBufDat ( ); // invoke-virtual {v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufDat()[B
/* array-length v2, v2 */
/* .line 724 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .int2Bytes ( v2 );
/* .line 726 */
/* .local v2, "dat_len":[B */
int v3 = 3; // const/4 v3, 0x3
/* aget-byte v4, v2, v3 */
int v5 = 2; // const/4 v5, 0x2
/* aput-byte v4, v0, v5 */
/* .line 727 */
/* aget-byte v4, v2, v5 */
/* aput-byte v4, v0, v3 */
/* .line 728 */
int v3 = 1; // const/4 v3, 0x1
/* aget-byte v4, v2, v3 */
int v5 = 4; // const/4 v5, 0x4
/* aput-byte v4, v0, v5 */
/* .line 729 */
int v4 = 0; // const/4 v4, 0x0
/* aget-byte v4, v2, v4 */
int v5 = 5; // const/4 v5, 0x5
/* aput-byte v4, v0, v5 */
/* .line 730 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 731 */
return;
} // .end method
public void ctrlExcute ( ) {
/* .locals 2 */
/* .line 738 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_DFU_CTRL_EXECUTE;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 739 */
return;
} // .end method
public java.lang.String getAddress ( ) {
/* .locals 1 */
/* .line 211 */
v0 = this.mAddress;
} // .end method
public android.bluetooth.BluetoothDevice getDevice ( ) {
/* .locals 1 */
/* .line 207 */
v0 = this.mDevice;
} // .end method
public android.bluetooth.BluetoothGatt getGatt ( ) {
/* .locals 1 */
/* .line 203 */
v0 = this.mGatt;
} // .end method
public void getKeyboardStatus ( ) {
/* .locals 2 */
/* .line 712 */
v0 = com.android.server.input.padkeyboard.iic.CommunicationUtil$BleDeviceUtil.CMD_KB_STATUS;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 714 */
return;
} // .end method
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 215 */
v0 = this.mName;
} // .end method
public com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile getOtaFile ( ) {
/* .locals 1 */
/* .line 255 */
v0 = this.mOtaFile;
} // .end method
public Double getOtaProgerss ( ) {
/* .locals 2 */
/* .line 237 */
/* iget-wide v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D */
/* return-wide v0 */
} // .end method
public Integer getPackageSize ( ) {
/* .locals 1 */
/* .line 246 */
/* iget v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I */
} // .end method
public java.lang.String getVersion ( ) {
/* .locals 1 */
/* .line 194 */
v0 = this.mCurVersion;
} // .end method
public Boolean isDfuDatDone ( ) {
/* .locals 1 */
/* .line 220 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
} // .end method
public Boolean isOtaRun ( ) {
/* .locals 1 */
/* .line 229 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaRunning:Z */
} // .end method
public void notifyDeviceFree ( ) {
/* .locals 2 */
/* .line 774 */
v0 = this.mDeviceBusyLock;
/* monitor-enter v0 */
/* .line 775 */
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
/* .line 776 */
v1 = this.mDeviceBusyLock;
(( java.lang.Object ) v1 ).notifyAll ( ); // invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
/* .line 777 */
/* monitor-exit v0 */
/* .line 778 */
return;
/* .line 777 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void parseInfo ( Object[] p0 ) {
/* .locals 7 */
/* .param p1, "info" # [B */
/* .line 645 */
v0 = this.mGatt;
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
/* if-nez v0, :cond_0 */
/* .line 646 */
final String v0 = "parseInfo: mGatt is null"; // const-string v0, "parseInfo: mGatt is null"
android.util.Slog .i ( v1,v0 );
/* .line 647 */
return;
/* .line 650 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* aget-byte v2, p1, v0 */
/* const/16 v3, 0x60 */
/* if-ne v2, v3, :cond_6 */
/* .line 651 */
int v2 = 2; // const/4 v2, 0x2
/* aget-byte v3, p1, v2 */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v3, v4, :cond_1 */
/* .line 652 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "-- Recive: DFU_CTRL_RESPONSE Failed: cmd: "; // const-string v3, "-- Recive: DFU_CTRL_RESPONSE Failed: cmd: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v3, p1, v4 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "result:"; // const-string v3, "result:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v2, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 654 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).sendOtaFailed ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaFailed()V
/* .line 655 */
return;
/* .line 657 */
} // :cond_1
/* aget-byte v2, p1, v4 */
/* packed-switch v2, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_1 */
/* .line 659 */
/* :pswitch_1 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 660 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlCreateBin()V */
/* goto/16 :goto_1 */
/* .line 662 */
} // :cond_2
final String v0 = "-- Recive: DAT DFU_CTRL_SELECT SUCCESS"; // const-string v0, "-- Recive: DAT DFU_CTRL_SELECT SUCCESS"
android.util.Slog .i ( v1,v0 );
/* .line 663 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlSetPrn()V */
/* .line 665 */
/* goto/16 :goto_1 */
/* .line 694 */
/* :pswitch_2 */
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
/* if-nez v2, :cond_3 */
/* .line 695 */
/* iput-boolean v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
/* .line 696 */
final String v0 = "-- Recive: DAT DFU_CTRL_EXECTUE SUCCESS"; // const-string v0, "-- Recive: DAT DFU_CTRL_EXECTUE SUCCESS"
android.util.Slog .i ( v1,v0 );
/* .line 697 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlSelectBin()V */
/* goto/16 :goto_1 */
/* .line 699 */
} // :cond_3
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "-- Recive: BIN DFU_CTRL_EXECTUE SUCCESS ota time: "; // const-string v3, "-- Recive: BIN DFU_CTRL_EXECTUE SUCCESS ota time: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 700 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* iget-wide v5, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaStartTime:J */
/* sub-long/2addr v3, v5 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 699 */
android.util.Slog .i ( v1,v2 );
/* .line 701 */
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaResult(I)V */
/* goto/16 :goto_1 */
/* .line 674 */
/* :pswitch_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 675 */
/* .local v0, "file_crc":I */
/* iget-boolean v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 676 */
final String v2 = "-- Recive: BIN DFU_CTRL_CAL_CHECKSUM SUCCESS"; // const-string v2, "-- Recive: BIN DFU_CTRL_CAL_CHECKSUM SUCCESS"
android.util.Slog .i ( v1,v2 );
/* .line 677 */
v2 = this.mOtaFile;
v0 = (( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v2 ).getFileBufBinCrc ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufBinCrc()I
/* .line 679 */
} // :cond_4
final String v2 = "-- Recive: DAT DFU_CTRL_CAL_CHECKSUM SUCCESS"; // const-string v2, "-- Recive: DAT DFU_CTRL_CAL_CHECKSUM SUCCESS"
android.util.Slog .i ( v1,v2 );
/* .line 680 */
v2 = this.mOtaFile;
v0 = (( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v2 ).getFileBufDatCrc ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->getFileBufDatCrc()I
/* .line 682 */
} // :goto_0
int v2 = 7; // const/4 v2, 0x7
/* aget-byte v2, p1, v2 */
/* and-int/lit16 v2, v2, 0xff */
/* const/16 v3, 0x8 */
/* aget-byte v4, p1, v3 */
/* and-int/lit16 v4, v4, 0xff */
/* shl-int/lit8 v3, v4, 0x8 */
/* add-int/2addr v2, v3 */
/* const/16 v3, 0x9 */
/* aget-byte v3, p1, v3 */
/* and-int/lit16 v3, v3, 0xff */
/* shl-int/lit8 v3, v3, 0x10 */
/* add-int/2addr v2, v3 */
/* const/16 v3, 0xa */
/* aget-byte v3, p1, v3 */
/* and-int/lit16 v3, v3, 0xff */
/* shl-int/lit8 v3, v3, 0x18 */
/* add-int/2addr v2, v3 */
/* .line 684 */
/* .local v2, "crc":I */
/* if-ne v0, v2, :cond_5 */
/* .line 685 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlExecute()V */
/* .line 687 */
} // :cond_5
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "-- Recive: DFU_CTRL_CAL_CHECKSUM crc Failed"; // const-string v4, "-- Recive: DFU_CTRL_CAL_CHECKSUM crc Failed"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "/"; // const-string v4, "/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 690 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).sendOtaFailed ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaFailed()V
/* .line 692 */
/* .line 670 */
} // .end local v0 # "file_crc":I
} // .end local v2 # "crc":I
/* :pswitch_4 */
final String v0 = "-- Recive: DAT DFU_CTRL_SET_PRN SUCCESS"; // const-string v0, "-- Recive: DAT DFU_CTRL_SET_PRN SUCCESS"
android.util.Slog .i ( v1,v0 );
/* .line 671 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlCreateDat()V */
/* .line 672 */
/* .line 667 */
/* :pswitch_5 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendDataPacket()V */
/* .line 668 */
/* nop */
/* .line 706 */
} // :cond_6
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void selectBin ( ) {
/* .locals 2 */
/* .line 742 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_CTRL_SELECT_BIN;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 743 */
return;
} // .end method
public void selectDat ( ) {
/* .locals 2 */
/* .line 716 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_CTRL_SELECT_DAT;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 717 */
return;
} // .end method
public void sendData ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "index" # I */
/* .line 765 */
/* if-nez p1, :cond_0 */
/* .line 766 */
/* invoke-direct {p0, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendDatInfo(I)V */
/* .line 768 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_1 */
/* .line 769 */
/* invoke-direct {p0, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendBinInfo(I)V */
/* .line 771 */
} // :cond_1
return;
} // .end method
public void sendGetKeyboardStatus ( ) {
/* .locals 2 */
/* .line 601 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 602 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 2; // const/4 v1, 0x2
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 603 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 604 */
return;
} // .end method
public void sendOtaFailed ( ) {
/* .locals 2 */
/* .line 632 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 633 */
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 636 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
/* .line 637 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).setOtaRun ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V
/* .line 638 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaResult(I)V */
/* .line 639 */
return;
} // .end method
public void sendPacket ( ) {
/* .locals 2 */
/* .line 757 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 758 */
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendBinInfo(I)V */
/* .line 760 */
} // :cond_0
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendDatInfo(I)V */
/* .line 762 */
} // :goto_0
return;
} // .end method
public void sendSetMtu ( ) {
/* .locals 2 */
/* .line 595 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 596 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 597 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 598 */
return;
} // .end method
public void sendStartOta ( ) {
/* .locals 4 */
/* .line 607 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
v0 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 608 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 610 */
} // :cond_0
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;
/* .line 611 */
/* .local v0, "message":Landroid/os/Message; */
/* iput v1, v0, Landroid/os/Message;->what:I */
/* .line 613 */
v1 = this.mHandler;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 614 */
return;
} // .end method
public void setConnected ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "connected" # Z */
/* .line 190 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z */
/* .line 191 */
return;
} // .end method
public void setDfuDatDone ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "done" # Z */
/* .line 224 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mDatDone:Z */
/* .line 225 */
return;
} // .end method
public void setMtu ( ) {
/* .locals 2 */
/* .line 708 */
v0 = this.mGatt;
/* const/16 v1, 0x16b */
(( android.bluetooth.BluetoothGatt ) v0 ).requestMtu ( v1 ); // invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGatt;->requestMtu(I)Z
/* .line 709 */
return;
} // .end method
public void setNotificationCharacters ( ) {
/* .locals 4 */
/* .line 314 */
v0 = this.mGatt;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 315 */
v0 = v0 = this.mSupportProfile;
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
/* if-nez v0, :cond_0 */
/* .line 316 */
final String v0 = "Device SupportProfile is null"; // const-string v0, "Device SupportProfile is null"
android.util.Slog .i ( v1,v0 );
/* .line 317 */
return;
/* .line 321 */
} // :cond_0
v0 = this.mSupportProfile;
v0 = java.lang.Integer .valueOf ( v2 );
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 322 */
v0 = this.mDfuControl;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 323 */
v3 = this.mGatt;
v0 = (( android.bluetooth.BluetoothGatt ) v3 ).setCharacteristicNotification ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z
/* if-nez v0, :cond_1 */
/* .line 324 */
/* const-string/jumbo v0, "set Notification DfuControl Characteristic failed" */
android.util.Slog .e ( v1,v0 );
/* .line 326 */
} // :cond_1
v0 = this.mDfuControl;
v3 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CLIENT_CHARACTERISTIC_CONFIG;
(( android.bluetooth.BluetoothGattCharacteristic ) v0 ).getDescriptor ( v3 ); // invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;
/* .line 328 */
/* .local v0, "descriptor":Landroid/bluetooth/BluetoothGattDescriptor; */
v3 = android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
(( android.bluetooth.BluetoothGattDescriptor ) v0 ).setValue ( v3 ); // invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z
/* .line 329 */
v3 = this.mGatt;
(( android.bluetooth.BluetoothGatt ) v3 ).writeDescriptor ( v0 ); // invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z
/* .line 331 */
} // .end local v0 # "descriptor":Landroid/bluetooth/BluetoothGattDescriptor;
} // :cond_2
v0 = this.mDfuPacket;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 332 */
v3 = this.mGatt;
v0 = (( android.bluetooth.BluetoothGatt ) v3 ).setCharacteristicNotification ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z
/* if-nez v0, :cond_3 */
/* .line 333 */
/* const-string/jumbo v0, "set Notification DfuPacket Characteristic failed" */
android.util.Slog .e ( v1,v0 );
/* .line 338 */
} // :cond_3
v0 = this.mSupportProfile;
v0 = java.lang.Integer .valueOf ( v3 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 339 */
v0 = this.mBleKeyboardHidRead;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 340 */
v3 = this.mGatt;
v0 = (( android.bluetooth.BluetoothGatt ) v3 ).setCharacteristicNotification ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z
/* if-nez v0, :cond_4 */
/* .line 341 */
/* const-string/jumbo v0, "set Notification Hid Characteristic failed" */
android.util.Slog .e ( v1,v0 );
/* .line 344 */
} // :cond_4
v0 = this.mBleKeyboardBatteryRead;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 345 */
v3 = this.mGatt;
v0 = (( android.bluetooth.BluetoothGatt ) v3 ).setCharacteristicNotification ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z
/* if-nez v0, :cond_5 */
/* .line 347 */
/* const-string/jumbo v0, "set Notification Battery Characteristic failed" */
android.util.Slog .e ( v1,v0 );
/* .line 352 */
} // :cond_5
return;
} // .end method
public void setOtaFile ( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile p0 ) {
/* .locals 0 */
/* .param p1, "otaFile" # Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile; */
/* .line 259 */
this.mOtaFile = p1;
/* .line 260 */
return;
} // .end method
public void setOtaProgerss ( Double p0 ) {
/* .locals 0 */
/* .param p1, "otaProgerss" # D */
/* .line 241 */
/* iput-wide p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaProgerss:D */
/* .line 242 */
return;
} // .end method
public void setOtaRun ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "run" # Z */
/* .line 233 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaRunning:Z */
/* .line 234 */
return;
} // .end method
public void setPackageSize ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "size" # I */
/* .line 250 */
/* iput p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mPackageSize:I */
/* .line 251 */
return;
} // .end method
public void setPrn ( ) {
/* .locals 2 */
/* .line 719 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.CMD_CTRL_SET_PRN;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).syncWriteCommand ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
/* .line 720 */
return;
} // .end method
public void setVersionAndStartOta ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "version" # Ljava/lang/String; */
/* .line 198 */
this.mCurVersion = p1;
/* .line 199 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).sendStartOta ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendStartOta()V
/* .line 200 */
return;
} // .end method
public void startUpgrade ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "binPath" # Ljava/lang/String; */
/* .line 511 */
final String v0 = "GattDeviceItem"; // const-string v0, "GattDeviceItem"
try { // :try_start_0
v1 = (( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).isOtaRun ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->isOtaRun()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 512 */
final String v1 = "StartUpgrade ota is running"; // const-string v1, "StartUpgrade ota is running"
android.util.Slog .i ( v0,v1 );
/* .line 513 */
return;
/* .line 516 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).setOtaRun ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V
/* .line 519 */
int v2 = 0; // const/4 v2, 0x0
/* .line 520 */
/* .local v2, "zipValid":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 521 */
/* .local v3, "upgradeFile":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile; */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 522 */
/* new-instance v4, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile; */
/* invoke-direct {v4, p1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;-><init>(Ljava/lang/String;)V */
/* move-object v3, v4 */
/* .line 523 */
v4 = (( com.android.server.input.padkeyboard.bluetooth.upgrade.UpgradeZipFile ) v3 ).isValidFile ( ); // invoke-virtual {v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;->isValidFile()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 524 */
int v2 = 1; // const/4 v2, 0x1
/* .line 525 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).setOtaFile ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaFile(Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;)V
/* .line 528 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* if-nez v2, :cond_2 */
/* .line 529 */
final String v1 = "The OTA file unvalid!"; // const-string v1, "The OTA file unvalid!"
android.util.Slog .i ( v0,v1 );
/* .line 530 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).setOtaRun ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V
/* .line 531 */
return;
/* .line 535 */
} // :cond_2
v5 = /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->checkVersion()Z */
/* if-nez v5, :cond_3 */
/* .line 536 */
/* const-string/jumbo v1, "version not need to upgrade" */
android.util.Slog .i ( v0,v1 );
/* .line 537 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).setOtaRun ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setOtaRun(Z)V
/* .line 538 */
return;
/* .line 542 */
} // :cond_3
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mOtaStartTime:J */
/* .line 544 */
/* invoke-direct {p0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaResult(I)V */
/* .line 546 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendCtrlSelectDat()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 549 */
} // .end local v2 # "zipValid":Z
} // .end local v3 # "upgradeFile":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/UpgradeZipFile;
/* .line 547 */
/* :catch_0 */
/* move-exception v1 */
/* .line 548 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "startUpgrade exception " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 550 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void syncWriteCommand ( Object[] p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "command" # [B */
/* .param p2, "type" # I */
/* .line 355 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p2, v0, :cond_1 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
/* .line 385 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->writeCommand([BI)V */
/* .line 386 */
return;
/* .line 358 */
} // :cond_1
} // :goto_0
try { // :try_start_0
final String v0 = "GattDeviceItem"; // const-string v0, "GattDeviceItem"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "syncWriteCommand " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x12 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " type = "; // const-string v3, " type = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 360 */
v0 = this.mDeviceBusyLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 361 */
try { // :try_start_1
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
/* if-nez v1, :cond_2 */
/* .line 363 */
v1 = this.mDeviceBusyLock;
/* const-wide/16 v3, 0x1f4 */
(( java.lang.Object ) v1 ).wait ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V
/* .line 365 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
/* if-nez v1, :cond_3 */
/* .line 366 */
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
/* const-string/jumbo v2, "syncWriteCommand ota failed" */
android.util.Slog .i ( v1,v2 );
/* .line 367 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) p0 ).sendOtaFailed ( ); // invoke-virtual {p0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendOtaFailed()V
/* .line 368 */
/* monitor-exit v0 */
return;
/* .line 370 */
} // :cond_3
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mConnected:Z */
/* if-nez v1, :cond_4 */
/* .line 371 */
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
/* const-string/jumbo v2, "syncWriteCommand failed because device has disconnected" */
android.util.Slog .i ( v1,v2 );
/* .line 373 */
/* monitor-exit v0 */
return;
/* .line 375 */
} // :cond_4
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
/* .line 376 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->writeCommand([BI)V */
/* .line 377 */
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "syncWriteCommand " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", type = "; // const-string v3, ", type = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", out mRequestCompleted "; // const-string v3, ", out mRequestCompleted "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->mRequestCompleted:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 379 */
/* monitor-exit v0 */
/* .line 382 */
/* .line 379 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;
} // .end local p1 # "command":[B
} // .end local p2 # "type":I
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/InterruptedException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 380 */
/* .restart local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem; */
/* .restart local p1 # "command":[B */
/* .restart local p2 # "type":I */
/* :catch_0 */
/* move-exception v0 */
/* .line 381 */
/* .local v0, "e":Ljava/lang/InterruptedException; */
final String v1 = "GattDeviceItem"; // const-string v1, "GattDeviceItem"
final String v2 = "Sleeping interrupted"; // const-string v2, "Sleeping interrupted"
android.util.Slog .i ( v1,v2 );
/* .line 383 */
} // .end local v0 # "e":Ljava/lang/InterruptedException;
} // :goto_1
return;
} // .end method
