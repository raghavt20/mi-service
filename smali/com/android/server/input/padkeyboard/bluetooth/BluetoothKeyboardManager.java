public class com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager {
	 /* .source "BluetoothKeyboardManager.java" */
	 /* # static fields */
	 public static java.lang.String ACTION_CONNECTION_STATE_CHANGED;
	 private static final java.lang.String ACTION_USER_CONFIRMATION_RESULT;
	 private static final java.lang.String EXTRA_KEYBOARD_CONFIRMATION_RESULT;
	 private static final java.lang.String TAG;
	 public static final Integer XIAOMI_KEYBOARD_ATTACH_FINISH_SOURCE;
	 public static final Integer XIAOMI_KEYBOARD_ATTACH_FINISH_SOURCE_N83;
	 private static volatile com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager mInstance;
	 /* # instance fields */
	 private final java.util.concurrent.ConcurrentHashMap mBleGattMapForAdd;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/concurrent/ConcurrentHashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private volatile Boolean mIICKeyboardSleepStatus;
private final android.content.BroadcastReceiver mInternalBroadcastReceiver;
/* # direct methods */
static java.util.concurrent.ConcurrentHashMap -$$Nest$fgetmBleGattMapForAdd ( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mBleGattMapForAdd;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmIICKeyboardSleepStatus ( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mIICKeyboardSleepStatus:Z */
} // .end method
static com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ( ) {
/* .locals 1 */
/* .line 35 */
final String v0 = "com.xiaomi.bluetooth.action.keyboard.CONNECTION_STATE_CHANGED"; // const-string v0, "com.xiaomi.bluetooth.action.keyboard.CONNECTION_STATE_CHANGED"
return;
} // .end method
private com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 59 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 77 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mBleGattMapForAdd = v0;
/* .line 98 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;-><init>(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)V */
this.mInternalBroadcastReceiver = v0;
/* .line 60 */
this.mContext = p1;
/* .line 62 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->registerBroadcastReceiver()V */
/* .line 63 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
(( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).setBlueToothKeyboardManagerLocked ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setBlueToothKeyboardManagerLocked(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)V
/* .line 64 */
return;
} // .end method
public static com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 49 */
v0 = com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager.mInstance;
/* if-nez v0, :cond_1 */
/* .line 50 */
/* const-class v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager; */
/* monitor-enter v0 */
/* .line 51 */
try { // :try_start_0
	 v1 = com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager.mInstance;
	 /* if-nez v1, :cond_0 */
	 /* .line 52 */
	 /* new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;-><init>(Landroid/content/Context;)V */
	 /* .line 54 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 56 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager.mInstance;
} // .end method
static void lambda$notifyUpgradeIfNeed$2 ( java.lang.String p0, com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "s" # Ljava/lang/String; */
/* .param p1, "bluetoothGattCallback" # Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback; */
/* .line 172 */
(( com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback ) p1 ).notifyUpgradeIfNeed ( ); // invoke-virtual {p1}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->notifyUpgradeIfNeed()V
return;
} // .end method
static void lambda$wakeUpKeyboardIfNeed$0 ( java.lang.String p0, com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "s" # Ljava/lang/String; */
/* .param p1, "bluetoothGattCallback" # Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback; */
/* .line 94 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback ) p1 ).setActiveMode ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->setActiveMode(Z)V
return;
} // .end method
static void lambda$writeDataToBleDevice$1 ( Object[] p0, java.lang.String p1, com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback p2 ) { //synthethic
/* .locals 0 */
/* .param p0, "command" # [B */
/* .param p1, "s" # Ljava/lang/String; */
/* .param p2, "bluetoothGattCallback" # Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback; */
/* .line 166 */
(( com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback ) p2 ).sendCommandToKeyboard ( p0 ); // invoke-virtual {p2, p0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->sendCommandToKeyboard([B)V
return;
} // .end method
private void registerBroadcastReceiver ( ) {
/* .locals 3 */
/* .line 67 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 68 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
v1 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 69 */
v1 = com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager.ACTION_CONNECTION_STATE_CHANGED;
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 71 */
} // :cond_0
final String v1 = "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"; // const-string v1, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 72 */
final String v1 = "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"; // const-string v1, "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 74 */
} // :goto_0
v1 = this.mContext;
v2 = this.mInternalBroadcastReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 75 */
return;
} // .end method
/* # virtual methods */
public void notifyUpgradeIfNeed ( ) {
/* .locals 2 */
/* .line 171 */
v0 = this.mBleGattMapForAdd;
/* new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda0;-><init>()V */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 174 */
return;
} // .end method
public void setCurrentBleKeyboardAddress ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "address" # Ljava/lang/String; */
/* .line 81 */
/* if-nez p1, :cond_0 */
return;
/* .line 82 */
} // :cond_0
(( java.lang.String ) p1 ).toUpperCase ( ); // invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 83 */
/* .local v0, "convertedStr":Ljava/lang/String; */
v1 = this.mBleGattMapForAdd;
v1 = (( java.util.concurrent.ConcurrentHashMap ) v1 ).containsKey ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 84 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update ble address:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BluetoothKeyboardManager"; // const-string v2, "BluetoothKeyboardManager"
android.util.Slog .i ( v2,v1 );
/* .line 85 */
v1 = this.mBleGattMapForAdd;
/* new-instance v2, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback; */
/* invoke-direct {v2}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;-><init>()V */
(( java.util.concurrent.ConcurrentHashMap ) v1 ).put ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 87 */
} // :cond_1
return;
} // .end method
public void setIICKeyboardSleepStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isKeyboardSleep" # Z */
/* .line 89 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mIICKeyboardSleepStatus:Z */
/* .line 90 */
return;
} // .end method
public void wakeUpKeyboardIfNeed ( ) {
/* .locals 2 */
/* .line 93 */
v0 = this.mBleGattMapForAdd;
/* new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda1;-><init>()V */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 96 */
return;
} // .end method
public void writeDataToBleDevice ( Object[] p0 ) {
/* .locals 2 */
/* .param p1, "command" # [B */
/* .line 165 */
v0 = this.mBleGattMapForAdd;
/* new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda2;-><init>([B)V */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 168 */
return;
} // .end method
