.class public Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;
.super Ljava/lang/Object;
.source "BluetoothKeyboardManager.java"


# static fields
.field public static ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = null

.field private static final ACTION_USER_CONFIRMATION_RESULT:Ljava/lang/String; = "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"

.field private static final EXTRA_KEYBOARD_CONFIRMATION_RESULT:Ljava/lang/String; = "com.xiaomi.bluetooth.keyboard.extra.CONFIRMATION_RESULT"

.field private static final TAG:Ljava/lang/String; = "BluetoothKeyboardManager"

.field public static final XIAOMI_KEYBOARD_ATTACH_FINISH_SOURCE:I = 0x2303

.field public static final XIAOMI_KEYBOARD_ATTACH_FINISH_SOURCE_N83:I = 0x301

.field private static volatile mInstance:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;


# instance fields
.field private final mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private volatile mIICKeyboardSleepStatus:Z

.field private final mInternalBroadcastReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBleGattMapForAdd(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIICKeyboardSleepStatus(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mIICKeyboardSleepStatus:Z

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 35
    const-string v0, "com.xiaomi.bluetooth.action.keyboard.CONNECTION_STATE_CHANGED"

    sput-object v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    .line 98
    new-instance v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;-><init>(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)V

    iput-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mInternalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 60
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mContext:Landroid/content/Context;

    .line 62
    invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->registerBroadcastReceiver()V

    .line 63
    sget-object v0, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setBlueToothKeyboardManagerLocked(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)V

    .line 64
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 49
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mInstance:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    if-nez v0, :cond_1

    .line 50
    const-class v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    monitor-enter v0

    .line 51
    :try_start_0
    sget-object v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mInstance:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    if-nez v1, :cond_0

    .line 52
    new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-direct {v1, p0}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mInstance:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    .line 54
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 56
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mInstance:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    return-object v0
.end method

.method static synthetic lambda$notifyUpgradeIfNeed$2(Ljava/lang/String;Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;)V
    .locals 0
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "bluetoothGattCallback"    # Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;

    .line 172
    invoke-virtual {p1}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->notifyUpgradeIfNeed()V

    return-void
.end method

.method static synthetic lambda$wakeUpKeyboardIfNeed$0(Ljava/lang/String;Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "bluetoothGattCallback"    # Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->setActiveMode(Z)V

    return-void
.end method

.method static synthetic lambda$writeDataToBleDevice$1([BLjava/lang/String;Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;)V
    .locals 0
    .param p0, "command"    # [B
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "bluetoothGattCallback"    # Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;

    .line 166
    invoke-virtual {p2, p0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->sendCommandToKeyboard([B)V

    return-void
.end method

.method private registerBroadcastReceiver()V
    .locals 3

    .line 67
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 68
    .local v0, "filter":Landroid/content/IntentFilter;
    invoke-static {}, Lmiui/hardware/input/InputFeature;->isSingleBleKeyboard()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    sget-object v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_0
    const-string v1, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 72
    const-string v1, "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mInternalBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 75
    return-void
.end method


# virtual methods
.method public notifyUpgradeIfNeed()V
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 174
    return-void
.end method

.method public setCurrentBleKeyboardAddress(Ljava/lang/String;)V
    .locals 3
    .param p1, "address"    # Ljava/lang/String;

    .line 81
    if-nez p1, :cond_0

    return-void

    .line 82
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "convertedStr":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 84
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update ble address:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BluetoothKeyboardManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v2, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;

    invoke-direct {v2}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;-><init>()V

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_1
    return-void
.end method

.method public setIICKeyboardSleepStatus(Z)V
    .locals 0
    .param p1, "isKeyboardSleep"    # Z

    .line 89
    iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mIICKeyboardSleepStatus:Z

    .line 90
    return-void
.end method

.method public wakeUpKeyboardIfNeed()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda1;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 96
    return-void
.end method

.method public writeDataToBleDevice([B)V
    .locals 2
    .param p1, "command"    # [B

    .line 165
    iget-object v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->mBleGattMapForAdd:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v1, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda2;

    invoke-direct {v1, p1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$$ExternalSyntheticLambda2;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 168
    return-void
.end method
