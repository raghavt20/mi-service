class com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager$1 extends android.content.BroadcastReceiver {
	 /* .source "BluetoothKeyboardManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # static fields */
static final Integer CONNECT_STATUS_DEFAULT;
static final Integer CONNECT_STATUS_FAIL;
static final Integer CONNECT_STATUS_LOSE;
static final Integer CONNECT_STATUS_SUCCESS;
/* # instance fields */
final com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager this$0; //synthetic
/* # direct methods */
 com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager; */
/* .line 98 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 13 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 105 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 106 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"; // const-string v1, "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 108 */
	 v1 = this.this$0;
	 v1 = 	 com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .-$$Nest$fgetmIICKeyboardSleepStatus ( v1 );
	 if ( v1 != null) { // if-eqz v1, :cond_c
		 /* .line 109 */
		 v1 = this.this$0;
		 com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .-$$Nest$fgetmContext ( v1 );
		 com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v1 );
		 /* goto/16 :goto_1 */
		 /* .line 112 */
	 } // :cond_0
	 final String v1 = "android.bluetooth.profile.extra.STATE"; // const-string v1, "android.bluetooth.profile.extra.STATE"
	 int v2 = 0; // const/4 v2, 0x0
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 113 */
	 /* .local v1, "newState":I */
	 final String v3 = "android.bluetooth.profile.extra.PREVIOUS_STATE"; // const-string v3, "android.bluetooth.profile.extra.PREVIOUS_STATE"
	 v3 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v3, v2 ); // invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 114 */
	 /* .local v3, "oldState":I */
	 final String v4 = "android.bluetooth.device.extra.DEVICE"; // const-string v4, "android.bluetooth.device.extra.DEVICE"
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v4 ); // invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v4, Landroid/bluetooth/BluetoothDevice; */
	 /* .line 115 */
	 /* .local v4, "device":Landroid/bluetooth/BluetoothDevice; */
	 final String v5 = "BluetoothKeyboardManager"; // const-string v5, "BluetoothKeyboardManager"
	 /* if-nez v4, :cond_1 */
	 /* .line 116 */
	 final String v2 = "BLE BroadcastReceiver changed, but the device is null"; // const-string v2, "BLE BroadcastReceiver changed, but the device is null"
	 android.util.Slog .i ( v5,v2 );
	 /* .line 117 */
	 return;
	 /* .line 119 */
} // :cond_1
(( android.bluetooth.BluetoothDevice ) v4 ).getAddress ( ); // invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
/* .line 121 */
/* .local v6, "bleAddress":Ljava/lang/String; */
v7 = com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager.ACTION_CONNECTION_STATE_CHANGED;
v7 = (( java.lang.String ) v7 ).equals ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
	 /* .line 122 */
	 v7 = this.this$0;
	 (( com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager ) v7 ).setCurrentBleKeyboardAddress ( v6 ); // invoke-virtual {v7, v6}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->setCurrentBleKeyboardAddress(Ljava/lang/String;)V
	 /* .line 124 */
} // :cond_2
int v7 = 0; // const/4 v7, 0x0
/* .line 125 */
/* .local v7, "state":I */
v8 = this.this$0;
com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .-$$Nest$fgetmBleGattMapForAdd ( v8 );
v8 = (( java.util.concurrent.ConcurrentHashMap ) v8 ).containsKey ( v6 ); // invoke-virtual {v8, v6}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_b
	 /* .line 126 */
	 v8 = this.this$0;
	 com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .-$$Nest$fgetmBleGattMapForAdd ( v8 );
	 (( java.util.concurrent.ConcurrentHashMap ) v8 ).get ( v6 ); // invoke-virtual {v8, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
	 /* check-cast v8, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback; */
	 /* .line 127 */
	 /* .local v8, "callback":Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback; */
	 (( com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback ) v8 ).getBluetoothGatt ( ); // invoke-virtual {v8}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->getBluetoothGatt()Landroid/bluetooth/BluetoothGatt;
	 /* .line 128 */
	 /* .local v9, "gatt":Landroid/bluetooth/BluetoothGatt; */
	 int v10 = 1; // const/4 v10, 0x1
	 int v11 = 2; // const/4 v11, 0x2
	 /* if-ne v1, v11, :cond_4 */
	 /* .line 129 */
	 /* if-nez v9, :cond_3 */
	 /* .line 130 */
	 v12 = this.this$0;
	 com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .-$$Nest$fgetmContext ( v12 );
	 (( android.bluetooth.BluetoothDevice ) v4 ).connectGatt ( v12, v2, v8, v11 ); // invoke-virtual {v4, v12, v2, v8, v11}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;I)Landroid/bluetooth/BluetoothGatt;
	 /* .line 132 */
} // :cond_3
com.android.server.input.padkeyboard.iic.CommunicationUtil .getInstance ( );
(( com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback ) v8 ).setCommunicationUtil ( v2 ); // invoke-virtual {v8, v2}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->setCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)V
/* .line 133 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Connect the device successfully, keyboard:"; // const-string v11, "Connect the device successfully, keyboard:"
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 134 */
int v7 = 1; // const/4 v7, 0x1
/* .line 135 */
} // :cond_4
/* if-nez v1, :cond_5 */
/* if-ne v3, v10, :cond_5 */
/* .line 138 */
int v7 = 2; // const/4 v7, 0x2
/* .line 139 */
} // :cond_5
/* if-nez v1, :cond_6 */
/* if-eq v3, v11, :cond_7 */
} // :cond_6
/* if-nez v1, :cond_8 */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v3, v2, :cond_8 */
/* .line 144 */
} // :cond_7
int v7 = 3; // const/4 v7, 0x3
/* .line 146 */
} // :cond_8
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "status:" */
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = ", keyboard:"; // const-string v11, ", keyboard:"
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 147 */
v2 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v2 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v2 ).isConnectIICLocked ( ); // invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v2, :cond_9 */
/* .line 148 */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 149 */
/* .local v2, "castData":Landroid/os/Bundle; */
final String v5 = "keyboardStatus"; // const-string v5, "keyboardStatus"
(( android.os.Bundle ) v2 ).putInt ( v5, v7 ); // invoke-virtual {v2, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 150 */
v5 = this.this$0;
com.android.server.input.padkeyboard.bluetooth.BluetoothKeyboardManager .-$$Nest$fgetmContext ( v5 );
com.android.server.input.padkeyboard.MiuiKeyboardUtil .notifySettingsKeyboardStatusChanged ( v5,v2 );
/* .line 153 */
} // .end local v2 # "castData":Landroid/os/Bundle;
} // :cond_9
/* if-eq v7, v10, :cond_a */
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 154 */
(( android.bluetooth.BluetoothGatt ) v9 ).disconnect ( ); // invoke-virtual {v9}, Landroid/bluetooth/BluetoothGatt;->disconnect()V
/* .line 156 */
} // .end local v8 # "callback":Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;
} // .end local v9 # "gatt":Landroid/bluetooth/BluetoothGatt;
} // :cond_a
/* .line 157 */
} // :cond_b
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "The Ble devices is not keyboard! "; // const-string v8, "The Ble devices is not keyboard! "
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.bluetooth.BluetoothDevice ) v4 ).getAddress ( ); // invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 161 */
} // .end local v1 # "newState":I
} // .end local v3 # "oldState":I
} // .end local v4 # "device":Landroid/bluetooth/BluetoothDevice;
} // .end local v6 # "bleAddress":Ljava/lang/String;
} // .end local v7 # "state":I
} // :cond_c
} // :goto_1
return;
} // .end method
