public class com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback extends android.bluetooth.BluetoothGattCallback {
	 /* .source "KeyboardGattCallback.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public final Integer BLE_DEVICE_SLEEP_INTERVAL;
	 private Boolean mAlreadyNotifyBattery;
	 private final android.os.Handler mBleHandler;
	 private com.android.server.input.padkeyboard.iic.CommunicationUtil mCommunicationUtil;
	 private com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem mGattDeviceItem;
	 private volatile Boolean mIsActiveMode;
	 /* # direct methods */
	 public com.android.server.input.padkeyboard.bluetooth.KeyboardGattCallback ( ) {
		 /* .locals 1 */
		 /* .line 48 */
		 /* invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V */
		 /* .line 41 */
		 /* const/16 v0, 0x28 */
		 /* iput v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->BLE_DEVICE_SLEEP_INTERVAL:I */
		 /* .line 44 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z */
		 /* .line 49 */
		 com.android.server.input.MiuiInputThread .getHandler ( );
		 this.mBleHandler = v0;
		 /* .line 50 */
		 return;
	 } // .end method
	 private void WriteBleCharacter ( Object[] p0, Integer p1 ) {
		 /* .locals 3 */
		 /* .param p1, "buf" # [B */
		 /* .param p2, "type" # I */
		 /* .line 223 */
		 v0 = this.mGattDeviceItem;
		 final String v1 = "KeyboardGattCallback"; // const-string v1, "KeyboardGattCallback"
		 /* if-nez v0, :cond_0 */
		 /* .line 224 */
		 final String v0 = "WriteBleCharacter mGattDeviceItem is null"; // const-string v0, "WriteBleCharacter mGattDeviceItem is null"
		 android.util.Slog .i ( v1,v0 );
		 /* .line 225 */
		 return;
		 /* .line 227 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "WriteBleCharacter type "; // const-string v2, "WriteBleCharacter type "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = ", write data "; // const-string v2, ", write data "
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 228 */
	 /* const/16 v2, 0x12 */
	 com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p1,v2 );
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 227 */
	 android.util.Slog .i ( v1,v0 );
	 /* .line 229 */
	 v0 = this.mGattDeviceItem;
	 (( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).syncWriteCommand ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->syncWriteCommand([BI)V
	 /* .line 230 */
	 return;
} // .end method
private synchronized void clearDevice ( ) {
	 /* .locals 2 */
	 /* monitor-enter p0 */
	 /* .line 261 */
	 try { // :try_start_0
		 final String v0 = "KeyboardGattCallback"; // const-string v0, "KeyboardGattCallback"
		 final String v1 = "clearDevice"; // const-string v1, "clearDevice"
		 android.util.Slog .i ( v0,v1 );
		 /* .line 262 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z */
		 /* .line 263 */
		 v1 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
		 (( com.android.server.input.padkeyboard.KeyboardInteraction ) v1 ).setConnectBleLocked ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->setConnectBleLocked(Z)V
		 /* .line 264 */
		 v0 = this.mGattDeviceItem;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 265 */
			 (( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).clear ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->clear()V
			 /* .line 266 */
			 int v0 = 0; // const/4 v0, 0x0
			 this.mGattDeviceItem = v0;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 268 */
		 } // .end local p0 # "this":Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;
	 } // :cond_0
	 /* monitor-exit p0 */
	 return;
	 /* .line 260 */
	 /* :catchall_0 */
	 /* move-exception v0 */
	 /* monitor-exit p0 */
	 /* throw v0 */
} // .end method
/* # virtual methods */
public android.bluetooth.BluetoothGatt getBluetoothGatt ( ) {
	 /* .locals 1 */
	 /* .line 250 */
	 v0 = this.mGattDeviceItem;
	 /* if-nez v0, :cond_0 */
	 /* .line 251 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 253 */
} // :cond_0
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).getGatt ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->getGatt()Landroid/bluetooth/BluetoothGatt;
} // .end method
public void notifyUpgradeIfNeed ( ) {
/* .locals 1 */
/* .line 270 */
v0 = this.mGattDeviceItem;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 271 */
	 (( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).sendStartOta ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendStartOta()V
	 /* .line 273 */
} // :cond_0
return;
} // .end method
public void onCharacteristicChanged ( android.bluetooth.BluetoothGatt p0, android.bluetooth.BluetoothGattCharacteristic p1, Object[] p2 ) {
/* .locals 9 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "characteristic" # Landroid/bluetooth/BluetoothGattCharacteristic; */
/* .param p3, "value" # [B */
/* .line 125 */
/* invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;[B)V */
/* .line 126 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
final String v1 = "KeyboardGattCallback"; // const-string v1, "KeyboardGattCallback"
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 127 */
	 final String v0 = "The iic devices is connected, ignore ble info!"; // const-string v0, "The iic devices is connected, ignore ble info!"
	 android.util.Slog .w ( v1,v0 );
	 /* .line 128 */
	 return;
	 /* .line 131 */
} // :cond_0
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_BATTERY_LEVEL;
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getUuid ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;
v0 = (( java.util.UUID ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 132 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "get keyboard battery :"; // const-string v4, "get keyboard battery :"
	 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* aget-byte v4, p3, v3 */
	 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v1,v0 );
	 /* .line 133 */
	 v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
	 v0 = 	 (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectBleLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectBleLocked()Z
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 134 */
		 /* aget-byte v0, p3, v3 */
		 /* .line 135 */
		 /* .local v0, "nowElectricity":I */
		 /* const/16 v1, 0xa */
		 /* if-gt v0, v1, :cond_1 */
		 /* iget-boolean v4, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z */
		 /* if-nez v4, :cond_1 */
		 /* .line 136 */
		 /* iput-boolean v2, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z */
		 /* .line 137 */
		 /* new-instance v2, Landroid/os/Bundle; */
		 /* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
		 /* .line 138 */
		 /* .local v2, "castData":Landroid/os/Bundle; */
		 final String v4 = "electricity"; // const-string v4, "electricity"
		 (( android.os.Bundle ) v2 ).putInt ( v4, v0 ); // invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
		 /* .line 140 */
		 android.app.ActivityThread .currentActivityThread ( );
		 (( android.app.ActivityThread ) v4 ).getSystemContext ( ); // invoke-virtual {v4}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
		 /* .line 139 */
		 com.android.server.input.padkeyboard.MiuiKeyboardUtil .notifySettingsKeyboardStatusChanged ( v4,v2 );
		 /* .line 142 */
	 } // .end local v2 # "castData":Landroid/os/Bundle;
} // :cond_1
/* if-le v0, v1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 143 */
	 /* iput-boolean v3, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mAlreadyNotifyBattery:Z */
	 /* .line 146 */
} // .end local v0 # "nowElectricity":I
} // :cond_2
return;
/* .line 149 */
} // :cond_3
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_DFU_CONTROL_CHARACTER;
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getUuid ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;
v0 = (( java.util.UUID ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 150 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "deviceName:"; // const-string v2, "deviceName:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " received:"; // const-string v2, " received:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 152 */
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getValue ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getValue ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B
/* array-length v3, v3 */
/* .line 151 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v2,v3 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 150 */
android.util.Slog .i ( v1,v0 );
/* .line 153 */
v0 = this.mGattDeviceItem;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 154 */
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getValue ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).parseInfo ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->parseInfo([B)V
/* .line 156 */
} // :cond_4
return;
/* .line 159 */
} // :cond_5
/* array-length v0, p3 */
/* if-le v0, v2, :cond_8 */
/* .line 160 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "get raw ble data:"; // const-string v4, "get raw ble data:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, p3 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p3,v4 );
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 162 */
/* const/16 v0, 0x40 */
/* new-array v0, v0, [B */
/* .line 163 */
/* .local v0, "temp":[B */
/* const/16 v4, 0x23 */
/* aput-byte v4, v0, v3 */
/* .line 164 */
/* aget-byte v4, p3, v3 */
/* aput-byte v4, v0, v2 */
/* .line 165 */
/* const/16 v4, 0x38 */
int v5 = 2; // const/4 v5, 0x2
/* aput-byte v4, v0, v5 */
/* .line 166 */
/* const/16 v4, -0x80 */
int v6 = 3; // const/4 v6, 0x3
/* aput-byte v4, v0, v6 */
/* .line 167 */
/* aget-byte v4, p3, v2 */
int v7 = 4; // const/4 v7, 0x4
/* aput-byte v4, v0, v7 */
/* .line 168 */
/* array-length v4, p3 */
/* if-lt v4, v6, :cond_6 */
/* .line 169 */
/* array-length v4, p3 */
/* sub-int/2addr v4, v5 */
int v8 = 5; // const/4 v8, 0x5
java.lang.System .arraycopy ( p3,v5,v0,v8,v4 );
/* .line 172 */
} // :cond_6
/* aget-byte v3, p3, v3 */
/* const/16 v4, 0x30 */
/* if-ne v3, v4, :cond_7 */
/* aget-byte v3, p3, v2 */
/* if-ne v3, v2, :cond_7 */
/* .line 174 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* aget-byte v3, p3, v7 */
java.lang.Byte .valueOf ( v3 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
final String v4 = "%02x"; // const-string v4, "%02x"
java.lang.String .format ( v4,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v3, p3, v6 */
/* .line 175 */
java.lang.Byte .valueOf ( v3 );
/* filled-new-array {v3}, [Ljava/lang/Object; */
/* .line 174 */
java.lang.String .format ( v4,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 176 */
/* .local v2, "version":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "ble device version:"; // const-string v4, "ble device version:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 177 */
v1 = this.mGattDeviceItem;
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 178 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v1 ).setVersionAndStartOta ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setVersionAndStartOta(Ljava/lang/String;)V
/* .line 181 */
} // .end local v2 # "version":Ljava/lang/String;
} // :cond_7
v1 = this.mCommunicationUtil;
(( com.android.server.input.padkeyboard.iic.CommunicationUtil ) v1 ).receiverNanoAppData ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->receiverNanoAppData([B)V
/* .line 183 */
} // .end local v0 # "temp":[B
} // :cond_8
return;
} // .end method
public void onCharacteristicRead ( android.bluetooth.BluetoothGatt p0, android.bluetooth.BluetoothGattCharacteristic p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "characteristic" # Landroid/bluetooth/BluetoothGattCharacteristic; */
/* .param p3, "status" # I */
/* .line 189 */
/* invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V */
/* .line 190 */
return;
} // .end method
public void onCharacteristicWrite ( android.bluetooth.BluetoothGatt p0, android.bluetooth.BluetoothGattCharacteristic p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "characteristic" # Landroid/bluetooth/BluetoothGattCharacteristic; */
/* .param p3, "status" # I */
/* .line 108 */
/* invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V */
/* .line 109 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " onCharacteristicWrite: write data:"; // const-string v1, " onCharacteristicWrite: write data:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 110 */
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getValue ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B
/* .line 109 */
/* const/16 v2, 0x12 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", and status is : "; // const-string v1, ", and status is : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "KeyboardGattCallback"; // const-string v1, "KeyboardGattCallback"
android.util.Slog .i ( v1,v0 );
/* .line 111 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_DFU_CONTROL_CHARACTER;
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getUuid ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;
v0 = (( java.util.UUID ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = com.android.server.input.padkeyboard.bluetooth.upgrade.Constant.UUID_DFU_PACKET_CHARACTER;
/* .line 113 */
(( android.bluetooth.BluetoothGattCharacteristic ) p2 ).getUuid ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;
/* .line 112 */
v0 = (( java.util.UUID ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 115 */
} // :cond_0
v0 = this.mGattDeviceItem;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 116 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).notifyDeviceFree ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->notifyDeviceFree()V
/* .line 119 */
} // :cond_1
return;
} // .end method
public void onConnectionStateChange ( android.bluetooth.BluetoothGatt p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "status" # I */
/* .param p3, "newState" # I */
/* .line 66 */
final String v0 = "KeyboardGattCallback"; // const-string v0, "KeyboardGattCallback"
/* if-nez p1, :cond_0 */
/* .line 67 */
final String v1 = "ConnectionStateChanged, but gatt is null!"; // const-string v1, "ConnectionStateChanged, but gatt is null!"
android.util.Slog .e ( v0,v1 );
/* .line 68 */
return;
/* .line 70 */
} // :cond_0
/* packed-switch p3, :pswitch_data_0 */
/* .line 81 */
/* :pswitch_0 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "New state not processed: "; // const-string v2, "New state not processed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 82 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->clearDevice()V */
/* .line 72 */
/* :pswitch_1 */
final String v1 = "ConnectionStateChanged connected!"; // const-string v1, "ConnectionStateChanged connected!"
android.util.Slog .e ( v0,v1 );
/* .line 73 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z */
/* .line 74 */
(( android.bluetooth.BluetoothGatt ) p1 ).discoverServices ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z
/* .line 75 */
/* .line 77 */
/* :pswitch_2 */
final String v1 = "ConnectionStateChanged disconnected!"; // const-string v1, "ConnectionStateChanged disconnected!"
android.util.Slog .i ( v0,v1 );
/* .line 78 */
/* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->clearDevice()V */
/* .line 79 */
/* nop */
/* .line 85 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void onConnectionUpdated ( android.bluetooth.BluetoothGatt p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 1 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "interval" # I */
/* .param p3, "latency" # I */
/* .param p4, "timeout" # I */
/* .param p5, "status" # I */
/* .line 55 */
/* if-nez p5, :cond_1 */
/* .line 56 */
/* const/16 v0, 0x28 */
/* if-ge p2, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z */
/* .line 58 */
} // :cond_1
return;
} // .end method
public void onDescriptorRead ( android.bluetooth.BluetoothGatt p0, android.bluetooth.BluetoothGattDescriptor p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "descriptor" # Landroid/bluetooth/BluetoothGattDescriptor; */
/* .param p3, "status" # I */
/* .line 205 */
/* invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V */
/* .line 206 */
(( android.bluetooth.BluetoothGattDescriptor ) p2 ).getValue ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B
/* .line 207 */
/* .local v0, "value":[B */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onDescriptorRead: deviceName:"; // const-string v2, "onDescriptorRead: deviceName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "read data:"; // const-string v2, "read data:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v0 */
/* .line 208 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 207 */
final String v2 = "KeyboardGattCallback"; // const-string v2, "KeyboardGattCallback"
android.util.Slog .i ( v2,v1 );
/* .line 209 */
return;
} // .end method
public void onDescriptorWrite ( android.bluetooth.BluetoothGatt p0, android.bluetooth.BluetoothGattDescriptor p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "descriptor" # Landroid/bluetooth/BluetoothGattDescriptor; */
/* .param p3, "status" # I */
/* .line 214 */
/* invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V */
/* .line 215 */
(( android.bluetooth.BluetoothGattDescriptor ) p2 ).getValue ( ); // invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getValue()[B
/* .line 216 */
/* .local v0, "value":[B */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onDescriptorWrite: deviceName:"; // const-string v2, "onDescriptorWrite: deviceName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " write data:"; // const-string v2, " write data:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v2, v0 */
/* .line 217 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v0,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 216 */
final String v2 = "KeyboardGattCallback"; // const-string v2, "KeyboardGattCallback"
android.util.Slog .i ( v2,v1 );
/* .line 219 */
return;
} // .end method
public void onMtuChanged ( android.bluetooth.BluetoothGatt p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "mtu" # I */
/* .param p3, "status" # I */
/* .line 194 */
/* invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V */
/* .line 195 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onMtuChanged: deviceName:"; // const-string v1, "onMtuChanged: deviceName:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v1 ).getName ( ); // invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mtu:"; // const-string v1, " mtu:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "KeyboardGattCallback"; // const-string v1, "KeyboardGattCallback"
android.util.Slog .i ( v1,v0 );
/* .line 196 */
v0 = this.mGattDeviceItem;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 197 */
/* add-int/lit8 v1, p2, -0x3 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).setPackageSize ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setPackageSize(I)V
/* .line 198 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).sendGetKeyboardStatus ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendGetKeyboardStatus()V
/* .line 200 */
} // :cond_0
return;
} // .end method
public void onServicesDiscovered ( android.bluetooth.BluetoothGatt p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "gatt" # Landroid/bluetooth/BluetoothGatt; */
/* .param p2, "status" # I */
/* .line 89 */
final String v0 = "KeyboardGattCallback"; // const-string v0, "KeyboardGattCallback"
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-nez p2, :cond_1 */
/* .line 90 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onServicesDiscovered: deviceName:"; // const-string v2, "onServicesDiscovered: deviceName:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 91 */
(( android.bluetooth.BluetoothGatt ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;
(( android.bluetooth.BluetoothDevice ) v2 ).getName ( ); // invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " status:"; // const-string v2, " status:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 90 */
android.util.Slog .i ( v0,v1 );
/* .line 92 */
v0 = this.mGattDeviceItem;
/* if-nez v0, :cond_0 */
/* .line 93 */
/* new-instance v0, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem; */
/* invoke-direct {v0, p1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;-><init>(Landroid/bluetooth/BluetoothGatt;)V */
this.mGattDeviceItem = v0;
/* .line 94 */
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).setNotificationCharacters ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setNotificationCharacters()V
/* .line 95 */
v0 = this.mGattDeviceItem;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).setConnected ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->setConnected(Z)V
/* .line 97 */
} // :cond_0
v0 = miui.hardware.input.InputFeature .isSingleBleKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 98 */
v0 = this.mGattDeviceItem;
(( com.android.server.input.padkeyboard.bluetooth.upgrade.GattDeviceItem ) v0 ).sendSetMtu ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/bluetooth/upgrade/GattDeviceItem;->sendSetMtu()V
/* .line 101 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onServicesDiscovered received: "; // const-string v2, "onServicesDiscovered received: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " or null pointer"; // const-string v2, " or null pointer"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 103 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void sendCommandToKeyboard ( Object[] p0 ) {
/* .locals 4 */
/* .param p1, "command" # [B */
/* .line 234 */
v0 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v0 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v0 ).isConnectIICLocked ( ); // invoke-virtual {v0}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z */
/* if-nez v0, :cond_0 */
/* .line 238 */
} // :cond_0
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getSystemContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
/* .line 239 */
/* .local v0, "context":Landroid/content/Context; */
if ( p1 != null) { // if-eqz p1, :cond_1
/* array-length v1, p1 */
int v2 = 4; // const/4 v2, 0x4
/* if-le v1, v2, :cond_1 */
/* .line 240 */
int v1 = 1; // const/4 v1, 0x1
/* aget-byte v1, p1, v1 */
/* const/16 v2, 0x23 */
/* if-ne v1, v2, :cond_1 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 241 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 3; // const/4 v2, 0x3
/* aget-byte v2, p1, v2 */
final String v3 = "keyboard_back_light_brightness"; // const-string v3, "keyboard_back_light_brightness"
android.provider.Settings$System .putInt ( v1,v3,v2 );
/* .line 245 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, p1, v1}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->WriteBleCharacter([BI)V */
/* .line 246 */
return;
/* .line 235 */
} // .end local v0 # "context":Landroid/content/Context;
} // :cond_2
} // :goto_0
final String v0 = "KeyboardGattCallback"; // const-string v0, "KeyboardGattCallback"
final String v1 = "Abandon command because ble isn\'t connected or sleep"; // const-string v1, "Abandon command because ble isn\'t connected or sleep"
android.util.Slog .w ( v0,v1 );
/* .line 236 */
return;
} // .end method
public void setActiveMode ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isActiveMode" # Z */
/* .line 61 */
/* iput-boolean p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->mIsActiveMode:Z */
/* .line 62 */
return;
} // .end method
public void setCommunicationUtil ( com.android.server.input.padkeyboard.iic.CommunicationUtil p0 ) {
/* .locals 0 */
/* .param p1, "communicationUtil" # Lcom/android/server/input/padkeyboard/iic/CommunicationUtil; */
/* .line 257 */
this.mCommunicationUtil = p1;
/* .line 258 */
return;
} // .end method
