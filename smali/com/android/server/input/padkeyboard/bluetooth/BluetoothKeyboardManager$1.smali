.class Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothKeyboardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final CONNECT_STATUS_DEFAULT:I = 0x0

.field static final CONNECT_STATUS_FAIL:I = 0x2

.field static final CONNECT_STATUS_LOSE:I = 0x3

.field static final CONNECT_STATUS_SUCCESS:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;


# direct methods
.method constructor <init>(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    .line 98
    iput-object p1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 105
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.xiaomi.bluetooth.action.keyboard.USER_CONFIRMATION_RESULT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->-$$Nest$fgetmIICKeyboardSleepStatus(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 109
    iget-object v1, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->wakeKeyboard176()V

    goto/16 :goto_1

    .line 112
    :cond_0
    const-string v1, "android.bluetooth.profile.extra.STATE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 113
    .local v1, "newState":I
    const-string v3, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {p2, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 114
    .local v3, "oldState":I
    const-string v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 115
    .local v4, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, "BluetoothKeyboardManager"

    if-nez v4, :cond_1

    .line 116
    const-string v2, "BLE BroadcastReceiver changed, but the device is null"

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    return-void

    .line 119
    :cond_1
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    .line 121
    .local v6, "bleAddress":Ljava/lang/String;
    sget-object v7, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 122
    iget-object v7, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-virtual {v7, v6}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->setCurrentBleKeyboardAddress(Ljava/lang/String;)V

    .line 124
    :cond_2
    const/4 v7, 0x0

    .line 125
    .local v7, "state":I
    iget-object v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-static {v8}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->-$$Nest$fgetmBleGattMapForAdd(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 126
    iget-object v8, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-static {v8}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->-$$Nest$fgetmBleGattMapForAdd(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;

    .line 127
    .local v8, "callback":Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;
    invoke-virtual {v8}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->getBluetoothGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v9

    .line 128
    .local v9, "gatt":Landroid/bluetooth/BluetoothGatt;
    const/4 v10, 0x1

    const/4 v11, 0x2

    if-ne v1, v11, :cond_4

    .line 129
    if-nez v9, :cond_3

    .line 130
    iget-object v12, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-static {v12}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v4, v12, v2, v8, v11}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;I)Landroid/bluetooth/BluetoothGatt;

    .line 132
    :cond_3
    invoke-static {}, Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;->getInstance()Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;->setCommunicationUtil(Lcom/android/server/input/padkeyboard/iic/CommunicationUtil;)V

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Connect the device successfully, keyboard:"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v7, 0x1

    goto :goto_0

    .line 135
    :cond_4
    if-nez v1, :cond_5

    if-ne v3, v10, :cond_5

    .line 138
    const/4 v7, 0x2

    goto :goto_0

    .line 139
    :cond_5
    if-nez v1, :cond_6

    if-eq v3, v11, :cond_7

    :cond_6
    if-nez v1, :cond_8

    const/4 v2, 0x3

    if-ne v3, v2, :cond_8

    .line 144
    :cond_7
    const/4 v7, 0x3

    .line 146
    :cond_8
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "status:"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, ", keyboard:"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    sget-object v2, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v2}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v2

    if-nez v2, :cond_9

    .line 148
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 149
    .local v2, "castData":Landroid/os/Bundle;
    const-string v5, "keyboardStatus"

    invoke-virtual {v2, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    iget-object v5, p0, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager$1;->this$0:Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;

    invoke-static {v5}, Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;->-$$Nest$fgetmContext(Lcom/android/server/input/padkeyboard/bluetooth/BluetoothKeyboardManager;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2}, Lcom/android/server/input/padkeyboard/MiuiKeyboardUtil;->notifySettingsKeyboardStatusChanged(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 153
    .end local v2    # "castData":Landroid/os/Bundle;
    :cond_9
    if-eq v7, v10, :cond_a

    if-eqz v9, :cond_a

    .line 154
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 156
    .end local v8    # "callback":Lcom/android/server/input/padkeyboard/bluetooth/KeyboardGattCallback;
    .end local v9    # "gatt":Landroid/bluetooth/BluetoothGatt;
    :cond_a
    goto :goto_1

    .line 157
    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The Ble devices is not keyboard! "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    .end local v1    # "newState":I
    .end local v3    # "oldState":I
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v6    # "bleAddress":Ljava/lang/String;
    .end local v7    # "state":I
    :cond_c
    :goto_1
    return-void
.end method
