public class com.android.server.input.padkeyboard.KeyboardAuthHelper {
	 /* .source "KeyboardAuthHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$KeyboardAuthInstance; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLASS_NAME;
public static final Integer INTERNAL_ERROR_LIMIT;
public static final Integer KEYBOARD_AUTH_OK;
public static final Integer KEYBOARD_IDENTITY_RETRY_TIME;
public static final Integer KEYBOARD_INTERNAL_ERROR;
public static final Integer KEYBOARD_NEED_CHECK_AGAIN;
public static final Integer KEYBOARD_REJECT;
public static final Integer KEYBOARD_TRANSFER_ERROR;
private static final java.lang.String MIDEVAUTH_TAG;
private static final java.lang.String PACKAGE_NAME;
public static final Integer TRANSFER_ERROR_LIMIT;
private static Integer sInternalErrorCount;
private static Integer sTransferErrorCount;
/* # instance fields */
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback mChallengeCallbackLaunchAfterU;
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback mChallengeCallbackLaunchBeforeU;
private android.content.ServiceConnection mConn;
private android.content.Context mContext;
private android.os.IBinder$DeathRecipient mDeathRecipient;
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback mInitCallbackLaunchAfterU;
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager$CommandCallback mInitCallbackLaunchBeforeU;
private com.xiaomi.devauth.IMiDevAuthInterface mService;
/* # direct methods */
static android.content.ServiceConnection -$$Nest$fgetmConn ( com.android.server.input.padkeyboard.KeyboardAuthHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mConn;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.padkeyboard.KeyboardAuthHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.IBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.android.server.input.padkeyboard.KeyboardAuthHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDeathRecipient;
} // .end method
static com.xiaomi.devauth.IMiDevAuthInterface -$$Nest$fgetmService ( com.android.server.input.padkeyboard.KeyboardAuthHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mService;
} // .end method
static void -$$Nest$fputmService ( com.android.server.input.padkeyboard.KeyboardAuthHelper p0, com.xiaomi.devauth.IMiDevAuthInterface p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mService = p1;
	 return;
} // .end method
static void -$$Nest$minitService ( com.android.server.input.padkeyboard.KeyboardAuthHelper p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V */
	 return;
} // .end method
static com.android.server.input.padkeyboard.KeyboardAuthHelper ( ) {
	 /* .locals 1 */
	 /* .line 42 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 43 */
	 return;
} // .end method
private com.android.server.input.padkeyboard.KeyboardAuthHelper ( ) {
	 /* .locals 1 */
	 /* .line 73 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 36 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mContext = v0;
	 /* .line 37 */
	 this.mService = v0;
	 /* .line 87 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda0;-><init>()V */
	 this.mInitCallbackLaunchBeforeU = v0;
	 /* .line 100 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda1; */
	 /* invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda1;-><init>()V */
	 this.mChallengeCallbackLaunchBeforeU = v0;
	 /* .line 114 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda2; */
	 /* invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda2;-><init>()V */
	 this.mInitCallbackLaunchAfterU = v0;
	 /* .line 127 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda3; */
	 /* invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$$ExternalSyntheticLambda3;-><init>()V */
	 this.mChallengeCallbackLaunchAfterU = v0;
	 /* .line 148 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$1;-><init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V */
	 this.mDeathRecipient = v0;
	 /* .line 159 */
	 /* new-instance v0, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper$2;-><init>(Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;)V */
	 this.mConn = v0;
	 /* .line 74 */
	 return;
} // .end method
 com.android.server.input.padkeyboard.KeyboardAuthHelper ( ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;-><init>()V */
	 return;
} // .end method
private static void BreakInternalErrorCounter ( ) {
	 /* .locals 1 */
	 /* .line 62 */
	 int v0 = 3; // const/4 v0, 0x3
	 /* .line 63 */
	 return;
} // .end method
private static void BreakTransferErrorCounter ( ) {
	 /* .locals 1 */
	 /* .line 66 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* .line 67 */
	 return;
} // .end method
public static com.android.server.input.padkeyboard.KeyboardAuthHelper getInstance ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 81 */
	 final String v0 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v0, "MiuiKeyboardManager_MiDevAuthService"
	 final String v1 = "Init bind to MiDevAuth service"; // const-string v1, "Init bind to MiDevAuth service"
	 android.util.Slog .i ( v0,v1 );
	 /* .line 82 */
	 com.android.server.input.padkeyboard.KeyboardAuthHelper$KeyboardAuthInstance .-$$Nest$sfgetINSTANCE ( );
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->setContext(Landroid/content/Context;)V */
	 /* .line 83 */
	 com.android.server.input.padkeyboard.KeyboardAuthHelper$KeyboardAuthInstance .-$$Nest$sfgetINSTANCE ( );
	 /* invoke-direct {v0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V */
	 /* .line 84 */
	 com.android.server.input.padkeyboard.KeyboardAuthHelper$KeyboardAuthInstance .-$$Nest$sfgetINSTANCE ( );
} // .end method
private static void increaseInternalErrorCounter ( ) {
	 /* .locals 1 */
	 /* .line 50 */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* .line 51 */
	 return;
} // .end method
private static void increaseTransferErrorCounter ( ) {
	 /* .locals 1 */
	 /* .line 46 */
	 /* add-int/lit8 v0, v0, 0x1 */
	 /* .line 47 */
	 return;
} // .end method
private void initService ( ) {
	 /* .locals 5 */
	 /* .line 184 */
	 /* new-instance v0, Landroid/content/Intent; */
	 /* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
	 /* .line 185 */
	 /* .local v0, "intent":Landroid/content/Intent; */
	 /* new-instance v1, Landroid/content/ComponentName; */
	 final String v2 = "com.xiaomi.devauth"; // const-string v2, "com.xiaomi.devauth"
	 final String v3 = "com.xiaomi.devauth.MiDevAuthService"; // const-string v3, "com.xiaomi.devauth.MiDevAuthService"
	 /* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
	 (( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
	 /* .line 186 */
	 v1 = this.mContext;
	 v2 = this.mConn;
	 int v3 = 1; // const/4 v3, 0x1
	 v4 = android.os.UserHandle.CURRENT;
	 v1 = 	 (( android.content.Context ) v1 ).bindServiceAsUser ( v0, v2, v3, v4 ); // invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
	 /* if-nez v1, :cond_0 */
	 /* .line 187 */
	 final String v1 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v1, "MiuiKeyboardManager_MiDevAuthService"
	 final String v2 = "cannot bind service: com.xiaomi.devauth.MiDevAuthService"; // const-string v2, "cannot bind service: com.xiaomi.devauth.MiDevAuthService"
	 android.util.Slog .e ( v1,v2 );
	 /* .line 189 */
} // :cond_0
return;
} // .end method
static Boolean lambda$new$0 ( Object[] p0 ) { //synthethic
/* .locals 5 */
/* .param p0, "recBuf" # [B */
/* .line 88 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[mInitCallbackLaunchBeforeU]Init, get rsp:"; // const-string v1, "[mInitCallbackLaunchBeforeU]Init, get rsp:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p0,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v1, "MiuiKeyboardManager_MiDevAuthService"
android.util.Slog .i ( v1,v0 );
/* .line 89 */
int v0 = 5; // const/4 v0, 0x5
/* aget-byte v2, p0, v0 */
/* const/16 v3, 0x1a */
int v4 = 0; // const/4 v4, 0x0
/* if-eq v2, v3, :cond_0 */
/* .line 90 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "[mInitCallbackLaunchBeforeU]Init, Wrong length:"; // const-string v3, "[mInitCallbackLaunchBeforeU]Init, Wrong length:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p0, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 91 */
/* .line 93 */
} // :cond_0
/* const/16 v0, 0x20 */
/* aget-byte v2, p0, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p0,v4,v0,v2 );
/* if-nez v0, :cond_1 */
/* .line 94 */
final String v0 = "[mInitCallbackLaunchBeforeU]MiDevAuth Init, Receive wrong checksum"; // const-string v0, "[mInitCallbackLaunchBeforeU]MiDevAuth Init, Receive wrong checksum"
android.util.Slog .i ( v1,v0 );
/* .line 95 */
/* .line 97 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
static Boolean lambda$new$1 ( Object[] p0 ) { //synthethic
/* .locals 5 */
/* .param p0, "recBuf" # [B */
/* .line 101 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, rsp:"; // const-string v1, "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, rsp:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p0,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v1, "MiuiKeyboardManager_MiDevAuthService"
android.util.Slog .i ( v1,v0 );
/* .line 103 */
int v0 = 5; // const/4 v0, 0x5
/* aget-byte v2, p0, v0 */
/* const/16 v3, 0x10 */
int v4 = 0; // const/4 v4, 0x0
/* if-eq v2, v3, :cond_0 */
/* .line 104 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, Wrong length:"; // const-string v3, "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, Wrong length:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p0, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 105 */
/* .line 107 */
} // :cond_0
/* const/16 v0, 0x16 */
/* aget-byte v2, p0, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p0,v4,v0,v2 );
/* if-nez v0, :cond_1 */
/* .line 108 */
final String v0 = "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, Receive wrong checksum"; // const-string v0, "[mChallengeCallbackLaunchBeforeU]Get token from keyboard, Receive wrong checksum"
android.util.Slog .i ( v1,v0 );
/* .line 109 */
/* .line 111 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
static Boolean lambda$new$2 ( Object[] p0 ) { //synthethic
/* .locals 5 */
/* .param p0, "recBuf" # [B */
/* .line 115 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[mInitCallbackLaunchAfterU]Init, get rsp:"; // const-string v1, "[mInitCallbackLaunchAfterU]Init, get rsp:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p0,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v1, "MiuiKeyboardManager_MiDevAuthService"
android.util.Slog .i ( v1,v0 );
/* .line 116 */
int v0 = 5; // const/4 v0, 0x5
/* aget-byte v2, p0, v0 */
/* const/16 v3, 0x1a */
int v4 = 0; // const/4 v4, 0x0
/* if-eq v2, v3, :cond_0 */
/* .line 117 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "[mInitCallbackLaunchAfterU]Init, Wrong length:"; // const-string v3, "[mInitCallbackLaunchAfterU]Init, Wrong length:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p0, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 118 */
/* .line 120 */
} // :cond_0
/* const/16 v0, 0x20 */
/* aget-byte v2, p0, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p0,v4,v0,v2 );
/* if-nez v0, :cond_1 */
/* .line 121 */
final String v0 = "[mInitCallbackLaunchAfterU]MiDevAuth Init, Receive wrong checksum"; // const-string v0, "[mInitCallbackLaunchAfterU]MiDevAuth Init, Receive wrong checksum"
android.util.Slog .i ( v1,v0 );
/* .line 122 */
/* .line 124 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
static Boolean lambda$new$3 ( Object[] p0 ) { //synthethic
/* .locals 6 */
/* .param p0, "recBuf" # [B */
/* .line 128 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "[mChallengeCallbackLaunchAfterU]Get token from keyboard, rsp:"; // const-string v1, "[mChallengeCallbackLaunchAfterU]Get token from keyboard, rsp:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v1, p0 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( p0,v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v1, "MiuiKeyboardManager_MiDevAuthService"
android.util.Slog .i ( v1,v0 );
/* .line 130 */
int v0 = 5; // const/4 v0, 0x5
/* aget-byte v2, p0, v0 */
/* const/16 v3, 0x20 */
/* const/16 v4, 0x10 */
int v5 = 0; // const/4 v5, 0x0
/* if-eq v2, v3, :cond_0 */
/* aget-byte v2, p0, v0 */
/* if-eq v2, v4, :cond_0 */
/* .line 131 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "[mChallengeCallbackLaunchAfterU]Get token from keyboard, Wrong length: "; // const-string v3, "[mChallengeCallbackLaunchAfterU]Get token from keyboard, Wrong length: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-byte v0, p0, v0 */
java.lang.Byte .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v3 = "%02x"; // const-string v3, "%02x"
java.lang.String .format ( v3,v0 );
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 132 */
/* .line 136 */
} // :cond_0
/* aget-byte v0, p0, v0 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v4, :cond_1 */
/* .line 137 */
final String v0 = "[mChallengeCallbackLaunchAfterU]Get token from keyboard should be 32 bytes but hack for 120 version"; // const-string v0, "[mChallengeCallbackLaunchAfterU]Get token from keyboard should be 32 bytes but hack for 120 version"
android.util.Slog .i ( v1,v0 );
/* .line 138 */
/* .line 141 */
} // :cond_1
/* const/16 v0, 0x26 */
/* aget-byte v3, p0, v0 */
v0 = com.android.server.input.padkeyboard.MiuiKeyboardUtil .checkSum ( p0,v5,v0,v3 );
/* if-nez v0, :cond_2 */
/* .line 142 */
final String v0 = "[mChallengeCallbackLaunchAfterU]Get token from keyboard, Receive wrong checksum"; // const-string v0, "[mChallengeCallbackLaunchAfterU]Get token from keyboard, Receive wrong checksum"
android.util.Slog .i ( v1,v0 );
/* .line 143 */
/* .line 145 */
} // :cond_2
} // .end method
private static void resetInternalErrorCounter ( ) {
/* .locals 1 */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* .line 59 */
return;
} // .end method
private static void resetTransferErrorCounter ( ) {
/* .locals 1 */
/* .line 54 */
int v0 = 0; // const/4 v0, 0x0
/* .line 55 */
return;
} // .end method
private void setContext ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 77 */
this.mContext = p1;
/* .line 78 */
return;
} // .end method
/* # virtual methods */
public Integer doCheckKeyboardIdentityLaunchAfterU ( com.android.server.input.padkeyboard.MiuiPadKeyboardManager p0, Boolean p1 ) {
/* .locals 21 */
/* .param p1, "miuiPadKeyboardManager" # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
/* .param p2, "isFirst" # Z */
/* .line 318 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU ]Begin check keyboardIdentity "; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU ]Begin check keyboardIdentity "
final String v3 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v3, "MiuiKeyboardManager_MiDevAuthService"
android.util.Slog .i ( v3,v0 );
/* .line 319 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 320 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 321 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetInternalErrorCounter ( );
/* .line 324 */
} // :cond_0
int v4 = 4; // const/4 v4, 0x4
int v5 = 2; // const/4 v5, 0x2
/* if-le v0, v5, :cond_1 */
/* .line 325 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[doCheckKeyboardIdentityLaunchAfterU] Meet transfer error counter:"; // const-string v5, "[doCheckKeyboardIdentityLaunchAfterU] Meet transfer error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 326 */
/* .line 328 */
} // :cond_1
int v6 = 3; // const/4 v6, 0x3
/* if-le v0, v6, :cond_2 */
/* .line 329 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[doCheckKeyboardIdentityLaunchAfterU] Meet internal error counter:"; // const-string v4, "[doCheckKeyboardIdentityLaunchAfterU] Meet internal error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 330 */
/* .line 333 */
} // :cond_2
/* const/16 v0, 0x10 */
/* new-array v13, v0, [B */
/* .line 334 */
/* .local v13, "uid":[B */
/* new-array v14, v4, [B */
/* .line 335 */
/* .local v14, "keyMetaData1":[B */
/* new-array v15, v4, [B */
/* .line 337 */
/* .local v15, "keyMetaData2":[B */
int v7 = 0; // const/4 v7, 0x0
/* .line 338 */
/* .local v7, "chooseKeyMeta":[B */
int v8 = 0; // const/4 v8, 0x0
/* .line 340 */
/* .local v8, "challenge":[B */
/* const/16 v16, 0x0 */
/* .line 344 */
/* .local v16, "result":I */
/* invoke-interface/range {p1 ..p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiDevAuthInit()[B */
/* .line 345 */
/* .local v12, "initCommand":[B */
v9 = this.mInitCallbackLaunchAfterU;
/* .line 346 */
/* .local v11, "r1":[B */
/* array-length v9, v11 */
/* if-nez v9, :cond_3 */
/* .line 347 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseTransferErrorCounter ( );
/* .line 348 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[doCheckKeyboardIdentityLaunchAfterU]Get wrong init response, Error counter:"; // const-string v4, "[doCheckKeyboardIdentityLaunchAfterU]Get wrong init response, Error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 349 */
/* .line 352 */
} // :cond_3
/* const/16 v9, 0x8 */
int v10 = 0; // const/4 v10, 0x0
java.lang.System .arraycopy ( v11,v9,v13,v10,v0 );
/* .line 353 */
/* const/16 v9, 0x18 */
java.lang.System .arraycopy ( v11,v9,v14,v10,v4 );
/* .line 354 */
/* const/16 v9, 0x1c */
java.lang.System .arraycopy ( v11,v9,v15,v10,v4 );
/* .line 355 */
v9 = this.mService;
/* if-nez v9, :cond_4 */
/* .line 356 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V */
/* .line 358 */
} // :cond_4
v9 = this.mService;
/* if-nez v9, :cond_5 */
/* .line 359 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] MiDevAuth Service is unavaiable!"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] MiDevAuth Service is unavaiable!"
android.util.Slog .e ( v3,v0 );
/* .line 360 */
/* .line 364 */
} // :cond_5
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .line 369 */
} // .end local v7 # "chooseKeyMeta":[B
/* .local v9, "chooseKeyMeta":[B */
/* nop */
/* .line 370 */
/* array-length v7, v9 */
int v6 = 1; // const/4 v6, 0x1
/* if-eq v7, v4, :cond_7 */
/* .line 371 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] Choose KeyMeta from midevauth service fail!"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Choose KeyMeta from midevauth service fail!"
android.util.Slog .e ( v3,v0 );
/* .line 372 */
/* array-length v0, v9 */
/* if-nez v0, :cond_6 */
/* .line 373 */
/* .line 375 */
} // :cond_6
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 376 */
/* .line 383 */
} // :cond_7
try { // :try_start_1
v4 = this.mService;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_4 */
/* .line 388 */
} // .end local v8 # "challenge":[B
/* .local v4, "challenge":[B */
/* nop */
/* .line 389 */
/* array-length v7, v4 */
/* if-eq v7, v0, :cond_8 */
/* .line 390 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 391 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] Get challenge from midevauth service fail!"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Get challenge from midevauth service fail!"
android.util.Slog .e ( v3,v0 );
/* .line 392 */
/* .line 395 */
} // :cond_8
/* .line 396 */
/* .local v8, "challengeCommand":[B */
v7 = this.mChallengeCallbackLaunchAfterU;
/* .line 397 */
/* .local v7, "r2":[B */
/* array-length v6, v7 */
/* if-nez v6, :cond_9 */
/* .line 398 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseTransferErrorCounter ( );
/* .line 399 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "[doCheckKeyboardIdentityLaunchAfterU] Get wrong token from keyboard! Error counter:"; // const-string v6, "[doCheckKeyboardIdentityLaunchAfterU] Get wrong token from keyboard! Error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 400 */
/* .line 404 */
} // :cond_9
int v6 = 5; // const/4 v6, 0x5
/* aget-byte v6, v7, v6 */
/* if-ne v6, v0, :cond_a */
/* .line 405 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] hack for 120 version, return auth ok"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] hack for 120 version, return auth ok"
android.util.Slog .i ( v3,v0 );
/* .line 406 */
/* .line 409 */
} // :cond_a
/* new-array v6, v0, [B */
/* .line 410 */
/* .local v6, "keyboardToken":[B */
int v5 = 6; // const/4 v5, 0x6
java.lang.System .arraycopy ( v7,v5,v6,v10,v0 );
/* .line 414 */
try { // :try_start_2
v5 = this.mService;
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_3 */
/* const/16 v17, 0x1 */
/* move-object/from16 v18, v14 */
/* move-object v14, v7 */
} // .end local v7 # "r2":[B
/* .local v14, "r2":[B */
/* .local v18, "keyMetaData1":[B */
/* move-object v7, v5 */
/* move-object v5, v8 */
} // .end local v8 # "challengeCommand":[B
/* .local v5, "challengeCommand":[B */
/* move/from16 v8, v17 */
/* move-object/from16 v17, v9 */
} // .end local v9 # "chooseKeyMeta":[B
/* .local v17, "chooseKeyMeta":[B */
/* move-object v9, v13 */
/* move-object/from16 v10, v17 */
/* move-object/from16 v19, v11 */
} // .end local v11 # "r1":[B
/* .local v19, "r1":[B */
/* move-object v11, v4 */
/* move-object/from16 v20, v12 */
} // .end local v12 # "initCommand":[B
/* .local v20, "initCommand":[B */
/* move-object v12, v6 */
try { // :try_start_3
v7 = /* invoke-interface/range {v7 ..v12}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->tokenVerify(I[B[B[B[B)I */
/* :try_end_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 419 */
} // .end local v16 # "result":I
/* .local v7, "result":I */
/* nop */
/* .line 420 */
int v8 = 1; // const/4 v8, 0x1
/* if-ne v7, v8, :cond_b */
/* .line 421 */
final String v8 = "[doCheckKeyboardIdentityLaunchAfterU] verify token success with online key"; // const-string v8, "[doCheckKeyboardIdentityLaunchAfterU] verify token success with online key"
android.util.Slog .i ( v3,v8 );
/* .line 423 */
} // :cond_b
int v8 = 2; // const/4 v8, 0x2
/* if-ne v7, v8, :cond_c */
/* .line 424 */
final String v8 = "[doCheckKeyboardIdentityLaunchAfterU] verify token success with offline key"; // const-string v8, "[doCheckKeyboardIdentityLaunchAfterU] verify token success with offline key"
android.util.Slog .i ( v3,v8 );
/* .line 426 */
} // :cond_c
int v8 = 3; // const/4 v8, 0x3
/* if-ne v7, v8, :cond_d */
/* .line 427 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 428 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 429 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] Meet internal error when try verify token!"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Meet internal error when try verify token!"
android.util.Slog .e ( v3,v0 );
/* .line 430 */
int v3 = 2; // const/4 v3, 0x2
/* .line 432 */
} // :cond_d
int v8 = -1; // const/4 v8, -0x1
/* if-ne v7, v8, :cond_e */
/* .line 433 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .BreakInternalErrorCounter ( );
/* .line 434 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .BreakTransferErrorCounter ( );
/* .line 435 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] fail to verify keyboard token!"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] fail to verify keyboard token!"
android.util.Slog .e ( v3,v0 );
/* .line 436 */
int v8 = 1; // const/4 v8, 0x1
/* .line 441 */
} // :cond_e
int v8 = 1; // const/4 v8, 0x1
/* const/16 v9, 0x16 */
int v10 = 0; // const/4 v10, 0x0
java.lang.System .arraycopy ( v14,v9,v4,v10,v0 );
/* .line 442 */
int v9 = 0; // const/4 v9, 0x0
/* .line 444 */
/* .local v9, "padToken":[B */
try { // :try_start_4
v11 = this.mService;
/* :try_end_4 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_1 */
/* move-object/from16 v12, v17 */
} // .end local v17 # "chooseKeyMeta":[B
/* .local v12, "chooseKeyMeta":[B */
try { // :try_start_5
/* :try_end_5 */
/* .catch Landroid/os/RemoteException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 449 */
} // .end local v9 # "padToken":[B
/* .local v8, "padToken":[B */
/* nop */
/* .line 451 */
/* array-length v9, v8 */
/* if-eq v9, v0, :cond_f */
/* .line 452 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 453 */
final String v0 = "[doCheckKeyboardIdentityLaunchAfterU] Get token from midevauth service fail!"; // const-string v0, "[doCheckKeyboardIdentityLaunchAfterU] Get token from midevauth service fail!"
android.util.Slog .e ( v3,v0 );
/* .line 454 */
int v3 = 2; // const/4 v3, 0x2
/* .line 456 */
} // :cond_f
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "[doCheckKeyboardIdentityLaunchAfterU] tokenGet from midevauthservice = "; // const-string v9, "[doCheckKeyboardIdentityLaunchAfterU] tokenGet from midevauthservice = "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v9, v8 */
com.android.server.input.padkeyboard.MiuiKeyboardUtil .Bytes2Hex ( v8,v9 );
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 458 */
/* .line 459 */
/* .local v0, "verifyDeviceCommand":[B */
int v3 = 0; // const/4 v3, 0x0
/* .line 461 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 462 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetInternalErrorCounter ( );
/* .line 463 */
/* .line 445 */
} // .end local v0 # "verifyDeviceCommand":[B
} // .end local v8 # "padToken":[B
/* .restart local v9 # "padToken":[B */
/* :catch_0 */
/* move-exception v0 */
} // .end local v12 # "chooseKeyMeta":[B
/* .restart local v17 # "chooseKeyMeta":[B */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v12, v17 */
/* .line 446 */
} // .end local v17 # "chooseKeyMeta":[B
/* .local v0, "e":Landroid/os/RemoteException; */
/* .restart local v12 # "chooseKeyMeta":[B */
} // :goto_0
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 447 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "[doCheckKeyboardIdentityLaunchAfterU] call tokenGet fail: "; // const-string v10, "[doCheckKeyboardIdentityLaunchAfterU] call tokenGet fail: "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v8 );
/* .line 448 */
int v3 = 2; // const/4 v3, 0x2
/* .line 415 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v7 # "result":I
} // .end local v9 # "padToken":[B
} // .end local v12 # "chooseKeyMeta":[B
/* .restart local v16 # "result":I */
/* .restart local v17 # "chooseKeyMeta":[B */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v12, v17 */
} // .end local v17 # "chooseKeyMeta":[B
/* .restart local v12 # "chooseKeyMeta":[B */
} // .end local v5 # "challengeCommand":[B
} // .end local v18 # "keyMetaData1":[B
} // .end local v19 # "r1":[B
} // .end local v20 # "initCommand":[B
/* .local v7, "r2":[B */
/* .local v8, "challengeCommand":[B */
/* .local v9, "chooseKeyMeta":[B */
/* .restart local v11 # "r1":[B */
/* .local v12, "initCommand":[B */
/* .local v14, "keyMetaData1":[B */
/* :catch_3 */
/* move-exception v0 */
/* move-object v5, v8 */
/* move-object/from16 v19, v11 */
/* move-object/from16 v20, v12 */
/* move-object/from16 v18, v14 */
/* move-object v14, v7 */
/* move-object v12, v9 */
/* .line 416 */
} // .end local v7 # "r2":[B
} // .end local v8 # "challengeCommand":[B
} // .end local v9 # "chooseKeyMeta":[B
} // .end local v11 # "r1":[B
/* .restart local v0 # "e":Landroid/os/RemoteException; */
/* .restart local v5 # "challengeCommand":[B */
/* .local v12, "chooseKeyMeta":[B */
/* .local v14, "r2":[B */
/* .restart local v18 # "keyMetaData1":[B */
/* .restart local v19 # "r1":[B */
/* .restart local v20 # "initCommand":[B */
} // :goto_1
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 417 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "[doCheckKeyboardIdentityLaunchAfterU] call token_verify fail: "; // const-string v8, "[doCheckKeyboardIdentityLaunchAfterU] call token_verify fail: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v7 );
/* .line 418 */
int v3 = 2; // const/4 v3, 0x2
/* .line 384 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v4 # "challenge":[B
} // .end local v5 # "challengeCommand":[B
} // .end local v6 # "keyboardToken":[B
} // .end local v18 # "keyMetaData1":[B
} // .end local v19 # "r1":[B
} // .end local v20 # "initCommand":[B
/* .local v8, "challenge":[B */
/* .restart local v9 # "chooseKeyMeta":[B */
/* .restart local v11 # "r1":[B */
/* .local v12, "initCommand":[B */
/* .local v14, "keyMetaData1":[B */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v19, v11 */
/* move-object/from16 v20, v12 */
/* move-object/from16 v18, v14 */
/* move-object v12, v9 */
/* .line 385 */
} // .end local v9 # "chooseKeyMeta":[B
} // .end local v11 # "r1":[B
} // .end local v14 # "keyMetaData1":[B
/* .restart local v0 # "e":Landroid/os/RemoteException; */
/* .local v12, "chooseKeyMeta":[B */
/* .restart local v18 # "keyMetaData1":[B */
/* .restart local v19 # "r1":[B */
/* .restart local v20 # "initCommand":[B */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 386 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[doCheckKeyboardIdentityLaunchAfterU] call getChallenge fail: "; // const-string v5, "[doCheckKeyboardIdentityLaunchAfterU] call getChallenge fail: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 387 */
int v3 = 2; // const/4 v3, 0x2
/* .line 365 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v18 # "keyMetaData1":[B
} // .end local v19 # "r1":[B
} // .end local v20 # "initCommand":[B
/* .local v7, "chooseKeyMeta":[B */
/* .restart local v11 # "r1":[B */
/* .local v12, "initCommand":[B */
/* .restart local v14 # "keyMetaData1":[B */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v19, v11 */
/* move-object/from16 v20, v12 */
/* move-object/from16 v18, v14 */
/* move-object v4, v0 */
} // .end local v11 # "r1":[B
} // .end local v12 # "initCommand":[B
} // .end local v14 # "keyMetaData1":[B
/* .restart local v18 # "keyMetaData1":[B */
/* .restart local v19 # "r1":[B */
/* .restart local v20 # "initCommand":[B */
/* move-object v0, v4 */
/* .line 366 */
/* .restart local v0 # "e":Landroid/os/RemoteException; */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 367 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[doCheckKeyboardIdentityLaunchAfterU] call chooseKeyMeta fail: "; // const-string v5, "[doCheckKeyboardIdentityLaunchAfterU] call chooseKeyMeta fail: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 368 */
int v3 = 2; // const/4 v3, 0x2
} // .end method
public Integer doCheckKeyboardIdentityLaunchBeforeU ( com.android.server.input.padkeyboard.MiuiPadKeyboardManager p0, Boolean p1 ) {
/* .locals 21 */
/* .param p1, "miuiPadKeyboardManager" # Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
/* .param p2, "isFirst" # Z */
/* .line 192 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU ]Begin check keyboardIdentity "; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU ]Begin check keyboardIdentity "
final String v3 = "MiuiKeyboardManager_MiDevAuthService"; // const-string v3, "MiuiKeyboardManager_MiDevAuthService"
android.util.Slog .i ( v3,v0 );
/* .line 193 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 194 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 195 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetInternalErrorCounter ( );
/* .line 197 */
} // :cond_0
int v4 = 4; // const/4 v4, 0x4
int v5 = 2; // const/4 v5, 0x2
/* if-le v0, v5, :cond_1 */
/* .line 198 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[doCheckKeyboardIdentityLaunchBeforeU] Meet transfer error counter:"; // const-string v5, "[doCheckKeyboardIdentityLaunchBeforeU] Meet transfer error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 199 */
/* .line 201 */
} // :cond_1
int v6 = 3; // const/4 v6, 0x3
/* if-le v0, v6, :cond_2 */
/* .line 202 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[doCheckKeyboardIdentityLaunchBeforeU] Meet internal error counter:"; // const-string v4, "[doCheckKeyboardIdentityLaunchBeforeU] Meet internal error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 203 */
/* .line 206 */
} // :cond_2
/* const/16 v0, 0x10 */
/* new-array v13, v0, [B */
/* .line 207 */
/* .local v13, "uid":[B */
/* new-array v14, v4, [B */
/* .line 208 */
/* .local v14, "keyMetaData1":[B */
/* new-array v15, v4, [B */
/* .line 210 */
/* .local v15, "keyMetaData2":[B */
int v7 = 0; // const/4 v7, 0x0
/* .line 211 */
/* .local v7, "chooseKeyMeta":[B */
int v8 = 0; // const/4 v8, 0x0
/* .line 213 */
/* .local v8, "challenge":[B */
/* const/16 v16, 0x0 */
/* .line 217 */
/* .local v16, "result":I */
/* invoke-interface/range {p1 ..p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->commandMiDevAuthInit()[B */
/* .line 218 */
/* .local v12, "initCommand":[B */
v9 = this.mInitCallbackLaunchBeforeU;
/* .line 219 */
/* .local v11, "r1":[B */
/* array-length v9, v11 */
/* if-nez v9, :cond_3 */
/* .line 220 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseTransferErrorCounter ( );
/* .line 221 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Init fail! Error counter:"; // const-string v4, "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Init fail! Error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 222 */
/* .line 225 */
} // :cond_3
/* const/16 v9, 0x8 */
int v10 = 0; // const/4 v10, 0x0
java.lang.System .arraycopy ( v11,v9,v13,v10,v0 );
/* .line 226 */
/* const/16 v9, 0x18 */
java.lang.System .arraycopy ( v11,v9,v14,v10,v4 );
/* .line 227 */
/* const/16 v9, 0x1c */
java.lang.System .arraycopy ( v11,v9,v15,v10,v4 );
/* .line 228 */
v9 = this.mService;
/* if-nez v9, :cond_4 */
/* .line 229 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/padkeyboard/KeyboardAuthHelper;->initService()V */
/* .line 231 */
} // :cond_4
v9 = this.mService;
/* if-nez v9, :cond_5 */
/* .line 232 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Service is unavaiable!"; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Service is unavaiable!"
android.util.Slog .e ( v3,v0 );
/* .line 233 */
/* .line 237 */
} // :cond_5
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .line 242 */
} // .end local v7 # "chooseKeyMeta":[B
/* .local v9, "chooseKeyMeta":[B */
/* nop */
/* .line 243 */
/* array-length v7, v9 */
int v6 = 1; // const/4 v6, 0x1
/* if-eq v7, v4, :cond_7 */
/* .line 244 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU] Choose KeyMeta from midevauth service fail!"; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Choose KeyMeta from midevauth service fail!"
android.util.Slog .e ( v3,v0 );
/* .line 245 */
/* array-length v0, v9 */
/* if-nez v0, :cond_6 */
/* .line 246 */
/* .line 248 */
} // :cond_6
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 249 */
/* .line 256 */
} // :cond_7
try { // :try_start_1
v4 = this.mService;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .line 261 */
} // .end local v8 # "challenge":[B
/* .local v4, "challenge":[B */
/* nop */
/* .line 262 */
/* array-length v7, v4 */
/* if-eq v7, v0, :cond_8 */
/* .line 263 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 264 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU] Get challenge from midevauth service fail!"; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Get challenge from midevauth service fail!"
android.util.Slog .e ( v3,v0 );
/* .line 265 */
/* .line 268 */
} // :cond_8
/* .line 269 */
/* .local v8, "challengeCommand":[B */
v7 = this.mChallengeCallbackLaunchBeforeU;
/* .line 271 */
/* .local v7, "r2":[B */
/* array-length v6, v7 */
/* if-nez v6, :cond_9 */
/* .line 272 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseTransferErrorCounter ( );
/* .line 273 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Get token from keyboard fail! Error counter:"; // const-string v6, "[doCheckKeyboardIdentityLaunchBeforeU] MiDevAuth Get token from keyboard fail! Error counter:"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v0 );
/* .line 274 */
/* .line 276 */
} // :cond_9
/* new-array v6, v0, [B */
/* .line 277 */
/* .local v6, "token":[B */
int v5 = 6; // const/4 v5, 0x6
java.lang.System .arraycopy ( v7,v5,v6,v10,v0 );
/* .line 281 */
try { // :try_start_2
v0 = this.mService;
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_1 */
int v5 = 1; // const/4 v5, 0x1
/* move-object/from16 v17, v7 */
} // .end local v7 # "r2":[B
/* .local v17, "r2":[B */
/* move-object v7, v0 */
/* move-object/from16 v18, v8 */
} // .end local v8 # "challengeCommand":[B
/* .local v18, "challengeCommand":[B */
/* move v8, v5 */
/* move-object v5, v9 */
} // .end local v9 # "chooseKeyMeta":[B
/* .local v5, "chooseKeyMeta":[B */
/* move-object v9, v13 */
/* move v0, v10 */
/* move-object v10, v5 */
/* move-object/from16 v19, v11 */
} // .end local v11 # "r1":[B
/* .local v19, "r1":[B */
/* move-object v11, v4 */
/* move-object/from16 v20, v12 */
} // .end local v12 # "initCommand":[B
/* .local v20, "initCommand":[B */
/* move-object v12, v6 */
try { // :try_start_3
v7 = /* invoke-interface/range {v7 ..v12}, Lcom/xiaomi/devauth/IMiDevAuthInterface;->tokenVerify(I[B[B[B[B)I */
/* :try_end_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 286 */
} // .end local v16 # "result":I
/* .local v7, "result":I */
/* nop */
/* .line 287 */
int v8 = 1; // const/4 v8, 0x1
/* if-ne v7, v8, :cond_a */
/* .line 288 */
final String v8 = "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard PASS with online key"; // const-string v8, "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard PASS with online key"
android.util.Slog .i ( v3,v8 );
/* .line 289 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 290 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetInternalErrorCounter ( );
/* .line 291 */
/* .line 293 */
} // :cond_a
int v8 = 2; // const/4 v8, 0x2
/* if-ne v7, v8, :cond_b */
/* .line 294 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard PASS with offline key, need check again later"; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard PASS with offline key, need check again later"
android.util.Slog .i ( v3,v0 );
/* .line 295 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 296 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetInternalErrorCounter ( );
/* .line 297 */
/* .line 299 */
} // :cond_b
int v0 = 3; // const/4 v0, 0x3
/* if-ne v7, v0, :cond_c */
/* .line 300 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .resetTransferErrorCounter ( );
/* .line 301 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 302 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU] Meet internal error when try check keyboard!"; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] Meet internal error when try check keyboard!"
android.util.Slog .e ( v3,v0 );
/* .line 303 */
/* .line 305 */
} // :cond_c
int v0 = -1; // const/4 v0, -0x1
/* if-ne v7, v0, :cond_d */
/* .line 306 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .BreakInternalErrorCounter ( );
/* .line 307 */
com.android.server.input.padkeyboard.KeyboardAuthHelper .BreakTransferErrorCounter ( );
/* .line 308 */
final String v0 = "[doCheckKeyboardIdentityLaunchBeforeU] fail to verify keyboard token!"; // const-string v0, "[doCheckKeyboardIdentityLaunchBeforeU] fail to verify keyboard token!"
android.util.Slog .e ( v3,v0 );
/* .line 309 */
int v0 = 1; // const/4 v0, 0x1
/* .line 312 */
} // :cond_d
int v0 = 1; // const/4 v0, 0x1
final String v8 = "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard Fail!"; // const-string v8, "[doCheckKeyboardIdentityLaunchBeforeU] Check keyboard Fail!"
android.util.Slog .i ( v3,v8 );
/* .line 313 */
/* .line 282 */
} // .end local v7 # "result":I
/* .restart local v16 # "result":I */
/* :catch_0 */
/* move-exception v0 */
} // .end local v5 # "chooseKeyMeta":[B
} // .end local v17 # "r2":[B
} // .end local v18 # "challengeCommand":[B
} // .end local v19 # "r1":[B
} // .end local v20 # "initCommand":[B
/* .local v7, "r2":[B */
/* .restart local v8 # "challengeCommand":[B */
/* .restart local v9 # "chooseKeyMeta":[B */
/* .restart local v11 # "r1":[B */
/* .restart local v12 # "initCommand":[B */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v17, v7 */
/* move-object/from16 v18, v8 */
/* move-object v5, v9 */
/* move-object/from16 v19, v11 */
/* move-object/from16 v20, v12 */
/* .line 283 */
} // .end local v7 # "r2":[B
} // .end local v8 # "challengeCommand":[B
} // .end local v9 # "chooseKeyMeta":[B
} // .end local v11 # "r1":[B
} // .end local v12 # "initCommand":[B
/* .local v0, "e":Landroid/os/RemoteException; */
/* .restart local v5 # "chooseKeyMeta":[B */
/* .restart local v17 # "r2":[B */
/* .restart local v18 # "challengeCommand":[B */
/* .restart local v19 # "r1":[B */
/* .restart local v20 # "initCommand":[B */
} // :goto_0
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 284 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "[doCheckKeyboardIdentityLaunchBeforeU] call token_verify fail: "; // const-string v8, "[doCheckKeyboardIdentityLaunchBeforeU] call token_verify fail: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v7 );
/* .line 285 */
int v3 = 2; // const/4 v3, 0x2
/* .line 257 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v4 # "challenge":[B
} // .end local v5 # "chooseKeyMeta":[B
} // .end local v6 # "token":[B
} // .end local v17 # "r2":[B
} // .end local v18 # "challengeCommand":[B
} // .end local v19 # "r1":[B
} // .end local v20 # "initCommand":[B
/* .local v8, "challenge":[B */
/* .restart local v9 # "chooseKeyMeta":[B */
/* .restart local v11 # "r1":[B */
/* .restart local v12 # "initCommand":[B */
/* :catch_2 */
/* move-exception v0 */
/* move-object v5, v9 */
/* move-object/from16 v19, v11 */
/* move-object/from16 v20, v12 */
/* .line 258 */
} // .end local v9 # "chooseKeyMeta":[B
} // .end local v11 # "r1":[B
} // .end local v12 # "initCommand":[B
/* .restart local v0 # "e":Landroid/os/RemoteException; */
/* .restart local v5 # "chooseKeyMeta":[B */
/* .restart local v19 # "r1":[B */
/* .restart local v20 # "initCommand":[B */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 259 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "[doCheckKeyboardIdentityLaunchBeforeU] call getChallenge fail: "; // const-string v6, "[doCheckKeyboardIdentityLaunchBeforeU] call getChallenge fail: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 260 */
int v3 = 2; // const/4 v3, 0x2
/* .line 238 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // .end local v5 # "chooseKeyMeta":[B
} // .end local v19 # "r1":[B
} // .end local v20 # "initCommand":[B
/* .local v7, "chooseKeyMeta":[B */
/* .restart local v11 # "r1":[B */
/* .restart local v12 # "initCommand":[B */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v19, v11 */
/* move-object/from16 v20, v12 */
/* move-object v4, v0 */
} // .end local v11 # "r1":[B
} // .end local v12 # "initCommand":[B
/* .restart local v19 # "r1":[B */
/* .restart local v20 # "initCommand":[B */
/* move-object v0, v4 */
/* .line 239 */
/* .restart local v0 # "e":Landroid/os/RemoteException; */
com.android.server.input.padkeyboard.KeyboardAuthHelper .increaseInternalErrorCounter ( );
/* .line 240 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "[doCheckKeyboardIdentityLaunchBeforeU] call chooseKeyMeta fail: "; // const-string v5, "[doCheckKeyboardIdentityLaunchBeforeU] call chooseKeyMeta fail: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 241 */
int v3 = 2; // const/4 v3, 0x2
} // .end method
