class com.android.server.input.MiuiInputManagerService$ShortcutListenerRecord implements android.os.IBinder$DeathRecipient {
	 /* .source "MiuiInputManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/MiuiInputManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ShortcutListenerRecord" */
} // .end annotation
/* # instance fields */
private final miui.hardware.input.IShortcutSettingsChangedListener mChangedListener;
private final Integer mPid;
final com.android.server.input.MiuiInputManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.input.MiuiInputManagerService$ShortcutListenerRecord ( ) {
/* .locals 0 */
/* .param p2, "settingsChangedListener" # Lmiui/hardware/input/IShortcutSettingsChangedListener; */
/* .param p3, "pid" # I */
/* .line 430 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 431 */
this.mChangedListener = p2;
/* .line 432 */
/* iput p3, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mPid:I */
/* .line 433 */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 2 */
/* .line 437 */
v0 = this.this$0;
/* iget v1, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mPid:I */
com.android.server.input.MiuiInputManagerService .-$$Nest$mremoveListenerFromSystem ( v0,v1 );
/* .line 438 */
return;
} // .end method
public void notifyShortcutSettingsChanged ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 442 */
try { // :try_start_0
	 v0 = this.mChangedListener;
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 447 */
	 /* .line 443 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 444 */
	 /* .local v0, "ex":Landroid/os/RemoteException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Failed to notify process "; // const-string v2, "Failed to notify process "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v2, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mPid:I */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = " that Settings Changed, assuming it died."; // const-string v2, " that Settings Changed, assuming it died."
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
	 android.util.Slog .w ( v2,v1,v0 );
	 /* .line 446 */
	 (( com.android.server.input.MiuiInputManagerService$ShortcutListenerRecord ) p0 ).binderDied ( ); // invoke-virtual {p0}, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->binderDied()V
	 /* .line 448 */
} // .end local v0 # "ex":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
