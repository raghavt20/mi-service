public class com.android.server.input.TouchWakeUpFeatureManager {
	 /* .source "TouchWakeUpFeatureManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;, */
	 /* Lcom/android/server/input/TouchWakeUpFeatureManager$TouchWakeUpFeatureManagerHolder; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static final Integer WAKEUP_OFF;
private static final Integer WAKEUP_ON;
/* # instance fields */
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private final com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver mSettingsObserver;
private final miui.util.ITouchFeature mTouchFeature;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.TouchWakeUpFeatureManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.input.TouchWakeUpFeatureManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$monGestureWakeupSettingsChanged ( com.android.server.input.TouchWakeUpFeatureManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onGestureWakeupSettingsChanged(Z)V */
	 return;
} // .end method
static void -$$Nest$monSubScreenGestureWakeupSettingsChanged ( com.android.server.input.TouchWakeUpFeatureManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onSubScreenGestureWakeupSettingsChanged(Z)V */
	 return;
} // .end method
private com.android.server.input.TouchWakeUpFeatureManager ( ) {
	 /* .locals 2 */
	 /* .line 45 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 46 */
	 android.app.ActivityThread .currentActivityThread ( );
	 (( android.app.ActivityThread ) v0 ).getSystemContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
	 this.mContext = v0;
	 /* .line 47 */
	 /* new-instance v0, Landroid/os/Handler; */
	 com.android.server.input.MiuiInputThread .getHandler ( );
	 (( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 48 */
	 miui.util.ITouchFeature .getInstance ( );
	 this.mTouchFeature = v1;
	 /* .line 49 */
	 /* new-instance v1, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver; */
	 /* invoke-direct {v1, p0, v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;-><init>(Lcom/android/server/input/TouchWakeUpFeatureManager;Landroid/os/Handler;)V */
	 this.mSettingsObserver = v1;
	 /* .line 50 */
	 return;
} // .end method
 com.android.server.input.TouchWakeUpFeatureManager ( ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/input/TouchWakeUpFeatureManager;-><init>()V */
	 return;
} // .end method
public static com.android.server.input.TouchWakeUpFeatureManager getInstance ( ) {
	 /* .locals 1 */
	 /* .line 57 */
	 com.android.server.input.TouchWakeUpFeatureManager$TouchWakeUpFeatureManagerHolder .-$$Nest$sfgetsInstance ( );
} // .end method
private void onGestureWakeupSettingsChanged ( Boolean p0 ) {
	 /* .locals 4 */
	 /* .param p1, "newState" # Z */
	 /* .line 70 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "On gesture wakeup settings changed, newState = "; // const-string v1, "On gesture wakeup settings changed, newState = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "TouchWakeUpFeatureManager"; // const-string v1, "TouchWakeUpFeatureManager"
	 android.util.Slog .w ( v1,v0 );
	 /* .line 71 */
	 v0 = this.mTouchFeature;
	 v0 = 	 (( miui.util.ITouchFeature ) v0 ).hasDoubleTapWakeUpSupport ( ); // invoke-virtual {v0}, Lmiui/util/ITouchFeature;->hasDoubleTapWakeUpSupport()Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 73 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, v0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->setTouchMode(IZ)V */
		 /* .line 75 */
		 v0 = 		 miui.util.MiuiMultiDisplayTypeInfo .isFoldDevice ( );
		 /* if-nez v0, :cond_0 */
		 /* .line 76 */
		 v0 = 		 miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
		 if ( v0 != null) { // if-eqz v0, :cond_3
			 /* .line 77 */
		 } // :cond_0
		 int v0 = 1; // const/4 v0, 0x1
		 /* invoke-direct {p0, v0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->setTouchMode(IZ)V */
		 /* .line 81 */
	 } // :cond_1
	 com.android.server.input.config.InputCommonConfig .getInstance ( );
	 /* .line 82 */
	 /* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
	 if ( p1 != null) { // if-eqz p1, :cond_2
		 int v2 = 5; // const/4 v2, 0x5
	 } // :cond_2
	 int v2 = 4; // const/4 v2, 0x4
} // :goto_0
(( com.android.server.input.config.InputCommonConfig ) v0 ).setWakeUpMode ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/input/config/InputCommonConfig;->setWakeUpMode(I)V
/* .line 83 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 84 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Flush wake up state to native, newState = "; // const-string v3, "Flush wake up state to native, newState = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 86 */
} // .end local v0 # "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
} // :cond_3
} // :goto_1
return;
} // .end method
private void onSubScreenGestureWakeupSettingsChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "newState" # Z */
/* .line 89 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "On sub screen gesture wakeup settings changed, newState = "; // const-string v1, "On sub screen gesture wakeup settings changed, newState = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "TouchWakeUpFeatureManager"; // const-string v1, "TouchWakeUpFeatureManager"
android.util.Slog .w ( v1,v0 );
/* .line 90 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->setTouchMode(IZ)V */
/* .line 91 */
return;
} // .end method
private void setTouchMode ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "id" # I */
/* .param p2, "newState" # Z */
/* .line 94 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Update state to ITouchFeature, id = "; // const-string v1, "Update state to ITouchFeature, id = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mode = "; // const-string v1, ", mode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0xe */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", newState = "; // const-string v2, ", newState = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 96 */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 94 */
final String v2 = "TouchWakeUpFeatureManager"; // const-string v2, "TouchWakeUpFeatureManager"
android.util.Slog .w ( v2,v0 );
/* .line 97 */
v0 = this.mTouchFeature;
(( miui.util.ITouchFeature ) v0 ).setTouchMode ( p1, v1, p2 ); // invoke-virtual {v0, p1, v1, p2}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 98 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 150 */
/* new-instance v0, Landroid/util/IndentingPrintWriter; */
final String v1 = " "; // const-string v1, " "
/* invoke-direct {v0, p1, v1, p2}, Landroid/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 151 */
/* .local v0, "ipw":Landroid/util/IndentingPrintWriter; */
final String v1 = "TouchWakeUpFeatureManager"; // const-string v1, "TouchWakeUpFeatureManager"
(( android.util.IndentingPrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 152 */
(( android.util.IndentingPrintWriter ) v0 ).increaseIndent ( ); // invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->increaseIndent()Landroid/util/IndentingPrintWriter;
/* .line 153 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "HasDoubleTapWakeUpSupport = "; // const-string v2, "HasDoubleTapWakeUpSupport = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTouchFeature;
v2 = (( miui.util.ITouchFeature ) v2 ).hasDoubleTapWakeUpSupport ( ); // invoke-virtual {v2}, Lmiui/util/ITouchFeature;->hasDoubleTapWakeUpSupport()Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.IndentingPrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 154 */
(( android.util.IndentingPrintWriter ) v0 ).decreaseIndent ( ); // invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->decreaseIndent()Landroid/util/IndentingPrintWriter;
/* .line 155 */
return;
} // .end method
public void onSystemBooted ( ) {
/* .locals 1 */
/* .line 61 */
v0 = this.mSettingsObserver;
(( com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver ) v0 ).register ( ); // invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->register()V
/* .line 62 */
v0 = this.mSettingsObserver;
(( com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver ) v0 ).refreshAllSettings ( ); // invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->refreshAllSettings()V
/* .line 63 */
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "userId" # I */
/* .line 66 */
v0 = this.mSettingsObserver;
(( com.android.server.input.TouchWakeUpFeatureManager$SettingsObserver ) v0 ).refreshAllSettings ( ); // invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->refreshAllSettings()V
/* .line 67 */
return;
} // .end method
