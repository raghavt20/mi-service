public class com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils {
	 /* .source "ScrollerOptimizationConfigProviderUtils.java" */
	 /* # static fields */
	 public static final java.lang.String APP_LIST_NAME;
	 private static final java.lang.String FLING_VELOCITY_MAXIMUM;
	 private static final java.lang.String FLING_VELOCITY_SCALED;
	 private static final java.lang.String FLING_VELOCITY_THRESHOLD;
	 private static final java.lang.String FLY_WHEEL;
	 private static final java.lang.String FLY_WHEEL_PARAM_1;
	 private static final java.lang.String FLY_WHEEL_PARAM_2;
	 private static final java.lang.String FLY_WHEEL_PARAM_3;
	 private static final java.lang.String FLY_WHEEL_TIME_INTERVAL_THRESHOLD;
	 private static final java.lang.String FLY_WHEEL_VELOCITY_THRESHOLD_1;
	 private static final java.lang.String FLY_WHEEL_VELOCITY_THRESHOLD_2;
	 private static final java.lang.String FRICTION;
	 private static final java.lang.String OPTIMIZE_ENABLE;
	 public static final java.lang.String PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 private static final java.lang.String VELOCITY_THRESHOLD;
	 /* # direct methods */
	 static com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils ( ) {
		 /* .locals 1 */
		 /* .line 18 */
		 /* const-class v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils ( ) {
		 /* .locals 0 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static org.json.JSONObject getLocalFileConfig ( java.lang.String p0 ) {
		 /* .locals 5 */
		 /* .param p0, "filePath" # Ljava/lang/String; */
		 /* .line 115 */
		 /* new-instance v0, Lorg/json/JSONObject; */
		 /* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
		 /* .line 116 */
		 /* .local v0, "jsonObject":Lorg/json/JSONObject; */
		 com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils .readLocalFile ( p0 );
		 /* .line 118 */
		 /* .local v1, "configString":Ljava/lang/String; */
		 try { // :try_start_0
			 /* new-instance v2, Lorg/json/JSONObject; */
			 /* invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
			 /* :try_end_0 */
			 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* move-object v0, v2 */
			 /* .line 121 */
			 /* .line 119 */
			 /* :catch_0 */
			 /* move-exception v2 */
			 /* .line 120 */
			 /* .local v2, "e":Lorg/json/JSONException; */
			 v3 = com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils.TAG;
			 final String v4 = "exception when getLocalFileConfig: "; // const-string v4, "exception when getLocalFileConfig: "
			 android.util.Slog .e ( v3,v4,v2 );
			 /* .line 122 */
		 } // .end local v2 # "e":Lorg/json/JSONException;
	 } // :goto_0
} // .end method
public static java.lang.String parseGeneralConfig ( org.json.JSONObject p0 ) {
	 /* .locals 4 */
	 /* .param p0, "jsonAll" # Lorg/json/JSONObject; */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Lorg/json/JSONException; */
	 /* } */
} // .end annotation
/* .line 37 */
v0 = (( org.json.JSONObject ) p0 ).length ( ); // invoke-virtual {p0}, Lorg/json/JSONObject;->length()I
/* if-nez v0, :cond_0 */
/* .line 38 */
int v0 = 0; // const/4 v0, 0x0
/* .line 40 */
} // :cond_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 42 */
/* .local v0, "jsonGeneralConfig":Lorg/json/JSONObject; */
final String v1 = "isOptimizeEnable"; // const-string v1, "isOptimizeEnable"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 43 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 44 */
/* .local v2, "isOptimizeEnable":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 46 */
} // .end local v2 # "isOptimizeEnable":I
} // :cond_1
final String v1 = "friction"; // const-string v1, "friction"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 47 */
(( org.json.JSONObject ) p0 ).getDouble ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v2 */
/* .line 48 */
/* .local v2, "friction":D */
(( org.json.JSONObject ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 50 */
} // .end local v2 # "friction":D
} // :cond_2
/* const-string/jumbo v1, "velocityThreshold" */
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 51 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 52 */
/* .local v2, "velocityThreshold":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 54 */
} // .end local v2 # "velocityThreshold":I
} // :cond_3
final String v1 = "flywheel"; // const-string v1, "flywheel"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 55 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 56 */
/* .local v2, "flywheel":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 58 */
} // .end local v2 # "flywheel":I
} // :cond_4
final String v1 = "flywheelTimeIntervalThreshold"; // const-string v1, "flywheelTimeIntervalThreshold"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 59 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 60 */
/* .local v2, "flywheelTimeIntervalThreshold":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 62 */
} // .end local v2 # "flywheelTimeIntervalThreshold":I
} // :cond_5
final String v1 = "flywheelVelocityThreshold1"; // const-string v1, "flywheelVelocityThreshold1"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 63 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 64 */
/* .local v2, "flywheelVelocityThreshold1":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 66 */
} // .end local v2 # "flywheelVelocityThreshold1":I
} // :cond_6
final String v1 = "flywheelVelocityThreshold2"; // const-string v1, "flywheelVelocityThreshold2"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 67 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 68 */
/* .local v2, "flywheelVelocityThreshold2":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 70 */
} // .end local v2 # "flywheelVelocityThreshold2":I
} // :cond_7
final String v1 = "flywheelParam1"; // const-string v1, "flywheelParam1"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 71 */
(( org.json.JSONObject ) p0 ).getDouble ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v2 */
/* .line 72 */
/* .local v2, "flywheelParam1":D */
(( org.json.JSONObject ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 74 */
} // .end local v2 # "flywheelParam1":D
} // :cond_8
final String v1 = "flywheelParam2"; // const-string v1, "flywheelParam2"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 75 */
(( org.json.JSONObject ) p0 ).getDouble ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v2 */
/* .line 76 */
/* .local v2, "flywheelParam2":D */
(( org.json.JSONObject ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 78 */
} // .end local v2 # "flywheelParam2":D
} // :cond_9
final String v1 = "flywheelParam3"; // const-string v1, "flywheelParam3"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 79 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 80 */
/* .local v2, "flywheelParam3":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 82 */
} // .end local v2 # "flywheelParam3":I
} // :cond_a
final String v1 = "flingVelocityThreshold"; // const-string v1, "flingVelocityThreshold"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 83 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 84 */
/* .local v2, "flingVelocityThreshold":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 86 */
} // .end local v2 # "flingVelocityThreshold":I
} // :cond_b
final String v1 = "flingVelocityScaled"; // const-string v1, "flingVelocityScaled"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 87 */
(( org.json.JSONObject ) p0 ).getDouble ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v2 */
/* .line 88 */
/* .local v2, "flingVelocityScaled":D */
(( org.json.JSONObject ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
/* .line 90 */
} // .end local v2 # "flingVelocityScaled":D
} // :cond_c
final String v1 = "flingVelocityMaximum"; // const-string v1, "flingVelocityMaximum"
v2 = (( org.json.JSONObject ) p0 ).has ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_d
/* .line 91 */
v2 = (( org.json.JSONObject ) p0 ).getInt ( v1 ); // invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 92 */
/* .local v2, "flingVelocityMaximum":I */
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 94 */
} // .end local v2 # "flingVelocityMaximum":I
} // :cond_d
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String readLocalFile ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .line 98 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 99 */
/* .local v0, "stringBuilder":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 100 */
/* .local v1, "file":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 101 */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
/* invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 102 */
/* .local v2, "inputStream":Ljava/io/InputStream; */
/* const/16 v3, 0x400 */
try { // :try_start_1
/* new-array v3, v3, [B */
/* .line 104 */
/* .local v3, "buffer":[B */
} // :goto_0
v4 = (( java.io.InputStream ) v2 ).read ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I
/* move v5, v4 */
/* .local v5, "lenth":I */
int v6 = -1; // const/4 v6, -0x1
/* if-eq v4, v6, :cond_0 */
/* .line 105 */
/* new-instance v4, Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v4, v3, v6, v5}, Ljava/lang/String;-><init>([BII)V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 107 */
} // .end local v3 # "buffer":[B
} // .end local v5 # "lenth":I
} // :cond_0
try { // :try_start_2
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 109 */
} // .end local v2 # "inputStream":Ljava/io/InputStream;
/* .line 101 */
/* .restart local v2 # "inputStream":Ljava/io/InputStream; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.InputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/InputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "stringBuilder":Ljava/lang/StringBuilder;
} // .end local v1 # "file":Ljava/io/File;
} // .end local p0 # "filePath":Ljava/lang/String;
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 107 */
} // .end local v2 # "inputStream":Ljava/io/InputStream;
/* .restart local v0 # "stringBuilder":Ljava/lang/StringBuilder; */
/* .restart local v1 # "file":Ljava/io/File; */
/* .restart local p0 # "filePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 108 */
/* .local v2, "e":Ljava/io/IOException; */
v3 = com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils.TAG;
final String v4 = "exception when readLocalFile: "; // const-string v4, "exception when readLocalFile: "
android.util.Slog .e ( v3,v4,v2 );
/* .line 111 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
