.class final Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
.super Ljava/lang/Object;
.source "ScrollerOptimizationConfigProvider.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ScrollerOptimizationConfigChangedListenerRecord"
.end annotation


# instance fields
.field public mListener:Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;

.field public mPackageName:Ljava/lang/String;

.field public mPid:I

.field final synthetic this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;


# direct methods
.method constructor <init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;ILjava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V
    .locals 2
    .param p2, "pid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "listener"    # Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;

    .line 397
    iput-object p1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    iput p2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mPid:I

    .line 399
    iput-object p3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mPackageName:Ljava/lang/String;

    .line 400
    iput-object p4, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mListener:Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;

    .line 401
    if-nez p4, :cond_0

    .line 402
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "listener is null, packageName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    :cond_0
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 408
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    iget v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$monScrollerStateChangedListenerDied(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;I)V

    .line 409
    return-void
.end method
