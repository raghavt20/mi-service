.class public Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;
.super Ljava/lang/Object;
.source "ScrollerOptimizationConfigProviderUtils.java"


# static fields
.field public static final APP_LIST_NAME:Ljava/lang/String; = "appList"

.field private static final FLING_VELOCITY_MAXIMUM:Ljava/lang/String; = "flingVelocityMaximum"

.field private static final FLING_VELOCITY_SCALED:Ljava/lang/String; = "flingVelocityScaled"

.field private static final FLING_VELOCITY_THRESHOLD:Ljava/lang/String; = "flingVelocityThreshold"

.field private static final FLY_WHEEL:Ljava/lang/String; = "flywheel"

.field private static final FLY_WHEEL_PARAM_1:Ljava/lang/String; = "flywheelParam1"

.field private static final FLY_WHEEL_PARAM_2:Ljava/lang/String; = "flywheelParam2"

.field private static final FLY_WHEEL_PARAM_3:Ljava/lang/String; = "flywheelParam3"

.field private static final FLY_WHEEL_TIME_INTERVAL_THRESHOLD:Ljava/lang/String; = "flywheelTimeIntervalThreshold"

.field private static final FLY_WHEEL_VELOCITY_THRESHOLD_1:Ljava/lang/String; = "flywheelVelocityThreshold1"

.field private static final FLY_WHEEL_VELOCITY_THRESHOLD_2:Ljava/lang/String; = "flywheelVelocityThreshold2"

.field private static final FRICTION:Ljava/lang/String; = "friction"

.field private static final OPTIMIZE_ENABLE:Ljava/lang/String; = "isOptimizeEnable"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final TAG:Ljava/lang/String;

.field private static final VELOCITY_THRESHOLD:Ljava/lang/String; = "velocityThreshold"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const-class v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocalFileConfig(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "filePath"    # Ljava/lang/String;

    .line 115
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 116
    .local v0, "jsonObject":Lorg/json/JSONObject;
    invoke-static {p0}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->readLocalFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "configString":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 121
    goto :goto_0

    .line 119
    :catch_0
    move-exception v2

    .line 120
    .local v2, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->TAG:Ljava/lang/String;

    const-string v4, "exception when getLocalFileConfig: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method

.method public static parseGeneralConfig(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 4
    .param p0, "jsonAll"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 37
    invoke-virtual {p0}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    return-object v0

    .line 40
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 42
    .local v0, "jsonGeneralConfig":Lorg/json/JSONObject;
    const-string v1, "isOptimizeEnable"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 43
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 44
    .local v2, "isOptimizeEnable":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 46
    .end local v2    # "isOptimizeEnable":I
    :cond_1
    const-string v1, "friction"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 47
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 48
    .local v2, "friction":D
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 50
    .end local v2    # "friction":D
    :cond_2
    const-string/jumbo v1, "velocityThreshold"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 51
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 52
    .local v2, "velocityThreshold":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 54
    .end local v2    # "velocityThreshold":I
    :cond_3
    const-string v1, "flywheel"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 55
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 56
    .local v2, "flywheel":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 58
    .end local v2    # "flywheel":I
    :cond_4
    const-string v1, "flywheelTimeIntervalThreshold"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 59
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 60
    .local v2, "flywheelTimeIntervalThreshold":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 62
    .end local v2    # "flywheelTimeIntervalThreshold":I
    :cond_5
    const-string v1, "flywheelVelocityThreshold1"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 63
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 64
    .local v2, "flywheelVelocityThreshold1":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 66
    .end local v2    # "flywheelVelocityThreshold1":I
    :cond_6
    const-string v1, "flywheelVelocityThreshold2"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 67
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 68
    .local v2, "flywheelVelocityThreshold2":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 70
    .end local v2    # "flywheelVelocityThreshold2":I
    :cond_7
    const-string v1, "flywheelParam1"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 71
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 72
    .local v2, "flywheelParam1":D
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 74
    .end local v2    # "flywheelParam1":D
    :cond_8
    const-string v1, "flywheelParam2"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 75
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 76
    .local v2, "flywheelParam2":D
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 78
    .end local v2    # "flywheelParam2":D
    :cond_9
    const-string v1, "flywheelParam3"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 79
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 80
    .local v2, "flywheelParam3":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 82
    .end local v2    # "flywheelParam3":I
    :cond_a
    const-string v1, "flingVelocityThreshold"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 83
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 84
    .local v2, "flingVelocityThreshold":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 86
    .end local v2    # "flingVelocityThreshold":I
    :cond_b
    const-string v1, "flingVelocityScaled"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 87
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 88
    .local v2, "flingVelocityScaled":D
    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 90
    .end local v2    # "flingVelocityScaled":D
    :cond_c
    const-string v1, "flingVelocityMaximum"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 91
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 92
    .local v2, "flingVelocityMaximum":I
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 94
    .end local v2    # "flingVelocityMaximum":I
    :cond_d
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static readLocalFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    .local v2, "inputStream":Ljava/io/InputStream;
    const/16 v3, 0x400

    :try_start_1
    new-array v3, v3, [B

    .line 104
    .local v3, "buffer":[B
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    move v5, v4

    .local v5, "lenth":I
    const/4 v6, -0x1

    if-eq v4, v6, :cond_0

    .line 105
    new-instance v4, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v4, v3, v6, v5}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 107
    .end local v3    # "buffer":[B
    .end local v5    # "lenth":I
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 109
    .end local v2    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .line 101
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v1    # "file":Ljava/io/File;
    .end local p0    # "filePath":Ljava/lang/String;
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 107
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v0    # "stringBuilder":Ljava/lang/StringBuilder;
    .restart local v1    # "file":Ljava/io/File;
    .restart local p0    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 108
    .local v2, "e":Ljava/io/IOException;
    sget-object v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->TAG:Ljava/lang/String;

    const-string v4, "exception when readLocalFile: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 111
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
