.class Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;
.super Landroid/database/ContentObserver;
.source "ScrollerOptimizationConfigProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerPowerPerformanceModeChangeObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 117
    iput-object p1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    iput-object p3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .line 120
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    iget-object v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->val$context:Landroid/content/Context;

    .line 121
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    invoke-static {v2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$fgetmCurrentUserId(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;)I

    move-result v2

    .line 120
    const-string v3, "POWER_PERFORMANCE_MODE_OPEN"

    const/4 v4, 0x0

    invoke-static {v1, v3, v4, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move v4, v2

    :cond_0
    invoke-static {v0, v4}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$fputmPowerPerformanceMode(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Z)V

    .line 123
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPowerPerformanceModeChangeObserver mPowerPerformanceMode:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    invoke-static {v3}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$fgetmPowerPerformanceMode(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;->this$0:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    const/4 v1, -0x1

    invoke-static {v0, v2, v1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->-$$Nest$mcallScrollerOptimizationConfigListener(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;ZI)V

    .line 126
    return-void
.end method
