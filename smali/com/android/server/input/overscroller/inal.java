class inal implements android.os.IBinder$DeathRecipient {
	 /* .source "ScrollerOptimizationConfigProvider.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x12 */
/* name = "ScrollerOptimizationConfigChangedListenerRecord" */
} // .end annotation
/* # instance fields */
public miui.hardware.input.IAppScrollerOptimizationConfigChangedListener mListener;
public java.lang.String mPackageName;
public Integer mPid;
final com.android.server.input.overscroller.ScrollerOptimizationConfigProvider this$0; //synthetic
/* # direct methods */
 inal ( ) {
/* .locals 2 */
/* .param p2, "pid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "listener" # Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener; */
/* .line 397 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 398 */
/* iput p2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mPid:I */
/* .line 399 */
this.mPackageName = p3;
/* .line 400 */
this.mListener = p4;
/* .line 401 */
/* if-nez p4, :cond_0 */
/* .line 402 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$sfgetTAG ( );
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "listener is null, packageName:"; // const-string v1, "listener is null, packageName:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( p1,v0 );
/* .line 404 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 2 */
/* .line 408 */
v0 = this.this$0;
/* iget v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mPid:I */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$monScrollerStateChangedListenerDied ( v0,v1 );
/* .line 409 */
return;
} // .end method
