public class com.android.server.input.overscroller.ScrollerOptimizationConfigProvider {
	 /* .source "ScrollerOptimizationConfigProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static volatile com.android.server.input.overscroller.ScrollerOptimizationConfigProvider INSTANCE;
private static final java.lang.String POWER_PERFORMANCE_MODE;
private static final Integer SCROLLER_MODE_UN_KNOWN;
private static final Integer SCROLLER_NORMAL_MODE;
private static final java.lang.String SCROLLER_OPTIMIZATION_CONFIG_FILE_PATH;
private static final java.lang.String SCROLLER_OPTIMIZATION_MODULE_NAME;
private static final Integer SCROLLER_POWER_MODE;
private static final java.lang.String TAG;
private static java.util.HashMap mCloudAppScrollerOptimizationConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.lang.String mCloudGeneralScrollerOptimizationConfig;
private static java.util.HashMap mLocalAppScrollerOptimizationConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.lang.String mLocalGeneralScrollerOptimizationConfig;
/* # instance fields */
private Integer mCloudConfigVersion;
private Integer mCurrentUserId;
private android.os.Handler mHandler;
private Integer mLocalConfigVersion;
private Boolean mPowerPerformanceMode;
protected android.database.ContentObserver mPowerPerformanceModeChangeObserver;
private final java.lang.Object mScrollerListenerLock;
private final android.util.SparseArray mScrollerStateChangedListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$jFXUHcc3Q4fE8ONRWjV-HcVLGdE ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0, android.content.Context p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->lambda$systemBooted$0(Landroid/content/Context;)V */
return;
} // .end method
static Integer -$$Nest$fgetmCurrentUserId ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCurrentUserId:I */
} // .end method
static Boolean -$$Nest$fgetmPowerPerformanceMode ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
} // .end method
static void -$$Nest$fputmPowerPerformanceMode ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
return;
} // .end method
static void -$$Nest$mcallScrollerOptimizationConfigListener ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0, Boolean p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->callScrollerOptimizationConfigListener(ZI)V */
return;
} // .end method
static void -$$Nest$monScrollerStateChangedListenerDied ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->onScrollerStateChangedListenerDied(I)V */
return;
} // .end method
static void -$$Nest$mupdateCloudScrollerOptimizationConfig ( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->updateCloudScrollerOptimizationConfig(Landroid/content/Context;)V */
return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
} // .end method
static com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ( ) {
/* .locals 1 */
/* .line 36 */
/* const-class v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 54 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
private com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ( ) {
/* .locals 2 */
/* .line 67 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 63 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
/* .line 64 */
/* iput v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I */
/* .line 65 */
/* iput v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I */
/* .line 351 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mScrollerStateChangedListeners = v0;
/* .line 353 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mScrollerListenerLock = v0;
/* .line 68 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 69 */
/* new-instance v0, Landroid/os/HandlerThread; */
/* const-string/jumbo v1, "scrolleroptimizationthread" */
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 70 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 71 */
(( android.os.HandlerThread ) v0 ).getThreadHandler ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadHandler()Landroid/os/Handler;
this.mHandler = v1;
/* .line 73 */
} // .end local v0 # "handlerThread":Landroid/os/HandlerThread;
} // :cond_0
return;
} // .end method
private void callScrollerOptimizationConfigListener ( Boolean p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "isScrollerModeChanged" # Z */
/* .param p2, "dateChangedScrollerMode" # I */
/* .line 141 */
int v0 = 1; // const/4 v0, 0x1
/* if-nez p1, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* if-eq p2, v0, :cond_0 */
/* .line 143 */
return;
/* .line 145 */
} // :cond_0
/* if-nez p1, :cond_2 */
/* .line 146 */
/* iget-boolean v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-eq p2, v0, :cond_1 */
/* .line 148 */
return;
/* .line 149 */
} // :cond_1
/* if-nez v1, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 151 */
return;
/* .line 154 */
} // :cond_2
int v1 = 2; // const/4 v1, 0x2
/* new-array v1, v1, [Ljava/lang/String; */
/* .line 155 */
/* .local v1, "result":[Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_3
java.lang.String .valueOf ( v0 );
} // :cond_3
java.lang.String .valueOf ( v3 );
} // :goto_0
/* aput-object v2, v1, v3 */
/* .line 156 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
v3 = this.mScrollerStateChangedListeners;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_5 */
/* .line 157 */
v3 = this.mScrollerStateChangedListeners;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord; */
/* .line 158 */
/* .local v3, "scrollerOptimizationConfigChangedListenerRecord":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord; */
/* iget-boolean v4, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 160 */
int v4 = 0; // const/4 v4, 0x0
/* aput-object v4, v1, v0 */
/* .line 163 */
} // :cond_4
v4 = this.mPackageName;
/* invoke-direct {p0, v4}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getNormalModeAppScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v4, v1, v0 */
/* .line 166 */
} // :goto_2
try { // :try_start_0
v4 = this.mListener;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 169 */
/* .line 167 */
/* :catch_0 */
/* move-exception v4 */
/* .line 168 */
/* .local v4, "e":Ljava/lang/Exception; */
v5 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
(( java.lang.Exception ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 156 */
} // .end local v3 # "scrollerOptimizationConfigChangedListenerRecord":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 171 */
} // .end local v2 # "i":I
} // :cond_5
return;
} // .end method
private java.lang.String getCloudScrollerOptimizationConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 333 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudAppScrollerOptimizationConfig;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 334 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudAppScrollerOptimizationConfig;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 336 */
} // :cond_0
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudGeneralScrollerOptimizationConfig;
} // .end method
public static com.android.server.input.overscroller.ScrollerOptimizationConfigProvider getInstance ( ) {
/* .locals 2 */
/* .line 76 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.INSTANCE;
/* if-nez v0, :cond_1 */
/* .line 77 */
/* const-class v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider; */
/* monitor-enter v0 */
/* .line 78 */
try { // :try_start_0
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.INSTANCE;
/* if-nez v1, :cond_0 */
/* .line 79 */
/* new-instance v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider; */
/* invoke-direct {v1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;-><init>()V */
/* .line 81 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 83 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.INSTANCE;
} // .end method
private java.lang.String getLocalScrollerOptimizationConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 344 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalAppScrollerOptimizationConfig;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 345 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalAppScrollerOptimizationConfig;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 347 */
} // :cond_0
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalGeneralScrollerOptimizationConfig;
} // .end method
private java.lang.String getNormalModeAppScrollerOptimizationConfig ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 292 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_NORMAL_SUPPORT:Z */
/* if-nez v0, :cond_0 */
/* .line 293 */
int v0 = 0; // const/4 v0, 0x0
/* .line 295 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getLocalScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String; */
/* .line 296 */
/* .local v0, "config":Ljava/lang/String; */
/* iget v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I */
/* iget v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I */
/* if-le v1, v2, :cond_1 */
/* .line 297 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getCloudScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String; */
/* .line 298 */
/* .local v1, "cloudConfig":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
/* .line 299 */
/* move-object v0, v1 */
/* .line 302 */
} // .end local v1 # "cloudConfig":Ljava/lang/String;
} // :cond_1
} // .end method
private void lambda$systemBooted$0 ( android.content.Context p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 95 */
/* invoke-direct {p0}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->loadLocalScrollerOptimizationConfig()V */
/* .line 96 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->updateCloudScrollerOptimizationConfig(Landroid/content/Context;)V */
/* .line 97 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerDataObserver(Landroid/content/Context;)V */
/* .line 98 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_POWER_SUPPORT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 100 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerPowerPerformanceModeChangeObserver(Landroid/content/Context;)V */
/* .line 102 */
} // :cond_0
return;
} // .end method
private void loadLocalScrollerOptimizationConfig ( ) {
/* .locals 7 */
/* .line 249 */
final String v0 = "appList"; // const-string v0, "appList"
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
final String v2 = "load local scroller optimization config"; // const-string v2, "load local scroller optimization config"
android.util.Slog .i ( v1,v2 );
/* .line 251 */
try { // :try_start_0
/* const-string/jumbo v2, "system_ext/etc/overscrolleroptimization/config.json" */
/* .line 252 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils .getLocalFileConfig ( v2 );
/* .line 253 */
/* .local v2, "localJson":Lorg/json/JSONObject; */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v2, v3}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->preParseConfig(Lorg/json/JSONObject;Z)Lorg/json/JSONObject; */
/* move-object v2, v3 */
/* .line 254 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "local config: "; // const-string v4, "local config: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONObject ) v2 ).toString ( ); // invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 256 */
/* nop */
/* .line 257 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils .parseGeneralConfig ( v2 );
/* .line 259 */
v1 = (( org.json.JSONObject ) v2 ).has ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 260 */
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalAppScrollerOptimizationConfig;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 261 */
/* nop */
/* .line 262 */
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 263 */
/* .local v0, "appConfigArray":Lorg/json/JSONArray; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v3 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v3, :cond_0 */
/* .line 264 */
(( org.json.JSONArray ) v0 ).getJSONObject ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 265 */
/* .local v3, "appConfig":Lorg/json/JSONObject; */
final String v4 = "packageName"; // const-string v4, "packageName"
(( org.json.JSONObject ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 266 */
/* .local v4, "packageName":Ljava/lang/String; */
v5 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalAppScrollerOptimizationConfig;
(( org.json.JSONObject ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.HashMap ) v5 ).put ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 263 */
/* nop */
} // .end local v3 # "appConfig":Lorg/json/JSONObject;
} // .end local v4 # "packageName":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 271 */
} // .end local v0 # "appConfigArray":Lorg/json/JSONArray;
} // .end local v1 # "i":I
} // .end local v2 # "localJson":Lorg/json/JSONObject;
} // :cond_0
/* .line 269 */
/* :catch_0 */
/* move-exception v0 */
/* .line 270 */
/* .local v0, "e":Lorg/json/JSONException; */
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
final String v2 = "exception when loadLocalScrollerOptimizationConfig: "; // const-string v2, "exception when loadLocalScrollerOptimizationConfig: "
android.util.Slog .e ( v1,v2,v0 );
/* .line 272 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_1
return;
} // .end method
private void onScrollerStateChangedListenerDied ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 355 */
v0 = this.mScrollerListenerLock;
/* monitor-enter v0 */
/* .line 356 */
try { // :try_start_0
v1 = this.mScrollerStateChangedListeners;
(( android.util.SparseArray ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 357 */
/* monitor-exit v0 */
/* .line 358 */
return;
/* .line 357 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private org.json.JSONObject preParseConfig ( org.json.JSONObject p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .param p2, "isLocal" # Z */
/* .line 214 */
final String v0 = "common"; // const-string v0, "common"
/* const-string/jumbo v1, "version" */
v2 = android.os.Build.DEVICE;
/* .line 216 */
/* .local v2, "deviceName":Ljava/lang/String; */
try { // :try_start_0
v3 = (( org.json.JSONObject ) p1 ).has ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 217 */
v1 = (( org.json.JSONObject ) p1 ).getInt ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 218 */
/* .local v1, "version":I */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 219 */
/* iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I */
/* .line 220 */
v3 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "parse local device version : "; // const-string v5, "parse local device version : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 222 */
} // :cond_0
/* iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I */
/* .line 223 */
v3 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "parse cloud device version : "; // const-string v5, "parse cloud device version : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 225 */
} // .end local v1 # "version":I
} // :goto_0
/* .line 226 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 227 */
/* iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I */
/* .line 229 */
} // :cond_2
/* iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I */
/* .line 232 */
} // :goto_1
v1 = (( org.json.JSONObject ) p1 ).has ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 233 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "parse device config device name : "; // const-string v3, "parse device config device name : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 234 */
(( org.json.JSONObject ) p1 ).getJSONObject ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* move-object p1, v0 */
/* .line 235 */
} // :cond_3
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 236 */
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
final String v3 = "parse device config common"; // const-string v3, "parse device config common"
android.util.Slog .w ( v1,v3 );
/* .line 237 */
(( org.json.JSONObject ) p1 ).getJSONObject ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object p1, v0 */
/* .line 241 */
} // :cond_4
} // :goto_2
/* .line 239 */
/* :catch_0 */
/* move-exception v0 */
/* .line 240 */
/* .local v0, "e":Lorg/json/JSONException; */
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
final String v3 = "exception when preParseConfig: "; // const-string v3, "exception when preParseConfig: "
android.util.Slog .e ( v1,v3,v0 );
/* .line 242 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_3
} // .end method
private void registerDataObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 106 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 107 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$1; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$1;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 106 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 114 */
return;
} // .end method
private void registerPowerPerformanceModeChangeObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 117 */
/* new-instance v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V */
this.mPowerPerformanceModeChangeObserver = v0;
/* .line 129 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 130 */
final String v1 = "POWER_PERFORMANCE_MODE_OPEN"; // const-string v1, "POWER_PERFORMANCE_MODE_OPEN"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mPowerPerformanceModeChangeObserver;
/* .line 129 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 132 */
v0 = this.mPowerPerformanceModeChangeObserver;
int v1 = 1; // const/4 v1, 0x1
(( android.database.ContentObserver ) v0 ).onChange ( v1 ); // invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 133 */
return;
} // .end method
private void updateCloudScrollerOptimizationConfig ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 177 */
final String v0 = "appList"; // const-string v0, "appList"
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
/* const-string/jumbo v2, "update cloud scroller optimization config" */
android.util.Slog .i ( v1,v2 );
/* .line 178 */
/* nop */
/* .line 179 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "over_scroller_optimization_cloud_config"; // const-string v3, "over_scroller_optimization_cloud_config"
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v2,v3,v3,v4,v5 );
/* .line 181 */
/* .local v2, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v2 != null) { // if-eqz v2, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v2 ).json ( ); // invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 183 */
try { // :try_start_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v2 ).json ( ); // invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 184 */
/* .local v3, "cloudJson":Lorg/json/JSONObject; */
/* invoke-direct {p0, v3, v5}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->preParseConfig(Lorg/json/JSONObject;Z)Lorg/json/JSONObject; */
/* move-object v3, v4 */
/* .line 185 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "cloud config: "; // const-string v6, "cloud config: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( org.json.JSONObject ) v3 ).toString ( ); // invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 187 */
/* nop */
/* .line 188 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProviderUtils .parseGeneralConfig ( v3 );
/* .line 190 */
v1 = (( org.json.JSONObject ) v3 ).has ( v0 ); // invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 191 */
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudAppScrollerOptimizationConfig;
(( java.util.HashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->clear()V
/* .line 192 */
(( org.json.JSONObject ) v3 ).getJSONArray ( v0 ); // invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 193 */
/* .local v0, "appConfigArray":Lorg/json/JSONArray; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v4, :cond_0 */
/* .line 194 */
(( org.json.JSONArray ) v0 ).getJSONObject ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 195 */
/* .local v4, "appConfig":Lorg/json/JSONObject; */
final String v6 = "packageName"; // const-string v6, "packageName"
(( org.json.JSONObject ) v4 ).getString ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 196 */
/* .local v6, "packageName":Ljava/lang/String; */
v7 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudAppScrollerOptimizationConfig;
(( org.json.JSONObject ) v4 ).toString ( ); // invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.HashMap ) v7 ).put ( v6, v8 ); // invoke-virtual {v7, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 193 */
/* nop */
} // .end local v4 # "appConfig":Lorg/json/JSONObject;
} // .end local v6 # "packageName":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 199 */
} // .end local v0 # "appConfigArray":Lorg/json/JSONArray;
} // .end local v1 # "i":I
} // :cond_0
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudGeneralScrollerOptimizationConfig;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_2 */
/* .line 200 */
/* iget v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I */
/* iget v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I */
/* if-gt v0, v1, :cond_1 */
/* .line 201 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
final String v1 = "cloud version less than local version"; // const-string v1, "cloud version less than local version"
android.util.Slog .i ( v0,v1 );
/* .line 202 */
return;
/* .line 205 */
} // :cond_1
/* invoke-direct {p0, v5, v5}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->callScrollerOptimizationConfigListener(ZI)V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 209 */
} // .end local v3 # "cloudJson":Lorg/json/JSONObject;
} // :cond_2
/* .line 207 */
/* :catch_0 */
/* move-exception v0 */
/* .line 208 */
/* .local v0, "e":Lorg/json/JSONException; */
v1 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
final String v3 = "exception when updateCloudScrollerOptimizationConfig: "; // const-string v3, "exception when updateCloudScrollerOptimizationConfig: "
android.util.Slog .e ( v1,v3,v0 );
/* .line 211 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :cond_3
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 1 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 361 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 362 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 363 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 364 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z */
/* if-nez v0, :cond_0 */
/* .line 365 */
final String v0 = "device not support"; // const-string v0, "device not support"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 366 */
return;
/* .line 368 */
} // :cond_0
final String v0 = "localConfigVersion = "; // const-string v0, "localConfigVersion = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 369 */
/* iget v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 370 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 371 */
final String v0 = "cloudConfigVersion = "; // const-string v0, "cloudConfigVersion = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 372 */
/* iget v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 373 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 374 */
final String v0 = "cloudAppSize = "; // const-string v0, "cloudAppSize = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 375 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudAppScrollerOptimizationConfig;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 376 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 377 */
final String v0 = "localAppSize = "; // const-string v0, "localAppSize = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 378 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalAppScrollerOptimizationConfig;
v0 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 379 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 380 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudGeneralScrollerOptimizationConfig;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_1 */
/* .line 381 */
final String v0 = "cloudCommon = "; // const-string v0, "cloudCommon = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 382 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mCloudGeneralScrollerOptimizationConfig;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 383 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 385 */
} // :cond_1
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalGeneralScrollerOptimizationConfig;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_2 */
/* .line 386 */
final String v0 = "localCommon = "; // const-string v0, "localCommon = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 387 */
v0 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.mLocalGeneralScrollerOptimizationConfig;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 389 */
} // :cond_2
return;
} // .end method
public java.lang.String getAppScrollerOptimizationConfigAndSwitchState ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 275 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 276 */
/* .line 278 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [Ljava/lang/String; */
/* .line 279 */
/* .local v0, "result":[Ljava/lang/String; */
/* iget-boolean v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
java.lang.String .valueOf ( v4 );
} // :cond_1
java.lang.String .valueOf ( v3 );
} // :goto_0
/* aput-object v2, v0, v3 */
/* .line 280 */
/* iget-boolean v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 281 */
/* aput-object v1, v0, v4 */
/* .line 283 */
} // :cond_2
/* invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getNormalModeAppScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String; */
/* aput-object v1, v0, v4 */
/* .line 285 */
} // :goto_1
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "newUserId" # I */
/* .line 87 */
/* iput p1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCurrentUserId:I */
/* .line 88 */
return;
} // .end method
public void registerAppScrollerOptimizationConfigListener ( java.lang.String p0, miui.hardware.input.IAppScrollerOptimizationConfigChangedListener p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "listener" # Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener; */
/* .line 306 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z */
/* if-nez v0, :cond_0 */
/* .line 307 */
return;
/* .line 310 */
} // :cond_0
v0 = this.mScrollerListenerLock;
/* monitor-enter v0 */
/* .line 311 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 312 */
/* .local v1, "callingPid":I */
v2 = this.mScrollerStateChangedListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 313 */
v2 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
/* new-instance v3, Ljava/lang/SecurityException; */
final String v4 = "The calling process has already registered an AppScrollerOptimizationConfigChangedListener"; // const-string v4, "The calling process has already registered an AppScrollerOptimizationConfigChangedListener"
/* invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* .line 314 */
(( java.lang.SecurityException ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;
/* .line 313 */
android.util.Slog .e ( v2,v3 );
/* .line 316 */
} // :cond_1
/* new-instance v2, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord; */
/* invoke-direct {v2, p0, v1, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;ILjava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 319 */
/* .local v2, "record":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord; */
try { // :try_start_1
/* .line 320 */
/* .local v3, "binder":Landroid/os/IBinder; */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 323 */
} // .end local v3 # "binder":Landroid/os/IBinder;
/* .line 321 */
/* :catch_0 */
/* move-exception v3 */
/* .line 322 */
/* .local v3, "ex":Landroid/os/RemoteException; */
try { // :try_start_2
v4 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider.TAG;
/* new-instance v5, Ljava/lang/RuntimeException; */
/* invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
(( java.lang.RuntimeException ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 324 */
} // .end local v3 # "ex":Landroid/os/RemoteException;
} // :goto_0
v3 = this.mScrollerStateChangedListeners;
(( android.util.SparseArray ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 325 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
/* monitor-exit v0 */
/* .line 327 */
return;
/* .line 325 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void systemBooted ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 91 */
/* sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z */
/* if-nez v0, :cond_0 */
/* .line 92 */
return;
/* .line 94 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 103 */
return;
} // .end method
