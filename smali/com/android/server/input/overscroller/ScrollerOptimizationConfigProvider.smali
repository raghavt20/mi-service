.class public Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;
.super Ljava/lang/Object;
.source "ScrollerOptimizationConfigProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
    }
.end annotation


# static fields
.field private static volatile INSTANCE:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider; = null

.field private static final POWER_PERFORMANCE_MODE:Ljava/lang/String; = "POWER_PERFORMANCE_MODE_OPEN"

.field private static final SCROLLER_MODE_UN_KNOWN:I = -0x1

.field private static final SCROLLER_NORMAL_MODE:I = 0x0

.field private static final SCROLLER_OPTIMIZATION_CONFIG_FILE_PATH:Ljava/lang/String; = "system_ext/etc/overscrolleroptimization/config.json"

.field private static final SCROLLER_OPTIMIZATION_MODULE_NAME:Ljava/lang/String; = "over_scroller_optimization_cloud_config"

.field private static final SCROLLER_POWER_MODE:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mCloudGeneralScrollerOptimizationConfig:Ljava/lang/String;

.field private static mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mLocalGeneralScrollerOptimizationConfig:Ljava/lang/String;


# instance fields
.field private mCloudConfigVersion:I

.field private mCurrentUserId:I

.field private mHandler:Landroid/os/Handler;

.field private mLocalConfigVersion:I

.field private mPowerPerformanceMode:Z

.field protected mPowerPerformanceModeChangeObserver:Landroid/database/ContentObserver;

.field private final mScrollerListenerLock:Ljava/lang/Object;

.field private final mScrollerStateChangedListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$jFXUHcc3Q4fE8ONRWjV-HcVLGdE(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->lambda$systemBooted$0(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentUserId(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;)I
    .locals 0

    iget p0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCurrentUserId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerPerformanceMode(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmPowerPerformanceMode(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcallScrollerOptimizationConfigListener(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->callScrollerOptimizationConfigListener(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monScrollerStateChangedListenerDied(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->onScrollerStateChangedListenerDied(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudScrollerOptimizationConfig(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->updateCloudScrollerOptimizationConfig(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 36
    const-class v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->INSTANCE:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    .line 64
    iput v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I

    .line 65
    iput v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I

    .line 351
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerStateChangedListeners:Landroid/util/SparseArray;

    .line 353
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerListenerLock:Ljava/lang/Object;

    .line 68
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "scrolleroptimizationthread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 70
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 71
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadHandler()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mHandler:Landroid/os/Handler;

    .line 73
    .end local v0    # "handlerThread":Landroid/os/HandlerThread;
    :cond_0
    return-void
.end method

.method private callScrollerOptimizationConfigListener(ZI)V
    .locals 7
    .param p1, "isScrollerModeChanged"    # Z
    .param p2, "dateChangedScrollerMode"    # I

    .line 141
    const/4 v0, 0x1

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    if-eq p2, v0, :cond_0

    .line 143
    return-void

    .line 145
    :cond_0
    if-nez p1, :cond_2

    .line 146
    iget-boolean v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    if-eqz v1, :cond_1

    if-eq p2, v0, :cond_1

    .line 148
    return-void

    .line 149
    :cond_1
    if-nez v1, :cond_2

    if-eqz p2, :cond_2

    .line 151
    return-void

    .line 154
    :cond_2
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    .line 155
    .local v1, "result":[Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    aput-object v2, v1, v3

    .line 156
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerStateChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 157
    iget-object v3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerStateChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;

    .line 158
    .local v3, "scrollerOptimizationConfigChangedListenerRecord":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
    iget-boolean v4, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    if-eqz v4, :cond_4

    .line 160
    const/4 v4, 0x0

    aput-object v4, v1, v0

    goto :goto_2

    .line 163
    :cond_4
    iget-object v4, v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mPackageName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getNormalModeAppScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 166
    :goto_2
    :try_start_0
    iget-object v4, v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;->mListener:Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;

    invoke-interface {v4, v1}, Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;->onScrollerOptimizationConfig([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    goto :goto_3

    .line 167
    :catch_0
    move-exception v4

    .line 168
    .local v4, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    .end local v3    # "scrollerOptimizationConfigChangedListenerRecord":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 171
    .end local v2    # "i":I
    :cond_5
    return-void
.end method

.method private getCloudScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 333
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 336
    :cond_0
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudGeneralScrollerOptimizationConfig:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;
    .locals 2

    .line 76
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->INSTANCE:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    if-nez v0, :cond_1

    .line 77
    const-class v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    monitor-enter v0

    .line 78
    :try_start_0
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->INSTANCE:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    if-nez v1, :cond_0

    .line 79
    new-instance v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    invoke-direct {v1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;-><init>()V

    sput-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->INSTANCE:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    .line 81
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 83
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->INSTANCE:Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    return-object v0
.end method

.method private getLocalScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 344
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 347
    :cond_0
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalGeneralScrollerOptimizationConfig:Ljava/lang/String;

    return-object v0
.end method

.method private getNormalModeAppScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 292
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_NORMAL_SUPPORT:Z

    if-nez v0, :cond_0

    .line 293
    const/4 v0, 0x0

    return-object v0

    .line 295
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getLocalScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "config":Ljava/lang/String;
    iget v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I

    iget v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I

    if-le v1, v2, :cond_1

    .line 297
    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getCloudScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "cloudConfig":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 299
    move-object v0, v1

    .line 302
    .end local v1    # "cloudConfig":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private synthetic lambda$systemBooted$0(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 95
    invoke-direct {p0}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->loadLocalScrollerOptimizationConfig()V

    .line 96
    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->updateCloudScrollerOptimizationConfig(Landroid/content/Context;)V

    .line 97
    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerDataObserver(Landroid/content/Context;)V

    .line 98
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_POWER_SUPPORT:Z

    if-eqz v0, :cond_0

    .line 100
    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerPowerPerformanceModeChangeObserver(Landroid/content/Context;)V

    .line 102
    :cond_0
    return-void
.end method

.method private loadLocalScrollerOptimizationConfig()V
    .locals 7

    .line 249
    const-string v0, "appList"

    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string v2, "load local scroller optimization config"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :try_start_0
    const-string/jumbo v2, "system_ext/etc/overscrolleroptimization/config.json"

    .line 252
    invoke-static {v2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->getLocalFileConfig(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 253
    .local v2, "localJson":Lorg/json/JSONObject;
    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->preParseConfig(Lorg/json/JSONObject;Z)Lorg/json/JSONObject;

    move-result-object v3

    move-object v2, v3

    .line 254
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "local config: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    nop

    .line 257
    invoke-static {v2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->parseGeneralConfig(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalGeneralScrollerOptimizationConfig:Ljava/lang/String;

    .line 259
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 261
    nop

    .line 262
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 263
    .local v0, "appConfigArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 264
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 265
    .local v3, "appConfig":Lorg/json/JSONObject;
    const-string v4, "packageName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 266
    .local v4, "packageName":Ljava/lang/String;
    sget-object v5, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 263
    nop

    .end local v3    # "appConfig":Lorg/json/JSONObject;
    .end local v4    # "packageName":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 271
    .end local v0    # "appConfigArray":Lorg/json/JSONArray;
    .end local v1    # "i":I
    .end local v2    # "localJson":Lorg/json/JSONObject;
    :cond_0
    goto :goto_1

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Lorg/json/JSONException;
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string v2, "exception when loadLocalScrollerOptimizationConfig: "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 272
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method

.method private onScrollerStateChangedListenerDied(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 355
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerListenerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 356
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerStateChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 357
    monitor-exit v0

    .line 358
    return-void

    .line 357
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private preParseConfig(Lorg/json/JSONObject;Z)Lorg/json/JSONObject;
    .locals 6
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .param p2, "isLocal"    # Z

    .line 214
    const-string v0, "common"

    const-string/jumbo v1, "version"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 216
    .local v2, "deviceName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 217
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 218
    .local v1, "version":I
    if-eqz p2, :cond_0

    .line 219
    iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I

    .line 220
    sget-object v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parse local device version : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 222
    :cond_0
    iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I

    .line 223
    sget-object v3, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parse cloud device version : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    .end local v1    # "version":I
    :goto_0
    goto :goto_1

    .line 226
    :cond_1
    const/4 v1, 0x0

    if-eqz p2, :cond_2

    .line 227
    iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I

    goto :goto_1

    .line 229
    :cond_2
    iput v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I

    .line 232
    :goto_1
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 233
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parse device config device name : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object p1, v0

    goto :goto_2

    .line 235
    :cond_3
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 236
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string v3, "parse device config common"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v0

    .line 241
    :cond_4
    :goto_2
    goto :goto_3

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Lorg/json/JSONException;
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string v3, "exception when preParseConfig: "

    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 242
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    return-object p1
.end method

.method private registerDataObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 107
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$1;

    iget-object v3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$1;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V

    .line 106
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 114
    return-void
.end method

.method private registerPowerPerformanceModeChangeObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 117
    new-instance v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;

    iget-object v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$2;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceModeChangeObserver:Landroid/database/ContentObserver;

    .line 129
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 130
    const-string v1, "POWER_PERFORMANCE_MODE_OPEN"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceModeChangeObserver:Landroid/database/ContentObserver;

    .line 129
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 132
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceModeChangeObserver:Landroid/database/ContentObserver;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 133
    return-void
.end method

.method private updateCloudScrollerOptimizationConfig(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 177
    const-string v0, "appList"

    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "update cloud scroller optimization config"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    nop

    .line 179
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "over_scroller_optimization_cloud_config"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v3, v3, v4, v5}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v2

    .line 181
    .local v2, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 183
    :try_start_0
    invoke-virtual {v2}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v3

    .line 184
    .local v3, "cloudJson":Lorg/json/JSONObject;
    invoke-direct {p0, v3, v5}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->preParseConfig(Lorg/json/JSONObject;Z)Lorg/json/JSONObject;

    move-result-object v4

    move-object v3, v4

    .line 185
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cloud config: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    nop

    .line 188
    invoke-static {v3}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProviderUtils;->parseGeneralConfig(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudGeneralScrollerOptimizationConfig:Ljava/lang/String;

    .line 190
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 192
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 193
    .local v0, "appConfigArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 194
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 195
    .local v4, "appConfig":Lorg/json/JSONObject;
    const-string v6, "packageName"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 196
    .local v6, "packageName":Ljava/lang/String;
    sget-object v7, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    nop

    .end local v4    # "appConfig":Lorg/json/JSONObject;
    .end local v6    # "packageName":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    .end local v0    # "appConfigArray":Lorg/json/JSONArray;
    .end local v1    # "i":I
    :cond_0
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudGeneralScrollerOptimizationConfig:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 200
    iget v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I

    iget v1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I

    if-gt v0, v1, :cond_1

    .line 201
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string v1, "cloud version less than local version"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    return-void

    .line 205
    :cond_1
    invoke-direct {p0, v5, v5}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->callScrollerOptimizationConfigListener(ZI)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    .end local v3    # "cloudJson":Lorg/json/JSONObject;
    :cond_2
    goto :goto_1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Lorg/json/JSONException;
    sget-object v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    const-string v3, "exception when updateCloudScrollerOptimizationConfig: "

    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 361
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 362
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 364
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z

    if-nez v0, :cond_0

    .line 365
    const-string v0, "device not support"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 366
    return-void

    .line 368
    :cond_0
    const-string v0, "localConfigVersion = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 369
    iget v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalConfigVersion:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 370
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 371
    const-string v0, "cloudConfigVersion = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 372
    iget v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudConfigVersion:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 373
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 374
    const-string v0, "cloudAppSize = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 375
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 376
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 377
    const-string v0, "localAppSize = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 378
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalAppScrollerOptimizationConfig:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 379
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 380
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudGeneralScrollerOptimizationConfig:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    const-string v0, "cloudCommon = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 382
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCloudGeneralScrollerOptimizationConfig:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 385
    :cond_1
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalGeneralScrollerOptimizationConfig:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 386
    const-string v0, "localCommon = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 387
    sget-object v0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mLocalGeneralScrollerOptimizationConfig:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 389
    :cond_2
    return-void
.end method

.method public getAppScrollerOptimizationConfigAndSwitchState(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 275
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 276
    return-object v1

    .line 278
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 279
    .local v0, "result":[Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    aput-object v2, v0, v3

    .line 280
    iget-boolean v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mPowerPerformanceMode:Z

    if-eqz v2, :cond_2

    .line 281
    aput-object v1, v0, v4

    goto :goto_1

    .line 283
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getNormalModeAppScrollerOptimizationConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 285
    :goto_1
    return-object v0
.end method

.method public onUserSwitch(I)V
    .locals 0
    .param p1, "newUserId"    # I

    .line 87
    iput p1, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mCurrentUserId:I

    .line 88
    return-void
.end method

.method public registerAppScrollerOptimizationConfigListener(Ljava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;

    .line 306
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z

    if-nez v0, :cond_0

    .line 307
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerListenerLock:Ljava/lang/Object;

    monitor-enter v0

    .line 311
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 312
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerStateChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 313
    sget-object v2, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/SecurityException;

    const-string v4, "The calling process has already registered an AppScrollerOptimizationConfigChangedListener"

    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .line 314
    invoke-virtual {v3}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v3

    .line 313
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_1
    new-instance v2, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;

    invoke-direct {v2, p0, v1, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;ILjava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    .local v2, "record":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
    :try_start_1
    invoke-interface {p2}, Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 320
    .local v3, "binder":Landroid/os/IBinder;
    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 323
    .end local v3    # "binder":Landroid/os/IBinder;
    goto :goto_0

    .line 321
    :catch_0
    move-exception v3

    .line 322
    .local v3, "ex":Landroid/os/RemoteException;
    :try_start_2
    sget-object v4, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v5}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    .end local v3    # "ex":Landroid/os/RemoteException;
    :goto_0
    iget-object v3, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mScrollerStateChangedListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 325
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$ScrollerOptimizationConfigChangedListenerRecord;
    monitor-exit v0

    .line 327
    return-void

    .line 325
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public systemBooted(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 91
    sget-boolean v0, Lmiui/os/DeviceFeature;->SCROLLER_OPTIMIZATION_SUPPORT:Z

    if-nez v0, :cond_0

    .line 92
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 103
    return-void
.end method
