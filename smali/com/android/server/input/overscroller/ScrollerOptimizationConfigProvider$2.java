class com.android.server.input.overscroller.ScrollerOptimizationConfigProvider$2 extends android.database.ContentObserver {
	 /* .source "ScrollerOptimizationConfigProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerPowerPerformanceModeChangeObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.overscroller.ScrollerOptimizationConfigProvider this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.input.overscroller.ScrollerOptimizationConfigProvider$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 117 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 120 */
v0 = this.this$0;
v1 = this.val$context;
/* .line 121 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = this.this$0;
v2 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$fgetmCurrentUserId ( v2 );
/* .line 120 */
final String v3 = "POWER_PERFORMANCE_MODE_OPEN"; // const-string v3, "POWER_PERFORMANCE_MODE_OPEN"
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$System .getIntForUser ( v1,v3,v4,v2 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
/* move v4, v2 */
} // :cond_0
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$fputmPowerPerformanceMode ( v0,v4 );
/* .line 123 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mPowerPerformanceModeChangeObserver mPowerPerformanceMode:"; // const-string v3, "mPowerPerformanceModeChangeObserver mPowerPerformanceMode:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$fgetmPowerPerformanceMode ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 125 */
v0 = this.this$0;
int v1 = -1; // const/4 v1, -0x1
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .-$$Nest$mcallScrollerOptimizationConfigListener ( v0,v2,v1 );
/* .line 126 */
return;
} // .end method
