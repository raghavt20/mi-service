class com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack {
	 /* .source "InputOneTrackUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/InputOneTrackUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "InputDeviceOneTrack" */
} // .end annotation
/* # static fields */
static final java.lang.String TYPE_KEYBOARD;
static final java.lang.String TYPE_MOUSE;
static final java.lang.String TYPE_STYLUS;
static final java.lang.String TYPE_TOUCHPAD;
static final java.lang.String TYPE_UNKNOW;
/* # instance fields */
private java.util.Map mTrackMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mXiaomiDeviceMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.Map -$$Nest$fgetmTrackMap ( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTrackMap;
} // .end method
static java.util.Map -$$Nest$fgetmXiaomiDeviceMap ( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mXiaomiDeviceMap;
} // .end method
 com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack ( ) {
/* .locals 1 */
/* .line 374 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 380 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mTrackMap = v0;
/* .line 381 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mXiaomiDeviceMap = v0;
return;
} // .end method
/* # virtual methods */
public Boolean hasDuplicationValue ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 392 */
v0 = v0 = this.mTrackMap;
} // .end method
public Boolean hasDuplicationXiaomiDevice ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "vendorId" # I */
/* .line 396 */
v0 = this.mXiaomiDeviceMap;
v0 = java.lang.Integer .valueOf ( p1 );
} // .end method
public void putTrackValue ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "type" # Ljava/lang/String; */
/* .line 384 */
v0 = this.mTrackMap;
/* .line 385 */
return;
} // .end method
public void putXiaomiDeviceInfo ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "vendorId" # I */
/* .param p2, "type" # Ljava/lang/String; */
/* .line 388 */
v0 = this.mXiaomiDeviceMap;
java.lang.Integer .valueOf ( p1 );
/* .line 389 */
return;
} // .end method
