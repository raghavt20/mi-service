.class Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
.super Ljava/lang/Object;
.source "MiuiInputManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiuiInputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MotionEventListenerRecord"
.end annotation


# instance fields
.field private final mMotionEventListener:Lmiui/hardware/input/IMiuiMotionEventListener;

.field private final mPid:I

.field final synthetic this$0:Lcom/android/server/input/MiuiInputManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/input/MiuiInputManagerService;Lmiui/hardware/input/IMiuiMotionEventListener;I)V
    .locals 0
    .param p2, "motionEventListener"    # Lmiui/hardware/input/IMiuiMotionEventListener;
    .param p3, "pid"    # I

    .line 565
    iput-object p1, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566
    iput-object p2, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mMotionEventListener:Lmiui/hardware/input/IMiuiMotionEventListener;

    .line 567
    iput p3, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mPid:I

    .line 568
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 572
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    iget v1, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$mremoveMotionEventListener(Lcom/android/server/input/MiuiInputManagerService;I)V

    .line 573
    return-void
.end method

.method public notifyMotionEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 577
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mMotionEventListener:Lmiui/hardware/input/IMiuiMotionEventListener;

    invoke-interface {v0, p1}, Lmiui/hardware/input/IMiuiMotionEventListener;->onMiuiMotionEvent(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    goto :goto_0

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "ex":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to notify process "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , assuming it died."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiInputManagerService"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 580
    invoke-virtual {p0}, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->binderDied()V

    .line 582
    .end local v0    # "ex":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
