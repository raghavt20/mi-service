class com.android.server.input.shoulderkey.ShoulderKeyManagerService$LocalService implements com.android.server.input.shoulderkey.ShoulderKeyManagerInternal {
	 /* .source "ShoulderKeyManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "LocalService" */
} // .end annotation
/* # instance fields */
final com.android.server.input.shoulderkey.ShoulderKeyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.input.shoulderkey.ShoulderKeyManagerService$LocalService ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
/* .line 746 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void handleShoulderKeyEvent ( android.view.KeyEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 754 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$mhandleShoulderKeyEventInternal ( v0,p1 );
/* .line 755 */
return;
} // .end method
public void notifyTouchMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 769 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmHandler ( v0 );
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 770 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmHandler ( v1 );
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendMessage(Landroid/os/Message;)Z
/* .line 771 */
return;
} // .end method
public void onUserSwitch ( ) {
/* .locals 1 */
/* .line 775 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmSettingsObserver ( v0 );
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) v0 ).update ( ); // invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->update()V
/* .line 776 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmMiuiShoulderKeyShortcutListener ( v0 );
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener ) v0 ).updateSettings ( ); // invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->updateSettings()V
/* .line 777 */
return;
} // .end method
public void setShoulderKeySwitchStatus ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "position" # I */
/* .param p2, "isPopup" # Z */
/* .line 764 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$msetShoulderKeySwitchStatusInternal ( v0,p1,p2 );
/* .line 765 */
return;
} // .end method
public void systemReady ( ) {
/* .locals 1 */
/* .line 749 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$msystemReadyInternal ( v0 );
/* .line 750 */
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isScreenOn" # Z */
/* .line 759 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$mupdateScreenStateInternal ( v0,p1 );
/* .line 760 */
return;
} // .end method
