.class Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack;
.super Ljava/lang/Object;
.source "ShoulderKeyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShoulderKeyOneTrack"
.end annotation


# static fields
.field private static final EXTRA_APP_ID:Ljava/lang/String; = "31000000481"

.field private static final EXTRA_EVENT_NAME:Ljava/lang/String; = "shoulderkey"

.field private static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.shoulderkey"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final INTENT_ACTION_ONETRACK:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final INTENT_PACKAGE_ONETRACK:Ljava/lang/String; = "com.miui.analytics"

.field private static final KEY_ACTION:Ljava/lang/String; = "action"

.field private static final KEY_EVENT_TYPE:Ljava/lang/String; = "event_type"

.field private static final KEY_GAME_NAME:Ljava/lang/String; = "game_name"

.field private static final KEY_IS_GAMEBOOSTER:Ljava/lang/String; = "is_gamebooster"

.field private static final KEY_POSITION:Ljava/lang/String; = "position"

.field private static final TAG:Ljava/lang/String; = "ShoulderKeyOneTrack"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 855
    return-void
.end method

.method static synthetic lambda$reportShoulderKeyActionOneTrack$0(IIIZLjava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "eventType"    # I
    .param p1, "position"    # I
    .param p2, "action"    # I
    .param p3, "isGameMode"    # Z
    .param p4, "pkgLabel"    # Ljava/lang/String;
    .param p5, "context"    # Landroid/content/Context;

    .line 863
    const-string v0, "ShoulderKeyOneTrack"

    new-instance v1, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 864
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 865
    const-string v2, "APP_ID"

    const-string v3, "31000000481"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 866
    const-string v2, "EVENT_NAME"

    const-string/jumbo v3, "shoulderkey"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 867
    const-string v2, "PACKAGE"

    const-string v3, "com.xiaomi.shoulderkey"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 868
    const-string v2, "event_type"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 869
    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 870
    const-string v2, "action"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 871
    const-string v2, "is_gamebooster"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 872
    if-eqz p3, :cond_0

    .line 873
    const-string v2, "game_name"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 875
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 877
    :try_start_0
    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p5, v1, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 880
    :catch_0
    move-exception v2

    .line 881
    .local v2, "e":Ljava/lang/SecurityException;
    const-string v3, "Unable to start service."

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 878
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v2

    .line 879
    .local v2, "e":Ljava/lang/IllegalStateException;
    const-string v3, "Failed to upload ShoulderKey event."

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    nop

    .line 883
    :goto_1
    return-void
.end method

.method public static reportShoulderKeyActionOneTrack(Landroid/content/Context;IIIZLjava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventType"    # I
    .param p2, "position"    # I
    .param p3, "action"    # I
    .param p4, "isGameMode"    # Z
    .param p5, "pkgLabel"    # Ljava/lang/String;

    .line 859
    if-eqz p0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 862
    :cond_0
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v8, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack$$ExternalSyntheticLambda0;

    move-object v1, v8

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack$$ExternalSyntheticLambda0;-><init>(IIIZLjava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 884
    return-void

    .line 860
    :cond_1
    :goto_0
    return-void
.end method
