.class Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "ShoulderKeyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 782
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    .line 783
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 784
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 787
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 788
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 790
    const-string/jumbo v1, "shoulder_quick_star_gameturbo"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 792
    const-string/jumbo v1, "shoulderkey_sound_switch"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 794
    const-string/jumbo v1, "shoulderkey_sound_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 796
    invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->update()V

    .line 797
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChanged"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 808
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 809
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, -0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v2, :cond_1

    .line 810
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v5, :cond_0

    move v4, v5

    :cond_0
    invoke-static {v2, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmIsGameMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 812
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmIsGameMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 813
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 815
    :cond_1
    const-string/jumbo v1, "shoulder_quick_star_gameturbo"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 816
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1, v5, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v5, :cond_2

    move v4, v5

    :cond_2
    invoke-static {v2, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmBoosterSwitch(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    goto :goto_0

    .line 818
    :cond_3
    const-string/jumbo v1, "shoulderkey_sound_switch"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 819
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v5, :cond_4

    move v4, v5

    :cond_4
    invoke-static {v2, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmShoulderKeySoundSwitch(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 821
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mloadSoundResourceIfNeeded(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    goto :goto_0

    .line 822
    :cond_5
    const-string/jumbo v1, "shoulderkey_sound_type"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 823
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmShoulderKeySoundType(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)V

    .line 826
    :cond_6
    :goto_0
    return-void
.end method

.method update()V
    .locals 2

    .line 800
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 801
    const-string/jumbo v0, "shoulder_quick_star_gameturbo"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 802
    const-string/jumbo v0, "shoulderkey_sound_switch"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 803
    const-string/jumbo v0, "shoulderkey_sound_type"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 804
    return-void
.end method
