class com.android.server.input.shoulderkey.ShoulderKeyManagerService$H extends android.os.Handler {
	 /* .source "ShoulderKeyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final Integer MSG_DELIVER_TOUCH_MOTIONEVENT;
private static final Integer MSG_EXIT_GAMEMODE;
private static final Integer MSG_INJECT_EVENT_STATUS;
private static final Integer MSG_INJECT_MOTIONEVENT;
private static final Integer MSG_LOAD_LIFTKEYMAP;
private static final Integer MSG_RECORD_EVENT_STATUS;
private static final Integer MSG_RESET_SHOULDERKEY_TRIGGER_STATE;
private static final Integer MSG_UNLOAD_LIFTKEYMAP;
/* # instance fields */
final com.android.server.input.shoulderkey.ShoulderKeyManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 898 */
this.this$0 = p1;
/* .line 899 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 900 */
return;
} // .end method
private void updateInjectEventStatus ( ) {
/* .locals 2 */
/* .line 903 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmInjectEventPids ( v1 );
v1 = (( java.util.HashSet ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->size()I
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( com.android.server.input.config.InputCommonConfig ) v0 ).setInjectEventStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputCommonConfig;->setInjectEventStatus(Z)V
/* .line 904 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 905 */
return;
} // .end method
private void updateRecordEventStatus ( java.lang.Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Ljava/lang/Boolean; */
/* .line 908 */
v0 = this.this$0;
v1 = (( java.lang.Boolean ) p1 ).booleanValue ( ); // invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmRecordEventStatus ( v0,v1 );
/* .line 909 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
v1 = this.this$0;
v1 = com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmRecordEventStatus ( v1 );
(( com.android.server.input.config.InputCommonConfig ) v0 ).setRecordEventStatus ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputCommonConfig;->setRecordEventStatus(Z)V
/* .line 910 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 911 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 915 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "ShoulderKeyManager"; // const-string v2, "ShoulderKeyManager"
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 962 */
/* :pswitch_0 */
v0 = this.this$0;
v0 = com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 963 */
final String v0 = "EXIT GAMEMODE"; // const-string v0, "EXIT GAMEMODE"
android.util.Slog .d ( v2,v0 );
/* .line 965 */
} // :cond_0
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmInjectEventPids ( v0 );
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 966 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V */
/* .line 967 */
java.lang.Boolean .valueOf ( v1 );
/* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateRecordEventStatus(Ljava/lang/Boolean;)V */
/* .line 968 */
/* goto/16 :goto_0 */
/* .line 975 */
/* :pswitch_1 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Boolean; */
/* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateRecordEventStatus(Ljava/lang/Boolean;)V */
/* .line 976 */
/* goto/16 :goto_0 */
/* .line 971 */
/* :pswitch_2 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V */
/* .line 972 */
/* goto/16 :goto_0 */
/* .line 951 */
/* :pswitch_3 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmLeftShoulderKeySwitchTriggered ( v0,v1 );
/* .line 952 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmRightShoulderKeySwitchTriggered ( v0,v1 );
/* .line 953 */
/* goto/16 :goto_0 */
/* .line 956 */
/* :pswitch_4 */
v0 = this.obj;
/* check-cast v0, Landroid/view/MotionEvent; */
/* .line 957 */
/* .local v0, "event":Landroid/view/MotionEvent; */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$mtransformMotionEventForDeliver ( v1,v0 );
/* .line 958 */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$mdeliverTouchMotionEvent ( v1,v0 );
/* .line 959 */
/* goto/16 :goto_0 */
/* .line 936 */
} // .end local v0 # "event":Landroid/view/MotionEvent;
/* :pswitch_5 */
v0 = this.obj;
/* check-cast v0, Landroid/view/MotionEvent; */
/* .line 937 */
/* .restart local v0 # "event":Landroid/view/MotionEvent; */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$mtransformMotionEventForInjection ( v1,v0 );
/* .line 938 */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmMiuiInputManagerInternal ( v1 );
/* iget v3, p1, Landroid/os/Message;->arg1:I */
(( com.android.server.input.MiuiInputManagerInternal ) v1 ).injectMotionEvent ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Lcom/android/server/input/MiuiInputManagerInternal;->injectMotionEvent(Landroid/view/MotionEvent;I)V
/* .line 939 */
v1 = this.this$0;
v1 = com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetDEBUG ( v1 );
final String v3 = " "; // const-string v3, " "
final String v4 = " inject motionEvent "; // const-string v4, " inject motionEvent "
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 940 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v5, p1, Landroid/os/Message;->arg2:I */
com.android.server.am.ProcessUtils .getProcessNameByPid ( v5 );
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p1, Landroid/os/Message;->arg1:I */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 942 */
(( android.view.MotionEvent ) v0 ).toString ( ); // invoke-virtual {v0}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 940 */
android.util.Slog .d ( v2,v1 );
/* goto/16 :goto_0 */
/* .line 943 */
} // :cond_1
v1 = (( android.view.MotionEvent ) v0 ).getActionMasked ( ); // invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I
int v5 = 2; // const/4 v5, 0x2
/* if-eq v1, v5, :cond_3 */
/* .line 944 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v5, p1, Landroid/os/Message;->arg2:I */
com.android.server.am.ProcessUtils .getProcessNameByPid ( v5 );
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p1, Landroid/os/Message;->arg1:I */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 946 */
v3 = (( android.view.MotionEvent ) v0 ).getAction ( ); // invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I
android.view.MotionEvent .actionToString ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 944 */
android.util.Slog .d ( v2,v1 );
/* .line 930 */
} // .end local v0 # "event":Landroid/view/MotionEvent;
/* :pswitch_6 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmInjectEventPids ( v0 );
v1 = android.os.Process .myPid ( );
java.lang.Integer .valueOf ( v1 );
(( java.util.HashSet ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 931 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V */
/* .line 932 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmLifeKeyMapper ( v0 );
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 933 */
/* .line 917 */
/* :pswitch_7 */
v0 = this.obj;
/* check-cast v0, Ljava/util/Map; */
/* .line 918 */
/* .local v0, "mapper":Ljava/util/Map; */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmInjectEventPids ( v1 );
v3 = android.os.Process .myPid ( );
java.lang.Integer .valueOf ( v3 );
(( java.util.HashSet ) v1 ).add ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 919 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V */
/* .line 920 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 921 */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmLifeKeyMapper ( v1 );
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 922 */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmLifeKeyMapper ( v1 );
(( android.util.ArrayMap ) v1 ).putAll ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->putAll(Ljava/util/Map;)V
/* .line 923 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "loadLiftKeyMap "; // const-string v3, "loadLiftKeyMap "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmLifeKeyMapper ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 925 */
} // :cond_2
final String v1 = "loadLiftKeyMap : null"; // const-string v1, "loadLiftKeyMap : null"
android.util.Slog .d ( v2,v1 );
/* .line 927 */
/* nop */
/* .line 981 */
} // .end local v0 # "mapper":Ljava/util/Map;
} // :cond_3
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
