.class public Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.super Lmiui/hardware/shoulderkey/IShoulderKeyManager$Stub;
.source "ShoulderKeyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;,
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;,
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;,
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;,
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack;,
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;,
        Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final GAMEBOOSTER_DEBOUNCE_DELAY_MILLS:J = 0x96L

.field private static final GAME_BOOSTER_SWITCH:Ljava/lang/String; = "shoulder_quick_star_gameturbo"

.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final NONUI_SENSOR_ID:I = 0x1fa2653

.field public static final SHOULDEKEY_SOUND_TYPE:Ljava/lang/String; = "shoulderkey_sound_type"

.field private static final SHOULDERKEY_POSITION_LEFT:I = 0x0

.field private static final SHOULDERKEY_POSITION_RIGHT:I = 0x1

.field public static final SHOULDERKEY_SOUND_SWITCH:Ljava/lang/String; = "shoulderkey_sound_switch"

.field private static final TAG:Ljava/lang/String; = "ShoulderKeyManager"


# instance fields
.field private DEBUG:Z

.field private mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mBoosterSwitch:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentForegroundAppLabel:Ljava/lang/String;

.field private mCurrentForegroundPkg:Ljava/lang/String;

.field private mDisplayInfo:Landroid/view/DisplayInfo;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private mDownTime:J

.field private mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private final mInjcetEeventPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInjectEventPids:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsGameMode:Z

.field private mIsPocketMode:Z

.field private mIsScreenOn:Z

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLeftShoulderKeySwitchStatus:Z

.field private mLeftShoulderKeySwitchTriggered:Z

.field private mLifeKeyMapper:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Lmiui/hardware/shoulderkey/ShoulderKey;",
            "Lmiui/hardware/shoulderkey/ShoulderKeyMap;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalService:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;

.field private final mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private mMiuiShoulderKeyShortcutListener:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

.field private mNonUIListener:Landroid/hardware/SensorEventListener;

.field private mNonUISensor:Landroid/hardware/Sensor;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mRecordEventStatus:Z

.field private mRegisteredNonUI:Z

.field private mRightShoulderKeySwitchStatus:Z

.field private mRightShoulderKeySwitchTriggered:Z

.field private mSettingsObserver:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;

.field private mShoulderKeySoundSwitch:Z

.field private mShoulderKeySoundType:Ljava/lang/String;

.field private mSm:Landroid/hardware/SensorManager;

.field private mSupportShoulderKey:Z

.field private mTaskStackListener:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;

.field private final mTempTouchMotionEventListenersToNotify:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mTouchMotionEventListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mTouchMotionEventLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetDEBUG(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmActivityTaskManager(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/app/IActivityTaskManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentForegroundPkg(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mCurrentForegroundPkg:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayManagerInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/hardware/display/DisplayManagerInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInjectEventPids(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjectEventPids:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsGameMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsPocketMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLifeKeyMapper(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/util/ArrayMap;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLifeKeyMapper:Landroid/util/ArrayMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiInputManagerInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/MiuiInputManagerInternal;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiShoulderKeyShortcutListener(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mMiuiShoulderKeyShortcutListener:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRecordEventStatus(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSettingsObserver(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSettingsObserver:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmBoosterSwitch(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mBoosterSwitch:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentForegroundAppLabel(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mCurrentForegroundAppLabel:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentForegroundPkg(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mCurrentForegroundPkg:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDisplayInfo(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/DisplayInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsGameMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsPocketMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftShoulderKeySwitchTriggered(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRecordEventStatus(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightShoulderKeySwitchTriggered(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmShoulderKeySoundSwitch(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmShoulderKeySoundType(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundType:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mdeliverTouchMotionEvent(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->deliverTouchMotionEvent(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetAppLabelByPkgName(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->getAppLabelByPkgName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhandleShoulderKeyEventInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/KeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->handleShoulderKeyEventInternal(Landroid/view/KeyEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mloadSoundResourceIfNeeded(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->loadSoundResourceIfNeeded()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyForegroundAppChanged(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->notifyForegroundAppChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monTouchMotionEventListenerDied(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->onTouchMotionEventListenerDied(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetShoulderKeySwitchStatusInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msystemReadyInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->systemReadyInternal()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtransformMotionEventForDeliver(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->transformMotionEventForDeliver(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtransformMotionEventForInjection(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->transformMotionEventForInjection(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateScreenStateInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->updateScreenStateInternal(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 154
    invoke-direct {p0}, Lmiui/hardware/shoulderkey/IShoulderKeyManager$Stub;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    .line 104
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjectEventPids:Ljava/util/HashSet;

    .line 107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjcetEeventPackages:Ljava/util/HashSet;

    .line 119
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$1;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 548
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;

    invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mNonUIListener:Landroid/hardware/SensorEventListener;

    .line 648
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventLock:Ljava/lang/Object;

    .line 650
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    .line 652
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTempTouchMotionEventListenersToNotify:Ljava/util/List;

    .line 155
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    .line 156
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "shoulderKey"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 157
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 158
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    .line 159
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;

    invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLocalService:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;

    .line 160
    const-class v1, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    invoke-static {v1, v0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 161
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSettingsObserver:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;

    .line 162
    invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->observe()V

    .line 163
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;

    invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTaskStackListener:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;

    .line 164
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLifeKeyMapper:Landroid/util/ArrayMap;

    .line 165
    sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_SHOULDERKEY:Z

    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z

    .line 166
    new-instance v0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mMiuiShoulderKeyShortcutListener:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 167
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 168
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->init()V

    .line 169
    return-void
.end method

.method private checkInjectEventsPermission()Z
    .locals 3

    .line 282
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 283
    .local v0, "pid":I
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjcetEeventPackages:Ljava/util/HashSet;

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private deliverTouchMotionEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 728
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 729
    const-string v0, "ShoulderKeyManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deliverTouchMotionEvent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTempTouchMotionEventListenersToNotify:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 733
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 734
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 735
    .local v1, "numListeners":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 736
    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTempTouchMotionEventListenersToNotify:Ljava/util/List;

    iget-object v4, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    .line 737
    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;

    .line 736
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 735
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 739
    .end local v2    # "i":I
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 740
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 741
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTempTouchMotionEventListenersToNotify:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;

    invoke-virtual {v2, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->notifyTouchMotionEvent(Landroid/view/MotionEvent;)V

    .line 740
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 744
    .end local v0    # "i":I
    :cond_2
    return-void

    .line 739
    .end local v1    # "numListeners":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private getAppLabelByPkgName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 634
    const-string v0, ""

    .line 635
    .local v0, "label":Ljava/lang/String;
    const/4 v1, 0x0

    .line 637
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v3, 0x40

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 640
    goto :goto_0

    .line 638
    :catch_0
    move-exception v2

    .line 639
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 641
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    if-eqz v1, :cond_0

    .line 642
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 644
    :cond_0
    return-object v0
.end method

.method private handleShoulderKeyEventInternal(Landroid/view/KeyEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 438
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->isShoulderKeyCanShortCut(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mMiuiShoulderKeyShortcutListener:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-virtual {v0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->handleShoulderKeyShortcut(Landroid/view/KeyEvent;)V

    .line 441
    :cond_0
    const/4 v0, -0x1

    .line 442
    .local v0, "position":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x83

    if-ne v1, v2, :cond_1

    .line 443
    const/4 v0, 0x0

    goto :goto_0

    .line 444
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x84

    if-ne v1, v2, :cond_2

    .line 445
    const/4 v0, 0x1

    .line 448
    :cond_2
    :goto_0
    const/4 v1, -0x1

    const/4 v2, 0x1

    if-eq v0, v1, :cond_3

    .line 449
    nop

    .line 450
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    .line 449
    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->sendShoulderKeyEventBroadcast(III)V

    .line 451
    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLifeKeyMapper:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 452
    invoke-direct {p0, p1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->handleShoulderKeyToMotionEvent(Landroid/view/KeyEvent;I)V

    .line 457
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_7

    .line 458
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v3, 0x85

    const/4 v4, 0x0

    if-ne v1, v3, :cond_4

    .line 459
    invoke-direct {p0, v4, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V

    goto :goto_1

    .line 460
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v3, 0x86

    if-ne v1, v3, :cond_5

    .line 461
    invoke-direct {p0, v4, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V

    goto :goto_1

    .line 462
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v3, 0x87

    if-ne v1, v3, :cond_6

    .line 463
    invoke-direct {p0, v2, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V

    goto :goto_1

    .line 464
    :cond_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v3, 0x88

    if-ne v1, v3, :cond_7

    .line 465
    invoke-direct {p0, v2, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V

    .line 468
    :cond_7
    :goto_1
    return-void
.end method

.method private handleShoulderKeyToMotionEvent(Landroid/view/KeyEvent;I)V
    .locals 23
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "position"    # I

    .line 306
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v10

    .line 307
    .local v10, "action":I
    if-eqz v10, :cond_0

    const/4 v2, 0x1

    if-eq v10, v2, :cond_0

    .line 308
    return-void

    .line 310
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getProductId()I

    move-result v11

    .line 311
    .local v11, "productId":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v12

    .line 312
    .local v12, "keycode":I
    const/4 v2, 0x0

    .line 313
    .local v2, "keymap":Lmiui/hardware/shoulderkey/ShoulderKeyMap;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLifeKeyMapper:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 314
    iget-object v4, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLifeKeyMapper:Landroid/util/ArrayMap;

    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/hardware/shoulderkey/ShoulderKey;

    invoke-virtual {v4, v11, v12}, Lmiui/hardware/shoulderkey/ShoulderKey;->equals(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 315
    iget-object v4, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLifeKeyMapper:Landroid/util/ArrayMap;

    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    move-object v2, v4

    check-cast v2, Lmiui/hardware/shoulderkey/ShoulderKeyMap;

    .line 316
    move-object v13, v2

    goto :goto_1

    .line 313
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move-object v13, v2

    .line 319
    .end local v2    # "keymap":Lmiui/hardware/shoulderkey/ShoulderKeyMap;
    .end local v3    # "i":I
    .local v13, "keymap":Lmiui/hardware/shoulderkey/ShoulderKeyMap;
    :goto_1
    if-nez v13, :cond_3

    .line 320
    return-void

    .line 323
    :cond_3
    if-nez v10, :cond_4

    .line 324
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J

    .line 326
    :cond_4
    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->isIsSeparateMapping()Z

    move-result v2

    const/4 v14, 0x2

    if-eqz v2, :cond_6

    .line 328
    if-nez v10, :cond_5

    .line 329
    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getDownCenterX()F

    move-result v2

    .line 330
    .local v2, "centerX":F
    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getDownCenterY()F

    move-result v3

    .local v3, "centerY":F
    goto :goto_2

    .line 332
    .end local v2    # "centerX":F
    .end local v3    # "centerY":F
    :cond_5
    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getUpCenterX()F

    move-result v2

    .line 333
    .restart local v2    # "centerX":F
    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getUpCenterY()F

    move-result v3

    .line 336
    .restart local v3    # "centerY":F
    :goto_2
    iget-wide v4, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J

    .line 337
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v17

    const/16 v19, 0x0

    const/16 v22, 0x0

    .line 336
    move-wide v15, v4

    move/from16 v20, v2

    move/from16 v21, v3

    invoke-static/range {v15 .. v22}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v4

    .line 339
    .local v4, "downEvent":Landroid/view/MotionEvent;
    iget-object v5, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    .line 340
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v6

    .line 339
    invoke-virtual {v5, v14, v1, v6, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 340
    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    .line 342
    iget-wide v5, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J

    .line 343
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v17

    const/16 v19, 0x1

    .line 342
    move-wide v15, v5

    invoke-static/range {v15 .. v22}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v5

    .line 345
    .local v5, "upEvent":Landroid/view/MotionEvent;
    iget-object v6, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    .line 346
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v7

    .line 345
    invoke-virtual {v6, v14, v1, v7, v5}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 346
    invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V

    .line 347
    .end local v2    # "centerX":F
    .end local v3    # "centerY":F
    .end local v4    # "downEvent":Landroid/view/MotionEvent;
    .end local v5    # "upEvent":Landroid/view/MotionEvent;
    goto :goto_3

    .line 348
    :cond_6
    iget-wide v2, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J

    .line 349
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 350
    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getCenterX()F

    move-result v7

    invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getCenterY()F

    move-result v8

    const/4 v9, 0x0

    .line 348
    move v6, v10

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v2

    .line 351
    .local v2, "motionEvent":Landroid/view/MotionEvent;
    iget-object v3, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    .line 352
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    .line 351
    invoke-virtual {v3, v14, v1, v4, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 352
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 354
    .end local v2    # "motionEvent":Landroid/view/MotionEvent;
    :goto_3
    return-void
.end method

.method private init()V
    .locals 2

    .line 172
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 173
    nop

    .line 174
    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->getShoulderKeySwitchStatus(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z

    .line 175
    nop

    .line 176
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->getShoulderKeySwitchStatus(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjectEventPids:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 179
    iput-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z

    .line 180
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjcetEeventPackages:Ljava/util/HashSet;

    const-string v1, "com.xiaomi.macro"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjcetEeventPackages:Ljava/util/HashSet;

    const-string v1, "com.xiaomi.migameservice"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 182
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjcetEeventPackages:Ljava/util/HashSet;

    const-string v1, "com.xiaomi.joyose"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 183
    return-void
.end method

.method private interceptGameBooster()V
    .locals 2

    .line 515
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    .line 516
    .local v0, "isKeyguardShown":Z
    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mBoosterSwitch:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_0

    goto :goto_0

    .line 520
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchTriggered:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchTriggered:Z

    if-eqz v1, :cond_1

    .line 521
    invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->isUserSetupComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 522
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->launchGameBooster()V

    .line 524
    :cond_1
    return-void

    .line 517
    :cond_2
    :goto_0
    return-void
.end method

.method private launchGameBooster()V
    .locals 3

    .line 569
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.gamebooster.action.ACCESS_MAINACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 570
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "jump_target"

    const-string v2, "gamebox"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 571
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 572
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 573
    return-void
.end method

.method private loadSoundResourceIfNeeded()V
    .locals 1

    .line 592
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->loadSoundResource(Landroid/content/Context;)V

    goto :goto_0

    .line 595
    :cond_0
    invoke-static {}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->releaseSoundResource()V

    .line 597
    :goto_0
    return-void
.end method

.method private notifyForegroundAppChanged()V
    .locals 2

    .line 609
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    new-instance v1, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;

    invoke-direct {v1, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 631
    return-void
.end method

.method private onTouchMotionEventListenerDied(I)V
    .locals 4
    .param p1, "pid"    # I

    .line 719
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 720
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 721
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    .line 722
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 721
    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 722
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 723
    monitor-exit v0

    .line 724
    return-void

    .line 723
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private playSoundIfNeeded(Ljava/lang/String;)V
    .locals 1
    .param p1, "soundId"    # Ljava/lang/String;

    .line 509
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z

    if-eqz v0, :cond_0

    .line 510
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->playSound(Ljava/lang/String;Z)V

    .line 512
    :cond_0
    return-void
.end method

.method private registerForegroundAppUpdater()V
    .locals 3

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTaskStackListener:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to register foreground app updater: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ShoulderKeyManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private registerNonUIListener()V
    .locals 6

    .line 527
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z

    if-eqz v0, :cond_0

    return-void

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSm:Landroid/hardware/SensorManager;

    const-string v1, "ShoulderKeyManager"

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mNonUISensor:Landroid/hardware/Sensor;

    if-eqz v2, :cond_1

    .line 529
    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mNonUIListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 531
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z

    .line 532
    const-string v0, " register NonUISensorListener"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 534
    :cond_1
    const-string v0, " mNonUISensor is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :goto_0
    return-void
.end method

.method private sendShoulderKeyEventBroadcast(III)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "position"    # I
    .param p3, "action"    # I

    .line 581
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    iget-boolean v4, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    iget-object v5, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mCurrentForegroundAppLabel:Ljava/lang/String;

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v0 .. v5}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack;->reportShoulderKeyActionOneTrack(Landroid/content/Context;IIIZLjava/lang/String;)V

    .line 584
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.shoulderkey"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 585
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 586
    const-string v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 587
    const-string v1, "action"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 588
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 589
    return-void
.end method

.method private setShoulderKeySwitchStatusInternal(IZ)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "isPopup"    # Z

    .line 482
    if-nez p1, :cond_0

    .line 483
    iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z

    .line 484
    iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchTriggered:Z

    goto :goto_0

    .line 485
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 486
    iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z

    .line 487
    iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchTriggered:Z

    .line 492
    :goto_0
    if-eqz p2, :cond_1

    .line 493
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    const/4 v1, 0x4

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendEmptyMessageDelayed(IJ)Z

    .line 496
    :cond_1
    move v0, p2

    .line 497
    .local v0, "action":I
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->sendShoulderKeyEventBroadcast(III)V

    .line 498
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->interceptGameBooster()V

    .line 499
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->playSoundIfNeeded(Ljava/lang/String;)V

    .line 501
    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z

    if-eqz v1, :cond_3

    :cond_2
    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z

    if-nez v1, :cond_3

    .line 502
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->registerNonUIListener()V

    goto :goto_1

    .line 504
    :cond_3
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->unregisterNonUIListener()V

    .line 506
    :goto_1
    return-void

    .line 489
    .end local v0    # "action":I
    :cond_4
    return-void
.end method

.method private systemReadyInternal()V
    .locals 3

    .line 186
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 187
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 188
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 189
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->registerForegroundAppUpdater()V

    .line 190
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSm:Landroid/hardware/SensorManager;

    .line 192
    const v1, 0x1fa2653

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mNonUISensor:Landroid/hardware/Sensor;

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    const-class v1, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 195
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 196
    const-class v0, Landroid/hardware/display/DisplayManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 197
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    .line 199
    return-void
.end method

.method private transformMotionEventForDeliver(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 398
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    const-string v1, "ShoulderKeyManager"

    if-eqz v0, :cond_0

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "before transform for deliver: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v0, v0, Landroid/view/DisplayInfo;->rotation:I

    .line 402
    .local v0, "rotation":I
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    .line 403
    .local v2, "width":I
    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v3, v3, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 405
    .local v3, "height":I
    iget-object v4, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    invoke-virtual {v4}, Landroid/view/DisplayInfo;->getMode()Landroid/view/Display$Mode;

    move-result-object v4

    .line 406
    .local v4, "mode":Landroid/view/Display$Mode;
    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalWidth()I

    move-result v5

    .line 407
    .local v5, "physicalWidth":I
    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v6

    .line 408
    .local v6, "physicalHeight":I
    const/high16 v7, 0x3f800000    # 1.0f

    .line 409
    .local v7, "ratio":F
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 410
    .local v8, "matrix":Landroid/graphics/Matrix;
    const/high16 v9, 0x3f800000    # 1.0f

    packed-switch v0, :pswitch_data_0

    .line 427
    if-eqz v5, :cond_4

    int-to-float v9, v2

    int-to-float v10, v5

    div-float/2addr v9, v10

    goto :goto_0

    .line 422
    :pswitch_0
    if-eqz v5, :cond_1

    int-to-float v9, v3

    int-to-float v10, v5

    div-float/2addr v9, v10

    :cond_1
    move v7, v9

    .line 423
    const/4 v9, 0x3

    invoke-static {v9, v6, v5}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v8

    .line 425
    goto :goto_1

    .line 417
    :pswitch_1
    if-eqz v5, :cond_2

    int-to-float v9, v2

    int-to-float v10, v5

    div-float/2addr v9, v10

    :cond_2
    move v7, v9

    .line 418
    const/4 v9, 0x2

    invoke-static {v9, v5, v6}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v8

    .line 420
    goto :goto_1

    .line 412
    :pswitch_2
    if-eqz v5, :cond_3

    int-to-float v9, v3

    int-to-float v10, v5

    div-float/2addr v9, v10

    :cond_3
    move v7, v9

    .line 413
    const/4 v9, 0x1

    invoke-static {v9, v6, v5}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v8

    .line 415
    goto :goto_1

    .line 427
    :cond_4
    :goto_0
    move v7, v9

    .line 430
    :goto_1
    invoke-virtual {v8, v7, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 431
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V

    .line 432
    iget-boolean v9, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    if-eqz v9, :cond_5

    .line 433
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "after transform for deliver: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private transformMotionEventForInjection(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 359
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    const-string v1, "ShoulderKeyManager"

    if-eqz v0, :cond_0

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "before transform for injection: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v0, v0, Landroid/view/DisplayInfo;->rotation:I

    .line 363
    .local v0, "rotation":I
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    .line 364
    .local v2, "width":I
    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v3, v3, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 366
    .local v3, "height":I
    iget-object v4, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    invoke-virtual {v4}, Landroid/view/DisplayInfo;->getMode()Landroid/view/Display$Mode;

    move-result-object v4

    .line 367
    .local v4, "mode":Landroid/view/Display$Mode;
    invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalWidth()I

    move-result v5

    .line 368
    .local v5, "physicalWidth":I
    const/high16 v6, 0x3f800000    # 1.0f

    .line 369
    .local v6, "ratio":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 370
    .local v7, "matrix":Landroid/graphics/Matrix;
    const/high16 v8, 0x3f800000    # 1.0f

    packed-switch v0, :pswitch_data_0

    .line 387
    if-eqz v2, :cond_4

    int-to-float v8, v5

    int-to-float v9, v2

    div-float/2addr v8, v9

    goto :goto_0

    .line 382
    :pswitch_0
    if-eqz v3, :cond_1

    int-to-float v8, v5

    int-to-float v9, v3

    div-float/2addr v8, v9

    :cond_1
    move v6, v8

    .line 383
    const/4 v8, 0x1

    invoke-static {v8, v3, v2}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v7

    .line 385
    goto :goto_1

    .line 377
    :pswitch_1
    if-eqz v2, :cond_2

    int-to-float v8, v5

    int-to-float v9, v2

    div-float/2addr v8, v9

    :cond_2
    move v6, v8

    .line 378
    const/4 v8, 0x2

    invoke-static {v8, v2, v3}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v7

    .line 380
    goto :goto_1

    .line 372
    :pswitch_2
    if-eqz v3, :cond_3

    int-to-float v8, v5

    int-to-float v9, v3

    div-float/2addr v8, v9

    :cond_3
    move v6, v8

    .line 373
    const/4 v8, 0x3

    invoke-static {v8, v3, v2}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v7

    .line 375
    goto :goto_1

    .line 387
    :cond_4
    :goto_0
    move v6, v8

    .line 390
    :goto_1
    invoke-virtual {v7, v6, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 391
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V

    .line 392
    iget-boolean v8, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    if-eqz v8, :cond_5

    .line 393
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "after transform for injection: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private unregisterNonUIListener()V
    .locals 2

    .line 539
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z

    if-nez v0, :cond_0

    return-void

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSm:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 541
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mNonUIListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 542
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z

    .line 543
    iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z

    .line 544
    const-string v0, "ShoulderKeyManager"

    const-string v1, " unregister NonUISensorListener,mIsPocketMode = false"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    :cond_1
    return-void
.end method

.method private updateScreenStateInternal(Z)V
    .locals 1
    .param p1, "isScreenOn"    # Z

    .line 600
    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z

    .line 601
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z

    if-eqz v0, :cond_1

    :cond_0
    if-nez p1, :cond_1

    .line 602
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->registerNonUIListener()V

    goto :goto_0

    .line 604
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->unregisterNonUIListener()V

    .line 606
    :goto_0
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 274
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    const-string v1, "ShoulderKeyManager"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 275
    :cond_0
    array-length v0, p3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string v0, "debuglog"

    const/4 v1, 0x0

    aget-object v2, p3, v1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 276
    const/4 v0, 0x1

    aget-object v2, p3, v0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_1

    move v1, v0

    :cond_1
    iput-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    .line 278
    :cond_2
    invoke-virtual {p0, p2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->dumpInternal(Ljava/io/PrintWriter;)V

    .line 279
    return-void
.end method

.method public dumpInternal(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 287
    const-string v0, "    "

    .line 288
    .local v0, "INDENT":Ljava/lang/String;
    const-string v1, "SHOULDERKEY MANAGER (dumpsys shoulderkey)\n"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 289
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DEBUG="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mIsGameMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 291
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mSupportShoulderKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mRecordEventStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mInjectEventPids="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjectEventPids:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 294
    iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z

    if-eqz v1, :cond_0

    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mBoosterSwitch="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mBoosterSwitch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mLeftShoulderKeySwitchStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 297
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mRightShoulderKeySwitchStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mShoulderKeySoundSwitch="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mShoulderKeySoundType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 300
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DisplayInfo { rotation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v2, v2, Landroid/view/DisplayInfo;->rotation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v2, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 303
    :cond_0
    return-void
.end method

.method public getShoulderKeySwitchStatus(I)Z
    .locals 1
    .param p1, "position"    # I

    .line 231
    if-nez p1, :cond_0

    .line 232
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z

    return v0

    .line 233
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 234
    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z

    return v0

    .line 236
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public injectTouchMotionEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 260
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->checkInjectEventsPermission()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Not have INJECT_EVENTS permission!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShoulderKeyManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 265
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 266
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 267
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 268
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    invoke-virtual {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendMessage(Landroid/os/Message;)Z

    .line 269
    return-void
.end method

.method public isShoulderKeyCanShortCut(I)Z
    .locals 2
    .param p1, "keyCode"    # I

    .line 471
    const/16 v0, 0x83

    const/4 v1, 0x0

    if-eq p1, v0, :cond_0

    const/16 v0, 0x84

    if-eq p1, v0, :cond_0

    .line 472
    return v1

    .line 474
    :cond_0
    sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_SHOULDERKEY_MORE:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z

    if-nez v0, :cond_2

    .line 475
    invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->isUserSetupComplete()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 478
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 476
    :cond_2
    :goto_0
    return v1
.end method

.method public isUserSetupComplete()Z
    .locals 4

    .line 576
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "user_setup_complete"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method public loadLiftKeyMap(Ljava/util/Map;)V
    .locals 2
    .param p1, "mapper"    # Ljava/util/Map;

    .line 211
    if-eqz p1, :cond_1

    .line 214
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "loadLiftKeyMap, mapper.size() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShoulderKeyManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 219
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 220
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    invoke-virtual {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendMessage(Landroid/os/Message;)Z

    .line 221
    return-void

    .line 215
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "lift key mapper is empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "lift key mapper is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerTouchMotionEventListener(Lmiui/hardware/shoulderkey/ITouchMotionEventListener;)V
    .locals 6
    .param p1, "listener"    # Lmiui/hardware/shoulderkey/ITouchMotionEventListener;

    .line 687
    if-eqz p1, :cond_1

    .line 691
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 692
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 693
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 697
    new-instance v2, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;

    invoke-direct {v2, p0, v1, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;ILmiui/hardware/shoulderkey/ITouchMotionEventListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 700
    .local v2, "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
    :try_start_1
    invoke-interface {p1}, Lmiui/hardware/shoulderkey/ITouchMotionEventListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 701
    .local v3, "binder":Landroid/os/IBinder;
    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 704
    .end local v3    # "binder":Landroid/os/IBinder;
    nop

    .line 705
    :try_start_2
    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 706
    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v3, v5, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 707
    .end local v1    # "callingPid":I
    .end local v2    # "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
    monitor-exit v0

    .line 708
    return-void

    .line 702
    .restart local v1    # "callingPid":I
    .restart local v2    # "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
    :catch_0
    move-exception v3

    .line 703
    .local v3, "ex":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
    .end local p1    # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener;
    throw v4

    .line 694
    .end local v2    # "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
    .end local v3    # "ex":Landroid/os/RemoteException;
    .restart local p0    # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
    .restart local p1    # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener;
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The calling process has already registered a TabletModeChangedListener."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
    .end local p1    # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener;
    throw v2

    .line 707
    .end local v1    # "callingPid":I
    .restart local p0    # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
    .restart local p1    # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 688
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setInjectMotionEventStatus(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 242
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->checkInjectEventsPermission()Z

    move-result v0

    const-string v1, "ShoulderKeyManager"

    if-nez v0, :cond_0

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "process Not have INJECT_EVENTS permission!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    return-void

    .line 247
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-static {v2}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " setInjectMotionEventStatus "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 250
    .local v0, "pid":I
    if-eqz p1, :cond_1

    .line 251
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjectEventPids:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 253
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mInjectEventPids:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 255
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendEmptyMessage(I)Z

    .line 256
    return-void
.end method

.method public unloadLiftKeyMap()V
    .locals 2

    .line 225
    const-string v0, "ShoulderKeyManager"

    const-string/jumbo v1, "unloadLiftKeyMap"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mHandler:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendEmptyMessage(I)Z

    .line 227
    return-void
.end method

.method public unregisterTouchMotionEventListener()V
    .locals 2

    .line 712
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mTouchMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 713
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 714
    .local v1, "callingPid":I
    invoke-direct {p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->onTouchMotionEventListenerDied(I)V

    .line 715
    .end local v1    # "callingPid":I
    monitor-exit v0

    .line 716
    return-void

    .line 715
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
