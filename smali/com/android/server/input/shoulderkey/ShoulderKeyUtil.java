public class com.android.server.input.shoulderkey.ShoulderKeyUtil {
	 /* .source "ShoulderKeyUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$CommonSoundKeys; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.util.ArrayList LOADED_SOUND_IDS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String SHOULDERKEY_STATE_FILE;
private static final android.util.ArrayMap SOUNDS_MAP;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TAG;
private static final Integer TYPE_SYSTEM;
private static android.content.Context mContext;
private static Boolean mIsSoundPooLoadComplete;
private static android.media.SoundPool mSoundPool;
/* # direct methods */
static java.util.ArrayList -$$Nest$sfgetLOADED_SOUND_IDS ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.LOADED_SOUND_IDS;
} // .end method
static void -$$Nest$smcheckSoundPoolLoadCompleted ( ) { //bridge//synthethic
/* .locals 0 */
com.android.server.input.shoulderkey.ShoulderKeyUtil .checkSoundPoolLoadCompleted ( );
return;
} // .end method
static com.android.server.input.shoulderkey.ShoulderKeyUtil ( ) {
/* .locals 1 */
/* .line 25 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
/* .line 26 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 27 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.input.shoulderkey.ShoulderKeyUtil.mIsSoundPooLoadComplete = (v0!= 0);
return;
} // .end method
public com.android.server.input.shoulderkey.ShoulderKeyUtil ( ) {
/* .locals 0 */
/* .line 17 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static void checkSoundPoolLoadCompleted ( ) {
/* .locals 2 */
/* .line 70 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.LOADED_SOUND_IDS;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* const/16 v1, 0x10 */
/* if-ne v0, v1, :cond_0 */
/* .line 71 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.input.shoulderkey.ShoulderKeyUtil.mIsSoundPooLoadComplete = (v0!= 0);
/* .line 73 */
} // :cond_0
return;
} // .end method
public static Boolean getShoulderKeySwitchStatus ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "position" # I */
/* .line 30 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/dev/gamekey"; // const-string v1, "/dev/gamekey"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 31 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 32 */
int v1 = 4; // const/4 v1, 0x4
/* new-array v1, v1, [B */
/* .line 33 */
/* .local v1, "filecontent":[B */
int v3 = 0; // const/4 v3, 0x0
/* .line 35 */
/* .local v3, "in":Ljava/io/FileInputStream; */
try { // :try_start_0
/* new-instance v4, Ljava/io/FileInputStream; */
/* invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v4 */
/* .line 36 */
(( java.io.FileInputStream ) v3 ).read ( v1 ); // invoke-virtual {v3, v1}, Ljava/io/FileInputStream;->read([B)I
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 42 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 39 */
/* :catch_0 */
/* move-exception v4 */
/* .line 40 */
/* .local v4, "e":Ljava/io/IOException; */
try { // :try_start_1
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 37 */
/* :catch_1 */
/* move-exception v4 */
/* .line 38 */
/* .local v4, "e":Ljava/io/FileNotFoundException; */
(( java.io.FileNotFoundException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 42 */
} // .end local v4 # "e":Ljava/io/FileNotFoundException;
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 43 */
/* nop */
/* .line 45 */
int v4 = 1; // const/4 v4, 0x1
/* if-nez p0, :cond_1 */
/* .line 46 */
/* aget-byte v5, v1, v2 */
/* if-nez v5, :cond_0 */
} // :cond_0
/* move v2, v4 */
} // :goto_1
/* .line 47 */
} // :cond_1
/* if-ne p0, v4, :cond_3 */
/* .line 48 */
/* aget-byte v5, v1, v4 */
/* if-nez v5, :cond_2 */
} // :cond_2
/* move v2, v4 */
} // :goto_2
/* .line 42 */
} // :goto_3
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 43 */
/* throw v2 */
/* .line 51 */
} // .end local v1 # "filecontent":[B
} // .end local v3 # "in":Ljava/io/FileInputStream;
} // :cond_3
} // .end method
private static void initSoundPool ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 55 */
/* .line 56 */
/* new-instance v0, Landroid/media/SoundPool; */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0xa */
/* invoke-direct {v0, v3, v1, v2}, Landroid/media/SoundPool;-><init>(III)V */
/* .line 57 */
/* new-instance v1, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$1; */
/* invoke-direct {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$1;-><init>()V */
(( android.media.SoundPool ) v0 ).setOnLoadCompleteListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V
/* .line 67 */
return;
} // .end method
private static Integer load ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "resId" # I */
/* .line 115 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.mSoundPool;
/* if-nez v0, :cond_0 */
/* .line 116 */
int v0 = -1; // const/4 v0, -0x1
/* .line 119 */
} // :cond_0
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil.mContext;
int v2 = 1; // const/4 v2, 0x1
v0 = (( android.media.SoundPool ) v0 ).load ( v1, p0, v2 ); // invoke-virtual {v0, v1, p0, v2}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I
} // .end method
public static void loadSoundResource ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 76 */
com.android.server.input.shoulderkey.ShoulderKeyUtil .initSoundPool ( p0 );
/* .line 77 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.SOUNDS_MAP;
/* .line 78 */
/* const v1, 0x110e0007 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 77 */
final String v2 = "classic-0-0"; // const-string v2, "classic-0-0"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 79 */
/* nop */
/* .line 80 */
/* const v1, 0x110e0008 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 79 */
final String v2 = "classic-1-0"; // const-string v2, "classic-1-0"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 81 */
/* nop */
/* .line 82 */
/* const v1, 0x110e0009 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 81 */
final String v2 = "classic-0-1"; // const-string v2, "classic-0-1"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 83 */
/* nop */
/* .line 84 */
/* const v1, 0x110e000a */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 83 */
final String v2 = "classic-1-1"; // const-string v2, "classic-1-1"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 86 */
/* nop */
/* .line 87 */
/* const v1, 0x110e000b */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 86 */
final String v2 = "bullet-0-0"; // const-string v2, "bullet-0-0"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 88 */
/* nop */
/* .line 89 */
/* const v1, 0x110e000c */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 88 */
final String v2 = "bullet-1-0"; // const-string v2, "bullet-1-0"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 90 */
/* nop */
/* .line 91 */
/* const v1, 0x110e000d */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 90 */
final String v2 = "bullet-0-1"; // const-string v2, "bullet-0-1"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 92 */
/* nop */
/* .line 93 */
/* const v1, 0x110e000e */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 92 */
final String v2 = "bullet-1-1"; // const-string v2, "bullet-1-1"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 95 */
/* nop */
/* .line 96 */
/* const v1, 0x110e000f */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 95 */
final String v2 = "current-0-0"; // const-string v2, "current-0-0"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 97 */
/* nop */
/* .line 98 */
/* const v1, 0x110e0010 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 97 */
final String v2 = "current-1-0"; // const-string v2, "current-1-0"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 99 */
/* nop */
/* .line 100 */
/* const v1, 0x110e0011 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 99 */
final String v2 = "current-0-1"; // const-string v2, "current-0-1"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 101 */
/* nop */
/* .line 102 */
/* const v1, 0x110e0012 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 101 */
final String v2 = "current-1-1"; // const-string v2, "current-1-1"
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 104 */
/* nop */
/* .line 105 */
/* const v1, 0x110e0003 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 104 */
/* const-string/jumbo v2, "wind-0-0" */
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 106 */
/* nop */
/* .line 107 */
/* const v1, 0x110e0004 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 106 */
/* const-string/jumbo v2, "wind-1-0" */
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 108 */
/* nop */
/* .line 109 */
/* const v1, 0x110e0005 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 108 */
/* const-string/jumbo v2, "wind-0-1" */
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 110 */
/* nop */
/* .line 111 */
/* const v1, 0x110e0006 */
v1 = com.android.server.input.shoulderkey.ShoulderKeyUtil .load ( v1 );
java.lang.Integer .valueOf ( v1 );
/* .line 110 */
/* const-string/jumbo v2, "wind-1-1" */
(( android.util.ArrayMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 112 */
return;
} // .end method
public static void pause ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "streamID" # I */
/* .line 142 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.mSoundPool;
(( android.media.SoundPool ) v0 ).pause ( p0 ); // invoke-virtual {v0, p0}, Landroid/media/SoundPool;->pause(I)V
/* .line 143 */
return;
} // .end method
public static void playSound ( java.lang.String p0, Boolean p1 ) {
/* .locals 9 */
/* .param p0, "soundId" # Ljava/lang/String; */
/* .param p1, "isLoop" # Z */
/* .line 132 */
/* sget-boolean v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mIsSoundPooLoadComplete:Z */
/* if-nez v0, :cond_0 */
/* .line 133 */
return;
/* .line 135 */
} // :cond_0
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.SOUNDS_MAP;
v1 = (( android.util.ArrayMap ) v0 ).indexOfKey ( p0 ); // invoke-virtual {v0, p0}, Landroid/util/ArrayMap;->indexOfKey(Ljava/lang/Object;)I
/* if-ltz v1, :cond_2 */
/* .line 136 */
v2 = com.android.server.input.shoulderkey.ShoulderKeyUtil.mSoundPool;
(( android.util.ArrayMap ) v0 ).get ( p0 ); // invoke-virtual {v0, p0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* const/high16 v4, 0x3f800000 # 1.0f */
/* const/high16 v5, 0x3f800000 # 1.0f */
int v6 = 1; // const/4 v6, 0x1
/* .line 137 */
if ( p1 != null) { // if-eqz p1, :cond_1
int v0 = -1; // const/4 v0, -0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move v7, v0 */
/* const v8, 0x3f733333 # 0.95f */
/* .line 136 */
/* invoke-virtual/range {v2 ..v8}, Landroid/media/SoundPool;->play(IFFIIF)I */
/* .line 139 */
} // :cond_2
return;
} // .end method
public static void releaseSoundResource ( ) {
/* .locals 2 */
/* .line 149 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.mSoundPool;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 150 */
final String v0 = "ShoulderKeyUtil"; // const-string v0, "ShoulderKeyUtil"
final String v1 = "SoundPool release"; // const-string v1, "SoundPool release"
android.util.Log .d ( v0,v1 );
/* .line 151 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.input.shoulderkey.ShoulderKeyUtil.mIsSoundPooLoadComplete = (v0!= 0);
/* .line 152 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.SOUNDS_MAP;
(( android.util.ArrayMap ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V
/* .line 153 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.LOADED_SOUND_IDS;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 154 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil.mSoundPool;
(( android.media.SoundPool ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/media/SoundPool;->release()V
/* .line 155 */
int v0 = 0; // const/4 v0, 0x0
/* .line 157 */
} // :cond_0
return;
} // .end method
