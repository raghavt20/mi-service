.class Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$CommonSoundKeys;
.super Ljava/lang/Object;
.source "ShoulderKeyUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CommonSoundKeys"
.end annotation


# static fields
.field public static final SOUND_SHOULDER_BULLET_CLOSE_L:Ljava/lang/String; = "bullet-0-0"

.field public static final SOUND_SHOULDER_BULLET_CLOSE_R:Ljava/lang/String; = "bullet-1-0"

.field public static final SOUND_SHOULDER_BULLET_OPEN_L:Ljava/lang/String; = "bullet-0-1"

.field public static final SOUND_SHOULDER_BULLET_OPEN_R:Ljava/lang/String; = "bullet-1-1"

.field public static final SOUND_SHOULDER_CLASSIC_CLOSE_L:Ljava/lang/String; = "classic-0-0"

.field public static final SOUND_SHOULDER_CLASSIC_CLOSE_R:Ljava/lang/String; = "classic-1-0"

.field public static final SOUND_SHOULDER_CLASSIC_OPEN_L:Ljava/lang/String; = "classic-0-1"

.field public static final SOUND_SHOULDER_CLASSIC_OPEN_R:Ljava/lang/String; = "classic-1-1"

.field public static final SOUND_SHOULDER_CURRENT_CLOSE_L:Ljava/lang/String; = "current-0-0"

.field public static final SOUND_SHOULDER_CURRENT_CLOSE_R:Ljava/lang/String; = "current-1-0"

.field public static final SOUND_SHOULDER_CURRENT_OPEN_L:Ljava/lang/String; = "current-0-1"

.field public static final SOUND_SHOULDER_CURRENT_OPEN_R:Ljava/lang/String; = "current-1-1"

.field public static final SOUND_SHOULDER_WIND_CLOSE_L:Ljava/lang/String; = "wind-0-0"

.field public static final SOUND_SHOULDER_WIND_CLOSE_R:Ljava/lang/String; = "wind-1-0"

.field public static final SOUND_SHOULDER_WIND_OPEN_L:Ljava/lang/String; = "wind-0-1"

.field public static final SOUND_SHOULDER_WIND_OPEN_R:Ljava/lang/String; = "wind-1-1"


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;


# direct methods
.method private constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;)V
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$CommonSoundKeys;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
