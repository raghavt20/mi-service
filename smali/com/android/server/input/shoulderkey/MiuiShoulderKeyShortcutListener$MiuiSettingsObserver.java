class com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiShoulderKeyShortcutListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener this$0; //synthetic
/* # direct methods */
 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 272 */
this.this$0 = p1;
/* .line 273 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 274 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 277 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 278 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* nop */
/* .line 279 */
final String v1 = "left_shoulder_key_single_click"; // const-string v1, "left_shoulder_key_single_click"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 278 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 281 */
/* nop */
/* .line 282 */
final String v1 = "left_shoulder_key_double_click"; // const-string v1, "left_shoulder_key_double_click"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 281 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 284 */
/* nop */
/* .line 285 */
final String v1 = "left_shoulder_key_long_press"; // const-string v1, "left_shoulder_key_long_press"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 284 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 287 */
/* nop */
/* .line 288 */
final String v1 = "right_shoulder_key_single_click"; // const-string v1, "right_shoulder_key_single_click"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 287 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 290 */
/* nop */
/* .line 291 */
final String v1 = "right_shoulder_key_double_click"; // const-string v1, "right_shoulder_key_double_click"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 290 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 293 */
/* nop */
/* .line 294 */
final String v1 = "right_shoulder_key_long_press"; // const-string v1, "right_shoulder_key_long_press"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 293 */
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 296 */
final String v1 = "do_not_show_shoulder_key_shortcut_prompt"; // const-string v1, "do_not_show_shoulder_key_shortcut_prompt"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 299 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 320 */
final String v0 = "left_shoulder_key_single_click"; // const-string v0, "left_shoulder_key_single_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 321 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "MiuiShoulderKeyShortcutListener"; // const-string v2, "MiuiShoulderKeyShortcutListener"
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 322 */
	 v1 = this.this$0;
	 /* .line 323 */
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$mgetFunction ( v1,v0 );
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeySingleClickFunction ( v1,v0 );
	 /* .line 324 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "mLeftShoulderKeySingleClickFunction = "; // const-string v1, "mLeftShoulderKeySingleClickFunction = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeySingleClickFunction ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 326 */
} // :cond_0
final String v0 = "left_shoulder_key_double_click"; // const-string v0, "left_shoulder_key_double_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 327 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 328 */
	 v1 = this.this$0;
	 /* .line 329 */
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$mgetFunction ( v1,v0 );
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeyDoubleClickFunction ( v1,v0 );
	 /* .line 330 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "mLeftShoulderKeyDoubleClickFunction = "; // const-string v1, "mLeftShoulderKeyDoubleClickFunction = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeyDoubleClickFunction ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 332 */
} // :cond_1
final String v0 = "left_shoulder_key_long_press"; // const-string v0, "left_shoulder_key_long_press"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 333 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 334 */
	 v1 = this.this$0;
	 /* .line 335 */
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$mgetFunction ( v1,v0 );
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeyLongPressFunction ( v1,v0 );
	 /* .line 336 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "mLeftShoulderKeyLongPressFunction = "; // const-string v1, "mLeftShoulderKeyLongPressFunction = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeyLongPressFunction ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 338 */
} // :cond_2
final String v0 = "right_shoulder_key_single_click"; // const-string v0, "right_shoulder_key_single_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 339 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 340 */
	 v1 = this.this$0;
	 /* .line 341 */
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$mgetFunction ( v1,v0 );
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeySingleClickFunction ( v1,v0 );
	 /* .line 342 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "mRightShoulderKeySingleClickFunction = "; // const-string v1, "mRightShoulderKeySingleClickFunction = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeySingleClickFunction ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 344 */
} // :cond_3
final String v0 = "right_shoulder_key_double_click"; // const-string v0, "right_shoulder_key_double_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 345 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
	 /* .line 346 */
	 v1 = this.this$0;
	 /* .line 347 */
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$mgetFunction ( v1,v0 );
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeyDoubleClickFunction ( v1,v0 );
	 /* .line 348 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "mRightShoulderKeyDoubleClickFunction = "; // const-string v1, "mRightShoulderKeyDoubleClickFunction = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeyDoubleClickFunction ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v0 );
	 /* .line 350 */
} // :cond_4
final String v0 = "right_shoulder_key_long_press"; // const-string v0, "right_shoulder_key_long_press"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 351 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
	 /* .line 352 */
	 v1 = this.this$0;
	 /* .line 353 */
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$mgetFunction ( v1,v0 );
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeyLongPressFunction ( v1,v0 );
	 /* .line 354 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "mRightShoulderKeyLongPressFunction = "; // const-string v1, "mRightShoulderKeyLongPressFunction = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeyLongPressFunction ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v0 );
	 /* .line 356 */
} // :cond_5
final String v0 = "do_not_show_shoulder_key_shortcut_prompt"; // const-string v0, "do_not_show_shoulder_key_shortcut_prompt"
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 357 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
	 /* .line 358 */
	 v1 = this.this$0;
	 com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 int v4 = -2; // const/4 v4, -0x2
	 int v5 = 0; // const/4 v5, 0x0
	 v0 = 	 android.provider.Settings$Secure .getIntForUser ( v3,v0,v5,v4 );
	 int v3 = 1; // const/4 v3, 0x1
	 /* if-ne v0, v3, :cond_6 */
	 /* move v5, v3 */
} // :cond_6
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmDoNotShowDialogAgain ( v1,v5 );
/* .line 361 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mDoNotShowDialogAgain = "; // const-string v1, "mDoNotShowDialogAgain = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmDoNotShowDialogAgain ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 363 */
} // :cond_7
} // :goto_0
return;
} // .end method
void update ( ) {
/* .locals 2 */
/* .line 302 */
/* nop */
/* .line 303 */
final String v0 = "left_shoulder_key_single_click"; // const-string v0, "left_shoulder_key_single_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 302 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 304 */
/* nop */
/* .line 305 */
final String v0 = "left_shoulder_key_double_click"; // const-string v0, "left_shoulder_key_double_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 304 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 306 */
/* nop */
/* .line 307 */
final String v0 = "left_shoulder_key_long_press"; // const-string v0, "left_shoulder_key_long_press"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 306 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 308 */
/* nop */
/* .line 309 */
final String v0 = "right_shoulder_key_single_click"; // const-string v0, "right_shoulder_key_single_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 308 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 310 */
/* nop */
/* .line 311 */
final String v0 = "right_shoulder_key_double_click"; // const-string v0, "right_shoulder_key_double_click"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 310 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 312 */
/* nop */
/* .line 313 */
final String v0 = "right_shoulder_key_long_press"; // const-string v0, "right_shoulder_key_long_press"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 312 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 314 */
final String v0 = "do_not_show_shoulder_key_shortcut_prompt"; // const-string v0, "do_not_show_shoulder_key_shortcut_prompt"
android.provider.Settings$Secure .getUriFor ( v0 );
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 316 */
return;
} // .end method
