.class Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiShoulderKeyShortcutListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;


# direct methods
.method constructor <init>(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 272
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 273
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 274
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 277
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 278
    .local v0, "resolver":Landroid/content/ContentResolver;
    nop

    .line 279
    const-string v1, "left_shoulder_key_single_click"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 278
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 281
    nop

    .line 282
    const-string v1, "left_shoulder_key_double_click"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 281
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 284
    nop

    .line 285
    const-string v1, "left_shoulder_key_long_press"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 284
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 287
    nop

    .line 288
    const-string v1, "right_shoulder_key_single_click"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 287
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 290
    nop

    .line 291
    const-string v1, "right_shoulder_key_double_click"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 290
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 293
    nop

    .line 294
    const-string v1, "right_shoulder_key_long_press"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 293
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 296
    const-string v1, "do_not_show_shoulder_key_shortcut_prompt"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 299
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 320
    const-string v0, "left_shoulder_key_single_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 321
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "MiuiShoulderKeyShortcutListener"

    if-eqz v1, :cond_0

    .line 322
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 323
    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLeftShoulderKeySingleClickFunction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 326
    :cond_0
    const-string v0, "left_shoulder_key_double_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 327
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 329
    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLeftShoulderKeyDoubleClickFunction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 332
    :cond_1
    const-string v0, "left_shoulder_key_long_press"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 333
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 334
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 335
    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V

    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mLeftShoulderKeyLongPressFunction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 338
    :cond_2
    const-string v0, "right_shoulder_key_single_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 339
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 340
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 341
    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRightShoulderKeySingleClickFunction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 344
    :cond_3
    const-string v0, "right_shoulder_key_double_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 345
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 346
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 347
    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRightShoulderKeyDoubleClickFunction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 350
    :cond_4
    const-string v0, "right_shoulder_key_long_press"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 351
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 352
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 353
    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mRightShoulderKeyLongPressFunction = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 356
    :cond_5
    const-string v0, "do_not_show_shoulder_key_shortcut_prompt"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 357
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 358
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, -0x2

    const/4 v5, 0x0

    invoke-static {v3, v0, v5, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_6

    move v5, v3

    :cond_6
    invoke-static {v1, v5}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmDoNotShowDialogAgain(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mDoNotShowDialogAgain = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmDoNotShowDialogAgain(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_7
    :goto_0
    return-void
.end method

.method update()V
    .locals 2

    .line 302
    nop

    .line 303
    const-string v0, "left_shoulder_key_single_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 302
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 304
    nop

    .line 305
    const-string v0, "left_shoulder_key_double_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 304
    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 306
    nop

    .line 307
    const-string v0, "left_shoulder_key_long_press"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 306
    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 308
    nop

    .line 309
    const-string v0, "right_shoulder_key_single_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 308
    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 310
    nop

    .line 311
    const-string v0, "right_shoulder_key_double_click"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 310
    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 312
    nop

    .line 313
    const-string v0, "right_shoulder_key_long_press"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 312
    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 314
    const-string v0, "do_not_show_shoulder_key_shortcut_prompt"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 316
    return-void
.end method
