class com.android.server.input.shoulderkey.ShoulderKeyManagerService$2 implements android.hardware.SensorEventListener {
	 /* .source "ShoulderKeyManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.shoulderkey.ShoulderKeyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.input.shoulderkey.ShoulderKeyManagerService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
/* .line 548 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "accuracy" # I */
/* .line 564 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/hardware/SensorEvent; */
/* .line 551 */
v0 = this.sensor;
v0 = (( android.hardware.Sensor ) v0 ).getType ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I
/* const v1, 0x1fa2653 */
/* if-ne v0, v1, :cond_2 */
/* .line 552 */
v0 = this.values;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 553 */
	 v0 = this.this$0;
	 int v1 = 1; // const/4 v1, 0x1
	 com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmIsPocketMode ( v0,v1 );
	 /* .line 554 */
} // :cond_0
v0 = this.values;
/* aget v0, v0, v1 */
/* cmpl-float v0, v0, v2 */
/* if-nez v0, :cond_1 */
/* .line 555 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmIsPocketMode ( v0,v1 );
/* .line 557 */
} // :cond_1
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "NonUIEventListener onSensorChanged,mIsPocketMode = "; // const-string v1, "NonUIEventListener onSensorChanged,mIsPocketMode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmIsPocketMode ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
android.util.Slog .d ( v1,v0 );
/* .line 560 */
} // :cond_2
return;
} // .end method
