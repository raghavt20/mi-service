public class com.android.server.input.shoulderkey.ShoulderKeyManagerService extends miui.hardware.shoulderkey.IShoulderKeyManager$Stub {
	 /* .source "ShoulderKeyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;, */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;, */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;, */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;, */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack;, */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;, */
	 /* Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long GAMEBOOSTER_DEBOUNCE_DELAY_MILLS;
private static final java.lang.String GAME_BOOSTER_SWITCH;
private static final java.lang.String KEY_GAME_BOOSTER;
private static final Integer NONUI_SENSOR_ID;
public static final java.lang.String SHOULDEKEY_SOUND_TYPE;
private static final Integer SHOULDERKEY_POSITION_LEFT;
private static final Integer SHOULDERKEY_POSITION_RIGHT;
public static final java.lang.String SHOULDERKEY_SOUND_SWITCH;
private static final java.lang.String TAG;
/* # instance fields */
private Boolean DEBUG;
private android.app.IActivityTaskManager mActivityTaskManager;
private Boolean mBoosterSwitch;
private android.content.Context mContext;
private java.lang.String mCurrentForegroundAppLabel;
private java.lang.String mCurrentForegroundPkg;
private android.view.DisplayInfo mDisplayInfo;
private final android.hardware.display.DisplayManager$DisplayListener mDisplayListener;
private android.hardware.display.DisplayManager mDisplayManager;
private android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
private Long mDownTime;
private com.android.server.input.shoulderkey.ShoulderKeyManagerService$H mHandler;
private android.os.HandlerThread mHandlerThread;
private final java.util.HashSet mInjcetEeventPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashSet mInjectEventPids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsGameMode;
private Boolean mIsPocketMode;
private Boolean mIsScreenOn;
private android.app.KeyguardManager mKeyguardManager;
private Boolean mLeftShoulderKeySwitchStatus;
private Boolean mLeftShoulderKeySwitchTriggered;
private android.util.ArrayMap mLifeKeyMapper;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Lmiui/hardware/shoulderkey/ShoulderKey;", */
/* "Lmiui/hardware/shoulderkey/ShoulderKeyMap;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.input.shoulderkey.ShoulderKeyManagerService$LocalService mLocalService;
private final com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener mMiuiShoulderKeyShortcutListener;
private android.hardware.SensorEventListener mNonUIListener;
private android.hardware.Sensor mNonUISensor;
private android.content.pm.PackageManager mPackageManager;
private Boolean mRecordEventStatus;
private Boolean mRegisteredNonUI;
private Boolean mRightShoulderKeySwitchStatus;
private Boolean mRightShoulderKeySwitchTriggered;
private com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver mSettingsObserver;
private Boolean mShoulderKeySoundSwitch;
private java.lang.String mShoulderKeySoundType;
private android.hardware.SensorManager mSm;
private Boolean mSupportShoulderKey;
private com.android.server.input.shoulderkey.ShoulderKeyManagerService$TaskStackListenerImpl mTaskStackListener;
private final java.util.List mTempTouchMotionEventListenersToNotify;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mTouchMotionEventListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mTouchMotionEventLock;
/* # direct methods */
static Boolean -$$Nest$fgetDEBUG ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
} // .end method
static android.app.IActivityTaskManager -$$Nest$fgetmActivityTaskManager ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mActivityTaskManager;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.lang.String -$$Nest$fgetmCurrentForegroundPkg ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentForegroundPkg;
} // .end method
static android.hardware.display.DisplayManagerInternal -$$Nest$fgetmDisplayManagerInternal ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayManagerInternal;
} // .end method
static com.android.server.input.shoulderkey.ShoulderKeyManagerService$H -$$Nest$fgetmHandler ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.util.HashSet -$$Nest$fgetmInjectEventPids ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInjectEventPids;
} // .end method
static Boolean -$$Nest$fgetmIsGameMode ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
} // .end method
static Boolean -$$Nest$fgetmIsPocketMode ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z */
} // .end method
static android.util.ArrayMap -$$Nest$fgetmLifeKeyMapper ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLifeKeyMapper;
} // .end method
static com.android.server.input.MiuiInputManagerInternal -$$Nest$fgetmMiuiInputManagerInternal ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiInputManagerInternal;
} // .end method
static com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener -$$Nest$fgetmMiuiShoulderKeyShortcutListener ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiShoulderKeyShortcutListener;
} // .end method
static Boolean -$$Nest$fgetmRecordEventStatus ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z */
} // .end method
static com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver -$$Nest$fgetmSettingsObserver ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSettingsObserver;
} // .end method
static void -$$Nest$fputmBoosterSwitch ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mBoosterSwitch:Z */
return;
} // .end method
static void -$$Nest$fputmCurrentForegroundAppLabel ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentForegroundAppLabel = p1;
return;
} // .end method
static void -$$Nest$fputmCurrentForegroundPkg ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentForegroundPkg = p1;
return;
} // .end method
static void -$$Nest$fputmDisplayInfo ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, android.view.DisplayInfo p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDisplayInfo = p1;
return;
} // .end method
static void -$$Nest$fputmIsGameMode ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
return;
} // .end method
static void -$$Nest$fputmIsPocketMode ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z */
return;
} // .end method
static void -$$Nest$fputmLeftShoulderKeySwitchTriggered ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchTriggered:Z */
return;
} // .end method
static void -$$Nest$fputmRecordEventStatus ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z */
return;
} // .end method
static void -$$Nest$fputmRightShoulderKeySwitchTriggered ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchTriggered:Z */
return;
} // .end method
static void -$$Nest$fputmShoulderKeySoundSwitch ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z */
return;
} // .end method
static void -$$Nest$fputmShoulderKeySoundType ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mShoulderKeySoundType = p1;
return;
} // .end method
static void -$$Nest$mdeliverTouchMotionEvent ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, android.view.MotionEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->deliverTouchMotionEvent(Landroid/view/MotionEvent;)V */
return;
} // .end method
static java.lang.String -$$Nest$mgetAppLabelByPkgName ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->getAppLabelByPkgName(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static void -$$Nest$mhandleShoulderKeyEventInternal ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, android.view.KeyEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->handleShoulderKeyEventInternal(Landroid/view/KeyEvent;)V */
return;
} // .end method
static void -$$Nest$mloadSoundResourceIfNeeded ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->loadSoundResourceIfNeeded()V */
return;
} // .end method
static void -$$Nest$mnotifyForegroundAppChanged ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->notifyForegroundAppChanged()V */
return;
} // .end method
static void -$$Nest$monTouchMotionEventListenerDied ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->onTouchMotionEventListenerDied(I)V */
return;
} // .end method
static void -$$Nest$msetShoulderKeySwitchStatusInternal ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Integer p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V */
return;
} // .end method
static void -$$Nest$msystemReadyInternal ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->systemReadyInternal()V */
return;
} // .end method
static void -$$Nest$mtransformMotionEventForDeliver ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, android.view.MotionEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->transformMotionEventForDeliver(Landroid/view/MotionEvent;)V */
return;
} // .end method
static void -$$Nest$mtransformMotionEventForInjection ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, android.view.MotionEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->transformMotionEventForInjection(Landroid/view/MotionEvent;)V */
return;
} // .end method
static void -$$Nest$mupdateScreenStateInternal ( com.android.server.input.shoulderkey.ShoulderKeyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->updateScreenStateInternal(Z)V */
return;
} // .end method
public com.android.server.input.shoulderkey.ShoulderKeyManagerService ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 154 */
/* invoke-direct {p0}, Lmiui/hardware/shoulderkey/IShoulderKeyManager$Stub;-><init>()V */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
/* .line 104 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mInjectEventPids = v0;
/* .line 107 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mInjcetEeventPackages = v0;
/* .line 119 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$1;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V */
this.mDisplayListener = v0;
/* .line 548 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V */
this.mNonUIListener = v0;
/* .line 648 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mTouchMotionEventLock = v0;
/* .line 650 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mTouchMotionEventListeners = v0;
/* .line 652 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTempTouchMotionEventListenersToNotify = v0;
/* .line 155 */
this.mContext = p1;
/* .line 156 */
/* new-instance v0, Landroid/os/HandlerThread; */
/* const-string/jumbo v1, "shoulderKey" */
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 157 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 158 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 159 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V */
this.mLocalService = v0;
/* .line 160 */
/* const-class v1, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal; */
com.android.server.LocalServices .addService ( v1,v0 );
/* .line 161 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 162 */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->observe()V
/* .line 163 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TaskStackListenerImpl;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V */
this.mTaskStackListener = v0;
/* .line 164 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mLifeKeyMapper = v0;
/* .line 165 */
/* sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_SHOULDERKEY:Z */
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z */
/* .line 166 */
/* new-instance v0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;-><init>(Landroid/content/Context;)V */
this.mMiuiShoulderKeyShortcutListener = v0;
/* .line 167 */
/* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
this.mMiuiInputManagerInternal = v0;
/* .line 168 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->init()V */
/* .line 169 */
return;
} // .end method
private Boolean checkInjectEventsPermission ( ) {
/* .locals 3 */
/* .line 282 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 283 */
/* .local v0, "pid":I */
v1 = this.mInjcetEeventPackages;
com.android.server.am.ProcessUtils .getProcessNameByPid ( v0 );
v1 = (( java.util.HashSet ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
} // .end method
private void deliverTouchMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 728 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 729 */
final String v0 = "ShoulderKeyManager"; // const-string v0, "ShoulderKeyManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "deliverTouchMotionEvent "; // const-string v2, "deliverTouchMotionEvent "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.view.MotionEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 731 */
} // :cond_0
v0 = this.mTempTouchMotionEventListenersToNotify;
/* .line 733 */
v0 = this.mTouchMotionEventLock;
/* monitor-enter v0 */
/* .line 734 */
try { // :try_start_0
v1 = this.mTouchMotionEventListeners;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* .line 735 */
/* .local v1, "numListeners":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* .line 736 */
v3 = this.mTempTouchMotionEventListenersToNotify;
v4 = this.mTouchMotionEventListeners;
/* .line 737 */
(( android.util.SparseArray ) v4 ).valueAt ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord; */
/* .line 736 */
/* .line 735 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 739 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 740 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
/* if-ge v0, v1, :cond_2 */
/* .line 741 */
v2 = this.mTempTouchMotionEventListenersToNotify;
/* check-cast v2, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord; */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$TouchMotionEventListenerRecord ) v2 ).notifyTouchMotionEvent ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->notifyTouchMotionEvent(Landroid/view/MotionEvent;)V
/* .line 740 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 744 */
} // .end local v0 # "i":I
} // :cond_2
return;
/* .line 739 */
} // .end local v1 # "numListeners":I
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private java.lang.String getAppLabelByPkgName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 634 */
final String v0 = ""; // const-string v0, ""
/* .line 635 */
/* .local v0, "label":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 637 */
/* .local v1, "ai":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
v2 = this.mPackageManager;
/* const/16 v3, 0x40 */
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v1, v2 */
/* .line 640 */
/* .line 638 */
/* :catch_0 */
/* move-exception v2 */
/* .line 639 */
/* .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 641 */
} // .end local v2 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 642 */
v2 = this.mPackageManager;
(( android.content.pm.ApplicationInfo ) v1 ).loadLabel ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* .line 644 */
} // :cond_0
} // .end method
private void handleShoulderKeyEventInternal ( android.view.KeyEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 438 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v0 = (( com.android.server.input.shoulderkey.ShoulderKeyManagerService ) p0 ).isShoulderKeyCanShortCut ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->isShoulderKeyCanShortCut(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 439 */
v0 = this.mMiuiShoulderKeyShortcutListener;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener ) v0 ).handleShoulderKeyShortcut ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->handleShoulderKeyShortcut(Landroid/view/KeyEvent;)V
/* .line 441 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
/* .line 442 */
/* .local v0, "position":I */
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v2, 0x83 */
/* if-ne v1, v2, :cond_1 */
/* .line 443 */
int v0 = 0; // const/4 v0, 0x0
/* .line 444 */
} // :cond_1
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v2, 0x84 */
/* if-ne v1, v2, :cond_2 */
/* .line 445 */
int v0 = 1; // const/4 v0, 0x1
/* .line 448 */
} // :cond_2
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
int v2 = 1; // const/4 v2, 0x1
/* if-eq v0, v1, :cond_3 */
/* .line 449 */
/* nop */
/* .line 450 */
v1 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* .line 449 */
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->sendShoulderKeyEventBroadcast(III)V */
/* .line 451 */
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = this.mLifeKeyMapper;
v1 = (( android.util.ArrayMap ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->isEmpty()Z
/* if-nez v1, :cond_3 */
/* .line 452 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->handleShoulderKeyToMotionEvent(Landroid/view/KeyEvent;I)V */
/* .line 457 */
} // :cond_3
v1 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v1, :cond_7 */
/* .line 458 */
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v3, 0x85 */
int v4 = 0; // const/4 v4, 0x0
/* if-ne v1, v3, :cond_4 */
/* .line 459 */
/* invoke-direct {p0, v4, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V */
/* .line 460 */
} // :cond_4
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v3, 0x86 */
/* if-ne v1, v3, :cond_5 */
/* .line 461 */
/* invoke-direct {p0, v4, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V */
/* .line 462 */
} // :cond_5
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v3, 0x87 */
/* if-ne v1, v3, :cond_6 */
/* .line 463 */
/* invoke-direct {p0, v2, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V */
/* .line 464 */
} // :cond_6
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v3, 0x88 */
/* if-ne v1, v3, :cond_7 */
/* .line 465 */
/* invoke-direct {p0, v2, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->setShoulderKeySwitchStatusInternal(IZ)V */
/* .line 468 */
} // :cond_7
} // :goto_1
return;
} // .end method
private void handleShoulderKeyToMotionEvent ( android.view.KeyEvent p0, Integer p1 ) {
/* .locals 23 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "position" # I */
/* .line 306 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p2 */
v10 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getAction()I */
/* .line 307 */
/* .local v10, "action":I */
if ( v10 != null) { // if-eqz v10, :cond_0
int v2 = 1; // const/4 v2, 0x1
/* if-eq v10, v2, :cond_0 */
/* .line 308 */
return;
/* .line 310 */
} // :cond_0
/* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice; */
v11 = (( android.view.InputDevice ) v2 ).getProductId ( ); // invoke-virtual {v2}, Landroid/view/InputDevice;->getProductId()I
/* .line 311 */
/* .local v11, "productId":I */
v12 = /* invoke-virtual/range {p1 ..p1}, Landroid/view/KeyEvent;->getKeyCode()I */
/* .line 312 */
/* .local v12, "keycode":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 313 */
/* .local v2, "keymap":Lmiui/hardware/shoulderkey/ShoulderKeyMap; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = this.mLifeKeyMapper;
v4 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* if-ge v3, v4, :cond_2 */
/* .line 314 */
v4 = this.mLifeKeyMapper;
(( android.util.ArrayMap ) v4 ).keyAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v4, Lmiui/hardware/shoulderkey/ShoulderKey; */
v4 = (( miui.hardware.shoulderkey.ShoulderKey ) v4 ).equals ( v11, v12 ); // invoke-virtual {v4, v11, v12}, Lmiui/hardware/shoulderkey/ShoulderKey;->equals(II)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 315 */
v4 = this.mLifeKeyMapper;
(( android.util.ArrayMap ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* move-object v2, v4 */
/* check-cast v2, Lmiui/hardware/shoulderkey/ShoulderKeyMap; */
/* .line 316 */
/* move-object v13, v2 */
/* .line 313 */
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
} // :cond_2
/* move-object v13, v2 */
/* .line 319 */
} // .end local v2 # "keymap":Lmiui/hardware/shoulderkey/ShoulderKeyMap;
} // .end local v3 # "i":I
/* .local v13, "keymap":Lmiui/hardware/shoulderkey/ShoulderKeyMap; */
} // :goto_1
/* if-nez v13, :cond_3 */
/* .line 320 */
return;
/* .line 323 */
} // :cond_3
/* if-nez v10, :cond_4 */
/* .line 324 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iput-wide v2, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J */
/* .line 326 */
} // :cond_4
v2 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).isIsSeparateMapping ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->isIsSeparateMapping()Z
int v14 = 2; // const/4 v14, 0x2
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 328 */
/* if-nez v10, :cond_5 */
/* .line 329 */
v2 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).getDownCenterX ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getDownCenterX()F
/* .line 330 */
/* .local v2, "centerX":F */
v3 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).getDownCenterY ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getDownCenterY()F
/* .local v3, "centerY":F */
/* .line 332 */
} // .end local v2 # "centerX":F
} // .end local v3 # "centerY":F
} // :cond_5
v2 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).getUpCenterX ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getUpCenterX()F
/* .line 333 */
/* .restart local v2 # "centerX":F */
v3 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).getUpCenterY ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getUpCenterY()F
/* .line 336 */
/* .restart local v3 # "centerY":F */
} // :goto_2
/* iget-wide v4, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J */
/* .line 337 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v17 */
/* const/16 v19, 0x0 */
/* const/16 v22, 0x0 */
/* .line 336 */
/* move-wide v15, v4 */
/* move/from16 v20, v2 */
/* move/from16 v21, v3 */
/* invoke-static/range {v15 ..v22}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent; */
/* .line 339 */
/* .local v4, "downEvent":Landroid/view/MotionEvent; */
v5 = this.mHandler;
/* .line 340 */
v6 = android.os.Process .myPid ( );
/* .line 339 */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v5 ).obtainMessage ( v14, v1, v6, v4 ); // invoke-virtual {v5, v14, v1, v6, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 340 */
(( android.os.Message ) v5 ).sendToTarget ( ); // invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V
/* .line 342 */
/* iget-wide v5, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J */
/* .line 343 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v17 */
/* const/16 v19, 0x1 */
/* .line 342 */
/* move-wide v15, v5 */
/* invoke-static/range {v15 ..v22}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent; */
/* .line 345 */
/* .local v5, "upEvent":Landroid/view/MotionEvent; */
v6 = this.mHandler;
/* .line 346 */
v7 = android.os.Process .myPid ( );
/* .line 345 */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v6 ).obtainMessage ( v14, v1, v7, v5 ); // invoke-virtual {v6, v14, v1, v7, v5}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 346 */
(( android.os.Message ) v6 ).sendToTarget ( ); // invoke-virtual {v6}, Landroid/os/Message;->sendToTarget()V
/* .line 347 */
} // .end local v2 # "centerX":F
} // .end local v3 # "centerY":F
} // .end local v4 # "downEvent":Landroid/view/MotionEvent;
} // .end local v5 # "upEvent":Landroid/view/MotionEvent;
/* .line 348 */
} // :cond_6
/* iget-wide v2, v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mDownTime:J */
/* .line 349 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 350 */
v7 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).getCenterX ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getCenterX()F
v8 = (( miui.hardware.shoulderkey.ShoulderKeyMap ) v13 ).getCenterY ( ); // invoke-virtual {v13}, Lmiui/hardware/shoulderkey/ShoulderKeyMap;->getCenterY()F
int v9 = 0; // const/4 v9, 0x0
/* .line 348 */
/* move v6, v10 */
/* invoke-static/range {v2 ..v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent; */
/* .line 351 */
/* .local v2, "motionEvent":Landroid/view/MotionEvent; */
v3 = this.mHandler;
/* .line 352 */
v4 = android.os.Process .myPid ( );
/* .line 351 */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v3 ).obtainMessage ( v14, v1, v4, v2 ); // invoke-virtual {v3, v14, v1, v4, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 352 */
(( android.os.Message ) v3 ).sendToTarget ( ); // invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V
/* .line 354 */
} // .end local v2 # "motionEvent":Landroid/view/MotionEvent;
} // :goto_3
return;
} // .end method
private void init ( ) {
/* .locals 2 */
/* .line 172 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 173 */
/* nop */
/* .line 174 */
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil .getShoulderKeySwitchStatus ( v1 );
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z */
/* .line 175 */
/* nop */
/* .line 176 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.android.server.input.shoulderkey.ShoulderKeyUtil .getShoulderKeySwitchStatus ( v0 );
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z */
/* .line 178 */
} // :cond_0
v0 = this.mInjectEventPids;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 179 */
/* iput-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z */
/* .line 180 */
v0 = this.mInjcetEeventPackages;
final String v1 = "com.xiaomi.macro"; // const-string v1, "com.xiaomi.macro"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 181 */
v0 = this.mInjcetEeventPackages;
final String v1 = "com.xiaomi.migameservice"; // const-string v1, "com.xiaomi.migameservice"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 182 */
v0 = this.mInjcetEeventPackages;
final String v1 = "com.xiaomi.joyose"; // const-string v1, "com.xiaomi.joyose"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 183 */
return;
} // .end method
private void interceptGameBooster ( ) {
/* .locals 2 */
/* .line 515 */
v0 = this.mKeyguardManager;
v0 = (( android.app.KeyguardManager ) v0 ).isKeyguardLocked ( ); // invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z
/* .line 516 */
/* .local v0, "isKeyguardShown":Z */
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
/* if-nez v1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mBoosterSwitch:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 520 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchTriggered:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchTriggered:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 521 */
v1 = (( com.android.server.input.shoulderkey.ShoulderKeyManagerService ) p0 ).isUserSetupComplete ( ); // invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->isUserSetupComplete()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 522 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->launchGameBooster()V */
/* .line 524 */
} // :cond_1
return;
/* .line 517 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void launchGameBooster ( ) {
/* .locals 3 */
/* .line 569 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.gamebooster.action.ACCESS_MAINACTIVITY"; // const-string v1, "com.miui.gamebooster.action.ACCESS_MAINACTIVITY"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 570 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "jump_target"; // const-string v1, "jump_target"
final String v2 = "gamebox"; // const-string v2, "gamebox"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 571 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 572 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startActivityAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 573 */
return;
} // .end method
private void loadSoundResourceIfNeeded ( ) {
/* .locals 1 */
/* .line 592 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 593 */
v0 = this.mContext;
com.android.server.input.shoulderkey.ShoulderKeyUtil .loadSoundResource ( v0 );
/* .line 595 */
} // :cond_0
com.android.server.input.shoulderkey.ShoulderKeyUtil .releaseSoundResource ( );
/* .line 597 */
} // :goto_0
return;
} // .end method
private void notifyForegroundAppChanged ( ) {
/* .locals 2 */
/* .line 609 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 631 */
return;
} // .end method
private void onTouchMotionEventListenerDied ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 719 */
v0 = this.mTouchMotionEventLock;
/* monitor-enter v0 */
/* .line 720 */
try { // :try_start_0
v1 = this.mTouchMotionEventListeners;
(( android.util.SparseArray ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 721 */
v1 = this.mHandler;
v2 = this.mTouchMotionEventListeners;
/* .line 722 */
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
java.lang.Boolean .valueOf ( v2 );
/* .line 721 */
int v3 = 6; // const/4 v3, 0x6
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v1 ).obtainMessage ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 722 */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 723 */
/* monitor-exit v0 */
/* .line 724 */
return;
/* .line 723 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void playSoundIfNeeded ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "soundId" # Ljava/lang/String; */
/* .line 509 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 510 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.input.shoulderkey.ShoulderKeyUtil .playSound ( p1,v0 );
/* .line 512 */
} // :cond_0
return;
} // .end method
private void registerForegroundAppUpdater ( ) {
/* .locals 3 */
/* .line 203 */
try { // :try_start_0
v0 = this.mActivityTaskManager;
v1 = this.mTaskStackListener;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 206 */
/* .line 204 */
/* :catch_0 */
/* move-exception v0 */
/* .line 205 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to register foreground app updater: "; // const-string v2, "Failed to register foreground app updater: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ShoulderKeyManager"; // const-string v2, "ShoulderKeyManager"
android.util.Slog .e ( v2,v1 );
/* .line 207 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
private void registerNonUIListener ( ) {
/* .locals 6 */
/* .line 527 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 528 */
} // :cond_0
v0 = this.mSm;
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = this.mNonUISensor;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 529 */
v3 = this.mNonUIListener;
int v4 = 3; // const/4 v4, 0x3
v5 = this.mHandler;
(( android.hardware.SensorManager ) v0 ).registerListener ( v3, v2, v4, v5 ); // invoke-virtual {v0, v3, v2, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z
/* .line 531 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z */
/* .line 532 */
final String v0 = " register NonUISensorListener"; // const-string v0, " register NonUISensorListener"
android.util.Slog .w ( v1,v0 );
/* .line 534 */
} // :cond_1
final String v0 = " mNonUISensor is null"; // const-string v0, " mNonUISensor is null"
android.util.Slog .w ( v1,v0 );
/* .line 536 */
} // :goto_0
return;
} // .end method
private void sendShoulderKeyEventBroadcast ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "type" # I */
/* .param p2, "position" # I */
/* .param p3, "action" # I */
/* .line 581 */
v0 = this.mContext;
/* iget-boolean v4, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
v5 = this.mCurrentForegroundAppLabel;
/* move v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* invoke-static/range {v0 ..v5}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack;->reportShoulderKeyActionOneTrack(Landroid/content/Context;IIIZLjava/lang/String;)V */
/* .line 584 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.shoulderkey"; // const-string v1, "com.miui.shoulderkey"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 585 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const-string/jumbo v1, "type" */
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 586 */
final String v1 = "position"; // const-string v1, "position"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 587 */
final String v1 = "action"; // const-string v1, "action"
(( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 588 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 589 */
return;
} // .end method
private void setShoulderKeySwitchStatusInternal ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "position" # I */
/* .param p2, "isPopup" # Z */
/* .line 482 */
/* if-nez p1, :cond_0 */
/* .line 483 */
/* iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z */
/* .line 484 */
/* iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchTriggered:Z */
/* .line 485 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_4 */
/* .line 486 */
/* iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z */
/* .line 487 */
/* iput-boolean p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchTriggered:Z */
/* .line 492 */
} // :goto_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 493 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
/* const-wide/16 v2, 0x96 */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 496 */
} // :cond_1
/* move v0, p2 */
/* .line 497 */
/* .local v0, "action":I */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1, p1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->sendShoulderKeyEventBroadcast(III)V */
/* .line 498 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->interceptGameBooster()V */
/* .line 499 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mShoulderKeySoundType;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "-"; // const-string v2, "-"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->playSoundIfNeeded(Ljava/lang/String;)V */
/* .line 501 */
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z */
/* if-nez v1, :cond_2 */
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z */
/* if-nez v1, :cond_3 */
/* .line 502 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->registerNonUIListener()V */
/* .line 504 */
} // :cond_3
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->unregisterNonUIListener()V */
/* .line 506 */
} // :goto_1
return;
/* .line 489 */
} // .end local v0 # "action":I
} // :cond_4
return;
} // .end method
private void systemReadyInternal ( ) {
/* .locals 3 */
/* .line 186 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 187 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 188 */
v0 = this.mContext;
final String v1 = "keyguard"; // const-string v1, "keyguard"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/KeyguardManager; */
this.mKeyguardManager = v0;
/* .line 189 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->registerForegroundAppUpdater()V */
/* .line 190 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 191 */
v0 = this.mContext;
/* const-string/jumbo v1, "sensor" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.mSm = v0;
/* .line 192 */
/* const v1, 0x1fa2653 */
int v2 = 1; // const/4 v2, 0x1
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;
this.mNonUISensor = v0;
/* .line 194 */
} // :cond_0
v0 = this.mContext;
/* const-class v1, Landroid/hardware/display/DisplayManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
this.mDisplayManager = v0;
/* .line 195 */
v1 = this.mDisplayListener;
int v2 = 0; // const/4 v2, 0x0
(( android.hardware.display.DisplayManager ) v0 ).registerDisplayListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
/* .line 196 */
/* const-class v0, Landroid/hardware/display/DisplayManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/hardware/display/DisplayManagerInternal; */
this.mDisplayManagerInternal = v0;
/* .line 197 */
int v1 = 0; // const/4 v1, 0x0
(( android.hardware.display.DisplayManagerInternal ) v0 ).getDisplayInfo ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;
this.mDisplayInfo = v0;
/* .line 199 */
return;
} // .end method
private void transformMotionEventForDeliver ( android.view.MotionEvent p0 ) {
/* .locals 11 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 398 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 399 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "before transform for deliver: "; // const-string v2, "before transform for deliver: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.view.MotionEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 401 */
} // :cond_0
v0 = this.mDisplayInfo;
/* iget v0, v0, Landroid/view/DisplayInfo;->rotation:I */
/* .line 402 */
/* .local v0, "rotation":I */
v2 = this.mDisplayInfo;
/* iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I */
/* .line 403 */
/* .local v2, "width":I */
v3 = this.mDisplayInfo;
/* iget v3, v3, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 405 */
/* .local v3, "height":I */
v4 = this.mDisplayInfo;
(( android.view.DisplayInfo ) v4 ).getMode ( ); // invoke-virtual {v4}, Landroid/view/DisplayInfo;->getMode()Landroid/view/Display$Mode;
/* .line 406 */
/* .local v4, "mode":Landroid/view/Display$Mode; */
v5 = (( android.view.Display$Mode ) v4 ).getPhysicalWidth ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalWidth()I
/* .line 407 */
/* .local v5, "physicalWidth":I */
v6 = (( android.view.Display$Mode ) v4 ).getPhysicalHeight ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalHeight()I
/* .line 408 */
/* .local v6, "physicalHeight":I */
/* const/high16 v7, 0x3f800000 # 1.0f */
/* .line 409 */
/* .local v7, "ratio":F */
/* new-instance v8, Landroid/graphics/Matrix; */
/* invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V */
/* .line 410 */
/* .local v8, "matrix":Landroid/graphics/Matrix; */
/* const/high16 v9, 0x3f800000 # 1.0f */
/* packed-switch v0, :pswitch_data_0 */
/* .line 427 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* int-to-float v9, v2 */
/* int-to-float v10, v5 */
/* div-float/2addr v9, v10 */
/* .line 422 */
/* :pswitch_0 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* int-to-float v9, v3 */
/* int-to-float v10, v5 */
/* div-float/2addr v9, v10 */
} // :cond_1
/* move v7, v9 */
/* .line 423 */
int v9 = 3; // const/4 v9, 0x3
android.view.MotionEvent .createRotateMatrix ( v9,v6,v5 );
/* .line 425 */
/* .line 417 */
/* :pswitch_1 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* int-to-float v9, v2 */
/* int-to-float v10, v5 */
/* div-float/2addr v9, v10 */
} // :cond_2
/* move v7, v9 */
/* .line 418 */
int v9 = 2; // const/4 v9, 0x2
android.view.MotionEvent .createRotateMatrix ( v9,v5,v6 );
/* .line 420 */
/* .line 412 */
/* :pswitch_2 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* int-to-float v9, v3 */
/* int-to-float v10, v5 */
/* div-float/2addr v9, v10 */
} // :cond_3
/* move v7, v9 */
/* .line 413 */
int v9 = 1; // const/4 v9, 0x1
android.view.MotionEvent .createRotateMatrix ( v9,v6,v5 );
/* .line 415 */
/* .line 427 */
} // :cond_4
} // :goto_0
/* move v7, v9 */
/* .line 430 */
} // :goto_1
(( android.graphics.Matrix ) v8 ).postScale ( v7, v7 ); // invoke-virtual {v8, v7, v7}, Landroid/graphics/Matrix;->postScale(FF)Z
/* .line 431 */
(( android.view.MotionEvent ) p1 ).applyTransform ( v8 ); // invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V
/* .line 432 */
/* iget-boolean v9, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 433 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "after transform for deliver: "; // const-string v10, "after transform for deliver: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.view.MotionEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v9 );
/* .line 435 */
} // :cond_5
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void transformMotionEventForInjection ( android.view.MotionEvent p0 ) {
/* .locals 10 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 359 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 360 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "before transform for injection: "; // const-string v2, "before transform for injection: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.view.MotionEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 362 */
} // :cond_0
v0 = this.mDisplayInfo;
/* iget v0, v0, Landroid/view/DisplayInfo;->rotation:I */
/* .line 363 */
/* .local v0, "rotation":I */
v2 = this.mDisplayInfo;
/* iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I */
/* .line 364 */
/* .local v2, "width":I */
v3 = this.mDisplayInfo;
/* iget v3, v3, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 366 */
/* .local v3, "height":I */
v4 = this.mDisplayInfo;
(( android.view.DisplayInfo ) v4 ).getMode ( ); // invoke-virtual {v4}, Landroid/view/DisplayInfo;->getMode()Landroid/view/Display$Mode;
/* .line 367 */
/* .local v4, "mode":Landroid/view/Display$Mode; */
v5 = (( android.view.Display$Mode ) v4 ).getPhysicalWidth ( ); // invoke-virtual {v4}, Landroid/view/Display$Mode;->getPhysicalWidth()I
/* .line 368 */
/* .local v5, "physicalWidth":I */
/* const/high16 v6, 0x3f800000 # 1.0f */
/* .line 369 */
/* .local v6, "ratio":F */
/* new-instance v7, Landroid/graphics/Matrix; */
/* invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V */
/* .line 370 */
/* .local v7, "matrix":Landroid/graphics/Matrix; */
/* const/high16 v8, 0x3f800000 # 1.0f */
/* packed-switch v0, :pswitch_data_0 */
/* .line 387 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* int-to-float v8, v5 */
/* int-to-float v9, v2 */
/* div-float/2addr v8, v9 */
/* .line 382 */
/* :pswitch_0 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* int-to-float v8, v5 */
/* int-to-float v9, v3 */
/* div-float/2addr v8, v9 */
} // :cond_1
/* move v6, v8 */
/* .line 383 */
int v8 = 1; // const/4 v8, 0x1
android.view.MotionEvent .createRotateMatrix ( v8,v3,v2 );
/* .line 385 */
/* .line 377 */
/* :pswitch_1 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* int-to-float v8, v5 */
/* int-to-float v9, v2 */
/* div-float/2addr v8, v9 */
} // :cond_2
/* move v6, v8 */
/* .line 378 */
int v8 = 2; // const/4 v8, 0x2
android.view.MotionEvent .createRotateMatrix ( v8,v2,v3 );
/* .line 380 */
/* .line 372 */
/* :pswitch_2 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* int-to-float v8, v5 */
/* int-to-float v9, v3 */
/* div-float/2addr v8, v9 */
} // :cond_3
/* move v6, v8 */
/* .line 373 */
int v8 = 3; // const/4 v8, 0x3
android.view.MotionEvent .createRotateMatrix ( v8,v3,v2 );
/* .line 375 */
/* .line 387 */
} // :cond_4
} // :goto_0
/* move v6, v8 */
/* .line 390 */
} // :goto_1
(( android.graphics.Matrix ) v7 ).postScale ( v6, v6 ); // invoke-virtual {v7, v6, v6}, Landroid/graphics/Matrix;->postScale(FF)Z
/* .line 391 */
(( android.view.MotionEvent ) p1 ).applyTransform ( v7 ); // invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V
/* .line 392 */
/* iget-boolean v8, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 393 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "after transform for injection: "; // const-string v9, "after transform for injection: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.view.MotionEvent ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v8 );
/* .line 395 */
} // :cond_5
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void unregisterNonUIListener ( ) {
/* .locals 2 */
/* .line 539 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 540 */
} // :cond_0
v0 = this.mSm;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 541 */
v1 = this.mNonUIListener;
(( android.hardware.SensorManager ) v0 ).unregisterListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
/* .line 542 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRegisteredNonUI:Z */
/* .line 543 */
/* iput-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z */
/* .line 544 */
final String v0 = "ShoulderKeyManager"; // const-string v0, "ShoulderKeyManager"
final String v1 = " unregister NonUISensorListener,mIsPocketMode = false"; // const-string v1, " unregister NonUISensorListener,mIsPocketMode = false"
android.util.Slog .w ( v0,v1 );
/* .line 546 */
} // :cond_1
return;
} // .end method
private void updateScreenStateInternal ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isScreenOn" # Z */
/* .line 600 */
/* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsScreenOn:Z */
/* .line 601 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 602 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->registerNonUIListener()V */
/* .line 604 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->unregisterNonUIListener()V */
/* .line 606 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 3 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 274 */
v0 = this.mContext;
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
return;
/* .line 275 */
} // :cond_0
/* array-length v0, p3 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
final String v0 = "debuglog"; // const-string v0, "debuglog"
int v1 = 0; // const/4 v1, 0x0
/* aget-object v2, p3, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 276 */
int v0 = 1; // const/4 v0, 0x1
/* aget-object v2, p3, v0 */
v2 = java.lang.Integer .parseInt ( v2 );
/* if-ne v2, v0, :cond_1 */
/* move v1, v0 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
/* .line 278 */
} // :cond_2
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService ) p0 ).dumpInternal ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->dumpInternal(Ljava/io/PrintWriter;)V
/* .line 279 */
return;
} // .end method
public void dumpInternal ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 287 */
final String v0 = " "; // const-string v0, " "
/* .line 288 */
/* .local v0, "INDENT":Ljava/lang/String; */
final String v1 = "SHOULDERKEY MANAGER (dumpsys shoulderkey)\n"; // const-string v1, "SHOULDERKEY MANAGER (dumpsys shoulderkey)\n"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 289 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "DEBUG="; // const-string v2, "DEBUG="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->DEBUG:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 290 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mIsGameMode="; // const-string v2, "mIsGameMode="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 291 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mSupportShoulderKey="; // const-string v2, "mSupportShoulderKey="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 292 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mRecordEventStatus="; // const-string v2, "mRecordEventStatus="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRecordEventStatus:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 293 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mInjectEventPids="; // const-string v2, "mInjectEventPids="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mInjectEventPids;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 294 */
/* iget-boolean v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mSupportShoulderKey:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 295 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mBoosterSwitch="; // const-string v2, "mBoosterSwitch="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mBoosterSwitch:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 296 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mLeftShoulderKeySwitchStatus="; // const-string v2, "mLeftShoulderKeySwitchStatus="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 297 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mRightShoulderKeySwitchStatus="; // const-string v2, "mRightShoulderKeySwitchStatus="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 298 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mShoulderKeySoundSwitch="; // const-string v2, "mShoulderKeySoundSwitch="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mShoulderKeySoundSwitch:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 299 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mShoulderKeySoundType="; // const-string v2, "mShoulderKeySoundType="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mShoulderKeySoundType;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 300 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "DisplayInfo { rotation="; // const-string v2, "DisplayInfo { rotation="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mDisplayInfo;
/* iget v2, v2, Landroid/view/DisplayInfo;->rotation:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " width="; // const-string v2, " width="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mDisplayInfo;
/* iget v2, v2, Landroid/view/DisplayInfo;->logicalWidth:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " height="; // const-string v2, " height="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mDisplayInfo;
/* iget v2, v2, Landroid/view/DisplayInfo;->logicalHeight:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " }"; // const-string v2, " }"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 303 */
} // :cond_0
return;
} // .end method
public Boolean getShoulderKeySwitchStatus ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "position" # I */
/* .line 231 */
/* if-nez p1, :cond_0 */
/* .line 232 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mLeftShoulderKeySwitchStatus:Z */
/* .line 233 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_1 */
/* .line 234 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mRightShoulderKeySwitchStatus:Z */
/* .line 236 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void injectTouchMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 260 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->checkInjectEventsPermission()Z */
/* if-nez v0, :cond_0 */
/* .line 261 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " Not have INJECT_EVENTS permission!"; // const-string v1, " Not have INJECT_EVENTS permission!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
android.util.Slog .d ( v1,v0 );
/* .line 262 */
return;
/* .line 264 */
} // :cond_0
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(I)Landroid/os/Message;
/* .line 265 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 266 */
/* iput v1, v0, Landroid/os/Message;->arg1:I */
/* .line 267 */
v1 = android.os.Binder .getCallingPid ( );
/* iput v1, v0, Landroid/os/Message;->arg2:I */
/* .line 268 */
v1 = this.mHandler;
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendMessage(Landroid/os/Message;)Z
/* .line 269 */
return;
} // .end method
public Boolean isShoulderKeyCanShortCut ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "keyCode" # I */
/* .line 471 */
/* const/16 v0, 0x83 */
int v1 = 0; // const/4 v1, 0x0
/* if-eq p1, v0, :cond_0 */
/* const/16 v0, 0x84 */
/* if-eq p1, v0, :cond_0 */
/* .line 472 */
/* .line 474 */
} // :cond_0
/* sget-boolean v0, Lmiui/hardware/shoulderkey/ShoulderKeyManager;->SUPPORT_SHOULDERKEY_MORE:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsPocketMode:Z */
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->mIsGameMode:Z */
/* if-nez v0, :cond_2 */
/* .line 475 */
v0 = (( com.android.server.input.shoulderkey.ShoulderKeyManagerService ) p0 ).isUserSetupComplete ( ); // invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->isUserSetupComplete()Z
/* if-nez v0, :cond_1 */
/* .line 478 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 476 */
} // :cond_2
} // :goto_0
} // .end method
public Boolean isUserSetupComplete ( ) {
/* .locals 4 */
/* .line 576 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "user_setup_complete" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
} // .end method
public void loadLiftKeyMap ( java.util.Map p0 ) {
/* .locals 2 */
/* .param p1, "mapper" # Ljava/util/Map; */
/* .line 211 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = /* .line 214 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 217 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "loadLiftKeyMap, mapper.size() = "; // const-string v1, "loadLiftKeyMap, mapper.size() = "
v1 = (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
android.util.Slog .d ( v1,v0 );
/* .line 218 */
v0 = this.mHandler;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(I)Landroid/os/Message;
/* .line 219 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 220 */
v1 = this.mHandler;
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendMessage(Landroid/os/Message;)Z
/* .line 221 */
return;
/* .line 215 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "lift key mapper is empty"; // const-string v1, "lift key mapper is empty"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 212 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "lift key mapper is null"; // const-string v1, "lift key mapper is null"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void registerTouchMotionEventListener ( miui.hardware.shoulderkey.ITouchMotionEventListener p0 ) {
/* .locals 6 */
/* .param p1, "listener" # Lmiui/hardware/shoulderkey/ITouchMotionEventListener; */
/* .line 687 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 691 */
v0 = this.mTouchMotionEventLock;
/* monitor-enter v0 */
/* .line 692 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 693 */
/* .local v1, "callingPid":I */
v2 = this.mTouchMotionEventListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* if-nez v2, :cond_0 */
/* .line 697 */
/* new-instance v2, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord; */
/* invoke-direct {v2, p0, v1, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;-><init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;ILmiui/hardware/shoulderkey/ITouchMotionEventListener;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 700 */
/* .local v2, "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord; */
try { // :try_start_1
/* .line 701 */
/* .local v3, "binder":Landroid/os/IBinder; */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 704 */
} // .end local v3 # "binder":Landroid/os/IBinder;
/* nop */
/* .line 705 */
try { // :try_start_2
v3 = this.mTouchMotionEventListeners;
(( android.util.SparseArray ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 706 */
v3 = this.mHandler;
int v4 = 1; // const/4 v4, 0x1
java.lang.Boolean .valueOf ( v4 );
int v5 = 6; // const/4 v5, 0x6
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v3 ).obtainMessage ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v3 ).sendToTarget ( ); // invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V
/* .line 707 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
/* monitor-exit v0 */
/* .line 708 */
return;
/* .line 702 */
/* .restart local v1 # "callingPid":I */
/* .restart local v2 # "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord; */
/* :catch_0 */
/* move-exception v3 */
/* .line 703 */
/* .local v3, "ex":Landroid/os/RemoteException; */
/* new-instance v4, Ljava/lang/IllegalStateException; */
/* invoke-direct {v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V */
} // .end local p0 # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
} // .end local p1 # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener;
/* throw v4 */
/* .line 694 */
} // .end local v2 # "record":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
} // .end local v3 # "ex":Landroid/os/RemoteException;
/* .restart local p0 # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
/* .restart local p1 # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener; */
} // :cond_0
/* new-instance v2, Ljava/lang/IllegalStateException; */
final String v3 = "The calling process has already registered a TabletModeChangedListener."; // const-string v3, "The calling process has already registered a TabletModeChangedListener."
/* invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
} // .end local p1 # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener;
/* throw v2 */
/* .line 707 */
} // .end local v1 # "callingPid":I
/* .restart local p0 # "this":Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
/* .restart local p1 # "listener":Lmiui/hardware/shoulderkey/ITouchMotionEventListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 688 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "listener must not be null"; // const-string v1, "listener must not be null"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void setInjectMotionEventStatus ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 242 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->checkInjectEventsPermission()Z */
final String v1 = "ShoulderKeyManager"; // const-string v1, "ShoulderKeyManager"
/* if-nez v0, :cond_0 */
/* .line 243 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "process Not have INJECT_EVENTS permission!"; // const-string v2, "process Not have INJECT_EVENTS permission!"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 244 */
return;
/* .line 247 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v2 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getProcessNameByPid ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " setInjectMotionEventStatus "; // const-string v2, " setInjectMotionEventStatus "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 249 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 250 */
/* .local v0, "pid":I */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 251 */
v1 = this.mInjectEventPids;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashSet ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 253 */
} // :cond_1
v1 = this.mInjectEventPids;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashSet ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 255 */
} // :goto_0
v1 = this.mHandler;
int v2 = 5; // const/4 v2, 0x5
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendEmptyMessage(I)Z
/* .line 256 */
return;
} // .end method
public void unloadLiftKeyMap ( ) {
/* .locals 2 */
/* .line 225 */
final String v0 = "ShoulderKeyManager"; // const-string v0, "ShoulderKeyManager"
/* const-string/jumbo v1, "unloadLiftKeyMap" */
android.util.Slog .d ( v0,v1 );
/* .line 226 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendEmptyMessage(I)Z
/* .line 227 */
return;
} // .end method
public void unregisterTouchMotionEventListener ( ) {
/* .locals 2 */
/* .line 712 */
v0 = this.mTouchMotionEventLock;
/* monitor-enter v0 */
/* .line 713 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 714 */
/* .local v1, "callingPid":I */
/* invoke-direct {p0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->onTouchMotionEventListenerDied(I)V */
/* .line 715 */
} // .end local v1 # "callingPid":I
/* monitor-exit v0 */
/* .line 716 */
return;
/* .line 715 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
