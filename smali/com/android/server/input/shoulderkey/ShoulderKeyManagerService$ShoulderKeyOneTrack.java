class com.android.server.input.shoulderkey.ShoulderKeyManagerService$ShoulderKeyOneTrack {
	 /* .source "ShoulderKeyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ShoulderKeyOneTrack" */
} // .end annotation
/* # static fields */
private static final java.lang.String EXTRA_APP_ID;
private static final java.lang.String EXTRA_EVENT_NAME;
private static final java.lang.String EXTRA_PACKAGE_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final java.lang.String INTENT_ACTION_ONETRACK;
private static final java.lang.String INTENT_PACKAGE_ONETRACK;
private static final java.lang.String KEY_ACTION;
private static final java.lang.String KEY_EVENT_TYPE;
private static final java.lang.String KEY_GAME_NAME;
private static final java.lang.String KEY_IS_GAMEBOOSTER;
private static final java.lang.String KEY_POSITION;
private static final java.lang.String TAG;
/* # direct methods */
private com.android.server.input.shoulderkey.ShoulderKeyManagerService$ShoulderKeyOneTrack ( ) {
/* .locals 0 */
/* .line 853 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 855 */
return;
} // .end method
static void lambda$reportShoulderKeyActionOneTrack$0 ( Integer p0, Integer p1, Integer p2, Boolean p3, java.lang.String p4, android.content.Context p5 ) { //synthethic
/* .locals 4 */
/* .param p0, "eventType" # I */
/* .param p1, "position" # I */
/* .param p2, "action" # I */
/* .param p3, "isGameMode" # Z */
/* .param p4, "pkgLabel" # Ljava/lang/String; */
/* .param p5, "context" # Landroid/content/Context; */
/* .line 863 */
final String v0 = "ShoulderKeyOneTrack"; // const-string v0, "ShoulderKeyOneTrack"
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 864 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
(( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 865 */
final String v2 = "APP_ID"; // const-string v2, "APP_ID"
final String v3 = "31000000481"; // const-string v3, "31000000481"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 866 */
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
/* const-string/jumbo v3, "shoulderkey" */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 867 */
final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
final String v3 = "com.xiaomi.shoulderkey"; // const-string v3, "com.xiaomi.shoulderkey"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 868 */
final String v2 = "event_type"; // const-string v2, "event_type"
(( android.content.Intent ) v1 ).putExtra ( v2, p0 ); // invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 869 */
final String v2 = "position"; // const-string v2, "position"
(( android.content.Intent ) v1 ).putExtra ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 870 */
final String v2 = "action"; // const-string v2, "action"
(( android.content.Intent ) v1 ).putExtra ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 871 */
final String v2 = "is_gamebooster"; // const-string v2, "is_gamebooster"
(( android.content.Intent ) v1 ).putExtra ( v2, p3 ); // invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 872 */
if ( p3 != null) { // if-eqz p3, :cond_0
	 /* .line 873 */
	 final String v2 = "game_name"; // const-string v2, "game_name"
	 (( android.content.Intent ) v1 ).putExtra ( v2, p4 ); // invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 875 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
(( android.content.Intent ) v1 ).setFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 877 */
try { // :try_start_0
	 v2 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) p5 ).startServiceAsUser ( v1, v2 ); // invoke-virtual {p5, v1, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_1 */
	 /* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 880 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 881 */
	 /* .local v2, "e":Ljava/lang/SecurityException; */
	 final String v3 = "Unable to start service."; // const-string v3, "Unable to start service."
	 android.util.Slog .w ( v0,v3 );
	 /* .line 878 */
} // .end local v2 # "e":Ljava/lang/SecurityException;
/* :catch_1 */
/* move-exception v2 */
/* .line 879 */
/* .local v2, "e":Ljava/lang/IllegalStateException; */
final String v3 = "Failed to upload ShoulderKey event."; // const-string v3, "Failed to upload ShoulderKey event."
android.util.Slog .w ( v0,v3 );
/* .line 882 */
} // .end local v2 # "e":Ljava/lang/IllegalStateException;
} // :goto_0
/* nop */
/* .line 883 */
} // :goto_1
return;
} // .end method
public static void reportShoulderKeyActionOneTrack ( android.content.Context p0, Integer p1, Integer p2, Integer p3, Boolean p4, java.lang.String p5 ) {
/* .locals 9 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "eventType" # I */
/* .param p2, "position" # I */
/* .param p3, "action" # I */
/* .param p4, "isGameMode" # Z */
/* .param p5, "pkgLabel" # Ljava/lang/String; */
/* .line 859 */
if ( p0 != null) { // if-eqz p0, :cond_1
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 862 */
} // :cond_0
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v8, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack$$ExternalSyntheticLambda0; */
/* move-object v1, v8 */
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p0 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$ShoulderKeyOneTrack$$ExternalSyntheticLambda0;-><init>(IIIZLjava/lang/String;Landroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v8 ); // invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 884 */
return;
/* .line 860 */
} // :cond_1
} // :goto_0
return;
} // .end method
