class com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack {
	 /* .source "MiuiShoulderKeyShortcutListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ShoulderKeyShortcutOneTrack" */
} // .end annotation
/* # static fields */
private static final java.lang.String EXTRA_APP_ID;
private static final java.lang.String EXTRA_EVENT_NAME;
private static final java.lang.String EXTRA_PACKAGE_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final java.lang.String INTENT_ACTION_ONETRACK;
private static final java.lang.String INTENT_PACKAGE_ONETRACK;
private static final java.lang.String KEY_SHORTCUT_ACTION;
private static final java.lang.String KEY_SHORTCUT_FUNCTION;
private static final java.lang.String TAG;
/* # direct methods */
private com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack ( ) {
/* .locals 0 */
/* .line 379 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 381 */
return;
} // .end method
static void lambda$reportShoulderKeyShortcutOneTrack$0 ( java.lang.String p0, java.lang.String p1, android.content.Context p2 ) { //synthethic
/* .locals 4 */
/* .param p0, "action" # Ljava/lang/String; */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 389 */
final String v0 = "ShoulderKeyShortcutOneTrack"; // const-string v0, "ShoulderKeyShortcutOneTrack"
/* new-instance v1, Landroid/content/Intent; */
final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 390 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
(( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 391 */
final String v2 = "APP_ID"; // const-string v2, "APP_ID"
final String v3 = "31000000481"; // const-string v3, "31000000481"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 392 */
final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
/* const-string/jumbo v3, "shortcut" */
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 393 */
final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
final String v3 = "com.xiaomi.shoulderkey"; // const-string v3, "com.xiaomi.shoulderkey"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 394 */
/* const-string/jumbo v2, "shortcut_action" */
(( android.content.Intent ) v1 ).putExtra ( v2, p0 ); // invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 395 */
/* const-string/jumbo v2, "shortcut_function" */
(( android.content.Intent ) v1 ).putExtra ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 396 */
int v2 = 2; // const/4 v2, 0x2
(( android.content.Intent ) v1 ).setFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 398 */
try { // :try_start_0
	 v2 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) p2 ).startServiceAsUser ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_1 */
	 /* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 401 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 402 */
	 /* .local v2, "e":Ljava/lang/SecurityException; */
	 final String v3 = "Unable to start service."; // const-string v3, "Unable to start service."
	 android.util.Slog .w ( v0,v3 );
	 /* .line 399 */
} // .end local v2 # "e":Ljava/lang/SecurityException;
/* :catch_1 */
/* move-exception v2 */
/* .line 400 */
/* .local v2, "e":Ljava/lang/IllegalStateException; */
final String v3 = "Failed to upload ShoulderKey shortcut event."; // const-string v3, "Failed to upload ShoulderKey shortcut event."
android.util.Slog .w ( v0,v3 );
/* .line 403 */
} // .end local v2 # "e":Ljava/lang/IllegalStateException;
} // :goto_0
/* nop */
/* .line 404 */
} // :goto_1
return;
} // .end method
public static void reportShoulderKeyShortcutOneTrack ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 385 */
if ( p0 != null) { // if-eqz p0, :cond_1
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 388 */
} // :cond_0
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1, p2, p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 405 */
return;
/* .line 386 */
} // :cond_1
} // :goto_0
return;
} // .end method
