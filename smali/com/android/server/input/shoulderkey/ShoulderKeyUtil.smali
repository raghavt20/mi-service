.class public Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;
.super Ljava/lang/Object;
.source "ShoulderKeyUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$CommonSoundKeys;
    }
.end annotation


# static fields
.field private static final LOADED_SOUND_IDS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final SHOULDERKEY_STATE_FILE:Ljava/lang/String; = "/dev/gamekey"

.field private static final SOUNDS_MAP:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ShoulderKeyUtil"

.field private static final TYPE_SYSTEM:I = 0x1

.field private static mContext:Landroid/content/Context;

.field private static mIsSoundPooLoadComplete:Z

.field private static mSoundPool:Landroid/media/SoundPool;


# direct methods
.method static bridge synthetic -$$Nest$sfgetLOADED_SOUND_IDS()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->LOADED_SOUND_IDS:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smcheckSoundPoolLoadCompleted()V
    .locals 0

    invoke-static {}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->checkSoundPoolLoadCompleted()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->SOUNDS_MAP:Landroid/util/ArrayMap;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->LOADED_SOUND_IDS:Ljava/util/ArrayList;

    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mIsSoundPooLoadComplete:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkSoundPoolLoadCompleted()V
    .locals 2

    .line 70
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->LOADED_SOUND_IDS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 71
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mIsSoundPooLoadComplete:Z

    .line 73
    :cond_0
    return-void
.end method

.method public static getShoulderKeySwitchStatus(I)Z
    .locals 6
    .param p0, "position"    # I

    .line 30
    new-instance v0, Ljava/io/File;

    const-string v1, "/dev/gamekey"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 31
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 32
    const/4 v1, 0x4

    new-array v1, v1, [B

    .line 33
    .local v1, "filecontent":[B
    const/4 v3, 0x0

    .line 35
    .local v3, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v3, v4

    .line 36
    invoke-virtual {v3, v1}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 42
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 39
    :catch_0
    move-exception v4

    .line 40
    .local v4, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 37
    :catch_1
    move-exception v4

    .line 38
    .local v4, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    .end local v4    # "e":Ljava/io/FileNotFoundException;
    :goto_0
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 43
    nop

    .line 45
    const/4 v4, 0x1

    if-nez p0, :cond_1

    .line 46
    aget-byte v5, v1, v2

    if-nez v5, :cond_0

    goto :goto_1

    :cond_0
    move v2, v4

    :goto_1
    return v2

    .line 47
    :cond_1
    if-ne p0, v4, :cond_3

    .line 48
    aget-byte v5, v1, v4

    if-nez v5, :cond_2

    goto :goto_2

    :cond_2
    move v2, v4

    :goto_2
    return v2

    .line 42
    :goto_3
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 43
    throw v2

    .line 51
    .end local v1    # "filecontent":[B
    .end local v3    # "in":Ljava/io/FileInputStream;
    :cond_3
    return v2
.end method

.method private static initSoundPool(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 55
    sput-object p0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mContext:Landroid/content/Context;

    .line 56
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-direct {v0, v3, v1, v2}, Landroid/media/SoundPool;-><init>(III)V

    sput-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    .line 57
    new-instance v1, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$1;

    invoke-direct {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil$1;-><init>()V

    invoke-virtual {v0, v1}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 67
    return-void
.end method

.method private static load(I)I
    .locals 3
    .param p0, "resId"    # I

    .line 115
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 116
    const/4 v0, -0x1

    return v0

    .line 119
    :cond_0
    sget-object v1, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method public static loadSoundResource(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 76
    invoke-static {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->initSoundPool(Landroid/content/Context;)V

    .line 77
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->SOUNDS_MAP:Landroid/util/ArrayMap;

    .line 78
    const v1, 0x110e0007

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 77
    const-string v2, "classic-0-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    nop

    .line 80
    const v1, 0x110e0008

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 79
    const-string v2, "classic-1-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    nop

    .line 82
    const v1, 0x110e0009

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 81
    const-string v2, "classic-0-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    nop

    .line 84
    const v1, 0x110e000a

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 83
    const-string v2, "classic-1-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    nop

    .line 87
    const v1, 0x110e000b

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 86
    const-string v2, "bullet-0-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    nop

    .line 89
    const v1, 0x110e000c

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 88
    const-string v2, "bullet-1-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    nop

    .line 91
    const v1, 0x110e000d

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 90
    const-string v2, "bullet-0-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    nop

    .line 93
    const v1, 0x110e000e

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 92
    const-string v2, "bullet-1-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    nop

    .line 96
    const v1, 0x110e000f

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 95
    const-string v2, "current-0-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    nop

    .line 98
    const v1, 0x110e0010

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 97
    const-string v2, "current-1-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    nop

    .line 100
    const v1, 0x110e0011

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 99
    const-string v2, "current-0-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    nop

    .line 102
    const v1, 0x110e0012

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 101
    const-string v2, "current-1-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    nop

    .line 105
    const v1, 0x110e0003

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 104
    const-string/jumbo v2, "wind-0-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    nop

    .line 107
    const v1, 0x110e0004

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 106
    const-string/jumbo v2, "wind-1-0"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    nop

    .line 109
    const v1, 0x110e0005

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 108
    const-string/jumbo v2, "wind-0-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    nop

    .line 111
    const v1, 0x110e0006

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->load(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 110
    const-string/jumbo v2, "wind-1-1"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    return-void
.end method

.method public static pause(I)V
    .locals 1
    .param p0, "streamID"    # I

    .line 142
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0, p0}, Landroid/media/SoundPool;->pause(I)V

    .line 143
    return-void
.end method

.method public static playSound(Ljava/lang/String;Z)V
    .locals 9
    .param p0, "soundId"    # Ljava/lang/String;
    .param p1, "isLoop"    # Z

    .line 132
    sget-boolean v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mIsSoundPooLoadComplete:Z

    if-nez v0, :cond_0

    .line 133
    return-void

    .line 135
    :cond_0
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->SOUNDS_MAP:Landroid/util/ArrayMap;

    invoke-virtual {v0, p0}, Landroid/util/ArrayMap;->indexOfKey(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_2

    .line 136
    sget-object v2, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0, p0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    .line 137
    if-eqz p1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    move v7, v0

    const v8, 0x3f733333    # 0.95f

    .line 136
    invoke-virtual/range {v2 .. v8}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 139
    :cond_2
    return-void
.end method

.method public static releaseSoundResource()V
    .locals 2

    .line 149
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "ShoulderKeyUtil"

    const-string v1, "SoundPool release"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mIsSoundPooLoadComplete:Z

    .line 152
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->SOUNDS_MAP:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 153
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->LOADED_SOUND_IDS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 154
    sget-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 155
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->mSoundPool:Landroid/media/SoundPool;

    .line 157
    :cond_0
    return-void
.end method
