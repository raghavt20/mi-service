class com.android.server.input.shoulderkey.ShoulderKeyUtil$CommonSoundKeys {
	 /* .source "ShoulderKeyUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "CommonSoundKeys" */
} // .end annotation
/* # static fields */
public static final java.lang.String SOUND_SHOULDER_BULLET_CLOSE_L;
public static final java.lang.String SOUND_SHOULDER_BULLET_CLOSE_R;
public static final java.lang.String SOUND_SHOULDER_BULLET_OPEN_L;
public static final java.lang.String SOUND_SHOULDER_BULLET_OPEN_R;
public static final java.lang.String SOUND_SHOULDER_CLASSIC_CLOSE_L;
public static final java.lang.String SOUND_SHOULDER_CLASSIC_CLOSE_R;
public static final java.lang.String SOUND_SHOULDER_CLASSIC_OPEN_L;
public static final java.lang.String SOUND_SHOULDER_CLASSIC_OPEN_R;
public static final java.lang.String SOUND_SHOULDER_CURRENT_CLOSE_L;
public static final java.lang.String SOUND_SHOULDER_CURRENT_CLOSE_R;
public static final java.lang.String SOUND_SHOULDER_CURRENT_OPEN_L;
public static final java.lang.String SOUND_SHOULDER_CURRENT_OPEN_R;
public static final java.lang.String SOUND_SHOULDER_WIND_CLOSE_L;
public static final java.lang.String SOUND_SHOULDER_WIND_CLOSE_R;
public static final java.lang.String SOUND_SHOULDER_WIND_OPEN_L;
public static final java.lang.String SOUND_SHOULDER_WIND_OPEN_R;
/* # instance fields */
final com.android.server.input.shoulderkey.ShoulderKeyUtil this$0; //synthetic
/* # direct methods */
private com.android.server.input.shoulderkey.ShoulderKeyUtil$CommonSoundKeys ( ) {
/* .locals 0 */
/* .line 159 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
