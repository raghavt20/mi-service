.class Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;
.super Ljava/lang/Object;
.source "ShoulderKeyManagerService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    .line 548
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .line 564
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .line 551
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const v1, 0x1fa2653

    if-ne v0, v1, :cond_2

    .line 552
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmIsPocketMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    goto :goto_0

    .line 554
    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v1

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmIsPocketMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 557
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NonUIEventListener onSensorChanged,mIsPocketMode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$2;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmIsPocketMode(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShoulderKeyManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    :cond_2
    return-void
.end method
