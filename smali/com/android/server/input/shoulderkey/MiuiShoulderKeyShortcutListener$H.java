class com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H extends android.os.Handler {
	 /* .source "MiuiShoulderKeyShortcutListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
static final Integer MSG_LEFT_SHOULDER_KEY_DOUBLE_CLICK;
static final Integer MSG_LEFT_SHOULDER_KEY_LONG_PRESS;
static final Integer MSG_LEFT_SHOULDER_KEY_SINGLE_CLICK;
static final Integer MSG_RIGHT_SHOULDER_KEY_DOUBLE_CLICK;
static final Integer MSG_RIGHT_SHOULDER_KEY_LONG_PRESS;
static final Integer MSG_RIGHT_SHOULDER_KEY_SINGLE_CLICK;
static final Integer MSG_SHOW_DIALOG;
/* # instance fields */
final com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener this$0; //synthetic
/* # direct methods */
public com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 193 */
this.this$0 = p1;
/* .line 194 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 195 */
return;
} // .end method
private void sendShowPromptDialogBroadcast ( ) {
/* .locals 3 */
/* .line 263 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 264 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.shoulderkey.shortcut"; // const-string v1, "com.miui.shoulderkey.shortcut"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 265 */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 266 */
v1 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 267 */
return;
} // .end method
private void triggerFunction ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 249 */
v0 = this.this$0;
v0 = com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$misEmpty ( v0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 return;
	 /* .line 250 */
} // :cond_0
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmPowerManager ( v0 );
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
/* if-nez v0, :cond_1 */
/* .line 251 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmPowerManager ( v0 );
/* .line 252 */
/* const v1, 0x1000000a */
/* const-string/jumbo v2, "shoulderkey:bright" */
(( android.os.PowerManager ) v0 ).newWakeLock ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
/* .line 254 */
/* .local v0, "wakeLock":Landroid/os/PowerManager$WakeLock; */
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
/* .line 255 */
(( android.os.PowerManager$WakeLock ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
/* .line 257 */
} // .end local v0 # "wakeLock":Landroid/os/PowerManager$WakeLock;
} // :cond_1
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmContext ( v0 );
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, p2, v1, v2 ); // invoke-virtual {v0, p1, p2, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 259 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmContext ( v0 );
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack .reportShoulderKeyShortcutOneTrack ( v0,p2,p1 );
/* .line 260 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 199 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 241 */
/* :pswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendShowPromptDialogBroadcast()V */
/* .line 242 */
/* goto/16 :goto_0 */
/* .line 236 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeyLongPressTriggered ( v0,v2 );
/* .line 237 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeyLongPressFunction ( v0 );
final String v1 = "right_shoulder_key_long_press"; // const-string v1, "right_shoulder_key_long_press"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 239 */
/* goto/16 :goto_0 */
/* .line 231 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeyDoubleClickTriggered ( v0,v2 );
/* .line 232 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeyDoubleClickFunction ( v0 );
final String v1 = "right_shoulder_key_double_click"; // const-string v1, "right_shoulder_key_double_click"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 234 */
/* goto/16 :goto_0 */
/* .line 221 */
/* :pswitch_3 */
v0 = this.this$0;
v0 = com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeyLongPressTriggered ( v0 );
/* if-nez v0, :cond_0 */
v0 = this.this$0;
v0 = com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeyDoubleClickTriggered ( v0 );
/* if-nez v0, :cond_0 */
/* .line 223 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmRightShoulderKeySingleClickFunction ( v0 );
final String v1 = "right_shoulder_key_single_click"; // const-string v1, "right_shoulder_key_single_click"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 226 */
} // :cond_0
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeyLongPressTriggered ( v0,v1 );
/* .line 227 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmRightShoulderKeyDoubleClickTriggered ( v0,v1 );
/* .line 229 */
/* .line 216 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeyLongPressTriggered ( v0,v2 );
/* .line 217 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeyLongPressFunction ( v0 );
final String v1 = "left_shoulder_key_long_press"; // const-string v1, "left_shoulder_key_long_press"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 219 */
/* .line 211 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeyDoubleClickTriggered ( v0,v2 );
/* .line 212 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeyDoubleClickFunction ( v0 );
final String v1 = "left_shoulder_key_double_click"; // const-string v1, "left_shoulder_key_double_click"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 214 */
/* .line 201 */
/* :pswitch_6 */
v0 = this.this$0;
v0 = com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeyLongPressTriggered ( v0 );
/* if-nez v0, :cond_1 */
v0 = this.this$0;
v0 = com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeyDoubleClickTriggered ( v0 );
/* if-nez v0, :cond_1 */
/* .line 203 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fgetmLeftShoulderKeySingleClickFunction ( v0 );
final String v1 = "left_shoulder_key_single_click"; // const-string v1, "left_shoulder_key_single_click"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 206 */
} // :cond_1
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeyLongPressTriggered ( v0,v1 );
/* .line 207 */
v0 = this.this$0;
com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener .-$$Nest$fputmLeftShoulderKeyDoubleClickTriggered ( v0,v1 );
/* .line 209 */
/* nop */
/* .line 246 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
