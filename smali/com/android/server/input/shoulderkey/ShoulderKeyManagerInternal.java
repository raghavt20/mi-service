public abstract class com.android.server.input.shoulderkey.ShoulderKeyManagerInternal {
	 /* .source "ShoulderKeyManagerInternal.java" */
	 /* # virtual methods */
	 public abstract void handleShoulderKeyEvent ( android.view.KeyEvent p0 ) {
	 } // .end method
	 public abstract void notifyTouchMotionEvent ( android.view.MotionEvent p0 ) {
	 } // .end method
	 public abstract void onUserSwitch ( ) {
	 } // .end method
	 public abstract void setShoulderKeySwitchStatus ( Integer p0, Boolean p1 ) {
	 } // .end method
	 public abstract void systemReady ( ) {
	 } // .end method
	 public abstract void updateScreenState ( Boolean p0 ) {
	 } // .end method
