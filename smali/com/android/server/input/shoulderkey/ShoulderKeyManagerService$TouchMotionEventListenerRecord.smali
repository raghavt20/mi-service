.class final Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;
.super Ljava/lang/Object;
.source "ShoulderKeyManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TouchMotionEventListenerRecord"
.end annotation


# instance fields
.field private final mListener:Lmiui/hardware/shoulderkey/ITouchMotionEventListener;

.field private final mPid:I

.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;ILmiui/hardware/shoulderkey/ITouchMotionEventListener;)V
    .locals 0
    .param p2, "pid"    # I
    .param p3, "listener"    # Lmiui/hardware/shoulderkey/ITouchMotionEventListener;

    .line 661
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662
    iput p2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->mPid:I

    .line 663
    iput-object p3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->mListener:Lmiui/hardware/shoulderkey/ITouchMotionEventListener;

    .line 664
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 668
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetDEBUG(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Touch MoitonEvent listener for pid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " died."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ShoulderKeyManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    iget v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$monTouchMotionEventListenerDied(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;I)V

    .line 672
    return-void
.end method

.method public notifyTouchMotionEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 676
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->mListener:Lmiui/hardware/shoulderkey/ITouchMotionEventListener;

    invoke-interface {v0, p1}, Lmiui/hardware/shoulderkey/ITouchMotionEventListener;->onTouchMotionEvent(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 681
    goto :goto_0

    .line 677
    :catch_0
    move-exception v0

    .line 678
    .local v0, "ex":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to notify process "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->mPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " that Touch MotionEvent, assuming it died."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ShoulderKeyManager"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 680
    invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$TouchMotionEventListenerRecord;->binderDied()V

    .line 682
    .end local v0    # "ex":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
