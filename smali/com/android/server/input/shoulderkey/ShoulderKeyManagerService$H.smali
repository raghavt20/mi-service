.class Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;
.super Landroid/os/Handler;
.source "ShoulderKeyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field private static final MSG_DELIVER_TOUCH_MOTIONEVENT:I = 0x3

.field private static final MSG_EXIT_GAMEMODE:I = 0x7

.field private static final MSG_INJECT_EVENT_STATUS:I = 0x5

.field private static final MSG_INJECT_MOTIONEVENT:I = 0x2

.field private static final MSG_LOAD_LIFTKEYMAP:I = 0x0

.field private static final MSG_RECORD_EVENT_STATUS:I = 0x6

.field private static final MSG_RESET_SHOULDERKEY_TRIGGER_STATE:I = 0x4

.field private static final MSG_UNLOAD_LIFTKEYMAP:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 898
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    .line 899
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 900
    return-void
.end method

.method private updateInjectEventStatus()V
    .locals 2

    .line 903
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmInjectEventPids(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputCommonConfig;->setInjectEventStatus(Z)V

    .line 904
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 905
    return-void
.end method

.method private updateRecordEventStatus(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "enable"    # Ljava/lang/Boolean;

    .line 908
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmRecordEventStatus(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 909
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmRecordEventStatus(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputCommonConfig;->setRecordEventStatus(Z)V

    .line 910
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 911
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 915
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const-string v2, "ShoulderKeyManager"

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 962
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetDEBUG(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    const-string v0, "EXIT GAMEMODE"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmInjectEventPids(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 966
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V

    .line 967
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateRecordEventStatus(Ljava/lang/Boolean;)V

    .line 968
    goto/16 :goto_0

    .line 975
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateRecordEventStatus(Ljava/lang/Boolean;)V

    .line 976
    goto/16 :goto_0

    .line 971
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V

    .line 972
    goto/16 :goto_0

    .line 951
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmLeftShoulderKeySwitchTriggered(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 952
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmRightShoulderKeySwitchTriggered(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 953
    goto/16 :goto_0

    .line 956
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/MotionEvent;

    .line 957
    .local v0, "event":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mtransformMotionEventForDeliver(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/MotionEvent;)V

    .line 958
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mdeliverTouchMotionEvent(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/MotionEvent;)V

    .line 959
    goto/16 :goto_0

    .line 936
    .end local v0    # "event":Landroid/view/MotionEvent;
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/MotionEvent;

    .line 937
    .restart local v0    # "event":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mtransformMotionEventForInjection(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/MotionEvent;)V

    .line 938
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmMiuiInputManagerInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/MiuiInputManagerInternal;

    move-result-object v1

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v0, v3}, Lcom/android/server/input/MiuiInputManagerInternal;->injectMotionEvent(Landroid/view/MotionEvent;I)V

    .line 939
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetDEBUG(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Z

    move-result v1

    const-string v3, " "

    const-string v4, " inject motionEvent "

    if-eqz v1, :cond_1

    .line 940
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-static {v5}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 942
    invoke-virtual {v0}, Landroid/view/MotionEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 940
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 943
    :cond_1
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v5, 0x2

    if-eq v1, v5, :cond_3

    .line 944
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-static {v5}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 946
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-static {v3}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 944
    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 930
    .end local v0    # "event":Landroid/view/MotionEvent;
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmInjectEventPids(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 931
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V

    .line 932
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmLifeKeyMapper(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/util/ArrayMap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 933
    goto :goto_0

    .line 917
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    .line 918
    .local v0, "mapper":Ljava/util/Map;
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmInjectEventPids(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/util/HashSet;

    move-result-object v1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 919
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->updateInjectEventStatus()V

    .line 920
    if-eqz v0, :cond_2

    .line 921
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmLifeKeyMapper(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/util/ArrayMap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 922
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmLifeKeyMapper(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/util/ArrayMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->putAll(Ljava/util/Map;)V

    .line 923
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadLiftKeyMap "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v3}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmLifeKeyMapper(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/util/ArrayMap;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 925
    :cond_2
    const-string v1, "loadLiftKeyMap : null"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    nop

    .line 981
    .end local v0    # "mapper":Ljava/util/Map;
    :cond_3
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
