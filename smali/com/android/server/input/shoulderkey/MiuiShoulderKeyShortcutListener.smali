.class public Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;
.super Ljava/lang/Object;
.source "MiuiShoulderKeyShortcutListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;,
        Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;,
        Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack;
    }
.end annotation


# static fields
.field private static final ENABLE_SHOULDER_KEY_PRESS_INTERVAL:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "MiuiShoulderKeyShortcutListener"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDoNotShowDialogAgain:Z

.field private mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

.field private final mKeyLongPressTimeout:I

.field private mLeftShoulderKeyDoubleClickFunction:Ljava/lang/String;

.field private mLeftShoulderKeyDoubleClickTriggered:Z

.field private mLeftShoulderKeyLastDownTime:J

.field private mLeftShoulderKeyLongPressFunction:Ljava/lang/String;

.field private mLeftShoulderKeyLongPressTriggered:Z

.field private mLeftShoulderKeyPressedCount:I

.field private mLeftShoulderKeySingleClickFunction:Ljava/lang/String;

.field private mMiuiSettingsObserver:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRightShoulderKeyDoubleClickFunction:Ljava/lang/String;

.field private mRightShoulderKeyDoubleClickTriggered:Z

.field private mRightShoulderKeyLastDownTime:J

.field private mRightShoulderKeyLongPressFunction:Ljava/lang/String;

.field private mRightShoulderKeyLongPressTriggered:Z

.field private mRightShoulderKeyPressedCount:I

.field private mRightShoulderKeySingleClickFunction:Ljava/lang/String;

.field private mShoulderKeyWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoNotShowDialogAgain(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLeftShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLeftShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickTriggered:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLeftShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLeftShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressTriggered:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLeftShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeySingleClickFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerManager(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/os/PowerManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mPowerManager:Landroid/os/PowerManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRightShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRightShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickTriggered:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRightShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRightShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressTriggered:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRightShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeySingleClickFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDoNotShowDialogAgain(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeySingleClickFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeySingleClickFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misEmpty(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mContext:Landroid/content/Context;

    .line 61
    new-instance v0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;-><init>(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    .line 62
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    iput v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mKeyLongPressTimeout:I

    .line 64
    nop

    .line 65
    const-string v0, "left_shoulder_key_single_click"

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeySingleClickFunction:Ljava/lang/String;

    .line 66
    nop

    .line 67
    const-string v0, "left_shoulder_key_double_click"

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickFunction:Ljava/lang/String;

    .line 68
    nop

    .line 69
    const-string v0, "left_shoulder_key_long_press"

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressFunction:Ljava/lang/String;

    .line 70
    nop

    .line 71
    const-string v0, "right_shoulder_key_single_click"

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeySingleClickFunction:Ljava/lang/String;

    .line 72
    nop

    .line 73
    const-string v0, "right_shoulder_key_double_click"

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickFunction:Ljava/lang/String;

    .line 74
    nop

    .line 75
    const-string v0, "right_shoulder_key_long_press"

    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressFunction:Ljava/lang/String;

    .line 77
    new-instance v0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;-><init>(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mMiuiSettingsObserver:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;

    .line 78
    invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->observe()V

    .line 79
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "do_not_show_shoulder_key_shortcut_prompt"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z

    .line 82
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mPowerManager:Landroid/os/PowerManager;

    .line 83
    const-string v2, "PhoneWindowManager.mShoulderKeyWakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mShoulderKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 85
    return-void
.end method

.method private getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "function":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    const-string v0, "none"

    .line 92
    :cond_0
    return-object v0
.end method

.method private handleLeftShoulderKeyDoubleClickAction()V
    .locals 6

    .line 149
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 150
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLastDownTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    const/4 v3, 0x1

    if-gez v2, :cond_0

    .line 151
    iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I

    goto :goto_0

    .line 153
    :cond_0
    iput v3, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I

    .line 154
    iput-wide v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLastDownTime:J

    .line 156
    :goto_0
    iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    .line 157
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V

    .line 160
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v2, v4}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessage(I)Z

    .line 162
    :cond_2
    return-void
.end method

.method private handleRightShoulderKeyDoubleClickAction()V
    .locals 6

    .line 165
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 166
    .local v0, "now":J
    iget-wide v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLastDownTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    const/4 v3, 0x1

    if-gez v2, :cond_0

    .line 167
    iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I

    goto :goto_0

    .line 169
    :cond_0
    iput v3, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I

    .line 170
    iput-wide v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLastDownTime:J

    .line 172
    :goto_0
    iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 173
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V

    .line 176
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessage(I)Z

    .line 178
    :cond_2
    return-void
.end method

.method private isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "function"    # Ljava/lang/String;

    .line 145
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private promptUser(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 128
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z

    if-eqz v0, :cond_0

    goto :goto_2

    .line 130
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v2, 0x83

    const/4 v3, 0x1

    if-ne v0, v2, :cond_2

    .line 131
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickFunction:Ljava/lang/String;

    .line 133
    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressFunction:Ljava/lang/String;

    .line 134
    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v3

    goto :goto_0

    :cond_1
    nop

    .line 132
    :goto_0
    return v1

    .line 135
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v2, 0x84

    if-ne v0, v2, :cond_4

    .line 136
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_4

    .line 137
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickFunction:Ljava/lang/String;

    .line 138
    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressFunction:Ljava/lang/String;

    .line 139
    invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v3

    goto :goto_1

    :cond_3
    nop

    .line 137
    :goto_1
    return v1

    .line 141
    :cond_4
    return v1

    .line 129
    :cond_5
    :goto_2
    return v1
.end method


# virtual methods
.method public handleShoulderKeyShortcut(Landroid/view/KeyEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 96
    invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->promptUser(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessage(I)Z

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mShoulderKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 100
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x83

    const/4 v2, 0x1

    if-ne v0, v1, :cond_2

    .line 101
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mKeyLongPressTimeout:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessageDelayed(IJ)Z

    .line 104
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->handleLeftShoulderKeyDoubleClickAction()V

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 106
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V

    goto :goto_0

    .line 112
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x84

    if-ne v0, v1, :cond_4

    .line 113
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x6

    if-nez v0, :cond_3

    .line 114
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mKeyLongPressTimeout:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessageDelayed(IJ)Z

    .line 116
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->handleRightShoulderKeyDoubleClickAction()V

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 118
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 119
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mHandler:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;

    invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V

    .line 125
    :cond_4
    :goto_0
    return-void
.end method

.method public updateSettings()V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mMiuiSettingsObserver:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;

    invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->update()V

    .line 182
    return-void
.end method
