.class Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;
.super Ljava/lang/Object;
.source "ShoulderKeyManagerService.java"

# interfaces
.implements Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    .line 746
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleShoulderKeyEvent(Landroid/view/KeyEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 754
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mhandleShoulderKeyEventInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Landroid/view/KeyEvent;)V

    .line 755
    return-void
.end method

.method public notifyTouchMotionEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 769
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 770
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->sendMessage(Landroid/os/Message;)Z

    .line 771
    return-void
.end method

.method public onUserSwitch()V
    .locals 1

    .line 775
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmSettingsObserver(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->update()V

    .line 776
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmMiuiShoulderKeyShortcutListener(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->updateSettings()V

    .line 777
    return-void
.end method

.method public setShoulderKeySwitchStatus(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "isPopup"    # Z

    .line 764
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, p1, p2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$msetShoulderKeySwitchStatusInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;IZ)V

    .line 765
    return-void
.end method

.method public systemReady()V
    .locals 1

    .line 749
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$msystemReadyInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V

    .line 750
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 1
    .param p1, "isScreenOn"    # Z

    .line 759
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$LocalService;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mupdateScreenStateInternal(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Z)V

    .line 760
    return-void
.end method
