.class Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;
.super Ljava/lang/Object;
.source "ShoulderKeyManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->notifyForegroundAppChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    .line 609
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 613
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmActivityTaskManager(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Landroid/app/IActivityTaskManager;

    move-result-object v0

    .line 614
    invoke-interface {v0}, Landroid/app/IActivityTaskManager;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;

    move-result-object v0

    .line 615
    .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v0, :cond_2

    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-nez v1, :cond_0

    goto :goto_0

    .line 618
    :cond_0
    iget-object v1, v0, Landroid/app/ActivityTaskManager$RootTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 620
    .local v1, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmCurrentForegroundPkg(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fgetmCurrentForegroundPkg(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;)Ljava/lang/String;

    move-result-object v2

    .line 621
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 622
    return-void

    .line 624
    :cond_1
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v2, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmCurrentForegroundPkg(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)V

    .line 625
    iget-object v2, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$3;->this$0:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-static {v2, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$mgetAppLabelByPkgName(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;->-$$Nest$fputmCurrentForegroundAppLabel(Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 628
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v1    # "packageName":Ljava/lang/String;
    goto :goto_1

    .line 616
    .restart local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :cond_2
    :goto_0
    return-void

    .line 626
    .end local v0    # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
    :catch_0
    move-exception v0

    .line 629
    :goto_1
    return-void
.end method
