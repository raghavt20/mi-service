class com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "ShoulderKeyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.input.shoulderkey.ShoulderKeyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 782 */
this.this$0 = p1;
/* .line 783 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 784 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 787 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 788 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 790 */
/* const-string/jumbo v1, "shoulder_quick_star_gameturbo" */
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 792 */
/* const-string/jumbo v1, "shoulderkey_sound_switch" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 794 */
/* const-string/jumbo v1, "shoulderkey_sound_type" */
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 796 */
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) p0 ).update ( ); // invoke-virtual {p0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->update()V
/* .line 797 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChanged" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 808 */
v0 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 809 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 810 */
	 v2 = this.this$0;
	 v1 = 	 android.provider.Settings$Secure .getIntForUser ( v0,v1,v4,v3 );
	 /* if-ne v1, v5, :cond_0 */
	 /* move v4, v5 */
} // :cond_0
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmIsGameMode ( v2,v4 );
/* .line 812 */
v1 = this.this$0;
v1 = com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmIsGameMode ( v1 );
/* if-nez v1, :cond_6 */
/* .line 813 */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fgetmHandler ( v1 );
int v2 = 7; // const/4 v2, 0x7
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$H ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$H;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 815 */
} // :cond_1
/* const-string/jumbo v1, "shoulder_quick_star_gameturbo" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 816 */
v2 = this.this$0;
v1 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v5,v3 );
/* if-ne v1, v5, :cond_2 */
/* move v4, v5 */
} // :cond_2
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmBoosterSwitch ( v2,v4 );
/* .line 818 */
} // :cond_3
/* const-string/jumbo v1, "shoulderkey_sound_switch" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 819 */
v2 = this.this$0;
v1 = android.provider.Settings$System .getIntForUser ( v0,v1,v4,v3 );
/* if-ne v1, v5, :cond_4 */
/* move v4, v5 */
} // :cond_4
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmShoulderKeySoundSwitch ( v2,v4 );
/* .line 821 */
v1 = this.this$0;
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$mloadSoundResourceIfNeeded ( v1 );
/* .line 822 */
} // :cond_5
/* const-string/jumbo v1, "shoulderkey_sound_type" */
android.provider.Settings$System .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 823 */
v2 = this.this$0;
android.provider.Settings$System .getStringForUser ( v0,v1,v3 );
com.android.server.input.shoulderkey.ShoulderKeyManagerService .-$$Nest$fputmShoulderKeySoundType ( v2,v1 );
/* .line 826 */
} // :cond_6
} // :goto_0
return;
} // .end method
void update ( ) {
/* .locals 2 */
/* .line 800 */
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 801 */
/* const-string/jumbo v0, "shoulder_quick_star_gameturbo" */
android.provider.Settings$Secure .getUriFor ( v0 );
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 802 */
/* const-string/jumbo v0, "shoulderkey_sound_switch" */
android.provider.Settings$System .getUriFor ( v0 );
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 803 */
/* const-string/jumbo v0, "shoulderkey_sound_type" */
android.provider.Settings$System .getUriFor ( v0 );
(( com.android.server.input.shoulderkey.ShoulderKeyManagerService$MiuiSettingsObserver ) p0 ).onChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 804 */
return;
} // .end method
