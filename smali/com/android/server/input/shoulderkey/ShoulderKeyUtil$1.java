class com.android.server.input.shoulderkey.ShoulderKeyUtil$1 implements android.media.SoundPool$OnLoadCompleteListener {
	 /* .source "ShoulderKeyUtil.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/input/shoulderkey/ShoulderKeyUtil;->initSoundPool(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.android.server.input.shoulderkey.ShoulderKeyUtil$1 ( ) {
/* .locals 0 */
/* .line 57 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onLoadComplete ( android.media.SoundPool p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "soundPool" # Landroid/media/SoundPool; */
/* .param p2, "sampleId" # I */
/* .param p3, "status" # I */
/* .line 60 */
/* if-nez p3, :cond_0 */
/* .line 61 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SoundPool Load Complete, sampleId:"; // const-string v1, "SoundPool Load Complete, sampleId:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ShoulderKeyUtil"; // const-string v1, "ShoulderKeyUtil"
android.util.Log .d ( v1,v0 );
/* .line 62 */
com.android.server.input.shoulderkey.ShoulderKeyUtil .-$$Nest$sfgetLOADED_SOUND_IDS ( );
java.lang.Integer .valueOf ( p2 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 63 */
com.android.server.input.shoulderkey.ShoulderKeyUtil .-$$Nest$smcheckSoundPoolLoadCompleted ( );
/* .line 65 */
} // :cond_0
return;
} // .end method
