.class Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack;
.super Ljava/lang/Object;
.source "MiuiShoulderKeyShortcutListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShoulderKeyShortcutOneTrack"
.end annotation


# static fields
.field private static final EXTRA_APP_ID:Ljava/lang/String; = "31000000481"

.field private static final EXTRA_EVENT_NAME:Ljava/lang/String; = "shortcut"

.field private static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.shoulderkey"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final INTENT_ACTION_ONETRACK:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final INTENT_PACKAGE_ONETRACK:Ljava/lang/String; = "com.miui.analytics"

.field private static final KEY_SHORTCUT_ACTION:Ljava/lang/String; = "shortcut_action"

.field private static final KEY_SHORTCUT_FUNCTION:Ljava/lang/String; = "shortcut_function"

.field private static final TAG:Ljava/lang/String; = "ShoulderKeyShortcutOneTrack"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381
    return-void
.end method

.method static synthetic lambda$reportShoulderKeyShortcutOneTrack$0(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .line 389
    const-string v0, "ShoulderKeyShortcutOneTrack"

    new-instance v1, Landroid/content/Intent;

    const-string v2, "onetrack.action.TRACK_EVENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 390
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.miui.analytics"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    const-string v2, "APP_ID"

    const-string v3, "31000000481"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    const-string v2, "EVENT_NAME"

    const-string/jumbo v3, "shortcut"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    const-string v2, "PACKAGE"

    const-string v3, "com.xiaomi.shoulderkey"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const-string/jumbo v2, "shortcut_action"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    const-string/jumbo v2, "shortcut_function"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 398
    :try_start_0
    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 401
    :catch_0
    move-exception v2

    .line 402
    .local v2, "e":Ljava/lang/SecurityException;
    const-string v3, "Unable to start service."

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 399
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v2

    .line 400
    .local v2, "e":Ljava/lang/IllegalStateException;
    const-string v3, "Failed to upload ShoulderKey shortcut event."

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :goto_0
    nop

    .line 404
    :goto_1
    return-void
.end method

.method public static reportShoulderKeyShortcutOneTrack(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 385
    if-eqz p0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 388
    :cond_0
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p2, p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 405
    return-void

    .line 386
    :cond_1
    :goto_0
    return-void
.end method
