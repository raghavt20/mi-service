.class Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;
.super Landroid/os/Handler;
.source "MiuiShoulderKeyShortcutListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field static final MSG_LEFT_SHOULDER_KEY_DOUBLE_CLICK:I = 0x2

.field static final MSG_LEFT_SHOULDER_KEY_LONG_PRESS:I = 0x3

.field static final MSG_LEFT_SHOULDER_KEY_SINGLE_CLICK:I = 0x1

.field static final MSG_RIGHT_SHOULDER_KEY_DOUBLE_CLICK:I = 0x5

.field static final MSG_RIGHT_SHOULDER_KEY_LONG_PRESS:I = 0x6

.field static final MSG_RIGHT_SHOULDER_KEY_SINGLE_CLICK:I = 0x4

.field static final MSG_SHOW_DIALOG:I = 0x7


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;


# direct methods
.method public constructor <init>(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 193
    iput-object p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    .line 194
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 195
    return-void
.end method

.method private sendShowPromptDialogBroadcast()V
    .locals 3

    .line 263
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 264
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.shoulderkey.shortcut"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    const-string v1, "com.android.settings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    iget-object v1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 267
    return-void
.end method

.method private triggerFunction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 249
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$misEmpty(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmPowerManager(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmPowerManager(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/os/PowerManager;

    move-result-object v0

    .line 252
    const v1, 0x1000000a

    const-string/jumbo v2, "shoulderkey:bright"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 254
    .local v0, "wakeLock":Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 255
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 257
    .end local v0    # "wakeLock":Landroid/os/PowerManager$WakeLock;
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 259
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmContext(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack;->reportShoulderKeyShortcutOneTrack(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 199
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 241
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendShowPromptDialogBroadcast()V

    .line 242
    goto/16 :goto_0

    .line 236
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v2}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 237
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "right_shoulder_key_long_press"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    goto/16 :goto_0

    .line 231
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v2}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 232
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "right_shoulder_key_double_click"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    goto/16 :goto_0

    .line 221
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmRightShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "right_shoulder_key_single_click"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 227
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmRightShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 229
    goto :goto_0

    .line 216
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v2}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 217
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeyLongPressFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "left_shoulder_key_long_press"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    goto :goto_0

    .line 211
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v2}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 212
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeyDoubleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "left_shoulder_key_double_click"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    goto :goto_0

    .line 201
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fgetmLeftShoulderKeySingleClickFunction(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "left_shoulder_key_single_click"

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeyLongPressTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 207
    iget-object v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->this$0:Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;

    invoke-static {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->-$$Nest$fputmLeftShoulderKeyDoubleClickTriggered(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Z)V

    .line 209
    nop

    .line 246
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
