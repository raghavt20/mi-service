public class com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener {
	 /* .source "MiuiShoulderKeyShortcutListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;, */
	 /* Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;, */
	 /* Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$ShoulderKeyShortcutOneTrack; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ENABLE_SHOULDER_KEY_PRESS_INTERVAL;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private Boolean mDoNotShowDialogAgain;
private com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H mHandler;
private final Integer mKeyLongPressTimeout;
private java.lang.String mLeftShoulderKeyDoubleClickFunction;
private Boolean mLeftShoulderKeyDoubleClickTriggered;
private Long mLeftShoulderKeyLastDownTime;
private java.lang.String mLeftShoulderKeyLongPressFunction;
private Boolean mLeftShoulderKeyLongPressTriggered;
private Integer mLeftShoulderKeyPressedCount;
private java.lang.String mLeftShoulderKeySingleClickFunction;
private com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver mMiuiSettingsObserver;
private android.os.PowerManager mPowerManager;
private java.lang.String mRightShoulderKeyDoubleClickFunction;
private Boolean mRightShoulderKeyDoubleClickTriggered;
private Long mRightShoulderKeyLastDownTime;
private java.lang.String mRightShoulderKeyLongPressFunction;
private Boolean mRightShoulderKeyLongPressTriggered;
private Integer mRightShoulderKeyPressedCount;
private java.lang.String mRightShoulderKeySingleClickFunction;
private android.os.PowerManager$WakeLock mShoulderKeyWakeLock;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmDoNotShowDialogAgain ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z */
} // .end method
static java.lang.String -$$Nest$fgetmLeftShoulderKeyDoubleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLeftShoulderKeyDoubleClickFunction;
} // .end method
static Boolean -$$Nest$fgetmLeftShoulderKeyDoubleClickTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickTriggered:Z */
} // .end method
static java.lang.String -$$Nest$fgetmLeftShoulderKeyLongPressFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLeftShoulderKeyLongPressFunction;
} // .end method
static Boolean -$$Nest$fgetmLeftShoulderKeyLongPressTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressTriggered:Z */
} // .end method
static java.lang.String -$$Nest$fgetmLeftShoulderKeySingleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mLeftShoulderKeySingleClickFunction;
} // .end method
static android.os.PowerManager -$$Nest$fgetmPowerManager ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mPowerManager;
} // .end method
static java.lang.String -$$Nest$fgetmRightShoulderKeyDoubleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mRightShoulderKeyDoubleClickFunction;
} // .end method
static Boolean -$$Nest$fgetmRightShoulderKeyDoubleClickTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickTriggered:Z */
} // .end method
static java.lang.String -$$Nest$fgetmRightShoulderKeyLongPressFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mRightShoulderKeyLongPressFunction;
} // .end method
static Boolean -$$Nest$fgetmRightShoulderKeyLongPressTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressTriggered:Z */
} // .end method
static java.lang.String -$$Nest$fgetmRightShoulderKeySingleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mRightShoulderKeySingleClickFunction;
} // .end method
static void -$$Nest$fputmDoNotShowDialogAgain ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z */
	 return;
} // .end method
static void -$$Nest$fputmLeftShoulderKeyDoubleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mLeftShoulderKeyDoubleClickFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmLeftShoulderKeyDoubleClickTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyDoubleClickTriggered:Z */
	 return;
} // .end method
static void -$$Nest$fputmLeftShoulderKeyLongPressFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mLeftShoulderKeyLongPressFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmLeftShoulderKeyLongPressTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLongPressTriggered:Z */
	 return;
} // .end method
static void -$$Nest$fputmLeftShoulderKeySingleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mLeftShoulderKeySingleClickFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmRightShoulderKeyDoubleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mRightShoulderKeyDoubleClickFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmRightShoulderKeyDoubleClickTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyDoubleClickTriggered:Z */
	 return;
} // .end method
static void -$$Nest$fputmRightShoulderKeyLongPressFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mRightShoulderKeyLongPressFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmRightShoulderKeyLongPressTriggered ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLongPressTriggered:Z */
	 return;
} // .end method
static void -$$Nest$fputmRightShoulderKeySingleClickFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mRightShoulderKeySingleClickFunction = p1;
	 return;
} // .end method
static java.lang.String -$$Nest$mgetFunction ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static Boolean -$$Nest$misEmpty ( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z */
} // .end method
public com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 59 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 60 */
	 this.mContext = p1;
	 /* .line 61 */
	 /* new-instance v0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H; */
	 (( android.content.Context ) p1 ).getMainLooper ( ); // invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;-><init>(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 62 */
	 v0 = 	 android.view.ViewConfiguration .getLongPressTimeout ( );
	 /* iput v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mKeyLongPressTimeout:I */
	 /* .line 64 */
	 /* nop */
	 /* .line 65 */
	 final String v0 = "left_shoulder_key_single_click"; // const-string v0, "left_shoulder_key_single_click"
	 /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
	 this.mLeftShoulderKeySingleClickFunction = v0;
	 /* .line 66 */
	 /* nop */
	 /* .line 67 */
	 final String v0 = "left_shoulder_key_double_click"; // const-string v0, "left_shoulder_key_double_click"
	 /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
	 this.mLeftShoulderKeyDoubleClickFunction = v0;
	 /* .line 68 */
	 /* nop */
	 /* .line 69 */
	 final String v0 = "left_shoulder_key_long_press"; // const-string v0, "left_shoulder_key_long_press"
	 /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
	 this.mLeftShoulderKeyLongPressFunction = v0;
	 /* .line 70 */
	 /* nop */
	 /* .line 71 */
	 final String v0 = "right_shoulder_key_single_click"; // const-string v0, "right_shoulder_key_single_click"
	 /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
	 this.mRightShoulderKeySingleClickFunction = v0;
	 /* .line 72 */
	 /* nop */
	 /* .line 73 */
	 final String v0 = "right_shoulder_key_double_click"; // const-string v0, "right_shoulder_key_double_click"
	 /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
	 this.mRightShoulderKeyDoubleClickFunction = v0;
	 /* .line 74 */
	 /* nop */
	 /* .line 75 */
	 final String v0 = "right_shoulder_key_long_press"; // const-string v0, "right_shoulder_key_long_press"
	 /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->getFunction(Ljava/lang/String;)Ljava/lang/String; */
	 this.mRightShoulderKeyLongPressFunction = v0;
	 /* .line 77 */
	 /* new-instance v0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver; */
	 v1 = this.mHandler;
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;-><init>(Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;Landroid/os/Handler;)V */
	 this.mMiuiSettingsObserver = v0;
	 /* .line 78 */
	 (( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->observe()V
	 /* .line 79 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 int v1 = -2; // const/4 v1, -0x2
	 final String v2 = "do_not_show_shoulder_key_shortcut_prompt"; // const-string v2, "do_not_show_shoulder_key_shortcut_prompt"
	 int v3 = 0; // const/4 v3, 0x0
	 v0 = 	 android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne v0, v1, :cond_0 */
	 /* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z */
/* .line 82 */
v0 = this.mContext;
final String v2 = "power"; // const-string v2, "power"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 83 */
final String v2 = "PhoneWindowManager.mShoulderKeyWakeLock"; // const-string v2, "PhoneWindowManager.mShoulderKeyWakeLock"
(( android.os.PowerManager ) v0 ).newWakeLock ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mShoulderKeyWakeLock = v0;
/* .line 85 */
return;
} // .end method
private java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 88 */
v0 = this.mContext;
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,p1 );
/* .line 89 */
/* .local v0, "function":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 90 */
	 final String v0 = "none"; // const-string v0, "none"
	 /* .line 92 */
} // :cond_0
} // .end method
private void handleLeftShoulderKeyDoubleClickAction ( ) {
/* .locals 6 */
/* .line 149 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 150 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLastDownTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/16 v4, 0x12c */
/* cmp-long v2, v2, v4 */
int v3 = 1; // const/4 v3, 0x1
/* if-gez v2, :cond_0 */
/* .line 151 */
/* iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I */
/* add-int/2addr v2, v3 */
/* iput v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I */
/* .line 153 */
} // :cond_0
/* iput v3, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I */
/* .line 154 */
/* iput-wide v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyLastDownTime:J */
/* .line 156 */
} // :goto_0
/* iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mLeftShoulderKeyPressedCount:I */
int v4 = 2; // const/4 v4, 0x2
/* if-ne v2, v4, :cond_2 */
/* .line 157 */
v2 = this.mHandler;
v2 = (( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v2 ).hasMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 158 */
v2 = this.mHandler;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V
/* .line 160 */
} // :cond_1
v2 = this.mHandler;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v2 ).sendEmptyMessage ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessage(I)Z
/* .line 162 */
} // :cond_2
return;
} // .end method
private void handleRightShoulderKeyDoubleClickAction ( ) {
/* .locals 6 */
/* .line 165 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 166 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLastDownTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/16 v4, 0x12c */
/* cmp-long v2, v2, v4 */
int v3 = 1; // const/4 v3, 0x1
/* if-gez v2, :cond_0 */
/* .line 167 */
/* iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I */
/* add-int/2addr v2, v3 */
/* iput v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I */
/* .line 169 */
} // :cond_0
/* iput v3, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I */
/* .line 170 */
/* iput-wide v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyLastDownTime:J */
/* .line 172 */
} // :goto_0
/* iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mRightShoulderKeyPressedCount:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 173 */
v2 = this.mHandler;
int v3 = 4; // const/4 v3, 0x4
v2 = (( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v2 ).hasMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 174 */
v2 = this.mHandler;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v2 ).removeMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V
/* .line 176 */
} // :cond_1
v2 = this.mHandler;
int v3 = 5; // const/4 v3, 0x5
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessage(I)Z
/* .line 178 */
} // :cond_2
return;
} // .end method
private Boolean isEmpty ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 145 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
final String v0 = "none"; // const-string v0, "none"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean promptUser ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 128 */
v0 = this.mPowerManager;
v0 = (( android.os.PowerManager ) v0 ).isScreenOn ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mDoNotShowDialogAgain:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
} // :cond_0
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v2, 0x83 */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v2, :cond_2 */
/* .line 131 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v0, :cond_2 */
/* .line 132 */
v0 = this.mLeftShoulderKeyDoubleClickFunction;
/* .line 133 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mLeftShoulderKeyLongPressFunction;
/* .line 134 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v1, v3 */
} // :cond_1
/* nop */
/* .line 132 */
} // :goto_0
/* .line 135 */
} // :cond_2
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v2, 0x84 */
/* if-ne v0, v2, :cond_4 */
/* .line 136 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v0, :cond_4 */
/* .line 137 */
v0 = this.mRightShoulderKeyDoubleClickFunction;
/* .line 138 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mRightShoulderKeyLongPressFunction;
/* .line 139 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->isEmpty(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* move v1, v3 */
} // :cond_3
/* nop */
/* .line 137 */
} // :goto_1
/* .line 141 */
} // :cond_4
/* .line 129 */
} // :cond_5
} // :goto_2
} // .end method
/* # virtual methods */
public void handleShoulderKeyShortcut ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 96 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->promptUser(Landroid/view/KeyEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 97 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessage(I)Z
/* .line 99 */
} // :cond_0
v0 = this.mShoulderKeyWakeLock;
/* const-wide/16 v1, 0x3e8 */
(( android.os.PowerManager$WakeLock ) v0 ).acquire ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 100 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v1, 0x83 */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v1, :cond_2 */
/* .line 101 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 3; // const/4 v1, 0x3
/* if-nez v0, :cond_1 */
/* .line 102 */
v0 = this.mHandler;
/* iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mKeyLongPressTimeout:I */
/* int-to-long v2, v2 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 104 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->handleLeftShoulderKeyDoubleClickAction()V */
/* .line 105 */
} // :cond_1
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-ne v0, v2, :cond_4 */
/* .line 106 */
v0 = this.mHandler;
v0 = (( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 107 */
v0 = this.mHandler;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V
/* .line 112 */
} // :cond_2
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v1, 0x84 */
/* if-ne v0, v1, :cond_4 */
/* .line 113 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 6; // const/4 v1, 0x6
/* if-nez v0, :cond_3 */
/* .line 114 */
v0 = this.mHandler;
/* iget v2, p0, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->mKeyLongPressTimeout:I */
/* int-to-long v2, v2 */
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 116 */
/* invoke-direct {p0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener;->handleRightShoulderKeyDoubleClickAction()V */
/* .line 117 */
} // :cond_3
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-ne v0, v2, :cond_4 */
/* .line 118 */
v0 = this.mHandler;
v0 = (( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 119 */
v0 = this.mHandler;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$H ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$H;->removeMessages(I)V
/* .line 125 */
} // :cond_4
} // :goto_0
return;
} // .end method
public void updateSettings ( ) {
/* .locals 1 */
/* .line 181 */
v0 = this.mMiuiSettingsObserver;
(( com.android.server.input.shoulderkey.MiuiShoulderKeyShortcutListener$MiuiSettingsObserver ) v0 ).update ( ); // invoke-virtual {v0}, Lcom/android/server/input/shoulderkey/MiuiShoulderKeyShortcutListener$MiuiSettingsObserver;->update()V
/* .line 182 */
return;
} // .end method
