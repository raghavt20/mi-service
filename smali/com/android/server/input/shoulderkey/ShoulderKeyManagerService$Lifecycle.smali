.class public final Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "ShoulderKeyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 142
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 143
    new-instance v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-direct {v0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$Lifecycle;->mService:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    .line 144
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 148
    const-string/jumbo v0, "shoulderkey"

    iget-object v1, p0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$Lifecycle;->mService:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 149
    return-void
.end method
