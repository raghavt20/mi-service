.class public interface abstract Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;
.super Ljava/lang/Object;
.source "ShoulderKeyManagerInternal.java"


# virtual methods
.method public abstract handleShoulderKeyEvent(Landroid/view/KeyEvent;)V
.end method

.method public abstract notifyTouchMotionEvent(Landroid/view/MotionEvent;)V
.end method

.method public abstract onUserSwitch()V
.end method

.method public abstract setShoulderKeySwitchStatus(IZ)V
.end method

.method public abstract systemReady()V
.end method

.method public abstract updateScreenState(Z)V
.end method
