.class Lcom/android/server/input/InputOneTrackUtil$2;
.super Ljava/lang/Object;
.source "InputOneTrackUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/InputOneTrackUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/InputOneTrackUtil;


# direct methods
.method public static synthetic $r8$lambda$PaL6vsVCFdwvJ1v5e14WdajMETg(Lcom/android/server/input/InputOneTrackUtil$2;Landroid/os/IBinder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/InputOneTrackUtil$2;->lambda$onServiceConnected$0(Landroid/os/IBinder;)V

    return-void
.end method

.method public static synthetic $r8$lambda$ioMSn7cSuU_oABQSNEuTn8tRRzk(Lcom/android/server/input/InputOneTrackUtil$2;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil$2;->lambda$onServiceDisconnected$1()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/input/InputOneTrackUtil;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/InputOneTrackUtil;

    .line 126
    iput-object p1, p0, Lcom/android/server/input/InputOneTrackUtil$2;->this$0:Lcom/android/server/input/InputOneTrackUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synthetic lambda$onServiceConnected$0(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "service"    # Landroid/os/IBinder;

    .line 130
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$2;->this$0:Lcom/android/server/input/InputOneTrackUtil;

    invoke-static {p1}, Lcom/miui/analytics/ITrackBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->-$$Nest$fputmService(Lcom/android/server/input/InputOneTrackUtil;Lcom/miui/analytics/ITrackBinder;)V

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onServiceConnected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/InputOneTrackUtil$2;->this$0:Lcom/android/server/input/InputOneTrackUtil;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->-$$Nest$fgetmService(Lcom/android/server/input/InputOneTrackUtil;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputOneTrackUtil"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    return-void
.end method

.method private synthetic lambda$onServiceDisconnected$1()V
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$2;->this$0:Lcom/android/server/input/InputOneTrackUtil;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->-$$Nest$fputmService(Lcom/android/server/input/InputOneTrackUtil;Lcom/miui/analytics/ITrackBinder;)V

    .line 139
    const-string v0, "InputOneTrackUtil"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 129
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p2}, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/InputOneTrackUtil$2;Landroid/os/IBinder;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 133
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 137
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/InputOneTrackUtil$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 141
    return-void
.end method
