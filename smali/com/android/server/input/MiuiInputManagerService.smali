.class public Lcom/android/server/input/MiuiInputManagerService;
.super Lmiui/hardware/input/IMiuiInputManager$Stub;
.source "MiuiInputManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/MiuiInputManagerService$H;,
        Lcom/android/server/input/MiuiInputManagerService$LocalService;,
        Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;,
        Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;,
        Lcom/android/server/input/MiuiInputManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiInputManagerService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDisplayViewports:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;"
        }
    .end annotation
.end field

.field private mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

.field private mFlingTracker:Lcom/android/server/input/fling/FlingTracker;

.field private final mHandler:Landroid/os/Handler;

.field private mIsScreenOn:Z

.field private volatile mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

.field private mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

.field private final mMiuiGestureListener:Lcom/miui/server/input/gesture/MiuiGestureListener;

.field private mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

.field private mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

.field private mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

.field private final mMiuiShortcutSettingsLock:Ljava/lang/Object;

.field private mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

.field private final mMotionEventListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mMotionEventListenersToNotify:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mMotionEventLock:Ljava/lang/Object;

.field private final mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

.field private mPointerDisplayId:I

.field private final mPokeUserActivityRunnable:Ljava/lang/Runnable;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mShortcutListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mShortcutListenersToNotify:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;


# direct methods
.method public static synthetic $r8$lambda$5oypteGBNu0VQxOa99XZwvLqlTY(Lcom/android/server/input/MiuiInputManagerService;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->sendMotionEventToNotify(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public static synthetic $r8$lambda$c1zlw3fgnapbP_GtpingSg8G7_g(Lcom/android/server/input/MiuiInputManagerService;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->lambda$sendMotionEventToNotify$1(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public static synthetic $r8$lambda$jBIVkscI9QqFvDdFp5vaFm-AyoE(Lcom/android/server/input/MiuiInputManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerService;->lambda$new$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayViewports(Lcom/android/server/input/MiuiInputManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mDisplayViewports:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiShortcutSettingsLock(Lcom/android/server/input/MiuiInputManagerService;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiShortcutSettingsLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPointerDisplayId(Lcom/android/server/input/MiuiInputManagerService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmShortcutListeners(Lcom/android/server/input/MiuiInputManagerService;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListeners:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShortcutListenersToNotify(Lcom/android/server/input/MiuiInputManagerService;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListenersToNotify:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mremoveListenerFromSystem(Lcom/android/server/input/MiuiInputManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->removeListenerFromSystem(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveMotionEventListener(Lcom/android/server/input/MiuiInputManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->removeMotionEventListener(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetScreenState(Lcom/android/server/input/MiuiInputManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->setScreenState(Z)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 129
    invoke-direct {p0}, Lmiui/hardware/input/IMiuiInputManager$Stub;-><init>()V

    .line 78
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiShortcutSettingsLock:Ljava/lang/Object;

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListeners:Landroid/util/SparseArray;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListenersToNotify:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventLock:Ljava/lang/Object;

    .line 83
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListenersToNotify:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiGestureListener:Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I

    .line 90
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mDisplayViewports:Ljava/util/List;

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    .line 97
    new-instance v1, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V

    iput-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPokeUserActivityRunnable:Ljava/lang/Runnable;

    .line 130
    new-instance v1, Lcom/android/server/input/NativeMiuiInputManagerService;

    invoke-direct {v1, p0}, Lcom/android/server/input/NativeMiuiInputManagerService;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V

    iput-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    .line 131
    iput-object p1, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    .line 132
    new-instance v1, Lcom/android/server/input/MiuiInputManagerService$H;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getThread()Lcom/android/server/input/MiuiInputThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/server/input/MiuiInputManagerService$H;-><init>(Lcom/android/server/input/MiuiInputManagerService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mHandler:Landroid/os/Handler;

    .line 133
    invoke-static {p1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    .line 134
    const-class v1, Landroid/os/PowerManager;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPowerManager:Landroid/os/PowerManager;

    .line 135
    const-class v1, Lcom/android/server/input/MiuiInputManagerInternal;

    new-instance v2, Lcom/android/server/input/MiuiInputManagerService$LocalService;

    invoke-direct {v2, p0, v0}, Lcom/android/server/input/MiuiInputManagerService$LocalService;-><init>(Lcom/android/server/input/MiuiInputManagerService;Lcom/android/server/input/MiuiInputManagerService$LocalService-IA;)V

    invoke-static {v1, v2}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 136
    new-instance v0, Lcom/android/server/input/fling/FlingTracker;

    invoke-direct {v0, p1}, Lcom/android/server/input/fling/FlingTracker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mFlingTracker:Lcom/android/server/input/fling/FlingTracker;

    .line 137
    return-void
.end method

.method private beforeNotifyMotion(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 633
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-eqz v0, :cond_0

    .line 634
    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->processMotionEventForQuickNote(Landroid/view/MotionEvent;)V

    .line 636
    :cond_0
    return-void
.end method

.method private ensureMiuiMagicPointerServiceInit()Z
    .locals 2

    .line 840
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 841
    return v1

    .line 843
    :cond_0
    const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 845
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private getKeyboardStatus(Lmiui/hardware/input/MiuiKeyboardStatus;)Ljava/lang/String;
    .locals 3
    .param p1, "status"    # Lmiui/hardware/input/MiuiKeyboardStatus;

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Keyboard is Normal"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 329
    .local v0, "result":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->shouldIgnoreKeyboard()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 330
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 331
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isAngleStatusWork()Z

    move-result v1

    if-nez v1, :cond_0

    .line 332
    const-string v1, "AngleStatus Exception "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 333
    :cond_0
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isAuthStatus()Z

    move-result v1

    if-nez v1, :cond_1

    .line 334
    const-string v1, "AuthStatus Exception "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 335
    :cond_1
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isLidStatus()Z

    move-result v1

    if-nez v1, :cond_2

    .line 336
    const-string v1, "LidStatus Exception "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 337
    :cond_2
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isTabletStatus()Z

    move-result v1

    if-nez v1, :cond_3

    .line 338
    const-string v1, "TabletStatus Exception "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 339
    :cond_3
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isConnected()Z

    move-result v1

    if-nez v1, :cond_4

    .line 340
    const-string v1, "The Keyboard is disConnect "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    :cond_4
    :goto_0
    const-string v1, ", MCU:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->getMCUVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Keyboard:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 344
    invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->getKeyboardVersion()Ljava/lang/String;

    move-result-object v2

    .line 343
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static isSystemApp(I)Z
    .locals 1
    .param p0, "pid"    # I

    .line 714
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemApp(I)Z

    move-result v0

    return v0
.end method

.method private isXiaomiKeyboard(II)I
    .locals 1
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I

    .line 656
    invoke-static {p2, p1}, Lmiui/hardware/input/MiuiKeyboardHelper;->getXiaomiKeyboardType(II)I

    move-result v0

    return v0
.end method

.method private isXiaomiStylus(II)I
    .locals 1
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I

    .line 651
    invoke-static {p1, p2}, Landroid/view/InputDevice;->isXiaomiStylus(II)I

    move-result v0

    return v0
.end method

.method private isXiaomiTouchpad(II)I
    .locals 1
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I

    .line 661
    invoke-static {p2, p1}, Lmiui/hardware/input/MiuiKeyboardHelper;->getXiaomiTouchPadType(II)I

    move-result v0

    return v0
.end method

.method private synthetic lambda$new$0()V
    .locals 5

    .line 98
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->userActivity(JII)V

    return-void
.end method

.method private synthetic lambda$sendMotionEventToNotify$1(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 548
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListenersToNotify:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 549
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 550
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 551
    iget-object v2, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListenersToNotify:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 553
    .end local v1    # "i":I
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListenersToNotify:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;

    .line 555
    .local v1, "record":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
    invoke-virtual {v1, p1}, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->notifyMotionEvent(Landroid/view/MotionEvent;)V

    .line 556
    .end local v1    # "record":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
    goto :goto_1

    .line 557
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 558
    return-void

    .line 553
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method static synthetic lambda$updateDisplayViewport$3(Ljava/util/List;Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;)V
    .locals 0
    .param p0, "viewports"    # Ljava/util/List;
    .param p1, "s"    # Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 600
    invoke-virtual {p1, p0}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->setDisplayViewports(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$updatePointerDisplayId$2(ILcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;)V
    .locals 0
    .param p0, "displayId"    # I
    .param p1, "s"    # Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 591
    invoke-virtual {p1, p0}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->updatePointerDisplayId(I)V

    return-void
.end method

.method private notifyTouchMotionEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 640
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    if-nez v0, :cond_0

    .line 641
    const-class v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    .line 644
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mShoulderKeyManagerInternal:Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;

    if-eqz v0, :cond_1

    .line 645
    invoke-interface {v0, p1}, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal;->notifyTouchMotionEvent(Landroid/view/MotionEvent;)V

    .line 647
    :cond_1
    return-void
.end method

.method private removeListenerFromSystem(I)V
    .locals 2
    .param p1, "pid"    # I

    .line 402
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiShortcutSettingsLock:Ljava/lang/Object;

    monitor-enter v0

    .line 403
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 404
    monitor-exit v0

    .line 405
    return-void

    .line 404
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private removeMotionEventListener(I)V
    .locals 3
    .param p1, "pid"    # I

    .line 529
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 530
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->contains(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 531
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 532
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 533
    const-string v1, "MiuiInputManagerService"

    const-string/jumbo v2, "unregister pointer event listener"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    iget-object v2, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiGestureListener:Lcom/miui/server/input/gesture/MiuiGestureListener;

    invoke-virtual {v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 537
    :cond_0
    monitor-exit v0

    .line 538
    return-void

    .line 537
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private sendMotionEventToNotify(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 541
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 542
    .local v0, "action":I
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 544
    return-void

    .line 546
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;

    move-result-object v1

    .line 547
    .local v1, "event":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/android/server/input/MiuiInputManagerService;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v1}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/MiuiInputManagerService;Landroid/view/MotionEvent;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 559
    return-void
.end method

.method private sendSettingsChangedMessage(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "messageWhat"    # I
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "newFunction"    # Ljava/lang/String;

    .line 408
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 409
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "action"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v1, "function"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 412
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 413
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 414
    return-void
.end method

.method private setMagicPointerPosition(FF)V
    .locals 1
    .param p1, "pointerX"    # F
    .param p2, "pointerY"    # F

    .line 813
    invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerService;->ensureMiuiMagicPointerServiceInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814
    return-void

    .line 816
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->updateMagicPointerPosition(FF)V

    .line 817
    return-void
.end method

.method private setMagicPointerVisibility(Z)V
    .locals 1
    .param p1, "visibility"    # Z

    .line 825
    invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerService;->ensureMiuiMagicPointerServiceInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    return-void

    .line 828
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->setMagicPointerVisibility(Z)V

    .line 829
    return-void
.end method

.method private setScreenState(Z)V
    .locals 4
    .param p1, "isScreenOn"    # Z

    .line 615
    iput-boolean p1, p0, Lcom/android/server/input/MiuiInputManagerService;->mIsScreenOn:Z

    .line 616
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setScreenState(Z)V

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-nez v0, :cond_1

    .line 620
    return-void

    .line 622
    :cond_1
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 625
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    xor-int/lit8 v1, p1, 0x1

    .line 626
    .local v1, "needSyncMotion":Z
    invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputCommonConfig;->setNeedSyncMotion(Z)V

    .line 627
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 628
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Flush needSyncMotion to native, value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiInputManagerService"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 849
    const-string v0, "MI INPUT MANAGER (dumpsys input)\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    invoke-virtual {v0}, Lcom/android/server/input/NativeMiuiInputManagerService;->dump()Ljava/lang/String;

    move-result-object v0

    .line 851
    .local v0, "dumpStr":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 852
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 854
    :cond_0
    const-string v1, "Mi InputManagerService (Java) State:\n"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 855
    invoke-static {}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->getInstance()Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->dump(Ljava/io/PrintWriter;)V

    .line 856
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 857
    invoke-static {}, Lcom/android/server/input/TouchWakeUpFeatureManager;->getInstance()Lcom/android/server/input/TouchWakeUpFeatureManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, p1, v2}, Lcom/android/server/input/TouchWakeUpFeatureManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 858
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 859
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 860
    return-void
.end method

.method public getAppScrollerOptimizationConfig(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 261
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getInstance()Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    move-result-object v0

    .line 262
    invoke-virtual {v0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getAppScrollerOptimizationConfigAndSwitchState(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 261
    return-object v0
.end method

.method public getDefaultKeyboardShortcutInfos()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation

    .line 181
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 182
    const-string v0, "Not Support normal application get keyboard Default shortcut"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-object v1

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    if-eqz v0, :cond_1

    .line 186
    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getDefaultKeyboardShortcutInfos()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 188
    :cond_1
    const-string v0, "Can\'t get Keyboard ShortcutKey because Not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    return-object v1
.end method

.method public getEdgeSuppressionSize(Z)[I
    .locals 3
    .param p1, "isAbsolute"    # Z

    .line 212
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 213
    const-string v0, "Not Support normal application get edge suppression size"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    return-object v1

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    if-eqz v0, :cond_2

    .line 217
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getAbsoluteLevel()[I

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_1
    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getConditionLevel()[I

    move-result-object v0

    .line 217
    :goto_0
    return-object v0

    .line 220
    :cond_2
    const-string v0, "Can\'t get EdgeSuppression Info because not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    return-object v1
.end method

.method public getInputMethodSizeScope()[I
    .locals 3

    .line 227
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 228
    const-string v0, "Not Support normal application get inputmethod size"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    return-object v1

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    if-eqz v0, :cond_1

    .line 232
    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getInputMethodSizeScope()[I

    move-result-object v0

    return-object v0

    .line 234
    :cond_1
    const-string v0, "Can\'t get inputmethod size because not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    return-object v1
.end method

.method public getKeyboardBackLightBrightness()I
    .locals 3

    .line 291
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const/4 v1, -0x1

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 292
    const-string v0, "Not Support normal application interaction gesture shortcut"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    return v1

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_2

    .line 300
    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardBackLightBrightness()I

    move-result v0

    return v0

    .line 302
    :cond_2
    const-string v0, "get padKeyboard backLight fail because Not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    return v1
.end method

.method public getKeyboardShortcut()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation

    .line 167
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 168
    const-string v0, "Not Support normal application get keyboard shortcut"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return-object v1

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getKeyboardShortcutInfo()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 174
    :cond_1
    const-string v0, "Can\'t get Keyboard ShortcutKey because Not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    return-object v1
.end method

.method public getKeyboardType()I
    .locals 5

    .line 309
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemApp(I)Z

    move-result v0

    const/4 v1, -0x1

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "the app is not system :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 311
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v3

    .line 312
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    .line 311
    invoke-virtual {v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemApp(I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 310
    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const-string v0, "Not Support normal application interaction gesture shortcut"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    return v1

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_2

    .line 320
    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardType()I

    move-result v0

    return v0

    .line 322
    :cond_2
    const-string v0, "get padKeyboard type fail because Not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    return v1
.end method

.method public getMiKeyboardStatus()Ljava/lang/String;
    .locals 3

    .line 195
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 196
    const-string v0, "Not Support normal application get keyboard status"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    return-object v1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_2

    .line 203
    invoke-interface {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardStatus()Lmiui/hardware/input/MiuiKeyboardStatus;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/input/MiuiInputManagerService;->getKeyboardStatus(Lmiui/hardware/input/MiuiKeyboardStatus;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 205
    :cond_2
    const-string v0, "Get Keyboard Status fail because Not Support"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    return-object v1
.end method

.method public hideCursor()Z
    .locals 2

    .line 251
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const-string v1, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 252
    const-string v0, "Not Support normal application hide cursor"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const/4 v0, 0x0

    return v0

    .line 255
    :cond_0
    const-string v0, "hideCursor"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    invoke-virtual {v0}, Lcom/android/server/input/NativeMiuiInputManagerService;->hideCursor()Z

    move-result v0

    return v0
.end method

.method public notifyDeviceShareListenerSocketBroken(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 672
    invoke-static {}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->getInstance()Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->onSocketBroken(I)V

    .line 673
    return-void
.end method

.method public obtainLaserPointerController()Lcom/miui/server/input/stylus/laser/PointerControllerInterface;
    .locals 2

    .line 605
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    if-nez v0, :cond_0

    .line 606
    new-instance v0, Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    .line 607
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    iget v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayId(I)V

    .line 608
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mDisplayViewports:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayViewPort(Ljava/util/List;)V

    .line 609
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    iget-boolean v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mIsScreenOn:Z

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setScreenState(Z)V

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    return-object v0
.end method

.method public pokeUserActivity()V
    .locals 2

    .line 677
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPokeUserActivityRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 678
    return-void
.end method

.method public putStringForUser(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 350
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const-string v1, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 351
    const-string v0, "Not Support normal application interaction gesture shortcut"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const/4 v0, 0x0

    return v0

    .line 354
    :cond_0
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v0

    .line 355
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    .line 354
    invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameForPid(I)Ljava/lang/String;

    move-result-object v0

    .line 356
    .local v0, "targetPackage":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed Action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",new Function:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",because for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 359
    .local v1, "origId":J
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, -0x2

    invoke-static {v3, p1, p2, v4}, Landroid/provider/MiuiSettings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 361
    const/4 v3, 0x1

    invoke-direct {p0, v3, p1, p2}, Lcom/android/server/input/MiuiInputManagerService;->sendSettingsChangedMessage(ILjava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 363
    return v3
.end method

.method public registerAppScrollerOptimizationConfigListener(Ljava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;

    .line 268
    invoke-static {}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getInstance()Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerAppScrollerOptimizationConfigListener(Ljava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V

    .line 270
    return-void
.end method

.method public registerMiuiMotionEventListener(Lmiui/hardware/input/IMiuiMotionEventListener;)V
    .locals 6
    .param p1, "listener"    # Lmiui/hardware/input/IMiuiMotionEventListener;

    .line 484
    if-eqz p1, :cond_3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 490
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 491
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 492
    const-string v2, "MiuiInputManagerService"

    const-string v3, "Can\'t register repeat listener to motion event"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    monitor-exit v0

    return-void

    .line 496
    :cond_1
    new-instance v2, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;

    invoke-direct {v2, p0, p1, v1}, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;-><init>(Lcom/android/server/input/MiuiInputManagerService;Lmiui/hardware/input/IMiuiMotionEventListener;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    .local v2, "motionEventListenerRecord":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
    :try_start_1
    invoke-interface {p1}, Lmiui/hardware/input/IMiuiMotionEventListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 500
    .local v3, "binder":Landroid/os/IBinder;
    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 503
    .end local v3    # "binder":Landroid/os/IBinder;
    goto :goto_0

    .line 501
    :catch_0
    move-exception v3

    .line 502
    .local v3, "ex":Landroid/os/RemoteException;
    :try_start_2
    const-string v4, "MiuiInputManagerService"

    const-string v5, "Can\'t linkToDeath because IllegalStateException"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    .end local v3    # "ex":Landroid/os/RemoteException;
    :goto_0
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 505
    const-string v3, "MiuiInputManagerService"

    const-string v4, "register pointer event listener"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    iget-object v4, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiGestureListener:Lcom/miui/server/input/gesture/MiuiGestureListener;

    invoke-virtual {v3, v4}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 508
    :cond_2
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 509
    .end local v1    # "callingPid":I
    .end local v2    # "motionEventListenerRecord":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
    monitor-exit v0

    .line 510
    return-void

    .line 509
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 485
    :cond_3
    :goto_1
    const-string v0, "MiuiInputManagerService"

    const-string v1, "Not Support normal application interaction motion event"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    return-void
.end method

.method public registerShortcutChangedListener(Lmiui/hardware/input/IShortcutSettingsChangedListener;)V
    .locals 5
    .param p1, "listener"    # Lmiui/hardware/input/IShortcutSettingsChangedListener;

    .line 368
    if-eqz p1, :cond_2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiShortcutSettingsLock:Ljava/lang/Object;

    monitor-enter v0

    .line 374
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    .line 375
    .local v1, "callingPid":I
    iget-object v2, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 380
    new-instance v2, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;

    invoke-direct {v2, p0, p1, v1}, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;-><init>(Lcom/android/server/input/MiuiInputManagerService;Lmiui/hardware/input/IShortcutSettingsChangedListener;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    .local v2, "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
    :try_start_1
    invoke-interface {p1}, Lmiui/hardware/input/IShortcutSettingsChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 384
    .local v3, "binder":Landroid/os/IBinder;
    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    .end local v3    # "binder":Landroid/os/IBinder;
    nop

    .line 388
    :try_start_2
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService;->mShortcutListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 389
    .end local v1    # "callingPid":I
    .end local v2    # "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
    monitor-exit v0

    .line 390
    return-void

    .line 385
    .restart local v1    # "callingPid":I
    .restart local v2    # "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
    :catch_0
    move-exception v3

    .line 386
    .local v3, "ex":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/input/MiuiInputManagerService;
    .end local p1    # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener;
    throw v4

    .line 376
    .end local v2    # "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
    .end local v3    # "ex":Landroid/os/RemoteException;
    .restart local p0    # "this":Lcom/android/server/input/MiuiInputManagerService;
    .restart local p1    # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener;
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Can\'t register repeat listener to MiuiShortcutSettings"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/input/MiuiInputManagerService;
    .end local p1    # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener;
    throw v2

    .line 389
    .end local v1    # "callingPid":I
    .restart local p0    # "this":Lcom/android/server/input/MiuiInputManagerService;
    .restart local p1    # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 369
    :cond_2
    :goto_0
    const-string v0, "MiuiInputManagerService"

    const-string v1, "Not Support normal application interaction gesture shortcut"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    return-void
.end method

.method public reportFlingEvent(Ljava/lang/String;II)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "downTimes"    # I
    .param p3, "flingTimes"    # I

    .line 523
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMotionEventLock:Ljava/lang/Object;

    monitor-enter v0

    .line 524
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mFlingTracker:Lcom/android/server/input/fling/FlingTracker;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/input/fling/FlingTracker;->trackEvent(Ljava/lang/String;II)V

    .line 525
    monitor-exit v0

    .line 526
    return-void

    .line 525
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public requestRedirect(I)V
    .locals 2
    .param p1, "motionEventId"    # I

    .line 418
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "app requestRedirect, motionEventId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiInputManagerService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    const-string v0, "Not Support non-system application requestRedirect"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    return-void

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/input/NativeMiuiInputManagerService;->requestRedirect(II)V

    .line 424
    return-void
.end method

.method public setCursorPosition(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 241
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const-string v1, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 242
    const-string v0, "Not Support normal application set cursor position"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v0, 0x0

    return v0

    .line 245
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setCursorPosition("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/NativeMiuiInputManagerService;->setCursorPosition(FF)Z

    move-result v0

    return v0
.end method

.method public setDeviceShareListener(Landroid/os/ParcelFileDescriptor;ILmiui/hardware/input/IDeviceShareStateChangedListener;)V
    .locals 2
    .param p1, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "flags"    # I
    .param p3, "listener"    # Lmiui/hardware/input/IDeviceShareStateChangedListener;

    .line 464
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    const-string v0, "MiuiInputManagerService"

    const-string v1, "Not Support normal application set device share listener :("

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    return-void

    .line 468
    :cond_0
    invoke-static {}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->getInstance()Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->setDeviceShareListener(ILandroid/os/ParcelFileDescriptor;ILmiui/hardware/input/IDeviceShareStateChangedListener;)V

    .line 470
    return-void
.end method

.method public setHasEditTextOnScreen(Z)V
    .locals 1
    .param p1, "hasEditText"    # Z

    .line 456
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->setHasEditTextOnScreen(Z)V

    .line 457
    return-void
.end method

.method public setInteractive(Z)V
    .locals 1
    .param p1, "interactive"    # Z

    .line 103
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mNative:Lcom/android/server/input/NativeMiuiInputManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/input/NativeMiuiInputManagerService;->setInteractive(Z)V

    .line 104
    return-void
.end method

.method public setKeyboardBackLightBrightness(I)V
    .locals 2
    .param p1, "brightness"    # I

    .line 274
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const-string v1, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 275
    const-string v0, "Not Support normal application interaction gesture shortcut"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->getKeyboardManager(Landroid/content/Context;)Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-eqz v0, :cond_2

    .line 282
    invoke-interface {v0, p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->setKeyboardBackLightBrightness(I)V

    goto :goto_0

    .line 284
    :cond_2
    const-string/jumbo v0, "set padKeyboard backLight fail because Not Support"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :goto_0
    return-void
.end method

.method public setTouchpadButtonState(IZ)V
    .locals 2
    .param p1, "deviceId"    # I
    .param p2, "isDown"    # Z

    .line 474
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    const-string v0, "MiuiInputManagerService"

    const-string v1, "Not Support normal application set touchpad button state :("

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    return-void

    .line 478
    :cond_0
    invoke-static {}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->getInstance()Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->setTouchpadButtonState(IIZ)V

    .line 480
    return-void
.end method

.method start()V
    .locals 1

    .line 140
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 143
    :cond_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/ITouchFeature;->hasSupportEdgeMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mEdgeSuppressionManager:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 146
    :cond_1
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportOffScreenQuickNote()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getInstance()Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiStylusShortcutManager:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mFlingTracker:Lcom/android/server/input/fling/FlingTracker;

    invoke-virtual {v0}, Lcom/android/server/input/fling/FlingTracker;->registerPointerEventListener()V

    .line 150
    return-void
.end method

.method public trackTouchpadEvent(III)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .line 682
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->trackTouchpadEvent(III)V

    .line 683
    return-void
.end method

.method public unregisterMiuiMotionEventListener()V
    .locals 2

    .line 514
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    const-string v0, "MiuiInputManagerService"

    const-string v1, "Not Support normal application interaction motion event"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    return-void

    .line 518
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/input/MiuiInputManagerService;->removeMotionEventListener(I)V

    .line 519
    return-void
.end method

.method public unregisterShortcutChangedListener()V
    .locals 2

    .line 394
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    const-string v0, "MiuiInputManagerService"

    const-string v1, "Not Support normal application interaction gesture shortcut"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    return-void

    .line 398
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/input/MiuiInputManagerService;->removeListenerFromSystem(I)V

    .line 399
    return-void
.end method

.method public updateDisplayViewport(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;)V"
        }
    .end annotation

    .line 595
    .local p1, "viewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;"
    iput-object p1, p0, Lcom/android/server/input/MiuiInputManagerService;->mDisplayViewports:Ljava/util/List;

    .line 596
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayViewPort(Ljava/util/List;)V

    .line 599
    :cond_0
    const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v0}, Ljava/util/Optional;->ofNullable(Ljava/lang/Object;)Ljava/util/Optional;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda3;-><init>(Ljava/util/List;)V

    .line 600
    invoke-virtual {v0, v1}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 601
    return-void
.end method

.method public updateKeyboardShortcut(Landroid/view/KeyboardShortcutInfo;I)V
    .locals 2
    .param p1, "info"    # Landroid/view/KeyboardShortcutInfo;
    .param p2, "type"    # I

    .line 154
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->isSystemApp(I)Z

    move-result v0

    const-string v1, "MiuiInputManagerService"

    if-nez v0, :cond_0

    .line 155
    const-string v0, "Not Support normal application update keyboard shortcut"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->updateKeyboardShortcut(Landroid/view/KeyboardShortcutInfo;I)V

    goto :goto_0

    .line 161
    :cond_1
    const-string v0, "Can\'t update Keyboard ShortcutKey because Not Support"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :goto_0
    return-void
.end method

.method public updatePointerDisplayId(I)V
    .locals 2
    .param p1, "displayId"    # I

    .line 586
    iput p1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I

    .line 587
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayId(I)V

    .line 590
    :cond_0
    const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v0}, Ljava/util/Optional;->ofNullable(Ljava/lang/Object;)Ljava/util/Optional;

    move-result-object v0

    new-instance v1, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda4;

    invoke-direct {v1, p1}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda4;-><init>(I)V

    .line 591
    invoke-virtual {v0, v1}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 592
    return-void
.end method
