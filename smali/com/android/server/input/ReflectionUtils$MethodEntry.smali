.class Lcom/android/server/input/ReflectionUtils$MethodEntry;
.super Ljava/lang/Object;
.source "ReflectionUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/ReflectionUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MethodEntry"
.end annotation


# instance fields
.field mClassName:Ljava/lang/Class;

.field mMethod:Ljava/lang/reflect/Method;

.field mMethodName:Ljava/lang/String;

.field mParmasClass:[Ljava/lang/Class;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    return-void
.end method

.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V
    .locals 0
    .param p1, "className"    # Ljava/lang/Class;
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "parmasClass"    # [Ljava/lang/Class;
    .param p4, "method"    # Ljava/lang/reflect/Method;

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mClassName:Ljava/lang/Class;

    .line 164
    iput-object p2, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethodName:Ljava/lang/String;

    .line 165
    iput-object p3, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mParmasClass:[Ljava/lang/Class;

    .line 166
    iput-object p4, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethod:Ljava/lang/reflect/Method;

    .line 167
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 171
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 172
    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 173
    :cond_1
    move-object v2, p1

    check-cast v2, Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 174
    .local v2, "that":Lcom/android/server/input/ReflectionUtils$MethodEntry;
    iget-object v3, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mParmasClass:[Ljava/lang/Class;

    iget-object v4, v2, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mParmasClass:[Ljava/lang/Class;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethodName:Ljava/lang/String;

    iget-object v4, v2, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethodName:Ljava/lang/String;

    .line 175
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mClassName:Ljava/lang/Class;

    iget-object v4, v2, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mClassName:Ljava/lang/Class;

    .line 176
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 174
    :goto_0
    return v0

    .line 172
    .end local v2    # "that":Lcom/android/server/input/ReflectionUtils$MethodEntry;
    :cond_3
    :goto_1
    return v1
.end method

.method public getClassName()Ljava/lang/Class;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mClassName:Ljava/lang/Class;

    return-object v0
.end method

.method public getMethodName()Ljava/lang/String;
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethodName:Ljava/lang/String;

    return-object v0
.end method

.method public getParmasClass()[Ljava/lang/Class;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mParmasClass:[Ljava/lang/Class;

    return-object v0
.end method

.method public getmMethod()Ljava/lang/reflect/Method;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethodName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mClassName:Ljava/lang/Class;

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    .line 182
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mParmasClass:[Ljava/lang/Class;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v1, v2

    .line 183
    .end local v0    # "result":I
    .local v1, "result":I
    return v1
.end method

.method public setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
    .locals 0
    .param p1, "className"    # Ljava/lang/Class;

    .line 197
    iput-object p1, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mClassName:Ljava/lang/Class;

    .line 198
    return-object p0
.end method

.method public setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
    .locals 0
    .param p1, "methodName"    # Ljava/lang/String;

    .line 192
    iput-object p1, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethodName:Ljava/lang/String;

    .line 193
    return-object p0
.end method

.method public setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
    .locals 0
    .param p1, "parmasClass"    # [Ljava/lang/Class;

    .line 187
    iput-object p1, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mParmasClass:[Ljava/lang/Class;

    .line 188
    return-object p0
.end method

.method public setmMethod(Ljava/lang/reflect/Method;)V
    .locals 0
    .param p1, "mMethod"    # Ljava/lang/reflect/Method;

    .line 202
    iput-object p1, p0, Lcom/android/server/input/ReflectionUtils$MethodEntry;->mMethod:Ljava/lang/reflect/Method;

    .line 203
    return-void
.end method
