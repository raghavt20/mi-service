.class public Lcom/android/server/input/ReflectionUtils;
.super Ljava/lang/Object;
.source "ReflectionUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/ReflectionUtils$MethodEntry;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "InputFeatureReflection"

.field private static sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

.field private static volatile sMethodEntryMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/android/server/input/ReflectionUtils$MethodEntry;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 15
    new-instance v0, Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-direct {v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>()V

    sput-object v0, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    .line 19
    const-class v0, Lcom/android/server/input/InputManagerService;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    filled-new-array {v1, v2}, [Ljava/lang/Class;

    move-result-object v1

    const-string v2, "nativeSwitchTouchWorkMode"

    invoke-static {v0, v2, v1}, Lcom/android/server/input/ReflectionUtils;->initMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 21
    const-class v0, Lcom/android/server/input/InputManagerService;

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    filled-new-array {v1, v2}, [Ljava/lang/Class;

    move-result-object v1

    const-string v2, "nativeSetDebugInput"

    invoke-static {v0, v2, v1}, Lcom/android/server/input/ReflectionUtils;->initMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs callPrivateMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p0, "whichClass"    # Ljava/lang/Class;
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "params"    # [Ljava/lang/Object;

    .line 68
    invoke-static {p3}, Lcom/android/server/input/ReflectionUtils;->getParameterTypes([Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v0

    .line 69
    .local v0, "paramsClass":[Ljava/lang/Class;
    sget-object v1, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 72
    const/4 v1, 0x0

    :try_start_0
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v3, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 73
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v4, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Method;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 74
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v3, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Method;

    invoke-virtual {v2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 76
    :cond_0
    invoke-virtual {p0, p2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 77
    .local v2, "method":Ljava/lang/reflect/Method;
    if-nez v2, :cond_1

    .line 78
    return-object v1

    .line 80
    :cond_1
    sget-object v4, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :try_start_1
    sget-object v5, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    new-instance v6, Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 82
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v6, v7, p2, v0, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 81
    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    :try_start_2
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 87
    invoke-virtual {v2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v1

    .line 84
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "paramsClass":[Ljava/lang/Class;
    .end local p0    # "whichClass":Ljava/lang/Class;
    .end local p1    # "obj":Ljava/lang/Object;
    .end local p2    # "methodName":Ljava/lang/String;
    .end local p3    # "params":[Ljava/lang/Object;
    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 88
    .end local v2    # "method":Ljava/lang/reflect/Method;
    .restart local v0    # "paramsClass":[Ljava/lang/Class;
    .restart local p0    # "whichClass":Ljava/lang/Class;
    .restart local p1    # "obj":Ljava/lang/Object;
    .restart local p2    # "methodName":Ljava/lang/String;
    .restart local p3    # "params":[Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 89
    .local v2, "ex":Ljava/lang/Exception;
    const-string v3, "InputFeatureReflection"

    const-string v4, "callPrivateMethod Fail!"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    return-object v1
.end method

.method public static varargs callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "params"    # [Ljava/lang/Object;

    .line 40
    invoke-static {p2}, Lcom/android/server/input/ReflectionUtils;->getParameterTypes([Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v0

    .line 41
    .local v0, "paramsClass":[Ljava/lang/Class;
    sget-object v1, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 44
    const/4 v1, 0x0

    :try_start_0
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v3, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 45
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v4, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Method;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 46
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v3, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Method;

    invoke-virtual {v2, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .line 48
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 49
    .local v2, "method":Ljava/lang/reflect/Method;
    if-nez v2, :cond_1

    .line 50
    return-object v1

    .line 52
    :cond_1
    sget-object v4, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :try_start_1
    sget-object v5, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    new-instance v6, Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 54
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v6, v7, p1, v0, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    .line 53
    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    :try_start_2
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 59
    invoke-virtual {v2, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v1

    .line 56
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "paramsClass":[Ljava/lang/Class;
    .end local p0    # "obj":Ljava/lang/Object;
    .end local p1    # "methodName":Ljava/lang/String;
    .end local p2    # "params":[Ljava/lang/Object;
    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 60
    .end local v2    # "method":Ljava/lang/reflect/Method;
    .restart local v0    # "paramsClass":[Ljava/lang/Class;
    .restart local p0    # "obj":Ljava/lang/Object;
    .restart local p1    # "methodName":Ljava/lang/String;
    .restart local p2    # "params":[Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 61
    .local v2, "ex":Ljava/lang/Exception;
    const-string v3, "InputFeatureReflection"

    const-string v4, "callPrivateMethod Fail!"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-object v1
.end method

.method public static varargs callStaticMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p0, "whichClass"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "paramsClass"    # [Ljava/lang/Class;
    .param p3, "params"    # [Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-virtual {v0, p0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;

    .line 104
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 105
    sget-object v1, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v3, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 106
    sget-object v1, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntry:Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Method;

    invoke-virtual {v1, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 108
    :cond_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 109
    .local v1, "method":Ljava/lang/reflect/Method;
    if-nez v1, :cond_1

    .line 110
    return-object v0

    .line 112
    :cond_1
    sget-object v3, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :try_start_1
    sget-object v4, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    new-instance v5, Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-direct {v5, p0, p1, p2, v1}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    :try_start_2
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 117
    invoke-virtual {v1, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v0

    .line 114
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local p0    # "whichClass":Ljava/lang/Class;
    .end local p1    # "methodName":Ljava/lang/String;
    .end local p2    # "paramsClass":[Ljava/lang/Class;
    .end local p3    # "params":[Ljava/lang/Object;
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 118
    .end local v1    # "method":Ljava/lang/reflect/Method;
    .restart local p0    # "whichClass":Ljava/lang/Class;
    .restart local p1    # "methodName":Ljava/lang/String;
    .restart local p2    # "paramsClass":[Ljava/lang/Class;
    .restart local p3    # "params":[Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 119
    .local v1, "ex":Ljava/lang/Exception;
    const-string v2, "InputFeatureReflection"

    const-string v3, "callStaticMethod Fail!"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return-object v0
.end method

.method public static varargs callStaticMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "whichClass"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "params"    # [Ljava/lang/Object;

    .line 95
    invoke-static {p2}, Lcom/android/server/input/ReflectionUtils;->getParameterTypes([Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v0

    .line 96
    .local v0, "paramsClass":[Ljava/lang/Class;
    invoke-static {p0, p1, v0, p2}, Lcom/android/server/input/ReflectionUtils;->callStaticMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private static varargs getParameterTypes([Ljava/lang/Object;)[Ljava/lang/Class;
    .locals 4
    .param p0, "args"    # [Ljava/lang/Object;

    .line 125
    if-nez p0, :cond_0

    .line 126
    const/4 v0, 0x0

    return-object v0

    .line 128
    :cond_0
    array-length v0, p0

    new-array v0, v0, [Ljava/lang/Class;

    .line 129
    .local v0, "parameterTypes":[Ljava/lang/Class;
    const/4 v1, 0x0

    .local v1, "i":I
    array-length v2, p0

    .local v2, "j":I
    :goto_0
    if-ge v1, v2, :cond_9

    .line 130
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 131
    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 132
    :cond_1
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Byte;

    if-eqz v3, :cond_2

    .line 133
    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 134
    :cond_2
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Short;

    if-eqz v3, :cond_3

    .line 135
    sget-object v3, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 136
    :cond_3
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Float;

    if-eqz v3, :cond_4

    .line 137
    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 138
    :cond_4
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Double;

    if-eqz v3, :cond_5

    .line 139
    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 140
    :cond_5
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Character;

    if-eqz v3, :cond_6

    .line 141
    sget-object v3, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 142
    :cond_6
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Long;

    if-eqz v3, :cond_7

    .line 143
    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 144
    :cond_7
    aget-object v3, p0, v1

    instance-of v3, v3, Ljava/lang/Boolean;

    if-eqz v3, :cond_8

    .line 145
    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v1

    goto :goto_1

    .line 147
    :cond_8
    aget-object v3, p0, v1

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v0, v1

    .line 129
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    .end local v1    # "i":I
    .end local v2    # "j":I
    :cond_9
    return-object v0
.end method

.method private static initMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V
    .locals 4
    .param p0, "whichClass"    # Ljava/lang/Class;
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "paramsClass"    # [Ljava/lang/Class;

    .line 27
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 28
    .local v0, "method":Ljava/lang/reflect/Method;
    if-eqz v0, :cond_0

    .line 29
    sget-object v1, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :try_start_1
    sget-object v2, Lcom/android/server/input/ReflectionUtils;->sMethodEntryMap:Ljava/util/Map;

    new-instance v3, Lcom/android/server/input/ReflectionUtils$MethodEntry;

    invoke-direct {v3, p0, p1, p2, v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "whichClass":Ljava/lang/Class;
    .end local p1    # "methodName":Ljava/lang/String;
    .end local p2    # "paramsClass":[Ljava/lang/Class;
    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 35
    .end local v0    # "method":Ljava/lang/reflect/Method;
    .restart local p0    # "whichClass":Ljava/lang/Class;
    .restart local p1    # "methodName":Ljava/lang/String;
    .restart local p2    # "paramsClass":[Ljava/lang/Class;
    :cond_0
    :goto_0
    goto :goto_1

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "InputFeatureReflection"

    const-string v2, "Init method Fail!"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
