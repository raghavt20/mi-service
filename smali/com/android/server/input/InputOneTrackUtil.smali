.class public Lcom/android/server/input/InputOneTrackUtil;
.super Ljava/lang/Object;
.source "InputOneTrackUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;
    }
.end annotation


# static fields
.field private static final APP_ID_EDGE_SUPPRESSION:Ljava/lang/String; = "31000000735"

.field private static final APP_ID_EXTERNAL_DEVICE:Ljava/lang/String; = "31000000618"

.field private static final APP_ID_KEYBOARD_STYLUS:Ljava/lang/String; = "31000000824"

.field private static final APP_ID_SHORTCUT:Ljava/lang/String; = "31000401650"

.field private static final APP_ID_SHORTCUT_GLOBAL:Ljava/lang/String; = "31000401666"

.field private static final EVENT_KEYBOARD_DAU_NAME:Ljava/lang/String; = "keyboard_dau"

.field private static final EVENT_NAME_EXTERNAL_DEVICE:Ljava/lang/String; = "external_inputdevice"

.field private static final EVENT_SHORTCUT_KEY_USE_NAME:Ljava/lang/String; = "hotkeys_use"

.field private static final EXTERNAL_DEVICE_NAME:Ljava/lang/String; = "device_name"

.field private static final EXTERNAL_DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final KEYBOARD_6F_FUNCTION_TRIGGER_TIP:Ljava/lang/String; = "899.4.0.1.27102"

.field private static final KEYBOARD_DAU_TIP:Ljava/lang/String; = "899.7.0.1.27104"

.field private static final KEYBOARD_SHORTCUT_KEY_TRIGGER_TIP:Ljava/lang/String; = "899.4.0.1.20553"

.field private static final KEYBOARD_VOICE_TO_WORD_TRIGGER_TIP:Ljava/lang/String; = "899.4.0.1.27103"

.field private static final PACKAGE_NAME_EDGE_SUPPRESSION:Ljava/lang/String; = "com.xiaomi.edgesuppression"

.field private static final PACKAGE_NAME_EXTERNAL_DEVICE:Ljava/lang/String; = "com.xiaomi.padkeyboard"

.field private static final PACKAGE_NAME_KEYBOARD_STYLUS:Ljava/lang/String; = "com.xiaomi.extendDevice"

.field private static final PACKAGE_NAME_SHORTCUT:Ljava/lang/String; = "com.xiaomi.input.shortcut"

.field private static final PARAMS_KEYBOARD_DAU_NAME:Ljava/lang/String; = "connect_type"

.field private static final PARAMS_KEYBOARD_SHORTCUT_NAME:Ljava/lang/String; = "hotkeys_type"

.field private static final PENCIL_DAU_TRACK_TYPE:Ljava/lang/String; = "pencil_dau"

.field private static final SERVICE_NAME:Ljava/lang/String; = "com.miui.analytics.onetrack.TrackService"

.field private static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final STATUS_EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final STATUS_EVENT_TIP:Ljava/lang/String; = "tip"

.field private static final STYLUS_TRACK_TIP_PENCIL_DAU_TIP:Ljava/lang/String; = "899.6.0.1.27101"

.field private static final SUPPRESSION_FUNCTION_INITIATIVE:Ljava/lang/String; = "initiative_settings_by_user"

.field private static final SUPPRESSION_FUNCTION_SIZE:Ljava/lang/String; = "edge_size"

.field private static final SUPPRESSION_FUNCTION_SIZE_CHANGED:Ljava/lang/String; = "edge_size_changed"

.field private static final SUPPRESSION_FUNCTION_TYPE:Ljava/lang/String; = "edge_type"

.field private static final SUPPRESSION_FUNCTION_TYPE_CHANGED:Ljava/lang/String; = "edge_type_changed"

.field private static final SUPPRESSION_TRACK_TYPE:Ljava/lang/String; = "edge_suppression"

.field private static final TAG:Ljava/lang/String; = "InputOneTrackUtil"

.field private static volatile sInstance:Lcom/android/server/input/InputOneTrackUtil;


# instance fields
.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private final mExternalDeviceAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private final mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

.field private mIsBound:Z

.field private mService:Lcom/miui/analytics/ITrackBinder;


# direct methods
.method public static synthetic $r8$lambda$hTDN6ybx1zZZIK47Hry-shZCsHE(Lcom/android/server/input/InputOneTrackUtil;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/InputOneTrackUtil;->lambda$trackEvents$1(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$mkzJxLFc7_r4dBIFhR2E-g6V-yo(Lcom/android/server/input/InputOneTrackUtil;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/InputOneTrackUtil;->lambda$trackEvent$0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmInputDeviceOneTrack(Lcom/android/server/input/InputOneTrackUtil;)Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/input/InputOneTrackUtil;)Lcom/miui/analytics/ITrackBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/InputOneTrackUtil;->mService:Lcom/miui/analytics/ITrackBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmService(Lcom/android/server/input/InputOneTrackUtil;Lcom/miui/analytics/ITrackBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/input/InputOneTrackUtil;->mService:Lcom/miui/analytics/ITrackBinder;

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAlarmForTrack(Lcom/android/server/input/InputOneTrackUtil;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->updateAlarmForTrack()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Lcom/android/server/input/InputOneTrackUtil$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/InputOneTrackUtil$1;-><init>(Lcom/android/server/input/InputOneTrackUtil;)V

    iput-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mExternalDeviceAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 126
    new-instance v0, Lcom/android/server/input/InputOneTrackUtil$2;

    invoke-direct {v0, p0}, Lcom/android/server/input/InputOneTrackUtil$2;-><init>(Lcom/android/server/input/InputOneTrackUtil;)V

    iput-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mConnection:Landroid/content/ServiceConnection;

    .line 109
    invoke-direct {p0, p1}, Lcom/android/server/input/InputOneTrackUtil;->bindTrackService(Landroid/content/Context;)V

    .line 110
    new-instance v0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    invoke-direct {v0}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    .line 111
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mContext:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mAlarmManager:Landroid/app/AlarmManager;

    .line 113
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->updateAlarmForTrack()V

    .line 116
    :cond_0
    return-void
.end method

.method private bindTrackService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 145
    iput-object p1, p0, Lcom/android/server/input/InputOneTrackUtil;->mContext:Landroid/content/Context;

    .line 146
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 147
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    const-string v2, "com.miui.analytics.onetrack.TrackService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    iget-object v1, p0, Lcom/android/server/input/InputOneTrackUtil;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/input/InputOneTrackUtil;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    sget-object v4, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/input/InputOneTrackUtil;->mIsBound:Z

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bindTrackService: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/input/InputOneTrackUtil;->mIsBound:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InputOneTrackUtil"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return-void
.end method

.method private getEdgeSuppressionStatusData(ZILjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "changedSize"    # I
    .param p3, "changedType"    # Ljava/lang/String;
    .param p4, "size"    # I
    .param p5, "type"    # Ljava/lang/String;

    .line 285
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 287
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    const-string v2, "edge_suppression"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 288
    const-string v1, "initiative_settings_by_user"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 289
    const-string v1, "edge_size_changed"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 290
    const-string v1, "edge_type_changed"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 291
    const-string v1, "edge_size"

    invoke-virtual {v0, v1, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 292
    const-string v1, "edge_type"

    invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    goto :goto_0

    .line 293
    :catch_0
    move-exception v1

    .line 294
    .local v1, "exception":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "construct TrackEvent data fail!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InputOneTrackUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    .end local v1    # "exception":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getEventDeviceType(Landroid/view/InputEvent;)Ljava/lang/String;
    .locals 3
    .param p0, "event"    # Landroid/view/InputEvent;

    .line 347
    const-string v0, ""

    .line 348
    .local v0, "type":Ljava/lang/String;
    const/16 v1, 0x4002

    invoke-virtual {p0, v1}, Landroid/view/InputEvent;->isFromSource(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    const-string v0, "STYLUS"

    goto :goto_0

    .line 350
    :cond_0
    invoke-virtual {p0}, Landroid/view/InputEvent;->getSource()I

    move-result v1

    const/16 v2, 0x101

    if-ne v1, v2, :cond_1

    .line 351
    const-string v0, "KEYBOARD"

    goto :goto_0

    .line 352
    :cond_1
    invoke-virtual {p0}, Landroid/view/InputEvent;->getSource()I

    move-result v1

    const/16 v2, 0x2002

    if-ne v1, v2, :cond_2

    .line 353
    const-string v0, "MOUSE"

    goto :goto_0

    .line 354
    :cond_2
    invoke-virtual {p0}, Landroid/view/InputEvent;->getSource()I

    move-result v1

    const v2, 0x100008

    if-ne v1, v2, :cond_3

    .line 355
    const-string v0, "TOUCHPAD"

    goto :goto_0

    .line 357
    :cond_3
    const-string v0, "UNKNOW"

    .line 359
    :goto_0
    return-object v0
.end method

.method private getExternalDeviceStatusData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 300
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 302
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    const-string v2, "external_inputdevice"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 303
    const-string v1, "device_type"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 304
    const-string v1, "device_name"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    goto :goto_0

    .line 305
    :catch_0
    move-exception v1

    .line 306
    .local v1, "exception":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "construct TrackEvent data fail!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InputOneTrackUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    .end local v1    # "exception":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 98
    sget-object v0, Lcom/android/server/input/InputOneTrackUtil;->sInstance:Lcom/android/server/input/InputOneTrackUtil;

    if-nez v0, :cond_1

    .line 99
    const-class v0, Lcom/android/server/input/InputOneTrackUtil;

    monitor-enter v0

    .line 100
    :try_start_0
    sget-object v1, Lcom/android/server/input/InputOneTrackUtil;->sInstance:Lcom/android/server/input/InputOneTrackUtil;

    if-nez v1, :cond_0

    .line 101
    new-instance v1, Lcom/android/server/input/InputOneTrackUtil;

    invoke-direct {v1, p0}, Lcom/android/server/input/InputOneTrackUtil;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/InputOneTrackUtil;->sInstance:Lcom/android/server/input/InputOneTrackUtil;

    .line 103
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 105
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/InputOneTrackUtil;->sInstance:Lcom/android/server/input/InputOneTrackUtil;

    return-object v0
.end method

.method private getKeyboardDAUStatusData(Z)Ljava/lang/String;
    .locals 4
    .param p1, "isIICConnected"    # Z

    .line 273
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 275
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    const-string v2, "keyboard_dau"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 276
    const-string v1, "connect_type"

    if-eqz p1, :cond_0

    const-string v2, "pogopin"

    goto :goto_0

    :cond_0
    const-string/jumbo v2, "\u84dd\u7259"

    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 277
    const-string/jumbo v1, "tip"

    const-string v2, "899.7.0.1.27104"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    goto :goto_1

    .line 278
    :catch_0
    move-exception v1

    .line 279
    .local v1, "exception":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "construct TrackEvent data fail!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InputOneTrackUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    .end local v1    # "exception":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getKeyboardShortcutKeyStatusData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "tip"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 261
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 263
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    const-string v2, "hotkeys_use"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 264
    const-string v1, "hotkeys_type"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 265
    const-string/jumbo v1, "tip"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    goto :goto_0

    .line 266
    :catch_0
    move-exception v1

    .line 267
    .local v1, "exception":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "construct TrackEvent data fail!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InputOneTrackUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    .end local v1    # "exception":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getShortcutAppId()Ljava/lang/String;
    .locals 1

    .line 256
    invoke-static {}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->isShortcutTrackForOneTrack()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "31000401650"

    goto :goto_0

    .line 257
    :cond_0
    const-string v0, "31000401666"

    .line 256
    :goto_0
    return-object v0
.end method

.method private getStylusDeviceStatusData()Ljava/lang/String;
    .locals 4

    .line 312
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 314
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    const-string v2, "pencil_dau"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 315
    const-string/jumbo v1, "tip"

    const-string v2, "899.6.0.1.27101"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    goto :goto_0

    .line 316
    :catch_0
    move-exception v1

    .line 317
    .local v1, "exception":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "construct TrackEvent data fail!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InputOneTrackUtil"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    .end local v1    # "exception":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private synthetic lambda$trackEvent$0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;
    .param p4, "flag"    # I

    .line 161
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mService:Lcom/miui/analytics/ITrackBinder;

    const-string v1, "InputOneTrackUtil"

    if-nez v0, :cond_0

    .line 162
    const-string/jumbo v0, "trackEvent: track service not bound"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/input/InputOneTrackUtil;->bindTrackService(Landroid/content/Context;)V

    .line 166
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mService:Lcom/miui/analytics/ITrackBinder;

    if-eqz v0, :cond_1

    .line 167
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 169
    :cond_1
    const-string/jumbo v0, "trackEvent: track service is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    goto :goto_1

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "trackEvent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private synthetic lambda$trackEvents$1(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "dataList"    # Ljava/util/List;
    .param p4, "flag"    # I

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "trackEvents: appId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " packageName "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputOneTrackUtil"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mService:Lcom/miui/analytics/ITrackBinder;

    if-nez v0, :cond_0

    .line 181
    const-string/jumbo v0, "trackEvents: track service not bound"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/input/InputOneTrackUtil;->bindTrackService(Landroid/content/Context;)V

    .line 185
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mService:Lcom/miui/analytics/ITrackBinder;

    if-eqz v0, :cond_1

    .line 186
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/analytics/ITrackBinder;->trackEvents(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    goto :goto_0

    .line 188
    :cond_1
    const-string/jumbo v0, "trackEvents: track service is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_0
    goto :goto_1

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "trackEvents: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method public static shouldCountDevice(Landroid/view/InputEvent;)Z
    .locals 4
    .param p0, "event"    # Landroid/view/InputEvent;

    .line 363
    invoke-virtual {p0}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 364
    .local v0, "device":Landroid/view/InputDevice;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 365
    return v1

    .line 367
    :cond_0
    invoke-virtual {v0}, Landroid/view/InputDevice;->isExternal()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 368
    :cond_1
    const/16 v2, 0x4002

    invoke-virtual {p0, v2}, Landroid/view/InputEvent;->isFromSource(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 370
    invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I

    move-result v3

    .line 369
    invoke-static {v2, v3}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiExternalDevice(II)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 367
    :cond_3
    return v1
.end method

.method private updateAlarmForTrack()V
    .locals 7

    .line 119
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v1, 0x1

    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    const-string v4, "clear_external_device"

    iget-object v5, p0, Lcom/android/server/input/InputOneTrackUtil;->mExternalDeviceAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 122
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v6

    .line 119
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 123
    return-void
.end method


# virtual methods
.method public track6FShortcut(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .line 202
    const-string v0, "899.4.0.1.27102"

    invoke-virtual {p0, p1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method public trackEdgeSuppressionEvent(ZILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "changedSize"    # I
    .param p3, "changedType"    # Ljava/lang/String;
    .param p4, "size"    # I
    .param p5, "type"    # Ljava/lang/String;

    .line 228
    nop

    .line 229
    invoke-direct/range {p0 .. p5}, Lcom/android/server/input/InputOneTrackUtil;->getEdgeSuppressionStatusData(ZILjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    const-string v1, "31000000735"

    const-string v2, "com.xiaomi.edgesuppression"

    const/4 v3, 0x2

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 231
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/String;
    .param p4, "flag"    # I

    .line 160
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v7, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/InputOneTrackUtil;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 175
    return-void
.end method

.method public trackEvents(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V
    .locals 8
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p4, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .line 178
    .local p3, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v7, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/InputOneTrackUtil;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 194
    return-void
.end method

.method public trackExtendDevice(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 234
    const-string v0, "STYLUS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    .line 235
    nop

    .line 236
    invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->getStylusDeviceStatusData()Ljava/lang/String;

    move-result-object v0

    .line 235
    const-string v2, "31000000824"

    const-string v3, "com.xiaomi.extendDevice"

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 238
    :cond_0
    nop

    .line 239
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/InputOneTrackUtil;->getExternalDeviceStatusData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    const-string v2, "31000000618"

    const-string v3, "com.xiaomi.padkeyboard"

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 241
    :goto_0
    return-void
.end method

.method public trackExternalDevice(Landroid/view/InputEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/InputEvent;

    .line 323
    invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    .line 324
    invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-virtual {v0, v1}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->hasDuplicationValue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 327
    :cond_0
    invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "name":Ljava/lang/String;
    invoke-static {p1}, Lcom/android/server/input/InputOneTrackUtil;->getEventDeviceType(Landroid/view/InputEvent;)Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "type":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getVendorId()I

    move-result v2

    .line 330
    .local v2, "vendorId":I
    invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/InputDevice;->getProductId()I

    move-result v3

    .line 331
    .local v3, "productId":I
    const-string v4, "UNKNOW"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 332
    return-void

    .line 334
    :cond_1
    iget-object v4, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    invoke-virtual {v4, v0}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->hasDuplicationValue(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 335
    iget-object v4, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    invoke-virtual {v4, v0, v1}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->putTrackValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackExtendDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-static {v3, v2}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiKeyboard(II)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    .line 338
    invoke-virtual {v4, v2}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->hasDuplicationXiaomiDevice(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 340
    iget-object v4, p0, Lcom/android/server/input/InputOneTrackUtil;->mInputDeviceOneTrack:Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;

    invoke-virtual {v4, v2, v1}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->putXiaomiDeviceInfo(ILjava/lang/String;)V

    .line 341
    sget-object v4, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->INTERACTION:Lcom/android/server/input/padkeyboard/KeyboardInteraction;

    invoke-virtual {v4}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardDAU(Z)V

    .line 344
    :cond_2
    return-void

    .line 325
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "type":Ljava/lang/String;
    .end local v2    # "vendorId":I
    .end local v3    # "productId":I
    :cond_3
    :goto_0
    return-void
.end method

.method public trackFlingEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;

    .line 248
    const-string v0, "com.xiaomi.input.shortcut"

    const/4 v1, 0x2

    const-string v2, "31000401650"

    invoke-virtual {p0, v2, v0, p1, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 249
    return-void
.end method

.method public trackKeyboardDAU(Z)V
    .locals 4
    .param p1, "data"    # Z

    .line 218
    nop

    .line 219
    invoke-direct {p0, p1}, Lcom/android/server/input/InputOneTrackUtil;->getKeyboardDAUStatusData(Z)Ljava/lang/String;

    move-result-object v0

    .line 218
    const-string v1, "31000000824"

    const-string v2, "com.xiaomi.extendDevice"

    const/4 v3, 0x2

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 220
    return-void
.end method

.method public trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "tip"    # Ljava/lang/String;

    .line 210
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    return-void

    .line 213
    :cond_0
    nop

    .line 214
    invoke-direct {p0, p2, p1}, Lcom/android/server/input/InputOneTrackUtil;->getKeyboardShortcutKeyStatusData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    const-string v1, "31000000824"

    const-string v2, "com.xiaomi.extendDevice"

    const/4 v3, 0x2

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 215
    return-void
.end method

.method public trackKeyboardShortcut(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .line 198
    const-string v0, "899.4.0.1.20553"

    invoke-virtual {p0, p1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method public trackShortcutEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;

    .line 244
    invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->getShortcutAppId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.xiaomi.input.shortcut"

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 245
    return-void
.end method

.method public trackStylusEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;

    .line 223
    const-string v0, "com.xiaomi.extendDevice"

    const/4 v1, 0x2

    const-string v2, "31000000824"

    invoke-virtual {p0, v2, v0, p1, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 224
    return-void
.end method

.method public trackTouchpadEvents(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 251
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "com.xiaomi.extendDevice"

    const/4 v1, 0x2

    const-string v2, "31000000824"

    invoke-virtual {p0, v2, v0, p1, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvents(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    .line 253
    return-void
.end method

.method public trackVoice2Word(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .line 206
    const-string v0, "899.4.0.1.27103"

    invoke-virtual {p0, p1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    return-void
.end method

.method public unbindTrackService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 154
    iget-boolean v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mIsBound:Z

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 157
    :cond_0
    return-void
.end method
