.class public Lcom/android/server/input/KeyboardCombinationManagerStubImpl;
.super Ljava/lang/Object;
.source "KeyboardCombinationManagerStubImpl.java"

# interfaces
.implements Lcom/android/server/input/KeyboardCombinationManagerStub;


# static fields
.field static final TAG:Ljava/lang/String; = "KeyboardCombinationManagerStubImpl"


# instance fields
.field private mActiveRule:Lcom/android/server/policy/KeyboardCombinationRule;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field public mKeyboardShortcutRules:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/android/server/policy/KeyboardCombinationRule;",
            ">;"
        }
    .end annotation
.end field

.field private mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

.field private mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;


# direct methods
.method public static synthetic $r8$lambda$2w5V772XSCDu1mgMLlsvAxE6aEA(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$addRule$1(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$4vtwHlbIr_VW4pfZ_E33cZRukqg(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$resetRule$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$7-ODEWhdKSOa1IZoY3j0Ue65XCs(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$updateRule$3(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$AmGzw_bi01fpKeH9wvbSFffz428(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$removeRule$2(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    return-void
.end method

.method public static synthetic $r8$lambda$cOnXph6CTg7Fp9_KLk74HeGRI48(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;IILcom/android/server/policy/KeyboardCombinationRule;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$interceptKeyboardCombination$0(IILcom/android/server/policy/KeyboardCombinationRule;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    return-void
.end method

.method private forAllKeyboardCombinationRules(Lcom/android/internal/util/ToBooleanFunction;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/ToBooleanFunction<",
            "Lcom/android/server/policy/KeyboardCombinationRule;",
            ">;)Z"
        }
    .end annotation

    .line 142
    .local p1, "callback":Lcom/android/internal/util/ToBooleanFunction;, "Lcom/android/internal/util/ToBooleanFunction<Lcom/android/server/policy/KeyboardCombinationRule;>;"
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    .line 143
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 144
    iget-object v2, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/KeyboardCombinationRule;

    .line 145
    .local v2, "rule":Lcom/android/server/policy/KeyboardCombinationRule;
    invoke-interface {p1, v2}, Lcom/android/internal/util/ToBooleanFunction;->apply(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    const/4 v3, 0x1

    return v3

    .line 143
    .end local v2    # "rule":Lcom/android/server/policy/KeyboardCombinationRule;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    .end local v1    # "index":I
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public static getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;
    .locals 1

    .line 30
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStub;->get()Lcom/android/server/input/KeyboardCombinationManagerStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    return-object v0
.end method

.method private getMetaState(Landroid/view/KeyEvent;)I
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, "metaState":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    or-int/lit16 v0, v0, 0x1000

    .line 93
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 94
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_1

    .line 95
    or-int/lit8 v0, v0, 0x22

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_2

    .line 97
    or-int/lit8 v0, v0, 0x12

    .line 100
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 101
    or-int/lit8 v0, v0, 0x1

    .line 103
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isMetaPressed()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 104
    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    .line 106
    :cond_4
    return v0
.end method

.method private synthetic lambda$addRule$1(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 111
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v1

    new-instance v3, Lcom/android/server/policy/KeyboardCombinationRule;

    iget-object v4, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, v4, v5, p1}, Lcom/android/server/policy/KeyboardCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 113
    return-void
.end method

.method private synthetic lambda$interceptKeyboardCombination$0(IILcom/android/server/policy/KeyboardCombinationRule;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "metaState"    # I
    .param p3, "rule"    # Lcom/android/server/policy/KeyboardCombinationRule;

    .line 72
    invoke-virtual {p3, p1, p2}, Lcom/android/server/policy/KeyboardCombinationRule;->shouldInterceptKeys(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mActiveRule:Lcom/android/server/policy/KeyboardCombinationRule;

    .line 74
    const/4 v0, 0x0

    return v0

    .line 76
    :cond_0
    iput-object p3, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mActiveRule:Lcom/android/server/policy/KeyboardCombinationRule;

    .line 77
    invoke-virtual {p3}, Lcom/android/server/policy/KeyboardCombinationRule;->execute()V

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method private synthetic lambda$removeRule$2(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 118
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->delete(J)V

    .line 119
    return-void
.end method

.method private synthetic lambda$resetRule$4()V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 135
    invoke-virtual {p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->initRules()V

    .line 136
    return-void
.end method

.method private synthetic lambda$updateRule$3(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 6
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 124
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getHistoryKeyCode()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->delete(J)V

    .line 125
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v1

    new-instance v3, Lcom/android/server/policy/KeyboardCombinationRule;

    iget-object v4, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, v4, v5, p1}, Lcom/android/server/policy/KeyboardCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 129
    :cond_0
    return-void
.end method


# virtual methods
.method public addRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 110
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 153
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 154
    const-string v0, "KeyboardCombinationManagerStubImpl"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 155
    const-string v0, "mKeyboardShortcutRules ="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 156
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 157
    iget-object v1, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/policy/KeyboardCombinationRule;

    invoke-virtual {v1}, Lcom/android/server/policy/KeyboardCombinationRule;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    iput-object p1, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mContext:Landroid/content/Context;

    .line 36
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    .line 37
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 38
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    .line 39
    invoke-virtual {p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->initRules()V

    .line 40
    return-void
.end method

.method public initRules()V
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 44
    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getMiuiKeyboardShortcutInfo()Landroid/util/LongSparseArray;

    move-result-object v0

    .line 45
    .local v0, "infos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 46
    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 47
    .local v2, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    invoke-virtual {v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    new-instance v3, Lcom/android/server/policy/KeyboardCombinationRule;

    iget-object v4, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, v4, v5, v2}, Lcom/android/server/policy/KeyboardCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    .line 49
    .local v3, "rule":Lcom/android/server/policy/KeyboardCombinationRule;
    iget-object v4, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mKeyboardShortcutRules:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 45
    .end local v2    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .end local v3    # "rule":Lcom/android/server/policy/KeyboardCombinationRule;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public interceptKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 56
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mMiuiKeyInterceptExtend:Lcom/android/server/policy/MiuiKeyInterceptExtend;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyboardShortcutEnable()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 57
    iput-object v2, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mActiveRule:Lcom/android/server/policy/KeyboardCombinationRule;

    .line 58
    return v1

    .line 60
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 61
    .local v0, "keyCode":I
    invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getMetaState(Landroid/view/KeyEvent;)I

    move-result v3

    .line 62
    .local v3, "metaState":I
    if-nez v3, :cond_1

    .line 63
    iput-object v2, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mActiveRule:Lcom/android/server/policy/KeyboardCombinationRule;

    .line 64
    return v1

    .line 66
    :cond_1
    invoke-virtual {p0, v0, v3}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->interceptKeyboardCombination(II)Z

    move-result v1

    return v1
.end method

.method public interceptKeyboardCombination(II)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "metaState"    # I

    .line 71
    new-instance v0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;II)V

    invoke-direct {p0, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->forAllKeyboardCombinationRules(Lcom/android/internal/util/ToBooleanFunction;)Z

    move-result v0

    return v0
.end method

.method public isKeyConsumed(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 83
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mActiveRule:Lcom/android/server/policy/KeyboardCombinationRule;

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getMetaState(Landroid/view/KeyEvent;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/policy/KeyboardCombinationRule;->shouldInterceptKeys(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0
.end method

.method public removeRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 117
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 120
    return-void
.end method

.method public resetRule()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 137
    return-void
.end method

.method public updateRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 123
    iget-object v0, p0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 130
    return-void
.end method
