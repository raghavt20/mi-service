class com.android.server.input.MiInputPhotoHandleManager$HandleStatusSynchronize extends android.os.Handler {
	 /* .source "MiInputPhotoHandleManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/MiInputPhotoHandleManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "HandleStatusSynchronize" */
} // .end annotation
/* # instance fields */
final com.android.server.input.MiInputPhotoHandleManager this$0; //synthetic
/* # direct methods */
public com.android.server.input.MiInputPhotoHandleManager$HandleStatusSynchronize ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 158 */
this.this$0 = p1;
/* .line 159 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 160 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 164 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 169 */
/* :pswitch_0 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
int v3 = 1; // const/4 v3, 0x1
com.android.server.input.MiInputPhotoHandleManager .-$$Nest$mnotifyHandleConnectStatus ( v0,v3,v1,v2 );
/* .line 170 */
/* .line 166 */
/* :pswitch_1 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
int v3 = 0; // const/4 v3, 0x0
com.android.server.input.MiInputPhotoHandleManager .-$$Nest$mnotifyHandleConnectStatus ( v0,v3,v1,v2 );
/* .line 167 */
/* nop */
/* .line 174 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
