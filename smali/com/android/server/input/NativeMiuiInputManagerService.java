class com.android.server.input.NativeMiuiInputManagerService {
	 /* .source "NativeMiuiInputManagerService.java" */
	 /* # instance fields */
	 private final Long mPtr;
	 /* # direct methods */
	 static com.android.server.input.NativeMiuiInputManagerService ( ) {
		 /* .locals 1 */
		 /* .line 15 */
		 final String v0 = "miinputflinger"; // const-string v0, "miinputflinger"
		 java.lang.System .loadLibrary ( v0 );
		 /* .line 16 */
		 return;
	 } // .end method
	 com.android.server.input.NativeMiuiInputManagerService ( ) {
		 /* .locals 2 */
		 /* .param p1, "miuiInputManagerService" # Lcom/android/server/input/MiuiInputManagerService; */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 22 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/input/NativeMiuiInputManagerService;->init(Lcom/android/server/input/MiuiInputManagerService;)J */
		 /* move-result-wide v0 */
		 /* iput-wide v0, p0, Lcom/android/server/input/NativeMiuiInputManagerService;->mPtr:J */
		 /* .line 23 */
		 return;
	 } // .end method
	 private native Long init ( com.android.server.input.MiuiInputManagerService p0 ) {
	 } // .end method
	 /* # virtual methods */
	 public native java.lang.String dump ( ) {
	 } // .end method
	 public native Boolean hideCursor ( ) {
	 } // .end method
	 public native void hideMouseCursor ( ) {
	 } // .end method
	 public native void injectMotionEvent ( android.view.MotionEvent p0, Integer p1 ) {
	 } // .end method
	 public native void notifyPhotoHandleConnectionStatus ( Boolean p0, Integer p1 ) {
	 } // .end method
	 public native void requestRedirect ( Integer p0, Integer p1 ) {
	 } // .end method
	 public native Boolean setCursorPosition ( Float p0, Float p1 ) {
	 } // .end method
	 public native void setDeviceShareListener ( Integer p0, java.io.FileDescriptor p1, Integer p2 ) {
	 } // .end method
	 public native void setDimState ( Boolean p0 ) {
	 } // .end method
	 public native void setInputConfig ( Integer p0, Long p1 ) {
	 } // .end method
	 public native void setInteractive ( Boolean p0 ) {
	 } // .end method
	 public native void setTouchpadButtonState ( Integer p0, Boolean p1 ) {
	 } // .end method
