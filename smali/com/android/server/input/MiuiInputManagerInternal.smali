.class public abstract Lcom/android/server/input/MiuiInputManagerInternal;
.super Ljava/lang/Object;
.source "MiuiInputManagerInternal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract doubleTap(III)Z
.end method

.method public abstract getCurrentDisplayViewPorts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCurrentPointerDisplayId()I
.end method

.method public abstract hideMouseCursor()V
.end method

.method public abstract injectMotionEvent(Landroid/view/MotionEvent;I)V
.end method

.method public abstract notifyPhotoHandleConnectionStatus(ZI)V
.end method

.method public abstract obtainLaserPointerController()Lcom/miui/server/input/stylus/laser/PointerControllerInterface;
.end method

.method public abstract setCustomPointerIcon(Landroid/view/PointerIcon;)V
.end method

.method public abstract setDeviceShareListener(ILjava/io/FileDescriptor;I)V
.end method

.method public abstract setDimState(Z)V
.end method

.method public abstract setInputConfig(IJ)V
.end method

.method public abstract setPointerIconType(I)V
.end method

.method public abstract setScreenState(Z)V
.end method

.method public abstract setTouchpadButtonState(IZ)V
.end method

.method public abstract swipe(IIIII)Z
.end method

.method public abstract swipe(IIIIII)Z
.end method

.method public abstract tap(II)Z
.end method
