.class public Lcom/android/server/input/TouchWakeUpFeatureManager;
.super Ljava/lang/Object;
.source "TouchWakeUpFeatureManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;,
        Lcom/android/server/input/TouchWakeUpFeatureManager$TouchWakeUpFeatureManagerHolder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TouchWakeUpFeatureManager"

.field private static final WAKEUP_OFF:I = 0x4

.field private static final WAKEUP_ON:I = 0x5


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mSettingsObserver:Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;

.field private final mTouchFeature:Lmiui/util/ITouchFeature;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/TouchWakeUpFeatureManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/input/TouchWakeUpFeatureManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$monGestureWakeupSettingsChanged(Lcom/android/server/input/TouchWakeUpFeatureManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onGestureWakeupSettingsChanged(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monSubScreenGestureWakeupSettingsChanged(Lcom/android/server/input/TouchWakeUpFeatureManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->onSubScreenGestureWakeupSettingsChanged(Z)V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mHandler:Landroid/os/Handler;

    .line 48
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mTouchFeature:Lmiui/util/ITouchFeature;

    .line 49
    new-instance v1, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;

    invoke-direct {v1, p0, v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;-><init>(Lcom/android/server/input/TouchWakeUpFeatureManager;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mSettingsObserver:Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/TouchWakeUpFeatureManager-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/TouchWakeUpFeatureManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/server/input/TouchWakeUpFeatureManager;
    .locals 1

    .line 57
    invoke-static {}, Lcom/android/server/input/TouchWakeUpFeatureManager$TouchWakeUpFeatureManagerHolder;->-$$Nest$sfgetsInstance()Lcom/android/server/input/TouchWakeUpFeatureManager;

    move-result-object v0

    return-object v0
.end method

.method private onGestureWakeupSettingsChanged(Z)V
    .locals 4
    .param p1, "newState"    # Z

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "On gesture wakeup settings changed, newState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TouchWakeUpFeatureManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mTouchFeature:Lmiui/util/ITouchFeature;

    invoke-virtual {v0}, Lmiui/util/ITouchFeature;->hasDoubleTapWakeUpSupport()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->setTouchMode(IZ)V

    .line 75
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->setTouchMode(IZ)V

    goto :goto_1

    .line 81
    :cond_1
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 82
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    if-eqz p1, :cond_2

    const/4 v2, 0x5

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    :goto_0
    invoke-virtual {v0, v2}, Lcom/android/server/input/config/InputCommonConfig;->setWakeUpMode(I)V

    .line 83
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Flush wake up state to native, newState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    .end local v0    # "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    :cond_3
    :goto_1
    return-void
.end method

.method private onSubScreenGestureWakeupSettingsChanged(Z)V
    .locals 2
    .param p1, "newState"    # Z

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "On sub screen gesture wakeup settings changed, newState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TouchWakeUpFeatureManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/android/server/input/TouchWakeUpFeatureManager;->setTouchMode(IZ)V

    .line 91
    return-void
.end method

.method private setTouchMode(IZ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "newState"    # Z

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update state to ITouchFeature, id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", newState = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 96
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    const-string v2, "TouchWakeUpFeatureManager"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mTouchFeature:Lmiui/util/ITouchFeature;

    invoke-virtual {v0, p1, v1, p2}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 98
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 150
    new-instance v0, Landroid/util/IndentingPrintWriter;

    const-string v1, "  "

    invoke-direct {v0, p1, v1, p2}, Landroid/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .local v0, "ipw":Landroid/util/IndentingPrintWriter;
    const-string v1, "TouchWakeUpFeatureManager"

    invoke-virtual {v0, v1}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->increaseIndent()Landroid/util/IndentingPrintWriter;

    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HasDoubleTapWakeUpSupport = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mTouchFeature:Lmiui/util/ITouchFeature;

    invoke-virtual {v2}, Lmiui/util/ITouchFeature;->hasDoubleTapWakeUpSupport()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 154
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->decreaseIndent()Landroid/util/IndentingPrintWriter;

    .line 155
    return-void
.end method

.method public onSystemBooted()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mSettingsObserver:Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->register()V

    .line 62
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mSettingsObserver:Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->refreshAllSettings()V

    .line 63
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "userId"    # I

    .line 66
    iget-object v0, p0, Lcom/android/server/input/TouchWakeUpFeatureManager;->mSettingsObserver:Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;

    invoke-virtual {v0}, Lcom/android/server/input/TouchWakeUpFeatureManager$SettingsObserver;->refreshAllSettings()V

    .line 67
    return-void
.end method
