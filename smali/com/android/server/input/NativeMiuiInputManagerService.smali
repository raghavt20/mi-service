.class Lcom/android/server/input/NativeMiuiInputManagerService;
.super Ljava/lang/Object;
.source "NativeMiuiInputManagerService.java"


# instance fields
.field private final mPtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    const-string v0, "miinputflinger"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method constructor <init>(Lcom/android/server/input/MiuiInputManagerService;)V
    .locals 2
    .param p1, "miuiInputManagerService"    # Lcom/android/server/input/MiuiInputManagerService;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-direct {p0, p1}, Lcom/android/server/input/NativeMiuiInputManagerService;->init(Lcom/android/server/input/MiuiInputManagerService;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/input/NativeMiuiInputManagerService;->mPtr:J

    .line 23
    return-void
.end method

.method private native init(Lcom/android/server/input/MiuiInputManagerService;)J
.end method


# virtual methods
.method public native dump()Ljava/lang/String;
.end method

.method public native hideCursor()Z
.end method

.method public native hideMouseCursor()V
.end method

.method public native injectMotionEvent(Landroid/view/MotionEvent;I)V
.end method

.method public native notifyPhotoHandleConnectionStatus(ZI)V
.end method

.method public native requestRedirect(II)V
.end method

.method public native setCursorPosition(FF)Z
.end method

.method public native setDeviceShareListener(ILjava/io/FileDescriptor;I)V
.end method

.method public native setDimState(Z)V
.end method

.method public native setInputConfig(IJ)V
.end method

.method public native setInteractive(Z)V
.end method

.method public native setTouchpadButtonState(IZ)V
.end method
