class com.android.server.input.InputOneTrackUtil$2 implements android.content.ServiceConnection {
	 /* .source "InputOneTrackUtil.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/InputOneTrackUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.InputOneTrackUtil this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$PaL6vsVCFdwvJ1v5e14WdajMETg ( com.android.server.input.InputOneTrackUtil$2 p0, android.os.IBinder p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/InputOneTrackUtil$2;->lambda$onServiceConnected$0(Landroid/os/IBinder;)V */
return;
} // .end method
public static void $r8$lambda$ioMSn7cSuU_oABQSNEuTn8tRRzk ( com.android.server.input.InputOneTrackUtil$2 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil$2;->lambda$onServiceDisconnected$1()V */
return;
} // .end method
 com.android.server.input.InputOneTrackUtil$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/InputOneTrackUtil; */
/* .line 126 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void lambda$onServiceConnected$0 ( android.os.IBinder p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "service" # Landroid/os/IBinder; */
/* .line 130 */
v0 = this.this$0;
com.miui.analytics.ITrackBinder$Stub .asInterface ( p1 );
com.android.server.input.InputOneTrackUtil .-$$Nest$fputmService ( v0,v1 );
/* .line 131 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onServiceConnected: "; // const-string v1, "onServiceConnected: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.android.server.input.InputOneTrackUtil .-$$Nest$fgetmService ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InputOneTrackUtil"; // const-string v1, "InputOneTrackUtil"
android.util.Slog .d ( v1,v0 );
/* .line 132 */
return;
} // .end method
private void lambda$onServiceDisconnected$1 ( ) { //synthethic
/* .locals 2 */
/* .line 138 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.input.InputOneTrackUtil .-$$Nest$fputmService ( v0,v1 );
/* .line 139 */
final String v0 = "InputOneTrackUtil"; // const-string v0, "InputOneTrackUtil"
final String v1 = "onServiceDisconnected"; // const-string v1, "onServiceDisconnected"
android.util.Slog .d ( v0,v1 );
/* .line 140 */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 129 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v1, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p2}, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/InputOneTrackUtil$2;Landroid/os/IBinder;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 133 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 137 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v1, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/InputOneTrackUtil$2$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/InputOneTrackUtil$2;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 141 */
return;
} // .end method
