class com.android.server.input.ReflectionUtils$MethodEntry {
	 /* .source "ReflectionUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/ReflectionUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "MethodEntry" */
} // .end annotation
/* # instance fields */
java.lang.Class mClassName;
java.lang.reflect.Method mMethod;
java.lang.String mMethodName;
java.lang.Class mParmasClass;
/* # direct methods */
 com.android.server.input.ReflectionUtils$MethodEntry ( ) {
/* .locals 0 */
/* .line 159 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 160 */
return;
} // .end method
 com.android.server.input.ReflectionUtils$MethodEntry ( ) {
/* .locals 0 */
/* .param p1, "className" # Ljava/lang/Class; */
/* .param p2, "methodName" # Ljava/lang/String; */
/* .param p3, "parmasClass" # [Ljava/lang/Class; */
/* .param p4, "method" # Ljava/lang/reflect/Method; */
/* .line 162 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 163 */
this.mClassName = p1;
/* .line 164 */
this.mMethodName = p2;
/* .line 165 */
this.mParmasClass = p3;
/* .line 166 */
this.mMethod = p4;
/* .line 167 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 171 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_0 */
/* .line 172 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* if-eq v2, v3, :cond_1 */
/* .line 173 */
} // :cond_1
/* move-object v2, p1 */
/* check-cast v2, Lcom/android/server/input/ReflectionUtils$MethodEntry; */
/* .line 174 */
/* .local v2, "that":Lcom/android/server/input/ReflectionUtils$MethodEntry; */
v3 = this.mParmasClass;
v4 = this.mParmasClass;
v3 = java.util.Arrays .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_2
v3 = this.mMethodName;
v4 = this.mMethodName;
/* .line 175 */
v3 = java.util.Objects .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_2
	 v3 = this.mClassName;
	 v4 = this.mClassName;
	 /* .line 176 */
	 v3 = 	 java.util.Objects .equals ( v3,v4 );
	 if ( v3 != null) { // if-eqz v3, :cond_2
	 } // :cond_2
	 /* move v0, v1 */
	 /* .line 174 */
} // :goto_0
/* .line 172 */
} // .end local v2 # "that":Lcom/android/server/input/ReflectionUtils$MethodEntry;
} // :cond_3
} // :goto_1
} // .end method
public java.lang.Class getClassName ( ) {
/* .locals 1 */
/* .line 214 */
v0 = this.mClassName;
} // .end method
public java.lang.String getMethodName ( ) {
/* .locals 1 */
/* .line 210 */
v0 = this.mMethodName;
} // .end method
public java.lang.Class getParmasClass ( ) {
/* .locals 1 */
/* .line 218 */
v0 = this.mParmasClass;
} // .end method
public java.lang.reflect.Method getmMethod ( ) {
/* .locals 1 */
/* .line 206 */
v0 = this.mMethod;
} // .end method
public Integer hashCode ( ) {
/* .locals 3 */
/* .line 181 */
v0 = this.mMethodName;
v1 = this.mClassName;
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
/* .line 182 */
/* .local v0, "result":I */
/* mul-int/lit8 v1, v0, 0x1f */
v2 = this.mParmasClass;
v2 = java.util.Arrays .hashCode ( v2 );
/* add-int/2addr v1, v2 */
/* .line 183 */
} // .end local v0 # "result":I
/* .local v1, "result":I */
} // .end method
public com.android.server.input.ReflectionUtils$MethodEntry setClassName ( java.lang.Class p0 ) {
/* .locals 0 */
/* .param p1, "className" # Ljava/lang/Class; */
/* .line 197 */
this.mClassName = p1;
/* .line 198 */
} // .end method
public com.android.server.input.ReflectionUtils$MethodEntry setMethodName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .line 192 */
this.mMethodName = p1;
/* .line 193 */
} // .end method
public com.android.server.input.ReflectionUtils$MethodEntry setParmasClass ( java.lang.Class[] p0 ) {
/* .locals 0 */
/* .param p1, "parmasClass" # [Ljava/lang/Class; */
/* .line 187 */
this.mParmasClass = p1;
/* .line 188 */
} // .end method
public void setmMethod ( java.lang.reflect.Method p0 ) {
/* .locals 0 */
/* .param p1, "mMethod" # Ljava/lang/reflect/Method; */
/* .line 202 */
this.mMethod = p1;
/* .line 203 */
return;
} // .end method
