.class Lcom/android/server/input/InputManagerServiceStubImpl$3;
.super Landroid/database/ContentObserver;
.source "InputManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/InputManagerServiceStubImpl;->registerMiuiOptimizationObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/InputManagerServiceStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/InputManagerServiceStubImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 327
    iput-object p1, p0, Lcom/android/server/input/InputManagerServiceStubImpl$3;->this$0:Lcom/android/server/input/InputManagerServiceStubImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 330
    nop

    .line 331
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 330
    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "persist.sys.miui_optimization"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/2addr v0, v1

    .line 332
    .local v0, "isCtsMode":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ctsMode  is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "InputManagerServiceStubImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl$3;->this$0:Lcom/android/server/input/InputManagerServiceStubImpl;

    invoke-static {v2, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->-$$Nest$msetCtsMode(Lcom/android/server/input/InputManagerServiceStubImpl;Z)V

    .line 334
    iget-object v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl$3;->this$0:Lcom/android/server/input/InputManagerServiceStubImpl;

    invoke-static {v2}, Lcom/android/server/input/InputManagerServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/input/InputManagerServiceStubImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiKeyInterceptExtend;

    move-result-object v2

    .line 336
    .local v2, "miuiKeyInterceptExtend":Lcom/android/server/policy/MiuiKeyInterceptExtend;
    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V

    .line 337
    invoke-virtual {v2, v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setAospKeyboardShortcutEnable(Z)V

    .line 338
    return-void
.end method
