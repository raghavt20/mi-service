class com.android.server.input.MiuiInputManagerService$H extends android.os.Handler {
	 /* .source "MiuiInputManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/MiuiInputManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final java.lang.String DATA_ACTION;
private static final java.lang.String DATA_FUNCTION;
private static final Integer MSG_NOTIFY_LISTENER;
/* # instance fields */
final com.android.server.input.MiuiInputManagerService this$0; //synthetic
/* # direct methods */
public com.android.server.input.MiuiInputManagerService$H ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/MiuiInputManagerService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 690 */
this.this$0 = p1;
/* .line 691 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 692 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 7 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 696 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 697 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "action"; // const-string v1, "action"
final String v2 = ""; // const-string v2, ""
(( android.os.Bundle ) v0 ).getString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 698 */
/* .local v1, "action":Ljava/lang/String; */
final String v2 = "function"; // const-string v2, "function"
final String v3 = ""; // const-string v3, ""
(( android.os.Bundle ) v0 ).getString ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 699 */
/* .local v2, "function":Ljava/lang/String; */
/* iget v3, p1, Landroid/os/Message;->what:I */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_1 */
/* .line 700 */
v3 = this.this$0;
com.android.server.input.MiuiInputManagerService .-$$Nest$fgetmShortcutListenersToNotify ( v3 );
(( java.util.ArrayList ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V
/* .line 701 */
v3 = this.this$0;
com.android.server.input.MiuiInputManagerService .-$$Nest$fgetmMiuiShortcutSettingsLock ( v3 );
/* monitor-enter v3 */
/* .line 702 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
try { // :try_start_0
v5 = this.this$0;
com.android.server.input.MiuiInputManagerService .-$$Nest$fgetmShortcutListeners ( v5 );
v5 = (( android.util.SparseArray ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/SparseArray;->size()I
/* if-ge v4, v5, :cond_0 */
/* .line 703 */
v5 = this.this$0;
com.android.server.input.MiuiInputManagerService .-$$Nest$fgetmShortcutListenersToNotify ( v5 );
v6 = this.this$0;
com.android.server.input.MiuiInputManagerService .-$$Nest$fgetmShortcutListeners ( v6 );
(( android.util.SparseArray ) v6 ).valueAt ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord; */
(( java.util.ArrayList ) v5 ).add ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 702 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 705 */
} // .end local v4 # "i":I
} // :cond_0
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 706 */
v3 = this.this$0;
com.android.server.input.MiuiInputManagerService .-$$Nest$fgetmShortcutListenersToNotify ( v3 );
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord; */
/* .line 707 */
/* .local v4, "record":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord; */
(( com.android.server.input.MiuiInputManagerService$ShortcutListenerRecord ) v4 ).notifyShortcutSettingsChanged ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->notifyShortcutSettingsChanged(Ljava/lang/String;Ljava/lang/String;)V
/* .line 708 */
} // .end local v4 # "record":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
/* .line 705 */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
/* .line 710 */
} // :cond_1
return;
} // .end method
