.class Lcom/android/server/input/InputManagerServiceStubImpl$6;
.super Landroid/database/ContentObserver;
.source "InputManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/InputManagerServiceStubImpl;->registerDefaultInputMethodSettingObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/InputManagerServiceStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/InputManagerServiceStubImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 430
    iput-object p1, p0, Lcom/android/server/input/InputManagerServiceStubImpl$6;->this$0:Lcom/android/server/input/InputManagerServiceStubImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 433
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl$6;->this$0:Lcom/android/server/input/InputManagerServiceStubImpl;

    invoke-virtual {v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDefaultInputMethodFromSettings()V

    .line 434
    return-void
.end method
