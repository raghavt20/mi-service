public class com.android.server.input.InputOneTrackUtil {
	 /* .source "InputOneTrackUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID_EDGE_SUPPRESSION;
private static final java.lang.String APP_ID_EXTERNAL_DEVICE;
private static final java.lang.String APP_ID_KEYBOARD_STYLUS;
private static final java.lang.String APP_ID_SHORTCUT;
private static final java.lang.String APP_ID_SHORTCUT_GLOBAL;
private static final java.lang.String EVENT_KEYBOARD_DAU_NAME;
private static final java.lang.String EVENT_NAME_EXTERNAL_DEVICE;
private static final java.lang.String EVENT_SHORTCUT_KEY_USE_NAME;
private static final java.lang.String EXTERNAL_DEVICE_NAME;
private static final java.lang.String EXTERNAL_DEVICE_TYPE;
private static final Integer FLAG_NON_ANONYMOUS;
private static final java.lang.String KEYBOARD_6F_FUNCTION_TRIGGER_TIP;
private static final java.lang.String KEYBOARD_DAU_TIP;
private static final java.lang.String KEYBOARD_SHORTCUT_KEY_TRIGGER_TIP;
private static final java.lang.String KEYBOARD_VOICE_TO_WORD_TRIGGER_TIP;
private static final java.lang.String PACKAGE_NAME_EDGE_SUPPRESSION;
private static final java.lang.String PACKAGE_NAME_EXTERNAL_DEVICE;
private static final java.lang.String PACKAGE_NAME_KEYBOARD_STYLUS;
private static final java.lang.String PACKAGE_NAME_SHORTCUT;
private static final java.lang.String PARAMS_KEYBOARD_DAU_NAME;
private static final java.lang.String PARAMS_KEYBOARD_SHORTCUT_NAME;
private static final java.lang.String PENCIL_DAU_TRACK_TYPE;
private static final java.lang.String SERVICE_NAME;
private static final java.lang.String SERVICE_PACKAGE_NAME;
private static final java.lang.String STATUS_EVENT_NAME;
private static final java.lang.String STATUS_EVENT_TIP;
private static final java.lang.String STYLUS_TRACK_TIP_PENCIL_DAU_TIP;
private static final java.lang.String SUPPRESSION_FUNCTION_INITIATIVE;
private static final java.lang.String SUPPRESSION_FUNCTION_SIZE;
private static final java.lang.String SUPPRESSION_FUNCTION_SIZE_CHANGED;
private static final java.lang.String SUPPRESSION_FUNCTION_TYPE;
private static final java.lang.String SUPPRESSION_FUNCTION_TYPE_CHANGED;
private static final java.lang.String SUPPRESSION_TRACK_TYPE;
private static final java.lang.String TAG;
private static volatile com.android.server.input.InputOneTrackUtil sInstance;
/* # instance fields */
private final android.app.AlarmManager mAlarmManager;
private final android.content.ServiceConnection mConnection;
private android.content.Context mContext;
private final android.app.AlarmManager$OnAlarmListener mExternalDeviceAlarmListener;
private final com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack mInputDeviceOneTrack;
private Boolean mIsBound;
private com.miui.analytics.ITrackBinder mService;
/* # direct methods */
public static void $r8$lambda$hTDN6ybx1zZZIK47Hry-shZCsHE ( com.android.server.input.InputOneTrackUtil p0, java.lang.String p1, java.lang.String p2, java.util.List p3, Integer p4 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/InputOneTrackUtil;->lambda$trackEvents$1(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V */
	 return;
} // .end method
public static void $r8$lambda$mkzJxLFc7_r4dBIFhR2E-g6V-yo ( com.android.server.input.InputOneTrackUtil p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/InputOneTrackUtil;->lambda$trackEvent$0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V */
	 return;
} // .end method
static com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack -$$Nest$fgetmInputDeviceOneTrack ( com.android.server.input.InputOneTrackUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mInputDeviceOneTrack;
} // .end method
static com.miui.analytics.ITrackBinder -$$Nest$fgetmService ( com.android.server.input.InputOneTrackUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mService;
} // .end method
static void -$$Nest$fputmService ( com.android.server.input.InputOneTrackUtil p0, com.miui.analytics.ITrackBinder p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mService = p1;
	 return;
} // .end method
static void -$$Nest$mupdateAlarmForTrack ( com.android.server.input.InputOneTrackUtil p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->updateAlarmForTrack()V */
	 return;
} // .end method
private com.android.server.input.InputOneTrackUtil ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 108 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 86 */
	 /* new-instance v0, Lcom/android/server/input/InputOneTrackUtil$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/InputOneTrackUtil$1;-><init>(Lcom/android/server/input/InputOneTrackUtil;)V */
	 this.mExternalDeviceAlarmListener = v0;
	 /* .line 126 */
	 /* new-instance v0, Lcom/android/server/input/InputOneTrackUtil$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/InputOneTrackUtil$2;-><init>(Lcom/android/server/input/InputOneTrackUtil;)V */
	 this.mConnection = v0;
	 /* .line 109 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/InputOneTrackUtil;->bindTrackService(Landroid/content/Context;)V */
	 /* .line 110 */
	 /* new-instance v0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack; */
	 /* invoke-direct {v0}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;-><init>()V */
	 this.mInputDeviceOneTrack = v0;
	 /* .line 111 */
	 v0 = this.mContext;
	 final String v1 = "alarm"; // const-string v1, "alarm"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/app/AlarmManager; */
	 this.mAlarmManager = v0;
	 /* .line 113 */
	 /* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 114 */
		 /* invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->updateAlarmForTrack()V */
		 /* .line 116 */
	 } // :cond_0
	 return;
} // .end method
private void bindTrackService ( android.content.Context p0 ) {
	 /* .locals 5 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 145 */
	 this.mContext = p1;
	 /* .line 146 */
	 /* new-instance v0, Landroid/content/Intent; */
	 /* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
	 /* .line 147 */
	 /* .local v0, "intent":Landroid/content/Intent; */
	 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
	 final String v2 = "com.miui.analytics.onetrack.TrackService"; // const-string v2, "com.miui.analytics.onetrack.TrackService"
	 (( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 148 */
	 v1 = this.mContext;
	 v2 = this.mConnection;
	 int v3 = 1; // const/4 v3, 0x1
	 v4 = android.os.UserHandle.SYSTEM;
	 v1 = 	 (( android.content.Context ) v1 ).bindServiceAsUser ( v0, v2, v3, v4 ); // invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
	 /* iput-boolean v1, p0, Lcom/android/server/input/InputOneTrackUtil;->mIsBound:Z */
	 /* .line 150 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "bindTrackService: "; // const-string v2, "bindTrackService: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v2, p0, Lcom/android/server/input/InputOneTrackUtil;->mIsBound:Z */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "InputOneTrackUtil"; // const-string v2, "InputOneTrackUtil"
	 android.util.Slog .d ( v2,v1 );
	 /* .line 151 */
	 return;
} // .end method
private java.lang.String getEdgeSuppressionStatusData ( Boolean p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
	 /* .locals 4 */
	 /* .param p1, "changed" # Z */
	 /* .param p2, "changedSize" # I */
	 /* .param p3, "changedType" # Ljava/lang/String; */
	 /* .param p4, "size" # I */
	 /* .param p5, "type" # Ljava/lang/String; */
	 /* .line 285 */
	 /* new-instance v0, Lorg/json/JSONObject; */
	 /* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
	 /* .line 287 */
	 /* .local v0, "jsonObject":Lorg/json/JSONObject; */
	 try { // :try_start_0
		 final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
		 final String v2 = "edge_suppression"; // const-string v2, "edge_suppression"
		 (( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
		 /* .line 288 */
		 final String v1 = "initiative_settings_by_user"; // const-string v1, "initiative_settings_by_user"
		 (( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
		 /* .line 289 */
		 final String v1 = "edge_size_changed"; // const-string v1, "edge_size_changed"
		 (( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
		 /* .line 290 */
		 final String v1 = "edge_type_changed"; // const-string v1, "edge_type_changed"
		 (( org.json.JSONObject ) v0 ).put ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
		 /* .line 291 */
		 final String v1 = "edge_size"; // const-string v1, "edge_size"
		 (( org.json.JSONObject ) v0 ).put ( v1, p4 ); // invoke-virtual {v0, v1, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
		 /* .line 292 */
		 final String v1 = "edge_type"; // const-string v1, "edge_type"
		 (( org.json.JSONObject ) v0 ).put ( v1, p5 ); // invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 295 */
		 /* .line 293 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 294 */
		 /* .local v1, "exception":Ljava/lang/Exception; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "construct TrackEvent data fail!"; // const-string v3, "construct TrackEvent data fail!"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v3 = "InputOneTrackUtil"; // const-string v3, "InputOneTrackUtil"
		 android.util.Slog .e ( v3,v2 );
		 /* .line 296 */
	 } // .end local v1 # "exception":Ljava/lang/Exception;
} // :goto_0
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String getEventDeviceType ( android.view.InputEvent p0 ) {
/* .locals 3 */
/* .param p0, "event" # Landroid/view/InputEvent; */
/* .line 347 */
final String v0 = ""; // const-string v0, ""
/* .line 348 */
/* .local v0, "type":Ljava/lang/String; */
/* const/16 v1, 0x4002 */
v1 = (( android.view.InputEvent ) p0 ).isFromSource ( v1 ); // invoke-virtual {p0, v1}, Landroid/view/InputEvent;->isFromSource(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 349 */
	 final String v0 = "STYLUS"; // const-string v0, "STYLUS"
	 /* .line 350 */
} // :cond_0
v1 = (( android.view.InputEvent ) p0 ).getSource ( ); // invoke-virtual {p0}, Landroid/view/InputEvent;->getSource()I
/* const/16 v2, 0x101 */
/* if-ne v1, v2, :cond_1 */
/* .line 351 */
final String v0 = "KEYBOARD"; // const-string v0, "KEYBOARD"
/* .line 352 */
} // :cond_1
v1 = (( android.view.InputEvent ) p0 ).getSource ( ); // invoke-virtual {p0}, Landroid/view/InputEvent;->getSource()I
/* const/16 v2, 0x2002 */
/* if-ne v1, v2, :cond_2 */
/* .line 353 */
final String v0 = "MOUSE"; // const-string v0, "MOUSE"
/* .line 354 */
} // :cond_2
v1 = (( android.view.InputEvent ) p0 ).getSource ( ); // invoke-virtual {p0}, Landroid/view/InputEvent;->getSource()I
/* const v2, 0x100008 */
/* if-ne v1, v2, :cond_3 */
/* .line 355 */
final String v0 = "TOUCHPAD"; // const-string v0, "TOUCHPAD"
/* .line 357 */
} // :cond_3
final String v0 = "UNKNOW"; // const-string v0, "UNKNOW"
/* .line 359 */
} // :goto_0
} // .end method
private java.lang.String getExternalDeviceStatusData ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "type" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 300 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 302 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "external_inputdevice"; // const-string v2, "external_inputdevice"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 303 */
final String v1 = "device_type"; // const-string v1, "device_type"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 304 */
final String v1 = "device_name"; // const-string v1, "device_name"
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 307 */
/* .line 305 */
/* :catch_0 */
/* move-exception v1 */
/* .line 306 */
/* .local v1, "exception":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "construct TrackEvent data fail!"; // const-string v3, "construct TrackEvent data fail!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "InputOneTrackUtil"; // const-string v3, "InputOneTrackUtil"
android.util.Slog .e ( v3,v2 );
/* .line 308 */
} // .end local v1 # "exception":Ljava/lang/Exception;
} // :goto_0
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
} // .end method
public static com.android.server.input.InputOneTrackUtil getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 98 */
v0 = com.android.server.input.InputOneTrackUtil.sInstance;
/* if-nez v0, :cond_1 */
/* .line 99 */
/* const-class v0, Lcom/android/server/input/InputOneTrackUtil; */
/* monitor-enter v0 */
/* .line 100 */
try { // :try_start_0
v1 = com.android.server.input.InputOneTrackUtil.sInstance;
/* if-nez v1, :cond_0 */
/* .line 101 */
/* new-instance v1, Lcom/android/server/input/InputOneTrackUtil; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/InputOneTrackUtil;-><init>(Landroid/content/Context;)V */
/* .line 103 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 105 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.InputOneTrackUtil.sInstance;
} // .end method
private java.lang.String getKeyboardDAUStatusData ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isIICConnected" # Z */
/* .line 273 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 275 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "keyboard_dau"; // const-string v2, "keyboard_dau"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 276 */
final String v1 = "connect_type"; // const-string v1, "connect_type"
if ( p1 != null) { // if-eqz p1, :cond_0
final String v2 = "pogopin"; // const-string v2, "pogopin"
} // :cond_0
/* const-string/jumbo v2, "\u84dd\u7259" */
} // :goto_0
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 277 */
/* const-string/jumbo v1, "tip" */
final String v2 = "899.7.0.1.27104"; // const-string v2, "899.7.0.1.27104"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 280 */
/* .line 278 */
/* :catch_0 */
/* move-exception v1 */
/* .line 279 */
/* .local v1, "exception":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "construct TrackEvent data fail!"; // const-string v3, "construct TrackEvent data fail!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "InputOneTrackUtil"; // const-string v3, "InputOneTrackUtil"
android.util.Slog .e ( v3,v2 );
/* .line 281 */
} // .end local v1 # "exception":Ljava/lang/Exception;
} // :goto_1
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
} // .end method
private java.lang.String getKeyboardShortcutKeyStatusData ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "tip" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 261 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 263 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "hotkeys_use"; // const-string v2, "hotkeys_use"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 264 */
final String v1 = "hotkeys_type"; // const-string v1, "hotkeys_type"
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 265 */
/* const-string/jumbo v1, "tip" */
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 268 */
/* .line 266 */
/* :catch_0 */
/* move-exception v1 */
/* .line 267 */
/* .local v1, "exception":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "construct TrackEvent data fail!"; // const-string v3, "construct TrackEvent data fail!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "InputOneTrackUtil"; // const-string v3, "InputOneTrackUtil"
android.util.Slog .e ( v3,v2 );
/* .line 269 */
} // .end local v1 # "exception":Ljava/lang/Exception;
} // :goto_0
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
} // .end method
private java.lang.String getShortcutAppId ( ) {
/* .locals 1 */
/* .line 256 */
v0 = com.android.server.input.shortcut.ShortcutOneTrackHelper .isShortcutTrackForOneTrack ( );
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "31000401650"; // const-string v0, "31000401650"
/* .line 257 */
} // :cond_0
final String v0 = "31000401666"; // const-string v0, "31000401666"
/* .line 256 */
} // :goto_0
} // .end method
private java.lang.String getStylusDeviceStatusData ( ) {
/* .locals 4 */
/* .line 312 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 314 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "pencil_dau"; // const-string v2, "pencil_dau"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 315 */
/* const-string/jumbo v1, "tip" */
final String v2 = "899.6.0.1.27101"; // const-string v2, "899.6.0.1.27101"
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 318 */
/* .line 316 */
/* :catch_0 */
/* move-exception v1 */
/* .line 317 */
/* .local v1, "exception":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "construct TrackEvent data fail!"; // const-string v3, "construct TrackEvent data fail!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "InputOneTrackUtil"; // const-string v3, "InputOneTrackUtil"
android.util.Slog .e ( v3,v2 );
/* .line 319 */
} // .end local v1 # "exception":Ljava/lang/Exception;
} // :goto_0
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
} // .end method
private void lambda$trackEvent$0 ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3 ) { //synthethic
/* .locals 4 */
/* .param p1, "appId" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "data" # Ljava/lang/String; */
/* .param p4, "flag" # I */
/* .line 161 */
v0 = this.mService;
final String v1 = "InputOneTrackUtil"; // const-string v1, "InputOneTrackUtil"
/* if-nez v0, :cond_0 */
/* .line 162 */
/* const-string/jumbo v0, "trackEvent: track service not bound" */
android.util.Slog .d ( v1,v0 );
/* .line 163 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/input/InputOneTrackUtil;->bindTrackService(Landroid/content/Context;)V */
/* .line 166 */
} // :cond_0
try { // :try_start_0
v0 = this.mService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 167 */
/* .line 169 */
} // :cond_1
/* const-string/jumbo v0, "trackEvent: track service is null" */
android.util.Slog .e ( v1,v0 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 173 */
} // :goto_0
/* .line 171 */
/* :catch_0 */
/* move-exception v0 */
/* .line 172 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "trackEvent: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.RemoteException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 174 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
private void lambda$trackEvents$1 ( java.lang.String p0, java.lang.String p1, java.util.List p2, Integer p3 ) { //synthethic
/* .locals 4 */
/* .param p1, "appId" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "dataList" # Ljava/util/List; */
/* .param p4, "flag" # I */
/* .line 179 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "trackEvents: appId " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " packageName "; // const-string v1, " packageName "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " size "; // const-string v1, " size "
v1 = (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InputOneTrackUtil"; // const-string v1, "InputOneTrackUtil"
android.util.Slog .d ( v1,v0 );
/* .line 180 */
v0 = this.mService;
/* if-nez v0, :cond_0 */
/* .line 181 */
/* const-string/jumbo v0, "trackEvents: track service not bound" */
android.util.Slog .d ( v1,v0 );
/* .line 182 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/input/InputOneTrackUtil;->bindTrackService(Landroid/content/Context;)V */
/* .line 185 */
} // :cond_0
try { // :try_start_0
v0 = this.mService;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 186 */
/* .line 188 */
} // :cond_1
/* const-string/jumbo v0, "trackEvents: track service is null" */
android.util.Slog .e ( v1,v0 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 192 */
} // :goto_0
/* .line 190 */
/* :catch_0 */
/* move-exception v0 */
/* .line 191 */
/* .local v0, "e":Landroid/os/RemoteException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "trackEvents: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.os.RemoteException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 193 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
public static Boolean shouldCountDevice ( android.view.InputEvent p0 ) {
/* .locals 4 */
/* .param p0, "event" # Landroid/view/InputEvent; */
/* .line 363 */
(( android.view.InputEvent ) p0 ).getDevice ( ); // invoke-virtual {p0}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
/* .line 364 */
/* .local v0, "device":Landroid/view/InputDevice; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 365 */
/* .line 367 */
} // :cond_0
v2 = (( android.view.InputDevice ) v0 ).isExternal ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->isExternal()Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = (( android.view.InputDevice ) v0 ).isVirtual ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->isVirtual()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 368 */
} // :cond_1
/* const/16 v2, 0x4002 */
v2 = (( android.view.InputEvent ) p0 ).isFromSource ( v2 ); // invoke-virtual {p0, v2}, Landroid/view/InputEvent;->isFromSource(I)Z
/* if-nez v2, :cond_2 */
/* .line 370 */
v2 = (( android.view.InputDevice ) v0 ).getProductId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I
v3 = (( android.view.InputDevice ) v0 ).getVendorId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I
/* .line 369 */
v2 = miui.hardware.input.MiuiKeyboardHelper .isXiaomiExternalDevice ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_3
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
/* .line 367 */
} // :cond_3
} // .end method
private void updateAlarmForTrack ( ) {
/* .locals 7 */
/* .line 119 */
v0 = this.mAlarmManager;
int v1 = 1; // const/4 v1, 0x1
/* .line 120 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* const-wide/32 v4, 0x5265c00 */
/* add-long/2addr v2, v4 */
final String v4 = "clear_external_device"; // const-string v4, "clear_external_device"
v5 = this.mExternalDeviceAlarmListener;
/* .line 122 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* .line 119 */
/* invoke-virtual/range {v0 ..v6}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 123 */
return;
} // .end method
/* # virtual methods */
public void track6FShortcut ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 202 */
final String v0 = "899.4.0.1.27102"; // const-string v0, "899.4.0.1.27102"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackKeyboardEvent ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V
/* .line 203 */
return;
} // .end method
public void trackEdgeSuppressionEvent ( Boolean p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 4 */
/* .param p1, "changed" # Z */
/* .param p2, "changedSize" # I */
/* .param p3, "changedType" # Ljava/lang/String; */
/* .param p4, "size" # I */
/* .param p5, "type" # Ljava/lang/String; */
/* .line 228 */
/* nop */
/* .line 229 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/input/InputOneTrackUtil;->getEdgeSuppressionStatusData(ZILjava/lang/String;ILjava/lang/String;)Ljava/lang/String; */
/* .line 228 */
final String v1 = "31000000735"; // const-string v1, "31000000735"
final String v2 = "com.xiaomi.edgesuppression"; // const-string v2, "com.xiaomi.edgesuppression"
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v1, v2, v0, v3 ); // invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 231 */
return;
} // .end method
public void trackEvent ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "appId" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "data" # Ljava/lang/String; */
/* .param p4, "flag" # I */
/* .line 160 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v7, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/InputOneTrackUtil;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 175 */
return;
} // .end method
public void trackEvents ( java.lang.String p0, java.lang.String p1, java.util.List p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "appId" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p4, "flag" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 178 */
/* .local p3, "dataList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
com.android.server.input.MiuiInputThread .getHandler ( );
/* new-instance v7, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda0; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/InputOneTrackUtil$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/InputOneTrackUtil;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 194 */
return;
} // .end method
public void trackExtendDevice ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "type" # Ljava/lang/String; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 234 */
final String v0 = "STYLUS"; // const-string v0, "STYLUS"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 2; // const/4 v1, 0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 235 */
/* nop */
/* .line 236 */
/* invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->getStylusDeviceStatusData()Ljava/lang/String; */
/* .line 235 */
final String v2 = "31000000824"; // const-string v2, "31000000824"
final String v3 = "com.xiaomi.extendDevice"; // const-string v3, "com.xiaomi.extendDevice"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v2, v3, v0, v1 ); // invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 238 */
} // :cond_0
/* nop */
/* .line 239 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/input/InputOneTrackUtil;->getExternalDeviceStatusData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 238 */
final String v2 = "31000000618"; // const-string v2, "31000000618"
final String v3 = "com.xiaomi.padkeyboard"; // const-string v3, "com.xiaomi.padkeyboard"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v2, v3, v0, v1 ); // invoke-virtual {p0, v2, v3, v0, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 241 */
} // :goto_0
return;
} // .end method
public void trackExternalDevice ( android.view.InputEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/InputEvent; */
/* .line 323 */
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mInputDeviceOneTrack;
/* .line 324 */
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
(( android.view.InputDevice ) v1 ).getName ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->getName()Ljava/lang/String;
/* .line 323 */
v0 = (( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack ) v0 ).hasDuplicationValue ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->hasDuplicationValue(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 327 */
} // :cond_0
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
(( android.view.InputDevice ) v0 ).getName ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;
/* .line 328 */
/* .local v0, "name":Ljava/lang/String; */
com.android.server.input.InputOneTrackUtil .getEventDeviceType ( p1 );
/* .line 329 */
/* .local v1, "type":Ljava/lang/String; */
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
v2 = (( android.view.InputDevice ) v2 ).getVendorId ( ); // invoke-virtual {v2}, Landroid/view/InputDevice;->getVendorId()I
/* .line 330 */
/* .local v2, "vendorId":I */
(( android.view.InputEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getDevice()Landroid/view/InputDevice;
v3 = (( android.view.InputDevice ) v3 ).getProductId ( ); // invoke-virtual {v3}, Landroid/view/InputDevice;->getProductId()I
/* .line 331 */
/* .local v3, "productId":I */
final String v4 = "UNKNOW"; // const-string v4, "UNKNOW"
v4 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 332 */
return;
/* .line 334 */
} // :cond_1
v4 = this.mInputDeviceOneTrack;
v4 = (( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack ) v4 ).hasDuplicationValue ( v0 ); // invoke-virtual {v4, v0}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->hasDuplicationValue(Ljava/lang/String;)Z
/* if-nez v4, :cond_2 */
/* .line 335 */
v4 = this.mInputDeviceOneTrack;
(( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack ) v4 ).putTrackValue ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->putTrackValue(Ljava/lang/String;Ljava/lang/String;)V
/* .line 336 */
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackExtendDevice ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackExtendDevice(Ljava/lang/String;Ljava/lang/String;)V
/* .line 337 */
v4 = miui.hardware.input.MiuiKeyboardHelper .isXiaomiKeyboard ( v3,v2 );
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.mInputDeviceOneTrack;
/* .line 338 */
v4 = (( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack ) v4 ).hasDuplicationXiaomiDevice ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->hasDuplicationXiaomiDevice(I)Z
/* if-nez v4, :cond_2 */
/* .line 340 */
v4 = this.mInputDeviceOneTrack;
(( com.android.server.input.InputOneTrackUtil$InputDeviceOneTrack ) v4 ).putXiaomiDeviceInfo ( v2, v1 ); // invoke-virtual {v4, v2, v1}, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->putXiaomiDeviceInfo(ILjava/lang/String;)V
/* .line 341 */
v4 = com.android.server.input.padkeyboard.KeyboardInteraction.INTERACTION;
v4 = (( com.android.server.input.padkeyboard.KeyboardInteraction ) v4 ).isConnectIICLocked ( ); // invoke-virtual {v4}, Lcom/android/server/input/padkeyboard/KeyboardInteraction;->isConnectIICLocked()Z
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackKeyboardDAU ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardDAU(Z)V
/* .line 344 */
} // :cond_2
return;
/* .line 325 */
} // .end local v0 # "name":Ljava/lang/String;
} // .end local v1 # "type":Ljava/lang/String;
} // .end local v2 # "vendorId":I
} // .end local v3 # "productId":I
} // :cond_3
} // :goto_0
return;
} // .end method
public void trackFlingEvent ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 248 */
final String v0 = "com.xiaomi.input.shortcut"; // const-string v0, "com.xiaomi.input.shortcut"
int v1 = 2; // const/4 v1, 0x2
final String v2 = "31000401650"; // const-string v2, "31000401650"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v2, v0, p1, v1 ); // invoke-virtual {p0, v2, v0, p1, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 249 */
return;
} // .end method
public void trackKeyboardDAU ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "data" # Z */
/* .line 218 */
/* nop */
/* .line 219 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/InputOneTrackUtil;->getKeyboardDAUStatusData(Z)Ljava/lang/String; */
/* .line 218 */
final String v1 = "31000000824"; // const-string v1, "31000000824"
final String v2 = "com.xiaomi.extendDevice"; // const-string v2, "com.xiaomi.extendDevice"
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v1, v2, v0, v3 ); // invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 220 */
return;
} // .end method
public void trackKeyboardEvent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "data" # Ljava/lang/String; */
/* .param p2, "tip" # Ljava/lang/String; */
/* .line 210 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 211 */
return;
/* .line 213 */
} // :cond_0
/* nop */
/* .line 214 */
/* invoke-direct {p0, p2, p1}, Lcom/android/server/input/InputOneTrackUtil;->getKeyboardShortcutKeyStatusData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 213 */
final String v1 = "31000000824"; // const-string v1, "31000000824"
final String v2 = "com.xiaomi.extendDevice"; // const-string v2, "com.xiaomi.extendDevice"
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v1, v2, v0, v3 ); // invoke-virtual {p0, v1, v2, v0, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 215 */
return;
} // .end method
public void trackKeyboardShortcut ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 198 */
final String v0 = "899.4.0.1.20553"; // const-string v0, "899.4.0.1.20553"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackKeyboardEvent ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V
/* .line 199 */
return;
} // .end method
public void trackShortcutEvent ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 244 */
/* invoke-direct {p0}, Lcom/android/server/input/InputOneTrackUtil;->getShortcutAppId()Ljava/lang/String; */
final String v1 = "com.xiaomi.input.shortcut"; // const-string v1, "com.xiaomi.input.shortcut"
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v0, v1, p1, v2 ); // invoke-virtual {p0, v0, v1, p1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 245 */
return;
} // .end method
public void trackStylusEvent ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 223 */
final String v0 = "com.xiaomi.extendDevice"; // const-string v0, "com.xiaomi.extendDevice"
int v1 = 2; // const/4 v1, 0x2
final String v2 = "31000000824"; // const-string v2, "31000000824"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvent ( v2, v0, p1, v1 ); // invoke-virtual {p0, v2, v0, p1, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
/* .line 224 */
return;
} // .end method
public void trackTouchpadEvents ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 251 */
/* .local p1, "data":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v0 = "com.xiaomi.extendDevice"; // const-string v0, "com.xiaomi.extendDevice"
int v1 = 2; // const/4 v1, 0x2
final String v2 = "31000000824"; // const-string v2, "31000000824"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackEvents ( v2, v0, p1, v1 ); // invoke-virtual {p0, v2, v0, p1, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackEvents(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V
/* .line 253 */
return;
} // .end method
public void trackVoice2Word ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 206 */
final String v0 = "899.4.0.1.27103"; // const-string v0, "899.4.0.1.27103"
(( com.android.server.input.InputOneTrackUtil ) p0 ).trackKeyboardEvent ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/input/InputOneTrackUtil;->trackKeyboardEvent(Ljava/lang/String;Ljava/lang/String;)V
/* .line 207 */
return;
} // .end method
public void unbindTrackService ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 154 */
/* iget-boolean v0, p0, Lcom/android/server/input/InputOneTrackUtil;->mIsBound:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 155 */
v0 = this.mConnection;
(( android.content.Context ) p1 ).unbindService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 157 */
} // :cond_0
return;
} // .end method
