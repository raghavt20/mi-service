.class public Lcom/android/server/input/InputShellCommandStubImpl;
.super Ljava/lang/Object;
.source "InputShellCommandStubImpl.java"

# interfaces
.implements Lcom/android/server/input/InputShellCommandStub;


# static fields
.field private static final INVALID_ARGUMENTS:Ljava/lang/String; = "Error: Invalid arguments for command: "


# instance fields
.field private mInputShellCommand:Lcom/android/server/input/InputShellCommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getNextArg()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/android/server/input/InputShellCommandStubImpl;->mInputShellCommand:Lcom/android/server/input/InputShellCommand;

    invoke-virtual {v0}, Lcom/android/server/input/InputShellCommand;->getNextArg()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getNextArgRequired()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/android/server/input/InputShellCommandStubImpl;->mInputShellCommand:Lcom/android/server/input/InputShellCommand;

    invoke-virtual {v0}, Lcom/android/server/input/InputShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleDefaultCommands(Ljava/lang/String;)V
    .locals 1
    .param p1, "arg"    # Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/android/server/input/InputShellCommandStubImpl;->mInputShellCommand:Lcom/android/server/input/InputShellCommand;

    invoke-virtual {v0, p1}, Lcom/android/server/input/InputShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    .line 63
    return-void
.end method

.method private sendSwipe()V
    .locals 16

    .line 38
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 39
    .local v0, "x1":I
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 40
    .local v8, "y1":I
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 41
    .local v9, "x2":I
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 42
    .local v10, "y2":I
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArg()Ljava/lang/String;

    move-result-object v11

    .line 43
    .local v11, "durationArg":Ljava/lang/String;
    if-eqz v11, :cond_0

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    .line 44
    .local v1, "duration":I
    :goto_0
    if-gez v1, :cond_1

    .line 45
    const/16 v1, 0x12c

    move v12, v1

    goto :goto_1

    .line 44
    :cond_1
    move v12, v1

    .line 47
    .end local v1    # "duration":I
    .local v12, "duration":I
    :goto_1
    const-class v1, Lcom/android/server/input/MiuiInputManagerInternal;

    .line 48
    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/android/server/input/MiuiInputManagerInternal;

    .line 49
    .local v13, "miuiInputManagerInternal":Lcom/android/server/input/MiuiInputManagerInternal;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArg()Ljava/lang/String;

    move-result-object v14

    .line 50
    .local v14, "everyDelayTimeArg":Ljava/lang/String;
    if-nez v14, :cond_2

    .line 51
    move-object v1, v13

    move v2, v0

    move v3, v8

    move v4, v9

    move v5, v10

    move v6, v12

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/input/MiuiInputManagerInternal;->swipe(IIIII)Z

    .line 52
    return-void

    .line 54
    :cond_2
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 55
    .local v1, "everyDelayTime":I
    if-gez v1, :cond_3

    .line 56
    const/4 v1, 0x5

    move v15, v1

    goto :goto_2

    .line 55
    :cond_3
    move v15, v1

    .line 58
    .end local v1    # "everyDelayTime":I
    .local v15, "everyDelayTime":I
    :goto_2
    move-object v1, v13

    move v2, v0

    move v3, v8

    move v4, v9

    move v5, v10

    move v6, v12

    move v7, v15

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/input/MiuiInputManagerInternal;->swipe(IIIIII)Z

    .line 59
    return-void
.end method


# virtual methods
.method public init(Lcom/android/server/input/InputShellCommand;)V
    .locals 0
    .param p1, "inputShellCommand"    # Lcom/android/server/input/InputShellCommand;

    .line 13
    iput-object p1, p0, Lcom/android/server/input/InputShellCommandStubImpl;->mInputShellCommand:Lcom/android/server/input/InputShellCommand;

    .line 14
    return-void
.end method

.method public onCommand(Ljava/lang/String;)Z
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .line 18
    move-object v0, p1

    .line 19
    .local v0, "arg":Ljava/lang/String;
    const-string v1, "miui"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 21
    const/4 v1, 0x0

    return v1

    .line 24
    :cond_0
    invoke-direct {p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String;

    move-result-object v0

    .line 26
    :try_start_0
    const-string/jumbo v1, "swipe"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    invoke-direct {p0}, Lcom/android/server/input/InputShellCommandStubImpl;->sendSwipe()V

    goto :goto_0

    .line 29
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/server/input/InputShellCommandStubImpl;->handleDefaultCommands(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    nop

    .line 34
    const/4 v1, 0x1

    return v1

    .line 31
    :catch_0
    move-exception v1

    .line 32
    .local v1, "ex":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: Invalid arguments for command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
