.class public final Lcom/android/server/input/MiuiInputManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiInputManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiuiInputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private mService:Lcom/android/server/input/MiuiInputManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 110
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 113
    invoke-static {}, Lcom/android/server/input/InputManagerServiceStub;->getInstance()Lcom/android/server/input/InputManagerServiceStub;

    move-result-object v0

    .line 114
    .local v0, "inputManagerServiceStub":Lcom/android/server/input/InputManagerServiceStub;
    instance-of v1, v0, Lcom/android/server/input/InputManagerServiceStubImpl;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/android/server/input/InputManagerServiceStubImpl;

    .line 115
    .local v1, "impl":Lcom/android/server/input/InputManagerServiceStubImpl;
    invoke-virtual {v1}, Lcom/android/server/input/InputManagerServiceStubImpl;->getMiuiInputManagerService()Lcom/android/server/input/MiuiInputManagerService;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/input/MiuiInputManagerService$Lifecycle;->mService:Lcom/android/server/input/MiuiInputManagerService;

    .line 117
    .end local v1    # "impl":Lcom/android/server/input/InputManagerServiceStubImpl;
    :cond_0
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$Lifecycle;->mService:Lcom/android/server/input/MiuiInputManagerService;

    if-nez v0, :cond_0

    .line 122
    const-string v0, "MiuiInputManagerService"

    const-string v1, "MiuiInputManagerService not init..."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return-void

    .line 125
    :cond_0
    const-string v1, "MiuiInputManager"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/input/MiuiInputManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 126
    return-void
.end method
