public class com.android.server.input.MiInputPhotoHandleManager {
	 /* .source "MiInputPhotoHandleManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_HANDLE_STATE_CHANGED;
private static final java.lang.String EXTRA_HANDLE_CONNECT_STATE;
private static final java.lang.String EXTRA_HANDLE_PID;
private static final java.lang.String EXTRA_HANDLE_VID;
private static final Integer HANDLE_STATUS_CONNECTION;
private static final Integer HANDLE_STATUS_DISCONNECTION;
public static final java.lang.String PHOTO_HANDLE_HAS_BEEN_CONNECTED;
private static final java.lang.String TAG;
private static com.android.server.input.MiInputPhotoHandleManager sMiInputPhotoHandleManager;
/* # instance fields */
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private android.os.Handler mHandler;
private com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private Integer mPhotoHandleConnectionStatus;
private Boolean mPhotoHandleHasConnected;
private final android.content.BroadcastReceiver mPhotoHandleStatusReceiver;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.input.MiInputPhotoHandleManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Integer -$$Nest$fgetmPhotoHandleConnectionStatus ( com.android.server.input.MiInputPhotoHandleManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleConnectionStatus:I */
} // .end method
static void -$$Nest$fputmPhotoHandleConnectionStatus ( com.android.server.input.MiInputPhotoHandleManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleConnectionStatus:I */
	 return;
} // .end method
static void -$$Nest$mnotifyHandleConnectStatus ( com.android.server.input.MiInputPhotoHandleManager p0, Boolean p1, Integer p2, Integer p3 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/input/MiInputPhotoHandleManager;->notifyHandleConnectStatus(ZII)V */
	 return;
} // .end method
private com.android.server.input.MiInputPhotoHandleManager ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 62 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 137 */
	 /* new-instance v0, Lcom/android/server/input/MiInputPhotoHandleManager$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/MiInputPhotoHandleManager$1;-><init>(Lcom/android/server/input/MiInputPhotoHandleManager;)V */
	 this.mPhotoHandleStatusReceiver = v0;
	 /* .line 63 */
	 this.mContext = p1;
	 /* .line 64 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 this.mContentResolver = v0;
	 /* .line 65 */
	 return;
} // .end method
public static com.android.server.input.MiInputPhotoHandleManager getInstance ( android.content.Context p0 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .line 54 */
	 /* const-class v0, Lcom/android/server/input/MiInputPhotoHandleManager; */
	 /* monitor-enter v0 */
	 /* .line 55 */
	 try { // :try_start_0
		 v1 = com.android.server.input.MiInputPhotoHandleManager.sMiInputPhotoHandleManager;
		 /* if-nez v1, :cond_0 */
		 /* .line 56 */
		 /* new-instance v1, Lcom/android/server/input/MiInputPhotoHandleManager; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/input/MiInputPhotoHandleManager;-><init>(Landroid/content/Context;)V */
		 /* .line 58 */
	 } // :cond_0
	 v1 = com.android.server.input.MiInputPhotoHandleManager.sMiInputPhotoHandleManager;
	 /* monitor-exit v0 */
	 /* .line 59 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
private void initIntentFilter ( ) {
	 /* .locals 4 */
	 /* .line 103 */
	 /* new-instance v0, Landroid/content/IntentFilter; */
	 /* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
	 /* .line 104 */
	 /* .local v0, "photoHandleFilter":Landroid/content/IntentFilter; */
	 final String v1 = "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"; // const-string v1, "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"
	 (( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
	 /* .line 105 */
	 v1 = this.mContext;
	 v2 = this.mPhotoHandleStatusReceiver;
	 int v3 = 2; // const/4 v3, 0x2
	 (( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
	 /* .line 107 */
	 return;
} // .end method
private void notifyHandleConnectStatus ( Boolean p0, Integer p1, Integer p2 ) {
	 /* .locals 4 */
	 /* .param p1, "connection" # Z */
	 /* .param p2, "pid" # I */
	 /* .param p3, "vid" # I */
	 /* .line 118 */
	 v0 = 	 (( com.android.server.input.MiInputPhotoHandleManager ) p0 ).getPhotoHandleDeviceId ( p2, p3 ); // invoke-virtual {p0, p2, p3}, Lcom/android/server/input/MiInputPhotoHandleManager;->getPhotoHandleDeviceId(II)I
	 /* .line 119 */
	 /* .local v0, "deviceId":I */
	 v1 = this.mMiuiInputManagerInternal;
	 (( com.android.server.input.MiuiInputManagerInternal ) v1 ).notifyPhotoHandleConnectionStatus ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/input/MiuiInputManagerInternal;->notifyPhotoHandleConnectionStatus(ZI)V
	 /* .line 120 */
	 com.android.server.policy.PhoneWindowManagerStub .getInstance ( );
	 /* .line 122 */
	 /* iget-boolean v1, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z */
	 /* if-nez v1, :cond_0 */
	 /* .line 123 */
	 v1 = this.mContentResolver;
	 final String v2 = "photo_handle_has_been_connected"; // const-string v2, "photo_handle_has_been_connected"
	 int v3 = 1; // const/4 v3, 0x1
	 android.provider.Settings$System .putInt ( v1,v2,v3 );
	 /* .line 124 */
	 /* iput-boolean v3, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z */
	 /* .line 126 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyHandleConnectStatus, connection="; // const-string v2, "notifyHandleConnectStatus, connection="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " pid="; // const-string v2, " pid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " vid="; // const-string v2, " vid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " deviceId="; // const-string v2, " deviceId="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiInputPhotoHandleManager"; // const-string v2, "MiInputPhotoHandleManager"
android.util.Slog .d ( v2,v1 );
/* .line 128 */
return;
} // .end method
/* # virtual methods */
public Integer getPhotoHandleDeviceId ( Integer p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "pid" # I */
/* .param p2, "vid" # I */
/* .line 84 */
v0 = this.mContext;
/* const-class v1, Landroid/hardware/input/InputManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/input/InputManager; */
/* .line 85 */
/* .local v0, "inputManager":Landroid/hardware/input/InputManager; */
final String v1 = "MiInputPhotoHandleManager"; // const-string v1, "MiInputPhotoHandleManager"
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 86 */
	 (( android.hardware.input.InputManager ) v0 ).getInputDeviceIds ( ); // invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I
	 /* array-length v3, v2 */
	 int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* aget v5, v2, v4 */
/* .line 87 */
/* .local v5, "deviceId":I */
(( android.hardware.input.InputManager ) v0 ).getInputDevice ( v5 ); // invoke-virtual {v0, v5}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 88 */
/* .local v6, "inputDevice":Landroid/view/InputDevice; */
v7 = (( android.view.InputDevice ) v6 ).getProductId ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->getProductId()I
/* if-ne v7, p1, :cond_0 */
v7 = (( android.view.InputDevice ) v6 ).getVendorId ( ); // invoke-virtual {v6}, Landroid/view/InputDevice;->getVendorId()I
/* if-ne v7, p2, :cond_0 */
/* .line 89 */
/* .line 86 */
} // .end local v5 # "deviceId":I
} // .end local v6 # "inputDevice":Landroid/view/InputDevice;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 93 */
} // :cond_1
final String v2 = "inputManager is null"; // const-string v2, "inputManager is null"
android.util.Slog .d ( v1,v2 );
/* .line 95 */
} // :cond_2
final String v2 = "not found deviceId"; // const-string v2, "not found deviceId"
android.util.Slog .d ( v1,v2 );
/* .line 96 */
int v1 = -1; // const/4 v1, -0x1
} // .end method
public void init ( ) {
/* .locals 3 */
/* .line 68 */
final String v0 = "MiInputPhotoHandleManager"; // const-string v0, "MiInputPhotoHandleManager"
final String v1 = "init photo handle manager"; // const-string v1, "init photo handle manager"
android.util.Slog .d ( v0,v1 );
/* .line 69 */
/* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
this.mMiuiInputManagerInternal = v0;
/* .line 70 */
/* new-instance v0, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize; */
com.android.server.input.MiuiInputThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/input/MiInputPhotoHandleManager$HandleStatusSynchronize;-><init>(Lcom/android/server/input/MiInputPhotoHandleManager;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 71 */
/* invoke-direct {p0}, Lcom/android/server/input/MiInputPhotoHandleManager;->initIntentFilter()V */
/* .line 72 */
v0 = this.mContentResolver;
final String v1 = "photo_handle_has_been_connected"; // const-string v1, "photo_handle_has_been_connected"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* iput-boolean v2, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z */
/* .line 74 */
return;
} // .end method
public Boolean isPhotoHandleHasConnected ( ) {
/* .locals 1 */
/* .line 131 */
/* iget-boolean v0, p0, Lcom/android/server/input/MiInputPhotoHandleManager;->mPhotoHandleHasConnected:Z */
} // .end method
