.class Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;
.super Ljava/lang/Object;
.source "InputOneTrackUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/InputOneTrackUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InputDeviceOneTrack"
.end annotation


# static fields
.field static final TYPE_KEYBOARD:Ljava/lang/String; = "KEYBOARD"

.field static final TYPE_MOUSE:Ljava/lang/String; = "MOUSE"

.field static final TYPE_STYLUS:Ljava/lang/String; = "STYLUS"

.field static final TYPE_TOUCHPAD:Ljava/lang/String; = "TOUCHPAD"

.field static final TYPE_UNKNOW:Ljava/lang/String; = "UNKNOW"


# instance fields
.field private mTrackMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mXiaomiDeviceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmTrackMap(Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mTrackMap:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmXiaomiDeviceMap(Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mXiaomiDeviceMap:Ljava/util/Map;

    return-object p0
.end method

.method constructor <init>()V
    .locals 1

    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mTrackMap:Ljava/util/Map;

    .line 381
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mXiaomiDeviceMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public hasDuplicationValue(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 392
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mTrackMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasDuplicationXiaomiDevice(I)Z
    .locals 2
    .param p1, "vendorId"    # I

    .line 396
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mXiaomiDeviceMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public putTrackValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .line 384
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mTrackMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    return-void
.end method

.method public putXiaomiDeviceInfo(ILjava/lang/String;)V
    .locals 2
    .param p1, "vendorId"    # I
    .param p2, "type"    # Ljava/lang/String;

    .line 388
    iget-object v0, p0, Lcom/android/server/input/InputOneTrackUtil$InputDeviceOneTrack;->mXiaomiDeviceMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    return-void
.end method
