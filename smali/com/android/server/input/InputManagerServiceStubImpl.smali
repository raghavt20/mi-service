.class public Lcom/android/server/input/InputManagerServiceStubImpl;
.super Ljava/lang/Object;
.source "InputManagerServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/input/InputManagerServiceStub;


# static fields
.field private static final ANDROID_SYSTEM_UI:Ljava/lang/String; = "com.android.systemui"

.field private static final BERSERK_MODE:Z

.field private static final INPUT_LOG_LEVEL:Ljava/lang/String; = "input_log_level"

.field private static final KEY_EVENT_FLAG_NAVIGATION_SYSTEM_UI:I = -0x80000000

.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final KEY_STYLUS_QUICK_NOTE_MODE:Ljava/lang/String; = "stylus_quick_note_screen_off"

.field private static final MIUI_INPUTMETHOD:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SHOW_TOUCHES_PREVENTRECORDER:Ljava/lang/String; = "show_touches_preventrecord"

.field private static final SUPPORT_QUICK_NOTE_DEFAULT:Z

.field private static final TAG_INPUT:Ljava/lang/String; = "MIUIInput"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCustomizeInputMethod:Z

.field private mFromSetInputLevel:I

.field private mHandler:Landroid/os/Handler;

.field private mInputDebugLevel:I

.field private mInputManagerService:Lcom/android/server/input/InputManagerService;

.field private mMiuiCustomizeShortCutUtils:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

.field private mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

.field private mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

.field private mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

.field private mNativeInputManagerService:Lcom/android/server/input/NativeInputManagerService;

.field private mPointerLocationShow:I

.field private mPtr:J

.field private mSetDebugLevel:I

.field private mShowTouches:I

.field private mShowTouchesPreventRecord:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/InputManagerServiceStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msetCtsMode(Lcom/android/server/input/InputManagerServiceStubImpl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerServiceStubImpl;->setCtsMode(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateFromInputLevelSetting(Lcom/android/server/input/InputManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateFromInputLevelSetting()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateMouseGestureSettings(Lcom/android/server/input/InputManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateMouseGestureSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateOnewayModeFromSettings(Lcom/android/server/input/InputManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateOnewayModeFromSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePointerLocationFromSettings(Lcom/android/server/input/InputManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updatePointerLocationFromSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSynergyModeFromSettings(Lcom/android/server/input/InputManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateSynergyModeFromSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateTouchesPreventRecorderFromSettings(Lcom/android/server/input/InputManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchesPreventRecorderFromSettings()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 49
    nop

    .line 50
    const-string v0, "persist.berserk.mode.support"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/input/InputManagerServiceStubImpl;->BERSERK_MODE:Z

    .line 53
    nop

    .line 54
    const-string v0, "persist.sys.quick.note.enable.default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/input/InputManagerServiceStubImpl;->SUPPORT_QUICK_NOTE_DEFAULT:Z

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/input/InputManagerServiceStubImpl;->MIUI_INPUTMETHOD:Ljava/util/List;

    .line 72
    const-string v1, "com.baidu.input_mi"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    const-string v1, "com.sohu.inputmethod.sogou.xiaomi"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    const-string v1, "com.iflytek.inputmethod.miui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "InputManagerServiceStubImpl"

    iput-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->TAG:Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 66
    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    .line 67
    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I

    .line 68
    iput-boolean v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mCustomizeInputMethod:Z

    return-void
.end method

.method private ensureMiuiMagicPointerServiceInit()Z
    .locals 2

    .line 597
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 598
    return v1

    .line 600
    :cond_0
    const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    iput-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    .line 602
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private static getPackageNameForPid(I)Ljava/lang/String;
    .locals 1
    .param p0, "pid"    # I

    .line 540
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceImpl;->getInstance()Lcom/android/server/am/ActivityManagerServiceImpl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameForPid(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;
    .locals 1

    .line 544
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    if-nez v0, :cond_0

    .line 545
    const-class v0, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    iput-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiPadKeyboardManager:Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    return-object v0
.end method

.method private getPointerLocationSettings(I)I
    .locals 4
    .param p1, "defaultValue"    # I

    .line 384
    move v0, p1

    .line 386
    .local v0, "result":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "pointer_location"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 390
    goto :goto_0

    .line 388
    :catch_0
    move-exception v1

    .line 389
    .local v1, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 391
    .end local v1    # "snfe":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    return v0
.end method

.method private getShowTouchesPreventRecordSetting(I)I
    .locals 4
    .param p1, "defaultValue"    # I

    .line 412
    move v0, p1

    .line 414
    .local v0, "result":I
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_touches_preventrecord"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 418
    goto :goto_0

    .line 416
    :catch_0
    move-exception v1

    .line 417
    .local v1, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 419
    .end local v1    # "snfe":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    return v0
.end method

.method private registerDefaultInputMethodSettingObserver()V
    .locals 5

    .line 428
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 429
    const-string v1, "default_input_method"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$6;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$6;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 428
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 436
    return-void
.end method

.method private registerGameMode()V
    .locals 5

    .line 478
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 479
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$8;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$8;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 478
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 486
    return-void
.end method

.method private registerInputLevelSelect()V
    .locals 5

    .line 450
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 451
    const-string v1, "input_log_level"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$7;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$7;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 450
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 458
    return-void
.end method

.method private registerMouseGestureSettingObserver()V
    .locals 5

    .line 298
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 299
    const-string v1, "mouse_gesture_naturalscroll"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$2;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$2;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 298
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 306
    return-void
.end method

.method private registerPointerLocationSettingObserver()V
    .locals 5

    .line 366
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 367
    const-string v1, "pointer_location"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$4;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$4;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 366
    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 374
    return-void
.end method

.method private registerSynergyModeSettingObserver()V
    .locals 5

    .line 268
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 269
    const-string/jumbo v1, "synergy_mode"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$1;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$1;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 268
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 276
    return-void
.end method

.method private registerTouchesPreventRecordSettingObserver()V
    .locals 5

    .line 396
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 397
    const-string/jumbo v1, "show_touches_preventrecord"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$5;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$5;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 396
    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 404
    return-void
.end method

.method private setCtsMode(Z)V
    .locals 2
    .param p1, "ctsMode"    # Z

    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CtsMode changed, mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputManagerServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 349
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setCtsMode(Z)V

    .line 350
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 351
    return-void
.end method

.method private setInputLogLevel()V
    .locals 4

    .line 552
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouches:I

    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPointerLocationShow:I

    or-int/2addr v0, v1

    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouchesPreventRecord:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    .line 553
    invoke-static {}, Lcom/android/server/input/config/InputDebugConfig;->getInstance()Lcom/android/server/input/config/InputDebugConfig;

    move-result-object v0

    .line 554
    .local v0, "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig;
    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I

    or-int/2addr v1, v2

    iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputDebugConfig;->setInputDebugFromDump(I)V

    .line 555
    invoke-virtual {v0}, Lcom/android/server/input/config/InputDebugConfig;->flushToNative()V

    .line 556
    invoke-static {}, Lcom/android/server/policy/MiuiInputLog;->getInstance()Lcom/android/server/policy/MiuiInputLog;

    move-result-object v1

    iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    iget v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I

    or-int/2addr v2, v3

    iget v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    or-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/android/server/policy/MiuiInputLog;->setLogLevel(I)V

    .line 557
    return-void
.end method

.method private setSynergyMode(I)V
    .locals 2
    .param p1, "synergyMode"    # I

    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SynergyMode changed, mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InputManagerServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 287
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setSynergyMode(I)V

    .line 288
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 289
    return-void
.end method

.method private switchMouseNaturalScrollStatus(Z)V
    .locals 1
    .param p1, "mouseNaturalScrollStatus"    # Z

    .line 309
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 310
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setMouseNaturalScrollStatus(Z)V

    .line 311
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 312
    return-void
.end method

.method private switchPadMode(Z)V
    .locals 1
    .param p1, "padMode"    # Z

    .line 315
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 316
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setPadMode(Z)V

    .line 317
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 318
    return-void
.end method

.method private updateDebugLevel()V
    .locals 3

    .line 354
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouches:I

    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPointerLocationShow:I

    or-int/2addr v0, v1

    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouchesPreventRecord:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    .line 355
    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 356
    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    .line 358
    :cond_0
    invoke-static {}, Lcom/android/server/input/config/InputDebugConfig;->getInstance()Lcom/android/server/input/config/InputDebugConfig;

    move-result-object v0

    .line 359
    .local v0, "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig;
    iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/input/config/InputDebugConfig;->setInputDispatcherMajor(II)V

    .line 360
    invoke-virtual {v0}, Lcom/android/server/input/config/InputDebugConfig;->flushToNative()V

    .line 361
    invoke-static {}, Lcom/android/server/policy/MiuiInputLog;->getInstance()Lcom/android/server/policy/MiuiInputLog;

    move-result-object v1

    iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I

    invoke-virtual {v1, v2}, Lcom/android/server/policy/MiuiInputLog;->setLogLevel(I)V

    .line 362
    return-void
.end method

.method private updateFromInputLevelSetting()V
    .locals 4

    .line 461
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string v3, "input_log_level"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I

    .line 463
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 464
    return-void
.end method

.method private updateMouseGestureSettings()V
    .locals 3

    .line 292
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mouse_gesture_naturalscroll"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 294
    .local v0, "mouseNaturalScroll":I
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/server/input/InputManagerServiceStubImpl;->switchMouseNaturalScrollStatus(Z)V

    .line 295
    return-void
.end method

.method private updateOnewayModeFromSettings()V
    .locals 5

    .line 489
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "gb_boosting"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v3

    .line 491
    .local v0, "isGameMode":Z
    :goto_0
    nop

    .line 492
    const-string v2, "ro.miui.cts"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 491
    xor-int/2addr v2, v1

    const-string v4, "persist.sys.miui_optimization"

    invoke-static {v4, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    xor-int/2addr v2, v1

    .line 493
    .local v2, "isCtsMode":Z
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v4

    .line 494
    .local v4, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    if-eqz v0, :cond_1

    if-nez v2, :cond_1

    move v3, v1

    :cond_1
    invoke-virtual {v4, v3}, Lcom/android/server/input/config/InputCommonConfig;->setOnewayMode(Z)V

    .line 495
    invoke-virtual {v4}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 496
    return-void
.end method

.method private updatePointerLocationFromSettings()V
    .locals 1

    .line 378
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPointerLocationSettings(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPointerLocationShow:I

    .line 379
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDebugLevel()V

    .line 380
    return-void
.end method

.method private updateSynergyModeFromSettings()V
    .locals 3

    .line 279
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "synergy_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 281
    .local v0, "synergyMode":I
    invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setSynergyMode(I)V

    .line 282
    return-void
.end method

.method private updateTouchesPreventRecorderFromSettings()V
    .locals 1

    .line 407
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getShowTouchesPreventRecordSetting(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouchesPreventRecord:I

    .line 408
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDebugLevel()V

    .line 409
    return-void
.end method


# virtual methods
.method public beforeInjectEvent(Landroid/view/InputEvent;I)V
    .locals 6
    .param p1, "event"    # Landroid/view/InputEvent;
    .param p2, "pid"    # I

    .line 520
    instance-of v0, p1, Landroid/view/KeyEvent;

    const-string v1, " action "

    const-string v2, "Input event injection from pid "

    const-string v3, "MIUIInput"

    if-eqz v0, :cond_1

    .line 521
    move-object v0, p1

    check-cast v0, Landroid/view/KeyEvent;

    .line 522
    .local v0, "keyEvent":Landroid/view/KeyEvent;
    const-string v4, "com.android.systemui"

    invoke-static {p2}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPackageNameForPid(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 523
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getFlags()I

    move-result v4

    const/high16 v5, -0x80000000

    or-int/2addr v4, v5

    invoke-virtual {v0, v4}, Landroid/view/KeyEvent;->setFlags(I)V

    .line 525
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 526
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-static {v2}, Landroid/view/KeyEvent;->actionToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " keycode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 525
    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "keyEvent":Landroid/view/KeyEvent;
    goto :goto_0

    .line 527
    :cond_1
    instance-of v0, p1, Landroid/view/MotionEvent;

    if-eqz v0, :cond_3

    .line 528
    move-object v0, p1

    check-cast v0, Landroid/view/MotionEvent;

    .line 529
    .local v0, "motionEvent":Landroid/view/MotionEvent;
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    .line 530
    .local v4, "maskedAction":I
    if-eqz v4, :cond_2

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    .line 532
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 533
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-static {v2}, Landroid/view/MotionEvent;->actionToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 532
    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 527
    .end local v0    # "motionEvent":Landroid/view/MotionEvent;
    .end local v4    # "maskedAction":I
    :cond_3
    :goto_0
    nop

    .line 537
    :cond_4
    :goto_1
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 11
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 141
    const-string v0, "InputManagerServiceStubImpl"

    const-string v1, "dump InputManagerServiceStubImpl"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    array-length v0, p3

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const-string v0, "debuglog"

    aget-object v4, p3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "open Input debuglog, "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, p3, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v3, p3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 145
    aget-object v0, p3, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 146
    invoke-static {}, Lcom/android/server/input/config/InputDebugConfig;->getInstance()Lcom/android/server/input/config/InputDebugConfig;

    move-result-object v0

    .line 147
    .local v0, "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig;
    iget v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    invoke-virtual {v0, v3}, Lcom/android/server/input/config/InputDebugConfig;->setInputDebugFromDump(I)V

    .line 148
    invoke-virtual {v0}, Lcom/android/server/input/config/InputDebugConfig;->flushToNative()V

    .line 149
    invoke-static {}, Lcom/android/server/policy/MiuiInputLog;->getInstance()Lcom/android/server/policy/MiuiInputLog;

    move-result-object v3

    iget v4, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    invoke-virtual {v3, v4}, Lcom/android/server/policy/MiuiInputLog;->setLogLevel(I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    return v2

    .line 151
    .end local v0    # "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/ClassCastException;
    const-string v2, "open Reader and Dispatcher debuglog, ClassCastException!!!"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 153
    .end local v0    # "e":Ljava/lang/ClassCastException;
    goto/16 :goto_6

    .line 154
    :cond_0
    array-length v0, p3

    const/4 v4, 0x3

    if-ne v0, v4, :cond_4

    const-string v0, "logging"

    aget-object v5, p3, v1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    const-string v0, "enable-text"

    aget-object v5, p3, v2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v5, "InputReader"

    const-string v6, "MIUIInput"

    const-string v7, "InputDispatcher"

    const-string v8, "InputTransport"

    const/4 v9, -0x1

    if-eqz v0, :cond_2

    aget-object v0, p3, v3

    if-eqz v0, :cond_2

    .line 156
    aget-object v0, p3, v3

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v3

    goto :goto_1

    :sswitch_1
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_2
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :sswitch_3
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v4

    goto :goto_1

    :goto_0
    move v2, v9

    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_2

    .line 170
    :pswitch_0
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 171
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 172
    goto :goto_2

    .line 166
    :pswitch_1
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 167
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 168
    goto :goto_2

    .line 162
    :pswitch_2
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 163
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 164
    goto :goto_2

    .line 158
    :pswitch_3
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    or-int/2addr v0, v3

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 159
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 160
    nop

    .line 175
    :goto_2
    goto/16 :goto_6

    .line 176
    :cond_2
    const-string v0, "disable-text"

    aget-object v10, p3, v2

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    aget-object v0, p3, v3

    if-eqz v0, :cond_5

    .line 177
    aget-object v0, p3, v3

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_1

    :cond_3
    goto :goto_3

    :sswitch_4
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    goto :goto_4

    :sswitch_5
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v1

    goto :goto_4

    :sswitch_6
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_4

    :sswitch_7
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v4

    goto :goto_4

    :goto_3
    move v2, v9

    :goto_4
    packed-switch v2, :pswitch_data_1

    goto :goto_5

    .line 191
    :pswitch_4
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    and-int/lit8 v2, v0, 0x20

    not-int v2, v2

    and-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 192
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 193
    goto :goto_5

    .line 187
    :pswitch_5
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    and-int/lit8 v2, v0, 0x10

    not-int v2, v2

    and-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 188
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 189
    goto :goto_5

    .line 183
    :pswitch_6
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    and-int/lit8 v2, v0, 0x8

    not-int v2, v2

    and-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 184
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 185
    goto :goto_5

    .line 179
    :pswitch_7
    iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    and-int/lit8 v2, v0, 0x2

    not-int v2, v2

    and-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I

    .line 180
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V

    .line 181
    nop

    .line 196
    :goto_5
    goto :goto_6

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    invoke-virtual {v0, p2}, Lcom/android/server/input/MiuiInputManagerService;->dump(Ljava/io/PrintWriter;)V

    .line 201
    :cond_5
    :goto_6
    return v1

    :sswitch_data_0
    .sparse-switch
        -0x49ba5161 -> :sswitch_3
        0x308e51f1 -> :sswitch_2
        0x4907087a -> :sswitch_1
        0x4e9cc50d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x49ba5161 -> :sswitch_7
        0x308e51f1 -> :sswitch_6
        0x4907087a -> :sswitch_5
        0x4e9cc50d -> :sswitch_4
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public filterKeyboardDeviceIfNeeded([Landroid/view/InputDevice;)[Landroid/view/InputDevice;
    .locals 2
    .param p1, "inputDevices"    # [Landroid/view/InputDevice;

    .line 235
    move-object v0, p1

    .line 236
    .local v0, "newInputDevices":[Landroid/view/InputDevice;
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->removeKeyboardDevicesIfNeeded([Landroid/view/InputDevice;)[Landroid/view/InputDevice;

    move-result-object v0

    .line 239
    :cond_0
    return-object v0
.end method

.method getMiuiInputManagerService()Lcom/android/server/input/MiuiInputManagerService;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    return-object v0
.end method

.method public getPtr()J
    .locals 2

    .line 136
    iget-wide v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPtr:J

    return-wide v0
.end method

.method public handleAppDied(ILcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "pid"    # I
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 614
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->ensureMiuiMagicPointerServiceInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615
    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->handleAppDied(ILcom/android/server/am/ProcessRecord;)V

    .line 618
    return-void
.end method

.method public init(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/InputManagerService;Lcom/android/server/input/NativeInputManagerService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "inputManagerService"    # Lcom/android/server/input/InputManagerService;
    .param p4, "nativeInputManagerService"    # Lcom/android/server/input/NativeInputManagerService;

    .line 84
    const-string v0, "InputManagerServiceStubImpl"

    const-string v1, "init"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iput-object p1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    .line 87
    iput-object p4, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mNativeInputManagerService:Lcom/android/server/input/NativeInputManagerService;

    .line 88
    invoke-interface {p4}, Lcom/android/server/input/NativeInputManagerService;->getPtr()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPtr:J

    .line 89
    iput-object p3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputManagerService:Lcom/android/server/input/InputManagerService;

    .line 93
    new-instance v0, Lcom/android/server/input/MiuiInputManagerService;

    invoke-direct {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    .line 94
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->isPad()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->switchPadMode(Z)V

    .line 95
    return-void
.end method

.method public interceptKeyboardNotification(Landroid/content/Context;[I)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyboardDeviceIds"    # [I

    .line 245
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 246
    return v1

    .line 248
    :cond_0
    array-length v0, p2

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_2

    aget v3, p2, v2

    .line 249
    .local v3, "deviceId":I
    iget-object v4, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputManagerService:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v4, v3}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v4

    .line 250
    .local v4, "device":Landroid/view/InputDevice;
    if-eqz v4, :cond_1

    .line 251
    invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I

    move-result v5

    .line 252
    .local v5, "vendorId":I
    invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I

    move-result v6

    .line 253
    .local v6, "productId":I
    invoke-static {v6, v5}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiKeyboard(II)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 254
    const/4 v0, 0x1

    return v0

    .line 248
    .end local v3    # "deviceId":I
    .end local v4    # "device":Landroid/view/InputDevice;
    .end local v5    # "vendorId":I
    .end local v6    # "productId":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 258
    :cond_2
    return v1
.end method

.method public isPad()Z
    .locals 1

    .line 424
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    return v0
.end method

.method public jumpPermissionCheck(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "permission"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 468
    const-string v0, "android.permission.INJECT_EVENTS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3ea

    if-ne p2, v0, :cond_0

    .line 470
    const-string v0, "InputManagerServiceStubImpl"

    const-string v1, "INJECT_EVENTS Permission Denial, bypass BLUETOOTH_UID!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    const/4 v0, 0x1

    return v0

    .line 473
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public needInterceptSetCustomPointerIcon(Landroid/view/PointerIcon;)Z
    .locals 1
    .param p1, "icon"    # Landroid/view/PointerIcon;

    .line 582
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->ensureMiuiMagicPointerServiceInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    const/4 v0, 0x0

    return v0

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->needInterceptSetCustomPointerIcon(Landroid/view/PointerIcon;)Z

    move-result v0

    return v0
.end method

.method public needInterceptSetPointerIconType(I)Z
    .locals 1
    .param p1, "iconType"    # I

    .line 574
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->ensureMiuiMagicPointerServiceInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    const/4 v0, 0x0

    return v0

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiMagicPointerService:Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->needInterceptSetPointerIconType(I)Z

    move-result v0

    return v0
.end method

.method public notifySwitch(JII)V
    .locals 2
    .param p1, "whenNanos"    # J
    .param p3, "switchValues"    # I
    .param p4, "switchMask"    # I

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifySwitch: values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 230
    invoke-static {p4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    const-string v1, "InputManagerServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    return-void
.end method

.method public notifyTabletSwitchChanged(Z)V
    .locals 1
    .param p1, "tabletOpen"    # Z

    .line 262
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager;->notifyTabletSwitchChanged(Z)V

    .line 265
    :cond_0
    return-void
.end method

.method public onDisplayViewportsSet(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;)V"
        }
    .end annotation

    .line 564
    .local p1, "viewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;"
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->updateDisplayViewport(Ljava/util/List;)V

    .line 565
    return-void
.end method

.method public onUpdatePointerDisplayId(I)V
    .locals 1
    .param p1, "displayId"    # I

    .line 560
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->updatePointerDisplayId(I)V

    .line 561
    return-void
.end method

.method public registerMiuiOptimizationObserver()V
    .locals 5

    .line 327
    new-instance v0, Lcom/android/server/input/InputManagerServiceStubImpl$3;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/input/InputManagerServiceStubImpl$3;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 340
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    .line 341
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 340
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 343
    invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 344
    return-void
.end method

.method public registerStub()V
    .locals 2

    .line 106
    const-string v0, "InputManagerServiceStubImpl"

    const-string v1, "registerStub"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerService;->start()V

    .line 108
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerSynergyModeSettingObserver()V

    .line 109
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateSynergyModeFromSettings()V

    .line 110
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerPointerLocationSettingObserver()V

    .line 111
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updatePointerLocationFromSettings()V

    .line 112
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerTouchesPreventRecordSettingObserver()V

    .line 113
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchesPreventRecorderFromSettings()V

    .line 114
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerDefaultInputMethodSettingObserver()V

    .line 115
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDefaultInputMethodFromSettings()V

    .line 116
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerInputLevelSelect()V

    .line 117
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateFromInputLevelSetting()V

    .line 118
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerMiuiOptimizationObserver()V

    .line 119
    invoke-static {}, Lcom/miui/server/input/PadManager;->getInstance()Lcom/miui/server/input/PadManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerMouseGestureSettingObserver()V

    .line 121
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateMouseGestureSettings()V

    .line 124
    :cond_0
    sget-boolean v0, Lcom/android/server/input/InputManagerServiceStubImpl;->BERSERK_MODE:Z

    if-eqz v0, :cond_1

    .line 125
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerGameMode()V

    .line 128
    :cond_1
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportOffScreenQuickNote()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerStylusQuickNoteModeSetting()V

    .line 130
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchStylusQuickNoteMode()V

    .line 132
    :cond_2
    return-void
.end method

.method public registerStylusQuickNoteModeSetting()V
    .locals 5

    .line 500
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 501
    const-string/jumbo v1, "stylus_quick_note_screen_off"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$9;

    iget-object v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$9;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V

    .line 500
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 508
    return-void
.end method

.method setCustomPointerIcon(Landroid/view/PointerIcon;)V
    .locals 1
    .param p1, "pointerIcon"    # Landroid/view/PointerIcon;

    .line 610
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputManagerService:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/input/InputManagerService;->setCustomPointerIcon(Landroid/view/PointerIcon;)V

    .line 611
    return-void
.end method

.method public setInputMethodStatus(Z)V
    .locals 2
    .param p1, "shown"    # Z

    .line 321
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    .line 322
    .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig;
    iget-boolean v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mCustomizeInputMethod:Z

    invoke-virtual {v0, p1, v1}, Lcom/android/server/input/config/InputCommonConfig;->setInputMethodStatus(ZZ)V

    .line 323
    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 324
    return-void
.end method

.method public setInteractive(Z)V
    .locals 1
    .param p1, "interactive"    # Z

    .line 569
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mMiuiInputManagerService:Lcom/android/server/input/MiuiInputManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->setInteractive(Z)V

    .line 570
    return-void
.end method

.method setPointerIconType(I)V
    .locals 1
    .param p1, "iconType"    # I

    .line 606
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputManagerService:Lcom/android/server/input/InputManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/input/InputManagerService;->setPointerIconType(I)V

    .line 607
    return-void
.end method

.method public setShowTouchLevelFromSettings(I)V
    .locals 0
    .param p1, "showTouchesSwitch"    # I

    .line 206
    iput p1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouches:I

    .line 207
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDebugLevel()V

    .line 208
    return-void
.end method

.method public updateDefaultInputMethodFromSettings()V
    .locals 4

    .line 439
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "inputMethodId":Ljava/lang/String;
    const-string v1, ""

    .line 442
    .local v1, "defaultInputMethod":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 443
    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 445
    :cond_0
    sget-object v2, Lcom/android/server/input/InputManagerServiceStubImpl;->MIUI_INPUTMETHOD:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mCustomizeInputMethod:Z

    .line 446
    return-void
.end method

.method public updateFromUserSwitch()V
    .locals 1

    .line 212
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateSynergyModeFromSettings()V

    .line 213
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updatePointerLocationFromSettings()V

    .line 214
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchesPreventRecorderFromSettings()V

    .line 215
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDefaultInputMethodFromSettings()V

    .line 216
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateFromInputLevelSetting()V

    .line 218
    sget-boolean v0, Lcom/android/server/input/InputManagerServiceStubImpl;->BERSERK_MODE:Z

    if-eqz v0, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateOnewayModeFromSettings()V

    .line 222
    :cond_0
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportOffScreenQuickNote()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchStylusQuickNoteMode()V

    .line 225
    :cond_1
    return-void
.end method

.method public updateTouchStylusQuickNoteMode()V
    .locals 4

    .line 511
    iget-object v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 512
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/input/InputManagerServiceStubImpl;->SUPPORT_QUICK_NOTE_DEFAULT:Z

    .line 511
    const-string/jumbo v2, "stylus_quick_note_screen_off"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    .line 514
    .local v0, "stylusQuickNoteMode":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update stylus quick note when screen off mode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "InputManagerServiceStubImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v1

    .line 516
    nop

    .line 515
    const/4 v2, 0x0

    const/16 v3, 0x18

    invoke-virtual {v1, v2, v3, v0}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 517
    return-void
.end method
