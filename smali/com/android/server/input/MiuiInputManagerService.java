public class com.android.server.input.MiuiInputManagerService extends miui.hardware.input.IMiuiInputManager$Stub {
	 /* .source "MiuiInputManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/MiuiInputManagerService$H;, */
	 /* Lcom/android/server/input/MiuiInputManagerService$LocalService;, */
	 /* Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;, */
	 /* Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;, */
	 /* Lcom/android/server/input/MiuiInputManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final android.content.Context mContext;
private java.util.List mDisplayViewports;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/hardware/display/DisplayViewport;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.input.edgesuppression.EdgeSuppressionManager mEdgeSuppressionManager;
private com.android.server.input.fling.FlingTracker mFlingTracker;
private final android.os.Handler mHandler;
private Boolean mIsScreenOn;
private volatile com.miui.server.input.stylus.laser.LaserPointerController mLaserPointerController;
private com.miui.server.input.util.MiuiCustomizeShortCutUtils mMiuiCustomizeShortCutUtils;
private final com.miui.server.input.gesture.MiuiGestureListener mMiuiGestureListener;
private com.miui.server.input.gesture.MiuiGestureMonitor mMiuiGestureMonitor;
private com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal mMiuiMagicPointerService;
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager mMiuiPadKeyboardManager;
private final java.lang.Object mMiuiShortcutSettingsLock;
private com.miui.server.input.stylus.MiuiStylusShortcutManager mMiuiStylusShortcutManager;
private final android.util.SparseArray mMotionEventListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.ArrayList mMotionEventListenersToNotify;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Object mMotionEventLock;
private final com.android.server.input.NativeMiuiInputManagerService mNative;
private Integer mPointerDisplayId;
private final java.lang.Runnable mPokeUserActivityRunnable;
private android.os.PowerManager mPowerManager;
private final android.util.SparseArray mShortcutListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.ArrayList mShortcutListenersToNotify;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.input.shoulderkey.ShoulderKeyManagerInternal mShoulderKeyManagerInternal;
/* # direct methods */
public static void $r8$lambda$5oypteGBNu0VQxOa99XZwvLqlTY ( com.android.server.input.MiuiInputManagerService p0, android.view.MotionEvent p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->sendMotionEventToNotify(Landroid/view/MotionEvent;)V */
return;
} // .end method
public static void $r8$lambda$c1zlw3fgnapbP_GtpingSg8G7_g ( com.android.server.input.MiuiInputManagerService p0, android.view.MotionEvent p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->lambda$sendMotionEventToNotify$1(Landroid/view/MotionEvent;)V */
return;
} // .end method
public static void $r8$lambda$jBIVkscI9QqFvDdFp5vaFm-AyoE ( com.android.server.input.MiuiInputManagerService p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerService;->lambda$new$0()V */
return;
} // .end method
static java.util.List -$$Nest$fgetmDisplayViewports ( com.android.server.input.MiuiInputManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayViewports;
} // .end method
static java.lang.Object -$$Nest$fgetmMiuiShortcutSettingsLock ( com.android.server.input.MiuiInputManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiShortcutSettingsLock;
} // .end method
static com.android.server.input.NativeMiuiInputManagerService -$$Nest$fgetmNative ( com.android.server.input.MiuiInputManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNative;
} // .end method
static Integer -$$Nest$fgetmPointerDisplayId ( com.android.server.input.MiuiInputManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I */
} // .end method
static android.util.SparseArray -$$Nest$fgetmShortcutListeners ( com.android.server.input.MiuiInputManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mShortcutListeners;
} // .end method
static java.util.ArrayList -$$Nest$fgetmShortcutListenersToNotify ( com.android.server.input.MiuiInputManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mShortcutListenersToNotify;
} // .end method
static void -$$Nest$mremoveListenerFromSystem ( com.android.server.input.MiuiInputManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->removeListenerFromSystem(I)V */
return;
} // .end method
static void -$$Nest$mremoveMotionEventListener ( com.android.server.input.MiuiInputManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->removeMotionEventListener(I)V */
return;
} // .end method
static void -$$Nest$msetScreenState ( com.android.server.input.MiuiInputManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService;->setScreenState(Z)V */
return;
} // .end method
 com.android.server.input.MiuiInputManagerService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 129 */
/* invoke-direct {p0}, Lmiui/hardware/input/IMiuiInputManager$Stub;-><init>()V */
/* .line 78 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mMiuiShortcutSettingsLock = v0;
/* .line 79 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mShortcutListeners = v0;
/* .line 80 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mShortcutListenersToNotify = v0;
/* .line 82 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mMotionEventLock = v0;
/* .line 83 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mMotionEventListeners = v0;
/* .line 86 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mMotionEventListenersToNotify = v0;
/* .line 88 */
/* new-instance v0, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V */
this.mMiuiGestureListener = v0;
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I */
/* .line 90 */
java.util.Collections .emptyList ( );
this.mDisplayViewports = v0;
/* .line 94 */
int v0 = 0; // const/4 v0, 0x0
this.mShoulderKeyManagerInternal = v0;
/* .line 97 */
/* new-instance v1, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V */
this.mPokeUserActivityRunnable = v1;
/* .line 130 */
/* new-instance v1, Lcom/android/server/input/NativeMiuiInputManagerService; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/NativeMiuiInputManagerService;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V */
this.mNative = v1;
/* .line 131 */
this.mContext = p1;
/* .line 132 */
/* new-instance v1, Lcom/android/server/input/MiuiInputManagerService$H; */
com.android.server.input.MiuiInputThread .getThread ( );
(( com.android.server.input.MiuiInputThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/input/MiuiInputManagerService$H;-><init>(Lcom/android/server/input/MiuiInputManagerService;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 133 */
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( p1 );
this.mMiuiGestureMonitor = v1;
/* .line 134 */
/* const-class v1, Landroid/os/PowerManager; */
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/os/PowerManager; */
this.mPowerManager = v1;
/* .line 135 */
/* const-class v1, Lcom/android/server/input/MiuiInputManagerInternal; */
/* new-instance v2, Lcom/android/server/input/MiuiInputManagerService$LocalService; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/input/MiuiInputManagerService$LocalService;-><init>(Lcom/android/server/input/MiuiInputManagerService;Lcom/android/server/input/MiuiInputManagerService$LocalService-IA;)V */
com.android.server.LocalServices .addService ( v1,v2 );
/* .line 136 */
/* new-instance v0, Lcom/android/server/input/fling/FlingTracker; */
/* invoke-direct {v0, p1}, Lcom/android/server/input/fling/FlingTracker;-><init>(Landroid/content/Context;)V */
this.mFlingTracker = v0;
/* .line 137 */
return;
} // .end method
private void beforeNotifyMotion ( android.view.MotionEvent p0 ) {
/* .locals 1 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 633 */
v0 = this.mMiuiStylusShortcutManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 634 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v0 ).processMotionEventForQuickNote ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->processMotionEventForQuickNote(Landroid/view/MotionEvent;)V
/* .line 636 */
} // :cond_0
return;
} // .end method
private Boolean ensureMiuiMagicPointerServiceInit ( ) {
/* .locals 2 */
/* .line 840 */
v0 = this.mMiuiMagicPointerService;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 841 */
/* .line 843 */
} // :cond_0
/* const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
this.mMiuiMagicPointerService = v0;
/* .line 845 */
/* if-nez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private java.lang.String getKeyboardStatus ( miui.hardware.input.MiuiKeyboardStatus p0 ) {
/* .locals 3 */
/* .param p1, "status" # Lmiui/hardware/input/MiuiKeyboardStatus; */
/* .line 328 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = "Keyboard is Normal"; // const-string v1, "Keyboard is Normal"
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 329 */
/* .local v0, "result":Ljava/lang/StringBuilder; */
v1 = (( miui.hardware.input.MiuiKeyboardStatus ) p1 ).shouldIgnoreKeyboard ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->shouldIgnoreKeyboard()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 330 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.StringBuilder ) v0 ).setLength ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 331 */
v1 = (( miui.hardware.input.MiuiKeyboardStatus ) p1 ).isAngleStatusWork ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isAngleStatusWork()Z
/* if-nez v1, :cond_0 */
/* .line 332 */
final String v1 = "AngleStatus Exception "; // const-string v1, "AngleStatus Exception "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 333 */
} // :cond_0
v1 = (( miui.hardware.input.MiuiKeyboardStatus ) p1 ).isAuthStatus ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isAuthStatus()Z
/* if-nez v1, :cond_1 */
/* .line 334 */
final String v1 = "AuthStatus Exception "; // const-string v1, "AuthStatus Exception "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 335 */
} // :cond_1
v1 = (( miui.hardware.input.MiuiKeyboardStatus ) p1 ).isLidStatus ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isLidStatus()Z
/* if-nez v1, :cond_2 */
/* .line 336 */
final String v1 = "LidStatus Exception "; // const-string v1, "LidStatus Exception "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 337 */
} // :cond_2
v1 = (( miui.hardware.input.MiuiKeyboardStatus ) p1 ).isTabletStatus ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isTabletStatus()Z
/* if-nez v1, :cond_3 */
/* .line 338 */
final String v1 = "TabletStatus Exception "; // const-string v1, "TabletStatus Exception "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 339 */
} // :cond_3
v1 = (( miui.hardware.input.MiuiKeyboardStatus ) p1 ).isConnected ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->isConnected()Z
/* if-nez v1, :cond_4 */
/* .line 340 */
final String v1 = "The Keyboard is disConnect "; // const-string v1, "The Keyboard is disConnect "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 343 */
} // :cond_4
} // :goto_0
final String v1 = ", MCU:"; // const-string v1, ", MCU:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.hardware.input.MiuiKeyboardStatus ) p1 ).getMCUVersion ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->getMCUVersion()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", Keyboard:"; // const-string v2, ", Keyboard:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 344 */
(( miui.hardware.input.MiuiKeyboardStatus ) p1 ).getKeyboardVersion ( ); // invoke-virtual {p1}, Lmiui/hardware/input/MiuiKeyboardStatus;->getKeyboardVersion()Ljava/lang/String;
/* .line 343 */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 345 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private static Boolean isSystemApp ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "pid" # I */
/* .line 714 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
v0 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).isSystemApp ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemApp(I)Z
} // .end method
private Integer isXiaomiKeyboard ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "vendorId" # I */
/* .param p2, "productId" # I */
/* .line 656 */
v0 = miui.hardware.input.MiuiKeyboardHelper .getXiaomiKeyboardType ( p2,p1 );
} // .end method
private Integer isXiaomiStylus ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "vendorId" # I */
/* .param p2, "productId" # I */
/* .line 651 */
v0 = android.view.InputDevice .isXiaomiStylus ( p1,p2 );
} // .end method
private Integer isXiaomiTouchpad ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "vendorId" # I */
/* .param p2, "productId" # I */
/* .line 661 */
v0 = miui.hardware.input.MiuiKeyboardHelper .getXiaomiTouchPadType ( p2,p1 );
} // .end method
private void lambda$new$0 ( ) { //synthethic
/* .locals 5 */
/* .line 98 */
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
(( android.os.PowerManager ) v0 ).userActivity ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/PowerManager;->userActivity(JII)V
return;
} // .end method
private void lambda$sendMotionEventToNotify$1 ( android.view.MotionEvent p0 ) { //synthethic
/* .locals 4 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 548 */
v0 = this.mMotionEventListenersToNotify;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 549 */
v0 = this.mMotionEventLock;
/* monitor-enter v0 */
/* .line 550 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mMotionEventListeners;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 551 */
v2 = this.mMotionEventListenersToNotify;
v3 = this.mMotionEventListeners;
(( android.util.SparseArray ) v3 ).valueAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord; */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 550 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 553 */
} // .end local v1 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 554 */
v0 = this.mMotionEventListenersToNotify;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord; */
/* .line 555 */
/* .local v1, "record":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord; */
(( com.android.server.input.MiuiInputManagerService$MotionEventListenerRecord ) v1 ).notifyMotionEvent ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->notifyMotionEvent(Landroid/view/MotionEvent;)V
/* .line 556 */
} // .end local v1 # "record":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
/* .line 557 */
} // :cond_1
(( android.view.MotionEvent ) p1 ).recycle ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V
/* .line 558 */
return;
/* .line 553 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
static void lambda$updateDisplayViewport$3 ( java.util.List p0, com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "viewports" # Ljava/util/List; */
/* .param p1, "s" # Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
/* .line 600 */
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) p1 ).setDisplayViewports ( p0 ); // invoke-virtual {p1, p0}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->setDisplayViewports(Ljava/util/List;)V
return;
} // .end method
static void lambda$updatePointerDisplayId$2 ( Integer p0, com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "displayId" # I */
/* .param p1, "s" # Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
/* .line 591 */
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) p1 ).updatePointerDisplayId ( p0 ); // invoke-virtual {p1, p0}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->updatePointerDisplayId(I)V
return;
} // .end method
private void notifyTouchMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 640 */
v0 = this.mShoulderKeyManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 641 */
/* const-class v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/shoulderkey/ShoulderKeyManagerInternal; */
this.mShoulderKeyManagerInternal = v0;
/* .line 644 */
} // :cond_0
v0 = this.mShoulderKeyManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 645 */
/* .line 647 */
} // :cond_1
return;
} // .end method
private void removeListenerFromSystem ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 402 */
v0 = this.mMiuiShortcutSettingsLock;
/* monitor-enter v0 */
/* .line 403 */
try { // :try_start_0
v1 = this.mShortcutListeners;
(( android.util.SparseArray ) v1 ).delete ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V
/* .line 404 */
/* monitor-exit v0 */
/* .line 405 */
return;
/* .line 404 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void removeMotionEventListener ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 529 */
v0 = this.mMotionEventLock;
/* monitor-enter v0 */
/* .line 530 */
try { // :try_start_0
v1 = this.mMotionEventListeners;
v1 = (( android.util.SparseArray ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->contains(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 531 */
v1 = this.mMotionEventListeners;
(( android.util.SparseArray ) v1 ).delete ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V
/* .line 532 */
v1 = this.mMotionEventListeners;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-nez v1, :cond_0 */
/* .line 533 */
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
/* const-string/jumbo v2, "unregister pointer event listener" */
android.util.Slog .i ( v1,v2 );
/* .line 534 */
v1 = this.mMiuiGestureMonitor;
v2 = this.mMiuiGestureListener;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v1 ).unregisterPointerEventListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 537 */
} // :cond_0
/* monitor-exit v0 */
/* .line 538 */
return;
/* .line 537 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void sendMotionEventToNotify ( android.view.MotionEvent p0 ) {
/* .locals 4 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 541 */
v0 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
/* .line 542 */
/* .local v0, "action":I */
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_0 */
/* .line 544 */
return;
/* .line 546 */
} // :cond_0
(( android.view.MotionEvent ) p1 ).copy ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->copy()Landroid/view/MotionEvent;
/* .line 547 */
/* .local v1, "event":Landroid/view/MotionEvent; */
v2 = this.mHandler;
/* new-instance v3, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0, v1}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/MiuiInputManagerService;Landroid/view/MotionEvent;)V */
(( android.os.Handler ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 559 */
return;
} // .end method
private void sendSettingsChangedMessage ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "messageWhat" # I */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "newFunction" # Ljava/lang/String; */
/* .line 408 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 409 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "action"; // const-string v1, "action"
(( android.os.Bundle ) v0 ).putString ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 410 */
final String v1 = "function"; // const-string v1, "function"
(( android.os.Bundle ) v0 ).putString ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 411 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).obtainMessage ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 412 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).setData ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 413 */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 414 */
return;
} // .end method
private void setMagicPointerPosition ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "pointerX" # F */
/* .param p2, "pointerY" # F */
/* .line 813 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerService;->ensureMiuiMagicPointerServiceInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 814 */
return;
/* .line 816 */
} // :cond_0
v0 = this.mMiuiMagicPointerService;
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v0 ).updateMagicPointerPosition ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->updateMagicPointerPosition(FF)V
/* .line 817 */
return;
} // .end method
private void setMagicPointerVisibility ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "visibility" # Z */
/* .line 825 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerService;->ensureMiuiMagicPointerServiceInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 826 */
return;
/* .line 828 */
} // :cond_0
v0 = this.mMiuiMagicPointerService;
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v0 ).setMagicPointerVisibility ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->setMagicPointerVisibility(Z)V
/* .line 829 */
return;
} // .end method
private void setScreenState ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isScreenOn" # Z */
/* .line 615 */
/* iput-boolean p1, p0, Lcom/android/server/input/MiuiInputManagerService;->mIsScreenOn:Z */
/* .line 616 */
v0 = this.mLaserPointerController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 617 */
v0 = this.mLaserPointerController;
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).setScreenState ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setScreenState(Z)V
/* .line 619 */
} // :cond_0
v0 = this.mMiuiStylusShortcutManager;
/* if-nez v0, :cond_1 */
/* .line 620 */
return;
/* .line 622 */
} // :cond_1
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 625 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
/* xor-int/lit8 v1, p1, 0x1 */
/* .line 626 */
/* .local v1, "needSyncMotion":Z */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setNeedSyncMotion ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputCommonConfig;->setNeedSyncMotion(Z)V
/* .line 627 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 628 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Flush needSyncMotion to native, value = "; // const-string v3, "Flush needSyncMotion to native, value = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiInputManagerService"; // const-string v3, "MiuiInputManagerService"
android.util.Slog .i ( v3,v2 );
/* .line 629 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 849 */
final String v0 = "MI INPUT MANAGER (dumpsys input)\n"; // const-string v0, "MI INPUT MANAGER (dumpsys input)\n"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 850 */
v0 = this.mNative;
(( com.android.server.input.NativeMiuiInputManagerService ) v0 ).dump ( ); // invoke-virtual {v0}, Lcom/android/server/input/NativeMiuiInputManagerService;->dump()Ljava/lang/String;
/* .line 851 */
/* .local v0, "dumpStr":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 852 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 854 */
} // :cond_0
final String v1 = "Mi InputManagerService (Java) State:\n"; // const-string v1, "Mi InputManagerService (Java) State:\n"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 855 */
com.miui.server.input.deviceshare.MiuiDeviceShareManager .getInstance ( );
(( com.miui.server.input.deviceshare.MiuiDeviceShareManager ) v1 ).dump ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->dump(Ljava/io/PrintWriter;)V
/* .line 856 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 857 */
com.android.server.input.TouchWakeUpFeatureManager .getInstance ( );
final String v2 = ""; // const-string v2, ""
(( com.android.server.input.TouchWakeUpFeatureManager ) v1 ).dump ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Lcom/android/server/input/TouchWakeUpFeatureManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
/* .line 858 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 859 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 860 */
return;
} // .end method
public java.lang.String getAppScrollerOptimizationConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 261 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .getInstance ( );
/* .line 262 */
(( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ) v0 ).getAppScrollerOptimizationConfigAndSwitchState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->getAppScrollerOptimizationConfigAndSwitchState(Ljava/lang/String;)[Ljava/lang/String;
/* .line 261 */
} // .end method
public java.util.List getDefaultKeyboardShortcutInfos ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 181 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 182 */
final String v0 = "Not Support normal application get keyboard Default shortcut"; // const-string v0, "Not Support normal application get keyboard Default shortcut"
android.util.Slog .e ( v2,v0 );
/* .line 183 */
/* .line 185 */
} // :cond_0
v0 = this.mMiuiCustomizeShortCutUtils;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 186 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v0 ).getDefaultKeyboardShortcutInfos ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getDefaultKeyboardShortcutInfos()Ljava/util/List;
/* .line 188 */
} // :cond_1
final String v0 = "Can\'t get Keyboard ShortcutKey because Not Support"; // const-string v0, "Can\'t get Keyboard ShortcutKey because Not Support"
android.util.Slog .e ( v2,v0 );
/* .line 190 */
} // .end method
public getEdgeSuppressionSize ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isAbsolute" # Z */
/* .line 212 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 213 */
final String v0 = "Not Support normal application get edge suppression size"; // const-string v0, "Not Support normal application get edge suppression size"
android.util.Slog .e ( v2,v0 );
/* .line 214 */
/* .line 216 */
} // :cond_0
v0 = this.mEdgeSuppressionManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 217 */
if ( p1 != null) { // if-eqz p1, :cond_1
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).getAbsoluteLevel ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getAbsoluteLevel()[I
/* .line 218 */
} // :cond_1
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).getConditionLevel ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getConditionLevel()[I
/* .line 217 */
} // :goto_0
/* .line 220 */
} // :cond_2
final String v0 = "Can\'t get EdgeSuppression Info because not Support"; // const-string v0, "Can\'t get EdgeSuppression Info because not Support"
android.util.Slog .e ( v2,v0 );
/* .line 222 */
} // .end method
public getInputMethodSizeScope ( ) {
/* .locals 3 */
/* .line 227 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 228 */
final String v0 = "Not Support normal application get inputmethod size"; // const-string v0, "Not Support normal application get inputmethod size"
android.util.Slog .e ( v2,v0 );
/* .line 229 */
/* .line 231 */
} // :cond_0
v0 = this.mEdgeSuppressionManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 232 */
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).getInputMethodSizeScope ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getInputMethodSizeScope()[I
/* .line 234 */
} // :cond_1
final String v0 = "Can\'t get inputmethod size because not Support"; // const-string v0, "Can\'t get inputmethod size because not Support"
android.util.Slog .e ( v2,v0 );
/* .line 236 */
} // .end method
public Integer getKeyboardBackLightBrightness ( ) {
/* .locals 3 */
/* .line 291 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
int v1 = -1; // const/4 v1, -0x1
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 292 */
final String v0 = "Not Support normal application interaction gesture shortcut"; // const-string v0, "Not Support normal application interaction gesture shortcut"
android.util.Slog .e ( v2,v0 );
/* .line 293 */
/* .line 296 */
} // :cond_0
v0 = this.mMiuiPadKeyboardManager;
/* if-nez v0, :cond_1 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 297 */
v0 = this.mContext;
com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v0 );
this.mMiuiPadKeyboardManager = v0;
/* .line 299 */
} // :cond_1
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* .line 300 */
/* .line 302 */
} // :cond_2
final String v0 = "get padKeyboard backLight fail because Not Support"; // const-string v0, "get padKeyboard backLight fail because Not Support"
android.util.Slog .e ( v2,v0 );
/* .line 304 */
} // .end method
public java.util.List getKeyboardShortcut ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 167 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 168 */
final String v0 = "Not Support normal application get keyboard shortcut"; // const-string v0, "Not Support normal application get keyboard shortcut"
android.util.Slog .e ( v2,v0 );
/* .line 169 */
/* .line 171 */
} // :cond_0
v0 = this.mMiuiCustomizeShortCutUtils;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 172 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v0 ).getKeyboardShortcutInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getKeyboardShortcutInfo()Ljava/util/List;
/* .line 174 */
} // :cond_1
final String v0 = "Can\'t get Keyboard ShortcutKey because Not Support"; // const-string v0, "Can\'t get Keyboard ShortcutKey because Not Support"
android.util.Slog .e ( v2,v0 );
/* .line 176 */
} // .end method
public Integer getKeyboardType ( ) {
/* .locals 5 */
/* .line 309 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
v1 = android.os.Binder .getCallingPid ( );
v0 = (( com.android.server.am.ActivityManagerServiceImpl ) v0 ).isSystemApp ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemApp(I)Z
int v1 = -1; // const/4 v1, -0x1
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 310 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "the app is not system :" */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 311 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
/* .line 312 */
v4 = android.os.Binder .getCallingPid ( );
/* .line 311 */
v3 = (( com.android.server.am.ActivityManagerServiceImpl ) v3 ).isSystemApp ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/ActivityManagerServiceImpl;->isSystemApp(I)Z
/* xor-int/lit8 v3, v3, 0x1 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 310 */
android.util.Slog .e ( v2,v0 );
/* .line 313 */
final String v0 = "Not Support normal application interaction gesture shortcut"; // const-string v0, "Not Support normal application interaction gesture shortcut"
android.util.Slog .e ( v2,v0 );
/* .line 314 */
/* .line 316 */
} // :cond_0
v0 = this.mMiuiPadKeyboardManager;
/* if-nez v0, :cond_1 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 317 */
v0 = this.mContext;
com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v0 );
this.mMiuiPadKeyboardManager = v0;
/* .line 319 */
} // :cond_1
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* .line 320 */
/* .line 322 */
} // :cond_2
final String v0 = "get padKeyboard type fail because Not Support"; // const-string v0, "get padKeyboard type fail because Not Support"
android.util.Slog .e ( v2,v0 );
/* .line 324 */
} // .end method
public java.lang.String getMiKeyboardStatus ( ) {
/* .locals 3 */
/* .line 195 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 196 */
final String v0 = "Not Support normal application get keyboard status"; // const-string v0, "Not Support normal application get keyboard status"
android.util.Slog .e ( v2,v0 );
/* .line 197 */
/* .line 199 */
} // :cond_0
v0 = this.mMiuiPadKeyboardManager;
/* if-nez v0, :cond_1 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 200 */
v0 = this.mContext;
com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v0 );
this.mMiuiPadKeyboardManager = v0;
/* .line 202 */
} // :cond_1
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 203 */
/* invoke-direct {p0, v0}, Lcom/android/server/input/MiuiInputManagerService;->getKeyboardStatus(Lmiui/hardware/input/MiuiKeyboardStatus;)Ljava/lang/String; */
/* .line 205 */
} // :cond_2
final String v0 = "Get Keyboard Status fail because Not Support"; // const-string v0, "Get Keyboard Status fail because Not Support"
android.util.Slog .e ( v2,v0 );
/* .line 207 */
} // .end method
public Boolean hideCursor ( ) {
/* .locals 2 */
/* .line 251 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 252 */
final String v0 = "Not Support normal application hide cursor"; // const-string v0, "Not Support normal application hide cursor"
android.util.Slog .e ( v1,v0 );
/* .line 253 */
int v0 = 0; // const/4 v0, 0x0
/* .line 255 */
} // :cond_0
final String v0 = "hideCursor"; // const-string v0, "hideCursor"
android.util.Slog .i ( v1,v0 );
/* .line 256 */
v0 = this.mNative;
v0 = (( com.android.server.input.NativeMiuiInputManagerService ) v0 ).hideCursor ( ); // invoke-virtual {v0}, Lcom/android/server/input/NativeMiuiInputManagerService;->hideCursor()Z
} // .end method
public void notifyDeviceShareListenerSocketBroken ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 672 */
com.miui.server.input.deviceshare.MiuiDeviceShareManager .getInstance ( );
(( com.miui.server.input.deviceshare.MiuiDeviceShareManager ) v0 ).onSocketBroken ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->onSocketBroken(I)V
/* .line 673 */
return;
} // .end method
public com.miui.server.input.stylus.laser.PointerControllerInterface obtainLaserPointerController ( ) {
/* .locals 2 */
/* .line 605 */
v0 = this.mLaserPointerController;
/* if-nez v0, :cond_0 */
/* .line 606 */
/* new-instance v0, Lcom/miui/server/input/stylus/laser/LaserPointerController; */
/* invoke-direct {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;-><init>()V */
this.mLaserPointerController = v0;
/* .line 607 */
v0 = this.mLaserPointerController;
/* iget v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I */
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).setDisplayId ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayId(I)V
/* .line 608 */
v0 = this.mLaserPointerController;
v1 = this.mDisplayViewports;
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).setDisplayViewPort ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayViewPort(Ljava/util/List;)V
/* .line 609 */
v0 = this.mLaserPointerController;
/* iget-boolean v1, p0, Lcom/android/server/input/MiuiInputManagerService;->mIsScreenOn:Z */
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).setScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setScreenState(Z)V
/* .line 611 */
} // :cond_0
v0 = this.mLaserPointerController;
} // .end method
public void pokeUserActivity ( ) {
/* .locals 2 */
/* .line 677 */
com.android.server.input.MiuiInputThread .getHandler ( );
v1 = this.mPokeUserActivityRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 678 */
return;
} // .end method
public Boolean putStringForUser ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 350 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 351 */
final String v0 = "Not Support normal application interaction gesture shortcut"; // const-string v0, "Not Support normal application interaction gesture shortcut"
android.util.Slog .e ( v1,v0 );
/* .line 352 */
int v0 = 0; // const/4 v0, 0x0
/* .line 354 */
} // :cond_0
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
/* .line 355 */
v2 = android.os.Binder .getCallingPid ( );
/* .line 354 */
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getPackageNameForPid ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameForPid(I)Ljava/lang/String;
/* .line 356 */
/* .local v0, "targetPackage":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Changed Action:"; // const-string v3, "Changed Action:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ",new Function:"; // const-string v3, ",new Function:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ",because for "; // const-string v3, ",because for "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 358 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 359 */
/* .local v1, "origId":J */
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v4 = -2; // const/4 v4, -0x2
android.provider.MiuiSettings$System .putStringForUser ( v3,p1,p2,v4 );
/* .line 361 */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v3, p1, p2}, Lcom/android/server/input/MiuiInputManagerService;->sendSettingsChangedMessage(ILjava/lang/String;Ljava/lang/String;)V */
/* .line 362 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 363 */
} // .end method
public void registerAppScrollerOptimizationConfigListener ( java.lang.String p0, miui.hardware.input.IAppScrollerOptimizationConfigChangedListener p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "listener" # Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener; */
/* .line 268 */
com.android.server.input.overscroller.ScrollerOptimizationConfigProvider .getInstance ( );
(( com.android.server.input.overscroller.ScrollerOptimizationConfigProvider ) v0 ).registerAppScrollerOptimizationConfigListener ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/overscroller/ScrollerOptimizationConfigProvider;->registerAppScrollerOptimizationConfigListener(Ljava/lang/String;Lmiui/hardware/input/IAppScrollerOptimizationConfigChangedListener;)V
/* .line 270 */
return;
} // .end method
public void registerMiuiMotionEventListener ( miui.hardware.input.IMiuiMotionEventListener p0 ) {
/* .locals 6 */
/* .param p1, "listener" # Lmiui/hardware/input/IMiuiMotionEventListener; */
/* .line 484 */
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 489 */
} // :cond_0
v0 = this.mMotionEventLock;
/* monitor-enter v0 */
/* .line 490 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 491 */
/* .local v1, "callingPid":I */
v2 = this.mMotionEventListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 492 */
final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
final String v3 = "Can\'t register repeat listener to motion event"; // const-string v3, "Can\'t register repeat listener to motion event"
android.util.Slog .e ( v2,v3 );
/* .line 493 */
/* monitor-exit v0 */
return;
/* .line 496 */
} // :cond_1
/* new-instance v2, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord; */
/* invoke-direct {v2, p0, p1, v1}, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;-><init>(Lcom/android/server/input/MiuiInputManagerService;Lmiui/hardware/input/IMiuiMotionEventListener;I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 499 */
/* .local v2, "motionEventListenerRecord":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord; */
try { // :try_start_1
/* .line 500 */
/* .local v3, "binder":Landroid/os/IBinder; */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 503 */
} // .end local v3 # "binder":Landroid/os/IBinder;
/* .line 501 */
/* :catch_0 */
/* move-exception v3 */
/* .line 502 */
/* .local v3, "ex":Landroid/os/RemoteException; */
try { // :try_start_2
final String v4 = "MiuiInputManagerService"; // const-string v4, "MiuiInputManagerService"
final String v5 = "Can\'t linkToDeath because IllegalStateException"; // const-string v5, "Can\'t linkToDeath because IllegalStateException"
android.util.Slog .e ( v4,v5 );
/* .line 504 */
} // .end local v3 # "ex":Landroid/os/RemoteException;
} // :goto_0
v3 = this.mMotionEventListeners;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-nez v3, :cond_2 */
/* .line 505 */
final String v3 = "MiuiInputManagerService"; // const-string v3, "MiuiInputManagerService"
final String v4 = "register pointer event listener"; // const-string v4, "register pointer event listener"
android.util.Slog .i ( v3,v4 );
/* .line 506 */
v3 = this.mMiuiGestureMonitor;
v4 = this.mMiuiGestureListener;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v3 ).registerPointerEventListener ( v4 ); // invoke-virtual {v3, v4}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 508 */
} // :cond_2
v3 = this.mMotionEventListeners;
(( android.util.SparseArray ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 509 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "motionEventListenerRecord":Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;
/* monitor-exit v0 */
/* .line 510 */
return;
/* .line 509 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 485 */
} // :cond_3
} // :goto_1
final String v0 = "MiuiInputManagerService"; // const-string v0, "MiuiInputManagerService"
final String v1 = "Not Support normal application interaction motion event"; // const-string v1, "Not Support normal application interaction motion event"
android.util.Slog .e ( v0,v1 );
/* .line 486 */
return;
} // .end method
public void registerShortcutChangedListener ( miui.hardware.input.IShortcutSettingsChangedListener p0 ) {
/* .locals 5 */
/* .param p1, "listener" # Lmiui/hardware/input/IShortcutSettingsChangedListener; */
/* .line 368 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 373 */
} // :cond_0
v0 = this.mMiuiShortcutSettingsLock;
/* monitor-enter v0 */
/* .line 374 */
try { // :try_start_0
v1 = android.os.Binder .getCallingPid ( );
/* .line 375 */
/* .local v1, "callingPid":I */
v2 = this.mShortcutListeners;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* if-nez v2, :cond_1 */
/* .line 380 */
/* new-instance v2, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord; */
/* invoke-direct {v2, p0, p1, v1}, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;-><init>(Lcom/android/server/input/MiuiInputManagerService;Lmiui/hardware/input/IShortcutSettingsChangedListener;I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 383 */
/* .local v2, "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord; */
try { // :try_start_1
/* .line 384 */
/* .local v3, "binder":Landroid/os/IBinder; */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 387 */
} // .end local v3 # "binder":Landroid/os/IBinder;
/* nop */
/* .line 388 */
try { // :try_start_2
v3 = this.mShortcutListeners;
(( android.util.SparseArray ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 389 */
} // .end local v1 # "callingPid":I
} // .end local v2 # "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
/* monitor-exit v0 */
/* .line 390 */
return;
/* .line 385 */
/* .restart local v1 # "callingPid":I */
/* .restart local v2 # "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord; */
/* :catch_0 */
/* move-exception v3 */
/* .line 386 */
/* .local v3, "ex":Landroid/os/RemoteException; */
/* new-instance v4, Ljava/lang/IllegalStateException; */
/* invoke-direct {v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V */
} // .end local p0 # "this":Lcom/android/server/input/MiuiInputManagerService;
} // .end local p1 # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener;
/* throw v4 */
/* .line 376 */
} // .end local v2 # "shortcutListenerRecord":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
} // .end local v3 # "ex":Landroid/os/RemoteException;
/* .restart local p0 # "this":Lcom/android/server/input/MiuiInputManagerService; */
/* .restart local p1 # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener; */
} // :cond_1
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
final String v3 = "Can\'t register repeat listener to MiuiShortcutSettings"; // const-string v3, "Can\'t register repeat listener to MiuiShortcutSettings"
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/input/MiuiInputManagerService;
} // .end local p1 # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener;
/* throw v2 */
/* .line 389 */
} // .end local v1 # "callingPid":I
/* .restart local p0 # "this":Lcom/android/server/input/MiuiInputManagerService; */
/* .restart local p1 # "listener":Lmiui/hardware/input/IShortcutSettingsChangedListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 369 */
} // :cond_2
} // :goto_0
final String v0 = "MiuiInputManagerService"; // const-string v0, "MiuiInputManagerService"
final String v1 = "Not Support normal application interaction gesture shortcut"; // const-string v1, "Not Support normal application interaction gesture shortcut"
android.util.Slog .e ( v0,v1 );
/* .line 370 */
return;
} // .end method
public void reportFlingEvent ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "downTimes" # I */
/* .param p3, "flingTimes" # I */
/* .line 523 */
v0 = this.mMotionEventLock;
/* monitor-enter v0 */
/* .line 524 */
try { // :try_start_0
v1 = this.mFlingTracker;
(( com.android.server.input.fling.FlingTracker ) v1 ).trackEvent ( p1, p2, p3 ); // invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/input/fling/FlingTracker;->trackEvent(Ljava/lang/String;II)V
/* .line 525 */
/* monitor-exit v0 */
/* .line 526 */
return;
/* .line 525 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void requestRedirect ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "motionEventId" # I */
/* .line 418 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "app requestRedirect, motionEventId:"; // const-string v1, "app requestRedirect, motionEventId:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
android.util.Slog .i ( v1,v0 );
/* .line 419 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 420 */
final String v0 = "Not Support non-system application requestRedirect"; // const-string v0, "Not Support non-system application requestRedirect"
android.util.Slog .e ( v1,v0 );
/* .line 421 */
return;
/* .line 423 */
} // :cond_0
v0 = this.mNative;
v1 = android.os.Binder .getCallingPid ( );
(( com.android.server.input.NativeMiuiInputManagerService ) v0 ).requestRedirect ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/input/NativeMiuiInputManagerService;->requestRedirect(II)V
/* .line 424 */
return;
} // .end method
public Boolean setCursorPosition ( Float p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .line 241 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 242 */
final String v0 = "Not Support normal application set cursor position"; // const-string v0, "Not Support normal application set cursor position"
android.util.Slog .e ( v1,v0 );
/* .line 243 */
int v0 = 0; // const/4 v0, 0x0
/* .line 245 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setCursorPosition(" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v2 = ")"; // const-string v2, ")"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 246 */
v0 = this.mNative;
v0 = (( com.android.server.input.NativeMiuiInputManagerService ) v0 ).setCursorPosition ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/input/NativeMiuiInputManagerService;->setCursorPosition(FF)Z
} // .end method
public void setDeviceShareListener ( android.os.ParcelFileDescriptor p0, Integer p1, miui.hardware.input.IDeviceShareStateChangedListener p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Landroid/os/ParcelFileDescriptor; */
/* .param p2, "flags" # I */
/* .param p3, "listener" # Lmiui/hardware/input/IDeviceShareStateChangedListener; */
/* .line 464 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 465 */
final String v0 = "MiuiInputManagerService"; // const-string v0, "MiuiInputManagerService"
final String v1 = "Not Support normal application set device share listener :("; // const-string v1, "Not Support normal application set device share listener :("
android.util.Slog .e ( v0,v1 );
/* .line 466 */
return;
/* .line 468 */
} // :cond_0
com.miui.server.input.deviceshare.MiuiDeviceShareManager .getInstance ( );
v1 = android.os.Binder .getCallingPid ( );
(( com.miui.server.input.deviceshare.MiuiDeviceShareManager ) v0 ).setDeviceShareListener ( v1, p1, p2, p3 ); // invoke-virtual {v0, v1, p1, p2, p3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->setDeviceShareListener(ILandroid/os/ParcelFileDescriptor;ILmiui/hardware/input/IDeviceShareStateChangedListener;)V
/* .line 470 */
return;
} // .end method
public void setHasEditTextOnScreen ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "hasEditText" # Z */
/* .line 456 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v0 ).setHasEditTextOnScreen ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->setHasEditTextOnScreen(Z)V
/* .line 457 */
return;
} // .end method
public void setInteractive ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "interactive" # Z */
/* .line 103 */
v0 = this.mNative;
(( com.android.server.input.NativeMiuiInputManagerService ) v0 ).setInteractive ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/NativeMiuiInputManagerService;->setInteractive(Z)V
/* .line 104 */
return;
} // .end method
public void setKeyboardBackLightBrightness ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "brightness" # I */
/* .line 274 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 275 */
final String v0 = "Not Support normal application interaction gesture shortcut"; // const-string v0, "Not Support normal application interaction gesture shortcut"
android.util.Slog .e ( v1,v0 );
/* .line 276 */
return;
/* .line 278 */
} // :cond_0
v0 = this.mMiuiPadKeyboardManager;
/* if-nez v0, :cond_1 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 279 */
v0 = this.mContext;
com.android.server.input.padkeyboard.MiuiPadKeyboardManager .getKeyboardManager ( v0 );
this.mMiuiPadKeyboardManager = v0;
/* .line 281 */
} // :cond_1
v0 = this.mMiuiPadKeyboardManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 282 */
/* .line 284 */
} // :cond_2
/* const-string/jumbo v0, "set padKeyboard backLight fail because Not Support" */
android.util.Slog .e ( v1,v0 );
/* .line 286 */
} // :goto_0
return;
} // .end method
public void setTouchpadButtonState ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "deviceId" # I */
/* .param p2, "isDown" # Z */
/* .line 474 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 475 */
final String v0 = "MiuiInputManagerService"; // const-string v0, "MiuiInputManagerService"
final String v1 = "Not Support normal application set touchpad button state :("; // const-string v1, "Not Support normal application set touchpad button state :("
android.util.Slog .e ( v0,v1 );
/* .line 476 */
return;
/* .line 478 */
} // :cond_0
com.miui.server.input.deviceshare.MiuiDeviceShareManager .getInstance ( );
v1 = android.os.Binder .getCallingPid ( );
(( com.miui.server.input.deviceshare.MiuiDeviceShareManager ) v0 ).setTouchpadButtonState ( v1, p1, p2 ); // invoke-virtual {v0, v1, p1, p2}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->setTouchpadButtonState(IIZ)V
/* .line 480 */
return;
} // .end method
void start ( ) {
/* .locals 1 */
/* .line 140 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 141 */
v0 = this.mContext;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .getInstance ( v0 );
this.mMiuiCustomizeShortCutUtils = v0;
/* .line 143 */
} // :cond_0
miui.util.ITouchFeature .getInstance ( );
v0 = (( miui.util.ITouchFeature ) v0 ).hasSupportEdgeMode ( ); // invoke-virtual {v0}, Lmiui/util/ITouchFeature;->hasSupportEdgeMode()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 144 */
v0 = this.mContext;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .getInstance ( v0 );
this.mEdgeSuppressionManager = v0;
/* .line 146 */
} // :cond_1
v0 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportOffScreenQuickNote ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 147 */
com.miui.server.input.stylus.MiuiStylusShortcutManager .getInstance ( );
this.mMiuiStylusShortcutManager = v0;
/* .line 149 */
} // :cond_2
v0 = this.mFlingTracker;
(( com.android.server.input.fling.FlingTracker ) v0 ).registerPointerEventListener ( ); // invoke-virtual {v0}, Lcom/android/server/input/fling/FlingTracker;->registerPointerEventListener()V
/* .line 150 */
return;
} // .end method
public void trackTouchpadEvent ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "x" # I */
/* .param p3, "y" # I */
/* .line 682 */
v0 = this.mContext;
com.android.server.input.touchpad.TouchpadOneTrackHelper .getInstance ( v0 );
(( com.android.server.input.touchpad.TouchpadOneTrackHelper ) v0 ).trackTouchpadEvent ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->trackTouchpadEvent(III)V
/* .line 683 */
return;
} // .end method
public void unregisterMiuiMotionEventListener ( ) {
/* .locals 2 */
/* .line 514 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 515 */
final String v0 = "MiuiInputManagerService"; // const-string v0, "MiuiInputManagerService"
final String v1 = "Not Support normal application interaction motion event"; // const-string v1, "Not Support normal application interaction motion event"
android.util.Slog .e ( v0,v1 );
/* .line 516 */
return;
/* .line 518 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
/* invoke-direct {p0, v0}, Lcom/android/server/input/MiuiInputManagerService;->removeMotionEventListener(I)V */
/* .line 519 */
return;
} // .end method
public void unregisterShortcutChangedListener ( ) {
/* .locals 2 */
/* .line 394 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
/* if-nez v0, :cond_0 */
/* .line 395 */
final String v0 = "MiuiInputManagerService"; // const-string v0, "MiuiInputManagerService"
final String v1 = "Not Support normal application interaction gesture shortcut"; // const-string v1, "Not Support normal application interaction gesture shortcut"
android.util.Slog .e ( v0,v1 );
/* .line 396 */
return;
/* .line 398 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
/* invoke-direct {p0, v0}, Lcom/android/server/input/MiuiInputManagerService;->removeListenerFromSystem(I)V */
/* .line 399 */
return;
} // .end method
public void updateDisplayViewport ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/hardware/display/DisplayViewport;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 595 */
/* .local p1, "viewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;" */
this.mDisplayViewports = p1;
/* .line 596 */
v0 = this.mLaserPointerController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 597 */
v0 = this.mLaserPointerController;
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).setDisplayViewPort ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayViewPort(Ljava/util/List;)V
/* .line 599 */
} // :cond_0
/* const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
java.util.Optional .ofNullable ( v0 );
/* new-instance v1, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p1}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda3;-><init>(Ljava/util/List;)V */
/* .line 600 */
(( java.util.Optional ) v0 ).ifPresent ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 601 */
return;
} // .end method
public void updateKeyboardShortcut ( android.view.KeyboardShortcutInfo p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "info" # Landroid/view/KeyboardShortcutInfo; */
/* .param p2, "type" # I */
/* .line 154 */
v0 = android.os.Binder .getCallingPid ( );
v0 = com.android.server.input.MiuiInputManagerService .isSystemApp ( v0 );
final String v1 = "MiuiInputManagerService"; // const-string v1, "MiuiInputManagerService"
/* if-nez v0, :cond_0 */
/* .line 155 */
final String v0 = "Not Support normal application update keyboard shortcut"; // const-string v0, "Not Support normal application update keyboard shortcut"
android.util.Slog .e ( v1,v0 );
/* .line 156 */
return;
/* .line 158 */
} // :cond_0
v0 = this.mMiuiCustomizeShortCutUtils;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 159 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v0 ).updateKeyboardShortcut ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->updateKeyboardShortcut(Landroid/view/KeyboardShortcutInfo;I)V
/* .line 161 */
} // :cond_1
final String v0 = "Can\'t update Keyboard ShortcutKey because Not Support"; // const-string v0, "Can\'t update Keyboard ShortcutKey because Not Support"
android.util.Slog .e ( v1,v0 );
/* .line 163 */
} // :goto_0
return;
} // .end method
public void updatePointerDisplayId ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "displayId" # I */
/* .line 586 */
/* iput p1, p0, Lcom/android/server/input/MiuiInputManagerService;->mPointerDisplayId:I */
/* .line 587 */
v0 = this.mLaserPointerController;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 588 */
v0 = this.mLaserPointerController;
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).setDisplayId ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setDisplayId(I)V
/* .line 590 */
} // :cond_0
/* const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
java.util.Optional .ofNullable ( v0 );
/* new-instance v1, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p1}, Lcom/android/server/input/MiuiInputManagerService$$ExternalSyntheticLambda4;-><init>(I)V */
/* .line 591 */
(( java.util.Optional ) v0 ).ifPresent ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 592 */
return;
} // .end method
