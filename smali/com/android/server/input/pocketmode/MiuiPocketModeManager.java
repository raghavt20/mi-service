public class com.android.server.input.pocketmode.MiuiPocketModeManager {
	 /* .source "MiuiPocketModeManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer AQUIRE_ALLOW_TIME_DIFFERENCE;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.ContentResolver mContentResolver;
private android.content.Context mContext;
private com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper mMiuiPocketModeSensorWrapper;
private com.android.server.input.pocketmode.MiuiPocketModeManager$MiuiPocketModeSettingsObserver mMiuiPocketModeSettingsObserver;
private Boolean mPocketModeEnableSettings;
/* # direct methods */
static android.content.ContentResolver -$$Nest$fgetmContentResolver ( com.android.server.input.pocketmode.MiuiPocketModeManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContentResolver;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.pocketmode.MiuiPocketModeManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmPocketModeEnableSettings ( com.android.server.input.pocketmode.MiuiPocketModeManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z */
} // .end method
static void -$$Nest$fputmPocketModeEnableSettings ( com.android.server.input.pocketmode.MiuiPocketModeManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z */
	 return;
} // .end method
public com.android.server.input.pocketmode.MiuiPocketModeManager ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 31 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 29 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z */
	 /* .line 32 */
	 this.mContext = p1;
	 /* .line 33 */
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 this.mContentResolver = v0;
	 /* .line 34 */
	 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)V */
	 this.mMiuiPocketModeSettingsObserver = v0;
	 /* .line 35 */
	 (( com.android.server.input.pocketmode.MiuiPocketModeManager$MiuiPocketModeSettingsObserver ) v0 ).initObserver ( ); // invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->initObserver()V
	 /* .line 36 */
	 v0 = 	 com.android.server.input.pocketmode.MiuiPocketModeManager .isSupportInertialAndLightSensor ( p1 );
	 final String v1 = "MiuiPocketModeManager"; // const-string v1, "MiuiPocketModeManager"
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 37 */
		 /* const-string/jumbo v0, "set inertial and light sensor" */
		 android.util.Slog .d ( v1,v0 );
		 /* .line 38 */
		 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor; */
		 /* invoke-direct {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;-><init>(Landroid/content/Context;)V */
		 this.mMiuiPocketModeSensorWrapper = v0;
		 /* .line 40 */
	 } // :cond_0
	 /* const-string/jumbo v0, "set proximity sensor" */
	 android.util.Slog .d ( v1,v0 );
	 /* .line 41 */
	 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeProximitySensor; */
	 /* invoke-direct {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeProximitySensor;-><init>(Landroid/content/Context;)V */
	 this.mMiuiPocketModeSensorWrapper = v0;
	 /* .line 43 */
} // :goto_0
return;
} // .end method
private static android.hardware.SensorManager getSensorManager ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 61 */
/* const-string/jumbo v0, "sensor" */
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
} // .end method
private static Boolean hasSupportSensor ( android.content.Context p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "type" # I */
/* .line 51 */
com.android.server.input.pocketmode.MiuiPocketModeManager .getSensorManager ( p0 );
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( p1 ); // invoke-virtual {v0, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
/* .line 52 */
/* .local v0, "sensor":Landroid/hardware/Sensor; */
/* const v1, 0x1fa2697 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* if-ne v1, p1, :cond_1 */
/* .line 53 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 54 */
	 v1 = 	 (( android.hardware.Sensor ) v0 ).getVersion ( ); // invoke-virtual {v0}, Landroid/hardware/Sensor;->getVersion()I
	 int v4 = 2; // const/4 v4, 0x2
	 /* if-ne v1, v4, :cond_0 */
} // :cond_0
/* move v2, v3 */
} // :goto_0
/* .line 57 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
/* move v2, v3 */
} // :goto_1
} // .end method
public static Boolean isSupportInertialAndLightSensor ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 46 */
/* const v0, 0x1fa2697 */
v0 = com.android.server.input.pocketmode.MiuiPocketModeManager .hasSupportSensor ( p0,v0 );
} // .end method
/* # virtual methods */
public Boolean getPocketModeEnableSettings ( ) {
/* .locals 1 */
/* .line 84 */
/* iget-boolean v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z */
} // .end method
public Integer getStateStableDelay ( ) {
/* .locals 1 */
/* .line 77 */
v0 = this.mMiuiPocketModeSensorWrapper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 78 */
v0 = (( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) v0 ).getStateStableDelay ( ); // invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getStateStableDelay()I
/* .line 80 */
} // :cond_0
/* const/16 v0, 0x12c */
} // .end method
public void registerListener ( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener p0 ) {
/* .locals 1 */
/* .param p1, "sensorListener" # Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener; */
/* .line 65 */
v0 = this.mMiuiPocketModeSensorWrapper;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 66 */
(( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) v0 ).registerListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V
/* .line 68 */
} // :cond_0
return;
} // .end method
public void unregisterListener ( ) {
/* .locals 1 */
/* .line 71 */
v0 = this.mMiuiPocketModeSensorWrapper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
(( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) v0 ).unregisterListener ( ); // invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->unregisterListener()V
/* .line 74 */
} // :cond_0
return;
} // .end method
