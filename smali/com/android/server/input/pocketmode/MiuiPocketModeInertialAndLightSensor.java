public class com.android.server.input.pocketmode.MiuiPocketModeInertialAndLightSensor extends com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper {
	 /* .source "MiuiPocketModeInertialAndLightSensor.java" */
	 /* # static fields */
	 public static final Integer SENSOR_TYPE_INERTIAL_AND_LIGHT;
	 /* # instance fields */
	 private android.hardware.Sensor mInertialLightSensor;
	 /* # direct methods */
	 public com.android.server.input.pocketmode.MiuiPocketModeInertialAndLightSensor ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 12 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;-><init>(Landroid/content/Context;)V */
		 /* .line 13 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected android.hardware.Sensor getSensor ( ) {
		 /* .locals 2 */
		 /* .line 17 */
		 v0 = this.mInertialLightSensor;
		 /* if-nez v0, :cond_0 */
		 /* .line 18 */
		 v0 = this.mSensorManager;
		 /* const v1, 0x1fa2697 */
		 (( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
		 this.mInertialLightSensor = v0;
		 /* .line 20 */
	 } // :cond_0
	 v0 = this.mInertialLightSensor;
} // .end method
protected Integer getStateStableDelay ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
