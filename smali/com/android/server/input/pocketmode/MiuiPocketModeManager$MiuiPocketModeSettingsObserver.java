class com.android.server.input.pocketmode.MiuiPocketModeManager$MiuiPocketModeSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiPocketModeManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/pocketmode/MiuiPocketModeManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiPocketModeSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.input.pocketmode.MiuiPocketModeManager this$0; //synthetic
/* # direct methods */
public com.android.server.input.pocketmode.MiuiPocketModeManager$MiuiPocketModeSettingsObserver ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/input/pocketmode/MiuiPocketModeManager; */
/* .line 89 */
this.this$0 = p1;
/* .line 90 */
com.android.server.input.MiuiInputThread .getHandler ( );
/* invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 91 */
return;
} // .end method
/* # virtual methods */
public void initObserver ( ) {
/* .locals 4 */
/* .line 94 */
v0 = this.this$0;
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fgetmContentResolver ( v0 );
/* .line 95 */
final String v1 = "enable_screen_on_proximity_sensor"; // const-string v1, "enable_screen_on_proximity_sensor"
android.provider.Settings$Global .getUriFor ( v1 );
/* .line 94 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 98 */
(( com.android.server.input.pocketmode.MiuiPocketModeManager$MiuiPocketModeSettingsObserver ) p0 ).onChange ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->onChange(Z)V
/* .line 99 */
return;
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .line 103 */
v0 = this.this$0;
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fgetmContext ( v0 );
/* .line 104 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 103 */
final String v1 = "enable_screen_on_proximity_sensor"; // const-string v1, "enable_screen_on_proximity_sensor"
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 106 */
/* .local v0, "pocketModeEnableGlobalSettings":I */
/* if-ne v0, v2, :cond_0 */
/* .line 107 */
v2 = this.this$0;
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fgetmContext ( v2 );
/* .line 108 */
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v4 = this.this$0;
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fgetmContext ( v4 );
/* .line 110 */
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x11050073 */
v4 = (( android.content.res.Resources ) v4 ).getBoolean ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z
/* .line 107 */
v3 = android.provider.MiuiSettings$System .getBoolean ( v3,v1,v4 );
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fputmPocketModeEnableSettings ( v2,v3 );
/* .line 112 */
v2 = this.this$0;
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fgetmContext ( v2 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v3 = this.this$0;
v3 = com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fgetmPocketModeEnableSettings ( v3 );
android.provider.MiuiSettings$Global .putBoolean ( v2,v1,v3 );
/* .line 116 */
} // :cond_0
v1 = this.this$0;
if ( v0 != null) { // if-eqz v0, :cond_1
int v2 = 1; // const/4 v2, 0x1
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
com.android.server.input.pocketmode.MiuiPocketModeManager .-$$Nest$fputmPocketModeEnableSettings ( v1,v2 );
/* .line 118 */
} // :goto_1
return;
} // .end method
