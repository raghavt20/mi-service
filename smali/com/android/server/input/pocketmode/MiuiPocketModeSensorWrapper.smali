.class public Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;
.super Ljava/lang/Object;
.source "MiuiPocketModeSensorWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_SENSOR_STATE:I = -0x1

.field public static final EVENT_FAR:I = 0x1

.field public static final EVENT_TOO_CLOSE:I = 0x0

.field private static final PROXIMITY_THRESHOLD:F = 4.0f

.field public static final STATE_STABLE_DELAY:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "MiuiPocketModeSensorWrapper"


# instance fields
.field private mDefaultSensor:Landroid/hardware/Sensor;

.field private final mDefaultSensorListener:Landroid/hardware/SensorEventListener;

.field protected mHandler:Landroid/os/Handler;

.field private mProximitySensorChangeListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

.field private mSensorEventListener:Landroid/hardware/SensorEventListener;

.field protected mSensorManager:Landroid/hardware/SensorManager;

.field protected mSensorState:I


# direct methods
.method static bridge synthetic -$$Nest$mnotifyListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->notifyListener(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mDefaultSensorListener:Landroid/hardware/SensorEventListener;

    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->init(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensorEventListener"    # Landroid/hardware/SensorEventListener;

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mDefaultSensorListener:Landroid/hardware/SensorEventListener;

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->init(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V

    .line 58
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensorEventListener"    # Landroid/hardware/SensorEventListener;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I

    .line 66
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorManager:Landroid/hardware/SensorManager;

    .line 67
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$2;

    invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$2;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mHandler:Landroid/os/Handler;

    .line 83
    if-nez p2, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mDefaultSensorListener:Landroid/hardware/SensorEventListener;

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    goto :goto_0

    .line 86
    :cond_0
    iput-object p2, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 88
    :goto_0
    return-void
.end method

.method private notifyListener(Z)V
    .locals 1
    .param p1, "tooClose"    # Z

    .line 91
    monitor-enter p0

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mProximitySensorChangeListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    if-eqz v0, :cond_0

    .line 93
    invoke-interface {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;->onSensorChanged(Z)V

    .line 95
    :cond_0
    monitor-exit p0

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected getSensor()Landroid/hardware/Sensor;
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mDefaultSensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mDefaultSensor:Landroid/hardware/Sensor;

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mDefaultSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method protected getStateStableDelay()I
    .locals 1

    .line 125
    const/16 v0, 0x12c

    return v0
.end method

.method public registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    .line 99
    monitor-enter p0

    .line 100
    if-eqz p1, :cond_0

    .line 101
    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I

    .line 102
    iput-object p1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mProximitySensorChangeListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    .line 103
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getSensor()Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 106
    :cond_0
    monitor-exit p0

    .line 107
    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterListener()V
    .locals 3

    .line 110
    monitor-enter p0

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getSensor()Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mProximitySensorChangeListener:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    .line 113
    monitor-exit p0

    .line 114
    return-void

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
