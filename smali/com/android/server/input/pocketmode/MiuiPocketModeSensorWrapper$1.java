class com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$1 implements android.hardware.SensorEventListener {
	 /* .source "MiuiPocketModeSensorWrapper.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper this$0; //synthetic
/* # direct methods */
 com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper; */
/* .line 28 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAccuracyChanged ( android.hardware.Sensor p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "sensor" # Landroid/hardware/Sensor; */
/* .param p2, "i" # I */
/* .line 53 */
return;
} // .end method
public void onSensorChanged ( android.hardware.SensorEvent p0 ) {
/* .locals 6 */
/* .param p1, "sensorEvent" # Landroid/hardware/SensorEvent; */
/* .line 31 */
v0 = this.values;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* .line 32 */
/* .local v0, "distance":F */
/* float-to-double v2, v0 */
/* const-wide/16 v4, 0x0 */
/* cmpl-double v2, v2, v4 */
int v3 = 1; // const/4 v3, 0x1
/* if-ltz v2, :cond_0 */
/* const/high16 v2, 0x40800000 # 4.0f */
/* cmpg-float v2, v0, v2 */
/* if-gez v2, :cond_0 */
v2 = this.this$0;
/* .line 33 */
(( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) v2 ).getSensor ( ); // invoke-virtual {v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getSensor()Landroid/hardware/Sensor;
v2 = (( android.hardware.Sensor ) v2 ).getMaximumRange ( ); // invoke-virtual {v2}, Landroid/hardware/Sensor;->getMaximumRange()F
/* cmpg-float v2, v0, v2 */
/* if-gez v2, :cond_0 */
/* move v2, v3 */
} // :cond_0
/* move v2, v1 */
/* .line 34 */
/* .local v2, "isTooClose":Z */
} // :goto_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "proximity distance: "; // const-string v5, "proximity distance: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiPocketModeSensorWrapper"; // const-string v5, "MiuiPocketModeSensorWrapper"
android.util.Slog .d ( v5,v4 );
/* .line 35 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 36 */
v4 = this.this$0;
/* iget v4, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I */
/* if-eq v4, v3, :cond_2 */
/* .line 37 */
v4 = this.this$0;
/* iput v3, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I */
/* .line 38 */
v4 = this.this$0;
v4 = this.mHandler;
(( android.os.Handler ) v4 ).removeMessages ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 39 */
v3 = this.this$0;
v3 = this.mHandler;
v4 = this.this$0;
v4 = (( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) v4 ).getStateStableDelay ( ); // invoke-virtual {v4}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getStateStableDelay()I
/* int-to-long v4, v4 */
(( android.os.Handler ) v3 ).sendEmptyMessageDelayed ( v1, v4, v5 ); // invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 42 */
} // :cond_1
v4 = this.this$0;
/* iget v4, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 43 */
v4 = this.this$0;
/* iput v1, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I */
/* .line 44 */
v4 = this.this$0;
v4 = this.mHandler;
(( android.os.Handler ) v4 ).removeMessages ( v1 ); // invoke-virtual {v4, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 45 */
v1 = this.this$0;
v1 = this.mHandler;
v4 = this.this$0;
v4 = (( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) v4 ).getStateStableDelay ( ); // invoke-virtual {v4}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getStateStableDelay()I
/* int-to-long v4, v4 */
(( android.os.Handler ) v1 ).sendEmptyMessageDelayed ( v3, v4, v5 ); // invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 48 */
} // :cond_2
} // :goto_1
return;
} // .end method
