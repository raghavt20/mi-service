.class public Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;
.super Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;
.source "MiuiPocketModeInertialAndLightSensor.java"


# static fields
.field public static final SENSOR_TYPE_INERTIAL_AND_LIGHT:I = 0x1fa2697


# instance fields
.field private mInertialLightSensor:Landroid/hardware/Sensor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 12
    invoke-direct {p0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;-><init>(Landroid/content/Context;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected getSensor()Landroid/hardware/Sensor;
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;->mInertialLightSensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 18
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1fa2697

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;->mInertialLightSensor:Landroid/hardware/Sensor;

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;->mInertialLightSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method protected getStateStableDelay()I
    .locals 1

    .line 25
    const/4 v0, 0x0

    return v0
.end method
