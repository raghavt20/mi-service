.class public Lcom/android/server/input/pocketmode/MiuiPocketModeManager;
.super Ljava/lang/Object;
.source "MiuiPocketModeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;
    }
.end annotation


# static fields
.field public static final AQUIRE_ALLOW_TIME_DIFFERENCE:I = 0x64

.field private static final TAG:Ljava/lang/String; = "MiuiPocketModeManager"


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mMiuiPocketModeSensorWrapper:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

.field private mMiuiPocketModeSettingsObserver:Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;

.field private mPocketModeEnableSettings:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmContentResolver(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/ContentResolver;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mContentResolver:Landroid/content/ContentResolver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPocketModeEnableSettings(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmPocketModeEnableSettings(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z

    .line 32
    iput-object p1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mContext:Landroid/content/Context;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 34
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)V

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mMiuiPocketModeSettingsObserver:Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;

    .line 35
    invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->initObserver()V

    .line 36
    invoke-static {p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->isSupportInertialAndLightSensor(Landroid/content/Context;)Z

    move-result v0

    const-string v1, "MiuiPocketModeManager"

    if-eqz v0, :cond_0

    .line 37
    const-string/jumbo v0, "set inertial and light sensor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;

    invoke-direct {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeInertialAndLightSensor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mMiuiPocketModeSensorWrapper:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    goto :goto_0

    .line 40
    :cond_0
    const-string/jumbo v0, "set proximity sensor"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeProximitySensor;

    invoke-direct {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeProximitySensor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mMiuiPocketModeSensorWrapper:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    .line 43
    :goto_0
    return-void
.end method

.method private static getSensorManager(Landroid/content/Context;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 61
    const-string/jumbo v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    return-object v0
.end method

.method private static hasSupportSensor(Landroid/content/Context;I)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # I

    .line 51
    invoke-static {p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->getSensorManager(Landroid/content/Context;)Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 52
    .local v0, "sensor":Landroid/hardware/Sensor;
    const v1, 0x1fa2697

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, p1, :cond_1

    .line 53
    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVersion()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    return v2

    .line 57
    :cond_1
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v2, v3

    :goto_1
    return v2
.end method

.method public static isSupportInertialAndLightSensor(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 46
    const v0, 0x1fa2697

    invoke-static {p0, v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->hasSupportSensor(Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getPocketModeEnableSettings()Z
    .locals 1

    .line 84
    iget-boolean v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mPocketModeEnableSettings:Z

    return v0
.end method

.method public getStateStableDelay()I
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mMiuiPocketModeSensorWrapper:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getStateStableDelay()I

    move-result v0

    return v0

    .line 80
    :cond_0
    const/16 v0, 0x12c

    return v0
.end method

.method public registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V
    .locals 1
    .param p1, "sensorListener"    # Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;

    .line 65
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mMiuiPocketModeSensorWrapper:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 66
    invoke-virtual {v0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->registerListener(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener;)V

    .line 68
    :cond_0
    return-void
.end method

.method public unregisterListener()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->mMiuiPocketModeSensorWrapper:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->unregisterListener()V

    .line 74
    :cond_0
    return-void
.end method
