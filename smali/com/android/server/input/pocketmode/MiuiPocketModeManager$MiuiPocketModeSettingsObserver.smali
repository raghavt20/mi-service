.class Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiPocketModeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/pocketmode/MiuiPocketModeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiPocketModeSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;


# direct methods
.method public constructor <init>(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    .line 89
    iput-object p1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    .line 90
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 91
    return-void
.end method


# virtual methods
.method public initObserver()V
    .locals 4

    .line 94
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-static {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fgetmContentResolver(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/ContentResolver;

    move-result-object v0

    .line 95
    const-string v1, "enable_screen_on_proximity_sensor"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 94
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 98
    invoke-virtual {p0, v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->onChange(Z)V

    .line 99
    return-void
.end method

.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .line 103
    iget-object v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-static {v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fgetmContext(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/Context;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 103
    const-string v1, "enable_screen_on_proximity_sensor"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 106
    .local v0, "pocketModeEnableGlobalSettings":I
    if-ne v0, v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-static {v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fgetmContext(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/Context;

    move-result-object v3

    .line 108
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-static {v4}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fgetmContext(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/Context;

    move-result-object v4

    .line 110
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x11050073

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 107
    invoke-static {v3, v1, v4}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fputmPocketModeEnableSettings(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;Z)V

    .line 112
    iget-object v2, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-static {v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fgetmContext(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    invoke-static {v3}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fgetmPocketModeEnableSettings(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;)Z

    move-result v3

    invoke-static {v2, v1, v3}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto :goto_1

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeManager$MiuiPocketModeSettingsObserver;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeManager;

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v1, v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeManager;->-$$Nest$fputmPocketModeEnableSettings(Lcom/android/server/input/pocketmode/MiuiPocketModeManager;Z)V

    .line 118
    :goto_1
    return-void
.end method
