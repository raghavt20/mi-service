.class Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;
.super Ljava/lang/Object;
.source "MiuiPocketModeSensorWrapper.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;


# direct methods
.method constructor <init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    .line 28
    iput-object p1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "i"    # I

    .line 53
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "sensorEvent"    # Landroid/hardware/SensorEvent;

    .line 31
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 32
    .local v0, "distance":F
    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    const/4 v3, 0x1

    if-ltz v2, :cond_0

    const/high16 v2, 0x40800000    # 4.0f

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    .line 33
    invoke-virtual {v2}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getSensor()Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v1

    .line 34
    .local v2, "isTooClose":Z
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "proximity distance: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiPocketModeSensorWrapper"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    if-eqz v2, :cond_1

    .line 36
    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iget v4, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I

    if-eq v4, v3, :cond_2

    .line 37
    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iput v3, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I

    .line 38
    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iget-object v4, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 39
    iget-object v3, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iget-object v3, v3, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    invoke-virtual {v4}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getStateStableDelay()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 42
    :cond_1
    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iget v4, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I

    if-eqz v4, :cond_2

    .line 43
    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iput v1, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I

    .line 44
    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iget-object v4, v4, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 45
    iget-object v1, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    iget-object v1, v1, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;->this$0:Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;

    invoke-virtual {v4}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getStateStableDelay()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 48
    :cond_2
    :goto_1
    return-void
.end method
