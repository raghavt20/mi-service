public class com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper {
	 /* .source "MiuiPocketModeSensorWrapper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer DEFAULT_SENSOR_STATE;
public static final Integer EVENT_FAR;
public static final Integer EVENT_TOO_CLOSE;
private static final Float PROXIMITY_THRESHOLD;
public static final Integer STATE_STABLE_DELAY;
private static final java.lang.String TAG;
/* # instance fields */
private android.hardware.Sensor mDefaultSensor;
private final android.hardware.SensorEventListener mDefaultSensorListener;
protected android.os.Handler mHandler;
private com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener mProximitySensorChangeListener;
private android.hardware.SensorEventListener mSensorEventListener;
protected android.hardware.SensorManager mSensorManager;
protected Integer mSensorState;
/* # direct methods */
static void -$$Nest$mnotifyListener ( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->notifyListener(Z)V */
	 return;
} // .end method
public com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 60 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 28 */
	 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V */
	 this.mDefaultSensorListener = v0;
	 /* .line 61 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* invoke-direct {p0, p1, v0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->init(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V */
	 /* .line 62 */
	 return;
} // .end method
public com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "sensorEventListener" # Landroid/hardware/SensorEventListener; */
	 /* .line 56 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 28 */
	 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$1;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V */
	 this.mDefaultSensorListener = v0;
	 /* .line 57 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->init(Landroid/content/Context;Landroid/hardware/SensorEventListener;)V */
	 /* .line 58 */
	 return;
} // .end method
private void init ( android.content.Context p0, android.hardware.SensorEventListener p1 ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "sensorEventListener" # Landroid/hardware/SensorEventListener; */
	 /* .line 65 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I */
	 /* .line 66 */
	 /* const-string/jumbo v0, "sensor" */
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/SensorManager; */
	 this.mSensorManager = v0;
	 /* .line 67 */
	 /* new-instance v0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$2;-><init>(Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;)V */
	 this.mHandler = v0;
	 /* .line 83 */
	 /* if-nez p2, :cond_0 */
	 /* .line 84 */
	 v0 = this.mDefaultSensorListener;
	 this.mSensorEventListener = v0;
	 /* .line 86 */
} // :cond_0
this.mSensorEventListener = p2;
/* .line 88 */
} // :goto_0
return;
} // .end method
private void notifyListener ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "tooClose" # Z */
/* .line 91 */
/* monitor-enter p0 */
/* .line 92 */
try { // :try_start_0
v0 = this.mProximitySensorChangeListener;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 93 */
	 /* .line 95 */
} // :cond_0
/* monitor-exit p0 */
/* .line 96 */
return;
/* .line 95 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
/* # virtual methods */
protected android.hardware.Sensor getSensor ( ) {
/* .locals 2 */
/* .line 117 */
v0 = this.mDefaultSensor;
/* if-nez v0, :cond_0 */
/* .line 118 */
v0 = this.mSensorManager;
/* const/16 v1, 0x8 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
this.mDefaultSensor = v0;
/* .line 121 */
} // :cond_0
v0 = this.mDefaultSensor;
} // .end method
protected Integer getStateStableDelay ( ) {
/* .locals 1 */
/* .line 125 */
/* const/16 v0, 0x12c */
} // .end method
public void registerListener ( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper$ProximitySensorChangeListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper$ProximitySensorChangeListener; */
/* .line 99 */
/* monitor-enter p0 */
/* .line 100 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 101 */
int v0 = -1; // const/4 v0, -0x1
try { // :try_start_0
	 /* iput v0, p0, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->mSensorState:I */
	 /* .line 102 */
	 this.mProximitySensorChangeListener = p1;
	 /* .line 103 */
	 v0 = this.mSensorManager;
	 v1 = this.mSensorEventListener;
	 (( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) p0 ).getSensor ( ); // invoke-virtual {p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getSensor()Landroid/hardware/Sensor;
	 int v3 = 0; // const/4 v3, 0x0
	 (( android.hardware.SensorManager ) v0 ).registerListener ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
	 /* .line 106 */
} // :cond_0
/* monitor-exit p0 */
/* .line 107 */
return;
/* .line 106 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void unregisterListener ( ) {
/* .locals 3 */
/* .line 110 */
/* monitor-enter p0 */
/* .line 111 */
try { // :try_start_0
	 v0 = this.mSensorManager;
	 v1 = this.mSensorEventListener;
	 (( com.android.server.input.pocketmode.MiuiPocketModeSensorWrapper ) p0 ).getSensor ( ); // invoke-virtual {p0}, Lcom/android/server/input/pocketmode/MiuiPocketModeSensorWrapper;->getSensor()Landroid/hardware/Sensor;
	 (( android.hardware.SensorManager ) v0 ).unregisterListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V
	 /* .line 112 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mProximitySensorChangeListener = v0;
	 /* .line 113 */
	 /* monitor-exit p0 */
	 /* .line 114 */
	 return;
	 /* .line 113 */
	 /* :catchall_0 */
	 /* move-exception v0 */
	 /* monitor-exit p0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v0 */
} // .end method
