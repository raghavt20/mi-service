public abstract class com.android.server.input.MiuiInputManagerInternal {
	 /* .source "MiuiInputManagerInternal.java" */
	 /* # direct methods */
	 public com.android.server.input.MiuiInputManagerInternal ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract Boolean doubleTap ( Integer p0, Integer p1, Integer p2 ) {
	 } // .end method
	 public abstract java.util.List getCurrentDisplayViewPorts ( ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "()", */
		 /* "Ljava/util/List<", */
		 /* "Landroid/hardware/display/DisplayViewport;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
} // .end method
public abstract Integer getCurrentPointerDisplayId ( ) {
} // .end method
public abstract void hideMouseCursor ( ) {
} // .end method
public abstract void injectMotionEvent ( android.view.MotionEvent p0, Integer p1 ) {
} // .end method
public abstract void notifyPhotoHandleConnectionStatus ( Boolean p0, Integer p1 ) {
} // .end method
public abstract com.miui.server.input.stylus.laser.PointerControllerInterface obtainLaserPointerController ( ) {
} // .end method
public abstract void setCustomPointerIcon ( android.view.PointerIcon p0 ) {
} // .end method
public abstract void setDeviceShareListener ( Integer p0, java.io.FileDescriptor p1, Integer p2 ) {
} // .end method
public abstract void setDimState ( Boolean p0 ) {
} // .end method
public abstract void setInputConfig ( Integer p0, Long p1 ) {
} // .end method
public abstract void setPointerIconType ( Integer p0 ) {
} // .end method
public abstract void setScreenState ( Boolean p0 ) {
} // .end method
public abstract void setTouchpadButtonState ( Integer p0, Boolean p1 ) {
} // .end method
public abstract Boolean swipe ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
} // .end method
public abstract Boolean swipe ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) {
} // .end method
public abstract Boolean tap ( Integer p0, Integer p1 ) {
} // .end method
