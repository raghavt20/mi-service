public class com.android.server.input.InputManagerServiceStubImpl implements com.android.server.input.InputManagerServiceStub {
	 /* .source "InputManagerServiceStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String ANDROID_SYSTEM_UI;
	 private static final Boolean BERSERK_MODE;
	 private static final java.lang.String INPUT_LOG_LEVEL;
	 private static final Integer KEY_EVENT_FLAG_NAVIGATION_SYSTEM_UI;
	 private static final java.lang.String KEY_GAME_BOOSTER;
	 private static final java.lang.String KEY_STYLUS_QUICK_NOTE_MODE;
	 private static final java.util.List MIUI_INPUTMETHOD;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String SHOW_TOUCHES_PREVENTRECORDER;
private static final Boolean SUPPORT_QUICK_NOTE_DEFAULT;
private static final java.lang.String TAG_INPUT;
/* # instance fields */
private final java.lang.String TAG;
private android.content.Context mContext;
private Boolean mCustomizeInputMethod;
private Integer mFromSetInputLevel;
private android.os.Handler mHandler;
private Integer mInputDebugLevel;
private com.android.server.input.InputManagerService mInputManagerService;
private com.miui.server.input.util.MiuiCustomizeShortCutUtils mMiuiCustomizeShortCutUtils;
private com.android.server.input.MiuiInputManagerService mMiuiInputManagerService;
private com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal mMiuiMagicPointerService;
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager mMiuiPadKeyboardManager;
private com.android.server.input.NativeInputManagerService mNativeInputManagerService;
private Integer mPointerLocationShow;
private Long mPtr;
private Integer mSetDebugLevel;
private Integer mShowTouches;
private Integer mShowTouchesPreventRecord;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static void -$$Nest$msetCtsMode ( com.android.server.input.InputManagerServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/InputManagerServiceStubImpl;->setCtsMode(Z)V */
return;
} // .end method
static void -$$Nest$mupdateFromInputLevelSetting ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateFromInputLevelSetting()V */
return;
} // .end method
static void -$$Nest$mupdateMouseGestureSettings ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateMouseGestureSettings()V */
return;
} // .end method
static void -$$Nest$mupdateOnewayModeFromSettings ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateOnewayModeFromSettings()V */
return;
} // .end method
static void -$$Nest$mupdatePointerLocationFromSettings ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updatePointerLocationFromSettings()V */
return;
} // .end method
static void -$$Nest$mupdateSynergyModeFromSettings ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateSynergyModeFromSettings()V */
return;
} // .end method
static void -$$Nest$mupdateTouchesPreventRecorderFromSettings ( com.android.server.input.InputManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchesPreventRecorderFromSettings()V */
return;
} // .end method
static com.android.server.input.InputManagerServiceStubImpl ( ) {
/* .locals 2 */
/* .line 49 */
/* nop */
/* .line 50 */
final String v0 = "persist.berserk.mode.support"; // const-string v0, "persist.berserk.mode.support"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.input.InputManagerServiceStubImpl.BERSERK_MODE = (v0!= 0);
/* .line 53 */
/* nop */
/* .line 54 */
final String v0 = "persist.sys.quick.note.enable.default"; // const-string v0, "persist.sys.quick.note.enable.default"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.input.InputManagerServiceStubImpl.SUPPORT_QUICK_NOTE_DEFAULT = (v0!= 0);
/* .line 57 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 72 */
final String v1 = "com.baidu.input_mi"; // const-string v1, "com.baidu.input_mi"
/* .line 73 */
final String v1 = "com.sohu.inputmethod.sogou.xiaomi"; // const-string v1, "com.sohu.inputmethod.sogou.xiaomi"
/* .line 74 */
final String v1 = "com.iflytek.inputmethod.miui"; // const-string v1, "com.iflytek.inputmethod.miui"
/* .line 75 */
return;
} // .end method
public com.android.server.input.InputManagerServiceStubImpl ( ) {
/* .locals 1 */
/* .line 42 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 43 */
final String v0 = "InputManagerServiceStubImpl"; // const-string v0, "InputManagerServiceStubImpl"
this.TAG = v0;
/* .line 62 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 66 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
/* .line 67 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I */
/* .line 68 */
/* iput-boolean v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mCustomizeInputMethod:Z */
return;
} // .end method
private Boolean ensureMiuiMagicPointerServiceInit ( ) {
/* .locals 2 */
/* .line 597 */
v0 = this.mMiuiMagicPointerService;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 598 */
	 /* .line 600 */
} // :cond_0
/* const-class v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal; */
this.mMiuiMagicPointerService = v0;
/* .line 602 */
/* if-nez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private static java.lang.String getPackageNameForPid ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "pid" # I */
/* .line 540 */
com.android.server.am.ActivityManagerServiceImpl .getInstance ( );
(( com.android.server.am.ActivityManagerServiceImpl ) v0 ).getPackageNameForPid ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/am/ActivityManagerServiceImpl;->getPackageNameForPid(I)Ljava/lang/String;
} // .end method
private com.android.server.input.padkeyboard.MiuiPadKeyboardManager getPadManagerInstance ( ) {
/* .locals 1 */
/* .line 544 */
v0 = this.mMiuiPadKeyboardManager;
/* if-nez v0, :cond_0 */
/* .line 545 */
/* const-class v0, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
this.mMiuiPadKeyboardManager = v0;
/* .line 547 */
} // :cond_0
v0 = this.mMiuiPadKeyboardManager;
} // .end method
private Integer getPointerLocationSettings ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "defaultValue" # I */
/* .line 384 */
/* move v0, p1 */
/* .line 386 */
/* .local v0, "result":I */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "pointer_location"; // const-string v2, "pointer_location"
int v3 = -2; // const/4 v3, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 390 */
/* .line 388 */
/* :catch_0 */
/* move-exception v1 */
/* .line 389 */
/* .local v1, "snfe":Landroid/provider/Settings$SettingNotFoundException; */
(( android.provider.Settings$SettingNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V
/* .line 391 */
} // .end local v1 # "snfe":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_0
} // .end method
private Integer getShowTouchesPreventRecordSetting ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "defaultValue" # I */
/* .line 412 */
/* move v0, p1 */
/* .line 414 */
/* .local v0, "result":I */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "show_touches_preventrecord" */
int v3 = -2; // const/4 v3, -0x2
v1 = android.provider.Settings$System .getIntForUser ( v1,v2,v3 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 418 */
/* .line 416 */
/* :catch_0 */
/* move-exception v1 */
/* .line 417 */
/* .local v1, "snfe":Landroid/provider/Settings$SettingNotFoundException; */
(( android.provider.Settings$SettingNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V
/* .line 419 */
} // .end local v1 # "snfe":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_0
} // .end method
private void registerDefaultInputMethodSettingObserver ( ) {
/* .locals 5 */
/* .line 428 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 429 */
final String v1 = "default_input_method"; // const-string v1, "default_input_method"
android.provider.Settings$Secure .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$6; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$6;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 428 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 436 */
return;
} // .end method
private void registerGameMode ( ) {
/* .locals 5 */
/* .line 478 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 479 */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$8; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$8;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 478 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 486 */
return;
} // .end method
private void registerInputLevelSelect ( ) {
/* .locals 5 */
/* .line 450 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 451 */
final String v1 = "input_log_level"; // const-string v1, "input_log_level"
android.provider.Settings$System .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$7; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$7;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 450 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 458 */
return;
} // .end method
private void registerMouseGestureSettingObserver ( ) {
/* .locals 5 */
/* .line 298 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 299 */
final String v1 = "mouse_gesture_naturalscroll"; // const-string v1, "mouse_gesture_naturalscroll"
android.provider.Settings$Secure .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$2; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$2;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 298 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 306 */
return;
} // .end method
private void registerPointerLocationSettingObserver ( ) {
/* .locals 5 */
/* .line 366 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 367 */
final String v1 = "pointer_location"; // const-string v1, "pointer_location"
android.provider.Settings$System .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$4; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$4;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 366 */
int v3 = 1; // const/4 v3, 0x1
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 374 */
return;
} // .end method
private void registerSynergyModeSettingObserver ( ) {
/* .locals 5 */
/* .line 268 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 269 */
/* const-string/jumbo v1, "synergy_mode" */
android.provider.Settings$Secure .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$1; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$1;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 268 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 276 */
return;
} // .end method
private void registerTouchesPreventRecordSettingObserver ( ) {
/* .locals 5 */
/* .line 396 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 397 */
/* const-string/jumbo v1, "show_touches_preventrecord" */
android.provider.Settings$System .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$5; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$5;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 396 */
int v3 = 1; // const/4 v3, 0x1
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 404 */
return;
} // .end method
private void setCtsMode ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "ctsMode" # Z */
/* .line 347 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "CtsMode changed, mode = "; // const-string v1, "CtsMode changed, mode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InputManagerServiceStubImpl"; // const-string v1, "InputManagerServiceStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 348 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 349 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setCtsMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setCtsMode(Z)V
/* .line 350 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 351 */
return;
} // .end method
private void setInputLogLevel ( ) {
/* .locals 4 */
/* .line 552 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouches:I */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPointerLocationShow:I */
/* or-int/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouchesPreventRecord:I */
/* or-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
/* .line 553 */
com.android.server.input.config.InputDebugConfig .getInstance ( );
/* .line 554 */
/* .local v0, "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig; */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I */
/* or-int/2addr v1, v2 */
/* iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
/* or-int/2addr v1, v2 */
(( com.android.server.input.config.InputDebugConfig ) v0 ).setInputDebugFromDump ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputDebugConfig;->setInputDebugFromDump(I)V
/* .line 555 */
(( com.android.server.input.config.InputDebugConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputDebugConfig;->flushToNative()V
/* .line 556 */
com.android.server.policy.MiuiInputLog .getInstance ( );
/* iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* iget v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I */
/* or-int/2addr v2, v3 */
/* iget v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
/* or-int/2addr v2, v3 */
(( com.android.server.policy.MiuiInputLog ) v1 ).setLogLevel ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/policy/MiuiInputLog;->setLogLevel(I)V
/* .line 557 */
return;
} // .end method
private void setSynergyMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "synergyMode" # I */
/* .line 285 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SynergyMode changed, mode = "; // const-string v1, "SynergyMode changed, mode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InputManagerServiceStubImpl"; // const-string v1, "InputManagerServiceStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 286 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 287 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setSynergyMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setSynergyMode(I)V
/* .line 288 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 289 */
return;
} // .end method
private void switchMouseNaturalScrollStatus ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "mouseNaturalScrollStatus" # Z */
/* .line 309 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 310 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setMouseNaturalScrollStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setMouseNaturalScrollStatus(Z)V
/* .line 311 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 312 */
return;
} // .end method
private void switchPadMode ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "padMode" # Z */
/* .line 315 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 316 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setPadMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setPadMode(Z)V
/* .line 317 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 318 */
return;
} // .end method
private void updateDebugLevel ( ) {
/* .locals 3 */
/* .line 354 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouches:I */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPointerLocationShow:I */
/* or-int/2addr v0, v1 */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouchesPreventRecord:I */
/* or-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
/* .line 355 */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I */
int v2 = 2; // const/4 v2, 0x2
/* if-ge v1, v2, :cond_0 */
/* .line 356 */
/* or-int/2addr v0, v1 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
/* .line 358 */
} // :cond_0
com.android.server.input.config.InputDebugConfig .getInstance ( );
/* .line 359 */
/* .local v0, "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig; */
/* iget v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
(( com.android.server.input.config.InputDebugConfig ) v0 ).setInputDispatcherMajor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/input/config/InputDebugConfig;->setInputDispatcherMajor(II)V
/* .line 360 */
(( com.android.server.input.config.InputDebugConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputDebugConfig;->flushToNative()V
/* .line 361 */
com.android.server.policy.MiuiInputLog .getInstance ( );
/* iget v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mSetDebugLevel:I */
(( com.android.server.policy.MiuiInputLog ) v1 ).setLogLevel ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/policy/MiuiInputLog;->setLogLevel(I)V
/* .line 362 */
return;
} // .end method
private void updateFromInputLevelSetting ( ) {
/* .locals 4 */
/* .line 461 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = -2; // const/4 v2, -0x2
final String v3 = "input_log_level"; // const-string v3, "input_log_level"
v0 = android.provider.Settings$System .getIntForUser ( v0,v3,v1,v2 );
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mFromSetInputLevel:I */
/* .line 463 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 464 */
return;
} // .end method
private void updateMouseGestureSettings ( ) {
/* .locals 3 */
/* .line 292 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mouse_gesture_naturalscroll"; // const-string v1, "mouse_gesture_naturalscroll"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 294 */
/* .local v0, "mouseNaturalScroll":I */
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* invoke-direct {p0, v2}, Lcom/android/server/input/InputManagerServiceStubImpl;->switchMouseNaturalScrollStatus(Z)V */
/* .line 295 */
return;
} // .end method
private void updateOnewayModeFromSettings ( ) {
/* .locals 5 */
/* .line 489 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "gb_boosting"; // const-string v2, "gb_boosting"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v3 */
/* .line 491 */
/* .local v0, "isGameMode":Z */
} // :goto_0
/* nop */
/* .line 492 */
final String v2 = "ro.miui.cts"; // const-string v2, "ro.miui.cts"
android.os.SystemProperties .get ( v2 );
final String v4 = "1"; // const-string v4, "1"
v2 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 491 */
/* xor-int/2addr v2, v1 */
final String v4 = "persist.sys.miui_optimization"; // const-string v4, "persist.sys.miui_optimization"
v2 = android.os.SystemProperties .getBoolean ( v4,v2 );
/* xor-int/2addr v2, v1 */
/* .line 493 */
/* .local v2, "isCtsMode":Z */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 494 */
/* .local v4, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez v2, :cond_1 */
/* move v3, v1 */
} // :cond_1
(( com.android.server.input.config.InputCommonConfig ) v4 ).setOnewayMode ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/config/InputCommonConfig;->setOnewayMode(Z)V
/* .line 495 */
(( com.android.server.input.config.InputCommonConfig ) v4 ).flushToNative ( ); // invoke-virtual {v4}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 496 */
return;
} // .end method
private void updatePointerLocationFromSettings ( ) {
/* .locals 1 */
/* .line 378 */
int v0 = 0; // const/4 v0, 0x0
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPointerLocationSettings(I)I */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPointerLocationShow:I */
/* .line 379 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDebugLevel()V */
/* .line 380 */
return;
} // .end method
private void updateSynergyModeFromSettings ( ) {
/* .locals 3 */
/* .line 279 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "synergy_mode" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* .line 281 */
/* .local v0, "synergyMode":I */
/* invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setSynergyMode(I)V */
/* .line 282 */
return;
} // .end method
private void updateTouchesPreventRecorderFromSettings ( ) {
/* .locals 1 */
/* .line 407 */
int v0 = 0; // const/4 v0, 0x0
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getShowTouchesPreventRecordSetting(I)I */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouchesPreventRecord:I */
/* .line 408 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDebugLevel()V */
/* .line 409 */
return;
} // .end method
/* # virtual methods */
public void beforeInjectEvent ( android.view.InputEvent p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/view/InputEvent; */
/* .param p2, "pid" # I */
/* .line 520 */
/* instance-of v0, p1, Landroid/view/KeyEvent; */
final String v1 = " action "; // const-string v1, " action "
final String v2 = "Input event injection from pid "; // const-string v2, "Input event injection from pid "
final String v3 = "MIUIInput"; // const-string v3, "MIUIInput"
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 521 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/view/KeyEvent; */
/* .line 522 */
/* .local v0, "keyEvent":Landroid/view/KeyEvent; */
final String v4 = "com.android.systemui"; // const-string v4, "com.android.systemui"
com.android.server.input.InputManagerServiceStubImpl .getPackageNameForPid ( p2 );
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 523 */
v4 = (( android.view.KeyEvent ) v0 ).getFlags ( ); // invoke-virtual {v0}, Landroid/view/KeyEvent;->getFlags()I
/* const/high16 v5, -0x80000000 */
/* or-int/2addr v4, v5 */
(( android.view.KeyEvent ) v0 ).setFlags ( v4 ); // invoke-virtual {v0, v4}, Landroid/view/KeyEvent;->setFlags(I)V
/* .line 525 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 526 */
v2 = (( android.view.KeyEvent ) v0 ).getAction ( ); // invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I
android.view.KeyEvent .actionToString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " keycode "; // const-string v2, " keycode "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( android.view.KeyEvent ) v0 ).getKeyCode ( ); // invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 525 */
android.util.Slog .w ( v3,v1 );
} // .end local v0 # "keyEvent":Landroid/view/KeyEvent;
/* .line 527 */
} // :cond_1
/* instance-of v0, p1, Landroid/view/MotionEvent; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 528 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/view/MotionEvent; */
/* .line 529 */
/* .local v0, "motionEvent":Landroid/view/MotionEvent; */
v4 = (( android.view.MotionEvent ) v0 ).getActionMasked ( ); // invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I
/* .line 530 */
/* .local v4, "maskedAction":I */
if ( v4 != null) { // if-eqz v4, :cond_2
int v5 = 1; // const/4 v5, 0x1
/* if-eq v4, v5, :cond_2 */
int v5 = 3; // const/4 v5, 0x3
/* if-ne v4, v5, :cond_4 */
/* .line 532 */
} // :cond_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 533 */
v2 = (( android.view.MotionEvent ) v0 ).getAction ( ); // invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I
android.view.MotionEvent .actionToString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 532 */
android.util.Slog .w ( v3,v1 );
/* .line 527 */
} // .end local v0 # "motionEvent":Landroid/view/MotionEvent;
} // .end local v4 # "maskedAction":I
} // :cond_3
} // :goto_0
/* nop */
/* .line 537 */
} // :cond_4
} // :goto_1
return;
} // .end method
public Boolean dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 11 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 141 */
final String v0 = "InputManagerServiceStubImpl"; // const-string v0, "InputManagerServiceStubImpl"
final String v1 = "dump InputManagerServiceStubImpl"; // const-string v1, "dump InputManagerServiceStubImpl"
android.util.Slog .d ( v0,v1 );
/* .line 142 */
/* array-length v0, p3 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_0 */
final String v0 = "debuglog"; // const-string v0, "debuglog"
/* aget-object v4, p3, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 144 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "open Input debuglog, "; // const-string v3, "open Input debuglog, "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v3, p3, v1 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v3, p3, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 145 */
/* aget-object v0, p3, v2 */
v0 = java.lang.Integer .parseInt ( v0 );
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 146 */
com.android.server.input.config.InputDebugConfig .getInstance ( );
/* .line 147 */
/* .local v0, "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig; */
/* iget v3, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
(( com.android.server.input.config.InputDebugConfig ) v0 ).setInputDebugFromDump ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/input/config/InputDebugConfig;->setInputDebugFromDump(I)V
/* .line 148 */
(( com.android.server.input.config.InputDebugConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputDebugConfig;->flushToNative()V
/* .line 149 */
com.android.server.policy.MiuiInputLog .getInstance ( );
/* iget v4, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
(( com.android.server.policy.MiuiInputLog ) v3 ).setLogLevel ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/policy/MiuiInputLog;->setLogLevel(I)V
/* :try_end_0 */
/* .catch Ljava/lang/ClassCastException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 150 */
/* .line 151 */
} // .end local v0 # "inputDebugConfig":Lcom/android/server/input/config/InputDebugConfig;
/* :catch_0 */
/* move-exception v0 */
/* .line 152 */
/* .local v0, "e":Ljava/lang/ClassCastException; */
final String v2 = "open Reader and Dispatcher debuglog, ClassCastException!!!"; // const-string v2, "open Reader and Dispatcher debuglog, ClassCastException!!!"
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 153 */
} // .end local v0 # "e":Ljava/lang/ClassCastException;
/* goto/16 :goto_6 */
/* .line 154 */
} // :cond_0
/* array-length v0, p3 */
int v4 = 3; // const/4 v4, 0x3
/* if-ne v0, v4, :cond_4 */
final String v0 = "logging"; // const-string v0, "logging"
/* aget-object v5, p3, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 155 */
final String v0 = "enable-text"; // const-string v0, "enable-text"
/* aget-object v5, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v5 = "InputReader"; // const-string v5, "InputReader"
final String v6 = "MIUIInput"; // const-string v6, "MIUIInput"
final String v7 = "InputDispatcher"; // const-string v7, "InputDispatcher"
final String v8 = "InputTransport"; // const-string v8, "InputTransport"
int v9 = -1; // const/4 v9, -0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* aget-object v0, p3, v3 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 156 */
/* aget-object v0, p3, v3 */
v10 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v10, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v2, v3 */
/* :sswitch_1 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v2, v1 */
/* :sswitch_2 */
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* :sswitch_3 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v2, v4 */
} // :goto_0
/* move v2, v9 */
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 170 */
/* :pswitch_0 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* or-int/lit8 v0, v0, 0x20 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 171 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 172 */
/* .line 166 */
/* :pswitch_1 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* or-int/lit8 v0, v0, 0x10 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 167 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 168 */
/* .line 162 */
/* :pswitch_2 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* or-int/lit8 v0, v0, 0x8 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 163 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 164 */
/* .line 158 */
/* :pswitch_3 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* or-int/2addr v0, v3 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 159 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 160 */
/* nop */
/* .line 175 */
} // :goto_2
/* goto/16 :goto_6 */
/* .line 176 */
} // :cond_2
final String v0 = "disable-text"; // const-string v0, "disable-text"
/* aget-object v10, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* aget-object v0, p3, v3 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 177 */
/* aget-object v0, p3, v3 */
v10 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v10, :sswitch_data_1 */
} // :cond_3
/* :sswitch_4 */
v0 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* move v2, v3 */
/* :sswitch_5 */
v0 = (( java.lang.String ) v0 ).equals ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* move v2, v1 */
/* :sswitch_6 */
v0 = (( java.lang.String ) v0 ).equals ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* :sswitch_7 */
v0 = (( java.lang.String ) v0 ).equals ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* move v2, v4 */
} // :goto_3
/* move v2, v9 */
} // :goto_4
/* packed-switch v2, :pswitch_data_1 */
/* .line 191 */
/* :pswitch_4 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* and-int/lit8 v2, v0, 0x20 */
/* not-int v2, v2 */
/* and-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 192 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 193 */
/* .line 187 */
/* :pswitch_5 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* and-int/lit8 v2, v0, 0x10 */
/* not-int v2, v2 */
/* and-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 188 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 189 */
/* .line 183 */
/* :pswitch_6 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* and-int/lit8 v2, v0, 0x8 */
/* not-int v2, v2 */
/* and-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 184 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 185 */
/* .line 179 */
/* :pswitch_7 */
/* iget v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* and-int/lit8 v2, v0, 0x2 */
/* not-int v2, v2 */
/* and-int/2addr v0, v2 */
/* iput v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mInputDebugLevel:I */
/* .line 180 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->setInputLogLevel()V */
/* .line 181 */
/* nop */
/* .line 196 */
} // :goto_5
/* .line 199 */
} // :cond_4
v0 = this.mMiuiInputManagerService;
(( com.android.server.input.MiuiInputManagerService ) v0 ).dump ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/input/MiuiInputManagerService;->dump(Ljava/io/PrintWriter;)V
/* .line 201 */
} // :cond_5
} // :goto_6
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x49ba5161 -> :sswitch_3 */
/* 0x308e51f1 -> :sswitch_2 */
/* 0x4907087a -> :sswitch_1 */
/* 0x4e9cc50d -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :sswitch_data_1 */
/* .sparse-switch */
/* -0x49ba5161 -> :sswitch_7 */
/* 0x308e51f1 -> :sswitch_6 */
/* 0x4907087a -> :sswitch_5 */
/* 0x4e9cc50d -> :sswitch_4 */
} // .end sparse-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
} // .end packed-switch
} // .end method
public android.view.InputDevice filterKeyboardDeviceIfNeeded ( android.view.InputDevice[] p0 ) {
/* .locals 2 */
/* .param p1, "inputDevices" # [Landroid/view/InputDevice; */
/* .line 235 */
/* move-object v0, p1 */
/* .line 236 */
/* .local v0, "newInputDevices":[Landroid/view/InputDevice; */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 237 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
/* .line 239 */
} // :cond_0
} // .end method
com.android.server.input.MiuiInputManagerService getMiuiInputManagerService ( ) {
/* .locals 1 */
/* .line 101 */
v0 = this.mMiuiInputManagerService;
} // .end method
public Long getPtr ( ) {
/* .locals 2 */
/* .line 136 */
/* iget-wide v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPtr:J */
/* return-wide v0 */
} // .end method
public void handleAppDied ( Integer p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 614 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->ensureMiuiMagicPointerServiceInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 615 */
return;
/* .line 617 */
} // :cond_0
v0 = this.mMiuiMagicPointerService;
(( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v0 ).handleAppDied ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->handleAppDied(ILcom/android/server/am/ProcessRecord;)V
/* .line 618 */
return;
} // .end method
public void init ( android.content.Context p0, android.os.Handler p1, com.android.server.input.InputManagerService p2, com.android.server.input.NativeInputManagerService p3 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "inputManagerService" # Lcom/android/server/input/InputManagerService; */
/* .param p4, "nativeInputManagerService" # Lcom/android/server/input/NativeInputManagerService; */
/* .line 84 */
final String v0 = "InputManagerServiceStubImpl"; // const-string v0, "InputManagerServiceStubImpl"
final String v1 = "init"; // const-string v1, "init"
android.util.Slog .d ( v0,v1 );
/* .line 85 */
this.mContext = p1;
/* .line 86 */
this.mHandler = p2;
/* .line 87 */
this.mNativeInputManagerService = p4;
/* .line 88 */
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mPtr:J */
/* .line 89 */
this.mInputManagerService = p3;
/* .line 93 */
/* new-instance v0, Lcom/android/server/input/MiuiInputManagerService; */
/* invoke-direct {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;-><init>(Landroid/content/Context;)V */
this.mMiuiInputManagerService = v0;
/* .line 94 */
v0 = (( com.android.server.input.InputManagerServiceStubImpl ) p0 ).isPad ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->isPad()Z
/* invoke-direct {p0, v0}, Lcom/android/server/input/InputManagerServiceStubImpl;->switchPadMode(Z)V */
/* .line 95 */
return;
} // .end method
public Boolean interceptKeyboardNotification ( android.content.Context p0, Integer[] p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "keyboardDeviceIds" # [I */
/* .line 245 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 246 */
/* .line 248 */
} // :cond_0
/* array-length v0, p2 */
/* move v2, v1 */
} // :goto_0
/* if-ge v2, v0, :cond_2 */
/* aget v3, p2, v2 */
/* .line 249 */
/* .local v3, "deviceId":I */
v4 = this.mInputManagerService;
(( com.android.server.input.InputManagerService ) v4 ).getInputDevice ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/input/InputManagerService;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 250 */
/* .local v4, "device":Landroid/view/InputDevice; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 251 */
v5 = (( android.view.InputDevice ) v4 ).getVendorId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getVendorId()I
/* .line 252 */
/* .local v5, "vendorId":I */
v6 = (( android.view.InputDevice ) v4 ).getProductId ( ); // invoke-virtual {v4}, Landroid/view/InputDevice;->getProductId()I
/* .line 253 */
/* .local v6, "productId":I */
v7 = miui.hardware.input.MiuiKeyboardHelper .isXiaomiKeyboard ( v6,v5 );
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 254 */
int v0 = 1; // const/4 v0, 0x1
/* .line 248 */
} // .end local v3 # "deviceId":I
} // .end local v4 # "device":Landroid/view/InputDevice;
} // .end local v5 # "vendorId":I
} // .end local v6 # "productId":I
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 258 */
} // :cond_2
} // .end method
public Boolean isPad ( ) {
/* .locals 1 */
/* .line 424 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
} // .end method
public Boolean jumpPermissionCheck ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "permission" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 468 */
final String v0 = "android.permission.INJECT_EVENTS"; // const-string v0, "android.permission.INJECT_EVENTS"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x3ea */
/* if-ne p2, v0, :cond_0 */
/* .line 470 */
final String v0 = "InputManagerServiceStubImpl"; // const-string v0, "InputManagerServiceStubImpl"
final String v1 = "INJECT_EVENTS Permission Denial, bypass BLUETOOTH_UID!"; // const-string v1, "INJECT_EVENTS Permission Denial, bypass BLUETOOTH_UID!"
android.util.Slog .d ( v0,v1 );
/* .line 471 */
int v0 = 1; // const/4 v0, 0x1
/* .line 473 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean needInterceptSetCustomPointerIcon ( android.view.PointerIcon p0 ) {
/* .locals 1 */
/* .param p1, "icon" # Landroid/view/PointerIcon; */
/* .line 582 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->ensureMiuiMagicPointerServiceInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 583 */
int v0 = 0; // const/4 v0, 0x0
/* .line 585 */
} // :cond_0
v0 = this.mMiuiMagicPointerService;
v0 = (( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v0 ).needInterceptSetCustomPointerIcon ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->needInterceptSetCustomPointerIcon(Landroid/view/PointerIcon;)Z
} // .end method
public Boolean needInterceptSetPointerIconType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "iconType" # I */
/* .line 574 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->ensureMiuiMagicPointerServiceInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 575 */
int v0 = 0; // const/4 v0, 0x0
/* .line 577 */
} // :cond_0
v0 = this.mMiuiMagicPointerService;
v0 = (( com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ) v0 ).needInterceptSetPointerIconType ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;->needInterceptSetPointerIconType(I)Z
} // .end method
public void notifySwitch ( Long p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "whenNanos" # J */
/* .param p3, "switchValues" # I */
/* .param p4, "switchMask" # I */
/* .line 229 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifySwitch: values="; // const-string v1, "notifySwitch: values="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toHexString ( p3 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mask="; // const-string v1, ", mask="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 230 */
java.lang.Integer .toHexString ( p4 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 229 */
final String v1 = "InputManagerServiceStubImpl"; // const-string v1, "InputManagerServiceStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 231 */
return;
} // .end method
public void notifyTabletSwitchChanged ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "tabletOpen" # Z */
/* .line 262 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 263 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->getPadManagerInstance()Lcom/android/server/input/padkeyboard/MiuiPadKeyboardManager; */
/* .line 265 */
} // :cond_0
return;
} // .end method
public void onDisplayViewportsSet ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/hardware/display/DisplayViewport;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 564 */
/* .local p1, "viewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;" */
v0 = this.mMiuiInputManagerService;
(( com.android.server.input.MiuiInputManagerService ) v0 ).updateDisplayViewport ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->updateDisplayViewport(Ljava/util/List;)V
/* .line 565 */
return;
} // .end method
public void onUpdatePointerDisplayId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "displayId" # I */
/* .line 560 */
v0 = this.mMiuiInputManagerService;
(( com.android.server.input.MiuiInputManagerService ) v0 ).updatePointerDisplayId ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->updatePointerDisplayId(I)V
/* .line 561 */
return;
} // .end method
public void registerMiuiOptimizationObserver ( ) {
/* .locals 5 */
/* .line 327 */
/* new-instance v0, Lcom/android/server/input/InputManagerServiceStubImpl$3; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/input/InputManagerServiceStubImpl$3;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 340 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = android.provider.MiuiSettings$Secure.MIUI_OPTIMIZATION;
/* .line 341 */
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 340 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 343 */
(( android.database.ContentObserver ) v0 ).onChange ( v3 ); // invoke-virtual {v0, v3}, Landroid/database/ContentObserver;->onChange(Z)V
/* .line 344 */
return;
} // .end method
public void registerStub ( ) {
/* .locals 2 */
/* .line 106 */
final String v0 = "InputManagerServiceStubImpl"; // const-string v0, "InputManagerServiceStubImpl"
final String v1 = "registerStub"; // const-string v1, "registerStub"
android.util.Slog .d ( v0,v1 );
/* .line 107 */
v0 = this.mMiuiInputManagerService;
(( com.android.server.input.MiuiInputManagerService ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerService;->start()V
/* .line 108 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerSynergyModeSettingObserver()V */
/* .line 109 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateSynergyModeFromSettings()V */
/* .line 110 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerPointerLocationSettingObserver()V */
/* .line 111 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updatePointerLocationFromSettings()V */
/* .line 112 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerTouchesPreventRecordSettingObserver()V */
/* .line 113 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchesPreventRecorderFromSettings()V */
/* .line 114 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerDefaultInputMethodSettingObserver()V */
/* .line 115 */
(( com.android.server.input.InputManagerServiceStubImpl ) p0 ).updateDefaultInputMethodFromSettings ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDefaultInputMethodFromSettings()V
/* .line 116 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerInputLevelSelect()V */
/* .line 117 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateFromInputLevelSetting()V */
/* .line 118 */
(( com.android.server.input.InputManagerServiceStubImpl ) p0 ).registerMiuiOptimizationObserver ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerMiuiOptimizationObserver()V
/* .line 119 */
com.miui.server.input.PadManager .getInstance ( );
v0 = (( com.miui.server.input.PadManager ) v0 ).isPad ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 120 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerMouseGestureSettingObserver()V */
/* .line 121 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateMouseGestureSettings()V */
/* .line 124 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/input/InputManagerServiceStubImpl;->BERSERK_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 125 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerGameMode()V */
/* .line 128 */
} // :cond_1
v0 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportOffScreenQuickNote ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 129 */
(( com.android.server.input.InputManagerServiceStubImpl ) p0 ).registerStylusQuickNoteModeSetting ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->registerStylusQuickNoteModeSetting()V
/* .line 130 */
(( com.android.server.input.InputManagerServiceStubImpl ) p0 ).updateTouchStylusQuickNoteMode ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchStylusQuickNoteMode()V
/* .line 132 */
} // :cond_2
return;
} // .end method
public void registerStylusQuickNoteModeSetting ( ) {
/* .locals 5 */
/* .line 500 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 501 */
/* const-string/jumbo v1, "stylus_quick_note_screen_off" */
android.provider.Settings$System .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/input/InputManagerServiceStubImpl$9; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/input/InputManagerServiceStubImpl$9;-><init>(Lcom/android/server/input/InputManagerServiceStubImpl;Landroid/os/Handler;)V */
/* .line 500 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 508 */
return;
} // .end method
void setCustomPointerIcon ( android.view.PointerIcon p0 ) {
/* .locals 1 */
/* .param p1, "pointerIcon" # Landroid/view/PointerIcon; */
/* .line 610 */
v0 = this.mInputManagerService;
(( com.android.server.input.InputManagerService ) v0 ).setCustomPointerIcon ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/InputManagerService;->setCustomPointerIcon(Landroid/view/PointerIcon;)V
/* .line 611 */
return;
} // .end method
public void setInputMethodStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "shown" # Z */
/* .line 321 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* .line 322 */
/* .local v0, "inputCommonConfig":Lcom/android/server/input/config/InputCommonConfig; */
/* iget-boolean v1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mCustomizeInputMethod:Z */
(( com.android.server.input.config.InputCommonConfig ) v0 ).setInputMethodStatus ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/input/config/InputCommonConfig;->setInputMethodStatus(ZZ)V
/* .line 323 */
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 324 */
return;
} // .end method
public void setInteractive ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "interactive" # Z */
/* .line 569 */
v0 = this.mMiuiInputManagerService;
(( com.android.server.input.MiuiInputManagerService ) v0 ).setInteractive ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->setInteractive(Z)V
/* .line 570 */
return;
} // .end method
void setPointerIconType ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "iconType" # I */
/* .line 606 */
v0 = this.mInputManagerService;
(( com.android.server.input.InputManagerService ) v0 ).setPointerIconType ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/InputManagerService;->setPointerIconType(I)V
/* .line 607 */
return;
} // .end method
public void setShowTouchLevelFromSettings ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "showTouchesSwitch" # I */
/* .line 206 */
/* iput p1, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mShowTouches:I */
/* .line 207 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDebugLevel()V */
/* .line 208 */
return;
} // .end method
public void updateDefaultInputMethodFromSettings ( ) {
/* .locals 4 */
/* .line 439 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "default_input_method"; // const-string v1, "default_input_method"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 441 */
/* .local v0, "inputMethodId":Ljava/lang/String; */
final String v1 = ""; // const-string v1, ""
/* .line 442 */
/* .local v1, "defaultInputMethod":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
final String v2 = "/"; // const-string v2, "/"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 443 */
/* const/16 v2, 0x2f */
v2 = (( java.lang.String ) v0 ).indexOf ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I
int v3 = 0; // const/4 v3, 0x0
(( java.lang.String ) v0 ).substring ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 445 */
} // :cond_0
v2 = v2 = com.android.server.input.InputManagerServiceStubImpl.MIUI_INPUTMETHOD;
/* iput-boolean v2, p0, Lcom/android/server/input/InputManagerServiceStubImpl;->mCustomizeInputMethod:Z */
/* .line 446 */
return;
} // .end method
public void updateFromUserSwitch ( ) {
/* .locals 1 */
/* .line 212 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateSynergyModeFromSettings()V */
/* .line 213 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updatePointerLocationFromSettings()V */
/* .line 214 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchesPreventRecorderFromSettings()V */
/* .line 215 */
(( com.android.server.input.InputManagerServiceStubImpl ) p0 ).updateDefaultInputMethodFromSettings ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateDefaultInputMethodFromSettings()V
/* .line 216 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateFromInputLevelSetting()V */
/* .line 218 */
/* sget-boolean v0, Lcom/android/server/input/InputManagerServiceStubImpl;->BERSERK_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 219 */
/* invoke-direct {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateOnewayModeFromSettings()V */
/* .line 222 */
} // :cond_0
v0 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportOffScreenQuickNote ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 223 */
(( com.android.server.input.InputManagerServiceStubImpl ) p0 ).updateTouchStylusQuickNoteMode ( ); // invoke-virtual {p0}, Lcom/android/server/input/InputManagerServiceStubImpl;->updateTouchStylusQuickNoteMode()V
/* .line 225 */
} // :cond_1
return;
} // .end method
public void updateTouchStylusQuickNoteMode ( ) {
/* .locals 4 */
/* .line 511 */
v0 = this.mContext;
/* .line 512 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* sget-boolean v1, Lcom/android/server/input/InputManagerServiceStubImpl;->SUPPORT_QUICK_NOTE_DEFAULT:Z */
/* .line 511 */
/* const-string/jumbo v2, "stylus_quick_note_screen_off" */
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.MiuiSettings$System .getBooleanForUser ( v0,v2,v1,v3 );
/* .line 514 */
/* .local v0, "stylusQuickNoteMode":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Update stylus quick note when screen off mode = "; // const-string v2, "Update stylus quick note when screen off mode = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "InputManagerServiceStubImpl"; // const-string v2, "InputManagerServiceStubImpl"
android.util.Slog .d ( v2,v1 );
/* .line 515 */
miui.util.ITouchFeature .getInstance ( );
/* .line 516 */
/* nop */
/* .line 515 */
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x18 */
(( miui.util.ITouchFeature ) v1 ).setTouchMode ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 517 */
return;
} // .end method
