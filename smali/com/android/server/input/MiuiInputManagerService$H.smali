.class Lcom/android/server/input/MiuiInputManagerService$H;
.super Landroid/os/Handler;
.source "MiuiInputManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiuiInputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field private static final DATA_ACTION:Ljava/lang/String; = "action"

.field private static final DATA_FUNCTION:Ljava/lang/String; = "function"

.field private static final MSG_NOTIFY_LISTENER:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/input/MiuiInputManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/input/MiuiInputManagerService;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/MiuiInputManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 690
    iput-object p1, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    .line 691
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 692
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .line 696
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 697
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "action"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 698
    .local v1, "action":Ljava/lang/String;
    const-string v2, "function"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 699
    .local v2, "function":Ljava/lang/String;
    iget v3, p1, Landroid/os/Message;->what:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 700
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v3}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmShortcutListenersToNotify(Lcom/android/server/input/MiuiInputManagerService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 701
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v3}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmMiuiShortcutSettingsLock(Lcom/android/server/input/MiuiInputManagerService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 702
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v5}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmShortcutListeners(Lcom/android/server/input/MiuiInputManagerService;)Landroid/util/SparseArray;

    move-result-object v5

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 703
    iget-object v5, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v5}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmShortcutListenersToNotify(Lcom/android/server/input/MiuiInputManagerService;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v6}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmShortcutListeners(Lcom/android/server/input/MiuiInputManagerService;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 702
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 705
    .end local v4    # "i":I
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 706
    iget-object v3, p0, Lcom/android/server/input/MiuiInputManagerService$H;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v3}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmShortcutListenersToNotify(Lcom/android/server/input/MiuiInputManagerService;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;

    .line 707
    .local v4, "record":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
    invoke-virtual {v4, v1, v2}, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->notifyShortcutSettingsChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    .end local v4    # "record":Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
    goto :goto_1

    .line 705
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 710
    :cond_1
    return-void
.end method
