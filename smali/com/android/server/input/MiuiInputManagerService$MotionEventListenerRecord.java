class com.android.server.input.MiuiInputManagerService$MotionEventListenerRecord implements android.os.IBinder$DeathRecipient {
	 /* .source "MiuiInputManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/MiuiInputManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MotionEventListenerRecord" */
} // .end annotation
/* # instance fields */
private final miui.hardware.input.IMiuiMotionEventListener mMotionEventListener;
private final Integer mPid;
final com.android.server.input.MiuiInputManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.input.MiuiInputManagerService$MotionEventListenerRecord ( ) {
/* .locals 0 */
/* .param p2, "motionEventListener" # Lmiui/hardware/input/IMiuiMotionEventListener; */
/* .param p3, "pid" # I */
/* .line 565 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 566 */
this.mMotionEventListener = p2;
/* .line 567 */
/* iput p3, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mPid:I */
/* .line 568 */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 2 */
/* .line 572 */
v0 = this.this$0;
/* iget v1, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mPid:I */
com.android.server.input.MiuiInputManagerService .-$$Nest$mremoveMotionEventListener ( v0,v1 );
/* .line 573 */
return;
} // .end method
public void notifyMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 577 */
try { // :try_start_0
	 v0 = this.mMotionEventListener;
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 581 */
	 /* .line 578 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 579 */
	 /* .local v0, "ex":Landroid/os/RemoteException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Failed to notify process "; // const-string v2, "Failed to notify process "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v2, p0, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->mPid:I */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v2 = " , assuming it died."; // const-string v2, " , assuming it died."
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v2 = "MiuiInputManagerService"; // const-string v2, "MiuiInputManagerService"
	 android.util.Slog .w ( v2,v1,v0 );
	 /* .line 580 */
	 (( com.android.server.input.MiuiInputManagerService$MotionEventListenerRecord ) p0 ).binderDied ( ); // invoke-virtual {p0}, Lcom/android/server/input/MiuiInputManagerService$MotionEventListenerRecord;->binderDied()V
	 /* .line 582 */
} // .end local v0 # "ex":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
