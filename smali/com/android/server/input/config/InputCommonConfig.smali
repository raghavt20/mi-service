.class public final Lcom/android/server/input/config/InputCommonConfig;
.super Lcom/android/server/input/config/BaseInputConfig;
.source "InputCommonConfig.java"


# static fields
.field public static final CONFIG_TYPE:I = 0x2

.field private static volatile instance:Lcom/android/server/input/config/InputCommonConfig;


# instance fields
.field private mCtsMode:Z

.field private mCustomized:Z

.field private mInjectEventStatus:Z

.field private mIsFilterInterceptMode:Z

.field private mIsLaserDrawing:Z

.field private mIsUsingMagicPointer:Z

.field private mMiInputEventTimeLineEnable:Z

.field private mMouseNaturalScroll:Z

.field private mNeedSyncMotion:Z

.field private mOnewayMode:Z

.field private mPadMode:Z

.field private mProductId:I

.field private mRecordEventStatus:Z

.field private mShown:Z

.field private mSlidGestureHotZoneWidthRate:F

.field private mStylusBlockerDelayTime:I

.field private mStylusBlockerEnable:Z

.field private mStylusBlockerMoveThreshold:F

.field private mSynergyMode:I

.field private mTapTouchPad:Z

.field private mTopGestureHotZoneHeightRate:F

.field private mVendorId:I

.field private mWakeUpMode:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 75
    invoke-direct {p0}, Lcom/android/server/input/config/BaseInputConfig;-><init>()V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsFilterInterceptMode:Z

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mMiInputEventTimeLineEnable:Z

    .line 76
    return-void
.end method

.method public static getInstance()Lcom/android/server/input/config/InputCommonConfig;
    .locals 2

    .line 80
    sget-object v0, Lcom/android/server/input/config/InputCommonConfig;->instance:Lcom/android/server/input/config/InputCommonConfig;

    if-nez v0, :cond_1

    .line 81
    const-class v0, Lcom/android/server/input/config/InputCommonConfig;

    monitor-enter v0

    .line 82
    :try_start_0
    sget-object v1, Lcom/android/server/input/config/InputCommonConfig;->instance:Lcom/android/server/input/config/InputCommonConfig;

    if-nez v1, :cond_0

    .line 83
    new-instance v1, Lcom/android/server/input/config/InputCommonConfig;

    invoke-direct {v1}, Lcom/android/server/input/config/InputCommonConfig;-><init>()V

    sput-object v1, Lcom/android/server/input/config/InputCommonConfig;->instance:Lcom/android/server/input/config/InputCommonConfig;

    .line 85
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 88
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/config/InputCommonConfig;->instance:Lcom/android/server/input/config/InputCommonConfig;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic flushToNative()V
    .locals 0

    .line 5
    invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->flushToNative()V

    return-void
.end method

.method public bridge synthetic getConfigNativePtr()J
    .locals 2

    .line 5
    invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigNativePtr()J

    move-result-wide v0

    return-wide v0
.end method

.method public getConfigType()I
    .locals 1

    .line 122
    const/4 v0, 0x2

    return v0
.end method

.method public setCtsMode(Z)V
    .locals 0
    .param p1, "ctsMode"    # Z

    .line 181
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mCtsMode:Z

    .line 182
    return-void
.end method

.method public setFilterInterceptMode(Z)V
    .locals 0
    .param p1, "interceptMode"    # Z

    .line 201
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsFilterInterceptMode:Z

    .line 202
    return-void
.end method

.method public setInjectEventStatus(Z)V
    .locals 0
    .param p1, "injectEventStatus"    # Z

    .line 134
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mInjectEventStatus:Z

    .line 135
    return-void
.end method

.method public setInputMethodStatus(ZZ)V
    .locals 0
    .param p1, "shown"    # Z
    .param p2, "customized"    # Z

    .line 142
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mShown:Z

    .line 143
    iput-boolean p2, p0, Lcom/android/server/input/config/InputCommonConfig;->mCustomized:Z

    .line 144
    return-void
.end method

.method public setIsUsingMagicPointer(Z)V
    .locals 0
    .param p1, "isUsingMagicPointer"    # Z

    .line 209
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsUsingMagicPointer:Z

    .line 210
    return-void
.end method

.method public setLaserIsDrawing(Z)V
    .locals 0
    .param p1, "isDrawing"    # Z

    .line 197
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsLaserDrawing:Z

    .line 198
    return-void
.end method

.method public setMiInputEventTimeLineMode(Z)V
    .locals 0
    .param p1, "isMiInputEventTimeLineEnable"    # Z

    .line 205
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mMiInputEventTimeLineEnable:Z

    .line 206
    return-void
.end method

.method public setMiuiKeyboardInfo(II)V
    .locals 0
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I

    .line 155
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mVendorId:I

    .line 156
    iput p2, p0, Lcom/android/server/input/config/InputCommonConfig;->mProductId:I

    .line 157
    return-void
.end method

.method public setMouseNaturalScrollStatus(Z)V
    .locals 0
    .param p1, "mouseNaturalScroll"    # Z

    .line 151
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mMouseNaturalScroll:Z

    .line 152
    return-void
.end method

.method public setNeedSyncMotion(Z)V
    .locals 0
    .param p1, "needSyncMotion"    # Z

    .line 185
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mNeedSyncMotion:Z

    .line 186
    return-void
.end method

.method public setOnewayMode(Z)V
    .locals 0
    .param p1, "onewayMode"    # Z

    .line 160
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mOnewayMode:Z

    .line 161
    return-void
.end method

.method public setPadMode(Z)V
    .locals 0
    .param p1, "padMode"    # Z

    .line 147
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mPadMode:Z

    .line 148
    return-void
.end method

.method public setRecordEventStatus(Z)V
    .locals 0
    .param p1, "recordEventStatus"    # Z

    .line 138
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mRecordEventStatus:Z

    .line 139
    return-void
.end method

.method public setSlidGestureHotZoneWidthRate(F)V
    .locals 0
    .param p1, "rate"    # F

    .line 193
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mSlidGestureHotZoneWidthRate:F

    .line 194
    return-void
.end method

.method public setStylusBlockerDelayTime(I)V
    .locals 0
    .param p1, "stylusBlockerDelayTime"    # I

    .line 173
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mStylusBlockerDelayTime:I

    .line 174
    return-void
.end method

.method public setStylusBlockerEnable(Z)V
    .locals 0
    .param p1, "stylusBlockerEnable"    # Z

    .line 169
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mStylusBlockerEnable:Z

    .line 170
    return-void
.end method

.method public setStylusBlockerMoveThreshold(F)V
    .locals 0
    .param p1, "stylusBlockerMoveThreshold"    # F

    .line 177
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mStylusBlockerMoveThreshold:F

    .line 178
    return-void
.end method

.method public setSynergyMode(I)V
    .locals 0
    .param p1, "synergyMode"    # I

    .line 130
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mSynergyMode:I

    .line 131
    return-void
.end method

.method public setTapTouchPad(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 165
    iput-boolean p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mTapTouchPad:Z

    .line 166
    return-void
.end method

.method public setTopGestureHotZoneHeightRate(F)V
    .locals 0
    .param p1, "rate"    # F

    .line 189
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mTopGestureHotZoneHeightRate:F

    .line 190
    return-void
.end method

.method public setWakeUpMode(I)V
    .locals 0
    .param p1, "wakeUpMode"    # I

    .line 126
    iput p1, p0, Lcom/android/server/input/config/InputCommonConfig;->mWakeUpMode:I

    .line 127
    return-void
.end method

.method protected writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;

    .line 93
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mWakeUpMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mSynergyMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mInjectEventStatus:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 96
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mRecordEventStatus:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 97
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mShown:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 98
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mCustomized:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 99
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mPadMode:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 100
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mMouseNaturalScroll:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 101
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mProductId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mVendorId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mOnewayMode:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 106
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mTapTouchPad:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 107
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mStylusBlockerEnable:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 108
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mStylusBlockerDelayTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mStylusBlockerMoveThreshold:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 110
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mCtsMode:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 111
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mNeedSyncMotion:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 112
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mTopGestureHotZoneHeightRate:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 113
    iget v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mSlidGestureHotZoneWidthRate:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 114
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsLaserDrawing:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 115
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsFilterInterceptMode:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 116
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mMiInputEventTimeLineEnable:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 117
    iget-boolean v0, p0, Lcom/android/server/input/config/InputCommonConfig;->mIsUsingMagicPointer:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 118
    return-void
.end method
