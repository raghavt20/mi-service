.class public final Lcom/android/server/input/config/InputKnockConfig;
.super Lcom/android/server/input/config/BaseInputConfig;
.source "InputKnockConfig.java"


# static fields
.field public static final CONFIG_TYPE:I = 0x3

.field private static volatile mInstance:Lcom/android/server/input/config/InputKnockConfig;


# instance fields
.field private mKnockAlgorithmPath:Ljava/lang/String;

.field private mKnockDeviceProperty:I

.field private mKnockFeatureState:I

.field private mKnockInValidRectBottom:I

.field private mKnockInValidRectLeft:I

.field private mKnockInValidRectRight:I

.field private mKnockInValidRectTop:I

.field private mKnockScoreThreshold:F

.field private mKnockValidRectBottom:I

.field private mKnockValidRectLeft:I

.field private mKnockValidRectRight:I

.field private mKnockValidRectTop:I

.field private mQuickMoveSpeed:F

.field private mSensorThreshold:[I

.field private mUseFrame:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/android/server/input/config/BaseInputConfig;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockAlgorithmPath:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    filled-new-array {v0, v0, v0, v0}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mSensorThreshold:[I

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/android/server/input/config/InputKnockConfig;
    .locals 2

    .line 34
    sget-object v0, Lcom/android/server/input/config/InputKnockConfig;->mInstance:Lcom/android/server/input/config/InputKnockConfig;

    if-nez v0, :cond_1

    .line 35
    const-class v0, Lcom/android/server/input/config/InputKnockConfig;

    monitor-enter v0

    .line 36
    :try_start_0
    sget-object v1, Lcom/android/server/input/config/InputKnockConfig;->mInstance:Lcom/android/server/input/config/InputKnockConfig;

    if-nez v1, :cond_0

    .line 37
    new-instance v1, Lcom/android/server/input/config/InputKnockConfig;

    invoke-direct {v1}, Lcom/android/server/input/config/InputKnockConfig;-><init>()V

    sput-object v1, Lcom/android/server/input/config/InputKnockConfig;->mInstance:Lcom/android/server/input/config/InputKnockConfig;

    .line 39
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 41
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/config/InputKnockConfig;->mInstance:Lcom/android/server/input/config/InputKnockConfig;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic flushToNative()V
    .locals 0

    .line 5
    invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->flushToNative()V

    return-void
.end method

.method public bridge synthetic getConfigNativePtr()J
    .locals 2

    .line 5
    invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigNativePtr()J

    move-result-wide v0

    return-wide v0
.end method

.method public getConfigType()I
    .locals 1

    .line 64
    const/4 v0, 0x3

    return v0
.end method

.method public setKnockDeviceProperty(I)V
    .locals 0
    .param p1, "property"    # I

    .line 81
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockDeviceProperty:I

    .line 82
    return-void
.end method

.method public setKnockFeatureState(I)V
    .locals 0
    .param p1, "state"    # I

    .line 72
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockFeatureState:I

    .line 73
    return-void
.end method

.method public setKnockInValidRect(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 108
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectLeft:I

    .line 109
    iput p2, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectTop:I

    .line 110
    iput p3, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectRight:I

    .line 111
    iput p4, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectBottom:I

    .line 112
    return-void
.end method

.method public setKnockQuickMoveSpeed(F)V
    .locals 0
    .param p1, "quickMoveSpeed"    # F

    .line 144
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mQuickMoveSpeed:F

    .line 145
    return-void
.end method

.method public setKnockScoreThreshold(F)V
    .locals 0
    .param p1, "score"    # F

    .line 128
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockScoreThreshold:F

    .line 129
    return-void
.end method

.method public setKnockSensorThreshold([I)V
    .locals 0
    .param p1, "sensorThreshold"    # [I

    .line 148
    iput-object p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mSensorThreshold:[I

    .line 149
    return-void
.end method

.method public setKnockUseFrame(I)V
    .locals 0
    .param p1, "useFrame"    # I

    .line 136
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mUseFrame:I

    .line 137
    return-void
.end method

.method public setKnockValidRect(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 93
    iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectLeft:I

    .line 94
    iput p2, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectTop:I

    .line 95
    iput p3, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectRight:I

    .line 96
    iput p4, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectBottom:I

    .line 97
    return-void
.end method

.method public updateAlgorithmPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .line 120
    iput-object p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockAlgorithmPath:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;

    .line 46
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockFeatureState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockDeviceProperty:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectLeft:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectTop:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectRight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectBottom:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectLeft:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectTop:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectRight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectBottom:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget-object v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockAlgorithmPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString8(Ljava/lang/String;)V

    .line 57
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockScoreThreshold:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 58
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mUseFrame:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mQuickMoveSpeed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 60
    iget-object v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mSensorThreshold:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 61
    return-void
.end method
