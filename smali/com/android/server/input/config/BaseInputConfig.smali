.class abstract Lcom/android/server/input/config/BaseInputConfig;
.super Ljava/lang/Object;
.source "BaseInputConfig.java"


# static fields
.field public static final PARCEL_NATIVE_PTR_INVALID:J = -0x1L

.field private static final TAG:Ljava/lang/String; = "InputConfig"


# instance fields
.field private final mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/android/server/input/config/BaseInputConfig;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 21
    return-void
.end method


# virtual methods
.method public flushToNative()V
    .locals 4

    .line 45
    iget-object v0, p0, Lcom/android/server/input/config/BaseInputConfig;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigType()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigNativePtr()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/MiuiInputManagerInternal;->setInputConfig(IJ)V

    .line 46
    return-void
.end method

.method public getConfigNativePtr()J
    .locals 5

    .line 24
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 25
    .local v0, "dest":Landroid/os/Parcel;
    invoke-virtual {p0, v0}, Lcom/android/server/input/config/BaseInputConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 26
    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    .line 27
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 28
    const-class v1, Landroid/os/Parcel;

    .line 31
    .local v1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/os/Parcel;>;"
    :try_start_0
    const-string v2, "mNativePtr"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 32
    .local v2, "privateStringFiled":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 33
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v3

    .line 34
    .end local v2    # "privateStringFiled":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v2

    .line 35
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "InputConfig"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 37
    .end local v2    # "e":Ljava/lang/Exception;
    const-wide/16 v2, -0x1

    return-wide v2
.end method

.method public abstract getConfigType()I
.end method

.method protected abstract writeToParcel(Landroid/os/Parcel;)V
.end method
