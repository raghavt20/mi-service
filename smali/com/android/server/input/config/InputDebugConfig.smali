.class public final Lcom/android/server/input/config/InputDebugConfig;
.super Lcom/android/server/input/config/BaseInputConfig;
.source "InputDebugConfig.java"


# static fields
.field public static final CONFIG_TYPE:I = 0x1

.field private static volatile instance:Lcom/android/server/input/config/InputDebugConfig;


# instance fields
.field private DEBUG_INPUT_DISPATCHER_ALL:I

.field private DEBUG_INPUT_EVENT_DETAIL:I

.field private DEBUG_INPUT_EVENT_MAJAR:I

.field private DEBUG_INPUT_READER_ALL:I

.field private DEBUG_INPUT_TRANSPORT_ALL:I

.field private mInputReaderAll:I

.field private mInputTransportAll:I

.field private mInputdispatcherAll:I

.field private mInputdispatcherDetail:I

.field private mInputdispatcherMajor:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/android/server/input/config/BaseInputConfig;-><init>()V

    .line 14
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_EVENT_MAJAR:I

    .line 15
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_EVENT_DETAIL:I

    .line 16
    const/16 v0, 0x8

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_DISPATCHER_ALL:I

    .line 17
    const/16 v0, 0x10

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_READER_ALL:I

    .line 18
    const/16 v0, 0x20

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_TRANSPORT_ALL:I

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/android/server/input/config/InputDebugConfig;
    .locals 2

    .line 35
    sget-object v0, Lcom/android/server/input/config/InputDebugConfig;->instance:Lcom/android/server/input/config/InputDebugConfig;

    if-nez v0, :cond_1

    .line 36
    const-class v0, Lcom/android/server/input/config/InputDebugConfig;

    monitor-enter v0

    .line 37
    :try_start_0
    sget-object v1, Lcom/android/server/input/config/InputDebugConfig;->instance:Lcom/android/server/input/config/InputDebugConfig;

    if-nez v1, :cond_0

    .line 38
    new-instance v1, Lcom/android/server/input/config/InputDebugConfig;

    invoke-direct {v1}, Lcom/android/server/input/config/InputDebugConfig;-><init>()V

    sput-object v1, Lcom/android/server/input/config/InputDebugConfig;->instance:Lcom/android/server/input/config/InputDebugConfig;

    .line 40
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 43
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/config/InputDebugConfig;->instance:Lcom/android/server/input/config/InputDebugConfig;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic flushToNative()V
    .locals 0

    .line 9
    invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->flushToNative()V

    return-void
.end method

.method public bridge synthetic getConfigNativePtr()J
    .locals 2

    .line 9
    invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigNativePtr()J

    move-result-wide v0

    return-wide v0
.end method

.method public getConfigType()I
    .locals 1

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public setInputDebugFromDump(I)V
    .locals 1
    .param p1, "debugLog"    # I

    .line 48
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_EVENT_MAJAR:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherMajor:I

    .line 49
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_EVENT_DETAIL:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherDetail:I

    .line 50
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_DISPATCHER_ALL:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherAll:I

    .line 51
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_READER_ALL:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputReaderAll:I

    .line 52
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_TRANSPORT_ALL:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputTransportAll:I

    .line 53
    return-void
.end method

.method public setInputDispatcherMajor(II)V
    .locals 1
    .param p1, "cmdSetting"    # I
    .param p2, "inputDispatcherMajor"    # I

    .line 73
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->DEBUG_INPUT_EVENT_MAJAR:I

    and-int/2addr v0, p1

    .line 74
    .local v0, "cmdMajor":I
    if-nez v0, :cond_0

    .line 75
    iput p2, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherMajor:I

    goto :goto_0

    .line 77
    :cond_0
    iput v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherMajor:I

    .line 79
    :goto_0
    return-void
.end method

.method protected writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;

    .line 58
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherMajor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherDetail:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputdispatcherAll:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputReaderAll:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    iget v0, p0, Lcom/android/server/input/config/InputDebugConfig;->mInputTransportAll:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    return-void
.end method
