abstract class com.android.server.input.config.BaseInputConfig {
	 /* .source "BaseInputConfig.java" */
	 /* # static fields */
	 public static final Long PARCEL_NATIVE_PTR_INVALID;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
	 /* # direct methods */
	 public com.android.server.input.config.BaseInputConfig ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 20 */
		 /* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
		 this.mMiuiInputManagerInternal = v0;
		 /* .line 21 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void flushToNative ( ) {
		 /* .locals 4 */
		 /* .line 45 */
		 v0 = this.mMiuiInputManagerInternal;
		 v1 = 		 (( com.android.server.input.config.BaseInputConfig ) p0 ).getConfigType ( ); // invoke-virtual {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigType()I
		 (( com.android.server.input.config.BaseInputConfig ) p0 ).getConfigNativePtr ( ); // invoke-virtual {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigNativePtr()J
		 /* move-result-wide v2 */
		 (( com.android.server.input.MiuiInputManagerInternal ) v0 ).setInputConfig ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/input/MiuiInputManagerInternal;->setInputConfig(IJ)V
		 /* .line 46 */
		 return;
	 } // .end method
	 public Long getConfigNativePtr ( ) {
		 /* .locals 5 */
		 /* .line 24 */
		 android.os.Parcel .obtain ( );
		 /* .line 25 */
		 /* .local v0, "dest":Landroid/os/Parcel; */
		 (( com.android.server.input.config.BaseInputConfig ) p0 ).writeToParcel ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/config/BaseInputConfig;->writeToParcel(Landroid/os/Parcel;)V
		 /* .line 26 */
		 (( android.os.Parcel ) v0 ).marshall ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
		 /* .line 27 */
		 int v1 = 0; // const/4 v1, 0x0
		 (( android.os.Parcel ) v0 ).setDataPosition ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V
		 /* .line 28 */
		 /* const-class v1, Landroid/os/Parcel; */
		 /* .line 31 */
		 /* .local v1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/os/Parcel;>;" */
		 try { // :try_start_0
			 final String v2 = "mNativePtr"; // const-string v2, "mNativePtr"
			 (( java.lang.Class ) v1 ).getDeclaredField ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
			 /* .line 32 */
			 /* .local v2, "privateStringFiled":Ljava/lang/reflect/Field; */
			 int v3 = 1; // const/4 v3, 0x1
			 (( java.lang.reflect.Field ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
			 /* .line 33 */
			 (( java.lang.reflect.Field ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v3, Ljava/lang/Long; */
			 (( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
			 /* move-result-wide v3 */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* return-wide v3 */
			 /* .line 34 */
		 } // .end local v2 # "privateStringFiled":Ljava/lang/reflect/Field;
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 35 */
		 /* .local v2, "e":Ljava/lang/Exception; */
		 final String v3 = "InputConfig"; // const-string v3, "InputConfig"
		 (( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
		 android.util.Slog .e ( v3,v4,v2 );
		 /* .line 37 */
	 } // .end local v2 # "e":Ljava/lang/Exception;
	 /* const-wide/16 v2, -0x1 */
	 /* return-wide v2 */
} // .end method
public abstract Integer getConfigType ( ) {
} // .end method
protected abstract void writeToParcel ( android.os.Parcel p0 ) {
} // .end method
