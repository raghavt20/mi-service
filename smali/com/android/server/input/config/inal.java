public class inal extends com.android.server.input.config.BaseInputConfig {
	 /* .source "InputKnockConfig.java" */
	 /* # static fields */
	 public static final Integer CONFIG_TYPE;
	 private static volatile com.android.server.input.config.InputKnockConfig mInstance;
	 /* # instance fields */
	 private java.lang.String mKnockAlgorithmPath;
	 private Integer mKnockDeviceProperty;
	 private Integer mKnockFeatureState;
	 private Integer mKnockInValidRectBottom;
	 private Integer mKnockInValidRectLeft;
	 private Integer mKnockInValidRectRight;
	 private Integer mKnockInValidRectTop;
	 private Float mKnockScoreThreshold;
	 private Integer mKnockValidRectBottom;
	 private Integer mKnockValidRectLeft;
	 private Integer mKnockValidRectRight;
	 private Integer mKnockValidRectTop;
	 private Float mQuickMoveSpeed;
	 private mSensorThreshold;
	 private Integer mUseFrame;
	 /* # direct methods */
	 private inal ( ) {
		 /* .locals 1 */
		 /* .line 30 */
		 /* invoke-direct {p0}, Lcom/android/server/input/config/BaseInputConfig;-><init>()V */
		 /* .line 23 */
		 final String v0 = ""; // const-string v0, ""
		 this.mKnockAlgorithmPath = v0;
		 /* .line 27 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* filled-new-array {v0, v0, v0, v0}, [I */
		 this.mSensorThreshold = v0;
		 /* .line 31 */
		 return;
	 } // .end method
	 public static com.android.server.input.config.InputKnockConfig getInstance ( ) {
		 /* .locals 2 */
		 /* .line 34 */
		 v0 = com.android.server.input.config.InputKnockConfig.mInstance;
		 /* if-nez v0, :cond_1 */
		 /* .line 35 */
		 /* const-class v0, Lcom/android/server/input/config/InputKnockConfig; */
		 /* monitor-enter v0 */
		 /* .line 36 */
		 try { // :try_start_0
			 v1 = com.android.server.input.config.InputKnockConfig.mInstance;
			 /* if-nez v1, :cond_0 */
			 /* .line 37 */
			 /* new-instance v1, Lcom/android/server/input/config/InputKnockConfig; */
			 /* invoke-direct {v1}, Lcom/android/server/input/config/InputKnockConfig;-><init>()V */
			 /* .line 39 */
		 } // :cond_0
		 /* monitor-exit v0 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
		 /* .line 41 */
	 } // :cond_1
} // :goto_0
v0 = com.android.server.input.config.InputKnockConfig.mInstance;
} // .end method
/* # virtual methods */
public void flushToNative ( ) { //bridge//synthethic
/* .locals 0 */
/* .line 5 */
/* invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->flushToNative()V */
return;
} // .end method
public Long getConfigNativePtr ( ) { //bridge//synthethic
/* .locals 2 */
/* .line 5 */
/* invoke-super {p0}, Lcom/android/server/input/config/BaseInputConfig;->getConfigNativePtr()J */
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Integer getConfigType ( ) {
/* .locals 1 */
/* .line 64 */
int v0 = 3; // const/4 v0, 0x3
} // .end method
public void setKnockDeviceProperty ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "property" # I */
/* .line 81 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockDeviceProperty:I */
/* .line 82 */
return;
} // .end method
public void setKnockFeatureState ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "state" # I */
/* .line 72 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockFeatureState:I */
/* .line 73 */
return;
} // .end method
public void setKnockInValidRect ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 0 */
/* .param p1, "left" # I */
/* .param p2, "top" # I */
/* .param p3, "right" # I */
/* .param p4, "bottom" # I */
/* .line 108 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectLeft:I */
/* .line 109 */
/* iput p2, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectTop:I */
/* .line 110 */
/* iput p3, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectRight:I */
/* .line 111 */
/* iput p4, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectBottom:I */
/* .line 112 */
return;
} // .end method
public void setKnockQuickMoveSpeed ( Float p0 ) {
/* .locals 0 */
/* .param p1, "quickMoveSpeed" # F */
/* .line 144 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mQuickMoveSpeed:F */
/* .line 145 */
return;
} // .end method
public void setKnockScoreThreshold ( Float p0 ) {
/* .locals 0 */
/* .param p1, "score" # F */
/* .line 128 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockScoreThreshold:F */
/* .line 129 */
return;
} // .end method
public void setKnockSensorThreshold ( Integer[] p0 ) {
/* .locals 0 */
/* .param p1, "sensorThreshold" # [I */
/* .line 148 */
this.mSensorThreshold = p1;
/* .line 149 */
return;
} // .end method
public void setKnockUseFrame ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "useFrame" # I */
/* .line 136 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mUseFrame:I */
/* .line 137 */
return;
} // .end method
public void setKnockValidRect ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 0 */
/* .param p1, "left" # I */
/* .param p2, "top" # I */
/* .param p3, "right" # I */
/* .param p4, "bottom" # I */
/* .line 93 */
/* iput p1, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectLeft:I */
/* .line 94 */
/* iput p2, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectTop:I */
/* .line 95 */
/* iput p3, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectRight:I */
/* .line 96 */
/* iput p4, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectBottom:I */
/* .line 97 */
return;
} // .end method
public void updateAlgorithmPath ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 120 */
this.mKnockAlgorithmPath = p1;
/* .line 121 */
return;
} // .end method
public void writeToParcel ( android.os.Parcel p0 ) {
/* .locals 1 */
/* .param p1, "dest" # Landroid/os/Parcel; */
/* .line 46 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockFeatureState:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 47 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockDeviceProperty:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 48 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectLeft:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 49 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectTop:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 50 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectRight:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 51 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockValidRectBottom:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 52 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectLeft:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 53 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectTop:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 54 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectRight:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 55 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockInValidRectBottom:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 56 */
v0 = this.mKnockAlgorithmPath;
(( android.os.Parcel ) p1 ).writeString8 ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString8(Ljava/lang/String;)V
/* .line 57 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mKnockScoreThreshold:F */
(( android.os.Parcel ) p1 ).writeFloat ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 58 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mUseFrame:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 59 */
/* iget v0, p0, Lcom/android/server/input/config/InputKnockConfig;->mQuickMoveSpeed:F */
(( android.os.Parcel ) p1 ).writeFloat ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 60 */
v0 = this.mSensorThreshold;
(( android.os.Parcel ) p1 ).writeIntArray ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 61 */
return;
} // .end method
