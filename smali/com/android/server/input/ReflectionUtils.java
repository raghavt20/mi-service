public class com.android.server.input.ReflectionUtils {
	 /* .source "ReflectionUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/input/ReflectionUtils$MethodEntry; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.android.server.input.ReflectionUtils$MethodEntry sMethodEntry;
private static volatile java.util.Map sMethodEntryMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lcom/android/server/input/ReflectionUtils$MethodEntry;", */
/* "Ljava/lang/reflect/Method;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.input.ReflectionUtils ( ) {
/* .locals 3 */
/* .line 15 */
/* new-instance v0, Lcom/android/server/input/ReflectionUtils$MethodEntry; */
/* invoke-direct {v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>()V */
/* .line 16 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 19 */
/* const-class v0, Lcom/android/server/input/InputManagerService; */
v1 = java.lang.Long.TYPE;
v2 = java.lang.Integer.TYPE;
/* filled-new-array {v1, v2}, [Ljava/lang/Class; */
final String v2 = "nativeSwitchTouchWorkMode"; // const-string v2, "nativeSwitchTouchWorkMode"
com.android.server.input.ReflectionUtils .initMethod ( v0,v2,v1 );
/* .line 21 */
/* const-class v0, Lcom/android/server/input/InputManagerService; */
v1 = java.lang.Long.TYPE;
v2 = java.lang.Integer.TYPE;
/* filled-new-array {v1, v2}, [Ljava/lang/Class; */
final String v2 = "nativeSetDebugInput"; // const-string v2, "nativeSetDebugInput"
com.android.server.input.ReflectionUtils .initMethod ( v0,v2,v1 );
/* .line 23 */
return;
} // .end method
public com.android.server.input.ReflectionUtils ( ) {
/* .locals 0 */
/* .line 13 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.lang.Object callPrivateMethod ( java.lang.Class p0, java.lang.Object p1, java.lang.String p2, java.lang.Object...p3 ) {
/* .locals 8 */
/* .param p0, "whichClass" # Ljava/lang/Class; */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .param p2, "methodName" # Ljava/lang/String; */
/* .param p3, "params" # [Ljava/lang/Object; */
/* .line 68 */
com.android.server.input.ReflectionUtils .getParameterTypes ( p3 );
/* .line 69 */
/* .local v0, "paramsClass":[Ljava/lang/Class; */
v1 = com.android.server.input.ReflectionUtils.sMethodEntry;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v1 ).setClassName ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v1 ).setMethodName ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v1 ).setParmasClass ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
/* .line 72 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v2 = v3 = com.android.server.input.ReflectionUtils.sMethodEntry;
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 73 */
	 v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
	 v4 = com.android.server.input.ReflectionUtils.sMethodEntry;
	 /* check-cast v2, Ljava/lang/reflect/Method; */
	 (( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
	 /* .line 74 */
	 v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
	 v3 = com.android.server.input.ReflectionUtils.sMethodEntry;
	 /* check-cast v2, Ljava/lang/reflect/Method; */
	 (( java.lang.reflect.Method ) v2 ).invoke ( p1, p3 ); // invoke-virtual {v2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* .line 76 */
} // :cond_0
(( java.lang.Class ) p0 ).getDeclaredMethod ( p2, v0 ); // invoke-virtual {p0, p2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 77 */
/* .local v2, "method":Ljava/lang/reflect/Method; */
/* if-nez v2, :cond_1 */
/* .line 78 */
/* .line 80 */
} // :cond_1
v4 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 81 */
try { // :try_start_1
v5 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* new-instance v6, Lcom/android/server/input/ReflectionUtils$MethodEntry; */
/* .line 82 */
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* invoke-direct {v6, v7, p2, v0, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V */
/* .line 81 */
/* .line 84 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 86 */
try { // :try_start_2
	 (( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
	 /* .line 87 */
	 (( java.lang.reflect.Method ) v2 ).invoke ( p1, p3 ); // invoke-virtual {v2, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* :try_end_2 */
	 /* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
	 /* .line 84 */
	 /* :catchall_0 */
	 /* move-exception v3 */
	 try { // :try_start_3
		 /* monitor-exit v4 */
		 /* :try_end_3 */
		 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
	 } // .end local v0 # "paramsClass":[Ljava/lang/Class;
} // .end local p0 # "whichClass":Ljava/lang/Class;
} // .end local p1 # "obj":Ljava/lang/Object;
} // .end local p2 # "methodName":Ljava/lang/String;
} // .end local p3 # "params":[Ljava/lang/Object;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 88 */
} // .end local v2 # "method":Ljava/lang/reflect/Method;
/* .restart local v0 # "paramsClass":[Ljava/lang/Class; */
/* .restart local p0 # "whichClass":Ljava/lang/Class; */
/* .restart local p1 # "obj":Ljava/lang/Object; */
/* .restart local p2 # "methodName":Ljava/lang/String; */
/* .restart local p3 # "params":[Ljava/lang/Object; */
/* :catch_0 */
/* move-exception v2 */
/* .line 89 */
/* .local v2, "ex":Ljava/lang/Exception; */
final String v3 = "InputFeatureReflection"; // const-string v3, "InputFeatureReflection"
final String v4 = "callPrivateMethod Fail!"; // const-string v4, "callPrivateMethod Fail!"
android.util.Slog .i ( v3,v4 );
/* .line 90 */
} // .end method
public static java.lang.Object callPrivateMethod ( java.lang.Object p0, java.lang.String p1, java.lang.Object...p2 ) {
/* .locals 8 */
/* .param p0, "obj" # Ljava/lang/Object; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "params" # [Ljava/lang/Object; */
/* .line 40 */
com.android.server.input.ReflectionUtils .getParameterTypes ( p2 );
/* .line 41 */
/* .local v0, "paramsClass":[Ljava/lang/Class; */
v1 = com.android.server.input.ReflectionUtils.sMethodEntry;
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v1 ).setClassName ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v1 ).setMethodName ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v1 ).setParmasClass ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
/* .line 44 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v2 = v3 = com.android.server.input.ReflectionUtils.sMethodEntry;
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 45 */
v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v4 = com.android.server.input.ReflectionUtils.sMethodEntry;
/* check-cast v2, Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 46 */
v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v3 = com.android.server.input.ReflectionUtils.sMethodEntry;
/* check-cast v2, Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).invoke ( p0, p2 ); // invoke-virtual {v2, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 48 */
} // :cond_0
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v2 ).getDeclaredMethod ( p1, v0 ); // invoke-virtual {v2, p1, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 49 */
/* .local v2, "method":Ljava/lang/reflect/Method; */
/* if-nez v2, :cond_1 */
/* .line 50 */
/* .line 52 */
} // :cond_1
v4 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* monitor-enter v4 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 53 */
try { // :try_start_1
v5 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* new-instance v6, Lcom/android/server/input/ReflectionUtils$MethodEntry; */
/* .line 54 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* invoke-direct {v6, v7, p1, v0, v2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V */
/* .line 53 */
/* .line 56 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 58 */
try { // :try_start_2
(( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 59 */
(( java.lang.reflect.Method ) v2 ).invoke ( p0, p2 ); // invoke-virtual {v2, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 56 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "paramsClass":[Ljava/lang/Class;
} // .end local p0 # "obj":Ljava/lang/Object;
} // .end local p1 # "methodName":Ljava/lang/String;
} // .end local p2 # "params":[Ljava/lang/Object;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 60 */
} // .end local v2 # "method":Ljava/lang/reflect/Method;
/* .restart local v0 # "paramsClass":[Ljava/lang/Class; */
/* .restart local p0 # "obj":Ljava/lang/Object; */
/* .restart local p1 # "methodName":Ljava/lang/String; */
/* .restart local p2 # "params":[Ljava/lang/Object; */
/* :catch_0 */
/* move-exception v2 */
/* .line 61 */
/* .local v2, "ex":Ljava/lang/Exception; */
final String v3 = "InputFeatureReflection"; // const-string v3, "InputFeatureReflection"
final String v4 = "callPrivateMethod Fail!"; // const-string v4, "callPrivateMethod Fail!"
android.util.Slog .i ( v3,v4 );
/* .line 62 */
} // .end method
public static java.lang.Object callStaticMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Class[] p2, java.lang.Object...p3 ) {
/* .locals 6 */
/* .param p0, "whichClass" # Ljava/lang/Class; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "paramsClass" # [Ljava/lang/Class; */
/* .param p3, "params" # [Ljava/lang/Object; */
/* .line 101 */
v0 = com.android.server.input.ReflectionUtils.sMethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v0 ).setClassName ( p0 ); // invoke-virtual {v0, p0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setClassName(Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v0 ).setMethodName ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setMethodName(Ljava/lang/String;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
(( com.android.server.input.ReflectionUtils$MethodEntry ) v0 ).setParmasClass ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/input/ReflectionUtils$MethodEntry;->setParmasClass([Ljava/lang/Class;)Lcom/android/server/input/ReflectionUtils$MethodEntry;
/* .line 104 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v1 = v2 = com.android.server.input.ReflectionUtils.sMethodEntry;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 105 */
v1 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v3 = com.android.server.input.ReflectionUtils.sMethodEntry;
/* check-cast v1, Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 106 */
v1 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
v2 = com.android.server.input.ReflectionUtils.sMethodEntry;
/* check-cast v1, Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, p3 ); // invoke-virtual {v1, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 108 */
} // :cond_0
(( java.lang.Class ) p0 ).getDeclaredMethod ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 109 */
/* .local v1, "method":Ljava/lang/reflect/Method; */
/* if-nez v1, :cond_1 */
/* .line 110 */
/* .line 112 */
} // :cond_1
v3 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* monitor-enter v3 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 113 */
try { // :try_start_1
v4 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* new-instance v5, Lcom/android/server/input/ReflectionUtils$MethodEntry; */
/* invoke-direct {v5, p0, p1, p2, v1}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V */
/* .line 114 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 116 */
try { // :try_start_2
(( java.lang.reflect.Method ) v1 ).setAccessible ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 117 */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, p3 ); // invoke-virtual {v1, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 114 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local p0 # "whichClass":Ljava/lang/Class;
} // .end local p1 # "methodName":Ljava/lang/String;
} // .end local p2 # "paramsClass":[Ljava/lang/Class;
} // .end local p3 # "params":[Ljava/lang/Object;
try { // :try_start_4
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 118 */
} // .end local v1 # "method":Ljava/lang/reflect/Method;
/* .restart local p0 # "whichClass":Ljava/lang/Class; */
/* .restart local p1 # "methodName":Ljava/lang/String; */
/* .restart local p2 # "paramsClass":[Ljava/lang/Class; */
/* .restart local p3 # "params":[Ljava/lang/Object; */
/* :catch_0 */
/* move-exception v1 */
/* .line 119 */
/* .local v1, "ex":Ljava/lang/Exception; */
final String v2 = "InputFeatureReflection"; // const-string v2, "InputFeatureReflection"
final String v3 = "callStaticMethod Fail!"; // const-string v3, "callStaticMethod Fail!"
android.util.Slog .i ( v2,v3 );
/* .line 120 */
} // .end method
public static java.lang.Object callStaticMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Object...p2 ) {
/* .locals 2 */
/* .param p0, "whichClass" # Ljava/lang/Class; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "params" # [Ljava/lang/Object; */
/* .line 95 */
com.android.server.input.ReflectionUtils .getParameterTypes ( p2 );
/* .line 96 */
/* .local v0, "paramsClass":[Ljava/lang/Class; */
com.android.server.input.ReflectionUtils .callStaticMethod ( p0,p1,v0,p2 );
} // .end method
private static java.lang.Class getParameterTypes ( java.lang.Object...p0 ) {
/* .locals 4 */
/* .param p0, "args" # [Ljava/lang/Object; */
/* .line 125 */
/* if-nez p0, :cond_0 */
/* .line 126 */
int v0 = 0; // const/4 v0, 0x0
/* .line 128 */
} // :cond_0
/* array-length v0, p0 */
/* new-array v0, v0, [Ljava/lang/Class; */
/* .line 129 */
/* .local v0, "parameterTypes":[Ljava/lang/Class; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
/* array-length v2, p0 */
/* .local v2, "j":I */
} // :goto_0
/* if-ge v1, v2, :cond_9 */
/* .line 130 */
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Integer; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 131 */
v3 = java.lang.Integer.TYPE;
/* aput-object v3, v0, v1 */
/* .line 132 */
} // :cond_1
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Byte; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 133 */
v3 = java.lang.Byte.TYPE;
/* aput-object v3, v0, v1 */
/* .line 134 */
} // :cond_2
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Short; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 135 */
v3 = java.lang.Short.TYPE;
/* aput-object v3, v0, v1 */
/* .line 136 */
} // :cond_3
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Float; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 137 */
v3 = java.lang.Float.TYPE;
/* aput-object v3, v0, v1 */
/* .line 138 */
} // :cond_4
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Double; */
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 139 */
v3 = java.lang.Double.TYPE;
/* aput-object v3, v0, v1 */
/* .line 140 */
} // :cond_5
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Character; */
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 141 */
v3 = java.lang.Character.TYPE;
/* aput-object v3, v0, v1 */
/* .line 142 */
} // :cond_6
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Long; */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 143 */
v3 = java.lang.Long.TYPE;
/* aput-object v3, v0, v1 */
/* .line 144 */
} // :cond_7
/* aget-object v3, p0, v1 */
/* instance-of v3, v3, Ljava/lang/Boolean; */
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 145 */
v3 = java.lang.Boolean.TYPE;
/* aput-object v3, v0, v1 */
/* .line 147 */
} // :cond_8
/* aget-object v3, p0, v1 */
(( java.lang.Object ) v3 ).getClass ( ); // invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* aput-object v3, v0, v1 */
/* .line 129 */
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 150 */
} // .end local v1 # "i":I
} // .end local v2 # "j":I
} // :cond_9
} // .end method
private static void initMethod ( java.lang.Class p0, java.lang.String p1, java.lang.Class[] p2 ) {
/* .locals 4 */
/* .param p0, "whichClass" # Ljava/lang/Class; */
/* .param p1, "methodName" # Ljava/lang/String; */
/* .param p2, "paramsClass" # [Ljava/lang/Class; */
/* .line 27 */
try { // :try_start_0
(( java.lang.Class ) p0 ).getDeclaredMethod ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 28 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 29 */
v1 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 30 */
try { // :try_start_1
v2 = com.android.server.input.ReflectionUtils.sMethodEntryMap;
/* new-instance v3, Lcom/android/server/input/ReflectionUtils$MethodEntry; */
/* invoke-direct {v3, p0, p1, p2, v0}, Lcom/android/server/input/ReflectionUtils$MethodEntry;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;Ljava/lang/reflect/Method;)V */
/* .line 31 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "whichClass":Ljava/lang/Class;
} // .end local p1 # "methodName":Ljava/lang/String;
} // .end local p2 # "paramsClass":[Ljava/lang/Class;
try { // :try_start_2
/* throw v2 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 35 */
} // .end local v0 # "method":Ljava/lang/reflect/Method;
/* .restart local p0 # "whichClass":Ljava/lang/Class; */
/* .restart local p1 # "methodName":Ljava/lang/String; */
/* .restart local p2 # "paramsClass":[Ljava/lang/Class; */
} // :cond_0
} // :goto_0
/* .line 33 */
/* :catch_0 */
/* move-exception v0 */
/* .line 34 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "InputFeatureReflection"; // const-string v1, "InputFeatureReflection"
final String v2 = "Init method Fail!"; // const-string v2, "Init method Fail!"
android.util.Slog .i ( v1,v2 );
/* .line 36 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
