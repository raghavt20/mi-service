public class com.android.server.input.InputShellCommandStubImpl implements com.android.server.input.InputShellCommandStub {
	 /* .source "InputShellCommandStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String INVALID_ARGUMENTS;
	 /* # instance fields */
	 private com.android.server.input.InputShellCommand mInputShellCommand;
	 /* # direct methods */
	 public com.android.server.input.InputShellCommandStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private java.lang.String getNextArg ( ) {
		 /* .locals 1 */
		 /* .line 67 */
		 v0 = this.mInputShellCommand;
		 (( com.android.server.input.InputShellCommand ) v0 ).getNextArg ( ); // invoke-virtual {v0}, Lcom/android/server/input/InputShellCommand;->getNextArg()Ljava/lang/String;
	 } // .end method
	 private java.lang.String getNextArgRequired ( ) {
		 /* .locals 1 */
		 /* .line 71 */
		 v0 = this.mInputShellCommand;
		 (( com.android.server.input.InputShellCommand ) v0 ).getNextArgRequired ( ); // invoke-virtual {v0}, Lcom/android/server/input/InputShellCommand;->getNextArgRequired()Ljava/lang/String;
	 } // .end method
	 private void handleDefaultCommands ( java.lang.String p0 ) {
		 /* .locals 1 */
		 /* .param p1, "arg" # Ljava/lang/String; */
		 /* .line 62 */
		 v0 = this.mInputShellCommand;
		 (( com.android.server.input.InputShellCommand ) v0 ).handleDefaultCommands ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/InputShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
		 /* .line 63 */
		 return;
	 } // .end method
	 private void sendSwipe ( ) {
		 /* .locals 16 */
		 /* .line 38 */
		 /* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String; */
		 v0 = 		 java.lang.Integer .parseInt ( v0 );
		 /* .line 39 */
		 /* .local v0, "x1":I */
		 /* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String; */
		 v8 = 		 java.lang.Integer .parseInt ( v1 );
		 /* .line 40 */
		 /* .local v8, "y1":I */
		 /* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String; */
		 v9 = 		 java.lang.Integer .parseInt ( v1 );
		 /* .line 41 */
		 /* .local v9, "x2":I */
		 /* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String; */
		 v10 = 		 java.lang.Integer .parseInt ( v1 );
		 /* .line 42 */
		 /* .local v10, "y2":I */
		 /* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArg()Ljava/lang/String; */
		 /* .line 43 */
		 /* .local v11, "durationArg":Ljava/lang/String; */
		 if ( v11 != null) { // if-eqz v11, :cond_0
			 v1 = 			 java.lang.Integer .parseInt ( v11 );
		 } // :cond_0
		 int v1 = -1; // const/4 v1, -0x1
		 /* .line 44 */
		 /* .local v1, "duration":I */
	 } // :goto_0
	 /* if-gez v1, :cond_1 */
	 /* .line 45 */
	 /* const/16 v1, 0x12c */
	 /* move v12, v1 */
	 /* .line 44 */
} // :cond_1
/* move v12, v1 */
/* .line 47 */
} // .end local v1 # "duration":I
/* .local v12, "duration":I */
} // :goto_1
/* const-class v1, Lcom/android/server/input/MiuiInputManagerInternal; */
/* .line 48 */
com.android.server.LocalServices .getService ( v1 );
/* move-object v13, v1 */
/* check-cast v13, Lcom/android/server/input/MiuiInputManagerInternal; */
/* .line 49 */
/* .local v13, "miuiInputManagerInternal":Lcom/android/server/input/MiuiInputManagerInternal; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArg()Ljava/lang/String; */
/* .line 50 */
/* .local v14, "everyDelayTimeArg":Ljava/lang/String; */
/* if-nez v14, :cond_2 */
/* .line 51 */
/* move-object v1, v13 */
/* move v2, v0 */
/* move v3, v8 */
/* move v4, v9 */
/* move v5, v10 */
/* move v6, v12 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/input/MiuiInputManagerInternal;->swipe(IIIII)Z */
/* .line 52 */
return;
/* .line 54 */
} // :cond_2
v1 = java.lang.Integer .parseInt ( v14 );
/* .line 55 */
/* .local v1, "everyDelayTime":I */
/* if-gez v1, :cond_3 */
/* .line 56 */
int v1 = 5; // const/4 v1, 0x5
/* move v15, v1 */
/* .line 55 */
} // :cond_3
/* move v15, v1 */
/* .line 58 */
} // .end local v1 # "everyDelayTime":I
/* .local v15, "everyDelayTime":I */
} // :goto_2
/* move-object v1, v13 */
/* move v2, v0 */
/* move v3, v8 */
/* move v4, v9 */
/* move v5, v10 */
/* move v6, v12 */
/* move v7, v15 */
/* invoke-virtual/range {v1 ..v7}, Lcom/android/server/input/MiuiInputManagerInternal;->swipe(IIIIII)Z */
/* .line 59 */
return;
} // .end method
/* # virtual methods */
public void init ( com.android.server.input.InputShellCommand p0 ) {
/* .locals 0 */
/* .param p1, "inputShellCommand" # Lcom/android/server/input/InputShellCommand; */
/* .line 13 */
this.mInputShellCommand = p1;
/* .line 14 */
return;
} // .end method
public Boolean onCommand ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 18 */
/* move-object v0, p1 */
/* .line 19 */
/* .local v0, "arg":Ljava/lang/String; */
final String v1 = "miui"; // const-string v1, "miui"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 21 */
int v1 = 0; // const/4 v1, 0x0
/* .line 24 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/input/InputShellCommandStubImpl;->getNextArgRequired()Ljava/lang/String; */
/* .line 26 */
try { // :try_start_0
/* const-string/jumbo v1, "swipe" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 27 */
/* invoke-direct {p0}, Lcom/android/server/input/InputShellCommandStubImpl;->sendSwipe()V */
/* .line 29 */
} // :cond_1
/* invoke-direct {p0, v0}, Lcom/android/server/input/InputShellCommandStubImpl;->handleDefaultCommands(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 33 */
} // :goto_0
/* nop */
/* .line 34 */
int v1 = 1; // const/4 v1, 0x1
/* .line 31 */
/* :catch_0 */
/* move-exception v1 */
/* .line 32 */
/* .local v1, "ex":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error: Invalid arguments for command: "; // const-string v4, "Error: Invalid arguments for command: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
} // .end method
