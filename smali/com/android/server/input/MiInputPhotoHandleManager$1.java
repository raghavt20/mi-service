class com.android.server.input.MiInputPhotoHandleManager$1 extends android.content.BroadcastReceiver {
	 /* .source "MiInputPhotoHandleManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/input/MiInputPhotoHandleManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.MiInputPhotoHandleManager this$0; //synthetic
/* # direct methods */
 com.android.server.input.MiInputPhotoHandleManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/MiInputPhotoHandleManager; */
/* .line 137 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 140 */
final String v0 = "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"; // const-string v0, "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"
int v1 = 0; // const/4 v1, 0x0
v0 = (( android.content.Intent ) p2 ).getIntExtra ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 141 */
/* .local v0, "connectionStatus":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "receive handle status,connection="; // const-string v3, "receive handle status,connection="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiInputPhotoHandleManager"; // const-string v3, "MiInputPhotoHandleManager"
android.util.Slog .d ( v3,v2 );
/* .line 143 */
v2 = this.this$0;
v2 = com.android.server.input.MiInputPhotoHandleManager .-$$Nest$fgetmPhotoHandleConnectionStatus ( v2 );
/* if-eq v2, v0, :cond_0 */
/* .line 144 */
v2 = this.this$0;
com.android.server.input.MiInputPhotoHandleManager .-$$Nest$fputmPhotoHandleConnectionStatus ( v2,v0 );
/* .line 145 */
final String v2 = "pid"; // const-string v2, "pid"
v2 = (( android.content.Intent ) p2 ).getIntExtra ( v2, v1 ); // invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 146 */
/* .local v2, "pid":I */
/* const-string/jumbo v3, "vid" */
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v3, v1 ); // invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 147 */
/* .local v1, "vid":I */
v3 = this.this$0;
com.android.server.input.MiInputPhotoHandleManager .-$$Nest$fgetmHandler ( v3 );
android.os.Message .obtain ( v3,v0,v2,v1 );
/* .line 148 */
/* .local v3, "message":Landroid/os/Message; */
v4 = this.this$0;
com.android.server.input.MiInputPhotoHandleManager .-$$Nest$fgetmHandler ( v4 );
(( android.os.Handler ) v4 ).sendMessage ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 151 */
} // .end local v1 # "vid":I
} // .end local v2 # "pid":I
} // .end local v3 # "message":Landroid/os/Message;
} // :cond_0
return;
} // .end method
