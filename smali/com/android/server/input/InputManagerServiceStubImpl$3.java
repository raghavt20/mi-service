class com.android.server.input.InputManagerServiceStubImpl$3 extends android.database.ContentObserver {
	 /* .source "InputManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/input/InputManagerServiceStubImpl;->registerMiuiOptimizationObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.input.InputManagerServiceStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.input.InputManagerServiceStubImpl$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/input/InputManagerServiceStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 327 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 330 */
/* nop */
/* .line 331 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 330 */
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
final String v2 = "persist.sys.miui_optimization"; // const-string v2, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v2,v0 );
/* xor-int/2addr v0, v1 */
/* .line 332 */
/* .local v0, "isCtsMode":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ctsMode is:"; // const-string v3, "ctsMode is:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "InputManagerServiceStubImpl"; // const-string v3, "InputManagerServiceStubImpl"
android.util.Slog .i ( v3,v2 );
/* .line 333 */
v2 = this.this$0;
com.android.server.input.InputManagerServiceStubImpl .-$$Nest$msetCtsMode ( v2,v0 );
/* .line 334 */
v2 = this.this$0;
com.android.server.input.InputManagerServiceStubImpl .-$$Nest$fgetmContext ( v2 );
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v2 );
/* .line 336 */
/* .local v2, "miuiKeyInterceptExtend":Lcom/android/server/policy/MiuiKeyInterceptExtend; */
/* if-nez v0, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( com.android.server.policy.MiuiKeyInterceptExtend ) v2 ).setKeyboardShortcutEnable ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setKeyboardShortcutEnable(Z)V
/* .line 337 */
(( com.android.server.policy.MiuiKeyInterceptExtend ) v2 ).setAospKeyboardShortcutEnable ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->setAospKeyboardShortcutEnable(Z)V
/* .line 338 */
return;
} // .end method
