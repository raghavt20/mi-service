.class final Lcom/android/server/input/MiuiInputManagerService$LocalService;
.super Lcom/android/server/input/MiuiInputManagerInternal;
.source "MiuiInputManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiuiInputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/MiuiInputManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/input/MiuiInputManagerService;)V
    .locals 0

    .line 717
    iput-object p1, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-direct {p0}, Lcom/android/server/input/MiuiInputManagerInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/input/MiuiInputManagerService;Lcom/android/server/input/MiuiInputManagerService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/MiuiInputManagerService$LocalService;-><init>(Lcom/android/server/input/MiuiInputManagerService;)V

    return-void
.end method


# virtual methods
.method public doubleTap(III)Z
    .locals 1
    .param p1, "tapX"    # I
    .param p2, "tapY"    # I
    .param p3, "duration"    # I

    .line 734
    invoke-static {}, Lcom/miui/server/input/util/MiuiInputShellCommand;->getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/input/util/MiuiInputShellCommand;->doubleTapGenerator(III)Z

    move-result v0

    return v0
.end method

.method public getCurrentDisplayViewPorts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;"
        }
    .end annotation

    .line 803
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmDisplayViewports(Lcom/android/server/input/MiuiInputManagerService;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPointerDisplayId()I
    .locals 1

    .line 798
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmPointerDisplayId(Lcom/android/server/input/MiuiInputManagerService;)I

    move-result v0

    return v0
.end method

.method public hideMouseCursor()V
    .locals 1

    .line 749
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/NativeMiuiInputManagerService;->hideMouseCursor()V

    .line 750
    return-void
.end method

.method public injectMotionEvent(Landroid/view/MotionEvent;I)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "mode"    # I

    .line 739
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/NativeMiuiInputManagerService;->injectMotionEvent(Landroid/view/MotionEvent;I)V

    .line 740
    return-void
.end method

.method public notifyPhotoHandleConnectionStatus(ZI)V
    .locals 1
    .param p1, "connection"    # Z
    .param p2, "deviceId"    # I

    .line 774
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/NativeMiuiInputManagerService;->notifyPhotoHandleConnectionStatus(ZI)V

    .line 775
    return-void
.end method

.method public obtainLaserPointerController()Lcom/miui/server/input/stylus/laser/PointerControllerInterface;
    .locals 1

    .line 769
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerService;->obtainLaserPointerController()Lcom/miui/server/input/stylus/laser/PointerControllerInterface;

    move-result-object v0

    return-object v0
.end method

.method public setCustomPointerIcon(Landroid/view/PointerIcon;)V
    .locals 2
    .param p1, "customPointerIcon"    # Landroid/view/PointerIcon;

    .line 791
    invoke-static {}, Lcom/android/server/input/InputManagerServiceStub;->getInstance()Lcom/android/server/input/InputManagerServiceStub;

    move-result-object v0

    instance-of v1, v0, Lcom/android/server/input/InputManagerServiceStubImpl;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/server/input/InputManagerServiceStubImpl;

    .line 792
    .local v0, "stub":Lcom/android/server/input/InputManagerServiceStubImpl;
    invoke-virtual {v0, p1}, Lcom/android/server/input/InputManagerServiceStubImpl;->setCustomPointerIcon(Landroid/view/PointerIcon;)V

    .line 794
    .end local v0    # "stub":Lcom/android/server/input/InputManagerServiceStubImpl;
    :cond_0
    return-void
.end method

.method public setDeviceShareListener(ILjava/io/FileDescriptor;I)V
    .locals 1
    .param p1, "pid"    # I
    .param p2, "fileDescriptor"    # Ljava/io/FileDescriptor;
    .param p3, "flags"    # I

    .line 754
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/input/NativeMiuiInputManagerService;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V

    .line 755
    return-void
.end method

.method public setDimState(Z)V
    .locals 1
    .param p1, "isDimming"    # Z

    .line 779
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/NativeMiuiInputManagerService;->setDimState(Z)V

    .line 780
    return-void
.end method

.method public setInputConfig(IJ)V
    .locals 1
    .param p1, "configType"    # I
    .param p2, "configNativePtr"    # J

    .line 744
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/input/NativeMiuiInputManagerService;->setInputConfig(IJ)V

    .line 745
    return-void
.end method

.method public setPointerIconType(I)V
    .locals 2
    .param p1, "iconType"    # I

    .line 784
    invoke-static {}, Lcom/android/server/input/InputManagerServiceStub;->getInstance()Lcom/android/server/input/InputManagerServiceStub;

    move-result-object v0

    instance-of v1, v0, Lcom/android/server/input/InputManagerServiceStubImpl;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/server/input/InputManagerServiceStubImpl;

    .line 785
    .local v0, "stub":Lcom/android/server/input/InputManagerServiceStubImpl;
    invoke-virtual {v0, p1}, Lcom/android/server/input/InputManagerServiceStubImpl;->setPointerIconType(I)V

    .line 787
    .end local v0    # "stub":Lcom/android/server/input/InputManagerServiceStubImpl;
    :cond_0
    return-void
.end method

.method public setScreenState(Z)V
    .locals 1
    .param p1, "isScreenOn"    # Z

    .line 764
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0, p1}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$msetScreenState(Lcom/android/server/input/MiuiInputManagerService;Z)V

    .line 765
    return-void
.end method

.method public setTouchpadButtonState(IZ)V
    .locals 1
    .param p1, "deviceId"    # I
    .param p2, "isDown"    # Z

    .line 759
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$LocalService;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-static {v0}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$fgetmNative(Lcom/android/server/input/MiuiInputManagerService;)Lcom/android/server/input/NativeMiuiInputManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/input/NativeMiuiInputManagerService;->setTouchpadButtonState(IZ)V

    .line 760
    return-void
.end method

.method public swipe(IIIII)Z
    .locals 6
    .param p1, "downX"    # I
    .param p2, "downY"    # I
    .param p3, "upX"    # I
    .param p4, "upY"    # I
    .param p5, "duration"    # I

    .line 719
    invoke-static {}, Lcom/miui/server/input/util/MiuiInputShellCommand;->getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/input/util/MiuiInputShellCommand;->swipeGenerator(IIIII)Z

    move-result v0

    return v0
.end method

.method public swipe(IIIIII)Z
    .locals 7
    .param p1, "downX"    # I
    .param p2, "downY"    # I
    .param p3, "upX"    # I
    .param p4, "upY"    # I
    .param p5, "duration"    # I
    .param p6, "everyDelayTime"    # I

    .line 725
    invoke-static {}, Lcom/miui/server/input/util/MiuiInputShellCommand;->getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/miui/server/input/util/MiuiInputShellCommand;->swipeGenerator(IIIIII)Z

    move-result v0

    return v0
.end method

.method public tap(II)Z
    .locals 1
    .param p1, "tapX"    # I
    .param p2, "tapY"    # I

    .line 730
    invoke-static {}, Lcom/miui/server/input/util/MiuiInputShellCommand;->getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/input/util/MiuiInputShellCommand;->tapGenerator(II)Z

    move-result v0

    return v0
.end method
