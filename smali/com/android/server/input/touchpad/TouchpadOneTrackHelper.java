public class com.android.server.input.touchpad.TouchpadOneTrackHelper {
	 /* .source "TouchpadOneTrackHelper.java" */
	 /* # static fields */
	 private static final java.lang.String DOWN_EVENT_NAME;
	 private static final java.lang.String DOWN_EVENT_TRACK_TIP;
	 private static final java.lang.String EVENT_NAME;
	 private static Integer MIN_EVENTS_REPORT;
	 private static final java.lang.String TAG;
	 private static final java.lang.String TRACK_TIP;
	 private static Integer TYPE_DOWN;
	 private static Integer TYPE_UP;
	 private static final java.lang.String UP_EVENT_NAME;
	 private static final java.lang.String UP_EVENT_TRACK_TIP;
	 public static final java.lang.String X_COORDINATE;
	 public static final java.lang.String Y_COORDINATE;
	 private static volatile com.android.server.input.touchpad.TouchpadOneTrackHelper sInstance;
	 /* # instance fields */
	 java.util.List mCachedEvents;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
/* # direct methods */
static com.android.server.input.touchpad.TouchpadOneTrackHelper ( ) {
/* .locals 1 */
/* .line 29 */
int v0 = 0; // const/4 v0, 0x0
/* .line 30 */
int v0 = 1; // const/4 v0, 0x1
/* .line 32 */
/* const/16 v0, 0x64 */
return;
} // .end method
private com.android.server.input.touchpad.TouchpadOneTrackHelper ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 36 */
this.mContext = p1;
/* .line 37 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCachedEvents = v0;
/* .line 38 */
return;
} // .end method
public static com.android.server.input.touchpad.TouchpadOneTrackHelper getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 41 */
v0 = com.android.server.input.touchpad.TouchpadOneTrackHelper.sInstance;
/* if-nez v0, :cond_1 */
/* .line 42 */
/* const-class v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper; */
/* monitor-enter v0 */
/* .line 43 */
try { // :try_start_0
	 v1 = com.android.server.input.touchpad.TouchpadOneTrackHelper.sInstance;
	 /* if-nez v1, :cond_0 */
	 /* .line 44 */
	 /* new-instance v1, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;-><init>(Landroid/content/Context;)V */
	 /* .line 46 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 48 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.touchpad.TouchpadOneTrackHelper.sInstance;
} // .end method
private org.json.JSONObject getTrackJSONObject ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "x" # I */
/* .param p3, "y" # I */
/* .line 52 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 54 */
/* .local v0, "obj":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
/* if-ne p1, v2, :cond_0 */
final String v2 = "down"; // const-string v2, "down"
} // :cond_0
/* const-string/jumbo v2, "up" */
} // :goto_0
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 55 */
/* const-string/jumbo v1, "tip" */
/* if-ne p1, v2, :cond_1 */
final String v2 = "899.8.0.1.32956"; // const-string v2, "899.8.0.1.32956"
} // :cond_1
final String v2 = "899.8.0.1.32957"; // const-string v2, "899.8.0.1.32957"
} // :goto_1
(( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 56 */
/* const-string/jumbo v1, "x_coordinate" */
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 57 */
/* const-string/jumbo v1, "y_coordinate" */
(( org.json.JSONObject ) v0 ).put ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 60 */
/* .line 58 */
/* :catch_0 */
/* move-exception v1 */
/* .line 59 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 61 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_2
} // .end method
/* # virtual methods */
public void trackTouchpadEvent ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "x" # I */
/* .param p3, "y" # I */
/* .line 65 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->getTrackJSONObject(III)Lorg/json/JSONObject; */
/* .line 66 */
/* .local v0, "obj":Lorg/json/JSONObject; */
v1 = this.mCachedEvents;
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 67 */
v1 = v1 = this.mCachedEvents;
/* if-lt v1, v2, :cond_0 */
/* .line 68 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.mCachedEvents;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 69 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v2 );
(( com.android.server.input.InputOneTrackUtil ) v2 ).trackTouchpadEvents ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackTouchpadEvents(Ljava/util/List;)V
/* .line 70 */
v2 = this.mCachedEvents;
/* .line 72 */
} // .end local v1 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_0
return;
} // .end method
