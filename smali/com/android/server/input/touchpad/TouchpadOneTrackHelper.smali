.class public Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;
.super Ljava/lang/Object;
.source "TouchpadOneTrackHelper.java"


# static fields
.field private static final DOWN_EVENT_NAME:Ljava/lang/String; = "down"

.field private static final DOWN_EVENT_TRACK_TIP:Ljava/lang/String; = "899.8.0.1.32956"

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static MIN_EVENTS_REPORT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "TouchpadOneTrackHelper"

.field private static final TRACK_TIP:Ljava/lang/String; = "tip"

.field private static TYPE_DOWN:I = 0x0

.field private static TYPE_UP:I = 0x0

.field private static final UP_EVENT_NAME:Ljava/lang/String; = "up"

.field private static final UP_EVENT_TRACK_TIP:Ljava/lang/String; = "899.8.0.1.32957"

.field public static final X_COORDINATE:Ljava/lang/String; = "x_coordinate"

.field public static final Y_COORDINATE:Ljava/lang/String; = "y_coordinate"

.field private static volatile sInstance:Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;


# instance fields
.field mCachedEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const/4 v0, 0x0

    sput v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->TYPE_DOWN:I

    .line 30
    const/4 v0, 0x1

    sput v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->TYPE_UP:I

    .line 32
    const/16 v0, 0x64

    sput v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->MIN_EVENTS_REPORT:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mCachedEvents:Ljava/util/List;

    .line 38
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 41
    sget-object v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->sInstance:Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    if-nez v0, :cond_1

    .line 42
    const-class v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    monitor-enter v0

    .line 43
    :try_start_0
    sget-object v1, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->sInstance:Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    if-nez v1, :cond_0

    .line 44
    new-instance v1, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    invoke-direct {v1, p0}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->sInstance:Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    .line 46
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 48
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->sInstance:Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;

    return-object v0
.end method

.method private getTrackJSONObject(III)Lorg/json/JSONObject;
    .locals 3
    .param p1, "type"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .line 52
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 54
    .local v0, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    sget v2, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->TYPE_DOWN:I

    if-ne p1, v2, :cond_0

    const-string v2, "down"

    goto :goto_0

    :cond_0
    const-string/jumbo v2, "up"

    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 55
    const-string/jumbo v1, "tip"

    sget v2, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->TYPE_DOWN:I

    if-ne p1, v2, :cond_1

    const-string v2, "899.8.0.1.32956"

    goto :goto_1

    :cond_1
    const-string v2, "899.8.0.1.32957"

    :goto_1
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    const-string/jumbo v1, "x_coordinate"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 57
    const-string/jumbo v1, "y_coordinate"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    goto :goto_2

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 61
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_2
    return-object v0
.end method


# virtual methods
.method public trackTouchpadEvent(III)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->getTrackJSONObject(III)Lorg/json/JSONObject;

    move-result-object v0

    .line 66
    .local v0, "obj":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mCachedEvents:Ljava/util/List;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v1, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mCachedEvents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sget v2, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->MIN_EVENTS_REPORT:I

    if-lt v1, v2, :cond_0

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mCachedEvents:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 69
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/input/InputOneTrackUtil;->trackTouchpadEvents(Ljava/util/List;)V

    .line 70
    iget-object v2, p0, Lcom/android/server/input/touchpad/TouchpadOneTrackHelper;->mCachedEvents:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 72
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    return-void
.end method
