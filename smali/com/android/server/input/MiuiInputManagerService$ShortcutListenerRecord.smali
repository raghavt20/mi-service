.class Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;
.super Ljava/lang/Object;
.source "MiuiInputManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/input/MiuiInputManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShortcutListenerRecord"
.end annotation


# instance fields
.field private final mChangedListener:Lmiui/hardware/input/IShortcutSettingsChangedListener;

.field private final mPid:I

.field final synthetic this$0:Lcom/android/server/input/MiuiInputManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/input/MiuiInputManagerService;Lmiui/hardware/input/IShortcutSettingsChangedListener;I)V
    .locals 0
    .param p2, "settingsChangedListener"    # Lmiui/hardware/input/IShortcutSettingsChangedListener;
    .param p3, "pid"    # I

    .line 430
    iput-object p1, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 431
    iput-object p2, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mChangedListener:Lmiui/hardware/input/IShortcutSettingsChangedListener;

    .line 432
    iput p3, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mPid:I

    .line 433
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 437
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->this$0:Lcom/android/server/input/MiuiInputManagerService;

    iget v1, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/android/server/input/MiuiInputManagerService;->-$$Nest$mremoveListenerFromSystem(Lcom/android/server/input/MiuiInputManagerService;I)V

    .line 438
    return-void
.end method

.method public notifyShortcutSettingsChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 442
    :try_start_0
    iget-object v0, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mChangedListener:Lmiui/hardware/input/IShortcutSettingsChangedListener;

    invoke-interface {v0, p1, p2}, Lmiui/hardware/input/IShortcutSettingsChangedListener;->onSettingsChanged(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    goto :goto_0

    .line 443
    :catch_0
    move-exception v0

    .line 444
    .local v0, "ex":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to notify process "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->mPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " that Settings Changed, assuming it died."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiInputManagerService"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 446
    invoke-virtual {p0}, Lcom/android/server/input/MiuiInputManagerService$ShortcutListenerRecord;->binderDied()V

    .line 448
    .end local v0    # "ex":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
