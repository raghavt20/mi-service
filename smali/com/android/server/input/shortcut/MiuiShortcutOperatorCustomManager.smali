.class public Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;
.super Ljava/lang/Object;
.source "MiuiShortcutOperatorCustomManager.java"


# static fields
.field private static final RO_MIUI_CUSTOMIZED_REGION:Ljava/lang/String; = "ro.miui.customized.region"

.field private static final RO_PRODUCT_DEVICE:Ljava/lang/String; = "ro.product.mod_device"

.field private static final SOFT_BACK_CUSTOMIZED_REGION:Ljava/lang/String; = "jp_sb"

.field private static final SOFT_BACK_OPERATOR_DEVICE_NAME_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mHandler:Landroid/os/Handler;

.field private final mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    const-string v0, "lilac_jp_sb_global"

    invoke-static {v0}, Ljava/util/List;->of(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->SOFT_BACK_OPERATOR_DEVICE_NAME_LIST:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mContext:Landroid/content/Context;

    .line 40
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mHandler:Landroid/os/Handler;

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mContentResolver:Landroid/content/ContentResolver;

    .line 42
    invoke-static {p1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 43
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 46
    sget-object v0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->sMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    if-nez v0, :cond_1

    .line 47
    const-class v0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    monitor-enter v0

    .line 48
    :try_start_0
    sget-object v1, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->sMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    if-nez v1, :cond_0

    .line 49
    new-instance v1, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    invoke-direct {v1, p0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->sMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    .line 52
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 54
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->sMiuiShortcutOperatorCustomManager:Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;

    return-object v0
.end method

.method private isSoftBankCustom()Z
    .locals 3

    .line 103
    sget-object v0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->SOFT_BACK_OPERATOR_DEVICE_NAME_LIST:Ljava/util/List;

    .line 104
    const-string v1, "ro.product.mod_device"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "ro.miui.customized.region"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    const-string v1, "jp_sb"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0
.end method

.method private setDefaultCustomFunction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "currentFunction"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "function"    # Ljava/lang/String;
    .param p4, "global"    # I
    .param p5, "region"    # Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0, p4, p5}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isLegalData(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 71
    const-string v0, "emergency_gesture_sound_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-direct {p0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->isSoftBankCustom()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I

    invoke-static {v0, p2, v1, v2}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public hasOperatorCustomFunctionForMiuiRule(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "currentFunction"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 89
    const-string v0, "double_click_power_key"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->isKDDIOperator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "au_pay"

    iget v2, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I

    invoke-static {v0, p2, v1, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 92
    const/4 v0, 0x1

    return v0

    .line 94
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public initShortcut()V
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "emergency_gesture_sound_enabled"

    iget v2, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "emergency_gesture_sound_enabled"

    const/4 v6, 0x0

    const/4 v7, 0x1

    const-string v8, ""

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->setDefaultCustomFunction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 63
    return-void
.end method

.method public isKDDIOperator()Z
    .locals 1

    .line 114
    sget-boolean v0, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI:Z

    return v0
.end method

.method public onUserSwitch(I)V
    .locals 0
    .param p1, "newUserId"    # I

    .line 118
    iput p1, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I

    .line 119
    return-void
.end method
