.class public Lcom/android/server/input/shortcut/MiuiGestureRuleManager;
.super Ljava/lang/Object;
.source "MiuiGestureRuleManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiGestureRuleManager"

.field private static volatile sGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGestureRuleHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/policy/MiuiGestureRule;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    .line 22
    iput-object p1, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mContext:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mHandler:Landroid/os/Handler;

    .line 24
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/os/Handler;)Lcom/android/server/input/shortcut/MiuiGestureRuleManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "handler"    # Landroid/os/Handler;

    .line 27
    sget-object v0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->sGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    if-nez v0, :cond_1

    .line 28
    const-class v0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    monitor-enter v0

    .line 29
    :try_start_0
    sget-object v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->sGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    if-nez v1, :cond_0

    .line 30
    new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    sput-object v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->sGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    .line 32
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 35
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->sGestureManager:Lcom/android/server/input/shortcut/MiuiGestureRuleManager;

    return-object v0
.end method

.method static synthetic lambda$initGestureRule$2(Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 0
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "miuiGestureRule"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 79
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiGestureRule;->init()V

    return-void
.end method

.method static synthetic lambda$onUserSwitch$0(IZLjava/lang/String;Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 0
    .param p0, "currentUserId"    # I
    .param p1, "isNewUser"    # Z
    .param p2, "k"    # Ljava/lang/String;
    .param p3, "v"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 59
    invoke-virtual {p3, p0, p1}, Lcom/android/server/policy/MiuiGestureRule;->onUserSwitch(IZ)V

    return-void
.end method

.method static synthetic lambda$resetDefaultFunction$1(Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 2
    .param p0, "k"    # Ljava/lang/String;
    .param p1, "v"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 63
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiGestureRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    return-void
.end method


# virtual methods
.method public addRule(Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "miuiGestureRule"    # Lcom/android/server/policy/MiuiGestureRule;

    .line 39
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 67
    const-string v0, "MiuiGestureRuleManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 70
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 71
    .local v1, "miuiGestureRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;>;"
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 72
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 73
    const-string v2, "="

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 74
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/MiuiGestureRule;

    invoke-virtual {v2}, Lcom/android/server/policy/MiuiGestureRule;->getFunction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 75
    .end local v1    # "miuiGestureRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;>;"
    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiGestureRule;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiGestureRule;->getFunction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGestureManager(Ljava/lang/String;)Lcom/android/server/policy/MiuiGestureRule;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiGestureRule;

    return-object v0
.end method

.method public hasActionInGestureRuleMap(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public initGestureRule()V
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda1;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 80
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 2
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 59
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p2}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda0;-><init>(IZ)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 60
    return-void
.end method

.method public removeRule(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public resetDefaultFunction()V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;->mGestureRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 64
    return-void
.end method
