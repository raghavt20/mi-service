public class com.android.server.input.shortcut.ShortcutOneTrackHelper {
	 /* .source "ShortcutOneTrackHelper.java" */
	 /* # static fields */
	 private static final java.lang.String DEVICE_TYPE_FLIP;
	 private static final java.lang.String DEVICE_TYPE_FOLD;
	 private static final java.lang.String DEVICE_TYPE_PAD;
	 private static final java.lang.String DEVICE_TYPE_PHONE;
	 private static final java.lang.String EVENT_NAME;
	 private static final java.lang.String GLOBAL_PRIVATE_KEY_ID;
	 private static final java.lang.String GLOBAL_PROJECT_ID;
	 private static final java.lang.String GLOBAL_TOPIC;
	 private static final java.lang.String SCREEN_TYPE;
	 private static final java.lang.String SCREEN_TYPE_EXTERNAL_SCREEN;
	 private static final java.lang.String SCREEN_TYPE_INNER_SCREEN;
	 private static final java.util.List SHORTCUT_TRACK_FOR_ONE_TRACK;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String SHORTCUT_TRACK_MODE_TYPE;
private static final java.lang.String SHORTCUT_TRACK_PHONE_TYPE;
private static final java.lang.String SHORTCUT_TRACK_TIP;
private static final java.lang.String SHORTCUT_TRACK_TIP_STATUS;
private static final java.lang.String SHORTCUT_TRACK_TIP_TRIGGER;
public static final java.lang.String STATUS_TRACK_TYPE;
private static final java.lang.String TAG;
public static final java.lang.String TRIGGER_TRACK_TYPE;
private static volatile com.android.server.input.shortcut.ShortcutOneTrackHelper sInstance;
/* # instance fields */
private final android.app.AlarmManager$OnAlarmListener mAlarmListener;
private final android.app.AlarmManager mAlarmManager;
private final android.content.Context mContext;
private Boolean mFoldStatus;
private final android.os.Handler mHandler;
private java.util.List mShortcutSupportTrackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mtrackShortcutEventStatus ( com.android.server.input.shortcut.ShortcutOneTrackHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventStatus()V */
return;
} // .end method
static com.android.server.input.shortcut.ShortcutOneTrackHelper ( ) {
/* .locals 3 */
/* .line 31 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "cn"; // const-string v1, "cn"
final String v2 = "ru"; // const-string v2, "ru"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
/* .line 32 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 31 */
return;
} // .end method
private com.android.server.input.shortcut.ShortcutOneTrackHelper ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 61 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 129 */
/* new-instance v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper$1;-><init>(Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;)V */
this.mAlarmListener = v0;
/* .line 62 */
this.mContext = p1;
/* .line 63 */
com.android.server.input.MiuiInputThread .getHandler ( );
this.mHandler = v0;
/* .line 64 */
final String v0 = "alarm"; // const-string v0, "alarm"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.mAlarmManager = v0;
/* .line 65 */
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->initShortcutTrackList()V */
/* .line 66 */
return;
} // .end method
private java.lang.String getDeviceType ( ) {
/* .locals 1 */
/* .line 239 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFoldDevice ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 240 */
final String v0 = "fold"; // const-string v0, "fold"
/* .line 241 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 242 */
final String v0 = "pad"; // const-string v0, "pad"
/* .line 243 */
} // :cond_1
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 244 */
final String v0 = "flip"; // const-string v0, "flip"
/* .line 246 */
} // :cond_2
final String v0 = "phone"; // const-string v0, "phone"
} // .end method
private java.lang.String getFunctionByAction ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 252 */
/* const-string/jumbo v0, "volumekey_launch_camera" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = -2; // const/4 v1, -0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 253 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$System .getIntForUser ( v0,p1,v2,v1 );
/* .line 255 */
/* .local v0, "functionStatus":I */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getValidFunctionForValueIsIntType(Ljava/lang/String;I)Ljava/lang/String; */
/* .line 256 */
/* .local v0, "function":Ljava/lang/String; */
/* .line 257 */
} // .end local v0 # "function":Ljava/lang/String;
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v0,p1,v1 );
/* .line 259 */
/* .restart local v0 # "function":Ljava/lang/String; */
final String v1 = "none"; // const-string v1, "none"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 260 */
/* const-string/jumbo v0, "user_close" */
/* .line 261 */
} // :cond_1
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 262 */
final String v0 = "default_close"; // const-string v0, "default_close"
/* .line 265 */
} // :cond_2
} // :goto_0
} // .end method
public static com.android.server.input.shortcut.ShortcutOneTrackHelper getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 106 */
v0 = com.android.server.input.shortcut.ShortcutOneTrackHelper.sInstance;
/* if-nez v0, :cond_1 */
/* .line 107 */
/* const-class v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper; */
/* monitor-enter v0 */
/* .line 108 */
try { // :try_start_0
v1 = com.android.server.input.shortcut.ShortcutOneTrackHelper.sInstance;
/* if-nez v1, :cond_0 */
/* .line 109 */
/* new-instance v1, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;-><init>(Landroid/content/Context;)V */
/* .line 111 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 113 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.shortcut.ShortcutOneTrackHelper.sInstance;
} // .end method
private java.lang.String getShortcutTrackAction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 191 */
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v0 = "double_click_power"; // const-string v0, "double_click_power"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_1 */
final String v0 = "double_click_volume_down"; // const-string v0, "double_click_volume_down"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_2 */
final String v0 = "power_double_tap"; // const-string v0, "power_double_tap"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 198 */
/* .line 196 */
/* :pswitch_0 */
/* const-string/jumbo v0, "volumekey_launch_camera" */
/* .line 194 */
/* :pswitch_1 */
final String v0 = "double_click_power_key"; // const-string v0, "double_click_power_key"
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x65d77e51 -> :sswitch_2 */
/* -0x61111e7e -> :sswitch_1 */
/* -0x1d3bebe0 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private org.json.JSONObject getTrackJSONObject ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "type" # Ljava/lang/String; */
/* .line 213 */
final String v0 = "pad"; // const-string v0, "pad"
/* const-string/jumbo v1, "trigger" */
/* const-string/jumbo v2, "status" */
/* new-instance v3, Lorg/json/JSONObject; */
/* invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V */
/* .line 215 */
/* .local v3, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
v4 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* const-string/jumbo v5, "tip" */
final String v6 = "EVENT_NAME"; // const-string v6, "EVENT_NAME"
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 216 */
try { // :try_start_1
(( org.json.JSONObject ) v3 ).put ( v6, v2 ); // invoke-virtual {v3, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 217 */
final String v1 = "1257.0.0.0.28747"; // const-string v1, "1257.0.0.0.28747"
(( org.json.JSONObject ) v3 ).put ( v5, v1 ); // invoke-virtual {v3, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 218 */
} // :cond_0
v2 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 219 */
(( org.json.JSONObject ) v3 ).put ( v6, v1 ); // invoke-virtual {v3, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 220 */
final String v1 = "1257.0.0.0.28748"; // const-string v1, "1257.0.0.0.28748"
(( org.json.JSONObject ) v3 ).put ( v5, v1 ); // invoke-virtual {v3, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 223 */
} // :cond_1
} // :goto_0
v1 = com.android.server.input.shortcut.ShortcutOneTrackHelper .isShortcutTrackForOneTrack ( );
/* if-nez v1, :cond_2 */
/* .line 224 */
final String v1 = "PROJECT_ID"; // const-string v1, "PROJECT_ID"
final String v2 = "gesture-shortcuts-c097f"; // const-string v2, "gesture-shortcuts-c097f"
(( org.json.JSONObject ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 225 */
final String v1 = "TOPIC"; // const-string v1, "TOPIC"
/* const-string/jumbo v2, "topic_ods_pubsub_event_di_31000401650" */
(( org.json.JSONObject ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 226 */
final String v1 = "PRIVATE_KEY_ID"; // const-string v1, "PRIVATE_KEY_ID"
final String v2 = "60d438821b8068f4edd8d1a3d85339eb65402dc6"; // const-string v2, "60d438821b8068f4edd8d1a3d85339eb65402dc6"
(( org.json.JSONObject ) v3 ).put ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 229 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getDeviceType()Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 230 */
/* .local v1, "isPad":Z */
final String v2 = "model_type"; // const-string v2, "model_type"
if ( v1 != null) { // if-eqz v1, :cond_3
} // :cond_3
final String v0 = "phone"; // const-string v0, "phone"
} // :goto_1
(( org.json.JSONObject ) v3 ).put ( v2, v0 ); // invoke-virtual {v3, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 231 */
final String v0 = "phone_type"; // const-string v0, "phone_type"
if ( v1 != null) { // if-eqz v1, :cond_4
int v2 = 0; // const/4 v2, 0x0
} // :cond_4
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getDeviceType()Ljava/lang/String; */
} // :goto_2
(( org.json.JSONObject ) v3 ).put ( v0, v2 ); // invoke-virtual {v3, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 234 */
/* nop */
} // .end local v1 # "isPad":Z
/* .line 232 */
/* :catch_0 */
/* move-exception v0 */
/* .line 233 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 235 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_3
} // .end method
private java.lang.String getValidFunctionForValueIsIntType ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # I */
/* .line 269 */
/* const-string/jumbo v0, "volumekey_launch_camera" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 270 */
/* if-ne p2, v1, :cond_0 */
/* .line 271 */
final String v0 = "launch_camera"; // const-string v0, "launch_camera"
/* .line 272 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_4 */
/* .line 273 */
final String v0 = "launch_camera_and_take_photo"; // const-string v0, "launch_camera_and_take_photo"
/* .line 275 */
} // :cond_1
final String v0 = "screen_key_press_app_switch"; // const-string v0, "screen_key_press_app_switch"
v2 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 276 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$System .getIntForUser ( v2,v0,v1,v3 );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move v0, v1 */
/* .line 280 */
/* .local v0, "pressToAppSwitch":Z */
if ( v0 != null) { // if-eqz v0, :cond_3
final String v1 = "launch_recents"; // const-string v1, "launch_recents"
} // :cond_3
/* const-string/jumbo v1, "show_menu" */
} // :goto_1
/* .line 283 */
} // .end local v0 # "pressToAppSwitch":Z
} // :cond_4
/* if-nez p2, :cond_5 */
/* .line 284 */
/* const-string/jumbo v0, "user_close" */
/* .line 286 */
} // :cond_5
final String v0 = "default_close"; // const-string v0, "default_close"
} // .end method
private void initShortcutTrackList ( ) {
/* .locals 28 */
/* .line 69 */
/* move-object/from16 v0, p0 */
v1 = this.mShortcutSupportTrackList;
/* if-nez v1, :cond_0 */
/* .line 70 */
/* new-instance v1, Ljava/util/ArrayList; */
final String v2 = "long_press_power_key"; // const-string v2, "long_press_power_key"
final String v3 = "long_press_home_key"; // const-string v3, "long_press_home_key"
final String v4 = "long_press_menu_key"; // const-string v4, "long_press_menu_key"
final String v5 = "long_press_back_key"; // const-string v5, "long_press_back_key"
final String v6 = "screen_key_press_app_switch"; // const-string v6, "screen_key_press_app_switch"
final String v7 = "double_click_power_key"; // const-string v7, "double_click_power_key"
/* const-string/jumbo v8, "three_gesture_down" */
/* const-string/jumbo v9, "three_gesture_long_press" */
/* const-string/jumbo v10, "volumekey_launch_camera" */
final String v11 = "back_double_tap"; // const-string v11, "back_double_tap"
final String v12 = "back_triple_tap"; // const-string v12, "back_triple_tap"
final String v13 = "double_knock"; // const-string v13, "double_knock"
final String v14 = "knock_gesture_v"; // const-string v14, "knock_gesture_v"
final String v15 = "knock_slide_shape"; // const-string v15, "knock_slide_shape"
final String v16 = "knock_long_press_horizontal_slid"; // const-string v16, "knock_long_press_horizontal_slid"
final String v17 = "fingerprint_double_tap"; // const-string v17, "fingerprint_double_tap"
final String v18 = "key_combination_power_volume_down"; // const-string v18, "key_combination_power_volume_down"
final String v19 = "key_combination_power_volume_up"; // const-string v19, "key_combination_power_volume_up"
final String v20 = "key_combination_power_home"; // const-string v20, "key_combination_power_home"
final String v21 = "key_combination_power_menu"; // const-string v21, "key_combination_power_menu"
final String v22 = "key_combination_power_back"; // const-string v22, "key_combination_power_back"
final String v23 = "long_press_camera_key"; // const-string v23, "long_press_camera_key"
final String v24 = "power_double_tap"; // const-string v24, "power_double_tap"
final String v25 = "double_click_power"; // const-string v25, "double_click_power"
final String v26 = "double_click_volume_down"; // const-string v26, "double_click_volume_down"
/* const-string/jumbo v27, "very_long_press_power" */
/* filled-new-array/range {v2 ..v27}, [Ljava/lang/String; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mShortcutSupportTrackList = v1;
/* .line 99 */
} // :cond_0
return;
} // .end method
public static Boolean isShortcutTrackForOneTrack ( ) {
/* .locals 2 */
/* .line 202 */
v0 = com.android.server.input.shortcut.ShortcutOneTrackHelper.SHORTCUT_TRACK_FOR_ONE_TRACK;
v0 = v1 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_REGION;
} // .end method
private Boolean needSkipTrackByAction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 157 */
final String v0 = "key_combination_power_volume_up"; // const-string v0, "key_combination_power_volume_up"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
v0 = this.mContext;
/* .line 158 */
com.android.server.input.MiInputPhotoHandleManager .getInstance ( v0 );
v0 = (( com.android.server.input.MiInputPhotoHandleManager ) v0 ).isPhotoHandleHasConnected ( ); // invoke-virtual {v0}, Lcom/android/server/input/MiInputPhotoHandleManager;->isPhotoHandleHasConnected()Z
/* if-nez v0, :cond_0 */
/* .line 159 */
final String v0 = "long_press_camera_key"; // const-string v0, "long_press_camera_key"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
} // :cond_0
/* nop */
/* .line 160 */
/* const-string/jumbo v0, "very_long_press_power" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 157 */
} // :goto_1
} // .end method
private void trackShortcutEventStatus ( ) {
/* .locals 5 */
/* .line 142 */
/* const-string/jumbo v0, "status" */
/* invoke-direct {p0, v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 144 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
v1 = this.mShortcutSupportTrackList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 145 */
/* .local v2, "action":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->needSkipTrackByAction(Ljava/lang/String;)Z */
/* if-nez v3, :cond_0 */
/* .line 146 */
/* invoke-direct {p0, v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getShortcutTrackAction(Ljava/lang/String;)Ljava/lang/String; */
/* .line 147 */
/* .local v3, "shortcutTrackAction":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getFunctionByAction(Ljava/lang/String;)Ljava/lang/String; */
(( org.json.JSONObject ) v0 ).put ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 149 */
} // .end local v2 # "action":Ljava/lang/String;
} // .end local v3 # "shortcutTrackAction":Ljava/lang/String;
} // :cond_0
/* .line 152 */
} // :cond_1
/* .line 150 */
/* :catch_0 */
/* move-exception v1 */
/* .line 151 */
/* .local v1, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
/* .line 153 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_1
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackShortcutEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackShortcutEvent(Ljava/lang/String;)V
/* .line 154 */
return;
} // .end method
/* # virtual methods */
public void notifyFoldStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "folded" # Z */
/* .line 290 */
/* iput-boolean p1, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mFoldStatus:Z */
/* .line 291 */
return;
} // .end method
public void setUploadShortcutAlarm ( Boolean p0 ) {
/* .locals 11 */
/* .param p1, "init" # Z */
/* .line 122 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 123 */
/* .local v0, "nowTime":J */
if ( p1 != null) { // if-eqz p1, :cond_0
/* const-wide/32 v2, 0x1b7740 */
/* .line 124 */
} // :cond_0
/* const-wide/32 v2, 0x5265c00 */
} // :goto_0
/* add-long/2addr v2, v0 */
/* .line 125 */
/* .local v2, "nextTime":J */
v4 = this.mAlarmManager;
int v5 = 1; // const/4 v5, 0x1
/* const-string/jumbo v8, "upload_shortcut_status" */
v9 = this.mAlarmListener;
v10 = this.mHandler;
/* move-wide v6, v2 */
/* invoke-virtual/range {v4 ..v10}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 127 */
return;
} // .end method
public Boolean supportTrackForCurrentAction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 102 */
v0 = v0 = this.mShortcutSupportTrackList;
} // .end method
public void trackShortcutEventTrigger ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 170 */
v0 = (( com.android.server.input.shortcut.ShortcutOneTrackHelper ) p0 ).supportTrackForCurrentAction ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->supportTrackForCurrentAction(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 171 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getShortcutTrackAction(Ljava/lang/String;)Ljava/lang/String; */
/* .line 172 */
/* .local v0, "shortcutTriggerAction":Ljava/lang/String; */
/* const-string/jumbo v1, "trigger" */
/* invoke-direct {p0, v1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 174 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
v2 = miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 175 */
/* const-string/jumbo v2, "screen_type" */
/* iget-boolean v3, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mFoldStatus:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "external_screen"; // const-string v3, "external_screen"
} // :cond_0
final String v3 = "inner_screen"; // const-string v3, "inner_screen"
} // :goto_0
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 177 */
} // :cond_1
(( org.json.JSONObject ) v1 ).put ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 180 */
/* .line 178 */
/* :catch_0 */
/* move-exception v2 */
/* .line 179 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
/* .line 181 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_1
v2 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v2 );
(( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v2 ).trackShortcutEvent ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackShortcutEvent(Ljava/lang/String;)V
/* .line 183 */
} // .end local v0 # "shortcutTriggerAction":Ljava/lang/String;
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
} // :cond_2
return;
} // .end method
