public class com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager {
	 /* .source "MiuiShortcutOperatorCustomManager.java" */
	 /* # static fields */
	 private static final java.lang.String RO_MIUI_CUSTOMIZED_REGION;
	 private static final java.lang.String RO_PRODUCT_DEVICE;
	 private static final java.lang.String SOFT_BACK_CUSTOMIZED_REGION;
	 private static final java.util.List SOFT_BACK_OPERATOR_DEVICE_NAME_LIST;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static volatile com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager sMiuiShortcutOperatorCustomManager;
/* # instance fields */
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private Integer mCurrentUserId;
private final android.os.Handler mHandler;
private final com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
/* # direct methods */
static com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ( ) {
/* .locals 1 */
/* .line 25 */
final String v0 = "lilac_jp_sb_global"; // const-string v0, "lilac_jp_sb_global"
java.util.List .of ( v0 );
return;
} // .end method
private com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 38 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 39 */
this.mContext = p1;
/* .line 40 */
com.android.server.input.MiuiInputThread .getHandler ( );
this.mHandler = v0;
/* .line 41 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 42 */
com.android.server.policy.MiuiShortcutTriggerHelper .getInstance ( p1 );
this.mMiuiShortcutTriggerHelper = v0;
/* .line 43 */
return;
} // .end method
public static com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 46 */
v0 = com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager.sMiuiShortcutOperatorCustomManager;
/* if-nez v0, :cond_1 */
/* .line 47 */
/* const-class v0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager; */
/* monitor-enter v0 */
/* .line 48 */
try { // :try_start_0
	 v1 = com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager.sMiuiShortcutOperatorCustomManager;
	 /* if-nez v1, :cond_0 */
	 /* .line 49 */
	 /* new-instance v1, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;-><init>(Landroid/content/Context;)V */
	 /* .line 52 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 54 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager.sMiuiShortcutOperatorCustomManager;
} // .end method
private Boolean isSoftBankCustom ( ) {
/* .locals 3 */
/* .line 103 */
v0 = com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager.SOFT_BACK_OPERATOR_DEVICE_NAME_LIST;
/* .line 104 */
final String v1 = "ro.product.mod_device"; // const-string v1, "ro.product.mod_device"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
v0 = /* .line 103 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 105 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
android.os.SystemProperties .get ( v0,v2 );
/* .line 104 */
final String v1 = "jp_sb"; // const-string v1, "jp_sb"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 103 */
} // :goto_0
} // .end method
private void setDefaultCustomFunction ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 3 */
/* .param p1, "currentFunction" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "function" # Ljava/lang/String; */
/* .param p4, "global" # I */
/* .param p5, "region" # Ljava/lang/String; */
/* .line 70 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isLegalData ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isLegalData(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez p1, :cond_0 */
/* .line 71 */
final String v0 = "emergency_gesture_sound_enabled"; // const-string v0, "emergency_gesture_sound_enabled"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->isSoftBankCustom()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 74 */
	 v0 = this.mContentResolver;
	 int v1 = 1; // const/4 v1, 0x1
	 /* iget v2, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I */
	 android.provider.Settings$Secure .putIntForUser ( v0,p2,v1,v2 );
	 /* .line 80 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Boolean hasOperatorCustomFunctionForMiuiRule ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "currentFunction" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 89 */
final String v0 = "double_click_power_key"; // const-string v0, "double_click_power_key"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = 	 (( com.android.server.input.shortcut.MiuiShortcutOperatorCustomManager ) p0 ).isKDDIOperator ( ); // invoke-virtual {p0}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->isKDDIOperator()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 90 */
		 v0 = this.mContentResolver;
		 final String v1 = "au_pay"; // const-string v1, "au_pay"
		 /* iget v2, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I */
		 android.provider.Settings$System .putStringForUser ( v0,p2,v1,v2 );
		 /* .line 92 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 94 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public void initShortcut ( ) {
	 /* .locals 9 */
	 /* .line 59 */
	 v0 = this.mContentResolver;
	 final String v1 = "emergency_gesture_sound_enabled"; // const-string v1, "emergency_gesture_sound_enabled"
	 /* iget v2, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I */
	 android.provider.Settings$Secure .getStringForUser ( v0,v1,v2 );
	 final String v5 = "emergency_gesture_sound_enabled"; // const-string v5, "emergency_gesture_sound_enabled"
	 int v6 = 0; // const/4 v6, 0x0
	 int v7 = 1; // const/4 v7, 0x1
	 final String v8 = ""; // const-string v8, ""
	 /* move-object v3, p0 */
	 /* invoke-direct/range {v3 ..v8}, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->setDefaultCustomFunction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V */
	 /* .line 63 */
	 return;
} // .end method
public Boolean isKDDIOperator ( ) {
	 /* .locals 1 */
	 /* .line 114 */
	 /* sget-boolean v0, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI:Z */
} // .end method
public void onUserSwitch ( Integer p0 ) {
	 /* .locals 0 */
	 /* .param p1, "newUserId" # I */
	 /* .line 118 */
	 /* iput p1, p0, Lcom/android/server/input/shortcut/MiuiShortcutOperatorCustomManager;->mCurrentUserId:I */
	 /* .line 119 */
	 return;
} // .end method
