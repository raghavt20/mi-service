.class public Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;
.super Ljava/lang/Object;
.source "ShortcutOneTrackHelper.java"


# static fields
.field private static final DEVICE_TYPE_FLIP:Ljava/lang/String; = "flip"

.field private static final DEVICE_TYPE_FOLD:Ljava/lang/String; = "fold"

.field private static final DEVICE_TYPE_PAD:Ljava/lang/String; = "pad"

.field private static final DEVICE_TYPE_PHONE:Ljava/lang/String; = "phone"

.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final GLOBAL_PRIVATE_KEY_ID:Ljava/lang/String; = "60d438821b8068f4edd8d1a3d85339eb65402dc6"

.field private static final GLOBAL_PROJECT_ID:Ljava/lang/String; = "gesture-shortcuts-c097f"

.field private static final GLOBAL_TOPIC:Ljava/lang/String; = "topic_ods_pubsub_event_di_31000401650"

.field private static final SCREEN_TYPE:Ljava/lang/String; = "screen_type"

.field private static final SCREEN_TYPE_EXTERNAL_SCREEN:Ljava/lang/String; = "external_screen"

.field private static final SCREEN_TYPE_INNER_SCREEN:Ljava/lang/String; = "inner_screen"

.field private static final SHORTCUT_TRACK_FOR_ONE_TRACK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SHORTCUT_TRACK_MODE_TYPE:Ljava/lang/String; = "model_type"

.field private static final SHORTCUT_TRACK_PHONE_TYPE:Ljava/lang/String; = "phone_type"

.field private static final SHORTCUT_TRACK_TIP:Ljava/lang/String; = "tip"

.field private static final SHORTCUT_TRACK_TIP_STATUS:Ljava/lang/String; = "1257.0.0.0.28747"

.field private static final SHORTCUT_TRACK_TIP_TRIGGER:Ljava/lang/String; = "1257.0.0.0.28748"

.field public static final STATUS_TRACK_TYPE:Ljava/lang/String; = "status"

.field private static final TAG:Ljava/lang/String; = "ShortcutOneTrackHelper"

.field public static final TRIGGER_TRACK_TYPE:Ljava/lang/String; = "trigger"

.field private static volatile sInstance:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;


# instance fields
.field private final mAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mContext:Landroid/content/Context;

.field private mFoldStatus:Z

.field private final mHandler:Landroid/os/Handler;

.field private mShortcutSupportTrackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mtrackShortcutEventStatus(Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventStatus()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "cn"

    const-string v2, "ru"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->SHORTCUT_TRACK_FOR_ONE_TRACK:Ljava/util/List;

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper$1;-><init>(Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;)V

    iput-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    .line 62
    iput-object p1, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    .line 63
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mHandler:Landroid/os/Handler;

    .line 64
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mAlarmManager:Landroid/app/AlarmManager;

    .line 65
    invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->initShortcutTrackList()V

    .line 66
    return-void
.end method

.method private getDeviceType()Ljava/lang/String;
    .locals 1

    .line 239
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string v0, "fold"

    return-object v0

    .line 241
    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_1

    .line 242
    const-string v0, "pad"

    return-object v0

    .line 243
    :cond_1
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    const-string v0, "flip"

    return-object v0

    .line 246
    :cond_2
    const-string v0, "phone"

    return-object v0
.end method

.method private getFunctionByAction(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .line 252
    const-string/jumbo v0, "volumekey_launch_camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, -0x2

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, -0x1

    invoke-static {v0, p1, v2, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 255
    .local v0, "functionStatus":I
    invoke-direct {p0, p1, v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getValidFunctionForValueIsIntType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "function":Ljava/lang/String;
    goto :goto_0

    .line 257
    .end local v0    # "function":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 259
    .restart local v0    # "function":Ljava/lang/String;
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    const-string/jumbo v0, "user_close"

    goto :goto_0

    .line 261
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 262
    const-string v0, "default_close"

    .line 265
    :cond_2
    :goto_0
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 106
    sget-object v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->sInstance:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    if-nez v0, :cond_1

    .line 107
    const-class v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    monitor-enter v0

    .line 108
    :try_start_0
    sget-object v1, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->sInstance:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    if-nez v1, :cond_0

    .line 109
    new-instance v1, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    invoke-direct {v1, p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->sInstance:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    .line 111
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 113
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->sInstance:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    return-object v0
.end method

.method private getShortcutTrackAction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 191
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "double_click_power"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v0, "double_click_volume_down"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v0, "power_double_tap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 198
    return-object p1

    .line 196
    :pswitch_0
    const-string/jumbo v0, "volumekey_launch_camera"

    return-object v0

    .line 194
    :pswitch_1
    const-string v0, "double_click_power_key"

    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x65d77e51 -> :sswitch_2
        -0x61111e7e -> :sswitch_1
        -0x1d3bebe0 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getTrackJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 7
    .param p1, "type"    # Ljava/lang/String;

    .line 213
    const-string v0, "pad"

    const-string/jumbo v1, "trigger"

    const-string/jumbo v2, "status"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 215
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v5, "tip"

    const-string v6, "EVENT_NAME"

    if-eqz v4, :cond_0

    .line 216
    :try_start_1
    invoke-virtual {v3, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 217
    const-string v1, "1257.0.0.0.28747"

    invoke-virtual {v3, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 219
    invoke-virtual {v3, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 220
    const-string v1, "1257.0.0.0.28748"

    invoke-virtual {v3, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 223
    :cond_1
    :goto_0
    invoke-static {}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->isShortcutTrackForOneTrack()Z

    move-result v1

    if-nez v1, :cond_2

    .line 224
    const-string v1, "PROJECT_ID"

    const-string v2, "gesture-shortcuts-c097f"

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 225
    const-string v1, "TOPIC"

    const-string/jumbo v2, "topic_ods_pubsub_event_di_31000401650"

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 226
    const-string v1, "PRIVATE_KEY_ID"

    const-string v2, "60d438821b8068f4edd8d1a3d85339eb65402dc6"

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 229
    :cond_2
    invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getDeviceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 230
    .local v1, "isPad":Z
    const-string v2, "model_type"

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const-string v0, "phone"

    :goto_1
    invoke-virtual {v3, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    const-string v0, "phone_type"

    if-eqz v1, :cond_4

    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getDeviceType()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v3, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 234
    nop

    .end local v1    # "isPad":Z
    goto :goto_3

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 235
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    return-object v3
.end method

.method private getValidFunctionForValueIsIntType(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # I

    .line 269
    const-string/jumbo v0, "volumekey_launch_camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 270
    if-ne p2, v1, :cond_0

    .line 271
    const-string v0, "launch_camera"

    return-object v0

    .line 272
    :cond_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_4

    .line 273
    const-string v0, "launch_camera_and_take_photo"

    return-object v0

    .line 275
    :cond_1
    const-string v0, "screen_key_press_app_switch"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 276
    iget-object v2, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, -0x2

    invoke-static {v2, v0, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    .line 280
    .local v0, "pressToAppSwitch":Z
    if-eqz v0, :cond_3

    const-string v1, "launch_recents"

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "show_menu"

    :goto_1
    return-object v1

    .line 283
    .end local v0    # "pressToAppSwitch":Z
    :cond_4
    if-nez p2, :cond_5

    .line 284
    const-string/jumbo v0, "user_close"

    return-object v0

    .line 286
    :cond_5
    const-string v0, "default_close"

    return-object v0
.end method

.method private initShortcutTrackList()V
    .locals 28

    .line 69
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mShortcutSupportTrackList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    const-string v2, "long_press_power_key"

    const-string v3, "long_press_home_key"

    const-string v4, "long_press_menu_key"

    const-string v5, "long_press_back_key"

    const-string v6, "screen_key_press_app_switch"

    const-string v7, "double_click_power_key"

    const-string/jumbo v8, "three_gesture_down"

    const-string/jumbo v9, "three_gesture_long_press"

    const-string/jumbo v10, "volumekey_launch_camera"

    const-string v11, "back_double_tap"

    const-string v12, "back_triple_tap"

    const-string v13, "double_knock"

    const-string v14, "knock_gesture_v"

    const-string v15, "knock_slide_shape"

    const-string v16, "knock_long_press_horizontal_slid"

    const-string v17, "fingerprint_double_tap"

    const-string v18, "key_combination_power_volume_down"

    const-string v19, "key_combination_power_volume_up"

    const-string v20, "key_combination_power_home"

    const-string v21, "key_combination_power_menu"

    const-string v22, "key_combination_power_back"

    const-string v23, "long_press_camera_key"

    const-string v24, "power_double_tap"

    const-string v25, "double_click_power"

    const-string v26, "double_click_volume_down"

    const-string/jumbo v27, "very_long_press_power"

    filled-new-array/range {v2 .. v27}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mShortcutSupportTrackList:Ljava/util/List;

    .line 99
    :cond_0
    return-void
.end method

.method public static isShortcutTrackForOneTrack()Z
    .locals 2

    .line 202
    sget-object v0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->SHORTCUT_TRACK_FOR_ONE_TRACK:Ljava/util/List;

    sget-object v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private needSkipTrackByAction(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 157
    const-string v0, "key_combination_power_volume_up"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    .line 158
    invoke-static {v0}, Lcom/android/server/input/MiInputPhotoHandleManager;->getInstance(Landroid/content/Context;)Lcom/android/server/input/MiInputPhotoHandleManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/input/MiInputPhotoHandleManager;->isPhotoHandleHasConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const-string v0, "long_press_camera_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    nop

    .line 160
    const-string/jumbo v0, "very_long_press_power"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 157
    :goto_1
    return v0
.end method

.method private trackShortcutEventStatus()V
    .locals 5

    .line 142
    const-string/jumbo v0, "status"

    invoke-direct {p0, v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 144
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    iget-object v1, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mShortcutSupportTrackList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 145
    .local v2, "action":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->needSkipTrackByAction(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 146
    invoke-direct {p0, v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getShortcutTrackAction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 147
    .local v3, "shortcutTrackAction":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getFunctionByAction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    .end local v2    # "action":Ljava/lang/String;
    .end local v3    # "shortcutTrackAction":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 152
    :cond_1
    goto :goto_1

    .line 150
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 153
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_1
    iget-object v1, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackShortcutEvent(Ljava/lang/String;)V

    .line 154
    return-void
.end method


# virtual methods
.method public notifyFoldStatus(Z)V
    .locals 0
    .param p1, "folded"    # Z

    .line 290
    iput-boolean p1, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mFoldStatus:Z

    .line 291
    return-void
.end method

.method public setUploadShortcutAlarm(Z)V
    .locals 11
    .param p1, "init"    # Z

    .line 122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 123
    .local v0, "nowTime":J
    if-eqz p1, :cond_0

    const-wide/32 v2, 0x1b7740

    goto :goto_0

    .line 124
    :cond_0
    const-wide/32 v2, 0x5265c00

    :goto_0
    add-long/2addr v2, v0

    .line 125
    .local v2, "nextTime":J
    iget-object v4, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v5, 0x1

    const-string/jumbo v8, "upload_shortcut_status"

    iget-object v9, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mAlarmListener:Landroid/app/AlarmManager$OnAlarmListener;

    iget-object v10, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mHandler:Landroid/os/Handler;

    move-wide v6, v2

    invoke-virtual/range {v4 .. v10}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 127
    return-void
.end method

.method public supportTrackForCurrentAction(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 102
    iget-object v0, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mShortcutSupportTrackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 170
    invoke-virtual {p0, p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->supportTrackForCurrentAction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getShortcutTrackAction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "shortcutTriggerAction":Ljava/lang/String;
    const-string/jumbo v1, "trigger"

    invoke-direct {p0, v1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 174
    .local v1, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    const-string/jumbo v2, "screen_type"

    iget-boolean v3, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mFoldStatus:Z

    if-eqz v3, :cond_0

    const-string v3, "external_screen"

    goto :goto_0

    :cond_0
    const-string v3, "inner_screen"

    :goto_0
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 177
    :cond_1
    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    goto :goto_1

    .line 178
    :catch_0
    move-exception v2

    .line 179
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 181
    .end local v2    # "e":Lorg/json/JSONException;
    :goto_1
    iget-object v2, p0, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/input/InputOneTrackUtil;->trackShortcutEvent(Ljava/lang/String;)V

    .line 183
    .end local v0    # "shortcutTriggerAction":Ljava/lang/String;
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    return-void
.end method
