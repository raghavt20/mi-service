.class public Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;
.super Lcom/android/server/policy/MiuiSingleKeyRule;
.source "VolumeDownKeyRule.java"


# static fields
.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_CLOSE:I = 0x0

.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA:I = 0x1

.field public static final DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA_AND_TAKE_PHOTO:I = 0x2


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mMaxCountMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field private mPolicyFlag:I

.field private final mPowerManager:Landroid/os/PowerManager;

.field private final mVolumeKeyWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;


# direct methods
.method public static synthetic $r8$lambda$M7s7N7sST1B0wXXhrJei-MZOeQc(Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->lambda$updatePolicyFlag$1(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$WIdoVnY8sfteerWO2vzr2-G4vJE(Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p4, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .param p5, "currentUserId"    # I

    .line 40
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V

    .line 41
    iput-object p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mHandler:Landroid/os/Handler;

    .line 43
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMaxCountMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mMaxCountMap:Ljava/util/Map;

    .line 44
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mPowerManager:Landroid/os/PowerManager;

    .line 45
    const/4 v1, 0x1

    const-string v2, "MiuiKeyShortcutTrigger.mVolumeKeyWakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mVolumeKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 47
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 48
    iput-object p4, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 49
    return-void
.end method

.method private getAudioManager()Landroid/media/AudioManager;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mAudioManager:Landroid/media/AudioManager;

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method private isAudioActive()Z
    .locals 6

    .line 118
    const/4 v0, 0x0

    .line 119
    .local v0, "active":Z
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 120
    .local v1, "mode":I
    const-string v2, "isAudioActive():"

    if-lez v1, :cond_0

    const/4 v3, 0x7

    if-ge v1, v3, :cond_0

    .line 121
    const/4 v0, 0x1

    .line 122
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 123
    return v0

    .line 125
    :cond_0
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v3

    .line 126
    .local v3, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_3

    .line 127
    const/4 v5, 0x1

    if-ne v5, v4, :cond_1

    .line 129
    goto :goto_1

    .line 131
    :cond_1
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v0

    .line 132
    if-eqz v0, :cond_2

    .line 133
    goto :goto_2

    .line 126
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 136
    .end local v4    # "i":I
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 137
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 139
    :cond_4
    return v0
.end method

.method private isEnableLaunchCamera()Z
    .locals 5

    .line 70
    const-string/jumbo v0, "volumekey_launch_camera"

    invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "function":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    .line 73
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :goto_0
    nop

    .line 75
    .local v1, "volumeDownLaunchCameraStatus":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    return v2
.end method

.method private synthetic lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 108
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    return-void
.end method

.method private synthetic lambda$updatePolicyFlag$1(I)V
    .locals 0
    .param p1, "policyFlags"    # I

    .line 114
    iput p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mPolicyFlag:I

    return-void
.end method

.method private postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 8
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 107
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 110
    return-void
.end method

.method private volumeDownKeyDoubleClick()V
    .locals 8

    .line 81
    iget v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mPolicyFlag:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 82
    .local v0, "isInjected":Z
    :goto_0
    const/4 v2, 0x1

    .line 83
    .local v2, "keyguardNotActive":Z
    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v4, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v4, :cond_1

    .line 84
    check-cast v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 85
    invoke-virtual {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isKeyGuardNotActive()Z

    move-result v2

    .line 88
    :cond_1
    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v3}, Lcom/android/server/policy/WindowManagerPolicy;->isScreenOn()Z

    move-result v3

    .line 90
    .local v3, "isScreenOn":Z
    if-nez v0, :cond_4

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 97
    :cond_2
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->isAudioActive()Z

    move-result v4

    if-nez v4, :cond_3

    .line 98
    const-string/jumbo v4, "volumekey_launch_camera"

    invoke-virtual {p0, v4}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 99
    .local v4, "function":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mVolumeKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v6, 0x1388

    invoke-virtual {v5, v6, v7}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 100
    nop

    .line 101
    invoke-static {v4}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getDoubleVolumeDownKeyFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 100
    const-string v6, "double_click_volume_down"

    const/4 v7, 0x0

    invoke-direct {p0, v6, v5, v7, v1}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 103
    .end local v4    # "function":Ljava/lang/String;
    :cond_3
    return-void

    .line 91
    :cond_4
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "volume down key launch fail,isInjected="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " keyguardNotActive="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " isScreenOn="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 94
    return-void
.end method


# virtual methods
.method protected getMiuiMaxMultiPressCount()I
    .locals 2

    .line 65
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->isEnableLaunchCamera()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mMaxCountMap:Ljava/util/Map;

    const-string/jumbo v1, "volumekey_launch_camera"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 66
    :cond_0
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0
.end method

.method protected miuiSupportLongPress()Z
    .locals 1

    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method protected onMiuiMultiPress(JI)V
    .locals 1
    .param p1, "downTime"    # J
    .param p3, "count"    # I

    .line 58
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->volumeDownKeyDoubleClick()V

    .line 61
    :cond_0
    return-void
.end method

.method public updatePolicyFlag(I)V
    .locals 2
    .param p1, "policyFlags"    # I

    .line 114
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 115
    return-void
.end method
