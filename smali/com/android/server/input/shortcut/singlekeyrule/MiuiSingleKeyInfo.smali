.class public Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
.super Ljava/lang/Object;
.source "MiuiSingleKeyInfo.java"


# instance fields
.field private final mActionAndDefaultFunctionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mActionMapForType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mActionMaxCountMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mLongPressTimeOut:J

.field private final mPrimaryKey:I


# direct methods
.method public constructor <init>(ILjava/util/Map;JLjava/util/Map;Ljava/util/Map;)V
    .locals 0
    .param p1, "primaryKey"    # I
    .param p3, "longPressTimeOut"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 18
    .local p2, "actionAndDefaultFunctionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p5, "actionMaxCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p6, "actionMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mPrimaryKey:I

    .line 20
    iput-object p2, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mActionAndDefaultFunctionMap:Ljava/util/Map;

    .line 21
    iput-wide p3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mLongPressTimeOut:J

    .line 22
    iput-object p5, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mActionMaxCountMap:Ljava/util/Map;

    .line 23
    iput-object p6, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mActionMapForType:Ljava/util/Map;

    .line 24
    return-void
.end method


# virtual methods
.method public getActionAndDefaultFunctionMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mActionAndDefaultFunctionMap:Ljava/util/Map;

    return-object v0
.end method

.method public getActionMapForType()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mActionMapForType:Ljava/util/Map;

    return-object v0
.end method

.method public getActionMaxCountMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mActionMaxCountMap:Ljava/util/Map;

    return-object v0
.end method

.method public getLongPressTimeOut()J
    .locals 2

    .line 31
    iget-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mLongPressTimeOut:J

    return-wide v0
.end method

.method public getPrimaryKey()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mPrimaryKey:I

    return v0
.end method
