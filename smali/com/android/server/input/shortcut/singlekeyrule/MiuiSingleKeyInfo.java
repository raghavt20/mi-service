public class com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo {
	 /* .source "MiuiSingleKeyInfo.java" */
	 /* # instance fields */
	 private final java.util.Map mActionAndDefaultFunctionMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.Map mActionMapForType;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mActionMaxCountMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final Long mLongPressTimeOut;
private final Integer mPrimaryKey;
/* # direct methods */
public com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ( ) {
/* .locals 0 */
/* .param p1, "primaryKey" # I */
/* .param p3, "longPressTimeOut" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;J", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 18 */
/* .local p2, "actionAndDefaultFunctionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* .local p5, "actionMaxCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* .local p6, "actionMapForType":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
/* iput p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mPrimaryKey:I */
/* .line 20 */
this.mActionAndDefaultFunctionMap = p2;
/* .line 21 */
/* iput-wide p3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mLongPressTimeOut:J */
/* .line 22 */
this.mActionMaxCountMap = p5;
/* .line 23 */
this.mActionMapForType = p6;
/* .line 24 */
return;
} // .end method
/* # virtual methods */
public java.util.Map getActionAndDefaultFunctionMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 27 */
v0 = this.mActionAndDefaultFunctionMap;
} // .end method
public java.util.Map getActionMapForType ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 43 */
v0 = this.mActionMapForType;
} // .end method
public java.util.Map getActionMaxCountMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 39 */
v0 = this.mActionMaxCountMap;
} // .end method
public Long getLongPressTimeOut ( ) {
/* .locals 2 */
/* .line 31 */
/* iget-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mLongPressTimeOut:J */
/* return-wide v0 */
} // .end method
public Integer getPrimaryKey ( ) {
/* .locals 1 */
/* .line 35 */
/* iget v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->mPrimaryKey:I */
} // .end method
