.class public Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;
.super Lcom/android/server/policy/MiuiSingleKeyRule;
.source "CameraKeyRule.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mLongPressAction:Ljava/lang/String;

.field private final mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;


# direct methods
.method public static synthetic $r8$lambda$mGYnYUrnKklh5gb6zN0ITiVzXxM(Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p4, "currentUserId"    # I

    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V

    .line 35
    iput-object p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mHandler:Landroid/os/Handler;

    .line 37
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMapForType()Ljava/util/Map;

    move-result-object v0

    const-string v1, "longPress"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mLongPressAction:Ljava/lang/String;

    .line 39
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 40
    return-void
.end method

.method private synthetic lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 62
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    return-void
.end method

.method private triggerLongPress()V
    .locals 5

    .line 48
    const/4 v0, 0x0

    .line 49
    .local v0, "windowState":Lcom/android/server/policy/WindowManagerPolicy$WindowState;
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v2, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v2, :cond_0

    .line 50
    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getFocusedWindow()Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    move-result-object v0

    .line 52
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "com.android.camera"

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 53
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mLongPressAction:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "longPressCameraFunction":Ljava/lang/String;
    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, "long_press_camera_key"

    invoke-virtual {p0, v4, v1, v2, v3}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 57
    .end local v1    # "longPressCameraFunction":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .line 69
    const-string/jumbo v0, "volumekey_launch_camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    invoke-virtual {p0}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "doubleClickVolumeDownStatus":Ljava/lang/String;
    nop

    .line 73
    invoke-static {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getDoubleVolumeDownKeyFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "longPressCameraFunction":Ljava/lang/String;
    const-string v2, "launch_camera_and_take_photo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 76
    return-object v2

    .line 78
    :cond_0
    const-string v2, "launch_camera"

    return-object v2

    .line 81
    .end local v0    # "doubleClickVolumeDownStatus":Ljava/lang/String;
    .end local v1    # "longPressCameraFunction":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;

    move-result-object v0

    const-string v1, "long_press_camera_key"

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onMiuiLongPress(J)V
    .locals 0
    .param p1, "eventTime"    # J

    .line 44
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->triggerLongPress()V

    .line 45
    return-void
.end method

.method public postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
    .locals 8
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 61
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule$$ExternalSyntheticLambda0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method
