.class public Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;
.super Lcom/android/server/policy/MiuiSingleKeyRule;
.source "DefaultSingleKeyRule.java"


# instance fields
.field private final mActionMapForType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mLongPressFunction:Ljava/lang/String;

.field private final mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field private final mPrimaryKey:I


# direct methods
.method public static synthetic $r8$lambda$ADXaH2a8rZK6Bd9R8rk67FtZ9WU(Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p4, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .param p5, "currentUserId"    # I

    .line 29
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V

    .line 30
    iput-object p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mHandler:Landroid/os/Handler;

    .line 32
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I

    move-result v0

    iput v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mPrimaryKey:I

    .line 33
    iput-object p4, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 34
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMapForType()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mActionMapForType:Ljava/util/Map;

    .line 35
    return-void
.end method

.method private synthetic lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 56
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    return-void
.end method

.method private postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 8
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 55
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule$$ExternalSyntheticLambda0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 58
    return-void
.end method

.method private triggerFunction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 61
    const/16 v0, 0x52

    iget v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mPrimaryKey:I

    if-ne v0, v1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mActionMapForType:Ljava/util/Map;

    .line 63
    const-string v1, "longPress"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    .line 68
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "close_app"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v0, v1

    .line 70
    const-string/jumbo v1, "shortcut_type"

    const-string v2, "phone_shortcut"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 74
    return-void
.end method


# virtual methods
.method protected getMiuiLongPressTimeoutMs()J
    .locals 2

    .line 78
    invoke-super {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiLongPressTimeoutMs()J

    move-result-wide v0

    return-wide v0
.end method

.method protected miuiSupportLongPress()Z
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mActionMapForType:Ljava/util/Map;

    const-string v1, "longPress"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mLongPressFunction:Ljava/lang/String;

    .line 48
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "none"

    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mLongPressFunction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mPrimaryKey:I

    .line 49
    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportAOSPTriggerFunction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 48
    :goto_0
    return v0
.end method

.method protected onMiuiLongPress(J)V
    .locals 3
    .param p1, "eventTime"    # J

    .line 39
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mLongPressFunction:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "none"

    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mLongPressFunction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mLongPressFunction:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mActionMapForType:Ljava/util/Map;

    const-string v2, "longPress"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :cond_0
    return-void
.end method
