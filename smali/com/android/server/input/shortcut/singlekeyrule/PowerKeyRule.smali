.class public Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;
.super Lcom/android/server/policy/MiuiSingleKeyRule;
.source "PowerKeyRule.java"

# interfaces
.implements Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;


# static fields
.field private static final LAUNCH_AU_PAY_WHEN_SOS_ENABLE_DELAY_TIME:I = 0x1f4

.field private static final SUPPORT_POWERFP:Z

.field private static final TAG:Ljava/lang/String; = "PowerKeyRule"


# instance fields
.field private final mActionMaxCountMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private final mGlobalActionKeyTimeout:J

.field private final mHandler:Landroid/os/Handler;

.field private mIsPowerGuideTriggeredByLongPressPower:Z

.field private volatile mIsXiaoaiServiceTriggeredByLongPressPower:Z

.field private mLongPressTimeout:J

.field private final mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field private final mOriginalPowerKeyRuleBridge:Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

.field private mShouldSendPowerUpToSmartHome:Z

.field private final mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

.field private mXiaoaiPowerGuideCount:I


# direct methods
.method public static synthetic $r8$lambda$32avnCJ7QwIB_MrJFJ-6NkImC9I(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetTriggerDoubleClickReason(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getTriggerDoubleClickReason(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 33
    const-string v0, "ro.hardware.fp.sideCap"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->SUPPORT_POWERFP:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p4, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .param p5, "currentUserId"    # I

    .line 57
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mXiaoaiPowerGuideCount:I

    .line 58
    iput-object p2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mHandler:Landroid/os/Handler;

    .line 59
    iput-object p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContext:Landroid/content/Context;

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContentResolver:Landroid/content/ContentResolver;

    .line 61
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMaxCountMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mActionMaxCountMap:Ljava/util/Map;

    .line 62
    iput p5, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I

    .line 63
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 64
    new-instance v0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

    invoke-direct {v0}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mOriginalPowerKeyRuleBridge:Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

    .line 65
    iput-object p4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 66
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getDeviceGlobalActionKeyTimeout()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mGlobalActionKeyTimeout:J

    .line 67
    invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getLongPressTimeOut()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mLongPressTimeout:J

    .line 68
    return-void
.end method

.method private changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .line 253
    const-string v0, "long_press_power_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    iget v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mXiaoaiPowerGuideCount:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mXiaoaiPowerGuideCount:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_0

    .line 258
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "xiaoai_power_guide"

    iget v4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I

    invoke-static {v0, v3, v1, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 262
    :cond_0
    iput-boolean v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z

    goto :goto_0

    .line 263
    :cond_1
    const-string v0, "power_up"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    iput-boolean v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z

    .line 266
    :cond_2
    :goto_0
    return-void
.end method

.method private doubleClickPowerRunnable()Ljava/lang/Runnable;
    .locals 1

    .line 403
    new-instance v0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;

    invoke-direct {v0, p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;)V

    return-object v0
.end method

.method private getTriggerDoubleClickReason(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "function"    # Ljava/lang/String;

    .line 358
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "double_click_power_key"

    if-eqz v0, :cond_0

    .line 359
    return-object v1

    .line 361
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v0, "launch_camera"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_1
    const-string v0, "mi_pay"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v0, "au_pay"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_3
    const-string v0, "google_pay"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 369
    return-object v1

    .line 367
    :pswitch_0
    const-string v0, "power_double_tap"

    return-object v0

    .line 365
    :pswitch_1
    const-string v0, "double_click_power"

    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x5b7b865e -> :sswitch_3
        -0x53dc4de3 -> :sswitch_2
        -0x400b407b -> :sswitch_1
        0x67859d31 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private inFingerprintEnrolling()Z
    .locals 5

    .line 374
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 376
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 377
    .local v3, "topClassName":Ljava/lang/String;
    const-string v4, "com.android.settings.NewFingerprintInternalActivity"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    .line 378
    return v2

    .line 382
    .end local v3    # "topClassName":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 380
    :catch_0
    move-exception v2

    .line 381
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "PowerKeyRule"

    const-string v4, "Exception"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 383
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return v1
.end method

.method private isSupportRsa()Z
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportRSARegion()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 181
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getRSAGuideStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 180
    :goto_0
    return v1
.end method

.method private synthetic lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 247
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    return-void
.end method

.method private longPressPowerIsTrigger()Z
    .locals 1

    .line 301
    iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsPowerGuideTriggeredByLongPressPower:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private needDelayTriggerFunction(Ljava/lang/String;)Z
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .line 387
    invoke-virtual {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "function":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 389
    return v2

    .line 391
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v3, "launch_camera"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_1
    const-string v3, "mi_pay"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_1

    :sswitch_2
    const-string v3, "au_pay"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_3
    const-string v3, "google_pay"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 398
    return v2

    .line 396
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSos()Z

    move-result v1

    return v1

    :sswitch_data_0
    .sparse-switch
        -0x5b7b865e -> :sswitch_3
        -0x53dc4de3 -> :sswitch_2
        -0x400b407b -> :sswitch_1
        0x67859d31 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
    .locals 8
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 246
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$$ExternalSyntheticLambda0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method private postTriggerPowerGuide(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 219
    const/4 v0, 0x0

    .line 220
    .local v0, "result":Z
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 221
    sget-object v1, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->CURRENT_DEVICE_REGION:Ljava/lang/String;

    const-string v2, "cn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 222
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 223
    .local v1, "voiceBundle":Landroid/os/Bundle;
    const-string v3, "powerGuide"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 224
    const-string v3, "extra_long_press_power_function"

    const-string v4, "long_press_power_key"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v3, "launch_voice_assistant"

    invoke-direct {p0, p1, v3, v1, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v0

    .line 228
    if-eqz v0, :cond_0

    .line 229
    invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V

    .line 231
    .end local v1    # "voiceBundle":Landroid/os/Bundle;
    :cond_0
    goto :goto_0

    .line 232
    :cond_1
    const-string v1, "launch_global_power_guide"

    const/4 v3, 0x0

    invoke-direct {p0, p1, v1, v3, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v0

    .line 234
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I

    const-string v5, "global_power_guide"

    invoke-static {v1, v5, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 237
    iput-boolean v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsPowerGuideTriggeredByLongPressPower:Z

    .line 238
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v1, p2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setLongPressPowerBehavior(Ljava/lang/String;)V

    .line 241
    :cond_2
    :goto_0
    return v0
.end method

.method private supportMiuiLongPress()Z
    .locals 4

    .line 165
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 166
    const-string v0, "PowerKeyRule"

    const-string/jumbo v2, "user set up not complete"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return v1

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 170
    return v2

    .line 171
    :cond_1
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->longPressPowerIsTrigger()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    return v2

    .line 175
    :cond_2
    const-string v0, "long_press_power_key"

    invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "function":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "none"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    :cond_3
    return v1
.end method

.method private triggerDoubleClick()Z
    .locals 9

    .line 326
    sget-boolean v0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->SUPPORT_POWERFP:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->inFingerprintEnrolling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const-string v0, "PowerKeyRule"

    const-string v2, "Power button double tap gesture detected, but in FingerprintEnrolling, return."

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    return v1

    .line 332
    :cond_0
    const-string v0, "double_click_power_key"

    .line 333
    .local v0, "action":Ljava/lang/String;
    const-string v2, "double_click_power_key"

    invoke-virtual {p0, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "function":Ljava/lang/String;
    const/4 v4, 0x0

    .line 336
    .local v4, "bundle":Landroid/os/Bundle;
    invoke-direct {p0, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->needDelayTriggerFunction(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_1

    .line 337
    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getPowerManager()Landroid/os/PowerManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 338
    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->doubleClickPowerRunnable()Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v7, 0x1f4

    invoke-virtual {v2, v5, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 342
    :cond_1
    const-string v5, "launch_smarthome"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 343
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    move-object v4, v5

    .line 344
    const-string v5, "event_source"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_2
    invoke-direct {p0, v3}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getTriggerDoubleClickReason(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v3, v4, v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 349
    :cond_3
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "none"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v1, v6

    :cond_4
    return v1
.end method

.method private triggerFiveClick()Z
    .locals 4

    .line 318
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 319
    const-string v0, "launch_sos"

    const/4 v2, 0x0

    const-string v3, "five_tap_power"

    invoke-direct {p0, v3, v0, v2, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v0

    return v0

    .line 322
    :cond_0
    return v1
.end method

.method private triggerLongPress()Z
    .locals 7

    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "intercept":Z
    const-string v1, "long_press_power_key"

    .line 188
    .local v1, "action":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 189
    .local v2, "function":Ljava/lang/String;
    const-string v3, "launch_google_search"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    .line 191
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->isSupportRsa()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isCtsMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 192
    invoke-direct {p0, v1, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerPowerGuide(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 194
    :cond_0
    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v0

    goto :goto_0

    .line 196
    :cond_1
    const-string v3, "launch_voice_assistant"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v5, "long_press_power_key"

    const-string v6, "extra_long_press_power_function"

    if-eqz v3, :cond_3

    .line 197
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 198
    .local v3, "bundle":Landroid/os/Bundle;
    invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v5, "powerGuide"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 201
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v0

    .line 202
    if-eqz v0, :cond_2

    .line 203
    invoke-direct {p0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V

    .line 205
    .end local v3    # "bundle":Landroid/os/Bundle;
    :cond_2
    goto :goto_0

    :cond_3
    const-string v3, "launch_smarthome"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 206
    iput-boolean v4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z

    .line 207
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 208
    .local v3, "smartHomeBundle":Landroid/os/Bundle;
    invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v0

    .line 211
    .end local v3    # "smartHomeBundle":Landroid/os/Bundle;
    :cond_4
    goto :goto_0

    :cond_5
    const-string v3, "none"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 212
    :cond_6
    invoke-direct {p0, v1, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerPowerGuide(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 215
    :goto_0
    return v0
.end method

.method private triggerMultiPress(I)Z
    .locals 2
    .param p1, "count"    # I

    .line 306
    sparse-switch p1, :sswitch_data_0

    .line 312
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->doubleClickPowerRunnable()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 314
    const/4 v0, 0x0

    return v0

    .line 310
    :sswitch_0
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerFiveClick()Z

    move-result v0

    return v0

    .line 308
    :sswitch_1
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerDoubleClick()Z

    move-result v0

    return v0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method private workAtPowerUp()V
    .locals 8

    .line 270
    iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z

    const-string v1, "long_press_power_key"

    const-string v2, "disable_shortcut_track"

    const-string v3, "extra_long_press_power_function"

    const-string v4, "power_up"

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v0, :cond_0

    .line 271
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 272
    .local v0, "voiceBundle":Landroid/os/Bundle;
    const-string v7, "powerGuide"

    invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 273
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 276
    const-string v7, "launch_voice_assistant"

    invoke-direct {p0, v1, v7, v0, v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v7

    .line 278
    .local v7, "triggered":Z
    if-eqz v7, :cond_0

    .line 279
    invoke-direct {p0, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V

    .line 282
    .end local v0    # "voiceBundle":Landroid/os/Bundle;
    .end local v7    # "triggered":Z
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z

    if-eqz v0, :cond_1

    .line 283
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 284
    .local v0, "smartHomeBundle":Landroid/os/Bundle;
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 287
    const-string v2, "launch_smarthome"

    invoke-direct {p0, v1, v2, v0, v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    move-result v1

    .line 289
    .local v1, "triggered":Z
    if-eqz v1, :cond_1

    .line 290
    iput-boolean v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z

    .line 294
    .end local v0    # "smartHomeBundle":Landroid/os/Bundle;
    .end local v1    # "triggered":Z
    :cond_1
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V

    .line 296
    iput-boolean v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsPowerGuideTriggeredByLongPressPower:Z

    .line 298
    :cond_2
    return-void
.end method


# virtual methods
.method protected getMiuiLongPressTimeoutMs()J
    .locals 2

    .line 142
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->supportMiuiLongPress()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mGlobalActionKeyTimeout:J

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mLongPressTimeout:J

    :goto_0
    return-wide v0
.end method

.method protected getMiuiMaxMultiPressCount()I
    .locals 5

    .line 147
    const/4 v0, 0x1

    .line 149
    .local v0, "maxPressCount":I
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mActionMaxCountMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 150
    .local v2, "actionMaxCountEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "function":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "none"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 152
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lt v4, v0, :cond_0

    .line 153
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 156
    .end local v2    # "actionMaxCountEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "function":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSos()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 158
    const/4 v0, 0x5

    .line 161
    :cond_2
    return v0
.end method

.method public init()V
    .locals 1

    .line 71
    invoke-super {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->init()V

    .line 72
    invoke-virtual {p0, p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->registerShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;)V

    .line 73
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V

    .line 74
    return-void
.end method

.method protected miuiSupportVeryLongPress()Z
    .locals 1

    .line 131
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->supportMiuiLongPress()Z

    move-result v0

    return v0
.end method

.method protected onMiuiKeyDown(Landroid/view/KeyEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 83
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isTorchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 85
    invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getPowerManager()Landroid/os/PowerManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    check-cast v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setPowerKeyHandled(Z)V

    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v0, "torchBundle":Landroid/os/Bundle;
    const-string v2, "extra_torch_enabled"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 89
    const-string/jumbo v2, "skip_telecom_check"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    const-string/jumbo v2, "triggered_by_power"

    const-string/jumbo v3, "turn_on_torch"

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 95
    .end local v0    # "torchBundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method protected onMiuiLongPress(J)V
    .locals 3
    .param p1, "eventTime"    # J

    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "triggered":Z
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->supportMiuiLongPress()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setPowerKeyHandled(Z)V

    .line 102
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerLongPress()Z

    move-result v0

    .line 105
    :cond_0
    if-nez v0, :cond_1

    .line 106
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mOriginalPowerKeyRuleBridge:Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

    invoke-virtual {v1, p1, p2}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onLongPress(J)V

    .line 109
    :cond_1
    return-void
.end method

.method protected onMiuiLongPressKeyUp(Landroid/view/KeyEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 126
    invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->workAtPowerUp()V

    .line 127
    return-void
.end method

.method protected onMiuiMultiPress(JI)V
    .locals 2
    .param p1, "downTime"    # J
    .param p3, "count"    # I

    .line 118
    invoke-direct {p0, p3}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerMultiPress(I)Z

    move-result v0

    .line 119
    .local v0, "triggered":Z
    if-nez v0, :cond_0

    .line 120
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mOriginalPowerKeyRuleBridge:Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

    invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onMultiPress(JI)V

    .line 122
    :cond_0
    return-void
.end method

.method protected onMiuiPress(J)V
    .locals 1
    .param p1, "downTime"    # J

    .line 78
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mOriginalPowerKeyRuleBridge:Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onPress(J)V

    .line 79
    return-void
.end method

.method protected onMiuiVeryLongPress(J)V
    .locals 1
    .param p1, "eventTime"    # J

    .line 113
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mOriginalPowerKeyRuleBridge:Lcom/android/server/policy/OriginalPowerKeyRuleBridge;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onVeryLongPress(J)V

    .line 114
    return-void
.end method

.method public onSingleChanged(Lcom/android/server/policy/MiuiSingleKeyRule;Landroid/net/Uri;)V
    .locals 2
    .param p1, "rule"    # Lcom/android/server/policy/MiuiSingleKeyRule;
    .param p2, "uri"    # Landroid/net/Uri;

    .line 415
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->getPrimaryKey()I

    move-result v0

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_0

    const-string v0, "long_press_power_key"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 416
    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V

    .line 418
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 419
    invoke-virtual {p1, v0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-virtual {v1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setLongPressPowerBehavior(Ljava/lang/String;)V

    .line 421
    :cond_0
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 0
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 425
    iput p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I

    .line 426
    invoke-super {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onUserSwitch(IZ)V

    .line 427
    return-void
.end method
