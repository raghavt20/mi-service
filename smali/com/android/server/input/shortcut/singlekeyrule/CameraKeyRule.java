public class com.android.server.input.shortcut.singlekeyrule.CameraKeyRule extends com.android.server.policy.MiuiSingleKeyRule {
	 /* .source "CameraKeyRule.java" */
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 private final java.lang.String mLongPressAction;
	 private final com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
	 /* # direct methods */
	 public static void $r8$lambda$mGYnYUrnKklh5gb6zN0ITiVzXxM ( com.android.server.input.shortcut.singlekeyrule.CameraKeyRule p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3, Boolean p4 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
		 return;
	 } // .end method
	 public com.android.server.input.shortcut.singlekeyrule.CameraKeyRule ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
		 /* .param p4, "currentUserId" # I */
		 /* .line 34 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V */
		 /* .line 35 */
		 this.mContext = p1;
		 /* .line 36 */
		 this.mHandler = p2;
		 /* .line 37 */
		 (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getActionMapForType ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMapForType()Ljava/util/Map;
		 final String v1 = "longPress"; // const-string v1, "longPress"
		 /* check-cast v0, Ljava/lang/String; */
		 this.mLongPressAction = v0;
		 /* .line 39 */
		 /* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
		 this.mWindowManagerPolicy = v0;
		 /* .line 40 */
		 return;
	 } // .end method
	 private void lambda$postTriggerFunction$0 ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) { //synthethic
		 /* .locals 1 */
		 /* .param p1, "function" # Ljava/lang/String; */
		 /* .param p2, "action" # Ljava/lang/String; */
		 /* .param p3, "bundle" # Landroid/os/Bundle; */
		 /* .param p4, "hapticFeedback" # Z */
		 /* .line 62 */
		 v0 = this.mContext;
		 com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
		 (( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
		 return;
	 } // .end method
	 private void triggerLongPress ( ) {
		 /* .locals 5 */
		 /* .line 48 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 49 */
		 /* .local v0, "windowState":Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
		 v1 = this.mWindowManagerPolicy;
		 /* instance-of v2, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 50 */
			 /* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
			 (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).getFocusedWindow ( ); // invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getFocusedWindow()Lcom/android/server/policy/WindowManagerPolicy$WindowState;
			 /* .line 52 */
		 } // :cond_0
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
			 v1 = 			 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 /* if-nez v1, :cond_1 */
			 /* .line 53 */
			 v1 = this.mLongPressAction;
			 (( com.android.server.input.shortcut.singlekeyrule.CameraKeyRule ) p0 ).getFunction ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
			 /* .line 54 */
			 /* .local v1, "longPressCameraFunction":Ljava/lang/String; */
			 int v2 = 0; // const/4 v2, 0x0
			 int v3 = 1; // const/4 v3, 0x1
			 final String v4 = "long_press_camera_key"; // const-string v4, "long_press_camera_key"
			 (( com.android.server.input.shortcut.singlekeyrule.CameraKeyRule ) p0 ).postTriggerFunction ( v4, v1, v2, v3 ); // invoke-virtual {p0, v4, v1, v2, v3}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
			 /* .line 57 */
		 } // .end local v1 # "longPressCameraFunction":Ljava/lang/String;
	 } // :cond_1
	 return;
} // .end method
/* # virtual methods */
public java.lang.String getFunction ( java.lang.String p0 ) {
	 /* .locals 4 */
	 /* .param p1, "action" # Ljava/lang/String; */
	 /* .line 69 */
	 /* const-string/jumbo v0, "volumekey_launch_camera" */
	 v1 = 	 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 70 */
		 (( com.android.server.input.shortcut.singlekeyrule.CameraKeyRule ) p0 ).getObserver ( ); // invoke-virtual {p0}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
		 (( com.android.server.policy.MiuiShortcutObserver ) v1 ).getFunction ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction(Ljava/lang/String;)Ljava/lang/String;
		 /* .line 72 */
		 /* .local v0, "doubleClickVolumeDownStatus":Ljava/lang/String; */
		 /* nop */
		 /* .line 73 */
		 com.android.server.policy.MiuiShortcutTriggerHelper .getDoubleVolumeDownKeyFunction ( v0 );
		 /* .line 75 */
		 /* .local v1, "longPressCameraFunction":Ljava/lang/String; */
		 final String v2 = "launch_camera_and_take_photo"; // const-string v2, "launch_camera_and_take_photo"
		 v3 = 		 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 76 */
			 /* .line 78 */
		 } // :cond_0
		 final String v2 = "launch_camera"; // const-string v2, "launch_camera"
		 /* .line 81 */
	 } // .end local v0 # "doubleClickVolumeDownStatus":Ljava/lang/String;
} // .end local v1 # "longPressCameraFunction":Ljava/lang/String;
} // :cond_1
(( com.android.server.input.shortcut.singlekeyrule.CameraKeyRule ) p0 ).getObserver ( ); // invoke-virtual {p0}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
final String v1 = "long_press_camera_key"; // const-string v1, "long_press_camera_key"
(( com.android.server.policy.MiuiShortcutObserver ) v0 ).getFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->getFunction(Ljava/lang/String;)Ljava/lang/String;
} // .end method
protected void onMiuiLongPress ( Long p0 ) {
/* .locals 0 */
/* .param p1, "eventTime" # J */
/* .line 44 */
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;->triggerLongPress()V */
/* .line 45 */
return;
} // .end method
public Boolean postTriggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) {
/* .locals 8 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 61 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule$$ExternalSyntheticLambda0; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p2 */
/* move-object v4, p1 */
/* move-object v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
v0 = (( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
} // .end method
