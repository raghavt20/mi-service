public class com.android.server.input.shortcut.singlekeyrule.VolumeDownKeyRule extends com.android.server.policy.MiuiSingleKeyRule {
	 /* .source "VolumeDownKeyRule.java" */
	 /* # static fields */
	 public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_CLOSE;
	 public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA;
	 public static final Integer DOUBLE_VOLUME_DOWN_KEY_TYPE_LAUNCH_CAMERA_AND_TAKE_PHOTO;
	 /* # instance fields */
	 private android.media.AudioManager mAudioManager;
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 private final java.util.Map mMaxCountMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
private Integer mPolicyFlag;
private final android.os.PowerManager mPowerManager;
private final android.os.PowerManager$WakeLock mVolumeKeyWakeLock;
private final com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
/* # direct methods */
public static void $r8$lambda$M7s7N7sST1B0wXXhrJei-MZOeQc ( com.android.server.input.shortcut.singlekeyrule.VolumeDownKeyRule p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->lambda$updatePolicyFlag$1(I)V */
return;
} // .end method
public static void $r8$lambda$WIdoVnY8sfteerWO2vzr2-G4vJE ( com.android.server.input.shortcut.singlekeyrule.VolumeDownKeyRule p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3, Boolean p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
return;
} // .end method
public com.android.server.input.shortcut.singlekeyrule.VolumeDownKeyRule ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* .param p4, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .param p5, "currentUserId" # I */
/* .line 40 */
/* invoke-direct {p0, p1, p2, p3, p5}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V */
/* .line 41 */
this.mContext = p1;
/* .line 42 */
this.mHandler = p2;
/* .line 43 */
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getActionMaxCountMap ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMaxCountMap()Ljava/util/Map;
this.mMaxCountMap = v0;
/* .line 44 */
final String v0 = "power"; // const-string v0, "power"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 45 */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "MiuiKeyShortcutTrigger.mVolumeKeyWakeLock"; // const-string v2, "MiuiKeyShortcutTrigger.mVolumeKeyWakeLock"
(( android.os.PowerManager ) v0 ).newWakeLock ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
this.mVolumeKeyWakeLock = v0;
/* .line 47 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v0;
/* .line 48 */
this.mMiuiShortcutTriggerHelper = p4;
/* .line 49 */
return;
} // .end method
private android.media.AudioManager getAudioManager ( ) {
/* .locals 2 */
/* .line 143 */
v0 = this.mAudioManager;
/* if-nez v0, :cond_0 */
/* .line 144 */
v0 = this.mContext;
final String v1 = "audio"; // const-string v1, "audio"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/media/AudioManager; */
this.mAudioManager = v0;
/* .line 146 */
} // :cond_0
v0 = this.mAudioManager;
} // .end method
private Boolean isAudioActive ( ) {
/* .locals 6 */
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
/* .line 119 */
/* .local v0, "active":Z */
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->getAudioManager()Landroid/media/AudioManager; */
v1 = (( android.media.AudioManager ) v1 ).getMode ( ); // invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I
/* .line 120 */
/* .local v1, "mode":I */
final String v2 = "isAudioActive():"; // const-string v2, "isAudioActive():"
/* if-lez v1, :cond_0 */
int v3 = 7; // const/4 v3, 0x7
/* if-ge v1, v3, :cond_0 */
/* .line 121 */
int v0 = 1; // const/4 v0, 0x1
/* .line 122 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v2 );
/* .line 123 */
/* .line 125 */
} // :cond_0
v3 = android.media.AudioSystem .getNumStreamTypes ( );
/* .line 126 */
/* .local v3, "size":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_3 */
/* .line 127 */
int v5 = 1; // const/4 v5, 0x1
/* if-ne v5, v4, :cond_1 */
/* .line 129 */
/* .line 131 */
} // :cond_1
int v5 = 0; // const/4 v5, 0x0
v0 = android.media.AudioSystem .isStreamActive ( v4,v5 );
/* .line 132 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 133 */
/* .line 126 */
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 136 */
} // .end local v4 # "i":I
} // :cond_3
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 137 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v2 );
/* .line 139 */
} // :cond_4
} // .end method
private Boolean isEnableLaunchCamera ( ) {
/* .locals 5 */
/* .line 70 */
/* const-string/jumbo v0, "volumekey_launch_camera" */
(( com.android.server.input.shortcut.singlekeyrule.VolumeDownKeyRule ) p0 ).getFunction ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 72 */
/* .local v0, "function":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v2 */
/* .line 73 */
} // :cond_0
v1 = java.lang.Integer .parseInt ( v0 );
} // :goto_0
/* nop */
/* .line 75 */
/* .local v1, "volumeDownLaunchCameraStatus":I */
int v3 = 1; // const/4 v3, 0x1
/* if-eq v1, v3, :cond_1 */
int v4 = 2; // const/4 v4, 0x2
/* if-ne v1, v4, :cond_2 */
} // :cond_1
/* move v2, v3 */
} // :cond_2
} // .end method
private void lambda$postTriggerFunction$0 ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) { //synthethic
/* .locals 1 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 108 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
return;
} // .end method
private void lambda$updatePolicyFlag$1 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "policyFlags" # I */
/* .line 114 */
/* iput p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mPolicyFlag:I */
return;
} // .end method
private void postTriggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) {
/* .locals 8 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 107 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p2 */
/* move-object v4, p1 */
/* move-object v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 110 */
return;
} // .end method
private void volumeDownKeyDoubleClick ( ) {
/* .locals 8 */
/* .line 81 */
/* iget v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->mPolicyFlag:I */
/* const/high16 v1, 0x1000000 */
/* and-int/2addr v0, v1 */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 82 */
/* .local v0, "isInjected":Z */
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
/* .line 83 */
/* .local v2, "keyguardNotActive":Z */
v3 = this.mWindowManagerPolicy;
/* instance-of v4, v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 84 */
/* check-cast v3, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 85 */
v2 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v3 ).isKeyGuardNotActive ( ); // invoke-virtual {v3}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->isKeyGuardNotActive()Z
/* .line 88 */
} // :cond_1
v3 = v3 = this.mWindowManagerPolicy;
/* .line 90 */
/* .local v3, "isScreenOn":Z */
/* if-nez v0, :cond_4 */
if ( v3 != null) { // if-eqz v3, :cond_2
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 97 */
} // :cond_2
v4 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->isAudioActive()Z */
/* if-nez v4, :cond_3 */
/* .line 98 */
/* const-string/jumbo v4, "volumekey_launch_camera" */
(( com.android.server.input.shortcut.singlekeyrule.VolumeDownKeyRule ) p0 ).getFunction ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 99 */
/* .local v4, "function":Ljava/lang/String; */
v5 = this.mVolumeKeyWakeLock;
/* const-wide/16 v6, 0x1388 */
(( android.os.PowerManager$WakeLock ) v5 ).acquire ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
/* .line 100 */
/* nop */
/* .line 101 */
com.android.server.policy.MiuiShortcutTriggerHelper .getDoubleVolumeDownKeyFunction ( v4 );
/* .line 100 */
final String v6 = "double_click_volume_down"; // const-string v6, "double_click_volume_down"
int v7 = 0; // const/4 v7, 0x0
/* invoke-direct {p0, v6, v5, v7, v1}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
/* .line 103 */
} // .end local v4 # "function":Ljava/lang/String;
} // :cond_3
return;
/* .line 91 */
} // :cond_4
} // :goto_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "volume down key launch fail,isInjected=" */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " keyguardNotActive="; // const-string v4, " keyguardNotActive="
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " isScreenOn="; // const-string v4, " isScreenOn="
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v1 );
/* .line 94 */
return;
} // .end method
/* # virtual methods */
protected Integer getMiuiMaxMultiPressCount ( ) {
/* .locals 2 */
/* .line 65 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->isEnableLaunchCamera()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMaxCountMap;
/* const-string/jumbo v1, "volumekey_launch_camera" */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 66 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 65 */
} // :goto_0
} // .end method
protected Boolean miuiSupportLongPress ( ) {
/* .locals 1 */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected void onMiuiMultiPress ( Long p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "downTime" # J */
/* .param p3, "count" # I */
/* .line 58 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p3, v0, :cond_0 */
/* .line 59 */
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;->volumeDownKeyDoubleClick()V */
/* .line 61 */
} // :cond_0
return;
} // .end method
public void updatePolicyFlag ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "policyFlags" # I */
/* .line 114 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 115 */
return;
} // .end method
