public class com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager {
	 /* .source "MiuiSingleKeyRuleManager.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager sMiuiSingleKeyRuleManager;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 private final com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
	 private final com.android.server.policy.SingleKeyGestureDetector mSingleKeyGestureDetector;
	 private final java.util.HashMap mSingleKeyRuleHashMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Lcom/android/server/policy/MiuiSingleKeyRule;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.HashMap mSingleKeyRuleHashMapForAction;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/policy/MiuiSingleKeyRule;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager ( ) {
/* .locals 1 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .param p4, "singleKeyGestureDetector" # Lcom/android/server/policy/SingleKeyGestureDetector; */
/* .line 31 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 25 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mSingleKeyRuleHashMap = v0;
/* .line 26 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mSingleKeyRuleHashMapForAction = v0;
/* .line 32 */
this.mHandler = p1;
/* .line 33 */
this.mContext = p2;
/* .line 34 */
this.mMiuiShortcutTriggerHelper = p3;
/* .line 35 */
this.mSingleKeyGestureDetector = p4;
/* .line 36 */
return;
} // .end method
public static com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager getInstance ( android.content.Context p0, android.os.Handler p1, com.android.server.policy.MiuiShortcutTriggerHelper p2, com.android.server.policy.SingleKeyGestureDetector p3 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .param p3, "singleKeyGestureDetector" # Lcom/android/server/policy/SingleKeyGestureDetector; */
/* .line 41 */
v0 = com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager.sMiuiSingleKeyRuleManager;
/* if-nez v0, :cond_1 */
/* .line 42 */
/* const-class v0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager; */
/* monitor-enter v0 */
/* .line 43 */
try { // :try_start_0
v1 = com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager.sMiuiSingleKeyRuleManager;
/* if-nez v1, :cond_0 */
/* .line 44 */
/* new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager; */
/* invoke-direct {v1, p1, p0, p2, p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;-><init>(Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/policy/MiuiShortcutTriggerHelper;Lcom/android/server/policy/SingleKeyGestureDetector;)V */
/* .line 47 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 49 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyRuleManager.sMiuiSingleKeyRuleManager;
} // .end method
static void lambda$initSingleKeyRule$2 ( java.lang.Integer p0, com.android.server.policy.MiuiSingleKeyRule p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "key" # Ljava/lang/Integer; */
/* .param p1, "miuiSingleKeyRule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .line 127 */
(( com.android.server.policy.MiuiSingleKeyRule ) p1 ).init ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->init()V
return;
} // .end method
static void lambda$onUserSwitch$0 ( Integer p0, Boolean p1, java.lang.Integer p2, com.android.server.policy.MiuiSingleKeyRule p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "currentUserId" # I */
/* .param p1, "isNewUser" # Z */
/* .param p2, "k" # Ljava/lang/Integer; */
/* .param p3, "v" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .line 100 */
(( com.android.server.policy.MiuiSingleKeyRule ) p3 ).onUserSwitch ( p0, p1 ); // invoke-virtual {p3, p0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->onUserSwitch(IZ)V
return;
} // .end method
static void lambda$resetShortcutSettings$1 ( java.lang.Integer p0, com.android.server.policy.MiuiSingleKeyRule p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "k" # Ljava/lang/Integer; */
/* .param p1, "v" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .line 104 */
(( com.android.server.policy.MiuiSingleKeyRule ) p1 ).getObserver ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.policy.MiuiShortcutObserver ) v0 ).setDefaultFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
return;
} // .end method
/* # virtual methods */
public void addRule ( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo p0, com.android.server.policy.MiuiSingleKeyRule p1 ) {
/* .locals 5 */
/* .param p1, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* .param p2, "miuiSingleKeyRule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .line 79 */
v0 = (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p1 ).getPrimaryKey ( ); // invoke-virtual {p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I
/* .line 80 */
/* .local v0, "primaryKey":I */
v1 = this.mSingleKeyRuleHashMap;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).put ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 82 */
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p1 ).getActionAndDefaultFunctionMap ( ); // invoke-virtual {p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionAndDefaultFunctionMap()Ljava/util/Map;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 83 */
/* .local v2, "actionMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
v3 = this.mSingleKeyRuleHashMapForAction;
/* check-cast v4, Ljava/lang/String; */
(( java.util.HashMap ) v3 ).put ( v4, p2 ); // invoke-virtual {v3, v4, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 84 */
} // .end local v2 # "actionMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
/* .line 85 */
} // :cond_0
return;
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 3 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 116 */
final String v0 = "MiuiSingleKeyRuleManager"; // const-string v0, "MiuiSingleKeyRuleManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 117 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 119 */
v0 = this.mSingleKeyRuleHashMap;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 120 */
/* .local v1, "miuiSingleKeyRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;>;" */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 121 */
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 122 */
/* check-cast v2, Lcom/android/server/policy/MiuiSingleKeyRule; */
(( com.android.server.policy.MiuiSingleKeyRule ) v2 ).dump ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
/* .line 123 */
} // .end local v1 # "miuiSingleKeyRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;>;"
/* .line 124 */
} // :cond_0
return;
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 96 */
v0 = this.mSingleKeyRuleHashMapForAction;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiSingleKeyRule; */
(( com.android.server.policy.MiuiSingleKeyRule ) v0 ).getFunction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
} // .end method
public com.android.server.policy.MiuiSingleKeyRule getMiuiSingleKeyRule ( Integer p0, com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "primaryKey" # I */
/* .param p2, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* .param p3, "currentUserId" # I */
/* .line 54 */
int v0 = 0; // const/4 v0, 0x0
/* .line 55 */
/* .local v0, "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule; */
/* packed-switch p1, :pswitch_data_0 */
/* .line 69 */
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule; */
v2 = this.mContext;
v3 = this.mHandler;
v5 = this.mMiuiShortcutTriggerHelper;
/* move-object v1, v7 */
/* move-object v4, p2 */
/* move v6, p3 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V */
/* move-object v0, v7 */
/* .line 65 */
/* :pswitch_0 */
/* new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule; */
v2 = this.mContext;
v3 = this.mHandler;
/* invoke-direct {v1, v2, v3, p2, p3}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V */
/* move-object v0, v1 */
/* .line 67 */
/* .line 57 */
/* :pswitch_1 */
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule; */
v2 = this.mContext;
v3 = this.mHandler;
v5 = this.mMiuiShortcutTriggerHelper;
/* move-object v1, v7 */
/* move-object v4, p2 */
/* move v6, p3 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V */
/* move-object v0, v7 */
/* .line 59 */
/* .line 61 */
/* :pswitch_2 */
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule; */
v2 = this.mContext;
v3 = this.mHandler;
v5 = this.mMiuiShortcutTriggerHelper;
/* move-object v1, v7 */
/* move-object v4, p2 */
/* move v6, p3 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V */
/* move-object v0, v7 */
/* .line 63 */
/* nop */
/* .line 73 */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "create single key rule,primary="; // const-string v2, "create single key rule,primary="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.policy.MiuiSingleKeyRule ) v0 ).getPrimaryKey ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getPrimaryKey()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSingleKeyRuleManager"; // const-string v2, "MiuiSingleKeyRuleManager"
android.util.Slog .i ( v2,v1 );
/* .line 74 */
v1 = this.mSingleKeyGestureDetector;
(( com.android.server.policy.SingleKeyGestureDetector ) v1 ).removeRule ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/policy/SingleKeyGestureDetector;->removeRule(Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;)V
/* .line 75 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x19 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public com.android.server.policy.MiuiSingleKeyRule getSingleKeyRuleForPrimaryKey ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "primaryKey" # I */
/* .line 92 */
v0 = this.mSingleKeyRuleHashMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiSingleKeyRule; */
} // .end method
public Boolean hasActionInSingleKeyMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 108 */
v0 = this.mSingleKeyRuleHashMapForAction;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
} // .end method
public void initSingleKeyRule ( ) {
/* .locals 2 */
/* .line 127 */
v0 = this.mSingleKeyRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda0;-><init>()V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 128 */
return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "currentUserId" # I */
/* .param p2, "isNewUser" # Z */
/* .line 100 */
v0 = this.mSingleKeyRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p1, p2}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda1;-><init>(IZ)V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 101 */
return;
} // .end method
public void removeRule ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "primaryKey" # I */
/* .line 88 */
v0 = this.mSingleKeyRuleHashMap;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 89 */
return;
} // .end method
public void resetShortcutSettings ( ) {
/* .locals 2 */
/* .line 104 */
v0 = this.mSingleKeyRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda2;-><init>()V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 105 */
return;
} // .end method
public void updatePolicyFlag ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "policyFlags" # I */
/* .line 112 */
v0 = this.mSingleKeyRuleHashMap;
/* const/16 v1, 0x19 */
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiSingleKeyRule; */
(( com.android.server.policy.MiuiSingleKeyRule ) v0 ).updatePolicyFlag ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->updatePolicyFlag(I)V
/* .line 113 */
return;
} // .end method
