public class com.android.server.input.shortcut.singlekeyrule.PowerKeyRule extends com.android.server.policy.MiuiSingleKeyRule implements com.android.server.policy.MiuiShortcutObserver$MiuiShortcutListener {
	 /* .source "PowerKeyRule.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer LAUNCH_AU_PAY_WHEN_SOS_ENABLE_DELAY_TIME;
	 private static final Boolean SUPPORT_POWERFP;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.util.Map mActionMaxCountMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final android.content.ContentResolver mContentResolver;
private final android.content.Context mContext;
private Integer mCurrentUserId;
private final Long mGlobalActionKeyTimeout;
private final android.os.Handler mHandler;
private Boolean mIsPowerGuideTriggeredByLongPressPower;
private volatile Boolean mIsXiaoaiServiceTriggeredByLongPressPower;
private Long mLongPressTimeout;
private final com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
private final com.android.server.policy.OriginalPowerKeyRuleBridge mOriginalPowerKeyRuleBridge;
private Boolean mShouldSendPowerUpToSmartHome;
private final com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
private Integer mXiaoaiPowerGuideCount;
/* # direct methods */
public static void $r8$lambda$32avnCJ7QwIB_MrJFJ-6NkImC9I ( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3, Boolean p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static java.lang.String -$$Nest$mgetTriggerDoubleClickReason ( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getTriggerDoubleClickReason(Ljava/lang/String;)Ljava/lang/String; */
} // .end method
static com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ( ) {
/* .locals 2 */
/* .line 33 */
final String v0 = "ro.hardware.fp.sideCap"; // const-string v0, "ro.hardware.fp.sideCap"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.input.shortcut.singlekeyrule.PowerKeyRule.SUPPORT_POWERFP = (v0!= 0);
return;
} // .end method
public com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* .param p4, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .param p5, "currentUserId" # I */
/* .line 57 */
/* invoke-direct {p0, p1, p2, p3, p5}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V */
/* .line 45 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mXiaoaiPowerGuideCount:I */
/* .line 58 */
this.mHandler = p2;
/* .line 59 */
this.mContext = p1;
/* .line 60 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mContentResolver = v0;
/* .line 61 */
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getActionMaxCountMap ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMaxCountMap()Ljava/util/Map;
this.mActionMaxCountMap = v0;
/* .line 62 */
/* iput p5, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I */
/* .line 63 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v0;
/* .line 64 */
/* new-instance v0, Lcom/android/server/policy/OriginalPowerKeyRuleBridge; */
/* invoke-direct {v0}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;-><init>()V */
this.mOriginalPowerKeyRuleBridge = v0;
/* .line 65 */
this.mMiuiShortcutTriggerHelper = p4;
/* .line 66 */
android.view.ViewConfiguration .get ( p1 );
(( android.view.ViewConfiguration ) v0 ).getDeviceGlobalActionKeyTimeout ( ); // invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getDeviceGlobalActionKeyTimeout()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mGlobalActionKeyTimeout:J */
/* .line 67 */
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getLongPressTimeOut ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getLongPressTimeOut()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mLongPressTimeout:J */
/* .line 68 */
return;
} // .end method
private void changeXiaoAiPowerGuideStatus ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 253 */
final String v0 = "long_press_power_key"; // const-string v0, "long_press_power_key"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 254 */
	 v0 = this.mMiuiShortcutTriggerHelper;
	 /* iget v0, v0, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->mXiaoaiPowerGuideFlag:I */
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v0, v2, :cond_0 */
	 /* iget v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mXiaoaiPowerGuideCount:I */
	 /* add-int/2addr v0, v2 */
	 /* iput v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mXiaoaiPowerGuideCount:I */
	 int v3 = 2; // const/4 v3, 0x2
	 /* if-lt v0, v3, :cond_0 */
	 /* .line 258 */
	 v0 = this.mContext;
	 (( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* const-string/jumbo v3, "xiaoai_power_guide" */
	 /* iget v4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I */
	 android.provider.Settings$System .putIntForUser ( v0,v3,v1,v4 );
	 /* .line 262 */
} // :cond_0
/* iput-boolean v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z */
/* .line 263 */
} // :cond_1
final String v0 = "power_up"; // const-string v0, "power_up"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 264 */
/* iput-boolean v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z */
/* .line 266 */
} // :cond_2
} // :goto_0
return;
} // .end method
private java.lang.Runnable doubleClickPowerRunnable ( ) {
/* .locals 1 */
/* .line 403 */
/* new-instance v0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;)V */
} // .end method
private java.lang.String getTriggerDoubleClickReason ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 358 */
v0 = android.text.TextUtils .isEmpty ( p1 );
final String v1 = "double_click_power_key"; // const-string v1, "double_click_power_key"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 359 */
/* .line 361 */
} // :cond_0
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v0 = "launch_camera"; // const-string v0, "launch_camera"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_1 */
final String v0 = "mi_pay"; // const-string v0, "mi_pay"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_2 */
final String v0 = "au_pay"; // const-string v0, "au_pay"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_3 */
final String v0 = "google_pay"; // const-string v0, "google_pay"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v0 = 2; // const/4 v0, 0x2
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 369 */
/* .line 367 */
/* :pswitch_0 */
final String v0 = "power_double_tap"; // const-string v0, "power_double_tap"
/* .line 365 */
/* :pswitch_1 */
final String v0 = "double_click_power"; // const-string v0, "double_click_power"
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5b7b865e -> :sswitch_3 */
/* -0x53dc4de3 -> :sswitch_2 */
/* -0x400b407b -> :sswitch_1 */
/* 0x67859d31 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean inFingerprintEnrolling ( ) {
/* .locals 5 */
/* .line 374 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 376 */
/* .local v0, "am":Landroid/app/ActivityManager; */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_0
(( android.app.ActivityManager ) v0 ).getRunningTasks ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo; */
v3 = this.topActivity;
(( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 377 */
/* .local v3, "topClassName":Ljava/lang/String; */
final String v4 = "com.android.settings.NewFingerprintInternalActivity"; // const-string v4, "com.android.settings.NewFingerprintInternalActivity"
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 378 */
/* .line 382 */
} // .end local v3 # "topClassName":Ljava/lang/String;
} // :cond_0
/* .line 380 */
/* :catch_0 */
/* move-exception v2 */
/* .line 381 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "PowerKeyRule"; // const-string v3, "PowerKeyRule"
final String v4 = "Exception"; // const-string v4, "Exception"
android.util.Slog .e ( v3,v4,v2 );
/* .line 383 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private Boolean isSupportRsa ( ) {
/* .locals 2 */
/* .line 180 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).supportRSARegion ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportRSARegion()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMiuiShortcutTriggerHelper;
/* .line 181 */
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).getRSAGuideStatus ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getRSAGuideStatus()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 180 */
} // :goto_0
} // .end method
private void lambda$postTriggerFunction$0 ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) { //synthethic
/* .locals 1 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 247 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
return;
} // .end method
private Boolean longPressPowerIsTrigger ( ) {
/* .locals 1 */
/* .line 301 */
/* iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsPowerGuideTriggeredByLongPressPower:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean needDelayTriggerFunction ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 387 */
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).getFunction ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 388 */
/* .local v0, "function":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 389 */
/* .line 391 */
} // :cond_0
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).getFunction ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
v3 = (( java.lang.String ) v1 ).hashCode ( ); // invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v3, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v3 = "launch_camera"; // const-string v3, "launch_camera"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 3; // const/4 v1, 0x3
/* :sswitch_1 */
final String v3 = "mi_pay"; // const-string v3, "mi_pay"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v1, v2 */
/* :sswitch_2 */
final String v3 = "au_pay"; // const-string v3, "au_pay"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 1; // const/4 v1, 0x1
/* :sswitch_3 */
final String v3 = "google_pay"; // const-string v3, "google_pay"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 2; // const/4 v1, 0x2
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 398 */
/* .line 396 */
/* :pswitch_0 */
v1 = this.mMiuiShortcutTriggerHelper;
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).shouldLaunchSos ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSos()Z
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x5b7b865e -> :sswitch_3 */
/* -0x53dc4de3 -> :sswitch_2 */
/* -0x400b407b -> :sswitch_1 */
/* 0x67859d31 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean postTriggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) {
/* .locals 8 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 246 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$$ExternalSyntheticLambda0; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p2 */
/* move-object v4, p1 */
/* move-object v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
v0 = (( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
} // .end method
private Boolean postTriggerPowerGuide ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 219 */
int v0 = 0; // const/4 v0, 0x0
/* .line 220 */
/* .local v0, "result":Z */
v1 = this.mMiuiShortcutTriggerHelper;
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).isSupportLongPressPowerGuide ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 221 */
v1 = com.android.server.policy.MiuiShortcutTriggerHelper.CURRENT_DEVICE_REGION;
final String v2 = "cn"; // const-string v2, "cn"
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 222 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 223 */
/* .local v1, "voiceBundle":Landroid/os/Bundle; */
final String v3 = "powerGuide"; // const-string v3, "powerGuide"
(( android.os.Bundle ) v1 ).putBoolean ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 224 */
final String v3 = "extra_long_press_power_function"; // const-string v3, "extra_long_press_power_function"
final String v4 = "long_press_power_key"; // const-string v4, "long_press_power_key"
(( android.os.Bundle ) v1 ).putString ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 226 */
final String v3 = "launch_voice_assistant"; // const-string v3, "launch_voice_assistant"
v0 = /* invoke-direct {p0, p1, v3, v1, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 228 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V */
/* .line 231 */
} // .end local v1 # "voiceBundle":Landroid/os/Bundle;
} // :cond_0
/* .line 232 */
} // :cond_1
final String v1 = "launch_global_power_guide"; // const-string v1, "launch_global_power_guide"
int v3 = 0; // const/4 v3, 0x0
v0 = /* invoke-direct {p0, p1, v1, v3, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 234 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 0; // const/4 v3, 0x0
/* iget v4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I */
final String v5 = "global_power_guide"; // const-string v5, "global_power_guide"
android.provider.Settings$System .putIntForUser ( v1,v5,v3,v4 );
/* .line 237 */
/* iput-boolean v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsPowerGuideTriggeredByLongPressPower:Z */
/* .line 238 */
v1 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).setLongPressPowerBehavior ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setLongPressPowerBehavior(Ljava/lang/String;)V
/* .line 241 */
} // :cond_2
} // :goto_0
} // .end method
private Boolean supportMiuiLongPress ( ) {
/* .locals 4 */
/* .line 165 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isUserSetUpComplete ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isUserSetUpComplete()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 166 */
final String v0 = "PowerKeyRule"; // const-string v0, "PowerKeyRule"
/* const-string/jumbo v2, "user set up not complete" */
android.util.Slog .d ( v0,v2 );
/* .line 167 */
/* .line 169 */
} // :cond_0
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isSupportLongPressPowerGuide ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 170 */
/* .line 171 */
} // :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->longPressPowerIsTrigger()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 173 */
/* .line 175 */
} // :cond_2
final String v0 = "long_press_power_key"; // const-string v0, "long_press_power_key"
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).getFunction ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 176 */
/* .local v0, "function":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v3, :cond_3 */
final String v3 = "none"; // const-string v3, "none"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_3 */
/* move v1, v2 */
} // :cond_3
} // .end method
private Boolean triggerDoubleClick ( ) {
/* .locals 9 */
/* .line 326 */
/* sget-boolean v0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->SUPPORT_POWERFP:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->inFingerprintEnrolling()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 327 */
final String v0 = "PowerKeyRule"; // const-string v0, "PowerKeyRule"
final String v2 = "Power button double tap gesture detected, but in FingerprintEnrolling, return."; // const-string v2, "Power button double tap gesture detected, but in FingerprintEnrolling, return."
android.util.Slog .i ( v0,v2 );
/* .line 330 */
/* .line 332 */
} // :cond_0
final String v0 = "double_click_power_key"; // const-string v0, "double_click_power_key"
/* .line 333 */
/* .local v0, "action":Ljava/lang/String; */
final String v2 = "double_click_power_key"; // const-string v2, "double_click_power_key"
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).getFunction ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 335 */
/* .local v3, "function":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 336 */
/* .local v4, "bundle":Landroid/os/Bundle; */
v5 = /* invoke-direct {p0, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->needDelayTriggerFunction(Ljava/lang/String;)Z */
int v6 = 1; // const/4 v6, 0x1
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 337 */
v2 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v2 ).getPowerManager ( ); // invoke-virtual {v2}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getPowerManager()Landroid/os/PowerManager;
v2 = (( android.os.PowerManager ) v2 ).isInteractive ( ); // invoke-virtual {v2}, Landroid/os/PowerManager;->isInteractive()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 338 */
v2 = this.mHandler;
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->doubleClickPowerRunnable()Ljava/lang/Runnable; */
/* const-wide/16 v7, 0x1f4 */
(( android.os.Handler ) v2 ).postDelayed ( v5, v7, v8 ); // invoke-virtual {v2, v5, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 342 */
} // :cond_1
final String v5 = "launch_smarthome"; // const-string v5, "launch_smarthome"
v5 = (( java.lang.String ) v5 ).equals ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 343 */
/* new-instance v5, Landroid/os/Bundle; */
/* invoke-direct {v5}, Landroid/os/Bundle;-><init>()V */
/* move-object v4, v5 */
/* .line 344 */
final String v5 = "event_source"; // const-string v5, "event_source"
(( android.os.Bundle ) v4 ).putString ( v5, v2 ); // invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 347 */
} // :cond_2
/* invoke-direct {p0, v3}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getTriggerDoubleClickReason(Ljava/lang/String;)Ljava/lang/String; */
/* invoke-direct {p0, v2, v3, v4, v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 349 */
} // :cond_3
} // :goto_0
v2 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v2, :cond_4 */
final String v2 = "none"; // const-string v2, "none"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_4 */
/* move v1, v6 */
} // :cond_4
} // .end method
private Boolean triggerFiveClick ( ) {
/* .locals 4 */
/* .line 318 */
v0 = this.mMiuiShortcutTriggerHelper;
int v1 = 1; // const/4 v1, 0x1
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).shouldLaunchSosByType ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSosByType(I)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 319 */
final String v0 = "launch_sos"; // const-string v0, "launch_sos"
int v2 = 0; // const/4 v2, 0x0
final String v3 = "five_tap_power"; // const-string v3, "five_tap_power"
v0 = /* invoke-direct {p0, v3, v0, v2, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 322 */
} // :cond_0
} // .end method
private Boolean triggerLongPress ( ) {
/* .locals 7 */
/* .line 186 */
int v0 = 0; // const/4 v0, 0x0
/* .line 187 */
/* .local v0, "intercept":Z */
final String v1 = "long_press_power_key"; // const-string v1, "long_press_power_key"
/* .line 188 */
/* .local v1, "action":Ljava/lang/String; */
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).getFunction ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 189 */
/* .local v2, "function":Ljava/lang/String; */
final String v3 = "launch_google_search"; // const-string v3, "launch_google_search"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 191 */
v3 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->isSupportRsa()Z */
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.mMiuiShortcutTriggerHelper;
v3 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v3 ).isCtsMode ( ); // invoke-virtual {v3}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isCtsMode()Z
/* if-nez v3, :cond_0 */
/* .line 192 */
v0 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerPowerGuide(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 194 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
v0 = /* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 196 */
} // :cond_1
final String v3 = "launch_voice_assistant"; // const-string v3, "launch_voice_assistant"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v5 = "long_press_power_key"; // const-string v5, "long_press_power_key"
final String v6 = "extra_long_press_power_function"; // const-string v6, "extra_long_press_power_function"
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 197 */
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* .line 198 */
/* .local v3, "bundle":Landroid/os/Bundle; */
(( android.os.Bundle ) v3 ).putString ( v6, v5 ); // invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 200 */
final String v5 = "powerGuide"; // const-string v5, "powerGuide"
int v6 = 0; // const/4 v6, 0x0
(( android.os.Bundle ) v3 ).putBoolean ( v5, v6 ); // invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 201 */
v0 = /* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 202 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 203 */
/* invoke-direct {p0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V */
/* .line 205 */
} // .end local v3 # "bundle":Landroid/os/Bundle;
} // :cond_2
} // :cond_3
final String v3 = "launch_smarthome"; // const-string v3, "launch_smarthome"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 206 */
/* iput-boolean v4, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z */
/* .line 207 */
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* .line 208 */
/* .local v3, "smartHomeBundle":Landroid/os/Bundle; */
(( android.os.Bundle ) v3 ).putString ( v6, v5 ); // invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 210 */
v0 = /* invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 211 */
} // .end local v3 # "smartHomeBundle":Landroid/os/Bundle;
} // :cond_4
} // :cond_5
final String v3 = "none"; // const-string v3, "none"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_6 */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 212 */
} // :cond_6
v0 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerPowerGuide(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 215 */
} // :goto_0
} // .end method
private Boolean triggerMultiPress ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "count" # I */
/* .line 306 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 312 */
v0 = this.mHandler;
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->doubleClickPowerRunnable()Ljava/lang/Runnable; */
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 314 */
int v0 = 0; // const/4 v0, 0x0
/* .line 310 */
/* :sswitch_0 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerFiveClick()Z */
/* .line 308 */
/* :sswitch_1 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerDoubleClick()Z */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x2 -> :sswitch_1 */
/* 0x5 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void workAtPowerUp ( ) {
/* .locals 8 */
/* .line 270 */
/* iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsXiaoaiServiceTriggeredByLongPressPower:Z */
final String v1 = "long_press_power_key"; // const-string v1, "long_press_power_key"
final String v2 = "disable_shortcut_track"; // const-string v2, "disable_shortcut_track"
final String v3 = "extra_long_press_power_function"; // const-string v3, "extra_long_press_power_function"
final String v4 = "power_up"; // const-string v4, "power_up"
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 271 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 272 */
/* .local v0, "voiceBundle":Landroid/os/Bundle; */
final String v7 = "powerGuide"; // const-string v7, "powerGuide"
(( android.os.Bundle ) v0 ).putBoolean ( v7, v5 ); // invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 273 */
(( android.os.Bundle ) v0 ).putString ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 275 */
(( android.os.Bundle ) v0 ).putBoolean ( v2, v6 ); // invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 276 */
final String v7 = "launch_voice_assistant"; // const-string v7, "launch_voice_assistant"
v7 = /* invoke-direct {p0, v1, v7, v0, v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 278 */
/* .local v7, "triggered":Z */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 279 */
/* invoke-direct {p0, v4}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->changeXiaoAiPowerGuideStatus(Ljava/lang/String;)V */
/* .line 282 */
} // .end local v0 # "voiceBundle":Landroid/os/Bundle;
} // .end local v7 # "triggered":Z
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 283 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 284 */
/* .local v0, "smartHomeBundle":Landroid/os/Bundle; */
(( android.os.Bundle ) v0 ).putString ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 286 */
(( android.os.Bundle ) v0 ).putBoolean ( v2, v6 ); // invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 287 */
final String v2 = "launch_smarthome"; // const-string v2, "launch_smarthome"
v1 = /* invoke-direct {p0, v1, v2, v0, v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 289 */
/* .local v1, "triggered":Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 290 */
/* iput-boolean v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mShouldSendPowerUpToSmartHome:Z */
/* .line 294 */
} // .end local v0 # "smartHomeBundle":Landroid/os/Bundle;
} // .end local v1 # "triggered":Z
} // :cond_1
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isSupportLongPressPowerGuide ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isSupportLongPressPowerGuide()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 295 */
v0 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).setVeryLongPressPowerBehavior ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V
/* .line 296 */
/* iput-boolean v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mIsPowerGuideTriggeredByLongPressPower:Z */
/* .line 298 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
protected Long getMiuiLongPressTimeoutMs ( ) {
/* .locals 2 */
/* .line 142 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->supportMiuiLongPress()Z */
/* if-nez v0, :cond_0 */
/* iget-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mGlobalActionKeyTimeout:J */
} // :cond_0
/* iget-wide v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mLongPressTimeout:J */
} // :goto_0
/* return-wide v0 */
} // .end method
protected Integer getMiuiMaxMultiPressCount ( ) {
/* .locals 5 */
/* .line 147 */
int v0 = 1; // const/4 v0, 0x1
/* .line 149 */
/* .local v0, "maxPressCount":I */
v1 = this.mActionMaxCountMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 150 */
/* .local v2, "actionMaxCountEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* check-cast v3, Ljava/lang/String; */
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).getFunction ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 151 */
/* .local v3, "function":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_0 */
final String v4 = "none"; // const-string v4, "none"
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_0 */
/* .line 152 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* if-lt v4, v0, :cond_0 */
/* .line 153 */
/* check-cast v4, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 156 */
} // .end local v2 # "actionMaxCountEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
} // .end local v3 # "function":Ljava/lang/String;
} // :cond_0
/* .line 157 */
} // :cond_1
v1 = this.mMiuiShortcutTriggerHelper;
v1 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).shouldLaunchSos ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->shouldLaunchSos()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 158 */
int v0 = 5; // const/4 v0, 0x5
/* .line 161 */
} // :cond_2
} // .end method
public void init ( ) {
/* .locals 1 */
/* .line 71 */
/* invoke-super {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->init()V */
/* .line 72 */
(( com.android.server.input.shortcut.singlekeyrule.PowerKeyRule ) p0 ).registerShortcutListener ( p0 ); // invoke-virtual {p0, p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->registerShortcutListener(Lcom/android/server/policy/MiuiShortcutObserver$MiuiShortcutListener;)V
/* .line 73 */
v0 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).setVeryLongPressPowerBehavior ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V
/* .line 74 */
return;
} // .end method
protected Boolean miuiSupportVeryLongPress ( ) {
/* .locals 1 */
/* .line 131 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->supportMiuiLongPress()Z */
} // .end method
protected void onMiuiKeyDown ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 83 */
v0 = this.mWindowManagerPolicy;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 84 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isTorchEnabled ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isTorchEnabled()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMiuiShortcutTriggerHelper;
/* .line 85 */
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).getPowerManager ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->getPowerManager()Landroid/os/PowerManager;
v0 = (( android.os.PowerManager ) v0 ).isInteractive ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z
/* if-nez v0, :cond_0 */
/* .line 86 */
v0 = this.mWindowManagerPolicy;
/* check-cast v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).setPowerKeyHandled ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setPowerKeyHandled(Z)V
/* .line 87 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 88 */
/* .local v0, "torchBundle":Landroid/os/Bundle; */
final String v2 = "extra_torch_enabled"; // const-string v2, "extra_torch_enabled"
int v3 = 0; // const/4 v3, 0x0
(( android.os.Bundle ) v0 ).putBoolean ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 89 */
/* const-string/jumbo v2, "skip_telecom_check" */
(( android.os.Bundle ) v0 ).putBoolean ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 90 */
/* const-string/jumbo v2, "triggered_by_power" */
/* const-string/jumbo v3, "turn_on_torch" */
/* invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z */
/* .line 95 */
} // .end local v0 # "torchBundle":Landroid/os/Bundle;
} // :cond_0
return;
} // .end method
protected void onMiuiLongPress ( Long p0 ) {
/* .locals 3 */
/* .param p1, "eventTime" # J */
/* .line 99 */
int v0 = 0; // const/4 v0, 0x0
/* .line 100 */
/* .local v0, "triggered":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->supportMiuiLongPress()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 101 */
v1 = this.mWindowManagerPolicy;
/* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).setPowerKeyHandled ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->setPowerKeyHandled(Z)V
/* .line 102 */
v0 = /* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerLongPress()Z */
/* .line 105 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 106 */
v1 = this.mOriginalPowerKeyRuleBridge;
(( com.android.server.policy.OriginalPowerKeyRuleBridge ) v1 ).onLongPress ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onLongPress(J)V
/* .line 109 */
} // :cond_1
return;
} // .end method
protected void onMiuiLongPressKeyUp ( android.view.KeyEvent p0 ) {
/* .locals 0 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 126 */
/* invoke-direct {p0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->workAtPowerUp()V */
/* .line 127 */
return;
} // .end method
protected void onMiuiMultiPress ( Long p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "downTime" # J */
/* .param p3, "count" # I */
/* .line 118 */
v0 = /* invoke-direct {p0, p3}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->triggerMultiPress(I)Z */
/* .line 119 */
/* .local v0, "triggered":Z */
/* if-nez v0, :cond_0 */
/* .line 120 */
v1 = this.mOriginalPowerKeyRuleBridge;
(( com.android.server.policy.OriginalPowerKeyRuleBridge ) v1 ).onMultiPress ( p1, p2, p3 ); // invoke-virtual {v1, p1, p2, p3}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onMultiPress(JI)V
/* .line 122 */
} // :cond_0
return;
} // .end method
protected void onMiuiPress ( Long p0 ) {
/* .locals 1 */
/* .param p1, "downTime" # J */
/* .line 78 */
v0 = this.mOriginalPowerKeyRuleBridge;
(( com.android.server.policy.OriginalPowerKeyRuleBridge ) v0 ).onPress ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onPress(J)V
/* .line 79 */
return;
} // .end method
protected void onMiuiVeryLongPress ( Long p0 ) {
/* .locals 1 */
/* .param p1, "eventTime" # J */
/* .line 113 */
v0 = this.mOriginalPowerKeyRuleBridge;
(( com.android.server.policy.OriginalPowerKeyRuleBridge ) v0 ).onVeryLongPress ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/policy/OriginalPowerKeyRuleBridge;->onVeryLongPress(J)V
/* .line 114 */
return;
} // .end method
public void onSingleChanged ( com.android.server.policy.MiuiSingleKeyRule p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "rule" # Lcom/android/server/policy/MiuiSingleKeyRule; */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 415 */
v0 = (( com.android.server.policy.MiuiSingleKeyRule ) p1 ).getPrimaryKey ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->getPrimaryKey()I
/* const/16 v1, 0x1a */
/* if-ne v0, v1, :cond_0 */
final String v0 = "long_press_power_key"; // const-string v0, "long_press_power_key"
android.provider.Settings$System .getUriFor ( v0 );
/* .line 416 */
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 417 */
v1 = this.mMiuiShortcutTriggerHelper;
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).setVeryLongPressPowerBehavior ( ); // invoke-virtual {v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setVeryLongPressPowerBehavior()V
/* .line 418 */
v1 = this.mMiuiShortcutTriggerHelper;
/* .line 419 */
(( com.android.server.policy.MiuiSingleKeyRule ) p1 ).getFunction ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 418 */
(( com.android.server.policy.MiuiShortcutTriggerHelper ) v1 ).setLongPressPowerBehavior ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->setLongPressPowerBehavior(Ljava/lang/String;)V
/* .line 421 */
} // :cond_0
return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "currentUserId" # I */
/* .param p2, "isNewUser" # Z */
/* .line 425 */
/* iput p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->mCurrentUserId:I */
/* .line 426 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->onUserSwitch(IZ)V */
/* .line 427 */
return;
} // .end method
