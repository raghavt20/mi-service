.class public Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;
.super Ljava/lang/Object;
.source "MiuiSingleKeyRuleManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiSingleKeyRuleManager"

.field private static volatile sMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

.field private final mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

.field private final mSingleKeyRuleHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/policy/MiuiSingleKeyRule;",
            ">;"
        }
    .end annotation
.end field

.field private final mSingleKeyRuleHashMapForAction:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/policy/MiuiSingleKeyRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/policy/MiuiShortcutTriggerHelper;Lcom/android/server/policy/SingleKeyGestureDetector;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .param p4, "singleKeyGestureDetector"    # Lcom/android/server/policy/SingleKeyGestureDetector;

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMapForAction:Ljava/util/HashMap;

    .line 32
    iput-object p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mHandler:Landroid/os/Handler;

    .line 33
    iput-object p2, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mContext:Landroid/content/Context;

    .line 34
    iput-object p3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    .line 35
    iput-object p4, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

    .line 36
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/MiuiShortcutTriggerHelper;Lcom/android/server/policy/SingleKeyGestureDetector;)Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "miuiShortcutTriggerHelper"    # Lcom/android/server/policy/MiuiShortcutTriggerHelper;
    .param p3, "singleKeyGestureDetector"    # Lcom/android/server/policy/SingleKeyGestureDetector;

    .line 41
    sget-object v0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->sMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    if-nez v0, :cond_1

    .line 42
    const-class v0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    monitor-enter v0

    .line 43
    :try_start_0
    sget-object v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->sMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    if-nez v1, :cond_0

    .line 44
    new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    invoke-direct {v1, p1, p0, p2, p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;-><init>(Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/policy/MiuiShortcutTriggerHelper;Lcom/android/server/policy/SingleKeyGestureDetector;)V

    sput-object v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->sMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    .line 47
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 49
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->sMiuiSingleKeyRuleManager:Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;

    return-object v0
.end method

.method static synthetic lambda$initSingleKeyRule$2(Ljava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/Integer;
    .param p1, "miuiSingleKeyRule"    # Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 127
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->init()V

    return-void
.end method

.method static synthetic lambda$onUserSwitch$0(IZLjava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;)V
    .locals 0
    .param p0, "currentUserId"    # I
    .param p1, "isNewUser"    # Z
    .param p2, "k"    # Ljava/lang/Integer;
    .param p3, "v"    # Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 100
    invoke-virtual {p3, p0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->onUserSwitch(IZ)V

    return-void
.end method

.method static synthetic lambda$resetShortcutSettings$1(Ljava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;)V
    .locals 2
    .param p0, "k"    # Ljava/lang/Integer;
    .param p1, "v"    # Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 104
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    return-void
.end method


# virtual methods
.method public addRule(Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiSingleKeyRule;)V
    .locals 5
    .param p1, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p2, "miuiSingleKeyRule"    # Lcom/android/server/policy/MiuiSingleKeyRule;

    .line 79
    invoke-virtual {p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I

    move-result v0

    .line 80
    .local v0, "primaryKey":I
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-virtual {p1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionAndDefaultFunctionMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 83
    .local v2, "actionMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMapForAction:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    .end local v2    # "actionMapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 116
    const-string v0, "MiuiSingleKeyRuleManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 119
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 120
    .local v1, "miuiSingleKeyRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;>;"
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 121
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 122
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/policy/MiuiSingleKeyRule;

    invoke-virtual {v2, p1, p2}, Lcom/android/server/policy/MiuiSingleKeyRule;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 123
    .end local v1    # "miuiSingleKeyRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/policy/MiuiSingleKeyRule;>;"
    goto :goto_0

    .line 124
    :cond_0
    return-void
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMapForAction:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiSingleKeyRule;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMiuiSingleKeyRule(ILcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)Lcom/android/server/policy/MiuiSingleKeyRule;
    .locals 8
    .param p1, "primaryKey"    # I
    .param p2, "miuiSingleKeyInfo"    # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;
    .param p3, "currentUserId"    # I

    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "miuiSingleKeyRule":Lcom/android/server/policy/MiuiSingleKeyRule;
    packed-switch p1, :pswitch_data_0

    .line 69
    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;

    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    move-object v1, v7

    move-object v4, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    move-object v0, v7

    goto :goto_0

    .line 65
    :pswitch_0
    new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;

    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p2, p3}, Lcom/android/server/input/shortcut/singlekeyrule/CameraKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V

    move-object v0, v1

    .line 67
    goto :goto_0

    .line 57
    :pswitch_1
    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;

    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    move-object v1, v7

    move-object v4, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    move-object v0, v7

    .line 59
    goto :goto_0

    .line 61
    :pswitch_2
    new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;

    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mMiuiShortcutTriggerHelper:Lcom/android/server/policy/MiuiShortcutTriggerHelper;

    move-object v1, v7

    move-object v4, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/singlekeyrule/VolumeDownKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;Lcom/android/server/policy/MiuiShortcutTriggerHelper;I)V

    move-object v0, v7

    .line 63
    nop

    .line 73
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "create single key rule,primary="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getPrimaryKey()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSingleKeyRuleManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyGestureDetector:Lcom/android/server/policy/SingleKeyGestureDetector;

    invoke-virtual {v1, v0}, Lcom/android/server/policy/SingleKeyGestureDetector;->removeRule(Lcom/android/server/policy/SingleKeyGestureDetector$SingleKeyRule;)V

    .line 75
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSingleKeyRuleForPrimaryKey(I)Lcom/android/server/policy/MiuiSingleKeyRule;
    .locals 2
    .param p1, "primaryKey"    # I

    .line 92
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiSingleKeyRule;

    return-object v0
.end method

.method public hasActionInSingleKeyMap(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMapForAction:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public initSingleKeyRule()V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 128
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 2
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 100
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, p2}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 101
    return-void
.end method

.method public removeRule(I)V
    .locals 2
    .param p1, "primaryKey"    # I

    .line 88
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-void
.end method

.method public resetShortcutSettings()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 105
    return-void
.end method

.method public updatePolicyFlag(I)V
    .locals 2
    .param p1, "policyFlags"    # I

    .line 112
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyRuleManager;->mSingleKeyRuleHashMap:Ljava/util/HashMap;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiSingleKeyRule;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/MiuiSingleKeyRule;->updatePolicyFlag(I)V

    .line 113
    return-void
.end method
