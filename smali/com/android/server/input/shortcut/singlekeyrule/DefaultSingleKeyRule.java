public class com.android.server.input.shortcut.singlekeyrule.DefaultSingleKeyRule extends com.android.server.policy.MiuiSingleKeyRule {
	 /* .source "DefaultSingleKeyRule.java" */
	 /* # instance fields */
	 private final java.util.Map mActionMapForType;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private java.lang.String mLongPressFunction;
private final com.android.server.policy.MiuiShortcutTriggerHelper mMiuiShortcutTriggerHelper;
private final Integer mPrimaryKey;
/* # direct methods */
public static void $r8$lambda$ADXaH2a8rZK6Bd9R8rk67FtZ9WU ( com.android.server.input.shortcut.singlekeyrule.DefaultSingleKeyRule p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3, Boolean p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
return;
} // .end method
public com.android.server.input.shortcut.singlekeyrule.DefaultSingleKeyRule ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "miuiSingleKeyInfo" # Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo; */
/* .param p4, "miuiShortcutTriggerHelper" # Lcom/android/server/policy/MiuiShortcutTriggerHelper; */
/* .param p5, "currentUserId" # I */
/* .line 29 */
/* invoke-direct {p0, p1, p2, p3, p5}, Lcom/android/server/policy/MiuiSingleKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;I)V */
/* .line 30 */
this.mContext = p1;
/* .line 31 */
this.mHandler = p2;
/* .line 32 */
v0 = (( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getPrimaryKey ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getPrimaryKey()I
/* iput v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mPrimaryKey:I */
/* .line 33 */
this.mMiuiShortcutTriggerHelper = p4;
/* .line 34 */
(( com.android.server.input.shortcut.singlekeyrule.MiuiSingleKeyInfo ) p3 ).getActionMapForType ( ); // invoke-virtual {p3}, Lcom/android/server/input/shortcut/singlekeyrule/MiuiSingleKeyInfo;->getActionMapForType()Ljava/util/Map;
this.mActionMapForType = v0;
/* .line 35 */
return;
} // .end method
private void lambda$postTriggerFunction$0 ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) { //synthethic
/* .locals 1 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 56 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
return;
} // .end method
private void postTriggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) {
/* .locals 8 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "bundle" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 55 */
v0 = this.mHandler;
/* new-instance v7, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule$$ExternalSyntheticLambda0; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
(( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 58 */
return;
} // .end method
private void triggerFunction ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 61 */
/* const/16 v0, 0x52 */
/* iget v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mPrimaryKey:I */
/* if-ne v0, v1, :cond_0 */
/* .line 62 */
v0 = this.mMiuiShortcutTriggerHelper;
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).isPressToAppSwitch ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->isPressToAppSwitch()Z
/* if-nez v0, :cond_0 */
v0 = this.mActionMapForType;
/* .line 63 */
final String v1 = "longPress"; // const-string v1, "longPress"
/* check-cast v0, Ljava/lang/String; */
(( com.android.server.input.shortcut.singlekeyrule.DefaultSingleKeyRule ) p0 ).getFunction ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
/* .line 62 */
final String v1 = "none"; // const-string v1, "none"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 64 */
	 return;
	 /* .line 67 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 68 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "close_app"; // const-string v1, "close_app"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 69 */
	 /* new-instance v1, Landroid/os/Bundle; */
	 /* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
	 /* move-object v0, v1 */
	 /* .line 70 */
	 /* const-string/jumbo v1, "shortcut_type" */
	 final String v2 = "phone_shortcut"; // const-string v2, "phone_shortcut"
	 (( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 73 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
/* .line 74 */
return;
} // .end method
/* # virtual methods */
protected Long getMiuiLongPressTimeoutMs ( ) {
/* .locals 2 */
/* .line 78 */
/* invoke-super {p0}, Lcom/android/server/policy/MiuiSingleKeyRule;->getMiuiLongPressTimeoutMs()J */
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
protected Boolean miuiSupportLongPress ( ) {
/* .locals 2 */
/* .line 47 */
v0 = this.mActionMapForType;
final String v1 = "longPress"; // const-string v1, "longPress"
/* check-cast v0, Ljava/lang/String; */
(( com.android.server.input.shortcut.singlekeyrule.DefaultSingleKeyRule ) p0 ).getFunction ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;
this.mLongPressFunction = v0;
/* .line 48 */
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_0 */
final String v0 = "none"; // const-string v0, "none"
v1 = this.mLongPressFunction;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = this.mMiuiShortcutTriggerHelper;
/* iget v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->mPrimaryKey:I */
/* .line 49 */
v0 = (( com.android.server.policy.MiuiShortcutTriggerHelper ) v0 ).supportAOSPTriggerFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutTriggerHelper;->supportAOSPTriggerFunction(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 48 */
} // :goto_0
} // .end method
protected void onMiuiLongPress ( Long p0 ) {
/* .locals 3 */
/* .param p1, "eventTime" # J */
/* .line 39 */
v0 = this.mLongPressFunction;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_0 */
final String v0 = "none"; // const-string v0, "none"
v1 = this.mLongPressFunction;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 41 */
v0 = this.mLongPressFunction;
v1 = this.mActionMapForType;
final String v2 = "longPress"; // const-string v2, "longPress"
/* check-cast v1, Ljava/lang/String; */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/DefaultSingleKeyRule;->triggerFunction(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 43 */
} // :cond_0
return;
} // .end method
