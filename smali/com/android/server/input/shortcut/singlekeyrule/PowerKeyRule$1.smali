.class Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;
.super Ljava/lang/Object;
.source "PowerKeyRule.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->doubleClickPowerRunnable()Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;


# direct methods
.method constructor <init>(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;

    .line 403
    iput-object p1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;->this$0:Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 406
    iget-object v0, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;->this$0:Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;

    const-string v1, "double_click_power_key"

    invoke-virtual {v0, v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->getFunction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, "doubleClickFunction":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;->this$0:Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;

    invoke-static {v1}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->-$$Nest$fgetmContext(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule$1;->this$0:Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;

    .line 408
    invoke-static {v2, v0}, Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;->-$$Nest$mgetTriggerDoubleClickReason(Lcom/android/server/input/shortcut/singlekeyrule/PowerKeyRule;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 407
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 409
    return-void
.end method
