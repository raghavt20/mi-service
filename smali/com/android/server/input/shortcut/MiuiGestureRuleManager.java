public class com.android.server.input.shortcut.MiuiGestureRuleManager {
	 /* .source "MiuiGestureRuleManager.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.input.shortcut.MiuiGestureRuleManager sGestureManager;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private final java.util.HashMap mGestureRuleHashMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/policy/MiuiGestureRule;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final android.os.Handler mHandler;
/* # direct methods */
private com.android.server.input.shortcut.MiuiGestureRuleManager ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 17 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mGestureRuleHashMap = v0;
/* .line 22 */
this.mContext = p1;
/* .line 23 */
this.mHandler = p2;
/* .line 24 */
return;
} // .end method
public static com.android.server.input.shortcut.MiuiGestureRuleManager getInstance ( android.content.Context p0, android.os.Handler p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .line 27 */
v0 = com.android.server.input.shortcut.MiuiGestureRuleManager.sGestureManager;
/* if-nez v0, :cond_1 */
/* .line 28 */
/* const-class v0, Lcom/android/server/input/shortcut/MiuiGestureRuleManager; */
/* monitor-enter v0 */
/* .line 29 */
try { // :try_start_0
	 v1 = com.android.server.input.shortcut.MiuiGestureRuleManager.sGestureManager;
	 /* if-nez v1, :cond_0 */
	 /* .line 30 */
	 /* new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager; */
	 /* invoke-direct {v1, p0, p1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
	 /* .line 32 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 35 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.shortcut.MiuiGestureRuleManager.sGestureManager;
} // .end method
static void lambda$initGestureRule$2 ( java.lang.String p0, com.android.server.policy.MiuiGestureRule p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "action" # Ljava/lang/String; */
/* .param p1, "miuiGestureRule" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 79 */
(( com.android.server.policy.MiuiGestureRule ) p1 ).init ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiGestureRule;->init()V
return;
} // .end method
static void lambda$onUserSwitch$0 ( Integer p0, Boolean p1, java.lang.String p2, com.android.server.policy.MiuiGestureRule p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "currentUserId" # I */
/* .param p1, "isNewUser" # Z */
/* .param p2, "k" # Ljava/lang/String; */
/* .param p3, "v" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 59 */
(( com.android.server.policy.MiuiGestureRule ) p3 ).onUserSwitch ( p0, p1 ); // invoke-virtual {p3, p0, p1}, Lcom/android/server/policy/MiuiGestureRule;->onUserSwitch(IZ)V
return;
} // .end method
static void lambda$resetDefaultFunction$1 ( java.lang.String p0, com.android.server.policy.MiuiGestureRule p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "k" # Ljava/lang/String; */
/* .param p1, "v" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 63 */
(( com.android.server.policy.MiuiGestureRule ) p1 ).getObserver ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiGestureRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.policy.MiuiShortcutObserver ) v0 ).setDefaultFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
return;
} // .end method
/* # virtual methods */
public void addRule ( java.lang.String p0, com.android.server.policy.MiuiGestureRule p1 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "miuiGestureRule" # Lcom/android/server/policy/MiuiGestureRule; */
/* .line 39 */
v0 = this.mGestureRuleHashMap;
(( java.util.HashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 40 */
return;
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 3 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 67 */
final String v0 = "MiuiGestureRuleManager"; // const-string v0, "MiuiGestureRuleManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 68 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 70 */
v0 = this.mGestureRuleHashMap;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 71 */
/* .local v1, "miuiGestureRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;>;" */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 72 */
/* check-cast v2, Ljava/lang/String; */
(( java.io.PrintWriter ) p2 ).print ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 73 */
final String v2 = "="; // const-string v2, "="
(( java.io.PrintWriter ) p2 ).print ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 74 */
/* check-cast v2, Lcom/android/server/policy/MiuiGestureRule; */
(( com.android.server.policy.MiuiGestureRule ) v2 ).getFunction ( ); // invoke-virtual {v2}, Lcom/android/server/policy/MiuiGestureRule;->getFunction()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 75 */
} // .end local v1 # "miuiGestureRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiGestureRule;>;"
/* .line 76 */
} // :cond_0
return;
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 47 */
v0 = this.mGestureRuleHashMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiGestureRule; */
(( com.android.server.policy.MiuiGestureRule ) v0 ).getFunction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiGestureRule;->getFunction()Ljava/lang/String;
} // .end method
public com.android.server.policy.MiuiGestureRule getGestureManager ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 51 */
v0 = this.mGestureRuleHashMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiGestureRule; */
} // .end method
public Boolean hasActionInGestureRuleMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 55 */
v0 = this.mGestureRuleHashMap;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
} // .end method
public void initGestureRule ( ) {
/* .locals 2 */
/* .line 79 */
v0 = this.mGestureRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda1;-><init>()V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 80 */
return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "currentUserId" # I */
/* .param p2, "isNewUser" # Z */
/* .line 59 */
v0 = this.mGestureRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1, p2}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda0;-><init>(IZ)V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 60 */
return;
} // .end method
public void removeRule ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 43 */
v0 = this.mGestureRuleHashMap;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 44 */
return;
} // .end method
public void resetDefaultFunction ( ) {
/* .locals 2 */
/* .line 63 */
v0 = this.mGestureRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v1}, Lcom/android/server/input/shortcut/MiuiGestureRuleManager$$ExternalSyntheticLambda2;-><init>()V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 64 */
return;
} // .end method
