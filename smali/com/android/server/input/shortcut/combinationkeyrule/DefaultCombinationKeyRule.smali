.class public Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;
.super Lcom/android/server/policy/MiuiCombinationRule;
.source "DefaultCombinationKeyRule.java"


# instance fields
.field private final mAction:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mFunction:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public static synthetic $r8$lambda$_pP24qLXw0UfJ0e2UkqUK1kjvbw(Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;IILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "primaryKey"    # I
    .param p4, "combinationKey"    # I
    .param p5, "action"    # Ljava/lang/String;
    .param p6, "defaultFunction"    # Ljava/lang/String;
    .param p7, "currentUserId"    # I

    .line 22
    invoke-direct/range {p0 .. p7}, Lcom/android/server/policy/MiuiCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;IILjava/lang/String;Ljava/lang/String;I)V

    .line 23
    iput-object p1, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mHandler:Landroid/os/Handler;

    .line 25
    iput-object p5, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mAction:Ljava/lang/String;

    .line 26
    return-void
.end method

.method private synthetic lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 52
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    return-void
.end method

.method private postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 8
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 51
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule$$ExternalSyntheticLambda0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 54
    return-void
.end method

.method private triggerFunction()V
    .locals 4

    .line 40
    const/4 v0, 0x0

    .line 41
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "close_app"

    iget-object v2, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mFunction:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v0, v1

    .line 43
    const-string/jumbo v1, "shortcut_type"

    const-string v2, "phone_shortcut"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mAction:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mFunction:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 47
    return-void
.end method


# virtual methods
.method protected miuiExecute()V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->triggerFunction()V

    .line 31
    return-void
.end method

.method protected miuiPreCondition()Z
    .locals 2

    .line 35
    invoke-virtual {p0}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->getFunction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mFunction:Ljava/lang/String;

    .line 36
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "none"

    iget-object v1, p0, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->mFunction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
