.class public Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;
.super Ljava/lang/Object;
.source "MiuiCombinationRuleManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiCombinationRuleManager"

.field private static volatile sMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;


# instance fields
.field private final mCombinationRuleHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/policy/MiuiCombinationRule;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/KeyCombinationManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "keyCombinationManager"    # Lcom/android/server/policy/KeyCombinationManager;

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    .line 27
    iput-object p1, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mHandler:Landroid/os/Handler;

    .line 29
    iput-object p3, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    .line 30
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/KeyCombinationManager;)Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "keyCombinationManager"    # Lcom/android/server/policy/KeyCombinationManager;

    .line 34
    sget-object v0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->sMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    if-nez v0, :cond_1

    .line 35
    const-class v0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    monitor-enter v0

    .line 36
    :try_start_0
    sget-object v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->sMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    if-nez v1, :cond_0

    .line 37
    new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/KeyCombinationManager;)V

    sput-object v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->sMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    .line 40
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 42
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->sMiuiCombinationRuleManager:Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;

    return-object v0
.end method

.method static synthetic lambda$initCombinationKeyRule$2(Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 0
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "miuiCombinationRule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 113
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->init()V

    return-void
.end method

.method static synthetic lambda$onUserSwitch$0(IZLjava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 0
    .param p0, "currentUserId"    # I
    .param p1, "isNewUser"    # Z
    .param p2, "k"    # Ljava/lang/String;
    .param p3, "v"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 82
    invoke-virtual {p3, p0, p1}, Lcom/android/server/policy/MiuiCombinationRule;->onUserSwitch(IZ)V

    return-void
.end method

.method static synthetic lambda$resetDefaultFunction$1(Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 2
    .param p0, "k"    # Ljava/lang/String;
    .param p1, "v"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 86
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V

    return-void
.end method


# virtual methods
.method public addRule(Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "miuiCombinationRule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 74
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 102
    const-string v0, "MiuiCombinationRuleManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 105
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 106
    .local v1, "miuiCombinationRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;>;"
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 107
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 108
    .end local v1    # "miuiCombinationRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;>;"
    goto :goto_0

    .line 109
    :cond_0
    return-void
.end method

.method public getCombinationRule(Ljava/lang/String;)Lcom/android/server/policy/MiuiCombinationRule;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiCombinationRule;

    return-object v0
.end method

.method public getFunction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/MiuiCombinationRule;

    invoke-virtual {v0}, Lcom/android/server/policy/MiuiCombinationRule;->getFunction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMiuiCombinationRule(IILjava/lang/String;Ljava/lang/String;I)Lcom/android/server/policy/MiuiCombinationRule;
    .locals 9
    .param p1, "primaryKey"    # I
    .param p2, "combinationKey"    # I
    .param p3, "action"    # Ljava/lang/String;
    .param p4, "function"    # Ljava/lang/String;
    .param p5, "currentUserId"    # I

    .line 48
    new-instance v8, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;

    iget-object v1, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mHandler:Landroid/os/Handler;

    move-object v0, v8

    move v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;IILjava/lang/String;Ljava/lang/String;I)V

    .line 51
    .local v0, "miuiCombinationRule":Lcom/android/server/policy/MiuiCombinationRule;
    invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->shouldHoldOnAOSPLogic(Lcom/android/server/policy/MiuiCombinationRule;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mKeyCombinationManager:Lcom/android/server/policy/KeyCombinationManager;

    invoke-virtual {v1, v0}, Lcom/android/server/policy/KeyCombinationManager;->removeRule(Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;)V

    .line 55
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "create miui combination rule,primaryKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " combinationKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiCombinationRuleManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    return-object v0
.end method

.method public hasActionInCombinationKeyMap(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public initCombinationKeyRule()V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 114
    return-void
.end method

.method public onUserSwitch(IZ)V
    .locals 2
    .param p1, "currentUserId"    # I
    .param p2, "isNewUser"    # Z

    .line 82
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, p2}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 83
    return-void
.end method

.method public removeRule(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public resetDefaultFunction()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->mCombinationRuleHashMap:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda2;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 87
    return-void
.end method

.method public shouldHoldOnAOSPLogic(Lcom/android/server/policy/MiuiCombinationRule;)Z
    .locals 3
    .param p1, "miuiCombinationRule"    # Lcom/android/server/policy/MiuiCombinationRule;

    .line 67
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getPrimaryKey()I

    move-result v0

    const/16 v1, 0x19

    const/16 v2, 0x1a

    if-ne v2, v0, :cond_0

    .line 68
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getCombinationKey()I

    move-result v0

    if-eq v1, v0, :cond_1

    :cond_0
    nop

    .line 69
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getPrimaryKey()I

    move-result v0

    if-ne v1, v0, :cond_2

    .line 70
    invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getCombinationKey()I

    move-result v0

    if-ne v2, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0
.end method
