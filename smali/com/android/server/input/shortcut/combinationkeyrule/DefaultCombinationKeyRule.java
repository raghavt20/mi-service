public class com.android.server.input.shortcut.combinationkeyrule.DefaultCombinationKeyRule extends com.android.server.policy.MiuiCombinationRule {
	 /* .source "DefaultCombinationKeyRule.java" */
	 /* # instance fields */
	 private final java.lang.String mAction;
	 private final android.content.Context mContext;
	 private java.lang.String mFunction;
	 private final android.os.Handler mHandler;
	 /* # direct methods */
	 public static void $r8$lambda$_pP24qLXw0UfJ0e2UkqUK1kjvbw ( com.android.server.input.shortcut.combinationkeyrule.DefaultCombinationKeyRule p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3, Boolean p4 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->lambda$postTriggerFunction$0(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
		 return;
	 } // .end method
	 public com.android.server.input.shortcut.combinationkeyrule.DefaultCombinationKeyRule ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "primaryKey" # I */
		 /* .param p4, "combinationKey" # I */
		 /* .param p5, "action" # Ljava/lang/String; */
		 /* .param p6, "defaultFunction" # Ljava/lang/String; */
		 /* .param p7, "currentUserId" # I */
		 /* .line 22 */
		 /* invoke-direct/range {p0 ..p7}, Lcom/android/server/policy/MiuiCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;IILjava/lang/String;Ljava/lang/String;I)V */
		 /* .line 23 */
		 this.mContext = p1;
		 /* .line 24 */
		 this.mHandler = p2;
		 /* .line 25 */
		 this.mAction = p5;
		 /* .line 26 */
		 return;
	 } // .end method
	 private void lambda$postTriggerFunction$0 ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) { //synthethic
		 /* .locals 1 */
		 /* .param p1, "function" # Ljava/lang/String; */
		 /* .param p2, "action" # Ljava/lang/String; */
		 /* .param p3, "bundle" # Landroid/os/Bundle; */
		 /* .param p4, "hapticFeedback" # Z */
		 /* .line 52 */
		 v0 = this.mContext;
		 com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
		 (( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
		 return;
	 } // .end method
	 private void postTriggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) {
		 /* .locals 8 */
		 /* .param p1, "action" # Ljava/lang/String; */
		 /* .param p2, "function" # Ljava/lang/String; */
		 /* .param p3, "bundle" # Landroid/os/Bundle; */
		 /* .param p4, "hapticFeedback" # Z */
		 /* .line 51 */
		 v0 = this.mHandler;
		 /* new-instance v7, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule$$ExternalSyntheticLambda0; */
		 /* move-object v1, v7 */
		 /* move-object v2, p0 */
		 /* move-object v3, p2 */
		 /* move-object v4, p1 */
		 /* move-object v5, p3 */
		 /* move v6, p4 */
		 /* invoke-direct/range {v1 ..v6}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
		 (( android.os.Handler ) v0 ).post ( v7 ); // invoke-virtual {v0, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
		 /* .line 54 */
		 return;
	 } // .end method
	 private void triggerFunction ( ) {
		 /* .locals 4 */
		 /* .line 40 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 41 */
		 /* .local v0, "bundle":Landroid/os/Bundle; */
		 final String v1 = "close_app"; // const-string v1, "close_app"
		 v2 = this.mFunction;
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 42 */
			 /* new-instance v1, Landroid/os/Bundle; */
			 /* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
			 /* move-object v0, v1 */
			 /* .line 43 */
			 /* const-string/jumbo v1, "shortcut_type" */
			 final String v2 = "phone_shortcut"; // const-string v2, "phone_shortcut"
			 (( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
			 /* .line 46 */
		 } // :cond_0
		 v1 = this.mAction;
		 v2 = this.mFunction;
		 int v3 = 1; // const/4 v3, 0x1
		 /* invoke-direct {p0, v1, v2, v0, v3}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->postTriggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V */
		 /* .line 47 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected void miuiExecute ( ) {
		 /* .locals 0 */
		 /* .line 30 */
		 /* invoke-direct {p0}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->triggerFunction()V */
		 /* .line 31 */
		 return;
	 } // .end method
	 protected Boolean miuiPreCondition ( ) {
		 /* .locals 2 */
		 /* .line 35 */
		 (( com.android.server.input.shortcut.combinationkeyrule.DefaultCombinationKeyRule ) p0 ).getFunction ( ); // invoke-virtual {p0}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;->getFunction()Ljava/lang/String;
		 this.mFunction = v0;
		 /* .line 36 */
		 v0 = 		 android.text.TextUtils .isEmpty ( v0 );
		 /* if-nez v0, :cond_0 */
		 final String v0 = "none"; // const-string v0, "none"
		 v1 = this.mFunction;
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* if-nez v0, :cond_0 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
