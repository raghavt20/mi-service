public class com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager {
	 /* .source "MiuiCombinationRuleManager.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager sMiuiCombinationRuleManager;
	 /* # instance fields */
	 private final java.util.HashMap mCombinationRuleHashMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/policy/MiuiCombinationRule;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private final com.android.server.policy.KeyCombinationManager mKeyCombinationManager;
/* # direct methods */
private com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "keyCombinationManager" # Lcom/android/server/policy/KeyCombinationManager; */
/* .line 26 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 22 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCombinationRuleHashMap = v0;
/* .line 27 */
this.mContext = p1;
/* .line 28 */
this.mHandler = p2;
/* .line 29 */
this.mKeyCombinationManager = p3;
/* .line 30 */
return;
} // .end method
public static com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager getInstance ( android.content.Context p0, android.os.Handler p1, com.android.server.policy.KeyCombinationManager p2 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "keyCombinationManager" # Lcom/android/server/policy/KeyCombinationManager; */
/* .line 34 */
v0 = com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager.sMiuiCombinationRuleManager;
/* if-nez v0, :cond_1 */
/* .line 35 */
/* const-class v0, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager; */
/* monitor-enter v0 */
/* .line 36 */
try { // :try_start_0
	 v1 = com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager.sMiuiCombinationRuleManager;
	 /* if-nez v1, :cond_0 */
	 /* .line 37 */
	 /* new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager; */
	 /* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/policy/KeyCombinationManager;)V */
	 /* .line 40 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 42 */
} // :cond_1
} // :goto_0
v0 = com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager.sMiuiCombinationRuleManager;
} // .end method
static void lambda$initCombinationKeyRule$2 ( java.lang.String p0, com.android.server.policy.MiuiCombinationRule p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "action" # Ljava/lang/String; */
/* .param p1, "miuiCombinationRule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 113 */
(( com.android.server.policy.MiuiCombinationRule ) p1 ).init ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->init()V
return;
} // .end method
static void lambda$onUserSwitch$0 ( Integer p0, Boolean p1, java.lang.String p2, com.android.server.policy.MiuiCombinationRule p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "currentUserId" # I */
/* .param p1, "isNewUser" # Z */
/* .param p2, "k" # Ljava/lang/String; */
/* .param p3, "v" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 82 */
(( com.android.server.policy.MiuiCombinationRule ) p3 ).onUserSwitch ( p0, p1 ); // invoke-virtual {p3, p0, p1}, Lcom/android/server/policy/MiuiCombinationRule;->onUserSwitch(IZ)V
return;
} // .end method
static void lambda$resetDefaultFunction$1 ( java.lang.String p0, com.android.server.policy.MiuiCombinationRule p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "k" # Ljava/lang/String; */
/* .param p1, "v" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 86 */
(( com.android.server.policy.MiuiCombinationRule ) p1 ).getObserver ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getObserver()Lcom/android/server/policy/MiuiShortcutObserver;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.policy.MiuiShortcutObserver ) v0 ).setDefaultFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/policy/MiuiShortcutObserver;->setDefaultFunction(Z)V
return;
} // .end method
/* # virtual methods */
public void addRule ( java.lang.String p0, com.android.server.policy.MiuiCombinationRule p1 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "miuiCombinationRule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 74 */
v0 = this.mCombinationRuleHashMap;
(( java.util.HashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 75 */
return;
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 3 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 102 */
final String v0 = "MiuiCombinationRuleManager"; // const-string v0, "MiuiCombinationRuleManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 103 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 105 */
v0 = this.mCombinationRuleHashMap;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 106 */
/* .local v1, "miuiCombinationRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;>;" */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 107 */
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 108 */
} // .end local v1 # "miuiCombinationRuleEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/policy/MiuiCombinationRule;>;"
/* .line 109 */
} // :cond_0
return;
} // .end method
public com.android.server.policy.MiuiCombinationRule getCombinationRule ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 90 */
v0 = this.mCombinationRuleHashMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiCombinationRule; */
} // .end method
public java.lang.String getFunction ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 98 */
v0 = this.mCombinationRuleHashMap;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/policy/MiuiCombinationRule; */
(( com.android.server.policy.MiuiCombinationRule ) v0 ).getFunction ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiCombinationRule;->getFunction()Ljava/lang/String;
} // .end method
public com.android.server.policy.MiuiCombinationRule getMiuiCombinationRule ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3, Integer p4 ) {
/* .locals 9 */
/* .param p1, "primaryKey" # I */
/* .param p2, "combinationKey" # I */
/* .param p3, "action" # Ljava/lang/String; */
/* .param p4, "function" # Ljava/lang/String; */
/* .param p5, "currentUserId" # I */
/* .line 48 */
/* new-instance v8, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule; */
v1 = this.mContext;
v2 = this.mHandler;
/* move-object v0, v8 */
/* move v3, p1 */
/* move v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* move v7, p5 */
/* invoke-direct/range {v0 ..v7}, Lcom/android/server/input/shortcut/combinationkeyrule/DefaultCombinationKeyRule;-><init>(Landroid/content/Context;Landroid/os/Handler;IILjava/lang/String;Ljava/lang/String;I)V */
/* .line 51 */
/* .local v0, "miuiCombinationRule":Lcom/android/server/policy/MiuiCombinationRule; */
v1 = (( com.android.server.input.shortcut.combinationkeyrule.MiuiCombinationRuleManager ) p0 ).shouldHoldOnAOSPLogic ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager;->shouldHoldOnAOSPLogic(Lcom/android/server/policy/MiuiCombinationRule;)Z
/* if-nez v1, :cond_0 */
/* .line 52 */
v1 = this.mKeyCombinationManager;
(( com.android.server.policy.KeyCombinationManager ) v1 ).removeRule ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/policy/KeyCombinationManager;->removeRule(Lcom/android/server/policy/KeyCombinationManager$TwoKeysCombinationRule;)V
/* .line 55 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "create miui combination rule,primaryKey="; // const-string v2, "create miui combination rule,primaryKey="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " combinationKey="; // const-string v2, " combinationKey="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiCombinationRuleManager"; // const-string v2, "MiuiCombinationRuleManager"
android.util.Slog .d ( v2,v1 );
/* .line 57 */
} // .end method
public Boolean hasActionInCombinationKeyMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 94 */
v0 = this.mCombinationRuleHashMap;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
} // .end method
public void initCombinationKeyRule ( ) {
/* .locals 2 */
/* .line 112 */
v0 = this.mCombinationRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda0;-><init>()V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 114 */
return;
} // .end method
public void onUserSwitch ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "currentUserId" # I */
/* .param p2, "isNewUser" # Z */
/* .line 82 */
v0 = this.mCombinationRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p1, p2}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda1;-><init>(IZ)V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 83 */
return;
} // .end method
public void removeRule ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 78 */
v0 = this.mCombinationRuleHashMap;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 79 */
return;
} // .end method
public void resetDefaultFunction ( ) {
/* .locals 2 */
/* .line 86 */
v0 = this.mCombinationRuleHashMap;
/* new-instance v1, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v1}, Lcom/android/server/input/shortcut/combinationkeyrule/MiuiCombinationRuleManager$$ExternalSyntheticLambda2;-><init>()V */
(( java.util.HashMap ) v0 ).forEach ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 87 */
return;
} // .end method
public Boolean shouldHoldOnAOSPLogic ( com.android.server.policy.MiuiCombinationRule p0 ) {
/* .locals 3 */
/* .param p1, "miuiCombinationRule" # Lcom/android/server/policy/MiuiCombinationRule; */
/* .line 67 */
v0 = (( com.android.server.policy.MiuiCombinationRule ) p1 ).getPrimaryKey ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getPrimaryKey()I
/* const/16 v1, 0x19 */
/* const/16 v2, 0x1a */
/* if-ne v2, v0, :cond_0 */
/* .line 68 */
v0 = (( com.android.server.policy.MiuiCombinationRule ) p1 ).getCombinationKey ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getCombinationKey()I
/* if-eq v1, v0, :cond_1 */
} // :cond_0
/* nop */
/* .line 69 */
v0 = (( com.android.server.policy.MiuiCombinationRule ) p1 ).getPrimaryKey ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getPrimaryKey()I
/* if-ne v1, v0, :cond_2 */
/* .line 70 */
v0 = (( com.android.server.policy.MiuiCombinationRule ) p1 ).getCombinationKey ( ); // invoke-virtual {p1}, Lcom/android/server/policy/MiuiCombinationRule;->getCombinationKey()I
/* if-ne v2, v0, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 67 */
} // :goto_0
} // .end method
