public class com.android.server.input.KeyboardCombinationManagerStubImpl implements com.android.server.input.KeyboardCombinationManagerStub {
	 /* .source "KeyboardCombinationManagerStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.policy.KeyboardCombinationRule mActiveRule;
	 private android.content.Context mContext;
	 private android.os.Handler mHandler;
	 public android.util.LongSparseArray mKeyboardShortcutRules;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/LongSparseArray<", */
	 /* "Lcom/android/server/policy/KeyboardCombinationRule;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private com.miui.server.input.util.MiuiCustomizeShortCutUtils mMiuiCustomizeShortCutUtils;
private com.android.server.policy.MiuiKeyInterceptExtend mMiuiKeyInterceptExtend;
/* # direct methods */
public static void $r8$lambda$2w5V772XSCDu1mgMLlsvAxE6aEA ( com.android.server.input.KeyboardCombinationManagerStubImpl p0, com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$addRule$1(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
return;
} // .end method
public static void $r8$lambda$4vtwHlbIr_VW4pfZ_E33cZRukqg ( com.android.server.input.KeyboardCombinationManagerStubImpl p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$resetRule$4()V */
return;
} // .end method
public static void $r8$lambda$7-ODEWhdKSOa1IZoY3j0Ue65XCs ( com.android.server.input.KeyboardCombinationManagerStubImpl p0, com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$updateRule$3(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
return;
} // .end method
public static void $r8$lambda$AmGzw_bi01fpKeH9wvbSFffz428 ( com.android.server.input.KeyboardCombinationManagerStubImpl p0, com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$removeRule$2(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
return;
} // .end method
public static Boolean $r8$lambda$cOnXph6CTg7Fp9_KLk74HeGRI48 ( com.android.server.input.KeyboardCombinationManagerStubImpl p0, Integer p1, Integer p2, com.android.server.policy.KeyboardCombinationRule p3 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->lambda$interceptKeyboardCombination$0(IILcom/android/server/policy/KeyboardCombinationRule;)Z */
} // .end method
public com.android.server.input.KeyboardCombinationManagerStubImpl ( ) {
/* .locals 1 */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 22 */
/* new-instance v0, Landroid/util/LongSparseArray; */
/* invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V */
this.mKeyboardShortcutRules = v0;
return;
} // .end method
private Boolean forAllKeyboardCombinationRules ( com.android.internal.util.ToBooleanFunction p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/internal/util/ToBooleanFunction<", */
/* "Lcom/android/server/policy/KeyboardCombinationRule;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 142 */
/* .local p1, "callback":Lcom/android/internal/util/ToBooleanFunction;, "Lcom/android/internal/util/ToBooleanFunction<Lcom/android/server/policy/KeyboardCombinationRule;>;" */
v0 = this.mKeyboardShortcutRules;
v0 = (( android.util.LongSparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I
/* .line 143 */
/* .local v0, "count":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "index":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 144 */
v2 = this.mKeyboardShortcutRules;
(( android.util.LongSparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/policy/KeyboardCombinationRule; */
/* .line 145 */
v3 = /* .local v2, "rule":Lcom/android/server/policy/KeyboardCombinationRule; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 146 */
int v3 = 1; // const/4 v3, 0x1
/* .line 143 */
} // .end local v2 # "rule":Lcom/android/server/policy/KeyboardCombinationRule;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 149 */
} // .end local v1 # "index":I
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static com.android.server.input.KeyboardCombinationManagerStubImpl getInstance ( ) {
/* .locals 1 */
/* .line 30 */
com.android.server.input.KeyboardCombinationManagerStub .get ( );
/* check-cast v0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl; */
} // .end method
private Integer getMetaState ( android.view.KeyEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 89 */
int v0 = 0; // const/4 v0, 0x0
/* .line 90 */
/* .local v0, "metaState":I */
v1 = (( android.view.KeyEvent ) p1 ).isCtrlPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isCtrlPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 91 */
/* or-int/lit16 v0, v0, 0x1000 */
/* .line 93 */
} // :cond_0
v1 = (( android.view.KeyEvent ) p1 ).isAltPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 94 */
v1 = (( android.view.KeyEvent ) p1 ).getMetaState ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I
/* and-int/lit8 v1, v1, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 95 */
/* or-int/lit8 v0, v0, 0x22 */
/* .line 96 */
} // :cond_1
v1 = (( android.view.KeyEvent ) p1 ).getMetaState ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I
/* and-int/lit8 v1, v1, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 97 */
/* or-int/lit8 v0, v0, 0x12 */
/* .line 100 */
} // :cond_2
} // :goto_0
v1 = (( android.view.KeyEvent ) p1 ).isShiftPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 101 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 103 */
} // :cond_3
v1 = (( android.view.KeyEvent ) p1 ).isMetaPressed ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->isMetaPressed()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 104 */
/* const/high16 v1, 0x10000 */
/* or-int/2addr v0, v1 */
/* .line 106 */
} // :cond_4
} // .end method
private void lambda$addRule$1 ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 111 */
v0 = this.mKeyboardShortcutRules;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v1 */
/* new-instance v3, Lcom/android/server/policy/KeyboardCombinationRule; */
v4 = this.mContext;
v5 = this.mHandler;
/* invoke-direct {v3, v4, v5, p1}, Lcom/android/server/policy/KeyboardCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
(( android.util.LongSparseArray ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 113 */
return;
} // .end method
private Boolean lambda$interceptKeyboardCombination$0 ( Integer p0, Integer p1, com.android.server.policy.KeyboardCombinationRule p2 ) { //synthethic
/* .locals 1 */
/* .param p1, "keyCode" # I */
/* .param p2, "metaState" # I */
/* .param p3, "rule" # Lcom/android/server/policy/KeyboardCombinationRule; */
/* .line 72 */
v0 = (( com.android.server.policy.KeyboardCombinationRule ) p3 ).shouldInterceptKeys ( p1, p2 ); // invoke-virtual {p3, p1, p2}, Lcom/android/server/policy/KeyboardCombinationRule;->shouldInterceptKeys(II)Z
/* if-nez v0, :cond_0 */
/* .line 73 */
int v0 = 0; // const/4 v0, 0x0
this.mActiveRule = v0;
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
/* .line 76 */
} // :cond_0
this.mActiveRule = p3;
/* .line 77 */
(( com.android.server.policy.KeyboardCombinationRule ) p3 ).execute ( ); // invoke-virtual {p3}, Lcom/android/server/policy/KeyboardCombinationRule;->execute()V
/* .line 78 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void lambda$removeRule$2 ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 118 */
v0 = this.mKeyboardShortcutRules;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v1 */
(( android.util.LongSparseArray ) v0 ).delete ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->delete(J)V
/* .line 119 */
return;
} // .end method
private void lambda$resetRule$4 ( ) { //synthethic
/* .locals 1 */
/* .line 134 */
v0 = this.mKeyboardShortcutRules;
(( android.util.LongSparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V
/* .line 135 */
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) p0 ).initRules ( ); // invoke-virtual {p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->initRules()V
/* .line 136 */
return;
} // .end method
private void lambda$updateRule$3 ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 124 */
v0 = this.mKeyboardShortcutRules;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getHistoryKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getHistoryKeyCode()J
/* move-result-wide v1 */
(( android.util.LongSparseArray ) v0 ).delete ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->delete(J)V
/* .line 125 */
v0 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).isEnable ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 126 */
v0 = this.mKeyboardShortcutRules;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v1 */
/* new-instance v3, Lcom/android/server/policy/KeyboardCombinationRule; */
v4 = this.mContext;
v5 = this.mHandler;
/* invoke-direct {v3, v4, v5, p1}, Lcom/android/server/policy/KeyboardCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
(( android.util.LongSparseArray ) v0 ).put ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 129 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void addRule ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 110 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 114 */
return;
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 153 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 154 */
final String v0 = "KeyboardCombinationManagerStubImpl"; // const-string v0, "KeyboardCombinationManagerStubImpl"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 155 */
final String v0 = "mKeyboardShortcutRules ="; // const-string v0, "mKeyboardShortcutRules ="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 156 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mKeyboardShortcutRules;
v1 = (( android.util.LongSparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I
/* if-ge v0, v1, :cond_0 */
/* .line 157 */
v1 = this.mKeyboardShortcutRules;
(( android.util.LongSparseArray ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/policy/KeyboardCombinationRule; */
(( com.android.server.policy.KeyboardCombinationRule ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/server/policy/KeyboardCombinationRule;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 156 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 159 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 35 */
this.mContext = p1;
/* .line 36 */
com.android.server.input.MiuiInputThread .getHandler ( );
this.mHandler = v0;
/* .line 37 */
v0 = this.mContext;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .getInstance ( v0 );
this.mMiuiCustomizeShortCutUtils = v0;
/* .line 38 */
v0 = this.mContext;
com.android.server.policy.MiuiKeyInterceptExtend .getInstance ( v0 );
this.mMiuiKeyInterceptExtend = v0;
/* .line 39 */
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) p0 ).initRules ( ); // invoke-virtual {p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->initRules()V
/* .line 40 */
return;
} // .end method
public void initRules ( ) {
/* .locals 7 */
/* .line 43 */
v0 = this.mMiuiCustomizeShortCutUtils;
/* .line 44 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) v0 ).getMiuiKeyboardShortcutInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->getMiuiKeyboardShortcutInfo()Landroid/util/LongSparseArray;
/* .line 45 */
/* .local v0, "infos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( android.util.LongSparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I
/* if-ge v1, v2, :cond_1 */
/* .line 46 */
(( android.util.LongSparseArray ) v0 ).valueAt ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 47 */
/* .local v2, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v3 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v2 ).isEnable ( ); // invoke-virtual {v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 48 */
/* new-instance v3, Lcom/android/server/policy/KeyboardCombinationRule; */
v4 = this.mContext;
v5 = this.mHandler;
/* invoke-direct {v3, v4, v5, v2}, Lcom/android/server/policy/KeyboardCombinationRule;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
/* .line 49 */
/* .local v3, "rule":Lcom/android/server/policy/KeyboardCombinationRule; */
v4 = this.mKeyboardShortcutRules;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v2 ).getShortcutKeyCode ( ); // invoke-virtual {v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v5 */
(( android.util.LongSparseArray ) v4 ).put ( v5, v6, v3 ); // invoke-virtual {v4, v5, v6, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 45 */
} // .end local v2 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
} // .end local v3 # "rule":Lcom/android/server/policy/KeyboardCombinationRule;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 52 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
public Boolean interceptKey ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 56 */
v0 = this.mMiuiKeyInterceptExtend;
v0 = (( com.android.server.policy.MiuiKeyInterceptExtend ) v0 ).getKeyboardShortcutEnable ( ); // invoke-virtual {v0}, Lcom/android/server/policy/MiuiKeyInterceptExtend;->getKeyboardShortcutEnable()Z
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 57 */
this.mActiveRule = v2;
/* .line 58 */
/* .line 60 */
} // :cond_0
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 61 */
/* .local v0, "keyCode":I */
v3 = /* invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getMetaState(Landroid/view/KeyEvent;)I */
/* .line 62 */
/* .local v3, "metaState":I */
/* if-nez v3, :cond_1 */
/* .line 63 */
this.mActiveRule = v2;
/* .line 64 */
/* .line 66 */
} // :cond_1
v1 = (( com.android.server.input.KeyboardCombinationManagerStubImpl ) p0 ).interceptKeyboardCombination ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->interceptKeyboardCombination(II)Z
} // .end method
public Boolean interceptKeyboardCombination ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "keyCode" # I */
/* .param p2, "metaState" # I */
/* .line 71 */
/* new-instance v0, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;II)V */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->forAllKeyboardCombinationRules(Lcom/android/internal/util/ToBooleanFunction;)Z */
} // .end method
public Boolean isKeyConsumed ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 83 */
v0 = this.mActiveRule;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 84 */
v1 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getMetaState(Landroid/view/KeyEvent;)I */
v0 = (( com.android.server.policy.KeyboardCombinationRule ) v0 ).shouldInterceptKeys ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/policy/KeyboardCombinationRule;->shouldInterceptKeys(II)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 83 */
} // :goto_0
} // .end method
public void removeRule ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 117 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 120 */
return;
} // .end method
public void resetRule ( ) {
/* .locals 2 */
/* .line 133 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 137 */
return;
} // .end method
public void updateRule ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 123 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/input/KeyboardCombinationManagerStubImpl;Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 130 */
return;
} // .end method
