.class public final Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;
.super Ljava/lang/Object;
.source "ScoutSystemMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ScoutSystemMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ScoutHandlerChecker"
.end annotation


# instance fields
.field private mCompleted:Z

.field private mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

.field private final mHandler:Landroid/os/Handler;

.field private final mMonitorQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/Watchdog$Monitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mMonitors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/Watchdog$Monitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private mPauseCount:I

.field private mStartTime:J

.field private final mWaitMax:J

.field final synthetic this$0:Lcom/android/server/ScoutSystemMonitor;


# direct methods
.method constructor <init>(Lcom/android/server/ScoutSystemMonitor;Landroid/os/Handler;Ljava/lang/String;J)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/ScoutSystemMonitor;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "waitMaxMillis"    # J

    .line 701
    iput-object p1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 694
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitors:Ljava/util/ArrayList;

    .line 695
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitorQueue:Ljava/util/ArrayList;

    .line 702
    iput-object p2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mHandler:Landroid/os/Handler;

    .line 703
    iput-object p3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    .line 704
    iput-wide p4, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mWaitMax:J

    .line 705
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    .line 706
    return-void
.end method


# virtual methods
.method addMonitorLocked(Lcom/android/server/Watchdog$Monitor;)V
    .locals 1
    .param p1, "monitor"    # Lcom/android/server/Watchdog$Monitor;

    .line 709
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitorQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    return-void
.end method

.method public describeBlockedStateLocked()Ljava/lang/String;
    .locals 4

    .line 771
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    const-string v1, ")"

    const-string v2, " ("

    if-nez v0, :cond_0

    .line 772
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Blocked in handler on "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 774
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Blocked in monitor "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " on "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 775
    invoke-virtual {p0}, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 774
    return-object v0
.end method

.method public getCompletionStateLocked()I
    .locals 6

    .line 741
    iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    if-eqz v0, :cond_0

    .line 742
    const/4 v0, 0x0

    return v0

    .line 744
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mStartTime:J

    sub-long/2addr v0, v2

    .line 745
    .local v0, "latency":J
    iget-wide v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mWaitMax:J

    const-wide/16 v4, 0x2

    div-long v4, v2, v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 746
    const/4 v2, 0x1

    return v2

    .line 747
    :cond_1
    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 748
    const/4 v2, 0x2

    return v2

    .line 751
    .end local v0    # "latency":J
    :cond_2
    const/4 v0, 0x3

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 767
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getThread()Ljava/lang/Thread;
    .locals 1

    .line 755
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method

.method public getThreadTid()I
    .locals 2

    .line 759
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getMessageMonitor()Landroid/os/perfdebug/MessageMonitor;

    move-result-object v0

    .line 760
    .local v0, "mMonitor":Landroid/os/perfdebug/MessageMonitor;
    if-eqz v0, :cond_0

    .line 761
    invoke-virtual {v0}, Landroid/os/perfdebug/MessageMonitor;->getThreadTid()I

    move-result v1

    return v1

    .line 763
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isHalfLocked()Z
    .locals 8

    .line 737
    iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mStartTime:J

    iget-wide v4, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mWaitMax:J

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOverdueLocked()Z
    .locals 6

    .line 733
    iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mStartTime:J

    iget-wide v4, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mWaitMax:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public pauseLocked(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .line 796
    iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    .line 797
    iput-boolean v1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    .line 798
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Pausing HandlerChecker: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Pause count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ScoutSystemMonitor"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    return-void
.end method

.method public resumeLocked(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 803
    iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    const-string v1, "ScoutSystemMonitor"

    if-lez v0, :cond_0

    .line 804
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    .line 805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resuming HandlerChecker: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for reason: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Pause count: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 808
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already resumed HandlerChecker: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    :goto_0
    return-void
.end method

.method public run()V
    .locals 4

    .line 781
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 782
    .local v0, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 783
    iget-object v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v2}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmScoutSysLock(Lcom/android/server/ScoutSystemMonitor;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 784
    :try_start_0
    iget-object v3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitors:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/Watchdog$Monitor;

    iput-object v3, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    .line 785
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 786
    invoke-interface {v3}, Lcom/android/server/Watchdog$Monitor;->monitor()V

    .line 782
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 785
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 789
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->this$0:Lcom/android/server/ScoutSystemMonitor;

    invoke-static {v1}, Lcom/android/server/ScoutSystemMonitor;->-$$Nest$fgetmScoutSysLock(Lcom/android/server/ScoutSystemMonitor;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 790
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    .line 791
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    .line 792
    monitor-exit v1

    .line 793
    return-void

    .line 792
    :catchall_1
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2
.end method

.method public scheduleCheckLocked()V
    .locals 2

    .line 713
    iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitors:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitorQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 715
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitorQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mMonitors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/MessageQueue;->isPolling()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mPauseCount:I

    if-lez v0, :cond_3

    .line 719
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    .line 720
    return-void

    .line 722
    :cond_3
    iget-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    if-nez v0, :cond_4

    .line 723
    return-void

    .line 726
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCompleted:Z

    .line 727
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mCurrentMonitor:Lcom/android/server/Watchdog$Monitor;

    .line 728
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mStartTime:J

    .line 729
    iget-object v0, p0, Lcom/android/server/ScoutSystemMonitor$ScoutHandlerChecker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 730
    return-void
.end method
