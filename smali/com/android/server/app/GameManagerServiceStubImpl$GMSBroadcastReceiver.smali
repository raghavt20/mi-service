.class Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/GameManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GMSBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/app/GameManagerServiceStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/app/GameManagerServiceStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/app/GameManagerServiceStubImpl;

    .line 862
    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 866
    const-string v0, "GameManagerServiceStub"

    const-string v1, "ACTION_BOOT_COMPLETED"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    if-ne v0, v1, :cond_0

    .line 868
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 869
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    const v1, 0x1b209

    invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z

    .line 872
    :cond_0
    return-void
.end method
