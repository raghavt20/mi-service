public class com.android.server.app.GmsUtil {
	 /* .source "GmsUtil.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.app.GmsUtil ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.lang.String calcuRatio ( Integer p0, Integer p1 ) {
		 /* .locals 9 */
		 /* .param p0, "targetWidth" # I */
		 /* .param p1, "currentWidth" # I */
		 /* .line 33 */
		 /* new-instance v0, Ljava/math/BigDecimal; */
		 /* int-to-float v1, p0 */
		 /* const/high16 v2, 0x3f800000 # 1.0f */
		 /* mul-float/2addr v1, v2 */
		 /* int-to-float v3, p1 */
		 /* div-float/2addr v1, v3 */
		 /* float-to-double v3, v1 */
		 /* invoke-direct {v0, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V */
		 /* .line 34 */
		 /* .local v0, "bigDecimal":Ljava/math/BigDecimal; */
		 int v1 = 2; // const/4 v1, 0x2
		 v3 = java.math.RoundingMode.UP;
		 (( java.math.BigDecimal ) v0 ).setScale ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
		 v1 = 		 (( java.math.BigDecimal ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F
		 /* .line 35 */
		 /* .local v1, "ratio":F */
		 /* cmpl-float v3, v1, v2 */
		 /* if-ltz v3, :cond_0 */
		 /* .line 36 */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "ratio >= 1 targetWidth = "; // const-string v3, "ratio >= 1 targetWidth = "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v3 = " currentWidth = "; // const-string v3, " currentWidth = "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v3 = "GameManagerServiceStub"; // const-string v3, "GameManagerServiceStub"
		 android.util.Slog .d ( v3,v2 );
		 /* .line 37 */
		 final String v2 = "disable"; // const-string v2, "disable"
		 /* .line 39 */
	 } // :cond_0
	 /* const v3, 0x3f19999a # 0.6f */
	 /* cmpl-float v4, v1, v3 */
	 /* if-lez v4, :cond_2 */
	 /* .line 40 */
	 /* const/high16 v4, 0x42c80000 # 100.0f */
	 /* mul-float v5, v1, v4 */
	 /* float-to-int v5, v5 */
	 /* .line 41 */
	 /* .local v5, "raTemp":I */
	 /* rem-int/lit8 v6, v5, 0xa */
	 /* .line 42 */
	 /* .local v6, "lastDigital":I */
	 /* div-int/lit8 v7, v5, 0xa */
	 /* .line 43 */
	 /* .local v7, "firstDigital":I */
	 if ( v6 != null) { // if-eqz v6, :cond_1
		 /* .line 44 */
		 int v6 = 5; // const/4 v6, 0x5
		 /* .line 46 */
	 } // :cond_1
	 /* mul-int/lit8 v8, v7, 0xa */
	 /* add-int/2addr v8, v6 */
	 /* .line 47 */
} // .end local v5 # "raTemp":I
/* .local v8, "raTemp":I */
/* int-to-float v5, v8 */
/* mul-float/2addr v5, v2 */
/* div-float v1, v5, v4 */
/* .line 49 */
} // .end local v6 # "lastDigital":I
} // .end local v7 # "firstDigital":I
} // .end local v8 # "raTemp":I
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = java.lang.Math .max ( v1,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v3 = ""; // const-string v3, ""
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static Boolean checkValidApp ( java.lang.String p0, com.android.server.app.GameManagerServiceStubImpl$DownscaleCloudData p1 ) {
/* .locals 2 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "downscaleCloudData" # Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* .line 133 */
v0 = android.text.TextUtils .isEmpty ( p0 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 134 */
/* .line 136 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.apps;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 137 */
v0 = this.apps;
(( java.util.HashMap ) v0 ).get ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
/* .line 138 */
/* .local v0, "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 139 */
/* .line 142 */
} // .end local v0 # "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static java.lang.String getTargetRatioForPad ( com.android.server.app.GameManagerServiceStubImpl$AppItem p0, Boolean p1, com.android.server.app.GameManagerServiceStubImpl$DownscaleCloudData p2, Boolean p3 ) {
/* .locals 11 */
/* .param p0, "appItem" # Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
/* .param p1, "mPowerSaving" # Z */
/* .param p2, "mDownscaleCloudData" # Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* .param p3, "isDebugPowerSave" # Z */
/* .line 56 */
/* iget v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->mode:I */
/* .line 57 */
/* .local v0, "mode":I */
final String v1 = "disable"; // const-string v1, "disable"
/* if-nez v0, :cond_0 */
/* .line 58 */
/* .line 60 */
} // :cond_0
/* iget v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->systemVersion:I */
/* .line 61 */
/* .local v2, "systemVersion":I */
/* if-nez v2, :cond_1 */
/* .line 62 */
/* .line 64 */
} // :cond_1
/* iget v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->appVersion:I */
/* .line 65 */
/* .local v3, "appVersion":I */
int v4 = 1; // const/4 v4, 0x1
/* if-le v3, v4, :cond_2 */
/* .line 66 */
/* .line 70 */
} // :cond_2
/* and-int/lit8 v5, v0, 0x2 */
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_3
/* move v5, v4 */
} // :cond_3
/* move v5, v6 */
/* .line 71 */
/* .local v5, "saveBatteryEnable":Z */
} // :goto_0
/* if-nez p3, :cond_5 */
v7 = this.devices;
v7 = com.android.server.app.GmsUtil .isContainDevice ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_4
} // :cond_4
/* move v7, v6 */
} // :cond_5
} // :goto_1
/* move v7, v4 */
/* .line 72 */
/* .local v7, "containDevice":Z */
} // :goto_2
/* and-int/lit8 v8, v2, 0x1 */
if ( v8 != null) { // if-eqz v8, :cond_6
/* move v8, v4 */
} // :cond_6
/* move v8, v6 */
/* .line 73 */
/* .local v8, "preVersionEnable":Z */
} // :goto_3
/* and-int/lit8 v9, v2, 0x2 */
if ( v9 != null) { // if-eqz v9, :cond_7
/* move v9, v4 */
} // :cond_7
/* move v9, v6 */
/* .line 74 */
/* .local v9, "devVersionEnable":Z */
} // :goto_4
/* and-int/lit8 v10, v2, 0x4 */
if ( v10 != null) { // if-eqz v10, :cond_8
} // :cond_8
/* move v4, v6 */
/* .line 75 */
/* .local v4, "stableVersionEnable":Z */
} // :goto_5
if ( v8 != null) { // if-eqz v8, :cond_9
/* sget-boolean v6, Lmiui/os/Build;->IS_PRE_VERSION:Z */
/* if-nez v6, :cond_b */
} // :cond_9
if ( v9 != null) { // if-eqz v9, :cond_a
/* sget-boolean v6, Lmiui/os/Build;->IS_DEV_VERSION:Z */
/* if-nez v6, :cond_b */
} // :cond_a
if ( v4 != null) { // if-eqz v4, :cond_d
/* sget-boolean v6, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
/* if-nez v6, :cond_b */
/* .line 80 */
} // :cond_b
if ( p1 != null) { // if-eqz p1, :cond_c
if ( v5 != null) { // if-eqz v5, :cond_c
if ( v7 != null) { // if-eqz v7, :cond_c
/* .line 81 */
v1 = this.scenes;
v1 = this.padSaveBattery;
/* .line 83 */
} // :cond_c
/* .line 78 */
} // :cond_d
} // :goto_6
} // .end method
public static Boolean isContainDevice ( java.util.HashSet p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 25 */
/* .local p0, "devices":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 26 */
v0 = android.os.Build.DEVICE;
v0 = (( java.util.HashSet ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* .line 28 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isWQHD ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "currentWidth" # I */
/* .line 87 */
/* const-string/jumbo v0, "screen_resolution_supported" */
miui.util.FeatureParser .getIntArray ( v0 );
/* .line 88 */
/* .local v0, "resolutionArray":[I */
final String v1 = "screen_compat_supported"; // const-string v1, "screen_compat_supported"
int v2 = 0; // const/4 v2, 0x0
v1 = miui.util.FeatureParser .getBoolean ( v1,v2 );
/* .line 89 */
/* .local v1, "screenCompatSupported":Z */
/* sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v3, :cond_1 */
/* const/16 v3, 0x438 */
/* if-le p0, v3, :cond_1 */
/* if-nez v0, :cond_0 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 91 */
} // :cond_0
final String v2 = "GameManagerServiceStub"; // const-string v2, "GameManagerServiceStub"
final String v3 = "2K need downscale "; // const-string v3, "2K need downscale "
android.util.Slog .d ( v2,v3 );
/* .line 92 */
int v2 = 1; // const/4 v2, 0x1
/* .line 94 */
} // :cond_1
} // .end method
public static Boolean needDownscale ( java.lang.String p0, com.android.server.app.GameManagerServiceStubImpl$DownscaleCloudData p1, Integer p2 ) {
/* .locals 8 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "downscaleCloudData" # Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* .param p2, "currentWidth" # I */
/* .line 100 */
v0 = android.text.TextUtils .isEmpty ( p0 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 101 */
/* .line 102 */
} // :cond_0
v0 = com.android.server.app.GmsUtil .checkValidApp ( p0,p1 );
/* if-nez v0, :cond_1 */
/* .line 103 */
/* .line 106 */
} // :cond_1
/* const-string/jumbo v0, "screen_resolution_supported" */
miui.util.FeatureParser .getIntArray ( v0 );
/* .line 107 */
/* .local v0, "resolutionArray":[I */
final String v2 = "screen_compat_supported"; // const-string v2, "screen_compat_supported"
v2 = miui.util.FeatureParser .getBoolean ( v2,v1 );
/* .line 108 */
/* .local v2, "screenCompatSupported":Z */
/* sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z */
int v4 = 1; // const/4 v4, 0x1
final String v5 = "GameManagerServiceStub"; // const-string v5, "GameManagerServiceStub"
/* if-nez v3, :cond_3 */
/* const/16 v3, 0x438 */
/* if-le p2, v3, :cond_3 */
/* if-nez v0, :cond_2 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 110 */
} // :cond_2
final String v1 = "2K need downscale "; // const-string v1, "2K need downscale "
android.util.Slog .d ( v5,v1 );
/* .line 111 */
/* .line 114 */
} // :cond_3
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 115 */
v3 = this.devices;
v3 = com.android.server.app.GmsUtil .isContainDevice ( v3 );
/* .line 116 */
/* .local v3, "contained":Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 117 */
final String v1 = "power need downscale "; // const-string v1, "power need downscale "
android.util.Slog .d ( v5,v1 );
/* .line 118 */
/* .line 120 */
} // :cond_4
v6 = com.android.server.wm.WindowManagerServiceStub .get ( );
/* .line 122 */
/* .local v6, "currentScale":F */
/* const/high16 v7, 0x3f800000 # 1.0f */
/* cmpl-float v7, v6, v7 */
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 123 */
final String v1 = "need downscale "; // const-string v1, "need downscale "
android.util.Slog .d ( v5,v1 );
/* .line 124 */
/* .line 128 */
} // .end local v3 # "contained":Z
} // .end local v6 # "currentScale":F
} // :cond_5
} // .end method
