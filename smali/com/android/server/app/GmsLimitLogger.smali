.class public Lcom/android/server/app/GmsLimitLogger;
.super Ljava/lang/Object;
.source "GmsLimitLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/app/GmsLimitLogger$Event;,
        Lcom/android/server/app/GmsLimitLogger$StringEvent;
    }
.end annotation


# instance fields
.field private final mEvents:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/app/GmsLimitLogger$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final mMemSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GmsLimitLogger;->mEvents:Ljava/util/LinkedList;

    .line 22
    iput p1, p0, Lcom/android/server/app/GmsLimitLogger;->mMemSize:I

    .line 23
    return-void
.end method


# virtual methods
.method public declared-synchronized dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    monitor-enter p0

    .line 33
    :try_start_0
    iget-object v0, p0, Lcom/android/server/app/GmsLimitLogger;->mEvents:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/app/GmsLimitLogger$Event;

    .line 34
    .local v1, "evt":Lcom/android/server/app/GmsLimitLogger$Event;
    invoke-virtual {v1}, Lcom/android/server/app/GmsLimitLogger$Event;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    .end local v1    # "evt":Lcom/android/server/app/GmsLimitLogger$Event;
    goto :goto_0

    .line 36
    .end local p0    # "this":Lcom/android/server/app/GmsLimitLogger;
    :cond_0
    monitor-exit p0

    return-void

    .line 32
    .end local p1    # "pw":Ljava/io/PrintWriter;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized log(Lcom/android/server/app/GmsLimitLogger$Event;)V
    .locals 2
    .param p1, "evt"    # Lcom/android/server/app/GmsLimitLogger$Event;

    monitor-enter p0

    .line 26
    :try_start_0
    iget-object v0, p0, Lcom/android/server/app/GmsLimitLogger;->mEvents:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/server/app/GmsLimitLogger;->mMemSize:I

    if-lt v0, v1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/android/server/app/GmsLimitLogger;->mEvents:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 29
    .end local p0    # "this":Lcom/android/server/app/GmsLimitLogger;
    :cond_0
    iget-object v0, p0, Lcom/android/server/app/GmsLimitLogger;->mEvents:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    monitor-exit p0

    return-void

    .line 25
    .end local p1    # "evt":Lcom/android/server/app/GmsLimitLogger$Event;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
