.class public Lcom/android/server/app/GmsUtil;
.super Ljava/lang/Object;
.source "GmsUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GameManagerServiceStub"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calcuRatio(II)Ljava/lang/String;
    .locals 9
    .param p0, "targetWidth"    # I
    .param p1, "currentWidth"    # I

    .line 33
    new-instance v0, Ljava/math/BigDecimal;

    int-to-float v1, p0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    int-to-float v3, p1

    div-float/2addr v1, v3

    float-to-double v3, v1

    invoke-direct {v0, v3, v4}, Ljava/math/BigDecimal;-><init>(D)V

    .line 34
    .local v0, "bigDecimal":Ljava/math/BigDecimal;
    const/4 v1, 0x2

    sget-object v3, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v1

    .line 35
    .local v1, "ratio":F
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_0

    .line 36
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ratio >= 1  targetWidth = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  currentWidth = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GameManagerServiceStub"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const-string v2, "disable"

    return-object v2

    .line 39
    :cond_0
    const v3, 0x3f19999a    # 0.6f

    cmpl-float v4, v1, v3

    if-lez v4, :cond_2

    .line 40
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float v5, v1, v4

    float-to-int v5, v5

    .line 41
    .local v5, "raTemp":I
    rem-int/lit8 v6, v5, 0xa

    .line 42
    .local v6, "lastDigital":I
    div-int/lit8 v7, v5, 0xa

    .line 43
    .local v7, "firstDigital":I
    if-eqz v6, :cond_1

    .line 44
    const/4 v6, 0x5

    .line 46
    :cond_1
    mul-int/lit8 v8, v7, 0xa

    add-int/2addr v8, v6

    .line 47
    .end local v5    # "raTemp":I
    .local v8, "raTemp":I
    int-to-float v5, v8

    mul-float/2addr v5, v2

    div-float v1, v5, v4

    .line 49
    .end local v6    # "lastDigital":I
    .end local v7    # "firstDigital":I
    .end local v8    # "raTemp":I
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static checkValidApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;)Z
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "downscaleCloudData"    # Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 133
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 134
    return v1

    .line 136
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->apps:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->apps:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;

    .line 138
    .local v0, "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    if-eqz v0, :cond_1

    .line 139
    return v1

    .line 142
    .end local v0    # "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static getTargetRatioForPad(Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;ZLcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;Z)Ljava/lang/String;
    .locals 11
    .param p0, "appItem"    # Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    .param p1, "mPowerSaving"    # Z
    .param p2, "mDownscaleCloudData"    # Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    .param p3, "isDebugPowerSave"    # Z

    .line 56
    iget v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->mode:I

    .line 57
    .local v0, "mode":I
    const-string v1, "disable"

    if-nez v0, :cond_0

    .line 58
    return-object v1

    .line 60
    :cond_0
    iget v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->systemVersion:I

    .line 61
    .local v2, "systemVersion":I
    if-nez v2, :cond_1

    .line 62
    return-object v1

    .line 64
    :cond_1
    iget v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->appVersion:I

    .line 65
    .local v3, "appVersion":I
    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    .line 66
    return-object v1

    .line 70
    :cond_2
    and-int/lit8 v5, v0, 0x2

    const/4 v6, 0x0

    if-eqz v5, :cond_3

    move v5, v4

    goto :goto_0

    :cond_3
    move v5, v6

    .line 71
    .local v5, "saveBatteryEnable":Z
    :goto_0
    if-nez p3, :cond_5

    iget-object v7, p2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->devices:Ljava/util/HashSet;

    invoke-static {v7}, Lcom/android/server/app/GmsUtil;->isContainDevice(Ljava/util/HashSet;)Z

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_1

    :cond_4
    move v7, v6

    goto :goto_2

    :cond_5
    :goto_1
    move v7, v4

    .line 72
    .local v7, "containDevice":Z
    :goto_2
    and-int/lit8 v8, v2, 0x1

    if-eqz v8, :cond_6

    move v8, v4

    goto :goto_3

    :cond_6
    move v8, v6

    .line 73
    .local v8, "preVersionEnable":Z
    :goto_3
    and-int/lit8 v9, v2, 0x2

    if-eqz v9, :cond_7

    move v9, v4

    goto :goto_4

    :cond_7
    move v9, v6

    .line 74
    .local v9, "devVersionEnable":Z
    :goto_4
    and-int/lit8 v10, v2, 0x4

    if-eqz v10, :cond_8

    goto :goto_5

    :cond_8
    move v4, v6

    .line 75
    .local v4, "stableVersionEnable":Z
    :goto_5
    if-eqz v8, :cond_9

    sget-boolean v6, Lmiui/os/Build;->IS_PRE_VERSION:Z

    if-nez v6, :cond_b

    :cond_9
    if-eqz v9, :cond_a

    sget-boolean v6, Lmiui/os/Build;->IS_DEV_VERSION:Z

    if-nez v6, :cond_b

    :cond_a
    if-eqz v4, :cond_d

    sget-boolean v6, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-nez v6, :cond_b

    goto :goto_6

    .line 80
    :cond_b
    if-eqz p1, :cond_c

    if-eqz v5, :cond_c

    if-eqz v7, :cond_c

    .line 81
    iget-object v1, p2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    iget-object v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->padSaveBattery:Ljava/lang/String;

    return-object v1

    .line 83
    :cond_c
    return-object v1

    .line 78
    :cond_d
    :goto_6
    return-object v1
.end method

.method public static isContainDevice(Ljava/util/HashSet;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 25
    .local p0, "devices":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 26
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 28
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static isWQHD(I)Z
    .locals 4
    .param p0, "currentWidth"    # I

    .line 87
    const-string/jumbo v0, "screen_resolution_supported"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 88
    .local v0, "resolutionArray":[I
    const-string v1, "screen_compat_supported"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 89
    .local v1, "screenCompatSupported":Z
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v3, :cond_1

    const/16 v3, 0x438

    if-le p0, v3, :cond_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 91
    :cond_0
    const-string v2, "GameManagerServiceStub"

    const-string v3, "2K need downscale "

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v2, 0x1

    return v2

    .line 94
    :cond_1
    return v2
.end method

.method public static needDownscale(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;I)Z
    .locals 8
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "downscaleCloudData"    # Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    .param p2, "currentWidth"    # I

    .line 100
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 101
    return v1

    .line 102
    :cond_0
    invoke-static {p0, p1}, Lcom/android/server/app/GmsUtil;->checkValidApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    return v1

    .line 106
    :cond_1
    const-string/jumbo v0, "screen_resolution_supported"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 107
    .local v0, "resolutionArray":[I
    const-string v2, "screen_compat_supported"

    invoke-static {v2, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 108
    .local v2, "screenCompatSupported":Z
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    const/4 v4, 0x1

    const-string v5, "GameManagerServiceStub"

    if-nez v3, :cond_3

    const/16 v3, 0x438

    if-le p2, v3, :cond_3

    if-nez v0, :cond_2

    if-eqz v2, :cond_3

    .line 110
    :cond_2
    const-string v1, "2K need downscale "

    invoke-static {v5, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return v4

    .line 114
    :cond_3
    if-eqz p1, :cond_5

    .line 115
    iget-object v3, p1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->devices:Ljava/util/HashSet;

    invoke-static {v3}, Lcom/android/server/app/GmsUtil;->isContainDevice(Ljava/util/HashSet;)Z

    move-result v3

    .line 116
    .local v3, "contained":Z
    if-eqz v3, :cond_4

    .line 117
    const-string v1, "power need downscale "

    invoke-static {v5, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return v4

    .line 120
    :cond_4
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v6

    invoke-interface {v6, p0, v1}, Lcom/android/server/wm/WindowManagerServiceStub;->getCompatScale(Ljava/lang/String;I)F

    move-result v6

    .line 122
    .local v6, "currentScale":F
    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v6, v7

    if-eqz v7, :cond_5

    .line 123
    const-string v1, "need downscale "

    invoke-static {v5, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    return v4

    .line 128
    .end local v3    # "contained":Z
    .end local v6    # "currentScale":F
    :cond_5
    return v1
.end method
