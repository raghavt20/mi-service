class com.android.server.app.GameManagerServiceStubImpl$GMSObserver extends android.database.ContentObserver {
	 /* .source "GameManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GameManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "GMSObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.app.GameManagerServiceStubImpl this$0; //synthetic
/* # direct methods */
public com.android.server.app.GameManagerServiceStubImpl$GMSObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/app/GameManagerServiceStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 819 */
this.this$0 = p1;
/* .line 820 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 821 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 825 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 826 */
/* if-nez p2, :cond_0 */
/* .line 827 */
return;
/* .line 829 */
} // :cond_0
final String v0 = "persist.sys.miui_downscale"; // const-string v0, "persist.sys.miui_downscale"
android.provider.Settings$Global .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "GameManagerServiceStub"; // const-string v2, "GameManagerServiceStub"
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 830 */
final String v1 = "DEBUG_PROP_KEY"; // const-string v1, "DEBUG_PROP_KEY"
android.util.Slog .d ( v2,v1 );
/* .line 831 */
v1 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .getString ( v1,v0 );
/* .line 833 */
/* .local v0, "proEnable":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmProEnable ( v1 );
v1 = android.text.TextUtils .equals ( v0,v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 834 */
	 return;
	 /* .line 836 */
} // :cond_1
v1 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fputmProEnable ( v1,v0 );
/* .line 837 */
v1 = this.this$0;
v1 = com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmPowerSaving ( v1 );
/* if-nez v1, :cond_2 */
v1 = this.this$0;
v1 = com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetintelligentPowerSavingEnable ( v1 );
/* if-nez v1, :cond_2 */
/* .line 838 */
return;
/* .line 840 */
} // :cond_2
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.app.GameManagerServiceStubImpl ) v1 ).toggleDownscale ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V
/* .line 841 */
} // .end local v0 # "proEnable":Ljava/lang/String;
/* goto/16 :goto_0 */
} // :cond_3
final String v0 = "POWER_SAVE_MODE_OPEN"; // const-string v0, "POWER_SAVE_MODE_OPEN"
android.provider.Settings$System .getUriFor ( v0 );
v0 = (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 842 */
final String v0 = "KEY_POWER_MODE_OPEN"; // const-string v0, "KEY_POWER_MODE_OPEN"
android.util.Slog .d ( v2,v0 );
/* .line 843 */
v0 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmHandler ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 844 */
v0 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmHandler ( v0 );
/* const v1, 0x1b208 */
(( com.android.server.app.GameManagerServiceStubImpl$InnerHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z
/* goto/16 :goto_0 */
/* .line 846 */
} // :cond_4
final String v0 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v0 );
v0 = (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 847 */
final String v0 = "CLOUD_ALL_DATA_CHANGE_URI"; // const-string v0, "CLOUD_ALL_DATA_CHANGE_URI"
android.util.Slog .d ( v2,v0 );
/* .line 848 */
v0 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmHandler ( v0 );
/* const v1, 0x1b20a */
(( com.android.server.app.GameManagerServiceStubImpl$InnerHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z
/* .line 849 */
} // :cond_5
final String v0 = "miui_screen_compat"; // const-string v0, "miui_screen_compat"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 850 */
final String v1 = "MIUI_SCREEN_COMPAT"; // const-string v1, "MIUI_SCREEN_COMPAT"
android.util.Slog .d ( v2,v1 );
/* .line 851 */
v1 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmContext ( v1 );
/* .line 852 */
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$System .getInt ( v2,v0,v4 );
/* if-ne v0, v4, :cond_6 */
/* move v3, v4 */
} // :cond_6
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fputintelligentPowerSavingEnable ( v1,v3 );
/* .line 853 */
v0 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmHandler ( v0 );
/* const v1, 0x1b20b */
(( com.android.server.app.GameManagerServiceStubImpl$InnerHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z
/* .line 854 */
} // :cond_7
final String v0 = "pc_security_center_extreme_mode_enter"; // const-string v0, "pc_security_center_extreme_mode_enter"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 855 */
final String v1 = "PC_SECURITY_CENTER_EXTREME_MODE"; // const-string v1, "PC_SECURITY_CENTER_EXTREME_MODE"
android.util.Slog .d ( v2,v1 );
/* .line 856 */
v1 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$Secure .getInt ( v2,v0,v3 );
/* if-ne v0, v4, :cond_8 */
/* move v3, v4 */
} // :cond_8
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fputmExtremeModeEnable ( v1,v3 );
/* .line 857 */
v0 = this.this$0;
com.android.server.app.GameManagerServiceStubImpl .-$$Nest$fgetmHandler ( v0 );
/* const v1, 0x1b20c */
(( com.android.server.app.GameManagerServiceStubImpl$InnerHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z
/* .line 859 */
} // :cond_9
} // :goto_0
return;
} // .end method
