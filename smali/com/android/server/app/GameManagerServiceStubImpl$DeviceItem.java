class com.android.server.app.GameManagerServiceStubImpl$DeviceItem {
	 /* .source "GameManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GameManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "DeviceItem" */
} // .end annotation
/* # instance fields */
java.lang.String name;
/* # direct methods */
public com.android.server.app.GameManagerServiceStubImpl$DeviceItem ( ) {
/* .locals 1 */
/* .param p1, "cloudDataDevice" # Lorg/json/JSONObject; */
/* .line 792 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 793 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 794 */
	 final String v0 = "name"; // const-string v0, "name"
	 (( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
	 this.name = v0;
	 /* .line 796 */
} // :cond_0
return;
} // .end method
