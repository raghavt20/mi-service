.class Lcom/android/server/app/PreCacheUtilStubImpl$PreCacheContentObserver;
.super Landroid/database/ContentObserver;
.source "PreCacheUtilStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/PreCacheUtilStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreCacheContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/app/PreCacheUtilStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/app/PreCacheUtilStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 55
    iput-object p1, p0, Lcom/android/server/app/PreCacheUtilStubImpl$PreCacheContentObserver;->this$0:Lcom/android/server/app/PreCacheUtilStubImpl;

    .line 56
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 57
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 61
    if-eqz p2, :cond_0

    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/android/server/app/PreCacheUtilStubImpl$PreCacheContentObserver;->this$0:Lcom/android/server/app/PreCacheUtilStubImpl;

    invoke-static {v0}, Lcom/android/server/app/PreCacheUtilStubImpl;->-$$Nest$mupdatePreCacheSetting(Lcom/android/server/app/PreCacheUtilStubImpl;)V

    .line 64
    :cond_0
    return-void
.end method
