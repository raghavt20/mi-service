.class Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;
.super Ljava/lang/Object;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/GameManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Scenes"
.end annotation


# instance fields
.field normal:I

.field padSaveBattery:Ljava/lang/String;

.field saveBattery:I


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "jsonObject"    # Lorg/json/JSONObject;

    .line 804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805
    if-eqz p1, :cond_0

    .line 806
    const-string v0, "normal"

    const/16 v1, 0x438

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->normal:I

    .line 807
    const/16 v0, 0x2d0

    const-string v1, "save_battery"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->saveBattery:I

    .line 808
    const-string v0, "0.85"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->padSaveBattery:Ljava/lang/String;

    .line 810
    :cond_0
    return-void
.end method
