.class public Lcom/android/server/app/DynamicLoadController;
.super Ljava/lang/Object;
.source "DynamicLoadController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/app/DynamicLoadController$AppInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DynamicLoadController"

.field private static mApps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/app/DynamicLoadController$AppInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    .line 58
    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.sina.weibo"

    const-string v3, "0.75"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.ss.android.article.news"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.taobao.taobao"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.kuaishou.nebula"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.ss.android.ugc.aweme"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.baidu.BaiduMap"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.eg.android.AlipayGphone"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.ss.android.ugc.aweme.lite"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.gotokeep.keep"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.dianping.v1 "

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.UCMobile"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.jingdong.app.mall"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.alibaba.android.rimet"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.kugou.android"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.tencent.qqmusic"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.douban.frodo"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.wuba"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.sinyee.babybus.recommendapp"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.jifen.qukan"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.yuncheapp.android.pearl"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.duoduo.child.story"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.bokecc.dance"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.bokecc.dance"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.guotai.dazhihui"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.guotai.dazhihui"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.cubic.autohome"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.cubic.autohome"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.able.wisdomtree"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.able.wisdomtree"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.snda.wifilocating"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.snda.wifilocating"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "com.hexin.plat.android"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.hexin.plat.android"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v4, "cn.xiaochuankeji.zuiyouLite"

    invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "cn.xiaochuankeji.zuiyouLite "

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo;

    const-string v2, "com.mampod.ergedd"

    invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.mampod.ergedd"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    return-void
.end method

.method public constructor <init>(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 18
    .param p1, "activityTaskManager"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 90
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v1, Landroid/util/ArraySet;

    .line 35
    const-wide/32 v2, 0xa09e1d7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 36
    const-wide/32 v2, 0xae57a6b

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 37
    const-wide/32 v2, 0xb52b546

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 38
    const-wide/32 v2, 0xa8bb021

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 39
    const-wide/32 v2, 0xb52b573

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 40
    const-wide/32 v2, 0xa8bb06d

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 41
    const-wide/32 v2, 0xb52b550

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 42
    const-wide/32 v2, 0xa8bb033

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 43
    const-wide/32 v2, 0xb52b674

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 44
    const-wide/32 v2, 0xa8bb015

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    .line 45
    const-wide/32 v2, 0xb52b576

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 46
    const-wide/32 v2, 0xb52b676

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 47
    const-wide/32 v2, 0xb52b555

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 48
    const-wide/32 v2, 0xb52b678

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    filled-new-array/range {v4 .. v17}, [Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/util/ArraySet;-><init>([Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/android/server/app/DynamicLoadController;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    .line 91
    move-object/from16 v1, p1

    iput-object v1, v0, Lcom/android/server/app/DynamicLoadController;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 92
    return-void
.end method

.method private downscaleApp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "ratio"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/android/server/app/DynamicLoadController;->mApps:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "DynamicLoadController"

    if-nez v0, :cond_0

    .line 104
    const-string v0, "invalid packageName, reject!!!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v0, 0x0

    return v0

    .line 108
    :cond_0
    invoke-static {p1}, Lcom/android/server/app/DynamicLoadController;->getCompatChangeId(Ljava/lang/String;)J

    move-result-wide v2

    .line 110
    .local v2, "changeId":J
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 112
    .local v0, "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 113
    iget-object v4, p0, Lcom/android/server/app/DynamicLoadController;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    .local v4, "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    goto :goto_0

    .line 115
    .end local v4    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_1
    const-wide/32 v4, 0xa09e1d7

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v4, p0, Lcom/android/server/app/DynamicLoadController;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    invoke-virtual {v4}, Landroid/util/ArraySet;->stream()Ljava/util/stream/Stream;

    move-result-object v4

    new-instance v5, Lcom/android/server/app/DynamicLoadController$$ExternalSyntheticLambda0;

    invoke-direct {v5, v2, v3}, Lcom/android/server/app/DynamicLoadController$$ExternalSyntheticLambda0;-><init>(J)V

    .line 118
    invoke-interface {v4, v5}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v4

    .line 119
    invoke-static {}, Ljava/util/stream/Collectors;->toSet()Ljava/util/stream/Collector;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    .line 122
    .restart local v4    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :goto_0
    nop

    .line 123
    const-string v5, "platform_compat"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    check-cast v5, Lcom/android/server/compat/PlatformCompat;

    .line 124
    .local v5, "platformCompat":Lcom/android/server/compat/PlatformCompat;
    new-instance v6, Lcom/android/internal/compat/CompatibilityChangeConfig;

    new-instance v7, Landroid/compat/Compatibility$ChangeConfig;

    invoke-direct {v7, v0, v4}, Landroid/compat/Compatibility$ChangeConfig;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    invoke-direct {v6, v7}, Lcom/android/internal/compat/CompatibilityChangeConfig;-><init>(Landroid/compat/Compatibility$ChangeConfig;)V

    .line 128
    .local v6, "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
    invoke-virtual {v5, v6, p2}, Lcom/android/server/compat/PlatformCompat;->setOverrides(Lcom/android/internal/compat/CompatibilityChangeConfig;Ljava/lang/String;)V

    .line 131
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dynamic load enable packageName = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ratio = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/4 v1, 0x1

    return v1
.end method

.method public static getCompatChangeId(Ljava/lang/String;)J
    .locals 2
    .param p0, "raw"    # Ljava/lang/String;

    .line 146
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v0, "0.85"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "0.75"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "0.65"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "0.55"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_4
    const-string v0, "0.45"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_5
    const-string v0, "0.35"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_6
    const-string v0, "0.9"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    goto :goto_1

    :sswitch_7
    const-string v0, "0.8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto :goto_1

    :sswitch_8
    const-string v0, "0.7"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v0, "0.6"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_a
    const-string v0, "0.5"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_b
    const-string v0, "0.4"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_c
    const-string v0, "0.3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 174
    const-wide/16 v0, 0x0

    return-wide v0

    .line 172
    :pswitch_0
    const-wide/32 v0, 0xae57a6b

    return-wide v0

    .line 170
    :pswitch_1
    const-wide/32 v0, 0xb52b546

    return-wide v0

    .line 168
    :pswitch_2
    const-wide/32 v0, 0xa8bb021

    return-wide v0

    .line 166
    :pswitch_3
    const-wide/32 v0, 0xb52b573

    return-wide v0

    .line 164
    :pswitch_4
    const-wide/32 v0, 0xa8bb06d

    return-wide v0

    .line 162
    :pswitch_5
    const-wide/32 v0, 0xb52b550

    return-wide v0

    .line 160
    :pswitch_6
    const-wide/32 v0, 0xa8bb033

    return-wide v0

    .line 158
    :pswitch_7
    const-wide/32 v0, 0xb52b674

    return-wide v0

    .line 156
    :pswitch_8
    const-wide/32 v0, 0xa8bb015

    return-wide v0

    .line 154
    :pswitch_9
    const-wide/32 v0, 0xb52b576

    return-wide v0

    .line 152
    :pswitch_a
    const-wide/32 v0, 0xb52b676

    return-wide v0

    .line 150
    :pswitch_b
    const-wide/32 v0, 0xb52b555

    return-wide v0

    .line 148
    :pswitch_c
    const-wide/32 v0, 0xb52b678

    return-wide v0

    :sswitch_data_0
    .sparse-switch
        0xb9f5 -> :sswitch_c
        0xb9f6 -> :sswitch_b
        0xb9f7 -> :sswitch_a
        0xb9f8 -> :sswitch_9
        0xb9f9 -> :sswitch_8
        0xb9fa -> :sswitch_7
        0xb9fb -> :sswitch_6
        0x1684e0 -> :sswitch_5
        0x1684ff -> :sswitch_4
        0x16851e -> :sswitch_3
        0x16853d -> :sswitch_2
        0x16855c -> :sswitch_1
        0x16857b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$downscaleApp$0(JLjava/lang/Long;)Z
    .locals 4
    .param p0, "changeId"    # J
    .param p2, "it"    # Ljava/lang/Long;

    .line 118
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0xa09e1d7

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public dynamicLoadWithPackageName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "ratio"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/android/server/app/DynamicLoadController;->downscaleApp(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
