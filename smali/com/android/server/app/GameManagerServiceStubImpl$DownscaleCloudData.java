class com.android.server.app.GameManagerServiceStubImpl$DownscaleCloudData {
	 /* .source "GameManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GameManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DownscaleCloudData" */
} // .end annotation
/* # instance fields */
java.util.HashMap apps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.HashSet devices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Boolean enable;
java.util.HashSet offLocalDevices;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
com.android.server.app.GameManagerServiceStubImpl$Scenes scenes;
final com.android.server.app.GameManagerServiceStubImpl this$0; //synthetic
Long version;
/* # direct methods */
public com.android.server.app.GameManagerServiceStubImpl$DownscaleCloudData ( ) {
/* .locals 9 */
/* .param p1, "this$0" # Lcom/android/server/app/GameManagerServiceStubImpl; */
/* .param p2, "cloudData" # Lorg/json/JSONObject; */
/* .line 695 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 690 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.devices = v0;
/* .line 691 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.apps = v0;
/* .line 693 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.offLocalDevices = v0;
/* .line 696 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 697 */
final String v0 = "enable"; // const-string v0, "enable"
int v1 = 0; // const/4 v1, 0x0
v2 = (( org.json.JSONObject ) p2 ).optBoolean ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z */
/* .line 698 */
/* const-string/jumbo v2, "version" */
/* const-wide/16 v3, -0x1 */
(( org.json.JSONObject ) p2 ).optLong ( v2, v3, v4 ); // invoke-virtual {p2, v2, v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->version:J */
/* .line 699 */
int v2 = 0; // const/4 v2, 0x0
/* .line 700 */
/* .local v2, "appsJson":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .line 701 */
/* .local v3, "devicesJson":Lorg/json/JSONArray; */
int v4 = 0; // const/4 v4, 0x0
/* .line 702 */
/* .local v4, "scenesJson":Lorg/json/JSONObject; */
/* sget-boolean v5, Lmiui/os/Build;->IS_TABLET:Z */
final String v6 = "scenes"; // const-string v6, "scenes"
final String v7 = "devices"; // const-string v7, "devices"
final String v8 = "apps"; // const-string v8, "apps"
/* if-nez v5, :cond_0 */
/* .line 703 */
(( org.json.JSONObject ) p2 ).optJSONArray ( v8 ); // invoke-virtual {p2, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 704 */
(( org.json.JSONObject ) p2 ).optJSONArray ( v7 ); // invoke-virtual {p2, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 705 */
(( org.json.JSONObject ) p2 ).optJSONObject ( v6 ); // invoke-virtual {p2, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 707 */
} // :cond_0
final String v5 = "pad"; // const-string v5, "pad"
(( org.json.JSONObject ) p2 ).optJSONObject ( v5 ); // invoke-virtual {p2, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 708 */
/* .local v5, "padJson":Lorg/json/JSONObject; */
/* if-nez v5, :cond_1 */
/* .line 709 */
return;
/* .line 711 */
} // :cond_1
v0 = (( org.json.JSONObject ) v5 ).optBoolean ( v0, v1 ); // invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z */
/* .line 713 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 714 */
(( org.json.JSONObject ) v5 ).optJSONArray ( v8 ); // invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 715 */
(( org.json.JSONObject ) v5 ).optJSONArray ( v7 ); // invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 716 */
(( org.json.JSONObject ) v5 ).optJSONObject ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 719 */
} // .end local v5 # "padJson":Lorg/json/JSONObject;
} // :cond_2
} // :goto_0
/* invoke-direct {p0, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseApps(Lorg/json/JSONArray;)V */
/* .line 721 */
final String v0 = "off_devices"; // const-string v0, "off_devices"
(( org.json.JSONObject ) p2 ).optJSONArray ( v0 ); // invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* invoke-direct {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseOffLocalDevices(Lorg/json/JSONArray;)V */
/* .line 722 */
/* invoke-direct {p0, v3}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseDevices(Lorg/json/JSONArray;)V */
/* .line 723 */
/* invoke-direct {p0, v4}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseScenes(Lorg/json/JSONObject;)V */
/* .line 725 */
} // .end local v2 # "appsJson":Lorg/json/JSONArray;
} // .end local v3 # "devicesJson":Lorg/json/JSONArray;
} // .end local v4 # "scenesJson":Lorg/json/JSONObject;
} // :cond_3
return;
} // .end method
private void parseApps ( org.json.JSONArray p0 ) {
/* .locals 4 */
/* .param p1, "appsJson" # Lorg/json/JSONArray; */
/* .line 728 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-lez v0, :cond_0 */
/* .line 729 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-ge v0, v1, :cond_0 */
/* .line 730 */
/* new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
(( org.json.JSONArray ) p1 ).optJSONObject ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;-><init>(Lorg/json/JSONObject;)V */
/* .line 731 */
/* .local v1, "appItem":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
v2 = this.apps;
v3 = this.packageName;
(( java.util.HashMap ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 729 */
} // .end local v1 # "appItem":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 734 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void parseDevices ( org.json.JSONArray p0 ) {
/* .locals 4 */
/* .param p1, "devicesJson" # Lorg/json/JSONArray; */
/* .line 736 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-lez v0, :cond_1 */
/* .line 737 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-ge v0, v1, :cond_0 */
/* .line 738 */
/* new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem; */
(( org.json.JSONArray ) p1 ).optJSONObject ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;-><init>(Lorg/json/JSONObject;)V */
/* .line 739 */
/* .local v1, "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem; */
v2 = this.devices;
v3 = this.name;
(( java.util.HashSet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 737 */
} // .end local v1 # "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 742 */
} // .end local v0 # "i":I
} // :cond_0
v0 = this.devices;
v1 = this.offLocalDevices;
com.android.server.app.GmsSettings .getLocalDevicesForPowerSaving ( v0,v1 );
/* .line 744 */
} // :cond_1
return;
} // .end method
private void parseOffLocalDevices ( org.json.JSONArray p0 ) {
/* .locals 4 */
/* .param p1, "offLocalDevicesJson" # Lorg/json/JSONArray; */
/* .line 751 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-lez v0, :cond_0 */
/* .line 752 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-ge v0, v1, :cond_0 */
/* .line 753 */
/* new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem; */
(( org.json.JSONArray ) p1 ).optJSONObject ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* invoke-direct {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;-><init>(Lorg/json/JSONObject;)V */
/* .line 754 */
/* .local v1, "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem; */
v2 = this.offLocalDevices;
v3 = this.name;
(( java.util.HashSet ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 752 */
} // .end local v1 # "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 757 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void parseScenes ( org.json.JSONObject p0 ) {
/* .locals 1 */
/* .param p1, "scenesJson" # Lorg/json/JSONObject; */
/* .line 746 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 747 */
/* new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes; */
/* invoke-direct {v0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;-><init>(Lorg/json/JSONObject;)V */
this.scenes = v0;
/* .line 749 */
} // :cond_0
return;
} // .end method
