public class com.android.server.app.DynamicLoadController {
	 /* .source "DynamicLoadController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/app/DynamicLoadController$AppInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static java.util.HashMap mApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/app/DynamicLoadController$AppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
final android.util.ArraySet DOWNSCALE_CHANGE_IDS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArraySet<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
com.android.server.wm.ActivityTaskManagerService mActivityTaskManager;
/* # direct methods */
static com.android.server.app.DynamicLoadController ( ) {
/* .locals 5 */
/* .line 55 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 58 */
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.sina.weibo"; // const-string v2, "com.sina.weibo"
final String v3 = "0.75"; // const-string v3, "0.75"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 59 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.ss.android.article.news"; // const-string v2, "com.ss.android.article.news"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 60 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.taobao.taobao"; // const-string v2, "com.taobao.taobao"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 61 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.kuaishou.nebula"; // const-string v2, "com.kuaishou.nebula"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 62 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.ss.android.ugc.aweme"; // const-string v2, "com.ss.android.ugc.aweme"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 63 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.baidu.BaiduMap"; // const-string v2, "com.baidu.BaiduMap"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 64 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.eg.android.AlipayGphone"; // const-string v2, "com.eg.android.AlipayGphone"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 65 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.ss.android.ugc.aweme.lite"; // const-string v2, "com.ss.android.ugc.aweme.lite"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 66 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.gotokeep.keep"; // const-string v2, "com.gotokeep.keep"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 67 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.dianping.v1 "; // const-string v2, "com.dianping.v1 "
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 68 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.UCMobile"; // const-string v2, "com.UCMobile"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 69 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.jingdong.app.mall"; // const-string v2, "com.jingdong.app.mall"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 70 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.alibaba.android.rimet"; // const-string v2, "com.alibaba.android.rimet"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 71 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.kugou.android"; // const-string v2, "com.kugou.android"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 72 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.tencent.qqmusic"; // const-string v2, "com.tencent.qqmusic"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 73 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.douban.frodo"; // const-string v2, "com.douban.frodo"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 74 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.wuba"; // const-string v2, "com.wuba"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 75 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.sinyee.babybus.recommendapp"; // const-string v2, "com.sinyee.babybus.recommendapp"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 76 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.jifen.qukan"; // const-string v4, "com.jifen.qukan"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 77 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.yuncheapp.android.pearl"; // const-string v4, "com.yuncheapp.android.pearl"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 78 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.duoduo.child.story"; // const-string v4, "com.duoduo.child.story"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 79 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.bokecc.dance"; // const-string v4, "com.bokecc.dance"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "com.bokecc.dance"; // const-string v4, "com.bokecc.dance"
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 80 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.guotai.dazhihui"; // const-string v4, "com.guotai.dazhihui"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "com.guotai.dazhihui"; // const-string v4, "com.guotai.dazhihui"
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 81 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.cubic.autohome"; // const-string v4, "com.cubic.autohome"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "com.cubic.autohome"; // const-string v4, "com.cubic.autohome"
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 82 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.able.wisdomtree"; // const-string v4, "com.able.wisdomtree"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "com.able.wisdomtree"; // const-string v4, "com.able.wisdomtree"
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 83 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.snda.wifilocating"; // const-string v4, "com.snda.wifilocating"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "com.snda.wifilocating"; // const-string v4, "com.snda.wifilocating"
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 84 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "com.hexin.plat.android"; // const-string v4, "com.hexin.plat.android"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "com.hexin.plat.android"; // const-string v4, "com.hexin.plat.android"
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 85 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v4 = "cn.xiaochuankeji.zuiyouLite"; // const-string v4, "cn.xiaochuankeji.zuiyouLite"
/* invoke-direct {v1, v4, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v4 = "cn.xiaochuankeji.zuiyouLite "; // const-string v4, "cn.xiaochuankeji.zuiyouLite "
(( java.util.HashMap ) v0 ).put ( v4, v1 ); // invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 86 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 87 */
v0 = com.android.server.app.DynamicLoadController.mApps;
/* new-instance v1, Lcom/android/server/app/DynamicLoadController$AppInfo; */
final String v2 = "com.mampod.ergedd"; // const-string v2, "com.mampod.ergedd"
/* invoke-direct {v1, v2, v3}, Lcom/android/server/app/DynamicLoadController$AppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
final String v2 = "com.mampod.ergedd"; // const-string v2, "com.mampod.ergedd"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 88 */
return;
} // .end method
public com.android.server.app.DynamicLoadController ( ) {
/* .locals 18 */
/* .param p1, "activityTaskManager" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .line 90 */
/* move-object/from16 v0, p0 */
/* invoke-direct/range {p0 ..p0}, Ljava/lang/Object;-><init>()V */
/* .line 34 */
/* new-instance v1, Landroid/util/ArraySet; */
/* .line 35 */
/* const-wide/32 v2, 0xa09e1d7 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 36 */
/* const-wide/32 v2, 0xae57a6b */
java.lang.Long .valueOf ( v2,v3 );
/* .line 37 */
/* const-wide/32 v2, 0xb52b546 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 38 */
/* const-wide/32 v2, 0xa8bb021 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 39 */
/* const-wide/32 v2, 0xb52b573 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 40 */
/* const-wide/32 v2, 0xa8bb06d */
java.lang.Long .valueOf ( v2,v3 );
/* .line 41 */
/* const-wide/32 v2, 0xb52b550 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 42 */
/* const-wide/32 v2, 0xa8bb033 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 43 */
/* const-wide/32 v2, 0xb52b674 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 44 */
/* const-wide/32 v2, 0xa8bb015 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 45 */
/* const-wide/32 v2, 0xb52b576 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 46 */
/* const-wide/32 v2, 0xb52b676 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 47 */
/* const-wide/32 v2, 0xb52b555 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 48 */
/* const-wide/32 v2, 0xb52b678 */
java.lang.Long .valueOf ( v2,v3 );
/* filled-new-array/range {v4 ..v17}, [Ljava/lang/Long; */
/* invoke-direct {v1, v2}, Landroid/util/ArraySet;-><init>([Ljava/lang/Object;)V */
this.DOWNSCALE_CHANGE_IDS = v1;
/* .line 91 */
/* move-object/from16 v1, p1 */
this.mActivityTaskManager = v1;
/* .line 92 */
return;
} // .end method
private Boolean downscaleApp ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "ratio" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 103 */
v0 = com.android.server.app.DynamicLoadController.mApps;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
final String v1 = "DynamicLoadController"; // const-string v1, "DynamicLoadController"
/* if-nez v0, :cond_0 */
/* .line 104 */
final String v0 = "invalid packageName, reject!!!"; // const-string v0, "invalid packageName, reject!!!"
android.util.Slog .e ( v1,v0 );
/* .line 105 */
int v0 = 0; // const/4 v0, 0x0
/* .line 108 */
} // :cond_0
com.android.server.app.DynamicLoadController .getCompatChangeId ( p1 );
/* move-result-wide v2 */
/* .line 110 */
/* .local v2, "changeId":J */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 112 */
/* .local v0, "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-nez v4, :cond_1 */
/* .line 113 */
v4 = this.DOWNSCALE_CHANGE_IDS;
/* .local v4, "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
/* .line 115 */
} // .end local v4 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
} // :cond_1
/* const-wide/32 v4, 0xa09e1d7 */
java.lang.Long .valueOf ( v4,v5 );
/* .line 116 */
java.lang.Long .valueOf ( v2,v3 );
/* .line 117 */
v4 = this.DOWNSCALE_CHANGE_IDS;
(( android.util.ArraySet ) v4 ).stream ( ); // invoke-virtual {v4}, Landroid/util/ArraySet;->stream()Ljava/util/stream/Stream;
/* new-instance v5, Lcom/android/server/app/DynamicLoadController$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, v2, v3}, Lcom/android/server/app/DynamicLoadController$$ExternalSyntheticLambda0;-><init>(J)V */
/* .line 118 */
/* .line 119 */
java.util.stream.Collectors .toSet ( );
/* check-cast v4, Ljava/util/Set; */
/* .line 122 */
/* .restart local v4 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
} // :goto_0
/* nop */
/* .line 123 */
final String v5 = "platform_compat"; // const-string v5, "platform_compat"
android.os.ServiceManager .getService ( v5 );
/* check-cast v5, Lcom/android/server/compat/PlatformCompat; */
/* .line 124 */
/* .local v5, "platformCompat":Lcom/android/server/compat/PlatformCompat; */
/* new-instance v6, Lcom/android/internal/compat/CompatibilityChangeConfig; */
/* new-instance v7, Landroid/compat/Compatibility$ChangeConfig; */
/* invoke-direct {v7, v0, v4}, Landroid/compat/Compatibility$ChangeConfig;-><init>(Ljava/util/Set;Ljava/util/Set;)V */
/* invoke-direct {v6, v7}, Lcom/android/internal/compat/CompatibilityChangeConfig;-><init>(Landroid/compat/Compatibility$ChangeConfig;)V */
/* .line 128 */
/* .local v6, "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig; */
(( com.android.server.compat.PlatformCompat ) v5 ).setOverrides ( v6, p2 ); // invoke-virtual {v5, v6, p2}, Lcom/android/server/compat/PlatformCompat;->setOverrides(Lcom/android/internal/compat/CompatibilityChangeConfig;Ljava/lang/String;)V
/* .line 131 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "dynamic load enable packageName = "; // const-string v8, "dynamic load enable packageName = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p2 ); // invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " ratio = "; // const-string v8, " ratio = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v1,v7 );
/* .line 132 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public static Long getCompatChangeId ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "raw" # Ljava/lang/String; */
/* .line 146 */
v0 = (( java.lang.String ) p0 ).hashCode ( ); // invoke-virtual {p0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_0
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v0 = "0.85"; // const-string v0, "0.85"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xb */
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v0 = "0.75"; // const-string v0, "0.75"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x9 */
/* goto/16 :goto_1 */
/* :sswitch_2 */
final String v0 = "0.65"; // const-string v0, "0.65"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 7; // const/4 v0, 0x7
/* goto/16 :goto_1 */
/* :sswitch_3 */
final String v0 = "0.55"; // const-string v0, "0.55"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_4 */
final String v0 = "0.45"; // const-string v0, "0.45"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 3; // const/4 v0, 0x3
/* :sswitch_5 */
final String v0 = "0.35"; // const-string v0, "0.35"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_6 */
final String v0 = "0.9"; // const-string v0, "0.9"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xc */
/* :sswitch_7 */
final String v0 = "0.8"; // const-string v0, "0.8"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0xa */
/* :sswitch_8 */
final String v0 = "0.7"; // const-string v0, "0.7"
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* const/16 v0, 0x8 */
	 /* :sswitch_9 */
	 final String v0 = "0.6"; // const-string v0, "0.6"
	 v0 = 	 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 6; // const/4 v0, 0x6
		 /* :sswitch_a */
		 final String v0 = "0.5"; // const-string v0, "0.5"
		 v0 = 		 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 4; // const/4 v0, 0x4
			 /* :sswitch_b */
			 final String v0 = "0.4"; // const-string v0, "0.4"
			 v0 = 			 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v0 = 2; // const/4 v0, 0x2
				 /* :sswitch_c */
				 final String v0 = "0.3"; // const-string v0, "0.3"
				 v0 = 				 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 int v0 = 0; // const/4 v0, 0x0
				 } // :goto_0
				 int v0 = -1; // const/4 v0, -0x1
			 } // :goto_1
			 /* packed-switch v0, :pswitch_data_0 */
			 /* .line 174 */
			 /* const-wide/16 v0, 0x0 */
			 /* return-wide v0 */
			 /* .line 172 */
			 /* :pswitch_0 */
			 /* const-wide/32 v0, 0xae57a6b */
			 /* return-wide v0 */
			 /* .line 170 */
			 /* :pswitch_1 */
			 /* const-wide/32 v0, 0xb52b546 */
			 /* return-wide v0 */
			 /* .line 168 */
			 /* :pswitch_2 */
			 /* const-wide/32 v0, 0xa8bb021 */
			 /* return-wide v0 */
			 /* .line 166 */
			 /* :pswitch_3 */
			 /* const-wide/32 v0, 0xb52b573 */
			 /* return-wide v0 */
			 /* .line 164 */
			 /* :pswitch_4 */
			 /* const-wide/32 v0, 0xa8bb06d */
			 /* return-wide v0 */
			 /* .line 162 */
			 /* :pswitch_5 */
			 /* const-wide/32 v0, 0xb52b550 */
			 /* return-wide v0 */
			 /* .line 160 */
			 /* :pswitch_6 */
			 /* const-wide/32 v0, 0xa8bb033 */
			 /* return-wide v0 */
			 /* .line 158 */
			 /* :pswitch_7 */
			 /* const-wide/32 v0, 0xb52b674 */
			 /* return-wide v0 */
			 /* .line 156 */
			 /* :pswitch_8 */
			 /* const-wide/32 v0, 0xa8bb015 */
			 /* return-wide v0 */
			 /* .line 154 */
			 /* :pswitch_9 */
			 /* const-wide/32 v0, 0xb52b576 */
			 /* return-wide v0 */
			 /* .line 152 */
			 /* :pswitch_a */
			 /* const-wide/32 v0, 0xb52b676 */
			 /* return-wide v0 */
			 /* .line 150 */
			 /* :pswitch_b */
			 /* const-wide/32 v0, 0xb52b555 */
			 /* return-wide v0 */
			 /* .line 148 */
			 /* :pswitch_c */
			 /* const-wide/32 v0, 0xb52b678 */
			 /* return-wide v0 */
			 /* :sswitch_data_0 */
			 /* .sparse-switch */
			 /* 0xb9f5 -> :sswitch_c */
			 /* 0xb9f6 -> :sswitch_b */
			 /* 0xb9f7 -> :sswitch_a */
			 /* 0xb9f8 -> :sswitch_9 */
			 /* 0xb9f9 -> :sswitch_8 */
			 /* 0xb9fa -> :sswitch_7 */
			 /* 0xb9fb -> :sswitch_6 */
			 /* 0x1684e0 -> :sswitch_5 */
			 /* 0x1684ff -> :sswitch_4 */
			 /* 0x16851e -> :sswitch_3 */
			 /* 0x16853d -> :sswitch_2 */
			 /* 0x16855c -> :sswitch_1 */
			 /* 0x16857b -> :sswitch_0 */
		 } // .end sparse-switch
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_c */
		 /* :pswitch_b */
		 /* :pswitch_a */
		 /* :pswitch_9 */
		 /* :pswitch_8 */
		 /* :pswitch_7 */
		 /* :pswitch_6 */
		 /* :pswitch_5 */
		 /* :pswitch_4 */
		 /* :pswitch_3 */
		 /* :pswitch_2 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
	 } // .end packed-switch
} // .end method
static Boolean lambda$downscaleApp$0 ( Long p0, java.lang.Long p1 ) { //synthethic
	 /* .locals 4 */
	 /* .param p0, "changeId" # J */
	 /* .param p2, "it" # Ljava/lang/Long; */
	 /* .line 118 */
	 (( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
	 /* move-result-wide v0 */
	 /* const-wide/32 v2, 0xa09e1d7 */
	 /* cmp-long v0, v0, v2 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 (( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
		 /* move-result-wide v0 */
		 /* cmp-long v0, v0, p0 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
/* # virtual methods */
public Boolean dynamicLoadWithPackageName ( java.lang.String p0, java.lang.String p1 ) {
	 /* .locals 1 */
	 /* .param p1, "ratio" # Ljava/lang/String; */
	 /* .param p2, "packageName" # Ljava/lang/String; */
	 /* .line 95 */
	 v0 = 	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/app/DynamicLoadController;->downscaleApp(Ljava/lang/String;Ljava/lang/String;)Z */
} // .end method
