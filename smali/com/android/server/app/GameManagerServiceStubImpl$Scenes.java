class com.android.server.app.GameManagerServiceStubImpl$Scenes {
	 /* .source "GameManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GameManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "Scenes" */
} // .end annotation
/* # instance fields */
Integer normal;
java.lang.String padSaveBattery;
Integer saveBattery;
/* # direct methods */
public com.android.server.app.GameManagerServiceStubImpl$Scenes ( ) {
/* .locals 2 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 804 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 805 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 806 */
	 final String v0 = "normal"; // const-string v0, "normal"
	 /* const/16 v1, 0x438 */
	 v0 = 	 (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
	 /* iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->normal:I */
	 /* .line 807 */
	 /* const/16 v0, 0x2d0 */
	 final String v1 = "save_battery"; // const-string v1, "save_battery"
	 v0 = 	 (( org.json.JSONObject ) p1 ).optInt ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
	 /* iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->saveBattery:I */
	 /* .line 808 */
	 final String v0 = "0.85"; // const-string v0, "0.85"
	 (( org.json.JSONObject ) p1 ).optString ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
	 this.padSaveBattery = v0;
	 /* .line 810 */
} // :cond_0
return;
} // .end method
