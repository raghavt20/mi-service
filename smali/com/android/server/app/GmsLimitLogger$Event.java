public abstract class com.android.server.app.GmsLimitLogger$Event {
	 /* .source "GmsLimitLogger.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GmsLimitLogger; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Event" */
} // .end annotation
/* # static fields */
private static final java.text.SimpleDateFormat sFormat;
/* # instance fields */
private final Long mTimestamp;
/* # direct methods */
static com.android.server.app.GmsLimitLogger$Event ( ) {
/* .locals 2 */
/* .line 40 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
final String v1 = "MM-dd HH:mm:ss:SSS"; // const-string v1, "MM-dd HH:mm:ss:SSS"
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
return;
} // .end method
 com.android.server.app.GmsLimitLogger$Event ( ) {
/* .locals 2 */
/* .line 44 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 45 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/app/GmsLimitLogger$Event;->mTimestamp:J */
/* .line 46 */
return;
} // .end method
/* # virtual methods */
public abstract java.lang.String eventToString ( ) {
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 49 */
/* new-instance v0, Ljava/lang/StringBuilder; */
v1 = com.android.server.app.GmsLimitLogger$Event.sFormat;
/* new-instance v2, Ljava/util/Date; */
/* iget-wide v3, p0, Lcom/android/server/app/GmsLimitLogger$Event;->mTimestamp:J */
/* invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v2 ); // invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 50 */
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.app.GmsLimitLogger$Event ) p0 ).eventToString ( ); // invoke-virtual {p0}, Lcom/android/server/app/GmsLimitLogger$Event;->eventToString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 49 */
} // .end method
