public class com.android.server.app.GmsSettings {
	 /* .source "GmsSettings.java" */
	 /* # static fields */
	 private static final java.util.HashSet mLocalDevicesForPowerSaving;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.app.GmsSettings ( ) {
/* .locals 2 */
/* .line 8 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 12 */
final String v1 = "aurora"; // const-string v1, "aurora"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 13 */
/* const-string/jumbo v1, "shennong" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 14 */
final String v1 = "houji"; // const-string v1, "houji"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 15 */
final String v1 = "chenfeng"; // const-string v1, "chenfeng"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 16 */
/* const-string/jumbo v1, "vermeer" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 17 */
final String v1 = "duchamp"; // const-string v1, "duchamp"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 18 */
final String v1 = "manet"; // const-string v1, "manet"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 19 */
final String v1 = "peridot"; // const-string v1, "peridot"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 20 */
final String v1 = "gold"; // const-string v1, "gold"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 21 */
/* const-string/jumbo v1, "sheng" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 23 */
final String v1 = "liuqin"; // const-string v1, "liuqin"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 24 */
/* const-string/jumbo v1, "yudi" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 25 */
final String v1 = "ishtar"; // const-string v1, "ishtar"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 26 */
final String v1 = "marble"; // const-string v1, "marble"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 27 */
final String v1 = "corot"; // const-string v1, "corot"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 28 */
final String v1 = "babylon"; // const-string v1, "babylon"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 29 */
final String v1 = "nuwa"; // const-string v1, "nuwa"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 30 */
final String v1 = "fuxi"; // const-string v1, "fuxi"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 31 */
final String v1 = "redwood"; // const-string v1, "redwood"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 32 */
/* const-string/jumbo v1, "yuechu" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 33 */
final String v1 = "pipa"; // const-string v1, "pipa"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 35 */
/* const-string/jumbo v1, "zeus" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 36 */
final String v1 = "cupid"; // const-string v1, "cupid"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 37 */
final String v1 = "ingres"; // const-string v1, "ingres"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 38 */
/* const-string/jumbo v1, "unicorn" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 39 */
final String v1 = "mayfly"; // const-string v1, "mayfly"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 40 */
/* const-string/jumbo v1, "thor" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 41 */
/* const-string/jumbo v1, "ziyi" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 42 */
final String v1 = "rubens"; // const-string v1, "rubens"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 43 */
final String v1 = "munch"; // const-string v1, "munch"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 44 */
final String v1 = "matisse"; // const-string v1, "matisse"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 46 */
/* const-string/jumbo v1, "venus" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 47 */
final String v1 = "odin"; // const-string v1, "odin"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 48 */
/* const-string/jumbo v1, "star" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 49 */
return;
} // .end method
public com.android.server.app.GmsSettings ( ) {
/* .locals 0 */
/* .line 6 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static void getLocalDevicesForPowerSaving ( java.util.HashSet p0, java.util.HashSet p1 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 57 */
/* .local p0, "devices":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* .local p1, "offLocalDevices":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
final String v0 = "GameManagerServiceStub"; // const-string v0, "GameManagerServiceStub"
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 58 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "cloud devices = "; // const-string v2, "cloud devices = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 59 */
v1 = com.android.server.app.GmsSettings.mLocalDevicesForPowerSaving;
(( java.util.HashSet ) p0 ).addAll ( v1 ); // invoke-virtual {p0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 62 */
} // :cond_0
if ( p0 != null) { // if-eqz p0, :cond_1
if ( p1 != null) { // if-eqz p1, :cond_1
	 v1 = 	 (( java.util.HashSet ) p1 ).isEmpty ( ); // invoke-virtual {p1}, Ljava/util/HashSet;->isEmpty()Z
	 /* if-nez v1, :cond_1 */
	 /* .line 63 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "offLocalDevices = "; // const-string v2, "offLocalDevices = "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v0,v1 );
	 /* .line 64 */
	 (( java.util.HashSet ) p0 ).removeAll ( p1 ); // invoke-virtual {p0, p1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z
	 /* .line 66 */
} // :cond_1
if ( p0 != null) { // if-eqz p0, :cond_2
	 /* .line 67 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "final devices = "; // const-string v2, "final devices = "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v0,v1 );
	 /* .line 69 */
} // :cond_2
return;
} // .end method
