.class Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;
.super Landroid/database/ContentObserver;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/GameManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GMSObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/app/GameManagerServiceStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/app/GameManagerServiceStubImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 819
    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    .line 820
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 821
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 825
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 826
    if-nez p2, :cond_0

    .line 827
    return-void

    .line 829
    :cond_0
    const-string v0, "persist.sys.miui_downscale"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "GameManagerServiceStub"

    if-eqz v1, :cond_3

    .line 830
    const-string v1, "DEBUG_PROP_KEY"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/app/GameManagerServiceStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 833
    .local v0, "proEnable":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmProEnable(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 834
    return-void

    .line 836
    :cond_1
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fputmProEnable(Lcom/android/server/app/GameManagerServiceStubImpl;Ljava/lang/String;)V

    .line 837
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmPowerSaving(Lcom/android/server/app/GameManagerServiceStubImpl;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetintelligentPowerSavingEnable(Lcom/android/server/app/GameManagerServiceStubImpl;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 838
    return-void

    .line 840
    :cond_2
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 841
    .end local v0    # "proEnable":Ljava/lang/String;
    goto/16 :goto_0

    :cond_3
    const-string v0, "POWER_SAVE_MODE_OPEN"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 842
    const-string v0, "KEY_POWER_MODE_OPEN"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 844
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    const v1, 0x1b208

    invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 846
    :cond_4
    const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 847
    const-string v0, "CLOUD_ALL_DATA_CHANGE_URI"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    const v1, 0x1b20a

    invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 849
    :cond_5
    const-string v0, "miui_screen_compat"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_7

    .line 850
    const-string v1, "MIUI_SCREEN_COMPAT"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/app/GameManagerServiceStubImpl;)Landroid/content/Context;

    move-result-object v2

    .line 852
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_6

    move v3, v4

    :cond_6
    invoke-static {v1, v3}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fputintelligentPowerSavingEnable(Lcom/android/server/app/GameManagerServiceStubImpl;Z)V

    .line 853
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    const v1, 0x1b20b

    invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 854
    :cond_7
    const-string v0, "pc_security_center_extreme_mode_enter"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 855
    const-string v1, "PC_SECURITY_CENTER_EXTREME_MODE"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/app/GameManagerServiceStubImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_8

    move v3, v4

    :cond_8
    invoke-static {v1, v3}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fputmExtremeModeEnable(Lcom/android/server/app/GameManagerServiceStubImpl;Z)V

    .line 857
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    move-result-object v0

    const v1, 0x1b20c

    invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendEmptyMessage(I)Z

    .line 859
    :cond_9
    :goto_0
    return-void
.end method
