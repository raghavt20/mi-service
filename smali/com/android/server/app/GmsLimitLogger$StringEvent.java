public class com.android.server.app.GmsLimitLogger$StringEvent extends com.android.server.app.GmsLimitLogger$Event {
	 /* .source "GmsLimitLogger.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GmsLimitLogger; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "StringEvent" */
} // .end annotation
/* # instance fields */
private final java.lang.String mMsg;
/* # direct methods */
public com.android.server.app.GmsLimitLogger$StringEvent ( ) {
/* .locals 0 */
/* .param p1, "msg" # Ljava/lang/String; */
/* .line 67 */
/* invoke-direct {p0}, Lcom/android/server/app/GmsLimitLogger$Event;-><init>()V */
/* .line 68 */
this.mMsg = p1;
/* .line 69 */
return;
} // .end method
/* # virtual methods */
public java.lang.String eventToString ( ) {
/* .locals 1 */
/* .line 73 */
v0 = this.mMsg;
} // .end method
