public class com.android.server.app.PreCacheUtilStubImpl extends com.android.server.app.PreCacheUtilStub {
	 /* .source "PreCacheUtilStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.app.PreCacheUtilStubImpl$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/app/PreCacheUtilStubImpl$PreCacheContentObserver; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_ALL_DATA_CHANGE_URI;
private static final java.lang.String CLOUD_PRE_CACHE_APPS_LIST;
private static final java.lang.String CLOUD_PRE_CACHE_MODULE;
private static final Integer MAX_APP_LIST_PROP_NUM;
private static final Integer MAX_PROP_LENGTH;
private static final java.lang.String PRE_CACHE_APPS_STR_NUM_PROP;
private static final java.lang.String PRE_CACHE_APPS_STR_PROP;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
/* # direct methods */
static void -$$Nest$mupdatePreCacheSetting ( com.android.server.app.PreCacheUtilStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/app/PreCacheUtilStubImpl;->updatePreCacheSetting()V */
return;
} // .end method
protected com.android.server.app.PreCacheUtilStubImpl ( ) {
/* .locals 0 */
/* .line 41 */
/* invoke-direct {p0}, Lcom/android/server/app/PreCacheUtilStub;-><init>()V */
/* .line 43 */
return;
} // .end method
private void updatePreCacheSetting ( ) {
/* .locals 12 */
/* .line 68 */
final String v0 = "app_list"; // const-string v0, "app_list"
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
final String v4 = "precache"; // const-string v4, "precache"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v1,v4,v2,v2,v3 );
/* .line 70 */
/* .local v1, "sData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v2 = "PreCacheUtilStubImpl"; // const-string v2, "PreCacheUtilStubImpl"
/* if-nez v1, :cond_0 */
/* .line 71 */
final String v0 = "no data in precache"; // const-string v0, "no data in precache"
android.util.Slog .d ( v2,v0 );
/* .line 72 */
return;
/* .line 75 */
} // :cond_0
try { // :try_start_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 76 */
/* .local v3, "jsonData":Lorg/json/JSONObject; */
v4 = (( org.json.JSONObject ) v3 ).has ( v0 ); // invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
/* if-nez v4, :cond_1 */
/* .line 77 */
final String v0 = "no app_list"; // const-string v0, "no app_list"
android.util.Slog .d ( v2,v0 );
/* .line 78 */
return;
/* .line 81 */
} // :cond_1
(( org.json.JSONObject ) v3 ).getJSONArray ( v0 ); // invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 82 */
/* .local v0, "appArray":Lorg/json/JSONArray; */
v4 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-nez v4, :cond_2 */
/* .line 83 */
final String v4 = "get member in app_list"; // const-string v4, "get member in app_list"
android.util.Slog .d ( v2,v4 );
/* .line 84 */
return;
/* .line 86 */
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "app number: "; // const-string v5, "app number: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* .line 88 */
int v4 = 0; // const/4 v4, 0x0
/* .line 90 */
/* .local v4, "propNum":I */
final String v5 = ""; // const-string v5, ""
/* .line 91 */
/* .local v5, "propValue":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_0
v7 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v8 = "="; // const-string v8, "="
final String v9 = "persist.sys.precache.appstrs"; // const-string v9, "persist.sys.precache.appstrs"
/* if-ge v6, v7, :cond_6 */
/* .line 92 */
try { // :try_start_1
(( org.json.JSONArray ) v0 ).getString ( v6 ); // invoke-virtual {v0, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 93 */
/* .local v7, "curApp":Ljava/lang/String; */
v10 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
v11 = (( java.lang.String ) v7 ).length ( ); // invoke-virtual {v7}, Ljava/lang/String;->length()I
/* add-int/2addr v10, v11 */
/* const/16 v11, 0x5a */
/* if-ge v10, v11, :cond_4 */
/* .line 94 */
v8 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
/* if-nez v8, :cond_3 */
/* .line 95 */
/* move-object v5, v7 */
/* .line 97 */
} // :cond_3
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ","; // const-string v9, ","
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v5, v8 */
/* .line 100 */
} // :cond_4
/* const/16 v10, 0xa */
/* if-le v4, v10, :cond_5 */
/* .line 101 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "prop number not enough, cur: "; // const-string v9, "prop number not enough, cur: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ", max:"; // const-string v9, ", max:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v8 );
/* .line 102 */
return;
/* .line 104 */
} // :cond_5
/* add-int/lit8 v4, v4, 0x1 */
/* .line 105 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v4 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 106 */
/* .local v9, "propKey":Ljava/lang/String; */
android.os.SystemProperties .set ( v9,v5 );
/* .line 107 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v8 );
/* .line 108 */
/* move-object v5, v7 */
/* .line 91 */
} // .end local v9 # "propKey":Ljava/lang/String;
} // :goto_1
/* add-int/lit8 v6, v6, 0x1 */
/* goto/16 :goto_0 */
/* .line 111 */
} // .end local v6 # "i":I
} // .end local v7 # "curApp":Ljava/lang/String;
} // :cond_6
v6 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
/* if-lez v6, :cond_7 */
/* .line 112 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 113 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v4 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 114 */
/* .local v6, "propKey":Ljava/lang/String; */
android.os.SystemProperties .set ( v6,v5 );
/* .line 115 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v7 );
/* .line 118 */
} // .end local v6 # "propKey":Ljava/lang/String;
} // :cond_7
final String v6 = "persist.sys.precache.number"; // const-string v6, "persist.sys.precache.number"
java.lang.String .valueOf ( v4 );
android.os.SystemProperties .set ( v6,v7 );
/* .line 119 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "persist.sys.precache.number="; // const-string v7, "persist.sys.precache.number="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v4 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v6 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 122 */
/* nop */
} // .end local v0 # "appArray":Lorg/json/JSONArray;
} // .end local v3 # "jsonData":Lorg/json/JSONObject;
} // .end local v4 # "propNum":I
} // .end local v5 # "propValue":Ljava/lang/String;
/* .line 120 */
/* :catch_0 */
/* move-exception v0 */
/* .line 121 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 123 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void registerPreCacheObserver ( android.content.Context p0, android.os.Handler p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 47 */
this.mContext = p1;
/* .line 48 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 49 */
final String v1 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v1, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v1 );
/* new-instance v2, Lcom/android/server/app/PreCacheUtilStubImpl$PreCacheContentObserver; */
/* invoke-direct {v2, p0, p2}, Lcom/android/server/app/PreCacheUtilStubImpl$PreCacheContentObserver;-><init>(Lcom/android/server/app/PreCacheUtilStubImpl;Landroid/os/Handler;)V */
/* .line 48 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 51 */
final String v0 = "PreCacheUtilStubImpl"; // const-string v0, "PreCacheUtilStubImpl"
final String v1 = "registerContentObserver"; // const-string v1, "registerContentObserver"
android.util.Slog .d ( v0,v1 );
/* .line 52 */
return;
} // .end method
