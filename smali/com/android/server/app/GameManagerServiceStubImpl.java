public class com.android.server.app.GameManagerServiceStubImpl extends com.android.server.app.GameManagerServiceStub {
	 /* .source "GameManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.app.GameManagerServiceStub$$" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;, */
/* Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;, */
/* Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;, */
/* Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;, */
/* Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;, */
/* Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;, */
/* Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem; */
/* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_ALL_DATA_CHANGE_URI;
private static final java.lang.String DEBUG_DOWNSCALE_POWER_SAVE;
private static final java.lang.String DEBUG_PROP_KEY;
private static final java.lang.String DOWNSCALE_APP_SETTINGS_FILE_PATH;
private static final java.lang.String DOWNSCALE_DISABLE;
private static final java.lang.String DOWNSCALE_ENABLE;
private static final Boolean FOLD_DEVICE;
private static final java.lang.String KEY_POWER_MODE_OPEN;
public static final Integer MIUI_PAD_SCREEN_COMPAT_VERSION;
public static final java.lang.String MIUI_RESOLUTION;
private static final java.lang.String MIUI_SCREEN_COMPAT;
public static final Integer MIUI_SCREEN_COMPAT_VERSION;
static final Integer MIUI_SCREEN_COMPAT_WIDTH;
private static final java.lang.String MODULE_NAME_DOWNSCALE;
public static final Integer MSG_CLOUD_DATA_CHANGE;
public static final Integer MSG_DOWNSCALE_APP_DIED;
public static final Integer MSG_EXTREME_MODE;
public static final Integer MSG_INTELLIGENT_POWERSAVE;
public static final Integer MSG_PROCESS_POWERSAVE_ACTION;
public static final Integer MSG_REBOOT_COMPLETED_ACTION;
private static final java.lang.String PC_SECURITY_CENTER_EXTREME_MODE;
public static final Float SCALE_MIN;
static final java.lang.String TAG;
/* # instance fields */
private java.lang.String cloudDataStr;
private Integer currentWidth;
private Boolean fromCloud;
private Boolean intelligentPowerSavingEnable;
private Boolean isDebugPowerSave;
private com.android.server.app.GmsLimitLogger limitLogger;
private java.util.HashMap mAppStates;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private java.util.HashMap mDownscaleApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.app.GameManagerServiceStubImpl$DownscaleCloudData mDownscaleCloudData;
private Boolean mExtremeModeEnable;
private com.android.server.app.GameManagerServiceStubImpl$InnerHandler mHandler;
public final com.android.server.ServiceThread mHandlerThread;
private com.android.server.app.GameManagerServiceStubImpl$GMSObserver mObserver;
private volatile Boolean mPowerSaving;
private java.lang.String mProEnable;
private com.android.server.am.ActivityManagerService mService;
private java.util.HashSet mShellCmdDownscalePackageNames;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashSet mSizeCompatApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
com.android.server.wm.WindowManagerService mWindowManager;
/* # direct methods */
static Boolean -$$Nest$fgetintelligentPowerSavingEnable ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.app.GameManagerServiceStubImpl$InnerHandler -$$Nest$fgetmHandler ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmPowerSaving ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
} // .end method
static java.lang.String -$$Nest$fgetmProEnable ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProEnable;
} // .end method
static java.util.HashSet -$$Nest$fgetmShellCmdDownscalePackageNames ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mShellCmdDownscalePackageNames;
} // .end method
static java.util.HashSet -$$Nest$fgetmSizeCompatApps ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSizeCompatApps;
} // .end method
static void -$$Nest$fputintelligentPowerSavingEnable ( com.android.server.app.GameManagerServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
return;
} // .end method
static void -$$Nest$fputmExtremeModeEnable ( com.android.server.app.GameManagerServiceStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z */
return;
} // .end method
static void -$$Nest$fputmProEnable ( com.android.server.app.GameManagerServiceStubImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mProEnable = p1;
return;
} // .end method
static void -$$Nest$mexecuteDownscale ( com.android.server.app.GameManagerServiceStubImpl p0, java.lang.String p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V */
return;
} // .end method
static void -$$Nest$mprocessBootCompletedAction ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processBootCompletedAction()V */
return;
} // .end method
static void -$$Nest$mprocessCloudData ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processCloudData()V */
return;
} // .end method
static void -$$Nest$mprocessIntelligentPowerSave ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processIntelligentPowerSave()V */
return;
} // .end method
static void -$$Nest$mprocessPowerSaveAction ( com.android.server.app.GameManagerServiceStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processPowerSaveAction()V */
return;
} // .end method
static Boolean -$$Nest$sfgetFOLD_DEVICE ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z */
} // .end method
static com.android.server.app.GameManagerServiceStubImpl ( ) {
/* .locals 1 */
/* .line 82 */
v0 = miui.util.MiuiMultiDisplayTypeInfo .isFoldDeviceInside ( );
com.android.server.app.GameManagerServiceStubImpl.FOLD_DEVICE = (v0!= 0);
return;
} // .end method
public com.android.server.app.GameManagerServiceStubImpl ( ) {
/* .locals 4 */
/* .line 67 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStub;-><init>()V */
/* .line 93 */
/* new-instance v0, Lcom/android/server/ServiceThread; */
final String v1 = "GameManagerServiceStub"; // const-string v1, "GameManagerServiceStub"
int v2 = -2; // const/4 v2, -0x2
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
this.mHandlerThread = v0;
/* .line 109 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mDownscaleApps = v0;
/* .line 114 */
final String v0 = "enable"; // const-string v0, "enable"
this.mProEnable = v0;
/* .line 115 */
/* iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z */
/* .line 125 */
/* iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
/* .line 127 */
/* const/16 v0, 0x438 */
/* iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
/* .line 128 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mSizeCompatApps = v0;
/* .line 132 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mShellCmdDownscalePackageNames = v0;
/* .line 134 */
/* iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z */
/* .line 137 */
/* iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 138 */
final String v0 = ""; // const-string v0, ""
this.cloudDataStr = v0;
/* .line 139 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAppStates = v0;
/* .line 140 */
/* new-instance v0, Lcom/android/server/app/GmsLimitLogger; */
/* const/16 v1, 0xc8 */
/* invoke-direct {v0, v1}, Lcom/android/server/app/GmsLimitLogger;-><init>(I)V */
this.limitLogger = v0;
return;
} // .end method
private void appsDownscale ( java.lang.String p0, Boolean p1 ) {
/* .locals 6 */
/* .param p1, "packageNameParam" # Ljava/lang/String; */
/* .param p2, "ignoreRunningApps" # Z */
/* .line 376 */
v0 = this.mDownscaleCloudData;
v0 = this.apps;
/* .line 377 */
/* .local v0, "apps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;" */
final String v1 = "persist.sys.miui_resolution"; // const-string v1, "persist.sys.miui_resolution"
android.os.SystemProperties .get ( v1 );
/* .line 378 */
/* .local v1, "currentResolution":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_1 */
/* .line 379 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 380 */
/* .local v2, "resolutionArr":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* array-length v4, v2 */
/* if-lez v4, :cond_0 */
/* .line 382 */
try { // :try_start_0
/* aget-object v3, v2, v3 */
v3 = java.lang.Integer .parseInt ( v3 );
/* iput v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 384 */
/* .line 383 */
/* :catch_0 */
/* move-exception v3 */
/* .line 386 */
} // .end local v2 # "resolutionArr":[Ljava/lang/String;
} // :cond_0
} // :goto_0
/* .line 387 */
} // :cond_1
/* new-instance v2, Landroid/graphics/Point; */
/* invoke-direct {v2}, Landroid/graphics/Point;-><init>()V */
/* .line 388 */
/* .local v2, "point":Landroid/graphics/Point; */
v4 = this.mWindowManager;
(( com.android.server.wm.WindowManagerService ) v4 ).getBaseDisplaySize ( v3, v2 ); // invoke-virtual {v4, v3, v2}, Lcom/android/server/wm/WindowManagerService;->getBaseDisplaySize(ILandroid/graphics/Point;)V
/* .line 389 */
/* iget v3, v2, Landroid/graphics/Point;->x:I */
/* .line 390 */
/* .local v3, "baseDisplayWidth":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "baseDisplayWidth = "; // const-string v5, "baseDisplayWidth = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "GameManagerServiceStub"; // const-string v5, "GameManagerServiceStub"
android.util.Slog .v ( v5,v4 );
/* .line 391 */
/* if-lez v3, :cond_2 */
/* .line 392 */
/* iput v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
/* .line 396 */
} // .end local v2 # "point":Landroid/graphics/Point;
} // .end local v3 # "baseDisplayWidth":I
} // :cond_2
} // :goto_1
v2 = android.text.TextUtils .isEmpty ( p1 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 397 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 398 */
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v3 = } // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 400 */
/* .local v3, "appItemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;" */
/* check-cast v4, Ljava/lang/String; */
/* check-cast v5, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
/* invoke-direct {p0, v4, v5, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->downscaleApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;Z)V */
/* .line 401 */
} // .end local v3 # "appItemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
} // :cond_3
/* .line 405 */
} // :cond_4
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 406 */
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
/* .line 407 */
/* .local v2, "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 408 */
v3 = this.packageName;
/* invoke-direct {p0, v3, v2, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->downscaleApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;Z)V */
/* .line 412 */
} // .end local v2 # "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
} // :cond_5
} // :goto_3
return;
} // .end method
private void downscaleApp ( java.lang.String p0, com.android.server.app.GameManagerServiceStubImpl$AppItem p1, Boolean p2 ) {
/* .locals 12 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "appItem" # Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
/* .param p3, "ignoreRunningApps" # Z */
/* .line 473 */
/* iget-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z */
/* if-nez v0, :cond_0 */
v0 = this.mDownscaleCloudData;
/* iget v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
v0 = com.android.server.app.GmsUtil .needDownscale ( p1,v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 474 */
return;
/* .line 476 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_a */
if ( p2 != null) { // if-eqz p2, :cond_a
/* .line 478 */
v0 = this.mProEnable;
final String v1 = "enable"; // const-string v1, "enable"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = "GameManagerServiceStub"; // const-string v2, "GameManagerServiceStub"
if ( v0 != null) { // if-eqz v0, :cond_1
if ( p3 != null) { // if-eqz p3, :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl;->runningApp(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 479 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "packageName = "; // const-string v1, "packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "is running do not downscale"; // const-string v1, "is running do not downscale"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v0 );
/* .line 480 */
return;
/* .line 482 */
} // :cond_1
final String v0 = "disable"; // const-string v0, "disable"
/* .line 483 */
/* .local v0, "ratioResult":Ljava/lang/String; */
/* sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v3, :cond_3 */
/* .line 484 */
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->getTargetWidth(Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;)I */
/* .line 485 */
/* .local v1, "targetWidth":I */
int v3 = -1; // const/4 v3, -0x1
/* if-eq v1, v3, :cond_2 */
/* .line 486 */
/* iget v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
com.android.server.app.GmsUtil .calcuRatio ( v1,v3 );
/* .line 488 */
} // .end local v1 # "targetWidth":I
} // :cond_2
/* .line 489 */
} // :cond_3
v3 = this.mProEnable;
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mDownscaleCloudData;
/* iget-boolean v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 490 */
/* iget-boolean v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
v3 = this.mDownscaleCloudData;
/* iget-boolean v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z */
com.android.server.app.GmsUtil .getTargetRatioForPad ( p2,v1,v3,v4 );
/* .line 495 */
} // :cond_4
} // :goto_0
/* sget-boolean v1, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z */
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_6
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v1 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v1 ).getAspectRatio ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectRatio(Ljava/lang/String;)F
/* cmpl-float v1, v1, v3 */
/* if-gtz v1, :cond_5 */
/* .line 496 */
v1 = com.android.server.wm.MiuiEmbeddingWindowServiceStub .get ( );
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 497 */
} // :cond_5
final String v0 = "disable"; // const-string v0, "disable"
/* .line 500 */
} // :cond_6
com.android.server.wm.WindowManagerServiceStub .get ( );
v1 = int v4 = 0; // const/4 v4, 0x0
/* .line 501 */
/* .local v1, "currentScale":F */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "downscale currentScale = "; // const-string v5, "downscale currentScale = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v4 );
/* .line 502 */
/* cmpl-float v3, v1, v3 */
final String v4 = ","; // const-string v4, ","
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 503 */
/* const/high16 v3, 0x3f800000 # 1.0f */
/* div-float v1, v3, v1 */
/* .line 504 */
java.lang.String .valueOf ( v1 );
v5 = (( java.lang.String ) v0 ).equals ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_7 */
/* .line 505 */
final String v5 = "disable"; // const-string v5, "disable"
v5 = android.text.TextUtils .equals ( v0,v5 );
if ( v5 != null) { // if-eqz v5, :cond_8
/* cmpl-float v3, v1, v3 */
/* if-nez v3, :cond_8 */
/* .line 506 */
} // :cond_7
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "downscale "; // const-string v5, "downscale "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " not change !!"; // const-string v5, " not change !!"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v3 );
/* .line 507 */
this.ratio = v0;
/* .line 508 */
v2 = this.mAppStates;
(( java.util.HashMap ) v2 ).put ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 509 */
v2 = this.limitLogger;
/* new-instance v3, Lcom/android/server/app/GmsLimitLogger$StringEvent; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v3, v4}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V */
(( com.android.server.app.GmsLimitLogger ) v2 ).log ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V
/* .line 510 */
return;
/* .line 514 */
} // :cond_8
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).getCompatChangeId ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->getCompatChangeId(Ljava/lang/String;)J
/* move-result-wide v5 */
/* .line 516 */
/* .local v5, "changeId":J */
/* new-instance v3, Landroid/util/ArraySet; */
/* invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V */
/* .line 518 */
/* .local v3, "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
/* const-wide/16 v7, 0x0 */
/* cmp-long v7, v5, v7 */
/* if-nez v7, :cond_9 */
/* .line 519 */
v7 = this.DOWNSCALE_CHANGE_IDS;
/* .local v7, "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
/* .line 521 */
} // .end local v7 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
} // :cond_9
/* const-wide/32 v7, 0xa09e1d7 */
java.lang.Long .valueOf ( v7,v8 );
/* .line 522 */
java.lang.Long .valueOf ( v5,v6 );
/* .line 523 */
v7 = this.DOWNSCALE_CHANGE_IDS;
(( android.util.ArraySet ) v7 ).stream ( ); // invoke-virtual {v7}, Landroid/util/ArraySet;->stream()Ljava/util/stream/Stream;
/* new-instance v8, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v8, v5, v6}, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda1;-><init>(J)V */
/* .line 524 */
/* .line 525 */
java.util.stream.Collectors .toSet ( );
/* check-cast v7, Ljava/util/Set; */
/* .line 528 */
/* .restart local v7 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
} // :goto_1
/* nop */
/* .line 529 */
final String v8 = "platform_compat"; // const-string v8, "platform_compat"
android.os.ServiceManager .getService ( v8 );
/* check-cast v8, Lcom/android/server/compat/PlatformCompat; */
/* .line 530 */
/* .local v8, "platformCompat":Lcom/android/server/compat/PlatformCompat; */
/* new-instance v9, Lcom/android/internal/compat/CompatibilityChangeConfig; */
/* new-instance v10, Landroid/compat/Compatibility$ChangeConfig; */
/* invoke-direct {v10, v3, v7}, Landroid/compat/Compatibility$ChangeConfig;-><init>(Ljava/util/Set;Ljava/util/Set;)V */
/* invoke-direct {v9, v10}, Lcom/android/internal/compat/CompatibilityChangeConfig;-><init>(Landroid/compat/Compatibility$ChangeConfig;)V */
/* .line 534 */
/* .local v9, "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig; */
(( com.android.server.compat.PlatformCompat ) v8 ).setOverridesForDownscale ( v9, p1 ); // invoke-virtual {v8, v9, p1}, Lcom/android/server/compat/PlatformCompat;->setOverridesForDownscale(Lcom/android/internal/compat/CompatibilityChangeConfig;Ljava/lang/String;)V
/* .line 535 */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "downscale enable packageName = "; // const-string v11, "downscale enable packageName = "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( p1 ); // invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " ratio = "; // const-string v11, " ratio = "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v10 );
/* .line 536 */
this.ratio = v0;
/* .line 537 */
v2 = this.mAppStates;
(( java.util.HashMap ) v2 ).put ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 538 */
v2 = this.limitLogger;
/* new-instance v10, Lcom/android/server/app/GmsLimitLogger$StringEvent; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( p1 ); // invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v10, v4}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V */
(( com.android.server.app.GmsLimitLogger ) v2 ).log ( v10 ); // invoke-virtual {v2, v10}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V
/* .line 540 */
} // .end local v0 # "ratioResult":Ljava/lang/String;
} // .end local v1 # "currentScale":F
} // .end local v3 # "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
} // .end local v5 # "changeId":J
} // .end local v7 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
} // .end local v8 # "platformCompat":Lcom/android/server/compat/PlatformCompat;
} // .end local v9 # "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
} // :cond_a
return;
} // .end method
private void executeDownscale ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "packageNameParam" # Ljava/lang/String; */
/* .param p2, "ignoreRunningApps" # Z */
/* .line 363 */
v0 = this.mDownscaleCloudData;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 364 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->appsDownscale(Ljava/lang/String;Z)V */
/* .line 365 */
return;
/* .line 367 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 368 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->initLocalDownscaleAppList()Lorg/json/JSONObject; */
/* .line 369 */
/* .local v0, "jsonAll":Lorg/json/JSONObject; */
/* new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* invoke-direct {v1, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V */
this.mDownscaleCloudData = v1;
/* .line 370 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 372 */
} // .end local v0 # "jsonAll":Lorg/json/JSONObject;
} // :cond_1
/* invoke-direct {p0, p1, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->appsDownscale(Ljava/lang/String;Z)V */
/* .line 373 */
return;
} // .end method
private Integer getTargetWidth ( com.android.server.app.GameManagerServiceStubImpl$AppItem p0 ) {
/* .locals 14 */
/* .param p1, "appItem" # Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
/* .line 415 */
final String v0 = "enable"; // const-string v0, "enable"
v1 = this.mProEnable;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = -1; // const/4 v1, -0x1
if ( v0 != null) { // if-eqz v0, :cond_15
v0 = this.mDownscaleCloudData;
/* iget-boolean v0, v0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_8 */
/* .line 418 */
} // :cond_0
/* iget v0, p1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->mode:I */
/* .line 419 */
/* .local v0, "mode":I */
/* if-nez v0, :cond_1 */
/* .line 420 */
/* .line 422 */
} // :cond_1
/* iget v2, p1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->systemVersion:I */
/* .line 423 */
/* .local v2, "systemVersion":I */
/* if-nez v2, :cond_2 */
/* .line 424 */
/* .line 426 */
} // :cond_2
/* iget v3, p1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->appVersion:I */
/* .line 427 */
/* .local v3, "appVersion":I */
int v4 = 3; // const/4 v4, 0x3
/* if-le v3, v4, :cond_3 */
/* .line 428 */
/* .line 431 */
} // :cond_3
/* and-int/lit8 v4, v0, 0x1 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
if ( v4 != null) { // if-eqz v4, :cond_4
/* move v4, v6 */
} // :cond_4
/* move v4, v5 */
/* .line 432 */
/* .local v4, "normalEnable":Z */
} // :goto_0
/* and-int/lit8 v7, v0, 0x2 */
if ( v7 != null) { // if-eqz v7, :cond_5
/* move v7, v6 */
} // :cond_5
/* move v7, v5 */
/* .line 433 */
/* .local v7, "saveBatteryEnable":Z */
} // :goto_1
/* iget-boolean v8, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z */
/* if-nez v8, :cond_7 */
v8 = this.mDownscaleCloudData;
v8 = this.devices;
v8 = com.android.server.app.GmsUtil .isContainDevice ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_6
} // :cond_6
/* move v8, v5 */
} // :cond_7
} // :goto_2
/* move v8, v6 */
/* .line 434 */
/* .local v8, "containDevice":Z */
} // :goto_3
/* and-int/lit8 v9, v2, 0x1 */
if ( v9 != null) { // if-eqz v9, :cond_8
/* move v9, v6 */
} // :cond_8
/* move v9, v5 */
/* .line 435 */
/* .local v9, "preVersionEnable":Z */
} // :goto_4
/* and-int/lit8 v10, v2, 0x2 */
if ( v10 != null) { // if-eqz v10, :cond_9
/* move v10, v6 */
} // :cond_9
/* move v10, v5 */
/* .line 436 */
/* .local v10, "devVersionEnable":Z */
} // :goto_5
/* and-int/lit8 v11, v2, 0x4 */
if ( v11 != null) { // if-eqz v11, :cond_a
} // :cond_a
/* move v6, v5 */
/* .line 437 */
/* .local v6, "stableVersionEnable":Z */
} // :goto_6
if ( v9 != null) { // if-eqz v9, :cond_b
/* sget-boolean v11, Lmiui/os/Build;->IS_PRE_VERSION:Z */
/* if-nez v11, :cond_d */
} // :cond_b
if ( v10 != null) { // if-eqz v10, :cond_c
/* sget-boolean v11, Lmiui/os/Build;->IS_DEV_VERSION:Z */
/* if-nez v11, :cond_d */
} // :cond_c
if ( v6 != null) { // if-eqz v6, :cond_14
/* sget-boolean v11, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
/* if-nez v11, :cond_d */
/* .line 440 */
} // :cond_d
/* iget-boolean v11, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z */
if ( v11 != null) { // if-eqz v11, :cond_e
/* .line 441 */
v1 = this.mDownscaleCloudData;
v1 = this.scenes;
/* iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->saveBattery:I */
/* .line 444 */
} // :cond_e
/* iget-boolean v11, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
if ( v11 != null) { // if-eqz v11, :cond_10
if ( v7 != null) { // if-eqz v7, :cond_10
if ( v8 != null) { // if-eqz v8, :cond_10
/* .line 445 */
/* iget v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
/* const/16 v5, 0x2d0 */
/* if-ne v1, v5, :cond_f */
/* .line 446 */
/* const/16 v1, 0x21c */
/* .line 449 */
} // :cond_f
v1 = this.mDownscaleCloudData;
v1 = this.scenes;
/* iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->saveBattery:I */
/* .line 452 */
} // :cond_10
/* const-string/jumbo v11, "screen_resolution_supported" */
miui.util.FeatureParser .getIntArray ( v11 );
/* .line 453 */
/* .local v11, "resolutionArray":[I */
final String v12 = "screen_compat_supported"; // const-string v12, "screen_compat_supported"
v5 = miui.util.FeatureParser .getBoolean ( v12,v5 );
/* .line 455 */
/* .local v5, "screenCompatSupported":Z */
/* sget-boolean v12, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v12, :cond_12 */
/* iget-boolean v12, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
if ( v12 != null) { // if-eqz v12, :cond_12
if ( v4 != null) { // if-eqz v4, :cond_12
/* iget v12, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
/* const/16 v13, 0x438 */
/* if-le v12, v13, :cond_12 */
/* if-nez v11, :cond_11 */
if ( v5 != null) { // if-eqz v5, :cond_12
/* .line 459 */
} // :cond_11
v1 = this.mDownscaleCloudData;
v1 = this.scenes;
/* iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->normal:I */
/* .line 460 */
} // :cond_12
/* sget-boolean v12, Lmiui/os/Build;->IS_TABLET:Z */
if ( v12 != null) { // if-eqz v12, :cond_13
/* iget-boolean v12, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
if ( v12 != null) { // if-eqz v12, :cond_13
if ( v4 != null) { // if-eqz v4, :cond_13
/* .line 462 */
v1 = this.mDownscaleCloudData;
v1 = this.scenes;
/* iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->normal:I */
/* .line 464 */
} // :cond_13
/* .line 438 */
} // .end local v5 # "screenCompatSupported":Z
} // .end local v11 # "resolutionArray":[I
} // :cond_14
} // :goto_7
/* .line 416 */
} // .end local v0 # "mode":I
} // .end local v2 # "systemVersion":I
} // .end local v3 # "appVersion":I
} // .end local v4 # "normalEnable":Z
} // .end local v6 # "stableVersionEnable":Z
} // .end local v7 # "saveBatteryEnable":Z
} // .end local v8 # "containDevice":Z
} // .end local v9 # "preVersionEnable":Z
} // .end local v10 # "devVersionEnable":Z
} // :cond_15
} // :goto_8
} // .end method
private org.json.JSONObject initLocalDownscaleAppList ( ) {
/* .locals 5 */
/* .line 543 */
final String v0 = ""; // const-string v0, ""
/* .line 545 */
/* .local v0, "jsonString":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* const-string/jumbo v2, "system/etc/DownscaleAppSettings.json" */
/* invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 546 */
/* .local v1, "f":Ljava/io/FileInputStream; */
try { // :try_start_1
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/InputStreamReader; */
/* invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 548 */
/* .local v2, "bis":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 549 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* move-object v0, v3 */
/* .line 551 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 553 */
} // .end local v2 # "bis":Ljava/io/BufferedReader;
/* .line 546 */
/* .restart local v2 # "bis":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_4
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_5
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "jsonString":Ljava/lang/String;
} // .end local v1 # "f":Ljava/io/FileInputStream;
} // .end local p0 # "this":Lcom/android/server/app/GameManagerServiceStubImpl;
} // :goto_1
/* throw v3 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 545 */
} // .end local v2 # "bis":Ljava/io/BufferedReader;
/* .restart local v0 # "jsonString":Ljava/lang/String; */
/* .restart local v1 # "f":Ljava/io/FileInputStream; */
/* .restart local p0 # "this":Lcom/android/server/app/GameManagerServiceStubImpl; */
/* :catchall_2 */
/* move-exception v2 */
/* .line 551 */
/* :catch_0 */
/* move-exception v2 */
/* .line 552 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_6
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 554 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
try { // :try_start_7
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_1 */
/* .line 557 */
} // .end local v1 # "f":Ljava/io/FileInputStream;
/* .line 545 */
/* .restart local v1 # "f":Ljava/io/FileInputStream; */
} // :goto_3
try { // :try_start_8
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* :catchall_3 */
/* move-exception v3 */
try { // :try_start_9
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "jsonString":Ljava/lang/String;
} // .end local p0 # "this":Lcom/android/server/app/GameManagerServiceStubImpl;
} // :goto_4
/* throw v2 */
/* :try_end_9 */
/* .catch Ljava/lang/Exception; {:try_start_9 ..:try_end_9} :catch_1 */
/* .line 554 */
} // .end local v1 # "f":Ljava/io/FileInputStream;
/* .restart local v0 # "jsonString":Ljava/lang/String; */
/* .restart local p0 # "this":Lcom/android/server/app/GameManagerServiceStubImpl; */
/* :catch_1 */
/* move-exception v1 */
/* .line 556 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "GameManagerServiceStub"; // const-string v2, "GameManagerServiceStub"
/* const-string/jumbo v3, "system/etc/DownscaleAppSettings.json not find" */
android.util.Slog .v ( v2,v3 );
/* .line 560 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_5
try { // :try_start_a
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_2 */
/* .line 561 */
/* .local v1, "appConfigJSON":Lorg/json/JSONObject; */
/* .line 562 */
} // .end local v1 # "appConfigJSON":Lorg/json/JSONObject;
/* :catch_2 */
/* move-exception v1 */
/* .line 565 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
static Boolean lambda$downscaleApp$1 ( Long p0, java.lang.Long p1 ) { //synthethic
/* .locals 4 */
/* .param p0, "changeId" # J */
/* .param p2, "it" # Ljava/lang/Long; */
/* .line 524 */
(( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* const-wide/32 v2, 0xa09e1d7 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* cmp-long v0, v0, p0 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
static Boolean lambda$shellCmd$0 ( Long p0, java.lang.Long p1 ) { //synthethic
/* .locals 4 */
/* .param p0, "changeId" # J */
/* .param p2, "it" # Ljava/lang/Long; */
/* .line 338 */
(( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* const-wide/32 v2, 0xa09e1d7 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* cmp-long v0, v0, p0 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void processBootCompletedAction ( ) {
/* .locals 5 */
/* .line 261 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "POWER_SAVE_MODE_OPEN"; // const-string v1, "POWER_SAVE_MODE_OPEN"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 262 */
/* .local v0, "powerModeOpen":Z */
} // :goto_0
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "pc_security_center_extreme_mode_enter"; // const-string v4, "pc_security_center_extreme_mode_enter"
v3 = android.provider.Settings$Secure .getInt ( v3,v4,v2 );
/* if-ne v3, v1, :cond_1 */
/* move v2, v1 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z */
/* .line 263 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ACTION_BOOT_COMPLETED --- powerModeOpen= "; // const-string v3, "ACTION_BOOT_COMPLETED --- powerModeOpen= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " mPowerSaving = "; // const-string v3, " mPowerSaving = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GameManagerServiceStub"; // const-string v3, "GameManagerServiceStub"
android.util.Slog .v ( v3,v2 );
/* .line 265 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->updateLocalDataBootCompleted()V */
/* .line 266 */
/* if-nez v0, :cond_2 */
/* iget-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 267 */
} // :cond_2
/* iput-boolean v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
/* .line 269 */
} // :cond_3
v1 = this.limitLogger;
/* new-instance v2, Lcom/android/server/app/GmsLimitLogger$StringEvent; */
final String v3 = "---rebootCompleted---"; // const-string v3, "---rebootCompleted---"
/* invoke-direct {v2, v3}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V */
(( com.android.server.app.GmsLimitLogger ) v1 ).log ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V
/* .line 270 */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).toggleDownscale ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V
/* .line 271 */
return;
} // .end method
private void processCloudData ( ) {
/* .locals 9 */
/* .line 193 */
int v0 = 0; // const/4 v0, 0x0
/* .line 194 */
/* .local v0, "jsonAll":Lorg/json/JSONObject; */
v1 = this.mContext;
/* .line 195 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 194 */
/* const-string/jumbo v2, "tdownscale" */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v1,v2,v2,v3,v4 );
/* .line 198 */
/* .local v1, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v1 != null) { // if-eqz v1, :cond_4
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* if-nez v2, :cond_0 */
/* .line 201 */
} // :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 203 */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
this.cloudDataStr = v2;
/* .line 204 */
/* const-wide/16 v4, -0x1 */
/* .line 205 */
/* .local v4, "version":J */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 206 */
/* const-string/jumbo v2, "version" */
(( org.json.JSONObject ) v0 ).optLong ( v2 ); // invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J
/* move-result-wide v4 */
/* .line 208 */
} // :cond_1
v2 = this.mDownscaleCloudData;
int v6 = 1; // const/4 v6, 0x1
/* if-nez v2, :cond_2 */
/* .line 209 */
/* new-instance v2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V */
/* .line 210 */
/* .local v2, "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
this.mDownscaleCloudData = v2;
/* .line 211 */
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).toggleDownscale ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V
/* .line 212 */
/* iput-boolean v6, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 213 */
} // .end local v2 # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
/* .line 215 */
} // :cond_2
/* iget-wide v7, v2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->version:J */
/* cmp-long v2, v7, v4 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* const-wide/16 v7, -0x1 */
/* cmp-long v2, v4, v7 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 216 */
/* new-instance v2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V */
/* .line 217 */
/* .restart local v2 # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
this.mDownscaleCloudData = v2;
/* .line 218 */
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).toggleDownscale ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V
/* .line 219 */
/* iput-boolean v6, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 222 */
} // .end local v2 # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
} // :cond_3
} // :goto_0
return;
/* .line 199 */
} // .end local v4 # "version":J
} // :cond_4
} // :goto_1
return;
} // .end method
private void processIntelligentPowerSave ( ) {
/* .locals 2 */
/* .line 681 */
final String v0 = "disable"; // const-string v0, "disable"
v1 = this.mProEnable;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 682 */
return;
/* .line 684 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V */
/* .line 685 */
return;
} // .end method
private void processPowerSaveAction ( ) {
/* .locals 3 */
/* .line 275 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "POWER_SAVE_MODE_OPEN"; // const-string v1, "POWER_SAVE_MODE_OPEN"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
/* .line 276 */
final String v0 = "disable"; // const-string v0, "disable"
v1 = this.mProEnable;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 277 */
return;
/* .line 279 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).toggleDownscale ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V
/* .line 280 */
return;
} // .end method
private Boolean runningApp ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 596 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_3 */
v0 = this.mService;
/* if-nez v0, :cond_0 */
/* .line 597 */
} // :cond_0
(( com.android.server.am.ActivityManagerService ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->getRunningAppProcesses()Ljava/util/List;
/* .line 598 */
/* .local v0, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 599 */
/* .local v3, "runningAppProcess":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v4 = this.processName;
v4 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 600 */
int v1 = 1; // const/4 v1, 0x1
/* .line 602 */
} // .end local v3 # "runningAppProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_1
/* .line 603 */
} // :cond_2
/* .line 596 */
} // .end local v0 # "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
} // :cond_3
} // :goto_1
} // .end method
private void updateLocalDataBootCompleted ( ) {
/* .locals 9 */
/* .line 228 */
int v0 = 0; // const/4 v0, 0x0
/* .line 229 */
/* .local v0, "jsonAll":Lorg/json/JSONObject; */
v1 = this.mContext;
/* .line 230 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 229 */
/* const-string/jumbo v2, "tdownscale" */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v1,v2,v2,v3,v4 );
/* .line 233 */
/* .local v1, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v1 != null) { // if-eqz v1, :cond_4
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* if-nez v2, :cond_0 */
/* .line 236 */
} // :cond_0
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v1 ).json ( ); // invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 238 */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
this.cloudDataStr = v2;
/* .line 239 */
/* const-wide/16 v2, -0x1 */
/* .line 240 */
/* .local v2, "version":J */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 241 */
/* const-string/jumbo v4, "version" */
(( org.json.JSONObject ) v0 ).optLong ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J
/* move-result-wide v2 */
/* .line 243 */
} // :cond_1
v4 = this.mDownscaleCloudData;
int v5 = 1; // const/4 v5, 0x1
final String v6 = "GameManagerServiceStub"; // const-string v6, "GameManagerServiceStub"
/* if-nez v4, :cond_2 */
/* .line 244 */
/* new-instance v4, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* invoke-direct {v4, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V */
/* .line 245 */
/* .local v4, "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
this.mDownscaleCloudData = v4;
/* .line 246 */
final String v7 = "BOOT_COMPLETED updateLocalDataBootCompleted use cloudData"; // const-string v7, "BOOT_COMPLETED updateLocalDataBootCompleted use cloudData"
android.util.Slog .v ( v6,v7 );
/* .line 247 */
/* iput-boolean v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 248 */
} // .end local v4 # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
/* .line 250 */
} // :cond_2
/* iget-wide v7, v4, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->version:J */
/* cmp-long v4, v7, v2 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* const-wide/16 v7, -0x1 */
/* cmp-long v4, v2, v7 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 251 */
/* new-instance v4, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* invoke-direct {v4, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V */
/* .line 252 */
/* .restart local v4 # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
this.mDownscaleCloudData = v4;
/* .line 253 */
final String v7 = "BOOT_COMPLETED updateLocalDataBootCompleted use cloudData different version"; // const-string v7, "BOOT_COMPLETED updateLocalDataBootCompleted use cloudData different version"
android.util.Slog .v ( v6,v7 );
/* .line 254 */
/* iput-boolean v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 257 */
} // .end local v4 # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
} // :cond_3
} // :goto_0
return;
/* .line 234 */
} // .end local v2 # "version":J
} // :cond_4
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void addSizeCompatApps ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 307 */
/* sget-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z */
/* if-nez v0, :cond_0 */
/* .line 308 */
return;
/* .line 310 */
} // :cond_0
v0 = this.mDownscaleCloudData;
v0 = com.android.server.app.GmsUtil .checkValidApp ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 311 */
v0 = this.mSizeCompatApps;
(( java.util.HashSet ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 313 */
} // :cond_1
return;
} // .end method
public void downscaleForSwitchResolution ( ) {
/* .locals 1 */
/* .line 292 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).toggleDownscale ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V
/* .line 293 */
return;
} // .end method
public void downscaleWithPackageNameAndRatio ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "ratio" # Ljava/lang/String; */
/* .line 298 */
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 608 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 609 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "WindowDownscale Service"; // const-string v1, "WindowDownscale Service"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 610 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "IS_TABLET = "; // const-string v3, "IS_TABLET = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 611 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "WQHD = "; // const-string v3, "WQHD = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
v3 = com.android.server.app.GmsUtil .isWQHD ( v3 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 613 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "IntelligentPowerSavingEnable = "; // const-string v3, "IntelligentPowerSavingEnable = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 615 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "PowerSaving = "; // const-string v3, "PowerSaving = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 617 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ExtremeModeEnable = "; // const-string v3, "ExtremeModeEnable = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 619 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "FromCloud = "; // const-string v3, "FromCloud = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 620 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "CloudData = "; // const-string v3, "CloudData = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.cloudDataStr;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 621 */
final String v1 = "--- Apps: begin ---"; // const-string v1, "--- Apps: begin ---"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 622 */
v1 = this.mAppStates;
(( java.util.HashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 623 */
/* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;" */
/* check-cast v4, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem; */
(( com.android.server.app.GameManagerServiceStubImpl$AppItem ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 624 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
/* .line 625 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 626 */
final String v1 = "--- Apps: end"; // const-string v1, "--- Apps: end"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 627 */
final String v1 = "--- Cmd app begin ---"; // const-string v1, "--- Cmd app begin ---"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 628 */
v1 = this.mShellCmdDownscalePackageNames;
(( java.util.HashSet ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 629 */
/* .local v3, "item":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "{packageName = " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "} " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 630 */
} // .end local v3 # "item":Ljava/lang/String;
/* .line 631 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 632 */
final String v1 = "--- Cmd end"; // const-string v1, "--- Cmd end"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 633 */
final String v1 = "--- trace ---"; // const-string v1, "--- trace ---"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 634 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 636 */
v1 = this.limitLogger;
(( com.android.server.app.GmsLimitLogger ) v1 ).dump ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/app/GmsLimitLogger;->dump(Ljava/io/PrintWriter;)V
/* .line 637 */
return;
} // .end method
public void dynamicRefresh ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "ratio" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 303 */
return;
} // .end method
public void handleAppDied ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 570 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 571 */
final String v0 = "com.tencent.mm"; // const-string v0, "com.tencent.mm"
v0 = android.text.TextUtils .equals ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 572 */
final String v0 = "GameManagerServiceStub"; // const-string v0, "GameManagerServiceStub"
final String v1 = "com.tencent.mm handleAppDied"; // const-string v1, "com.tencent.mm handleAppDied"
android.util.Slog .v ( v0,v1 );
/* .line 573 */
return;
/* .line 575 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z */
/* if-nez v0, :cond_1 */
v0 = this.mDownscaleCloudData;
/* iget v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I */
v0 = com.android.server.app.GmsUtil .needDownscale ( p1,v0,v1 );
/* if-nez v0, :cond_1 */
/* .line 576 */
return;
/* .line 578 */
} // :cond_1
v0 = this.mHandler;
/* const v1, 0x1b207 */
(( com.android.server.app.GameManagerServiceStubImpl$InnerHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 579 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 580 */
v1 = this.mHandler;
(( com.android.server.app.GameManagerServiceStubImpl$InnerHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 582 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_2
return;
} // .end method
public void registerDownscaleObserver ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "mContext" # Landroid/content/Context; */
/* .line 145 */
final String v0 = "GameManagerServiceStub"; // const-string v0, "GameManagerServiceStub"
final String v1 = "downscale register"; // const-string v1, "downscale register"
android.util.Slog .d ( v0,v1 );
/* .line 146 */
this.mContext = p1;
/* .line 147 */
v0 = this.mHandler;
/* if-nez v0, :cond_1 */
/* .line 148 */
v0 = this.mHandlerThread;
v0 = (( com.android.server.ServiceThread ) v0 ).isAlive ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->isAlive()Z
/* if-nez v0, :cond_0 */
/* .line 149 */
v0 = this.mHandlerThread;
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 151 */
} // :cond_0
/* new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler; */
v1 = this.mHandlerThread;
(( com.android.server.ServiceThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 153 */
} // :cond_1
/* new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Handler;)V */
this.mObserver = v0;
/* .line 155 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWindowManager = v0;
/* .line 156 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "POWER_SAVE_MODE_OPEN"; // const-string v1, "POWER_SAVE_MODE_OPEN"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_2 */
/* move v0, v3 */
} // :cond_2
/* move v0, v2 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z */
/* .line 158 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "persist.sys.downscale_power_save"; // const-string v4, "persist.sys.downscale_power_save"
v0 = android.provider.Settings$Global .getInt ( v0,v4,v2 );
/* if-ne v0, v3, :cond_3 */
/* move v0, v3 */
} // :cond_3
/* move v0, v2 */
} // :goto_1
/* iput-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z */
/* .line 162 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* xor-int/2addr v0, v3 */
/* .line 163 */
/* .local v0, "defValue":I */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "miui_screen_compat"; // const-string v5, "miui_screen_compat"
v4 = android.provider.Settings$System .getInt ( v4,v5,v0 );
/* if-ne v4, v3, :cond_4 */
/* move v4, v3 */
} // :cond_4
/* move v4, v2 */
} // :goto_2
/* iput-boolean v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z */
/* .line 166 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "persist.sys.miui_downscale"; // const-string v6, "persist.sys.miui_downscale"
android.provider.Settings$Global .getString ( v4,v6 );
/* .line 167 */
/* .local v4, "proEnable":Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v7, :cond_5 */
final String v7 = "disable"; // const-string v7, "disable"
v7 = android.text.TextUtils .equals ( v7,v4 );
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 168 */
this.mProEnable = v4;
/* .line 171 */
} // :cond_5
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 172 */
android.provider.Settings$Global .getUriFor ( v6 );
v8 = this.mObserver;
/* .line 171 */
(( android.content.ContentResolver ) v7 ).registerContentObserver ( v6, v2, v8 ); // invoke-virtual {v7, v6, v2, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 173 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 174 */
android.provider.Settings$System .getUriFor ( v1 );
v7 = this.mObserver;
/* .line 173 */
(( android.content.ContentResolver ) v6 ).registerContentObserver ( v1, v3, v7 ); // invoke-virtual {v6, v1, v3, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 175 */
/* new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver; */
/* invoke-direct {v1, p0}, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;)V */
/* new-instance v3, Landroid/content/IntentFilter; */
final String v6 = "android.intent.action.BOOT_COMPLETED"; // const-string v6, "android.intent.action.BOOT_COMPLETED"
/* invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
int v6 = 0; // const/4 v6, 0x0
(( android.content.Context ) p1 ).registerReceiverForAllUsers ( v1, v3, v6, v6 ); // invoke-virtual {p1, v1, v3, v6, v6}, Landroid/content/Context;->registerReceiverForAllUsers(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 177 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 178 */
final String v3 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v3, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
android.net.Uri .parse ( v3 );
v6 = this.mObserver;
/* .line 177 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v3, v2, v6 ); // invoke-virtual {v1, v3, v2, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 179 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 180 */
android.provider.Settings$System .getUriFor ( v5 );
v5 = this.mObserver;
/* .line 179 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v3, v2, v5 ); // invoke-virtual {v1, v3, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 181 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 182 */
final String v3 = "pc_security_center_extreme_mode_enter"; // const-string v3, "pc_security_center_extreme_mode_enter"
android.provider.Settings$Secure .getUriFor ( v3 );
v5 = this.mObserver;
/* .line 181 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v3, v2, v5 ); // invoke-virtual {v1, v3, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 184 */
v1 = this.mDownscaleCloudData;
/* if-nez v1, :cond_6 */
/* .line 185 */
/* invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->initLocalDownscaleAppList()Lorg/json/JSONObject; */
/* .line 186 */
/* .local v1, "jsonAll":Lorg/json/JSONObject; */
/* new-instance v3, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData; */
/* invoke-direct {v3, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V */
this.mDownscaleCloudData = v3;
/* .line 187 */
/* iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z */
/* .line 189 */
} // .end local v1 # "jsonAll":Lorg/json/JSONObject;
} // :cond_6
return;
} // .end method
public void setActivityManagerService ( com.android.server.am.ActivityManagerService p0 ) {
/* .locals 2 */
/* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
/* .line 586 */
this.mService = p1;
/* .line 587 */
v0 = this.mHandler;
/* if-nez v0, :cond_1 */
/* .line 588 */
v0 = this.mHandlerThread;
v0 = (( com.android.server.ServiceThread ) v0 ).isAlive ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->isAlive()Z
/* if-nez v0, :cond_0 */
/* .line 589 */
v0 = this.mHandlerThread;
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 591 */
} // :cond_0
/* new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler; */
v1 = this.mHandlerThread;
(( com.android.server.ServiceThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 593 */
} // :cond_1
return;
} // .end method
public void shellCmd ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "ratio" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 328 */
(( com.android.server.app.GameManagerServiceStubImpl ) p0 ).getCompatChangeId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl;->getCompatChangeId(Ljava/lang/String;)J
/* move-result-wide v0 */
/* .line 330 */
/* .local v0, "changeId":J */
/* new-instance v2, Landroid/util/ArraySet; */
/* invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V */
/* .line 332 */
/* .local v2, "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
/* const-wide/16 v3, 0x0 */
/* cmp-long v3, v0, v3 */
/* if-nez v3, :cond_0 */
/* .line 333 */
v3 = this.DOWNSCALE_CHANGE_IDS;
/* .local v3, "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
/* .line 335 */
} // .end local v3 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
} // :cond_0
/* const-wide/32 v3, 0xa09e1d7 */
java.lang.Long .valueOf ( v3,v4 );
/* .line 336 */
java.lang.Long .valueOf ( v0,v1 );
/* .line 337 */
v3 = this.DOWNSCALE_CHANGE_IDS;
(( android.util.ArraySet ) v3 ).stream ( ); // invoke-virtual {v3}, Landroid/util/ArraySet;->stream()Ljava/util/stream/Stream;
/* new-instance v4, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v4, v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda0;-><init>(J)V */
/* .line 338 */
/* .line 339 */
java.util.stream.Collectors .toSet ( );
/* check-cast v3, Ljava/util/Set; */
/* .line 341 */
/* .restart local v3 # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;" */
} // :goto_0
final String v4 = "disable"; // const-string v4, "disable"
v4 = android.text.TextUtils .equals ( p1,v4 );
final String v5 = "GameManagerServiceStub"; // const-string v5, "GameManagerServiceStub"
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 342 */
v4 = this.mShellCmdDownscalePackageNames;
v4 = (( java.util.HashSet ) v4 ).contains ( p2 ); // invoke-virtual {v4, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 343 */
v4 = this.mShellCmdDownscalePackageNames;
(( java.util.HashSet ) v4 ).remove ( p2 ); // invoke-virtual {v4, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 345 */
} // :cond_1
v4 = this.mShellCmdDownscalePackageNames;
v4 = (( java.util.HashSet ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/HashSet;->size()I
/* const/16 v6, 0x32 */
/* if-gt v4, v6, :cond_3 */
/* .line 346 */
v4 = this.mShellCmdDownscalePackageNames;
(( java.util.HashSet ) v4 ).add ( p2 ); // invoke-virtual {v4, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 351 */
} // :cond_2
} // :goto_1
/* nop */
/* .line 352 */
final String v4 = "platform_compat"; // const-string v4, "platform_compat"
android.os.ServiceManager .getService ( v4 );
/* check-cast v4, Lcom/android/server/compat/PlatformCompat; */
/* .line 353 */
/* .local v4, "platformCompat":Lcom/android/server/compat/PlatformCompat; */
/* new-instance v6, Lcom/android/internal/compat/CompatibilityChangeConfig; */
/* new-instance v7, Landroid/compat/Compatibility$ChangeConfig; */
/* invoke-direct {v7, v2, v3}, Landroid/compat/Compatibility$ChangeConfig;-><init>(Ljava/util/Set;Ljava/util/Set;)V */
/* invoke-direct {v6, v7}, Lcom/android/internal/compat/CompatibilityChangeConfig;-><init>(Landroid/compat/Compatibility$ChangeConfig;)V */
/* .line 356 */
/* .local v6, "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig; */
(( com.android.server.compat.PlatformCompat ) v4 ).setOverrides ( v6, p2 ); // invoke-virtual {v4, v6, p2}, Lcom/android/server/compat/PlatformCompat;->setOverrides(Lcom/android/internal/compat/CompatibilityChangeConfig;Ljava/lang/String;)V
/* .line 357 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "shellCmd--- " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p2 ); // invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ","; // const-string v8, ","
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 358 */
/* .local v7, "logStr":Ljava/lang/String; */
android.util.Slog .v ( v5,v7 );
/* .line 359 */
v5 = this.limitLogger;
/* new-instance v8, Lcom/android/server/app/GmsLimitLogger$StringEvent; */
/* invoke-direct {v8, v7}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V */
(( com.android.server.app.GmsLimitLogger ) v5 ).log ( v8 ); // invoke-virtual {v5, v8}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V
/* .line 360 */
return;
/* .line 348 */
} // .end local v4 # "platformCompat":Lcom/android/server/compat/PlatformCompat;
} // .end local v6 # "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
} // .end local v7 # "logStr":Ljava/lang/String;
} // :cond_3
final String v4 = "commands enable no more than 50"; // const-string v4, "commands enable no more than 50"
android.util.Slog .v ( v5,v4 );
/* .line 349 */
return;
} // .end method
public void toggleDownscale ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageNameParam" # Ljava/lang/String; */
/* .line 284 */
v0 = this.mDownscaleCloudData;
v0 = com.android.server.app.GmsUtil .checkValidApp ( p1,v0 );
/* if-nez v0, :cond_0 */
/* .line 285 */
return;
/* .line 287 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V */
/* .line 288 */
return;
} // .end method
public void toggleDownscaleForStopedApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 317 */
/* sget-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z */
/* if-nez v0, :cond_0 */
/* .line 318 */
return;
/* .line 320 */
} // :cond_0
v0 = this.mDownscaleCloudData;
v0 = com.android.server.app.GmsUtil .checkValidApp ( p1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl;->runningApp(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 323 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V */
/* .line 324 */
return;
/* .line 321 */
} // :cond_2
} // :goto_0
return;
} // .end method
