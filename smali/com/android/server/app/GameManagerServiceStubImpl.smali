.class public Lcom/android/server/app/GameManagerServiceStubImpl;
.super Lcom/android/server/app/GameManagerServiceStub;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.app.GameManagerServiceStub$$"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;,
        Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;,
        Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;,
        Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;,
        Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;,
        Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;,
        Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
    }
.end annotation


# static fields
.field private static final CLOUD_ALL_DATA_CHANGE_URI:Ljava/lang/String; = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

.field private static final DEBUG_DOWNSCALE_POWER_SAVE:Ljava/lang/String; = "persist.sys.downscale_power_save"

.field private static final DEBUG_PROP_KEY:Ljava/lang/String; = "persist.sys.miui_downscale"

.field private static final DOWNSCALE_APP_SETTINGS_FILE_PATH:Ljava/lang/String; = "system/etc/DownscaleAppSettings.json"

.field private static final DOWNSCALE_DISABLE:Ljava/lang/String; = "disable"

.field private static final DOWNSCALE_ENABLE:Ljava/lang/String; = "enable"

.field private static final FOLD_DEVICE:Z

.field private static final KEY_POWER_MODE_OPEN:Ljava/lang/String; = "POWER_SAVE_MODE_OPEN"

.field public static final MIUI_PAD_SCREEN_COMPAT_VERSION:I = 0x1

.field public static final MIUI_RESOLUTION:Ljava/lang/String; = "persist.sys.miui_resolution"

.field private static final MIUI_SCREEN_COMPAT:Ljava/lang/String; = "miui_screen_compat"

.field public static final MIUI_SCREEN_COMPAT_VERSION:I = 0x3

.field static final MIUI_SCREEN_COMPAT_WIDTH:I = 0x438

.field private static final MODULE_NAME_DOWNSCALE:Ljava/lang/String; = "tdownscale"

.field public static final MSG_CLOUD_DATA_CHANGE:I = 0x1b20a

.field public static final MSG_DOWNSCALE_APP_DIED:I = 0x1b207

.field public static final MSG_EXTREME_MODE:I = 0x1b20c

.field public static final MSG_INTELLIGENT_POWERSAVE:I = 0x1b20b

.field public static final MSG_PROCESS_POWERSAVE_ACTION:I = 0x1b208

.field public static final MSG_REBOOT_COMPLETED_ACTION:I = 0x1b209

.field private static final PC_SECURITY_CENTER_EXTREME_MODE:Ljava/lang/String; = "pc_security_center_extreme_mode_enter"

.field public static final SCALE_MIN:F = 0.6f

.field static final TAG:Ljava/lang/String; = "GameManagerServiceStub"


# instance fields
.field private cloudDataStr:Ljava/lang/String;

.field private currentWidth:I

.field private fromCloud:Z

.field private intelligentPowerSavingEnable:Z

.field private isDebugPowerSave:Z

.field private limitLogger:Lcom/android/server/app/GmsLimitLogger;

.field private mAppStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDownscaleApps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

.field private mExtremeModeEnable:Z

.field private mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

.field public final mHandlerThread:Lcom/android/server/ServiceThread;

.field private mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

.field private volatile mPowerSaving:Z

.field private mProEnable:Ljava/lang/String;

.field private mService:Lcom/android/server/am/ActivityManagerService;

.field private mShellCmdDownscalePackageNames:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSizeCompatApps:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mWindowManager:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetintelligentPowerSavingEnable(Lcom/android/server/app/GameManagerServiceStubImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/app/GameManagerServiceStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/app/GameManagerServiceStubImpl;)Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerSaving(Lcom/android/server/app/GameManagerServiceStubImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmProEnable(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShellCmdDownscalePackageNames(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSizeCompatApps(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mSizeCompatApps:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputintelligentPowerSavingEnable(Lcom/android/server/app/GameManagerServiceStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmExtremeModeEnable(Lcom/android/server/app/GameManagerServiceStubImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmProEnable(Lcom/android/server/app/GameManagerServiceStubImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mexecuteDownscale(Lcom/android/server/app/GameManagerServiceStubImpl;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessBootCompletedAction(Lcom/android/server/app/GameManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processBootCompletedAction()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessCloudData(Lcom/android/server/app/GameManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processCloudData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessIntelligentPowerSave(Lcom/android/server/app/GameManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processIntelligentPowerSave()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprocessPowerSaveAction(Lcom/android/server/app/GameManagerServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->processPowerSaveAction()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetFOLD_DEVICE()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 82
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFoldDeviceInside()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 67
    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStub;-><init>()V

    .line 93
    new-instance v0, Lcom/android/server/ServiceThread;

    const-string v1, "GameManagerServiceStub"

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleApps:Ljava/util/HashMap;

    .line 114
    const-string v0, "enable"

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    .line 115
    iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z

    .line 125
    iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    .line 127
    const/16 v0, 0x438

    iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    .line 128
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mSizeCompatApps:Ljava/util/HashSet;

    .line 132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    .line 134
    iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z

    .line 137
    iput-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 138
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->cloudDataStr:Ljava/lang/String;

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mAppStates:Ljava/util/HashMap;

    .line 140
    new-instance v0, Lcom/android/server/app/GmsLimitLogger;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/android/server/app/GmsLimitLogger;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->limitLogger:Lcom/android/server/app/GmsLimitLogger;

    return-void
.end method

.method private appsDownscale(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "packageNameParam"    # Ljava/lang/String;
    .param p2, "ignoreRunningApps"    # Z

    .line 376
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-object v0, v0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->apps:Ljava/util/HashMap;

    .line 377
    .local v0, "apps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
    const-string v1, "persist.sys.miui_resolution"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "currentResolution":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    .line 379
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 380
    .local v2, "resolutionArr":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v4, v2

    if-lez v4, :cond_0

    .line 382
    :try_start_0
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    goto :goto_0

    .line 383
    :catch_0
    move-exception v3

    .line 386
    .end local v2    # "resolutionArr":[Ljava/lang/String;
    :cond_0
    :goto_0
    goto :goto_1

    .line 387
    :cond_1
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 388
    .local v2, "point":Landroid/graphics/Point;
    iget-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v4, v3, v2}, Lcom/android/server/wm/WindowManagerService;->getBaseDisplaySize(ILandroid/graphics/Point;)V

    .line 389
    iget v3, v2, Landroid/graphics/Point;->x:I

    .line 390
    .local v3, "baseDisplayWidth":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "baseDisplayWidth = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "GameManagerServiceStub"

    invoke-static {v5, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    if-lez v3, :cond_2

    .line 392
    iput v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    .line 396
    .end local v2    # "point":Landroid/graphics/Point;
    .end local v3    # "baseDisplayWidth":I
    :cond_2
    :goto_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 397
    if-eqz v0, :cond_5

    .line 398
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 400
    .local v3, "appItemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;

    invoke-direct {p0, v4, v5, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->downscaleApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;Z)V

    .line 401
    .end local v3    # "appItemEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
    goto :goto_2

    :cond_3
    goto :goto_3

    .line 405
    :cond_4
    if-eqz v0, :cond_5

    .line 406
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;

    .line 407
    .local v2, "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    if-eqz v2, :cond_5

    .line 408
    iget-object v3, v2, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->packageName:Ljava/lang/String;

    invoke-direct {p0, v3, v2, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->downscaleApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;Z)V

    .line 412
    .end local v2    # "app":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    :cond_5
    :goto_3
    return-void
.end method

.method private downscaleApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;Z)V
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appItem"    # Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    .param p3, "ignoreRunningApps"    # Z

    .line 473
    iget-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    invoke-static {p1, v0, v1}, Lcom/android/server/app/GmsUtil;->needDownscale(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    return-void

    .line 476
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    if-eqz p2, :cond_a

    .line 478
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    const-string v1, "enable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "GameManagerServiceStub"

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl;->runningApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is running do not downscale"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return-void

    .line 482
    :cond_1
    const-string v0, "disable"

    .line 483
    .local v0, "ratioResult":Ljava/lang/String;
    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v3, :cond_3

    .line 484
    invoke-direct {p0, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->getTargetWidth(Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;)I

    move-result v1

    .line 485
    .local v1, "targetWidth":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_2

    .line 486
    iget v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    invoke-static {v1, v3}, Lcom/android/server/app/GmsUtil;->calcuRatio(II)Ljava/lang/String;

    move-result-object v0

    .line 488
    .end local v1    # "targetWidth":I
    :cond_2
    goto :goto_0

    .line 489
    :cond_3
    iget-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-boolean v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z

    if-eqz v1, :cond_4

    .line 490
    iget-boolean v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    iget-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-boolean v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z

    invoke-static {p2, v1, v3, v4}, Lcom/android/server/app/GmsUtil;->getTargetRatioForPad(Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;ZLcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;Z)Ljava/lang/String;

    move-result-object v0

    .line 495
    :cond_4
    :goto_0
    sget-boolean v1, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z

    const/4 v3, 0x0

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getAspectRatio(Ljava/lang/String;)F

    move-result v1

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_5

    .line 496
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isEmbeddingEnabledForPackage(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 497
    :cond_5
    const-string v0, "disable"

    .line 500
    :cond_6
    invoke-static {}, Lcom/android/server/wm/WindowManagerServiceStub;->get()Lcom/android/server/wm/WindowManagerServiceStub;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {v1, p1, v4}, Lcom/android/server/wm/WindowManagerServiceStub;->getCompatScale(Ljava/lang/String;I)F

    move-result v1

    .line 501
    .local v1, "currentScale":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "downscale currentScale = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    cmpl-float v3, v1, v3

    const-string v4, ","

    if-eqz v3, :cond_8

    .line 503
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v1, v3, v1

    .line 504
    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 505
    const-string v5, "disable"

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    cmpl-float v3, v1, v3

    if-nez v3, :cond_8

    .line 506
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "downscale "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " not change !!"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iput-object v0, p2, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->ratio:Ljava/lang/String;

    .line 508
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mAppStates:Ljava/util/HashMap;

    invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->limitLogger:Lcom/android/server/app/GmsLimitLogger;

    new-instance v3, Lcom/android/server/app/GmsLimitLogger$StringEvent;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V

    .line 510
    return-void

    .line 514
    :cond_8
    invoke-virtual {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->getCompatChangeId(Ljava/lang/String;)J

    move-result-wide v5

    .line 516
    .local v5, "changeId":J
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 518
    .local v3, "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-nez v7, :cond_9

    .line 519
    iget-object v7, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    .local v7, "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    goto :goto_1

    .line 521
    .end local v7    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_9
    const-wide/32 v7, 0xa09e1d7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 522
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 523
    iget-object v7, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    invoke-virtual {v7}, Landroid/util/ArraySet;->stream()Ljava/util/stream/Stream;

    move-result-object v7

    new-instance v8, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v8, v5, v6}, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda1;-><init>(J)V

    .line 524
    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v7

    .line 525
    invoke-static {}, Ljava/util/stream/Collectors;->toSet()Ljava/util/stream/Collector;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Set;

    .line 528
    .restart local v7    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :goto_1
    nop

    .line 529
    const-string v8, "platform_compat"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    check-cast v8, Lcom/android/server/compat/PlatformCompat;

    .line 530
    .local v8, "platformCompat":Lcom/android/server/compat/PlatformCompat;
    new-instance v9, Lcom/android/internal/compat/CompatibilityChangeConfig;

    new-instance v10, Landroid/compat/Compatibility$ChangeConfig;

    invoke-direct {v10, v3, v7}, Landroid/compat/Compatibility$ChangeConfig;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    invoke-direct {v9, v10}, Lcom/android/internal/compat/CompatibilityChangeConfig;-><init>(Landroid/compat/Compatibility$ChangeConfig;)V

    .line 534
    .local v9, "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
    invoke-virtual {v8, v9, p1}, Lcom/android/server/compat/PlatformCompat;->setOverridesForDownscale(Lcom/android/internal/compat/CompatibilityChangeConfig;Ljava/lang/String;)V

    .line 535
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "downscale enable packageName = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ratio = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iput-object v0, p2, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->ratio:Ljava/lang/String;

    .line 537
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mAppStates:Ljava/util/HashMap;

    invoke-virtual {v2, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->limitLogger:Lcom/android/server/app/GmsLimitLogger;

    new-instance v10, Lcom/android/server/app/GmsLimitLogger$StringEvent;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v4}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V

    .line 540
    .end local v0    # "ratioResult":Ljava/lang/String;
    .end local v1    # "currentScale":F
    .end local v3    # "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v5    # "changeId":J
    .end local v7    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v8    # "platformCompat":Lcom/android/server/compat/PlatformCompat;
    .end local v9    # "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
    :cond_a
    return-void
.end method

.method private executeDownscale(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "packageNameParam"    # Ljava/lang/String;
    .param p2, "ignoreRunningApps"    # Z

    .line 363
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    if-eqz v0, :cond_0

    .line 364
    invoke-direct {p0, p1, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->appsDownscale(Ljava/lang/String;Z)V

    .line 365
    return-void

    .line 367
    :cond_0
    if-nez v0, :cond_1

    .line 368
    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->initLocalDownscaleAppList()Lorg/json/JSONObject;

    move-result-object v0

    .line 369
    .local v0, "jsonAll":Lorg/json/JSONObject;
    new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-direct {v1, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V

    iput-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 370
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 372
    .end local v0    # "jsonAll":Lorg/json/JSONObject;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/android/server/app/GameManagerServiceStubImpl;->appsDownscale(Ljava/lang/String;Z)V

    .line 373
    return-void
.end method

.method private getTargetWidth(Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;)I
    .locals 14
    .param p1, "appItem"    # Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;

    .line 415
    const-string v0, "enable"

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-boolean v0, v0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z

    if-nez v0, :cond_0

    goto/16 :goto_8

    .line 418
    :cond_0
    iget v0, p1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->mode:I

    .line 419
    .local v0, "mode":I
    if-nez v0, :cond_1

    .line 420
    return v1

    .line 422
    :cond_1
    iget v2, p1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->systemVersion:I

    .line 423
    .local v2, "systemVersion":I
    if-nez v2, :cond_2

    .line 424
    return v1

    .line 426
    :cond_2
    iget v3, p1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->appVersion:I

    .line 427
    .local v3, "appVersion":I
    const/4 v4, 0x3

    if-le v3, v4, :cond_3

    .line 428
    return v1

    .line 431
    :cond_3
    and-int/lit8 v4, v0, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v4, :cond_4

    move v4, v6

    goto :goto_0

    :cond_4
    move v4, v5

    .line 432
    .local v4, "normalEnable":Z
    :goto_0
    and-int/lit8 v7, v0, 0x2

    if-eqz v7, :cond_5

    move v7, v6

    goto :goto_1

    :cond_5
    move v7, v5

    .line 433
    .local v7, "saveBatteryEnable":Z
    :goto_1
    iget-boolean v8, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-object v8, v8, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->devices:Ljava/util/HashSet;

    invoke-static {v8}, Lcom/android/server/app/GmsUtil;->isContainDevice(Ljava/util/HashSet;)Z

    move-result v8

    if-eqz v8, :cond_6

    goto :goto_2

    :cond_6
    move v8, v5

    goto :goto_3

    :cond_7
    :goto_2
    move v8, v6

    .line 434
    .local v8, "containDevice":Z
    :goto_3
    and-int/lit8 v9, v2, 0x1

    if-eqz v9, :cond_8

    move v9, v6

    goto :goto_4

    :cond_8
    move v9, v5

    .line 435
    .local v9, "preVersionEnable":Z
    :goto_4
    and-int/lit8 v10, v2, 0x2

    if-eqz v10, :cond_9

    move v10, v6

    goto :goto_5

    :cond_9
    move v10, v5

    .line 436
    .local v10, "devVersionEnable":Z
    :goto_5
    and-int/lit8 v11, v2, 0x4

    if-eqz v11, :cond_a

    goto :goto_6

    :cond_a
    move v6, v5

    .line 437
    .local v6, "stableVersionEnable":Z
    :goto_6
    if-eqz v9, :cond_b

    sget-boolean v11, Lmiui/os/Build;->IS_PRE_VERSION:Z

    if-nez v11, :cond_d

    :cond_b
    if-eqz v10, :cond_c

    sget-boolean v11, Lmiui/os/Build;->IS_DEV_VERSION:Z

    if-nez v11, :cond_d

    :cond_c
    if-eqz v6, :cond_14

    sget-boolean v11, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-nez v11, :cond_d

    goto :goto_7

    .line 440
    :cond_d
    iget-boolean v11, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z

    if-eqz v11, :cond_e

    .line 441
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-object v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->saveBattery:I

    return v1

    .line 444
    :cond_e
    iget-boolean v11, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    if-eqz v11, :cond_10

    if-eqz v7, :cond_10

    if-eqz v8, :cond_10

    .line 445
    iget v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    const/16 v5, 0x2d0

    if-ne v1, v5, :cond_f

    .line 446
    const/16 v1, 0x21c

    return v1

    .line 449
    :cond_f
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-object v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->saveBattery:I

    return v1

    .line 452
    :cond_10
    const-string/jumbo v11, "screen_resolution_supported"

    invoke-static {v11}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v11

    .line 453
    .local v11, "resolutionArray":[I
    const-string v12, "screen_compat_supported"

    invoke-static {v12, v5}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 455
    .local v5, "screenCompatSupported":Z
    sget-boolean v12, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v12, :cond_12

    iget-boolean v12, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    if-eqz v12, :cond_12

    if-eqz v4, :cond_12

    iget v12, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    const/16 v13, 0x438

    if-le v12, v13, :cond_12

    if-nez v11, :cond_11

    if-eqz v5, :cond_12

    .line 459
    :cond_11
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-object v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->normal:I

    return v1

    .line 460
    :cond_12
    sget-boolean v12, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v12, :cond_13

    iget-boolean v12, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    if-eqz v12, :cond_13

    if-eqz v4, :cond_13

    .line 462
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget-object v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    iget v1, v1, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;->normal:I

    return v1

    .line 464
    :cond_13
    return v1

    .line 438
    .end local v5    # "screenCompatSupported":Z
    .end local v11    # "resolutionArray":[I
    :cond_14
    :goto_7
    return v1

    .line 416
    .end local v0    # "mode":I
    .end local v2    # "systemVersion":I
    .end local v3    # "appVersion":I
    .end local v4    # "normalEnable":Z
    .end local v6    # "stableVersionEnable":Z
    .end local v7    # "saveBatteryEnable":Z
    .end local v8    # "containDevice":Z
    .end local v9    # "preVersionEnable":Z
    .end local v10    # "devVersionEnable":Z
    :cond_15
    :goto_8
    return v1
.end method

.method private initLocalDownscaleAppList()Lorg/json/JSONObject;
    .locals 5

    .line 543
    const-string v0, ""

    .line 545
    .local v0, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    const-string/jumbo v2, "system/etc/DownscaleAppSettings.json"

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 546
    .local v1, "f":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 548
    .local v2, "bis":Ljava/io/BufferedReader;
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "line":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 549
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v3

    goto :goto_0

    .line 551
    .end local v4    # "line":Ljava/lang/String;
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 553
    .end local v2    # "bis":Ljava/io/BufferedReader;
    goto :goto_2

    .line 546
    .restart local v2    # "bis":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_5
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "jsonString":Ljava/lang/String;
    .end local v1    # "f":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/android/server/app/GameManagerServiceStubImpl;
    :goto_1
    throw v3
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 545
    .end local v2    # "bis":Ljava/io/BufferedReader;
    .restart local v0    # "jsonString":Ljava/lang/String;
    .restart local v1    # "f":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/android/server/app/GameManagerServiceStubImpl;
    :catchall_2
    move-exception v2

    goto :goto_3

    .line 551
    :catch_0
    move-exception v2

    .line 552
    .local v2, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 554
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 557
    .end local v1    # "f":Ljava/io/FileInputStream;
    goto :goto_5

    .line 545
    .restart local v1    # "f":Ljava/io/FileInputStream;
    :goto_3
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v3

    :try_start_9
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "jsonString":Ljava/lang/String;
    .end local p0    # "this":Lcom/android/server/app/GameManagerServiceStubImpl;
    :goto_4
    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    .line 554
    .end local v1    # "f":Ljava/io/FileInputStream;
    .restart local v0    # "jsonString":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/app/GameManagerServiceStubImpl;
    :catch_1
    move-exception v1

    .line 556
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "GameManagerServiceStub"

    const-string/jumbo v3, "system/etc/DownscaleAppSettings.json not find"

    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_5
    :try_start_a
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    .line 561
    .local v1, "appConfigJSON":Lorg/json/JSONObject;
    return-object v1

    .line 562
    .end local v1    # "appConfigJSON":Lorg/json/JSONObject;
    :catch_2
    move-exception v1

    .line 565
    const/4 v1, 0x0

    return-object v1
.end method

.method static synthetic lambda$downscaleApp$1(JLjava/lang/Long;)Z
    .locals 4
    .param p0, "changeId"    # J
    .param p2, "it"    # Ljava/lang/Long;

    .line 524
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0xa09e1d7

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$shellCmd$0(JLjava/lang/Long;)Z
    .locals 4
    .param p0, "changeId"    # J
    .param p2, "it"    # Ljava/lang/Long;

    .line 338
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0xa09e1d7

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private processBootCompletedAction()V
    .locals 5

    .line 261
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "POWER_SAVE_MODE_OPEN"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 262
    .local v0, "powerModeOpen":Z
    :goto_0
    iget-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "pc_security_center_extreme_mode_enter"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z

    .line 263
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_BOOT_COMPLETED --- powerModeOpen= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mPowerSaving = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GameManagerServiceStub"

    invoke-static {v3, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->updateLocalDataBootCompleted()V

    .line 266
    if-nez v0, :cond_2

    iget-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z

    if-eqz v2, :cond_3

    .line 267
    :cond_2
    iput-boolean v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    .line 269
    :cond_3
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->limitLogger:Lcom/android/server/app/GmsLimitLogger;

    new-instance v2, Lcom/android/server/app/GmsLimitLogger$StringEvent;

    const-string v3, "---rebootCompleted---"

    invoke-direct {v2, v3}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V

    .line 270
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 271
    return-void
.end method

.method private processCloudData()V
    .locals 9

    .line 193
    const/4 v0, 0x0

    .line 194
    .local v0, "jsonAll":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 195
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 194
    const-string/jumbo v2, "tdownscale"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v1

    .line 198
    .local v1, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 201
    :cond_0
    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->cloudDataStr:Ljava/lang/String;

    .line 204
    const-wide/16 v4, -0x1

    .line 205
    .local v4, "version":J
    if-eqz v0, :cond_1

    .line 206
    const-string/jumbo v2, "version"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 208
    :cond_1
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    const/4 v6, 0x1

    if-nez v2, :cond_2

    .line 209
    new-instance v2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-direct {v2, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V

    .line 210
    .local v2, "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    iput-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 211
    invoke-virtual {p0, v3}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 212
    iput-boolean v6, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 213
    .end local v2    # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    goto :goto_0

    .line 215
    :cond_2
    iget-wide v7, v2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->version:J

    cmp-long v2, v7, v4

    if-eqz v2, :cond_3

    const-wide/16 v7, -0x1

    cmp-long v2, v4, v7

    if-eqz v2, :cond_3

    .line 216
    new-instance v2, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-direct {v2, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V

    .line 217
    .restart local v2    # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    iput-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 218
    invoke-virtual {p0, v3}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 219
    iput-boolean v6, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 222
    .end local v2    # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    :cond_3
    :goto_0
    return-void

    .line 199
    .end local v4    # "version":J
    :cond_4
    :goto_1
    return-void
.end method

.method private processIntelligentPowerSave()V
    .locals 2

    .line 681
    const-string v0, "disable"

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    return-void

    .line 684
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V

    .line 685
    return-void
.end method

.method private processPowerSaveAction()V
    .locals 3

    .line 275
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "POWER_SAVE_MODE_OPEN"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    .line 276
    const-string v0, "disable"

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    return-void

    .line 279
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method private runningApp(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 596
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    if-nez v0, :cond_0

    goto :goto_1

    .line 597
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 598
    .local v0, "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 599
    .local v3, "runningAppProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 600
    const/4 v1, 0x1

    return v1

    .line 602
    .end local v3    # "runningAppProcess":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    goto :goto_0

    .line 603
    :cond_2
    return v1

    .line 596
    .end local v0    # "runningAppProcesses":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_3
    :goto_1
    return v1
.end method

.method private updateLocalDataBootCompleted()V
    .locals 9

    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "jsonAll":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 230
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 229
    const-string/jumbo v2, "tdownscale"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v1

    .line 233
    .local v1, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 236
    :cond_0
    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->cloudDataStr:Ljava/lang/String;

    .line 239
    const-wide/16 v2, -0x1

    .line 240
    .local v2, "version":J
    if-eqz v0, :cond_1

    .line 241
    const-string/jumbo v4, "version"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 243
    :cond_1
    iget-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    const/4 v5, 0x1

    const-string v6, "GameManagerServiceStub"

    if-nez v4, :cond_2

    .line 244
    new-instance v4, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-direct {v4, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V

    .line 245
    .local v4, "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    iput-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 246
    const-string v7, "BOOT_COMPLETED updateLocalDataBootCompleted use cloudData"

    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iput-boolean v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 248
    .end local v4    # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    goto :goto_0

    .line 250
    :cond_2
    iget-wide v7, v4, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->version:J

    cmp-long v4, v7, v2

    if-eqz v4, :cond_3

    const-wide/16 v7, -0x1

    cmp-long v4, v2, v7

    if-eqz v4, :cond_3

    .line 251
    new-instance v4, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-direct {v4, p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V

    .line 252
    .restart local v4    # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    iput-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 253
    const-string v7, "BOOT_COMPLETED updateLocalDataBootCompleted use cloudData different version"

    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    iput-boolean v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 257
    .end local v4    # "downscaleCloudData":Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
    :cond_3
    :goto_0
    return-void

    .line 234
    .end local v2    # "version":J
    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public addSizeCompatApps(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 307
    sget-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z

    if-nez v0, :cond_0

    .line 308
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-static {p1, v0}, Lcom/android/server/app/GmsUtil;->checkValidApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mSizeCompatApps:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 313
    :cond_1
    return-void
.end method

.method public downscaleForSwitchResolution()V
    .locals 1

    .line 292
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method public downscaleWithPackageNameAndRatio(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "ratio"    # Ljava/lang/String;

    .line 298
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 609
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "WindowDownscale Service"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IS_TABLET = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lmiui/os/Build;->IS_TABLET:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 611
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WQHD = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    invoke-static {v3}, Lcom/android/server/app/GmsUtil;->isWQHD(I)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 613
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IntelligentPowerSavingEnable = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PowerSaving = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 617
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ExtremeModeEnable = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mExtremeModeEnable:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 619
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FromCloud = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 620
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CloudData = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->cloudDataStr:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const-string v1, "--- Apps: begin ---"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mAppStates:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 623
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;

    invoke-virtual {v4}, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;>;"
    goto :goto_0

    .line 625
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    const-string v1, "--- Apps: end"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    const-string v1, "--- Cmd app begin ---"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 629
    .local v3, "item":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "{packageName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "} "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    .end local v3    # "item":Ljava/lang/String;
    goto :goto_1

    .line 631
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 632
    const-string v1, "--- Cmd end"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    const-string v1, "--- trace ---"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 636
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->limitLogger:Lcom/android/server/app/GmsLimitLogger;

    invoke-virtual {v1, p1}, Lcom/android/server/app/GmsLimitLogger;->dump(Ljava/io/PrintWriter;)V

    .line 637
    return-void
.end method

.method public dynamicRefresh(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "ratio"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 303
    return-void
.end method

.method public handleAppDied(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 570
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    if-eqz v0, :cond_2

    .line 571
    const-string v0, "com.tencent.mm"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    const-string v0, "GameManagerServiceStub"

    const-string v1, "com.tencent.mm handleAppDied"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    return-void

    .line 575
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    iget v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->currentWidth:I

    invoke-static {p1, v0, v1}, Lcom/android/server/app/GmsUtil;->needDownscale(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 576
    return-void

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    const v1, 0x1b207

    invoke-virtual {v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 579
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 580
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 582
    .end local v0    # "msg":Landroid/os/Message;
    :cond_2
    return-void
.end method

.method public registerDownscaleObserver(Landroid/content/Context;)V
    .locals 9
    .param p1, "mContext"    # Landroid/content/Context;

    .line 145
    const-string v0, "GameManagerServiceStub"

    const-string v1, "downscale register"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mContext:Landroid/content/Context;

    .line 147
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    if-nez v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 151
    :cond_0
    new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    .line 153
    :cond_1
    new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    .line 155
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "POWER_SAVE_MODE_OPEN"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mPowerSaving:Z

    .line 158
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "persist.sys.downscale_power_save"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_3

    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->isDebugPowerSave:Z

    .line 162
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    xor-int/2addr v0, v3

    .line 163
    .local v0, "defValue":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "miui_screen_compat"

    invoke-static {v4, v5, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v3, :cond_4

    move v4, v3

    goto :goto_2

    :cond_4
    move v4, v2

    :goto_2
    iput-boolean v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->intelligentPowerSavingEnable:Z

    .line 166
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "persist.sys.miui_downscale"

    invoke-static {v4, v6}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 167
    .local v4, "proEnable":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "disable"

    invoke-static {v7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 168
    iput-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mProEnable:Ljava/lang/String;

    .line 171
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 172
    invoke-static {v6}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v8, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    .line 171
    invoke-virtual {v7, v6, v2, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 173
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 174
    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v7, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    .line 173
    invoke-virtual {v6, v1, v3, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 175
    new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/android/server/app/GameManagerServiceStubImpl$GMSBroadcastReceiver;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v3, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {p1, v1, v3, v6, v6}, Landroid/content/Context;->registerReceiverForAllUsers(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 177
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 178
    const-string v3, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v6, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    .line 177
    invoke-virtual {v1, v3, v2, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 179
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 180
    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    .line 179
    invoke-virtual {v1, v3, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 181
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 182
    const-string v3, "pc_security_center_extreme_mode_enter"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mObserver:Lcom/android/server/app/GameManagerServiceStubImpl$GMSObserver;

    .line 181
    invoke-virtual {v1, v3, v2, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 184
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    if-nez v1, :cond_6

    .line 185
    invoke-direct {p0}, Lcom/android/server/app/GameManagerServiceStubImpl;->initLocalDownscaleAppList()Lorg/json/JSONObject;

    move-result-object v1

    .line 186
    .local v1, "jsonAll":Lorg/json/JSONObject;
    new-instance v3, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-direct {v3, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V

    iput-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    .line 187
    iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->fromCloud:Z

    .line 189
    .end local v1    # "jsonAll":Lorg/json/JSONObject;
    :cond_6
    return-void
.end method

.method public setActivityManagerService(Lcom/android/server/am/ActivityManagerService;)V
    .locals 2
    .param p1, "service"    # Lcom/android/server/am/ActivityManagerService;

    .line 586
    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 587
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    if-nez v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 591
    :cond_0
    new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandlerThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v1}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;-><init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mHandler:Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;

    .line 593
    :cond_1
    return-void
.end method

.method public shellCmd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "ratio"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 328
    invoke-virtual {p0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl;->getCompatChangeId(Ljava/lang/String;)J

    move-result-wide v0

    .line 330
    .local v0, "changeId":J
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    .line 332
    .local v2, "enabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    .line 333
    iget-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    .local v3, "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    goto :goto_0

    .line 335
    .end local v3    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_0
    const-wide/32 v3, 0xa09e1d7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 337
    iget-object v3, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->DOWNSCALE_CHANGE_IDS:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->stream()Ljava/util/stream/Stream;

    move-result-object v3

    new-instance v4, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v4, v0, v1}, Lcom/android/server/app/GameManagerServiceStubImpl$$ExternalSyntheticLambda0;-><init>(J)V

    .line 338
    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v3

    .line 339
    invoke-static {}, Ljava/util/stream/Collectors;->toSet()Ljava/util/stream/Collector;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    .line 341
    .restart local v3    # "disabled":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :goto_0
    const-string v4, "disable"

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v5, "GameManagerServiceStub"

    if-eqz v4, :cond_1

    .line 342
    iget-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    invoke-virtual {v4, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 343
    iget-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    invoke-virtual {v4, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 345
    :cond_1
    iget-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    const/16 v6, 0x32

    if-gt v4, v6, :cond_3

    .line 346
    iget-object v4, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mShellCmdDownscalePackageNames:Ljava/util/HashSet;

    invoke-virtual {v4, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_2
    :goto_1
    nop

    .line 352
    const-string v4, "platform_compat"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    check-cast v4, Lcom/android/server/compat/PlatformCompat;

    .line 353
    .local v4, "platformCompat":Lcom/android/server/compat/PlatformCompat;
    new-instance v6, Lcom/android/internal/compat/CompatibilityChangeConfig;

    new-instance v7, Landroid/compat/Compatibility$ChangeConfig;

    invoke-direct {v7, v2, v3}, Landroid/compat/Compatibility$ChangeConfig;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    invoke-direct {v6, v7}, Lcom/android/internal/compat/CompatibilityChangeConfig;-><init>(Landroid/compat/Compatibility$ChangeConfig;)V

    .line 356
    .local v6, "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
    invoke-virtual {v4, v6, p2}, Lcom/android/server/compat/PlatformCompat;->setOverrides(Lcom/android/internal/compat/CompatibilityChangeConfig;Ljava/lang/String;)V

    .line 357
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "shellCmd--- "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 358
    .local v7, "logStr":Ljava/lang/String;
    invoke-static {v5, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v5, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->limitLogger:Lcom/android/server/app/GmsLimitLogger;

    new-instance v8, Lcom/android/server/app/GmsLimitLogger$StringEvent;

    invoke-direct {v8, v7}, Lcom/android/server/app/GmsLimitLogger$StringEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Lcom/android/server/app/GmsLimitLogger;->log(Lcom/android/server/app/GmsLimitLogger$Event;)V

    .line 360
    return-void

    .line 348
    .end local v4    # "platformCompat":Lcom/android/server/compat/PlatformCompat;
    .end local v6    # "overrides":Lcom/android/internal/compat/CompatibilityChangeConfig;
    .end local v7    # "logStr":Ljava/lang/String;
    :cond_3
    const-string v4, "commands enable no more than 50"

    invoke-static {v5, v4}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    return-void
.end method

.method public toggleDownscale(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageNameParam"    # Ljava/lang/String;

    .line 284
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-static {p1, v0}, Lcom/android/server/app/GmsUtil;->checkValidApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    return-void

    .line 287
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V

    .line 288
    return-void
.end method

.method public toggleDownscaleForStopedApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 317
    sget-boolean v0, Lcom/android/server/app/GameManagerServiceStubImpl;->FOLD_DEVICE:Z

    if-nez v0, :cond_0

    .line 318
    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl;->mDownscaleCloudData:Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;

    invoke-static {p1, v0}, Lcom/android/server/app/GmsUtil;->checkValidApp(Ljava/lang/String;Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl;->runningApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 323
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->executeDownscale(Ljava/lang/String;Z)V

    .line 324
    return-void

    .line 321
    :cond_2
    :goto_0
    return-void
.end method
