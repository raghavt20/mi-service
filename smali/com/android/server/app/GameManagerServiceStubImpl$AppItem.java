class com.android.server.app.GameManagerServiceStubImpl$AppItem {
	 /* .source "GameManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/GameManagerServiceStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "AppItem" */
} // .end annotation
/* # instance fields */
Integer appVersion;
Integer mode;
java.lang.String packageName;
java.lang.String ratio;
Integer systemVersion;
/* # direct methods */
public com.android.server.app.GameManagerServiceStubImpl$AppItem ( ) {
/* .locals 2 */
/* .param p1, "cloudDataApps" # Lorg/json/JSONObject; */
/* .line 769 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 767 */
final String v0 = ""; // const-string v0, ""
this.ratio = v0;
/* .line 770 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 771 */
	 final String v0 = "packageName"; // const-string v0, "packageName"
	 (( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
	 this.packageName = v0;
	 /* .line 772 */
	 /* const-string/jumbo v0, "systemVersion" */
	 int v1 = 7; // const/4 v1, 0x7
	 v0 = 	 (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
	 /* iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->systemVersion:I */
	 /* .line 773 */
	 /* const-string/jumbo v0, "version" */
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
	 /* iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->appVersion:I */
	 /* .line 774 */
	 final String v0 = "mode"; // const-string v0, "mode"
	 v0 = 	 (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
	 /* iput v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->mode:I */
	 /* .line 776 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 780 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{packageName=\'" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", appVersion="; // const-string v2, ", appVersion="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->appVersion:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", mode="; // const-string v2, ", mode="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->mode:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", ratio=\'"; // const-string v2, ", ratio=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.ratio;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
