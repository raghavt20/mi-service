.class final Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;
.super Landroid/os/Handler;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/GameManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "InnerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/app/GameManagerServiceStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/app/GameManagerServiceStubImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/app/GameManagerServiceStubImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 641
    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    .line 642
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 643
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 647
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 674
    const-string v0, "GameManagerServiceStub"

    const-string v1, "default case"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 671
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$mexecuteDownscale(Lcom/android/server/app/GameManagerServiceStubImpl;Ljava/lang/String;Z)V

    .line 672
    goto :goto_0

    .line 668
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$mprocessIntelligentPowerSave(Lcom/android/server/app/GameManagerServiceStubImpl;)V

    .line 669
    goto :goto_0

    .line 665
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$mprocessCloudData(Lcom/android/server/app/GameManagerServiceStubImpl;)V

    .line 666
    goto :goto_0

    .line 662
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$mprocessBootCompletedAction(Lcom/android/server/app/GameManagerServiceStubImpl;)V

    .line 663
    goto :goto_0

    .line 659
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$mprocessPowerSaveAction(Lcom/android/server/app/GameManagerServiceStubImpl;)V

    .line 660
    goto :goto_0

    .line 649
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 650
    .local v0, "processName":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$sfgetFOLD_DEVICE()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmSizeCompatApps(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmSizeCompatApps(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 651
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmSizeCompatApps(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 652
    goto :goto_0

    .line 654
    :cond_0
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-static {v1}, Lcom/android/server/app/GameManagerServiceStubImpl;->-$$Nest$fgetmShellCmdDownscalePackageNames(Lcom/android/server/app/GameManagerServiceStubImpl;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 655
    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$InnerHandler;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/app/GameManagerServiceStubImpl;->toggleDownscale(Ljava/lang/String;)V

    .line 677
    .end local v0    # "processName":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1b207
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
