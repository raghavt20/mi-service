public class com.android.server.app.GmsLimitLogger {
	 /* .source "GmsLimitLogger.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/app/GmsLimitLogger$Event;, */
	 /* Lcom/android/server/app/GmsLimitLogger$StringEvent; */
	 /* } */
} // .end annotation
/* # instance fields */
private final java.util.LinkedList mEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/app/GmsLimitLogger$Event;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final Integer mMemSize;
/* # direct methods */
public com.android.server.app.GmsLimitLogger ( ) {
/* .locals 1 */
/* .param p1, "size" # I */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 21 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mEvents = v0;
/* .line 22 */
/* iput p1, p0, Lcom/android/server/app/GmsLimitLogger;->mMemSize:I */
/* .line 23 */
return;
} // .end method
/* # virtual methods */
public synchronized void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* monitor-enter p0 */
/* .line 33 */
try { // :try_start_0
v0 = this.mEvents;
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/app/GmsLimitLogger$Event; */
/* .line 34 */
/* .local v1, "evt":Lcom/android/server/app/GmsLimitLogger$Event; */
(( com.android.server.app.GmsLimitLogger$Event ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/server/app/GmsLimitLogger$Event;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 35 */
} // .end local v1 # "evt":Lcom/android/server/app/GmsLimitLogger$Event;
/* .line 36 */
} // .end local p0 # "this":Lcom/android/server/app/GmsLimitLogger;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 32 */
} // .end local p1 # "pw":Ljava/io/PrintWriter;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void log ( com.android.server.app.GmsLimitLogger$Event p0 ) {
/* .locals 2 */
/* .param p1, "evt" # Lcom/android/server/app/GmsLimitLogger$Event; */
/* monitor-enter p0 */
/* .line 26 */
try { // :try_start_0
v0 = this.mEvents;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* iget v1, p0, Lcom/android/server/app/GmsLimitLogger;->mMemSize:I */
/* if-lt v0, v1, :cond_0 */
/* .line 27 */
v0 = this.mEvents;
(( java.util.LinkedList ) v0 ).removeFirst ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 29 */
} // .end local p0 # "this":Lcom/android/server/app/GmsLimitLogger;
} // :cond_0
v0 = this.mEvents;
(( java.util.LinkedList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 30 */
/* monitor-exit p0 */
return;
/* .line 25 */
} // .end local p1 # "evt":Lcom/android/server/app/GmsLimitLogger$Event;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
