class com.android.server.app.PreCacheUtilStubImpl$PreCacheContentObserver extends android.database.ContentObserver {
	 /* .source "PreCacheUtilStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/app/PreCacheUtilStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PreCacheContentObserver" */
} // .end annotation
/* # instance fields */
final com.android.server.app.PreCacheUtilStubImpl this$0; //synthetic
/* # direct methods */
public com.android.server.app.PreCacheUtilStubImpl$PreCacheContentObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 55 */
this.this$0 = p1;
/* .line 56 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 57 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 61 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v0 = "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"; // const-string v0, "content://com.android.settings.cloud.CloudSettings/cloud_all_data/notify"
	 android.net.Uri .parse ( v0 );
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 62 */
		 v0 = this.this$0;
		 com.android.server.app.PreCacheUtilStubImpl .-$$Nest$mupdatePreCacheSetting ( v0 );
		 /* .line 64 */
	 } // :cond_0
	 return;
} // .end method
