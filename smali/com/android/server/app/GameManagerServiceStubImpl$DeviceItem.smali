.class Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
.super Ljava/lang/Object;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/GameManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DeviceItem"
.end annotation


# instance fields
.field name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "cloudDataDevice"    # Lorg/json/JSONObject;

    .line 792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 793
    if-eqz p1, :cond_0

    .line 794
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;->name:Ljava/lang/String;

    .line 796
    :cond_0
    return-void
.end method
