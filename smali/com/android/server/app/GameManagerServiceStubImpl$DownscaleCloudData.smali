.class Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;
.super Ljava/lang/Object;
.source "GameManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/app/GameManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownscaleCloudData"
.end annotation


# instance fields
.field apps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;",
            ">;"
        }
    .end annotation
.end field

.field devices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field enable:Z

.field offLocalDevices:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

.field final synthetic this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

.field version:J


# direct methods
.method public constructor <init>(Lcom/android/server/app/GameManagerServiceStubImpl;Lorg/json/JSONObject;)V
    .locals 9
    .param p1, "this$0"    # Lcom/android/server/app/GameManagerServiceStubImpl;
    .param p2, "cloudData"    # Lorg/json/JSONObject;

    .line 695
    iput-object p1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->this$0:Lcom/android/server/app/GameManagerServiceStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 690
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->devices:Ljava/util/HashSet;

    .line 691
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->apps:Ljava/util/HashMap;

    .line 693
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->offLocalDevices:Ljava/util/HashSet;

    .line 696
    if-eqz p2, :cond_3

    .line 697
    const-string v0, "enable"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z

    .line 698
    const-string/jumbo v2, "version"

    const-wide/16 v3, -0x1

    invoke-virtual {p2, v2, v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->version:J

    .line 699
    const/4 v2, 0x0

    .line 700
    .local v2, "appsJson":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .line 701
    .local v3, "devicesJson":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .line 702
    .local v4, "scenesJson":Lorg/json/JSONObject;
    sget-boolean v5, Lmiui/os/Build;->IS_TABLET:Z

    const-string v6, "scenes"

    const-string v7, "devices"

    const-string v8, "apps"

    if-nez v5, :cond_0

    .line 703
    invoke-virtual {p2, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 704
    invoke-virtual {p2, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 705
    invoke-virtual {p2, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    goto :goto_0

    .line 707
    :cond_0
    const-string v5, "pad"

    invoke-virtual {p2, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 708
    .local v5, "padJson":Lorg/json/JSONObject;
    if-nez v5, :cond_1

    .line 709
    return-void

    .line 711
    :cond_1
    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->enable:Z

    .line 713
    if-eqz v5, :cond_2

    .line 714
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 715
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 716
    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 719
    .end local v5    # "padJson":Lorg/json/JSONObject;
    :cond_2
    :goto_0
    invoke-direct {p0, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseApps(Lorg/json/JSONArray;)V

    .line 721
    const-string v0, "off_devices"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseOffLocalDevices(Lorg/json/JSONArray;)V

    .line 722
    invoke-direct {p0, v3}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseDevices(Lorg/json/JSONArray;)V

    .line 723
    invoke-direct {p0, v4}, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->parseScenes(Lorg/json/JSONObject;)V

    .line 725
    .end local v2    # "appsJson":Lorg/json/JSONArray;
    .end local v3    # "devicesJson":Lorg/json/JSONArray;
    .end local v4    # "scenesJson":Lorg/json/JSONObject;
    :cond_3
    return-void
.end method

.method private parseApps(Lorg/json/JSONArray;)V
    .locals 4
    .param p1, "appsJson"    # Lorg/json/JSONArray;

    .line 728
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 729
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 730
    new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;-><init>(Lorg/json/JSONObject;)V

    .line 731
    .local v1, "appItem":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->apps:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    .end local v1    # "appItem":Lcom/android/server/app/GameManagerServiceStubImpl$AppItem;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 734
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private parseDevices(Lorg/json/JSONArray;)V
    .locals 4
    .param p1, "devicesJson"    # Lorg/json/JSONArray;

    .line 736
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 737
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 738
    new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;-><init>(Lorg/json/JSONObject;)V

    .line 739
    .local v1, "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->devices:Ljava/util/HashSet;

    iget-object v3, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 737
    .end local v1    # "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 742
    .end local v0    # "i":I
    :cond_0
    iget-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->devices:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->offLocalDevices:Ljava/util/HashSet;

    invoke-static {v0, v1}, Lcom/android/server/app/GmsSettings;->getLocalDevicesForPowerSaving(Ljava/util/HashSet;Ljava/util/HashSet;)V

    .line 744
    :cond_1
    return-void
.end method

.method private parseOffLocalDevices(Lorg/json/JSONArray;)V
    .locals 4
    .param p1, "offLocalDevicesJson"    # Lorg/json/JSONArray;

    .line 751
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 752
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 753
    new-instance v1, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;-><init>(Lorg/json/JSONObject;)V

    .line 754
    .local v1, "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
    iget-object v2, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->offLocalDevices:Ljava/util/HashSet;

    iget-object v3, v1, Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 752
    .end local v1    # "deviceItem":Lcom/android/server/app/GameManagerServiceStubImpl$DeviceItem;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 757
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private parseScenes(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "scenesJson"    # Lorg/json/JSONObject;

    .line 746
    if-eqz p1, :cond_0

    .line 747
    new-instance v0, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    invoke-direct {v0, p1}, Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/android/server/app/GameManagerServiceStubImpl$DownscaleCloudData;->scenes:Lcom/android/server/app/GameManagerServiceStubImpl$Scenes;

    .line 749
    :cond_0
    return-void
.end method
