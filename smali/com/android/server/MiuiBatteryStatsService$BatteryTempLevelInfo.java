class com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo {
	 /* .source "MiuiBatteryStatsService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryStatsService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "BatteryTempLevelInfo" */
} // .end annotation
/* # instance fields */
private Integer mHighLevelCount;
private Boolean mLastSatisfyTempLevelCondition;
private Integer mlowLevelCount;
final com.android.server.MiuiBatteryStatsService this$0; //synthetic
/* # direct methods */
static void -$$Nest$mcycleCheck ( com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->cycleCheck()V */
return;
} // .end method
static void -$$Nest$mdataReset ( com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->dataReset()V */
return;
} // .end method
 com.android.server.MiuiBatteryStatsService$BatteryTempLevelInfo ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryStatsService; */
/* .line 1401 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1402 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I */
/* .line 1403 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I */
/* .line 1404 */
/* iput-boolean v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z */
return;
} // .end method
private void cycleCheck ( ) {
/* .locals 7 */
/* .line 1407 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1408 */
/* .local v0, "btValue":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 1409 */
/* .local v1, "tlValue":I */
v2 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v2 );
(( miui.util.IMiCharge ) v2 ).getBatteryTbat ( ); // invoke-virtual {v2}, Lmiui/util/IMiCharge;->getBatteryTbat()Ljava/lang/String;
/* .line 1410 */
/* .local v2, "bt":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v3, :cond_0 */
/* .line 1411 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
v0 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v3 ).parseInt ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1413 */
} // :cond_0
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmPlugType ( v3 );
int v4 = 4; // const/4 v4, 0x4
/* if-ne v3, v4, :cond_2 */
/* .line 1414 */
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v3 );
/* const-string/jumbo v4, "wlscharge_control_limit" */
(( miui.util.IMiCharge ) v3 ).getMiChargePath ( v4 ); // invoke-virtual {v3, v4}, Lmiui/util/IMiCharge;->getMiChargePath(Ljava/lang/String;)Ljava/lang/String;
/* .line 1415 */
/* .local v3, "tl":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_1 */
/* .line 1416 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v4 );
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v4 ).parseInt ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1418 */
} // .end local v3 # "tl":Ljava/lang/String;
} // :cond_1
/* .line 1419 */
} // :cond_2
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmMiCharge ( v3 );
(( miui.util.IMiCharge ) v3 ).getBatteryThermaLevel ( ); // invoke-virtual {v3}, Lmiui/util/IMiCharge;->getBatteryThermaLevel()Ljava/lang/String;
/* .line 1420 */
/* .restart local v3 # "tl":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_3 */
/* .line 1421 */
v4 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v4 );
v1 = (( com.android.server.MiuiBatteryStatsService$BatteryStatsHandler ) v4 ).parseInt ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/MiuiBatteryStatsService$BatteryStatsHandler;->parseInt(Ljava/lang/String;)I
/* .line 1424 */
} // .end local v3 # "tl":Ljava/lang/String;
} // :cond_3
} // :goto_0
v3 = this.this$0;
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$fgetDEBUG ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1425 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "batteryTbatValue = "; // const-string v4, "batteryTbatValue = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " thermalLevelValue = "; // const-string v4, " thermalLevelValue = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiBatteryStatsService"; // const-string v4, "MiuiBatteryStatsService"
android.util.Slog .d ( v4,v3 );
/* .line 1427 */
} // :cond_4
/* const/16 v3, 0x1ae */
int v4 = 1; // const/4 v4, 0x1
/* if-lt v0, v3, :cond_5 */
/* const/16 v3, 0x1d6 */
/* if-gt v0, v3, :cond_5 */
/* const/16 v3, 0xd */
/* if-lt v1, v3, :cond_5 */
/* .line 1428 */
/* iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I */
/* add-int/2addr v3, v4 */
/* iput v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I */
/* .line 1430 */
} // :cond_5
/* iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I */
/* const/16 v5, 0xa */
int v6 = 0; // const/4 v6, 0x0
/* if-lt v3, v5, :cond_6 */
/* .line 1431 */
com.android.server.MiuiBatteryStatsService .-$$Nest$sfputmIsSatisfyTempLevelCondition ( v4 );
/* .line 1432 */
/* iput v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I */
/* .line 1434 */
} // :cond_6
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempLevelCondition ( );
if ( v3 != null) { // if-eqz v3, :cond_7
/* const/16 v3, 0x17c */
/* if-gt v0, v3, :cond_7 */
/* const/16 v3, 0x8 */
/* if-gt v1, v3, :cond_7 */
/* .line 1435 */
/* iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I */
/* add-int/2addr v3, v4 */
/* iput v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I */
/* .line 1437 */
} // :cond_7
/* iget v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I */
/* if-lt v3, v5, :cond_8 */
/* .line 1438 */
com.android.server.MiuiBatteryStatsService .-$$Nest$sfputmIsSatisfyTempLevelCondition ( v6 );
/* .line 1439 */
/* iput v6, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I */
/* .line 1442 */
} // :cond_8
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempLevelCondition ( );
if ( v3 != null) { // if-eqz v3, :cond_9
/* iget-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z */
if ( v3 != null) { // if-eqz v3, :cond_a
} // :cond_9
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempLevelCondition ( );
/* if-nez v3, :cond_b */
/* iget-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z */
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 1444 */
} // :cond_a
v3 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v3 );
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$msendAdjustVolBroadcast ( v3 );
/* .line 1445 */
v3 = com.android.server.MiuiBatteryStatsService .-$$Nest$sfgetmIsSatisfyTempLevelCondition ( );
/* iput-boolean v3, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mLastSatisfyTempLevelCondition:Z */
/* .line 1447 */
} // :cond_b
return;
} // .end method
private void dataReset ( ) {
/* .locals 1 */
/* .line 1450 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mHighLevelCount:I */
/* .line 1451 */
/* iput v0, p0, Lcom/android/server/MiuiBatteryStatsService$BatteryTempLevelInfo;->mlowLevelCount:I */
/* .line 1452 */
com.android.server.MiuiBatteryStatsService .-$$Nest$sfputmIsSatisfyTempLevelCondition ( v0 );
/* .line 1453 */
v0 = this.this$0;
com.android.server.MiuiBatteryStatsService .-$$Nest$fgetmHandler ( v0 );
com.android.server.MiuiBatteryStatsService$BatteryStatsHandler .-$$Nest$msendAdjustVolBroadcast ( v0 );
/* .line 1454 */
return;
} // .end method
