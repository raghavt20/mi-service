public class com.android.server.DarkModeTimeModeHelper {
	 /* .source "DarkModeTimeModeHelper.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.DarkModeTimeModeHelper ( ) {
		 /* .locals 0 */
		 /* .line 58 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Integer getDarkModeEndTime ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "ctx" # Landroid/content/Context; */
		 /* .line 256 */
		 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v1 = "dark_mode_time_end"; // const-string v1, "dark_mode_time_end"
		 /* const/16 v2, 0x1a4 */
		 v0 = 		 android.provider.Settings$System .getInt ( v0,v1,v2 );
	 } // .end method
	 public static Integer getDarkModeStartTime ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "ctx" # Landroid/content/Context; */
		 /* .line 244 */
		 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v1 = "dark_mode_time_start"; // const-string v1, "dark_mode_time_start"
		 /* const/16 v2, 0x474 */
		 v0 = 		 android.provider.Settings$System .getInt ( v0,v1,v2 );
	 } // .end method
	 public static Integer getDarkModeSuggestCount ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 128 */
		 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v1 = "dark_mode_suggest_notification_count"; // const-string v1, "dark_mode_suggest_notification_count"
		 int v2 = 0; // const/4 v2, 0x0
		 v0 = 		 android.provider.Settings$System .getInt ( v0,v1,v2 );
	 } // .end method
	 public static Integer getDarkModeTimeType ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "ctx" # Landroid/content/Context; */
		 /* .line 292 */
		 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v1 = "dark_mode_time_type"; // const-string v1, "dark_mode_time_type"
		 int v2 = 2; // const/4 v2, 0x2
		 v0 = 		 android.provider.Settings$System .getInt ( v0,v1,v2 );
	 } // .end method
	 private static java.util.List getHomeApplicationList ( android.content.Context p0 ) {
		 /* .locals 7 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Landroid/content/Context;", */
		 /* ")", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 145 */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 146 */
	 /* .local v0, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
	 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 /* .line 147 */
	 /* .local v1, "packageManager":Landroid/content/pm/PackageManager; */
	 /* new-instance v2, Landroid/content/Intent; */
	 final String v3 = "android.intent.action.MAIN"; // const-string v3, "android.intent.action.MAIN"
	 /* invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* .line 148 */
	 /* .local v2, "intent":Landroid/content/Intent; */
	 final String v3 = "android.intent.category.HOME"; // const-string v3, "android.intent.category.HOME"
	 (( android.content.Intent ) v2 ).addCategory ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 149 */
	 /* const/high16 v3, 0x10000 */
	 (( android.content.pm.PackageManager ) v1 ).queryIntentActivities ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
	 /* .line 150 */
	 /* .local v3, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_0
	 /* check-cast v5, Landroid/content/pm/ResolveInfo; */
	 /* .line 151 */
	 /* .local v5, "resolveInfo":Landroid/content/pm/ResolveInfo; */
	 v6 = this.activityInfo;
	 v6 = this.packageName;
	 /* .line 152 */
} // .end local v5 # "resolveInfo":Landroid/content/pm/ResolveInfo;
/* .line 153 */
} // :cond_0
} // .end method
public static Long getLastSuggestTime ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .line 305 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_last_time_of_suggest"; // const-string v1, "dark_mode_last_time_of_suggest"
/* const-wide/16 v2, -0x2760 */
android.provider.Settings$System .getLong ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public static Integer getNowTime ( ) {
/* .locals 4 */
/* .line 76 */
java.util.Calendar .getInstance ( );
/* .line 77 */
/* .local v0, "calendar":Ljava/util/Calendar; */
/* const/16 v1, 0xb */
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 78 */
/* .local v1, "hour":I */
/* const/16 v2, 0xc */
v2 = (( java.util.Calendar ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I
/* .line 79 */
/* .local v2, "minute":I */
/* mul-int/lit8 v3, v1, 0x3c */
/* add-int/2addr v3, v2 */
} // .end method
public static Long getNowTimeInMills ( ) {
/* .locals 2 */
/* .line 89 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public static Integer getSunRiseTime ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .line 268 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "auto_night_end_time"; // const-string v1, "auto_night_end_time"
/* const/16 v2, 0x1a4 */
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
} // .end method
public static Integer getSunSetTime ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .line 280 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "auto_night_start_time"; // const-string v1, "auto_night_start_time"
/* const/16 v2, 0x4b0 */
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
} // .end method
public static java.lang.String getTimeInString ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "time" # I */
/* .line 93 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* div-int/lit8 v1, p0, 0x3c */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ": "; // const-string v1, ": "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* rem-int/lit8 v1, p0, 0x3c */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static Boolean isDarkModeContrastEnable ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .line 340 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_contrast_enable"; // const-string v1, "dark_mode_contrast_enable"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.MiuiSettings$System .getBoolean ( v0,v1,v2 );
} // .end method
public static Boolean isDarkModeEnable ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 165 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_enable"; // const-string v1, "dark_mode_enable"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v1,v2,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
public static Boolean isDarkModeOpen ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 102 */
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeEnable ( p0 );
int v1 = 1; // const/4 v1, 0x1
final String v2 = "DarkModeTimeModeHelper"; // const-string v2, "DarkModeTimeModeHelper"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 103 */
final String v0 = "darkModeEnable = true"; // const-string v0, "darkModeEnable = true"
android.util.Slog .i ( v2,v0 );
/* .line 104 */
/* .line 107 */
} // :cond_0
v0 = com.android.server.DarkModeTimeModeHelper .isDarkModeTimeEnable ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 108 */
final String v0 = "darkModeTimeEnable = true"; // const-string v0, "darkModeTimeEnable = true"
android.util.Slog .i ( v2,v0 );
/* .line 109 */
/* .line 111 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isDarkModeSuggestEnable ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .line 317 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_has_get_suggest_from_cloud"; // const-string v1, "dark_mode_has_get_suggest_from_cloud"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.MiuiSettings$System .getBoolean ( v0,v1,v2 );
} // .end method
public static Boolean isDarkModeTimeEnable ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 177 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_time_enable"; // const-string v1, "dark_mode_time_enable"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.MiuiSettings$System .getBoolean ( v0,v1,v2 );
} // .end method
public static Boolean isInNight ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 66 */
v0 = com.android.server.DarkModeTimeModeHelper .getNowTime ( );
v1 = com.android.server.DarkModeTimeModeHelper .getSunSetTime ( p0 );
/* if-ge v0, v1, :cond_1 */
/* .line 67 */
v0 = com.android.server.DarkModeTimeModeHelper .getNowTime ( );
v1 = com.android.server.DarkModeTimeModeHelper .getSunRiseTime ( p0 );
/* if-gt v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 66 */
} // :goto_1
} // .end method
public static Boolean isOnHome ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 136 */
final String v0 = "activity"; // const-string v0, "activity"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 137 */
/* .local v0, "activityManager":Landroid/app/ActivityManager; */
int v1 = 1; // const/4 v1, 0x1
(( android.app.ActivityManager ) v0 ).getRunningTasks ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* .line 138 */
/* .local v1, "runningTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;" */
com.android.server.DarkModeTimeModeHelper .getHomeApplicationList ( p0 );
int v3 = 0; // const/4 v3, 0x0
/* check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo; */
v3 = this.topActivity;
v2 = (( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
} // .end method
public static Boolean isSunTimeFromCloud ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .line 350 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "get_sun_time_from_cloud"; // const-string v1, "get_sun_time_from_cloud"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
public static Boolean isSuntimeType ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 331 */
int v0 = 2; // const/4 v0, 0x2
v1 = com.android.server.DarkModeTimeModeHelper .getDarkModeTimeType ( p0 );
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static void setDarkModeAutoTimeEnable ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "enable" # Z */
/* .line 189 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_auto_time_enable"; // const-string v1, "dark_mode_auto_time_enable"
android.provider.MiuiSettings$System .putBoolean ( v0,v1,p1 );
/* .line 191 */
return;
} // .end method
public static void setDarkModeSuggestCount ( android.content.Context p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "count" # I */
/* .line 120 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_suggest_notification_count"; // const-string v1, "dark_mode_suggest_notification_count"
android.provider.Settings$System .putInt ( v0,v1,p1 );
/* .line 122 */
return;
} // .end method
public static void setDarkModeSuggestEnable ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "enable" # Z */
/* .line 233 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_has_get_suggest_from_cloud"; // const-string v1, "dark_mode_has_get_suggest_from_cloud"
android.provider.MiuiSettings$System .putBoolean ( v0,v1,p1 );
/* .line 235 */
return;
} // .end method
public static void setDarkModeTimeEnable ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "enable" # Z */
/* .line 211 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_time_enable"; // const-string v1, "dark_mode_time_enable"
android.provider.MiuiSettings$System .putBoolean ( v0,v1,p1 );
/* .line 213 */
return;
} // .end method
public static void setDarkModeTimeType ( android.content.Context p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "type" # I */
/* .line 326 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_time_type"; // const-string v1, "dark_mode_time_type"
android.provider.Settings$System .putInt ( v0,v1,p1 );
/* .line 328 */
return;
} // .end method
public static void setLastSuggestTime ( android.content.Context p0, Long p1 ) {
/* .locals 2 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "lastTime" # J */
/* .line 222 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_last_time_of_suggest"; // const-string v1, "dark_mode_last_time_of_suggest"
android.provider.Settings$System .putLong ( v0,v1,p1,p2 );
/* .line 224 */
return;
} // .end method
public static void setSunRiseSunSetMode ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "enable" # Z */
/* .line 200 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "dark_mode_sun_time_mode_enable"; // const-string v1, "dark_mode_sun_time_mode_enable"
android.provider.MiuiSettings$System .putBoolean ( v0,v1,p1 );
/* .line 202 */
return;
} // .end method
public static void setSunTimeFromCloud ( android.content.Context p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "ctx" # Landroid/content/Context; */
/* .param p1, "enable" # Z */
/* .line 346 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "get_sun_time_from_cloud"; // const-string v1, "get_sun_time_from_cloud"
android.provider.Settings$System .putInt ( v0,v1,p1 );
/* .line 347 */
return;
} // .end method
