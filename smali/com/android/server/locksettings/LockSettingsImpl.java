public class com.android.server.locksettings.LockSettingsImpl extends com.android.server.locksettings.LockSettingsStub {
	 /* .source "LockSettingsImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.locksettings.LockSettingsStub$$" */
} // .end annotation
/* # direct methods */
public com.android.server.locksettings.LockSettingsImpl ( ) {
	 /* .locals 0 */
	 /* .line 10 */
	 /* invoke-direct {p0}, Lcom/android/server/locksettings/LockSettingsStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public Boolean checkPrivacyPasswordPattern ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
	 /* .locals 1 */
	 /* .param p1, "pattern" # Ljava/lang/String; */
	 /* .param p2, "filename" # Ljava/lang/String; */
	 /* .param p3, "userId" # I */
	 /* .line 17 */
	 v0 = 	 android.security.MiuiLockPatternUtils .checkPrivacyPasswordPattern ( p1,p2,p3 );
} // .end method
public void miuiSavePinLength ( com.android.server.locksettings.LockSettingsService p0, com.android.internal.widget.LockscreenCredential p1, Integer p2 ) {
	 /* .locals 3 */
	 /* .param p1, "lockSettingsService" # Lcom/android/server/locksettings/LockSettingsService; */
	 /* .param p2, "newCredential" # Lcom/android/internal/widget/LockscreenCredential; */
	 /* .param p3, "userHandle" # I */
	 /* .line 24 */
	 v0 = 	 (( com.android.internal.widget.LockscreenCredential ) p2 ).isPin ( ); // invoke-virtual {p2}, Lcom/android/internal/widget/LockscreenCredential;->isPin()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 25 */
		 v0 = 		 (( com.android.internal.widget.LockscreenCredential ) p2 ).size ( ); // invoke-virtual {p2}, Lcom/android/internal/widget/LockscreenCredential;->size()I
		 /* int-to-long v0, v0 */
		 final String v2 = "lockscreen.password_length"; // const-string v2, "lockscreen.password_length"
		 (( com.android.server.locksettings.LockSettingsService ) p1 ).setLong ( v2, v0, v1, p3 ); // invoke-virtual {p1, v2, v0, v1, p3}, Lcom/android/server/locksettings/LockSettingsService;->setLong(Ljava/lang/String;JI)V
		 /* .line 27 */
	 } // :cond_0
	 return;
} // .end method
public void savePrivacyPasswordPattern ( java.lang.String p0, java.lang.String p1, Integer p2 ) {
	 /* .locals 0 */
	 /* .param p1, "pattern" # Ljava/lang/String; */
	 /* .param p2, "filename" # Ljava/lang/String; */
	 /* .param p3, "userId" # I */
	 /* .line 13 */
	 android.security.MiuiLockPatternUtils .savePrivacyPasswordPattern ( p1,p2,p3 );
	 /* .line 14 */
	 return;
} // .end method
