.class public Lcom/android/server/locksettings/LockSettingsImpl;
.super Lcom/android/server/locksettings/LockSettingsStub;
.source "LockSettingsImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.locksettings.LockSettingsStub$$"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/android/server/locksettings/LockSettingsStub;-><init>()V

    return-void
.end method


# virtual methods
.method public checkPrivacyPasswordPattern(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 17
    invoke-static {p1, p2, p3}, Landroid/security/MiuiLockPatternUtils;->checkPrivacyPasswordPattern(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public miuiSavePinLength(Lcom/android/server/locksettings/LockSettingsService;Lcom/android/internal/widget/LockscreenCredential;I)V
    .locals 3
    .param p1, "lockSettingsService"    # Lcom/android/server/locksettings/LockSettingsService;
    .param p2, "newCredential"    # Lcom/android/internal/widget/LockscreenCredential;
    .param p3, "userHandle"    # I

    .line 24
    invoke-virtual {p2}, Lcom/android/internal/widget/LockscreenCredential;->isPin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p2}, Lcom/android/internal/widget/LockscreenCredential;->size()I

    move-result v0

    int-to-long v0, v0

    const-string v2, "lockscreen.password_length"

    invoke-virtual {p1, v2, v0, v1, p3}, Lcom/android/server/locksettings/LockSettingsService;->setLong(Ljava/lang/String;JI)V

    .line 27
    :cond_0
    return-void
.end method

.method public savePrivacyPasswordPattern(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 13
    invoke-static {p1, p2, p3}, Landroid/security/MiuiLockPatternUtils;->savePrivacyPasswordPattern(Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    return-void
.end method
