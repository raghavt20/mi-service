.class Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ForceDarkAppListManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ForceDarkAppListManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppChangedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ForceDarkAppListManager;


# direct methods
.method private constructor <init>(Lcom/android/server/ForceDarkAppListManager;)V
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;->this$0:Lcom/android/server/ForceDarkAppListManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/ForceDarkAppListManager;Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;-><init>(Lcom/android/server/ForceDarkAppListManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 242
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 245
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 248
    .local v0, "data":Landroid/net/Uri;
    if-nez v0, :cond_2

    .line 249
    return-void

    .line 252
    .end local v0    # "data":Landroid/net/Uri;
    :cond_2
    iget-object v0, p0, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;->this$0:Lcom/android/server/ForceDarkAppListManager;

    invoke-static {v0}, Lcom/android/server/ForceDarkAppListManager;->-$$Nest$mclearCache(Lcom/android/server/ForceDarkAppListManager;)V

    .line 253
    return-void

    .line 243
    :cond_3
    :goto_0
    return-void
.end method
