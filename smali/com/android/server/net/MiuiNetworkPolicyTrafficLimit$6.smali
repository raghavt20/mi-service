.class Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyTrafficLimit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTrafficLimit(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 311
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->val$enabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 314
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->val$enabled:Z

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmIface(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/net/MiuiNetworkManagementService;->enableMobileTrafficLimit(ZLjava/lang/String;)Z

    move-result v0

    .line 315
    .local v0, "rst":Z
    if-eqz v0, :cond_0

    .line 316
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->val$enabled:Z

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mupdateTrafficLimitStatus(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;I)V

    .line 318
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmMobileTcDropEnabled(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v1

    const/4 v2, 0x1

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->setMobileTrafficLimit(ZJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mupdateTrafficLimitStatus(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;I)V

    .line 322
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fputmProcessEnableLimit(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V

    .line 323
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableMobileTrafficLimit rst="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V

    .line 324
    return-void
.end method
