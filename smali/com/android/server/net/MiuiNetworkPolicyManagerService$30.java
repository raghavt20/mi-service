class com.android.server.net.MiuiNetworkPolicyManagerService$30 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWmmerRouterWhitelistChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$30 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1938 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1942 */
try { // :try_start_0
	 v0 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v0 );
	 /* .line 1943 */
	 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v2 = "cloud_wmmer_router_whitelist"; // const-string v2, "cloud_wmmer_router_whitelist"
	 /* .line 1942 */
	 int v3 = -2; // const/4 v3, -0x2
	 android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).updateRouterWhitelist ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRouterWhitelist(Ljava/lang/String;)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 1948 */
	 /* .line 1946 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 1947 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 1949 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
