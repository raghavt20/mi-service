class com.android.server.net.MiuiNetworkPolicyManagerService$22 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerMiuiOptimizationChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$22 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1474 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 1477 */
/* nop */
/* .line 1478 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 1477 */
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
final String v2 = "persist.sys.miui_optimization"; // const-string v2, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v2,v0 );
/* xor-int/2addr v0, v1 */
/* .line 1479 */
/* .local v0, "isCtsMode":Z */
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mnetworkPriorityMode ( v2 );
/* .line 1480 */
/* .local v2, "networkPriority":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "miui optimization mode changed: "; // const-string v4, "miui optimization mode changed: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", current network priority: "; // const-string v4, ", current network priority: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiNetworkPolicy"; // const-string v4, "MiuiNetworkPolicy"
android.util.Log .i ( v4,v3 );
/* .line 1482 */
/* const/16 v3, 0xff */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 1483 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableNetworkPriority ( v1,v3 );
	 /* .line 1484 */
	 v1 = this.this$0;
	 int v3 = 0; // const/4 v3, 0x0
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableWmmer ( v1,v3 );
	 /* .line 1485 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkManager ( v1 );
	 (( com.android.server.net.MiuiNetworkManagementService ) v1 ).enableIptablesRestore ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->enableIptablesRestore(Z)Z
	 /* .line 1487 */
} // :cond_0
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkManager ( v4 );
(( com.android.server.net.MiuiNetworkManagementService ) v4 ).enableIptablesRestore ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableIptablesRestore(Z)Z
/* .line 1488 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableWmmer ( v1 );
/* .line 1489 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* if-eq v2, v3, :cond_1 */
	 /* .line 1490 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableNetworkPriority ( v1,v2 );
	 /* .line 1493 */
} // :cond_1
} // :goto_0
return;
} // .end method
