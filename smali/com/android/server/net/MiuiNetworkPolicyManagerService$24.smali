.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyManagerService.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 1522
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .line 1525
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, " mWifiConnected="

    const-string v2, " mNetworkPriorityMode ="

    const/4 v3, 0x4

    const/4 v4, 0x5

    const-string v5, "MiuiNetworkPolicy"

    const/4 v6, 0x0

    const/4 v7, 0x1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_4

    .line 1622
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v0

    .line 1623
    .local v0, "lastThermalForceMode":I
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    .line 1624
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mnetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v3

    .line 1625
    .local v3, "_networkPriority":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "thermal networkPriorityMode = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mThermalForceMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1627
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    if-eq v3, v1, :cond_0

    .line 1628
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableNetworkPriority(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    goto :goto_0

    .line 1630
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1632
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkBoostService(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1633
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkBoostService(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v4

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v5

    invoke-virtual {v1, v2, v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V

    .line 1636
    :cond_1
    :goto_0
    return v7

    .line 1619
    .end local v0    # "lastThermalForceMode":I
    .end local v3    # "_networkPriority":I
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v7, :cond_2

    move v6, v7

    :cond_2
    iget v1, p1, Landroid/os/Message;->arg2:I

    int-to-long v1, v1

    invoke-virtual {v0, v6, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setTrafficControllerForMobile(ZJ)I

    .line 1620
    return v7

    .line 1616
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateRuleForMobileTc(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1617
    return v7

    .line 1608
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1609
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mcheckRouterMTK(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmIsMtkRouter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1610
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mgetRouterModel(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmConnectedApModel(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V

    .line 1611
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "detect again, router is mtk:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmIsMtkRouter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1612
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableWmmer(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1614
    :cond_3
    return v7

    .line 1602
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1603
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmRpsEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1604
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmRpsEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableRps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1606
    :cond_4
    return v7

    .line 1599
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateMobileLatency(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1600
    return v7

    .line 1564
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    if-ne v2, v7, :cond_5

    move v6, v7

    :cond_5
    invoke-static {v0, v1, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdatePowerSaveForUidDataActivityChanged(Lcom/android/server/net/MiuiNetworkPolicyManagerService;IZ)V

    .line 1565
    return v7

    .line 1567
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v0

    .line 1568
    .local v0, "lastTrafficPolicyMode":I
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    .line 1569
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mnetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v3

    .line 1570
    .local v3, "networkPriority":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "networkPriorityMode = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1572
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    if-eq v3, v1, :cond_6

    .line 1573
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableNetworkPriority(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    goto :goto_1

    .line 1575
    :cond_6
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    if-eq v0, v1, :cond_7

    .line 1577
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkBoostService(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1578
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkBoostService(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v4

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v5

    invoke-virtual {v1, v2, v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V

    .line 1581
    :cond_7
    :goto_1
    return v7

    .line 1595
    .end local v0    # "lastTrafficPolicyMode":I
    .end local v3    # "networkPriority":I
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mcalculateBandWidth(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1596
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1597
    return v7

    .line 1589
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLimitEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1590
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0, v7}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateLimit(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1591
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mgetEnableLimitTimeout(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1593
    :cond_8
    return v7

    .line 1583
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLimitEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1584
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateLimit(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1585
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mgetDisableLimitTimeout(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1587
    :cond_9
    return v7

    .line 1533
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1534
    .local v0, "label":Ljava/lang/String;
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 1535
    .local v1, "uid":I
    iget v2, p1, Landroid/os/Message;->arg2:I

    if-ne v2, v7, :cond_a

    move v2, v7

    goto :goto_2

    :cond_a
    move v2, v6

    .line 1536
    .local v2, "isActive":Z
    :goto_2
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmInterfaceName(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1537
    if-eqz v2, :cond_b

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLimitEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1538
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1539
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mgetEnableLimitTimeout(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1540
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v7}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateLimit(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    goto/16 :goto_3

    .line 1541
    :cond_b
    if-nez v2, :cond_e

    .line 1542
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1543
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1544
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLimitEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1545
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateLimit(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    goto :goto_3

    .line 1550
    :cond_c
    const/16 v3, 0x76

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1551
    const/16 v3, 0x8

    if-eqz v2, :cond_d

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmPowerSaveEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1552
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1553
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v3, v1, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v5, 0x1388

    invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_3

    .line 1555
    :cond_d
    if-nez v2, :cond_e

    .line 1556
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1557
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmPowerSaveEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 1558
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdatePowerSaveForUidDataActivityChanged(Lcom/android/server/net/MiuiNetworkPolicyManagerService;IZ)V

    .line 1562
    :cond_e
    :goto_3
    return v7

    .line 1530
    .end local v0    # "label":Ljava/lang/String;
    .end local v1    # "uid":I
    .end local v2    # "isActive":Z
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mremoveUidState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    .line 1531
    goto :goto_4

    .line 1527
    :pswitch_d
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateUidState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;II)V

    .line 1528
    nop

    .line 1641
    :goto_4
    return v6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
