public class com.android.server.net.NetworkPolicyManagerServiceImpl extends com.android.server.net.NetworkPolicyManagerServiceStub {
	 /* .source "NetworkPolicyManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.NetworkManagementServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.net.MiuiNetworkManagementService mMiNMS;
com.android.server.net.MiuiNetworkPolicyManagerService mMiuiNetPolicyManager;
/* # direct methods */
public com.android.server.net.NetworkPolicyManagerServiceImpl ( ) {
	 /* .locals 0 */
	 /* .line 15 */
	 /* invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerServiceStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public void init ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p1, "mContext" # Landroid/content/Context; */
	 /* .line 43 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .make ( p1 );
	 this.mMiuiNetPolicyManager = v0;
	 /* .line 44 */
	 return;
} // .end method
public void initMiuiNMS ( android.content.Context p0 ) {
	 /* .locals 1 */
	 /* .param p1, "mContext" # Landroid/content/Context; */
	 /* .line 22 */
	 com.android.server.net.MiuiNetworkManagementService .Init ( p1 );
	 this.mMiNMS = v0;
	 /* .line 23 */
	 return;
} // .end method
public Boolean miuiNotifyInterfaceClassActivity ( Integer p0, Boolean p1, Long p2, Integer p3, Boolean p4 ) {
	 /* .locals 7 */
	 /* .param p1, "type" # I */
	 /* .param p2, "isActive" # Z */
	 /* .param p3, "tsNanos" # J */
	 /* .param p5, "uid" # I */
	 /* .param p6, "fromRadio" # Z */
	 /* .line 28 */
	 v0 = this.mMiNMS;
	 /* move v1, p1 */
	 /* move v2, p2 */
	 /* move-wide v3, p3 */
	 /* move v5, p5 */
	 /* move v6, p6 */
	 v0 = 	 /* invoke-virtual/range {v0 ..v6}, Lcom/android/server/net/MiuiNetworkManagementService;->miuiNotifyInterfaceClassActivity(IZJIZ)Z */
} // .end method
public void setOemNetd ( android.net.INetd p0 ) {
	 /* .locals 3 */
	 /* .param p1, "mNetdService" # Landroid/net/INetd; */
	 /* .line 34 */
	 try { // :try_start_0
		 v0 = this.mMiNMS;
		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).setOemNetd ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setOemNetd(Landroid/os/IBinder;)V
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 38 */
		 /* .line 35 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 36 */
		 /* .local v0, "e":Landroid/os/RemoteException; */
		 final String v1 = "NetworkManagementServic"; // const-string v1, "NetworkManagementServic"
		 final String v2 = "### setOemNetd failed ###"; // const-string v2, "### setOemNetd failed ###"
		 android.util.Slog .e ( v1,v2 );
		 /* .line 37 */
		 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
		 /* .line 39 */
	 } // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void systemReady ( ) {
/* .locals 1 */
/* .line 48 */
v0 = this.mMiuiNetPolicyManager;
(( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).systemReady ( ); // invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->systemReady()V
/* .line 49 */
return;
} // .end method
