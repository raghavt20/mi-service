.class public Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;
    }
.end annotation


# static fields
.field private static final ACTION_NETWORK_CONDITIONS_MEASURED:Ljava/lang/String; = "android.net.conn.NETWORK_CONDITIONS_MEASURED"

.field private static final ACTION_SLEEP_MODE_CHANGED:Ljava/lang/String; = "com.miui.powerkeeper_sleep_changed"

.field private static final AVOID_CHANNEL_IE:[B

.field private static final BG_MIN_BANDWIDTH:J = 0x186a0L

.field private static final CLOUD_AUTO_FORWARD_ENABLED:Ljava/lang/String; = "cloud_auto_forward_enabled"

.field private static final CLOUD_DNS_HAPPY_EYEBALLS_ENABLED:Ljava/lang/String; = "cloud_dns_happy_eyeballs_priority_enabled"

.field private static final CLOUD_DNS_HAPPY_EYEBALLS_PRIORITY_TIME:Ljava/lang/String; = "cloud_dns_happy_eyeballs_priority_time"

.field private static final CLOUD_LOW_LATENCY_APPLIST_FOR_MOBILE:Ljava/lang/String; = "cloud_block_scan_applist_for_mobile"

.field private static final CLOUD_LOW_LATENCY_WHITELIST:Ljava/lang/String; = "cloud_lowlatency_whitelist"

.field private static final CLOUD_MTK_WIFI_TRAFFIC_PRIORITY_MODE:Ljava/lang/String; = "cloud_mtk_wifi_traffic_priority_mode"

.field private static final CLOUD_MTK_WMMER_ENABLED:Ljava/lang/String; = "cloud_mtk_wmmer_enabled"

.field private static final CLOUD_NETWORK_PRIORITY_ENABLED:Ljava/lang/String; = "cloud_network_priority_enabled"

.field private static final CLOUD_NETWORK_TRUST_MIUI_ADD_DNS:Ljava/lang/String; = "cloud_trust_miui_add_dns"

.field private static final CLOUD_P2PHC_ENABLED:Ljava/lang/String; = "cloud_p2phc_enabled"

.field private static final CLOUD_RESTRICT_WIFI_POWERSAVE_APPLIST:Ljava/lang/String; = "cloud_block_scan_applist"

.field public static final CLOUD_SLEEPMODE_NETWORKPOLICY_ENABLED:Ljava/lang/String; = "cloud_sleepmode_networkpolicy_enabled"

.field private static final CLOUD_SMARTDNS_ENABLED:Ljava/lang/String; = "cloud_smartdns_enabled"

.field private static final CLOUD_WMMER_ENABLED:Ljava/lang/String; = "cloud_wmmer_enabled"

.field private static final CLOUD_WMMER_ROUTER_WHITELIST:Ljava/lang/String; = "cloud_wmmer_router_whitelist"

.field private static final DEBUG:Z = true

.field private static final DISABLE_LIMIT_TIMEOUT_LEVEL_1:I = 0x3a98

.field private static final DISABLE_LIMIT_TIMEOUT_LEVEL_2:I = 0x1d4c

.field private static final DISABLE_LIMIT_TIMEOUT_LEVEL_3:I = 0x1388

.field private static final DISABLE_POWER_SAVE_TIMEOUT:I = 0x1388

.field public static final EARTHQUAKE_WARNING_FLAG:Ljava/lang/String; = "key_open_earthquake_warning"

.field private static final EARTH_QUACK_ON:I = 0x1

.field private static final EID_VSA:I = 0xdd

.field private static final ENABLE_LIMIT_TIMEOUT_LEVEL_1:I = 0x3a98

.field private static final ENABLE_LIMIT_TIMEOUT_LEVEL_2:I = 0x57e4

.field private static final ENABLE_LIMIT_TIMEOUT_LEVEL_3:I = 0x61a8

.field private static final EXTRA_IS_CAPTIVE_PORTAL:Ljava/lang/String; = "extra_is_captive_portal"

.field private static final EXTRA_STATE:Ljava/lang/String; = "state"

.field private static final FG_MAX_BANDWIDTH:J = 0xf4240L

.field private static final HISTORY_BANDWIDTH_MIN:J = 0x30d40L

.field private static final HISTORY_BANDWIDTH_SIZE:I = 0xa

.field private static final IS_QCOM:Z

.field private static final LATENCY_ACTION_CHANGE_LEVEL:Ljava/lang/String; = "com.android.phone.intent.action.CHANGE_LEVEL"

.field private static final LATENCY_DEFAULT:I = -0x1

.field private static final LATENCY_KEY_LEVEL_DL:Ljava/lang/String; = "Level_DL"

.field private static final LATENCY_KEY_LEVEL_UL:Ljava/lang/String; = "Level_UL"

.field private static final LATENCY_KEY_RAT_TYPE:Ljava/lang/String; = "Rat_type"

.field private static final LATENCY_OFF:I = 0x0

.field private static final LATENCY_ON:I = 0x1

.field private static final LATENCY_VALUE_L1:J = 0x1L

.field private static final LATENCY_VALUE_L2:J = 0x2L

.field private static final LATENCY_VALUE_L3:J = 0x3L

.field private static final LATENCY_VALUE_L4:J = 0x4L

.field private static final LATENCY_VALUE_WLAN:J = 0x1L

.field private static final LATENCY_VALUE_WWAN:J = 0x0L

.field private static final LOCAL_NETWORK_PRIORITY_WHITELIST:[Ljava/lang/String;

.field private static final MAX_ROUTER_DETECT_TIME:J = 0x2dc6c0L

.field private static final MIN_THIRDAPP_UID:I = 0x2710

.field private static final MODEL_NAME:[B

.field private static final MSG_BANDWIDTH_POLL:I = 0x6

.field private static final MSG_CHECK_ROUTER_MTK:I = 0xb

.field private static final MSG_DISABLE_LIMIT_TIMEOUT:I = 0x5

.field private static final MSG_DISABLE_POWER_SAVE_TIMEOUT:I = 0x8

.field private static final MSG_ENABLE_LIMIT_TIMEOUT:I = 0x4

.field private static final MSG_MOBILE_LATENCY_CHANGED:I = 0x9

.field public static final MSG_SET_LIMIT_FOR_MOBILE:I = 0xd

.field private static final MSG_SET_RPS_STATS:I = 0xa

.field public static final MSG_SET_THERMAL_POLICY:I = 0xe

.field private static final MSG_SET_TRAFFIC_POLICY:I = 0x7

.field private static final MSG_UID_DATA_ACTIVITY_CHANGED:I = 0x3

.field public static final MSG_UID_STATE_CHANGED:I = 0x1

.field public static final MSG_UID_STATE_GONED:I = 0x2

.field public static final MSG_UPDATE_RULE_FOR_MOBILE_TRAFFIC:I = 0xc

.field private static final MTK_OUI1:[B

.field private static final MTK_OUI2:[B

.field private static final MTK_OUI3:[B

.field private static final MTK_OUI4:[B

.field private static final NETWORK_PRIORITY_MODE_CLOSED:I = 0xff

.field private static final NETWORK_PRIORITY_MODE_FAST:I = 0x2

.field private static final NETWORK_PRIORITY_MODE_NORMAL:I = 0x1

.field private static final NETWORK_PRIORITY_MODE_WMM:I = 0x0

.field private static final NETWORK_PRIORITY_WHITELIST:Ljava/lang/String; = "cloud_network_priority_whitelist"

.field private static final NOTIFACATION_RECEIVER_PACKAGE:Ljava/lang/String; = "com.android.phone"

.field private static final PERSIST_DEVICE_CONFIG_NETD_ENABLED_VALUE:Ljava/lang/String; = "persist.device_config.netd_native.happy_eyeballs_enable"

.field private static final PERSIST_DEVICE_CONFIG_NETD_PRIORITY_TIME:Ljava/lang/String; = "persist.device_config.netd_native.happy_eyeballs_master_server_priority_time"

.field private static final PERSIST_DEVICE_CONFIG_NETD_SMARTDNS_ENABLED_VALUE:Ljava/lang/String; = "persist.device_config.netd_native.smartdns_enable"

.field private static final PERSIST_DEVICE_CONFIG_NETD_VALUE:Ljava/lang/String; = "persist.device_config.netd_native.trust_miui_add_dns"

.field private static final PERSIST_WPA_SUPPLICANT_P2PHC_ENABLE:Ljava/lang/String; = "vendor.miui.wifi.p2phc"

.field private static final POLL_BANDWIDTH_INTERVAL_SECS:I = 0x3

.field private static final POWER_SAVE_IDLETIMER_LABEL:I = 0x76

.field private static final POWER_SAVE_IDLETIMER_TIMEOUT:I = 0x2

.field private static final SLEEPMODE_TAG:Ljava/lang/String; = "MiuiNetworkPolicySleepMode"

.field private static final SLEEP_MODE_ON:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiuiNetworkPolicy"

.field private static final THERMAL_FAST_LEVEL_1:I = 0x1

.field private static final THERMAL_FAST_LEVEL_2:I = 0x2

.field private static final THERMAL_FAST_LEVEL_3:I = 0x3

.field private static final THERMAL_FIRST_MODE:I = 0x0

.field private static final THERMAL_LAST_MODE:I = 0x3

.field private static final THERMAL_NORMAL_MODE:I = 0x0

.field private static final THERMAL_STATE_FILE:Ljava/lang/String; = "/sys/class/thermal/thermal_message/wifi_limit"

.field private static final WMM_AC_BE:I = 0x0

.field private static final WMM_AC_VI:I = 0x1

.field private static final WMM_AC_VO:I = 0x2

.field private static final WPA2_AKM_PSK_IE:[B

.field private static final WPA_AKM_EAP_IE:[B

.field private static final WPA_AKM_PSK_IE:[B

.field private static mMobileLatencyState:I

.field private static sSelf:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# instance fields
.field private ROUTER_DETECT_TIME:J

.field private cm:Landroid/net/ConnectivityManager;

.field private mAppBuckets:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

.field private mCloudWmmerEnable:Z

.field private mConnectState:Lcom/android/internal/telephony/PhoneConstants$DataState;

.field private mConnectedApModel:Ljava/lang/String;

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mCurrentNetwork:Landroid/net/Network;

.field private mDefaultInputMethod:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private mHasAutoForward:Z

.field private mHasListenWifiNetwork:Z

.field private mHistoryBandWidth:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mInterfaceName:Ljava/lang/String;

.field private mIsCaptivePortal:Z

.field private mIsMtkRouter:Z

.field private mLastIpAddress:Ljava/lang/String;

.field private mLastNetId:I

.field private mLastRxBytes:J

.field private mLimitEnabled:Z

.field private mLowLatencyApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLowLatencyAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

.field private mMobileLowLatencyApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mMobileLowLatencyAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mMobileNwReceiver:Landroid/content/BroadcastReceiver;

.field private mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

.field private mNeedRestrictPowerSaveApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedRestrictPowerSaveAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkBoostService:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

.field private mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

.field private mNetworkPriorityMode:I

.field private final mPackageReceiver:Landroid/content/BroadcastReceiver;

.field private mPowerSaveEnabled:Z

.field private mQosUtils:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

.field private mRpsEnabled:Z

.field private mSlaveWifiNetwork:Landroid/net/Network;

.field private mSleepModeEnable:Z

.field private mSleepModeEnter:Z

.field private final mSleepModeReceiver:Landroid/content/BroadcastReceiver;

.field private mSleepModeWhitelistUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSupport:Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

.field private mThermalForceMode:I

.field private mThermalStateListener:Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;

.field private mTrafficPolicyMode:I

.field private mUidDataActivityObserver:Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

.field final mUidState:Landroid/util/SparseIntArray;

.field private mUnRestrictApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUnRestrictAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserManager:Landroid/os/UserManager;

.field private mVPNNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mVPNNetworkRequest:Landroid/net/NetworkRequest;

.field private mVpnNetwork:Landroid/net/Network;

.field private mWifiConnected:Z

.field private mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mWifiNetworkRequest:Landroid/net/NetworkRequest;

.field private final mWifiStateReceiver:Landroid/content/BroadcastReceiver;

.field private mWmmerEnable:Z

.field private mWmmerRouterWhitelist:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppBuckets(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mAppBuckets:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCloudWmmerEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCloudWmmerEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmConnectState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/internal/telephony/PhoneConstants$DataState;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmConnectivityManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/ConnectivityManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/Network;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCurrentNetwork:Landroid/net/Network;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mDefaultInputMethod:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHasAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmInterfaceName(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mInterfaceName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsCaptivePortal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsCaptivePortal:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsMtkRouter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsMtkRouter:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLimitEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLowLatencyApps:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLowLatencyAppsPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiWifiManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/wifi/MiuiWifiManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMobileLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLowLatencyApps:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMobileLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLowLatencyAppsPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMobileTcUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNeedRestrictPowerSaveApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNeedRestrictPowerSaveApps:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNeedRestrictPowerSaveAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNeedRestrictPowerSaveAppsPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkBoostService(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkBoostService:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkManagementService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPowerSaveEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmQosUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mQosUtils:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRpsEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/Network;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSlaveWifiNetwork:Landroid/net/Network;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSleepModeEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnter:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSleepModeWhitelistUids(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupport(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSupport:Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I
    .locals 0

    iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUnRestrictApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUnRestrictAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictAppsPN:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/UserManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUserManager:Landroid/os/UserManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVpnNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/Network;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVpnNetwork:Landroid/net/Network;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWmmerEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmCloudWmmerEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCloudWmmerEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmConnectState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Lcom/android/internal/telephony/PhoneConstants$DataState;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmConnectedApModel(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectedApModel:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mDefaultInputMethod:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHasAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmInterfaceName(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mInterfaceName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsCaptivePortal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsCaptivePortal:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsMtkRouter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsMtkRouter:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLowLatencyAppsPN:Ljava/util/Set;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMiuiWifiManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/wifi/MiuiWifiManager;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMobileLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLowLatencyAppsPN:Ljava/util/Set;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmNeedRestrictPowerSaveAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNeedRestrictPowerSaveAppsPN:Ljava/util/Set;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRpsEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSlaveWifiNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSlaveWifiNetwork:Landroid/net/Network;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSleepModeEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnter:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmThermalForceMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTrafficPolicyMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUnRestrictAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictAppsPN:Ljava/util/Set;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVpnNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVpnNetwork:Landroid/net/Network;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcalculateBandWidth(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->calculateBandWidth()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckEnableAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/NetworkCapabilities;Landroid/net/Network;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkEnableAutoForward(Landroid/net/NetworkCapabilities;Landroid/net/Network;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckRouterMTK(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkRouterMTK()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mcheckWifiLimit(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkWifiLimit(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mclearSleepModeWhitelistUidRules(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->clearSleepModeWhitelistUidRules()V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableNetworkPriority(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableNetworkPriority(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$menablePowerSave(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableRps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableRps(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableSleepModeChain(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableSleepModeChain(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableWmmer(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableWmmer()V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableWmmer(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableWmmer(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getDefaultInputMethod()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetDisableLimitTimeout(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getDisableLimitTimeout()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetEnableLimitTimeout(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getEnableLimitTimeout()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/content/Context;)Ljava/util/Set;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getLowLatencyApps(Landroid/content/Context;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetMobileLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/content/Context;)Ljava/util/Set;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getMobileLowLatencyApps(Landroid/content/Context;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetNeedRestrictPowerSaveApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/content/Context;)Ljava/util/Set;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getNeedRestrictPowerSaveApps(Landroid/content/Context;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetRouterModel(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getRouterModel()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetUnRestrictedApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/content/Context;)Ljava/util/Set;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getUnRestrictedApps(Landroid/content/Context;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mhappyEyeballsEnableCloudControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->happyEyeballsEnableCloudControl()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhappyEyeballsMasterServerPriorityTimeCloudControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->happyEyeballsMasterServerPriorityTimeCloudControl()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misLimitterEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnetworkPriorityCloudControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->networkPriorityCloudControl()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->networkPriorityMode()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$monVpnNetworkChanged(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->onVpnNetworkChanged(Landroid/net/Network;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterSleepModeReceiver(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSleepModeReceiver()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterWifiNetworkListener(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWifiNetworkListener()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveUidState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->removeUidState(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSleepModeWhitelistUidRules(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setSleepModeWhitelistUidRules()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msleepModeEnableControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->sleepModeEnableControl()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$munregisterSleepModeReceiver(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->unregisterSleepModeReceiver()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateLimit(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateLimit(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateMobileLatency(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatency()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePowerSaveForUidDataActivityChanged(Lcom/android/server/net/MiuiNetworkPolicyManagerService;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updatePowerSaveForUidDataActivityChanged(IZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateRuleForMobileTc(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleForMobileTc()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateRuleGlobal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleGlobal()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateUidState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateUidState(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmMobileLatencyState()I
    .locals 1

    sget v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLatencyState:I

    return v0
.end method

.method static bridge synthetic -$$Nest$smisMobileLatencyAllowed()Z
    .locals 1

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyAllowed()Z

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 7

    .line 134
    const-string v0, "com.tencent.mm"

    const-string v1, "com.tencent.mobileqq"

    const-string v2, "com.xiaomi.xmsf"

    const-string v3, "com.google.android.gms"

    const-string v4, "com.google.android.dialer"

    const-string v5, "com.whatsapp"

    const-string v6, "com.miui.vpnsdkmanager"

    filled-new-array/range {v0 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->LOCAL_NETWORK_PRIORITY_WHITELIST:[Ljava/lang/String;

    .line 195
    const/4 v0, 0x4

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->AVOID_CHANNEL_IE:[B

    .line 196
    new-array v1, v0, [B

    fill-array-data v1, :array_1

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->WPA_AKM_EAP_IE:[B

    .line 197
    new-array v1, v0, [B

    fill-array-data v1, :array_2

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->WPA_AKM_PSK_IE:[B

    .line 198
    new-array v0, v0, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->WPA2_AKM_PSK_IE:[B

    .line 199
    const/4 v0, 0x3

    new-array v1, v0, [B

    fill-array-data v1, :array_4

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI1:[B

    .line 200
    new-array v1, v0, [B

    fill-array-data v1, :array_5

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI2:[B

    .line 201
    new-array v1, v0, [B

    fill-array-data v1, :array_6

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI3:[B

    .line 202
    new-array v0, v0, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI4:[B

    .line 203
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MODEL_NAME:[B

    .line 269
    const/4 v0, -0x1

    sput v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLatencyState:I

    .line 271
    const-string/jumbo v0, "vendor"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "qcom"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->IS_QCOM:Z

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        -0x60t
        -0x3at
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x50t
        -0xet
        0x1t
    .end array-data

    :array_2
    .array-data 1
        0x0t
        0x50t
        -0xet
        0x2t
    .end array-data

    :array_3
    .array-data 1
        0x0t
        0x50t
        -0xet
        0x4t
    .end array-data

    :array_4
    .array-data 1
        0x0t
        0xct
        -0x19t
    .end array-data

    :array_5
    .array-data 1
        0x0t
        0xat
        0x0t
    .end array-data

    :array_6
    .array-data 1
        0x0t
        0xct
        0x43t
    .end array-data

    :array_7
    .array-data 1
        0x0t
        0x17t
        -0x5bt
    .end array-data

    :array_8
    .array-data 1
        0x10t
        0x23t
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerRouterWhitelist:Ljava/util/HashSet;

    .line 204
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J

    .line 234
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mDefaultInputMethod:Ljava/lang/String;

    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    .line 239
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z

    .line 242
    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    .line 270
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    .line 280
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkBoostService:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    .line 289
    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    .line 290
    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z

    .line 1143
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;

    invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    .line 1522
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;

    invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandlerCallback:Landroid/os/Handler$Callback;

    .line 1645
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$25;

    invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$25;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidDataActivityObserver:Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

    .line 1656
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;

    invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    .line 1769
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;

    invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileNwReceiver:Landroid/content/BroadcastReceiver;

    .line 2048
    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnter:Z

    .line 2049
    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnable:Z

    .line 2097
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;

    invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    .line 2240
    iput v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    .line 2241
    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalStateListener:Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;

    .line 335
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    .line 336
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "MiuiNetworkPolicy"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 337
    .local v2, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 338
    new-instance v4, Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandlerCallback:Landroid/os/Handler$Callback;

    invoke-direct {v4, v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    .line 339
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    .line 340
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLowLatencyApps:Ljava/util/Set;

    .line 341
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNeedRestrictPowerSaveApps:Ljava/util/Set;

    .line 342
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLowLatencyApps:Ljava/util/Set;

    .line 343
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    .line 344
    const/16 v5, 0xff

    iput v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    .line 345
    iput v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    .line 346
    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    invoke-direct {v1, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSupport:Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    .line 347
    new-instance v1, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v1}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 348
    invoke-virtual {v1, v0}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 349
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 350
    const/16 v5, 0xf

    invoke-virtual {v0, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiNetworkRequest:Landroid/net/NetworkRequest;

    .line 351
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 352
    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    .line 353
    invoke-virtual {v0, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVPNNetworkRequest:Landroid/net/NetworkRequest;

    .line 354
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;

    invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 405
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;

    invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVPNNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 418
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isQosFeatureAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-direct {v0, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mQosUtils:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    .line 421
    :cond_0
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-direct {v0, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mAppBuckets:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    .line 422
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileTcFeatureAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 423
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-direct {v0, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    .line 428
    :cond_1
    :try_start_0
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;

    const-string v1, "/sys/class/thermal/thermal_message/wifi_limit"

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalStateListener:Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;

    .line 429
    invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;->startWatching()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    goto :goto_0

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Thremal state listener init failed:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private addHistoryBandWidth(J)V
    .locals 2
    .param p1, "bwBps"    # J

    .line 710
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 711
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 714
    return-void
.end method

.method private bytesToString([B)Ljava/lang/String;
    .locals 1
    .param p1, "bytes"    # [B

    .line 2031
    if-nez p1, :cond_0

    .line 2032
    const-string v0, ""

    return-object v0

    .line 2034
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method private calculateBandWidth()V
    .locals 10

    .line 689
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mInterfaceName:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/TrafficStats;->getRxBytes(Ljava/lang/String;)J

    move-result-wide v0

    .line 690
    .local v0, "rxBytes":J
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    const-string v5, "MiuiNetworkPolicy"

    if-ltz v4, :cond_0

    iget-wide v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J

    cmp-long v4, v6, v0

    if-lez v4, :cond_1

    .line 691
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rxByte: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", mLastRxBytes: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    iput-wide v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J

    .line 694
    :cond_1
    iget-wide v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J

    cmp-long v4, v6, v2

    if-nez v4, :cond_2

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    .line 695
    iput-wide v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J

    .line 696
    return-void

    .line 698
    :cond_2
    sub-long v2, v0, v6

    const-wide/16 v6, 0x3

    div-long/2addr v2, v6

    .line 699
    .local v2, "bwBps":J
    const-wide/32 v6, 0x30d40

    cmp-long v4, v2, v6

    if-ltz v4, :cond_3

    .line 700
    invoke-direct {p0, v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->addHistoryBandWidth(J)V

    .line 703
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bandwidth: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/16 v6, 0x3e8

    div-long v8, v2, v6

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " KB/s, Max bandwidth: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    .line 704
    invoke-static {v8}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    div-long/2addr v8, v6

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " KB/s"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 703
    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iput-wide v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J

    .line 707
    return-void
.end method

.method private checkEnableAutoForward(Landroid/net/NetworkCapabilities;Landroid/net/Network;)V
    .locals 6
    .param p1, "networkCapabilities"    # Landroid/net/NetworkCapabilities;
    .param p2, "network"    # Landroid/net/Network;

    .line 462
    nop

    .line 463
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 462
    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "persist.sys.miui_optimization"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/2addr v0, v1

    .line 464
    .local v0, "isCtsMode":Z
    if-eqz p1, :cond_4

    if-nez v0, :cond_4

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSlaveWifiNetwork:Landroid/net/Network;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVpnNetwork:Landroid/net/Network;

    if-eqz v2, :cond_0

    goto :goto_2

    .line 468
    :cond_0
    iput-object p2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCurrentNetwork:Landroid/net/Network;

    .line 469
    const/16 v2, 0xc

    invoke-virtual {p1, v2}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 470
    const/16 v2, 0x10

    invoke-virtual {p1, v2}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 471
    .local v2, "isValidated":Z
    :goto_0
    const-string v4, "MiuiNetworkPolicy"

    if-nez v2, :cond_2

    iget-boolean v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    if-nez v5, :cond_2

    .line 472
    const-string v3, "add auto forward rule "

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V

    goto :goto_1

    .line 474
    :cond_2
    if-eqz v2, :cond_3

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    if-eqz v1, :cond_3

    .line 475
    const-string/jumbo v1, "validation pass,  remove auto forward rule"

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V

    .line 478
    :cond_3
    :goto_1
    return-void

    .line 466
    .end local v2    # "isValidated":Z
    :cond_4
    :goto_2
    return-void
.end method

.method private checkIEMTK(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult$InformationElement;",
            ">;)Z"
        }
    .end annotation

    .line 1203
    .local p1, "infoElements":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult$InformationElement;>;"
    const/4 v0, 0x1

    if-eqz p1, :cond_6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 1207
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/ScanResult$InformationElement;

    .line 1210
    .local v2, "ie":Landroid/net/wifi/ScanResult$InformationElement;
    invoke-virtual {v2}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I

    move-result v4

    const/16 v5, 0xdd

    if-eq v4, v5, :cond_1

    goto :goto_0

    .line 1212
    :cond_1
    const/4 v4, 0x3

    new-array v5, v4, [B

    .line 1214
    .local v5, "oui":[B
    :try_start_0
    invoke-virtual {v2}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6, v5, v3, v4}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1218
    nop

    .line 1220
    sget-object v4, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI1:[B

    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI2:[B

    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI3:[B

    .line 1221
    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MTK_OUI4:[B

    invoke-static {v5, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    move v3, v0

    .line 1222
    .local v3, "isMtk":Z
    :cond_3
    if-eqz v3, :cond_4

    .line 1223
    return v0

    .line 1225
    .end local v2    # "ie":Landroid/net/wifi/ScanResult$InformationElement;
    .end local v3    # "isMtk":Z
    .end local v5    # "oui":[B
    :cond_4
    goto :goto_0

    .line 1215
    .restart local v2    # "ie":Landroid/net/wifi/ScanResult$InformationElement;
    .restart local v5    # "oui":[B
    :catch_0
    move-exception v0

    .line 1216
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiNetworkPolicy"

    const-string v4, "Failed to set network priority support config"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1217
    return v3

    .line 1227
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "ie":Landroid/net/wifi/ScanResult$InformationElement;
    .end local v5    # "oui":[B
    :cond_5
    return v3

    .line 1204
    :cond_6
    :goto_1
    return v0
.end method

.method private checkRouterMTK()Z
    .locals 9

    .line 1180
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1181
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 1182
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    .line 1183
    .local v2, "bssid":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    .line 1186
    .local v3, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    const/4 v4, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    iget-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J

    const-wide/32 v7, 0x2dc6c0

    cmp-long v7, v5, v7

    if-gez v7, :cond_1

    .line 1187
    iget-object v7, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xb

    invoke-virtual {v7, v8, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1188
    iget-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J

    const-wide/16 v7, 0x2

    mul-long/2addr v5, v7

    iput-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J

    .line 1189
    return v4

    .line 1191
    :cond_1
    const-wide/16 v5, 0x2710

    iput-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J

    .line 1193
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/ScanResult;

    .line 1194
    .local v6, "scanResult":Landroid/net/wifi/ScanResult;
    iget-object v7, v6, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v2, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1195
    invoke-virtual {v6}, Landroid/net/wifi/ScanResult;->getInformationElements()Ljava/util/List;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkIEMTK(Ljava/util/List;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1196
    return v4

    .line 1198
    .end local v6    # "scanResult":Landroid/net/wifi/ScanResult;
    :cond_2
    goto :goto_0

    .line 1199
    :cond_3
    const/4 v4, 0x0

    return v4
.end method

.method private checkWifiLimit(Ljava/lang/String;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .line 2359
    const-string v0, "MiuiNetworkPolicy"

    const/4 v1, 0x0

    .line 2361
    .local v1, "reader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 2362
    .local v2, "line":Ljava/lang/String;
    :try_start_0
    const-string v3, "/sys/class/thermal/thermal_message/wifi_limit"

    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->readLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_1

    .line 2363
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 2364
    .local v3, "str_temp":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 2365
    .local v4, "wifi_limit":I
    move v5, v4

    .line 2366
    .local v5, "thermalMode":I
    const/4 v6, 0x0

    .line 2368
    .local v6, "ret":Z
    const/16 v7, 0xa

    if-le v4, v7, :cond_0

    .line 2369
    invoke-direct {p0, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getThermalWifiLimitMode(I)I

    move-result v7

    move v5, v7

    .line 2372
    :cond_0
    invoke-direct {p0, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setThermalPolicy(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2373
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setThermalPolicy failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " path: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2379
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "str_temp":Ljava/lang/String;
    .end local v4    # "wifi_limit":I
    .end local v5    # "thermalMode":I
    .end local v6    # "ret":Z
    :cond_1
    goto :goto_0

    .line 2377
    :catch_0
    move-exception v2

    .line 2378
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "checkWifiLimit:"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2380
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private clearSleepModeWhitelistUidRules()V
    .locals 3

    .line 2194
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2195
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2196
    .local v1, "uid":I
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateSleepModeWhitelistUidRules(IZ)V

    .line 2197
    .end local v1    # "uid":I
    goto :goto_0

    .line 2198
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2200
    :cond_1
    return-void
.end method

.method private enableAutoForward(Z)V
    .locals 10
    .param p1, "enable"    # Z

    .line 306
    if-nez p1, :cond_0

    .line 307
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastIpAddress:Ljava/lang/String;

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastNetId:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->enableAutoForward(Ljava/lang/String;IZ)I

    .line 308
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    .line 309
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCurrentNetwork:Landroid/net/Network;

    invoke-virtual {v0}, Landroid/net/Network;->getNetId()I

    move-result v0

    .line 312
    .local v0, "netId":I
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCurrentNetwork:Landroid/net/Network;

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v1

    .line 313
    .local v1, "linkProperties":Landroid/net/LinkProperties;
    const-string v2, "MiuiNetworkPolicy"

    if-nez v1, :cond_1

    .line 314
    const-string v3, "link properties is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    return-void

    .line 317
    :cond_1
    invoke-virtual {v1}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v3

    .line 318
    .local v3, "curAddr":Ljava/util/List;, "Ljava/util/List<Landroid/net/LinkAddress;>;"
    invoke-virtual {v1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v4

    .line 320
    .local v4, "iface":Ljava/lang/String;
    const-string/jumbo v5, "wlan0"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 321
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/LinkAddress;

    .line 322
    .local v6, "address":Landroid/net/LinkAddress;
    invoke-virtual {v6}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v7

    instance-of v7, v7, Ljava/net/Inet4Address;

    if-eqz v7, :cond_2

    .line 323
    invoke-virtual {v6}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    .line 324
    .local v7, "inet4Addr":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "enable autoforward, current address: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", netid = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    const/4 v9, 0x1

    invoke-virtual {v8, v7, v0, v9}, Lcom/android/server/net/MiuiNetworkManagementService;->enableAutoForward(Ljava/lang/String;IZ)I

    .line 326
    iput-object v7, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastIpAddress:Ljava/lang/String;

    .line 327
    iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastNetId:I

    .line 328
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    .line 330
    .end local v6    # "address":Landroid/net/LinkAddress;
    .end local v7    # "inet4Addr":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 332
    :cond_3
    return-void
.end method

.method private enableBandwidthPoll(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 717
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 718
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 719
    if-eqz p1, :cond_0

    .line 720
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 721
    const-wide/32 v0, 0x30d40

    invoke-direct {p0, v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->addHistoryBandWidth(J)V

    .line 723
    :cond_0
    return-void
.end method

.method private enableMobileLowLatency(Z)V
    .locals 9
    .param p1, "enable"    # Z

    .line 1804
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableMobileLowLatency enable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mMobileLatencyState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLatencyState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1806
    move v0, p1

    .line 1807
    .local v0, "mobileState":I
    sget v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLatencyState:I

    if-eq v1, v0, :cond_2

    .line 1808
    sput v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLatencyState:I

    .line 1809
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1810
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.android.phone.intent.action.CHANGE_LEVEL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1811
    const-string v2, "com.android.phone"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1812
    const-string v2, "Rat_type"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1813
    const-wide/16 v2, 0x4

    const-wide/16 v4, 0x1

    if-eqz p1, :cond_0

    move-wide v6, v2

    goto :goto_0

    :cond_0
    move-wide v6, v4

    :goto_0
    const-string v8, "Level_UL"

    invoke-virtual {v1, v8, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1814
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-wide v2, v4

    :goto_1
    const-string v4, "Level_DL"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1815
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1817
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    return-void
.end method

.method private enableNetworkPriority(I)V
    .locals 7
    .param p1, "mode"    # I

    .line 849
    const/4 v0, 0x0

    .line 850
    .local v0, "isNeedUpdate":Z
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled()Z

    move-result v1

    .line 851
    .local v1, "wasLimitterEnabled":Z
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled(I)Z

    move-result v2

    .line 852
    .local v2, "isLimitterEnabled":Z
    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    .line 854
    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne p1, v4, :cond_0

    .line 855
    invoke-direct {p0, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableBandwidthPoll(Z)V

    goto :goto_0

    .line 857
    :cond_0
    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableBandwidthPoll(Z)V

    .line 859
    :goto_0
    if-eqz v1, :cond_1

    if-nez v2, :cond_1

    .line 860
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v3, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 861
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v4, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z

    .line 862
    return-void

    .line 863
    :cond_1
    if-nez v1, :cond_2

    if-eqz v2, :cond_2

    .line 864
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z

    .line 865
    const/4 v0, 0x1

    .line 866
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x5

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 867
    :cond_2
    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z

    if-eqz v3, :cond_3

    .line 868
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 869
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 871
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 872
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleGlobal()V

    .line 875
    :cond_4
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkBoostService:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    if-eqz v3, :cond_5

    .line 876
    iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    iget v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    iget v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V

    .line 878
    :cond_5
    return-void
.end method

.method private enablePowerSave(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .line 670
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enable ps, mPS = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    if-eq v0, p1, :cond_0

    .line 672
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    .line 673
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSupport:Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->enablePowerSave(Z)V

    .line 675
    :cond_0
    return-void
.end method

.method private enableRps(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 1842
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSupport:Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->updateIface(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1843
    .local v0, "iface":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableRps interface = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1844
    if-eqz v0, :cond_0

    .line 1845
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mInterfaceName:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableRps(Ljava/lang/String;Z)Z

    .line 1847
    :cond_0
    return-void
.end method

.method private enableSleepModeChain(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .line 2218
    const-string v0, "MiuiNetworkPolicySleepMode"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->cm:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_0

    .line 2219
    const-string v1, "android.net.ConnectivityManager"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 2220
    .local v1, "clazz":Ljava/lang/Class;
    const-string v2, "enableSleepModeChain"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 2221
    .local v2, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2222
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->cm:Landroid/net/ConnectivityManager;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v6

    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2223
    .local v3, "result":Ljava/lang/Object;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableSleepModeChain enable:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " result:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2227
    .end local v1    # "clazz":Ljava/lang/Class;
    .end local v2    # "method":Ljava/lang/reflect/Method;
    .end local v3    # "result":Ljava/lang/Object;
    :cond_0
    goto :goto_0

    .line 2225
    :catch_0
    move-exception v1

    .line 2226
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "enableSleepModeChain error!"

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2228
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private enableWmmer()V
    .locals 1

    .line 881
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isWmmerEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableWmmer(Z)V

    .line 882
    return-void
.end method

.method private enableWmmer(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 885
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z

    if-eq v0, p1, :cond_1

    .line 886
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableWmmer(Z)Z

    .line 887
    if-eqz p1, :cond_0

    .line 888
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleGlobal()V

    .line 890
    :cond_0
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z

    .line 892
    :cond_1
    return-void
.end method

.method public static get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .locals 2

    .line 298
    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->sSelf:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    if-eqz v0, :cond_0

    .line 299
    return-object v0

    .line 301
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MiuiNetworkPolicyManagerService has not been initialized "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getDefaultInputMethod()Ljava/lang/String;
    .locals 4

    .line 1374
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1376
    .local v0, "inputMethod":Ljava/lang/String;
    const-string v1, ""

    if-eqz v0, :cond_1

    .line 1377
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    .line 1378
    .local v2, "inputMethodCptName":Landroid/content/ComponentName;
    if-eqz v2, :cond_1

    .line 1379
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 1380
    .local v3, "inputMethodPkgName":Ljava/lang/String;
    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    return-object v1

    .line 1383
    .end local v2    # "inputMethodCptName":Landroid/content/ComponentName;
    .end local v3    # "inputMethodPkgName":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method private getDisableLimitTimeout()I
    .locals 3

    .line 2273
    iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2274
    const/16 v0, 0x1388

    return v0

    .line 2278
    :cond_0
    const/16 v0, 0x1388

    .line 2279
    .local v0, "timeout":I
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    packed-switch v1, :pswitch_data_0

    .line 2293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDisableLimitTimeout when thermal mode is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2289
    :pswitch_0
    const/16 v0, 0x1388

    .line 2290
    goto :goto_0

    .line 2285
    :pswitch_1
    const/16 v0, 0x1d4c

    .line 2286
    goto :goto_0

    .line 2281
    :pswitch_2
    const/16 v0, 0x3a98

    .line 2282
    nop

    .line 2296
    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getEnableLimitTimeout()I
    .locals 3

    .line 2245
    iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2246
    const/16 v0, 0x61a8

    return v0

    .line 2250
    :cond_0
    const/16 v0, 0x61a8

    .line 2251
    .local v0, "timeout":I
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    packed-switch v1, :pswitch_data_0

    .line 2265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getEnableLimitTimeout when thermal mode is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2261
    :pswitch_0
    const/16 v0, 0x61a8

    .line 2262
    goto :goto_0

    .line 2257
    :pswitch_1
    const/16 v0, 0x57e4

    .line 2258
    goto :goto_0

    .line 2253
    :pswitch_2
    const/16 v0, 0x3a98

    .line 2254
    nop

    .line 2268
    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getLowLatencyApps(Landroid/content/Context;)Ljava/util/Set;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1424
    nop

    .line 1425
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1424
    const-string v1, "cloud_lowlatency_whitelist"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1426
    .local v0, "whiteString":Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1427
    .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1428
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1429
    .local v2, "packages":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1430
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 1431
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1430
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1435
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_0
    return-object v1
.end method

.method private getMobileLowLatencyApps(Landroid/content/Context;)Ljava/util/Set;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1753
    nop

    .line 1754
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1753
    const-string v1, "cloud_block_scan_applist_for_mobile"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1755
    .local v0, "appString":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMobileLowLatencyApps appString="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1756
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1757
    .local v1, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1758
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1759
    .local v2, "packages":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1760
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 1761
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1760
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1765
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_0
    return-object v1
.end method

.method private getNeedRestrictPowerSaveApps(Landroid/content/Context;)Ljava/util/Set;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1323
    nop

    .line 1324
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1323
    const-string v1, "cloud_block_scan_applist"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1325
    .local v0, "appString":Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1326
    .local v1, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1327
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1328
    .local v2, "packages":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1329
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 1330
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1329
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1334
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_0
    return-object v1
.end method

.method private getNetworkPriorityCloudValue()Ljava/lang/String;
    .locals 3

    .line 754
    const-string/jumbo v0, "vendor"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mediatek"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, -0x2

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "cloud_mtk_wifi_traffic_priority_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .local v0, "cvalue":Ljava/lang/String;
    goto :goto_0

    .line 758
    .end local v0    # "cvalue":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "cloud_network_priority_enabled"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 761
    .restart local v0    # "cvalue":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_1

    .line 762
    const-string v0, ""

    .line 764
    :cond_1
    return-object v0
.end method

.method private getRouterModel()Ljava/lang/String;
    .locals 11

    .line 1975
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1976
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 1977
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getInformationElements()Ljava/util/List;

    move-result-object v2

    .line 1978
    .local v2, "ies":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult$InformationElement;>;"
    const-string v3, ""

    .line 1979
    .local v3, "routerModel":Ljava/lang/String;
    const-string v4, "MiuiNetworkPolicy"

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_0

    goto :goto_2

    .line 1984
    :cond_0
    const/4 v5, 0x4

    :try_start_0
    new-array v6, v5, [B

    .line 1986
    .local v6, "ouiInfo":[B
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/ScanResult$InformationElement;

    .line 1987
    .local v8, "ie":Landroid/net/wifi/ScanResult$InformationElement;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I

    move-result v9

    const/16 v10, 0xdd

    if-ne v9, v10, :cond_1

    .line 1988
    invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v6, v10, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 1989
    sget-object v9, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->WPA2_AKM_PSK_IE:[B

    invoke-static {v6, v9}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1990
    invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    new-array v5, v5, [B

    .line 1991
    .local v5, "data":[B
    invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 1992
    invoke-direct {p0, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->parseModelName([B)Ljava/lang/String;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v7

    .line 1993
    goto :goto_1

    .line 1996
    .end local v5    # "data":[B
    .end local v8    # "ie":Landroid/net/wifi/ScanResult$InformationElement;
    :cond_1
    goto :goto_0

    .line 2000
    .end local v6    # "ouiInfo":[B
    :cond_2
    :goto_1
    nop

    .line 2001
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getRouterModel = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2002
    return-object v3

    .line 1997
    :catch_0
    move-exception v5

    .line 1998
    .local v5, "e":Ljava/lang/Exception;
    const-string v6, "getRouterName Exception"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1999
    return-object v3

    .line 1980
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    const-string v5, "getRouterModel InformationElement empty"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1981
    return-object v3
.end method

.method private getThermalWifiLimitMode(I)I
    .locals 1
    .param p1, "wifi_limit"    # I

    .line 2389
    div-int/lit16 v0, p1, 0x2710

    rem-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private getUnRestrictedApps(Landroid/content/Context;)Ljava/util/Set;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1270
    nop

    .line 1271
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1270
    const-string v1, "cloud_network_priority_whitelist"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1272
    .local v0, "whiteString":Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1273
    .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1274
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1275
    .local v2, "packages":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1276
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 1277
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1276
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1280
    .end local v2    # "packages":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_0
    goto :goto_2

    .line 1281
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sget-object v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->LOCAL_NETWORK_PRIORITY_WHITELIST:[Ljava/lang/String;

    array-length v4, v3

    if-ge v2, v4, :cond_2

    .line 1282
    aget-object v3, v3, v2

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1281
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1285
    .end local v2    # "i":I
    :cond_2
    :goto_2
    return-object v1
.end method

.method private getWmmForUidState(II)I
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 569
    invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateWmmed(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLowLatencyApps:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsCaptivePortal:Z

    if-nez v0, :cond_0

    .line 571
    const/4 v0, 0x2

    return v0

    .line 573
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 576
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private happyEyeballsEnableCloudControl()V
    .locals 4

    .line 973
    const-string v0, "MiuiNetworkPolicy"

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    .line 975
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 974
    const-string v2, "cloud_dns_happy_eyeballs_priority_enabled"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 980
    .local v1, "enableValue":Ljava/lang/String;
    :try_start_0
    const-string v2, "persist.device_config.netd_native.happy_eyeballs_enable"

    const-string v3, "on"

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "1"

    goto :goto_0

    :cond_0
    const-string v3, "0"

    :goto_0
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "happy eyeballs enabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 984
    goto :goto_1

    .line 982
    :catch_0
    move-exception v2

    .line 983
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "Failed to set happy eyeballs cloud support config"

    invoke-static {v0, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 985
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private happyEyeballsMasterServerPriorityTimeCloudControl()V
    .locals 4

    .line 988
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    .line 990
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 989
    const-string v1, "cloud_dns_happy_eyeballs_priority_time"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 995
    .local v0, "timeValue":Ljava/lang/String;
    const-string v1, "MiuiNetworkPolicy"

    if-eqz v0, :cond_0

    :try_start_0
    const-string v2, ""

    if-eq v0, v2, :cond_0

    .line 996
    const-string v2, "persist.device_config.netd_native.happy_eyeballs_master_server_priority_time"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 998
    :catch_0
    move-exception v2

    goto :goto_1

    .line 997
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "happy eyeballs master server time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1000
    goto :goto_2

    .line 999
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    const-string v3, "Failed to set happy eyeballs master server time"

    invoke-static {v1, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1001
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private isAutoForwardEnabled()Z
    .locals 3

    .line 895
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    .line 896
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 895
    const-string v1, "cloud_auto_forward_enabled"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, "cvalue":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "on"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isLimitterEnabled()Z
    .locals 1

    .line 806
    iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled(I)Z

    move-result v0

    return v0
.end method

.method private isLimitterEnabled(I)Z
    .locals 2
    .param p1, "mode"    # I

    .line 810
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private static isMobileLatencyAllowed()Z
    .locals 1

    .line 1850
    nop

    .line 1851
    const/4 v0, 0x0

    return v0
.end method

.method private isMobileLatencyEnabledForUid(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 1791
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isMLEnabled state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",connect:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectState:Lcom/android/internal/telephony/PhoneConstants$DataState;

    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLowLatencyApps:Ljava/util/Set;

    .line 1794
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1792
    :goto_0
    return v0
.end method

.method public static isMobileTcFeatureAllowed()Z
    .locals 1

    .line 1878
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isNetworkPrioritySupport()Z
    .locals 6

    .line 726
    const/4 v0, 0x0

    .line 728
    .local v0, "support":Z
    :try_start_0
    const-string v1, "mediatek"

    const-string/jumbo v2, "vendor"

    invoke-static {v2}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "android.miui"

    const-string v3, "bool"

    if-eqz v1, :cond_1

    .line 729
    :try_start_1
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    .line 730
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "config_network_priority_mtk_global_enabled"

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    move v0, v1

    goto :goto_0

    .line 733
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "config_network_priority_mtk_enabled"

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    move v0, v1

    goto :goto_0

    .line 737
    :cond_1
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_2

    .line 738
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "config_network_priority_global_enabled"

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    move v0, v1

    goto :goto_0

    .line 741
    :cond_2
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "config_network_priority_enabled"

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v1

    .line 748
    :goto_0
    goto :goto_1

    .line 745
    :catch_0
    move-exception v1

    .line 746
    .local v1, "ex":Ljava/lang/Exception;
    const-string v2, "MiuiNetworkPolicy"

    const-string v3, "get network priority config fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    const/4 v0, 0x0

    .line 749
    .end local v1    # "ex":Ljava/lang/Exception;
    :goto_1
    return v0
.end method

.method private isPowerSaveRestrictedForUid(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 638
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNeedRestrictPowerSaveApps:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isQosFeatureAllowed()Z
    .locals 3

    .line 1865
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    const-string v0, "andromeda"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1866
    .local v0, "rst":Z
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isQosFeatureAllowed rst="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    return v0
.end method

.method private isStateUnRestrictedForUid(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 583
    if-ltz p2, :cond_1

    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    const/16 v0, 0x13

    if-ge p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    .line 584
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 583
    :goto_0
    return v0
.end method

.method private isStateWmmed(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 579
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isUidValidForRules(I)Z
    .locals 1
    .param p0, "uid"    # I

    .line 589
    const/16 v0, 0x3f5

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3fb

    if-eq p0, v0, :cond_1

    .line 590
    invoke-static {p0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 594
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 591
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private isWmmerEnabled()Z
    .locals 2

    .line 815
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCloudWmmerEnable:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsMtkRouter:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectedApModel:Ljava/lang/String;

    .line 816
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerRouterWhitelist:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectedApModel:Ljava/lang/String;

    .line 817
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 815
    :goto_0
    return v0
.end method

.method public static make(Landroid/content/Context;)Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 293
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->sSelf:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 294
    return-object v0
.end method

.method private networkPriorityCloudControl()V
    .locals 4

    .line 768
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getNetworkPriorityCloudValue()Ljava/lang/String;

    move-result-object v0

    .line 769
    .local v0, "cvalue":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isNetworkPrioritySupport()Z

    move-result v1

    if-nez v1, :cond_0

    .line 770
    const-string v0, "off"

    .line 773
    :cond_0
    :try_start_0
    const-string/jumbo v1, "sys.net.support.netprio"

    const-string v2, "off"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "false"

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "true"

    :goto_0
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 776
    goto :goto_1

    .line 774
    :catch_0
    move-exception v1

    .line 775
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiNetworkPolicy"

    const-string v3, "Failed to set network priority support config"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 777
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private networkPriorityMode()I
    .locals 4

    .line 780
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isNetworkPrioritySupport()Z

    move-result v0

    const/16 v1, 0xff

    if-nez v0, :cond_0

    .line 781
    return v1

    .line 783
    :cond_0
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getNetworkPriorityCloudValue()Ljava/lang/String;

    move-result-object v0

    .line 784
    .local v0, "cvalue":Ljava/lang/String;
    const-string v2, "off"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 785
    .local v2, "isCloudUnForceClosed":Z
    if-eqz v2, :cond_5

    .line 786
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    if-eqz v1, :cond_1

    .line 788
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "force mThermalForceMode = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MiuiNetworkPolicy"

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    const/4 v1, 0x2

    return v1

    .line 791
    :cond_1
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    if-eqz v1, :cond_2

    .line 792
    return v1

    .line 794
    :cond_2
    const/4 v1, 0x0

    .line 795
    .local v1, "def":I
    const-string v3, "on"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 796
    const/4 v1, 0x1

    goto :goto_0

    .line 797
    :cond_3
    const-string v3, "fast"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 798
    const/4 v1, 0x2

    .line 800
    :cond_4
    :goto_0
    return v1

    .line 802
    .end local v1    # "def":I
    :cond_5
    return v1
.end method

.method private onVpnNetworkChanged(Landroid/net/Network;Z)V
    .locals 11
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "add"    # Z

    .line 437
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v0

    .line 438
    .local v0, "networkCapabilities":Landroid/net/NetworkCapabilities;
    if-nez v0, :cond_0

    .line 439
    return-void

    .line 441
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkCapabilities;->getAdministratorUids()[I

    move-result-object v1

    .line 442
    .local v1, "uids":[I
    const/16 v2, 0x2710

    const/4 v3, -0x1

    const-string v4, "MiuiNetworkPolicy"

    const/4 v5, 0x0

    if-eqz p2, :cond_2

    .line 443
    array-length v6, v1

    :goto_0
    if-ge v5, v6, :cond_4

    aget v7, v1, v5

    .line 444
    .local v7, "uid":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "listened vpn network: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    if-eq v7, v3, :cond_1

    if-le v7, v2, :cond_1

    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 446
    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 447
    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    const/4 v9, 0x1

    invoke-virtual {v8, v7, v9}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z

    .line 443
    .end local v7    # "uid":I
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 451
    :cond_2
    array-length v6, v1

    move v7, v5

    :goto_1
    if-ge v7, v6, :cond_4

    aget v8, v1, v7

    .line 452
    .local v8, "uid":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "lost vpn network: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    if-eq v8, v3, :cond_3

    if-le v8, v2, :cond_3

    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 454
    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUnRestrictApps:Ljava/util/Set;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 455
    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v9, v8, v5}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z

    .line 451
    .end local v8    # "uid":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 459
    :cond_4
    return-void
.end method

.method private parseModelName([B)Ljava/lang/String;
    .locals 8
    .param p1, "bytes"    # [B

    .line 2006
    const-string v0, ""

    .line 2008
    .local v0, "modelName":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2009
    .local v1, "data":Ljava/nio/ByteBuffer;
    const/4 v2, 0x4

    new-array v3, v2, [B

    .line 2010
    .local v3, "flag":[B
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 2011
    const/4 v4, 0x2

    new-array v5, v4, [B

    .line 2012
    .local v5, "id":[B
    new-array v4, v4, [B

    .line 2013
    .local v4, "len":[B
    :goto_0
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    if-le v6, v2, :cond_1

    .line 2014
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 2015
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 2016
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    const/4 v7, 0x1

    aget-byte v7, v4, v7

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    new-array v6, v6, [B

    .line 2017
    .local v6, "name":[B
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 2018
    sget-object v7, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->MODEL_NAME:[B

    invoke-static {v5, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2019
    invoke-direct {p0, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->bytesToString([B)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 2020
    goto :goto_1

    .line 2022
    .end local v6    # "name":[B
    :cond_0
    goto :goto_0

    .line 2026
    .end local v1    # "data":Ljava/nio/ByteBuffer;
    .end local v3    # "flag":[B
    .end local v4    # "len":[B
    .end local v5    # "id":[B
    :cond_1
    :goto_1
    nop

    .line 2027
    return-object v0

    .line 2023
    :catch_0
    move-exception v1

    .line 2024
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiNetworkPolicy"

    const-string v3, "parseModelName Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2025
    return-object v0
.end method

.method private readLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .line 2318
    const-string v0, "readLine:"

    const-string v1, "close failed:"

    const-string v2, "MiuiNetworkPolicy"

    const/4 v3, 0x0

    .line 2319
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 2322
    .local v4, "line":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x400

    invoke-direct {v5, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v3, v5

    .line 2323
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v0

    .line 2332
    nop

    .line 2334
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2338
    :goto_0
    goto :goto_2

    .line 2336
    :catch_0
    move-exception v0

    .line 2337
    .local v0, "ioe2":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "ioe2":Ljava/io/IOException;
    goto :goto_0

    .line 2332
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 2328
    :catch_1
    move-exception v5

    .line 2329
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-static {v2, v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2332
    nop

    .end local v5    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_0

    .line 2334
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 2336
    :catch_2
    move-exception v0

    .line 2337
    .restart local v0    # "ioe2":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 2325
    .end local v0    # "ioe2":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 2326
    .local v5, "ioe":Ljava/io/IOException;
    :try_start_4
    invoke-static {v2, v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2332
    nop

    .end local v5    # "ioe":Ljava/io/IOException;
    if-eqz v3, :cond_0

    .line 2334
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 2336
    :catch_4
    move-exception v0

    .line 2337
    .restart local v0    # "ioe2":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 2341
    .end local v0    # "ioe2":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-object v4

    .line 2332
    :goto_3
    if-eqz v3, :cond_1

    .line 2334
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 2338
    goto :goto_4

    .line 2336
    :catch_5
    move-exception v5

    .line 2337
    .local v5, "ioe2":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2340
    .end local v5    # "ioe2":Ljava/io/IOException;
    :cond_1
    :goto_4
    throw v0
.end method

.method private registerDefaultInputMethodChangedObserver()V
    .locals 5

    .line 1387
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1418
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1419
    const-string v2, "default_input_method"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1418
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1421
    return-void
.end method

.method private registerHappyEyeballsChangeObserver()V
    .locals 5

    .line 1004
    const-string v0, "MiuiNetworkPolicy"

    const-string v1, "happy eyeballs observer register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1006
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1014
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1016
    const-string v2, "cloud_dns_happy_eyeballs_priority_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1015
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1021
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1023
    const-string v2, "cloud_dns_happy_eyeballs_priority_time"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1022
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1028
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$7;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$7;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1034
    return-void
.end method

.method private registerLowLatencyAppsChangedObserver()V
    .locals 5

    .line 1439
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$20;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$20;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1462
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1463
    const-string v2, "cloud_lowlatency_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1462
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1465
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$21;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$21;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1471
    return-void
.end method

.method private registerMiuiOptimizationChangedObserver()V
    .locals 5

    .line 1474
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1495
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    .line 1496
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1495
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1497
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$23;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$23;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1503
    return-void
.end method

.method private registerMobileLatencyAppsChangedObserver()V
    .locals 5

    .line 1719
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$27;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$27;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1741
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1742
    const-string v2, "cloud_block_scan_applist_for_mobile"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1741
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1744
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$28;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$28;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1750
    return-void
.end method

.method private registerNetworkProrityModeChangedObserver()V
    .locals 5

    .line 1111
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$12;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$12;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1127
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1128
    const-string v2, "cloud_network_priority_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1127
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1129
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1130
    const-string v2, "cloud_mtk_wifi_traffic_priority_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1129
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1132
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1133
    const-string v2, "cloud_auto_forward_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1132
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1134
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$13;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$13;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1140
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVPNNetworkRequest:Landroid/net/NetworkRequest;

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mVPNNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v1, v2, v3}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1141
    return-void
.end method

.method private registerP2PHCChangeObserver()V
    .locals 5

    .line 1068
    const-string v0, "MiuiNetworkPolicy"

    const-string v1, "p2phc observer register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$10;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$10;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1095
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1097
    const-string v2, "cloud_p2phc_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1096
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1102
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$11;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$11;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1108
    return-void
.end method

.method private registerPowerKeeperSleep()V
    .locals 2

    .line 2072
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUserManager:Landroid/os/UserManager;

    .line 2073
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    const-class v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->cm:Landroid/net/ConnectivityManager;

    .line 2074
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    .line 2075
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSleepModeEnableControlObserver()V

    .line 2076
    return-void
.end method

.method private registerRestrictPowerSaveAppsChangedObserver()V
    .locals 5

    .line 1289
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$15;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$15;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1311
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1312
    const-string v2, "cloud_block_scan_applist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1311
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1314
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$16;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$16;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1320
    return-void
.end method

.method private registerSleepModeEnableControlObserver()V
    .locals 5

    .line 2143
    const-string v0, "MiuiNetworkPolicySleepMode"

    const-string v1, "registerSleepModeEnableControlObserver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2144
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 2164
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2166
    const-string v2, "key_open_earthquake_warning"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2165
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2170
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2172
    const-string v2, "cloud_sleepmode_networkpolicy_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2171
    const/4 v4, -0x1

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2177
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$34;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$34;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2183
    return-void
.end method

.method private registerSleepModeReceiver()V
    .locals 7

    .line 2081
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2082
    .local v0, "sleepModeFilter":Landroid/content/IntentFilter;
    const-string v1, "com.miui.powerkeeper_sleep_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2083
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2084
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    move-object v3, v0

    invoke-virtual/range {v1 .. v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 2087
    const-string v1, "MiuiNetworkPolicySleepMode"

    const-string v2, "registerSleepModeReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2088
    return-void
.end method

.method private registerSmartDnsChangeObserver()V
    .locals 5

    .line 1037
    const-string v0, "MiuiNetworkPolicy"

    const-string v1, "SmartDns observer register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$8;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$8;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1052
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1054
    const-string v2, "cloud_smartdns_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1053
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1059
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$9;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$9;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1065
    return-void
.end method

.method private registerTrustMiuiAddDnsObserver()V
    .locals 5

    .line 945
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$5;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$5;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 965
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 966
    const-string v2, "cloud_trust_miui_add_dns"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 965
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 970
    return-void
.end method

.method private registerUnRestirctAppsChangedObserver()V
    .locals 5

    .line 1338
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$17;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$17;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1362
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1363
    const-string v2, "cloud_network_priority_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1362
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1365
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$18;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$18;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1371
    return-void
.end method

.method private registerWifiNetworkListener()V
    .locals 3

    .line 931
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isAutoForwardEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z

    if-nez v0, :cond_0

    .line 932
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiNetworkRequest:Landroid/net/NetworkRequest;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 933
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z

    goto :goto_0

    .line 934
    :cond_0
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isAutoForwardEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z

    if-eqz v0, :cond_2

    .line 935
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 936
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 937
    const-string v0, "MiuiNetworkPolicy"

    const-string v2, "cloud control is disabled, remove autoforward rule"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V

    .line 940
    :cond_1
    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z

    .line 942
    :cond_2
    :goto_0
    return-void
.end method

.method private registerWmmerEnableChangedObserver()V
    .locals 5

    .line 901
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$3;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 917
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 918
    const-string v2, "cloud_wmmer_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 917
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 919
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 920
    const-string v2, "cloud_mtk_wmmer_enabled"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 919
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 922
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$4;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$4;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 928
    return-void
.end method

.method private registerWmmerRouterWhitelistChangedObserver()V
    .locals 5

    .line 1938
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$30;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$30;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V

    .line 1952
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1953
    const-string v2, "cloud_wmmer_router_whitelist"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1952
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1955
    return-void
.end method

.method private removeUidState(I)V
    .locals 4
    .param p1, "uid"    # I

    .line 1247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "removeUidState uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    .line 1249
    .local v0, "index":I
    if-ltz v0, :cond_3

    .line 1250
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    .line 1251
    .local v1, "oldUidState":I
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    .line 1252
    const/16 v2, 0x13

    if-eq v1, v2, :cond_3

    .line 1253
    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRulesForUidStateChange(III)V

    .line 1255
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyAllowed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1256
    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyForUidStateChange(III)V

    .line 1259
    :cond_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mQosUtils:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosForUidStateChange(III)V

    .line 1261
    :cond_1
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mAppBuckets:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    if-eqz v3, :cond_2

    invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppBucketsForUidStateChange(III)V

    .line 1263
    :cond_2
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    if-eqz v3, :cond_3

    invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateRulesForUidStateChange(III)V

    .line 1267
    .end local v1    # "oldUidState":I
    :cond_3
    return-void
.end method

.method private setSleepModeWhitelistUidRules()V
    .locals 3

    .line 2186
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2187
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeWhitelistUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2188
    .local v1, "uid":I
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateSleepModeWhitelistUidRules(IZ)V

    .line 2189
    .end local v1    # "uid":I
    goto :goto_0

    .line 2191
    :cond_0
    return-void
.end method

.method private setThermalPolicy(I)Z
    .locals 3
    .param p1, "mode"    # I

    .line 2350
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->validateThermalMode(I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2351
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2352
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2353
    const/4 v0, 0x1

    return v0

    .line 2355
    :cond_0
    return v1
.end method

.method private sleepModeEnableControl()Z
    .locals 5

    .line 2127
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    .line 2129
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2128
    const-string v1, "key_open_earthquake_warning"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 2133
    .local v0, "earthQuack":I
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    .line 2135
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2134
    const-string v3, "cloud_sleepmode_networkpolicy_enabled"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 2138
    .local v1, "cloudEnable":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sleepModeEnableControl earthQuack:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cloudEnable:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiNetworkPolicySleepMode"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2139
    const-string v3, "on"

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    move v2, v3

    :cond_0
    return v2
.end method

.method private unregisterSleepModeReceiver()V
    .locals 2

    .line 2091
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 2092
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2093
    const-string v0, "MiuiNetworkPolicySleepMode"

    const-string/jumbo v1, "unregisterSleepModeReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2095
    :cond_0
    return-void
.end method

.method private updateLimit(Z)V
    .locals 8
    .param p1, "enabled"    # Z

    .line 1506
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateLimit mLimitEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mNetworkPriorityMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mThermalForceMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1507
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z

    if-eq v0, p1, :cond_2

    .line 1508
    const-wide/16 v0, 0x0

    .line 1509
    .local v0, "bwBps":J
    if-eqz p1, :cond_1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1510
    const-wide/32 v0, 0x30d40

    .line 1511
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1512
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHistoryBandWidth:Ljava/util/Deque;

    invoke-static {v2}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 1514
    :cond_0
    const-wide/16 v2, 0x50

    mul-long/2addr v2, v0

    const-wide/16 v4, 0x64

    div-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 1515
    .local v2, "fgBw":J
    sub-long v4, v0, v2

    const-wide/32 v6, 0x186a0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1517
    .end local v2    # "fgBw":J
    :cond_1
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v2, p1, v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setLimit(ZJ)Z

    .line 1518
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z

    .line 1520
    .end local v0    # "bwBps":J
    :cond_2
    return-void
.end method

.method private updateLimitterForUidState(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 618
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateUnRestrictedForUid(II)Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z

    .line 621
    :cond_0
    return-void
.end method

.method private updateMobileLatency()V
    .locals 6

    .line 1820
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    .line 1821
    .local v0, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1822
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 1823
    .local v2, "uid":I
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    const/16 v4, 0x13

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 1824
    .local v3, "state":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateMobileLatency uid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiNetworkPolicy"

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1825
    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isUidValidForRules(I)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileLowLatencyApps:Ljava/util/Set;

    .line 1826
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1827
    invoke-direct {p0, v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyStateForUidState(II)V

    .line 1821
    .end local v2    # "uid":I
    .end local v3    # "state":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1830
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private updateMobileLatencyForUidStateChange(III)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "oldUidState"    # I
    .param p3, "newUidState"    # I

    .line 1833
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateMLUid uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",old:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",new:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1834
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isUidValidForRules(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1835
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyEnabledForUid(II)Z

    move-result v0

    .line 1836
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyEnabledForUid(II)Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1837
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyStateForUidState(II)V

    .line 1839
    :cond_1
    return-void
.end method

.method private updateMobileLatencyStateForUidState(II)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 1798
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyEnabledForUid(II)Z

    move-result v0

    .line 1799
    .local v0, "enabled":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateMobileLowLatencyStateForUidState enabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1800
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableMobileLowLatency(Z)V

    .line 1801
    return-void
.end method

.method private updatePowerSaveForUidDataActivityChanged(IZ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "active"    # Z

    .line 648
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    const/16 v1, 0x13

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 649
    .local v0, "state":I
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z

    move-result v1

    .line 650
    .local v1, "restrict":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "update ps for data activity, uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", state= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", restrict = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", active = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mPS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiNetworkPolicy"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    if-eqz p2, :cond_0

    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    if-eqz v2, :cond_0

    .line 653
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V

    goto :goto_0

    .line 654
    :cond_0
    if-nez p2, :cond_1

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z

    if-nez v2, :cond_1

    .line 655
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V

    .line 657
    :cond_1
    :goto_0
    return-void
.end method

.method private updatePowerSaveStateForUidState(II)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 660
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z

    move-result v6

    .line 664
    .local v6, "restrict":Z
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    sget v1, Landroid/system/OsConstants;->IPPROTO_UDP:I

    const/16 v3, 0x76

    const/4 v4, 0x2

    move v2, p1

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/net/MiuiNetworkManagementService;->listenUidDataActivity(IIIIZ)Z

    .line 666
    xor-int/lit8 v0, v6, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V

    .line 667
    return-void
.end method

.method private updateRuleForMobileTc()V
    .locals 7

    .line 1884
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    .line 1885
    .local v0, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1886
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 1887
    .local v2, "uid":I
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    const/16 v4, 0x13

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 1888
    .local v3, "state":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateRuleForMobileTc uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", state = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MiuiNetworkPolicy"

    invoke-static {v6, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1889
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    if-eqz v5, :cond_0

    invoke-virtual {v5, v2, v4, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateRulesForUidStateChange(III)V

    .line 1885
    .end local v2    # "uid":I
    .end local v3    # "state":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1892
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private updateRuleGlobal()V
    .locals 7

    .line 679
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    .line 680
    .local v0, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 681
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 682
    .local v2, "uid":I
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    const/16 v4, 0x13

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 683
    .local v3, "state":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateRuleGlobal uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", state = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MiuiNetworkPolicy"

    invoke-static {v6, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    invoke-direct {p0, v2, v4, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRulesForUidStateChange(III)V

    .line 680
    .end local v2    # "uid":I
    .end local v3    # "state":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 686
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method private updateRulesForUidStateChange(III)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "oldUidState"    # I
    .param p3, "newUidState"    # I

    .line 624
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isUidValidForRules(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 625
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I

    move-result v0

    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 626
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateWmmForUidState(II)V

    .line 628
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateUnRestrictedForUid(II)Z

    move-result v0

    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateUnRestrictedForUid(II)Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 629
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateLimitterForUidState(II)V

    .line 631
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z

    move-result v0

    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z

    move-result v1

    if-eq v0, v1, :cond_2

    .line 632
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updatePowerSaveStateForUidState(II)V

    .line 635
    :cond_2
    return-void
.end method

.method private updateSleepModeWhitelistUidRules(IZ)V
    .locals 8
    .param p1, "uid"    # I
    .param p2, "added"    # Z

    .line 2204
    const-string v0, "MiuiNetworkPolicySleepMode"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->cm:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_0

    .line 2205
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateSleepModeWhitelistUidRules appId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " added:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2206
    const-string v1, "android.net.ConnectivityManager"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 2207
    .local v1, "clazz":Ljava/lang/Class;
    const-string/jumbo v2, "updateSleepModeUidRule"

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x1

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 2208
    .local v2, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v2, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2209
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->cm:Landroid/net/ConnectivityManager;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v6

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v7

    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2213
    .end local v1    # "clazz":Ljava/lang/Class;
    .end local v2    # "method":Ljava/lang/reflect/Method;
    :cond_0
    goto :goto_0

    .line 2211
    :catch_0
    move-exception v1

    .line 2212
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "updateSleepModeWhitelistUidRules error "

    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2214
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateUidState(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "uidState"    # I

    .line 1231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateUidState uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uidState = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1232
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    const/16 v1, 0x13

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    .line 1233
    .local v0, "oldUidState":I
    if-eq v0, p2, :cond_3

    .line 1235
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidState:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 1236
    invoke-direct {p0, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRulesForUidStateChange(III)V

    .line 1237
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyAllowed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1238
    invoke-direct {p0, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyForUidStateChange(III)V

    .line 1240
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mQosUtils:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosForUidStateChange(III)V

    .line 1241
    :cond_1
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mAppBuckets:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppBucketsForUidStateChange(III)V

    .line 1242
    :cond_2
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    if-eqz v1, :cond_3

    invoke-virtual {v1, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateRulesForUidStateChange(III)V

    .line 1244
    :cond_3
    return-void
.end method

.method private updateWmmForUidState(II)V
    .locals 6
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 598
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z

    if-eqz v0, :cond_0

    .line 599
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateWmmForUidState uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " wmm: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    const/4 v0, 0x0

    .local v0, "market_uid":I
    const/4 v1, 0x0

    .local v1, "download_uid":I
    const/4 v2, 0x0

    .line 602
    .local v2, "downloadui_uid":I
    :try_start_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 603
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.xiaomi.market"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    move v0, v4

    .line 604
    const-string v4, "com.android.providers.downloads"

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    move v1, v4

    .line 605
    const-string v4, "com.android.providers.downloads.ui"

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v4

    .line 608
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    goto :goto_0

    .line 606
    :catch_0
    move-exception v3

    .line 607
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 609
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I

    move-result v4

    invoke-virtual {v3, p1, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->updateWmm(II)Z

    .line 610
    if-ne p1, v0, :cond_0

    if-eqz v0, :cond_0

    .line 611
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I

    move-result v4

    invoke-virtual {v3, v1, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->updateWmm(II)Z

    .line 612
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->updateWmm(II)Z

    .line 615
    .end local v0    # "market_uid":I
    .end local v1    # "download_uid":I
    .end local v2    # "downloadui_uid":I
    :cond_0
    return-void
.end method

.method private validatePriorityMode(I)Z
    .locals 2
    .param p1, "mode"    # I

    .line 821
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/16 v1, 0xff

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private validateThermalMode(I)Z
    .locals 1
    .param p1, "mode"    # I

    .line 2345
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public getMiuiSlmVoipUdpAddress(I)J
    .locals 4
    .param p1, "uid"    # I

    .line 1916
    if-eqz p1, :cond_0

    .line 1917
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->getMiuiSlmVoipUdpAddress(I)J

    move-result-wide v0

    .line 1918
    .local v0, "address":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMiuiSlmVoipUdpAddress uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " address = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiNetworkPolicy"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1919
    return-wide v0

    .line 1921
    .end local v0    # "address":J
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getMiuiSlmVoipUdpPort(I)I
    .locals 3
    .param p1, "uid"    # I

    .line 1925
    if-eqz p1, :cond_0

    .line 1926
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->getMiuiSlmVoipUdpPort(I)I

    move-result v0

    .line 1927
    .local v0, "ports":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMiuiSlmVoipUdpPort uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ports = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1928
    return v0

    .line 1930
    .end local v0    # "ports":I
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getShareStats(I)J
    .locals 2
    .param p1, "type"    # I

    .line 1934
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->getShareStats(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public onSleepModeWhitelistChange(IZ)Z
    .locals 2
    .param p1, "appId"    # I
    .param p2, "added"    # Z

    .line 2056
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSleepModeWhitelistChange appId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicySleepMode"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2057
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;

    invoke-direct {v1, p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2068
    const/4 v0, 0x1

    return v0
.end method

.method public setMiuiSlmBpfUid(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1907
    if-eqz p1, :cond_0

    .line 1908
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->setMiuiSlmBpfUid(I)V

    .line 1909
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMiuiSlmBpfUid uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    const/4 v0, 0x1

    return v0

    .line 1912
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setNetworkTrafficPolicy(I)Z
    .locals 3
    .param p1, "mode"    # I

    .line 828
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->validatePriorityMode(I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 830
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 831
    const/4 v0, 0x1

    return v0

    .line 833
    :cond_0
    return v1
.end method

.method public setRpsStatus(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .line 837
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setRpsStatus/in ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z

    if-eq v0, p1, :cond_0

    .line 839
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 840
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 841
    const-string/jumbo v0, "setRpsStatus/out [ true ]"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    const/4 v0, 0x1

    return v0

    .line 844
    :cond_0
    const-string/jumbo v0, "setRpsStatus/out [ false]"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    const/4 v0, 0x0

    return v0
.end method

.method public setTrafficControllerForMobile(ZJ)I
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "rate"    # J

    .line 1895
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setTrafficControllerForMobile enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "rate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1897
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    if-nez v0, :cond_0

    .line 1898
    const-string/jumbo v0, "setTrafficControllerForMobile unsupport device!!!"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1899
    const/4 v0, -0x1

    return v0

    .line 1901
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimitForGame(ZJ)I

    move-result v0

    .line 1902
    .local v0, "rst":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setTrafficControllerForMobile rst = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1903
    return v0
.end method

.method public systemReady()V
    .locals 9

    .line 480
    const-string/jumbo v0, "wifi.interface"

    const-string/jumbo v1, "wlan0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mInterfaceName:Ljava/lang/String;

    .line 482
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    .line 483
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mUidDataActivityObserver:Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setNetworkEventObserver(Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;)V

    .line 484
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 488
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableWmmer(Z)Z

    .line 489
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z

    .line 491
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/net/NetworkStatsReporter;->make(Landroid/content/Context;)Lcom/android/server/net/NetworkStatsReporter;

    .line 493
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 495
    .local v0, "wifiStateFilter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.NETWORK_CONDITIONS_MEASURED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 496
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    move-object v4, v0

    invoke-virtual/range {v2 .. v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 498
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWmmerEnableChangedObserver()V

    .line 499
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerNetworkProrityModeChangedObserver()V

    .line 500
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->networkPriorityCloudControl()V

    .line 502
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getDefaultInputMethod()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mDefaultInputMethod:Ljava/lang/String;

    .line 504
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerUnRestirctAppsChangedObserver()V

    .line 506
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerLowLatencyAppsChangedObserver()V

    .line 508
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWmmerRouterWhitelistChangedObserver()V

    .line 510
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerDefaultInputMethodChangedObserver()V

    .line 513
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerRestrictPowerSaveAppsChangedObserver()V

    .line 516
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerTrustMiuiAddDnsObserver()V

    .line 519
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerHappyEyeballsChangeObserver()V

    .line 522
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerP2PHCChangeObserver()V

    .line 524
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSmartDnsChangeObserver()V

    .line 527
    nop

    .line 532
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 533
    .local v1, "packageFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 534
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 535
    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 536
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x4

    move-object v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 538
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSupport:Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    invoke-virtual {v2}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->registerUidObserver()V

    .line 540
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWifiNetworkListener()V

    .line 542
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyAllowed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 544
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerMobileLatencyAppsChangedObserver()V

    .line 545
    new-instance v5, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.ANY_DATA_STATE"

    invoke-direct {v5, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 547
    .local v5, "mobileNwFilter":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileNwReceiver:Landroid/content/BroadcastReceiver;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHandler:Landroid/os/Handler;

    const/4 v8, 0x4

    invoke-virtual/range {v3 .. v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    .line 550
    .end local v5    # "mobileNwFilter":Landroid/content/IntentFilter;
    :cond_0
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mQosUtils:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->systemReady(Lcom/android/server/net/MiuiNetworkManagementService;)V

    .line 551
    :cond_1
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mAppBuckets:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->systemReady()V

    .line 552
    :cond_2
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mMobileTcUtils:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkManager:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-virtual {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->systemReady(Lcom/android/server/net/MiuiNetworkManagementService;)V

    .line 556
    :cond_3
    sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z

    if-nez v2, :cond_4

    sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_GO_VERSION:Z

    if-nez v2, :cond_4

    .line 557
    const-string v2, "MiuiNetworkPolicy"

    const-string v3, "is MIUI or is not apply getMiuiLiteVersion"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->getInstance(Landroid/content/Context;)Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkBoostService:Lcom/xiaomi/NetworkBoost/NetworkBoostService;

    .line 560
    if-eqz v2, :cond_4

    .line 561
    iget v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I

    iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I

    iget v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I

    invoke-virtual {v2, v3, v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V

    .line 565
    :cond_4
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerPowerKeeperSleep()V

    .line 566
    return-void
.end method

.method updateRouterWhitelist(Ljava/lang/String;)V
    .locals 5
    .param p1, "whitelist"    # Ljava/lang/String;

    .line 1959
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "MiuiNetworkPolicy"

    if-eqz v0, :cond_0

    .line 1960
    :try_start_1
    const-string v0, "RT whitelist is empty."

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1961
    return-void

    .line 1963
    :cond_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1964
    .local v0, "temp":[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerRouterWhitelist:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 1965
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    .line 1966
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerRouterWhitelist:Ljava/util/HashSet;

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1967
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update RT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1965
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1971
    .end local v0    # "temp":[Ljava/lang/String;
    .end local v2    # "i":I
    :cond_1
    goto :goto_1

    .line 1969
    :catch_0
    move-exception v0

    .line 1970
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1972
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
