public class com.android.server.net.MiuiNetworkManagementService {
	 /* .source "MiuiNetworkManagementService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;, */
	 /* Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;, */
	 /* Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ALLOW;
private static final Integer MIUI_FIREWALL_RESPONSE_CODE;
private static final Integer POWER_SAVE_IDLETIMER_LABEL;
private static final Integer REJECT;
private static final java.lang.String TAG;
private static final Integer TYPE_ALL;
private static final Integer TYPE_MOBILE;
private static final Integer TYPE_ROAMING;
private static final Integer TYPE_WIFI;
private static com.android.server.net.MiuiNetworkManagementService sInstance;
/* # instance fields */
private android.os.IBinder mBinder;
private final android.content.Context mContext;
private Integer mCurrentNetworkState;
private java.util.Set mListenedIdleTimerType;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.net.MiuiNetworkManagementService$OemNetdUnsolicitedEventListener mListenter;
private final java.lang.Object mLock;
private android.util.SparseArray mMiuiFireRuleCache;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/Set<", */
/* "Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private com.android.internal.net.IOemNetd mNetd;
private android.os.INetworkManagementService mNms;
private com.android.server.net.MiuiNetworkManagementService$NetworkEventObserver mObserver;
private Integer mStateSetterPid;
private Integer mStateSetterUid;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.net.MiuiNetworkManagementService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
private com.android.server.net.MiuiNetworkManagementService ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 71 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 462 */
int v0 = 0; // const/4 v0, 0x0
this.mNms = v0;
/* .line 551 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 552 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I */
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterUid:I */
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterPid:I */
/* .line 72 */
this.mContext = p1;
/* .line 74 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mListenedIdleTimerType = v0;
/* .line 75 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
/* .line 76 */
return;
} // .end method
static synchronized com.android.server.net.MiuiNetworkManagementService Init ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/android/server/net/MiuiNetworkManagementService; */
/* monitor-enter v0 */
/* .line 63 */
try { // :try_start_0
/* new-instance v1, Lcom/android/server/net/MiuiNetworkManagementService; */
/* invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkManagementService;-><init>(Landroid/content/Context;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 64 */
/* monitor-exit v0 */
/* .line 62 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
private void closeSocketsForFirewall ( java.lang.String p0, Integer[] p1, Boolean p2 ) {
/* .locals 11 */
/* .param p1, "fwType" # Ljava/lang/String; */
/* .param p2, "uids" # [I */
/* .param p3, "isWhitelistUid" # Z */
/* .line 217 */
final String v0 = "fw_doze"; // const-string v0, "fw_doze"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 218 */
/* nop */
/* .line 219 */
/* const/16 v0, 0x2710 */
/* const v2, 0x7fffffff */
com.android.server.net.MiuiNetworkManagementService .makeUidRangeParcel ( v0,v2 );
/* filled-new-array {v0}, [Landroid/net/UidRangeParcel; */
/* .line 223 */
/* .local v0, "ranges":[Landroid/net/UidRangeParcel; */
/* move-object v2, p2 */
/* .local v2, "exemptUids":[I */
/* .line 225 */
} // .end local v0 # "ranges":[Landroid/net/UidRangeParcel;
} // .end local v2 # "exemptUids":[I
} // :cond_0
/* array-length v0, p2 */
/* new-array v0, v0, [Landroid/net/UidRangeParcel; */
/* .line 226 */
/* .restart local v0 # "ranges":[Landroid/net/UidRangeParcel; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p2 */
/* if-ge v2, v3, :cond_1 */
/* .line 227 */
/* aget v3, p2, v2 */
/* aget v4, p2, v2 */
com.android.server.net.MiuiNetworkManagementService .makeUidRangeParcel ( v3,v4 );
/* aput-object v3, v0, v2 */
/* .line 226 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 229 */
} // .end local v2 # "i":I
} // :cond_1
/* new-array v2, v1, [I */
/* .line 235 */
/* .local v2, "exemptUids":[I */
} // :goto_1
try { // :try_start_0
final String v3 = "com.android.internal.net.IOemNetd$Stub"; // const-string v3, "com.android.internal.net.IOemNetd$Stub"
java.lang.Class .forName ( v3 );
/* .line 236 */
/* .local v3, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v4 = "asInterface"; // const-string v4, "asInterface"
int v5 = 1; // const/4 v5, 0x1
/* new-array v6, v5, [Ljava/lang/Class; */
/* const-class v7, Landroid/os/IBinder; */
/* aput-object v7, v6, v1 */
(( java.lang.Class ) v3 ).getDeclaredMethod ( v4, v6 ); // invoke-virtual {v3, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 237 */
/* .local v4, "asInterface":Ljava/lang/reflect/Method; */
/* new-array v6, v5, [Ljava/lang/Object; */
v7 = this.mBinder;
/* aput-object v7, v6, v1 */
int v7 = 0; // const/4 v7, 0x0
(( java.lang.reflect.Method ) v4 ).invoke ( v7, v6 ); // invoke-virtual {v4, v7, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 238 */
/* .local v6, "IOemNetdObj":Ljava/lang/Object; */
(( java.lang.Object ) v6 ).getClass ( ); // invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* const-string/jumbo v8, "socketDestroy" */
int v9 = 2; // const/4 v9, 0x2
/* new-array v9, v9, [Ljava/lang/Class; */
/* const-class v10, [Landroid/net/UidRangeParcel; */
/* aput-object v10, v9, v1 */
/* const-class v1, [I */
/* aput-object v1, v9, v5 */
(( java.lang.Class ) v7 ).getDeclaredMethod ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 239 */
/* .local v1, "socketDestroy":Ljava/lang/reflect/Method; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 240 */
/* filled-new-array {v0, v2}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( v6, v5 ); // invoke-virtual {v1, v6, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 243 */
} // .end local v1 # "socketDestroy":Ljava/lang/reflect/Method;
} // .end local v3 # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v4 # "asInterface":Ljava/lang/reflect/Method;
} // .end local v6 # "IOemNetdObj":Ljava/lang/Object;
} // :cond_2
/* .line 241 */
/* :catch_0 */
/* move-exception v1 */
/* .line 242 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "closeSocketsForFirewall Exception"; // const-string v4, "closeSocketsForFirewall Exception"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": "; // const-string v4, ": "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "NetworkManagement"; // const-string v4, "NetworkManagement"
android.util.Log .e ( v4,v3 );
/* .line 244 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private java.lang.String convertTypeToLable ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 348 */
int v0 = 0; // const/4 v0, 0x0
/* .line 349 */
/* .local v0, "lable":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_0 */
/* .line 350 */
/* const-string/jumbo v1, "wifi.interface" */
/* const-string/jumbo v2, "wlan0" */
android.os.SystemProperties .get ( v1,v2 );
/* .line 351 */
} // :cond_0
/* const/16 v1, 0x76 */
/* if-le p1, v1, :cond_1 */
/* .line 352 */
java.lang.String .valueOf ( p1 );
/* .line 354 */
} // :cond_1
} // :goto_0
} // .end method
public static synchronized com.android.server.net.MiuiNetworkManagementService getInstance ( ) {
/* .locals 2 */
/* const-class v0, Lcom/android/server/net/MiuiNetworkManagementService; */
/* monitor-enter v0 */
/* .line 68 */
try { // :try_start_0
v1 = com.android.server.net.MiuiNetworkManagementService.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 68 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
private void getNmsService ( ) {
/* .locals 1 */
/* .line 465 */
v0 = this.mNms;
/* if-nez v0, :cond_0 */
/* .line 466 */
/* nop */
/* .line 467 */
final String v0 = "network_management"; // const-string v0, "network_management"
android.os.ServiceManager .getService ( v0 );
/* .line 466 */
android.os.INetworkManagementService$Stub .asInterface ( v0 );
this.mNms = v0;
/* .line 469 */
} // :cond_0
return;
} // .end method
private static android.net.UidRangeParcel makeUidRangeParcel ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "start" # I */
/* .param p1, "stop" # I */
/* .line 208 */
/* new-instance v0, Landroid/net/UidRangeParcel; */
/* invoke-direct {v0, p0, p1}, Landroid/net/UidRangeParcel;-><init>(II)V */
/* .line 209 */
/* .local v0, "range":Landroid/net/UidRangeParcel; */
} // .end method
private void setCurrentNetworkState ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "state" # I */
/* .param p2, "callerUid" # I */
/* .param p3, "callerPid" # I */
/* .line 573 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 574 */
try { // :try_start_0
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I */
/* .line 575 */
/* iput p2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterUid:I */
/* .line 576 */
/* iput p3, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterPid:I */
/* .line 577 */
/* monitor-exit v0 */
/* .line 578 */
return;
/* .line 577 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void setMiuiFirewallRule ( java.lang.String p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "rule" # I */
/* .param p4, "type" # I */
/* .param p5, "callerUid" # I */
/* .param p6, "callerPid" # I */
/* .line 556 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 557 */
try { // :try_start_0
v1 = this.mMiuiFireRuleCache;
/* if-nez v1, :cond_0 */
/* .line 558 */
/* new-instance v1, Landroid/util/SparseArray; */
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V */
this.mMiuiFireRuleCache = v1;
/* .line 560 */
} // :cond_0
/* new-instance v1, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
/* move-object v2, v1 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move v5, p3 */
/* move v6, p5 */
/* move v7, p6 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;-><init>(Ljava/lang/String;IIII)V */
/* .line 561 */
/* .local v1, "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
v2 = this.mMiuiFireRuleCache;
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
(( android.util.SparseArray ) v2 ).get ( p4, v3 ); // invoke-virtual {v2, p4, v3}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/util/Set; */
/* .line 562 */
/* .local v2, "typeRule":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;" */
int v3 = 2; // const/4 v3, 0x2
/* if-nez p3, :cond_1 */
/* if-eq p4, v3, :cond_2 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_3
/* if-eq p4, v3, :cond_3 */
/* .line 564 */
} // :cond_2
/* .line 566 */
} // :cond_3
/* .line 568 */
} // :goto_0
v3 = this.mMiuiFireRuleCache;
(( android.util.SparseArray ) v3 ).put ( p4, v2 ); // invoke-virtual {v3, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 569 */
} // .end local v1 # "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
} // .end local v2 # "typeRule":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;"
/* monitor-exit v0 */
/* .line 570 */
return;
/* .line 569 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Boolean addMiuiFirewallSharedUid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 178 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 180 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 181 */
/* :catch_0 */
/* move-exception v0 */
/* .line 182 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 183 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void closeSocketForAurogon ( Integer[] p0 ) {
/* .locals 3 */
/* .param p1, "uids" # [I */
/* .line 485 */
final String v0 = "NetworkManagement"; // const-string v0, "NetworkManagement"
try { // :try_start_0
v1 = this.mNms;
/* if-nez v1, :cond_0 */
/* .line 486 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkManagementService;->getNmsService()V */
/* .line 488 */
} // :cond_0
v1 = this.mNms;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 489 */
final String v1 = "call socket destroy!"; // const-string v1, "call socket destroy!"
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 495 */
} // :cond_1
/* .line 493 */
/* :catch_0 */
/* move-exception v1 */
/* .line 494 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "failed to close socket for aurogon!"; // const-string v2, "failed to close socket for aurogon!"
android.util.Slog .d ( v0,v2 );
/* .line 496 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void doDesSocketForUid ( java.lang.String p0, Integer[] p1, Boolean p2 ) {
/* .locals 12 */
/* .param p1, "fwType" # Ljava/lang/String; */
/* .param p2, "uids" # [I */
/* .param p3, "isWhitelistUid" # Z */
/* .line 247 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 252 */
try { // :try_start_0
final String v0 = "com.android.internal.net.IOemNetd$Stub"; // const-string v0, "com.android.internal.net.IOemNetd$Stub"
java.lang.Class .forName ( v0 );
/* .line 253 */
/* .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v1 = "asInterface"; // const-string v1, "asInterface"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Landroid/os/IBinder; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 254 */
/* .local v1, "asInterface":Ljava/lang/reflect/Method; */
/* new-array v4, v3, [Ljava/lang/Object; */
v5 = this.mBinder;
/* aput-object v5, v4, v6 */
int v5 = 0; // const/4 v5, 0x0
(( java.lang.reflect.Method ) v1 ).invoke ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 255 */
/* .local v4, "IOemNetdObj":Ljava/lang/Object; */
(( java.lang.Object ) v4 ).getClass ( ); // invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v7 = "doFireWallForUid"; // const-string v7, "doFireWallForUid"
int v8 = 3; // const/4 v8, 0x3
/* new-array v9, v8, [Ljava/lang/Class; */
/* const-class v10, Ljava/lang/String; */
/* aput-object v10, v9, v6 */
/* const-class v10, [I */
/* aput-object v10, v9, v3 */
v10 = java.lang.Boolean.TYPE;
int v11 = 2; // const/4 v11, 0x2
/* aput-object v10, v9, v11 */
(( java.lang.Class ) v5 ).getDeclaredMethod ( v7, v9 ); // invoke-virtual {v5, v7, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 256 */
/* .local v5, "doFireWallForUid":Ljava/lang/reflect/Method; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 257 */
/* new-array v7, v8, [Ljava/lang/Object; */
/* aput-object p1, v7, v6 */
/* aput-object p2, v7, v3 */
java.lang.Boolean .valueOf ( p3 );
/* aput-object v3, v7, v11 */
(( java.lang.reflect.Method ) v5 ).invoke ( v4, v7 ); // invoke-virtual {v5, v4, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 260 */
} // .end local v0 # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v1 # "asInterface":Ljava/lang/reflect/Method;
} // .end local v4 # "IOemNetdObj":Ljava/lang/Object;
} // .end local v5 # "doFireWallForUid":Ljava/lang/reflect/Method;
} // :cond_0
/* .line 258 */
/* :catch_0 */
/* move-exception v0 */
/* .line 259 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "doDesSocketForUid Exception: "; // const-string v3, "doDesSocketForUid Exception: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 262 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkManagementService;->closeSocketsForFirewall(Ljava/lang/String;[IZ)V */
/* .line 263 */
return;
} // .end method
public void doRestoreSockForUid ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "fwType" # Ljava/lang/String; */
/* .line 266 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 271 */
try { // :try_start_0
final String v0 = "com.android.internal.net.IOemNetd$Stub"; // const-string v0, "com.android.internal.net.IOemNetd$Stub"
java.lang.Class .forName ( v0 );
/* .line 272 */
/* .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v1 = "asInterface"; // const-string v1, "asInterface"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Landroid/os/IBinder; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 273 */
/* .local v1, "asInterface":Ljava/lang/reflect/Method; */
/* new-array v4, v3, [Ljava/lang/Object; */
v5 = this.mBinder;
/* aput-object v5, v4, v6 */
int v5 = 0; // const/4 v5, 0x0
(( java.lang.reflect.Method ) v1 ).invoke ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 274 */
/* .local v4, "IOemNetdObj":Ljava/lang/Object; */
(( java.lang.Object ) v4 ).getClass ( ); // invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v7 = "doRestoreSockForUid"; // const-string v7, "doRestoreSockForUid"
/* new-array v3, v3, [Ljava/lang/Class; */
/* const-class v8, Ljava/lang/String; */
/* aput-object v8, v3, v6 */
(( java.lang.Class ) v5 ).getDeclaredMethod ( v7, v3 ); // invoke-virtual {v5, v7, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 275 */
/* .local v3, "doRestoreSockForUid":Ljava/lang/reflect/Method; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 276 */
/* filled-new-array {p1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v3 ).invoke ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 279 */
} // .end local v0 # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v1 # "asInterface":Ljava/lang/reflect/Method;
} // .end local v3 # "doRestoreSockForUid":Ljava/lang/reflect/Method;
} // .end local v4 # "IOemNetdObj":Ljava/lang/Object;
} // :cond_0
/* .line 277 */
/* :catch_0 */
/* move-exception v0 */
/* .line 278 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "doRestoreSockForUid Exception: "; // const-string v3, "doRestoreSockForUid Exception: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 280 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 512 */
/* if-nez p1, :cond_0 */
/* .line 513 */
return;
/* .line 515 */
} // :cond_0
final String v0 = "========================================MiuiFireRule DUMP BEGIN========================================"; // const-string v0, "========================================MiuiFireRule DUMP BEGIN========================================"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 517 */
try { // :try_start_0
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 518 */
try { // :try_start_1
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I */
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_1 */
/* .line 519 */
final String v1 = "Didn\'t cache the current network state!"; // const-string v1, "Didn\'t cache the current network state!"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 521 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Current Network State: "; // const-string v2, "Current Network State: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", it\'s setter uid:"; // const-string v2, ", it\'s setter uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterUid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " pid:"; // const-string v2, " pid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterPid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 523 */
} // :goto_0
v1 = this.mMiuiFireRuleCache;
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-nez v1, :cond_2 */
/* .line 526 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_1
v2 = this.mMiuiFireRuleCache;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_5 */
/* .line 527 */
v2 = this.mMiuiFireRuleCache;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Ljava/util/Set; */
/* .line 528 */
/* .local v2, "rules":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_3
/* if-nez v3, :cond_3 */
/* .line 529 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Rule type: "; // const-string v4, "Rule type: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mMiuiFireRuleCache;
v4 = (( android.util.SparseArray ) v4 ).keyAt ( v1 ); // invoke-virtual {v4, v1}, Landroid/util/SparseArray;->keyAt(I)I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 530 */
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
/* .line 531 */
/* .local v4, "rule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " Rule detail: "; // const-string v6, " Rule detail: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.net.MiuiNetworkManagementService$MiuiFireRule ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 532 */
} // .end local v4 # "rule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
/* .line 526 */
} // .end local v2 # "rules":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;"
} // :cond_3
/* add-int/lit8 v1, v1, 0x1 */
/* .line 524 */
} // .end local v1 # "i":I
} // :cond_4
} // :goto_3
final String v1 = "Didn\'t cache the miui fire rule!"; // const-string v1, "Didn\'t cache the miui fire rule!"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 536 */
} // :cond_5
/* monitor-exit v0 */
/* .line 539 */
/* .line 536 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkManagementService;
} // .end local p1 # "pw":Ljava/io/PrintWriter;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 537 */
/* .restart local p0 # "this":Lcom/android/server/net/MiuiNetworkManagementService; */
/* .restart local p1 # "pw":Ljava/io/PrintWriter; */
/* :catch_0 */
/* move-exception v0 */
/* .line 538 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "NetworkManagement"; // const-string v1, "NetworkManagement"
final String v2 = "dump"; // const-string v2, "dump"
android.util.Slog .e ( v1,v2,v0 );
/* .line 540 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
final String v0 = "========================================MiuiFireRule DUMP END========================================"; // const-string v0, "========================================MiuiFireRule DUMP END========================================"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 541 */
return;
} // .end method
public Integer enableAutoForward ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "addr" # Ljava/lang/String; */
/* .param p2, "fwmark" # I */
/* .param p3, "enabled" # Z */
/* .line 501 */
int v0 = -1; // const/4 v0, -0x1
try { // :try_start_0
v1 = this.mNetd;
if ( v1 != null) { // if-eqz v1, :cond_0
v0 = /* .line 502 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 507 */
} // :cond_0
/* nop */
/* .line 508 */
/* .line 504 */
/* :catch_0 */
/* move-exception v1 */
/* .line 505 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 506 */
} // .end method
public Boolean enableIptablesRestore ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .line 137 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 139 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 140 */
/* :catch_0 */
/* move-exception v0 */
/* .line 141 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 142 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean enableLimitter ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .line 97 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 99 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 100 */
/* :catch_0 */
/* move-exception v0 */
/* .line 101 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 102 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean enableMobileTrafficLimit ( Boolean p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .param p2, "iface" # Ljava/lang/String; */
/* .line 402 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 404 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 405 */
/* :catch_0 */
/* move-exception v0 */
/* .line 406 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 407 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean enableQos ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .line 382 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 384 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 385 */
/* :catch_0 */
/* move-exception v0 */
/* .line 386 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 387 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean enableRps ( java.lang.String p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .line 311 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 313 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 314 */
/* :catch_0 */
/* move-exception v0 */
/* .line 315 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 316 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean enableWmmer ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .line 87 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 89 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 90 */
/* :catch_0 */
/* move-exception v0 */
/* .line 91 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 92 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
Boolean filterExtendEvent ( Integer p0, java.lang.String p1, java.lang.String[] p2 ) {
/* .locals 1 */
/* .param p1, "code" # I */
/* .param p2, "raw" # Ljava/lang/String; */
/* .param p3, "cooked" # [Ljava/lang/String; */
/* .line 325 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long getMiuiSlmVoipUdpAddress ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 442 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 444 */
try { // :try_start_0
v0 = this.mNetd;
/* move-result-wide v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* return-wide v0 */
/* .line 445 */
/* :catch_0 */
/* move-exception v0 */
/* .line 446 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 447 */
/* const-wide/16 v1, 0x0 */
/* return-wide v1 */
} // .end method
public Integer getMiuiSlmVoipUdpPort ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 452 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 454 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 455 */
/* :catch_0 */
/* move-exception v0 */
/* .line 456 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 457 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Long getShareStats ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 621 */
try { // :try_start_0
v0 = this.mNetd;
/* move-result-wide v0 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* return-wide v0 */
/* .line 622 */
/* :catch_0 */
/* move-exception v0 */
/* .line 623 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 624 */
/* const-wide/16 v1, -0x1 */
/* return-wide v1 */
} // .end method
public Boolean listenUidDataActivity ( Integer p0, Integer p1, Integer p2, Integer p3, Boolean p4 ) {
/* .locals 7 */
/* .param p1, "protocol" # I */
/* .param p2, "uid" # I */
/* .param p3, "type" # I */
/* .param p4, "timeout" # I */
/* .param p5, "listen" # Z */
/* .line 148 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 150 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mNetd;
/* move v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move v5, p4 */
/* move v6, p5 */
v1 = /* invoke-interface/range {v1 ..v6}, Lcom/android/internal/net/IOemNetd;->listenUidDataActivity(IIIIZ)Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 154 */
/* .local v1, "res":Z */
/* nop */
/* .line 155 */
if ( p5 != null) { // if-eqz p5, :cond_0
/* .line 156 */
v2 = this.mListenedIdleTimerType;
java.lang.Integer .valueOf ( p3 );
/* .line 158 */
} // :cond_0
v2 = this.mListenedIdleTimerType;
java.lang.Integer .valueOf ( p3 );
/* .line 160 */
} // :goto_0
/* .line 151 */
} // .end local v1 # "res":Z
/* :catch_0 */
/* move-exception v1 */
/* .line 152 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 153 */
} // .end method
Boolean miuiNotifyInterfaceClassActivity ( Integer p0, Boolean p1, Long p2, Integer p3, Boolean p4 ) {
/* .locals 8 */
/* .param p1, "type" # I */
/* .param p2, "isActive" # Z */
/* .param p3, "tsNanos" # J */
/* .param p5, "uid" # I */
/* .param p6, "fromRadio" # Z */
/* .line 330 */
v0 = this.mListenedIdleTimerType;
v0 = java.lang.Integer .valueOf ( p1 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 331 */
/* .line 334 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->convertTypeToLable(I)Ljava/lang/String; */
/* .line 335 */
/* .local v0, "lable":Ljava/lang/String; */
/* if-nez v0, :cond_1 */
/* .line 336 */
/* .line 338 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " <====> "; // const-string v2, " <====> "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
android.util.Log .d ( v2,v1 );
/* .line 340 */
v2 = this.mObserver;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 341 */
/* move-object v3, v0 */
/* move v4, p5 */
/* move v5, p2 */
/* move-wide v6, p3 */
/* invoke-interface/range {v2 ..v7}, Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;->uidDataActivityChanged(Ljava/lang/String;IZJ)V */
/* .line 344 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void modifySuspendBaseTime ( ) {
/* .locals 9 */
/* .line 283 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 285 */
try { // :try_start_0
/* const-string/jumbo v0, "suspend_control" */
android.os.ServiceManager .getService ( v0 );
/* .line 286 */
/* .local v0, "mBinder":Landroid/os/IBinder; */
final String v1 = "android.system.suspend.ISuspendControlService$Stub"; // const-string v1, "android.system.suspend.ISuspendControlService$Stub"
java.lang.Class .forName ( v1 );
/* .line 287 */
/* .local v1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v3 = "asInterface"; // const-string v3, "asInterface"
int v4 = 1; // const/4 v4, 0x1
/* new-array v4, v4, [Ljava/lang/Class; */
/* const-class v5, Landroid/os/IBinder; */
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 288 */
/* .local v3, "asInterface":Ljava/lang/reflect/Method; */
/* filled-new-array {v0}, [Ljava/lang/Object; */
int v5 = 0; // const/4 v5, 0x0
(( java.lang.reflect.Method ) v3 ).invoke ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 289 */
/* .local v4, "ISuspendControlObj":Ljava/lang/Object; */
(( java.lang.Object ) v4 ).getClass ( ); // invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v7 = "modifySuspendTime"; // const-string v7, "modifySuspendTime"
/* new-array v8, v6, [Ljava/lang/Class; */
(( java.lang.Class ) v5 ).getDeclaredMethod ( v7, v8 ); // invoke-virtual {v5, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 290 */
/* .local v5, "modifySuspendTime":Ljava/lang/reflect/Method; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 291 */
/* new-array v6, v6, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v5 ).invoke ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 292 */
} // :cond_0
final String v6 = "modify Suspend Base Time"; // const-string v6, "modify Suspend Base Time"
android.util.Log .e ( v2,v6 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 296 */
/* nop */
} // .end local v0 # "mBinder":Landroid/os/IBinder;
} // .end local v1 # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v3 # "asInterface":Ljava/lang/reflect/Method;
} // .end local v4 # "ISuspendControlObj":Ljava/lang/Object;
} // .end local v5 # "modifySuspendTime":Ljava/lang/reflect/Method;
/* .line 293 */
/* :catch_0 */
/* move-exception v0 */
/* .line 294 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 295 */
final String v1 = "SuspendControlService reflection Exception"; // const-string v1, "SuspendControlService reflection Exception"
android.util.Log .e ( v2,v1 );
/* .line 297 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean setCurrentNetworkState ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "state" # I */
/* .line 300 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 302 */
try { // :try_start_0
v0 = android.os.Binder .getCallingUid ( );
v1 = android.os.Binder .getCallingPid ( );
/* invoke-direct {p0, p1, v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setCurrentNetworkState(III)V */
/* .line 303 */
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 304 */
/* :catch_0 */
/* move-exception v0 */
/* .line 305 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 306 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean setLimit ( Boolean p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* .line 127 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 129 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 130 */
/* :catch_0 */
/* move-exception v0 */
/* .line 131 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 132 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean setMiuiFirewallRule ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "rule" # I */
/* .param p4, "type" # I */
/* .line 197 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 199 */
try { // :try_start_0
v8 = android.os.Binder .getCallingUid ( );
v9 = android.os.Binder .getCallingPid ( );
/* move-object v3, p0 */
/* move-object v4, p1 */
/* move v5, p2 */
/* move v6, p3 */
/* move v7, p4 */
/* invoke-direct/range {v3 ..v9}, Lcom/android/server/net/MiuiNetworkManagementService;->setMiuiFirewallRule(Ljava/lang/String;IIIII)V */
/* .line 200 */
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 201 */
/* :catch_0 */
/* move-exception v0 */
/* .line 202 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 203 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void setMiuiSlmBpfUid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 432 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 435 */
try { // :try_start_0
v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 438 */
/* .line 436 */
/* :catch_0 */
/* move-exception v0 */
/* .line 437 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 439 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean setMobileTrafficLimit ( Boolean p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* .line 412 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 414 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 415 */
/* :catch_0 */
/* move-exception v0 */
/* .line 416 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 417 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void setNetworkEventObserver ( com.android.server.net.MiuiNetworkManagementService$NetworkEventObserver p0 ) {
/* .locals 0 */
/* .param p1, "observer" # Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver; */
/* .line 321 */
this.mObserver = p1;
/* .line 322 */
return;
} // .end method
protected void setOemNetd ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p1, "ib" # Landroid/os/IBinder; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 79 */
this.mBinder = p1;
/* .line 81 */
com.android.internal.net.IOemNetd$Stub .asInterface ( p1 );
this.mNetd = v0;
/* .line 82 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;-><init>(Lcom/android/server/net/MiuiNetworkManagementService;Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener-IA;)V */
this.mListenter = v0;
/* .line 83 */
v1 = this.mNetd;
/* .line 84 */
return;
} // .end method
public void setPidForPackage ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .param p3, "uid" # I */
/* .line 188 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 190 */
try { // :try_start_0
v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 193 */
/* .line 191 */
/* :catch_0 */
/* move-exception v0 */
/* .line 192 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 194 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean setQos ( Integer p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "protocol" # I */
/* .param p2, "uid" # I */
/* .param p3, "tos" # I */
/* .param p4, "add" # Z */
/* .line 392 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 394 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 395 */
/* :catch_0 */
/* move-exception v0 */
/* .line 396 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 397 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void updateAurogonUidRule ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "allow" # Z */
/* .line 472 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 474 */
/* nop */
/* .line 480 */
/* nop */
/* .line 481 */
return;
} // .end method
public Boolean updateIface ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .line 165 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 167 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = v1 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 171 */
/* .local v1, "res":Z */
/* nop */
/* .line 172 */
/* const-string/jumbo v2, "wlan" */
v2 = (( java.lang.String ) p1 ).startsWith ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 173 */
v2 = this.mListenedIdleTimerType;
int v3 = 1; // const/4 v3, 0x1
java.lang.Integer .valueOf ( v3 );
/* .line 174 */
} // :cond_0
/* .line 168 */
} // .end local v1 # "res":Z
/* :catch_0 */
/* move-exception v1 */
/* .line 169 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 170 */
} // .end method
public Boolean updateWmm ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "wmm" # I */
/* .line 107 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 109 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 110 */
/* :catch_0 */
/* move-exception v0 */
/* .line 111 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 112 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean whiteListUid ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* .line 117 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 119 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 120 */
/* :catch_0 */
/* move-exception v0 */
/* .line 121 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 122 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean whiteListUidForMobileTraffic ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* .line 422 */
v0 = this.mContext;
final String v1 = "android.permission.CONNECTIVITY_INTERNAL"; // const-string v1, "android.permission.CONNECTIVITY_INTERNAL"
final String v2 = "NetworkManagement"; // const-string v2, "NetworkManagement"
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 424 */
try { // :try_start_0
v0 = v0 = this.mNetd;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 425 */
/* :catch_0 */
/* move-exception v0 */
/* .line 426 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 427 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
