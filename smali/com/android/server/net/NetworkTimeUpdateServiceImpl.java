public class com.android.server.net.NetworkTimeUpdateServiceImpl implements com.android.server.net.NetworkTimeUpdateServiceStub {
	 /* .source "NetworkTimeUpdateServiceImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static java.lang.String CN_NTP_SERVER;
	 private static final Boolean DBG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private java.lang.String mDefaultNtpServer;
	 private java.util.ArrayList mNtpServers;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.util.NtpTrustedTime mTime;
/* # direct methods */
static com.android.server.net.NetworkTimeUpdateServiceImpl ( ) {
/* .locals 1 */
/* .line 21 */
final String v0 = "pool.ntp.org"; // const-string v0, "pool.ntp.org"
return;
} // .end method
public com.android.server.net.NetworkTimeUpdateServiceImpl ( ) {
/* .locals 1 */
/* .line 18 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 23 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mNtpServers = v0;
return;
} // .end method
private void initDefaultNtpServer ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 57 */
/* if-nez p1, :cond_0 */
return;
/* .line 59 */
} // :cond_0
this.mContext = p1;
/* .line 60 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 61 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 62 */
/* .local v1, "res":Landroid/content/res/Resources; */
/* const-string/jumbo v2, "time.android.com" */
/* .line 64 */
/* .local v2, "defaultServer":Ljava/lang/String; */
final String v3 = "ntp_server"; // const-string v3, "ntp_server"
android.provider.Settings$Global .getString ( v0,v3 );
/* .line 66 */
/* .local v3, "secureServer":Ljava/lang/String; */
/* sget-boolean v4, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 67 */
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* move-object v4, v3 */
} // :cond_1
/* const-string/jumbo v4, "time.android.com" */
} // :goto_0
this.mDefaultNtpServer = v4;
/* .line 69 */
} // :cond_2
v4 = com.android.server.net.NetworkTimeUpdateServiceImpl.CN_NTP_SERVER;
this.mDefaultNtpServer = v4;
/* .line 71 */
} // :goto_1
return;
} // .end method
private Boolean refreshNtpServer ( Integer p0, android.net.Network p1 ) {
/* .locals 5 */
/* .param p1, "tryCounter" # I */
/* .param p2, "network" # Landroid/net/Network; */
/* .line 81 */
int v0 = 0; // const/4 v0, 0x0
/* .line 82 */
/* .local v0, "result":Z */
v1 = this.mNtpServers;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* rem-int v1, p1, v1 */
/* .line 83 */
/* .local v1, "index":I */
v2 = this.mNtpServers;
(( java.util.ArrayList ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 84 */
/* .local v2, "ntpServer":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "tryCounter = " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ",ntpServers = "; // const-string v4, ",ntpServers = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "NetworkTimeUpdateService"; // const-string v4, "NetworkTimeUpdateService"
android.util.Log .d ( v4,v3 );
/* .line 86 */
/* invoke-direct {p0, v2}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->setNtpServer(Ljava/lang/String;)V */
/* .line 87 */
v3 = this.mTime;
v0 = (( android.util.NtpTrustedTime ) v3 ).forceRefresh ( p2 ); // invoke-virtual {v3, p2}, Landroid/util/NtpTrustedTime;->forceRefresh(Landroid/net/Network;)Z
/* .line 88 */
v3 = this.mDefaultNtpServer;
/* invoke-direct {p0, v3}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->setNtpServer(Ljava/lang/String;)V */
/* .line 90 */
} // .end method
private void setNtpServer ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "ntpServer" # Ljava/lang/String; */
/* .line 74 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 75 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "ntp_server"; // const-string v1, "ntp_server"
android.provider.Settings$Global .putString ( v0,v1,p1 );
/* .line 78 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void initNtpServers ( android.content.Context p0, android.util.NtpTrustedTime p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "trustedTime" # Landroid/util/NtpTrustedTime; */
/* .line 30 */
this.mTime = p2;
/* .line 31 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->initDefaultNtpServer(Landroid/content/Context;)V */
/* .line 32 */
v0 = this.mNtpServers;
v1 = this.mDefaultNtpServer;
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 33 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x11030081 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 35 */
/* .local v0, "globalNtpServers":[Ljava/lang/String; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x1103001a */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 37 */
/* .local v1, "chinaNtpServers":[Ljava/lang/String; */
/* array-length v2, v0 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_0 */
/* aget-object v5, v0, v4 */
/* .line 38 */
/* .local v5, "ntpServer":Ljava/lang/String; */
v6 = this.mNtpServers;
(( java.util.ArrayList ) v6 ).add ( v5 ); // invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 37 */
} // .end local v5 # "ntpServer":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 40 */
} // :cond_0
/* sget-boolean v2, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z */
/* if-nez v2, :cond_1 */
/* .line 41 */
/* array-length v2, v1 */
} // :goto_1
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, v1, v3 */
/* .line 42 */
/* .local v4, "ntpServer":Ljava/lang/String; */
v5 = this.mNtpServers;
(( java.util.ArrayList ) v5 ).add ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 41 */
} // .end local v4 # "ntpServer":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 45 */
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "the servers are " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mNtpServers;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "NetworkTimeUpdateService"; // const-string v3, "NetworkTimeUpdateService"
android.util.Log .d ( v3,v2 );
/* .line 46 */
return;
} // .end method
public Boolean switchNtpServer ( Integer p0, android.util.NtpTrustedTime p1, android.net.Network p2 ) {
/* .locals 1 */
/* .param p1, "tryCounter" # I */
/* .param p2, "trustedTime" # Landroid/util/NtpTrustedTime; */
/* .param p3, "network" # Landroid/net/Network; */
/* .line 50 */
v0 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->refreshNtpServer(ILandroid/net/Network;)Z */
/* if-nez v0, :cond_0 */
/* .line 51 */
v0 = this.mTime;
v0 = (( android.util.NtpTrustedTime ) v0 ).forceRefresh ( p3 ); // invoke-virtual {v0, p3}, Landroid/util/NtpTrustedTime;->forceRefresh(Landroid/net/Network;)Z
/* .line 53 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
