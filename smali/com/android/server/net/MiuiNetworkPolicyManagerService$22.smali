.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;
.super Landroid/database/ContentObserver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerMiuiOptimizationChangedObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1474
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .line 1477
    nop

    .line 1478
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1477
    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "persist.sys.miui_optimization"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/2addr v0, v1

    .line 1479
    .local v0, "isCtsMode":Z
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mnetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v2

    .line 1480
    .local v2, "networkPriority":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "miui optimization mode changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", current network priority: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiNetworkPolicy"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1482
    const/16 v3, 0xff

    if-eqz v0, :cond_0

    .line 1483
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableNetworkPriority(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    .line 1484
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableWmmer(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1485
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->enableIptablesRestore(Z)Z

    goto :goto_0

    .line 1487
    :cond_0
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableIptablesRestore(Z)Z

    .line 1488
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableWmmer(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1489
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eq v2, v3, :cond_1

    .line 1490
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableNetworkPriority(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    .line 1493
    :cond_1
    :goto_0
    return-void
.end method
