class com.android.server.net.MiuiNetworkPolicyManagerService$24 implements android.os.Handler$Callback {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$24 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 1522 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean handleMessage ( android.os.Message p0 ) {
/* .locals 8 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1525 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = " mWifiConnected="; // const-string v1, " mWifiConnected="
final String v2 = " mNetworkPriorityMode ="; // const-string v2, " mNetworkPriorityMode ="
int v3 = 4; // const/4 v3, 0x4
int v4 = 5; // const/4 v4, 0x5
final String v5 = "MiuiNetworkPolicy"; // const-string v5, "MiuiNetworkPolicy"
int v6 = 0; // const/4 v6, 0x0
int v7 = 1; // const/4 v7, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_4 */
/* .line 1622 */
/* :pswitch_0 */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmThermalForceMode ( v0 );
/* .line 1623 */
/* .local v0, "lastThermalForceMode":I */
v3 = this.this$0;
/* iget v4, p1, Landroid/os/Message;->arg1:I */
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmThermalForceMode ( v3,v4 );
/* .line 1624 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mnetworkPriorityMode ( v3 );
/* .line 1625 */
/* .local v3, "_networkPriority":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "thermal networkPriorityMode = " */
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " mThermalForceMode="; // const-string v2, " mThermalForceMode="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmThermalForceMode ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v1 );
/* .line 1627 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 v1 = this.this$0;
	 v1 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v1 );
	 /* if-eq v3, v1, :cond_0 */
	 /* .line 1628 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableNetworkPriority ( v1,v3 );
	 /* .line 1630 */
} // :cond_0
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmThermalForceMode ( v1 );
/* if-eq v0, v1, :cond_1 */
/* .line 1632 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkBoostService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 1633 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkBoostService ( v1 );
	 v2 = this.this$0;
	 v2 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v2 );
	 v4 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmTrafficPolicyMode ( v4 );
	 v5 = this.this$0;
	 v5 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmThermalForceMode ( v5 );
	 (( com.xiaomi.NetworkBoost.NetworkBoostService ) v1 ).onNetworkPriorityChanged ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V
	 /* .line 1636 */
} // :cond_1
} // :goto_0
/* .line 1619 */
} // .end local v0 # "lastThermalForceMode":I
} // .end local v3 # "_networkPriority":I
/* :pswitch_1 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* if-ne v1, v7, :cond_2 */
/* move v6, v7 */
} // :cond_2
/* iget v1, p1, Landroid/os/Message;->arg2:I */
/* int-to-long v1, v1 */
(( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).setTrafficControllerForMobile ( v6, v1, v2 ); // invoke-virtual {v0, v6, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setTrafficControllerForMobile(ZJ)I
/* .line 1620 */
/* .line 1616 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateRuleForMobileTc ( v0 );
/* .line 1617 */
/* .line 1608 */
/* :pswitch_3 */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1609 */
v0 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mcheckRouterMTK ( v0 );
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmIsMtkRouter ( v0,v1 );
/* .line 1610 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetRouterModel ( v0 );
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmConnectedApModel ( v0,v1 );
/* .line 1611 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "detect again, router is mtk:"; // const-string v1, "detect again, router is mtk:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmIsMtkRouter ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v5,v0 );
/* .line 1612 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableWmmer ( v0 );
/* .line 1614 */
} // :cond_3
/* .line 1602 */
/* :pswitch_4 */
v0 = this.obj;
/* instance-of v0, v0, Ljava/lang/Boolean; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1603 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmRpsEnabled ( v0,v1 );
/* .line 1604 */
v0 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmRpsEnabled ( v0 );
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableRps ( v0,v1 );
/* .line 1606 */
} // :cond_4
/* .line 1599 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateMobileLatency ( v0 );
/* .line 1600 */
/* .line 1564 */
/* :pswitch_6 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
/* if-ne v2, v7, :cond_5 */
/* move v6, v7 */
} // :cond_5
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdatePowerSaveForUidDataActivityChanged ( v0,v1,v6 );
/* .line 1565 */
/* .line 1567 */
/* :pswitch_7 */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmTrafficPolicyMode ( v0 );
/* .line 1568 */
/* .local v0, "lastTrafficPolicyMode":I */
v3 = this.this$0;
/* iget v4, p1, Landroid/os/Message;->arg1:I */
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmTrafficPolicyMode ( v3,v4 );
/* .line 1569 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mnetworkPriorityMode ( v3 );
/* .line 1570 */
/* .local v3, "networkPriority":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "networkPriorityMode = "; // const-string v6, "networkPriorityMode = "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v4 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v1 );
/* .line 1572 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v1 );
/* if-eq v3, v1, :cond_6 */
/* .line 1573 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableNetworkPriority ( v1,v3 );
/* .line 1575 */
} // :cond_6
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmTrafficPolicyMode ( v1 );
/* if-eq v0, v1, :cond_7 */
/* .line 1577 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkBoostService ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1578 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkBoostService ( v1 );
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v2 );
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmTrafficPolicyMode ( v4 );
v5 = this.this$0;
v5 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmThermalForceMode ( v5 );
(( com.xiaomi.NetworkBoost.NetworkBoostService ) v1 ).onNetworkPriorityChanged ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V
/* .line 1581 */
} // :cond_7
} // :goto_1
/* .line 1595 */
} // .end local v0 # "lastTrafficPolicyMode":I
} // .end local v3 # "networkPriority":I
/* :pswitch_8 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mcalculateBandWidth ( v0 );
/* .line 1596 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v0 );
int v1 = 6; // const/4 v1, 0x6
/* const-wide/16 v2, 0xbb8 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1597 */
/* .line 1589 */
/* :pswitch_9 */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLimitEnabled ( v0 );
/* if-nez v0, :cond_8 */
/* .line 1590 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateLimit ( v0,v7 );
/* .line 1591 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetEnableLimitTimeout ( v1 );
/* int-to-long v1, v1 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1593 */
} // :cond_8
/* .line 1583 */
/* :pswitch_a */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLimitEnabled ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 1584 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateLimit ( v0,v6 );
/* .line 1585 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetDisableLimitTimeout ( v1 );
/* int-to-long v1, v1 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v4, v1, v2 ); // invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1587 */
} // :cond_9
/* .line 1533 */
/* :pswitch_b */
v0 = this.obj;
/* check-cast v0, Ljava/lang/String; */
/* .line 1534 */
/* .local v0, "label":Ljava/lang/String; */
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* .line 1535 */
/* .local v1, "uid":I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
/* if-ne v2, v7, :cond_a */
/* move v2, v7 */
} // :cond_a
/* move v2, v6 */
/* .line 1536 */
/* .local v2, "isActive":Z */
} // :goto_2
v5 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmInterfaceName ( v5 );
v5 = android.text.TextUtils .equals ( v5,v0 );
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 1537 */
if ( v2 != null) { // if-eqz v2, :cond_b
v5 = this.this$0;
v5 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLimitEnabled ( v5 );
/* if-nez v5, :cond_b */
/* .line 1538 */
v5 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v5 );
(( android.os.Handler ) v5 ).removeMessages ( v4 ); // invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1539 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v4 );
v5 = this.this$0;
v5 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetEnableLimitTimeout ( v5 );
/* int-to-long v5, v5 */
(( android.os.Handler ) v4 ).sendEmptyMessageDelayed ( v3, v5, v6 ); // invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1540 */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateLimit ( v3,v7 );
/* goto/16 :goto_3 */
/* .line 1541 */
} // :cond_b
/* if-nez v2, :cond_e */
/* .line 1542 */
v5 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v5 );
(( android.os.Handler ) v5 ).removeMessages ( v4 ); // invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1543 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v4 );
(( android.os.Handler ) v4 ).removeMessages ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1544 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLimitEnabled ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_e
/* .line 1545 */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateLimit ( v3,v6 );
/* .line 1550 */
} // :cond_c
/* const/16 v3, 0x76 */
java.lang.String .valueOf ( v3 );
v3 = android.text.TextUtils .equals ( v3,v0 );
if ( v3 != null) { // if-eqz v3, :cond_e
/* .line 1551 */
/* const/16 v3, 0x8 */
if ( v2 != null) { // if-eqz v2, :cond_d
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmPowerSaveEnabled ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 1552 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v4 );
(( android.os.Handler ) v4 ).removeMessages ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1553 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v4 );
v5 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v5 );
/* iget v6, p1, Landroid/os/Message;->arg2:I */
(( android.os.Handler ) v5 ).obtainMessage ( v3, v1, v6 ); // invoke-virtual {v5, v3, v1, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* const-wide/16 v5, 0x1388 */
(( android.os.Handler ) v4 ).sendMessageDelayed ( v3, v5, v6 ); // invoke-virtual {v4, v3, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1555 */
} // :cond_d
/* if-nez v2, :cond_e */
/* .line 1556 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v4 );
(( android.os.Handler ) v4 ).removeMessages ( v3 ); // invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1557 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmPowerSaveEnabled ( v3 );
/* if-nez v3, :cond_e */
/* .line 1558 */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdatePowerSaveForUidDataActivityChanged ( v3,v1,v2 );
/* .line 1562 */
} // :cond_e
} // :goto_3
/* .line 1530 */
} // .end local v0 # "label":Ljava/lang/String;
} // .end local v1 # "uid":I
} // .end local v2 # "isActive":Z
/* :pswitch_c */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mremoveUidState ( v0,v1 );
/* .line 1531 */
/* .line 1527 */
/* :pswitch_d */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* iget v2, p1, Landroid/os/Message;->arg2:I */
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateUidState ( v0,v1,v2 );
/* .line 1528 */
/* nop */
/* .line 1641 */
} // :goto_4
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
