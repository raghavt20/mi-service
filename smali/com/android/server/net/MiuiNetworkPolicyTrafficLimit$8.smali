.class Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyTrafficLimit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateWhiteListUidForMobileTraffic(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

.field final synthetic val$add:Z

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 354
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$uid:I

    iput-boolean p3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$add:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 357
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$uid:I

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$add:Z

    invoke-virtual {v0, v1, v2}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUidForMobileTraffic(IZ)Z

    move-result v0

    .line 358
    .local v0, "rst":Z
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateWhiteListUidForMobileTraffic rst="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V

    .line 359
    return-void
.end method
