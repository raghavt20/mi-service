.class Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyTrafficLimit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    .line 423
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 426
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 427
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    const-string v1, "networkType"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 429
    .local v1, "networkType":I
    if-nez v1, :cond_0

    .line 430
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    const-string v3, "BroadcastReceiver TYPE_MOBILE"

    invoke-static {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V

    .line 431
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$misAllowedToEnableMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)V

    .line 434
    .end local v1    # "networkType":I
    :cond_0
    return-void
.end method
