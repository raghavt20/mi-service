.class public Lcom/android/server/net/NetworkPolicyManagerServiceImpl;
.super Lcom/android/server/net/NetworkPolicyManagerServiceStub;
.source "NetworkPolicyManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.NetworkManagementServiceStub$$"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NetworkManagementServic"


# instance fields
.field private mMiNMS:Lcom/android/server/net/MiuiNetworkManagementService;

.field mMiuiNetPolicyManager:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/android/server/net/NetworkPolicyManagerServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .line 43
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->make(Landroid/content/Context;)Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerServiceImpl;->mMiuiNetPolicyManager:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 44
    return-void
.end method

.method public initMiuiNMS(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .line 22
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkManagementService;->Init(Landroid/content/Context;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerServiceImpl;->mMiNMS:Lcom/android/server/net/MiuiNetworkManagementService;

    .line 23
    return-void
.end method

.method public miuiNotifyInterfaceClassActivity(IZJIZ)Z
    .locals 7
    .param p1, "type"    # I
    .param p2, "isActive"    # Z
    .param p3, "tsNanos"    # J
    .param p5, "uid"    # I
    .param p6, "fromRadio"    # Z

    .line 28
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerServiceImpl;->mMiNMS:Lcom/android/server/net/MiuiNetworkManagementService;

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/server/net/MiuiNetworkManagementService;->miuiNotifyInterfaceClassActivity(IZJIZ)Z

    move-result v0

    return v0
.end method

.method public setOemNetd(Landroid/net/INetd;)V
    .locals 3
    .param p1, "mNetdService"    # Landroid/net/INetd;

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerServiceImpl;->mMiNMS:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-interface {p1}, Landroid/net/INetd;->getOemNetd()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setOemNetd(Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "NetworkManagementServic"

    const-string v2, "### setOemNetd failed ###"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 39
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public systemReady()V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/android/server/net/NetworkPolicyManagerServiceImpl;->mMiuiNetPolicyManager:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->systemReady()V

    .line 49
    return-void
.end method
