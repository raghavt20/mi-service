class com.android.server.net.MiuiNetworkPolicyManagerService$8 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSmartDnsChangeObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1039 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 1042 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_smartdns_enabled"; // const-string v2, "cloud_smartdns_enabled"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 1044 */
/* .local v1, "enableValue":Ljava/lang/String; */
try { // :try_start_0
	 final String v2 = "persist.device_config.netd_native.smartdns_enable"; // const-string v2, "persist.device_config.netd_native.smartdns_enable"
	 final String v3 = "on"; // const-string v3, "on"
	 v3 = 	 android.text.TextUtils .equals ( v3,v1 );
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 final String v3 = "1"; // const-string v3, "1"
	 } // :cond_0
	 final String v3 = "0"; // const-string v3, "0"
} // :goto_0
android.os.SystemProperties .set ( v2,v3 );
/* .line 1045 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "smartdns enabled = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1048 */
/* .line 1046 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1047 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "Failed to set smartdns support config"; // const-string v3, "Failed to set smartdns support config"
android.util.Log .e ( v0,v3,v2 );
/* .line 1049 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
