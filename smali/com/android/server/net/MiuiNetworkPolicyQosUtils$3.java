class com.android.server.net.MiuiNetworkPolicyQosUtils$3 implements java.lang.Runnable {
	 /* .source "MiuiNetworkPolicyQosUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyQosUtils this$0; //synthetic
final Boolean val$add; //synthetic
final Integer val$protocol; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyQosUtils$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 328 */
this.this$0 = p1;
/* iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I */
/* iput p3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I */
/* iput-boolean p4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 331 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmNetMgrService ( v0 );
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I */
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I */
/* const/16 v3, 0xb8 */
/* iget-boolean v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z */
v0 = (( com.android.server.net.MiuiNetworkManagementService ) v0 ).setQos ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->setQos(IIIZ)Z
/* .line 332 */
/* .local v0, "rst":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setQos rst=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ",add="; // const-string v2, ",add="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ",uid="; // const-string v2, ",uid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ",protocol="; // const-string v2, ",protocol="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicyQosUtils"; // const-string v2, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v2,v1 );
/* .line 333 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 334 */
v1 = this.this$0;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z */
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mupdateDscpStatus ( v1,v2 );
/* .line 335 */
v1 = this.this$0;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z */
/* iget v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I */
/* iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I */
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mupdateQosInfo ( v1,v2,v3,v4 );
/* .line 337 */
} // :cond_0
return;
} // .end method
