class com.android.server.net.MiuiNetworkPolicyManagerService$3 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWmmerEnableChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 901 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 904 */
final String v0 = ""; // const-string v0, ""
/* .line 905 */
/* .local v0, "value":Ljava/lang/String; */
/* const-string/jumbo v1, "vendor" */
miui.util.FeatureParser .getString ( v1 );
final String v2 = "mediatek"; // const-string v2, "mediatek"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = -2; // const/4 v2, -0x2
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 906 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v3 = "cloud_mtk_wmmer_enabled"; // const-string v3, "cloud_mtk_wmmer_enabled"
	 android.provider.Settings$System .getStringForUser ( v1,v3,v2 );
	 /* .line 909 */
} // :cond_0
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "cloud_wmmer_enabled"; // const-string v3, "cloud_wmmer_enabled"
android.provider.Settings$System .getStringForUser ( v1,v3,v2 );
/* .line 912 */
} // :goto_0
v1 = this.this$0;
if ( v0 != null) { // if-eqz v0, :cond_1
final String v2 = "on"; // const-string v2, "on"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 int v2 = 1; // const/4 v2, 0x1
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_1
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmCloudWmmerEnable ( v1,v2 );
/* .line 913 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " wmmer value:"; // const-string v2, " wmmer value:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " mCloudWmmerEnable:"; // const-string v2, " mCloudWmmerEnable:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmCloudWmmerEnable ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .d ( v2,v1 );
/* .line 914 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableWmmer ( v1 );
/* .line 915 */
return;
} // .end method
