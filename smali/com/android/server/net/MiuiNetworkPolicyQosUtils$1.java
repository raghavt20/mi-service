class com.android.server.net.MiuiNetworkPolicyQosUtils$1 extends miui.process.IActivityChangeListener$Stub {
	 /* .source "MiuiNetworkPolicyQosUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyQosUtils this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyQosUtils$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
/* .line 161 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IActivityChangeListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onActivityChanged ( android.content.ComponentName p0, android.content.ComponentName p1 ) {
/* .locals 11 */
/* .param p1, "preName" # Landroid/content/ComponentName; */
/* .param p2, "curName" # Landroid/content/ComponentName; */
/* .line 164 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onActivityChanged pre:"; // const-string v1, "onActivityChanged pre:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ",cur:"; // const-string v1, ",cur:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ",data="; // const-string v1, ",data="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 165 */
if ( p1 != null) { // if-eqz p1, :cond_a
	 /* if-nez p2, :cond_0 */
	 /* goto/16 :goto_5 */
	 /* .line 166 */
} // :cond_0
(( android.content.ComponentName ) p2 ).toString ( ); // invoke-virtual {p2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;
/* .line 167 */
/* .local v0, "cur":Ljava/lang/String; */
(( android.content.ComponentName ) p1 ).toString ( ); // invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;
/* .line 168 */
/* .local v2, "pre":Ljava/lang/String; */
v3 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetmMeetingAppsAN ( );
/* .line 169 */
/* .local v3, "size":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
/* if-ge v4, v3, :cond_3 */
/* .line 170 */
v7 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v7, :cond_1 */
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetmMeetingAppsAN ( );
/* check-cast v7, Ljava/lang/CharSequence; */
v7 = (( java.lang.String ) v0 ).contains ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
v7 = this.this$0;
v7 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_1
	 /* .line 171 */
	 final String v5 = "onActivityChanged setQos true"; // const-string v5, "onActivityChanged setQos true"
	 android.util.Log .i ( v1,v5 );
	 /* .line 172 */
	 v5 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mupdateVoipStatus ( v5,v0,v6,v4 );
	 /* .line 173 */
	 v5 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetmMeetingAppsPN ( );
	 /* check-cast v7, Ljava/lang/String; */
	 v7 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mgetUidFromMap ( v5,v7 );
	 v8 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetUDP ( );
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$msetQos ( v5,v6,v7,v8 );
	 /* .line 174 */
} // :cond_1
v6 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v6, :cond_2 */
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetmMeetingAppsAN ( );
/* check-cast v6, Ljava/lang/CharSequence; */
v6 = (( java.lang.String ) v2 ).contains ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
	 /* .line 175 */
	 final String v6 = "onActivityChanged setQos false"; // const-string v6, "onActivityChanged setQos false"
	 android.util.Log .i ( v1,v6 );
	 /* .line 176 */
	 v6 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mupdateVoipStatus ( v6,v2,v5,v4 );
	 /* .line 177 */
	 v6 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetmMeetingAppsPN ( );
	 /* check-cast v7, Ljava/lang/String; */
	 v7 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mgetUidFromMap ( v6,v7 );
	 v8 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetUDP ( );
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$msetQos ( v6,v5,v7,v8 );
	 /* .line 169 */
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 180 */
} // .end local v4 # "i":I
} // :cond_3
v4 = android.text.TextUtils .isEmpty ( v0 );
final String v7 = "com.tencent.mm.plugin.scanner.ui.BaseScanUI"; // const-string v7, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"
/* if-nez v4, :cond_4 */
v4 = (( java.lang.String ) v0 ).contains ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 182 */
final String v4 = "onActivityChanged enter ScanQR"; // const-string v4, "onActivityChanged enter ScanQR"
android.util.Log .i ( v1,v4 );
/* .line 183 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$menableLLMMode ( v1,v6,v6 );
/* .line 185 */
} // :cond_4
v4 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v4, :cond_5 */
v4 = (( java.lang.String ) v2 ).contains ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 186 */
final String v4 = "onActivityChanged exit ScanQR"; // const-string v4, "onActivityChanged exit ScanQR"
android.util.Log .i ( v1,v4 );
/* .line 187 */
v1 = this.this$0;
int v4 = -1; // const/4 v4, -0x1
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$menableLLMMode ( v1,v5,v4 );
/* .line 191 */
} // :cond_5
} // :goto_2
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 192 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v4 = "com.android.phone.intent.action.ACTIVITY_STATUS"; // const-string v4, "com.android.phone.intent.action.ACTIVITY_STATUS"
(( android.content.Intent ) v1 ).setAction ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 193 */
final String v4 = "com.android.phone"; // const-string v4, "com.android.phone"
(( android.content.Intent ) v1 ).setPackage ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 194 */
int v4 = 0; // const/4 v4, 0x0
/* .line 195 */
/* .local v4, "changed":Z */
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$sfgetmActivityStatusScene ( );
(( java.util.ArrayList ) v7 ).iterator ( ); // invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v8 = } // :goto_3
if ( v8 != null) { // if-eqz v8, :cond_8
/* check-cast v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene; */
/* .line 196 */
/* .local v8, "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene; */
v9 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v9, :cond_6 */
v9 = this.this$0;
v10 = this.EF_AN;
v9 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$misActivityNameContains ( v9,v0,v10 );
if ( v9 != null) { // if-eqz v9, :cond_6
v9 = this.this$0;
v9 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 197 */
v9 = this.sceneStatusExtra;
(( android.content.Intent ) v1 ).putExtra ( v9, v6 ); // invoke-virtual {v1, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 198 */
int v4 = 1; // const/4 v4, 0x1
/* .line 199 */
} // :cond_6
v9 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v9, :cond_7 */
v9 = this.this$0;
v10 = this.EF_AN;
v9 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$misActivityNameContains ( v9,v2,v10 );
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 200 */
v9 = this.sceneStatusExtra;
(( android.content.Intent ) v1 ).putExtra ( v9, v5 ); // invoke-virtual {v1, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 201 */
int v4 = 1; // const/4 v4, 0x1
/* .line 203 */
} // .end local v8 # "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
} // :cond_7
} // :goto_4
/* .line 204 */
} // :cond_8
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 205 */
v5 = this.this$0;
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmContext ( v5 );
v6 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v5 ).sendBroadcastAsUser ( v1, v6 ); // invoke-virtual {v5, v1, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 207 */
} // :cond_9
return;
/* .line 165 */
} // .end local v0 # "cur":Ljava/lang/String;
} // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "pre":Ljava/lang/String;
} // .end local v3 # "size":I
} // .end local v4 # "changed":Z
} // :cond_a
} // :goto_5
return;
} // .end method
